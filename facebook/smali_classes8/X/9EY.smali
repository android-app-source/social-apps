.class public final LX/9EY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/8pz;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Ea;


# direct methods
.method public constructor <init>(LX/9Ea;)V
    .locals 0

    .prologue
    .line 1457101
    iput-object p1, p0, LX/9EY;->a:LX/9Ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1457102
    check-cast p1, LX/8pz;

    .line 1457103
    iget-object v0, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v0

    .line 1457104
    iget-object v1, p0, LX/9EY;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1, v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    .line 1457105
    iget-object v2, p0, LX/9EY;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->m:LX/9EZ;

    invoke-virtual {v2, v0}, LX/9EZ;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457106
    iget-object v2, p0, LX/9EY;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->f:LX/9Do;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/9EY;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->c:LX/9CC;

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 1457107
    iget-object v1, p0, LX/9EY;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->f:LX/9Do;

    invoke-virtual {v1, v0}, LX/9Do;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457108
    iget-object v1, p0, LX/9EY;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->c:LX/9CC;

    invoke-virtual {v1, v0}, LX/9CC;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457109
    iget-object v0, p0, LX/9EY;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "live_comment"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1457110
    :cond_0
    return-void
.end method
