.class public final LX/A3E;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1615833
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1615834
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1615835
    if-eqz v0, :cond_0

    .line 1615836
    const-string v1, "electoral_vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615837
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1615838
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1615839
    if-eqz v0, :cond_1

    .line 1615840
    const-string v1, "fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615841
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1615842
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1615843
    if-eqz v0, :cond_2

    .line 1615844
    const-string v1, "is_winner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615845
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1615846
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1615847
    if-eqz v0, :cond_3

    .line 1615848
    const-string v1, "last_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615849
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1615850
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1615851
    if-eqz v0, :cond_4

    .line 1615852
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615853
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1615854
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1615855
    if-eqz v0, :cond_5

    .line 1615856
    const-string v1, "photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615857
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1615858
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1615859
    if-eqz v0, :cond_6

    .line 1615860
    const-string v1, "vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615861
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1615862
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1615863
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1615864
    const/4 v9, 0x0

    .line 1615865
    const/4 v8, 0x0

    .line 1615866
    const/4 v7, 0x0

    .line 1615867
    const/4 v6, 0x0

    .line 1615868
    const/4 v5, 0x0

    .line 1615869
    const/4 v4, 0x0

    .line 1615870
    const/4 v3, 0x0

    .line 1615871
    const/4 v2, 0x0

    .line 1615872
    const/4 v1, 0x0

    .line 1615873
    const/4 v0, 0x0

    .line 1615874
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1615875
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1615876
    const/4 v0, 0x0

    .line 1615877
    :goto_0
    return v0

    .line 1615878
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1615879
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 1615880
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1615881
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1615882
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1615883
    const-string v11, "electoral_vote_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1615884
    const/4 v2, 0x1

    .line 1615885
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 1615886
    :cond_2
    const-string v11, "fbid"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1615887
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1615888
    :cond_3
    const-string v11, "is_winner"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1615889
    const/4 v1, 0x1

    .line 1615890
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 1615891
    :cond_4
    const-string v11, "last_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1615892
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1615893
    :cond_5
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1615894
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1615895
    :cond_6
    const-string v11, "photo_uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1615896
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1615897
    :cond_7
    const-string v11, "vote_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1615898
    const/4 v0, 0x1

    .line 1615899
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    goto/16 :goto_1

    .line 1615900
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1615901
    if-eqz v2, :cond_9

    .line 1615902
    const/4 v2, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v2, v9, v10}, LX/186;->a(III)V

    .line 1615903
    :cond_9
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1615904
    if-eqz v1, :cond_a

    .line 1615905
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 1615906
    :cond_a
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1615907
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1615908
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1615909
    if-eqz v0, :cond_b

    .line 1615910
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 1615911
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
