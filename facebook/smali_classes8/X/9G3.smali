.class public final LX/9G3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9G4;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/9FA;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;LX/9G4;Ljava/lang/String;LX/9FA;)V
    .locals 0

    .prologue
    .line 1459259
    iput-object p1, p0, LX/9G3;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    iput-object p2, p0, LX/9G3;->a:LX/9G4;

    iput-object p3, p0, LX/9G3;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9G3;->c:LX/9FA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x61836c40

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1459260
    iget-object v0, p0, LX/9G3;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    iget-object v1, p0, LX/9G3;->a:LX/9G4;

    iget-object v1, v1, LX/9G4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iget-object v2, p0, LX/9G3;->a:LX/9G4;

    iget-object v2, v2, LX/9G4;->b:Lcom/facebook/graphql/model/GraphQLPage;

    iget-object v3, p0, LX/9G3;->b:Ljava/lang/String;

    .line 1459261
    if-nez v1, :cond_5

    .line 1459262
    const/4 v4, 0x0

    .line 1459263
    :goto_0
    move-object v8, v4

    .line 1459264
    iget-object v0, p0, LX/9G3;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    iget-object v1, p0, LX/9G3;->a:LX/9G4;

    iget-object v1, v1, LX/9G4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v2, p0, LX/9G3;->a:LX/9G4;

    iget-object v2, v2, LX/9G4;->b:Lcom/facebook/graphql/model/GraphQLPage;

    iget-object v3, p0, LX/9G3;->b:Ljava/lang/String;

    .line 1459265
    if-nez v1, :cond_6

    .line 1459266
    const/4 v4, 0x0

    .line 1459267
    :goto_1
    move-object v9, v4

    .line 1459268
    iget-object v0, p0, LX/9G3;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->b:LX/8xt;

    iget-object v1, p0, LX/9G3;->b:Ljava/lang/String;

    iget-object v2, p0, LX/9G3;->a:LX/9G4;

    iget-object v2, v2, LX/9G4;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/9G3;->a:LX/9G4;

    iget-object v3, v3, LX/9G4;->b:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v8, :cond_1

    .line 1459269
    iget-object v4, v8, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1459270
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_2
    if-eqz v9, :cond_0

    iget-object v5, v9, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    :cond_0
    new-instance v6, LX/9G2;

    invoke-direct {v6, p0}, LX/9G2;-><init>(LX/9G3;)V

    invoke-virtual/range {v0 .. v6}, LX/8xt;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLComment;LX/0TF;)V

    .line 1459271
    iget-object v0, p0, LX/9G3;->c:LX/9FA;

    .line 1459272
    iget-boolean v1, v0, LX/9FA;->c:Z

    move v0, v1

    .line 1459273
    if-eqz v0, :cond_3

    .line 1459274
    if-nez v8, :cond_2

    .line 1459275
    const v0, -0x7f5bf90e

    invoke-static {v0, v7}, LX/02F;->a(II)V

    .line 1459276
    :goto_3
    return-void

    :cond_1
    move-object v4, v5

    .line 1459277
    goto :goto_2

    .line 1459278
    :cond_2
    invoke-static {v8}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1459279
    iget-object v1, p0, LX/9G3;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->e:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1459280
    :goto_4
    const v0, 0x7d518bd

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_3

    .line 1459281
    :cond_3
    if-nez v9, :cond_4

    .line 1459282
    const v0, -0x634670b4

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_3

    .line 1459283
    :cond_4
    iget-object v0, p0, LX/9G3;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->f:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, v9, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, v9, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_4

    :cond_5
    iget-object v4, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->c:LX/189;

    invoke-virtual {v4, v1, v2, v3}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    goto/16 :goto_0

    :cond_6
    iget-object v4, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->d:LX/20j;

    invoke-virtual {v4, v1, v2, v3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)LX/6PS;

    move-result-object v4

    goto :goto_1
.end method
