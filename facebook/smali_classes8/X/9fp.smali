.class public final LX/9fp;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Vd;

.field public final synthetic b:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/utils/FetchImageUtils;LX/0Vd;)V
    .locals 0

    .prologue
    .line 1522666
    iput-object p1, p0, LX/9fp;->b:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    iput-object p2, p0, LX/9fp;->a:LX/0Vd;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1522667
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1522668
    :cond_0
    :goto_0
    return-void

    .line 1522669
    :cond_1
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1522670
    if-eqz v0, :cond_0

    .line 1522671
    new-instance v2, LX/1lZ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1FK;

    invoke-direct {v2, v1}, LX/1lZ;-><init>(LX/1FK;)V

    .line 1522672
    invoke-static {v2}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v1

    .line 1522673
    :try_start_0
    iget-object v3, v1, LX/1lW;->b:Ljava/lang/String;

    move-object v1, v3

    .line 1522674
    if-nez v1, :cond_2

    .line 1522675
    sget-object v1, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->a:Ljava/lang/String;

    const-string v3, "Unknown image format"

    invoke-static {v1, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522676
    iget-object v1, p0, LX/9fp;->a:LX/0Vd;

    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Unknown image format"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1522677
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1522678
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1522679
    :cond_2
    :try_start_1
    iget-object v3, p0, LX/9fp;->b:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    iget-object v3, v3, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->d:LX/1Er;

    const-string v4, "edit_gallery_fetch_image_temp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v5, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v3, v4, v1, v5}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v1

    .line 1522680
    invoke-static {v2, v1}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b(Ljava/io/InputStream;Ljava/io/File;)V

    .line 1522681
    iget-object v3, p0, LX/9fp;->a:LX/0Vd;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1522682
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1522683
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1522684
    :catch_0
    move-exception v1

    .line 1522685
    :try_start_2
    iget-object v3, p0, LX/9fp;->a:LX/0Vd;

    new-instance v4, Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1522686
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1522687
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1522688
    :catch_1
    move-exception v1

    .line 1522689
    :try_start_3
    iget-object v3, p0, LX/9fp;->a:LX/0Vd;

    new-instance v4, Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1522690
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1522691
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_0

    .line 1522692
    :catchall_0
    move-exception v1

    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1522693
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1
.end method

.method public final f(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1522694
    iget-object v0, p0, LX/9fp;->a:LX/0Vd;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Failed saving photo"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V

    .line 1522695
    return-void
.end method
