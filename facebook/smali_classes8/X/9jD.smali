.class public LX/9jD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1529273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1529274
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1529275
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    move v0, v0

    .line 1529276
    if-eqz v0, :cond_1

    .line 1529277
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v0

    .line 1529278
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1529279
    new-instance v1, LX/9jF;

    invoke-direct {v1, p1}, LX/9jF;-><init>(Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)V

    move-object v1, v1

    .line 1529280
    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    sget-object v3, LX/5RI;->PLACE_PICKER:LX/5RI;

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPicker(LX/5RI;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 1529281
    iput-object v0, v1, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1529282
    move-object v0, v1

    .line 1529283
    iget-object v1, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1529284
    if-nez v1, :cond_0

    .line 1529285
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1529286
    iput-object v1, v0, LX/9jF;->g:Ljava/lang/String;

    .line 1529287
    :cond_0
    invoke-virtual {v0}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object p1

    .line 1529288
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1529289
    const-string v1, "com.facebook.places.checkin.activity.SelectAtTagActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 1529290
    const-string v1, "place_picker_configuration"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1529291
    return-object v0
.end method
