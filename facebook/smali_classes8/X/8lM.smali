.class public LX/8lM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private a:LX/8lU;

.field private b:I

.field private c:Z

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(LX/8lU;II)V
    .locals 0

    .prologue
    .line 1397724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1397725
    iput-object p1, p0, LX/8lM;->a:LX/8lU;

    .line 1397726
    iput p2, p0, LX/8lM;->d:I

    .line 1397727
    iput p3, p0, LX/8lM;->e:I

    .line 1397728
    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1397729
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1397730
    :goto_0
    return-void

    .line 1397731
    :cond_0
    invoke-virtual {p1, v4}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1397732
    iget v1, p0, LX/8lM;->d:I

    .line 1397733
    iget v2, p0, LX/8lM;->e:I

    .line 1397734
    add-int/lit8 v3, p2, 0x1

    div-int v1, v3, v1

    .line 1397735
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 1397736
    mul-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    .line 1397737
    iget-boolean v1, p0, LX/8lM;->c:Z

    if-eqz v1, :cond_1

    .line 1397738
    iget-object v1, p0, LX/8lM;->a:LX/8lU;

    iget v2, p0, LX/8lM;->b:I

    sub-int v2, v0, v2

    invoke-virtual {v1, v0, v2}, LX/8lU;->a(II)V

    .line 1397739
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8lM;->c:Z

    .line 1397740
    iput v0, p0, LX/8lM;->b:I

    goto :goto_0

    .line 1397741
    :cond_1
    iget-object v1, p0, LX/8lM;->a:LX/8lU;

    invoke-virtual {v1, v0, v4}, LX/8lU;->a(II)V

    goto :goto_1
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 1397742
    if-nez p2, :cond_1

    .line 1397743
    iget-object v0, p0, LX/8lM;->a:LX/8lU;

    iget v1, p0, LX/8lM;->b:I

    .line 1397744
    iget-object v2, v0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v2, v2, Lcom/facebook/stickers/search/StickerSearchContainer;->y:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1397745
    iget-object p1, v0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object p1, p1, Lcom/facebook/stickers/search/StickerSearchContainer;->y:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result p1

    neg-int p2, v2

    div-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    cmpl-float p1, p1, p2

    if-gtz p1, :cond_0

    if-ge v1, v2, :cond_2

    .line 1397746
    :cond_0
    iget-object v2, v0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    const/4 p1, 0x0

    sget-object p2, LX/8lf;->SPRING_TO_VALUE:LX/8lf;

    invoke-static {v2, p1, p2}, Lcom/facebook/stickers/search/StickerSearchContainer;->a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;FLX/8lf;)V

    .line 1397747
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8lM;->c:Z

    .line 1397748
    :cond_1
    return-void

    .line 1397749
    :cond_2
    iget-object p1, v0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    neg-int v2, v2

    int-to-float v2, v2

    sget-object p2, LX/8lf;->SPRING_TO_VALUE:LX/8lf;

    invoke-static {p1, v2, p2}, Lcom/facebook/stickers/search/StickerSearchContainer;->a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;FLX/8lf;)V

    goto :goto_0
.end method
