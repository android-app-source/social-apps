.class public LX/8jY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<",
        "LX/8jW;",
        "LX/8jX;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/3dt;

.field private final c:LX/0aG;

.field private final d:Ljava/util/concurrent/Executor;

.field public e:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "LX/8jW;",
            "LX/8jX;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/1Mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1392615
    const-class v0, LX/8jY;

    sput-object v0, LX/8jY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3dt;LX/0aG;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392611
    iput-object p1, p0, LX/8jY;->b:LX/3dt;

    .line 1392612
    iput-object p2, p0, LX/8jY;->c:LX/0aG;

    .line 1392613
    iput-object p3, p0, LX/8jY;->d:Ljava/util/concurrent/Executor;

    .line 1392614
    return-void
.end method

.method public static a(LX/0QB;)LX/8jY;
    .locals 1

    .prologue
    .line 1392616
    invoke-static {p0}, LX/8jY;->b(LX/0QB;)LX/8jY;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8jY;
    .locals 4

    .prologue
    .line 1392608
    new-instance v3, LX/8jY;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v0

    check-cast v0, LX/3dt;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v1, v2}, LX/8jY;-><init>(LX/3dt;LX/0aG;Ljava/util/concurrent/Executor;)V

    .line 1392609
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1392588
    iget-object v0, p0, LX/8jY;->f:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1392589
    iget-object v0, p0, LX/8jY;->f:LX/1Mv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1392590
    :cond_0
    return-void
.end method

.method public final a(LX/8jW;)V
    .locals 4

    .prologue
    .line 1392591
    iget-object v0, p0, LX/8jY;->b:LX/3dt;

    iget-object v1, p1, LX/8jW;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/3dt;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1392592
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p1, LX/8jW;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1392593
    iget-object v1, p0, LX/8jY;->e:LX/3Mb;

    if-eqz v1, :cond_0

    .line 1392594
    iget-object v1, p0, LX/8jY;->e:LX/3Mb;

    new-instance v2, LX/8jX;

    invoke-direct {v2, v0}, LX/8jX;-><init>(Ljava/util/List;)V

    invoke-interface {v1, p1, v2}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1392595
    :cond_0
    :goto_0
    return-void

    .line 1392596
    :cond_1
    new-instance v0, Lcom/facebook/stickers/service/FetchStickersParams;

    iget-object v1, p1, LX/8jW;->a:Ljava/util/List;

    sget-object v2, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v0, v1, v2}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    .line 1392597
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1392598
    const-string v2, "fetchStickersParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1392599
    :try_start_0
    iget-object v0, p0, LX/8jY;->c:LX/0aG;

    const-string v2, "fetch_stickers"

    const v3, 0x5c22d262

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1392600
    new-instance v1, LX/8jV;

    invoke-direct {v1, p0, p1}, LX/8jV;-><init>(LX/8jY;LX/8jW;)V

    .line 1392601
    iget-object v2, p0, LX/8jY;->e:LX/3Mb;

    if-eqz v2, :cond_2

    .line 1392602
    iget-object v2, p0, LX/8jY;->e:LX/3Mb;

    invoke-interface {v2, p1, v0}, LX/3Mb;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1392603
    :cond_2
    iget-object v2, p0, LX/8jY;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1392604
    invoke-static {v0, v1}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v0

    iput-object v0, p0, LX/8jY;->f:LX/1Mv;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1392605
    :catch_0
    move-exception v0

    .line 1392606
    iget-object v1, p0, LX/8jY;->e:LX/3Mb;

    if-eqz v1, :cond_0

    .line 1392607
    iget-object v1, p0, LX/8jY;->e:LX/3Mb;

    invoke-interface {v1, p1, v0}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
