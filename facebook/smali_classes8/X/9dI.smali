.class public LX/9dI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:Z

.field public final b:Landroid/view/ViewConfiguration;

.field public c:Z

.field public d:F

.field public e:F

.field public f:LX/3rL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3rL",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/Jvf;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public h:Landroid/view/View$OnTouchListener;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Landroid/view/ViewParent;

.field public k:Z

.field public final l:LX/4oU;

.field private final m:Landroid/view/View$OnTouchListener;

.field private final n:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private final o:LX/9dH;

.field public final p:LX/8Ge;

.field public final q:LX/5jK;

.field public final r:LX/9d1;

.field public final s:Landroid/view/GestureDetector;

.field public final t:Z

.field public u:Landroid/view/View;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/widget/ScrollingAwareScrollView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public w:Landroid/view/View$OnClickListener;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/Ar0;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public y:Landroid/view/View$OnClickListener;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/9cu;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1517915
    const-class v0, LX/9dI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9dI;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9d1;ZLandroid/content/Context;LX/8Ge;)V
    .locals 3
    .param p1    # LX/9d1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1518026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518027
    iput-boolean v1, p0, LX/9dI;->k:Z

    .line 1518028
    new-instance v0, LX/9dE;

    invoke-direct {v0, p0}, LX/9dE;-><init>(LX/9dI;)V

    iput-object v0, p0, LX/9dI;->l:LX/4oU;

    .line 1518029
    new-instance v0, LX/9dF;

    invoke-direct {v0, p0}, LX/9dF;-><init>(LX/9dI;)V

    iput-object v0, p0, LX/9dI;->m:Landroid/view/View$OnTouchListener;

    .line 1518030
    new-instance v0, LX/9dG;

    invoke-direct {v0, p0}, LX/9dG;-><init>(LX/9dI;)V

    iput-object v0, p0, LX/9dI;->n:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 1518031
    new-instance v0, LX/9dH;

    invoke-direct {v0, p0}, LX/9dH;-><init>(LX/9dI;)V

    iput-object v0, p0, LX/9dI;->o:LX/9dH;

    .line 1518032
    new-instance v0, LX/5jK;

    sget-object v2, LX/5jJ;->NOT_SWIPING:LX/5jJ;

    invoke-direct {v0, v2}, LX/5jK;-><init>(LX/5jJ;)V

    move-object v0, v0

    .line 1518033
    iput-object v0, p0, LX/9dI;->q:LX/5jK;

    .line 1518034
    iput-boolean v1, p0, LX/9dI;->B:Z

    .line 1518035
    iput-object p1, p0, LX/9dI;->r:LX/9d1;

    .line 1518036
    iput-boolean p2, p0, LX/9dI;->t:Z

    .line 1518037
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, LX/9dI;->n:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p3, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/9dI;->s:Landroid/view/GestureDetector;

    .line 1518038
    iput-object p4, p0, LX/9dI;->p:LX/8Ge;

    .line 1518039
    invoke-static {p3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, LX/9dI;->b:Landroid/view/ViewConfiguration;

    .line 1518040
    iget-object v0, p0, LX/9dI;->p:LX/8Ge;

    const/high16 v1, 0x42c80000    # 100.0f

    const/high16 v2, 0x41700000    # 15.0f

    .line 1518041
    iput v2, v0, LX/8Ge;->h:F

    .line 1518042
    iput v1, v0, LX/8Ge;->g:F

    .line 1518043
    return-void
.end method

.method private a(FF)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1518022
    cmpg-float v0, p1, v2

    if-gtz v0, :cond_0

    .line 1518023
    iget-object v0, p0, LX/9dI;->p:LX/8Ge;

    iget-object v1, p0, LX/9dI;->o:LX/9dH;

    invoke-virtual {v0, v1, p1, v2}, LX/8Ge;->a(LX/9dH;FF)V

    .line 1518024
    :goto_0
    return-void

    .line 1518025
    :cond_0
    iget-object v0, p0, LX/9dI;->p:LX/8Ge;

    iget-object v1, p0, LX/9dI;->o:LX/9dH;

    invoke-virtual {v0, v1, p1, p2}, LX/8Ge;->a(LX/9dH;FF)V

    goto :goto_0
.end method

.method public static a$redex0(LX/9dI;FFZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1518011
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p0, v0}, LX/9dI;->d$redex0(LX/9dI;F)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p4, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/9dI;->A:Z

    .line 1518012
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1518013
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    .line 1518014
    iput p1, v0, LX/5jK;->c:F

    .line 1518015
    if-eqz p3, :cond_1

    .line 1518016
    invoke-virtual {p0, v1}, LX/9dI;->a(Z)V

    .line 1518017
    :cond_1
    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1518018
    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 1518019
    :cond_2
    :goto_1
    return-void

    .line 1518020
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1518021
    :cond_4
    sget-object v0, LX/9dI;->a:Ljava/lang/String;

    const-string v1, "Cannot update position while not swiping."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a$redex0(LX/9dI;LX/5jJ;)V
    .locals 2

    .prologue
    .line 1518002
    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1518003
    :cond_0
    :goto_0
    return-void

    .line 1518004
    :cond_1
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    .line 1518005
    new-instance v1, LX/5jK;

    invoke-direct {v1, v0}, LX/5jK;-><init>(LX/5jK;)V

    move-object v0, v1

    .line 1518006
    iget-boolean v1, p0, LX/9dI;->c:Z

    if-nez v1, :cond_2

    .line 1518007
    iget-object v1, p0, LX/9dI;->q:LX/5jK;

    .line 1518008
    iput-object p1, v1, LX/5jK;->a:LX/5jJ;

    .line 1518009
    :cond_2
    iget-object v1, p0, LX/9dI;->z:LX/9cu;

    if-eqz v1, :cond_0

    .line 1518010
    iget-object v1, p0, LX/9dI;->z:LX/9cu;

    invoke-interface {v1, v0, p1}, LX/9cu;->a(LX/5jK;LX/5jJ;)V

    goto :goto_0
.end method

.method private b(FF)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1517998
    cmpg-float v0, p1, v2

    if-gtz v0, :cond_0

    .line 1517999
    iget-object v0, p0, LX/9dI;->p:LX/8Ge;

    iget-object v1, p0, LX/9dI;->o:LX/9dH;

    neg-float v2, p2

    invoke-virtual {v0, v1, p1, v2}, LX/8Ge;->a(LX/9dH;FF)V

    .line 1518000
    :goto_0
    return-void

    .line 1518001
    :cond_0
    iget-object v0, p0, LX/9dI;->p:LX/8Ge;

    iget-object v1, p0, LX/9dI;->o:LX/9dH;

    invoke-virtual {v0, v1, p1, v2}, LX/8Ge;->a(LX/9dH;FF)V

    goto :goto_0
.end method

.method public static c(LX/9dI;Z)V
    .locals 1

    .prologue
    .line 1517994
    iget-boolean v0, p0, LX/9dI;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1517995
    :cond_0
    :goto_0
    return-void

    .line 1517996
    :cond_1
    iget-object v0, p0, LX/9dI;->j:Landroid/view/ViewParent;

    if-eqz v0, :cond_0

    .line 1517997
    iget-object v0, p0, LX/9dI;->j:Landroid/view/ViewParent;

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0
.end method

.method public static c(LX/9dI;Landroid/view/View$OnClickListener;)Z
    .locals 1
    .param p0    # LX/9dI;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517989
    invoke-virtual {p0}, LX/9dI;->g()V

    .line 1517990
    if-eqz p1, :cond_0

    .line 1517991
    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1517992
    const/4 v0, 0x1

    .line 1517993
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c$redex0(LX/9dI;F)V
    .locals 1

    .prologue
    .line 1518044
    iget-object v0, p0, LX/9dI;->p:LX/8Ge;

    invoke-virtual {v0}, LX/8Ge;->a()V

    .line 1518045
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    .line 1518046
    iput p1, v0, LX/5jK;->b:F

    .line 1518047
    sget-object v0, LX/5jJ;->ON_DOWN:LX/5jJ;

    invoke-static {p0, v0}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1518048
    return-void
.end method

.method public static d$redex0(LX/9dI;F)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1517983
    iget-object v1, p0, LX/9dI;->u:Landroid/view/View;

    if-nez v1, :cond_1

    .line 1517984
    :cond_0
    :goto_0
    return v0

    .line 1517985
    :cond_1
    iget-boolean v1, p0, LX/9dI;->t:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v1, p1, v1

    .line 1517986
    :goto_1
    const v2, 0x3d4ccccd    # 0.05f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1517987
    const/4 v0, 0x1

    goto :goto_0

    .line 1517988
    :cond_2
    iget-object v1, p0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v1, p1, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/9cu;)V
    .locals 2

    .prologue
    .line 1517979
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LX/9dI;->u:Landroid/view/View;

    .line 1517980
    iput-object p2, p0, LX/9dI;->z:LX/9cu;

    .line 1517981
    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    iget-object v1, p0, LX/9dI;->m:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1517982
    return-void
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1517950
    iget-object v1, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v1}, LX/5jK;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9dI;->u:Landroid/view/View;

    if-nez v1, :cond_1

    .line 1517951
    :cond_0
    :goto_0
    return-void

    .line 1517952
    :cond_1
    iget-boolean v1, p0, LX/9dI;->A:Z

    if-eqz v1, :cond_2

    move p1, v0

    .line 1517953
    :cond_2
    invoke-virtual {p0}, LX/9dI;->f()F

    move-result v3

    .line 1517954
    iget-boolean v1, p0, LX/9dI;->t:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    .line 1517955
    :goto_1
    iget-object v4, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v4}, LX/5jK;->a()F

    move-result v4

    .line 1517956
    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v1, v5

    .line 1517957
    sget-object v6, LX/5jJ;->FINISHING:LX/5jJ;

    invoke-static {p0, v6}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1517958
    if-eqz p1, :cond_6

    .line 1517959
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1517960
    invoke-direct {p0, v4, v1}, LX/9dI;->b(FF)V

    goto :goto_0

    .line 1517961
    :cond_3
    iget-object v1, p0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    goto :goto_1

    .line 1517962
    :cond_4
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1517963
    invoke-direct {p0, v4, v1}, LX/9dI;->a(FF)V

    goto :goto_0

    .line 1517964
    :cond_5
    invoke-virtual {p0}, LX/9dI;->g()V

    goto :goto_0

    .line 1517965
    :cond_6
    iget-object v6, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v6}, LX/5jK;->e()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1517966
    cmpl-float v3, v3, v5

    if-lez v3, :cond_7

    .line 1517967
    :goto_2
    if-eqz v0, :cond_8

    .line 1517968
    invoke-direct {p0, v4, v1}, LX/9dI;->a(FF)V

    goto :goto_0

    :cond_7
    move v0, v2

    .line 1517969
    goto :goto_2

    .line 1517970
    :cond_8
    invoke-direct {p0, v4, v1}, LX/9dI;->b(FF)V

    goto :goto_0

    .line 1517971
    :cond_9
    iget-object v6, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v6}, LX/5jK;->d()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1517972
    cmpg-float v3, v3, v5

    if-gez v3, :cond_a

    .line 1517973
    :goto_3
    if-eqz v0, :cond_b

    .line 1517974
    invoke-direct {p0, v4, v1}, LX/9dI;->b(FF)V

    goto :goto_0

    :cond_a
    move v0, v2

    .line 1517975
    goto :goto_3

    .line 1517976
    :cond_b
    invoke-direct {p0, v4, v1}, LX/9dI;->a(FF)V

    goto :goto_0

    .line 1517977
    :cond_c
    sget-object v3, LX/9dI;->a:Ljava/lang/String;

    const-string v5, "Finishing calculation error with no force forward. Delta : %f, Dimen : %f"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v6, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v3, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1517978
    invoke-virtual {p0}, LX/9dI;->g()V

    goto/16 :goto_0
.end method

.method public final a(ZLandroid/view/ViewParent;)V
    .locals 0

    .prologue
    .line 1517947
    iput-boolean p1, p0, LX/9dI;->i:Z

    .line 1517948
    iput-object p2, p0, LX/9dI;->j:Landroid/view/ViewParent;

    .line 1517949
    return-void
.end method

.method public final f()F
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1517937
    iget-boolean v0, p0, LX/9dI;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 1517938
    :goto_0
    iget-object v2, p0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v2}, LX/5jK;->a()F

    move-result v2

    .line 1517939
    cmpl-float v3, v2, v1

    if-lez v3, :cond_1

    .line 1517940
    :goto_1
    cmpg-float v3, v2, v1

    if-gez v3, :cond_2

    move v0, v1

    .line 1517941
    :goto_2
    return v0

    .line 1517942
    :cond_0
    iget-object v0, p0, LX/9dI;->u:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0

    .line 1517943
    :cond_1
    int-to-float v3, v0

    add-float/2addr v2, v3

    goto :goto_1

    .line 1517944
    :cond_2
    int-to-float v1, v0

    cmpl-float v1, v2, v1

    if-lez v1, :cond_3

    .line 1517945
    int-to-float v0, v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 1517946
    goto :goto_2
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1517929
    sget-object v0, LX/5jJ;->NOT_SWIPING:LX/5jJ;

    invoke-static {p0, v0}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1517930
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    move-object v0, v0

    .line 1517931
    invoke-virtual {v0}, LX/5jK;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517932
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    .line 1517933
    iput v1, v0, LX/5jK;->c:F

    .line 1517934
    iget-object v0, p0, LX/9dI;->q:LX/5jK;

    .line 1517935
    iput v1, v0, LX/5jK;->b:F

    .line 1517936
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1517916
    iget-object v0, p0, LX/9dI;->r:LX/9d1;

    invoke-interface {v0}, LX/9d1;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/9dI;->c:Z

    if-eqz v0, :cond_0

    .line 1517917
    iput-boolean v3, p0, LX/9dI;->c:Z

    .line 1517918
    iget-object v0, p0, LX/9dI;->f:LX/3rL;

    if-eqz v0, :cond_1

    .line 1517919
    iget-object v0, p0, LX/9dI;->f:LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1517920
    iget-object v0, p0, LX/9dI;->f:LX/3rL;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1517921
    const/4 v2, 0x0

    iput-object v2, p0, LX/9dI;->f:LX/3rL;

    .line 1517922
    invoke-static {p0, v1}, LX/9dI;->c$redex0(LX/9dI;F)V

    .line 1517923
    sget-object v1, LX/5jJ;->SWIPING:LX/5jJ;

    invoke-static {p0, v1}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1517924
    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-boolean v3, p0, LX/9dI;->A:Z

    invoke-static {p0, v0, v1, v2, v3}, LX/9dI;->a$redex0(LX/9dI;FFZZ)V

    .line 1517925
    :cond_0
    :goto_0
    return-void

    .line 1517926
    :cond_1
    iget v0, p0, LX/9dI;->d:F

    invoke-static {p0, v0}, LX/9dI;->c$redex0(LX/9dI;F)V

    .line 1517927
    sget-object v0, LX/5jJ;->SWIPING:LX/5jJ;

    invoke-static {p0, v0}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1517928
    iget v0, p0, LX/9dI;->e:F

    iget v1, p0, LX/9dI;->e:F

    iget v2, p0, LX/9dI;->d:F

    sub-float/2addr v1, v2

    invoke-static {p0, v0, v1, v3, v3}, LX/9dI;->a$redex0(LX/9dI;FFZZ)V

    goto :goto_0
.end method
