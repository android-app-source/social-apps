.class public final LX/9If;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9Ig;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLComment;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Landroid/view/View$OnClickListener;

.field public final synthetic e:LX/9Ig;


# direct methods
.method public constructor <init>(LX/9Ig;)V
    .locals 1

    .prologue
    .line 1463452
    iput-object p1, p0, LX/9If;->e:LX/9Ig;

    .line 1463453
    move-object v0, p1

    .line 1463454
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1463455
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1463456
    const-string v0, "ViewMoreRepliesComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1463457
    if-ne p0, p1, :cond_1

    .line 1463458
    :cond_0
    :goto_0
    return v0

    .line 1463459
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1463460
    goto :goto_0

    .line 1463461
    :cond_3
    check-cast p1, LX/9If;

    .line 1463462
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1463463
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1463464
    if-eq v2, v3, :cond_0

    .line 1463465
    iget-object v2, p0, LX/9If;->a:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9If;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p1, LX/9If;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1463466
    goto :goto_0

    .line 1463467
    :cond_5
    iget-object v2, p1, LX/9If;->a:Lcom/facebook/graphql/model/GraphQLComment;

    if-nez v2, :cond_4

    .line 1463468
    :cond_6
    iget-object v2, p0, LX/9If;->b:LX/0Px;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/9If;->b:LX/0Px;

    iget-object v3, p1, LX/9If;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1463469
    goto :goto_0

    .line 1463470
    :cond_8
    iget-object v2, p1, LX/9If;->b:LX/0Px;

    if-nez v2, :cond_7

    .line 1463471
    :cond_9
    iget-boolean v2, p0, LX/9If;->c:Z

    iget-boolean v3, p1, LX/9If;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1463472
    goto :goto_0

    .line 1463473
    :cond_a
    iget-object v2, p0, LX/9If;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/9If;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/9If;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1463474
    goto :goto_0

    .line 1463475
    :cond_b
    iget-object v2, p1, LX/9If;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
