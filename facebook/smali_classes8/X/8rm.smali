.class public final LX/8rm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field public f:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1409206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;)LX/8rm;
    .locals 2

    .prologue
    .line 1409178
    new-instance v0, LX/8rm;

    invoke-direct {v0}, LX/8rm;-><init>()V

    .line 1409179
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/8rm;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1409180
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8rm;->b:Ljava/lang/String;

    .line 1409181
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->j()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v1

    iput-object v1, v0, LX/8rm;->c:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    .line 1409182
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->v_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8rm;->d:Ljava/lang/String;

    .line 1409183
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->k()I

    move-result v1

    iput v1, v0, LX/8rm;->e:I

    .line 1409184
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->m()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v1

    iput-object v1, v0, LX/8rm;->f:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    .line 1409185
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 1409186
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1409187
    iget-object v1, p0, LX/8rm;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1409188
    iget-object v3, p0, LX/8rm;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1409189
    iget-object v5, p0, LX/8rm;->c:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1409190
    iget-object v6, p0, LX/8rm;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1409191
    iget-object v7, p0, LX/8rm;->f:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1409192
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1409193
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1409194
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1409195
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1409196
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1409197
    const/4 v1, 0x4

    iget v3, p0, LX/8rm;->e:I

    invoke-virtual {v0, v1, v3, v9}, LX/186;->a(III)V

    .line 1409198
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1409199
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1409200
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1409201
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1409202
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1409203
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1409204
    new-instance v1, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;

    invoke-direct {v1, v0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;-><init>(LX/15i;)V

    .line 1409205
    return-object v1
.end method
