.class public final LX/8wB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic e:Z


# instance fields
.field public a:[LX/8w9;

.field public b:I

.field public c:[[LX/8w9;

.field public d:[I

.field public final synthetic f:[LX/8w9;

.field public final synthetic g:LX/8wC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1421652
    const-class v0, Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/8wB;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/8wC;[LX/8w9;)V
    .locals 2

    .prologue
    .line 1421667
    iput-object p1, p0, LX/8wB;->g:LX/8wC;

    iput-object p2, p0, LX/8wB;->f:[LX/8w9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421668
    iget-object v0, p0, LX/8wB;->f:[LX/8w9;

    array-length v0, v0

    new-array v0, v0, [LX/8w9;

    iput-object v0, p0, LX/8wB;->a:[LX/8w9;

    .line 1421669
    iget-object v0, p0, LX/8wB;->a:[LX/8w9;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/8wB;->b:I

    .line 1421670
    iget-object v0, p0, LX/8wB;->g:LX/8wC;

    iget-object v1, p0, LX/8wB;->f:[LX/8w9;

    invoke-virtual {v0, v1}, LX/8wC;->a([LX/8w9;)[[LX/8w9;

    move-result-object v0

    iput-object v0, p0, LX/8wB;->c:[[LX/8w9;

    .line 1421671
    iget-object v0, p0, LX/8wB;->g:LX/8wC;

    invoke-virtual {v0}, LX/8wC;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/8wB;->d:[I

    return-void
.end method

.method private a(I)V
    .locals 7

    .prologue
    .line 1421658
    iget-object v0, p0, LX/8wB;->d:[I

    aget v0, v0, p1

    packed-switch v0, :pswitch_data_0

    .line 1421659
    :cond_0
    :goto_0
    return-void

    .line 1421660
    :pswitch_0
    iget-object v0, p0, LX/8wB;->d:[I

    const/4 v1, 0x1

    aput v1, v0, p1

    .line 1421661
    iget-object v0, p0, LX/8wB;->c:[[LX/8w9;

    aget-object v1, v0, p1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1421662
    iget-object v4, v3, LX/8w9;->a:LX/8wD;

    iget v4, v4, LX/8wD;->b:I

    invoke-direct {p0, v4}, LX/8wB;->a(I)V

    .line 1421663
    iget-object v4, p0, LX/8wB;->a:[LX/8w9;

    iget v5, p0, LX/8wB;->b:I

    add-int/lit8 v6, v5, -0x1

    iput v6, p0, LX/8wB;->b:I

    aput-object v3, v4, v5

    .line 1421664
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1421665
    :cond_1
    iget-object v0, p0, LX/8wB;->d:[I

    const/4 v1, 0x2

    aput v1, v0, p1

    goto :goto_0

    .line 1421666
    :pswitch_1
    sget-boolean v0, LX/8wB;->e:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()[LX/8w9;
    .locals 2

    .prologue
    .line 1421653
    const/4 v0, 0x0

    iget-object v1, p0, LX/8wB;->c:[[LX/8w9;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1421654
    invoke-direct {p0, v0}, LX/8wB;->a(I)V

    .line 1421655
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1421656
    :cond_0
    sget-boolean v0, LX/8wB;->e:Z

    if-nez v0, :cond_1

    iget v0, p0, LX/8wB;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1421657
    :cond_1
    iget-object v0, p0, LX/8wB;->a:[LX/8w9;

    return-object v0
.end method
