.class public LX/8yW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/8yW;


# instance fields
.field private final b:LX/8yZ;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1425929
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/8yW;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/8yZ;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8yZ;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425931
    iput-object p1, p0, LX/8yW;->b:LX/8yZ;

    .line 1425932
    iput-object p2, p0, LX/8yW;->c:LX/0Or;

    .line 1425933
    return-void
.end method

.method public static a(LX/0QB;)LX/8yW;
    .locals 5

    .prologue
    .line 1425934
    sget-object v0, LX/8yW;->d:LX/8yW;

    if-nez v0, :cond_1

    .line 1425935
    const-class v1, LX/8yW;

    monitor-enter v1

    .line 1425936
    :try_start_0
    sget-object v0, LX/8yW;->d:LX/8yW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1425937
    if-eqz v2, :cond_0

    .line 1425938
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1425939
    new-instance v4, LX/8yW;

    invoke-static {v0}, LX/8yZ;->a(LX/0QB;)LX/8yZ;

    move-result-object v3

    check-cast v3, LX/8yZ;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/8yW;-><init>(LX/8yZ;LX/0Or;)V

    .line 1425940
    move-object v0, v4

    .line 1425941
    sput-object v0, LX/8yW;->d:LX/8yW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1425942
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1425943
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1425944
    :cond_1
    sget-object v0, LX/8yW;->d:LX/8yW;

    return-object v0

    .line 1425945
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1425946
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/common/callercontext/CallerContext;IIIIZLjava/util/List;IIII)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p8    # Ljava/util/List;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "IIIIZ",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;IIII)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1425947
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1425948
    const/high16 v2, -0x80000000

    if-ne p3, v2, :cond_0

    .line 1425949
    const v2, 0x7f0b152f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 1425950
    :cond_0
    const/high16 v2, -0x80000000

    if-ne p4, v2, :cond_1

    .line 1425951
    const v2, 0x7f0a06c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p4

    .line 1425952
    :cond_1
    const/high16 v2, -0x80000000

    move/from16 v0, p5

    if-ne v0, v2, :cond_2

    .line 1425953
    const v2, 0x7f0b1531

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p5

    .line 1425954
    :cond_2
    const/high16 v2, -0x80000000

    move/from16 v0, p6

    if-ne v0, v2, :cond_3

    .line 1425955
    const v2, 0x7f0b1530

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p6

    .line 1425956
    :cond_3
    const/high16 v2, -0x80000000

    move/from16 v0, p9

    if-ne v0, v2, :cond_4

    .line 1425957
    const v2, 0x7f0c0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p9

    .line 1425958
    :cond_4
    if-gez p9, :cond_5

    .line 1425959
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "faceCount cannot be < 0"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1425960
    :cond_5
    const/high16 v2, -0x80000000

    move/from16 v0, p10

    if-ne v0, v2, :cond_6

    .line 1425961
    const v2, 0x7f0b1532

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p10

    .line 1425962
    :cond_6
    const/high16 v1, -0x80000000

    move/from16 v0, p11

    if-ne v0, v1, :cond_7

    .line 1425963
    const/16 p11, 0x0

    .line 1425964
    :cond_7
    invoke-interface/range {p8 .. p8}, Ljava/util/List;->size()I

    move-result v6

    .line 1425965
    if-eqz p7, :cond_9

    move/from16 v0, p9

    if-le v6, v0, :cond_9

    const/4 v1, 0x1

    move v3, v1

    .line 1425966
    :goto_0
    move/from16 v0, p9

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1425967
    if-eqz v3, :cond_a

    const/4 v1, 0x1

    :goto_1
    sub-int v7, v2, v1

    .line 1425968
    const/high16 v1, -0x80000000

    move/from16 v0, p12

    if-ne v0, v1, :cond_b

    .line 1425969
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    move-object v4, v1

    .line 1425970
    :goto_2
    move/from16 v0, p5

    int-to-float v1, v0

    invoke-virtual {v4, p4, v1}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v1

    move/from16 v0, p10

    int-to-float v2, v0

    invoke-virtual {v1, v2}, LX/4Ab;->d(F)LX/4Ab;

    .line 1425971
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    .line 1425972
    iget-object v1, p0, LX/8yW;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    .line 1425973
    const/4 v2, 0x0

    move v5, v2

    :goto_3
    if-ge v5, v7, :cond_c

    .line 1425974
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v2

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, LX/1nh;->h(I)LX/1nh;

    move-result-object v9

    invoke-virtual {v2, v9}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v9

    move-object/from16 v0, p8

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v1, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v2

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v9

    move/from16 v0, p11

    invoke-virtual {v9, v0}, LX/1nh;->h(I)LX/1nh;

    move-result-object v9

    invoke-virtual {v2, v9}, LX/1up;->b(LX/1n6;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    .line 1425975
    if-lez v5, :cond_8

    if-eqz p6, :cond_8

    .line 1425976
    const/4 v9, 0x4

    move/from16 v0, p6

    invoke-interface {v2, v9, v0}, LX/1Di;->a(II)LX/1Di;

    .line 1425977
    :cond_8
    invoke-interface {v8, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1425978
    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    .line 1425979
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 1425980
    :cond_9
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_0

    .line 1425981
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1425982
    :cond_b
    move/from16 v0, p12

    int-to-float v1, v0

    invoke-static {v1}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_2

    .line 1425983
    :cond_c
    if-eqz v3, :cond_e

    .line 1425984
    iget-object v1, p0, LX/8yW;->b:LX/8yZ;

    invoke-virtual {v1, p1}, LX/8yZ;->c(LX/1De;)LX/8yX;

    move-result-object v1

    sub-int v2, v6, v7

    invoke-virtual {v1, v2}, LX/8yX;->h(I)LX/8yX;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    .line 1425985
    if-eqz p6, :cond_d

    .line 1425986
    const/4 v2, 0x4

    move/from16 v0, p6

    invoke-interface {v1, v2, v0}, LX/1Di;->a(II)LX/1Di;

    .line 1425987
    :cond_d
    invoke-interface {v8, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1425988
    :cond_e
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1
.end method
