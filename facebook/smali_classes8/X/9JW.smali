.class public LX/9JW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2nf;


# instance fields
.field private a:LX/2kb;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/9JQ;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/os/Bundle;

.field private d:I

.field private e:LX/2kD;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Landroid/os/Bundle;LX/2kD;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JQ;",
            ">;",
            "Landroid/os/Bundle;",
            "LX/2kD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1465400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1465401
    iput-object p1, p0, LX/9JW;->b:Ljava/util/ArrayList;

    .line 1465402
    iput-object p2, p0, LX/9JW;->c:Landroid/os/Bundle;

    .line 1465403
    iput-object p3, p0, LX/9JW;->e:LX/2kD;

    .line 1465404
    const/4 v0, -0x1

    iput v0, p0, LX/9JW;->d:I

    .line 1465405
    return-void
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 1465399
    iget v0, p0, LX/9JW;->d:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/9JW;->d:I

    iget-object v1, p0, LX/9JW;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1465396
    iget-object v0, p0, LX/9JW;->b:Ljava/util/ArrayList;

    iget v1, p0, LX/9JW;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1465397
    iget v1, v0, LX/9JQ;->d:I

    move v0, v1

    .line 1465398
    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(LX/2kb;)V
    .locals 1

    .prologue
    .line 1465392
    iget-object v0, p0, LX/9JW;->a:LX/2kb;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1465393
    iput-object p1, p0, LX/9JW;->a:LX/2kb;

    .line 1465394
    return-void

    .line 1465395
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1465391
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SimpleEdgeStoreModelCursor::getRowVersion() not yet supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/facebook/flatbuffers/Flattenable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1465378
    iget-object v0, p0, LX/9JW;->b:Ljava/util/ArrayList;

    iget v1, p0, LX/9JW;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1465379
    :try_start_0
    iget-object v1, p0, LX/9JW;->e:LX/2kD;

    invoke-virtual {v0}, LX/9JQ;->e()I

    move-result v2

    invoke-virtual {v0}, LX/9JQ;->d()Ljava/lang/String;

    move-result-object v3

    .line 1465380
    iget-object v4, v0, LX/9JQ;->e:LX/0w5;

    move-object v4, v4

    .line 1465381
    iget-object p0, v0, LX/9JQ;->b:LX/9JR;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/9JQ;->b:LX/9JR;

    .line 1465382
    iget-object v0, p0, LX/9JR;->c:[B

    move-object p0, v0

    .line 1465383
    :goto_0
    move-object v0, p0

    .line 1465384
    invoke-virtual {v1, v2, v3, v4, v0}, LX/2kD;->a(ILjava/lang/String;LX/0w5;[B)Lcom/facebook/flatbuffers/Flattenable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1465385
    :goto_1
    return-object v0

    .line 1465386
    :catch_0
    move-exception v0

    .line 1465387
    const-class v1, LX/9JW;

    const-string v2, "Unable to load model"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1465388
    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    iget-object p0, v0, LX/9JQ;->a:LX/9JR;

    .line 1465389
    iget-object v0, p0, LX/9JR;->c:[B

    move-object p0, v0

    .line 1465390
    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1465373
    iget-object v0, p0, LX/9JW;->a:LX/2kb;

    if-eqz v0, :cond_0

    .line 1465374
    iget-object v0, p0, LX/9JW;->a:LX/2kb;

    invoke-interface {v0}, LX/2kb;->close()V

    .line 1465375
    :cond_0
    iput-object v1, p0, LX/9JW;->b:Ljava/util/ArrayList;

    .line 1465376
    iput-object v1, p0, LX/9JW;->e:LX/2kD;

    .line 1465377
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1465370
    iget-object v0, p0, LX/9JW;->b:Ljava/util/ArrayList;

    iget v1, p0, LX/9JW;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1465371
    iget v1, v0, LX/9JQ;->f:I

    move v0, v1

    .line 1465372
    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1465369
    iget-object v0, p0, LX/9JW;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1465356
    iget-object v0, p0, LX/9JW;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getPosition()I
    .locals 1

    .prologue
    .line 1465368
    iget v0, p0, LX/9JW;->d:I

    return v0
.end method

.method public final isClosed()Z
    .locals 1

    .prologue
    .line 1465367
    iget-object v0, p0, LX/9JW;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final moveToFirst()Z
    .locals 1

    .prologue
    .line 1465365
    const/4 v0, 0x0

    iput v0, p0, LX/9JW;->d:I

    .line 1465366
    invoke-direct {p0}, LX/9JW;->e()Z

    move-result v0

    return v0
.end method

.method public final moveToLast()Z
    .locals 1

    .prologue
    .line 1465363
    iget-object v0, p0, LX/9JW;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/9JW;->d:I

    .line 1465364
    invoke-direct {p0}, LX/9JW;->e()Z

    move-result v0

    return v0
.end method

.method public final moveToNext()Z
    .locals 1

    .prologue
    .line 1465361
    iget v0, p0, LX/9JW;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/9JW;->d:I

    .line 1465362
    invoke-direct {p0}, LX/9JW;->e()Z

    move-result v0

    return v0
.end method

.method public final moveToPosition(I)Z
    .locals 1

    .prologue
    .line 1465359
    iput p1, p0, LX/9JW;->d:I

    .line 1465360
    invoke-direct {p0}, LX/9JW;->e()Z

    move-result v0

    return v0
.end method

.method public final moveToPrevious()Z
    .locals 1

    .prologue
    .line 1465357
    iget v0, p0, LX/9JW;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/9JW;->d:I

    .line 1465358
    invoke-direct {p0}, LX/9JW;->e()Z

    move-result v0

    return v0
.end method
