.class public final LX/93v;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final synthetic b:LX/93s;


# direct methods
.method public constructor <init>(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 0

    .prologue
    .line 1434328
    iput-object p1, p0, LX/93v;->b:LX/93s;

    iput-object p2, p0, LX/93v;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1434329
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1434330
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_1

    .line 1434331
    iget-object v0, p0, LX/93v;->b:LX/93s;

    .line 1434332
    iget-object v1, v0, LX/93Q;->b:LX/03V;

    move-object v0, v1

    .line 1434333
    const-string v1, "composer_privacy_fetch_error"

    const-string v2, "Privacy options fetch failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1434334
    :cond_0
    :goto_0
    return-void

    .line 1434335
    :cond_1
    iget-object v0, p0, LX/93v;->b:LX/93s;

    iget v0, v0, LX/93s;->e:I

    if-gtz v0, :cond_0

    .line 1434336
    iget-object v0, p0, LX/93v;->b:LX/93s;

    .line 1434337
    iget v1, v0, LX/93s;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, LX/93s;->e:I

    .line 1434338
    iget-object v0, p0, LX/93v;->b:LX/93s;

    iget-object v1, p0, LX/93v;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v0, v1}, LX/93s;->b(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1434339
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1434340
    iget-object v0, p0, LX/93v;->b:LX/93s;

    const/4 v1, 0x0

    .line 1434341
    iput v1, v0, LX/93s;->e:I

    .line 1434342
    iget-object v0, p0, LX/93v;->b:LX/93s;

    const/4 v1, 0x1

    .line 1434343
    iput-boolean v1, v0, LX/93s;->d:Z

    .line 1434344
    iget-object v0, p0, LX/93v;->b:LX/93s;

    iget-object v0, v0, LX/93s;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1434345
    if-eqz v0, :cond_2

    .line 1434346
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0it;

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0it;

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0it;

    invoke-interface {v1}, LX/0it;->d()Z

    move-result v1

    if-nez v1, :cond_3

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qu;

    invoke-interface {v0}, LX/5Qu;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1434347
    :cond_1
    iget-object v0, p0, LX/93v;->b:LX/93s;

    iget-object v1, p0, LX/93v;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v0, v1, p1}, LX/93s;->b(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    .line 1434348
    :cond_2
    :goto_0
    return-void

    .line 1434349
    :cond_3
    iget-object v0, p0, LX/93v;->b:LX/93s;

    iget-object v0, v0, LX/93s;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2c9;

    sget-object v1, LX/2Zv;->PRIVACY_CHANGE_IGNORED_ON_FETCH:LX/2Zv;

    invoke-virtual {v0, v1}, LX/2c9;->a(LX/2Zv;)V

    goto :goto_0
.end method
