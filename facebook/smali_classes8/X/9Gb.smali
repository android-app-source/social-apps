.class public final LX/9Gb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9FG;

.field public final synthetic b:LX/9FA;

.field public final synthetic c:LX/9Gc;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;LX/9FG;LX/9FA;LX/9Gc;)V
    .locals 0

    .prologue
    .line 1460102
    iput-object p1, p0, LX/9Gb;->d:Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

    iput-object p2, p0, LX/9Gb;->a:LX/9FG;

    iput-object p3, p0, LX/9Gb;->b:LX/9FA;

    iput-object p4, p0, LX/9Gb;->c:LX/9Gc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x2492548b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1460103
    iget-object v1, p0, LX/9Gb;->a:LX/9FG;

    .line 1460104
    iget-object v6, v1, LX/9FG;->a:LX/0if;

    sget-object v7, LX/0ig;->j:LX/0ih;

    iget-wide v8, v1, LX/9FG;->b:J

    const-string v10, "reply_call_to_action_clicked"

    invoke-virtual {v6, v7, v8, v9, v10}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1460105
    iget-object v1, p0, LX/9Gb;->b:LX/9FA;

    iget-object v2, p0, LX/9Gb;->c:LX/9Gc;

    iget-object v2, v2, LX/9Gc;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/9Gb;->c:LX/9Gc;

    iget-object v3, v3, LX/9Gc;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v4, p0, LX/9Gb;->c:LX/9Gc;

    iget-object v4, v4, LX/9Gc;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1, v2, v3, v4}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1460106
    const v1, 0x4b251644    # 1.081914E7f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
