.class public final LX/AMH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1666019
    iput-object p1, p0, LX/AMH;->b:Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;

    iput-object p2, p0, LX/AMH;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/cameracore/ui/CaptureType;)Ljava/io/File;
    .locals 4

    .prologue
    .line 1666020
    sget-object v0, LX/AMI;->a:[I

    invoke-virtual {p1}, Lcom/facebook/cameracore/ui/CaptureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1666021
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1666022
    :pswitch_0
    const-string v0, "FB_IMG_%d.jpg"

    .line 1666023
    :goto_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1666024
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/AMH;->a:Ljava/io/File;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1

    .line 1666025
    :pswitch_1
    const-string v0, "FB_VID_%d.mp4"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1666026
    iget-object v0, p0, LX/AMH;->b:Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;

    invoke-virtual {v0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->finish()V

    .line 1666027
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 1666028
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1666029
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1666030
    iget-object v1, p0, LX/AMH;->b:Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->setResult(ILandroid/content/Intent;)V

    .line 1666031
    iget-object v0, p0, LX/AMH;->b:Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;

    invoke-virtual {v0}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->finish()V

    .line 1666032
    return-void
.end method
