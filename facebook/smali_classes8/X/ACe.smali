.class public final LX/ACe;
.super LX/8QL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8QL",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/ACe;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1643007
    new-instance v0, LX/ACd;

    invoke-direct {v0}, LX/ACd;-><init>()V

    sput-object v0, LX/ACe;->e:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;)V
    .locals 1

    .prologue
    .line 1643003
    sget-object v0, LX/8vA;->SUGGESTION:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1643004
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1643005
    iput-object p1, p0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    .line 1643006
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1643002
    iget-object v0, p0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1643001
    iget-object v0, p0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1643000
    const/4 v0, -0x1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1643008
    const/4 v0, -0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1642996
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/ACe;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1642997
    :goto_0
    return v0

    :cond_1
    move-object v0, p1

    .line 1642998
    check-cast v0, LX/ACe;

    iget-object v0, v0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    .line 1642999
    if-eq p1, p0, :cond_2

    iget-object v2, p0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1642995
    const/4 v0, -0x1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1642994
    const/4 v0, -0x1

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1642993
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1642992
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/ACe;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
