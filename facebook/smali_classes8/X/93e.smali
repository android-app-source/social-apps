.class public LX/93e;
.super LX/93Q;
.source ""


# instance fields
.field private final a:LX/2rX;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;LX/2rX;Ljava/lang/String;)V
    .locals 0
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/2rX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1433999
    invoke-direct {p0, p1, p2, p3}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1434000
    iput-object p4, p0, LX/93e;->a:LX/2rX;

    .line 1434001
    iput-object p5, p0, LX/93e;->b:Ljava/lang/String;

    .line 1434002
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1434003
    invoke-super {p0}, LX/93Q;->a()V

    .line 1434004
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    iget-object v1, p0, LX/93e;->a:LX/2rX;

    invoke-interface {v1}, LX/2rX;->c()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    .line 1434005
    iput-object v1, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434006
    move-object v0, v0

    .line 1434007
    iget-object v1, p0, LX/93e;->a:LX/2rX;

    invoke-interface {v1}, LX/2rX;->a()Ljava/lang/String;

    move-result-object v1

    .line 1434008
    iput-object v1, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1434009
    move-object v0, v0

    .line 1434010
    iget-object v1, p0, LX/93e;->a:LX/2rX;

    invoke-interface {v1}, LX/2rX;->b()Ljava/lang/String;

    move-result-object v1

    .line 1434011
    iput-object v1, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1434012
    move-object v0, v0

    .line 1434013
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434014
    new-instance v1, LX/7lP;

    invoke-direct {v1}, LX/7lP;-><init>()V

    const/4 v2, 0x1

    .line 1434015
    iput-boolean v2, v1, LX/7lP;->a:Z

    .line 1434016
    move-object v1, v1

    .line 1434017
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434018
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434019
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1434020
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fundraiser:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/93e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
