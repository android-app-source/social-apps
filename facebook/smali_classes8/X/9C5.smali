.class public final LX/9C5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/view/ViewGroup;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/LayoutInflater;

.field public final synthetic b:Landroid/view/ViewGroup;

.field public final synthetic c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1453041
    iput-object p1, p0, LX/9C5;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iput-object p2, p0, LX/9C5;->a:Landroid/view/LayoutInflater;

    iput-object p3, p0, LX/9C5;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1453042
    iget-object v0, p0, LX/9C5;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v1, p0, LX/9C5;->a:Landroid/view/LayoutInflater;

    iget-object v2, p0, LX/9C5;->b:Landroid/view/ViewGroup;

    .line 1453043
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    .line 1453044
    iget-object v4, v3, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390021

    const-string p0, "NNF_FlyoutBgInflationTime"

    invoke-interface {v4, v5, p0}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 1453045
    const v3, 0x7f030655

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 1453046
    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    .line 1453047
    iget-object v5, v4, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const p0, 0x390021

    const-string v0, "NNF_FlyoutBgInflationTime"

    invoke-interface {v5, p0, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 1453048
    move-object v0, v3

    .line 1453049
    return-object v0
.end method
