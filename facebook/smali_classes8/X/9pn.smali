.class public final LX/9pn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1545425
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1545426
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1545427
    :goto_0
    return v1

    .line 1545428
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1545429
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1545430
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1545431
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1545432
    const-string v7, "author_rating_integer"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1545433
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1545434
    :cond_1
    const-string v7, "formatted_preview"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1545435
    invoke-static {p0, p1}, LX/9pm;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1545436
    :cond_2
    const-string v7, "story"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1545437
    invoke-static {p0, p1}, LX/5tX;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1545438
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1545439
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1545440
    if-eqz v0, :cond_5

    .line 1545441
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1545442
    :cond_5
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1545443
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1545444
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1545445
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1545446
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1545447
    if-eqz v0, :cond_0

    .line 1545448
    const-string v1, "author_rating_integer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545449
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1545450
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545451
    if-eqz v0, :cond_1

    .line 1545452
    const-string v1, "formatted_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545453
    invoke-static {p0, v0, p2, p3}, LX/9pm;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545454
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545455
    if-eqz v0, :cond_2

    .line 1545456
    const-string v1, "story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545457
    invoke-static {p0, v0, p2, p3}, LX/5tX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545458
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1545459
    return-void
.end method
