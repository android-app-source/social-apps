.class public LX/AQ1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j3;",
        "DerivedData::",
        "LX/5RE;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginStatusHintGetter;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0in",
        "<TPluginData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/hint/controller/ComposerHintController$Delegate;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0il;LX/Hra;Landroid/content/res/Resources;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hra;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/hint/controller/ComposerHintController$Delegate;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670712
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AQ1;->b:Ljava/lang/ref/WeakReference;

    .line 1670713
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AQ1;->a:Ljava/lang/ref/WeakReference;

    .line 1670714
    iput-object p3, p0, LX/AQ1;->c:Landroid/content/res/Resources;

    .line 1670715
    invoke-direct {p0}, LX/AQ1;->b()V

    .line 1670716
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1670717
    iget-object v0, p0, LX/AQ1;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670718
    iget-object v1, p0, LX/AQ1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hra;

    move-object v2, v0

    .line 1670719
    check-cast v2, LX/0in;

    invoke-interface {v2}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AQ9;

    .line 1670720
    iget-object v3, v2, LX/AQ9;->K:LX/AQ4;

    move-object v2, v3

    .line 1670721
    if-eqz v2, :cond_0

    .line 1670722
    check-cast v0, LX/0in;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    .line 1670723
    iget-object v2, v0, LX/AQ9;->K:LX/AQ4;

    move-object v0, v2

    .line 1670724
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/Hra;->a(Ljava/lang/String;)V

    .line 1670725
    :goto_0
    return-void

    .line 1670726
    :cond_0
    iget-object v2, p0, LX/AQ1;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, LX/AQ3;->a(LX/0il;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Hra;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1670727
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1670728
    invoke-direct {p0}, LX/AQ1;->b()V

    .line 1670729
    return-void
.end method
