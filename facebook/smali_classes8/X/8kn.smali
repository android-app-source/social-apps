.class public final LX/8kn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;)V
    .locals 0

    .prologue
    .line 1396785
    iput-object p1, p0, LX/8kn;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1396786
    iget-object v1, p0, LX/8kn;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v1, v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;->setTabImageScale(Lcom/facebook/stickers/keyboard/StickerKeyboardTabView;F)V

    .line 1396787
    return-void
.end method
