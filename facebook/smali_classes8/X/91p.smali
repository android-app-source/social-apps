.class public LX/91p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/91M;


# direct methods
.method public constructor <init>(LX/91M;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1431472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1431473
    iput-object p1, p0, LX/91p;->a:LX/91M;

    .line 1431474
    return-void
.end method

.method public static a(LX/0QB;)LX/91p;
    .locals 4

    .prologue
    .line 1431461
    const-class v1, LX/91p;

    monitor-enter v1

    .line 1431462
    :try_start_0
    sget-object v0, LX/91p;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1431463
    sput-object v2, LX/91p;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1431464
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1431465
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1431466
    new-instance p0, LX/91p;

    invoke-static {v0}, LX/91M;->a(LX/0QB;)LX/91M;

    move-result-object v3

    check-cast v3, LX/91M;

    invoke-direct {p0, v3}, LX/91p;-><init>(LX/91M;)V

    .line 1431467
    move-object v0, p0

    .line 1431468
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1431469
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1431470
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1431471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
