.class public final LX/95h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/95f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EdgeType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/95f",
        "<TEdgeType;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TEdgeType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TEdgeType;>;)V"
        }
    .end annotation

    .prologue
    .line 1437176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437177
    iput-object p1, p0, LX/95h;->a:Ljava/util/ArrayList;

    .line 1437178
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Z
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITEdgeType;)Z"
        }
    .end annotation

    .prologue
    .line 1437179
    iget-object v0, p0, LX/95h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    if-eqz p2, :cond_0

    move-object v0, p2

    .line 1437180
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15i;

    .line 1437181
    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    instance-of v0, v0, Ljava/nio/MappedByteBuffer;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1437182
    iget-object v0, p0, LX/95h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1437183
    const/4 v0, 0x1

    .line 1437184
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
