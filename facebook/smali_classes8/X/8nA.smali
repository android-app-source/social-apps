.class public final LX/8nA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "LX/8cK;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8JX;

.field public final synthetic b:Ljava/lang/CharSequence;

.field public final synthetic c:LX/8nC;


# direct methods
.method public constructor <init>(LX/8nC;LX/8JX;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1400612
    iput-object p1, p0, LX/8nA;->c:LX/8nC;

    iput-object p2, p0, LX/8nA;->a:LX/8JX;

    iput-object p3, p0, LX/8nA;->b:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1400613
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1400614
    check-cast p1, Ljava/util/List;

    .line 1400615
    const-string v1, "db_bootstrap"

    sget-object v2, LX/8nE;->OTHERS:LX/8nE;

    invoke-virtual {v2}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 1400616
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1400617
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8cK;

    .line 1400618
    invoke-static {v4, v1, v2}, LX/3iT;->a(LX/8cK;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v4

    .line 1400619
    if-nez v3, :cond_1

    .line 1400620
    iget-object v7, v4, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v7, v7

    .line 1400621
    sget-object v8, LX/7Gr;->UNKNOWN:LX/7Gr;

    if-eq v7, v8, :cond_0

    .line 1400622
    :cond_1
    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1400623
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v0, v4

    .line 1400624
    iget-object v1, p0, LX/8nA;->a:LX/8JX;

    iget-object v2, p0, LX/8nA;->b:Ljava/lang/CharSequence;

    invoke-interface {v1, v2, v0}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1400625
    return-void
.end method
