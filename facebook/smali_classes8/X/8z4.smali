.class public LX/8z4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8z3;


# instance fields
.field public final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1426870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426871
    iput-object p1, p0, LX/8z4;->a:Landroid/content/res/Resources;

    .line 1426872
    return-void
.end method

.method public static a(LX/8z2;LX/8z9;I)V
    .locals 3

    .prologue
    .line 1426862
    sget-object v0, LX/8z1;->a:[I

    invoke-virtual {p0}, LX/8z2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1426863
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown tag type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1426864
    :pswitch_0
    invoke-interface {p1}, LX/8z9;->a()V

    .line 1426865
    :goto_0
    return-void

    .line 1426866
    :pswitch_1
    invoke-interface {p1, p2}, LX/8z9;->a(I)V

    goto :goto_0

    .line 1426867
    :pswitch_2
    invoke-interface {p1}, LX/8z9;->b()V

    goto :goto_0

    .line 1426868
    :pswitch_3
    invoke-interface {p1}, LX/8z9;->c()V

    goto :goto_0

    .line 1426869
    :pswitch_4
    invoke-interface {p1}, LX/8z9;->e()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(LX/8z4;Landroid/text/SpannableStringBuilder;IILandroid/text/style/CharacterStyle;LX/8z9;LX/8z2;I)V
    .locals 7

    .prologue
    .line 1426859
    invoke-static {p4}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v0

    const/16 v1, 0x21

    invoke-virtual {p1, v0, p2, p3, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p6

    move v4, p2

    move v5, p3

    move v6, p7

    .line 1426860
    invoke-static/range {v0 .. v6}, LX/8z4;->a(LX/8z4;Landroid/text/SpannableStringBuilder;LX/8z9;LX/8z2;III)V

    .line 1426861
    return-void
.end method

.method private static a(LX/8z4;Landroid/text/SpannableStringBuilder;LX/8z9;LX/8z2;III)V
    .locals 2

    .prologue
    .line 1426856
    if-nez p2, :cond_0

    .line 1426857
    :goto_0
    return-void

    .line 1426858
    :cond_0
    new-instance v0, LX/8z0;

    invoke-direct {v0, p0, p3, p2, p6}, LX/8z0;-><init>(LX/8z4;LX/8z2;LX/8z9;I)V

    const/16 v1, 0x21

    invoke-virtual {p1, v0, p4, p5, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;LX/8z5;LX/8z7;)V
    .locals 16

    .prologue
    .line 1426787
    move-object/from16 v0, p3

    iget-object v12, v0, LX/8z5;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1426788
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8z4;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, LX/8z5;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v13

    .line 1426789
    move-object/from16 v0, p3

    iget-object v8, v0, LX/8z5;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1426790
    move-object/from16 v0, p3

    iget-object v3, v0, LX/8z5;->h:LX/8z9;

    .line 1426791
    move-object/from16 v0, p3

    iget-object v14, v0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1426792
    invoke-virtual/range {p1 .. p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v15, v1, v2

    .line 1426793
    move-object/from16 v0, p4

    iget-object v1, v0, LX/8z7;->a:Landroid/text/style/CharacterStyle;

    invoke-virtual/range {p1 .. p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v4, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1426794
    if-eqz v8, :cond_0

    .line 1426795
    iget-object v1, v8, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 1426796
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v2, v15

    .line 1426797
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int v6, v2, v1

    .line 1426798
    move-object/from16 v0, p4

    iget-object v1, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    invoke-static {v1}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    const/16 v4, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v6, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1426799
    const/16 v1, 0x200c

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int v5, v1, v15

    .line 1426800
    sget-object v4, LX/8z2;->MINUTIAE:LX/8z2;

    const/4 v7, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v7}, LX/8z4;->a(LX/8z4;Landroid/text/SpannableStringBuilder;LX/8z9;LX/8z2;III)V

    .line 1426801
    invoke-virtual {v8}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v1

    add-int/lit8 v2, v5, 0x1

    const/16 v4, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1426802
    :cond_0
    if-eqz v13, :cond_2

    .line 1426803
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int v2, v1, v15

    .line 1426804
    move-object/from16 v0, p3

    iget-object v1, v0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1426805
    const/4 v11, 0x0

    :goto_0
    move-object/from16 v0, p3

    iget-object v1, v0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v11, v1, :cond_1

    .line 1426806
    move-object/from16 v0, p3

    iget-object v1, v0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v1, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1426807
    invoke-virtual {v13, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int v6, v2, v4

    .line 1426808
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int v7, v6, v1

    move-object/from16 v0, p4

    iget-object v8, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    sget-object v10, LX/8z2;->PERSON:LX/8z2;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object v9, v3

    invoke-static/range {v4 .. v11}, LX/8z4;->a(LX/8z4;Landroid/text/SpannableStringBuilder;IILandroid/text/style/CharacterStyle;LX/8z9;LX/8z2;I)V

    .line 1426809
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 1426810
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8z4;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, LX/8z5;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 1426811
    if-eqz v1, :cond_2

    .line 1426812
    invoke-virtual {v13, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int v6, v2, v4

    .line 1426813
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int v7, v6, v1

    move-object/from16 v0, p4

    iget-object v8, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    sget-object v10, LX/8z2;->PEOPLE:LX/8z2;

    const/4 v11, -0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object v9, v3

    invoke-static/range {v4 .. v11}, LX/8z4;->a(LX/8z4;Landroid/text/SpannableStringBuilder;IILandroid/text/style/CharacterStyle;LX/8z9;LX/8z2;I)V

    .line 1426814
    :cond_2
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1426815
    invoke-virtual {v12}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int v5, v1, v15

    .line 1426816
    invoke-virtual {v12}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int v6, v5, v1

    .line 1426817
    move-object/from16 v0, p4

    iget-object v1, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    invoke-static {v1}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    const/16 v2, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5, v6, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1426818
    sget-object v4, LX/8z2;->PLACE:LX/8z2;

    const/4 v7, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v7}, LX/8z4;->a(LX/8z4;Landroid/text/SpannableStringBuilder;LX/8z9;LX/8z2;III)V

    .line 1426819
    :cond_3
    if-eqz v14, :cond_4

    .line 1426820
    invoke-virtual {v14}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int v5, v1, v15

    .line 1426821
    invoke-virtual {v14}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int v6, v5, v1

    .line 1426822
    move-object/from16 v0, p4

    iget-object v1, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    invoke-static {v1}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    const/16 v2, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5, v6, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1426823
    sget-object v4, LX/8z2;->BRANDED_CONTENT:LX/8z2;

    const/4 v7, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v7}, LX/8z4;->a(LX/8z4;Landroid/text/SpannableStringBuilder;LX/8z9;LX/8z2;III)V

    .line 1426824
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(LX/8z5;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 1426855
    new-instance v0, LX/8z8;

    iget-object v1, p0, LX/8z4;->a:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/8z8;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/8z8;->a()LX/8z7;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/8z4;->a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;
    .locals 8

    .prologue
    .line 1426825
    iget-object v0, p1, LX/8z5;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Minutiae object parameter is not supported"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1426826
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1426827
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1426828
    iget-object v2, p1, LX/8z5;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1426829
    iget-object v1, p1, LX/8z5;->c:Ljava/lang/String;

    .line 1426830
    iget-object v3, p0, LX/8z4;->a:Landroid/content/res/Resources;

    invoke-virtual {p1, v3}, LX/8z5;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    .line 1426831
    iget-object v4, p1, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1426832
    if-eqz v3, :cond_8

    .line 1426833
    :goto_1
    move-object v3, v3

    .line 1426834
    if-nez v2, :cond_0

    if-eqz v1, :cond_6

    .line 1426835
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1426836
    :cond_1
    if-eqz v3, :cond_5

    .line 1426837
    iget-object v2, p0, LX/8z4;->a:Landroid/content/res/Resources;

    const v4, 0x7f081465

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1426838
    :goto_2
    move-object v1, v1

    .line 1426839
    if-nez v1, :cond_3

    .line 1426840
    :goto_3
    return-object v0

    .line 1426841
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1426842
    :cond_3
    iget-boolean v2, p1, LX/8z5;->f:Z

    if-eqz v2, :cond_4

    .line 1426843
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " \u2014 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1426844
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1426845
    invoke-direct {p0, v0, v1, p1, p2}, LX/8z4;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;LX/8z5;LX/8z7;)V

    goto :goto_3

    .line 1426846
    :cond_4
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_3

    .line 1426847
    :cond_5
    iget-object v2, p0, LX/8z4;->a:Landroid/content/res/Resources;

    const v3, 0x7f081464

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1426848
    :cond_6
    if-eqz v3, :cond_7

    .line 1426849
    iget-object v1, p0, LX/8z4;->a:Landroid/content/res/Resources;

    const v2, 0x7f081463

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v3, v4, v6

    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1426850
    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    .line 1426851
    :cond_8
    if-eqz v4, :cond_9

    .line 1426852
    iget-object v3, v4, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1426853
    goto :goto_1

    .line 1426854
    :cond_9
    const/4 v3, 0x0

    goto :goto_1
.end method
