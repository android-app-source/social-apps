.class public abstract LX/8tw;
.super LX/8tv;
.source ""


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1414288
    invoke-direct {p0}, LX/8tv;-><init>()V

    .line 1414289
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/8tw;->a:Ljava/lang/String;

    .line 1414290
    return-void

    .line 1414291
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/text/Editable;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1414292
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 1414293
    new-instance v1, LX/8u5;

    invoke-direct {v1, p1}, LX/8u5;-><init>(Ljava/lang/Object;)V

    const/16 v2, 0x11

    invoke-interface {p0, v1, v0, v0, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1414294
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;IILandroid/text/Editable;)V
.end method

.method public a(Ljava/lang/String;Landroid/text/Editable;)Z
    .locals 1

    .prologue
    .line 1414295
    iget-object v0, p0, LX/8tw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1414296
    const/4 v0, 0x0

    .line 1414297
    :goto_0
    return v0

    .line 1414298
    :cond_0
    invoke-virtual {p0, p2, p0}, LX/8tw;->b(Landroid/text/Editable;Ljava/lang/Object;)V

    .line 1414299
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lorg/xml/sax/Attributes;Landroid/text/Editable;)Z
    .locals 1

    .prologue
    .line 1414300
    iget-object v0, p0, LX/8tw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1414301
    const/4 v0, 0x0

    .line 1414302
    :goto_0
    return v0

    .line 1414303
    :cond_0
    invoke-static {p3, p0}, LX/8tw;->a(Landroid/text/Editable;Ljava/lang/Object;)V

    .line 1414304
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/text/Editable;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1414305
    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/8u5;

    invoke-interface {p1, v0, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8u5;

    .line 1414306
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    .line 1414307
    :cond_0
    if-ltz v1, :cond_2

    .line 1414308
    aget-object v2, v0, v1

    iget-object v2, v2, LX/8u5;->a:Ljava/lang/Object;

    if-ne v2, p2, :cond_0

    .line 1414309
    aget-object v0, v0, v1

    .line 1414310
    :goto_0
    move-object v0, v0

    .line 1414311
    if-nez v0, :cond_1

    .line 1414312
    :goto_1
    return-void

    .line 1414313
    :cond_1
    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 1414314
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    .line 1414315
    invoke-interface {p1, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1414316
    invoke-virtual {p0, p2, v1, v2, p1}, LX/8tw;->a(Ljava/lang/Object;IILandroid/text/Editable;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
