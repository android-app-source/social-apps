.class public final LX/8rd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/ui/ProfileListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V
    .locals 0

    .prologue
    .line 1409020
    iput-object p1, p0, LX/8rd;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1409021
    iget-object v0, p0, LX/8rd;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->l:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vI;

    .line 1409022
    iget-object v1, p0, LX/8rd;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    iget-object v1, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->f:LX/1nG;

    invoke-interface {v0}, LX/2vI;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-interface {v0}, LX/2vI;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/2vI;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1409023
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1409024
    invoke-interface {v0}, LX/2vI;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x285feb

    if-ne v3, v4, :cond_0

    .line 1409025
    invoke-static {v2, v0}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    .line 1409026
    :cond_0
    iget-object v0, p0, LX/8rd;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/8rd;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 1409027
    return-void
.end method
