.class public final LX/A5F;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1620809
    const/4 v12, 0x0

    .line 1620810
    const-wide/16 v10, 0x0

    .line 1620811
    const/4 v9, 0x0

    .line 1620812
    const/4 v8, 0x0

    .line 1620813
    const/4 v7, 0x0

    .line 1620814
    const/4 v6, 0x0

    .line 1620815
    const/4 v5, 0x0

    .line 1620816
    const/4 v4, 0x0

    .line 1620817
    const/4 v3, 0x0

    .line 1620818
    const/4 v2, 0x0

    .line 1620819
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 1620820
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1620821
    const/4 v2, 0x0

    .line 1620822
    :goto_0
    return v2

    .line 1620823
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v7, :cond_a

    .line 1620824
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1620825
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1620826
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v15, :cond_0

    if-eqz v3, :cond_0

    .line 1620827
    const-string v7, "all_share_stories"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1620828
    invoke-static/range {p0 .. p1}, LX/A5C;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 1620829
    :cond_1
    const-string v7, "creation_time"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1620830
    const/4 v2, 0x1

    .line 1620831
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 1620832
    :cond_2
    const-string v7, "external_url"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1620833
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 1620834
    :cond_3
    const-string v7, "id"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1620835
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 1620836
    :cond_4
    const-string v7, "link_media"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1620837
    invoke-static/range {p0 .. p1}, LX/A5D;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 1620838
    :cond_5
    const-string v7, "source"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1620839
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 1620840
    :cond_6
    const-string v7, "title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1620841
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 1620842
    :cond_7
    const-string v7, "url"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1620843
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 1620844
    :cond_8
    const-string v7, "video_share"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1620845
    invoke-static/range {p0 .. p1}, LX/A5E;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 1620846
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1620847
    :cond_a
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1620848
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1620849
    if-eqz v2, :cond_b

    .line 1620850
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1620851
    :cond_b
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1620852
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1620853
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1620854
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1620855
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1620856
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1620857
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1620858
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v13, v8

    move v14, v9

    move v8, v3

    move v9, v4

    move/from16 v16, v5

    move-wide v4, v10

    move v11, v6

    move/from16 v10, v16

    move v6, v12

    move v12, v7

    goto/16 :goto_1
.end method
