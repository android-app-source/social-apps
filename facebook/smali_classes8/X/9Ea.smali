.class public LX/9Ea;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9DS;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/9CC;

.field public final d:LX/9D1;

.field public final e:LX/03V;

.field public final f:LX/9Do;

.field private final g:LX/9Du;

.field private final h:LX/1K9;

.field public final i:LX/20j;

.field public final j:LX/1nK;

.field public final k:LX/3iO;

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Vi;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/9EZ;

.field public final n:LX/0ad;

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/1Ar;

.field public final q:Lcom/facebook/delights/floating/DelightsFireworks;

.field public final r:LX/0Zb;

.field public s:LX/9EL;

.field public t:LX/9F7;

.field public u:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private v:Lcom/facebook/graphql/model/GraphQLStory;

.field public w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1457155
    const-class v0, LX/9Ea;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9Ea;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;LX/1K9;LX/03V;LX/20j;LX/1nK;LX/3iO;LX/0ad;LX/0Ot;LX/9F8;LX/9EM;LX/1Ar;Lcom/facebook/delights/floating/DelightsFireworks;LX/0Zb;)V
    .locals 4
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9CC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9D1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/9Do;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/9Du;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/9EL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;",
            "LX/9CC;",
            "LX/9D1;",
            "LX/9Do;",
            "LX/9Du;",
            "LX/9EL;",
            "LX/1K9;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/20j;",
            "LX/1nK;",
            "LX/3iO;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/9F8;",
            "LX/9EM;",
            "LX/1Ar;",
            "Lcom/facebook/delights/floating/DelightsFireworks;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1457200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457201
    iput-object p1, p0, LX/9Ea;->b:LX/0QK;

    .line 1457202
    iput-object p2, p0, LX/9Ea;->c:LX/9CC;

    .line 1457203
    iput-object p3, p0, LX/9Ea;->d:LX/9D1;

    .line 1457204
    iput-object p8, p0, LX/9Ea;->e:LX/03V;

    .line 1457205
    iput-object p7, p0, LX/9Ea;->h:LX/1K9;

    .line 1457206
    iput-object p9, p0, LX/9Ea;->i:LX/20j;

    .line 1457207
    iput-object p10, p0, LX/9Ea;->j:LX/1nK;

    .line 1457208
    iput-object p11, p0, LX/9Ea;->k:LX/3iO;

    .line 1457209
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/9Ea;->l:Ljava/util/List;

    .line 1457210
    iput-object p4, p0, LX/9Ea;->f:LX/9Do;

    .line 1457211
    iput-object p5, p0, LX/9Ea;->g:LX/9Du;

    .line 1457212
    move-object/from16 v0, p12

    iput-object v0, p0, LX/9Ea;->n:LX/0ad;

    .line 1457213
    new-instance v1, LX/9EZ;

    invoke-direct {v1, p0}, LX/9EZ;-><init>(LX/9Ea;)V

    iput-object v1, p0, LX/9Ea;->m:LX/9EZ;

    .line 1457214
    move-object/from16 v0, p13

    iput-object v0, p0, LX/9Ea;->o:LX/0Ot;

    .line 1457215
    move-object/from16 v0, p18

    iput-object v0, p0, LX/9Ea;->r:LX/0Zb;

    .line 1457216
    iget-object v1, p0, LX/9Ea;->d:LX/9D1;

    if-eqz v1, :cond_0

    .line 1457217
    iget-object v1, p0, LX/9Ea;->g:LX/9Du;

    iget-object v2, p0, LX/9Ea;->d:LX/9D1;

    iget-object v3, p0, LX/9Ea;->c:LX/9CC;

    move-object/from16 v0, p14

    invoke-virtual {v0, v1, v2, v3}, LX/9F8;->a(LX/9Du;LX/9D1;LX/9CC;)LX/9F7;

    move-result-object v1

    iput-object v1, p0, LX/9Ea;->t:LX/9F7;

    .line 1457218
    :cond_0
    iput-object p6, p0, LX/9Ea;->s:LX/9EL;

    .line 1457219
    move-object/from16 v0, p16

    iput-object v0, p0, LX/9Ea;->p:LX/1Ar;

    .line 1457220
    move-object/from16 v0, p17

    iput-object v0, p0, LX/9Ea;->q:Lcom/facebook/delights/floating/DelightsFireworks;

    .line 1457221
    return-void
.end method

.method public static a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<",
            "Ljava/lang/String;",
            ">;>(",
            "Ljava/lang/Class",
            "<TE;>;",
            "LX/6Ve",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1457195
    iget-object v0, p0, LX/9Ea;->h:LX/1K9;

    iget-object v1, p0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    .line 1457196
    iget-object v1, p0, LX/9Ea;->h:LX/1K9;

    iget-object v2, p0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2, p2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v1

    .line 1457197
    iget-object v2, p0, LX/9Ea;->l:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1457198
    iget-object v0, p0, LX/9Ea;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1457199
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1457187
    iget-object v0, p0, LX/9Ea;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Vi;

    .line 1457188
    iget-object v2, p0, LX/9Ea;->h:LX/1K9;

    invoke-virtual {v2, v0}, LX/1K9;->a(LX/6Vi;)V

    goto :goto_0

    .line 1457189
    :cond_0
    iget-object v0, p0, LX/9Ea;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1457190
    iget-object v0, p0, LX/9Ea;->k:LX/3iO;

    invoke-virtual {v0, p0}, LX/3iO;->b(LX/3iN;)V

    .line 1457191
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9Ea;->w:Z

    .line 1457192
    iget-object v0, p0, LX/9Ea;->t:LX/9F7;

    if-eqz v0, :cond_1

    .line 1457193
    iget-object v0, p0, LX/9Ea;->t:LX/9F7;

    invoke-virtual {v0}, LX/9F7;->b()V

    .line 1457194
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1457222
    invoke-direct {p0}, LX/9Ea;->c()V

    .line 1457223
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1457160
    if-nez p1, :cond_1

    .line 1457161
    iput-object v1, p0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457162
    iput-object v1, p0, LX/9Ea;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1457163
    invoke-direct {p0}, LX/9Ea;->c()V

    .line 1457164
    :cond_0
    :goto_0
    return-void

    .line 1457165
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1457166
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457167
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_1
    iput-object v0, p0, LX/9Ea;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1457168
    iget-boolean v0, p0, LX/9Ea;->w:Z

    if-nez v0, :cond_0

    .line 1457169
    const/4 p1, 0x0

    .line 1457170
    const-class v0, LX/8px;

    new-instance v1, LX/9EQ;

    invoke-direct {v1, p0}, LX/9EQ;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457171
    const-class v0, LX/8q4;

    new-instance v1, LX/9ER;

    invoke-direct {v1, p0}, LX/9ER;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457172
    const-class v0, LX/8py;

    new-instance v1, LX/9ES;

    invoke-direct {v1, p0}, LX/9ES;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457173
    const-class v0, LX/8q2;

    new-instance v1, LX/9ET;

    invoke-direct {v1, p0}, LX/9ET;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457174
    const-class v0, LX/8q3;

    new-instance v1, LX/9EU;

    invoke-direct {v1, p0}, LX/9EU;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457175
    const-class v0, LX/82g;

    new-instance v1, LX/9EV;

    invoke-direct {v1, p0}, LX/9EV;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457176
    const-class v0, LX/82i;

    new-instance v1, LX/9EW;

    invoke-direct {v1, p0}, LX/9EW;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457177
    const-class v0, LX/82h;

    new-instance v1, LX/9EX;

    invoke-direct {v1, p0}, LX/9EX;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457178
    const-class v0, LX/8pz;

    new-instance v1, LX/9EY;

    invoke-direct {v1, p0}, LX/9EY;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457179
    const-class v0, LX/8q0;

    new-instance v1, LX/9EO;

    invoke-direct {v1, p0}, LX/9EO;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457180
    iget-object v0, p0, LX/9Ea;->d:LX/9D1;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/9Ea;->n:LX/0ad;

    sget-short v1, LX/0wn;->aI:S

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9Ea;->n:LX/0ad;

    sget-short v1, LX/0wn;->aH:S

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1457181
    :cond_2
    iget-object v0, p0, LX/9Ea;->t:LX/9F7;

    invoke-virtual {v0}, LX/9F7;->a()V

    .line 1457182
    const-class v0, LX/8q1;

    new-instance v1, LX/9EP;

    invoke-direct {v1, p0}, LX/9EP;-><init>(LX/9Ea;)V

    invoke-static {p0, v0, v1}, LX/9Ea;->a(LX/9Ea;Ljava/lang/Class;LX/6Ve;)V

    .line 1457183
    :cond_3
    iget-object v0, p0, LX/9Ea;->k:LX/3iO;

    invoke-virtual {v0, p0}, LX/3iO;->a(LX/3iN;)V

    .line 1457184
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9Ea;->w:Z

    .line 1457185
    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 1457186
    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 1457157
    iget-object v0, p0, LX/9Ea;->i:LX/20j;

    iget-object v1, p0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, p1}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1457158
    iget-object v1, p0, LX/9Ea;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457159
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1457156
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, LX/9Ea;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-void
.end method
