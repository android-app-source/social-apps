.class public LX/A8N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/A88;
.implements LX/A8H;


# instance fields
.field public a:LX/A7r;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/A8D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1627555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1627556
    new-instance v0, LX/A8J;

    invoke-direct {v0, p0}, LX/A8J;-><init>(LX/A8N;)V

    iput-object v0, p0, LX/A8N;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1627557
    return-void
.end method

.method public static a$redex0(LX/A8N;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, -0x2

    .line 1627558
    const/4 v0, 0x0

    .line 1627559
    new-instance v2, LX/A84;

    iget-object v1, p0, LX/A8N;->b:LX/A8D;

    invoke-virtual {v1}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, LX/A84;-><init>(Landroid/content/Context;)V

    .line 1627560
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1627561
    invoke-virtual {v2, v0}, LX/A84;->a(Landroid/view/View;)V

    .line 1627562
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 1627563
    goto :goto_0

    .line 1627564
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1627565
    invoke-virtual {v0, v1}, Landroid/view/View;->setMinimumWidth(I)V

    .line 1627566
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1627567
    if-nez v2, :cond_1

    .line 1627568
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v1, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1627569
    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 1627570
    :cond_1
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1627571
    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_2

    .line 1627572
    :cond_2
    return-void
.end method

.method public static d(LX/A8N;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1627573
    iget-object v0, p0, LX/A8N;->d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    if-eqz v0, :cond_0

    .line 1627574
    iget-object v0, p0, LX/A8N;->d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1627575
    :cond_0
    iget-object v0, p0, LX/A8N;->b:LX/A8D;

    invoke-virtual {v0}, LX/0ht;->h()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1627576
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1627577
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_2

    .line 1627578
    iget-object v2, p0, LX/A8N;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1627579
    :cond_1
    :goto_0
    iput-object v1, p0, LX/A8N;->c:Lcom/facebook/universalfeedback/ui/UniversalFeedbackExplanationRequestView;

    .line 1627580
    iput-object v1, p0, LX/A8N;->b:LX/A8D;

    .line 1627581
    iput-object v1, p0, LX/A8N;->d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    .line 1627582
    return-void

    .line 1627583
    :cond_2
    iget-object v2, p0, LX/A8N;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1627584
    iput p1, p0, LX/A8N;->e:I

    .line 1627585
    iget-object v0, p0, LX/A8N;->a:LX/A7r;

    if-eqz v0, :cond_0

    .line 1627586
    iget-object v0, p0, LX/A8N;->a:LX/A7r;

    .line 1627587
    iget-object p0, v0, LX/A7r;->a:LX/A7t;

    .line 1627588
    iput p1, p0, LX/A7t;->i:I

    .line 1627589
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1627590
    iget-object v0, p0, LX/A8N;->a:LX/A7r;

    if-eqz v0, :cond_0

    .line 1627591
    iget-object v0, p0, LX/A8N;->a:LX/A7r;

    .line 1627592
    iget-object p0, v0, LX/A7r;->a:LX/A7t;

    .line 1627593
    iput-object p1, p0, LX/A7t;->j:Ljava/lang/String;

    .line 1627594
    :cond_0
    return-void
.end method
