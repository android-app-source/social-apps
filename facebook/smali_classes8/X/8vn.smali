.class public LX/8vn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final A:I

.field private final B:I

.field private final C:I

.field private final D:I

.field private final a:I

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field public f:I

.field public g:I

.field private final h:I

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:J

.field private v:F

.field private final w:Landroid/view/animation/Interpolator;

.field private x:I

.field private y:F

.field private final z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/high16 v4, 0x40800000    # 4.0f

    const/high16 v3, 0x3f000000    # 0.5f

    .line 1419702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1419703
    const/4 v0, 0x0

    iput v0, p0, LX/8vn;->x:I

    .line 1419704
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8vn;->z:Landroid/graphics/Rect;

    .line 1419705
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1419706
    const v1, 0x7f021b1f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/8vn;->b:Landroid/graphics/drawable/Drawable;

    .line 1419707
    const v1, 0x7f021b20

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/8vn;->c:Landroid/graphics/drawable/Drawable;

    .line 1419708
    iput p2, p0, LX/8vn;->a:I

    .line 1419709
    iget-object v1, p0, LX/8vn;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, LX/8vn;->A:I

    .line 1419710
    iget-object v1, p0, LX/8vn;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, LX/8vn;->B:I

    .line 1419711
    iget-object v1, p0, LX/8vn;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, LX/8vn;->C:I

    .line 1419712
    iget v1, p0, LX/8vn;->B:I

    int-to-float v1, v1

    mul-float/2addr v1, v4

    iget v2, p0, LX/8vn;->B:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, LX/8vn;->C:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v1, v2

    iget v2, p0, LX/8vn;->B:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, LX/8vn;->D:I

    .line 1419713
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43960000    # 300.0f

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, LX/8vn;->h:I

    .line 1419714
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/8vn;->w:Landroid/view/animation/Interpolator;

    .line 1419715
    return-void
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/high16 v7, 0x447a0000    # 1000.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 1419665
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 1419666
    iget-wide v2, p0, LX/8vn;->u:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    iget v1, p0, LX/8vn;->v:F

    div-float/2addr v0, v1

    invoke-static {v0, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1419667
    iget-object v1, p0, LX/8vn;->w:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 1419668
    iget v2, p0, LX/8vn;->m:F

    iget v3, p0, LX/8vn;->n:F

    iget v4, p0, LX/8vn;->m:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    iput v2, p0, LX/8vn;->i:F

    .line 1419669
    iget v2, p0, LX/8vn;->o:F

    iget v3, p0, LX/8vn;->p:F

    iget v4, p0, LX/8vn;->o:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    iput v2, p0, LX/8vn;->j:F

    .line 1419670
    iget v2, p0, LX/8vn;->q:F

    iget v3, p0, LX/8vn;->r:F

    iget v4, p0, LX/8vn;->q:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    iput v2, p0, LX/8vn;->k:F

    .line 1419671
    iget v2, p0, LX/8vn;->s:F

    iget v3, p0, LX/8vn;->t:F

    iget v4, p0, LX/8vn;->s:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    iput v2, p0, LX/8vn;->l:F

    .line 1419672
    const v2, 0x3f7fbe77    # 0.999f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_0

    .line 1419673
    iget v0, p0, LX/8vn;->x:I

    packed-switch v0, :pswitch_data_0

    .line 1419674
    :cond_0
    :goto_0
    return-void

    .line 1419675
    :pswitch_0
    iput v8, p0, LX/8vn;->x:I

    .line 1419676
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/8vn;->u:J

    .line 1419677
    iput v7, p0, LX/8vn;->v:F

    .line 1419678
    iget v0, p0, LX/8vn;->i:F

    iput v0, p0, LX/8vn;->m:F

    .line 1419679
    iget v0, p0, LX/8vn;->j:F

    iput v0, p0, LX/8vn;->o:F

    .line 1419680
    iget v0, p0, LX/8vn;->k:F

    iput v0, p0, LX/8vn;->q:F

    .line 1419681
    iget v0, p0, LX/8vn;->l:F

    iput v0, p0, LX/8vn;->s:F

    .line 1419682
    iput v5, p0, LX/8vn;->n:F

    .line 1419683
    iput v5, p0, LX/8vn;->p:F

    .line 1419684
    iput v5, p0, LX/8vn;->r:F

    .line 1419685
    iput v5, p0, LX/8vn;->t:F

    goto :goto_0

    .line 1419686
    :pswitch_1
    const/4 v0, 0x4

    iput v0, p0, LX/8vn;->x:I

    .line 1419687
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/8vn;->u:J

    .line 1419688
    iput v7, p0, LX/8vn;->v:F

    .line 1419689
    iget v0, p0, LX/8vn;->i:F

    iput v0, p0, LX/8vn;->m:F

    .line 1419690
    iget v0, p0, LX/8vn;->j:F

    iput v0, p0, LX/8vn;->o:F

    .line 1419691
    iget v0, p0, LX/8vn;->k:F

    iput v0, p0, LX/8vn;->q:F

    .line 1419692
    iget v0, p0, LX/8vn;->l:F

    iput v0, p0, LX/8vn;->s:F

    .line 1419693
    iput v5, p0, LX/8vn;->n:F

    .line 1419694
    iput v5, p0, LX/8vn;->p:F

    .line 1419695
    iput v5, p0, LX/8vn;->r:F

    .line 1419696
    iput v5, p0, LX/8vn;->t:F

    goto :goto_0

    .line 1419697
    :pswitch_2
    iget v0, p0, LX/8vn;->t:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_1

    iget v0, p0, LX/8vn;->t:F

    iget v2, p0, LX/8vn;->t:F

    mul-float/2addr v0, v2

    div-float v0, v6, v0

    .line 1419698
    :goto_1
    iget v2, p0, LX/8vn;->o:F

    iget v3, p0, LX/8vn;->p:F

    iget v4, p0, LX/8vn;->o:F

    sub-float/2addr v3, v4

    mul-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    iput v0, p0, LX/8vn;->j:F

    .line 1419699
    iput v8, p0, LX/8vn;->x:I

    goto :goto_0

    .line 1419700
    :cond_1
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_1

    .line 1419701
    :pswitch_3
    const/4 v0, 0x0

    iput v0, p0, LX/8vn;->x:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Z)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1419662
    iget-object v1, p0, LX/8vn;->z:Landroid/graphics/Rect;

    iget v2, p0, LX/8vn;->d:I

    iget v3, p0, LX/8vn;->D:I

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1419663
    iget-object v1, p0, LX/8vn;->z:Landroid/graphics/Rect;

    iget v2, p0, LX/8vn;->f:I

    iget v3, p0, LX/8vn;->g:I

    if-eqz p1, :cond_0

    iget v0, p0, LX/8vn;->D:I

    :cond_0
    sub-int v0, v3, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1419664
    iget-object v0, p0, LX/8vn;->z:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final a(F)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/high16 v6, 0x40e00000    # 7.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1419639
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 1419640
    iget v2, p0, LX/8vn;->x:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, LX/8vn;->u:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    iget v3, p0, LX/8vn;->v:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 1419641
    :goto_0
    return-void

    .line 1419642
    :cond_0
    iget v2, p0, LX/8vn;->x:I

    if-eq v2, v7, :cond_1

    .line 1419643
    iput v5, p0, LX/8vn;->l:F

    .line 1419644
    :cond_1
    iput v7, p0, LX/8vn;->x:I

    .line 1419645
    iput-wide v0, p0, LX/8vn;->u:J

    .line 1419646
    const/high16 v0, 0x43270000    # 167.0f

    iput v0, p0, LX/8vn;->v:F

    .line 1419647
    iget v0, p0, LX/8vn;->y:F

    add-float/2addr v0, p1

    iput v0, p0, LX/8vn;->y:F

    .line 1419648
    iget v0, p0, LX/8vn;->y:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1419649
    const v1, 0x3f19999a    # 0.6f

    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, LX/8vn;->m:F

    iput v1, p0, LX/8vn;->i:F

    .line 1419650
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v6

    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/8vn;->o:F

    iput v0, p0, LX/8vn;->j:F

    .line 1419651
    iget v0, p0, LX/8vn;->k:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3f8ccccd    # 1.1f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/8vn;->q:F

    iput v0, p0, LX/8vn;->k:F

    .line 1419652
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1419653
    cmpl-float v1, p1, v4

    if-lez v1, :cond_2

    iget v1, p0, LX/8vn;->y:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_2

    .line 1419654
    neg-float v0, v0

    .line 1419655
    :cond_2
    iget v1, p0, LX/8vn;->y:F

    cmpl-float v1, v1, v4

    if-nez v1, :cond_3

    .line 1419656
    iput v4, p0, LX/8vn;->l:F

    .line 1419657
    :cond_3
    const/high16 v1, 0x40800000    # 4.0f

    iget v2, p0, LX/8vn;->l:F

    mul-float/2addr v0, v6

    add-float/2addr v0, v2

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/8vn;->s:F

    iput v0, p0, LX/8vn;->l:F

    .line 1419658
    iget v0, p0, LX/8vn;->i:F

    iput v0, p0, LX/8vn;->n:F

    .line 1419659
    iget v0, p0, LX/8vn;->j:F

    iput v0, p0, LX/8vn;->p:F

    .line 1419660
    iget v0, p0, LX/8vn;->k:F

    iput v0, p0, LX/8vn;->r:F

    .line 1419661
    iget v0, p0, LX/8vn;->l:F

    iput v0, p0, LX/8vn;->t:F

    goto/16 :goto_0
.end method

.method public final a(I)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 1419626
    const/4 v0, 0x2

    iput v0, p0, LX/8vn;->x:I

    .line 1419627
    const/16 v0, 0x64

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1419628
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, LX/8vn;->u:J

    .line 1419629
    const v1, 0x3dcccccd    # 0.1f

    int-to-float v2, v0

    const v3, 0x3cf5c28f    # 0.03f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p0, LX/8vn;->v:F

    .line 1419630
    iput v4, p0, LX/8vn;->m:F

    .line 1419631
    iput v4, p0, LX/8vn;->o:F

    iput v4, p0, LX/8vn;->j:F

    .line 1419632
    iput v5, p0, LX/8vn;->q:F

    .line 1419633
    iput v4, p0, LX/8vn;->s:F

    .line 1419634
    const/4 v1, 0x0

    mul-int/lit8 v2, v0, 0x8

    const/4 v3, 0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LX/8vn;->n:F

    .line 1419635
    mul-int/lit8 v1, v0, 0x8

    int-to-float v1, v1

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, LX/8vn;->p:F

    .line 1419636
    const v1, 0x3ccccccd    # 0.025f

    div-int/lit8 v2, v0, 0x64

    mul-int/2addr v2, v0

    int-to-float v2, v2

    const v3, 0x391d4952    # 1.5E-4f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x3fe00000    # 1.75f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, LX/8vn;->t:F

    .line 1419637
    iget v1, p0, LX/8vn;->q:F

    mul-int/lit8 v0, v0, 0x10

    int-to-float v0, v0

    const v2, 0x3727c5ac    # 1.0E-5f

    mul-float/2addr v0, v2

    invoke-static {v0, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/8vn;->r:F

    .line 1419638
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 1419623
    iput p1, p0, LX/8vn;->d:I

    .line 1419624
    iput p2, p0, LX/8vn;->e:I

    .line 1419625
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1419622
    iget v0, p0, LX/8vn;->x:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Canvas;)Z
    .locals 7

    .prologue
    const/high16 v6, 0x437f0000    # 255.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1419594
    invoke-direct {p0}, LX/8vn;->d()V

    .line 1419595
    iget-object v1, p0, LX/8vn;->c:Landroid/graphics/drawable/Drawable;

    iget v2, p0, LX/8vn;->k:F

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    mul-float/2addr v2, v6

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1419596
    iget v1, p0, LX/8vn;->B:I

    int-to-float v1, v1

    iget v2, p0, LX/8vn;->l:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/8vn;->B:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, LX/8vn;->C:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v1, v2

    iget v2, p0, LX/8vn;->B:I

    int-to-float v2, v2

    const/high16 v3, 0x40800000    # 4.0f

    mul-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-int v1, v1

    .line 1419597
    iget-object v2, p0, LX/8vn;->c:Landroid/graphics/drawable/Drawable;

    iget v3, p0, LX/8vn;->d:I

    invoke-virtual {v2, v0, v0, v3, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1419598
    iget-object v2, p0, LX/8vn;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1419599
    iget-object v2, p0, LX/8vn;->b:Landroid/graphics/drawable/Drawable;

    iget v3, p0, LX/8vn;->i:F

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1419600
    iget v2, p0, LX/8vn;->A:I

    int-to-float v2, v2

    iget v3, p0, LX/8vn;->j:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1419601
    iget-object v3, p0, LX/8vn;->b:Landroid/graphics/drawable/Drawable;

    iget v4, p0, LX/8vn;->d:I

    invoke-virtual {v3, v0, v0, v4, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1419602
    iget-object v3, p0, LX/8vn;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1419603
    iget v3, p0, LX/8vn;->x:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    if-nez v1, :cond_0

    if-nez v2, :cond_0

    .line 1419604
    iput v0, p0, LX/8vn;->x:I

    .line 1419605
    :cond_0
    iget v1, p0, LX/8vn;->x:I

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1419620
    const/4 v0, 0x0

    iput v0, p0, LX/8vn;->x:I

    .line 1419621
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1419606
    iput v2, p0, LX/8vn;->y:F

    .line 1419607
    iget v0, p0, LX/8vn;->x:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/8vn;->x:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1419608
    :goto_0
    return-void

    .line 1419609
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, LX/8vn;->x:I

    .line 1419610
    iget v0, p0, LX/8vn;->i:F

    iput v0, p0, LX/8vn;->m:F

    .line 1419611
    iget v0, p0, LX/8vn;->j:F

    iput v0, p0, LX/8vn;->o:F

    .line 1419612
    iget v0, p0, LX/8vn;->k:F

    iput v0, p0, LX/8vn;->q:F

    .line 1419613
    iget v0, p0, LX/8vn;->l:F

    iput v0, p0, LX/8vn;->s:F

    .line 1419614
    iput v2, p0, LX/8vn;->n:F

    .line 1419615
    iput v2, p0, LX/8vn;->p:F

    .line 1419616
    iput v2, p0, LX/8vn;->r:F

    .line 1419617
    iput v2, p0, LX/8vn;->t:F

    .line 1419618
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/8vn;->u:J

    .line 1419619
    const/high16 v0, 0x447a0000    # 1000.0f

    iput v0, p0, LX/8vn;->v:F

    goto :goto_0
.end method
