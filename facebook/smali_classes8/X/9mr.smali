.class public final LX/9mr;
.super Landroid/os/AsyncTask;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "LX/9ms;",
        "Ljava/lang/Void;",
        "LX/9mt",
        "<",
        "LX/5pY;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9mu;


# direct methods
.method public constructor <init>(LX/9mu;)V
    .locals 0

    .prologue
    .line 1536186
    iput-object p1, p0, LX/9mr;->a:LX/9mu;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/9mu;B)V
    .locals 0

    .prologue
    .line 1536185
    invoke-direct {p0, p1}, LX/9mr;-><init>(LX/9mu;)V

    return-void
.end method

.method private varargs a([LX/9ms;)LX/9mt;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/9ms;",
            ")",
            "LX/9mt",
            "<",
            "LX/5pY;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1536225
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1536226
    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    aget-object v1, p1, v0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1536227
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    .line 1536228
    iget-object v1, v0, LX/9ms;->b:LX/5q8;

    move-object v0, v1

    .line 1536229
    invoke-interface {v0}, LX/5q8;->a()Lcom/facebook/react/cxxbridge/JavaScriptExecutor;

    move-result-object v0

    .line 1536230
    iget-object v1, p0, LX/9mr;->a:LX/9mu;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    .line 1536231
    iget-object p0, v2, LX/9ms;->c:LX/346;

    move-object v2, p0

    .line 1536232
    invoke-static {v1, v0, v2}, LX/9mu;->a(LX/9mu;Lcom/facebook/react/cxxbridge/JavaScriptExecutor;LX/346;)LX/5pY;

    move-result-object v0

    invoke-static {v0}, LX/9mt;->a(Ljava/lang/Object;)LX/9mt;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1536233
    :goto_0
    return-object v0

    .line 1536234
    :catch_0
    move-exception v0

    .line 1536235
    invoke-static {v0}, LX/9mt;->a(Ljava/lang/Exception;)LX/9mt;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/9mt;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9mt",
            "<",
            "LX/5pY;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1536206
    :try_start_0
    iget-object v1, p0, LX/9mr;->a:LX/9mu;

    invoke-virtual {p1}, LX/9mt;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pY;

    invoke-static {v1, v0}, LX/9mu;->b(LX/9mu;LX/5pY;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1536207
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    .line 1536208
    iput-object v3, v0, LX/9mu;->e:LX/9mr;

    .line 1536209
    :goto_0
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    iget-object v0, v0, LX/9mu;->d:LX/9ms;

    if-eqz v0, :cond_0

    .line 1536210
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    iget-object v1, p0, LX/9mr;->a:LX/9mu;

    iget-object v1, v1, LX/9mu;->d:LX/9ms;

    .line 1536211
    iget-object v2, v1, LX/9ms;->b:LX/5q8;

    move-object v1, v2

    .line 1536212
    iget-object v2, p0, LX/9mr;->a:LX/9mu;

    iget-object v2, v2, LX/9mu;->d:LX/9ms;

    .line 1536213
    iget-object p1, v2, LX/9ms;->c:LX/346;

    move-object v2, p1

    .line 1536214
    invoke-static {v0, v1, v2}, LX/9mu;->a$redex0(LX/9mu;LX/5q8;LX/346;)V

    .line 1536215
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    .line 1536216
    iput-object v3, v0, LX/9mu;->d:LX/9ms;

    .line 1536217
    :cond_0
    return-void

    .line 1536218
    :catch_0
    move-exception v0

    .line 1536219
    :try_start_1
    iget-object v1, p0, LX/9mr;->a:LX/9mu;

    iget-object v1, v1, LX/9mu;->i:LX/5qI;

    invoke-interface {v1, v0}, LX/0o1;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1536220
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    .line 1536221
    iput-object v3, v0, LX/9mu;->e:LX/9mr;

    .line 1536222
    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/9mr;->a:LX/9mu;

    .line 1536223
    iput-object v3, v1, LX/9mu;->e:LX/9mr;

    .line 1536224
    throw v0
.end method

.method private b(LX/9mt;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9mt",
            "<",
            "LX/5pY;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1536195
    :try_start_0
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    iget-object v1, v0, LX/9mu;->t:LX/349;

    invoke-virtual {p1}, LX/9mt;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, LX/349;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1536196
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    .line 1536197
    iput-object v3, v0, LX/9mu;->e:LX/9mr;

    .line 1536198
    :goto_0
    return-void

    .line 1536199
    :catch_0
    move-exception v0

    .line 1536200
    :try_start_1
    const-string v1, "React"

    const-string v2, "Caught exception after cancelling react context init"

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1536201
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    .line 1536202
    iput-object v3, v0, LX/9mu;->e:LX/9mr;

    .line 1536203
    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/9mr;->a:LX/9mu;

    .line 1536204
    iput-object v3, v1, LX/9mu;->e:LX/9mr;

    .line 1536205
    throw v0
.end method


# virtual methods
.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1536236
    check-cast p1, [LX/9ms;

    invoke-direct {p0, p1}, LX/9mr;->a([LX/9ms;)LX/9mt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1536194
    check-cast p1, LX/9mt;

    invoke-direct {p0, p1}, LX/9mr;->b(LX/9mt;)V

    return-void
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1536193
    check-cast p1, LX/9mt;

    invoke-direct {p0, p1}, LX/9mr;->a(LX/9mt;)V

    return-void
.end method

.method public final onPreExecute()V
    .locals 2

    .prologue
    .line 1536187
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    iget-object v0, v0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_0

    .line 1536188
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    iget-object v1, p0, LX/9mr;->a:LX/9mu;

    iget-object v1, v1, LX/9mu;->l:LX/5pX;

    .line 1536189
    invoke-static {v0, v1}, LX/9mu;->a$redex0(LX/9mu;LX/5pX;)V

    .line 1536190
    iget-object v0, p0, LX/9mr;->a:LX/9mu;

    const/4 v1, 0x0

    .line 1536191
    iput-object v1, v0, LX/9mu;->l:LX/5pX;

    .line 1536192
    :cond_0
    return-void
.end method
