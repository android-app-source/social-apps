.class public final LX/AED;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Qd;


# instance fields
.field public final synthetic a:LX/AEE;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic e:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic f:Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;LX/AEE;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/1Pq;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1646183
    iput-object p1, p0, LX/AED;->f:Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;

    iput-object p2, p0, LX/AED;->a:LX/AEE;

    iput-object p3, p0, LX/AED;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iput-object p4, p0, LX/AED;->c:LX/1Pq;

    iput-object p5, p0, LX/AED;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p6, p0, LX/AED;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)V
    .locals 4

    .prologue
    .line 1646167
    iget-object v0, p0, LX/AED;->a:LX/AEE;

    .line 1646168
    iget-object v1, v0, LX/AEE;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v0, v1

    .line 1646169
    if-ne p1, v0, :cond_0

    .line 1646170
    :goto_0
    return-void

    .line 1646171
    :cond_0
    iget-object v0, p0, LX/AED;->a:LX/AEE;

    .line 1646172
    iput-object p1, v0, LX/AEE;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 1646173
    iget-object v0, p0, LX/AED;->a:LX/AEE;

    iget-object v1, p0, LX/AED;->f:Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;

    invoke-static {v1, p1}, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->a$redex0(Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-result-object v1

    .line 1646174
    iput-object v1, v0, LX/AEE;->e:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 1646175
    iget-object v0, p0, LX/AED;->a:LX/AEE;

    iget-object v1, p0, LX/AED;->f:Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;

    iget-object v2, p0, LX/AED;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1646176
    if-eqz p1, :cond_1

    .line 1646177
    iget-object v3, v1, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->c:LX/2v5;

    invoke-virtual {v3, p1}, LX/2v5;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)Ljava/lang/String;

    move-result-object v3

    .line 1646178
    if-eqz v3, :cond_1

    .line 1646179
    :goto_1
    move-object v1, v3

    .line 1646180
    iput-object v1, v0, LX/AEE;->f:Ljava/lang/String;

    .line 1646181
    iget-object v0, p0, LX/AED;->c:LX/1Pq;

    check-cast v0, LX/1Pr;

    new-instance v1, LX/AEC;

    iget-object v2, p0, LX/AED;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v1, v2}, LX/AEC;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    iget-object v2, p0, LX/AED;->a:LX/AEE;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1646182
    iget-object v0, p0, LX/AED;->c:LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/AED;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
