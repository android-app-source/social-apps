.class public final LX/9xR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 1589815
    const/16 v20, 0x0

    .line 1589816
    const/16 v19, 0x0

    .line 1589817
    const/16 v18, 0x0

    .line 1589818
    const/16 v17, 0x0

    .line 1589819
    const/16 v16, 0x0

    .line 1589820
    const/4 v15, 0x0

    .line 1589821
    const/4 v14, 0x0

    .line 1589822
    const/4 v13, 0x0

    .line 1589823
    const/4 v12, 0x0

    .line 1589824
    const/4 v11, 0x0

    .line 1589825
    const/4 v10, 0x0

    .line 1589826
    const/4 v9, 0x0

    .line 1589827
    const/4 v8, 0x0

    .line 1589828
    const/4 v7, 0x0

    .line 1589829
    const/4 v6, 0x0

    .line 1589830
    const/4 v5, 0x0

    .line 1589831
    const/4 v4, 0x0

    .line 1589832
    const/4 v3, 0x0

    .line 1589833
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 1589834
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1589835
    const/4 v3, 0x0

    .line 1589836
    :goto_0
    return v3

    .line 1589837
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1589838
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_11

    .line 1589839
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 1589840
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1589841
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 1589842
    const-string v22, "__type__"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2

    const-string v22, "__typename"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 1589843
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v20

    goto :goto_1

    .line 1589844
    :cond_3
    const-string v22, "android_urls"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1589845
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 1589846
    :cond_4
    const-string v22, "app_section"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 1589847
    invoke-static/range {p0 .. p1}, LX/8sg;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1589848
    :cond_5
    const-string v22, "backing_application"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 1589849
    invoke-static/range {p0 .. p1}, LX/6RK;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1589850
    :cond_6
    const-string v22, "formattype"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 1589851
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto :goto_1

    .line 1589852
    :cond_7
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 1589853
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1589854
    :cond_8
    const-string v22, "is_multi_company_group"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 1589855
    const/4 v5, 0x1

    .line 1589856
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 1589857
    :cond_9
    const-string v22, "is_viewer_coworker"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 1589858
    const/4 v4, 0x1

    .line 1589859
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 1589860
    :cond_a
    const-string v22, "is_work_user"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 1589861
    const/4 v3, 0x1

    .line 1589862
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 1589863
    :cond_b
    const-string v22, "name"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 1589864
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1589865
    :cond_c
    const-string v22, "page"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 1589866
    invoke-static/range {p0 .. p1}, LX/9xQ;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1589867
    :cond_d
    const-string v22, "profile_picture"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 1589868
    invoke-static/range {p0 .. p1}, LX/8sX;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1589869
    :cond_e
    const-string v22, "redirection_info"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_f

    .line 1589870
    invoke-static/range {p0 .. p1}, LX/5Q3;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1589871
    :cond_f
    const-string v22, "tag"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_10

    .line 1589872
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1589873
    :cond_10
    const-string v22, "url"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1589874
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1589875
    :cond_11
    const/16 v21, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1589876
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1589877
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1589878
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1589879
    const/16 v18, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1589880
    const/16 v17, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1589881
    const/16 v16, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1589882
    if-eqz v5, :cond_12

    .line 1589883
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->a(IZ)V

    .line 1589884
    :cond_12
    if-eqz v4, :cond_13

    .line 1589885
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 1589886
    :cond_13
    if-eqz v3, :cond_14

    .line 1589887
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(IZ)V

    .line 1589888
    :cond_14
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1589889
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1589890
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1589891
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1589892
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1589893
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1589894
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1589895
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1589896
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1589897
    if-eqz v0, :cond_0

    .line 1589898
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589899
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1589900
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1589901
    if-eqz v0, :cond_1

    .line 1589902
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589903
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1589904
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589905
    if-eqz v0, :cond_2

    .line 1589906
    const-string v1, "app_section"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589907
    invoke-static {p0, v0, p2}, LX/8sg;->a(LX/15i;ILX/0nX;)V

    .line 1589908
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589909
    if-eqz v0, :cond_3

    .line 1589910
    const-string v1, "backing_application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589911
    invoke-static {p0, v0, p2, p3}, LX/6RK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589912
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1589913
    if-eqz v0, :cond_4

    .line 1589914
    const-string v0, "formattype"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589915
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589916
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589917
    if-eqz v0, :cond_5

    .line 1589918
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589919
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589920
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589921
    if-eqz v0, :cond_6

    .line 1589922
    const-string v1, "is_multi_company_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589923
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589924
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589925
    if-eqz v0, :cond_7

    .line 1589926
    const-string v1, "is_viewer_coworker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589927
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589928
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589929
    if-eqz v0, :cond_8

    .line 1589930
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589931
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589932
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589933
    if-eqz v0, :cond_9

    .line 1589934
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589935
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589936
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589937
    if-eqz v0, :cond_a

    .line 1589938
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589939
    invoke-static {p0, v0, p2, p3}, LX/9xQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589940
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589941
    if-eqz v0, :cond_b

    .line 1589942
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589943
    invoke-static {p0, v0, p2}, LX/8sX;->a(LX/15i;ILX/0nX;)V

    .line 1589944
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589945
    if-eqz v0, :cond_c

    .line 1589946
    const-string v1, "redirection_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589947
    invoke-static {p0, v0, p2, p3}, LX/5Q3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589948
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589949
    if-eqz v0, :cond_d

    .line 1589950
    const-string v1, "tag"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589951
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589952
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589953
    if-eqz v0, :cond_e

    .line 1589954
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589955
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589956
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1589957
    return-void
.end method
