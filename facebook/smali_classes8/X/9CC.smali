.class public LX/9CC;
.super LX/62C;
.source ""

# interfaces
.implements LX/1Qr;


# instance fields
.field public final a:LX/9E8;

.field public final b:LX/9E8;

.field private final c:LX/9F5;

.field private final d:LX/9Dz;

.field public e:LX/1R4;


# direct methods
.method public constructor <init>(LX/9E8;LX/9E8;LX/9F5;ZLX/9Dz;)V
    .locals 2
    .param p1    # LX/9E8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9E8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9F5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1453594
    const/4 v0, 0x3

    new-array v0, v0, [LX/1Cw;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    invoke-direct {p0, p4, v0}, LX/62C;-><init>(Z[LX/1Cw;)V

    .line 1453595
    iput-object p1, p0, LX/9CC;->a:LX/9E8;

    .line 1453596
    iput-object p2, p0, LX/9CC;->b:LX/9E8;

    .line 1453597
    iput-object p3, p0, LX/9CC;->c:LX/9F5;

    .line 1453598
    iput-object p5, p0, LX/9CC;->d:LX/9Dz;

    .line 1453599
    return-void
.end method


# virtual methods
.method public final a(LX/5Mj;)V
    .locals 1

    .prologue
    .line 1453600
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->a(LX/5Mj;)V

    .line 1453601
    iget-object v0, p0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->a(LX/5Mj;)V

    .line 1453602
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1453603
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->a(Landroid/content/res/Configuration;)V

    .line 1453604
    iget-object v0, p0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->a(Landroid/content/res/Configuration;)V

    .line 1453605
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1453606
    if-nez p1, :cond_0

    .line 1453607
    :goto_0
    return-void

    .line 1453608
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/9CC;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1453615
    invoke-static {}, LX/9CF;->c()Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1453616
    iget-object v1, p0, LX/9CC;->d:LX/9Dz;

    .line 1453617
    iget-object v2, v1, LX/9Dz;->b:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1453618
    iget-object v2, v1, LX/9Dz;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453619
    const v0, -0x31105e9f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1453620
    return-void
.end method

.method public final b(I)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1453609
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/62C;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 1453610
    :cond_0
    :goto_0
    return-object v0

    .line 1453611
    :cond_1
    iget-object v1, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v1}, LX/99a;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_2

    .line 1453612
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/9E8;->a(I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    goto :goto_0

    .line 1453613
    :cond_2
    iget-object v1, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v1}, LX/99a;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    iget-object v2, p0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v2}, LX/99a;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1453614
    iget-object v0, p0, LX/9CC;->b:LX/9E8;

    iget-object v1, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v1}, LX/99a;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/9E8;->a(I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1453621
    if-eqz p1, :cond_0

    .line 1453622
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1453623
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453624
    :goto_0
    invoke-static {v0}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v0

    sget-object v1, LX/21y;->RANKED_ORDER:LX/21y;

    if-ne v0, v1, :cond_1

    .line 1453625
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/9E8;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1453626
    :goto_1
    return-void

    .line 1453627
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1453628
    :cond_1
    iget-object v0, p0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v0, p1}, LX/9E8;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 1453592
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0}, LX/99a;->getCount()I

    move-result v0

    .line 1453593
    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->c(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/9CC;->b:LX/9E8;

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, LX/1UF;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1453576
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0}, LX/1UF;->d()I

    move-result v0

    iget-object v1, p0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v1}, LX/1UF;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final e()LX/1R4;
    .locals 3

    .prologue
    .line 1453587
    iget-object v0, p0, LX/9CC;->e:LX/1R4;

    if-eqz v0, :cond_0

    .line 1453588
    :goto_0
    iget-object v0, p0, LX/9CC;->e:LX/1R4;

    return-object v0

    .line 1453589
    :cond_0
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0}, LX/1UF;->e()LX/1R4;

    move-result-object v0

    .line 1453590
    iget-object v1, p0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v1}, LX/1UF;->e()LX/1R4;

    move-result-object v1

    .line 1453591
    new-instance v2, LX/9CB;

    invoke-direct {v2, p0, v0, v1}, LX/9CB;-><init>(LX/9CC;LX/1R4;LX/1R4;)V

    iput-object v2, p0, LX/9CC;->e:LX/1R4;

    goto :goto_0
.end method

.method public final g(I)I
    .locals 2

    .prologue
    .line 1453585
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0}, LX/99a;->getCount()I

    move-result v0

    .line 1453586
    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->g(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/9CC;->b:LX/9E8;

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, LX/1UF;->g(I)I

    move-result v0

    goto :goto_0
.end method

.method public final h(I)V
    .locals 2

    .prologue
    .line 1453583
    iget-object v0, p0, LX/9CC;->c:LX/9F5;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9F5;->a(Ljava/lang/Integer;)V

    .line 1453584
    return-void
.end method

.method public final h_(I)I
    .locals 2

    .prologue
    .line 1453581
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0}, LX/99a;->getCount()I

    move-result v0

    .line 1453582
    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->h_(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/9CC;->b:LX/9E8;

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, LX/1UF;->h_(I)I

    move-result v0

    goto :goto_0
.end method

.method public final m_(I)I
    .locals 2

    .prologue
    .line 1453579
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0}, LX/1UF;->d()I

    move-result v0

    .line 1453580
    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->m_(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/9CC;->b:LX/9E8;

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, LX/1UF;->m_(I)I

    move-result v0

    goto :goto_0
.end method

.method public final n_(I)I
    .locals 2

    .prologue
    .line 1453577
    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0}, LX/1UF;->d()I

    move-result v0

    .line 1453578
    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v0, p1}, LX/1UF;->n_(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/9CC;->b:LX/9E8;

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, LX/1UF;->n_(I)I

    move-result v0

    goto :goto_0
.end method
