.class public LX/AIq;
.super LX/1a1;
.source ""


# instance fields
.field public l:LX/AJ8;

.field private m:LX/AJ2;

.field private n:Lcom/facebook/resources/ui/FbTextView;

.field public o:Lcom/facebook/resources/ui/FbCheckBox;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1660412
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1660413
    const v0, 0x7f0d2c53

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AIq;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1660414
    const v0, 0x7f0d2c55

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object v0, p0, LX/AIq;->o:Lcom/facebook/resources/ui/FbCheckBox;

    .line 1660415
    new-instance v0, LX/AJ5;

    invoke-direct {v0, p0}, LX/AJ5;-><init>(LX/AIq;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1660416
    iget-object v0, p0, LX/AIq;->o:Lcom/facebook/resources/ui/FbCheckBox;

    new-instance v1, LX/AJ6;

    invoke-direct {v1, p0}, LX/AJ6;-><init>(LX/AIq;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1660417
    return-void
.end method


# virtual methods
.method public final a(ZLX/AJ2;LX/AJ8;)V
    .locals 3

    .prologue
    .line 1660406
    iget-object v0, p0, LX/AIq;->o:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1660407
    iput-object p2, p0, LX/AIq;->m:LX/AJ2;

    .line 1660408
    iget-object v1, p0, LX/AIq;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, LX/AIq;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1660409
    iput-object p3, p0, LX/AIq;->l:LX/AJ8;

    .line 1660410
    return-void

    .line 1660411
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 3

    .prologue
    .line 1660387
    iget-object v1, p0, LX/AIq;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, LX/AIq;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1660388
    iget-object v0, p0, LX/AIq;->l:LX/AJ8;

    if-eqz v0, :cond_1

    .line 1660389
    iget-object v0, p0, LX/AIq;->l:LX/AJ8;

    iget-object v1, p0, LX/AIq;->m:LX/AJ2;

    .line 1660390
    sget-object v2, LX/AJ2;->NEWSFEED:LX/AJ2;

    if-ne v1, v2, :cond_3

    .line 1660391
    iget-object v2, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660392
    iput-boolean p1, v2, LX/AJZ;->e:Z

    .line 1660393
    iget-object v1, v2, LX/AJZ;->d:LX/7gT;

    if-eqz p1, :cond_4

    sget-object p0, LX/7gR;->INCLUDE_NEWS_FEED:LX/7gR;

    :goto_1
    invoke-virtual {v1, p0}, LX/7gT;->a(LX/7gR;)V

    .line 1660394
    iget-object v2, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->q:LX/AJt;

    invoke-virtual {v2}, LX/AJt;->d()V

    .line 1660395
    :cond_0
    :goto_2
    iget-object v2, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    const/4 p0, 0x1

    invoke-static {v2, p0}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->b(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;Z)V

    .line 1660396
    iget-object v2, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    invoke-virtual {v2}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->b()V

    .line 1660397
    :cond_1
    return-void

    .line 1660398
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1660399
    :cond_3
    sget-object v2, LX/AJ2;->MY_DAY:LX/AJ2;

    if-ne v1, v2, :cond_0

    .line 1660400
    iget-object v2, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660401
    iput-boolean p1, v2, LX/AJZ;->f:Z

    .line 1660402
    iget-object v1, v2, LX/AJZ;->d:LX/7gT;

    if-eqz p1, :cond_5

    sget-object p0, LX/7gR;->INCLUDE_STORY:LX/7gR;

    :goto_3
    invoke-virtual {v1, p0}, LX/7gT;->a(LX/7gR;)V

    .line 1660403
    iget-object v2, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->q:LX/AJt;

    invoke-virtual {v2}, LX/AJt;->e()V

    goto :goto_2

    .line 1660404
    :cond_4
    sget-object p0, LX/7gR;->EXCLUDE_NEWS_FEED:LX/7gR;

    goto :goto_1

    .line 1660405
    :cond_5
    sget-object p0, LX/7gR;->EXCLUDE_STORY:LX/7gR;

    goto :goto_3
.end method
