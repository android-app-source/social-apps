.class public LX/AE3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field private static c:LX/0Xm;


# instance fields
.field private final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1645962
    const v0, 0x7f02079d

    sput v0, LX/AE3;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1645963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1645964
    const v0, 0x7f0b09a7

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AE3;->b:I

    .line 1645965
    return-void
.end method

.method public static a(LX/0QB;)LX/AE3;
    .locals 4

    .prologue
    .line 1645966
    const-class v1, LX/AE3;

    monitor-enter v1

    .line 1645967
    :try_start_0
    sget-object v0, LX/AE3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1645968
    sput-object v2, LX/AE3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1645969
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645970
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1645971
    new-instance p0, LX/AE3;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/AE3;-><init>(Landroid/content/res/Resources;)V

    .line 1645972
    move-object v0, p0

    .line 1645973
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1645974
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AE3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1645975
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1645976
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/CharSequence;LX/1dc;ILX/1dc;IZLjava/lang/CharSequence;IILandroid/util/SparseArray;Landroid/view/View$OnClickListener;)LX/1Dg;
    .locals 6
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p11    # Landroid/util/SparseArray;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/CharSequence;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;I",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;IZ",
            "Ljava/lang/CharSequence;",
            "II",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1645977
    const/4 v1, 0x0

    const v2, 0x7f0e017b

    invoke-static {p1, v1, v2}, LX/1n8;->a(LX/1De;II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/16 v2, 0x8

    iget v3, p0, LX/AE3;->b:I

    invoke-interface {v1, v2, v3}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p8}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-interface {v1, v0}, LX/1Dh;->b(Landroid/util/SparseArray;)LX/1Dh;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-static {p1, v0}, LX/2yS;->onClick(LX/1De;Landroid/view/View$OnClickListener;)LX/1dQ;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    .line 1645978
    const/high16 v1, -0x80000000

    if-eq p9, v1, :cond_0

    .line 1645979
    const/4 v1, 0x6

    invoke-interface {v2, v1, p9}, LX/1Dh;->s(II)LX/1Dh;

    .line 1645980
    :cond_0
    if-eqz p5, :cond_1

    .line 1645981
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x5

    if-nez p2, :cond_5

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v3, v4, v1}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1645982
    :cond_1
    if-eqz p2, :cond_2

    .line 1645983
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b004e

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    .line 1645984
    const/high16 v3, -0x80000000

    if-ne p4, v3, :cond_6

    .line 1645985
    const v3, 0x7f0a09f5

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    .line 1645986
    :goto_1
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1645987
    :cond_2
    if-eqz p3, :cond_4

    .line 1645988
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    .line 1645989
    if-eqz p2, :cond_3

    .line 1645990
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x5

    const v5, 0x7f0b0065

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    .line 1645991
    :cond_3
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1645992
    :cond_4
    if-nez p7, :cond_7

    const/4 v1, 0x0

    .line 1645993
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x4

    invoke-interface {v3, v4}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x4

    move/from16 v0, p10

    invoke-interface {v3, v4, v0}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    iget v4, p0, LX/AE3;->b:I

    invoke-interface {v2, v3, v4}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 1645994
    :cond_5
    const v1, 0x7f0b0065

    goto/16 :goto_0

    .line 1645995
    :cond_6
    invoke-virtual {v1, p4}, LX/1ne;->m(I)LX/1ne;

    goto :goto_1

    .line 1645996
    :cond_7
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v3, 0x7f0a0160

    invoke-virtual {v1, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x7

    iget v4, p0, LX/AE3;->b:I

    invoke-interface {v1, v3, v4}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b00d0

    invoke-interface {v1, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const/4 v3, 0x4

    invoke-interface {v1, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    goto :goto_2
.end method
