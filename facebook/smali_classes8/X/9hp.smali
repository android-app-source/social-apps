.class public final LX/9hp;
.super LX/9hl;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/9hs;


# direct methods
.method public constructor <init>(LX/9hs;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1527020
    iput-object p1, p0, LX/9hp;->c:LX/9hs;

    iput-object p3, p0, LX/9hp;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object p4, p0, LX/9hp;->b:Ljava/lang/String;

    invoke-direct {p0, p2}, LX/9hl;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 3

    .prologue
    .line 1527021
    invoke-static {p1}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v0

    iget-object v1, p0, LX/9hp;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v2, p0, LX/9hp;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/9hi;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1527022
    iput-object v1, v0, LX/4XB;->aV:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1527023
    move-object v0, v0

    .line 1527024
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .locals 3

    .prologue
    .line 1527025
    invoke-static {p1}, LX/5kN;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)LX/5kN;

    move-result-object v0

    iget-object v1, p0, LX/9hp;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v2, p0, LX/9hp;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/9hi;->c(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v1

    .line 1527026
    iput-object v1, v0, LX/5kN;->X:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    .line 1527027
    move-object v0, v0

    .line 1527028
    invoke-virtual {v0}, LX/5kN;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    return-object v0
.end method
