.class public LX/9Id;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/1nu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/9EG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1463414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463415
    sget-short v0, LX/0wn;->ax:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/9Id;->c:Z

    .line 1463416
    return-void
.end method

.method public static a(LX/0QB;)LX/9Id;
    .locals 5

    .prologue
    .line 1463417
    const-class v1, LX/9Id;

    monitor-enter v1

    .line 1463418
    :try_start_0
    sget-object v0, LX/9Id;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463419
    sput-object v2, LX/9Id;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463420
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463421
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463422
    new-instance p0, LX/9Id;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/9Id;-><init>(LX/0ad;)V

    .line 1463423
    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/9EG;->a(LX/0QB;)LX/9EG;

    move-result-object v4

    check-cast v4, LX/9EG;

    .line 1463424
    iput-object v3, p0, LX/9Id;->a:LX/1nu;

    iput-object v4, p0, LX/9Id;->b:LX/9EG;

    .line 1463425
    move-object v0, p0

    .line 1463426
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463427
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Id;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463428
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463429
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
