.class public LX/9ec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9eb;


# instance fields
.field public A:Landroid/widget/ImageButton;

.field public B:Lcom/facebook/resources/ui/FbTextView;

.field public C:Lcom/facebook/resources/ui/FbTextView;

.field public D:Z

.field public E:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

.field public G:Z

.field public H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

.field public I:Z

.field public J:Landroid/net/Uri;

.field public K:Landroid/net/Uri;

.field public L:Z

.field private M:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;"
        }
    .end annotation
.end field

.field public N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

.field public O:I

.field private P:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Landroid/view/View$OnClickListener;

.field public final b:Landroid/view/View$OnClickListener;

.field public final c:Landroid/view/View$OnClickListener;

.field public final d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

.field public final e:Ljava/lang/String;

.field public final f:Landroid/content/Context;

.field public final g:LX/9cK;

.field public final h:LX/0TD;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/8GT;

.field public final k:LX/9f4;

.field public final l:LX/1HI;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

.field private final o:Landroid/view/View;

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Gc;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/8GN;

.field private final r:LX/9cH;

.field public final s:Landroid/graphics/Rect;

.field public final t:LX/0kL;

.field public u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

.field public v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public w:Z

.field public x:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:LX/9bx;

.field public z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/view/View;LX/9f4;Ljava/lang/String;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/0am;Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;LX/0kL;LX/9cH;LX/0Or;LX/8GN;LX/9cK;LX/0TD;LX/8GT;LX/0Ot;LX/0Ot;LX/1HI;Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/RotatingFrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/editgallery/EditableOverlayContainerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/drawee/fbpipeline/FbDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/9f4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/photos/editgallery/EditGalleryFragmentController;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/photos/creativeediting/RotatingFrameLayout;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            "Landroid/view/View;",
            "Lcom/facebook/photos/editgallery/EditGalleryFragmentController$FileEditingListener;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/editgallery/RotatingPhotoViewController;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;",
            "Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;",
            "LX/0kL;",
            "LX/9cH;",
            "LX/0Or",
            "<",
            "LX/8Gc;",
            ">;",
            "LX/8GN;",
            "LX/9cK;",
            "LX/0TD;",
            "LX/8GT;",
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/1HI;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1520008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1520009
    new-instance v1, LX/9eU;

    invoke-direct {v1, p0}, LX/9eU;-><init>(LX/9ec;)V

    iput-object v1, p0, LX/9ec;->a:Landroid/view/View$OnClickListener;

    .line 1520010
    new-instance v1, LX/9eV;

    invoke-direct {v1, p0}, LX/9eV;-><init>(LX/9ec;)V

    iput-object v1, p0, LX/9ec;->b:Landroid/view/View$OnClickListener;

    .line 1520011
    new-instance v1, LX/9eW;

    invoke-direct {v1, p0}, LX/9eW;-><init>(LX/9ec;)V

    iput-object v1, p0, LX/9ec;->c:Landroid/view/View$OnClickListener;

    .line 1520012
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, LX/9ec;->s:Landroid/graphics/Rect;

    .line 1520013
    const/4 v1, 0x0

    iput v1, p0, LX/9ec;->O:I

    .line 1520014
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/9ec;->P:LX/0am;

    .line 1520015
    iput-object p1, p0, LX/9ec;->J:Landroid/net/Uri;

    .line 1520016
    iput-object p8, p0, LX/9ec;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520017
    iput-object p2, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    .line 1520018
    move-object/from16 v0, p21

    iput-object v0, p0, LX/9ec;->f:Landroid/content/Context;

    .line 1520019
    iput-object p7, p0, LX/9ec;->e:Ljava/lang/String;

    .line 1520020
    iput-object p3, p0, LX/9ec;->F:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    .line 1520021
    iput-object p4, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1520022
    move-object/from16 v0, p16

    iput-object v0, p0, LX/9ec;->h:LX/0TD;

    .line 1520023
    move-object/from16 v0, p19

    iput-object v0, p0, LX/9ec;->i:LX/0Ot;

    .line 1520024
    move-object/from16 v0, p17

    iput-object v0, p0, LX/9ec;->j:LX/8GT;

    .line 1520025
    move-object/from16 v0, p20

    iput-object v0, p0, LX/9ec;->l:LX/1HI;

    .line 1520026
    iput-object p6, p0, LX/9ec;->k:LX/9f4;

    .line 1520027
    move-object/from16 v0, p18

    iput-object v0, p0, LX/9ec;->m:LX/0Ot;

    .line 1520028
    iput-object p5, p0, LX/9ec;->o:Landroid/view/View;

    .line 1520029
    iput-object p11, p0, LX/9ec;->t:LX/0kL;

    .line 1520030
    iget-object v1, p0, LX/9ec;->o:Landroid/view/View;

    const v2, 0x7f0d0393

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, LX/9ec;->A:Landroid/widget/ImageButton;

    .line 1520031
    iget-object v1, p0, LX/9ec;->o:Landroid/view/View;

    const v2, 0x7f0d0394

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, LX/9ec;->B:Lcom/facebook/resources/ui/FbTextView;

    .line 1520032
    iget-object v1, p0, LX/9ec;->o:Landroid/view/View;

    const v2, 0x7f0d0392

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, LX/9ec;->C:Lcom/facebook/resources/ui/FbTextView;

    .line 1520033
    iget-object v1, p0, LX/9ec;->A:Landroid/widget/ImageButton;

    iget-object v2, p0, LX/9ec;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1520034
    move-object/from16 v0, p15

    iput-object v0, p0, LX/9ec;->g:LX/9cK;

    .line 1520035
    move-object/from16 v0, p13

    iput-object v0, p0, LX/9ec;->p:LX/0Or;

    .line 1520036
    move-object/from16 v0, p14

    iput-object v0, p0, LX/9ec;->q:LX/8GN;

    .line 1520037
    new-instance v1, LX/9bx;

    iget-object v2, p0, LX/9ec;->f:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/9bx;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/9ec;->y:LX/9bx;

    .line 1520038
    iget-object v1, p0, LX/9ec;->y:LX/9bx;

    const v2, 0x7f0d00d3

    invoke-virtual {v1, v2}, LX/9bx;->setId(I)V

    .line 1520039
    iget-object v1, p0, LX/9ec;->y:LX/9bx;

    invoke-direct {p0}, LX/9ec;->p()LX/9eX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9bx;->setOnCropChangeListener(LX/9eX;)V

    .line 1520040
    iget-object v1, p0, LX/9ec;->y:LX/9bx;

    sget-object v2, LX/9bv;->FREE_FORM:LX/9bv;

    invoke-virtual {v1, v2}, LX/9bx;->setCropMode(LX/9bv;)V

    .line 1520041
    iput-object p10, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    .line 1520042
    iput-object p9, p0, LX/9ec;->M:LX/0am;

    .line 1520043
    new-instance v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;-><init>()V

    iput-object v1, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    .line 1520044
    iput-object p12, p0, LX/9ec;->r:LX/9cH;

    .line 1520045
    return-void
.end method

.method private a(Landroid/graphics/RectF;ZLandroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 1520085
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520086
    iget-boolean v0, p0, LX/9ec;->I:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    .line 1520087
    :goto_0
    return-void

    .line 1520088
    :cond_0
    iget-object v0, p0, LX/9ec;->k:LX/9f4;

    invoke-virtual {v0}, LX/9f4;->a()V

    .line 1520089
    iget-object v0, p0, LX/9ec;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "crop_task"

    .line 1520090
    new-instance v2, LX/9eZ;

    invoke-direct {v2, p0, p1}, LX/9eZ;-><init>(LX/9ec;Landroid/graphics/RectF;)V

    move-object v2, v2

    .line 1520091
    new-instance v3, LX/9eY;

    invoke-direct {v3, p0, p1, p2, p3}, LX/9eY;-><init>(LX/9ec;Landroid/graphics/RectF;ZLandroid/graphics/RectF;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static b(LX/9ec;Landroid/graphics/RectF;Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1520077
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1520078
    :cond_0
    const/4 v0, 0x0

    .line 1520079
    :goto_0
    return v0

    .line 1520080
    :cond_1
    iget v0, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p2}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 1520081
    iget v0, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 1520082
    iget v0, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p2}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 1520083
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 1520084
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1520065
    iget-object v0, p0, LX/9ec;->E:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1520066
    iget-object v0, p0, LX/9ec;->y:LX/9bx;

    if-eqz v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1520067
    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/5iB;->f(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1520068
    iget-object v0, p0, LX/9ec;->P:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1520069
    iget-object v0, p0, LX/9ec;->E:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9ec;->s:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1520070
    iget-object v0, p0, LX/9ec;->r:LX/9cH;

    iget-object v1, p0, LX/9ec;->s:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/9cH;->a(Landroid/graphics/Rect;)Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9ec;->P:LX/0am;

    .line 1520071
    :cond_0
    iget-object v1, p0, LX/9ec;->y:LX/9bx;

    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v2

    iget-object v0, p0, LX/9ec;->P:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1520072
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iput-object p0, v1, LX/9bx;->I:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1520073
    invoke-static {v1, v2}, LX/9bx;->setAdjustableOverlayItems(LX/9bx;Ljava/util/List;)V

    .line 1520074
    return-void

    :cond_1
    move v0, v2

    .line 1520075
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1520076
    goto :goto_1
.end method

.method private p()LX/9eX;
    .locals 1

    .prologue
    .line 1520064
    new-instance v0, LX/9eX;

    invoke-direct {v0, p0}, LX/9eX;-><init>(LX/9ec;)V

    return-object v0
.end method

.method public static s(LX/9ec;)V
    .locals 3

    .prologue
    .line 1520056
    iget-object v0, p0, LX/9ec;->y:LX/9bx;

    .line 1520057
    iget-object v1, v0, LX/9bx;->G:LX/9bv;

    move-object v0, v1

    .line 1520058
    sget-object v1, LX/9bv;->SQUARE:LX/9bv;

    if-eq v0, v1, :cond_1

    const/4 v1, 0x0

    .line 1520059
    iget-object v0, p0, LX/9ec;->x:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1520060
    if-eqz v0, :cond_1

    .line 1520061
    iget-object v0, p0, LX/9ec;->C:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1520062
    :goto_1
    return-void

    .line 1520063
    :cond_1
    iget-object v0, p0, LX/9ec;->C:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(LX/9ec;)V
    .locals 3

    .prologue
    .line 1520046
    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520047
    :goto_0
    return-void

    .line 1520048
    :cond_0
    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    .line 1520049
    iget-object v1, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getFinalRotation()I

    .line 1520050
    iget-object v1, p0, LX/9ec;->x:Landroid/graphics/RectF;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9ec;->x:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    .line 1520051
    :cond_1
    iget-object v1, p0, LX/9ec;->x:Landroid/graphics/RectF;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/9ec;->x:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 1520052
    :cond_2
    iget-object v1, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520053
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    move-object v1, v2

    .line 1520054
    iget-object v2, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/8GN;->b(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 1520055
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    iput-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1520007
    iget-object v0, p0, LX/9ec;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08241b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1520005
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, LX/9ec;->E:Landroid/graphics/RectF;

    .line 1520006
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1519979
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1519980
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1519981
    iget-object v0, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-object v1, p0, LX/9ec;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1519982
    iget v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    move v1, v2

    .line 1519983
    iget-object v2, p0, LX/9ec;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1519984
    iget v5, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    move v2, v5

    .line 1519985
    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a(II)V

    .line 1519986
    iput-object p1, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1519987
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1519988
    iput-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519989
    iput-boolean v4, p0, LX/9ec;->w:Z

    .line 1519990
    iput-boolean v3, p0, LX/9ec;->D:Z

    .line 1519991
    iput-boolean v3, p0, LX/9ec;->G:Z

    .line 1519992
    iput-boolean v4, p0, LX/9ec;->L:Z

    .line 1519993
    iget-object v0, p0, LX/9ec;->F:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1519994
    iget-object v0, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->b()V

    .line 1519995
    iget-object v0, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setVisibility(I)V

    .line 1519996
    iget-object v0, p0, LX/9ec;->A:Landroid/widget/ImageButton;

    const v1, 0x7f02174b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1519997
    iget-object v0, p0, LX/9ec;->A:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/9ec;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1519998
    iget-object v0, p0, LX/9ec;->B:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->f:Landroid/content/Context;

    const v2, 0x7f082417

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1519999
    iget-object v0, p0, LX/9ec;->B:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1520000
    iget-object v0, p0, LX/9ec;->B:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->f:Landroid/content/Context;

    const v2, 0x7f082420

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1520001
    iget-object v0, p0, LX/9ec;->C:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->f:Landroid/content/Context;

    const v2, 0x7f08241a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1520002
    iget-object v0, p0, LX/9ec;->C:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1520003
    iget-object v0, p0, LX/9ec;->C:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ec;->f:Landroid/content/Context;

    const v2, 0x7f082421

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1520004
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;)V
    .locals 1

    .prologue
    .line 1519977
    iget v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->d:I

    .line 1519978
    return-void
.end method

.method public final a(Z)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1520092
    iget-object v2, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    iget-object v0, p0, LX/9ec;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520093
    iget v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    move v0, v3

    .line 1520094
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, v2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->j:F

    .line 1520095
    iget-object v0, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    iget-object v2, p0, LX/9ec;->x:Landroid/graphics/RectF;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/9ec;->x:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    :goto_1
    iput v1, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->k:F

    .line 1520096
    iget-object v0, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    iget v1, p0, LX/9ec;->O:I

    iput v1, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->i:I

    .line 1520097
    iget-object v0, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    iput-boolean p1, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->l:Z

    .line 1520098
    iget-object v0, p0, LX/9ec;->M:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520099
    iget-object v0, p0, LX/9ec;->M:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c7;

    iget-object v1, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    .line 1520100
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v7, LX/9c4;->COMPOSER_CROP:LX/9c4;

    invoke-virtual {v7}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "composer"

    .line 1520101
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1520102
    move-object v6, v6

    .line 1520103
    sget-object v7, LX/9c5;->CROP_EDGE_CONTROL:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->f:Z

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/9c5;->CROP_PAN_CONTROL:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->g:Z

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/9c5;->CROP_ADJUSTMENT_COUNT:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->h:I

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/9c5;->ROTATION_COUNT:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->i:I

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/9c5;->CROP_ORIGINAL_ASPECT_RATIO:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->j:F

    float-to-double v8, v8

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/9c5;->CROP_FINAL_ASPECT_RATIO:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->k:F

    float-to-double v8, v8

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/9c5;->ACCEPTED:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->l:Z

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/9c5;->NUMBER_OF_TAGS_REMOVED:LX/9c5;

    invoke-virtual {v7}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->m:I

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 1520104
    invoke-static {v0, v6}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1520105
    :cond_0
    return-void

    .line 1520106
    :cond_1
    iget-object v0, p0, LX/9ec;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520107
    iget v3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    move v0, v3

    .line 1520108
    int-to-float v0, v0

    iget-object v3, p0, LX/9ec;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520109
    iget v4, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    move v3, v4

    .line 1520110
    int-to-float v3, v3

    div-float/2addr v0, v3

    goto/16 :goto_0

    .line 1520111
    :cond_2
    iget-object v1, p0, LX/9ec;->x:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, LX/9ec;->x:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    goto/16 :goto_1

    :cond_3
    iget-object v1, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    iget v1, v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->j:F

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1519824
    iget-boolean v0, p0, LX/9ec;->L:Z

    if-nez v0, :cond_0

    .line 1519825
    :goto_0
    return-void

    .line 1519826
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9ec;->L:Z

    .line 1519827
    iget-object v0, p0, LX/9ec;->y:LX/9bx;

    invoke-virtual {v0}, LX/9bx;->a()V

    .line 1519828
    iget-object v0, p0, LX/9ec;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1519829
    iget-object v0, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1519830
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1519831
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1519832
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1519833
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1519975
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9ec;->w:Z

    .line 1519976
    return-void
.end method

.method public final h()V
    .locals 15

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1519834
    iget-object v0, p0, LX/9ec;->A:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1519835
    iget-object v0, p0, LX/9ec;->B:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1519836
    iget-object v0, p0, LX/9ec;->C:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1519837
    const/4 v9, 0x0

    .line 1519838
    iget-object v0, p0, LX/9ec;->E:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/9ec;->L:Z

    if-nez v0, :cond_3

    .line 1519839
    :cond_0
    :goto_0
    const/4 v11, 0x0

    .line 1519840
    iget-object v5, p0, LX/9ec;->E:Landroid/graphics/RectF;

    if-eqz v5, :cond_1

    iget-boolean v5, p0, LX/9ec;->L:Z

    if-nez v5, :cond_b

    .line 1519841
    :cond_1
    :goto_1
    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/5iB;->f(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1519842
    invoke-direct {p0}, LX/9ec;->o()V

    .line 1519843
    :cond_2
    iget-object v1, p0, LX/9ec;->y:LX/9bx;

    iget-object v0, p0, LX/9ec;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Gc;

    const/4 v6, 0x0

    .line 1519844
    invoke-virtual {v1}, LX/9bx;->bringToFront()V

    .line 1519845
    if-eqz v0, :cond_e

    .line 1519846
    invoke-virtual {v0}, LX/8Gc;->a()V

    .line 1519847
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, LX/9bx;->setAlpha(F)V

    .line 1519848
    invoke-virtual {v1, v6}, LX/9bx;->setVisibility(I)V

    .line 1519849
    const/high16 v5, 0x41c80000    # 25.0f

    .line 1519850
    iput v5, v0, LX/8Gc;->f:F

    .line 1519851
    const/4 v5, 0x1

    invoke-virtual {v0, v1, v5}, LX/8Gc;->a(Landroid/view/View;I)V

    .line 1519852
    :goto_2
    iget-object v0, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setAlpha(F)V

    .line 1519853
    iget-object v0, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setVisibility(I)V

    .line 1519854
    iget-object v0, p0, LX/9ec;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Gc;

    iget-object v1, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v0, v1, v4}, LX/8Gc;->a(Landroid/view/View;I)V

    .line 1519855
    iget-object v0, p0, LX/9ec;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 1519856
    iget-object v0, p0, LX/9ec;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1519857
    iget-object v0, p0, LX/9ec;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Gc;

    iget-object v1, p0, LX/9ec;->o:Landroid/view/View;

    invoke-virtual {v0, v1, v4}, LX/8Gc;->a(Landroid/view/View;I)V

    .line 1519858
    return-void

    .line 1519859
    :cond_3
    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    .line 1519860
    if-eqz v1, :cond_8

    .line 1519861
    iget-object v0, p0, LX/9ec;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v5, p0, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v0, v5}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v0

    .line 1519862
    invoke-static {v0}, LX/8Ga;->a(I)I

    move-result v0

    invoke-static {v1, v0}, LX/8Ga;->a(Landroid/graphics/RectF;I)Landroid/graphics/RectF;

    move-result-object v0

    .line 1519863
    :goto_3
    new-instance v1, Landroid/graphics/RectF;

    iget-object v5, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-direct {v1, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1519864
    new-instance v5, Landroid/graphics/RectF;

    iget-object v6, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-direct {v5, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1519865
    iget-object v6, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p0, v5, v6}, LX/9ec;->b(LX/9ec;Landroid/graphics/RectF;Landroid/view/View;)Z

    .line 1519866
    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getPaddingTop()I

    move-result v7

    iget-object v8, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getPaddingBottom()I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v1, Landroid/graphics/RectF;->bottom:F

    .line 1519867
    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v1, Landroid/graphics/RectF;->right:F

    .line 1519868
    const/4 v7, 0x0

    .line 1519869
    if-eqz v0, :cond_4

    iget-boolean v6, p0, LX/9ec;->w:Z

    if-eqz v6, :cond_4

    iget-object v6, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_4

    iget-object v6, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpl-float v6, v6, v7

    if-nez v6, :cond_9

    .line 1519870
    :cond_4
    :goto_4
    iget-object v6, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p0, v0, v6}, LX/9ec;->b(LX/9ec;Landroid/graphics/RectF;Landroid/view/View;)Z

    move-result v6

    iput-boolean v6, p0, LX/9ec;->D:Z

    .line 1519871
    iget-object v6, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    const v7, 0x7f0d00d3

    invoke-virtual {v6, v7}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_5

    .line 1519872
    iget-object v6, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-object v7, p0, LX/9ec;->y:LX/9bx;

    invoke-virtual {v6, v7}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->addView(Landroid/view/View;)V

    .line 1519873
    :cond_5
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpl-float v6, v6, v9

    if-lez v6, :cond_7

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpl-float v6, v6, v9

    if-lez v6, :cond_7

    .line 1519874
    iget-object v6, p0, LX/9ec;->y:LX/9bx;

    .line 1519875
    invoke-virtual {v6}, LX/9bx;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    instance-of v7, v7, Landroid/widget/FrameLayout$LayoutParams;

    const-string v8, "Expected the container to be a FrameLayout."

    invoke-static {v7, v8}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1519876
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    iput-object v7, v6, LX/9bx;->r:Landroid/graphics/RectF;

    .line 1519877
    new-instance v7, Landroid/graphics/RectF;

    iget v8, v1, Landroid/graphics/RectF;->left:F

    iget v9, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v9, v1, Landroid/graphics/RectF;->top:F

    iget v10, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, v5, Landroid/graphics/RectF;->right:F

    iget v11, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    iget v11, v1, Landroid/graphics/RectF;->left:F

    iget v12, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v10, v11

    iget v11, v5, Landroid/graphics/RectF;->bottom:F

    iget v12, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v12

    iget v12, v1, Landroid/graphics/RectF;->top:F

    iget v13, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    add-float/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1519878
    if-nez v0, :cond_a

    .line 1519879
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v7}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v8, v6, LX/9bx;->t:Landroid/graphics/RectF;

    .line 1519880
    :cond_6
    :goto_5
    iget-object v8, v6, LX/9bx;->t:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iput v8, v6, LX/9bx;->m:F

    .line 1519881
    iget-object v8, v6, LX/9bx;->t:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iput v8, v6, LX/9bx;->n:F

    .line 1519882
    iget-object v8, v6, LX/9bx;->t:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iput v8, v6, LX/9bx;->o:F

    .line 1519883
    iget-object v8, v6, LX/9bx;->t:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    iput v8, v6, LX/9bx;->p:F

    .line 1519884
    iput-object v7, v6, LX/9bx;->r:Landroid/graphics/RectF;

    .line 1519885
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v7, v6, LX/9bx;->s:Landroid/graphics/RectF;

    .line 1519886
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v8, v6, LX/9bx;->s:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    float-to-int v8, v8

    iget-object v9, v6, LX/9bx;->s:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    float-to-int v9, v9

    invoke-direct {v7, v8, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1519887
    iget-object v8, v6, LX/9bx;->s:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    float-to-int v8, v8

    iput v8, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1519888
    iget-object v8, v6, LX/9bx;->s:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    iput v8, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1519889
    invoke-virtual {v6, v7}, LX/9bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1519890
    :cond_7
    invoke-static {p0}, LX/9ec;->s(LX/9ec;)V

    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    goto/16 :goto_3

    .line 1519891
    :cond_9
    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    mul-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/RectF;->left:F

    .line 1519892
    iget v6, v0, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    mul-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/RectF;->top:F

    .line 1519893
    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    mul-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/RectF;->right:F

    .line 1519894
    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    mul-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_4

    .line 1519895
    :cond_a
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 1519896
    iget-object v9, v6, LX/9bx;->r:Landroid/graphics/RectF;

    sget-object v10, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v8, v9, v7, v10}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1519897
    iget-object v9, v6, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v8, v9, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1519898
    iget-object v8, v6, LX/9bx;->D:LX/9eX;

    if-eqz v8, :cond_6

    .line 1519899
    iget-object v8, v6, LX/9bx;->D:LX/9eX;

    new-instance v9, Landroid/graphics/RectF;

    iget-object v10, v6, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-direct {v9, v10}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, LX/9eX;->a(Landroid/graphics/RectF;Z)V

    goto/16 :goto_5

    .line 1519900
    :cond_b
    iget-object v5, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v5}, LX/5iB;->c(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 1519901
    iget-object v5, p0, LX/9ec;->g:LX/9cK;

    invoke-virtual {v5}, LX/9cK;->d()V

    goto/16 :goto_1

    .line 1519902
    :cond_c
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v6, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    float-to-int v7, v7

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1519903
    iget-object v6, p0, LX/9ec;->E:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    iget-object v7, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1519904
    iget-object v6, p0, LX/9ec;->E:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    iget-object v7, p0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1519905
    iget-object v6, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v6, v5}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1519906
    iget-object v5, p0, LX/9ec;->g:LX/9cK;

    iget-object v6, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v7, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    float-to-int v7, v7

    iget-object v8, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-int v8, v8

    iget-object v9, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    .line 1519907
    iget-boolean v10, v9, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->g:Z

    move v9, v10

    .line 1519908
    if-eqz v9, :cond_d

    iget-object v9, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v9}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getFinalRotation()I

    move-result v9

    :goto_6
    iget-object v10, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const/4 v12, 0x3

    new-array v12, v12, [LX/9cJ;

    sget-object v13, LX/9cJ;->STICKERS:LX/9cJ;

    aput-object v13, v12, v11

    const/4 v13, 0x1

    sget-object v14, LX/9cJ;->TEXTS:LX/9cJ;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, LX/9cJ;->DOODLE:LX/9cJ;

    aput-object v14, v12, v13

    invoke-virtual/range {v5 .. v12}, LX/9cK;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;IIILandroid/view/View;Z[LX/9cJ;)V

    .line 1519909
    iget-object v5, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    iget-object v6, p0, LX/9ec;->g:LX/9cK;

    .line 1519910
    iput-object v6, v5, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    .line 1519911
    goto/16 :goto_1

    .line 1519912
    :cond_d
    iget-object v9, p0, LX/9ec;->m:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/8GX;

    iget-object v10, p0, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v9, v10}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v9

    goto :goto_6

    .line 1519913
    :cond_e
    invoke-virtual {v1, v6}, LX/9bx;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1519914
    iget-object v0, p0, LX/9ec;->y:LX/9bx;

    invoke-virtual {v0}, LX/9bx;->a()V

    .line 1519915
    iget-object v0, p0, LX/9ec;->H:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setVisibility(I)V

    .line 1519916
    iget-object v0, p0, LX/9ec;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1519917
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1519918
    sget-object v0, LX/5Rr;->CROP:LX/5Rr;

    return-object v0
.end method

.method public final k()LX/9ek;
    .locals 1

    .prologue
    .line 1519919
    sget-object v0, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1519920
    iget-boolean v0, p0, LX/9ec;->G:Z

    return v0
.end method

.method public final m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1519921
    iget-object v0, p0, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getFinalRotation()I

    move-result v4

    .line 1519922
    if-eqz v4, :cond_0

    const/16 v0, 0x168

    if-eq v4, v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 1519923
    :goto_0
    iget-object v0, p0, LX/9ec;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v3, p0, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v0, v3}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v5

    .line 1519924
    if-nez v1, :cond_1

    iget-boolean v0, p0, LX/9ec;->I:Z

    if-nez v0, :cond_1

    if-nez v5, :cond_1

    .line 1519925
    iget-object v0, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1519926
    :goto_1
    return-object v0

    :cond_0
    move v1, v2

    .line 1519927
    goto :goto_0

    .line 1519928
    :cond_1
    iget-object v0, p0, LX/9ec;->x:Landroid/graphics/RectF;

    if-eqz v0, :cond_3

    new-instance v0, Landroid/graphics/RectF;

    iget-object v3, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    div-float/2addr v3, v6

    iget-object v6, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    div-float/2addr v6, v7

    iget-object v7, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    div-float/2addr v7, v8

    iget-object v8, p0, LX/9ec;->x:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v9, p0, LX/9ec;->E:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    div-float/2addr v8, v9

    invoke-direct {v0, v3, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v3, v0

    .line 1519929
    :goto_2
    if-nez v1, :cond_4

    if-nez v5, :cond_4

    .line 1519930
    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v0

    .line 1519931
    iget-object v1, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-static {v3}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    iput-object v1, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519932
    iget-object v1, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v4, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519933
    iput-object v4, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519934
    if-eqz v3, :cond_2

    .line 1519935
    invoke-direct {p0, v3, v2, v0}, LX/9ec;->a(Landroid/graphics/RectF;ZLandroid/graphics/RectF;)V

    .line 1519936
    :cond_2
    iget-object v0, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    goto :goto_1

    .line 1519937
    :cond_3
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_2

    .line 1519938
    :cond_4
    invoke-static {v5}, LX/8Ga;->a(I)I

    move-result v0

    rsub-int/lit8 v2, v0, 0x4

    .line 1519939
    add-int v0, v5, v4

    rem-int/lit16 v6, v0, 0x168

    .line 1519940
    if-eq v6, v5, :cond_8

    .line 1519941
    iget-object v0, p0, LX/9ec;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v7, p0, LX/9ec;->J:Landroid/net/Uri;

    invoke-virtual {v0, v7, v6}, LX/8GX;->a(Landroid/net/Uri;I)V

    .line 1519942
    iget-object v0, p0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    iget v6, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->m:I

    iget-object v7, p0, LX/9ec;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1519943
    add-int v8, v5, v4

    rem-int/lit16 v8, v8, 0x168

    .line 1519944
    iget-object v9, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    invoke-interface {v9, v8}, LX/9el;->a(I)I

    move-result v9

    .line 1519945
    iget-object v10, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1519946
    iget-object v11, v10, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->y:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    move-object v10, v11

    .line 1519947
    iput v8, v10, LX/9dl;->d:I

    .line 1519948
    iget-object v8, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1519949
    iget-boolean v10, v8, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->k:Z

    if-nez v10, :cond_5

    .line 1519950
    iput v5, v8, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->h:I

    .line 1519951
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->k:Z

    .line 1519952
    :cond_5
    iget-object v8, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    const/4 v10, 0x1

    .line 1519953
    iput-boolean v10, v8, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    .line 1519954
    const/16 v8, 0x5a

    if-eq v4, v8, :cond_6

    const/16 v8, 0x10e

    if-ne v4, v8, :cond_7

    .line 1519955
    :cond_6
    iget v8, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    .line 1519956
    iget v10, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    iput v10, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->W:I

    .line 1519957
    iput v8, v7, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->V:I

    .line 1519958
    :cond_7
    invoke-static {v7}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1519959
    move v4, v9

    .line 1519960
    add-int/2addr v4, v6

    iput v4, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->m:I

    .line 1519961
    :cond_8
    if-nez v3, :cond_a

    .line 1519962
    iget-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setIsRotated(Z)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    iput-object v0, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519963
    iget-object v0, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519964
    iput-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519965
    if-eqz v1, :cond_9

    .line 1519966
    invoke-static {p0}, LX/9ec;->x(LX/9ec;)V

    .line 1519967
    :cond_9
    iget-object v0, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    goto/16 :goto_1

    .line 1519968
    :cond_a
    invoke-static {v3, v2}, LX/8Ga;->a(Landroid/graphics/RectF;I)Landroid/graphics/RectF;

    move-result-object v0

    .line 1519969
    iget-object v2, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    invoke-static {v2}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v2

    .line 1519970
    iget-object v3, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-static {v0}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setIsRotated(Z)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    iput-object v3, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519971
    iget-object v3, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v4, p0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519972
    iput-object v4, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1519973
    invoke-direct {p0, v0, v1, v2}, LX/9ec;->a(Landroid/graphics/RectF;ZLandroid/graphics/RectF;)V

    .line 1519974
    iget-object v0, p0, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    goto/16 :goto_1
.end method
