.class public LX/9D5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9D5;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1454736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1454737
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1454738
    iput-object v0, p0, LX/9D5;->a:LX/0Ot;

    .line 1454739
    return-void
.end method

.method public static a(LX/0QB;)LX/9D5;
    .locals 4

    .prologue
    .line 1454740
    sget-object v0, LX/9D5;->b:LX/9D5;

    if-nez v0, :cond_1

    .line 1454741
    const-class v1, LX/9D5;

    monitor-enter v1

    .line 1454742
    :try_start_0
    sget-object v0, LX/9D5;->b:LX/9D5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1454743
    if-eqz v2, :cond_0

    .line 1454744
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1454745
    new-instance v3, LX/9D5;

    invoke-direct {v3}, LX/9D5;-><init>()V

    .line 1454746
    const/16 p0, 0xaa5

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1454747
    iput-object p0, v3, LX/9D5;->a:LX/0Ot;

    .line 1454748
    move-object v0, v3

    .line 1454749
    sput-object v0, LX/9D5;->b:LX/9D5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1454750
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1454751
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1454752
    :cond_1
    sget-object v0, LX/9D5;->b:LX/9D5;

    return-object v0

    .line 1454753
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1454754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
