.class public LX/A6u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:[[D

.field public final d:I

.field private final e:I

.field public f:Ljava/lang/String;

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/508",
            "<",
            "LX/A6v;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;[[DI)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;[[DI)V"
        }
    .end annotation

    .prologue
    .line 1625091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625092
    const-string v0, ""

    iput-object v0, p0, LX/A6u;->f:Ljava/lang/String;

    .line 1625093
    iput-object p1, p0, LX/A6u;->a:Ljava/util/Map;

    .line 1625094
    iput-object p2, p0, LX/A6u;->b:Ljava/util/Map;

    .line 1625095
    iget-object v0, p0, LX/A6u;->b:Ljava/util/Map;

    const-string v1, "</s>"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/A6u;->e:I

    .line 1625096
    iput-object p3, p0, LX/A6u;->c:[[D

    .line 1625097
    iput p4, p0, LX/A6u;->d:I

    .line 1625098
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/A6u;->g:Ljava/util/ArrayList;

    .line 1625099
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 1625100
    invoke-static {v0}, LX/508;->a(Ljava/util/Comparator;)LX/504;

    move-result-object v0

    invoke-virtual {v0}, LX/504;->a()LX/508;

    move-result-object v0

    .line 1625101
    new-instance v1, LX/A6v;

    const-string v2, ""

    const-string v3, "</s>"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/A6v;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;)V

    invoke-virtual {v0, v1}, LX/508;->add(Ljava/lang/Object;)Z

    .line 1625102
    iget-object v1, p0, LX/A6u;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1625103
    return-void
.end method

.method public static a(LX/A6u;II)LX/508;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/508",
            "<",
            "LX/A6v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1625104
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 1625105
    invoke-static {v0}, LX/508;->a(Ljava/util/Comparator;)LX/504;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/504;->a(I)LX/504;

    move-result-object v0

    invoke-virtual {v0}, LX/504;->a()LX/508;

    move-result-object v2

    .line 1625106
    iget-object v0, p0, LX/A6u;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/508;

    invoke-virtual {v0}, LX/508;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A6v;

    .line 1625107
    iget-object v1, p0, LX/A6u;->b:Ljava/util/Map;

    .line 1625108
    iget-object v4, v0, LX/A6v;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1625109
    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1625110
    iget-object v4, p0, LX/A6u;->c:[[D

    aget-object v1, v4, v1

    iget v4, p0, LX/A6u;->e:I

    aget-wide v4, v1, v4

    .line 1625111
    new-instance v1, LX/A6v;

    .line 1625112
    iget-object v6, v0, LX/A6v;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1625113
    const-string v7, "</s>"

    invoke-virtual {v0}, LX/A6v;->a()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    add-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {v1, v6, v7, v0}, LX/A6v;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;)V

    .line 1625114
    invoke-virtual {v2, v1}, LX/508;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1625115
    :cond_0
    return-object v2
.end method

.method public static a(LX/A6u;LX/A6v;Ljava/lang/String;Ljava/util/Map;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/A6v;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/A6v;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1625116
    iget-object v0, p0, LX/A6u;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1625117
    iget-object v1, p1, LX/A6v;->b:Ljava/lang/String;

    move-object v2, v1

    .line 1625118
    iget-object v1, p1, LX/A6v;->a:Ljava/lang/String;

    move-object v3, v1

    .line 1625119
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1625120
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1625121
    const/4 v5, 0x0

    .line 1625122
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    aget-char v6, v6, v7

    .line 1625123
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    aget-char v7, v7, v5

    .line 1625124
    const-string v8, "</s>"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {v7}, LX/A6q;->a(C)Z

    move-result v8

    if-nez v8, :cond_7

    .line 1625125
    :cond_1
    const/16 v8, 0x94d

    if-ne v6, v8, :cond_b

    .line 1625126
    const/4 v8, 0x1

    .line 1625127
    :goto_1
    move v8, v8

    .line 1625128
    if-nez v8, :cond_2

    invoke-static {v6}, LX/A6q;->b(C)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    invoke-static {v7}, LX/A6q;->b(C)Z

    move-result v8

    if-nez v8, :cond_7

    :cond_3
    invoke-static {v6}, LX/A6q;->a(C)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {v7}, LX/A6q;->a(C)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {v7}, LX/A6q;->b(C)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1625129
    :cond_4
    const/16 v8, 0x900

    if-lt v6, v8, :cond_5

    const/16 v8, 0x914

    if-le v6, v8, :cond_6

    :cond_5
    const/16 v8, 0x929

    if-eq v6, v8, :cond_6

    const/16 v8, 0x931

    if-eq v6, v8, :cond_6

    const/16 v8, 0x934

    if-eq v6, v8, :cond_6

    const/16 v8, 0x93a

    if-lt v6, v8, :cond_c

    const/16 v8, 0x97f

    if-gt v6, v8, :cond_c

    .line 1625130
    :cond_6
    const/4 v8, 0x1

    .line 1625131
    :goto_2
    move v6, v8

    .line 1625132
    if-eqz v6, :cond_8

    .line 1625133
    const/16 v6, 0x93c

    if-ne v7, v6, :cond_d

    .line 1625134
    const/4 v6, 0x1

    .line 1625135
    :goto_3
    move v6, v6

    .line 1625136
    if-eqz v6, :cond_8

    .line 1625137
    :cond_7
    const/4 v5, 0x1

    .line 1625138
    :cond_8
    move v5, v5

    .line 1625139
    if-nez v5, :cond_0

    .line 1625140
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 1625141
    iget-object v0, p0, LX/A6u;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1625142
    iget-object v0, p0, LX/A6u;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1625143
    iget-object v8, p0, LX/A6u;->c:[[D

    aget-object v0, v8, v0

    aget-wide v8, v0, v5

    .line 1625144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1625145
    invoke-virtual {p1}, LX/A6v;->a()Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    add-double/2addr v6, v10

    add-double/2addr v6, v8

    const-wide/high16 v8, -0x4020000000000000L    # -0.5

    add-double/2addr v6, v8

    .line 1625146
    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1625147
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A6v;

    invoke-virtual {v0, v6, v7}, LX/A6v;->a(D)V

    goto/16 :goto_0

    .line 1625148
    :cond_9
    new-instance v5, LX/A6v;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v5, v0, v1, v6}, LX/A6v;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;)V

    .line 1625149
    invoke-interface {p3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1625150
    :cond_a
    return-void

    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_c
    const/4 v8, 0x0

    goto :goto_2

    :cond_d
    const/4 v6, 0x0

    goto :goto_3
.end method
