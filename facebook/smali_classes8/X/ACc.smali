.class public final enum LX/ACc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ACc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ACc;

.field public static final enum FETCH_TYPEAHEAD_RESULTS:LX/ACc;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1642876
    new-instance v0, LX/ACc;

    const-string v1, "FETCH_TYPEAHEAD_RESULTS"

    invoke-direct {v0, v1, v2}, LX/ACc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ACc;->FETCH_TYPEAHEAD_RESULTS:LX/ACc;

    .line 1642877
    const/4 v0, 0x1

    new-array v0, v0, [LX/ACc;

    sget-object v1, LX/ACc;->FETCH_TYPEAHEAD_RESULTS:LX/ACc;

    aput-object v1, v0, v2

    sput-object v0, LX/ACc;->$VALUES:[LX/ACc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1642878
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ACc;
    .locals 1

    .prologue
    .line 1642879
    const-class v0, LX/ACc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ACc;

    return-object v0
.end method

.method public static values()[LX/ACc;
    .locals 1

    .prologue
    .line 1642880
    sget-object v0, LX/ACc;->$VALUES:[LX/ACc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ACc;

    return-object v0
.end method
