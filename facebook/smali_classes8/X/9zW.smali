.class public final LX/9zW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1601819
    const/4 v9, 0x0

    .line 1601820
    const/4 v8, 0x0

    .line 1601821
    const/4 v7, 0x0

    .line 1601822
    const/4 v6, 0x0

    .line 1601823
    const/4 v5, 0x0

    .line 1601824
    const/4 v4, 0x0

    .line 1601825
    const/4 v3, 0x0

    .line 1601826
    const/4 v2, 0x0

    .line 1601827
    const/4 v1, 0x0

    .line 1601828
    const/4 v0, 0x0

    .line 1601829
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1601830
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1601831
    const/4 v0, 0x0

    .line 1601832
    :goto_0
    return v0

    .line 1601833
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1601834
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1601835
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1601836
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1601837
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1601838
    const-string v11, "corrected_query"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1601839
    invoke-static {p0, p1}, LX/9zU;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1601840
    :cond_2
    const-string v11, "count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1601841
    const/4 v1, 0x1

    .line 1601842
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 1601843
    :cond_3
    const-string v11, "disable_auto_load"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1601844
    const/4 v0, 0x1

    .line 1601845
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 1601846
    :cond_4
    const-string v11, "edges"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1601847
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1601848
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_5

    .line 1601849
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_5

    .line 1601850
    invoke-static {p0, p1}, LX/9zV;->b(LX/15w;LX/186;)I

    move-result v10

    .line 1601851
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1601852
    :cond_5
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1601853
    goto :goto_1

    .line 1601854
    :cond_6
    const-string v11, "page_info"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1601855
    invoke-static {p0, p1}, LX/80q;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1601856
    :cond_7
    const-string v11, "session_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1601857
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1601858
    :cond_8
    const-string v11, "speller_confidence"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1601859
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1601860
    :cond_9
    const-string v11, "vertical_to_log"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1601861
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 1601862
    :cond_a
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1601863
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1601864
    if-eqz v1, :cond_b

    .line 1601865
    const/4 v1, 0x1

    const/4 v9, 0x0

    invoke-virtual {p1, v1, v8, v9}, LX/186;->a(III)V

    .line 1601866
    :cond_b
    if-eqz v0, :cond_c

    .line 1601867
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1601868
    :cond_c
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1601869
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1601870
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1601871
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1601872
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1601873
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 1601874
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1601875
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1601876
    if-eqz v0, :cond_0

    .line 1601877
    const-string v1, "corrected_query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601878
    invoke-static {p0, v0, p2, p3}, LX/9zU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1601879
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1601880
    if-eqz v0, :cond_1

    .line 1601881
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601882
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1601883
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1601884
    if-eqz v0, :cond_2

    .line 1601885
    const-string v1, "disable_auto_load"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601886
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1601887
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601888
    if-eqz v0, :cond_4

    .line 1601889
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601890
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1601891
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1601892
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/9zV;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1601893
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1601894
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1601895
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601896
    if-eqz v0, :cond_5

    .line 1601897
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601898
    invoke-static {p0, v0, p2}, LX/80q;->a(LX/15i;ILX/0nX;)V

    .line 1601899
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1601900
    if-eqz v0, :cond_6

    .line 1601901
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601902
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601903
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1601904
    if-eqz v0, :cond_7

    .line 1601905
    const-string v0, "speller_confidence"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601906
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601907
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1601908
    if-eqz v0, :cond_8

    .line 1601909
    const-string v1, "vertical_to_log"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601910
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601911
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1601912
    return-void
.end method
