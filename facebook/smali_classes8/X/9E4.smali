.class public final LX/9E4;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:LX/5Gs;

.field public final synthetic c:LX/0TF;

.field public final synthetic d:LX/9E6;


# direct methods
.method public constructor <init>(LX/9E6;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;LX/0TF;)V
    .locals 0

    .prologue
    .line 1456481
    iput-object p1, p0, LX/9E4;->d:LX/9E6;

    iput-object p2, p0, LX/9E4;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/9E4;->b:LX/5Gs;

    iput-object p4, p0, LX/9E4;->c:LX/0TF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1456482
    iget-object v0, p0, LX/9E4;->d:LX/9E6;

    iget-object v0, v0, LX/9E6;->b:LX/3iL;

    invoke-virtual {v0, p1}, LX/3iL;->a(Ljava/lang/Throwable;)V

    .line 1456483
    iget-object v0, p0, LX/9E4;->c:LX/0TF;

    if-eqz v0, :cond_0

    .line 1456484
    iget-object v0, p0, LX/9E4;->c:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1456485
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1456486
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1456487
    if-nez p1, :cond_1

    .line 1456488
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fetch more comments returned null for id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9E4;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/9E4;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1456489
    :cond_0
    :goto_0
    return-void

    .line 1456490
    :cond_1
    iget-object v0, p0, LX/9E4;->d:LX/9E6;

    iget-object v0, v0, LX/9E6;->a:LX/1K9;

    new-instance v1, LX/82g;

    iget-object v2, p0, LX/9E4;->b:LX/5Gs;

    invoke-direct {v1, p1, v2}, LX/82g;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1456491
    iget-object v0, p0, LX/9E4;->c:LX/0TF;

    if-eqz v0, :cond_0

    .line 1456492
    iget-object v0, p0, LX/9E4;->c:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
