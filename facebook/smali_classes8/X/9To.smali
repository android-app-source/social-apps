.class public final enum LX/9To;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9To;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9To;

.field public static final enum ASK_TO_CONFIRM:LX/9To;

.field public static final enum COMPLETED:LX/9To;

.field public static final enum INITIAL:LX/9To;

.field public static final enum INITIATED:LX/9To;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1496758
    new-instance v0, LX/9To;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2}, LX/9To;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9To;->INITIAL:LX/9To;

    .line 1496759
    new-instance v0, LX/9To;

    const-string v1, "ASK_TO_CONFIRM"

    invoke-direct {v0, v1, v3}, LX/9To;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9To;->ASK_TO_CONFIRM:LX/9To;

    .line 1496760
    new-instance v0, LX/9To;

    const-string v1, "INITIATED"

    invoke-direct {v0, v1, v4}, LX/9To;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9To;->INITIATED:LX/9To;

    .line 1496761
    new-instance v0, LX/9To;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v5}, LX/9To;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9To;->COMPLETED:LX/9To;

    .line 1496762
    const/4 v0, 0x4

    new-array v0, v0, [LX/9To;

    sget-object v1, LX/9To;->INITIAL:LX/9To;

    aput-object v1, v0, v2

    sget-object v1, LX/9To;->ASK_TO_CONFIRM:LX/9To;

    aput-object v1, v0, v3

    sget-object v1, LX/9To;->INITIATED:LX/9To;

    aput-object v1, v0, v4

    sget-object v1, LX/9To;->COMPLETED:LX/9To;

    aput-object v1, v0, v5

    sput-object v0, LX/9To;->$VALUES:[LX/9To;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1496755
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9To;
    .locals 1

    .prologue
    .line 1496757
    const-class v0, LX/9To;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9To;

    return-object v0
.end method

.method public static values()[LX/9To;
    .locals 1

    .prologue
    .line 1496756
    sget-object v0, LX/9To;->$VALUES:[LX/9To;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9To;

    return-object v0
.end method
