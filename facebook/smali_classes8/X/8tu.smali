.class public LX/8tu;
.super Landroid/text/method/LinkMovementMethod;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8tu;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/5Qo;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/5Qo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1414239
    invoke-direct {p0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    .line 1414240
    iput-object p1, p0, LX/8tu;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1414241
    iput-object p2, p0, LX/8tu;->b:LX/5Qo;

    .line 1414242
    return-void
.end method

.method public static a(LX/0QB;)LX/8tu;
    .locals 5

    .prologue
    .line 1414243
    sget-object v0, LX/8tu;->c:LX/8tu;

    if-nez v0, :cond_1

    .line 1414244
    const-class v1, LX/8tu;

    monitor-enter v1

    .line 1414245
    :try_start_0
    sget-object v0, LX/8tu;->c:LX/8tu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1414246
    if-eqz v2, :cond_0

    .line 1414247
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1414248
    new-instance p0, LX/8tu;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/5Qo;->a(LX/0QB;)LX/5Qo;

    move-result-object v4

    check-cast v4, LX/5Qo;

    invoke-direct {p0, v3, v4}, LX/8tu;-><init>(Lcom/facebook/content/SecureContextHelper;LX/5Qo;)V

    .line 1414249
    move-object v0, p0

    .line 1414250
    sput-object v0, LX/8tu;->c:LX/8tu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1414251
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1414252
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1414253
    :cond_1
    sget-object v0, LX/8tu;->c:LX/8tu;

    return-object v0

    .line 1414254
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1414255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1414256
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1414257
    if-eq v2, v1, :cond_0

    if-nez v2, :cond_5

    .line 1414258
    :cond_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 1414259
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 1414260
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v4

    sub-int/2addr v0, v4

    .line 1414261
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1414262
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v4

    add-int/2addr v0, v4

    .line 1414263
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v4

    add-int/2addr v3, v4

    .line 1414264
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    .line 1414265
    invoke-virtual {v4, v3}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v3

    .line 1414266
    int-to-float v0, v0

    invoke-virtual {v4, v3, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v0

    .line 1414267
    const-class v3, Landroid/text/style/ClickableSpan;

    invoke-interface {p2, v0, v0, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 1414268
    array-length v3, v0

    if-eqz v3, :cond_4

    .line 1414269
    if-ne v2, v1, :cond_3

    .line 1414270
    aget-object v2, v0, v5

    instance-of v2, v2, Landroid/text/style/URLSpan;

    if-eqz v2, :cond_2

    .line 1414271
    aget-object v0, v0, v5

    check-cast v0, Landroid/text/style/URLSpan;

    .line 1414272
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    .line 1414273
    if-nez v2, :cond_6

    .line 1414274
    :cond_1
    :goto_0
    move v0, v1

    .line 1414275
    :goto_1
    return v0

    .line 1414276
    :cond_2
    aget-object v0, v0, v5

    invoke-virtual {v0, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 1414277
    :cond_3
    if-nez v2, :cond_1

    .line 1414278
    aget-object v2, v0, v5

    invoke-interface {p2, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    aget-object v0, v0, v5

    invoke-interface {p2, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p2, v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    goto :goto_0

    .line 1414279
    :cond_4
    invoke-static {p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    .line 1414280
    :cond_5
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/LinkMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 1414281
    :cond_6
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1414282
    iget-object v4, p0, LX/8tu;->b:LX/5Qo;

    invoke-virtual {v4, v2}, LX/5Qo;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1414283
    iget-object v4, p0, LX/8tu;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
