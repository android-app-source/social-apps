.class public LX/8un;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1415772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1415773
    return-void
.end method

.method public static a(Ljava/lang/CharSequence;)LX/8um;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1415741
    instance-of v0, p0, Landroid/text/Spannable;

    if-nez v0, :cond_0

    .line 1415742
    new-instance v0, LX/8um;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-direct {v0, v2, v1}, LX/8um;-><init>(II)V

    .line 1415743
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    .line 1415744
    check-cast v0, Landroid/text/Spannable;

    .line 1415745
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v6

    .line 1415746
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1415747
    if-eq v6, v1, :cond_1

    move-object v0, v4

    .line 1415748
    goto :goto_0

    .line 1415749
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 1415750
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const-class v5, LX/8uk;

    invoke-interface {v0, v2, v1, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/8uk;

    .line 1415751
    array-length v7, v1

    move v5, v2

    move v10, v3

    move v3, v2

    move v2, v10

    :goto_1
    if-ge v5, v7, :cond_5

    aget-object v8, v1, v5

    .line 1415752
    invoke-interface {v0, v8}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v9

    .line 1415753
    invoke-interface {v0, v8}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 1415754
    if-ge v9, v6, :cond_2

    if-le v8, v6, :cond_2

    move-object v0, v4

    .line 1415755
    goto :goto_0

    .line 1415756
    :cond_2
    if-ge v9, v6, :cond_4

    .line 1415757
    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1415758
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1415759
    :cond_4
    if-le v8, v6, :cond_3

    .line 1415760
    invoke-static {v2, v9}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_2

    .line 1415761
    :cond_5
    :goto_3
    if-ge v3, v2, :cond_6

    .line 1415762
    invoke-interface {v0, v3}, Landroid/text/Spannable;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1415763
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1415764
    :cond_6
    :goto_4
    add-int/lit8 v1, v2, -0x1

    if-ge v3, v1, :cond_7

    .line 1415765
    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Landroid/text/Spannable;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1415766
    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    .line 1415767
    :cond_7
    new-instance v0, LX/8um;

    invoke-direct {v0, v3, v2}, LX/8um;-><init>(II)V

    goto :goto_0
.end method

.method public static final b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1415768
    invoke-static {p0}, LX/8un;->a(Ljava/lang/CharSequence;)LX/8um;

    move-result-object v0

    .line 1415769
    if-nez v0, :cond_0

    .line 1415770
    const-string v0, ""

    .line 1415771
    :goto_0
    return-object v0

    :cond_0
    iget v1, v0, LX/8um;->a:I

    iget v0, v0, LX/8um;->b:I

    invoke-interface {p0, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
