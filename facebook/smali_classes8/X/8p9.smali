.class public LX/8p9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1405511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405512
    const/16 v0, 0x444

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/8p9;->a:Z

    .line 1405513
    return-void
.end method

.method public static a(LX/0QB;)LX/8p9;
    .locals 4

    .prologue
    .line 1405514
    const-class v1, LX/8p9;

    monitor-enter v1

    .line 1405515
    :try_start_0
    sget-object v0, LX/8p9;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1405516
    sput-object v2, LX/8p9;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1405517
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1405518
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1405519
    new-instance p0, LX/8p9;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/8p9;-><init>(LX/0Uh;)V

    .line 1405520
    move-object v0, p0

    .line 1405521
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1405522
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8p9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1405523
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1405524
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1405525
    iget-boolean v0, p0, LX/8p9;->a:Z

    return v0
.end method
