.class public final LX/A96;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$TargetingDescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "aymtLwiChannel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "aymtLwiChannel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field public f:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "insights"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "insights"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:J

.field public z:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1630054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1630055
    return-void
.end method

.method public static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;)LX/A96;
    .locals 4

    .prologue
    .line 1630056
    new-instance v0, LX/A96;

    invoke-direct {v0}, LX/A96;-><init>()V

    .line 1630057
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    .line 1630058
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 1630059
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/A96;->c:LX/15i;

    iput v1, v0, LX/A96;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1630060
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->l()I

    move-result v1

    iput v1, v0, LX/A96;->e:I

    .line 1630061
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v1

    iput-object v1, v0, LX/A96;->f:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 1630062
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 1630063
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v1

    iput-object v1, v0, LX/A96;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    .line 1630064
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    .line 1630065
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    .line 1630066
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    .line 1630067
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->s()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/A96;->l:LX/0Px;

    .line 1630068
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->t()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    iput-object v1, v0, LX/A96;->m:Lcom/facebook/graphql/model/FeedUnit;

    .line 1630069
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->u()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/A96;->n:LX/15i;

    iput v1, v0, LX/A96;->o:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1630070
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->v()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/A96;->p:LX/0Px;

    .line 1630071
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/A96;->q:Z

    .line 1630072
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->x()Z

    move-result v1

    iput-boolean v1, v0, LX/A96;->r:Z

    .line 1630073
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->y()Z

    move-result v1

    iput-boolean v1, v0, LX/A96;->s:Z

    .line 1630074
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->z()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v1

    iput-object v1, v0, LX/A96;->t:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 1630075
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->A()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v1

    iput-object v1, v0, LX/A96;->u:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 1630076
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->B()Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v1

    iput-object v1, v0, LX/A96;->v:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 1630077
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->C()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->w:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1630078
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->D()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->x:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 1630079
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->E()J

    move-result-wide v2

    iput-wide v2, v0, LX/A96;->y:J

    .line 1630080
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v2

    iput-wide v2, v0, LX/A96;->z:J

    .line 1630081
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->G()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->A:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 1630082
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->H()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$TargetingDescriptionModel;

    move-result-object v1

    iput-object v1, v0, LX/A96;->B:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$TargetingDescriptionModel;

    .line 1630083
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->I()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/A96;->C:LX/0Px;

    .line 1630084
    return-object v0

    .line 1630085
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1630086
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 25

    .prologue
    .line 1630087
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1630088
    move-object/from16 v0, p0

    iget-object v3, v0, LX/A96;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1630089
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A96;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1630090
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, LX/A96;->c:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/A96;->d:I

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v5, -0x6cdbb347

    invoke-static {v6, v7, v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1630091
    move-object/from16 v0, p0

    iget-object v6, v0, LX/A96;->f:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v2, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1630092
    move-object/from16 v0, p0

    iget-object v7, v0, LX/A96;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1630093
    move-object/from16 v0, p0

    iget-object v8, v0, LX/A96;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    invoke-virtual {v2, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1630094
    move-object/from16 v0, p0

    iget-object v9, v0, LX/A96;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1630095
    move-object/from16 v0, p0

    iget-object v10, v0, LX/A96;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1630096
    move-object/from16 v0, p0

    iget-object v11, v0, LX/A96;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1630097
    move-object/from16 v0, p0

    iget-object v12, v0, LX/A96;->l:LX/0Px;

    invoke-virtual {v2, v12}, LX/186;->c(Ljava/util/List;)I

    move-result v12

    .line 1630098
    move-object/from16 v0, p0

    iget-object v13, v0, LX/A96;->m:Lcom/facebook/graphql/model/FeedUnit;

    sget-object v14, LX/16Z;->a:LX/16Z;

    invoke-virtual {v2, v13, v14}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v13

    .line 1630099
    sget-object v14, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v14

    :try_start_1
    move-object/from16 v0, p0

    iget-object v15, v0, LX/A96;->n:LX/15i;

    move-object/from16 v0, p0

    iget v0, v0, LX/A96;->o:I

    move/from16 v16, v0

    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v14, -0x8f3627

    move/from16 v0, v16

    invoke-static {v15, v0, v14}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v14

    invoke-static {v2, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1630100
    move-object/from16 v0, p0

    iget-object v15, v0, LX/A96;->p:LX/0Px;

    invoke-static {v2, v15}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v15

    .line 1630101
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->t:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 1630102
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->u:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1630103
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->v:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 1630104
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->w:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1630105
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->x:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1630106
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->A:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1630107
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->B:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$TargetingDescriptionModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1630108
    move-object/from16 v0, p0

    iget-object v0, v0, LX/A96;->C:LX/0Px;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v23

    .line 1630109
    const/16 v24, 0x1b

    move/from16 v0, v24

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1630110
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1630111
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1630112
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1630113
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget v4, v0, LX/A96;->e:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1630114
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1630115
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1630116
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1630117
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1630118
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1630119
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1630120
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1630121
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1630122
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1630123
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1630124
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/A96;->q:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1630125
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/A96;->r:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1630126
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/A96;->s:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1630127
    const/16 v3, 0x11

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630128
    const/16 v3, 0x12

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630129
    const/16 v3, 0x13

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630130
    const/16 v3, 0x14

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630131
    const/16 v3, 0x15

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630132
    const/16 v3, 0x16

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/A96;->y:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1630133
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/A96;->z:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1630134
    const/16 v3, 0x18

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630135
    const/16 v3, 0x19

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630136
    const/16 v3, 0x1a

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1630137
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1630138
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1630139
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1630140
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1630141
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1630142
    new-instance v3, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    invoke-direct {v3, v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;-><init>(LX/15i;)V

    .line 1630143
    return-object v3

    .line 1630144
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 1630145
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method
