.class public final LX/95k;
.super LX/95j;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/95j",
        "<TT;>;",
        "Ljava/io/Closeable;"
    }
.end annotation


# static fields
.field private static final a:LX/2nj;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1437363
    new-instance v0, LX/2nj;

    .line 1437364
    const-string v1, "@LOCAL_BATCH:"

    move-object v1, v1

    .line 1437365
    sget-object v2, LX/2nk;->AFTER:LX/2nk;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LX/2nj;-><init>(Ljava/lang/String;LX/2nk;Z)V

    sput-object v0, LX/95k;->a:LX/2nj;

    return-void
.end method

.method public constructor <init>(LX/2nf;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nf;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1437415
    invoke-direct {p0, p1}, LX/95j;-><init>(LX/2nf;)V

    .line 1437416
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/95k;->c:Z

    .line 1437417
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/95k;->b:LX/0Px;

    .line 1437418
    return-void
.end method

.method private constructor <init>(LX/95k;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95k",
            "<TT;>;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1437411
    invoke-direct {p0, p1}, LX/95j;-><init>(LX/95j;)V

    .line 1437412
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/95k;->c:Z

    .line 1437413
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/95k;->b:LX/0Px;

    .line 1437414
    return-void
.end method

.method private declared-synchronized a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1437408
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/95k;->b:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437409
    monitor-exit p0

    return-void

    .line 1437410
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/95j;LX/2nj;[J[J[J)LX/0Px;
    .locals 10
    .param p1    # LX/95j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/2nj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95j",
            "<TT;>;",
            "LX/2nj;",
            "[J[J[J)",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437396
    monitor-enter p0

    :try_start_0
    instance-of v1, p1, LX/95k;

    if-eqz v1, :cond_0

    .line 1437397
    move-object v0, p1

    check-cast v0, LX/95k;

    move-object v1, v0

    .line 1437398
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, v1, LX/95k;->b:LX/0Px;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v1, v0

    .line 1437399
    new-instance v6, LX/95g;

    invoke-direct {v6, v1}, LX/95g;-><init>(Ljava/util/ArrayList;)V

    move-object v9, v1

    .line 1437400
    :goto_0
    new-instance v7, LX/95h;

    invoke-direct {v7, v9}, LX/95h;-><init>(Ljava/util/ArrayList;)V

    .line 1437401
    new-instance v8, LX/95i;

    invoke-direct {v8, v9}, LX/95i;-><init>(Ljava/util/ArrayList;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1437402
    invoke-virtual/range {v1 .. v8}, LX/95j;->a(LX/95j;[J[J[JLX/95f;LX/95f;LX/95f;)LX/0Px;

    move-result-object v1

    .line 1437403
    invoke-static {v9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v2}, LX/95k;->a(LX/0Px;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437404
    monitor-exit p0

    return-object v1

    .line 1437405
    :cond_0
    const/4 v6, 0x0

    .line 1437406
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v9, v1

    goto :goto_0

    .line 1437407
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 1437393
    iget-object v0, p0, LX/95k;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1437394
    iget-object v0, p0, LX/95k;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 1437395
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1437391
    add-int v0, p1, p2

    invoke-virtual {p0}, LX/95j;->c()I

    move-result v1

    invoke-static {p1, v0, v1}, LX/0PB;->checkPositionIndexes(III)V

    .line 1437392
    iget-object v0, p0, LX/95k;->b:LX/0Px;

    add-int v1, p1, p2

    invoke-virtual {v0, p1, v1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/2nj;
    .locals 2

    .prologue
    .line 1437384
    invoke-super {p0}, LX/95j;->c()I

    move-result v0

    invoke-virtual {p0}, LX/95j;->c()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 1437385
    invoke-super {p0}, LX/95j;->b()LX/2nj;

    move-result-object v1

    .line 1437386
    iget-object v0, v1, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1437387
    invoke-static {v0}, LX/3DQ;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, v1

    .line 1437388
    :goto_1
    return-object v0

    .line 1437389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1437390
    :cond_1
    sget-object v0, LX/95k;->a:LX/2nj;

    goto :goto_1
.end method

.method public final b(I)LX/95k;
    .locals 3
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/95k",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1437368
    monitor-enter p0

    .line 1437369
    :try_start_0
    invoke-virtual {p0}, LX/95j;->h()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/95k;->c:Z

    if-eqz v1, :cond_1

    .line 1437370
    :cond_0
    monitor-exit p0

    .line 1437371
    :goto_0
    return-object v0

    .line 1437372
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/95k;->c:Z

    .line 1437373
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1437374
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/95k;->b:LX/0Px;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1437375
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-super {p0, v2, p1}, LX/95j;->a(II)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1437376
    monitor-enter p0

    .line 1437377
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, p0, LX/95k;->c:Z

    .line 1437378
    invoke-virtual {p0}, LX/95j;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1437379
    monitor-exit p0

    goto :goto_0

    .line 1437380
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1437381
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1437382
    :cond_2
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1437383
    new-instance v0, LX/95k;

    invoke-direct {v0, p0, v1}, LX/95k;-><init>(LX/95k;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 1437367
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/95k;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1437366
    invoke-virtual {p0}, LX/95j;->c()I

    move-result v0

    invoke-super {p0}, LX/95j;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
