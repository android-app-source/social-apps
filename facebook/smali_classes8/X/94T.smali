.class public final LX/94T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

.field public final synthetic b:LX/94V;


# direct methods
.method public constructor <init>(LX/94V;Lcom/facebook/widget/text/SimpleVariableTextLayoutView;)V
    .locals 0

    .prologue
    .line 1435279
    iput-object p1, p0, LX/94T;->b:LX/94V;

    iput-object p2, p0, LX/94T;->a:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1435280
    iget-object v0, p0, LX/94T;->a:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    const/4 v3, 0x0

    .line 1435281
    iget-object p1, v0, LX/2Wj;->q:Landroid/text/Layout;

    if-eqz p1, :cond_0

    iget-object p1, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    move-result p1

    if-lez p1, :cond_0

    iget-object p1, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {p1, v3}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result p1

    if-lez p1, :cond_0

    .line 1435282
    const/4 v3, 0x1

    .line 1435283
    :cond_0
    move v0, v3

    .line 1435284
    if-eqz v0, :cond_1

    .line 1435285
    iget-object v0, p0, LX/94T;->b:LX/94V;

    invoke-virtual {v0}, LX/94V;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p0, LX/94T;->a:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 1435286
    iget-object p0, v0, LX/2Wj;->c:Ljava/lang/Object;

    move-object v0, p0

    .line 1435287
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1435288
    const/16 v3, 0x30

    invoke-virtual {v0, v3, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1435289
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 1435290
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method
