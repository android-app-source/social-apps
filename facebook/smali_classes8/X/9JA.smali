.class public LX/9JA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1464425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Px",
            "<TT;>;)",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1464426
    const-string v0, "BufferRowsHelper"

    const-string v1, "Re-flattening rows. It would be better to directly use the network response."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1464427
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1464428
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1464429
    :goto_0
    return-object v0

    .line 1464430
    :cond_0
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v5, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 1464431
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    new-array v6, v0, [I

    .line 1464432
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    new-array v7, v0, [Ljava/lang/Class;

    move v1, v4

    move v3, v4

    .line 1464433
    :goto_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1464434
    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1464435
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    aput-object v8, v7, v1

    .line 1464436
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-static {v0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    .line 1464437
    :try_start_0
    invoke-virtual {v5, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1464438
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-static {v8}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v8

    add-int/2addr v8, v3

    .line 1464439
    aput v8, v6, v1

    .line 1464440
    array-length v0, v0

    add-int/2addr v3, v0

    .line 1464441
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1464442
    :catch_0
    move-exception v0

    .line 1464443
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1464444
    :cond_1
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1464445
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1464446
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1464447
    const-string v1, "BufferRowsHelper.combineRows"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 1464448
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1464449
    :goto_2
    array-length v2, v6

    if-ge v4, v2, :cond_2

    .line 1464450
    aget v2, v6, v4

    aget-object v3, v7, v4

    invoke-virtual {v0, v2, v3}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    .line 1464451
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1464452
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1464453
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0Px;)Ljava/nio/ByteBuffer;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1464454
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    move-object v3, v1

    :goto_0
    if-ge v2, v5, :cond_4

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1464455
    instance-of v4, v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-nez v4, :cond_0

    move-object v0, v1

    .line 1464456
    :goto_1
    return-object v0

    .line 1464457
    :cond_0
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 1464458
    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v4

    .line 1464459
    if-eqz v4, :cond_1

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 1464460
    goto :goto_1

    .line 1464461
    :cond_2
    if-eqz v3, :cond_3

    if-eq v3, v4, :cond_3

    move-object v0, v1

    .line 1464462
    goto :goto_1

    .line 1464463
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v3, v4

    goto :goto_0

    .line 1464464
    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method
