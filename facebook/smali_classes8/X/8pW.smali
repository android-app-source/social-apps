.class public LX/8pW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8pW;


# instance fields
.field private final a:I

.field public final b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v1, 0x1e

    .line 1405951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405952
    iput v1, p0, LX/8pW;->a:I

    .line 1405953
    new-instance v0, LX/0aq;

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/8pW;->b:LX/0aq;

    .line 1405954
    return-void
.end method

.method public static a(LX/0QB;)LX/8pW;
    .locals 3

    .prologue
    .line 1405955
    sget-object v0, LX/8pW;->c:LX/8pW;

    if-nez v0, :cond_1

    .line 1405956
    const-class v1, LX/8pW;

    monitor-enter v1

    .line 1405957
    :try_start_0
    sget-object v0, LX/8pW;->c:LX/8pW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1405958
    if-eqz v2, :cond_0

    .line 1405959
    :try_start_1
    new-instance v0, LX/8pW;

    invoke-direct {v0}, LX/8pW;-><init>()V

    .line 1405960
    move-object v0, v0

    .line 1405961
    sput-object v0, LX/8pW;->c:LX/8pW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1405962
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1405963
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1405964
    :cond_1
    sget-object v0, LX/8pW;->c:LX/8pW;

    return-object v0

    .line 1405965
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1405966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1405967
    iget-object v0, p0, LX/8pW;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 1

    .prologue
    .line 1405968
    iget-object v0, p0, LX/8pW;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method
