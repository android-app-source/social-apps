.class public LX/ANt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/AOU;

.field public final c:LX/AOC;

.field public final d:LX/AOG;

.field public final e:LX/AOa;

.field public final f:LX/6Jt;

.field public final g:Landroid/content/Context;

.field public final h:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

.field public final i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final j:Landroid/view/ViewGroup;

.field public final k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private final l:LX/ANE;

.field public final m:LX/AMO;

.field public final n:LX/AMR;

.field public final o:LX/1FZ;

.field public p:LX/ANo;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/ANi;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/ANi;",
            "LX/ANo;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6KY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1668432
    const-class v0, LX/ANt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ANt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6Jt;Landroid/content/Context;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/ViewGroup;Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/ANE;LX/AOU;LX/AOC;LX/AOG;LX/AOa;LX/AMO;LX/AMR;LX/1FZ;)V
    .locals 0
    .param p1    # LX/6Jt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/ANE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1668433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668434
    iput-object p1, p0, LX/ANt;->f:LX/6Jt;

    .line 1668435
    iput-object p2, p0, LX/ANt;->g:Landroid/content/Context;

    .line 1668436
    iput-object p3, p0, LX/ANt;->h:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    .line 1668437
    iput-object p4, p0, LX/ANt;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668438
    iput-object p5, p0, LX/ANt;->j:Landroid/view/ViewGroup;

    .line 1668439
    iput-object p6, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668440
    iput-object p7, p0, LX/ANt;->l:LX/ANE;

    .line 1668441
    iput-object p8, p0, LX/ANt;->b:LX/AOU;

    .line 1668442
    iput-object p9, p0, LX/ANt;->c:LX/AOC;

    .line 1668443
    iput-object p10, p0, LX/ANt;->d:LX/AOG;

    .line 1668444
    iput-object p11, p0, LX/ANt;->e:LX/AOa;

    .line 1668445
    iput-object p12, p0, LX/ANt;->m:LX/AMO;

    .line 1668446
    iput-object p13, p0, LX/ANt;->n:LX/AMR;

    .line 1668447
    iput-object p14, p0, LX/ANt;->o:LX/1FZ;

    .line 1668448
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 1668449
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 1668450
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/ANt;->q:Ljava/util/List;

    .line 1668451
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/ANt;->r:Ljava/util/Map;

    .line 1668452
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668453
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1668454
    new-instance v1, Lcom/facebook/videocodec/effects/model/ColorFilter;

    const-string v2, "None"

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v1 .. v7}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668455
    invoke-static {}, Lcom/facebook/videocodec/effects/model/ColorFilter;->k()Lcom/facebook/videocodec/effects/model/ColorFilter;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668456
    invoke-static {}, Lcom/facebook/videocodec/effects/model/ColorFilter;->h()Lcom/facebook/videocodec/effects/model/ColorFilter;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668457
    invoke-static {}, Lcom/facebook/videocodec/effects/model/ColorFilter;->i()Lcom/facebook/videocodec/effects/model/ColorFilter;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668458
    invoke-static {}, Lcom/facebook/videocodec/effects/model/ColorFilter;->j()Lcom/facebook/videocodec/effects/model/ColorFilter;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668459
    invoke-static {}, Lcom/facebook/videocodec/effects/model/ColorFilter;->g()Lcom/facebook/videocodec/effects/model/ColorFilter;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668460
    new-instance v1, LX/ANj;

    iget-object v2, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/ANj;-><init>(Landroid/content/Context;LX/0Px;)V

    .line 1668461
    new-instance v2, LX/ANp;

    iget-object v3, p0, LX/ANt;->f:LX/6Jt;

    iget-object v4, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v5, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-direct {v2, v3, v4, v5}, LX/ANp;-><init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;)V

    .line 1668462
    iget-object v3, v2, LX/ANp;->i:LX/ANj;

    if-ne v3, v1, :cond_1

    .line 1668463
    :goto_0
    iget-object v3, p0, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668464
    iget-object v3, p0, LX/ANt;->r:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668465
    iget-object v1, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668466
    iget-object v3, v2, LX/ANp;->f:LX/6KY;

    move-object v2, v3

    .line 1668467
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668468
    new-instance v1, LX/AOD;

    iget-object v2, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/AOD;-><init>(Landroid/content/Context;)V

    .line 1668469
    iget-object v2, p0, LX/ANt;->d:LX/AOG;

    iget-object v3, p0, LX/ANt;->f:LX/6Jt;

    iget-object v4, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v5, p0, LX/ANt;->g:Landroid/content/Context;

    .line 1668470
    new-instance v8, LX/AOF;

    const-class v6, LX/7SL;

    invoke-interface {v2, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/7SL;

    invoke-direct {v8, v3, v4, v5, v6}, LX/AOF;-><init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;LX/7SL;)V

    .line 1668471
    move-object v2, v8

    .line 1668472
    iget-object v3, v2, LX/AOF;->h:LX/AOD;

    if-ne v3, v1, :cond_2

    .line 1668473
    :goto_1
    iget-object v3, p0, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668474
    iget-object v3, p0, LX/ANt;->r:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668475
    iget-object v3, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668476
    iget-object v4, v2, LX/AOF;->f:LX/6KY;

    move-object v4, v4

    .line 1668477
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668478
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1668479
    const/high16 v4, -0x1000000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668480
    const v4, -0xbbbbbc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668481
    const v4, -0x777778

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668482
    const v4, -0x333334

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668483
    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668484
    const/high16 v4, -0x10000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668485
    const v4, -0xff0100

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668486
    const v4, -0xffff01

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668487
    const/16 v4, -0x100

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668488
    const v4, -0xff0001

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668489
    const v4, -0xff01

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668490
    new-instance v4, LX/ANy;

    iget-object v5, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v4, v5, v3}, LX/ANy;-><init>(Landroid/content/Context;LX/0Px;)V

    .line 1668491
    new-instance v3, LX/AO3;

    iget-object v5, p0, LX/ANt;->f:LX/6Jt;

    iget-object v6, p0, LX/ANt;->j:Landroid/view/ViewGroup;

    iget-object v8, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v9, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-direct {v3, v5, v6, v8, v9}, LX/AO3;-><init>(LX/6Jt;Landroid/view/ViewGroup;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;)V

    .line 1668492
    iget-object v5, v3, LX/AO3;->m:LX/ANy;

    if-ne v5, v4, :cond_3

    .line 1668493
    :goto_2
    iget-object v5, p0, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668494
    iget-object v5, p0, LX/ANt;->r:Ljava/util/Map;

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668495
    iget-object v4, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668496
    iget-object v5, v3, LX/AO3;->k:LX/6KY;

    move-object v3, v5

    .line 1668497
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668498
    new-instance v3, LX/AOP;

    iget-object v4, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/AOP;-><init>(Landroid/content/Context;)V

    .line 1668499
    iget-object v4, p0, LX/ANt;->b:LX/AOU;

    iget-object v5, p0, LX/ANt;->f:LX/6Jt;

    iget-object v6, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v8, p0, LX/ANt;->g:Landroid/content/Context;

    .line 1668500
    new-instance v10, LX/AOT;

    invoke-static {v4}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->b(LX/0QB;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    move-result-object v9

    check-cast v9, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-direct {v10, v5, v6, v8, v9}, LX/AOT;-><init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V

    .line 1668501
    move-object v4, v10

    .line 1668502
    iget-object v5, p0, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668503
    iget-object v5, p0, LX/ANt;->r:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668504
    iget-object v3, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668505
    new-instance v5, LX/6KY;

    iget-object v6, v4, LX/AOT;->b:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-direct {v5, v6}, LX/6KY;-><init>(LX/61B;)V

    move-object v4, v5

    .line 1668506
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668507
    new-instance v3, LX/AO4;

    iget-object v4, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/AO4;-><init>(Landroid/content/Context;)V

    .line 1668508
    iget-object v4, p0, LX/ANt;->c:LX/AOC;

    iget-object v5, p0, LX/ANt;->f:LX/6Jt;

    iget-object v6, p0, LX/ANt;->j:Landroid/view/ViewGroup;

    iget-object v8, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v9, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-virtual {v4, v5, v6, v8, v9}, LX/AOC;->a(LX/6Jt;Landroid/view/ViewGroup;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;)LX/AOB;

    move-result-object v4

    .line 1668509
    iget-object v5, v4, LX/AOB;->l:LX/AO4;

    if-ne v5, v3, :cond_4

    .line 1668510
    :goto_3
    iget-object v5, p0, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668511
    iget-object v5, p0, LX/ANt;->r:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668512
    iget-object v3, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668513
    iget-object v5, v4, LX/AOB;->i:LX/6KY;

    move-object v5, v5

    .line 1668514
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668515
    new-instance v3, LX/AOV;

    iget-object v5, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-direct {v3, v5}, LX/AOV;-><init>(Landroid/content/Context;)V

    .line 1668516
    iget-object v5, p0, LX/ANt;->e:LX/AOa;

    iget-object v6, p0, LX/ANt;->f:LX/6Jt;

    iget-object v8, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v9, p0, LX/ANt;->g:Landroid/content/Context;

    .line 1668517
    new-instance v11, LX/AOZ;

    invoke-static {v5}, LX/BVQ;->b(LX/0QB;)LX/BVQ;

    move-result-object v10

    check-cast v10, LX/BVQ;

    invoke-direct {v11, v6, v8, v9, v10}, LX/AOZ;-><init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;LX/BVQ;)V

    .line 1668518
    move-object v5, v11

    .line 1668519
    iget-object v6, p0, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668520
    iget-object v6, p0, LX/ANt;->r:Ljava/util/Map;

    invoke-interface {v6, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668521
    iget-object v3, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668522
    new-instance v6, LX/6KY;

    iget-object v8, v5, LX/AOZ;->b:LX/BVQ;

    invoke-direct {v6, v8}, LX/6KY;-><init>(LX/61B;)V

    move-object v5, v6

    .line 1668523
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668524
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1668525
    const-string v5, "Hide FPS"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668526
    const-string v5, "Show FPS"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668527
    new-instance v5, LX/ANv;

    iget-object v6, p0, LX/ANt;->g:Landroid/content/Context;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v5, v6, v3}, LX/ANv;-><init>(Landroid/content/Context;LX/0Px;)V

    .line 1668528
    iget-object v3, p0, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668529
    new-instance v3, LX/ANx;

    iget-object v6, p0, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v8, p0, LX/ANt;->g:Landroid/content/Context;

    iget-object v9, p0, LX/ANt;->o:LX/1FZ;

    iget-object v10, p0, LX/ANt;->f:LX/6Jt;

    invoke-direct {v3, v6, v8, v9, v10}, LX/ANx;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;LX/1FZ;LX/6Jt;)V

    .line 1668530
    iget-object v6, v3, LX/ANx;->h:LX/ANv;

    if-ne v5, v6, :cond_5

    .line 1668531
    :goto_4
    iget-object v6, p0, LX/ANt;->r:Ljava/util/Map;

    invoke-interface {v6, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668532
    iget-object v5, p0, LX/ANt;->s:Ljava/util/List;

    .line 1668533
    iget-object v6, v3, LX/ANx;->f:LX/6KY;

    move-object v3, v6

    .line 1668534
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668535
    iget-object v3, p0, LX/ANt;->f:LX/6Jt;

    iget-object v5, p0, LX/ANt;->s:Ljava/util/List;

    invoke-virtual {v3, v5}, LX/6Jt;->a(Ljava/util/List;)V

    .line 1668536
    iget-object v3, p0, LX/ANt;->m:LX/AMO;

    new-instance v5, LX/ANq;

    invoke-direct {v5, p0, v4}, LX/ANq;-><init>(LX/ANt;LX/AOB;)V

    const/4 v4, 0x0

    .line 1668537
    invoke-virtual {v3, v7, v4, v5, v4}, LX/AMO;->a(ZLjava/lang/String;LX/AMN;LX/AZo;)V

    .line 1668538
    iget-object v3, p0, LX/ANt;->n:LX/AMR;

    new-instance v4, LX/ANr;

    invoke-direct {v4, p0, v1, v2}, LX/ANr;-><init>(LX/ANt;LX/AOD;LX/AOF;)V

    invoke-virtual {v3, v7, v4}, LX/AMR;->a(ZLX/ANr;)V

    .line 1668539
    new-instance v0, LX/AOJ;

    iget-object v1, p0, LX/ANt;->g:Landroid/content/Context;

    iget-object v2, p0, LX/ANt;->q:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    const/4 v3, -0x1

    new-instance v4, LX/ANs;

    invoke-direct {v4, p0}, LX/ANs;-><init>(LX/ANt;)V

    invoke-direct {v0, v1, v2, v3, v4}, LX/AOJ;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    .line 1668540
    iget-object v1, p0, LX/ANt;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668541
    iget-object v0, p0, LX/ANt;->l:LX/ANE;

    if-eqz v0, :cond_0

    .line 1668542
    iget-object v0, p0, LX/ANt;->l:LX/ANE;

    .line 1668543
    iget-object v1, v0, LX/ANE;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v1, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->S:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1668544
    :cond_0
    return-void

    .line 1668545
    :cond_1
    iput-object v1, v2, LX/ANp;->i:LX/ANj;

    .line 1668546
    new-instance v3, LX/AOI;

    iget-object v4, v2, LX/ANp;->c:Landroid/content/Context;

    iget-object v5, v2, LX/ANp;->i:LX/ANj;

    .line 1668547
    iget-object v6, v5, LX/ANj;->c:LX/0Px;

    move-object v5, v6

    .line 1668548
    const/4 v6, 0x0

    iget-object v8, v2, LX/ANp;->d:LX/ANk;

    invoke-direct {v3, v4, v5, v6, v8}, LX/AOI;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    iput-object v3, v2, LX/ANp;->g:LX/AOI;

    goto/16 :goto_0

    .line 1668549
    :cond_2
    iput-object v1, v2, LX/AOF;->h:LX/AOD;

    .line 1668550
    new-instance v3, LX/AOO;

    iget-object v4, v2, LX/AOF;->c:Landroid/content/Context;

    .line 1668551
    iget-object v5, v1, LX/AOD;->c:LX/0Px;

    move-object v5, v5

    .line 1668552
    const/4 v6, -0x1

    iget-object v8, v2, LX/AOF;->d:LX/ANk;

    invoke-direct {v3, v4, v5, v6, v8}, LX/AOO;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    iput-object v3, v2, LX/AOF;->e:LX/AOO;

    goto/16 :goto_1

    .line 1668553
    :cond_3
    iput-object v4, v3, LX/AO3;->m:LX/ANy;

    .line 1668554
    new-instance v5, LX/AOM;

    iget-object v6, v3, LX/AO3;->d:Landroid/content/Context;

    .line 1668555
    iget-object v8, v4, LX/ANy;->c:LX/0Px;

    move-object v8, v8

    .line 1668556
    const/4 v9, -0x1

    iget-object v10, v3, LX/AO3;->i:LX/ANk;

    invoke-direct {v5, v6, v8, v9, v10}, LX/AOM;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    iput-object v5, v3, LX/AO3;->l:LX/AOM;

    goto/16 :goto_2

    .line 1668557
    :cond_4
    iput-object v3, v4, LX/AOB;->l:LX/AO4;

    .line 1668558
    invoke-static {v4}, LX/AOB;->e(LX/AOB;)LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/AOB;->m:LX/0Px;

    .line 1668559
    new-instance v5, LX/AON;

    iget-object v6, v4, LX/AOB;->f:Landroid/content/Context;

    iget-object v8, v4, LX/AOB;->m:LX/0Px;

    const/4 v9, -0x1

    iget-object v10, v4, LX/AOB;->g:LX/ANk;

    invoke-direct {v5, v6, v8, v9, v10}, LX/AON;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    iput-object v5, v4, LX/AOB;->h:LX/AON;

    goto/16 :goto_3

    .line 1668560
    :cond_5
    iput-object v5, v3, LX/ANx;->h:LX/ANv;

    .line 1668561
    new-instance v6, LX/AOL;

    iget-object v8, v3, LX/ANx;->b:Landroid/content/Context;

    iget-object v9, v3, LX/ANx;->h:LX/ANv;

    .line 1668562
    iget-object v10, v9, LX/ANv;->c:LX/0Px;

    move-object v9, v10

    .line 1668563
    const/4 v10, 0x0

    iget-object v11, v3, LX/ANx;->d:LX/ANk;

    invoke-direct {v6, v8, v9, v10, v11}, LX/AOL;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    iput-object v6, v3, LX/ANx;->g:LX/AOL;

    goto/16 :goto_4
.end method
