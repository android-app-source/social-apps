.class public LX/AKi;
.super Landroid/widget/ProgressBar;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1664081
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AKi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1664082
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664083
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AKi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664084
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664085
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664086
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, LX/AKi;->setMax(I)V

    .line 1664087
    invoke-virtual {p0}, LX/AKi;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AKi;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1664088
    return-void
.end method
