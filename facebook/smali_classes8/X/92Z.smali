.class public LX/92Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static volatile d:LX/92Z;


# instance fields
.field public final c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1432412
    const-string v0, "60"

    sput-object v0, LX/92Z;->a:Ljava/lang/String;

    .line 1432413
    const-string v0, "32"

    sput-object v0, LX/92Z;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432410
    iput-object p1, p0, LX/92Z;->c:LX/0tX;

    .line 1432411
    return-void
.end method

.method public static a(LX/92Z;LX/92H;LX/0zS;)LX/0zO;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/92H;",
            "LX/0zS;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1432390
    iget-boolean v0, p1, LX/92H;->e:Z

    if-eqz v0, :cond_3

    const-wide/32 v0, 0x127500

    .line 1432391
    :goto_0
    invoke-static {}, LX/5LD;->a()LX/5LB;

    move-result-object v2

    .line 1432392
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    .line 1432393
    if-nez v5, :cond_0

    .line 1432394
    sget-object v5, LX/0wB;->a:LX/0wC;

    .line 1432395
    :cond_0
    invoke-static {}, LX/5LD;->a()LX/5LB;

    move-result-object v6

    .line 1432396
    const-string v7, "taggable_activity_id"

    iget-object v8, p1, LX/92H;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "taggable_object_count"

    iget v9, p1, LX/92H;->d:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "taggable_object_image_scale"

    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v7

    const-string v8, "taggable_object_pp_size"

    sget-object v9, LX/92Z;->a:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "image_scale"

    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v5

    const-string v7, "request_id"

    iget-object v8, p1, LX/92H;->g:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v7, "typeahead_session_id"

    iget v8, p1, LX/92H;->f:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v7, "place_id"

    iget-object v8, p1, LX/92H;->i:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v7, "surface"

    iget-object v8, p1, LX/92H;->j:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v7, "is_prefetch"

    iget-boolean v8, p1, LX/92H;->k:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v5

    const-string v7, "minutiae_image_size_large"

    sget-object v8, LX/92Z;->b:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v7, "get_background_color"

    iget-object v8, p1, LX/92H;->j:Ljava/lang/String;

    const-string v9, "now"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v7

    const-string v8, "taggable_object_query_string"

    iget-object v5, p1, LX/92H;->b:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p1, LX/92H;->b:Ljava/lang/String;

    :goto_1
    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "dont_load_templates"

    iget-boolean v5, p1, LX/92H;->h:Z

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_2
    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1432397
    iget-object v5, p1, LX/92H;->l:Landroid/location/Location;

    if-eqz v5, :cond_1

    .line 1432398
    const-string v5, "userLatitude"

    iget-object v7, p1, LX/92H;->l:Landroid/location/Location;

    invoke-virtual {v7}, Landroid/location/Location;->getLatitude()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v7, "userLongitude"

    iget-object v8, p1, LX/92H;->l:Landroid/location/Location;

    invoke-virtual {v8}, Landroid/location/Location;->getLongitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1432399
    :cond_1
    iget-object v5, p1, LX/92H;->c:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 1432400
    const-string v5, "taggable_object_cursor"

    iget-object v7, p1, LX/92H;->c:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1432401
    :cond_2
    iget-object v5, v6, LX/0gW;->e:LX/0w7;

    move-object v5, v5

    .line 1432402
    move-object v3, v5

    .line 1432403
    new-instance v4, LX/92d;

    invoke-direct {v4, v2, v3}, LX/92d;-><init>(LX/0gW;LX/0w7;)V

    .line 1432404
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v2

    .line 1432405
    move-object v0, v2

    .line 1432406
    return-object v0

    .line 1432407
    :cond_3
    const-wide/16 v0, 0x384

    goto/16 :goto_0

    .line 1432408
    :cond_4
    const-string v5, ""

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/92Z;
    .locals 4

    .prologue
    .line 1432377
    sget-object v0, LX/92Z;->d:LX/92Z;

    if-nez v0, :cond_1

    .line 1432378
    const-class v1, LX/92Z;

    monitor-enter v1

    .line 1432379
    :try_start_0
    sget-object v0, LX/92Z;->d:LX/92Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1432380
    if-eqz v2, :cond_0

    .line 1432381
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1432382
    new-instance p0, LX/92Z;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/92Z;-><init>(LX/0tX;)V

    .line 1432383
    move-object v0, p0

    .line 1432384
    sput-object v0, LX/92Z;->d:LX/92Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432385
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1432386
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432387
    :cond_1
    sget-object v0, LX/92Z;->d:LX/92Z;

    return-object v0

    .line 1432388
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1432389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/92H;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/92H;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1432374
    iget-object v0, p0, LX/92Z;->c:LX/0tX;

    .line 1432375
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-static {p0, p1, v1}, LX/92Z;->a(LX/92Z;LX/92H;LX/0zS;)LX/0zO;

    move-result-object v1

    move-object v1, v1

    .line 1432376
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
