.class public LX/AO3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANo;


# instance fields
.field public final a:LX/6Jt;

.field private final b:Landroid/view/ViewGroup;

.field public final c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final d:Landroid/content/Context;

.field private final e:Landroid/view/View$OnTouchListener;

.field private f:Landroid/view/View;

.field private g:Lcom/facebook/resources/ui/FbButton;

.field private h:Lcom/facebook/resources/ui/FbButton;

.field public final i:LX/ANk;

.field public final j:LX/7SB;

.field public final k:LX/6KY;

.field public l:LX/AOM;

.field public m:LX/ANy;

.field public n:I


# direct methods
.method public constructor <init>(LX/6Jt;Landroid/view/ViewGroup;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1668662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668663
    new-instance v0, LX/ANz;

    invoke-direct {v0, p0}, LX/ANz;-><init>(LX/AO3;)V

    iput-object v0, p0, LX/AO3;->i:LX/ANk;

    .line 1668664
    iput v1, p0, LX/AO3;->n:I

    .line 1668665
    iput-object p1, p0, LX/AO3;->a:LX/6Jt;

    .line 1668666
    iput-object p2, p0, LX/AO3;->b:Landroid/view/ViewGroup;

    .line 1668667
    iput-object p3, p0, LX/AO3;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668668
    iput-object p4, p0, LX/AO3;->d:Landroid/content/Context;

    .line 1668669
    new-instance v0, LX/AO2;

    invoke-direct {v0, p0}, LX/AO2;-><init>(LX/AO3;)V

    iput-object v0, p0, LX/AO3;->e:Landroid/view/View$OnTouchListener;

    .line 1668670
    new-instance v0, LX/7SB;

    .line 1668671
    sget-object v1, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v1, v1

    .line 1668672
    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/7SB;-><init>(LX/0So;Z)V

    iput-object v0, p0, LX/AO3;->j:LX/7SB;

    .line 1668673
    new-instance v0, LX/6KY;

    iget-object v1, p0, LX/AO3;->j:LX/7SB;

    invoke-direct {v0, v1}, LX/6KY;-><init>(LX/61B;)V

    iput-object v0, p0, LX/AO3;->k:LX/6KY;

    .line 1668674
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 4

    .prologue
    .line 1668675
    iget-object v0, p0, LX/AO3;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1668676
    iget-object v0, p0, LX/AO3;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030234

    iget-object v2, p0, LX/AO3;->b:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AO3;->f:Landroid/view/View;

    .line 1668677
    iget-object v0, p0, LX/AO3;->f:Landroid/view/View;

    const v1, 0x7f0d088a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AO3;->g:Lcom/facebook/resources/ui/FbButton;

    .line 1668678
    iget-object v0, p0, LX/AO3;->g:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AO0;

    invoke-direct {v1, p0}, LX/AO0;-><init>(LX/AO3;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1668679
    iget-object v0, p0, LX/AO3;->f:Landroid/view/View;

    const v1, 0x7f0d088b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AO3;->h:Lcom/facebook/resources/ui/FbButton;

    .line 1668680
    iget-object v0, p0, LX/AO3;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AO1;

    invoke-direct {v1, p0}, LX/AO1;-><init>(LX/AO3;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1668681
    :cond_0
    iget-object v0, p0, LX/AO3;->f:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 4

    .prologue
    .line 1668682
    iget-object v0, p0, LX/AO3;->e:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a(Landroid/view/View$OnTouchListener;)V

    .line 1668683
    iget-object v0, p0, LX/AO3;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AO3;->l:LX/AOM;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668684
    new-instance v0, LX/7S9;

    sget-object v1, LX/7S8;->VIEW_INIT:LX/7S8;

    invoke-virtual {p1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v0, v1, v2, v3}, LX/7S9;-><init>(LX/7S8;FF)V

    .line 1668685
    iget-object v1, p0, LX/AO3;->a:LX/6Jt;

    new-instance v2, LX/7Sg;

    invoke-direct {v2, v0}, LX/7Sg;-><init>(LX/7S9;)V

    iget-object v0, p0, LX/AO3;->j:LX/7SB;

    invoke-virtual {v1, v2, v0}, LX/6Jt;->a(LX/7Sb;LX/6Jv;)V

    .line 1668686
    return-void
.end method

.method public final b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668687
    iget-object v0, p0, LX/AO3;->e:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->b(Landroid/view/View$OnTouchListener;)V

    .line 1668688
    iget-object v0, p0, LX/AO3;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668689
    return-void
.end method
