.class public LX/8sE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1409834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409835
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;LX/3iM;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 9
    .param p4    # Lcom/facebook/ipc/media/MediaItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/ipc/media/StickerItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/media/MediaItem;",
            "Lcom/facebook/ipc/media/StickerItem;",
            "LX/3iM;",
            ")",
            "Lcom/facebook/graphql/model/GraphQLComment;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1409836
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1409837
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1409838
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1409839
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1409840
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1409841
    invoke-static {p1, p2, v2, v3}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1409842
    if-eqz p4, :cond_0

    .line 1409843
    sget-object v3, LX/8sD;->a:[I

    invoke-virtual {p4}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v4

    invoke-virtual {v4}, LX/4gF;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1409844
    :cond_0
    :goto_0
    if-eqz p5, :cond_1

    .line 1409845
    iget-object v0, p5, Lcom/facebook/ipc/media/StickerItem;->a:Lcom/facebook/stickers/model/Sticker;

    move-object v0, v0

    .line 1409846
    new-instance v3, LX/4XB;

    invoke-direct {v3}, LX/4XB;-><init>()V

    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    .line 1409847
    iput-object v4, v3, LX/4XB;->T:Ljava/lang/String;

    .line 1409848
    move-object v3, v3

    .line 1409849
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1409850
    iput-object v0, v4, LX/2dc;->h:Ljava/lang/String;

    .line 1409851
    move-object v0, v4

    .line 1409852
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1409853
    iput-object v0, v3, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1409854
    move-object v0, v3

    .line 1409855
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, -0xd725ee3

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1409856
    iput-object v3, v0, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1409857
    move-object v0, v0

    .line 1409858
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1409859
    new-instance v3, LX/39x;

    invoke-direct {v3}, LX/39x;-><init>()V

    .line 1409860
    iput-object v0, v3, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1409861
    move-object v0, v3

    .line 1409862
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 1409863
    iput-object v3, v0, LX/39x;->p:LX/0Px;

    .line 1409864
    move-object v0, v0

    .line 1409865
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1409866
    :cond_1
    new-instance v3, LX/4Vu;

    invoke-direct {v3}, LX/4Vu;-><init>()V

    .line 1409867
    iput-object p0, v3, LX/4Vu;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1409868
    move-object v3, v3

    .line 1409869
    iput-object v2, v3, LX/4Vu;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1409870
    move-object v2, v3

    .line 1409871
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1409872
    iput-wide v4, v2, LX/4Vu;->n:J

    .line 1409873
    move-object v2, v2

    .line 1409874
    iput-object v1, v2, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1409875
    move-object v1, v2

    .line 1409876
    iput-object p3, v1, LX/4Vu;->B:Ljava/lang/String;

    .line 1409877
    move-object v1, v1

    .line 1409878
    iput-object v0, v1, LX/4Vu;->d:LX/0Px;

    .line 1409879
    move-object v0, v1

    .line 1409880
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1409881
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {p6, v1, v2}, LX/3iM;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1409882
    return-object v0

    .line 1409883
    :pswitch_0
    new-instance v3, Ljava/io/File;

    invoke-virtual {p4}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 1409884
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1409885
    iput-object v3, v4, LX/2dc;->h:Ljava/lang/String;

    .line 1409886
    move-object v3, v4

    .line 1409887
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1409888
    new-instance v4, LX/4XB;

    invoke-direct {v4}, LX/4XB;-><init>()V

    .line 1409889
    iput-object v3, v4, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1409890
    move-object v3, v4

    .line 1409891
    invoke-virtual {p4}, Lcom/facebook/ipc/media/MediaItem;->o()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 1409892
    :goto_1
    iput-object v0, v3, LX/4XB;->T:Ljava/lang/String;

    .line 1409893
    move-object v0, v3

    .line 1409894
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1409895
    new-instance v3, LX/39x;

    invoke-direct {v3}, LX/39x;-><init>()V

    .line 1409896
    iput-object v0, v3, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1409897
    move-object v0, v3

    .line 1409898
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 1409899
    iput-object v3, v0, LX/39x;->p:LX/0Px;

    .line 1409900
    move-object v0, v0

    .line 1409901
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 1409902
    :cond_2
    invoke-virtual {p4}, Lcom/facebook/ipc/media/MediaItem;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
