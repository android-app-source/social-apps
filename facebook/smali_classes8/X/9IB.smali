.class public LX/9IB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1zC;

.field public final b:LX/1nu;

.field public final c:LX/1vg;


# direct methods
.method public constructor <init>(LX/1zC;LX/1nu;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462637
    iput-object p1, p0, LX/9IB;->a:LX/1zC;

    .line 1462638
    iput-object p2, p0, LX/9IB;->b:LX/1nu;

    .line 1462639
    iput-object p3, p0, LX/9IB;->c:LX/1vg;

    .line 1462640
    return-void
.end method

.method public static a(LX/0QB;)LX/9IB;
    .locals 6

    .prologue
    .line 1462641
    const-class v1, LX/9IB;

    monitor-enter v1

    .line 1462642
    :try_start_0
    sget-object v0, LX/9IB;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462643
    sput-object v2, LX/9IB;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462644
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462645
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462646
    new-instance p0, LX/9IB;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v3

    check-cast v3, LX/1zC;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-direct {p0, v3, v4, v5}, LX/9IB;-><init>(LX/1zC;LX/1nu;LX/1vg;)V

    .line 1462647
    move-object v0, p0

    .line 1462648
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462649
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462650
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
