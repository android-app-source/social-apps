.class public final LX/9mn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/react/ReactRootView;

.field private final b:Landroid/graphics/Rect;

.field private final c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/facebook/react/ReactRootView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1536086
    iput-object p1, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1536087
    iput v0, p0, LX/9mn;->d:I

    .line 1536088
    iput v0, p0, LX/9mn;->e:I

    .line 1536089
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9mn;->b:Landroid/graphics/Rect;

    .line 1536090
    const/high16 v0, 0x42700000    # 60.0f

    invoke-static {v0}, LX/5r2;->a(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/9mn;->c:I

    .line 1536091
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1536069
    iget-object v0, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    invoke-virtual {v0}, Lcom/facebook/react/ReactRootView;->getRootView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LX/9mn;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1536070
    sget-object v0, LX/5ql;->a:Landroid/util/DisplayMetrics;

    move-object v0, v0

    .line 1536071
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v1, p0, LX/9mn;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    .line 1536072
    iget v1, p0, LX/9mn;->d:I

    if-eq v1, v0, :cond_1

    iget v1, p0, LX/9mn;->c:I

    if-le v0, v1, :cond_1

    .line 1536073
    iput v0, p0, LX/9mn;->d:I

    .line 1536074
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 1536075
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 1536076
    const-string v2, "screenY"

    iget-object v3, p0, LX/9mn;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536077
    const-string v2, "screenX"

    iget-object v3, p0, LX/9mn;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536078
    const-string v2, "width"

    iget-object v3, p0, LX/9mn;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536079
    const-string v2, "height"

    iget v3, p0, LX/9mn;->d:I

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536080
    const-string v2, "endCoordinates"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 1536081
    const-string v1, "keyboardDidShow"

    invoke-direct {p0, v1, v0}, LX/9mn;->a(Ljava/lang/String;LX/5pH;)V

    .line 1536082
    :cond_0
    :goto_0
    return-void

    .line 1536083
    :cond_1
    iget v1, p0, LX/9mn;->d:I

    if-eqz v1, :cond_0

    iget v1, p0, LX/9mn;->c:I

    if-gt v0, v1, :cond_0

    .line 1536084
    const/4 v0, 0x0

    iput v0, p0, LX/9mn;->d:I

    .line 1536085
    const-string v0, "keyboardDidHide"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/9mn;->a(Ljava/lang/String;LX/5pH;)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1536050
    const/4 v3, 0x0

    .line 1536051
    packed-switch p1, :pswitch_data_0

    .line 1536052
    :goto_0
    return-void

    .line 1536053
    :pswitch_0
    const-string v2, "portrait-primary"

    .line 1536054
    const-wide/16 v0, 0x0

    .line 1536055
    :goto_1
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v4

    .line 1536056
    const-string v5, "name"

    invoke-interface {v4, v5, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536057
    const-string v2, "rotationDegrees"

    invoke-interface {v4, v2, v0, v1}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536058
    const-string v0, "isLandscape"

    invoke-interface {v4, v0, v3}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 1536059
    const-string v0, "namedOrientationDidChange"

    invoke-direct {p0, v0, v4}, LX/9mn;->a(Ljava/lang/String;LX/5pH;)V

    goto :goto_0

    .line 1536060
    :pswitch_1
    const-string v2, "landscape-primary"

    .line 1536061
    const-wide v0, -0x3fa9800000000000L    # -90.0

    move v3, v4

    .line 1536062
    goto :goto_1

    .line 1536063
    :pswitch_2
    const-string v2, "portrait-secondary"

    .line 1536064
    const-wide v0, 0x4066800000000000L    # 180.0

    .line 1536065
    goto :goto_1

    .line 1536066
    :pswitch_3
    const-string v2, "landscape-secondary"

    .line 1536067
    const-wide v0, 0x4056800000000000L    # 90.0

    move v3, v4

    .line 1536068
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/String;LX/5pH;)V
    .locals 2
    .param p2    # LX/5pH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1536092
    iget-object v0, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    iget-object v0, v0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    if-eqz v0, :cond_0

    .line 1536093
    iget-object v0, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    iget-object v0, v0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1536094
    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1536043
    iget-object v0, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    invoke-virtual {v0}, Lcom/facebook/react/ReactRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 1536044
    iget v1, p0, LX/9mn;->e:I

    if-ne v1, v0, :cond_0

    .line 1536045
    :goto_0
    return-void

    .line 1536046
    :cond_0
    iput v0, p0, LX/9mn;->e:I

    .line 1536047
    iget-object v1, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    invoke-virtual {v1}, Lcom/facebook/react/ReactRootView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/5ql;->b(Landroid/content/Context;)V

    .line 1536048
    invoke-direct {p0}, LX/9mn;->c()V

    .line 1536049
    invoke-direct {p0, v0}, LX/9mn;->a(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 1536024
    sget-object v0, LX/5ql;->a:Landroid/util/DisplayMetrics;

    move-object v0, v0

    .line 1536025
    sget-object v1, LX/5ql;->b:Landroid/util/DisplayMetrics;

    move-object v1, v1

    .line 1536026
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v2

    .line 1536027
    const-string v3, "width"

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-interface {v2, v3, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 1536028
    const-string v3, "height"

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-interface {v2, v3, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 1536029
    const-string v3, "scale"

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v4

    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536030
    const-string v3, "fontScale"

    iget v4, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    float-to-double v4, v4

    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536031
    const-string v3, "densityDpi"

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v4, v0

    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536032
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 1536033
    const-string v3, "width"

    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-interface {v0, v3, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 1536034
    const-string v3, "height"

    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-interface {v0, v3, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 1536035
    const-string v3, "scale"

    iget v4, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v4

    invoke-interface {v0, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536036
    const-string v3, "fontScale"

    iget v4, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    float-to-double v4, v4

    invoke-interface {v0, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536037
    const-string v3, "densityDpi"

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v4, v1

    invoke-interface {v0, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1536038
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 1536039
    const-string v3, "windowPhysicalPixels"

    invoke-interface {v1, v3, v2}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 1536040
    const-string v2, "screenPhysicalPixels"

    invoke-interface {v1, v2, v0}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 1536041
    const-string v0, "didUpdateDimensions"

    invoke-direct {p0, v0, v1}, LX/9mn;->a(Ljava/lang/String;LX/5pH;)V

    .line 1536042
    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 1536020
    iget-object v0, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    iget-object v0, v0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    iget-boolean v0, v0, Lcom/facebook/react/ReactRootView;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9mn;->a:Lcom/facebook/react/ReactRootView;

    iget-object v0, v0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1536021
    :cond_0
    :goto_0
    return-void

    .line 1536022
    :cond_1
    invoke-direct {p0}, LX/9mn;->a()V

    .line 1536023
    invoke-direct {p0}, LX/9mn;->b()V

    goto :goto_0
.end method
