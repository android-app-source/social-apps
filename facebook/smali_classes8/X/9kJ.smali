.class public LX/9kJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Long;",
        "Lcom/facebook/places/pagetopics/FetchPageTopicsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/Locale;

.field private c:LX/0lp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1531092
    const-class v0, LX/9kJ;

    sput-object v0, LX/9kJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1531088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1531089
    iput-object p1, p0, LX/9kJ;->b:Ljava/util/Locale;

    .line 1531090
    iput-object p2, p0, LX/9kJ;->c:LX/0lp;

    .line 1531091
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1531053
    check-cast p1, Ljava/lang/Long;

    .line 1531054
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1531055
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "locale"

    iget-object v3, p0, LX/9kJ;->b:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1531056
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "type"

    const-string v3, "placetopic"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1531057
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "topic_filter"

    const-string v3, "all"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1531058
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fields"

    .line 1531059
    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-virtual {v3}, LX/0mC;->b()LX/162;

    move-result-object v3

    .line 1531060
    const-string p0, "id"

    invoke-virtual {v3, p0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1531061
    const-string p0, "parent_ids"

    invoke-virtual {v3, p0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1531062
    const-string p0, "name"

    invoke-virtual {v3, p0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1531063
    const-string p0, "count"

    invoke-virtual {v3, p0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1531064
    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1531065
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1531066
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "topics_version"

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1531067
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "FetchPageTopics"

    .line 1531068
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1531069
    move-object v1, v1

    .line 1531070
    const-string v2, "GET"

    .line 1531071
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1531072
    move-object v1, v1

    .line 1531073
    const-string v2, "search"

    .line 1531074
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1531075
    move-object v1, v1

    .line 1531076
    sget-object v2, LX/14S;->STRING:LX/14S;

    .line 1531077
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1531078
    move-object v1, v1

    .line 1531079
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1531080
    move-object v0, v1

    .line 1531081
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1531082
    iget-object v0, p0, LX/9kJ;->c:LX/0lp;

    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 1531083
    const-class v1, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;

    .line 1531084
    iget-object v1, p0, LX/9kJ;->b:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->a(Ljava/lang/String;)V

    .line 1531085
    invoke-virtual {v0}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->b()Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1531086
    return-object v0

    .line 1531087
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
