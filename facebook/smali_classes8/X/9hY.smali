.class public final LX/9hY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/concurrent/Future",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/175;

.field public final synthetic c:LX/9hh;


# direct methods
.method public constructor <init>(LX/9hh;Ljava/lang/String;LX/175;)V
    .locals 0

    .prologue
    .line 1526743
    iput-object p1, p0, LX/9hY;->c:LX/9hh;

    iput-object p2, p0, LX/9hY;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9hY;->b:LX/175;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1526744
    iget-object v0, p0, LX/9hY;->c:LX/9hh;

    iget-object v0, v0, LX/9hh;->e:LX/9fy;

    iget-object v1, p0, LX/9hY;->a:Ljava/lang/String;

    iget-object v2, p0, LX/9hY;->b:LX/175;

    .line 1526745
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1526746
    new-instance v4, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;

    invoke-direct {v4, v1, v2}, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;-><init>(Ljava/lang/String;LX/175;)V

    .line 1526747
    const-string v5, "editPhotoCaptionParams"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1526748
    iget-object v4, v0, LX/9fy;->a:LX/0aG;

    const-string v5, "edit_photo_caption"

    const p0, 0xf5e2766

    invoke-static {v4, v5, v3, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    move-object v0, v3

    .line 1526749
    return-object v0
.end method
