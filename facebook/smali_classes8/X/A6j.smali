.class public final LX/A6j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/A6k;


# direct methods
.method public constructor <init>(LX/A6k;)V
    .locals 0

    .prologue
    .line 1624749
    iput-object p1, p0, LX/A6j;->a:LX/A6k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1624738
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A7C;

    .line 1624739
    iget-object v1, p0, LX/A6j;->a:LX/A6k;

    iget-object v1, v1, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    iget-object v2, v0, LX/A7C;->a:Ljava/lang/String;

    iget-object v3, p0, LX/A6j;->a:LX/A6k;

    iget-object v3, v3, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v3, v3, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    iget-object v4, p0, LX/A6j;->a:LX/A6k;

    iget-object v4, v4, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v4, v4, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624740
    iget p1, v4, LX/A6e;->h:I

    move v4, p1

    .line 1624741
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 1624742
    const-string p2, "algorithm"

    invoke-virtual {v3}, LX/A6N;->name()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624743
    const-string p2, "character"

    invoke-interface {p1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624744
    const-string p2, "version"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624745
    sget-object p2, LX/A6W;->HELP_USED:LX/A6W;

    iget-object p2, p2, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {v1, p2, p1}, LX/A6X;->a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V

    .line 1624746
    iget-object v1, p0, LX/A6j;->a:LX/A6k;

    iget-object v1, v1, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->a:Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1624747
    iget-object v1, p0, LX/A6j;->a:LX/A6k;

    iget-object v1, v1, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, LX/A7C;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/transliteration/TransliterationFragment;->c(Lcom/facebook/transliteration/TransliterationFragment;Ljava/lang/String;)V

    .line 1624748
    return-void
.end method
