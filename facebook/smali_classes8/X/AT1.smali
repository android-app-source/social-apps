.class public LX/AT1;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3II;
.implements LX/7DM;


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:F

.field public F:F

.field public G:F

.field public H:I

.field private final I:Landroid/view/TextureView$SurfaceTextureListener;

.field public a:LX/1xG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/ATL;

.field public final d:Ljava/lang/String;

.field private final e:Z

.field public f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

.field private g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public h:Landroid/view/View;

.field private i:Lcom/facebook/widget/FbImageView;

.field public j:Landroid/widget/ImageView;

.field public k:Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Landroid/view/View;

.field private n:Lcom/facebook/fbui/glyph/GlyphView;

.field private o:Landroid/view/View;

.field private p:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:Lcom/facebook/composer/attachments/ComposerAttachment;

.field public r:Lcom/facebook/common/callercontext/CallerContext;

.field private s:LX/ASz;

.field private t:LX/3IC;

.field private u:Landroid/view/GestureDetector;

.field public v:Landroid/animation/AnimatorSet;

.field public w:LX/3IP;

.field private x:Landroid/animation/ValueAnimator;

.field private y:LX/ASx;

.field public z:LX/ASy;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/ATL;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const-wide/16 v2, 0x3e8

    const/4 v6, 0x0

    .line 1674907
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1674908
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AT1;->A:Z

    .line 1674909
    iput-boolean v6, p0, LX/AT1;->B:Z

    .line 1674910
    iput-boolean v6, p0, LX/AT1;->C:Z

    .line 1674911
    iput-boolean v6, p0, LX/AT1;->D:Z

    .line 1674912
    iput v6, p0, LX/AT1;->H:I

    .line 1674913
    new-instance v0, LX/ASt;

    invoke-direct {v0, p0}, LX/ASt;-><init>(LX/AT1;)V

    iput-object v0, p0, LX/AT1;->I:Landroid/view/TextureView$SurfaceTextureListener;

    .line 1674914
    const-class v0, LX/AT1;

    invoke-static {v0, p0}, LX/AT1;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1674915
    iput-object p2, p0, LX/AT1;->c:LX/ATL;

    .line 1674916
    iput-object p3, p0, LX/AT1;->d:Ljava/lang/String;

    .line 1674917
    iput-boolean p4, p0, LX/AT1;->e:Z

    .line 1674918
    const v0, 0x7f03156b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1674919
    const v0, 0x7f0d2d43

    invoke-virtual {p0, v0}, LX/AT1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iput-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1674920
    const v0, 0x7f0d3030

    invoke-virtual {p0, v0}, LX/AT1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;

    iput-object v0, p0, LX/AT1;->k:Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;

    .line 1674921
    const v0, 0x7f0d3031

    invoke-virtual {p0, v0}, LX/AT1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AT1;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1674922
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AT1;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1674923
    const v0, 0x7f0d0a7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/AT1;->j:Landroid/widget/ImageView;

    .line 1674924
    const v0, 0x7f0d3035

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AT1;->h:Landroid/view/View;

    .line 1674925
    const v0, 0x7f0d3036

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/AT1;->i:Lcom/facebook/widget/FbImageView;

    .line 1674926
    const v0, 0x7f0d3032

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AT1;->m:Landroid/view/View;

    .line 1674927
    iget-object v0, p0, LX/AT1;->m:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1674928
    const v0, 0x7f0d3033

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AT1;->n:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1674929
    const v0, 0x7f0d3034

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AT1;->o:Landroid/view/View;

    .line 1674930
    iget-object v0, p0, LX/AT1;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1674931
    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v1, p0, LX/AT1;->I:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1674932
    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, v6}, LX/2qW;->setTiltInteractionEnabled(Z)V

    .line 1674933
    iget-object v0, p0, LX/AT1;->h:Landroid/view/View;

    new-instance v1, LX/ASu;

    invoke-direct {v1, p0}, LX/ASu;-><init>(LX/AT1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1674934
    new-instance v0, LX/ASz;

    invoke-direct {v0, p0}, LX/ASz;-><init>(LX/AT1;)V

    iput-object v0, p0, LX/AT1;->s:LX/ASz;

    .line 1674935
    new-instance v0, LX/3IC;

    invoke-virtual {p0}, LX/AT1;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, LX/AT1;->s:LX/ASz;

    iget-object v5, p0, LX/AT1;->s:LX/ASz;

    invoke-direct {v0, v1, v4, v5}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, LX/AT1;->t:LX/3IC;

    .line 1674936
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/AT1;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v4, LX/AT0;

    invoke-direct {v4}, LX/AT0;-><init>()V

    invoke-direct {v0, v1, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/AT1;->u:Landroid/view/GestureDetector;

    .line 1674937
    new-instance v0, LX/ASy;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, LX/ASy;-><init>(LX/AT1;JJ)V

    iput-object v0, p0, LX/AT1;->z:LX/ASy;

    .line 1674938
    new-instance v0, LX/ASx;

    invoke-direct {v0, p0}, LX/ASx;-><init>(LX/AT1;)V

    iput-object v0, p0, LX/AT1;->y:LX/ASx;

    .line 1674939
    new-instance v0, LX/3IP;

    invoke-direct {v0}, LX/3IP;-><init>()V

    iput-object v0, p0, LX/AT1;->w:LX/3IP;

    .line 1674940
    invoke-virtual {p0}, LX/AT1;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1674941
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, LX/AT1;->H:I

    .line 1674942
    return-void
.end method

.method private static a(LX/AT1;F)F
    .locals 1

    .prologue
    .line 1674906
    invoke-virtual {p0}, LX/AT1;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    return v0
.end method

.method public static a(LX/AT1;FF)V
    .locals 4

    .prologue
    .line 1674898
    iget-object v0, p0, LX/AT1;->x:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1674899
    iget-object v0, p0, LX/AT1;->x:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1674900
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/AT1;->x:Landroid/animation/ValueAnimator;

    .line 1674901
    iget-object v0, p0, LX/AT1;->x:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1674902
    iget-object v0, p0, LX/AT1;->x:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1674903
    iget-object v0, p0, LX/AT1;->x:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/AT1;->y:LX/ASx;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1674904
    iget-object v0, p0, LX/AT1;->x:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1674905
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AT1;

    invoke-static {p0}, LX/1xG;->b(LX/0QB;)LX/1xG;

    move-result-object v1

    check-cast v1, LX/1xG;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object p0

    check-cast p0, LX/1HI;

    iput-object v1, p1, LX/AT1;->a:LX/1xG;

    iput-object p0, p1, LX/AT1;->b:LX/1HI;

    return-void
.end method

.method public static i(LX/AT1;)V
    .locals 2

    .prologue
    .line 1674893
    iget-boolean v0, p0, LX/AT1;->B:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_1

    .line 1674894
    :cond_0
    :goto_0
    return-void

    .line 1674895
    :cond_1
    iget-object v0, p0, LX/AT1;->m:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1674896
    iget-object v0, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1674897
    iget-object v0, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method public static k(LX/AT1;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x7d0

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x3

    .line 1674872
    iget-object v0, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 1674873
    :goto_0
    return-void

    .line 1674874
    :cond_0
    iget-object v0, p0, LX/AT1;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    .line 1674875
    iget-object v1, p0, LX/AT1;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    .line 1674876
    const/high16 v2, 0x41c80000    # 25.0f

    invoke-static {p0, v2}, LX/AT1;->a(LX/AT1;F)F

    move-result v2

    .line 1674877
    const/high16 v3, 0x420c0000    # 35.0f

    invoke-static {p0, v3}, LX/AT1;->a(LX/AT1;F)F

    move-result v3

    .line 1674878
    const-string v4, "x"

    new-array v5, v8, [F

    sub-float v6, v0, v3

    aput v6, v5, v9

    aput v0, v5, v10

    add-float v6, v0, v3

    aput v6, v5, v11

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 1674879
    const-string v5, "x"

    new-array v6, v8, [F

    add-float v7, v0, v3

    aput v7, v6, v9

    aput v0, v6, v10

    sub-float/2addr v0, v3

    aput v0, v6, v11

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 1674880
    const-string v5, "y"

    new-array v6, v8, [F

    sub-float v7, v1, v2

    aput v7, v6, v9

    sub-float v3, v1, v3

    aput v3, v6, v10

    sub-float/2addr v1, v2

    aput v1, v6, v11

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 1674881
    const-string v2, "rotation"

    new-array v3, v8, [F

    fill-array-data v3, :array_0

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 1674882
    const-string v3, "rotation"

    new-array v5, v8, [F

    fill-array-data v5, :array_1

    invoke-static {v3, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 1674883
    iget-object v5, p0, LX/AT1;->n:Lcom/facebook/fbui/glyph/GlyphView;

    new-array v6, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v4, v6, v9

    aput-object v1, v6, v10

    aput-object v2, v6, v11

    invoke-static {v5, v6}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1674884
    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1674885
    invoke-virtual {v2, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1674886
    iget-object v4, p0, LX/AT1;->n:Lcom/facebook/fbui/glyph/GlyphView;

    new-array v5, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v5, v9

    aput-object v1, v5, v10

    aput-object v3, v5, v11

    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1674887
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1674888
    invoke-virtual {v0, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1674889
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    .line 1674890
    iget-object v1, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    new-array v3, v11, [Landroid/animation/Animator;

    aput-object v2, v3, v9

    aput-object v0, v3, v10

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1674891
    iget-object v0, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    new-instance v1, LX/ASv;

    invoke-direct {v1, p0}, LX/ASv;-><init>(LX/AT1;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        -0x3dcc0000    # -45.0f
        0x0
        0x42340000    # 45.0f
    .end array-data

    .line 1674892
    :array_1
    .array-data 4
        0x42340000    # 45.0f
        0x0
        -0x3dcc0000    # -45.0f
    .end array-data
.end method

.method public static n(LX/AT1;)V
    .locals 15

    .prologue
    .line 1674805
    iget-object v0, p0, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1674806
    :cond_0
    :goto_0
    return-void

    .line 1674807
    :cond_1
    iget-object v0, p0, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1674808
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v0, v1

    .line 1674809
    if-eqz v0, :cond_3

    .line 1674810
    const/high16 v14, 0x43b40000    # 360.0f

    const/high16 v13, 0x43340000    # 180.0f

    const/high16 v12, 0x40000000    # 2.0f

    .line 1674811
    iget v5, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    move v5, v5

    .line 1674812
    int-to-float v5, v5

    .line 1674813
    iget v6, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoHeightPixels:I

    move v6, v6

    .line 1674814
    int-to-float v6, v6

    .line 1674815
    iget v7, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageWidthPixels:I

    move v7, v7

    .line 1674816
    int-to-float v7, v7

    .line 1674817
    iget v8, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageHeightPixels:I

    move v8, v8

    .line 1674818
    int-to-float v8, v8

    .line 1674819
    iget v9, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaLeftPixels:I

    move v9, v9

    .line 1674820
    int-to-float v9, v9

    .line 1674821
    iget v10, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaTopPixels:I

    move v10, v10

    .line 1674822
    int-to-float v10, v10

    .line 1674823
    div-float/2addr v7, v5

    mul-float/2addr v7, v14

    .line 1674824
    div-float v11, v5, v12

    sub-float v9, v11, v9

    mul-float/2addr v9, v14

    div-float v5, v9, v5

    .line 1674825
    sub-float/2addr v7, v5

    .line 1674826
    div-float/2addr v8, v6

    mul-float/2addr v8, v13

    .line 1674827
    div-float v9, v6, v12

    sub-float/2addr v9, v10

    mul-float/2addr v9, v13

    div-float v6, v9, v6

    .line 1674828
    sub-float/2addr v8, v6

    .line 1674829
    new-instance v9, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-direct {v9, v5, v7, v6, v8}, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;-><init>(FFFF)V

    move-object v5, v9

    .line 1674830
    iget v6, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageHeightPixels:I

    move v6, v6

    .line 1674831
    int-to-float v6, v6

    .line 1674832
    iget v7, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoHeightPixels:I

    move v7, v7

    .line 1674833
    int-to-float v7, v7

    div-float/2addr v6, v7

    const/high16 v7, 0x43340000    # 180.0f

    mul-float/2addr v6, v7

    .line 1674834
    const/high16 v7, 0x42820000    # 65.0f

    invoke-static {v7, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, p0, LX/AT1;->G:F

    .line 1674835
    iget v6, p0, LX/AT1;->G:F

    const v7, 0x3f666666    # 0.9f

    mul-float/2addr v6, v7

    iput v6, p0, LX/AT1;->G:F

    .line 1674836
    new-instance v6, LX/7DF;

    invoke-direct {v6}, LX/7DF;-><init>()V

    .line 1674837
    invoke-virtual {v6}, LX/7DF;->b()LX/7DF;

    move-result-object v7

    .line 1674838
    iget v8, v5, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    move v8, v8

    .line 1674839
    iput v8, v7, LX/7DF;->d:F

    .line 1674840
    move-object v7, v7

    .line 1674841
    iget v8, v5, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    move v8, v8

    .line 1674842
    neg-float v8, v8

    .line 1674843
    iput v8, v7, LX/7DF;->c:F

    .line 1674844
    iget v7, v5, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    move v7, v7

    .line 1674845
    iget v8, v5, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    move v8, v8

    .line 1674846
    add-float/2addr v7, v8

    .line 1674847
    const/high16 v8, 0x43af0000    # 350.0f

    cmpg-float v7, v7, v8

    if-gez v7, :cond_2

    .line 1674848
    invoke-virtual {v6}, LX/7DF;->a()LX/7DF;

    move-result-object v7

    .line 1674849
    iget v8, v5, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    move v8, v8

    .line 1674850
    iput v8, v7, LX/7DF;->b:F

    .line 1674851
    move-object v7, v7

    .line 1674852
    iget v8, v5, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    move v8, v8

    .line 1674853
    neg-float v8, v8

    .line 1674854
    iput v8, v7, LX/7DF;->a:F

    .line 1674855
    :cond_2
    iget-object v7, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mProjectionType:Ljava/lang/String;

    move-object v7, v7

    .line 1674856
    iget-object v8, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-static {v7}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/2qW;->setProjectionType(LX/19o;)V

    .line 1674857
    iget-object v7, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v6}, LX/7DF;->c()LX/7DG;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/2qW;->setRendererBounds(LX/7DG;)V

    .line 1674858
    iget-object v6, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v6, v5}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setPanoBounds(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V

    .line 1674859
    iget-object v5, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget v6, p0, LX/AT1;->G:F

    invoke-virtual {v5, v6}, LX/2qW;->setMaxVerticalFOV(F)V

    .line 1674860
    iget-object v5, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget v6, p0, LX/AT1;->G:F

    invoke-virtual {v5, v6}, LX/2qW;->setPreferredVerticalFOV(F)V

    .line 1674861
    iget-object v5, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v5, v0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setSphericalPhotoMetadata(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V

    .line 1674862
    iget-object v5, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1674863
    iget-wide v9, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewPitchDegrees:D

    move-wide v7, v9

    .line 1674864
    double-to-float v6, v7

    .line 1674865
    iget-wide v9, v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewHeadingDegrees:D

    move-wide v7, v9

    .line 1674866
    double-to-float v7, v7

    invoke-virtual {v5, v6, v7}, LX/2qW;->a(FF)V

    .line 1674867
    :cond_3
    iget-object v0, p0, LX/AT1;->b:LX/1HI;

    iget-object v1, p0, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    new-instance v2, LX/1o9;

    const/16 v3, 0x800

    const/16 v4, 0x400

    invoke-direct {v2, v3, v4}, LX/1o9;-><init>(II)V

    .line 1674868
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 1674869
    move-object v1, v1

    .line 1674870
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    iget-object v2, p0, LX/AT1;->r:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    iput-object v0, p0, LX/AT1;->p:LX/1ca;

    .line 1674871
    iget-object v0, p0, LX/AT1;->p:LX/1ca;

    new-instance v1, LX/ASw;

    invoke-direct {v1, p0}, LX/ASw;-><init>(LX/AT1;)V

    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0
.end method

.method public static setSphericalUploadState(LX/AT1;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1674792
    iput-boolean p1, p0, LX/AT1;->A:Z

    .line 1674793
    iget-boolean v0, p0, LX/AT1;->A:Z

    if-eqz v0, :cond_1

    .line 1674794
    invoke-static {p0}, LX/AT1;->i(LX/AT1;)V

    .line 1674795
    :goto_0
    iget-object v3, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-boolean v0, p0, LX/AT1;->A:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setVisibility(I)V

    .line 1674796
    iget-object v0, p0, LX/AT1;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-boolean v3, p0, LX/AT1;->A:Z

    if-eqz v3, :cond_4

    :goto_2
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1674797
    iget-object v0, p0, LX/AT1;->i:Lcom/facebook/widget/FbImageView;

    iget-boolean v2, p0, LX/AT1;->A:Z

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setSelected(Z)V

    .line 1674798
    return-void

    .line 1674799
    :cond_1
    iget-object v0, p0, LX/AT1;->m:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1674800
    iget-object v0, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    .line 1674801
    iget-object v0, p0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1674802
    :cond_2
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1674803
    goto :goto_1

    :cond_4
    move v2, v1

    .line 1674804
    goto :goto_2
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1674791
    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1674753
    new-instance v0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentView$5;

    invoke-direct {v0, p0}, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentView$5;-><init>(LX/AT1;)V

    invoke-virtual {p0, v0}, LX/AT1;->post(Ljava/lang/Runnable;)Z

    .line 1674754
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1674790
    return-void
.end method

.method public get360TextureView()LX/2qW;
    .locals 1

    .prologue
    .line 1674789
    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    return-object v0
.end method

.method public getPitch()F
    .locals 1

    .prologue
    .line 1674788
    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->getPitch()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYaw()F
    .locals 1

    .prologue
    .line 1674787
    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->getYaw()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    .line 1674774
    iget v0, p0, LX/AT1;->F:F

    move v1, v0

    .line 1674775
    invoke-virtual {p0}, LX/AT1;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    new-instance v2, LX/ATB;

    invoke-direct {v2, p1, p2}, LX/ATB;-><init>(II)V

    invoke-static {v1, v0, v2}, LX/ATC;->a(FLandroid/widget/FrameLayout$LayoutParams;LX/ATB;)LX/ATB;

    move-result-object v0

    .line 1674776
    iget-object v1, p0, LX/AT1;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v0, LX/ATB;->a:I

    iget v4, v0, LX/ATB;->b:I

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1674777
    iget v1, v0, LX/ATB;->a:I

    .line 1674778
    iget v0, v0, LX/ATB;->b:I

    .line 1674779
    iget-boolean v2, p0, LX/AT1;->A:Z

    if-eqz v2, :cond_1

    move v0, p1

    .line 1674780
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1674781
    const/4 v2, 0x1

    const/high16 v3, 0x42a00000    # 80.0f

    invoke-virtual {p0}, LX/AT1;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1674782
    if-ge v1, v2, :cond_0

    .line 1674783
    iget-object v1, p0, LX/AT1;->j:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1674784
    :goto_1
    invoke-super {p0, v0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1674785
    return-void

    .line 1674786
    :cond_0
    iget-object v1, p0, LX/AT1;->j:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    move p1, v0

    move v0, v1

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x4232a544

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1674762
    iget-boolean v2, p0, LX/AT1;->e:Z

    if-eqz v2, :cond_0

    .line 1674763
    iget-object v2, p0, LX/AT1;->c:LX/ATL;

    iget-object v3, p0, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2, v3}, LX/ATL;->b(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1674764
    :cond_0
    iget-boolean v2, p0, LX/AT1;->A:Z

    if-eqz v2, :cond_2

    .line 1674765
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1674766
    :goto_0
    iget-object v2, p0, LX/AT1;->u:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1674767
    if-eqz v2, :cond_1

    .line 1674768
    const v2, -0x16ef2d11

    invoke-static {v2, v1}, LX/02F;->a(II)V

    .line 1674769
    :goto_1
    return v0

    .line 1674770
    :pswitch_0
    iget-object v2, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v2, v0}, LX/2qW;->a(Z)V

    goto :goto_0

    .line 1674771
    :pswitch_1
    iget-object v2, p0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v2, v0}, LX/2qW;->a(Z)V

    goto :goto_0

    .line 1674772
    :cond_1
    iget-object v0, p0, LX/AT1;->t:LX/3IC;

    invoke-virtual {v0, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x5ef4d0c1

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_1

    .line 1674773
    :cond_2
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x68f8a7e4

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1674760
    iget-object v0, p0, LX/AT1;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1674761
    return-void
.end method

.method public setScale(F)V
    .locals 0

    .prologue
    .line 1674755
    iput p1, p0, LX/AT1;->E:F

    .line 1674756
    invoke-virtual {p0, p1}, LX/AT1;->setScaleX(F)V

    .line 1674757
    invoke-virtual {p0, p1}, LX/AT1;->setScaleY(F)V

    .line 1674758
    invoke-virtual {p0, p1}, LX/AT1;->setAlpha(F)V

    .line 1674759
    return-void
.end method
