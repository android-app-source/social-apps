.class public final LX/9kQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
        "LX/0am",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;)V
    .locals 0

    .prologue
    .line 1531192
    iput-object p1, p0, LX/9kQ;->a:Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1531193
    check-cast p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    .line 1531194
    iget-object v0, p0, LX/9kQ;->a:Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    iget-object v0, v0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9kQ;->a:Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    iget-object v0, v0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    iget-object v0, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1531195
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1531196
    :goto_0
    return-object v0

    .line 1531197
    :cond_0
    new-instance v2, LX/3DI;

    const-string v0, "\u00a0\u00b7\u00a0"

    invoke-direct {v2, v0}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 1531198
    iget-object v3, p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->e:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1531199
    invoke-virtual {v2, v0}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 1531200
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1531201
    :cond_1
    invoke-virtual {v2}, LX/3DI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method
