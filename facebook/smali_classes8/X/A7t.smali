.class public LX/A7t;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/A8N;

.field public final g:LX/A7r;

.field public h:LX/4Jx;

.field public i:I

.field public j:Ljava/lang/String;

.field public k:LX/A7s;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1626802
    const-class v0, LX/A7t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/A7t;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/A8N;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/universalfeedback/annotations/IsInUniversalFeedbackGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/A8N;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1626765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1626766
    new-instance v0, LX/A7r;

    invoke-direct {v0, p0}, LX/A7r;-><init>(LX/A7t;)V

    iput-object v0, p0, LX/A7t;->g:LX/A7r;

    .line 1626767
    iput-object p1, p0, LX/A7t;->f:LX/A8N;

    .line 1626768
    iput-object p2, p0, LX/A7t;->b:LX/0tX;

    .line 1626769
    iput-object p3, p0, LX/A7t;->d:Ljava/util/concurrent/ExecutorService;

    .line 1626770
    iput-object p4, p0, LX/A7t;->e:LX/0Or;

    .line 1626771
    iput-object p5, p0, LX/A7t;->c:LX/0Or;

    .line 1626772
    return-void
.end method

.method public static b(LX/0QB;)LX/A7t;
    .locals 6

    .prologue
    .line 1626796
    new-instance v0, LX/A7t;

    .line 1626797
    new-instance v1, LX/A8N;

    invoke-direct {v1}, LX/A8N;-><init>()V

    .line 1626798
    move-object v1, v1

    .line 1626799
    move-object v1, v1

    .line 1626800
    check-cast v1, LX/A8N;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    const/16 v4, 0x15e7

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x158f

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/A7t;-><init>(LX/A8N;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;)V

    .line 1626801
    return-object v0
.end method


# virtual methods
.method public final a(LX/A7p;LX/0gc;)V
    .locals 3

    .prologue
    .line 1626773
    iget-object v0, p0, LX/A7t;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 1626774
    if-nez v0, :cond_0

    .line 1626775
    iget-object v0, p0, LX/A7t;->g:LX/A7r;

    invoke-virtual {v0}, LX/A7r;->c()V

    .line 1626776
    :goto_0
    return-void

    .line 1626777
    :cond_0
    new-instance v0, LX/4Jx;

    invoke-direct {v0}, LX/4Jx;-><init>()V

    .line 1626778
    iget-object v1, p1, LX/A7p;->a:Ljava/lang/String;

    .line 1626779
    const-string v2, "experience_type"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626780
    iget-object v1, p1, LX/A7p;->b:Ljava/lang/String;

    .line 1626781
    const-string v2, "delivery_type"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626782
    iget-object v1, p1, LX/A7p;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1626783
    iget-object v1, p1, LX/A7p;->c:Ljava/lang/String;

    .line 1626784
    const-string v2, "negative_feedback_node_token"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626785
    :cond_1
    move-object v0, v0

    .line 1626786
    iput-object v0, p0, LX/A7t;->h:LX/4Jx;

    .line 1626787
    iget-object v0, p0, LX/A7t;->f:LX/A8N;

    iget-object v1, p0, LX/A7t;->g:LX/A7r;

    .line 1626788
    iput-object v1, v0, LX/A8N;->a:LX/A7r;

    .line 1626789
    iget-object v0, p0, LX/A7t;->f:LX/A8N;

    .line 1626790
    new-instance v1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    invoke-direct {v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;-><init>()V

    iput-object v1, v0, LX/A8N;->d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    .line 1626791
    iget-object v1, v0, LX/A8N;->d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    .line 1626792
    iput-object v0, v1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;->m:LX/A8N;

    .line 1626793
    iget-object v1, v0, LX/A8N;->d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    const/4 v2, 0x2

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1626794
    iget-object v1, v0, LX/A8N;->d:Lcom/facebook/universalfeedback/ui/UniversalFeedbackDialogFragment;

    const-string v2, "UniversalFeedbackDialogFragment"

    invoke-virtual {v1, p2, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1626795
    goto :goto_0
.end method
