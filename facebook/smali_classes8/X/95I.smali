.class public LX/95I;
.super LX/2TZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "LX/95A;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 1436718
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 1436719
    const-string v0, "local_contact_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/95I;->b:I

    .line 1436720
    const-string v0, "contact_hash"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/95I;->c:I

    .line 1436721
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1436722
    new-instance v0, LX/95A;

    iget v1, p0, LX/95I;->b:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget v1, p0, LX/95I;->c:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, LX/95A;-><init>(JLjava/lang/String;)V

    return-object v0
.end method
