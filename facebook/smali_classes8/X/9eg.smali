.class public final LX/9eg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9ei;


# direct methods
.method public constructor <init>(LX/9ei;)V
    .locals 0

    .prologue
    .line 1520141
    iput-object p1, p0, LX/9eg;->a:LX/9ei;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x37fefe14

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1520142
    iget-object v1, p0, LX/9eg;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->o:Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

    .line 1520143
    iget p1, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->e:I

    add-int/lit8 p1, p1, 0x1

    iput p1, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->e:I

    .line 1520144
    iget-object v1, p0, LX/9eg;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->j:LX/9dx;

    .line 1520145
    iget-object p1, v1, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {p1}, Lcom/facebook/drawingview/DrawingView;->a()V

    .line 1520146
    const/4 p1, 0x1

    iput-boolean p1, v1, LX/9dx;->e:Z

    .line 1520147
    iget-object v1, p0, LX/9eg;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->l()V

    .line 1520148
    iget-object v1, p0, LX/9eg;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->invalidate()V

    .line 1520149
    iget-object v1, p0, LX/9eg;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520150
    iget-object v1, p0, LX/9eg;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520151
    const v1, -0x5a071bb4

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
