.class public LX/99t;
.super LX/8Hb;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LX/17S;

.field private final b:LX/17Q;

.field private final c:LX/0lB;

.field private final d:Landroid/content/res/Resources;

.field private e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

.field private f:LX/5kD;


# direct methods
.method public constructor <init>(LX/17S;LX/17Q;LX/0lB;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1447854
    invoke-direct {p0}, LX/8Hb;-><init>()V

    .line 1447855
    iput-object p1, p0, LX/99t;->a:LX/17S;

    .line 1447856
    iput-object p4, p0, LX/99t;->d:Landroid/content/res/Resources;

    .line 1447857
    iput-object p2, p0, LX/99t;->b:LX/17Q;

    .line 1447858
    iput-object p3, p0, LX/99t;->c:LX/0lB;

    .line 1447859
    return-void
.end method


# virtual methods
.method public final a(LX/5kD;Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1447860
    iput-object p1, p0, LX/99t;->f:LX/5kD;

    .line 1447861
    iget-object v1, p0, LX/99t;->f:LX/5kD;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/99t;->f:LX/5kD;

    invoke-interface {v1}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1447862
    :cond_0
    :goto_0
    return v0

    .line 1447863
    :cond_1
    iget-object v1, p0, LX/99t;->f:LX/5kD;

    invoke-interface {v1}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v1

    invoke-static {v1}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1447864
    if-eqz v1, :cond_0

    .line 1447865
    invoke-static {v1}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1447866
    iget-object v1, p0, LX/99t;->a:LX/17S;

    sget-object v2, LX/99r;->Photo:LX/99r;

    invoke-virtual {v1, v2}, LX/17S;->a(LX/99r;)Z

    move-result v1

    .line 1447867
    if-eqz v1, :cond_0

    .line 1447868
    instance-of v0, p4, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    if-eqz v0, :cond_2

    .line 1447869
    check-cast p4, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    iput-object p4, p0, LX/99t;->e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    .line 1447870
    :cond_2
    iget-object v0, p0, LX/99t;->d:Landroid/content/res/Resources;

    const v1, 0x7f020d78

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1447871
    iget-object v0, p0, LX/99t;->a:LX/17S;

    invoke-virtual {v0}, LX/17S;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1447872
    const v0, 0x7f0810dd

    invoke-virtual {p2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1447873
    :goto_1
    invoke-virtual {p2, p0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1447874
    const/4 v0, 0x1

    goto :goto_0

    .line 1447875
    :cond_3
    const v0, 0x7f0810dc

    invoke-virtual {p2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x38ed9d53

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1447876
    iget-object v0, p0, LX/99t;->f:LX/5kD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/99t;->f:LX/5kD;

    invoke-interface {v0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1447877
    :cond_0
    const v0, 0x5415ab48

    invoke-static {v3, v3, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1447878
    :goto_0
    return-void

    .line 1447879
    :cond_1
    iget-object v0, p0, LX/99t;->f:LX/5kD;

    invoke-interface {v0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1447880
    if-nez v3, :cond_2

    .line 1447881
    const v0, 0x6328c21e

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1447882
    :cond_2
    const/4 v1, 0x0

    .line 1447883
    iget-object v0, p0, LX/99t;->e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/99t;->e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    iget-object v0, v0, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1447884
    :try_start_0
    iget-object v0, p0, LX/99t;->c:LX/0lB;

    iget-object v4, p0, LX/99t;->e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    iget-object v4, v4, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1447885
    :goto_1
    iget-object v1, p0, LX/99t;->a:LX/17S;

    invoke-virtual {v1}, LX/17S;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1447886
    iget-object v1, p0, LX/99t;->a:LX/17S;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/99t;->e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    iget-object v4, v4, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;->c:Ljava/lang/String;

    sget-object v5, LX/99r;->Photo:LX/99r;

    invoke-virtual {v1, v3, v4, v5}, LX/17S;->a(Landroid/content/Context;Ljava/lang/String;LX/99r;)V

    .line 1447887
    :goto_2
    iget-object v1, p0, LX/99t;->e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/99t;->e:Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    iget-boolean v1, v1, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;->a:Z

    if-eqz v1, :cond_3

    .line 1447888
    invoke-static {v0}, LX/17Q;->q(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1447889
    iget-object v1, p0, LX/99t;->a:LX/17S;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/17S;->a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1447890
    :cond_3
    const v0, -0x67363d24

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1447891
    :catch_0
    move-exception v0

    .line 1447892
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "MPK we have a problem: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 1447893
    :cond_5
    iget-object v1, p0, LX/99t;->a:LX/17S;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, LX/99r;->Photo:LX/99r;

    invoke-virtual {v1, v4, v5, v3, v0}, LX/17S;->a(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;LX/0lF;)V

    goto :goto_2
.end method
