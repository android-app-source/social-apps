.class public final LX/8in;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/8ip;


# direct methods
.method public constructor <init>(LX/8ip;)V
    .locals 0

    .prologue
    .line 1391954
    iput-object p1, p0, LX/8in;->a:LX/8ip;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1391955
    iget-object v0, p0, LX/8in;->a:LX/8ip;

    iget-object v0, v0, LX/8ip;->f:LX/8ik;

    if-eqz v0, :cond_0

    .line 1391956
    iget-object v0, p0, LX/8in;->a:LX/8ip;

    iget-object v0, v0, LX/8ip;->f:LX/8ik;

    .line 1391957
    iget-object v2, v0, LX/8ik;->a:LX/8im;

    .line 1391958
    iget-object v3, v2, LX/8im;->f:LX/8ij;

    sget-object v4, LX/8ij;->PRESSING:LX/8ij;

    invoke-virtual {v3, v4}, LX/8ij;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1391959
    :cond_0
    :goto_0
    iget-object v0, p0, LX/8in;->a:LX/8ip;

    .line 1391960
    iput-boolean v1, v0, LX/8ip;->h:Z

    .line 1391961
    return v1

    .line 1391962
    :cond_1
    sget-object v3, LX/8ij;->PRESSING:LX/8ij;

    iput-object v3, v2, LX/8im;->f:LX/8ij;

    .line 1391963
    iget-object v3, v2, LX/8im;->d:LX/8YK;

    iget-object v4, v2, LX/8im;->b:LX/8YL;

    invoke-virtual {v3, v4}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 1391964
    iget-object v3, v2, LX/8im;->d:LX/8YK;

    const-wide v5, 0x3fe999999999999aL    # 0.8

    invoke-virtual {v3, v5, v6}, LX/8YK;->b(D)LX/8YK;

    goto :goto_0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 1391965
    iget-object v0, p0, LX/8in;->a:LX/8ip;

    iget-boolean v0, v0, LX/8ip;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8in;->a:LX/8ip;

    iget-object v0, v0, LX/8ip;->g:LX/8iq;

    if-eqz v0, :cond_0

    .line 1391966
    iget-object v0, p0, LX/8in;->a:LX/8ip;

    const/4 v1, 0x0

    .line 1391967
    iput-boolean v1, v0, LX/8ip;->h:Z

    .line 1391968
    iget-object v0, p0, LX/8in;->a:LX/8ip;

    invoke-static {v0}, LX/8ip;->b(LX/8ip;)V

    .line 1391969
    :cond_0
    return-void
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1391970
    iget-object v1, p0, LX/8in;->a:LX/8ip;

    .line 1391971
    iput-boolean v0, v1, LX/8ip;->h:Z

    .line 1391972
    iget-object v1, p0, LX/8in;->a:LX/8ip;

    .line 1391973
    iput-boolean v0, v1, LX/8ip;->i:Z

    .line 1391974
    iget-object v1, p0, LX/8in;->a:LX/8ip;

    iget-object v1, v1, LX/8ip;->f:LX/8ik;

    if-eqz v1, :cond_0

    .line 1391975
    iget-object v1, p0, LX/8in;->a:LX/8ip;

    iget-object v1, v1, LX/8ip;->f:LX/8ik;

    invoke-virtual {v1}, LX/8ik;->b()V

    .line 1391976
    :cond_0
    iget-object v1, p0, LX/8in;->a:LX/8ip;

    iget-object v1, v1, LX/8ip;->e:LX/8ir;

    if-eqz v1, :cond_1

    .line 1391977
    iget-object v0, p0, LX/8in;->a:LX/8ip;

    iget-object v0, v0, LX/8ip;->e:LX/8ir;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-virtual {v0}, LX/8ir;->a()V

    .line 1391978
    const/4 v0, 0x1

    .line 1391979
    :cond_1
    return v0
.end method
