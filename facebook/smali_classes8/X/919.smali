.class public LX/919;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/919;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430177
    iput-object p1, p0, LX/919;->a:LX/0Zb;

    .line 1430178
    return-void
.end method

.method public static a(LX/0QB;)LX/919;
    .locals 4

    .prologue
    .line 1430185
    sget-object v0, LX/919;->b:LX/919;

    if-nez v0, :cond_1

    .line 1430186
    const-class v1, LX/919;

    monitor-enter v1

    .line 1430187
    :try_start_0
    sget-object v0, LX/919;->b:LX/919;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1430188
    if-eqz v2, :cond_0

    .line 1430189
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1430190
    new-instance p0, LX/919;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/919;-><init>(LX/0Zb;)V

    .line 1430191
    move-object v0, p0

    .line 1430192
    sput-object v0, LX/919;->b:LX/919;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430193
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1430194
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1430195
    :cond_1
    sget-object v0, LX/919;->b:LX/919;

    return-object v0

    .line 1430196
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1430197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1430183
    new-instance v0, LX/8yQ;

    const-string v1, "activity_tag_picker"

    invoke-direct {v0, p0, p1, v1}, LX/8yQ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430184
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Px;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1430179
    const-string v0, "activity_picker_object_selected"

    invoke-static {v0, p1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/8yQ;->b(I)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/8yQ;->c(I)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p8}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p9}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p10}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p11}, LX/8yQ;->g(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    .line 1430180
    iget-object v1, v0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 1430181
    iget-object v1, p0, LX/919;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1430182
    return-void
.end method
