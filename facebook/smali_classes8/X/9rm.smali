.class public final LX/9rm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewEventGuestListActionFieldsModel$EventModel$EventViewerCapabilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1552844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    .line 1552845
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1552846
    iget-object v1, p0, LX/9rm;->a:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1552847
    iget-object v3, p0, LX/9rm;->b:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1552848
    iget-object v5, p0, LX/9rm;->c:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$EventModel$EventPlaceModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1552849
    iget-object v6, p0, LX/9rm;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewEventGuestListActionFieldsModel$EventModel$EventViewerCapabilityModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1552850
    iget-object v7, p0, LX/9rm;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1552851
    iget-object v8, p0, LX/9rm;->f:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1552852
    iget-object v9, p0, LX/9rm;->g:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1552853
    iget-object v10, p0, LX/9rm;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1552854
    iget-object v11, p0, LX/9rm;->i:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1552855
    const/16 v12, 0x9

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1552856
    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1552857
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1552858
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1552859
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1552860
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1552861
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1552862
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1552863
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1552864
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1552865
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1552866
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1552867
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1552868
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1552869
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1552870
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    invoke-direct {v1, v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;-><init>(LX/15i;)V

    .line 1552871
    return-object v1
.end method
