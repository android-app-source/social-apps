.class public LX/8pV;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:[Ljava/lang/String;

.field public e:LX/8pU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1405933
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1405934
    const v0, 0x7f031522

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1405935
    const v0, 0x7f0d2fb0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8pV;->b:Landroid/widget/TextView;

    .line 1405936
    const v0, 0x7f0d2fb1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    iput-object v0, p0, LX/8pV;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    .line 1405937
    const v0, 0x7f0d2fb2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8pV;->c:Landroid/widget/TextView;

    .line 1405938
    iget-object v0, p0, LX/8pV;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    new-instance p1, LX/8pT;

    invoke-direct {p1, p0}, LX/8pT;-><init>(LX/8pV;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(LX/8pS;)V

    .line 1405939
    invoke-virtual {p0}, LX/8pV;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f100038

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8pV;->d:[Ljava/lang/String;

    .line 1405940
    return-void
.end method

.method public static c(LX/8pV;I)V
    .locals 2

    .prologue
    .line 1405941
    iget-object v0, p0, LX/8pV;->c:Landroid/widget/TextView;

    iget-object v1, p0, LX/8pV;->d:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1405942
    return-void
.end method


# virtual methods
.method public getRating()I
    .locals 1

    .prologue
    .line 1405943
    iget-object v0, p0, LX/8pV;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    .line 1405944
    iget p0, v0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    move v0, p0

    .line 1405945
    return v0
.end method

.method public setRating(I)V
    .locals 1

    .prologue
    .line 1405946
    if-ltz p1, :cond_0

    const/4 v0, 0x5

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1405947
    iget-object v0, p0, LX/8pV;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->setRating(I)V

    .line 1405948
    invoke-static {p0, p1}, LX/8pV;->c(LX/8pV;I)V

    .line 1405949
    return-void

    .line 1405950
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
