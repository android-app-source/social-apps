.class public LX/8zn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8zl;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8zo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1428117
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8zn;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8zo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1428118
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1428119
    iput-object p1, p0, LX/8zn;->b:LX/0Ot;

    .line 1428120
    return-void
.end method

.method public static a(LX/0QB;)LX/8zn;
    .locals 4

    .prologue
    .line 1428121
    const-class v1, LX/8zn;

    monitor-enter v1

    .line 1428122
    :try_start_0
    sget-object v0, LX/8zn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1428123
    sput-object v2, LX/8zn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1428124
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428125
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1428126
    new-instance v3, LX/8zn;

    const/16 p0, 0x1995

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8zn;-><init>(LX/0Ot;)V

    .line 1428127
    move-object v0, v3

    .line 1428128
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1428129
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1428130
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1428131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;LX/92e;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/92e;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1428132
    const v0, -0x5b14c807

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1428133
    check-cast p2, LX/8zm;

    .line 1428134
    iget-object v0, p0, LX/8zn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8zo;

    iget-object v1, p2, LX/8zm;->a:LX/92e;

    .line 1428135
    const/4 v2, 0x0

    .line 1428136
    iget-object v3, v1, LX/92e;->a:LX/5LG;

    move-object v3, v3

    .line 1428137
    invoke-interface {v3}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1428138
    invoke-interface {v3}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1428139
    :cond_0
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p0

    iget-object p2, v0, LX/8zo;->a:LX/91M;

    invoke-virtual {p2, p1}, LX/91M;->c(LX/1De;)LX/91L;

    move-result-object p2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/91L;->a(Landroid/net/Uri;)LX/91L;

    move-result-object v2

    const p2, 0x7f0b1895

    invoke-virtual {v2, p2}, LX/91L;->i(I)LX/91L;

    move-result-object v2

    const p2, 0x7f0b1895

    invoke-virtual {v2, p2}, LX/91L;->k(I)LX/91L;

    move-result-object v2

    const/4 p2, 0x0

    invoke-virtual {v2, p2}, LX/91L;->m(I)LX/91L;

    move-result-object v2

    invoke-interface {v3}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/91L;->a(Ljava/lang/CharSequence;)LX/91L;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-interface {p0, v2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    .line 1428140
    const v3, -0x5b14c807

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v1, p0, p2

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1428141
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020307

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    const v4, 0x7f0b1898

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0720

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1428142
    return-object v0

    .line 1428143
    :cond_1
    invoke-interface {v3}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1428144
    invoke-interface {v3}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1428145
    invoke-static {}, LX/1dS;->b()V

    .line 1428146
    iget v0, p1, LX/1dQ;->b:I

    .line 1428147
    packed-switch v0, :pswitch_data_0

    .line 1428148
    :goto_0
    return-object v3

    .line 1428149
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1428150
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/92e;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1428151
    check-cast v2, LX/8zm;

    .line 1428152
    iget-object v4, p0, LX/8zn;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v4, v2, LX/8zm;->b:LX/915;

    .line 1428153
    iget-object v5, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object v5, v5, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->p:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/92D;

    invoke-virtual {v5}, LX/92D;->a()V

    .line 1428154
    iget-object v5, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-static {v5}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v5

    .line 1428155
    iget-object v6, v0, LX/92e;->a:LX/5LG;

    move-object v6, v6

    .line 1428156
    invoke-static {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->a(LX/5LG;)Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v6

    .line 1428157
    iput-object v6, v5, LX/937;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 1428158
    move-object v5, v5

    .line 1428159
    invoke-virtual {v5}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v5

    .line 1428160
    iget-object v6, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v5}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Landroid/content/Intent;

    move-result-object v6

    .line 1428161
    iget-object v5, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object v5, v5, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const/16 p1, 0xb

    iget-object p2, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-interface {v5, v6, p1, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1428162
    iget-object v5, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object v5, v5, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->o:LX/918;

    iget-object v6, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-static {v6}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v6

    .line 1428163
    iget-object p1, v6, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v6, p1

    .line 1428164
    iget-object p1, v0, LX/92e;->a:LX/5LG;

    move-object p1, p1

    .line 1428165
    invoke-interface {p1}, LX/5LG;->l()Ljava/lang/String;

    move-result-object p1

    .line 1428166
    iget-object p2, v0, LX/92e;->b:Ljava/lang/String;

    move-object p2, p2

    .line 1428167
    iget-object p0, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object p0, p0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->c:LX/8zj;

    .line 1428168
    iget-object v2, p0, LX/8zj;->a:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    move p0, v2

    .line 1428169
    iget-object v2, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->c:LX/8zj;

    invoke-virtual {v2}, LX/8zj;->e()I

    move-result v2

    iget-object v1, v4, LX/915;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->d:LX/8zs;

    .line 1428170
    iget-object v4, v1, LX/8zs;->e:LX/92o;

    invoke-virtual {v4}, LX/92o;->d()Ljava/lang/String;

    move-result-object v4

    move-object v1, v4

    .line 1428171
    const-string v4, "activities_selector_verb_selected"

    invoke-static {v4, v6}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, p1}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/8yQ;->g(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/8yQ;->c(I)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    .line 1428172
    iget-object v0, v4, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v4, v0

    .line 1428173
    iget-object v0, v5, LX/918;->a:LX/0Zb;

    invoke-interface {v0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428174
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x5b14c807
        :pswitch_0
    .end packed-switch
.end method
