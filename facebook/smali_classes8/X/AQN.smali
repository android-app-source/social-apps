.class public final LX/AQN;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V
    .locals 0

    .prologue
    .line 1671279
    iput-object p1, p0, LX/AQN;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 1671280
    iget-object v0, p0, LX/AQN;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1671281
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1671282
    const-string v1, "extra_composer_life_event_icon_model"

    iget-object v2, p0, LX/AQN;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-static {v2}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->k(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->l()LX/7l3;

    move-result-object v2

    iget-object v3, p0, LX/AQN;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v3, v3, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    invoke-virtual {v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1671283
    iput-object v3, v2, LX/7l3;->a:Ljava/lang/String;

    .line 1671284
    move-object v2, v2

    .line 1671285
    invoke-virtual {v2}, LX/7l3;->a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1671286
    iget-object v1, p0, LX/AQN;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1671287
    iget-object v0, p0, LX/AQN;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1671288
    :cond_0
    return-void
.end method
