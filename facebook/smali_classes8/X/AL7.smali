.class public LX/AL7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/WindowManager;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:I

.field public e:I

.field public f:I

.field public g:LX/AL6;

.field private h:I

.field public final i:[I

.field public j:I

.field private k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 1664617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1664618
    iput v1, p0, LX/AL7;->d:I

    .line 1664619
    iput v1, p0, LX/AL7;->e:I

    .line 1664620
    iput v1, p0, LX/AL7;->f:I

    .line 1664621
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/AL7;->i:[I

    .line 1664622
    iput v1, p0, LX/AL7;->j:I

    .line 1664623
    new-instance v0, LX/AL4;

    invoke-direct {v0, p0}, LX/AL4;-><init>(LX/AL7;)V

    iput-object v0, p0, LX/AL7;->l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1664624
    return-void
.end method

.method public static a(LX/0QB;)LX/AL7;
    .locals 1

    .prologue
    .line 1664583
    new-instance v0, LX/AL7;

    invoke-direct {v0}, LX/AL7;-><init>()V

    .line 1664584
    move-object v0, v0

    .line 1664585
    return-object v0
.end method

.method public static a(LX/AL7;I)V
    .locals 1

    .prologue
    .line 1664614
    iput p1, p0, LX/AL7;->j:I

    .line 1664615
    iget-object v0, p0, LX/AL7;->g:LX/AL6;

    invoke-interface {v0, p1}, LX/AL6;->I_(I)V

    .line 1664616
    return-void
.end method

.method public static a$redex0(LX/AL7;Landroid/app/Activity;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 1664604
    invoke-direct {p0}, LX/AL7;->c()V

    .line 1664605
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    and-int/lit16 v0, v0, 0xf0

    iput v0, p0, LX/AL7;->h:I

    .line 1664606
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, LX/AL7;->a:Landroid/view/WindowManager;

    .line 1664607
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AL7;->c:Landroid/view/View;

    .line 1664608
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x3eb

    const v4, 0x20018

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 1664609
    const/16 v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 1664610
    iput-object p2, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 1664611
    iget-object v1, p0, LX/AL7;->a:Landroid/view/WindowManager;

    iget-object v2, p0, LX/AL7;->c:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1664612
    iget-object v0, p0, LX/AL7;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AL7;->l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1664613
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1664598
    iget-object v0, p0, LX/AL7;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1664599
    :goto_0
    return-void

    .line 1664600
    :cond_0
    iget-object v0, p0, LX/AL7;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AL7;->l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1664601
    iget-object v0, p0, LX/AL7;->a:Landroid/view/WindowManager;

    iget-object v1, p0, LX/AL7;->c:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 1664602
    const/4 v0, 0x0

    iput-object v0, p0, LX/AL7;->c:Landroid/view/View;

    .line 1664603
    const/4 v0, 0x0

    iput v0, p0, LX/AL7;->h:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1664593
    iget-object v0, p0, LX/AL7;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1664594
    iget-object v0, p0, LX/AL7;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AL7;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1664595
    const/4 v0, 0x0

    iput-object v0, p0, LX/AL7;->b:Landroid/view/View;

    .line 1664596
    :cond_0
    invoke-direct {p0}, LX/AL7;->c()V

    .line 1664597
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1664586
    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p1

    :cond_0
    move-object v0, p1

    .line 1664587
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/AL7;->b:Landroid/view/View;

    .line 1664588
    iget-object v1, p0, LX/AL7;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1664589
    iget-object v1, p0, LX/AL7;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/AL7;->a$redex0(LX/AL7;Landroid/app/Activity;Landroid/os/IBinder;)V

    .line 1664590
    :goto_0
    return-void

    .line 1664591
    :cond_1
    new-instance v1, LX/AL5;

    invoke-direct {v1, p0, v0}, LX/AL5;-><init>(LX/AL7;Landroid/app/Activity;)V

    iput-object v1, p0, LX/AL7;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1664592
    iget-object v0, p0, LX/AL7;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AL7;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
