.class public final LX/9At;
.super LX/3m7;
.source ""


# instance fields
.field private e:Lcom/facebook/fbui/facepile/FacepileView;

.field private f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:F

.field private i:Landroid/graphics/Paint;

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1450739
    invoke-direct {p0}, LX/3m7;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1450740
    iget v0, p0, LX/9At;->j:I

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1450741
    iget-object v0, p0, LX/9At;->f:Ljava/lang/String;

    iget v1, p0, LX/9At;->h:F

    iget-object v2, p0, LX/9At;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1450742
    iget v0, p0, LX/9At;->k:I

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1450743
    iget-object v0, p0, LX/9At;->e:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->draw(Landroid/graphics/Canvas;)V

    .line 1450744
    return-void
.end method

.method public final a(Lcom/facebook/fbui/facepile/FacepileView;IILjava/lang/String;Ljava/lang/String;IFLandroid/graphics/Paint;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1450745
    iput-object p1, p0, LX/9At;->e:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1450746
    iget-object v1, p0, LX/9At;->e:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v3, -0x2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/fbui/facepile/FacepileView;->measure(II)V

    .line 1450747
    iget-object v1, p0, LX/9At;->e:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1}, Lcom/facebook/fbui/facepile/FacepileView;->getMeasuredWidth()I

    move-result v1

    .line 1450748
    iput p2, p0, LX/9At;->c:I

    .line 1450749
    add-int v2, p6, v1

    iput v2, p0, LX/9At;->b:I

    .line 1450750
    iput-object p4, p0, LX/9At;->f:Ljava/lang/String;

    .line 1450751
    iput-object p5, p0, LX/9At;->g:Ljava/lang/String;

    .line 1450752
    iput p7, p0, LX/9At;->h:F

    .line 1450753
    iput-object p8, p0, LX/9At;->i:Landroid/graphics/Paint;

    .line 1450754
    if-nez p9, :cond_0

    :goto_0
    iput v0, p0, LX/9At;->j:I

    .line 1450755
    if-nez p9, :cond_1

    add-int v0, p6, p3

    :goto_1
    iput v0, p0, LX/9At;->k:I

    .line 1450756
    return-void

    :cond_0
    move v0, v1

    .line 1450757
    goto :goto_0

    .line 1450758
    :cond_1
    neg-int v0, v1

    sub-int/2addr v0, p3

    goto :goto_1
.end method
