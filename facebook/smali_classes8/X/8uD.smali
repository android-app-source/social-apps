.class public LX/8uD;
.super LX/8u8;
.source ""


# static fields
.field public static d:I

.field public static e:I


# instance fields
.field private f:F

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1414574
    const/4 v0, 0x0

    sput v0, LX/8uD;->d:I

    .line 1414575
    const/4 v0, 0x1

    sput v0, LX/8uD;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1414587
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-direct {p0, v0}, LX/8uD;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414588
    return-void
.end method

.method private constructor <init>(Landroid/util/DisplayMetrics;)V
    .locals 2

    .prologue
    .line 1414582
    invoke-direct {p0, p1}, LX/8u8;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414583
    sget v0, LX/8uD;->d:I

    iput v0, p0, LX/8uD;->g:I

    .line 1414584
    const/4 v0, 0x2

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8uD;->f:F

    .line 1414585
    const v0, -0x6f6b64

    iput v0, p0, LX/8uD;->b:I

    .line 1414586
    return-void
.end method


# virtual methods
.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 4

    .prologue
    .line 1414576
    check-cast p8, Landroid/text/Spanned;

    invoke-interface {p8, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    if-eq v0, p9, :cond_0

    .line 1414577
    :goto_0
    return-void

    .line 1414578
    :cond_0
    invoke-virtual {p0, p2}, LX/8u8;->a(Landroid/graphics/Paint;)V

    .line 1414579
    iget v0, p0, LX/8uD;->g:I

    sget v1, LX/8uD;->e:I

    if-ne v0, v1, :cond_2

    .line 1414580
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, p0, LX/8uD;->h:I

    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    const-string v0, " "

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/8uD;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v1, p3

    int-to-float v2, p6

    iget-object v3, p0, LX/8u8;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 1414581
    :cond_2
    int-to-float v0, p3

    int-to-float v1, p4

    iget v2, p0, LX/8u8;->a:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    add-int v1, p5, p7

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, LX/8uD;->f:F

    iget-object v3, p0, LX/8u8;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method
