.class public final LX/8zm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8zn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/92e;

.field public b:LX/915;

.field public final synthetic c:LX/8zn;


# direct methods
.method public constructor <init>(LX/8zn;)V
    .locals 1

    .prologue
    .line 1428098
    iput-object p1, p0, LX/8zm;->c:LX/8zn;

    .line 1428099
    move-object v0, p1

    .line 1428100
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1428101
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1428102
    const-string v0, "MinutiaeVerbsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1428103
    if-ne p0, p1, :cond_1

    .line 1428104
    :cond_0
    :goto_0
    return v0

    .line 1428105
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1428106
    goto :goto_0

    .line 1428107
    :cond_3
    check-cast p1, LX/8zm;

    .line 1428108
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1428109
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1428110
    if-eq v2, v3, :cond_0

    .line 1428111
    iget-object v2, p0, LX/8zm;->a:LX/92e;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8zm;->a:LX/92e;

    iget-object v3, p1, LX/8zm;->a:LX/92e;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1428112
    goto :goto_0

    .line 1428113
    :cond_5
    iget-object v2, p1, LX/8zm;->a:LX/92e;

    if-nez v2, :cond_4

    .line 1428114
    :cond_6
    iget-object v2, p0, LX/8zm;->b:LX/915;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/8zm;->b:LX/915;

    iget-object v3, p1, LX/8zm;->b:LX/915;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1428115
    goto :goto_0

    .line 1428116
    :cond_7
    iget-object v2, p1, LX/8zm;->b:LX/915;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
