.class public LX/9nR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field private c:D

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 1537316
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LX/9nR;-><init>(Landroid/content/Context;Ljava/lang/String;DD)V

    .line 1537317
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;DD)V
    .locals 3

    .prologue
    .line 1537303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1537304
    iput-object p2, p0, LX/9nR;->b:Ljava/lang/String;

    .line 1537305
    mul-double v0, p3, p5

    iput-wide v0, p0, LX/9nR;->c:D

    .line 1537306
    invoke-direct {p0, p1}, LX/9nR;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/9nR;->a:Landroid/net/Uri;

    .line 1537307
    return-void
.end method

.method private a(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1537313
    :try_start_0
    iget-object v0, p0, LX/9nR;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1537314
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, LX/9nR;->b(Landroid/content/Context;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1537315
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    invoke-direct {p0, p1}, LX/9nR;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1537311
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9nR;->d:Z

    .line 1537312
    invoke-static {}, LX/9nS;->a()LX/9nS;

    move-result-object v0

    iget-object v1, p0, LX/9nR;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/9nS;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1537310
    iget-object v0, p0, LX/9nR;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1537309
    iget-object v0, p0, LX/9nR;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 1537308
    iget-wide v0, p0, LX/9nR;->c:D

    return-wide v0
.end method
