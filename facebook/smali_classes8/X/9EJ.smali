.class public final LX/9EJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/9EK;


# direct methods
.method public constructor <init>(LX/9EK;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1456856
    iput-object p1, p0, LX/9EJ;->b:LX/9EK;

    iput-object p2, p0, LX/9EJ;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x2

    const v0, 0xb246c43

    invoke-static {v2, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1456857
    iget-object v0, p0, LX/9EJ;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->r(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1456858
    const v0, -0x72bddeb

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1456859
    :goto_0
    return-void

    .line 1456860
    :cond_0
    iget-object v0, p0, LX/9EJ;->b:LX/9EK;

    iget-object v0, v0, LX/9EK;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v2, "tap_like_list"

    invoke-virtual {v0, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1456861
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1456862
    iget-object v2, p0, LX/9EJ;->b:LX/9EK;

    iget-object v2, v2, LX/9EK;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/9EJ;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    const-string v4, "native_permalink"

    sget-object v5, LX/8s1;->ACTIVITY_RESULT:LX/8s1;

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/8s1;)Landroid/content/Intent;

    move-result-object v2

    .line 1456863
    const-string v3, "fragment_title"

    const v4, 0x7f080fe7

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1456864
    const-string v3, "open_fragment_as_modal"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1456865
    iget-object v3, p0, LX/9EJ;->b:LX/9EK;

    iget-object v3, v3, LX/9EK;->a:Lcom/facebook/content/SecureContextHelper;

    const v4, 0xb256

    const-class v5, Landroid/app/Activity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v2, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1456866
    const v0, 0x192b0625

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
