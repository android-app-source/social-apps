.class public final LX/9g9;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1523074
    const-class v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    const v0, 0x41825906

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "MediaFetchByIds"

    const-string v6, "9eb26d99ca4ae505ff5ef2662c624ab9"

    const-string v7, "nodes"

    const-string v8, "10155211181691729"

    const/4 v9, 0x0

    .line 1523075
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1523076
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1523077
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1523078
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1523079
    sparse-switch v0, :sswitch_data_0

    .line 1523080
    :goto_0
    return-object p1

    .line 1523081
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1523082
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1523083
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1523084
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1523085
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1523086
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1523087
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1523088
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1523089
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1523090
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1523091
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1523092
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1523093
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1523094
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1523095
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1523096
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1523097
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1523098
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1523099
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1523100
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_13
        -0x680de62a -> :sswitch_b
        -0x6326fdb3 -> :sswitch_9
        -0x51484e72 -> :sswitch_5
        -0x4620c181 -> :sswitch_1
        -0x4496acc9 -> :sswitch_c
        -0x43eadc34 -> :sswitch_11
        -0x421ba035 -> :sswitch_a
        -0x3c54de38 -> :sswitch_10
        -0x2c889631 -> :sswitch_f
        -0x1b87b280 -> :sswitch_8
        -0x12efdeb3 -> :sswitch_d
        -0x93a55fc -> :sswitch_6
        0x196b8 -> :sswitch_7
        0xa1fa812 -> :sswitch_3
        0x214100e0 -> :sswitch_e
        0x3052e0ff -> :sswitch_0
        0x73a026b5 -> :sswitch_12
        0x7824fd38 -> :sswitch_4
        0x7c7626df -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1523101
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1523102
    :goto_1
    return v0

    .line 1523103
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_1
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1523104
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1523105
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1523106
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1523107
    new-instance v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueries$MediaFetchByIdsString$1;

    const-class v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueries$MediaFetchByIdsString$1;-><init>(LX/9g9;Ljava/lang/Class;)V

    return-object v0
.end method
