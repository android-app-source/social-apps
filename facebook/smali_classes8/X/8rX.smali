.class public LX/8rX;
.super LX/1Cv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/36M;",
        ">",
        "LX/1Cv;"
    }
.end annotation


# instance fields
.field private a:LX/55h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/55h",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/55h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/55h",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1408906
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1408907
    iput-object p1, p0, LX/8rX;->a:LX/55h;

    .line 1408908
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1408922
    new-instance v0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1408909
    check-cast p2, LX/36M;

    .line 1408910
    check-cast p3, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;

    .line 1408911
    iget-object v0, p0, LX/8rX;->a:LX/55h;

    .line 1408912
    iget-object p0, v0, LX/55h;->a:LX/0QK;

    invoke-interface {p0, p2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 1408913
    iget-object p1, v0, LX/55h;->b:Ljava/util/Map;

    invoke-interface {p1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1408914
    const/4 p0, -0x1

    .line 1408915
    :goto_0
    move v0, p0

    .line 1408916
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1408917
    :goto_1
    iput-boolean v0, p3, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->k:Z

    .line 1408918
    invoke-virtual {p3, p2}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->a(LX/36M;)V

    .line 1408919
    return-void

    .line 1408920
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-object p1, v0, LX/55h;->b:Ljava/util/Map;

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1408921
    iget-object v0, p0, LX/8rX;->a:LX/55h;

    invoke-virtual {v0}, LX/55h;->c()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1408904
    iget-object v0, p0, LX/8rX;->a:LX/55h;

    invoke-virtual {v0, p1}, LX/55h;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1408905
    int-to-long v0, p1

    return-wide v0
.end method
