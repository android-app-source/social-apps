.class public LX/8qa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0gw;


# direct methods
.method public constructor <init>(LX/0gw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1407452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407453
    iput-object p1, p0, LX/8qa;->a:LX/0gw;

    .line 1407454
    return-void
.end method

.method public static a(LX/8qC;LX/8qC;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8qC;",
            "LX/8qC;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1407455
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1407456
    instance-of v1, p0, LX/0hF;

    if-eqz v1, :cond_0

    .line 1407457
    check-cast p0, LX/0hF;

    invoke-interface {p0}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1407458
    :cond_0
    instance-of v1, p1, LX/0hF;

    if-eqz v1, :cond_1

    .line 1407459
    check-cast p1, LX/0hF;

    invoke-interface {p1}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1407460
    :cond_1
    return-object v0
.end method

.method public static final a(Landroid/app/Dialog;)V
    .locals 4

    .prologue
    .line 1407461
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1407462
    const/high16 v1, 0x44020000    # 520.0f

    move v1, v1

    .line 1407463
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1, v2}, LX/471;->a(Landroid/content/res/Resources;FI)I

    move-result v1

    .line 1407464
    const v2, 0x3f4ccccd    # 0.8f

    move v2, v2

    .line 1407465
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v2, v3}, LX/471;->a(Landroid/content/res/Resources;FI)I

    move-result v0

    .line 1407466
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/view/Window;->setLayout(II)V

    .line 1407467
    return-void
.end method

.method public static b(LX/0QB;)LX/8qa;
    .locals 2

    .prologue
    .line 1407468
    new-instance v1, LX/8qa;

    invoke-static {p0}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v0

    check-cast v0, LX/0gw;

    invoke-direct {v1, v0}, LX/8qa;-><init>(LX/0gw;)V

    .line 1407469
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1407470
    iget-object v0, p0, LX/8qa;->a:LX/0gw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8qa;->a:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
