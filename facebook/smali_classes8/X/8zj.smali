.class public LX/8zj;
.super LX/5Jf;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/8zn;

.field private final c:LX/915;


# direct methods
.method public constructor <init>(LX/62T;LX/915;LX/8zn;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/62T;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/915;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1428030
    invoke-direct {p0, p4, p1}, LX/5Jf;-><init>(Landroid/content/Context;LX/3wu;)V

    .line 1428031
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1428032
    iput-object v0, p0, LX/8zj;->a:LX/0Px;

    .line 1428033
    iput-object p3, p0, LX/8zj;->b:LX/8zn;

    .line 1428034
    iput-object p2, p0, LX/8zj;->c:LX/915;

    .line 1428035
    return-void
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1428036
    iget-object v0, p0, LX/8zj;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92e;

    .line 1428037
    iget-object v1, p0, LX/8zj;->b:LX/8zn;

    const/4 v2, 0x0

    .line 1428038
    new-instance v3, LX/8zm;

    invoke-direct {v3, v1}, LX/8zm;-><init>(LX/8zn;)V

    .line 1428039
    sget-object p2, LX/8zn;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/8zl;

    .line 1428040
    if-nez p2, :cond_0

    .line 1428041
    new-instance p2, LX/8zl;

    invoke-direct {p2}, LX/8zl;-><init>()V

    .line 1428042
    :cond_0
    invoke-static {p2, p1, v2, v2, v3}, LX/8zl;->a$redex0(LX/8zl;LX/1De;IILX/8zm;)V

    .line 1428043
    move-object v3, p2

    .line 1428044
    move-object v2, v3

    .line 1428045
    move-object v1, v2

    .line 1428046
    iget-object v2, v1, LX/8zl;->a:LX/8zm;

    iput-object v0, v2, LX/8zm;->a:LX/92e;

    .line 1428047
    iget-object v2, v1, LX/8zl;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1428048
    move-object v0, v1

    .line 1428049
    iget-object v1, p0, LX/8zj;->c:LX/915;

    .line 1428050
    iget-object v2, v0, LX/8zl;->a:LX/8zm;

    iput-object v1, v2, LX/8zm;->b:LX/915;

    .line 1428051
    iget-object v2, v0, LX/8zl;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1428052
    move-object v0, v0

    .line 1428053
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1428054
    const/4 v1, 0x0

    .line 1428055
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_4

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92e;

    .line 1428056
    iget-object v4, v0, LX/92e;->a:LX/5LG;

    move-object v0, v4

    .line 1428057
    if-nez v0, :cond_0

    .line 1428058
    const/4 v0, 0x1

    .line 1428059
    :goto_1
    if-nez v0, :cond_1

    .line 1428060
    :goto_2
    move-object v0, p1

    .line 1428061
    iput-object v0, p0, LX/8zj;->a:LX/0Px;

    .line 1428062
    invoke-virtual {p0}, LX/3mY;->bE_()V

    .line 1428063
    return-void

    .line 1428064
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1428065
    :cond_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1428066
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    :goto_3
    if-ge v1, v3, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92e;

    .line 1428067
    iget-object v4, v0, LX/92e;->a:LX/5LG;

    move-object v4, v4

    .line 1428068
    if-eqz v4, :cond_2

    .line 1428069
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1428070
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1428071
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1428072
    iget-object v0, p0, LX/8zj;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8zj;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1428073
    const/4 v0, 0x1

    return v0
.end method
