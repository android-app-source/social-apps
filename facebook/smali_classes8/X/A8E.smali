.class public final LX/A8E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;


# direct methods
.method public constructor <init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;)V
    .locals 0

    .prologue
    .line 1627416
    iput-object p1, p0, LX/A8E;->a:Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const v0, -0x31bfb802

    invoke-static {v4, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1627417
    check-cast p1, Landroid/widget/ImageButton;

    .line 1627418
    iget-object v1, p0, LX/A8E;->a:Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;

    iget-object v1, v1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->c:Landroid/widget/ImageButton;

    if-eq p1, v1, :cond_1

    .line 1627419
    iget-object v1, p0, LX/A8E;->a:Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;

    iget-object v1, v1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->c:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 1627420
    iget-object v1, p0, LX/A8E;->a:Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;

    iget-object v1, v1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->c:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1627421
    :cond_0
    iget-object v1, p0, LX/A8E;->a:Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;

    .line 1627422
    iput-object p1, v1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->c:Landroid/widget/ImageButton;

    .line 1627423
    invoke-virtual {p1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1627424
    iget-object v1, p0, LX/A8E;->a:Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;

    iget-object v1, v1, Lcom/facebook/universalfeedback/ui/UniversalFeedbackSatisfactionQuestionView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1627425
    :cond_1
    const v1, -0x1325af6d

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
