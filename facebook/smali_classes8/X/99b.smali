.class public LX/99b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/api/feed/data/LoadingMoreSentinel;

.field private static final b:Lcom/facebook/api/feed/data/EndOfFeedSentinel;


# instance fields
.field private final c:LX/0g1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/99g;

.field private final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1447425
    new-instance v0, Lcom/facebook/api/feed/data/LoadingMoreSentinel;

    invoke-direct {v0}, Lcom/facebook/api/feed/data/LoadingMoreSentinel;-><init>()V

    sput-object v0, LX/99b;->a:Lcom/facebook/api/feed/data/LoadingMoreSentinel;

    .line 1447426
    new-instance v0, Lcom/facebook/api/feed/data/EndOfFeedSentinel;

    invoke-direct {v0}, Lcom/facebook/api/feed/data/EndOfFeedSentinel;-><init>()V

    sput-object v0, LX/99b;->b:Lcom/facebook/api/feed/data/EndOfFeedSentinel;

    return-void
.end method

.method public constructor <init>(LX/0g1;LX/99g;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/99g;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1447434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1447435
    iput-object p1, p0, LX/99b;->c:LX/0g1;

    .line 1447436
    iput-object p2, p0, LX/99b;->d:LX/99g;

    .line 1447437
    iput-boolean p3, p0, LX/99b;->e:Z

    .line 1447438
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1447439
    iget-object v0, p0, LX/99b;->c:LX/0g1;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1447440
    iget-object v0, p0, LX/99b;->c:LX/0g1;

    invoke-interface {v0, p1}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1447441
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/99b;->d:LX/99g;

    invoke-interface {v0}, LX/99g;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/99b;->a:Lcom/facebook/api/feed/data/LoadingMoreSentinel;

    goto :goto_0

    :cond_1
    sget-object v0, LX/99b;->b:Lcom/facebook/api/feed/data/EndOfFeedSentinel;

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1447427
    iget-object v0, p0, LX/99b;->c:LX/0g1;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    .line 1447428
    if-lez v0, :cond_0

    .line 1447429
    add-int/lit8 v0, v0, 0x1

    .line 1447430
    :goto_0
    return v0

    .line 1447431
    :cond_0
    iget-boolean v0, p0, LX/99b;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/99b;->d:LX/99g;

    invoke-interface {v0}, LX/99g;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1447432
    const/4 v0, 0x1

    goto :goto_0

    .line 1447433
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
