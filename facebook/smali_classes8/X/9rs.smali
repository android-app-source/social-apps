.class public final LX/9rs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1553183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;
    .locals 15

    .prologue
    .line 1553184
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1553185
    iget-object v1, p0, LX/9rs;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1553186
    iget-object v2, p0, LX/9rs;->e:LX/0Px;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1553187
    iget-object v3, p0, LX/9rs;->f:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1553188
    iget-object v4, p0, LX/9rs;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1553189
    iget-object v5, p0, LX/9rs;->i:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1553190
    iget-object v6, p0, LX/9rs;->j:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1553191
    iget-object v7, p0, LX/9rs;->k:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1553192
    iget-object v8, p0, LX/9rs;->l:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1553193
    iget-object v9, p0, LX/9rs;->m:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1553194
    iget-object v10, p0, LX/9rs;->n:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1553195
    iget-object v11, p0, LX/9rs;->p:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1553196
    iget-object v12, p0, LX/9rs;->q:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 1553197
    iget-object v13, p0, LX/9rs;->r:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1553198
    const/16 v14, 0x12

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1553199
    const/4 v14, 0x0

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 1553200
    const/4 v1, 0x1

    iget-boolean v14, p0, LX/9rs;->b:Z

    invoke-virtual {v0, v1, v14}, LX/186;->a(IZ)V

    .line 1553201
    const/4 v1, 0x2

    iget-boolean v14, p0, LX/9rs;->c:Z

    invoke-virtual {v0, v1, v14}, LX/186;->a(IZ)V

    .line 1553202
    const/4 v1, 0x3

    iget-boolean v14, p0, LX/9rs;->d:Z

    invoke-virtual {v0, v1, v14}, LX/186;->a(IZ)V

    .line 1553203
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1553204
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1553205
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1553206
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/9rs;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1553207
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1553208
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1553209
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1553210
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1553211
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1553212
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1553213
    const/16 v1, 0xe

    iget-boolean v2, p0, LX/9rs;->o:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1553214
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1553215
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1553216
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 1553217
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1553218
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1553219
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1553220
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1553221
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1553222
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-direct {v1, v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;-><init>(LX/15i;)V

    .line 1553223
    return-object v1
.end method
