.class public LX/8wR;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/8wQ;",
        "LX/8wN;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8wR;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1422712
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1422713
    return-void
.end method

.method public static a(LX/0QB;)LX/8wR;
    .locals 3

    .prologue
    .line 1422714
    sget-object v0, LX/8wR;->a:LX/8wR;

    if-nez v0, :cond_1

    .line 1422715
    const-class v1, LX/8wR;

    monitor-enter v1

    .line 1422716
    :try_start_0
    sget-object v0, LX/8wR;->a:LX/8wR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1422717
    if-eqz v2, :cond_0

    .line 1422718
    :try_start_1
    new-instance v0, LX/8wR;

    invoke-direct {v0}, LX/8wR;-><init>()V

    .line 1422719
    move-object v0, v0

    .line 1422720
    sput-object v0, LX/8wR;->a:LX/8wR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1422721
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1422722
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1422723
    :cond_1
    sget-object v0, LX/8wR;->a:LX/8wR;

    return-object v0

    .line 1422724
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1422725
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
