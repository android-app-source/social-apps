.class public final enum LX/9c5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9c5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9c5;

.field public static final enum ACCEPTED:LX/9c5;

.field public static final enum APPLIED_FILTER:LX/9c5;

.field public static final enum CAMERA_FACING:LX/9c5;

.field public static final enum CREATIVECAM_SOURCE:LX/9c5;

.field public static final enum CROP_ADJUSTMENT_COUNT:LX/9c5;

.field public static final enum CROP_EDGE_CONTROL:LX/9c5;

.field public static final enum CROP_FINAL_ASPECT_RATIO:LX/9c5;

.field public static final enum CROP_ORIGINAL_ASPECT_RATIO:LX/9c5;

.field public static final enum CROP_PAN_CONTROL:LX/9c5;

.field public static final enum DELETED:LX/9c5;

.field public static final enum DOODLE_USED_COLOR_PICKER:LX/9c5;

.field public static final enum DURATION_MS:LX/9c5;

.field public static final enum ENABLED:LX/9c5;

.field public static final enum FILTER_NAME:LX/9c5;

.field public static final enum FINAL_SLIDER_VALUE:LX/9c5;

.field public static final enum MAX_SLIDER_VALUE:LX/9c5;

.field public static final enum MEDIA_ITEM_IDENTIFIER:LX/9c5;

.field public static final enum MIN_SLIDER_VALUE:LX/9c5;

.field public static final enum NUMBER_OF_STICKERS_ADDED:LX/9c5;

.field public static final enum NUMBER_OF_STICKERS_MOVED:LX/9c5;

.field public static final enum NUMBER_OF_STICKERS_REMOVED:LX/9c5;

.field public static final enum NUMBER_OF_STICKERS_RESIZED:LX/9c5;

.field public static final enum NUMBER_OF_STICKERS_ROTATED:LX/9c5;

.field public static final enum NUMBER_OF_TAGS_REMOVED:LX/9c5;

.field public static final enum NUM_OF_FILTER_SWIPES:LX/9c5;

.field public static final enum NUM_RESETS:LX/9c5;

.field public static final enum NUM_STROKES:LX/9c5;

.field public static final enum NUM_TEXT_ADDED:LX/9c5;

.field public static final enum NUM_TEXT_EDITED:LX/9c5;

.field public static final enum NUM_TEXT_MOVED:LX/9c5;

.field public static final enum NUM_TEXT_REMOVED:LX/9c5;

.field public static final enum NUM_TEXT_RESIZED:LX/9c5;

.field public static final enum NUM_TEXT_ROTATED:LX/9c5;

.field public static final enum NUM_UNDOS:LX/9c5;

.field public static final enum PUBLISHED:LX/9c5;

.field public static final enum ROTATION_COUNT:LX/9c5;

.field public static final enum SOURCE:LX/9c5;

.field public static final enum STICKER_ID:LX/9c5;

.field public static final enum TOGGLE_COUNT:LX/9c5;

.field public static final enum USED_COLOR_PICKER:LX/9c5;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1515844
    new-instance v0, LX/9c5;

    const-string v1, "ENABLED"

    const-string v2, "enabled"

    invoke-direct {v0, v1, v4, v2}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->ENABLED:LX/9c5;

    .line 1515845
    new-instance v0, LX/9c5;

    const-string v1, "TOGGLE_COUNT"

    const-string v2, "toggle_count"

    invoke-direct {v0, v1, v5, v2}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->TOGGLE_COUNT:LX/9c5;

    .line 1515846
    new-instance v0, LX/9c5;

    const-string v1, "MIN_SLIDER_VALUE"

    const-string v2, "min_slider_value"

    invoke-direct {v0, v1, v6, v2}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->MIN_SLIDER_VALUE:LX/9c5;

    .line 1515847
    new-instance v0, LX/9c5;

    const-string v1, "MAX_SLIDER_VALUE"

    const-string v2, "max_slider_value"

    invoke-direct {v0, v1, v7, v2}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->MAX_SLIDER_VALUE:LX/9c5;

    .line 1515848
    new-instance v0, LX/9c5;

    const-string v1, "FINAL_SLIDER_VALUE"

    const-string v2, "final_slider_value"

    invoke-direct {v0, v1, v8, v2}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->FINAL_SLIDER_VALUE:LX/9c5;

    .line 1515849
    new-instance v0, LX/9c5;

    const-string v1, "PUBLISHED"

    const/4 v2, 0x5

    const-string v3, "published"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->PUBLISHED:LX/9c5;

    .line 1515850
    new-instance v0, LX/9c5;

    const-string v1, "DELETED"

    const/4 v2, 0x6

    const-string v3, "deleted"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->DELETED:LX/9c5;

    .line 1515851
    new-instance v0, LX/9c5;

    const-string v1, "DURATION_MS"

    const/4 v2, 0x7

    const-string v3, "duration_ms"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->DURATION_MS:LX/9c5;

    .line 1515852
    new-instance v0, LX/9c5;

    const-string v1, "CROP_EDGE_CONTROL"

    const/16 v2, 0x8

    const-string v3, "used_edge_control"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->CROP_EDGE_CONTROL:LX/9c5;

    .line 1515853
    new-instance v0, LX/9c5;

    const-string v1, "CROP_PAN_CONTROL"

    const/16 v2, 0x9

    const-string v3, "used_pan_control"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->CROP_PAN_CONTROL:LX/9c5;

    .line 1515854
    new-instance v0, LX/9c5;

    const-string v1, "CROP_ADJUSTMENT_COUNT"

    const/16 v2, 0xa

    const-string v3, "crop_adjustment_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->CROP_ADJUSTMENT_COUNT:LX/9c5;

    .line 1515855
    new-instance v0, LX/9c5;

    const-string v1, "ROTATION_COUNT"

    const/16 v2, 0xb

    const-string v3, "rotation_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->ROTATION_COUNT:LX/9c5;

    .line 1515856
    new-instance v0, LX/9c5;

    const-string v1, "CROP_ORIGINAL_ASPECT_RATIO"

    const/16 v2, 0xc

    const-string v3, "original_aspect_ratio"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->CROP_ORIGINAL_ASPECT_RATIO:LX/9c5;

    .line 1515857
    new-instance v0, LX/9c5;

    const-string v1, "CROP_FINAL_ASPECT_RATIO"

    const/16 v2, 0xd

    const-string v3, "final_aspect_ratio"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->CROP_FINAL_ASPECT_RATIO:LX/9c5;

    .line 1515858
    new-instance v0, LX/9c5;

    const-string v1, "ACCEPTED"

    const/16 v2, 0xe

    const-string v3, "accepted"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->ACCEPTED:LX/9c5;

    .line 1515859
    new-instance v0, LX/9c5;

    const-string v1, "NUMBER_OF_TAGS_REMOVED"

    const/16 v2, 0xf

    const-string v3, "number_of_tags_removed"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUMBER_OF_TAGS_REMOVED:LX/9c5;

    .line 1515860
    new-instance v0, LX/9c5;

    const-string v1, "MEDIA_ITEM_IDENTIFIER"

    const/16 v2, 0x10

    const-string v3, "media_item_identifier"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->MEDIA_ITEM_IDENTIFIER:LX/9c5;

    .line 1515861
    new-instance v0, LX/9c5;

    const-string v1, "NUMBER_OF_STICKERS_ADDED"

    const/16 v2, 0x11

    const-string v3, "number_of_stickers_added"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUMBER_OF_STICKERS_ADDED:LX/9c5;

    .line 1515862
    new-instance v0, LX/9c5;

    const-string v1, "NUMBER_OF_STICKERS_REMOVED"

    const/16 v2, 0x12

    const-string v3, "number_of_stickers_removed"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUMBER_OF_STICKERS_REMOVED:LX/9c5;

    .line 1515863
    new-instance v0, LX/9c5;

    const-string v1, "NUMBER_OF_STICKERS_MOVED"

    const/16 v2, 0x13

    const-string v3, "moved_stickers_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUMBER_OF_STICKERS_MOVED:LX/9c5;

    .line 1515864
    new-instance v0, LX/9c5;

    const-string v1, "NUMBER_OF_STICKERS_RESIZED"

    const/16 v2, 0x14

    const-string v3, "resized_stickers_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUMBER_OF_STICKERS_RESIZED:LX/9c5;

    .line 1515865
    new-instance v0, LX/9c5;

    const-string v1, "NUMBER_OF_STICKERS_ROTATED"

    const/16 v2, 0x15

    const-string v3, "rotated_stickers_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUMBER_OF_STICKERS_ROTATED:LX/9c5;

    .line 1515866
    new-instance v0, LX/9c5;

    const-string v1, "STICKER_ID"

    const/16 v2, 0x16

    const-string v3, "sticker_id"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->STICKER_ID:LX/9c5;

    .line 1515867
    new-instance v0, LX/9c5;

    const-string v1, "NUM_TEXT_ADDED"

    const/16 v2, 0x17

    const-string v3, "insertion_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_TEXT_ADDED:LX/9c5;

    .line 1515868
    new-instance v0, LX/9c5;

    const-string v1, "NUM_TEXT_REMOVED"

    const/16 v2, 0x18

    const-string v3, "removal_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_TEXT_REMOVED:LX/9c5;

    .line 1515869
    new-instance v0, LX/9c5;

    const-string v1, "NUM_TEXT_MOVED"

    const/16 v2, 0x19

    const-string v3, "moved_text_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_TEXT_MOVED:LX/9c5;

    .line 1515870
    new-instance v0, LX/9c5;

    const-string v1, "NUM_TEXT_RESIZED"

    const/16 v2, 0x1a

    const-string v3, "resized_text_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_TEXT_RESIZED:LX/9c5;

    .line 1515871
    new-instance v0, LX/9c5;

    const-string v1, "NUM_TEXT_ROTATED"

    const/16 v2, 0x1b

    const-string v3, "rotated_text_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_TEXT_ROTATED:LX/9c5;

    .line 1515872
    new-instance v0, LX/9c5;

    const-string v1, "NUM_TEXT_EDITED"

    const/16 v2, 0x1c

    const-string v3, "edited_text_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_TEXT_EDITED:LX/9c5;

    .line 1515873
    new-instance v0, LX/9c5;

    const-string v1, "USED_COLOR_PICKER"

    const/16 v2, 0x1d

    const-string v3, "used_color_picker"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->USED_COLOR_PICKER:LX/9c5;

    .line 1515874
    new-instance v0, LX/9c5;

    const-string v1, "NUM_STROKES"

    const/16 v2, 0x1e

    const-string v3, "doodle_strokes_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_STROKES:LX/9c5;

    .line 1515875
    new-instance v0, LX/9c5;

    const-string v1, "NUM_UNDOS"

    const/16 v2, 0x1f

    const-string v3, "doodle_undo_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_UNDOS:LX/9c5;

    .line 1515876
    new-instance v0, LX/9c5;

    const-string v1, "NUM_RESETS"

    const/16 v2, 0x20

    const-string v3, "doodle_reset_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_RESETS:LX/9c5;

    .line 1515877
    new-instance v0, LX/9c5;

    const-string v1, "DOODLE_USED_COLOR_PICKER"

    const/16 v2, 0x21

    const-string v3, "doodle_used_color_picker"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->DOODLE_USED_COLOR_PICKER:LX/9c5;

    .line 1515878
    new-instance v0, LX/9c5;

    const-string v1, "NUM_OF_FILTER_SWIPES"

    const/16 v2, 0x22

    const-string v3, "filter_swipes_in_gallery"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->NUM_OF_FILTER_SWIPES:LX/9c5;

    .line 1515879
    new-instance v0, LX/9c5;

    const-string v1, "APPLIED_FILTER"

    const/16 v2, 0x23

    const-string v3, "applied_filter_in_gallery"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->APPLIED_FILTER:LX/9c5;

    .line 1515880
    new-instance v0, LX/9c5;

    const-string v1, "FILTER_NAME"

    const/16 v2, 0x24

    const-string v3, "filter_name"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->FILTER_NAME:LX/9c5;

    .line 1515881
    new-instance v0, LX/9c5;

    const-string v1, "CAMERA_FACING"

    const/16 v2, 0x25

    const-string v3, "camera_facing"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->CAMERA_FACING:LX/9c5;

    .line 1515882
    new-instance v0, LX/9c5;

    const-string v1, "CREATIVECAM_SOURCE"

    const/16 v2, 0x26

    const-string v3, "creativecam_source"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    .line 1515883
    new-instance v0, LX/9c5;

    const-string v1, "SOURCE"

    const/16 v2, 0x27

    const-string v3, "source"

    invoke-direct {v0, v1, v2, v3}, LX/9c5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c5;->SOURCE:LX/9c5;

    .line 1515884
    const/16 v0, 0x28

    new-array v0, v0, [LX/9c5;

    sget-object v1, LX/9c5;->ENABLED:LX/9c5;

    aput-object v1, v0, v4

    sget-object v1, LX/9c5;->TOGGLE_COUNT:LX/9c5;

    aput-object v1, v0, v5

    sget-object v1, LX/9c5;->MIN_SLIDER_VALUE:LX/9c5;

    aput-object v1, v0, v6

    sget-object v1, LX/9c5;->MAX_SLIDER_VALUE:LX/9c5;

    aput-object v1, v0, v7

    sget-object v1, LX/9c5;->FINAL_SLIDER_VALUE:LX/9c5;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9c5;->PUBLISHED:LX/9c5;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9c5;->DELETED:LX/9c5;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9c5;->DURATION_MS:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9c5;->CROP_EDGE_CONTROL:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9c5;->CROP_PAN_CONTROL:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9c5;->CROP_ADJUSTMENT_COUNT:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9c5;->ROTATION_COUNT:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9c5;->CROP_ORIGINAL_ASPECT_RATIO:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9c5;->CROP_FINAL_ASPECT_RATIO:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9c5;->ACCEPTED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9c5;->NUMBER_OF_TAGS_REMOVED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9c5;->MEDIA_ITEM_IDENTIFIER:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9c5;->NUMBER_OF_STICKERS_ADDED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9c5;->NUMBER_OF_STICKERS_REMOVED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9c5;->NUMBER_OF_STICKERS_MOVED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/9c5;->NUMBER_OF_STICKERS_RESIZED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/9c5;->NUMBER_OF_STICKERS_ROTATED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/9c5;->STICKER_ID:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/9c5;->NUM_TEXT_ADDED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/9c5;->NUM_TEXT_REMOVED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/9c5;->NUM_TEXT_MOVED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/9c5;->NUM_TEXT_RESIZED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/9c5;->NUM_TEXT_ROTATED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/9c5;->NUM_TEXT_EDITED:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/9c5;->USED_COLOR_PICKER:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/9c5;->NUM_STROKES:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/9c5;->NUM_UNDOS:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/9c5;->NUM_RESETS:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/9c5;->DOODLE_USED_COLOR_PICKER:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/9c5;->NUM_OF_FILTER_SWIPES:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/9c5;->APPLIED_FILTER:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/9c5;->FILTER_NAME:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/9c5;->CAMERA_FACING:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/9c5;->SOURCE:LX/9c5;

    aput-object v2, v0, v1

    sput-object v0, LX/9c5;->$VALUES:[LX/9c5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1515885
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1515886
    iput-object p3, p0, LX/9c5;->name:Ljava/lang/String;

    .line 1515887
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9c5;
    .locals 1

    .prologue
    .line 1515888
    const-class v0, LX/9c5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9c5;

    return-object v0
.end method

.method public static values()[LX/9c5;
    .locals 1

    .prologue
    .line 1515889
    sget-object v0, LX/9c5;->$VALUES:[LX/9c5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9c5;

    return-object v0
.end method


# virtual methods
.method public final getParamKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1515890
    iget-object v0, p0, LX/9c5;->name:Ljava/lang/String;

    return-object v0
.end method
