.class public final LX/95y;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Z

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/961;


# direct methods
.method public constructor <init>(LX/961;LX/1L9;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1438057
    iput-object p1, p0, LX/95y;->f:LX/961;

    iput-object p2, p0, LX/95y;->a:LX/1L9;

    iput-object p3, p0, LX/95y;->b:Ljava/lang/String;

    iput-object p4, p0, LX/95y;->c:Ljava/lang/String;

    iput-boolean p5, p0, LX/95y;->d:Z

    iput-object p6, p0, LX/95y;->e:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1438058
    iget-object v0, p0, LX/95y;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438059
    iget-object v0, p0, LX/95y;->a:LX/1L9;

    iget-object v1, p0, LX/95y;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438060
    :cond_0
    iget-object v0, p0, LX/95y;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1438061
    iget-object v0, p0, LX/95y;->f:LX/961;

    iget-object v0, v0, LX/961;->a:LX/0Zb;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/95y;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_fail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/95y;->b:Ljava/lang/String;

    iget-boolean v3, p0, LX/95y;->d:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/95y;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438062
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1438063
    iget-object v0, p0, LX/95y;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438064
    iget-object v0, p0, LX/95y;->a:LX/1L9;

    iget-object v1, p0, LX/95y;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1L9;->b(Ljava/lang/Object;)V

    .line 1438065
    :cond_0
    return-void
.end method
