.class public final LX/9rK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1552604
    const/16 v19, 0x0

    .line 1552605
    const/16 v18, 0x0

    .line 1552606
    const/16 v17, 0x0

    .line 1552607
    const/16 v16, 0x0

    .line 1552608
    const/4 v15, 0x0

    .line 1552609
    const/4 v14, 0x0

    .line 1552610
    const/4 v13, 0x0

    .line 1552611
    const/4 v12, 0x0

    .line 1552612
    const/4 v11, 0x0

    .line 1552613
    const/4 v10, 0x0

    .line 1552614
    const/4 v9, 0x0

    .line 1552615
    const/4 v8, 0x0

    .line 1552616
    const/4 v7, 0x0

    .line 1552617
    const/4 v6, 0x0

    .line 1552618
    const/4 v5, 0x0

    .line 1552619
    const/4 v4, 0x0

    .line 1552620
    const/4 v3, 0x0

    .line 1552621
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    .line 1552622
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1552623
    const/4 v3, 0x0

    .line 1552624
    :goto_0
    return v3

    .line 1552625
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1552626
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_11

    .line 1552627
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v20

    .line 1552628
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1552629
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    if-eqz v20, :cond_1

    .line 1552630
    const-string v21, "__type__"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_2

    const-string v21, "__typename"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1552631
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v19

    goto :goto_1

    .line 1552632
    :cond_3
    const-string v21, "collapse_state"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 1552633
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto :goto_1

    .line 1552634
    :cond_4
    const-string v21, "has_inner_borders"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1552635
    const/4 v4, 0x1

    .line 1552636
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto :goto_1

    .line 1552637
    :cond_5
    const-string v21, "id"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1552638
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto :goto_1

    .line 1552639
    :cond_6
    const-string v21, "impression_info"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1552640
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1552641
    :cond_7
    const-string v21, "page"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1552642
    invoke-static/range {p0 .. p1}, LX/5tO;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1552643
    :cond_8
    const-string v21, "reaction_aggregated_units"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1552644
    invoke-static/range {p0 .. p1}, LX/9r1;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1552645
    :cond_9
    const-string v21, "reaction_attachments"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1552646
    invoke-static/range {p0 .. p1}, LX/9rE;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1552647
    :cond_a
    const-string v21, "reaction_unit_components"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1552648
    invoke-static/range {p0 .. p1}, LX/9xa;->b(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1552649
    :cond_b
    const-string v21, "reaction_unit_header"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1552650
    invoke-static/range {p0 .. p1}, LX/9rL;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1552651
    :cond_c
    const-string v21, "unit_score"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 1552652
    const/4 v3, 0x1

    .line 1552653
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 1552654
    :cond_d
    const-string v21, "unit_style"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 1552655
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto/16 :goto_1

    .line 1552656
    :cond_e
    const-string v21, "unit_type_token"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 1552657
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1552658
    :cond_f
    const-string v21, "welcome_note_message"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 1552659
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1552660
    :cond_10
    const-string v21, "welcome_note_photo"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 1552661
    invoke-static/range {p0 .. p1}, LX/5lp;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1552662
    :cond_11
    const/16 v20, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1552663
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1552664
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1552665
    if-eqz v4, :cond_12

    .line 1552666
    const/4 v4, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1552667
    :cond_12
    const/4 v4, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1552668
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1552669
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1552670
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1552671
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1552672
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1552673
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1552674
    if-eqz v3, :cond_13

    .line 1552675
    const/16 v3, 0xa

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1552676
    :cond_13
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1552677
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1552678
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1552679
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1552680
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v3, 0xb

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1552681
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1552682
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1552683
    if-eqz v0, :cond_0

    .line 1552684
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552685
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1552686
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1552687
    if-eqz v0, :cond_1

    .line 1552688
    const-string v0, "collapse_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552689
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552690
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1552691
    if-eqz v0, :cond_2

    .line 1552692
    const-string v1, "has_inner_borders"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552693
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1552694
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1552695
    if-eqz v0, :cond_3

    .line 1552696
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552697
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552698
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1552699
    if-eqz v0, :cond_4

    .line 1552700
    const-string v1, "impression_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552701
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552702
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552703
    if-eqz v0, :cond_5

    .line 1552704
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552705
    invoke-static {p0, v0, p2, p3}, LX/5tO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552706
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552707
    if-eqz v0, :cond_6

    .line 1552708
    const-string v1, "reaction_aggregated_units"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552709
    invoke-static {p0, v0, p2, p3}, LX/9r1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552710
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552711
    if-eqz v0, :cond_7

    .line 1552712
    const-string v1, "reaction_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552713
    invoke-static {p0, v0, p2, p3}, LX/9rE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552714
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552715
    if-eqz v0, :cond_9

    .line 1552716
    const-string v1, "reaction_unit_components"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552717
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1552718
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_8

    .line 1552719
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/9xa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552720
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1552721
    :cond_8
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1552722
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552723
    if-eqz v0, :cond_a

    .line 1552724
    const-string v1, "reaction_unit_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552725
    invoke-static {p0, v0, p2, p3}, LX/9rL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552726
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1552727
    if-eqz v0, :cond_b

    .line 1552728
    const-string v1, "unit_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552729
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1552730
    :cond_b
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1552731
    if-eqz v0, :cond_c

    .line 1552732
    const-string v0, "unit_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552733
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552734
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1552735
    if-eqz v0, :cond_d

    .line 1552736
    const-string v1, "unit_type_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552737
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552738
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552739
    if-eqz v0, :cond_e

    .line 1552740
    const-string v1, "welcome_note_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552741
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1552742
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552743
    if-eqz v0, :cond_f

    .line 1552744
    const-string v1, "welcome_note_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552745
    invoke-static {p0, v0, p2, p3}, LX/5lp;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552746
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1552747
    return-void
.end method
