.class public final LX/ASx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:LX/AT1;


# direct methods
.method public constructor <init>(LX/AT1;)V
    .locals 0

    .prologue
    .line 1674692
    iput-object p1, p0, LX/ASx;->a:LX/AT1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1674693
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1674694
    iget-object v1, p0, LX/ASx;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1674695
    iget-object v1, p0, LX/ASx;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1674696
    iget-object v1, p0, LX/ASx;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->l:Lcom/facebook/resources/ui/FbTextView;

    sub-float v2, v3, v0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1674697
    iget-object v1, p0, LX/ASx;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->k:Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;

    sub-float v0, v3, v0

    invoke-virtual {v1, v0}, Lcom/facebook/composer/ui/underwood/SphericalPhotoIVSFrameView;->setProgress(F)V

    .line 1674698
    return-void
.end method
