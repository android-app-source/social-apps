.class public final LX/9dk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/9dl;


# direct methods
.method public constructor <init>(LX/9dl;)V
    .locals 0

    .prologue
    .line 1518524
    iput-object p1, p0, LX/9dk;->a:LX/9dl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1518525
    iget-object v2, p0, LX/9dk;->a:LX/9dl;

    iget-boolean v2, v2, LX/9dl;->j:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/9dk;->a:LX/9dl;

    iget-object v2, v2, LX/9dl;->c:Landroid/graphics/Rect;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1518526
    :cond_1
    :goto_0
    return v0

    .line 1518527
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 1518528
    iget-object v3, p0, LX/9dk;->a:LX/9dl;

    iget-object v3, v3, LX/9dl;->f:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v3, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1518529
    iget-object v3, p0, LX/9dk;->a:LX/9dl;

    iget-object v3, v3, LX/9dl;->g:LX/9e2;

    invoke-virtual {v3, p2}, LX/9e2;->a(Landroid/view/MotionEvent;)Z

    .line 1518530
    iget-object v3, p0, LX/9dk;->a:LX/9dl;

    iget-object v3, v3, LX/9dl;->e:Landroid/view/GestureDetector;

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1518531
    if-ne v2, v0, :cond_1

    .line 1518532
    iget-object v2, p0, LX/9dk;->a:LX/9dl;

    .line 1518533
    iput-boolean v1, v2, LX/9dl;->h:Z

    .line 1518534
    iget-object v1, p0, LX/9dk;->a:LX/9dl;

    invoke-virtual {v1}, LX/9dl;->getAnimationController()LX/9dr;

    move-result-object v1

    invoke-virtual {v1}, LX/9dr;->b()V

    .line 1518535
    iget-object v1, p0, LX/9dk;->a:LX/9dl;

    .line 1518536
    invoke-virtual {v1}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    .line 1518537
    iget-object v3, v2, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v2, v3

    .line 1518538
    if-nez v2, :cond_4

    .line 1518539
    :cond_3
    :goto_1
    iget-object v1, p0, LX/9dk;->a:LX/9dl;

    invoke-virtual {v1}, LX/9dl;->h()V

    .line 1518540
    iget-object v1, p0, LX/9dk;->a:LX/9dl;

    invoke-static {v1}, LX/9dl;->o(LX/9dl;)V

    .line 1518541
    iget-object v1, p0, LX/9dk;->a:LX/9dl;

    invoke-virtual {v1}, LX/9dl;->invalidate()V

    goto :goto_0

    .line 1518542
    :cond_4
    invoke-static {v1, v2}, LX/9dl;->c(LX/9dl;LX/362;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1518543
    invoke-virtual {v1, v2}, LX/9dl;->b(LX/362;)V

    goto :goto_1
.end method
