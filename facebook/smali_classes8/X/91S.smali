.class public final LX/91S;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/91S;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/91R;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/91T;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1430822
    const/4 v0, 0x0

    sput-object v0, LX/91S;->a:LX/91S;

    .line 1430823
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/91S;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1430819
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1430820
    new-instance v0, LX/91T;

    invoke-direct {v0}, LX/91T;-><init>()V

    iput-object v0, p0, LX/91S;->c:LX/91T;

    .line 1430821
    return-void
.end method

.method public static c(LX/1De;)LX/91R;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1430785
    new-instance v1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;-><init>()V

    .line 1430786
    sget-object v2, LX/91S;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/91R;

    .line 1430787
    if-nez v2, :cond_0

    .line 1430788
    new-instance v2, LX/91R;

    invoke-direct {v2}, LX/91R;-><init>()V

    .line 1430789
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/91R;->a$redex0(LX/91R;LX/1De;IILcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;)V

    .line 1430790
    move-object v1, v2

    .line 1430791
    move-object v0, v1

    .line 1430792
    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1430818
    const v0, -0x5fa57f65

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/91S;
    .locals 2

    .prologue
    .line 1430814
    const-class v1, LX/91S;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/91S;->a:LX/91S;

    if-nez v0, :cond_0

    .line 1430815
    new-instance v0, LX/91S;

    invoke-direct {v0}, LX/91S;-><init>()V

    sput-object v0, LX/91S;->a:LX/91S;

    .line 1430816
    :cond_0
    sget-object v0, LX/91S;->a:LX/91S;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1430817
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1430804
    check-cast p2, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;

    .line 1430805
    iget-object v0, p2, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->a:LX/1dc;

    iget-object v1, p2, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->b:Ljava/lang/CharSequence;

    iget v2, p2, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->c:I

    const/4 p2, 0x2

    .line 1430806
    if-nez v1, :cond_0

    .line 1430807
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08003b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1430808
    :cond_0
    if-nez v0, :cond_1

    .line 1430809
    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v3

    const v4, 0x7f021120

    invoke-virtual {v3, v4}, LX/1nm;->h(I)LX/1nm;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1430810
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b18a1

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b18a2

    invoke-interface {v4, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x0

    const p0, 0x7f0b18a3

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b18a4

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a0052

    invoke-virtual {v4, v5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f021121

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b18a5

    invoke-interface {v4, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b18a6

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    .line 1430811
    const v4, -0x5fa57f65

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1430812
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1430813
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1430793
    invoke-static {}, LX/1dS;->b()V

    .line 1430794
    iget v0, p1, LX/1dQ;->b:I

    .line 1430795
    packed-switch v0, :pswitch_data_0

    .line 1430796
    :goto_0
    return-object v2

    .line 1430797
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1430798
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1430799
    check-cast v1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;

    .line 1430800
    iget-object p0, v1, Lcom/facebook/composer/minutiae/common/NetworkRetryComponent$NetworkRetryComponentImpl;->f:Landroid/view/View$OnClickListener;

    .line 1430801
    if-eqz p0, :cond_0

    .line 1430802
    invoke-interface {p0, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1430803
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x5fa57f65
        :pswitch_0
    .end packed-switch
.end method
