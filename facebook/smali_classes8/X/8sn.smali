.class public LX/8sn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8sj;


# instance fields
.field private final a:J

.field private final b:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method public constructor <init>(JLandroid/animation/Animator$AnimatorListener;)V
    .locals 1

    .prologue
    .line 1411824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411825
    iput-wide p1, p0, LX/8sn;->a:J

    .line 1411826
    iput-object p3, p0, LX/8sn;->b:Landroid/animation/Animator$AnimatorListener;

    .line 1411827
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1411818
    iget-wide v0, p0, LX/8sn;->a:J

    return-wide v0
.end method

.method public final a(F)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 1411819
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1411820
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    iget-wide v2, p0, LX/8sn;->a:J

    long-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1411821
    iget-object v1, p0, LX/8sn;->b:Landroid/animation/Animator$AnimatorListener;

    if-eqz v1, :cond_0

    .line 1411822
    iget-object v1, p0, LX/8sn;->b:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1411823
    :cond_0
    return-object v0

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method
