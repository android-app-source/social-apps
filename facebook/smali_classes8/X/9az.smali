.class public final LX/9az;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:LX/9b1;


# direct methods
.method public constructor <init>(LX/9b1;)V
    .locals 0

    .prologue
    .line 1514175
    iput-object p1, p0, LX/9az;->a:LX/9b1;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 8

    .prologue
    .line 1514176
    new-instance v2, Landroid/widget/Filter$FilterResults;

    invoke-direct {v2}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1514177
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1514178
    iget-object v0, p0, LX/9az;->a:LX/9b1;

    iget-object v0, v0, LX/9b1;->a:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514179
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v6, v7, :cond_0

    .line 1514180
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1514181
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1514182
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1514183
    iput-object v0, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1514184
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    .line 1514185
    return-object v2
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 1514186
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-nez v0, :cond_0

    .line 1514187
    iget-object v0, p0, LX/9az;->a:LX/9b1;

    const v1, -0x5392b1d9

    invoke-static {v0, v1}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    .line 1514188
    :goto_0
    return-void

    .line 1514189
    :cond_0
    iget-object v1, p0, LX/9az;->a:LX/9b1;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    .line 1514190
    iput-object v0, v1, LX/9b1;->b:LX/0Px;

    .line 1514191
    iget-object v0, p0, LX/9az;->a:LX/9b1;

    const v1, 0x28761595

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
