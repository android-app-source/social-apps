.class public final LX/9Fm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9Fn;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;LX/9Fn;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1459062
    iput-object p1, p0, LX/9Fm;->c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    iput-object p2, p0, LX/9Fm;->a:LX/9Fn;

    iput-object p3, p0, LX/9Fm;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x6c3429c6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1459063
    iget-object v0, p0, LX/9Fm;->c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    iget-object v2, p0, LX/9Fm;->a:LX/9Fn;

    iget-object v2, v2, LX/9Fn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    iget-object v3, p0, LX/9Fm;->a:LX/9Fn;

    iget-object v3, v3, LX/9Fn;->b:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v4, p0, LX/9Fm;->b:Ljava/lang/String;

    .line 1459064
    if-nez v2, :cond_3

    .line 1459065
    const/4 v5, 0x0

    .line 1459066
    :goto_0
    move-object v2, v5

    .line 1459067
    iget-object v0, p0, LX/9Fm;->c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    iget-object v3, v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->f:LX/8y4;

    iget-object v4, p0, LX/9Fm;->b:Ljava/lang/String;

    iget-object v0, p0, LX/9Fm;->a:LX/9Fn;

    iget-object v0, v0, LX/9Fn;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    new-instance v6, LX/9Fl;

    invoke-direct {v6, p0}, LX/9Fl;-><init>(LX/9Fm;)V

    .line 1459068
    new-instance v7, LX/4DS;

    invoke-direct {v7}, LX/4DS;-><init>()V

    .line 1459069
    const-string v8, "comment_id"

    invoke-virtual {v7, v8, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459070
    move-object v7, v7

    .line 1459071
    const-string v8, "confirmed_profile_id"

    invoke-virtual {v7, v8, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459072
    move-object v7, v7

    .line 1459073
    new-instance v8, LX/5Iy;

    invoke-direct {v8}, LX/5Iy;-><init>()V

    move-object v8, v8

    .line 1459074
    const-string v9, "input"

    invoke-virtual {v8, v9, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1459075
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 1459076
    if-eqz v0, :cond_0

    .line 1459077
    invoke-static {v0}, LX/8y4;->a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$PlaceListRemoveConfirmedProfileRecommendationMutationModel;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/399;->a(LX/0jT;)LX/399;

    .line 1459078
    :cond_0
    iget-object v8, v3, LX/8y4;->b:LX/1Ck;

    const-string v9, "remove_confirmed_person_card"

    iget-object p1, v3, LX/8y4;->a:LX/0tX;

    invoke-virtual {p1, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v6}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object p1

    invoke-virtual {v8, v9, v7, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1459079
    if-nez v2, :cond_2

    .line 1459080
    const v0, -0xc000738

    invoke-static {v0, v1}, LX/02F;->a(II)V

    .line 1459081
    :goto_2
    return-void

    .line 1459082
    :cond_1
    iget-object v0, v2, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    goto :goto_1

    .line 1459083
    :cond_2
    iget-object v0, p0, LX/9Fm;->c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->h:LX/1K9;

    new-instance v3, LX/8q4;

    iget-object v4, v2, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, v2, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/1K9;->a(LX/1KJ;)V

    .line 1459084
    const v0, -0x1e1d76bc

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_2

    :cond_3
    iget-object v5, v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->g:LX/20j;

    const/4 v6, 0x1

    invoke-virtual {v5, v2, v3, v4, v6}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;Z)LX/6PS;

    move-result-object v5

    goto :goto_0
.end method
