.class public final LX/91J;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/91K;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1430326
    invoke-direct {p0}, LX/1X5;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/91J;LX/1De;IILcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;)V
    .locals 0

    .prologue
    .line 1430327
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1430328
    iput-object p4, p0, LX/91J;->a:Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    .line 1430329
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)LX/91J;
    .locals 1

    .prologue
    .line 1430330
    iget-object v0, p0, LX/91J;->a:Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    iput-object p1, v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->g:Landroid/view/View$OnClickListener;

    .line 1430331
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1430332
    invoke-super {p0}, LX/1X5;->a()V

    .line 1430333
    const/4 v0, 0x0

    iput-object v0, p0, LX/91J;->a:Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    .line 1430334
    sget-object v0, LX/91K;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1430335
    return-void
.end method

.method public final d()LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/91K;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1430336
    iget-object v0, p0, LX/91J;->a:Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    .line 1430337
    invoke-virtual {p0}, LX/91J;->a()V

    .line 1430338
    return-object v0
.end method

.method public final h(I)LX/91J;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1430339
    iget-object v0, p0, LX/91J;->a:Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->a:Ljava/lang/CharSequence;

    .line 1430340
    return-object p0
.end method
