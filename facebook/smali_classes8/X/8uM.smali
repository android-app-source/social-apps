.class public LX/8uM;
.super Landroid/graphics/drawable/shapes/RectShape;
.source ""


# instance fields
.field private a:[F

.field private b:Landroid/graphics/RectF;

.field private c:[F

.field private d:Landroid/graphics/RectF;

.field private e:Landroid/graphics/Path;

.field private f:F

.field private g:F

.field private h:F

.field private i:I

.field private j:Landroid/graphics/Path;

.field private k:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>([FLandroid/graphics/RectF;[F)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1414724
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    .line 1414725
    if-eqz p1, :cond_0

    array-length v0, p1

    if-ge v0, v1, :cond_0

    .line 1414726
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "outer radii must have >= 8 values"

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1414727
    :cond_0
    if-eqz p3, :cond_1

    array-length v0, p3

    if-ge v0, v1, :cond_1

    .line 1414728
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "inner radii must have >= 8 values"

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1414729
    :cond_1
    iput-object p1, p0, LX/8uM;->a:[F

    .line 1414730
    iput-object p2, p0, LX/8uM;->b:Landroid/graphics/RectF;

    .line 1414731
    iput-object p3, p0, LX/8uM;->c:[F

    .line 1414732
    if-eqz p2, :cond_2

    .line 1414733
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/8uM;->d:Landroid/graphics/RectF;

    .line 1414734
    :cond_2
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/8uM;->e:Landroid/graphics/Path;

    .line 1414735
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/8uM;->k:Landroid/graphics/RectF;

    .line 1414736
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1414709
    invoke-virtual {p0}, LX/8uM;->rect()Landroid/graphics/RectF;

    move-result-object v0

    .line 1414710
    iget-object v1, p0, LX/8uM;->e:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 1414711
    iget-object v1, p0, LX/8uM;->a:[F

    if-eqz v1, :cond_1

    .line 1414712
    iget-object v1, p0, LX/8uM;->e:Landroid/graphics/Path;

    iget-object v2, p0, LX/8uM;->a:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v0, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 1414713
    :goto_0
    iget-object v1, p0, LX/8uM;->d:Landroid/graphics/RectF;

    if-eqz v1, :cond_0

    .line 1414714
    iget-object v1, p0, LX/8uM;->d:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/8uM;->b:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/8uM;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/8uM;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v5

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, LX/8uM;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1414715
    iget-object v0, p0, LX/8uM;->d:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v1, p0, LX/8uM;->f:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, LX/8uM;->d:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget v1, p0, LX/8uM;->g:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1414716
    iget-object v0, p0, LX/8uM;->c:[F

    if-eqz v0, :cond_2

    .line 1414717
    iget-object v0, p0, LX/8uM;->e:Landroid/graphics/Path;

    iget-object v1, p0, LX/8uM;->d:Landroid/graphics/RectF;

    iget-object v2, p0, LX/8uM;->c:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 1414718
    iget-object v0, p0, LX/8uM;->j:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    .line 1414719
    iget-object v0, p0, LX/8uM;->j:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1414720
    iget-object v0, p0, LX/8uM;->j:Landroid/graphics/Path;

    iget-object v1, p0, LX/8uM;->d:Landroid/graphics/RectF;

    iget-object v2, p0, LX/8uM;->c:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 1414721
    :cond_0
    :goto_1
    return-void

    .line 1414722
    :cond_1
    iget-object v1, p0, LX/8uM;->e:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto :goto_0

    .line 1414723
    :cond_2
    iget-object v0, p0, LX/8uM;->e:Landroid/graphics/Path;

    iget-object v1, p0, LX/8uM;->d:Landroid/graphics/RectF;

    sget-object v2, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto :goto_1
.end method

.method private b()LX/8uM;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1414695
    invoke-super {p0}, Landroid/graphics/drawable/shapes/RectShape;->clone()Landroid/graphics/drawable/shapes/RectShape;

    move-result-object v0

    check-cast v0, LX/8uM;

    .line 1414696
    iget-object v1, p0, LX/8uM;->a:[F

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/8uM;->a:[F

    invoke-virtual {v1}, [F->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    :goto_0
    iput-object v1, v0, LX/8uM;->a:[F

    .line 1414697
    iget-object v1, p0, LX/8uM;->c:[F

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/8uM;->c:[F

    invoke-virtual {v1}, [F->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    :goto_1
    iput-object v1, v0, LX/8uM;->c:[F

    .line 1414698
    iget-object v1, p0, LX/8uM;->b:Landroid/graphics/RectF;

    if-eqz v1, :cond_4

    new-instance v1, Landroid/graphics/RectF;

    iget-object v3, p0, LX/8uM;->b:Landroid/graphics/RectF;

    invoke-direct {v1, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    :goto_2
    iput-object v1, v0, LX/8uM;->b:Landroid/graphics/RectF;

    .line 1414699
    iget-object v1, p0, LX/8uM;->d:Landroid/graphics/RectF;

    if-eqz v1, :cond_0

    new-instance v2, Landroid/graphics/RectF;

    iget-object v1, p0, LX/8uM;->d:Landroid/graphics/RectF;

    invoke-direct {v2, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    :cond_0
    iput-object v2, v0, LX/8uM;->d:Landroid/graphics/RectF;

    .line 1414700
    new-instance v1, Landroid/graphics/Path;

    iget-object v2, p0, LX/8uM;->e:Landroid/graphics/Path;

    invoke-direct {v1, v2}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v1, v0, LX/8uM;->e:Landroid/graphics/Path;

    .line 1414701
    iget v1, p0, LX/8uM;->h:F

    iput v1, v0, LX/8uM;->h:F

    .line 1414702
    iget v1, p0, LX/8uM;->i:I

    iput v1, v0, LX/8uM;->i:I

    .line 1414703
    iget-object v1, p0, LX/8uM;->j:Landroid/graphics/Path;

    if-eqz v1, :cond_1

    .line 1414704
    new-instance v1, Landroid/graphics/Path;

    iget-object v2, p0, LX/8uM;->j:Landroid/graphics/Path;

    invoke-direct {v1, v2}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v1, v0, LX/8uM;->j:Landroid/graphics/Path;

    .line 1414705
    :cond_1
    return-object v0

    :cond_2
    move-object v1, v2

    .line 1414706
    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 1414707
    goto :goto_1

    :cond_4
    move-object v1, v2

    .line 1414708
    goto :goto_2
.end method


# virtual methods
.method public final a([F)V
    .locals 2

    .prologue
    .line 1414690
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 1414691
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "outer radii must have >= 8 values"

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1414692
    :cond_0
    iput-object p1, p0, LX/8uM;->a:[F

    .line 1414693
    invoke-direct {p0}, LX/8uM;->a()V

    .line 1414694
    return-void
.end method

.method public final synthetic clone()Landroid/graphics/drawable/shapes/RectShape;
    .locals 1

    .prologue
    .line 1414689
    invoke-direct {p0}, LX/8uM;->b()LX/8uM;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Landroid/graphics/drawable/shapes/Shape;
    .locals 1

    .prologue
    .line 1414655
    invoke-direct {p0}, LX/8uM;->b()LX/8uM;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1414688
    invoke-direct {p0}, LX/8uM;->b()LX/8uM;

    move-result-object v0

    return-object v0
.end method

.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 12

    .prologue
    .line 1414661
    iget-object v0, p0, LX/8uM;->a:[F

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8uM;->d:Landroid/graphics/RectF;

    if-nez v0, :cond_1

    .line 1414662
    invoke-virtual {p0}, LX/8uM;->rect()Landroid/graphics/RectF;

    move-result-object v11

    .line 1414663
    iget-object v0, p0, LX/8uM;->k:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->left:F

    iget v2, v11, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, LX/8uM;->a:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, LX/8uM;->a:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1414664
    iget-object v1, p0, LX/8uM;->k:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000    # 180.0f

    const/high16 v3, 0x42b40000    # 90.0f

    const/4 v4, 0x1

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1414665
    iget-object v0, p0, LX/8uM;->k:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, LX/8uM;->a:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, v11, Landroid/graphics/RectF;->top:F

    iget v3, v11, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, LX/8uM;->a:[F

    const/4 v5, 0x3

    aget v4, v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1414666
    iget-object v1, p0, LX/8uM;->k:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    const/high16 v3, 0x42b40000    # 90.0f

    const/4 v4, 0x1

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1414667
    iget-object v0, p0, LX/8uM;->k:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, LX/8uM;->a:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, LX/8uM;->a:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, v11, Landroid/graphics/RectF;->right:F

    iget v4, v11, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1414668
    iget-object v1, p0, LX/8uM;->k:Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/high16 v3, 0x42b40000    # 90.0f

    const/4 v4, 0x1

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1414669
    iget-object v0, p0, LX/8uM;->k:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->left:F

    iget v2, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, LX/8uM;->a:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, v11, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, LX/8uM;->a:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, v11, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1414670
    iget-object v1, p0, LX/8uM;->k:Landroid/graphics/RectF;

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, 0x42b40000    # 90.0f

    const/4 v4, 0x1

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1414671
    iget v0, v11, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/8uM;->a:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, LX/8uM;->a:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    add-float/2addr v1, v0

    .line 1414672
    iget v0, v11, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, LX/8uM;->a:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, LX/8uM;->a:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    add-float/2addr v2, v0

    .line 1414673
    iget v0, v11, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, LX/8uM;->a:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, LX/8uM;->a:[F

    const/4 v5, 0x4

    aget v4, v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    sub-float v3, v0, v3

    .line 1414674
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, LX/8uM;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    iget-object v5, p0, LX/8uM;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    sub-float v4, v0, v4

    move-object v0, p1

    move-object v5, p2

    .line 1414675
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1414676
    iget v6, v11, Landroid/graphics/RectF;->left:F

    iget v0, v11, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, LX/8uM;->a:[F

    const/4 v7, 0x1

    aget v5, v5, v7

    add-float v7, v0, v5

    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, LX/8uM;->a:[F

    const/4 v8, 0x7

    aget v5, v5, v8

    sub-float v9, v0, v5

    move-object v5, p1

    move v8, v1

    move-object v10, p2

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1414677
    iget v0, v11, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/8uM;->a:[F

    const/4 v5, 0x0

    aget v1, v1, v5

    add-float v6, v0, v1

    iget v7, v11, Landroid/graphics/RectF;->top:F

    iget v0, v11, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/8uM;->a:[F

    const/4 v5, 0x2

    aget v1, v1, v5

    sub-float v8, v0, v1

    move-object v5, p1

    move v9, v2

    move-object v10, p2

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1414678
    iget v0, v11, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, LX/8uM;->a:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    add-float v7, v0, v1

    iget v8, v11, Landroid/graphics/RectF;->right:F

    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, LX/8uM;->a:[F

    const/4 v2, 0x5

    aget v1, v1, v2

    sub-float v9, v0, v1

    move-object v5, p1

    move v6, v3

    move-object v10, p2

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1414679
    iget v0, v11, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/8uM;->a:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    add-float v3, v0, v1

    iget v0, v11, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/8uM;->a:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    sub-float v5, v0, v1

    iget v6, v11, Landroid/graphics/RectF;->bottom:F

    move-object v2, p1

    move-object v7, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1414680
    :goto_0
    iget-object v0, p0, LX/8uM;->j:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    .line 1414681
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 1414682
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1414683
    iget v1, p0, LX/8uM;->h:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1414684
    iget v1, p0, LX/8uM;->i:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414685
    iget-object v1, p0, LX/8uM;->j:Landroid/graphics/Path;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1414686
    :cond_0
    return-void

    .line 1414687
    :cond_1
    iget-object v0, p0, LX/8uM;->e:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final onResize(FF)V
    .locals 0

    .prologue
    .line 1414656
    iput p1, p0, LX/8uM;->f:F

    .line 1414657
    iput p2, p0, LX/8uM;->g:F

    .line 1414658
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/shapes/RectShape;->onResize(FF)V

    .line 1414659
    invoke-direct {p0}, LX/8uM;->a()V

    .line 1414660
    return-void
.end method
