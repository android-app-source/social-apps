.class public final enum LX/8vI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8vI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8vI;

.field public static final enum HEADER:LX/8vI;

.field public static final enum ITEM:LX/8vI;

.field public static final enum LOADING_SECTION:LX/8vI;

.field public static final enum SUBTITLED_ITEM:LX/8vI;

.field public static final enum VIEW_MORE:LX/8vI;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1416853
    new-instance v0, LX/8vI;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/8vI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vI;->HEADER:LX/8vI;

    .line 1416854
    new-instance v0, LX/8vI;

    const-string v1, "ITEM"

    invoke-direct {v0, v1, v3}, LX/8vI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vI;->ITEM:LX/8vI;

    .line 1416855
    new-instance v0, LX/8vI;

    const-string v1, "VIEW_MORE"

    invoke-direct {v0, v1, v4}, LX/8vI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vI;->VIEW_MORE:LX/8vI;

    .line 1416856
    new-instance v0, LX/8vI;

    const-string v1, "SUBTITLED_ITEM"

    invoke-direct {v0, v1, v5}, LX/8vI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vI;->SUBTITLED_ITEM:LX/8vI;

    .line 1416857
    new-instance v0, LX/8vI;

    const-string v1, "LOADING_SECTION"

    invoke-direct {v0, v1, v6}, LX/8vI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vI;->LOADING_SECTION:LX/8vI;

    .line 1416858
    const/4 v0, 0x5

    new-array v0, v0, [LX/8vI;

    sget-object v1, LX/8vI;->HEADER:LX/8vI;

    aput-object v1, v0, v2

    sget-object v1, LX/8vI;->ITEM:LX/8vI;

    aput-object v1, v0, v3

    sget-object v1, LX/8vI;->VIEW_MORE:LX/8vI;

    aput-object v1, v0, v4

    sget-object v1, LX/8vI;->SUBTITLED_ITEM:LX/8vI;

    aput-object v1, v0, v5

    sget-object v1, LX/8vI;->LOADING_SECTION:LX/8vI;

    aput-object v1, v0, v6

    sput-object v0, LX/8vI;->$VALUES:[LX/8vI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1416852
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8vI;
    .locals 1

    .prologue
    .line 1416850
    const-class v0, LX/8vI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8vI;

    return-object v0
.end method

.method public static values()[LX/8vI;
    .locals 1

    .prologue
    .line 1416851
    sget-object v0, LX/8vI;->$VALUES:[LX/8vI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8vI;

    return-object v0
.end method
