.class public final LX/9f7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520814
    iput-object p1, p0, LX/9f7;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    .prologue
    .line 1520815
    iget-object v0, p0, LX/9f7;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->k()LX/9ek;

    move-result-object v0

    .line 1520816
    iget-object v1, p0, LX/9f7;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-boolean v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->X:Z

    if-nez v1, :cond_0

    sget-object v1, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    if-ne v0, v1, :cond_0

    .line 1520817
    iget-object v0, p0, LX/9f7;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->r(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1520818
    if-eqz v0, :cond_0

    .line 1520819
    iget-object v1, p0, LX/9f7;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v2, 0x1

    .line 1520820
    iput-boolean v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->X:Z

    .line 1520821
    iget-object v1, p0, LX/9f7;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/graphics/Rect;)V

    .line 1520822
    iget-object v0, p0, LX/9f7;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520823
    :cond_0
    return-void
.end method
