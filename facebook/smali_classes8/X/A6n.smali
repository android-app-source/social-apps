.class public final LX/A6n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/transliteration/TransliterationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/transliteration/TransliterationFragment;)V
    .locals 0

    .prologue
    .line 1624781
    iput-object p1, p0, LX/A6n;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x82ede75

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1624782
    iget-object v0, p0, LX/A6n;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1624783
    const v0, 0x60c6ef73

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1624784
    :goto_0
    return-void

    .line 1624785
    :cond_0
    iget-object v0, p0, LX/A6n;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    iget-object v2, p0, LX/A6n;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v2, v2, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1624786
    iget-object v2, p0, LX/A6n;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v2, v2, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    .line 1624787
    sget-object v3, LX/A6W;->MORE_OPENED:LX/A6W;

    iget-object v3, v3, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {v2, v3}, LX/A6X;->a(LX/A6X;Ljava/lang/String;)V

    .line 1624788
    new-instance v2, LX/31Y;

    iget-object v3, p0, LX/A6n;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    new-instance v3, LX/A6m;

    invoke-direct {v3, p0}, LX/A6m;-><init>(LX/A6n;)V

    invoke-virtual {v2, v0, v3}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f081214

    new-instance v3, LX/A6l;

    invoke-direct {v3, p0}, LX/A6l;-><init>(LX/A6n;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1624789
    const v0, 0x45382ac8

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
