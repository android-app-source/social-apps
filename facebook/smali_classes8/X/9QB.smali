.class public final LX/9QB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    .line 1486673
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1486674
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1486675
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1486676
    const/4 v2, 0x0

    .line 1486677
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1486678
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1486679
    :goto_1
    move v1, v2

    .line 1486680
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1486681
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1486682
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1486683
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1486684
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1486685
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1486686
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1486687
    const-string v5, "ranges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1486688
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1486689
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_3

    .line 1486690
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_3

    .line 1486691
    invoke-static {p0, p1}, LX/9QA;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1486692
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1486693
    :cond_3
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1486694
    goto :goto_2

    .line 1486695
    :cond_4
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1486696
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_2

    .line 1486697
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1486698
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1486699
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1486700
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1486701
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1486702
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1486703
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 1486704
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1486705
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1486706
    if-eqz v2, :cond_1

    .line 1486707
    const-string v3, "ranges"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486708
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1486709
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1486710
    invoke-virtual {p0, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/9QA;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486711
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1486712
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1486713
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1486714
    if-eqz v2, :cond_2

    .line 1486715
    const-string v3, "text"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486716
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1486717
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1486718
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1486719
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1486720
    return-void
.end method
