.class public LX/9HT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/feedback/ui/environment/HasCommentActions;",
        ":",
        "LX/1Pn;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasLoggingParams;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/9Hj;


# direct methods
.method public constructor <init>(LX/9Hj;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1461316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1461317
    iput-object p1, p0, LX/9HT;->a:LX/9Hj;

    .line 1461318
    return-void
.end method

.method public static a(LX/0QB;)LX/9HT;
    .locals 4

    .prologue
    .line 1461319
    const-class v1, LX/9HT;

    monitor-enter v1

    .line 1461320
    :try_start_0
    sget-object v0, LX/9HT;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461321
    sput-object v2, LX/9HT;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461322
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461323
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461324
    new-instance p0, LX/9HT;

    invoke-static {v0}, LX/9Hj;->a(LX/0QB;)LX/9Hj;

    move-result-object v3

    check-cast v3, LX/9Hj;

    invoke-direct {p0, v3}, LX/9HT;-><init>(LX/9Hj;)V

    .line 1461325
    move-object v0, p0

    .line 1461326
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461327
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9HT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461328
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461329
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
