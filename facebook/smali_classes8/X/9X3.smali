.class public final enum LX/9X3;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9X3;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9X3;

.field public static final enum EVENT_COMMENT_RECENT_POST:LX/9X3;

.field public static final enum EVENT_COMMENT_STORY:LX/9X3;

.field public static final enum EVENT_NON_ADMIN_SHARE_PHOTO:LX/9X3;

.field public static final enum EVENT_NON_ADMIN_WRITE_POST:LX/9X3;

.field public static final enum EVENT_SHARE_RECENT_POST:LX/9X3;

.field public static final enum EVENT_SHARE_STORY:LX/9X3;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502253
    new-instance v0, LX/9X3;

    const-string v1, "EVENT_COMMENT_RECENT_POST"

    const-string v2, "comment_on_recent_post"

    invoke-direct {v0, v1, v4, v2}, LX/9X3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X3;->EVENT_COMMENT_RECENT_POST:LX/9X3;

    .line 1502254
    new-instance v0, LX/9X3;

    const-string v1, "EVENT_SHARE_RECENT_POST"

    const-string v2, "share_recent_post"

    invoke-direct {v0, v1, v5, v2}, LX/9X3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X3;->EVENT_SHARE_RECENT_POST:LX/9X3;

    .line 1502255
    new-instance v0, LX/9X3;

    const-string v1, "EVENT_SHARE_STORY"

    const-string v2, "share_story"

    invoke-direct {v0, v1, v6, v2}, LX/9X3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X3;->EVENT_SHARE_STORY:LX/9X3;

    .line 1502256
    new-instance v0, LX/9X3;

    const-string v1, "EVENT_COMMENT_STORY"

    const-string v2, "comment_story"

    invoke-direct {v0, v1, v7, v2}, LX/9X3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X3;->EVENT_COMMENT_STORY:LX/9X3;

    .line 1502257
    new-instance v0, LX/9X3;

    const-string v1, "EVENT_NON_ADMIN_WRITE_POST"

    const-string v2, "non_admin_write_post"

    invoke-direct {v0, v1, v8, v2}, LX/9X3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X3;->EVENT_NON_ADMIN_WRITE_POST:LX/9X3;

    .line 1502258
    new-instance v0, LX/9X3;

    const-string v1, "EVENT_NON_ADMIN_SHARE_PHOTO"

    const/4 v2, 0x5

    const-string v3, "non_admin_share_photo"

    invoke-direct {v0, v1, v2, v3}, LX/9X3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X3;->EVENT_NON_ADMIN_SHARE_PHOTO:LX/9X3;

    .line 1502259
    const/4 v0, 0x6

    new-array v0, v0, [LX/9X3;

    sget-object v1, LX/9X3;->EVENT_COMMENT_RECENT_POST:LX/9X3;

    aput-object v1, v0, v4

    sget-object v1, LX/9X3;->EVENT_SHARE_RECENT_POST:LX/9X3;

    aput-object v1, v0, v5

    sget-object v1, LX/9X3;->EVENT_SHARE_STORY:LX/9X3;

    aput-object v1, v0, v6

    sget-object v1, LX/9X3;->EVENT_COMMENT_STORY:LX/9X3;

    aput-object v1, v0, v7

    sget-object v1, LX/9X3;->EVENT_NON_ADMIN_WRITE_POST:LX/9X3;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9X3;->EVENT_NON_ADMIN_SHARE_PHOTO:LX/9X3;

    aput-object v2, v0, v1

    sput-object v0, LX/9X3;->$VALUES:[LX/9X3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502260
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502261
    iput-object p3, p0, LX/9X3;->mEventName:Ljava/lang/String;

    .line 1502262
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9X3;
    .locals 1

    .prologue
    .line 1502263
    const-class v0, LX/9X3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9X3;

    return-object v0
.end method

.method public static values()[LX/9X3;
    .locals 1

    .prologue
    .line 1502264
    sget-object v0, LX/9X3;->$VALUES:[LX/9X3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9X3;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502265
    iget-object v0, p0, LX/9X3;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502266
    sget-object v0, LX/9XC;->ACTION:LX/9XC;

    return-object v0
.end method
