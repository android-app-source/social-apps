.class public LX/8v6;
.super LX/8uk;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8uk",
        "<",
        "LX/8v8;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:Landroid/graphics/drawable/Drawable;

.field private final n:I

.field private final o:I

.field public final p:I

.field public final q:Landroid/text/TextPaint;

.field public final r:Landroid/content/res/ColorStateList;

.field public final s:Lcom/facebook/user/tiles/UserTileDrawableController;

.field private final t:LX/1FZ;

.field private u:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public v:Landroid/graphics/drawable/Drawable;

.field private w:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>(LX/8v8;Landroid/text/TextPaint;Landroid/content/res/Resources;Landroid/content/res/ColorStateList;IIIIILandroid/graphics/drawable/Drawable;IIZLcom/facebook/user/tiles/UserTileDrawableController;LX/1FZ;)V
    .locals 0

    .prologue
    .line 1416649
    invoke-direct {p0, p1, p3, p5, p13}, LX/8uk;-><init>(LX/8QK;Landroid/content/res/Resources;IZ)V

    .line 1416650
    iput-object p2, p0, LX/8v6;->q:Landroid/text/TextPaint;

    .line 1416651
    iput-object p4, p0, LX/8v6;->r:Landroid/content/res/ColorStateList;

    .line 1416652
    iput p6, p0, LX/8v6;->p:I

    .line 1416653
    iput p7, p0, LX/8v6;->j:I

    .line 1416654
    iput p8, p0, LX/8v6;->k:I

    .line 1416655
    iput p9, p0, LX/8v6;->l:I

    .line 1416656
    iput-object p10, p0, LX/8v6;->m:Landroid/graphics/drawable/Drawable;

    .line 1416657
    iput p11, p0, LX/8v6;->n:I

    .line 1416658
    iput p12, p0, LX/8v6;->o:I

    .line 1416659
    iput-object p14, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1416660
    iput-object p15, p0, LX/8v6;->t:LX/1FZ;

    .line 1416661
    return-void
.end method

.method public synthetic constructor <init>(LX/8v8;Landroid/text/TextPaint;Landroid/content/res/Resources;Landroid/content/res/ColorStateList;IIIIILandroid/graphics/drawable/Drawable;IIZLcom/facebook/user/tiles/UserTileDrawableController;LX/1FZ;B)V
    .locals 0

    .prologue
    .line 1416662
    invoke-direct/range {p0 .. p15}, LX/8v6;-><init>(LX/8v8;Landroid/text/TextPaint;Landroid/content/res/Resources;Landroid/content/res/ColorStateList;IIIIILandroid/graphics/drawable/Drawable;IIZLcom/facebook/user/tiles/UserTileDrawableController;LX/1FZ;)V

    return-void
.end method

.method public static a$redex0(LX/8v6;Landroid/graphics/drawable/Drawable;ILandroid/text/TextPaint;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;
    .locals 19
    .param p0    # LX/8v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1416583
    move-object/from16 v0, p0

    iget v2, v0, LX/8v6;->l:I

    add-int v5, p2, v2

    .line 1416584
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8uk;->f:LX/8QK;

    check-cast v2, LX/8v8;

    invoke-virtual {v2}, LX/8QK;->s()Z

    move-result v6

    .line 1416585
    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8v6;->w:Landroid/graphics/Rect;

    if-nez v2, :cond_0

    .line 1416586
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/8v6;->w:Landroid/graphics/Rect;

    .line 1416587
    :cond_0
    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, LX/8v6;->n:I

    move-object/from16 v0, p0

    iget v3, v0, LX/8v6;->o:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    move v3, v2

    .line 1416588
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, LX/8uk;->h:I

    sub-int/2addr v2, v5

    move-object/from16 v0, p0

    iget v4, v0, LX/8v6;->l:I

    sub-int/2addr v2, v4

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const-string v4, " "

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    sub-float v7, v2, v4

    .line 1416589
    const/4 v2, 0x0

    cmpg-float v2, v7, v2

    if-gez v2, :cond_2

    .line 1416590
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Space available to draw display name can not be negative"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1416591
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, LX/8v6;->j:I

    move v3, v2

    goto :goto_0

    .line 1416592
    :cond_2
    const-string v4, ""

    .line 1416593
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8uk;->f:LX/8QK;

    check-cast v2, LX/8v8;

    invoke-virtual {v2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v2

    .line 1416594
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1416595
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8uk;->i:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8uk;->g:Landroid/content/res/Resources;

    const v8, 0x7f0801a7

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    invoke-virtual {v4, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :cond_3
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p3

    invoke-static {v2, v0, v7, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 1416596
    :cond_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    int-to-float v7, v5

    add-float/2addr v2, v7

    int-to-float v7, v3

    add-float/2addr v2, v7

    float-to-int v7, v2

    .line 1416597
    int-to-float v2, v7

    const-string v8, " "

    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v8

    add-float/2addr v2, v8

    float-to-int v8, v2

    .line 1416598
    invoke-virtual/range {p3 .. p3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v9

    .line 1416599
    move-object/from16 v0, p0

    iget v2, v0, LX/8v6;->k:I

    mul-int/lit8 v2, v2, 0x2

    add-int v10, p2, v2

    .line 1416600
    const/4 v2, 0x0

    .line 1416601
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8v6;->u:LX/1FJ;

    if-eqz v11, :cond_6

    .line 1416602
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8v6;->u:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1416603
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    if-ne v11, v8, :cond_5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-eq v11, v10, :cond_6

    .line 1416604
    :cond_5
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8v6;->u:LX/1FJ;

    invoke-virtual {v11}, LX/1FJ;->close()V

    .line 1416605
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-object v11, v0, LX/8v6;->u:LX/1FJ;

    .line 1416606
    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8v6;->u:LX/1FJ;

    if-nez v11, :cond_7

    .line 1416607
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8v6;->t:LX/1FZ;

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v8, v10, v11}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/8v6;->u:LX/1FJ;

    .line 1416608
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8v6;->u:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1416609
    :cond_7
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 1416610
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1416611
    invoke-static {v8, v10}, Ljava/lang/Math;->min(II)I

    move-result v12

    int-to-float v12, v12

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    .line 1416612
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 1416613
    move-object/from16 v0, p0

    iget-object v14, v0, LX/8uk;->g:Landroid/content/res/Resources;

    const v15, 0x7f0a0231

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    .line 1416614
    invoke-virtual/range {p0 .. p0}, LX/8uk;->c()[I

    move-result-object v15

    move-object/from16 v0, p4

    invoke-virtual {v0, v15, v14}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setColor(I)V

    .line 1416615
    new-instance v14, Landroid/graphics/RectF;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, LX/8v6;->k:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v7

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8v6;->k:I

    move/from16 v18, v0

    add-int v18, v18, p2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-direct/range {v14 .. v18}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1416616
    invoke-virtual {v11, v14, v12, v12, v13}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1416617
    if-eqz p1, :cond_8

    .line 1416618
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget v13, v0, LX/8v6;->k:I

    move-object/from16 v0, p0

    iget v14, v0, LX/8v6;->k:I

    add-int v14, v14, p2

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v0, v12, v13, v1, v14}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1416619
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1416620
    :cond_8
    iget v9, v9, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int v9, v10, v9

    div-int/lit8 v9, v9, 0x2

    move-object/from16 v0, p0

    iget v12, v0, LX/8v6;->k:I

    sub-int/2addr v9, v12

    int-to-float v9, v9

    .line 1416621
    int-to-float v5, v5

    move-object/from16 v0, p3

    invoke-virtual {v11, v4, v5, v9, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1416622
    if-eqz v6, :cond_9

    .line 1416623
    sub-int v3, v7, v3

    move-object/from16 v0, p0

    iget v4, v0, LX/8v6;->o:I

    add-int/2addr v3, v4

    .line 1416624
    move-object/from16 v0, p0

    iget v4, v0, LX/8v6;->n:I

    sub-int v4, v10, v4

    div-int/lit8 v4, v4, 0x2

    .line 1416625
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8v6;->w:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v6, v0, LX/8v6;->n:I

    add-int/2addr v6, v3

    move-object/from16 v0, p0

    iget v7, v0, LX/8v6;->n:I

    add-int/2addr v7, v4

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 1416626
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8v6;->m:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8v6;->w:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1416627
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8v6;->m:Landroid/graphics/drawable/Drawable;

    const/16 v4, 0x88

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1416628
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8v6;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v11}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1416629
    :cond_9
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8uk;->g:Landroid/content/res/Resources;

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1416630
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8uk;->e:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8uk;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8uk;->e:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8uk;->e:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v7, v0, LX/8uk;->e:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v10

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 1416631
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4, v8, v10}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 1416632
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8uk;->f:LX/8QK;

    check-cast v2, LX/8v8;

    invoke-virtual {v2}, LX/8QK;->a()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xff

    :goto_1
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 1416633
    return-object v3

    .line 1416634
    :cond_a
    const/16 v2, 0x80

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1416579
    iget-object v0, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->d()V

    .line 1416580
    iget-object v0, p0, LX/8v6;->u:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1416581
    const/4 v0, 0x0

    iput-object v0, p0, LX/8v6;->u:LX/1FJ;

    .line 1416582
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1416635
    iget-object v0, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1416636
    iget-object v0, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    iget-object v1, p0, LX/8v6;->q:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    .line 1416637
    iget-object v2, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->p:LX/8ui;

    invoke-virtual {v2, v1}, LX/8ui;->a(F)V

    .line 1416638
    iget-object v0, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Z)V

    .line 1416639
    iget-object v1, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    iget-object v0, p0, LX/8uk;->f:LX/8QK;

    check-cast v0, LX/8v8;

    .line 1416640
    iget-object v2, v0, LX/8v8;->a:Lcom/facebook/user/model/User;

    move-object v0, v2

    .line 1416641
    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(LX/8t9;)V

    .line 1416642
    iget-object v0, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->c()V

    .line 1416643
    iget-object v0, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1416644
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1416645
    iget v1, p0, LX/8v6;->p:I

    iget-object v2, p0, LX/8v6;->q:Landroid/text/TextPaint;

    iget-object v3, p0, LX/8v6;->r:Landroid/content/res/ColorStateList;

    invoke-static {p0, v0, v1, v2, v3}, LX/8v6;->a$redex0(LX/8v6;Landroid/graphics/drawable/Drawable;ILandroid/text/TextPaint;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/8v6;->v:Landroid/graphics/drawable/Drawable;

    .line 1416646
    iget-object v0, p0, LX/8v6;->s:Lcom/facebook/user/tiles/UserTileDrawableController;

    new-instance v1, LX/8v4;

    invoke-direct {v1, p0}, LX/8v4;-><init>(LX/8v6;)V

    .line 1416647
    iput-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->B:LX/8t6;

    .line 1416648
    return-void
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1416573
    iget-object v0, p0, LX/8uk;->f:LX/8QK;

    check-cast v0, LX/8v8;

    .line 1416574
    iget-boolean v1, v0, LX/8QK;->a:Z

    move v0, v1

    .line 1416575
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8v6;->w:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    .line 1416576
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 1416577
    :goto_0
    return-void

    .line 1416578
    :cond_1
    iget-object v0, p0, LX/8v6;->w:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 3

    .prologue
    .line 1416566
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1416567
    iget-object v0, p0, LX/8v6;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, p8, v0

    .line 1416568
    int-to-float v1, v0

    invoke-virtual {p1, p5, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1416569
    iget-object v1, p0, LX/8uk;->e:Landroid/graphics/Rect;

    float-to-int v2, p5

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1416570
    iget-object v0, p0, LX/8v6;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1416571
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1416572
    return-void
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2
    .param p5    # Landroid/graphics/Paint$FontMetricsInt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1416556
    check-cast p1, Landroid/text/TextPaint;

    .line 1416557
    if-eqz p5, :cond_0

    .line 1416558
    invoke-virtual {p1, p5}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    .line 1416559
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v1, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 1416560
    iget-object v1, p0, LX/8v6;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    sub-float v0, v1, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 1416561
    iget v1, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    int-to-float v1, v1

    sub-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 1416562
    iget v1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 1416563
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 1416564
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 1416565
    :cond_0
    iget-object v0, p0, LX/8v6;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method
