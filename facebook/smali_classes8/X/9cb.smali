.class public final LX/9cb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/4m4;

.field public final synthetic b:LX/9cg;


# direct methods
.method public constructor <init>(LX/9cg;LX/4m4;)V
    .locals 0

    .prologue
    .line 1516631
    iput-object p1, p0, LX/9cb;->b:LX/9cg;

    iput-object p2, p0, LX/9cb;->a:LX/4m4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x6a17290e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1516621
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/9cb;->b:LX/9cg;

    invoke-virtual {v2}, LX/9cg;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/stickers/store/StickerStoreActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1516622
    const-string v2, "stickerContext"

    iget-object v3, p0, LX/9cb;->a:LX/4m4;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1516623
    iget-object v2, p0, LX/9cb;->b:LX/9cg;

    iget-object v2, v2, LX/9cg;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/9cb;->b:LX/9cg;

    invoke-virtual {v3}, LX/9cg;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1516624
    iget-object v1, p0, LX/9cb;->b:LX/9cg;

    iget-object v1, v1, LX/9cg;->d:LX/9c3;

    iget-object v2, p0, LX/9cb;->b:LX/9cg;

    iget-object v2, v2, LX/9cg;->n:Ljava/lang/String;

    sget-object v3, LX/9c2;->ENTER_STICKER_STORE:LX/9c2;

    invoke-virtual {v1, v2, v3}, LX/9c3;->a(Ljava/lang/String;LX/9c2;)V

    .line 1516625
    iget-object v1, p0, LX/9cb;->b:LX/9cg;

    iget-object v1, v1, LX/9cg;->e:LX/9c7;

    .line 1516626
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/9c4;->COMPOSER_STICKERS_ENTER_STORE:LX/9c4;

    invoke-virtual {v3}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "composer"

    .line 1516627
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1516628
    move-object v2, v2

    .line 1516629
    invoke-static {v1, v2}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1516630
    const v1, 0x2bfe24ef

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
