.class public final LX/9qZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1547487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/9qZ;
    .locals 2

    .prologue
    .line 1547488
    new-instance v0, LX/9qZ;

    invoke-direct {v0}, LX/9qZ;-><init>()V

    .line 1547489
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/9qZ;->a:Z

    .line 1547490
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->m()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v1

    iput-object v1, v0, LX/9qZ;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    .line 1547491
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/9qZ;->c:Z

    .line 1547492
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9qZ;->d:Ljava/lang/String;

    .line 1547493
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gP_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9qZ;->e:Ljava/lang/String;

    .line 1547494
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->n()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v1

    iput-object v1, v0, LX/9qZ;->f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1547495
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->o()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    move-result-object v1

    iput-object v1, v0, LX/9qZ;->g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    .line 1547496
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->p()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    move-result-object v1

    iput-object v1, v0, LX/9qZ;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    .line 1547497
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9qZ;->i:Ljava/lang/String;

    .line 1547498
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 1547499
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1547500
    iget-object v1, p0, LX/9qZ;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1547501
    iget-object v3, p0, LX/9qZ;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1547502
    iget-object v5, p0, LX/9qZ;->e:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1547503
    iget-object v6, p0, LX/9qZ;->f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1547504
    iget-object v7, p0, LX/9qZ;->g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1547505
    iget-object v8, p0, LX/9qZ;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1547506
    iget-object v9, p0, LX/9qZ;->i:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1547507
    const/16 v10, 0x9

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1547508
    iget-boolean v10, p0, LX/9qZ;->a:Z

    invoke-virtual {v0, v11, v10}, LX/186;->a(IZ)V

    .line 1547509
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1547510
    const/4 v1, 0x2

    iget-boolean v10, p0, LX/9qZ;->c:Z

    invoke-virtual {v0, v1, v10}, LX/186;->a(IZ)V

    .line 1547511
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1547512
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1547513
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1547514
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1547515
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1547516
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1547517
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1547518
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1547519
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1547520
    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1547521
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1547522
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;-><init>(LX/15i;)V

    .line 1547523
    return-object v1
.end method
