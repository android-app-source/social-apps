.class public LX/9D7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Uf;

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field public c:Landroid/view/ViewStub;


# direct methods
.method public constructor <init>(LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1454760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1454761
    iput-object p1, p0, LX/9D7;->a:LX/1Uf;

    .line 1454762
    return-void
.end method

.method public static a(LX/0QB;)LX/9D7;
    .locals 2

    .prologue
    .line 1454763
    new-instance v1, LX/9D7;

    invoke-static {p0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v0

    check-cast v0, LX/1Uf;

    invoke-direct {v1, v0}, LX/9D7;-><init>(LX/1Uf;)V

    .line 1454764
    move-object v0, v1

    .line 1454765
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1454766
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v0, 0x0

    .line 1454767
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1454768
    :cond_0
    iget-object v0, p0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_1

    .line 1454769
    iget-object v0, p0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1454770
    :cond_1
    :goto_0
    return-void

    .line 1454771
    :cond_2
    iget-object v1, p0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    if-nez v1, :cond_3

    .line 1454772
    iget-object v1, p0, LX/9D7;->c:Landroid/view/ViewStub;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1454773
    iget-object v1, p0, LX/9D7;->c:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1454774
    :cond_3
    iget-object v1, p0, LX/9D7;->a:LX/1Uf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v1

    .line 1454775
    iget-object v2, p0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1454776
    iget-object v0, p0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1454777
    iget-object v0, p0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 1454778
    :cond_4
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    goto :goto_1
.end method
