.class public LX/8mf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8mf;


# instance fields
.field public a:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1399702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1399703
    iput-object p1, p0, LX/8mf;->a:LX/0So;

    .line 1399704
    return-void
.end method

.method public static a(LX/0QB;)LX/8mf;
    .locals 4

    .prologue
    .line 1399705
    sget-object v0, LX/8mf;->b:LX/8mf;

    if-nez v0, :cond_1

    .line 1399706
    const-class v1, LX/8mf;

    monitor-enter v1

    .line 1399707
    :try_start_0
    sget-object v0, LX/8mf;->b:LX/8mf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1399708
    if-eqz v2, :cond_0

    .line 1399709
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1399710
    new-instance p0, LX/8mf;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/8mf;-><init>(LX/0So;)V

    .line 1399711
    move-object v0, p0

    .line 1399712
    sput-object v0, LX/8mf;->b:LX/8mf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1399713
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1399714
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1399715
    :cond_1
    sget-object v0, LX/8mf;->b:LX/8mf;

    return-object v0

    .line 1399716
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1399717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
