.class public LX/9jN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

.field public g:LX/9jM;

.field public h:Z

.field public i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1529764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529765
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9jN;->a:Ljava/util/List;

    .line 1529766
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9jN;->b:Ljava/util/List;

    .line 1529767
    const-string v0, ""

    iput-object v0, p0, LX/9jN;->c:Ljava/lang/String;

    .line 1529768
    iput-boolean v1, p0, LX/9jN;->e:Z

    .line 1529769
    sget-object v0, LX/9jM;->TRADITIONAL:LX/9jM;

    iput-object v0, p0, LX/9jN;->g:LX/9jM;

    .line 1529770
    iput-boolean v1, p0, LX/9jN;->h:Z

    .line 1529771
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1529772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529773
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9jN;->a:Ljava/util/List;

    .line 1529774
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9jN;->b:Ljava/util/List;

    .line 1529775
    const-string v0, ""

    iput-object v0, p0, LX/9jN;->c:Ljava/lang/String;

    .line 1529776
    iput-boolean v1, p0, LX/9jN;->e:Z

    .line 1529777
    sget-object v0, LX/9jM;->TRADITIONAL:LX/9jM;

    iput-object v0, p0, LX/9jN;->g:LX/9jM;

    .line 1529778
    iput-boolean v1, p0, LX/9jN;->h:Z

    .line 1529779
    iput-object p1, p0, LX/9jN;->a:Ljava/util/List;

    .line 1529780
    iput-object p1, p0, LX/9jN;->b:Ljava/util/List;

    .line 1529781
    return-void
.end method
