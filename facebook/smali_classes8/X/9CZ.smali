.class public final LX/9CZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:J

.field public final synthetic d:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Landroid/app/ProgressDialog;Lcom/facebook/graphql/model/GraphQLFeedback;J)V
    .locals 0

    .prologue
    .line 1454163
    iput-object p1, p0, LX/9CZ;->d:LX/9Cd;

    iput-object p2, p0, LX/9CZ;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, LX/9CZ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-wide p4, p0, LX/9CZ;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454164
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 1454165
    iget-object v0, p0, LX/9CZ;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->d:LX/3iL;

    invoke-virtual {v0, p2}, LX/3iL;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1454166
    iget-object v0, p0, LX/9CZ;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1454167
    iget-object v0, p0, LX/9CZ;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->e:LX/0hx;

    iget-object v1, p0, LX/9CZ;->d:LX/9Cd;

    iget-object v1, v1, LX/9Cd;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/9CZ;->c:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 1454168
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1454169
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1454170
    iget-object v0, p0, LX/9CZ;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1454171
    iget-object v0, p0, LX/9CZ;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->b:LX/1K9;

    new-instance v1, LX/8py;

    iget-object v2, p0, LX/9CZ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8py;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1454172
    iget-object v0, p0, LX/9CZ;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->e:LX/0hx;

    iget-object v1, p0, LX/9CZ;->d:LX/9Cd;

    iget-object v1, v1, LX/9Cd;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/9CZ;->c:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 1454173
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454174
    return-void
.end method
