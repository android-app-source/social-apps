.class public final enum LX/8m8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8m8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8m8;

.field public static final enum HDPI:LX/8m8;

.field public static final enum MDPI:LX/8m8;

.field public static final enum XHDPI:LX/8m8;


# instance fields
.field public final numericValue:F

.field public final stringValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1398411
    new-instance v0, LX/8m8;

    const-string v1, "MDPI"

    const/high16 v2, 0x3f800000    # 1.0f

    const-string v3, "1"

    invoke-direct {v0, v1, v4, v2, v3}, LX/8m8;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, LX/8m8;->MDPI:LX/8m8;

    .line 1398412
    new-instance v0, LX/8m8;

    const-string v1, "HDPI"

    const/high16 v2, 0x3fc00000    # 1.5f

    const-string v3, "1.5"

    invoke-direct {v0, v1, v5, v2, v3}, LX/8m8;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, LX/8m8;->HDPI:LX/8m8;

    .line 1398413
    new-instance v0, LX/8m8;

    const-string v1, "XHDPI"

    const/high16 v2, 0x40000000    # 2.0f

    const-string v3, "2"

    invoke-direct {v0, v1, v6, v2, v3}, LX/8m8;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, LX/8m8;->XHDPI:LX/8m8;

    .line 1398414
    const/4 v0, 0x3

    new-array v0, v0, [LX/8m8;

    sget-object v1, LX/8m8;->MDPI:LX/8m8;

    aput-object v1, v0, v4

    sget-object v1, LX/8m8;->HDPI:LX/8m8;

    aput-object v1, v0, v5

    sget-object v1, LX/8m8;->XHDPI:LX/8m8;

    aput-object v1, v0, v6

    sput-object v0, LX/8m8;->$VALUES:[LX/8m8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1398415
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1398416
    iput p3, p0, LX/8m8;->numericValue:F

    .line 1398417
    iput-object p4, p0, LX/8m8;->stringValue:Ljava/lang/String;

    .line 1398418
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8m8;
    .locals 1

    .prologue
    .line 1398419
    const-class v0, LX/8m8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8m8;

    return-object v0
.end method

.method public static values()[LX/8m8;
    .locals 1

    .prologue
    .line 1398420
    sget-object v0, LX/8m8;->$VALUES:[LX/8m8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8m8;

    return-object v0
.end method
