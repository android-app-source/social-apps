.class public LX/929;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/1Ck;

.field public c:LX/03V;

.field public d:LX/0tX;

.field public e:LX/92T;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1432008
    const-class v0, LX/929;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/929;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0tX;LX/92T;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432010
    iput-object p1, p0, LX/929;->b:LX/1Ck;

    .line 1432011
    iput-object p2, p0, LX/929;->d:LX/0tX;

    .line 1432012
    iput-object p3, p0, LX/929;->e:LX/92T;

    .line 1432013
    iput-object p4, p0, LX/929;->c:LX/03V;

    .line 1432014
    return-void
.end method

.method public static b(LX/0QB;)LX/929;
    .locals 5

    .prologue
    .line 1432015
    new-instance v4, LX/929;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/92T;->b(LX/0QB;)LX/92T;

    move-result-object v2

    check-cast v2, LX/92T;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, v0, v1, v2, v3}, LX/929;-><init>(LX/1Ck;LX/0tX;LX/92T;LX/03V;)V

    .line 1432016
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1432017
    iget-object v0, p0, LX/929;->b:LX/1Ck;

    .line 1432018
    new-instance v1, LX/5LA;

    invoke-direct {v1}, LX/5LA;-><init>()V

    move-object v1, v1

    .line 1432019
    const-string v2, "legacy_activity_api_id"

    invoke-virtual {v1, v2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "object_id"

    invoke-virtual {v2, v3, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1432020
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1432021
    iget-object v2, p0, LX/929;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/927;

    invoke-direct {v2, p0}, LX/927;-><init>(LX/929;)V

    invoke-static {v1, v2, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 1432022
    new-instance v2, LX/928;

    invoke-direct {v2, p0, p5}, LX/928;-><init>(LX/929;LX/0Ve;)V

    move-object v2, v2

    .line 1432023
    invoke-virtual {v0, p1, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1432024
    return-void
.end method
