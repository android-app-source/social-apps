.class public final LX/8kR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 45

    .prologue
    .line 1396181
    const/16 v39, 0x0

    .line 1396182
    const/16 v38, 0x0

    .line 1396183
    const/16 v37, 0x0

    .line 1396184
    const/16 v36, 0x0

    .line 1396185
    const/16 v35, 0x0

    .line 1396186
    const/16 v34, 0x0

    .line 1396187
    const/16 v33, 0x0

    .line 1396188
    const/16 v32, 0x0

    .line 1396189
    const/16 v31, 0x0

    .line 1396190
    const/16 v30, 0x0

    .line 1396191
    const/16 v29, 0x0

    .line 1396192
    const/16 v28, 0x0

    .line 1396193
    const/16 v27, 0x0

    .line 1396194
    const/16 v26, 0x0

    .line 1396195
    const/16 v25, 0x0

    .line 1396196
    const/16 v24, 0x0

    .line 1396197
    const/16 v23, 0x0

    .line 1396198
    const/16 v22, 0x0

    .line 1396199
    const/16 v21, 0x0

    .line 1396200
    const/16 v20, 0x0

    .line 1396201
    const/16 v17, 0x0

    .line 1396202
    const-wide/16 v18, 0x0

    .line 1396203
    const/16 v16, 0x0

    .line 1396204
    const/4 v15, 0x0

    .line 1396205
    const/4 v14, 0x0

    .line 1396206
    const/4 v13, 0x0

    .line 1396207
    const/4 v12, 0x0

    .line 1396208
    const/4 v11, 0x0

    .line 1396209
    const/4 v10, 0x0

    .line 1396210
    const/4 v9, 0x0

    .line 1396211
    const/4 v8, 0x0

    .line 1396212
    const/4 v7, 0x0

    .line 1396213
    const/4 v6, 0x0

    .line 1396214
    const/4 v5, 0x0

    .line 1396215
    const/4 v4, 0x0

    .line 1396216
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_25

    .line 1396217
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1396218
    const/4 v4, 0x0

    .line 1396219
    :goto_0
    return v4

    .line 1396220
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1396221
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_17

    .line 1396222
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v40

    .line 1396223
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1396224
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    if-eqz v40, :cond_1

    .line 1396225
    const-string v41, "artist"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_2

    .line 1396226
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    goto :goto_1

    .line 1396227
    :cond_2
    const-string v41, "can_download"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_3

    .line 1396228
    const/16 v18, 0x1

    .line 1396229
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 1396230
    :cond_3
    const-string v41, "copyrights"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_4

    .line 1396231
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v37

    goto :goto_1

    .line 1396232
    :cond_4
    const-string v41, "description"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_5

    .line 1396233
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    goto :goto_1

    .line 1396234
    :cond_5
    const-string v41, "id"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_6

    .line 1396235
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    goto :goto_1

    .line 1396236
    :cond_6
    const-string v41, "in_sticker_tray"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_7

    .line 1396237
    const/16 v17, 0x1

    .line 1396238
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 1396239
    :cond_7
    const-string v41, "is_auto_downloadable"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_8

    .line 1396240
    const/16 v16, 0x1

    .line 1396241
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 1396242
    :cond_8
    const-string v41, "is_comments_capable"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_9

    .line 1396243
    const/4 v15, 0x1

    .line 1396244
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 1396245
    :cond_9
    const-string v41, "is_composer_capable"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_a

    .line 1396246
    const/4 v14, 0x1

    .line 1396247
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 1396248
    :cond_a
    const-string v41, "is_featured"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_b

    .line 1396249
    const/4 v13, 0x1

    .line 1396250
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto/16 :goto_1

    .line 1396251
    :cond_b
    const-string v41, "is_messenger_capable"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_c

    .line 1396252
    const/4 v12, 0x1

    .line 1396253
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 1396254
    :cond_c
    const-string v41, "is_montage_capable"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_d

    .line 1396255
    const/4 v11, 0x1

    .line 1396256
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 1396257
    :cond_d
    const-string v41, "is_posts_capable"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_e

    .line 1396258
    const/4 v10, 0x1

    .line 1396259
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 1396260
    :cond_e
    const-string v41, "is_promoted"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_f

    .line 1396261
    const/4 v9, 0x1

    .line 1396262
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 1396263
    :cond_f
    const-string v41, "is_sms_capable"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_10

    .line 1396264
    const/4 v8, 0x1

    .line 1396265
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto/16 :goto_1

    .line 1396266
    :cond_10
    const-string v41, "name"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_11

    .line 1396267
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 1396268
    :cond_11
    const-string v41, "preview_image"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_12

    .line 1396269
    invoke-static/range {p0 .. p1}, LX/8kN;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 1396270
    :cond_12
    const-string v41, "price"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_13

    .line 1396271
    const/4 v5, 0x1

    .line 1396272
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v22

    goto/16 :goto_1

    .line 1396273
    :cond_13
    const-string v41, "stickers"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_14

    .line 1396274
    invoke-static/range {p0 .. p1}, LX/8kP;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1396275
    :cond_14
    const-string v41, "thumbnail_image"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_15

    .line 1396276
    invoke-static/range {p0 .. p1}, LX/8kN;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1396277
    :cond_15
    const-string v41, "tray_button"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_16

    .line 1396278
    invoke-static/range {p0 .. p1}, LX/8kQ;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1396279
    :cond_16
    const-string v41, "updated_time"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_0

    .line 1396280
    const/4 v4, 0x1

    .line 1396281
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    goto/16 :goto_1

    .line 1396282
    :cond_17
    const/16 v40, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1396283
    const/16 v40, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1396284
    if-eqz v18, :cond_18

    .line 1396285
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1396286
    :cond_18
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1396287
    const/16 v18, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1396288
    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1396289
    if-eqz v17, :cond_19

    .line 1396290
    const/16 v17, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1396291
    :cond_19
    if-eqz v16, :cond_1a

    .line 1396292
    const/16 v16, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1396293
    :cond_1a
    if-eqz v15, :cond_1b

    .line 1396294
    const/4 v15, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 1396295
    :cond_1b
    if-eqz v14, :cond_1c

    .line 1396296
    const/16 v14, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 1396297
    :cond_1c
    if-eqz v13, :cond_1d

    .line 1396298
    const/16 v13, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 1396299
    :cond_1d
    if-eqz v12, :cond_1e

    .line 1396300
    const/16 v12, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 1396301
    :cond_1e
    if-eqz v11, :cond_1f

    .line 1396302
    const/16 v11, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1396303
    :cond_1f
    if-eqz v10, :cond_20

    .line 1396304
    const/16 v10, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1396305
    :cond_20
    if-eqz v9, :cond_21

    .line 1396306
    const/16 v9, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1396307
    :cond_21
    if-eqz v8, :cond_22

    .line 1396308
    const/16 v8, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1396309
    :cond_22
    const/16 v8, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1396310
    const/16 v8, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1396311
    if-eqz v5, :cond_23

    .line 1396312
    const/16 v5, 0x11

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1, v8}, LX/186;->a(III)V

    .line 1396313
    :cond_23
    const/16 v5, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1396314
    const/16 v5, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1396315
    const/16 v5, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1396316
    if-eqz v4, :cond_24

    .line 1396317
    const/16 v5, 0x15

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 1396318
    :cond_24
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_25
    move/from16 v43, v6

    move/from16 v44, v7

    move-wide/from16 v6, v18

    move/from16 v18, v16

    move/from16 v19, v17

    move/from16 v16, v14

    move/from16 v17, v15

    move v14, v12

    move v15, v13

    move v13, v11

    move v12, v10

    move v11, v9

    move v10, v8

    move/from16 v9, v44

    move/from16 v8, v43

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1396424
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1396425
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1396426
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/8kR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1396427
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1396428
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1396429
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1396430
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1396431
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1396432
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1396433
    invoke-static {p0, p1}, LX/8kR;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1396434
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1396435
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1396319
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1396320
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1396321
    if-eqz v0, :cond_0

    .line 1396322
    const-string v1, "artist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396323
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1396324
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396325
    if-eqz v0, :cond_1

    .line 1396326
    const-string v1, "can_download"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396327
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396328
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1396329
    if-eqz v0, :cond_2

    .line 1396330
    const-string v0, "copyrights"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396331
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1396332
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1396333
    if-eqz v0, :cond_3

    .line 1396334
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396335
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1396336
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1396337
    if-eqz v0, :cond_4

    .line 1396338
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396339
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1396340
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396341
    if-eqz v0, :cond_5

    .line 1396342
    const-string v1, "in_sticker_tray"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396343
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396344
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396345
    if-eqz v0, :cond_6

    .line 1396346
    const-string v1, "is_auto_downloadable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396347
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396348
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396349
    if-eqz v0, :cond_7

    .line 1396350
    const-string v1, "is_comments_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396351
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396352
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396353
    if-eqz v0, :cond_8

    .line 1396354
    const-string v1, "is_composer_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396355
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396356
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396357
    if-eqz v0, :cond_9

    .line 1396358
    const-string v1, "is_featured"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396359
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396360
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396361
    if-eqz v0, :cond_a

    .line 1396362
    const-string v1, "is_messenger_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396363
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396364
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396365
    if-eqz v0, :cond_b

    .line 1396366
    const-string v1, "is_montage_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396367
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396368
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396369
    if-eqz v0, :cond_c

    .line 1396370
    const-string v1, "is_posts_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396371
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396372
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396373
    if-eqz v0, :cond_d

    .line 1396374
    const-string v1, "is_promoted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396375
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396376
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396377
    if-eqz v0, :cond_e

    .line 1396378
    const-string v1, "is_sms_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396379
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396380
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1396381
    if-eqz v0, :cond_f

    .line 1396382
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396383
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1396384
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396385
    if-eqz v0, :cond_10

    .line 1396386
    const-string v1, "preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396387
    invoke-static {p0, v0, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396388
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1396389
    if-eqz v0, :cond_11

    .line 1396390
    const-string v1, "price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396391
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1396392
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396393
    if-eqz v0, :cond_14

    .line 1396394
    const-string v1, "stickers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396395
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1396396
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1396397
    if-eqz v1, :cond_13

    .line 1396398
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396399
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1396400
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v0

    if-ge v2, v0, :cond_12

    .line 1396401
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v0

    invoke-static {p0, v0, p2}, LX/8kO;->a(LX/15i;ILX/0nX;)V

    .line 1396402
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1396403
    :cond_12
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1396404
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1396405
    :cond_14
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396406
    if-eqz v0, :cond_15

    .line 1396407
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396408
    invoke-static {p0, v0, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396409
    :cond_15
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396410
    if-eqz v0, :cond_17

    .line 1396411
    const-string v1, "tray_button"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396412
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1396413
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1396414
    if-eqz v1, :cond_16

    .line 1396415
    const-string v2, "normal"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396416
    invoke-static {p0, v1, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396417
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1396418
    :cond_17
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1396419
    cmp-long v2, v0, v4

    if-eqz v2, :cond_18

    .line 1396420
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396421
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1396422
    :cond_18
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1396423
    return-void
.end method
