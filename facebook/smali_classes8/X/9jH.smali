.class public LX/9jH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529724
    iput-object p1, p0, LX/9jH;->a:LX/0Or;

    .line 1529725
    iput-object p2, p0, LX/9jH;->b:LX/0ad;

    .line 1529726
    return-void
.end method

.method public static a(LX/9jG;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)LX/9jF;
    .locals 3
    .param p2    # Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1529705
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v0

    .line 1529706
    iput-object p0, v0, LX/9jF;->q:LX/9jG;

    .line 1529707
    move-object v0, v0

    .line 1529708
    iput-object p1, v0, LX/9jF;->l:Ljava/lang/String;

    .line 1529709
    move-object v0, v0

    .line 1529710
    if-eqz p2, :cond_0

    .line 1529711
    new-instance v1, LX/5m9;

    invoke-direct {v1}, LX/5m9;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1529712
    iput-object v2, v1, LX/5m9;->f:Ljava/lang/String;

    .line 1529713
    move-object v1, v1

    .line 1529714
    invoke-virtual {p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 1529715
    iput-object v2, v1, LX/5m9;->h:Ljava/lang/String;

    .line 1529716
    move-object v1, v1

    .line 1529717
    invoke-virtual {v1}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    .line 1529718
    iput-object v1, v0, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1529719
    :goto_0
    return-object v0

    .line 1529720
    :cond_0
    const/4 v1, 0x1

    .line 1529721
    iput-boolean v1, v0, LX/9jF;->h:Z

    .line 1529722
    goto :goto_0
.end method

.method public static a(LX/9jH;LX/9jG;Ljava/lang/String;)LX/9jF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1529679
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v0

    .line 1529680
    iput-object p1, v0, LX/9jF;->q:LX/9jG;

    .line 1529681
    move-object v0, v0

    .line 1529682
    iput-object p2, v0, LX/9jF;->l:Ljava/lang/String;

    .line 1529683
    move-object v0, v0

    .line 1529684
    iput-boolean v3, v0, LX/9jF;->h:Z

    .line 1529685
    move-object v0, v0

    .line 1529686
    iget-object v1, p0, LX/9jH;->b:LX/0ad;

    sget-short v2, LX/5HH;->i:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1529687
    iput-boolean v3, v0, LX/9jF;->a:Z

    .line 1529688
    :cond_0
    iget-object v1, p0, LX/9jH;->b:LX/0ad;

    sget-short v2, LX/5HH;->j:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1529689
    iput-boolean v3, v0, LX/9jF;->b:Z

    .line 1529690
    :cond_1
    return-object v0
.end method

.method public static b(LX/0QB;)LX/9jH;
    .locals 3

    .prologue
    .line 1529703
    new-instance v1, LX/9jH;

    const/16 v0, 0x455

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v2, v0}, LX/9jH;-><init>(LX/0Or;LX/0ad;)V

    .line 1529704
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1529697
    sget-object v0, LX/9jG;->SOCIAL_SEARCH_CONVERSION:LX/9jG;

    const-string v1, "convert_to_social_search_post"

    invoke-static {p0, v0, v1}, LX/9jH;->a(LX/9jH;LX/9jG;Ljava/lang/String;)LX/9jF;

    move-result-object v0

    .line 1529698
    iput-object p2, v0, LX/9jF;->w:Ljava/lang/String;

    .line 1529699
    move-object v0, v0

    .line 1529700
    invoke-virtual {v0}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v1

    .line 1529701
    iget-object v0, p0, LX/9jH;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1, v1}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x138a

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1529702
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;Landroid/app/Activity;)V
    .locals 3
    .param p2    # Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1529691
    sget-object v0, LX/9jG;->PHOTO:LX/9jG;

    const-string v1, "edit_photo_location"

    invoke-static {v0, v1, p2}, LX/9jH;->a(LX/9jG;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)LX/9jF;

    move-result-object v0

    .line 1529692
    iput-object p1, v0, LX/9jF;->n:Ljava/lang/String;

    .line 1529693
    move-object v0, v0

    .line 1529694
    invoke-virtual {v0}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v1

    .line 1529695
    iget-object v0, p0, LX/9jH;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p3, v1}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x138a

    invoke-interface {v0, v1, v2, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1529696
    return-void
.end method
