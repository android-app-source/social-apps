.class public final LX/9Kv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$LocationPickerSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1468987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;
    .locals 15

    .prologue
    const/4 v4, 0x1

    const/4 v14, 0x0

    const/4 v2, 0x0

    .line 1468988
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1468989
    iget-object v1, p0, LX/9Kv;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1468990
    iget-object v3, p0, LX/9Kv;->b:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1468991
    iget-object v5, p0, LX/9Kv;->c:LX/0Px;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 1468992
    iget-object v6, p0, LX/9Kv;->d:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1468993
    iget-object v7, p0, LX/9Kv;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1468994
    iget-object v8, p0, LX/9Kv;->h:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$LocationPickerSettingModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1468995
    iget-object v9, p0, LX/9Kv;->i:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1468996
    iget-object v10, p0, LX/9Kv;->j:LX/0Px;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 1468997
    iget-object v11, p0, LX/9Kv;->k:LX/0Px;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/util/List;)I

    move-result v11

    .line 1468998
    iget-object v12, p0, LX/9Kv;->l:Ljava/lang/String;

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1468999
    const/16 v13, 0xe

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1469000
    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 1469001
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1469002
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1469003
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1469004
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1469005
    const/4 v1, 0x5

    iget-boolean v3, p0, LX/9Kv;->f:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1469006
    const/4 v1, 0x6

    iget-boolean v3, p0, LX/9Kv;->g:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1469007
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1469008
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1469009
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1469010
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1469011
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1469012
    const/16 v1, 0xc

    iget-boolean v3, p0, LX/9Kv;->m:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1469013
    const/16 v1, 0xd

    iget-boolean v3, p0, LX/9Kv;->n:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1469014
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1469015
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1469016
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1469017
    invoke-virtual {v1, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1469018
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1469019
    new-instance v1, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    invoke-direct {v1, v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;-><init>(LX/15i;)V

    .line 1469020
    return-object v1
.end method
