.class public final LX/9MK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1473443
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1473444
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1473445
    :goto_0
    return v1

    .line 1473446
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1473447
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1473448
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1473449
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1473450
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1473451
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1473452
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 1473453
    :cond_3
    const-string v4, "group_story_topics"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1473454
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1473455
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_c

    .line 1473456
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1473457
    :goto_2
    move v0, v3

    .line 1473458
    goto :goto_1

    .line 1473459
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1473460
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1473461
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1473462
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1473463
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 1473464
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1473465
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1473466
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_6

    if-eqz v7, :cond_6

    .line 1473467
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1473468
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v4

    goto :goto_3

    .line 1473469
    :cond_7
    const-string v8, "nodes"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1473470
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1473471
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_8

    .line 1473472
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_8

    .line 1473473
    invoke-static {p0, p1}, LX/9MJ;->b(LX/15w;LX/186;)I

    move-result v7

    .line 1473474
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1473475
    :cond_8
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1473476
    goto :goto_3

    .line 1473477
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1473478
    :cond_a
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1473479
    if-eqz v0, :cond_b

    .line 1473480
    invoke-virtual {p1, v3, v6, v3}, LX/186;->a(III)V

    .line 1473481
    :cond_b
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1473482
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_c
    move v0, v3

    move v5, v3

    move v6, v3

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1473483
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1473484
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1473485
    if-eqz v0, :cond_0

    .line 1473486
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473487
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1473488
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1473489
    if-eqz v0, :cond_4

    .line 1473490
    const-string v1, "group_story_topics"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473491
    const/4 v1, 0x0

    .line 1473492
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1473493
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1473494
    if-eqz v1, :cond_1

    .line 1473495
    const-string p1, "count"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473496
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1473497
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1473498
    if-eqz v1, :cond_3

    .line 1473499
    const-string p1, "nodes"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473500
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1473501
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 1473502
    invoke-virtual {p0, v1, p1}, LX/15i;->q(II)I

    move-result v0

    invoke-static {p0, v0, p2}, LX/9MJ;->a(LX/15i;ILX/0nX;)V

    .line 1473503
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1473504
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1473505
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1473506
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1473507
    return-void
.end method
