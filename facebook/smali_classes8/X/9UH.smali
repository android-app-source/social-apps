.class public final LX/9UH;
.super Landroid/content/AsyncQueryHandler;
.source ""


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1497766
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 1497767
    iput-object p1, p0, LX/9UH;->a:Ljava/lang/ref/WeakReference;

    .line 1497768
    return-void
.end method


# virtual methods
.method public final onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 1497769
    iget-object v0, p0, LX/9UH;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    .line 1497770
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1497771
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1497772
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1497773
    :goto_0
    return-void

    .line 1497774
    :cond_0
    invoke-virtual {v1, p3}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 1497775
    iget-object v1, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    check-cast v1, LX/9UD;

    .line 1497776
    iput-object p3, v1, LX/9UD;->d:Landroid/database/Cursor;

    .line 1497777
    invoke-static {v0}, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->b(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;)V

    goto :goto_0
.end method
