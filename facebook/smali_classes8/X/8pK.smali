.class public final LX/8pK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1405837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1405815
    if-nez p1, :cond_0

    .line 1405816
    :goto_0
    return v0

    .line 1405817
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v1

    const/4 v2, 0x0

    .line 1405818
    if-nez v1, :cond_1

    .line 1405819
    :goto_1
    move v1, v2

    .line 1405820
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1405821
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1405822
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1405823
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1405824
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1405825
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1405826
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1405827
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1405828
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1405829
    const/4 p1, 0x5

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1405830
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1405831
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1405832
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1405833
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1405834
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1405835
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1405836
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTranslatabilityGraphQL"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1405803
    if-nez p0, :cond_1

    .line 1405804
    :cond_0
    :goto_0
    return-object v2

    .line 1405805
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1405806
    invoke-static {v0, p0}, LX/8pK;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v1

    .line 1405807
    if-eqz v1, :cond_0

    .line 1405808
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1405809
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1405810
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1405811
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1405812
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1405813
    const-string v1, "TranslationConverter.getTranslatabilityGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1405814
    :cond_2
    new-instance v2, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0
.end method
