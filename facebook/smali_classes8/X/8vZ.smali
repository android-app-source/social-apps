.class public LX/8vZ;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field private a:Landroid/os/Parcelable;

.field public final synthetic b:LX/8vi;


# direct methods
.method public constructor <init>(LX/8vi;)V
    .locals 1

    .prologue
    .line 1417139
    iput-object p1, p0, LX/8vZ;->b:LX/8vi;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 1417140
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vZ;->a:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 1417141
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8vi;->aj:Z

    .line 1417142
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iget-object v1, p0, LX/8vZ;->b:LX/8vi;

    iget v1, v1, LX/8vi;->ao:I

    iput v1, v0, LX/8vi;->ap:I

    .line 1417143
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iget-object v1, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v1}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iput v1, v0, LX/8vi;->ao:I

    .line 1417144
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vZ;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iget v0, v0, LX/8vi;->ap:I

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iget v0, v0, LX/8vi;->ao:I

    if-lez v0, :cond_0

    .line 1417145
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iget-object v1, p0, LX/8vZ;->a:Landroid/os/Parcelable;

    invoke-static {v0, v1}, LX/8vi;->a(LX/8vi;Landroid/os/Parcelable;)V

    .line 1417146
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vZ;->a:Landroid/os/Parcelable;

    .line 1417147
    :goto_0
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v0}, LX/8vi;->l()V

    .line 1417148
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v0}, LX/8vi;->requestLayout()V

    .line 1417149
    return-void

    .line 1417150
    :cond_0
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v0}, LX/8vi;->o()V

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1417151
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8vi;->aj:Z

    .line 1417152
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417153
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-static {v0}, LX/8vi;->a(LX/8vi;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, LX/8vZ;->a:Landroid/os/Parcelable;

    .line 1417154
    :cond_0
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iget-object v1, p0, LX/8vZ;->b:LX/8vi;

    iget v1, v1, LX/8vi;->ao:I

    iput v1, v0, LX/8vi;->ap:I

    .line 1417155
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iput v3, v0, LX/8vi;->ao:I

    .line 1417156
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iput v2, v0, LX/8vi;->am:I

    .line 1417157
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iput-wide v4, v0, LX/8vi;->an:J

    .line 1417158
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iput v2, v0, LX/8vi;->ak:I

    .line 1417159
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iput-wide v4, v0, LX/8vi;->al:J

    .line 1417160
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    iput-boolean v3, v0, LX/8vi;->ad:Z

    .line 1417161
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v0}, LX/8vi;->l()V

    .line 1417162
    iget-object v0, p0, LX/8vZ;->b:LX/8vi;

    invoke-virtual {v0}, LX/8vi;->requestLayout()V

    .line 1417163
    return-void
.end method
