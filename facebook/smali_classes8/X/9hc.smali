.class public final LX/9hc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/concurrent/Future",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/9hh;


# direct methods
.method public constructor <init>(LX/9hh;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1526771
    iput-object p1, p0, LX/9hc;->b:LX/9hh;

    iput-object p2, p0, LX/9hc;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1526772
    iget-object v0, p0, LX/9hc;->b:LX/9hh;

    iget-object v0, v0, LX/9hh;->e:LX/9fy;

    iget-object v1, p0, LX/9hc;->a:Ljava/lang/String;

    .line 1526773
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1526774
    new-instance v3, Lcom/facebook/photos/data/method/DeleteVideoParams;

    invoke-direct {v3, v1}, Lcom/facebook/photos/data/method/DeleteVideoParams;-><init>(Ljava/lang/String;)V

    .line 1526775
    const-string v4, "deleteVideoParams"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1526776
    iget-object v3, v0, LX/9fy;->a:LX/0aG;

    const-string v4, "delete_video"

    const p0, -0x780ab1a0

    invoke-static {v3, v4, v2, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    move-object v0, v2

    .line 1526777
    return-object v0
.end method
