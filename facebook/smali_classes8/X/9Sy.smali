.class public LX/9Sy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B1W;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/B1W",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/api/feedtype/FeedType;

.field private final b:LX/0pn;


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V
    .locals 0
    .param p1    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1494842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1494843
    iput-object p1, p0, LX/9Sy;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 1494844
    iput-object p2, p0, LX/9Sy;->b:LX/0pn;

    .line 1494845
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Px;
    .locals 5

    .prologue
    .line 1494833
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel;

    .line 1494834
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel$GroupPendingStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel$GroupPendingStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel$GroupPendingStoriesModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1494835
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1494836
    :goto_0
    return-object v0

    .line 1494837
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1494838
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel$GroupPendingStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel$GroupPendingStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel$GroupPendingStoriesModel$NodesModel;

    .line 1494839
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel$GroupPendingStoriesModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1494840
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1494841
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1494826
    iget-object v0, p0, LX/9Sy;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 1494827
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v1

    .line 1494828
    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    .line 1494829
    iget-object v1, v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1494830
    new-instance v1, LX/9Rr;

    invoke-direct {v1}, LX/9Rr;-><init>()V

    move-object v1, v1

    .line 1494831
    const-string v2, "group_id"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "stories_to_fetch"

    iget-object v3, p0, LX/9Sy;->b:LX/0pn;

    invoke-virtual {v3}, LX/0pn;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1494832
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0TF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsFeedPendingPostIdsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1494825
    new-instance v0, LX/B1V;

    iget-object v1, p0, LX/9Sy;->a:Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p0, LX/9Sy;->b:LX/0pn;

    invoke-direct {v0, p0, v1, v2}, LX/B1V;-><init>(LX/B1W;Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    return-object v0
.end method
