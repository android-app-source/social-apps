.class public final LX/9BZ;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/9Be;

.field public final b:[F

.field public c:LX/9Bd;

.field private d:Landroid/graphics/PathMeasure;

.field public e:Z


# direct methods
.method public constructor <init>(LX/9Be;)V
    .locals 1

    .prologue
    .line 1451824
    iput-object p1, p0, LX/9BZ;->a:LX/9Be;

    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 1451825
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/9BZ;->b:[F

    .line 1451826
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9BZ;->e:Z

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    .line 1451827
    iget-object v0, p0, LX/9BZ;->d:Landroid/graphics/PathMeasure;

    if-nez v0, :cond_0

    .line 1451828
    :goto_0
    return-void

    .line 1451829
    :cond_0
    iget-boolean v4, p0, LX/9BZ;->e:Z

    if-nez v4, :cond_1

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v4

    const-wide v6, 0x3fe6666666666666L    # 0.7

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    .line 1451830
    const/4 v4, 0x1

    iput-boolean v4, p0, LX/9BZ;->e:Z

    .line 1451831
    iget-object v4, p0, LX/9BZ;->a:LX/9Be;

    iget-object v4, v4, LX/9Be;->E:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3RX;

    const-string v5, "reactions_like_down"

    invoke-virtual {v4, v5}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1451832
    iget-object v4, p0, LX/9BZ;->a:LX/9Be;

    iget-object v4, v4, LX/9Be;->G:LX/20v;

    const/4 v5, 0x1

    .line 1451833
    invoke-virtual {v4}, LX/20v;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, v4, LX/20v;->c:LX/0ad;

    sget-short v7, LX/98c;->c:S

    invoke-interface {v6, v7, v5}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_2

    :goto_1
    move v4, v5

    .line 1451834
    if-eqz v4, :cond_1

    iget-object v4, p0, LX/9BZ;->a:LX/9Be;

    iget-object v4, v4, LX/9Be;->Q:LX/1zt;

    .line 1451835
    iget v5, v4, LX/1zt;->e:I

    move v4, v5

    .line 1451836
    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 1451837
    iget-object v4, p0, LX/9BZ;->a:LX/9Be;

    iget-object v4, v4, LX/9Be;->E:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3RX;

    const-string v5, "reactions_ui_choose_love_down"

    invoke-virtual {v4, v5}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1451838
    :cond_1
    iget-object v0, p0, LX/9BZ;->d:Landroid/graphics/PathMeasure;

    iget-object v1, p0, LX/9BZ;->d:Landroid/graphics/PathMeasure;

    invoke-virtual {v1}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v1

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, LX/9BZ;->b:[F

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    .line 1451839
    iget-object v0, p0, LX/9BZ;->a:LX/9Be;

    invoke-virtual {v0}, LX/9Be;->invalidate()V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final a(LX/9Bd;Landroid/graphics/Path;)V
    .locals 2

    .prologue
    .line 1451840
    iput-object p1, p0, LX/9BZ;->c:LX/9Bd;

    .line 1451841
    new-instance v0, Landroid/graphics/PathMeasure;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    iput-object v0, p0, LX/9BZ;->d:Landroid/graphics/PathMeasure;

    .line 1451842
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 1

    .prologue
    .line 1451843
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9BZ;->e:Z

    .line 1451844
    return-void
.end method
