.class public final LX/APk;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:LX/APm;


# direct methods
.method public constructor <init>(LX/APm;)V
    .locals 0

    .prologue
    .line 1670327
    iput-object p1, p0, LX/APk;->a:LX/APm;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 1670328
    iget-object v0, p0, LX/APk;->a:LX/APm;

    invoke-static {v0}, LX/APm;->d(LX/APm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1670329
    iget-object v0, p0, LX/APk;->a:LX/APm;

    iget-object v0, v0, LX/APm;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hqz;

    .line 1670330
    iget-object v1, v0, LX/Hqz;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 1670331
    iget-boolean p0, v1, Lcom/facebook/composer/activity/ComposerFragment;->ao:Z

    if-eqz p0, :cond_1

    .line 1670332
    :goto_0
    return-void

    .line 1670333
    :cond_0
    iget-object v0, p0, LX/APk;->a:LX/APm;

    iget-object v0, v0, LX/APm;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hqz;

    .line 1670334
    iget-object v1, v0, LX/Hqz;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object p0, v1, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object p1, LX/0ge;->COMPOSER_POST_COMPOSITION_CLICK:LX/0ge;

    iget-object v1, v0, LX/Hqz;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1670335
    iget-object v1, v0, LX/Hqz;->a:Lcom/facebook/composer/activity/ComposerFragment;

    const/4 p0, 0x1

    invoke-static {v1, p0}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 1670336
    goto :goto_0

    .line 1670337
    :cond_1
    iget-object p0, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {p0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object p0

    invoke-static {p0}, LX/7kq;->j(LX/0Px;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1670338
    iget-object p0, v1, Lcom/facebook/composer/activity/ComposerFragment;->cj:LX/121;

    sget-object p1, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    const p2, 0x7f080e49

    invoke-virtual {v1, p2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance v0, LX/HrI;

    invoke-direct {v0, v1}, LX/HrI;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {p0, p1, p2, v0}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 1670339
    iget-object p0, v1, Lcom/facebook/composer/activity/ComposerFragment;->cj:LX/121;

    sget-object p1, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    .line 1670340
    iget-object p2, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object p2, p2

    .line 1670341
    invoke-virtual {p0, p1, p2}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_0

    .line 1670342
    :cond_2
    invoke-static {v1}, Lcom/facebook/composer/activity/ComposerFragment;->ag(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto :goto_0
.end method
