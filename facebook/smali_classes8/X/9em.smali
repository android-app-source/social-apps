.class public final LX/9em;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9el;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryActivity;)V
    .locals 0

    .prologue
    .line 1520296
    iput-object p1, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 1520297
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->u:LX/8GX;

    iget-object v1, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    invoke-virtual {v0, v1, p1}, LX/8GX;->a(Landroid/net/Uri;I)V

    .line 1520298
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 1

    .prologue
    .line 1520299
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520300
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    .line 1520301
    iput-object p1, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520302
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;Z)V
    .locals 7

    .prologue
    .line 1520303
    if-eqz p2, :cond_2

    .line 1520304
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-nez v0, :cond_0

    .line 1520305
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    .line 1520306
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->u:LX/8GX;

    iget-object v1, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v1

    .line 1520307
    sget-object v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->p:Landroid/graphics/RectF;

    .line 1520308
    :goto_0
    new-instance v3, LX/5Ru;

    invoke-direct {v3}, LX/5Ru;-><init>()V

    .line 1520309
    iput-object v2, v3, LX/5Ru;->a:Landroid/net/Uri;

    .line 1520310
    move-object v2, v3

    .line 1520311
    iput v1, v2, LX/5Ru;->b:I

    .line 1520312
    move-object v1, v2

    .line 1520313
    iget-object v2, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryActivity;->C:Ljava/lang/String;

    .line 1520314
    iput-object v2, v1, LX/5Ru;->c:Ljava/lang/String;

    .line 1520315
    move-object v1, v1

    .line 1520316
    iget-object v2, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520317
    iput-object v2, v1, LX/5Ru;->d:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520318
    move-object v1, v1

    .line 1520319
    iget-object v2, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryActivity;->A:Ljava/lang/String;

    .line 1520320
    iput-object v2, v1, LX/5Ru;->e:Ljava/lang/String;

    .line 1520321
    move-object v1, v1

    .line 1520322
    iput-object v0, v1, LX/5Ru;->f:Landroid/graphics/RectF;

    .line 1520323
    move-object v0, v1

    .line 1520324
    invoke-virtual {v0}, LX/5Ru;->a()Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    move-result-object v0

    .line 1520325
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1520326
    sget-object v2, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1520327
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->setResult(ILandroid/content/Intent;)V

    .line 1520328
    :goto_1
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v1, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->B:LX/5Rz;

    iget-object v2, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryActivity;->u:LX/8GX;

    iget-object v3, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v3, v3, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v2

    const/4 v4, 0x1

    .line 1520329
    invoke-static {}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->newBuilder()LX/9cC;

    move-result-object v5

    .line 1520330
    iput-boolean p2, v5, LX/9cC;->c:Z

    .line 1520331
    move-object v3, v5

    .line 1520332
    iput v2, v3, LX/9cC;->m:I

    .line 1520333
    if-eqz v0, :cond_4

    .line 1520334
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v3

    if-eqz v3, :cond_3

    move v3, v4

    .line 1520335
    :goto_2
    iput-boolean v3, v5, LX/9cC;->a:Z

    .line 1520336
    move-object v3, v5

    .line 1520337
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v6

    .line 1520338
    iput-object v6, v3, LX/9cC;->n:Ljava/lang/String;

    .line 1520339
    move-object v3, v3

    .line 1520340
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    .line 1520341
    iput v6, v3, LX/9cC;->j:I

    .line 1520342
    move-object v3, v3

    .line 1520343
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    .line 1520344
    iput v6, v3, LX/9cC;->k:I

    .line 1520345
    :goto_3
    iput v4, v5, LX/9cC;->d:I

    .line 1520346
    move-object v3, v5

    .line 1520347
    iget v4, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->d:I

    .line 1520348
    iput v4, v3, LX/9cC;->g:I

    .line 1520349
    move-object v3, v3

    .line 1520350
    iget v4, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->e:I

    .line 1520351
    iput v4, v3, LX/9cC;->h:I

    .line 1520352
    move-object v3, v3

    .line 1520353
    iget v4, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->b:I

    .line 1520354
    iput v4, v3, LX/9cC;->e:I

    .line 1520355
    move-object v3, v3

    .line 1520356
    iget v4, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->a:I

    .line 1520357
    iput v4, v3, LX/9cC;->f:I

    .line 1520358
    move-object v3, v3

    .line 1520359
    iput-object v1, v3, LX/9cC;->o:LX/5Rz;

    .line 1520360
    invoke-virtual {v5}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v3

    move-object v0, v3

    .line 1520361
    iget-object v1, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->z:LX/9c9;

    iget-object v2, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryActivity;->A:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/9c9;->a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)V

    .line 1520362
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->finish()V

    .line 1520363
    return-void

    .line 1520364
    :cond_0
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    .line 1520365
    :goto_4
    iget-object v1, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->u:LX/8GX;

    invoke-virtual {v1, v0}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v2

    .line 1520366
    iget-object v1, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    move-object v4, v1

    move v1, v2

    move-object v2, v0

    move-object v0, v4

    goto/16 :goto_0

    .line 1520367
    :cond_1
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->x:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_4

    .line 1520368
    :cond_2
    iget-object v0, p0, LX/9em;->a:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->setResult(I)V

    goto/16 :goto_1

    .line 1520369
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 1520370
    :cond_4
    sget-object v3, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v3}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v3

    .line 1520371
    iput-object v3, v5, LX/9cC;->n:Ljava/lang/String;

    .line 1520372
    goto :goto_3
.end method
