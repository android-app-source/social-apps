.class public final synthetic LX/8uq;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1415798
    invoke-static {}, LX/8ur;->values()[LX/8ur;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/8uq;->c:[I

    :try_start_0
    sget-object v0, LX/8uq;->c:[I

    sget-object v1, LX/8ur;->CLEAR:LX/8ur;

    invoke-virtual {v1}, LX/8ur;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, LX/8uq;->c:[I

    sget-object v1, LX/8ur;->INPUT_TYPE_SWITCH:LX/8ur;

    invoke-virtual {v1}, LX/8ur;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, LX/8uq;->c:[I

    sget-object v1, LX/8ur;->NONE:LX/8ur;

    invoke-virtual {v1}, LX/8ur;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    .line 1415799
    :goto_2
    invoke-static {}, LX/8us;->values()[LX/8us;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/8uq;->b:[I

    :try_start_3
    sget-object v0, LX/8uq;->b:[I

    sget-object v1, LX/8us;->WHILE_EDITING:LX/8us;

    invoke-virtual {v1}, LX/8us;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v0, LX/8uq;->b:[I

    sget-object v1, LX/8us;->ALWAYS:LX/8us;

    invoke-virtual {v1}, LX/8us;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, LX/8uq;->b:[I

    sget-object v1, LX/8us;->NEVER:LX/8us;

    invoke-virtual {v1}, LX/8us;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    .line 1415800
    :goto_5
    invoke-static {}, LX/8ux;->values()[LX/8ux;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/8uq;->a:[I

    :try_start_6
    sget-object v0, LX/8uq;->a:[I

    sget-object v1, LX/8ux;->BLUE:LX/8ux;

    invoke-virtual {v1}, LX/8ux;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, LX/8uq;->a:[I

    sget-object v1, LX/8ux;->RED:LX/8ux;

    invoke-virtual {v1}, LX/8ux;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    goto :goto_7

    :catch_1
    goto :goto_6

    :catch_2
    goto :goto_5

    :catch_3
    goto :goto_4

    :catch_4
    goto :goto_3

    :catch_5
    goto :goto_2

    :catch_6
    goto :goto_1

    :catch_7
    goto :goto_0
.end method
