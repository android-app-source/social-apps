.class public LX/9fU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9eb;
.implements LX/9f8;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:LX/9fH;

.field private final c:LX/9fT;

.field private final d:LX/9fS;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9cP;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/widget/FrameLayout;

.field private final g:Ljava/lang/String;

.field private final h:Landroid/content/Context;

.field private final i:LX/8Gc;

.field public j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

.field public k:LX/9cg;

.field private l:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

.field private m:Z

.field public n:Z

.field private o:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

.field public q:LX/9f8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1522063
    const-class v0, LX/9fU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9fU;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/widget/FrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/9f8;LX/0am;Landroid/content/Context;LX/8Gc;LX/0Ot;)V
    .locals 4
    .param p1    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/editgallery/EditableOverlayContainerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/9f8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/FrameLayout;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Ljava/lang/String;",
            "LX/9f8;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;",
            "Landroid/content/Context;",
            "LX/8Gc;",
            "LX/0Ot",
            "<",
            "LX/9cP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 1522046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522047
    new-instance v0, LX/9fR;

    invoke-direct {v0, p0}, LX/9fR;-><init>(LX/9fU;)V

    iput-object v0, p0, LX/9fU;->a:LX/9fH;

    .line 1522048
    new-instance v0, LX/9fT;

    invoke-direct {v0, p0}, LX/9fT;-><init>(LX/9fU;)V

    iput-object v0, p0, LX/9fU;->c:LX/9fT;

    .line 1522049
    new-instance v0, LX/9fS;

    invoke-direct {v0, p0}, LX/9fS;-><init>(LX/9fU;)V

    iput-object v0, p0, LX/9fU;->d:LX/9fS;

    .line 1522050
    new-instance v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;-><init>()V

    iput-object v0, p0, LX/9fU;->p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    .line 1522051
    iput-object p1, p0, LX/9fU;->f:Landroid/widget/FrameLayout;

    .line 1522052
    iput-object p3, p0, LX/9fU;->g:Ljava/lang/String;

    .line 1522053
    iput-object p6, p0, LX/9fU;->h:Landroid/content/Context;

    .line 1522054
    iput-object p8, p0, LX/9fU;->e:LX/0Ot;

    .line 1522055
    iput-object p7, p0, LX/9fU;->i:LX/8Gc;

    .line 1522056
    iput-object p2, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    .line 1522057
    new-instance v0, LX/9cg;

    iget-object v1, p0, LX/9fU;->h:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/9cg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9fU;->k:LX/9cg;

    .line 1522058
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/9cg;->setVisibility(I)V

    .line 1522059
    iget-object v0, p0, LX/9fU;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/9fU;->k:LX/9cg;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1522060
    iput-object p5, p0, LX/9fU;->o:LX/0am;

    .line 1522061
    iput-object p4, p0, LX/9fU;->q:LX/9f8;

    .line 1522062
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1522045
    iget-object v0, p0, LX/9fU;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082411

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5i7;)V
    .locals 1
    .param p1    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1522041
    if-eqz p1, :cond_0

    sget-object v0, LX/5i7;->STICKER:LX/5i7;

    if-ne p1, v0, :cond_0

    .line 1522042
    iget-object v0, p0, LX/9fU;->p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    .line 1522043
    iget p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->f:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->f:I

    .line 1522044
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1522040
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V
    .locals 9

    .prologue
    .line 1522010
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1522011
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522012
    iput-object p1, p0, LX/9fU;->l:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1522013
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    iget-object v1, p0, LX/9fU;->g:Ljava/lang/String;

    sget-object v2, LX/9fU;->b:Ljava/lang/String;

    sget-object v3, LX/4m4;->COMPOSER:LX/4m4;

    .line 1522014
    iput-object v1, v0, LX/9cg;->n:Ljava/lang/String;

    .line 1522015
    iget-object v4, v0, LX/9cg;->e:LX/9c7;

    invoke-virtual {v4, v1, v2}, LX/9c7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522016
    const v4, 0x7f0d285b

    invoke-virtual {v0, v4}, LX/9cg;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, LX/9cb;

    invoke-direct {v5, v0, v3}, LX/9cb;-><init>(LX/9cg;LX/4m4;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1522017
    iget-object v4, v0, LX/9cg;->b:LX/9ci;

    invoke-virtual {v0}, LX/9cg;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 1522018
    new-instance v7, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    .line 1522019
    new-instance v2, LX/9cZ;

    invoke-static {v4}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v6

    check-cast v6, LX/3dt;

    invoke-static {v4}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    invoke-static {v4}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object p1

    check-cast p1, LX/0SI;

    invoke-static {v4}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-direct {v2, v6, v8, p1, v1}, LX/9cZ;-><init>(LX/3dt;LX/0aG;LX/0SI;Ljava/util/concurrent/Executor;)V

    .line 1522020
    move-object v6, v2

    .line 1522021
    check-cast v6, LX/9cZ;

    const/16 v8, 0x33f

    invoke-static {v4, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct {v7, v5, v6, v8}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;-><init>(Landroid/content/Context;LX/9cZ;LX/0Or;)V

    .line 1522022
    move-object v4, v7

    .line 1522023
    iput-object v4, v0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    .line 1522024
    iget-object v4, v0, LX/9cg;->f:Lit/sephiroth/android/library/widget/HListView;

    iget-object v5, v0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    invoke-virtual {v4, v5}, LX/8vj;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1522025
    iget-object v4, v0, LX/9cg;->f:Lit/sephiroth/android/library/widget/HListView;

    new-instance v5, LX/9cc;

    invoke-direct {v5, v0}, LX/9cc;-><init>(LX/9cg;)V

    .line 1522026
    iput-object v5, v4, LX/8vi;->ah:LX/9cc;

    .line 1522027
    new-instance v4, LX/9cd;

    invoke-direct {v4, v0, v3}, LX/9cd;-><init>(LX/9cg;LX/4m4;)V

    iput-object v4, v0, LX/9cg;->j:Landroid/database/DataSetObserver;

    .line 1522028
    iget-object v4, v0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    iget-object v5, v0, LX/9cg;->j:Landroid/database/DataSetObserver;

    invoke-virtual {v4, v5}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1522029
    const v4, 0x7f0d2859

    invoke-virtual {v0, v4}, LX/9cg;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v4/view/ViewPager;

    iput-object v4, v0, LX/9cg;->g:Landroid/support/v4/view/ViewPager;

    .line 1522030
    iget-object v4, v0, LX/9cg;->g:Landroid/support/v4/view/ViewPager;

    new-instance v5, LX/9ce;

    invoke-direct {v5, v0}, LX/9ce;-><init>(LX/9cg;)V

    invoke-virtual {v4, v5}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1522031
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    invoke-virtual {v0}, LX/9cg;->d()V

    .line 1522032
    iget-object v0, p0, LX/9fU;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cP;

    iget-object v1, p0, LX/9fU;->c:LX/9fT;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1522033
    iget-object v0, p0, LX/9fU;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cP;

    iget-object v1, p0, LX/9fU;->d:LX/9fS;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1522034
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9fU;->m:Z

    .line 1522035
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const v1, 0x7f0219d9

    const v2, 0x7f0813c3

    const v3, 0x7f0813c4

    invoke-virtual {v0, v1, v2, v3}, LX/9dl;->a(III)V

    .line 1522036
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    iget-object v1, p0, LX/9fU;->a:LX/9fH;

    .line 1522037
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->m:LX/9fH;

    .line 1522038
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, LX/9dl;->e()V

    .line 1522039
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;)V
    .locals 1

    .prologue
    .line 1522008
    iget v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->b:I

    .line 1522009
    return-void
.end method

.method public final a(Ljava/lang/String;LX/5i7;)V
    .locals 1
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1522002
    if-eqz p2, :cond_0

    sget-object v0, LX/5i7;->STICKER:LX/5i7;

    if-ne p2, v0, :cond_0

    .line 1522003
    iget-object v0, p0, LX/9fU;->p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    .line 1522004
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522005
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->b:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1522006
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->b:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1522007
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1521983
    iget-object v0, p0, LX/9fU;->p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    iput-boolean p1, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->a:Z

    .line 1521984
    iget-object v0, p0, LX/9fU;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521985
    iget-object v0, p0, LX/9fU;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c7;

    iget-object v1, p0, LX/9fU;->p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    .line 1521986
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/9c4;->COMPOSER_STICKERS_EXIT_FLOW:LX/9c4;

    invoke-virtual {p0}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "composer"

    .line 1521987
    iput-object p0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1521988
    move-object v2, v2

    .line 1521989
    sget-object p0, LX/9c5;->NUMBER_OF_STICKERS_ADDED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1521990
    iget p1, v1, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->e:I

    move p1, p1

    .line 1521991
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->NUMBER_OF_STICKERS_REMOVED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1521992
    iget p1, v1, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->f:I

    move p1, p1

    .line 1521993
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->NUMBER_OF_STICKERS_RESIZED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1521994
    iget-object p1, v1, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    move p1, p1

    .line 1521995
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->NUMBER_OF_STICKERS_MOVED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1521996
    iget-object p1, v1, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    move p1, p1

    .line 1521997
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->NUMBER_OF_STICKERS_ROTATED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1521998
    iget-object p1, v1, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    move p1, p1

    .line 1521999
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->ACCEPTED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    iget-boolean p1, v1, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->a:Z

    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1522000
    invoke-static {v0, v2}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1522001
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1521972
    iget-boolean v0, p0, LX/9fU;->m:Z

    if-eqz v0, :cond_0

    .line 1521973
    iget-object v0, p0, LX/9fU;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cP;

    iget-object v1, p0, LX/9fU;->c:LX/9fT;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1521974
    iget-object v0, p0, LX/9fU;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cP;

    iget-object v1, p0, LX/9fU;->d:LX/9fS;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1521975
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    .line 1521976
    iget-object v1, v0, LX/9cg;->k:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, LX/9cg;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1521977
    iget-object v1, v0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    iget-object v2, v0, LX/9cg;->j:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1521978
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/9cg;->setVisibility(I)V

    .line 1521979
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, LX/9dl;->b()V

    .line 1521980
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, LX/9dl;->i()V

    .line 1521981
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9fU;->m:Z

    .line 1521982
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;LX/5i7;)V
    .locals 1
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1521966
    if-eqz p2, :cond_0

    sget-object v0, LX/5i7;->STICKER:LX/5i7;

    if-ne p2, v0, :cond_0

    .line 1521967
    iget-object v0, p0, LX/9fU;->p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    .line 1521968
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521969
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1521970
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->c:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521971
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1521964
    iget-object v0, p0, LX/9fU;->a:LX/9fH;

    invoke-interface {v0}, LX/9fH;->a()V

    .line 1521965
    return-void
.end method

.method public final c(Ljava/lang/String;LX/5i7;)V
    .locals 1
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1521932
    if-eqz p2, :cond_0

    sget-object v0, LX/5i7;->STICKER:LX/5i7;

    if-ne p2, v0, :cond_0

    .line 1521933
    iget-object v0, p0, LX/9fU;->p:Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;

    .line 1521934
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521935
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1521936
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/analytics/StickersOnPhotosLoggingParams;->d:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521937
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1521960
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    invoke-virtual {v0}, LX/9cg;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1521961
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    invoke-virtual {v0}, LX/9cg;->c()V

    .line 1521962
    const/4 v0, 0x1

    .line 1521963
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1521959
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1521957
    iget-object v0, p0, LX/9fU;->k:LX/9cg;

    invoke-virtual {v0}, LX/9cg;->d()V

    .line 1521958
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1521956
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1521949
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1521950
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setAlpha(F)V

    .line 1521951
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1521952
    iget-object v0, p0, LX/9fU;->i:LX/8Gc;

    invoke-virtual {v0}, LX/8Gc;->a()V

    .line 1521953
    iget-object v0, p0, LX/9fU;->i:LX/8Gc;

    iget-object v1, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0, v1, v2}, LX/8Gc;->a(Landroid/view/View;I)V

    .line 1521954
    :cond_0
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setActionButtonEnabled(Z)V

    .line 1521955
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1521946
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1521947
    iget-object v0, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setActionButtonEnabled(Z)V

    .line 1521948
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1521945
    sget-object v0, LX/5Rr;->STICKER:LX/5Rr;

    return-object v0
.end method

.method public final k()LX/9ek;
    .locals 1

    .prologue
    .line 1521944
    sget-object v0, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1521943
    iget-boolean v0, p0, LX/9fU;->n:Z

    return v0
.end method

.method public final m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
    .locals 4

    .prologue
    .line 1521938
    iget-object v0, p0, LX/9fU;->l:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, LX/9fU;->l:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521939
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v2

    .line 1521940
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    iget-object v2, p0, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const-class v3, Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-virtual {v2, v3}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Ljava/lang/Class;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setStickerParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    .line 1521941
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1521942
    iget-object v0, p0, LX/9fU;->l:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    return-object v0
.end method
