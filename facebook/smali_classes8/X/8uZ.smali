.class public final LX/8uZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V
    .locals 0

    .prologue
    .line 1415004
    iput-object p1, p0, LX/8uZ;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x661962b5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1415005
    iget-object v1, p0, LX/8uZ;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415006
    const-string v2, "updated_users"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object p0

    .line 1415007
    iget-object v2, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v2}, LX/8Vc;->d()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p3

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, p3, :cond_0

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserKey;

    .line 1415008
    invoke-virtual {p0, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1415009
    invoke-static {v1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->h(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    .line 1415010
    :cond_0
    const/16 v1, 0x27

    const v2, 0x7d0f59ee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1415011
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0
.end method
