.class public final LX/A3H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1616016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_c

    .line 1616017
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1616018
    :goto_0
    return v1

    .line 1616019
    :cond_0
    const-string v11, "total_electoral_votes_available"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1616020
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 1616021
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1616022
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1616023
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1616024
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1616025
    const-string v11, "all_candidate_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1616026
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1616027
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_2

    .line 1616028
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_2

    .line 1616029
    invoke-static {p0, p1}, LX/A3J;->b(LX/15w;LX/186;)I

    move-result v10

    .line 1616030
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1616031
    :cond_2
    invoke-static {v9, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v9

    move v9, v9

    .line 1616032
    goto :goto_1

    .line 1616033
    :cond_3
    const-string v11, "all_election_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1616034
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1616035
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_4

    .line 1616036
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_4

    .line 1616037
    invoke-static {p0, p1}, LX/A3G;->b(LX/15w;LX/186;)I

    move-result v10

    .line 1616038
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1616039
    :cond_4
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 1616040
    goto :goto_1

    .line 1616041
    :cond_5
    const-string v11, "data_source"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1616042
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1616043
    :cond_6
    const-string v11, "topic_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1616044
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1616045
    :cond_7
    const-string v11, "total_electoral_votes_needed"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1616046
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto/16 :goto_1

    .line 1616047
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1616048
    :cond_9
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1616049
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1616050
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1616051
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1616052
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1616053
    if-eqz v3, :cond_a

    .line 1616054
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 1616055
    :cond_a
    if-eqz v0, :cond_b

    .line 1616056
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 1616057
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1616058
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1616059
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1616060
    if-eqz v0, :cond_1

    .line 1616061
    const-string v1, "all_candidate_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616062
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1616063
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1616064
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/A3J;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1616065
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1616066
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1616067
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1616068
    if-eqz v0, :cond_3

    .line 1616069
    const-string v1, "all_election_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616070
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1616071
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1616072
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/A3G;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1616073
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1616074
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1616075
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1616076
    if-eqz v0, :cond_4

    .line 1616077
    const-string v1, "data_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616078
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1616079
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1616080
    if-eqz v0, :cond_5

    .line 1616081
    const-string v1, "topic_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616082
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1616083
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1616084
    if-eqz v0, :cond_6

    .line 1616085
    const-string v1, "total_electoral_votes_available"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616086
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1616087
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1616088
    if-eqz v0, :cond_7

    .line 1616089
    const-string v1, "total_electoral_votes_needed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616090
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1616091
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1616092
    return-void
.end method
