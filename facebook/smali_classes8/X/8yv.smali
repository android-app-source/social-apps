.class public final LX/8yv;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8yw;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/1X1",
            "<*>;>;"
        }
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1426693
    invoke-static {}, LX/8yw;->q()LX/8yw;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1426694
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1426695
    const-string v0, "SizeBasedSelector"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426696
    if-ne p0, p1, :cond_1

    .line 1426697
    :cond_0
    :goto_0
    return v0

    .line 1426698
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1426699
    goto :goto_0

    .line 1426700
    :cond_3
    check-cast p1, LX/8yv;

    .line 1426701
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1426702
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1426703
    if-eq v2, v3, :cond_0

    .line 1426704
    iget-object v2, p0, LX/8yv;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8yv;->a:LX/1X1;

    iget-object v3, p1, LX/8yv;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1426705
    goto :goto_0

    .line 1426706
    :cond_5
    iget-object v2, p1, LX/8yv;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 1426707
    :cond_6
    iget-object v2, p0, LX/8yv;->b:LX/0QR;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/8yv;->b:LX/0QR;

    iget-object v3, p1, LX/8yv;->b:LX/0QR;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1426708
    goto :goto_0

    .line 1426709
    :cond_8
    iget-object v2, p1, LX/8yv;->b:LX/0QR;

    if-nez v2, :cond_7

    .line 1426710
    :cond_9
    iget v2, p0, LX/8yv;->c:I

    iget v3, p1, LX/8yv;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1426711
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1426712
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/8yv;

    .line 1426713
    iget-object v1, v0, LX/8yv;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/8yv;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/8yv;->a:LX/1X1;

    .line 1426714
    return-object v0

    .line 1426715
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
