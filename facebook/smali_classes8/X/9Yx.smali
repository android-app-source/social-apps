.class public final LX/9Yx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "overallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "placeOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "placeOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "profilePictureAsCover"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "profilePictureAsCover"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Z

.field public P:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "recentPosters"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "recentPosters"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Z

.field public U:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "adminDisplayPreference"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "adminDisplayPreference"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "adminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "adminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "attribution"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "overallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1507733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1507734
    return-void
.end method


# virtual methods
.method public final a(LX/15i;I)LX/9Yx;
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "adminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1507735
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/9Yx;->h:LX/15i;

    iput p2, p0, LX/9Yx;->i:I

    monitor-exit v1

    .line 1507736
    return-object p0

    .line 1507737
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;
    .locals 36

    .prologue
    .line 1507738
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1507739
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9Yx;->a:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1507740
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9Yx;->b:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    invoke-static {v3, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1507741
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9Yx;->c:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    invoke-static {v3, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1507742
    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9Yx;->d:LX/15i;

    move-object/from16 v0, p0

    iget v9, v0, LX/9Yx;->e:I

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v7, 0x744b6a79

    invoke-static {v8, v9, v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v7

    invoke-static {v3, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1507743
    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_1
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9Yx;->f:LX/15i;

    move-object/from16 v0, p0

    iget v10, v0, LX/9Yx;->g:I

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v8, -0x1ba8526

    invoke-static {v9, v10, v8}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v8

    invoke-static {v3, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1507744
    sget-object v9, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v9

    :try_start_2
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9Yx;->h:LX/15i;

    move-object/from16 v0, p0

    iget v11, v0, LX/9Yx;->i:I

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v9, 0x8c3f43

    invoke-static {v10, v11, v9}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v9

    invoke-static {v3, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1507745
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9Yx;->j:LX/2uF;

    invoke-static {v10, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v10

    .line 1507746
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9Yx;->k:LX/0Px;

    invoke-virtual {v3, v11}, LX/186;->b(Ljava/util/List;)I

    move-result v11

    .line 1507747
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9Yx;->l:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    invoke-static {v3, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1507748
    sget-object v13, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v13

    :try_start_3
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9Yx;->m:LX/15i;

    move-object/from16 v0, p0

    iget v15, v0, LX/9Yx;->n:I

    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const v13, 0x5870cdbb

    invoke-static {v14, v15, v13}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v13

    invoke-static {v3, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1507749
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9Yx;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-static {v3, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1507750
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9Yx;->y:Ljava/lang/String;

    invoke-virtual {v3, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1507751
    sget-object v16, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v16

    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->z:LX/15i;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9Yx;->A:I

    move/from16 v18, v0

    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    const v16, 0x24160ca9

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1507752
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1507753
    sget-object v18, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v18

    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->C:LX/15i;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9Yx;->D:I

    move/from16 v20, v0

    monitor-exit v18
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    const v18, -0x41e35a26

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1507754
    sget-object v19, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v19

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->E:LX/15i;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9Yx;->F:I

    move/from16 v21, v0

    monitor-exit v19
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    const v19, -0x5dd27faa

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1507755
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->G:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 1507756
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->H:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    .line 1507757
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->I:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1507758
    sget-object v23, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v23

    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->J:LX/15i;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9Yx;->K:I

    move/from16 v25, v0

    monitor-exit v23
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    const v23, 0xb4e594d

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1507759
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->L:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1507760
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->M:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1507761
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1507762
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->P:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1507763
    sget-object v28, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v28

    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->Q:LX/15i;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9Yx;->R:I

    move/from16 v30, v0

    monitor-exit v28
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    const v28, -0xb0ab93f

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1507764
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->S:LX/0Px;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v29

    .line 1507765
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->U:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v30

    .line 1507766
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->V:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1507767
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->W:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 1507768
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->X:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v33

    .line 1507769
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9Yx;->Y:LX/0Px;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v3, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v34

    .line 1507770
    const/16 v35, 0x2a

    move/from16 v0, v35

    invoke-virtual {v3, v0}, LX/186;->c(I)V

    .line 1507771
    const/16 v35, 0x0

    move/from16 v0, v35

    invoke-virtual {v3, v0, v4}, LX/186;->b(II)V

    .line 1507772
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v5}, LX/186;->b(II)V

    .line 1507773
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 1507774
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 1507775
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 1507776
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 1507777
    const/4 v4, 0x6

    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 1507778
    const/4 v4, 0x7

    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 1507779
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v12}, LX/186;->b(II)V

    .line 1507780
    const/16 v4, 0x9

    invoke-virtual {v3, v4, v13}, LX/186;->b(II)V

    .line 1507781
    const/16 v4, 0xa

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->o:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507782
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->p:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507783
    const/16 v4, 0xc

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->q:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507784
    const/16 v4, 0xd

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->r:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507785
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->s:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507786
    const/16 v4, 0xf

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->t:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507787
    const/16 v4, 0x10

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->u:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507788
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->v:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507789
    const/16 v4, 0x12

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->w:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507790
    const/16 v4, 0x13

    invoke-virtual {v3, v4, v14}, LX/186;->b(II)V

    .line 1507791
    const/16 v4, 0x14

    invoke-virtual {v3, v4, v15}, LX/186;->b(II)V

    .line 1507792
    const/16 v4, 0x15

    move/from16 v0, v16

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507793
    const/16 v4, 0x16

    move/from16 v0, v17

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507794
    const/16 v4, 0x17

    move/from16 v0, v18

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507795
    const/16 v4, 0x18

    move/from16 v0, v19

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507796
    const/16 v4, 0x19

    move/from16 v0, v20

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507797
    const/16 v4, 0x1a

    move/from16 v0, v21

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507798
    const/16 v4, 0x1b

    move/from16 v0, v22

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507799
    const/16 v4, 0x1c

    move/from16 v0, v23

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507800
    const/16 v4, 0x1d

    move/from16 v0, v24

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507801
    const/16 v4, 0x1e

    move/from16 v0, v25

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507802
    const/16 v4, 0x1f

    move/from16 v0, v26

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507803
    const/16 v4, 0x20

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->O:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507804
    const/16 v4, 0x21

    move/from16 v0, v27

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507805
    const/16 v4, 0x22

    move/from16 v0, v28

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507806
    const/16 v4, 0x23

    move/from16 v0, v29

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507807
    const/16 v4, 0x24

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9Yx;->T:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1507808
    const/16 v4, 0x25

    move/from16 v0, v30

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507809
    const/16 v4, 0x26

    move/from16 v0, v31

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507810
    const/16 v4, 0x27

    move/from16 v0, v32

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507811
    const/16 v4, 0x28

    move/from16 v0, v33

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507812
    const/16 v4, 0x29

    move/from16 v0, v34

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1507813
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 1507814
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 1507815
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1507816
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1507817
    new-instance v3, LX/15i;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1507818
    new-instance v4, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-direct {v4, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;-><init>(LX/15i;)V

    .line 1507819
    return-object v4

    .line 1507820
    :catchall_0
    move-exception v3

    :try_start_9
    monitor-exit v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw v3

    .line 1507821
    :catchall_1
    move-exception v3

    :try_start_a
    monitor-exit v8
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    throw v3

    .line 1507822
    :catchall_2
    move-exception v3

    :try_start_b
    monitor-exit v9
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v3

    .line 1507823
    :catchall_3
    move-exception v3

    :try_start_c
    monitor-exit v13
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v3

    .line 1507824
    :catchall_4
    move-exception v3

    :try_start_d
    monitor-exit v16
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v3

    .line 1507825
    :catchall_5
    move-exception v3

    :try_start_e
    monitor-exit v18
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v3

    .line 1507826
    :catchall_6
    move-exception v3

    :try_start_f
    monitor-exit v19
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    throw v3

    .line 1507827
    :catchall_7
    move-exception v3

    :try_start_10
    monitor-exit v23
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    throw v3

    .line 1507828
    :catchall_8
    move-exception v3

    :try_start_11
    monitor-exit v28
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    throw v3
.end method
