.class public LX/98w;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FbMemoryConfig"
.end annotation


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/5pY;LX/0ad;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446579
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1446580
    iput-object p2, p0, LX/98w;->a:LX/0ad;

    .line 1446581
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1446582
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1446583
    iget-object v1, p0, LX/98w;->a:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-char v3, LX/0ds;->c:C

    const-string v4, "none"

    invoke-interface {v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1446584
    const-string v2, "js"

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1446585
    :goto_0
    return-object v0

    .line 1446586
    :cond_0
    const-string v1, "numBytes"

    iget-object v2, p0, LX/98w;->a:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget v4, LX/0ds;->b:I

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1446587
    const-string v0, "FbMemoryConfig"

    return-object v0
.end method
