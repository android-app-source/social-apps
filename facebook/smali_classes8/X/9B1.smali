.class public final LX/9B1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1zt;

.field public final synthetic b:LX/9B3;


# direct methods
.method public constructor <init>(LX/9B3;LX/1zt;)V
    .locals 0

    .prologue
    .line 1451009
    iput-object p1, p0, LX/9B1;->b:LX/9B3;

    iput-object p2, p0, LX/9B1;->a:LX/1zt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1451024
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1451010
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1451011
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 1451012
    :goto_0
    iget-object v1, p0, LX/9B1;->b:LX/9B3;

    iget-object v2, p0, LX/9B1;->a:LX/1zt;

    .line 1451013
    invoke-static {v0}, LX/9B3;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v3

    .line 1451014
    if-nez v3, :cond_2

    .line 1451015
    :goto_1
    iget-object v0, p0, LX/9B1;->b:LX/9B3;

    iget-object v1, p0, LX/9B1;->a:LX/1zt;

    .line 1451016
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    if-eqz v2, :cond_0

    .line 1451017
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    .line 1451018
    iget-object v0, v2, LX/9BK;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    .line 1451019
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/1zt;Z)V

    .line 1451020
    :cond_0
    return-void

    .line 1451021
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1451022
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    goto :goto_0

    .line 1451023
    :cond_2
    iget-object p1, v1, LX/9B3;->k:[LX/55h;

    invoke-static {v1, v2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v0

    aget-object p1, p1, v0

    invoke-static {v1, v3}, LX/9B3;->b(LX/9B3;Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/55h;->a(Ljava/lang/Iterable;)V

    goto :goto_1
.end method
