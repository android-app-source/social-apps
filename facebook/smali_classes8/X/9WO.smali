.class public LX/9WO;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final g:[I

.field private static final h:[I

.field private static final i:[I


# instance fields
.field public a:LX/9To;

.field public b:Lcom/facebook/fbui/glyph/GlyphView;

.field public c:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public d:Landroid/widget/ProgressBar;

.field public e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1501105
    new-array v0, v3, [I

    const v1, 0x7f010783

    aput v1, v0, v2

    sput-object v0, LX/9WO;->g:[I

    .line 1501106
    new-array v0, v3, [I

    const v1, 0x7f010784

    aput v1, v0, v2

    sput-object v0, LX/9WO;->h:[I

    .line 1501107
    new-array v0, v3, [I

    const v1, 0x7f010785

    aput v1, v0, v2

    sput-object v0, LX/9WO;->i:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1501108
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9WO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1501109
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1501110
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1501111
    sget-object v0, LX/9To;->INITIAL:LX/9To;

    iput-object v0, p0, LX/9WO;->a:LX/9To;

    .line 1501112
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9WO;->f:Z

    .line 1501113
    const v0, 0x7f030bd7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1501114
    const v0, 0x7f0d1d7a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1501115
    const v0, 0x7f0d1d7d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/9WO;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1501116
    const v0, 0x7f0d1d7c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/9WO;->d:Landroid/widget/ProgressBar;

    .line 1501117
    new-instance p1, LX/0zw;

    const v0, 0x7f0d1d7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {p1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p1, p0, LX/9WO;->e:LX/0zw;

    .line 1501118
    invoke-virtual {p0}, LX/9WO;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1501119
    const p1, 0x7f0210cf

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/9WO;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1501120
    return-void
.end method

.method public static setProgressBarVisibility(LX/9WO;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1501121
    if-eqz p1, :cond_0

    .line 1501122
    iget-object v0, p0, LX/9WO;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1501123
    iget-object v0, p0, LX/9WO;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1501124
    :goto_0
    return-void

    .line 1501125
    :cond_0
    iget-object v0, p0, LX/9WO;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1501126
    iget-object v0, p0, LX/9WO;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final onCreateDrawableState(I)[I
    .locals 3

    .prologue
    .line 1501127
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1501128
    sget-object v1, LX/9WN;->a:[I

    iget-object v2, p0, LX/9WO;->a:LX/9To;

    invoke-virtual {v2}, LX/9To;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1501129
    :goto_0
    return-object v0

    .line 1501130
    :pswitch_0
    sget-object v1, LX/9WO;->g:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 1501131
    :pswitch_1
    sget-object v1, LX/9WO;->h:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 1501132
    :pswitch_2
    sget-object v1, LX/9WO;->i:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 1501133
    :pswitch_3
    iget-boolean v1, p0, LX/9WO;->f:Z

    if-eqz v1, :cond_0

    .line 1501134
    sget-object v1, LX/9WO;->h:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 1501135
    :cond_0
    sget-object v1, LX/9WO;->g:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
