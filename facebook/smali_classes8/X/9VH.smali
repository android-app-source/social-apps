.class public LX/9VH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[LX/9VF;

.field public b:I

.field public c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/9VF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([LX/9VF;)V
    .locals 1

    .prologue
    .line 1499189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1499190
    const/4 v0, 0x0

    iput v0, p0, LX/9VH;->b:I

    .line 1499191
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/9VH;->c:Ljava/util/LinkedList;

    .line 1499192
    iput-object p1, p0, LX/9VH;->a:[LX/9VF;

    .line 1499193
    return-void
.end method


# virtual methods
.method public final a([LX/9VU;J)V
    .locals 11

    .prologue
    .line 1499194
    :goto_0
    iget v2, p0, LX/9VH;->b:I

    iget-object v3, p0, LX/9VH;->a:[LX/9VF;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v2, p0, LX/9VH;->a:[LX/9VF;

    iget v3, p0, LX/9VH;->b:I

    aget-object v2, v2, v3

    .line 1499195
    iget-wide v7, v2, LX/9VF;->a:J

    cmp-long v7, p2, v7

    if-ltz v7, :cond_3

    const/4 v7, 0x1

    :goto_1
    move v2, v7

    .line 1499196
    if-eqz v2, :cond_0

    .line 1499197
    iget-object v2, p0, LX/9VH;->c:Ljava/util/LinkedList;

    iget-object v3, p0, LX/9VH;->a:[LX/9VF;

    iget v4, p0, LX/9VH;->b:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/9VH;->b:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1499198
    :cond_0
    iget-object v0, p0, LX/9VH;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1499199
    iget-object v2, p0, LX/9VH;->c:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    .line 1499200
    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1499201
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9VF;

    .line 1499202
    iget v4, v2, LX/9VF;->c:I

    move v4, v4

    .line 1499203
    aget-object v4, p1, v4

    .line 1499204
    invoke-virtual {v2, v4, p2, p3}, LX/9VF;->a(LX/9VU;J)V

    .line 1499205
    iget-wide v6, v2, LX/9VF;->a:J

    iget-wide v8, v2, LX/9VF;->b:J

    add-long/2addr v6, v8

    cmp-long v6, p2, v6

    if-lez v6, :cond_4

    const/4 v6, 0x1

    :goto_3
    move v2, v6

    .line 1499206
    if-eqz v2, :cond_1

    .line 1499207
    invoke-interface {v3}, Ljava/util/ListIterator;->remove()V

    goto :goto_2

    .line 1499208
    :cond_2
    return-void

    :cond_3
    const/4 v7, 0x0

    goto :goto_1

    :cond_4
    const/4 v6, 0x0

    goto :goto_3
.end method
