.class public final LX/AAs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1639162
    const/16 v17, 0x0

    .line 1639163
    const/16 v16, 0x0

    .line 1639164
    const/4 v15, 0x0

    .line 1639165
    const/4 v14, 0x0

    .line 1639166
    const/4 v13, 0x0

    .line 1639167
    const/4 v12, 0x0

    .line 1639168
    const/4 v11, 0x0

    .line 1639169
    const/4 v10, 0x0

    .line 1639170
    const/4 v9, 0x0

    .line 1639171
    const/4 v8, 0x0

    .line 1639172
    const/4 v7, 0x0

    .line 1639173
    const/4 v6, 0x0

    .line 1639174
    const/4 v5, 0x0

    .line 1639175
    const/4 v4, 0x0

    .line 1639176
    const/4 v3, 0x0

    .line 1639177
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 1639178
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1639179
    const/4 v3, 0x0

    .line 1639180
    :goto_0
    return v3

    .line 1639181
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1639182
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_10

    .line 1639183
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 1639184
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1639185
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 1639186
    const-string v19, "about"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1639187
    invoke-static/range {p0 .. p1}, LX/AAB;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1639188
    :cond_2
    const-string v19, "address"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1639189
    invoke-static/range {p0 .. p1}, LX/4aS;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1639190
    :cond_3
    const-string v19, "admin_info"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1639191
    invoke-static/range {p0 .. p1}, LX/A9X;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1639192
    :cond_4
    const-string v19, "all_phones"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1639193
    invoke-static/range {p0 .. p1}, LX/AAo;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1639194
    :cond_5
    const-string v19, "commerce_store"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1639195
    invoke-static/range {p0 .. p1}, LX/AAK;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1639196
    :cond_6
    const-string v19, "cover_photo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1639197
    invoke-static/range {p0 .. p1}, LX/A9V;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1639198
    :cond_7
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1639199
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1639200
    :cond_8
    const-string v19, "location"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1639201
    invoke-static/range {p0 .. p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1639202
    :cond_9
    const-string v19, "name"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1639203
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1639204
    :cond_a
    const-string v19, "page_call_to_action"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 1639205
    invoke-static/range {p0 .. p1}, LX/AAp;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1639206
    :cond_b
    const-string v19, "page_thumbnail_uri"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 1639207
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1639208
    :cond_c
    const-string v19, "profile_picture"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 1639209
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1639210
    :cond_d
    const-string v19, "timeline_stories"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 1639211
    invoke-static/range {p0 .. p1}, LX/AAr;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1639212
    :cond_e
    const-string v19, "url"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 1639213
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1639214
    :cond_f
    const-string v19, "websites"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1639215
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1639216
    :cond_10
    const/16 v18, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1639217
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1639218
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1639219
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1639220
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1639221
    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1639222
    const/4 v13, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1639223
    const/4 v12, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1639224
    const/4 v11, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1639225
    const/16 v10, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1639226
    const/16 v9, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1639227
    const/16 v8, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1639228
    const/16 v7, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1639229
    const/16 v6, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1639230
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1639231
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1639232
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
