.class public final LX/8mc;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/store/StickerStorePackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStorePackFragment;)V
    .locals 0

    .prologue
    .line 1399532
    iput-object p1, p0, LX/8mc;->a:Lcom/facebook/stickers/store/StickerStorePackFragment;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1399533
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1399534
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 7
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1399535
    check-cast p2, LX/1ln;

    const/high16 v0, 0x40000000    # 2.0f

    .line 1399536
    if-nez p2, :cond_0

    .line 1399537
    :goto_0
    return-void

    .line 1399538
    :cond_0
    iget-object v1, p0, LX/8mc;->a:Lcom/facebook/stickers/store/StickerStorePackFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStorePackFragment;->w:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1399539
    iget-object v1, p0, LX/8mc;->a:Lcom/facebook/stickers/store/StickerStorePackFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStorePackFragment;->x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1399540
    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v3

    .line 1399541
    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v4

    .line 1399542
    iget-object v1, p0, LX/8mc;->a:Lcom/facebook/stickers/store/StickerStorePackFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1399543
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1399544
    iget-object v5, p0, LX/8mc;->a:Lcom/facebook/stickers/store/StickerStorePackFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    .line 1399545
    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 1399546
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1399547
    :cond_1
    int-to-float v1, v2

    int-to-float v5, v3

    div-float/2addr v1, v5

    .line 1399548
    cmpl-float v5, v1, v0

    if-lez v5, :cond_2

    .line 1399549
    int-to-float v1, v3

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 1399550
    :goto_1
    int-to-float v2, v4

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1399551
    iget-object v2, p0, LX/8mc;->a:Lcom/facebook/stickers/store/StickerStorePackFragment;

    iget-object v2, v2, Lcom/facebook/stickers/store/StickerStorePackFragment;->x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumWidth(I)V

    .line 1399552
    iget-object v1, p0, LX/8mc;->a:Lcom/facebook/stickers/store/StickerStorePackFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStorePackFragment;->x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumHeight(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1399553
    return-void
.end method
