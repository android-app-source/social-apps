.class public final LX/9oV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1539635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;
    .locals 17

    .prologue
    .line 1539636
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1539637
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9oV;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1539638
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9oV;->l:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1539639
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9oV;->m:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1539640
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9oV;->o:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1539641
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9oV;->p:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$LikersModel;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1539642
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9oV;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-static {v1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1539643
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9oV;->r:Ljava/lang/String;

    invoke-virtual {v1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1539644
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9oV;->s:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$ResharesModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1539645
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9oV;->t:LX/0Px;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 1539646
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9oV;->u:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$TopLevelCommentsModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1539647
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9oV;->v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1539648
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9oV;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1539649
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9oV;->x:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-static {v1, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1539650
    const/16 v15, 0x19

    invoke-virtual {v1, v15}, LX/186;->c(I)V

    .line 1539651
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->a:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539652
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->b:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539653
    const/4 v15, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->c:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539654
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->d:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539655
    const/4 v15, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->e:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539656
    const/4 v15, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->f:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539657
    const/4 v15, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->g:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539658
    const/4 v15, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->h:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539659
    const/16 v15, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9oV;->i:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, LX/186;->a(IZ)V

    .line 1539660
    const/16 v15, 0x9

    invoke-virtual {v1, v15, v2}, LX/186;->b(II)V

    .line 1539661
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v15, v0, LX/9oV;->k:Z

    invoke-virtual {v1, v2, v15}, LX/186;->a(IZ)V

    .line 1539662
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1539663
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1539664
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9oV;->n:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1539665
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1539666
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1539667
    const/16 v2, 0x10

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1539668
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1539669
    const/16 v2, 0x12

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1539670
    const/16 v2, 0x13

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1539671
    const/16 v2, 0x14

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1539672
    const/16 v2, 0x15

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1539673
    const/16 v2, 0x16

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1539674
    const/16 v2, 0x17

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1539675
    const/16 v2, 0x18

    move-object/from16 v0, p0

    iget v3, v0, LX/9oV;->y:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/186;->a(III)V

    .line 1539676
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1539677
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1539678
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1539679
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1539680
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1539681
    new-instance v2, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;

    invoke-direct {v2, v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;-><init>(LX/15i;)V

    .line 1539682
    return-object v2
.end method
