.class public final LX/8vd;
.super LX/0vn;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field public final synthetic b:LX/8vj;


# direct methods
.method public constructor <init>(LX/8vj;)V
    .locals 0

    .prologue
    .line 1417419
    iput-object p1, p0, LX/8vd;->b:LX/8vj;

    invoke-direct {p0}, LX/0vn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1417372
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 1417373
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0, p1}, LX/8vi;->a(Landroid/view/View;)I

    move-result v1

    .line 1417374
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 1417375
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-nez v0, :cond_1

    .line 1417376
    :cond_0
    :goto_0
    return-void

    .line 1417377
    :cond_1
    iget-object v2, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v2}, LX/8vj;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417378
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    .line 1417379
    iget v2, v0, LX/8vi;->ak:I

    move v0, v2

    .line 1417380
    if-ne v1, v0, :cond_3

    .line 1417381
    invoke-virtual {p2, v3}, LX/3sp;->e(Z)V

    .line 1417382
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1417383
    :goto_1
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1417384
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1417385
    invoke-virtual {p2, v3}, LX/3sp;->f(Z)V

    .line 1417386
    :cond_2
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417387
    const/16 v0, 0x20

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1417388
    invoke-virtual {p2, v3}, LX/3sp;->g(Z)V

    goto :goto_0

    .line 1417389
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1417390
    invoke-super {p0, p1, p2, p3}, LX/0vn;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1417391
    :goto_0
    return v0

    .line 1417392
    :cond_0
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0, p1}, LX/8vi;->a(Landroid/view/View;)I

    move-result v3

    .line 1417393
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 1417394
    if-eq v3, v6, :cond_1

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    .line 1417395
    goto :goto_0

    .line 1417396
    :cond_2
    iget-object v4, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v4}, LX/8vj;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    .line 1417397
    goto :goto_0

    .line 1417398
    :cond_4
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0, v3}, LX/8vi;->d(I)J

    move-result-wide v4

    .line 1417399
    sparse-switch p2, :sswitch_data_0

    move v0, v2

    .line 1417400
    goto :goto_0

    .line 1417401
    :sswitch_0
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    .line 1417402
    iget v4, v0, LX/8vi;->ak:I

    move v0, v4

    .line 1417403
    if-ne v0, v3, :cond_5

    .line 1417404
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0, v6}, LX/8vi;->setSelection(I)V

    move v0, v1

    .line 1417405
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1417406
    goto :goto_0

    .line 1417407
    :sswitch_1
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    .line 1417408
    iget v4, v0, LX/8vi;->ak:I

    move v0, v4

    .line 1417409
    if-eq v0, v3, :cond_6

    .line 1417410
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0, v3}, LX/8vi;->setSelection(I)V

    move v0, v1

    .line 1417411
    goto :goto_0

    :cond_6
    move v0, v2

    .line 1417412
    goto :goto_0

    .line 1417413
    :sswitch_2
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1417414
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0, p1, v3, v4, v5}, LX/8vi;->a(Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_0

    :cond_7
    move v0, v2

    .line 1417415
    goto :goto_0

    .line 1417416
    :sswitch_3
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1417417
    iget-object v0, p0, LX/8vd;->b:LX/8vj;

    invoke-virtual {v0, p1, v3, v4, v5}, LX/8vj;->b(Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_0

    :cond_8
    move v0, v2

    .line 1417418
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method
