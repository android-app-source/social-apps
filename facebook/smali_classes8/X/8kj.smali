.class public LX/8kj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<",
        "LX/8kh;",
        "LX/8ki;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final i:Ljava/lang/Object;


# instance fields
.field public b:Z

.field public c:LX/0aG;

.field public d:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "LX/8kh;",
            "LX/8ki;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/Executor;

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:LX/3dt;

.field public h:LX/0SI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1396767
    const-class v0, LX/8kj;

    sput-object v0, LX/8kj;->a:Ljava/lang/Class;

    .line 1396768
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/8kj;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3dt;LX/0aG;LX/0SI;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1396760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1396761
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8kj;->b:Z

    .line 1396762
    iput-object p1, p0, LX/8kj;->g:LX/3dt;

    .line 1396763
    iput-object p2, p0, LX/8kj;->c:LX/0aG;

    .line 1396764
    iput-object p3, p0, LX/8kj;->h:LX/0SI;

    .line 1396765
    iput-object p4, p0, LX/8kj;->e:Ljava/util/concurrent/Executor;

    .line 1396766
    return-void
.end method

.method public static a(LX/8kh;Ljava/util/List;)LX/0Px;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8kh;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1396751
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1396752
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1396753
    :goto_0
    return-object v0

    .line 1396754
    :cond_1
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1396755
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1396756
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v3, v3

    .line 1396757
    iget-object v4, p0, LX/8kh;->b:LX/4m4;

    invoke-virtual {v3, v4}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1396758
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1396759
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1396655
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 1396656
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1396657
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1396658
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1396659
    :cond_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1396660
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1396661
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1396662
    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1396663
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1396664
    :cond_1
    sget-object v0, LX/8kj;->a:Ljava/lang/Class;

    const-string v4, "Download preview cache out of sync"

    invoke-static {v0, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1

    .line 1396665
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/8kj;
    .locals 10

    .prologue
    .line 1396722
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1396723
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1396724
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1396725
    if-nez v1, :cond_0

    .line 1396726
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1396727
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1396728
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1396729
    sget-object v1, LX/8kj;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1396730
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1396731
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1396732
    :cond_1
    if-nez v1, :cond_4

    .line 1396733
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1396734
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1396735
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1396736
    new-instance p0, LX/8kj;

    invoke-static {v0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v1

    check-cast v1, LX/3dt;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v8

    check-cast v8, LX/0SI;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v1, v7, v8, v9}, LX/8kj;-><init>(LX/3dt;LX/0aG;LX/0SI;Ljava/util/concurrent/Executor;)V

    .line 1396737
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1396738
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1396739
    if-nez v1, :cond_2

    .line 1396740
    sget-object v0, LX/8kj;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8kj;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1396741
    :goto_1
    if-eqz v0, :cond_3

    .line 1396742
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1396743
    :goto_3
    check-cast v0, LX/8kj;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1396744
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1396745
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1396746
    :catchall_1
    move-exception v0

    .line 1396747
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1396748
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1396749
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1396750
    :cond_2
    :try_start_8
    sget-object v0, LX/8kj;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8kj;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static b(LX/8kj;LX/8kh;LX/3do;)LX/1ML;
    .locals 4

    .prologue
    .line 1396703
    new-instance v0, LX/3ea;

    .line 1396704
    if-eqz p1, :cond_0

    iget-object v1, p1, LX/8kh;->a:LX/8kg;

    if-nez v1, :cond_1

    .line 1396705
    :cond_0
    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    .line 1396706
    :goto_0
    move-object v1, v1

    .line 1396707
    invoke-direct {v0, p2, v1}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    iget-object v1, p1, LX/8kh;->b:LX/4m4;

    invoke-static {v1}, LX/8jc;->a(LX/4m4;)Ljava/lang/String;

    move-result-object v1

    .line 1396708
    iput-object v1, v0, LX/3ea;->c:Ljava/lang/String;

    .line 1396709
    move-object v0, v0

    .line 1396710
    invoke-virtual {v0}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v0

    .line 1396711
    iget-object v1, p0, LX/8kj;->c:LX/0aG;

    const-string v2, "fetch_sticker_packs"

    .line 1396712
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1396713
    const-string p1, "fetchStickerPacksParams"

    invoke-virtual {v3, p1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1396714
    const-string p1, "overridden_viewer_context"

    iget-object p2, p0, LX/8kj;->h:LX/0SI;

    invoke-interface {p2}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p2

    invoke-virtual {v3, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1396715
    move-object v0, v3

    .line 1396716
    const v3, 0x49109bb2    # 592315.1f

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1396717
    return-object v0

    .line 1396718
    :cond_1
    sget-object v1, LX/8kf;->a:[I

    iget-object v2, p1, LX/8kh;->a:LX/8kg;

    invoke-virtual {v2}, LX/8kg;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1396719
    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    goto :goto_0

    .line 1396720
    :pswitch_0
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    goto :goto_0

    .line 1396721
    :pswitch_1
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1396698
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8kj;->b:Z

    .line 1396699
    iget-object v0, p0, LX/8kj;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1396700
    iget-object v0, p0, LX/8kj;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1396701
    const/4 v0, 0x0

    iput-object v0, p0, LX/8kj;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1396702
    :cond_0
    return-void
.end method

.method public final a(LX/8kh;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1396666
    iget-object v1, p0, LX/8kj;->d:LX/3Mb;

    if-nez v1, :cond_0

    .line 1396667
    :goto_0
    return-void

    .line 1396668
    :cond_0
    iput-boolean v0, p0, LX/8kj;->b:Z

    .line 1396669
    sget-object v1, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    .line 1396670
    iget-object v2, p0, LX/8kj;->g:LX/3dt;

    invoke-virtual {v2, v1}, LX/3dt;->a(LX/3do;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1396671
    iget-object v2, p0, LX/8kj;->g:LX/3dt;

    invoke-virtual {v2, v1}, LX/3dt;->b(LX/3do;)LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/8kj;->a(LX/8kh;Ljava/util/List;)LX/0Px;

    move-result-object v2

    .line 1396672
    :goto_1
    move-object v1, v2

    .line 1396673
    iget-object v2, p0, LX/8kj;->g:LX/3dt;

    invoke-virtual {v2}, LX/3dt;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1396674
    iget-object v2, p0, LX/8kj;->g:LX/3dt;

    invoke-virtual {v2}, LX/3dt;->a()LX/0Px;

    move-result-object v2

    .line 1396675
    :goto_2
    move-object v2, v2

    .line 1396676
    iget-object v3, p0, LX/8kj;->g:LX/3dt;

    invoke-virtual {v3}, LX/3dt;->d()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1396677
    iget-object v3, p0, LX/8kj;->g:LX/3dt;

    invoke-virtual {v3}, LX/3dt;->c()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/8kj;->a(LX/8kh;Ljava/util/List;)LX/0Px;

    move-result-object v3

    .line 1396678
    :goto_3
    move-object v3, v3

    .line 1396679
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    iget-object v4, p1, LX/8kh;->a:LX/8kg;

    sget-object v5, LX/8kg;->CHECK_SERVER_FOR_NEW_DATA:LX/8kg;

    if-eq v4, v5, :cond_1

    .line 1396680
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1396681
    invoke-interface {v4, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1396682
    :goto_4
    if-nez v0, :cond_2

    .line 1396683
    iget-object v0, p0, LX/8kj;->d:LX/3Mb;

    new-instance v4, LX/8ki;

    invoke-static {v1, v3}, LX/8kj;->a(Ljava/util/List;Ljava/util/List;)LX/0Px;

    move-result-object v3

    invoke-direct {v4, v1, v3, v2}, LX/8ki;-><init>(LX/0Px;LX/0Px;Ljava/util/List;)V

    invoke-interface {v0, p1, v4}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1396684
    :cond_1
    const/4 v0, 0x1

    goto :goto_4

    .line 1396685
    :cond_2
    iget-object v0, p0, LX/8kj;->d:LX/3Mb;

    .line 1396686
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 1396687
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1396688
    sget-object v3, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-static {p0, p1, v3}, LX/8kj;->b(LX/8kj;LX/8kh;LX/3do;)LX/1ML;

    move-result-object v3

    .line 1396689
    iget-object v4, p0, LX/8kj;->c:LX/0aG;

    const-string v5, "fetch_recent_stickers"

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const v7, 0x4b19ee9

    invoke-static {v4, v5, v6, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 1396690
    move-object v4, v4

    .line 1396691
    iget-object v5, p0, LX/8kj;->c:LX/0aG;

    const-string v6, "fetch_download_preview_sticker_packs"

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const v8, 0x36704acc

    invoke-static {v5, v6, v7, v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    .line 1396692
    move-object v5, v5

    .line 1396693
    const/4 v6, 0x3

    new-array v6, v6, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v4, 0x1

    aput-object v3, v6, v4

    const/4 v3, 0x2

    aput-object v5, v6, v3

    invoke-static {v6}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, p0, LX/8kj;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1396694
    iget-object v3, p0, LX/8kj;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/8ke;

    invoke-direct {v4, p0, v2, p1, v1}, LX/8ke;-><init>(LX/8kj;Ljava/util/List;LX/8kh;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v2, p0, LX/8kj;->e:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1396695
    const-string v2, "fetchStickerPacksAsync (DOWNLOADED) started"

    invoke-static {v2}, LX/0PR;->b(Ljava/lang/String;)V

    .line 1396696
    move-object v1, v1

    .line 1396697
    invoke-interface {v0, p1, v1}, LX/3Mb;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_3
.end method
