.class public LX/9I3;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9I4;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9I3",
            "<TE;>.java/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9I4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462458
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462459
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/9I3;->b:LX/0Zi;

    .line 1462460
    iput-object p1, p0, LX/9I3;->a:LX/0Ot;

    .line 1462461
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1462462
    check-cast p2, LX/9I2;

    .line 1462463
    iget-object v0, p0, LX/9I3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9I4;

    iget-object v1, p2, LX/9I2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v2, p2, LX/9I2;->b:Z

    iget-object v3, p2, LX/9I2;->c:LX/1Pq;

    const/4 v5, 0x0

    .line 1462464
    invoke-static {v1}, LX/1zW;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v4

    .line 1462465
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v6

    .line 1462466
    iput-boolean v2, v6, LX/3mP;->a:Z

    .line 1462467
    move-object v6, v6

    .line 1462468
    invoke-interface {v4}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v7

    .line 1462469
    iput-object v7, v6, LX/3mP;->d:LX/25L;

    .line 1462470
    move-object v6, v6

    .line 1462471
    iput-object v4, v6, LX/3mP;->e:LX/0jW;

    .line 1462472
    move-object v4, v6

    .line 1462473
    invoke-virtual {v4}, LX/3mP;->a()LX/25M;

    move-result-object v7

    .line 1462474
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1462475
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1462476
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    .line 1462477
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v6, v5

    :goto_0
    if-ge v6, v11, :cond_0

    invoke-virtual {v10, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1462478
    new-instance p0, LX/9I5;

    sget-object p2, LX/9I6;->PENDING:LX/9I6;

    invoke-direct {p0, v4, p2}, LX/9I5;-><init>(Lcom/facebook/graphql/model/GraphQLPage;LX/9I6;)V

    invoke-virtual {v8, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1462479
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 1462480
    :cond_0
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    :goto_1
    if-ge v5, v9, :cond_1

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1462481
    new-instance v10, LX/9I5;

    sget-object v11, LX/9I6;->CONFIRMED:LX/9I6;

    invoke-direct {v10, v4, v11}, LX/9I5;-><init>(Lcom/facebook/graphql/model/GraphQLPage;LX/9I6;)V

    invoke-virtual {v8, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1462482
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 1462483
    :cond_1
    iget-object v4, v0, LX/9I4;->b:LX/9I8;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1462484
    new-instance v8, LX/9I7;

    const-class v6, Landroid/content/Context;

    invoke-interface {v4, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    check-cast v3, LX/1Pq;

    invoke-direct {v8, v6, v5, v3, v7}, LX/9I7;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1462485
    move-object v4, v8

    .line 1462486
    iget-object v5, v0, LX/9I4;->a:LX/3mL;

    invoke-virtual {v5, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->b()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1462487
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1462488
    invoke-static {}, LX/1dS;->b()V

    .line 1462489
    const/4 v0, 0x0

    return-object v0
.end method
