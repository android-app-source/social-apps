.class public LX/91Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/91M;

.field public final b:LX/8zJ;

.field public final c:LX/1vg;


# direct methods
.method public constructor <init>(LX/91M;LX/8zJ;LX/1vg;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1430728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430729
    iput-object p1, p0, LX/91Q;->a:LX/91M;

    .line 1430730
    iput-object p2, p0, LX/91Q;->b:LX/8zJ;

    .line 1430731
    iput-object p3, p0, LX/91Q;->c:LX/1vg;

    .line 1430732
    return-void
.end method

.method public static a(LX/0QB;)LX/91Q;
    .locals 6

    .prologue
    .line 1430733
    const-class v1, LX/91Q;

    monitor-enter v1

    .line 1430734
    :try_start_0
    sget-object v0, LX/91Q;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1430735
    sput-object v2, LX/91Q;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1430736
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430737
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1430738
    new-instance p0, LX/91Q;

    invoke-static {v0}, LX/91M;->a(LX/0QB;)LX/91M;

    move-result-object v3

    check-cast v3, LX/91M;

    invoke-static {v0}, LX/8zJ;->b(LX/0QB;)LX/8zJ;

    move-result-object v4

    check-cast v4, LX/8zJ;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-direct {p0, v3, v4, v5}, LX/91Q;-><init>(LX/91M;LX/8zJ;LX/1vg;)V

    .line 1430739
    move-object v0, p0

    .line 1430740
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1430741
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430742
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1430743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
