.class public final enum LX/8wW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8wW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8wW;

.field public static final enum CANCEL_FLOW:LX/8wW;

.field public static final enum ENTER_FLOW:LX/8wW;

.field public static final enum EVENT_CLIENT_INELIGIBLE_FLOW_ACTION_CLICK:LX/8wW;

.field public static final enum EVENT_CLIENT_INELIGIBLE_FLOW_CANCEL:LX/8wW;

.field public static final enum EVENT_CREATE_DIALOG_OPEN:LX/8wW;

.field public static final enum EVENT_RENDER_CREATE_ENTRY_POINT:LX/8wW;

.field public static final enum EVENT_RENDER_EDIT_ENTRY_POINT:LX/8wW;

.field public static final enum EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

.field public static final enum EVENT_RENDER_INSIGHTS_ENTRY_POINT:LX/8wW;

.field public static final enum EXIT_FLOW:LX/8wW;

.field public static final enum SUBMIT_FLOW:LX/8wW;

.field public static final enum SUBMIT_FLOW_CLICK:LX/8wW;

.field public static final enum SUBMIT_FLOW_ERROR:LX/8wW;


# instance fields
.field private final event:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1422745
    new-instance v0, LX/8wW;

    const-string v1, "EVENT_RENDER_CREATE_ENTRY_POINT"

    const-string v2, "render_create_entry_button"

    invoke-direct {v0, v1, v4, v2}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EVENT_RENDER_CREATE_ENTRY_POINT:LX/8wW;

    .line 1422746
    new-instance v0, LX/8wW;

    const-string v1, "EVENT_RENDER_EDIT_ENTRY_POINT"

    const-string v2, "render_edit_entry_button"

    invoke-direct {v0, v1, v5, v2}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EVENT_RENDER_EDIT_ENTRY_POINT:LX/8wW;

    .line 1422747
    new-instance v0, LX/8wW;

    const-string v1, "EVENT_RENDER_FAIL_ENTRY_POINT"

    const-string v2, "render_entry_button_error"

    invoke-direct {v0, v1, v6, v2}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    .line 1422748
    new-instance v0, LX/8wW;

    const-string v1, "EVENT_RENDER_INSIGHTS_ENTRY_POINT"

    const-string v2, "render_insights_entry_button"

    invoke-direct {v0, v1, v7, v2}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EVENT_RENDER_INSIGHTS_ENTRY_POINT:LX/8wW;

    .line 1422749
    new-instance v0, LX/8wW;

    const-string v1, "EVENT_CREATE_DIALOG_OPEN"

    const-string v2, "create_dialog_open"

    invoke-direct {v0, v1, v8, v2}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EVENT_CREATE_DIALOG_OPEN:LX/8wW;

    .line 1422750
    new-instance v0, LX/8wW;

    const-string v1, "EVENT_CLIENT_INELIGIBLE_FLOW_ACTION_CLICK"

    const/4 v2, 0x5

    const-string v3, "client_ineligible_flow_action_click"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EVENT_CLIENT_INELIGIBLE_FLOW_ACTION_CLICK:LX/8wW;

    .line 1422751
    new-instance v0, LX/8wW;

    const-string v1, "EVENT_CLIENT_INELIGIBLE_FLOW_CANCEL"

    const/4 v2, 0x6

    const-string v3, "client_ineligible_flow_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EVENT_CLIENT_INELIGIBLE_FLOW_CANCEL:LX/8wW;

    .line 1422752
    new-instance v0, LX/8wW;

    const-string v1, "ENTER_FLOW"

    const/4 v2, 0x7

    const-string v3, "enter_flow"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->ENTER_FLOW:LX/8wW;

    .line 1422753
    new-instance v0, LX/8wW;

    const-string v1, "EXIT_FLOW"

    const/16 v2, 0x8

    const-string v3, "exit_flow"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->EXIT_FLOW:LX/8wW;

    .line 1422754
    new-instance v0, LX/8wW;

    const-string v1, "SUBMIT_FLOW_CLICK"

    const/16 v2, 0x9

    const-string v3, "submit_flow_click"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->SUBMIT_FLOW_CLICK:LX/8wW;

    .line 1422755
    new-instance v0, LX/8wW;

    const-string v1, "CANCEL_FLOW"

    const/16 v2, 0xa

    const-string v3, "cancel_flow"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->CANCEL_FLOW:LX/8wW;

    .line 1422756
    new-instance v0, LX/8wW;

    const-string v1, "SUBMIT_FLOW"

    const/16 v2, 0xb

    const-string v3, "submit_flow"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->SUBMIT_FLOW:LX/8wW;

    .line 1422757
    new-instance v0, LX/8wW;

    const-string v1, "SUBMIT_FLOW_ERROR"

    const/16 v2, 0xc

    const-string v3, "submit_flow_error"

    invoke-direct {v0, v1, v2, v3}, LX/8wW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wW;->SUBMIT_FLOW_ERROR:LX/8wW;

    .line 1422758
    const/16 v0, 0xd

    new-array v0, v0, [LX/8wW;

    sget-object v1, LX/8wW;->EVENT_RENDER_CREATE_ENTRY_POINT:LX/8wW;

    aput-object v1, v0, v4

    sget-object v1, LX/8wW;->EVENT_RENDER_EDIT_ENTRY_POINT:LX/8wW;

    aput-object v1, v0, v5

    sget-object v1, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    aput-object v1, v0, v6

    sget-object v1, LX/8wW;->EVENT_RENDER_INSIGHTS_ENTRY_POINT:LX/8wW;

    aput-object v1, v0, v7

    sget-object v1, LX/8wW;->EVENT_CREATE_DIALOG_OPEN:LX/8wW;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8wW;->EVENT_CLIENT_INELIGIBLE_FLOW_ACTION_CLICK:LX/8wW;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8wW;->EVENT_CLIENT_INELIGIBLE_FLOW_CANCEL:LX/8wW;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8wW;->ENTER_FLOW:LX/8wW;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8wW;->EXIT_FLOW:LX/8wW;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8wW;->SUBMIT_FLOW_CLICK:LX/8wW;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8wW;->CANCEL_FLOW:LX/8wW;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8wW;->SUBMIT_FLOW:LX/8wW;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8wW;->SUBMIT_FLOW_ERROR:LX/8wW;

    aput-object v2, v0, v1

    sput-object v0, LX/8wW;->$VALUES:[LX/8wW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1422759
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1422760
    iput-object p3, p0, LX/8wW;->event:Ljava/lang/String;

    .line 1422761
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8wW;
    .locals 1

    .prologue
    .line 1422762
    const-class v0, LX/8wW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8wW;

    return-object v0
.end method

.method public static values()[LX/8wW;
    .locals 1

    .prologue
    .line 1422763
    sget-object v0, LX/8wW;->$VALUES:[LX/8wW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8wW;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1422764
    iget-object v0, p0, LX/8wW;->event:Ljava/lang/String;

    return-object v0
.end method
