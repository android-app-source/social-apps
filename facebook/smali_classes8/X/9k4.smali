.class public LX/9k4;
.super LX/3Tf;
.source ""


# static fields
.field private static i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;"
        }
    .end annotation
.end field

.field private static j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:J

.field private final f:Lcom/facebook/ipc/model/PageTopic;

.field private final g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IJ)V
    .locals 5

    .prologue
    .line 1530775
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 1530776
    iput-object p1, p0, LX/9k4;->c:Landroid/content/Context;

    .line 1530777
    iput p2, p0, LX/9k4;->d:I

    .line 1530778
    iput-wide p3, p0, LX/9k4;->e:J

    .line 1530779
    iget v0, p0, LX/9k4;->d:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/9k4;->g:Z

    .line 1530780
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9k4;->h:Ljava/util/List;

    .line 1530781
    iget v0, p0, LX/9k4;->d:I

    if-nez v0, :cond_1

    .line 1530782
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0}, LX/9k4;->a(Ljava/util/List;)V

    .line 1530783
    :goto_1
    sget-object v0, LX/9k4;->i:Ljava/util/Map;

    iget-wide v2, p0, LX/9k4;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    iput-object v0, p0, LX/9k4;->f:Lcom/facebook/ipc/model/PageTopic;

    .line 1530784
    return-void

    .line 1530785
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1530786
    :cond_1
    invoke-direct {p0}, LX/9k4;->d()V

    goto :goto_1
.end method

.method public static a(J)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1530770
    sget-object v0, LX/9k4;->i:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    .line 1530771
    sget-object v1, LX/9k4;->j:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1530772
    if-eqz v0, :cond_0

    .line 1530773
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 1530774
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/ipc/model/PageTopic;Lcom/facebook/ipc/model/PageTopic;)V
    .locals 2

    .prologue
    .line 1530787
    sget-object v0, LX/9k4;->j:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1530788
    if-nez v0, :cond_0

    .line 1530789
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1530790
    sget-object v1, LX/9k4;->j:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1530791
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530792
    return-void
.end method

.method private b(J)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1530814
    invoke-static {p1, p2}, LX/9k4;->a(J)Ljava/util/List;

    move-result-object v3

    .line 1530815
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1530816
    new-instance v0, LX/9k2;

    invoke-direct {v0, p0}, LX/9k2;-><init>(LX/9k4;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1530817
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1530818
    const/4 v0, 0x3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v1, v2

    .line 1530819
    :goto_0
    if-ge v1, v5, :cond_0

    .line 1530820
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    iget-object v0, v0, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530821
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1530822
    :cond_0
    const-string v0, ", "

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v0, v1}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1530823
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1530793
    iget-wide v0, p0, LX/9k4;->e:J

    invoke-static {v0, v1}, LX/9k4;->a(J)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/9k4;->h:Ljava/util/List;

    .line 1530794
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1530795
    const/4 v0, 0x0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1530796
    if-nez p4, :cond_0

    .line 1530797
    iget-object v0, p0, LX/9k4;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1530798
    const v1, 0x7f030eaf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 1530799
    :cond_0
    iget-boolean v0, p0, LX/9k4;->g:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 1530800
    const v0, 0x7f0d23e3

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/9k4;->f:Lcom/facebook/ipc/model/PageTopic;

    iget-object v1, v1, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1530801
    const v0, 0x7f0d23e4

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1530802
    const v0, 0x7f0d23e5

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1530803
    :goto_0
    return-object p4

    .line 1530804
    :cond_1
    const v0, 0x7f0d23e3

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/9k4;->h:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/PageTopic;

    iget-object v1, v1, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1530805
    iget-object v0, p0, LX/9k4;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    iget-wide v0, v0, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-direct {p0, v0, v1}, LX/9k4;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 1530806
    if-nez v0, :cond_2

    .line 1530807
    const v0, 0x7f0d23e4

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1530808
    const v0, 0x7f0d23e5

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1530809
    :cond_2
    const v0, 0x7f0d23e5

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1530810
    iget v0, p0, LX/9k4;->d:I

    if-nez v0, :cond_3

    .line 1530811
    const v0, 0x7f0d23e4

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1530812
    const v0, 0x7f0d23e4

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/9k4;->h:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/PageTopic;

    iget-wide v2, v1, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-direct {p0, v2, v3}, LX/9k4;->b(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1530813
    :cond_3
    const v0, 0x7f0d23e4

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1530757
    iget-boolean v0, p0, LX/9k4;->g:Z

    if-nez v0, :cond_0

    .line 1530758
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, LX/9k4;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1530759
    :goto_0
    return-object v0

    .line 1530760
    :cond_0
    if-nez p2, :cond_1

    .line 1530761
    iget-object v0, p0, LX/9k4;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1530762
    const v1, 0x7f030eb0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1530763
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 1530764
    const v0, 0x7f0d02a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, LX/9k4;->c:Landroid/content/Context;

    const v3, 0x7f0816b8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    move-object v0, v1

    .line 1530765
    goto :goto_0

    .line 1530766
    :pswitch_0
    const v0, 0x7f0d02a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, LX/9k4;->c:Landroid/content/Context;

    const v3, 0x7f0816b7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_1
    move-object v1, p2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1530767
    iget-boolean v0, p0, LX/9k4;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/9k4;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1530768
    :cond_0
    iget-object v0, p0, LX/9k4;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1530769
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/9k4;->f:Lcom/facebook/ipc/model/PageTopic;

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1530744
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/9k4;->i:Ljava/util/Map;

    .line 1530745
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/9k4;->j:Ljava/util/Map;

    .line 1530746
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    .line 1530747
    sget-object v2, LX/9k4;->i:Ljava/util/Map;

    iget-wide v4, v0, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1530748
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    .line 1530749
    iget-object v1, v0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 1530750
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1, v0}, LX/9k4;->a(Lcom/facebook/ipc/model/PageTopic;Lcom/facebook/ipc/model/PageTopic;)V

    goto :goto_1

    .line 1530751
    :cond_3
    iget-object v1, v0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 1530752
    sget-object v4, LX/9k4;->i:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/PageTopic;

    invoke-static {v1, v0}, LX/9k4;->a(Lcom/facebook/ipc/model/PageTopic;Lcom/facebook/ipc/model/PageTopic;)V

    goto :goto_2

    .line 1530753
    :cond_4
    sget-object v0, LX/9k4;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1530754
    new-instance v2, LX/9k3;

    invoke-direct {v2, p0}, LX/9k3;-><init>(LX/9k4;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_3

    .line 1530755
    :cond_5
    invoke-direct {p0}, LX/9k4;->d()V

    .line 1530756
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1530743
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1530742
    sget-object v0, LX/9k4;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 1530741
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1530740
    iget-boolean v0, p0, LX/9k4;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 1530734
    iget-boolean v0, p0, LX/9k4;->g:Z

    if-eqz v0, :cond_1

    .line 1530735
    if-nez p1, :cond_0

    .line 1530736
    const/4 v0, 0x1

    .line 1530737
    :goto_0
    return v0

    .line 1530738
    :cond_0
    iget-object v0, p0, LX/9k4;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 1530739
    :cond_1
    iget-object v0, p0, LX/9k4;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 1530732
    const/4 v0, 0x1

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1530733
    const/4 v0, 0x2

    return v0
.end method
