.class public final LX/A8a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3L3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3L3",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/text/TextUtils$TruncateAt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1627848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 1627847
    invoke-direct {p0}, LX/A8a;-><init>()V

    return-void
.end method

.method public static final a(Ljava/lang/CharSequence;IFF)F
    .locals 2

    .prologue
    .line 1627841
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 1627842
    :goto_0
    cmpl-float v1, p2, p3

    if-lez v1, :cond_0

    .line 1627843
    invoke-virtual {v0, p2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1627844
    invoke-static {v0, p0, p1}, LX/A8a;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1627845
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr p2, v1

    goto :goto_0

    .line 1627846
    :cond_0
    return p2
.end method

.method private static a(Ljava/util/List;Ljava/lang/CharSequence;I)LX/A8b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/text/TextPaint;",
            ">;",
            "Ljava/lang/CharSequence;",
            "I)",
            "LX/A8b;"
        }
    .end annotation

    .prologue
    .line 1627837
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextPaint;

    .line 1627838
    invoke-static {v0, p1, p2}, LX/A8a;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1627839
    new-instance v1, LX/A8b;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/A8b;-><init>(Landroid/text/TextPaint;Ljava/util/List;)V

    move-object v0, v1

    .line 1627840
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/A8b;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextPaint;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/A8b;-><init>(Landroid/text/TextPaint;Ljava/util/List;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<",
            "Landroid/text/TextPaint;",
            ">;I",
            "Landroid/text/Layout$Alignment;",
            "II)",
            "Landroid/text/Layout;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1627815
    if-ne p5, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1627816
    invoke-static {p2, p1, p3}, LX/A8a;->a(Ljava/util/List;Ljava/lang/CharSequence;I)LX/A8b;

    move-result-object v0

    .line 1627817
    iget-object v1, v0, LX/A8b;->b:Ljava/util/List;

    iget-object v0, v0, LX/A8b;->a:Landroid/text/TextPaint;

    invoke-direct {p0, v1, v0, p3, p4}, LX/A8a;->a(Ljava/util/List;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0

    .line 1627818
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Landroid/text/TextPaint;",
            "I",
            "Landroid/text/Layout$Alignment;",
            ")",
            "Landroid/text/StaticLayout;"
        }
    .end annotation

    .prologue
    .line 1627825
    if-nez p1, :cond_0

    .line 1627826
    const/4 v0, 0x0

    .line 1627827
    :goto_0
    return-object v0

    .line 1627828
    :cond_0
    const/4 v0, 0x1

    .line 1627829
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1627830
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 1627831
    if-nez v2, :cond_1

    .line 1627832
    const-string v2, "\n"

    invoke-interface {v1, v2}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 1627833
    :cond_1
    invoke-interface {v1, v0}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 1627834
    const/4 v0, 0x0

    move v2, v0

    .line 1627835
    goto :goto_1

    .line 1627836
    :cond_2
    new-instance v0, Landroid/text/StaticLayout;

    const/4 v2, 0x0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    const/16 v5, 0x4000

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p0, LX/A8a;->a:Landroid/text/TextUtils$TruncateAt;

    move-object v4, p2

    move-object/from16 v6, p4

    move v11, p3

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    goto :goto_0
.end method

.method private static a(Landroid/text/TextPaint;Ljava/lang/CharSequence;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1627823
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    .line 1627824
    int-to-float v2, p2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;
    .locals 7

    .prologue
    .line 1627822
    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, LX/A8a;->a(Ljava/lang/CharSequence;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1627819
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/A8a;->a:Landroid/text/TextUtils$TruncateAt;

    .line 1627820
    return-void

    .line 1627821
    :cond_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    goto :goto_0
.end method
