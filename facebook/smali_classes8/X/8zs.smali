.class public LX/8zs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public final b:LX/8zj;

.field public final c:Ljava/lang/String;

.field public final d:LX/909;

.field public final e:LX/92o;

.field public final f:Ljava/util/Locale;

.field public final g:LX/918;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8zj;Ljava/lang/String;LX/909;LX/92o;LX/0W9;LX/918;)V
    .locals 1
    .param p1    # LX/8zj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/909;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1428211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1428212
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1428213
    iput-object v0, p0, LX/8zs;->h:LX/0Px;

    .line 1428214
    iput-object p1, p0, LX/8zs;->b:LX/8zj;

    .line 1428215
    iput-object p2, p0, LX/8zs;->c:Ljava/lang/String;

    .line 1428216
    iput-object p3, p0, LX/8zs;->d:LX/909;

    .line 1428217
    iput-object p4, p0, LX/8zs;->e:LX/92o;

    .line 1428218
    invoke-virtual {p5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/8zs;->f:Ljava/util/Locale;

    .line 1428219
    iput-object p6, p0, LX/8zs;->g:LX/918;

    .line 1428220
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1428221
    iget-object v0, p0, LX/8zs;->e:LX/92o;

    new-instance v1, LX/8zr;

    invoke-direct {v1, p0}, LX/8zr;-><init>(LX/8zs;)V

    .line 1428222
    invoke-static {v0, v1}, LX/92o;->c(LX/92o;LX/8zq;)LX/8zq;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/92o;->a(LX/8zq;)V

    .line 1428223
    return-void
.end method
