.class public final enum LX/ASm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ASm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ASm;

.field public static final enum PAUSE_ON_CLICK:LX/ASm;

.field public static final enum RESUME_ON_CLICK:LX/ASm;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1674436
    new-instance v0, LX/ASm;

    const-string v1, "PAUSE_ON_CLICK"

    invoke-direct {v0, v1, v2}, LX/ASm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASm;->PAUSE_ON_CLICK:LX/ASm;

    .line 1674437
    new-instance v0, LX/ASm;

    const-string v1, "RESUME_ON_CLICK"

    invoke-direct {v0, v1, v3}, LX/ASm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASm;->RESUME_ON_CLICK:LX/ASm;

    .line 1674438
    const/4 v0, 0x2

    new-array v0, v0, [LX/ASm;

    sget-object v1, LX/ASm;->PAUSE_ON_CLICK:LX/ASm;

    aput-object v1, v0, v2

    sget-object v1, LX/ASm;->RESUME_ON_CLICK:LX/ASm;

    aput-object v1, v0, v3

    sput-object v0, LX/ASm;->$VALUES:[LX/ASm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1674439
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ASm;
    .locals 1

    .prologue
    .line 1674440
    const-class v0, LX/ASm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ASm;

    return-object v0
.end method

.method public static values()[LX/ASm;
    .locals 1

    .prologue
    .line 1674441
    sget-object v0, LX/ASm;->$VALUES:[LX/ASm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ASm;

    return-object v0
.end method
