.class public final LX/8xh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsInterfaces$PlaceListLightweightCreateFields;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V
    .locals 0

    .prologue
    .line 1424554
    iput-object p1, p0, LX/8xh;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1424551
    iget-object v0, p0, LX/8xh;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->setResult(I)V

    .line 1424552
    iget-object v0, p0, LX/8xh;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->finish()V

    .line 1424553
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1424549
    iget-object v0, p0, LX/8xh;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, LX/8xh;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    const v2, 0x7f0823e7

    invoke-virtual {v1, v2}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setErrorMessage(Ljava/lang/CharSequence;)V

    .line 1424550
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424548
    invoke-direct {p0}, LX/8xh;->a()V

    return-void
.end method
