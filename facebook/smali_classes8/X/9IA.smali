.class public LX/9IA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9I9;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9IB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462567
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9IA;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9IB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462568
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462569
    iput-object p1, p0, LX/9IA;->b:LX/0Ot;

    .line 1462570
    return-void
.end method

.method public static a(LX/0QB;)LX/9IA;
    .locals 4

    .prologue
    .line 1462571
    const-class v1, LX/9IA;

    monitor-enter v1

    .line 1462572
    :try_start_0
    sget-object v0, LX/9IA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462573
    sput-object v2, LX/9IA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462574
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462575
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462576
    new-instance v3, LX/9IA;

    const/16 p0, 0x1de4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9IA;-><init>(LX/0Ot;)V

    .line 1462577
    move-object v0, v3

    .line 1462578
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462579
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462580
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462581
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1462582
    check-cast p2, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;

    .line 1462583
    iget-object v0, p0, LX/9IA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9IB;

    iget-object v1, p2, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->a:LX/9GM;

    iget-object v2, p2, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->b:LX/9FD;

    iget-object v3, p2, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v6, 0x2

    .line 1462584
    iget-object v4, v1, LX/9GM;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1462585
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x7

    .line 1462586
    iget v7, v2, LX/9FD;->b:I

    move v7, v7

    .line 1462587
    invoke-interface {v5, v6, v7}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v5

    .line 1462588
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    .line 1462589
    if-eqz v6, :cond_2

    invoke-static {v6}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v6}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1462590
    :goto_0
    iget-object v7, v0, LX/9IB;->b:LX/1nu;

    invoke-virtual {v7, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v6

    sget-object v7, LX/1Up;->g:LX/1Up;

    invoke-virtual {v6, v7}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    const v7, 0x7f02111f

    invoke-virtual {v6, v7}, LX/1nw;->h(I)LX/1nw;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b08d0

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b08d0

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 1462591
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    const/4 v1, 0x2

    const/4 v3, 0x5

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v9, 0x4

    .line 1462592
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->B()Z

    move-result p0

    .line 1462593
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->w()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    move v6, v7

    .line 1462594
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p2

    invoke-interface {p2, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p2

    invoke-interface {p2, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p2

    const v1, 0x7f0b08d7

    invoke-interface {p2, v9, v1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p2

    const v1, 0x7f0b08d8

    invoke-interface {p2, v3, v1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p2

    .line 1462595
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-static {v4}, LX/36l;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    sget-object v2, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, v2}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b08d0

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    invoke-interface {p2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1462596
    if-eqz p0, :cond_0

    .line 1462597
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f021a25

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v9, v9}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v3, v8}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    invoke-interface {p2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1462598
    :cond_0
    if-eqz v6, :cond_1

    .line 1462599
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    iget-object v1, v0, LX/9IB;->c:LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    const v2, 0x7f0208d6

    invoke-virtual {v1, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    const v2, -0x413d37

    invoke-virtual {v1, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    if-eqz p0, :cond_4

    :goto_2
    invoke-interface {v6, v9, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, v3, v8}, LX/1Di;->h(II)LX/1Di;

    move-result-object v6

    invoke-interface {p2, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1462600
    :cond_1
    move-object v6, p2

    .line 1462601
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    .line 1462602
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1462603
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1462604
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1462605
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1462606
    iget-object v8, v0, LX/9IB;->a:LX/1zC;

    const v9, 0x7f0b004e

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v8, v7, v6}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 1462607
    :goto_3
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004e

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    sget-object v7, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v6, v7}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x4

    const v8, 0x7f0b08d9

    invoke-interface {v6, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x5

    const v8, 0x7f0b08da

    invoke-interface {v6, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b08d0

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    move-object v4, v6

    .line 1462608
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1462609
    const v5, 0x2f4b43de

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1462610
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1462611
    return-object v0

    .line 1462612
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_3
    move v6, v8

    .line 1462613
    goto/16 :goto_1

    :cond_4
    move v7, v9

    .line 1462614
    goto/16 :goto_2

    .line 1462615
    :cond_5
    invoke-static {v4}, LX/36l;->c(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-static {v8}, LX/1VO;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1462616
    const v8, 0x7f080fef

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_3

    .line 1462617
    :cond_6
    const v8, 0x7f080fee

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1462618
    invoke-static {}, LX/1dS;->b()V

    .line 1462619
    iget v0, p1, LX/1dQ;->b:I

    .line 1462620
    packed-switch v0, :pswitch_data_0

    .line 1462621
    :goto_0
    return-object v2

    .line 1462622
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1462623
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1462624
    check-cast v1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;

    .line 1462625
    iget-object v3, p0, LX/9IA;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, Lcom/facebook/feedback/ui/rows/views/specs/CommentReplyComponent$CommentReplyComponentImpl;->a:LX/9GM;

    .line 1462626
    iget-object p1, v3, LX/9GM;->d:LX/9Gs;

    iget-object p2, v3, LX/9GM;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1462627
    iget-object p0, p1, LX/9Gs;->c:Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    iget-object p0, p0, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->d:LX/3iR;

    iget-object v1, p1, LX/9Gs;->a:LX/9FA;

    .line 1462628
    iget-object v3, v1, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v3

    .line 1462629
    const-string v3, "comment_reply_preview_clicked"

    invoke-static {p0, v3, v1}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v3

    .line 1462630
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1462631
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 1462632
    :cond_0
    iget-object p0, p1, LX/9Gs;->a:LX/9FA;

    iget-object v1, p1, LX/9Gs;->b:LX/9Gt;

    iget-object v1, v1, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p1, LX/9Gs;->b:LX/9Gt;

    iget-object v3, v3, LX/9Gt;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1462633
    iget-object p1, p0, LX/9FA;->b:LX/9Bi;

    if-eqz p1, :cond_1

    .line 1462634
    iget-object p1, p0, LX/9FA;->b:LX/9Bi;

    iget-object v0, p0, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-interface {p1, v1, p2, v3, v0}, LX/9Bi;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1462635
    :cond_1
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2f4b43de
        :pswitch_0
    .end packed-switch
.end method
