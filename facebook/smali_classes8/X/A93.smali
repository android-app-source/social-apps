.class public final LX/A93;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "boostedPostDefaultSpec"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "boostedPostDefaultSpec"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteWebsiteAdminInfoModel$BoostedWebsitePromotionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1629491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1629492
    return-void
.end method

.method public static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)LX/A93;
    .locals 4

    .prologue
    .line 1629493
    new-instance v0, LX/A93;

    invoke-direct {v0}, LX/A93;-><init>()V

    .line 1629494
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v1

    iput-object v1, v0, LX/A93;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    .line 1629495
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;

    move-result-object v1

    iput-object v1, v0, LX/A93;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;

    .line 1629496
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;

    move-result-object v1

    iput-object v1, v0, LX/A93;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;

    .line 1629497
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;

    move-result-object v1

    iput-object v1, v0, LX/A93;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;

    .line 1629498
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    iput-object v1, v0, LX/A93;->e:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 1629499
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->n()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/A93;->f:LX/15i;

    iput v1, v0, LX/A93;->g:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1629500
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteWebsiteAdminInfoModel$BoostedWebsitePromotionsModel;

    move-result-object v1

    iput-object v1, v0, LX/A93;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteWebsiteAdminInfoModel$BoostedWebsitePromotionsModel;

    .line 1629501
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v1

    iput-object v1, v0, LX/A93;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    .line 1629502
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->q()Z

    move-result v1

    iput-boolean v1, v0, LX/A93;->j:Z

    .line 1629503
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->r()Z

    move-result v1

    iput-boolean v1, v0, LX/A93;->k:Z

    .line 1629504
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->s()I

    move-result v1

    iput v1, v0, LX/A93;->l:I

    .line 1629505
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/A93;->m:Z

    .line 1629506
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/A93;->n:Z

    .line 1629507
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v1

    iput-object v1, v0, LX/A93;->o:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 1629508
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/A93;->p:Z

    .line 1629509
    return-object v0

    .line 1629510
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v13, 0x0

    .line 1629511
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1629512
    iget-object v1, p0, LX/A93;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1629513
    iget-object v3, p0, LX/A93;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1629514
    iget-object v5, p0, LX/A93;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$LocalAwarenessAdminInfoModel$BoostedLocalAwarenessPromotionsModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1629515
    iget-object v6, p0, LX/A93;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PageLikeAdminInfoModel$BoostedPageLikePromotionsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1629516
    iget-object v7, p0, LX/A93;->e:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1629517
    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    iget-object v9, p0, LX/A93;->f:LX/15i;

    iget v10, p0, LX/A93;->g:I

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v8, -0x7fd442ff

    invoke-static {v9, v10, v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v8

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1629518
    iget-object v9, p0, LX/A93;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteWebsiteAdminInfoModel$BoostedWebsitePromotionsModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1629519
    iget-object v10, p0, LX/A93;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1629520
    iget-object v11, p0, LX/A93;->o:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1629521
    const/16 v12, 0xf

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1629522
    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1629523
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1629524
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1629525
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1629526
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1629527
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1629528
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1629529
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1629530
    const/16 v1, 0x8

    iget-boolean v3, p0, LX/A93;->j:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1629531
    const/16 v1, 0x9

    iget-boolean v3, p0, LX/A93;->k:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1629532
    const/16 v1, 0xa

    iget v3, p0, LX/A93;->l:I

    invoke-virtual {v0, v1, v3, v13}, LX/186;->a(III)V

    .line 1629533
    const/16 v1, 0xb

    iget-boolean v3, p0, LX/A93;->m:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1629534
    const/16 v1, 0xc

    iget-boolean v3, p0, LX/A93;->n:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1629535
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1629536
    const/16 v1, 0xe

    iget-boolean v3, p0, LX/A93;->p:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1629537
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1629538
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1629539
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1629540
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1629541
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1629542
    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-direct {v1, v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;-><init>(LX/15i;)V

    .line 1629543
    return-object v1

    .line 1629544
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
