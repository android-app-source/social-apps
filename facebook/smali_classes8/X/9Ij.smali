.class public LX/9Ij;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Os;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1463544
    const-class v0, LX/9In;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9Ij;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1463545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463546
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1463547
    iput-object v0, p0, LX/9Ij;->b:LX/0Ot;

    .line 1463548
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1463549
    iput-object v0, p0, LX/9Ij;->c:LX/0Ot;

    .line 1463550
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1463551
    iput-object v0, p0, LX/9Ij;->d:LX/0Ot;

    .line 1463552
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/9Ij;->e:LX/01J;

    .line 1463553
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1463542
    iget-object v0, p0, LX/9Ij;->e:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 1463543
    return-void
.end method
