.class public final LX/9iv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/photos/base/tagging/FaceBox;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9iw;


# direct methods
.method public constructor <init>(LX/9iw;)V
    .locals 0

    .prologue
    .line 1528771
    iput-object p1, p0, LX/9iv;->a:LX/9iw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1528772
    check-cast p1, Lcom/facebook/photos/base/tagging/FaceBox;

    check-cast p2, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528773
    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p2}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    .line 1528774
    if-nez v0, :cond_0

    .line 1528775
    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    .line 1528776
    :cond_0
    return v0
.end method
