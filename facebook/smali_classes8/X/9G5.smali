.class public final LX/9G5;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/9G6;

.field public final synthetic c:LX/0Px;

.field public final synthetic d:LX/0Px;

.field public final synthetic e:LX/0Px;

.field public final synthetic f:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;LX/0Px;LX/9G6;LX/0Px;LX/0Px;LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1459314
    iput-object p1, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iput-object p2, p0, LX/9G5;->a:LX/0Px;

    iput-object p3, p0, LX/9G5;->b:LX/9G6;

    iput-object p4, p0, LX/9G5;->c:LX/0Px;

    iput-object p5, p0, LX/9G5;->d:LX/0Px;

    iput-object p6, p0, LX/9G5;->e:LX/0Px;

    iput-object p7, p0, LX/9G5;->f:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1459315
    iget-object v2, p0, LX/9G5;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1459316
    iget-object v2, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iget-object v3, p0, LX/9G5;->a:LX/0Px;

    iget-object v4, p0, LX/9G5;->b:LX/9G6;

    .line 1459317
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_0

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1459318
    iget-object v8, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    new-instance v9, LX/9GC;

    iget-object v10, v4, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v11

    iget-object v12, v4, LX/9G6;->c:LX/9HA;

    invoke-direct {v9, v10, v5, v11, v12}, LX/9GC;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;ILX/9HA;)V

    invoke-virtual {p1, v8, v9}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1459319
    const/4 v0, 0x1

    .line 1459320
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 1459321
    :cond_0
    move v0, v1

    .line 1459322
    :cond_1
    iget-object v2, p0, LX/9G5;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1459323
    iget-object v2, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iget-object v3, p0, LX/9G5;->c:LX/0Px;

    iget-object v4, p0, LX/9G5;->b:LX/9G6;

    .line 1459324
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v7, :cond_2

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1459325
    iget-object v8, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->i:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    new-instance v9, LX/9Fz;

    iget-object v10, v4, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v11

    iget-object v12, v4, LX/9G6;->c:LX/9HA;

    invoke-direct {v9, v10, v5, v11, v12}, LX/9Fz;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;ILX/9HA;)V

    invoke-virtual {p1, v8, v9}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1459326
    const/4 v0, 0x1

    .line 1459327
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 1459328
    :cond_2
    move v0, v1

    .line 1459329
    :cond_3
    iget-object v2, p0, LX/9G5;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1459330
    iget-object v2, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iget-object v3, p0, LX/9G5;->d:LX/0Px;

    iget-object v4, p0, LX/9G5;->b:LX/9G6;

    .line 1459331
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v5, 0x0

    move v6, v5

    :goto_2
    if-ge v6, v7, :cond_5

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1459332
    const-string v8, "PLACEHOLDER_LOADING_PAGE_ID"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1459333
    iget-object v8, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoLoadingCardPartDefinition;

    new-instance v9, LX/9GB;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v10

    iget-object v11, v4, LX/9G6;->c:LX/9HA;

    invoke-direct {v9, v5, v10, v11}, LX/9GB;-><init>(Lcom/facebook/graphql/model/GraphQLPage;ILX/9HA;)V

    invoke-virtual {p1, v8, v9}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1459334
    :goto_3
    const/4 v0, 0x1

    .line 1459335
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 1459336
    :cond_4
    iget-object v8, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    new-instance v9, LX/9G8;

    iget-object v10, v4, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v11

    iget-object v12, v4, LX/9G6;->c:LX/9HA;

    invoke-direct {v9, v10, v5, v11, v12}, LX/9G8;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;ILX/9HA;)V

    invoke-virtual {p1, v8, v9}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_3

    .line 1459337
    :cond_5
    move v0, v1

    .line 1459338
    :cond_6
    iget-object v2, p0, LX/9G5;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1459339
    iget-object v2, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iget-object v3, p0, LX/9G5;->e:LX/0Px;

    iget-object v4, p0, LX/9G5;->b:LX/9G6;

    .line 1459340
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v5, 0x0

    move v6, v5

    :goto_4
    if-ge v6, v7, :cond_7

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1459341
    iget-object v8, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->h:Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    new-instance v9, LX/9Fn;

    iget-object v10, v4, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v11

    iget-object v12, v4, LX/9G6;->c:LX/9HA;

    invoke-direct {v9, v10, v5, v11, v12}, LX/9Fn;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;ILX/9HA;)V

    invoke-virtual {p1, v8, v9}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1459342
    const/4 v0, 0x1

    .line 1459343
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_4

    .line 1459344
    :cond_7
    move v0, v1

    .line 1459345
    :cond_8
    iget-object v2, p0, LX/9G5;->b:LX/9G6;

    iget-object v2, v2, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/8y3;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1459346
    iget-object v2, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iget-object v3, p0, LX/9G5;->b:LX/9G6;

    .line 1459347
    iget-object v4, v3, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1459348
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v5

    .line 1459349
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 1459350
    iget-object v5, v3, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v5}, LX/8y3;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1459351
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_5
    if-ge v5, v7, :cond_9

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    .line 1459352
    iget-object v8, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->g:Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    new-instance v9, LX/9Fb;

    iget-object v10, v3, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v11

    iget-object v12, v3, LX/9G6;->c:LX/9HA;

    invoke-direct {v9, v10, v4, v11, v12}, LX/9Fb;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;ILX/9HA;)V

    invoke-virtual {p1, v8, v9}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1459353
    const/4 v0, 0x1

    .line 1459354
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 1459355
    :cond_9
    move v0, v1

    .line 1459356
    :cond_a
    iget-object v2, p0, LX/9G5;->b:LX/9G6;

    iget-object v2, v2, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 1459357
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1459358
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-eqz v3, :cond_f

    .line 1459359
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1459360
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_f

    .line 1459361
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1459362
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_f

    .line 1459363
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1459364
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_6
    if-ge v5, v7, :cond_f

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    .line 1459365
    invoke-static {v3}, LX/8y3;->a(Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 1459366
    const/4 v3, 0x1

    .line 1459367
    :goto_7
    move v2, v3

    .line 1459368
    if-eqz v2, :cond_c

    iget-object v2, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    .line 1459369
    iget-object v3, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->l:LX/0ad;

    sget-short v4, LX/5HH;->h:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v2, v3

    .line 1459370
    if-eqz v2, :cond_c

    .line 1459371
    iget-object v2, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iget-object v3, p0, LX/9G5;->b:LX/9G6;

    .line 1459372
    iget-object v4, v3, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1459373
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v5

    .line 1459374
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 1459375
    invoke-static {v4}, LX/8y3;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/0Px;

    move-result-object v6

    .line 1459376
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_8
    if-ge v5, v7, :cond_b

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    .line 1459377
    iget-object v8, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    new-instance v9, LX/9GD;

    iget-object v10, v3, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v11

    iget-object v12, v3, LX/9G6;->c:LX/9HA;

    invoke-direct {v9, v10, v4, v11, v12}, LX/9GD;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;ILX/9HA;)V

    invoke-virtual {p1, v8, v9}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1459378
    const/4 v0, 0x1

    .line 1459379
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_8

    .line 1459380
    :cond_b
    move v0, v1

    .line 1459381
    :cond_c
    iget-object v1, p0, LX/9G5;->b:LX/9G6;

    iget-boolean v1, v1, LX/9G6;->b:Z

    if-eqz v1, :cond_d

    .line 1459382
    iget-object v1, p0, LX/9G5;->g:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;

    iget-object v2, p0, LX/9G5;->b:LX/9G6;

    iget-object v3, p0, LX/9G5;->f:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1459383
    iget-object v4, v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    new-instance v5, LX/9FT;

    iget-object v6, v2, LX/9G6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentHScrollPartDefinition;->a(Z)I

    move-result v8

    iget-object v9, v2, LX/9G6;->c:LX/9HA;

    invoke-direct {v5, v6, v7, v8, v9}, LX/9FT;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ILX/9HA;)V

    invoke-virtual {p1, v4, v5}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1459384
    :cond_d
    return-void

    .line 1459385
    :cond_e
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_6

    :cond_f
    move v3, v4

    .line 1459386
    goto :goto_7
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 1459387
    return-void
.end method
