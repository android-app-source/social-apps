.class public final enum LX/94k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/94k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/94k;

.field public static final enum ADD:LX/94k;

.field public static final enum DELETE:LX/94k;

.field public static final enum NONE:LX/94k;

.field public static final enum UPDATE:LX/94k;


# instance fields
.field public final buckContactChangeType:LX/6ON;

.field public final snapshotEntryChangeType:LX/95B;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1435624
    new-instance v0, LX/94k;

    const-string v1, "ADD"

    sget-object v2, LX/6ON;->ADD:LX/6ON;

    sget-object v3, LX/95B;->ADD:LX/95B;

    invoke-direct {v0, v1, v4, v2, v3}, LX/94k;-><init>(Ljava/lang/String;ILX/6ON;LX/95B;)V

    sput-object v0, LX/94k;->ADD:LX/94k;

    .line 1435625
    new-instance v0, LX/94k;

    const-string v1, "UPDATE"

    sget-object v2, LX/6ON;->MODIFY:LX/6ON;

    sget-object v3, LX/95B;->UPDATE:LX/95B;

    invoke-direct {v0, v1, v5, v2, v3}, LX/94k;-><init>(Ljava/lang/String;ILX/6ON;LX/95B;)V

    sput-object v0, LX/94k;->UPDATE:LX/94k;

    .line 1435626
    new-instance v0, LX/94k;

    const-string v1, "DELETE"

    sget-object v2, LX/6ON;->DELETE:LX/6ON;

    sget-object v3, LX/95B;->DELETE:LX/95B;

    invoke-direct {v0, v1, v6, v2, v3}, LX/94k;-><init>(Ljava/lang/String;ILX/6ON;LX/95B;)V

    sput-object v0, LX/94k;->DELETE:LX/94k;

    .line 1435627
    new-instance v0, LX/94k;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v7, v8, v8}, LX/94k;-><init>(Ljava/lang/String;ILX/6ON;LX/95B;)V

    sput-object v0, LX/94k;->NONE:LX/94k;

    .line 1435628
    const/4 v0, 0x4

    new-array v0, v0, [LX/94k;

    sget-object v1, LX/94k;->ADD:LX/94k;

    aput-object v1, v0, v4

    sget-object v1, LX/94k;->UPDATE:LX/94k;

    aput-object v1, v0, v5

    sget-object v1, LX/94k;->DELETE:LX/94k;

    aput-object v1, v0, v6

    sget-object v1, LX/94k;->NONE:LX/94k;

    aput-object v1, v0, v7

    sput-object v0, LX/94k;->$VALUES:[LX/94k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/6ON;LX/95B;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ON;",
            "LX/95B;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1435629
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1435630
    iput-object p3, p0, LX/94k;->buckContactChangeType:LX/6ON;

    .line 1435631
    iput-object p4, p0, LX/94k;->snapshotEntryChangeType:LX/95B;

    .line 1435632
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/94k;
    .locals 1

    .prologue
    .line 1435633
    const-class v0, LX/94k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/94k;

    return-object v0
.end method

.method public static values()[LX/94k;
    .locals 1

    .prologue
    .line 1435634
    sget-object v0, LX/94k;->$VALUES:[LX/94k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/94k;

    return-object v0
.end method
