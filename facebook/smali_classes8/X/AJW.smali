.class public final LX/AJW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/AJF;

.field public final synthetic c:LX/AJX;


# direct methods
.method public constructor <init>(LX/AJX;ILX/AJF;)V
    .locals 0

    .prologue
    .line 1661212
    iput-object p1, p0, LX/AJW;->c:LX/AJX;

    iput p2, p0, LX/AJW;->a:I

    iput-object p3, p0, LX/AJW;->b:LX/AJF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1661213
    sget-object v1, LX/AJH;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661214
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1661215
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1661216
    if-nez p1, :cond_1

    .line 1661217
    :cond_0
    :goto_0
    return-void

    .line 1661218
    :cond_1
    iget-object v0, p0, LX/AJW;->c:LX/AJX;

    iget-object v0, v0, LX/AJX;->e:[Z

    iget v1, p0, LX/AJW;->a:I

    aget-boolean v0, v0, v1

    if-nez v0, :cond_0

    .line 1661219
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1661220
    check-cast v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;

    invoke-static {v0}, LX/AJU;->a(Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;)LX/0Px;

    move-result-object v0

    .line 1661221
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1661222
    iget-object v1, p0, LX/AJW;->c:LX/AJX;

    iget-object v1, v1, LX/AJX;->e:[Z

    iget v2, p0, LX/AJW;->a:I

    const/4 v3, 0x1

    aput-boolean v3, v1, v2

    .line 1661223
    iget-object v1, p0, LX/AJW;->b:LX/AJF;

    iget-object v2, p0, LX/AJW;->c:LX/AJX;

    invoke-static {v2}, LX/AJX;->c(LX/AJX;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/AJF;->a(LX/0Px;Z)V

    goto :goto_0
.end method
