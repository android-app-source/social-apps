.class public LX/AIx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:LX/2qr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/4mU;

.field public f:LX/4mU;

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;LX/4mV;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x96

    .line 1660545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1660546
    iput-object p1, p0, LX/AIx;->b:Landroid/view/View;

    .line 1660547
    iput-object p2, p0, LX/AIx;->c:Landroid/view/View;

    .line 1660548
    iget-object v0, p0, LX/AIx;->b:Landroid/view/View;

    invoke-virtual {p3, v0}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, LX/AIx;->e:LX/4mU;

    .line 1660549
    iget-object v0, p0, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p3, v0}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, LX/AIx;->f:LX/4mU;

    .line 1660550
    iget-object v0, p0, LX/AIx;->e:LX/4mU;

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 1660551
    iget-object v0, p0, LX/AIx;->f:LX/4mU;

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 1660552
    return-void
.end method


# virtual methods
.method public final a(ZI)V
    .locals 4

    .prologue
    .line 1660553
    if-eqz p1, :cond_2

    .line 1660554
    iget-object v0, p0, LX/AIx;->e:LX/4mU;

    iget p1, p0, LX/AIx;->g:I

    neg-int p1, p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, LX/4mU;->j(F)V

    .line 1660555
    iget-object v0, p0, LX/AIx;->f:LX/4mU;

    iget p1, p0, LX/AIx;->h:I

    int-to-float p1, p1

    invoke-virtual {v0, p1}, LX/4mU;->j(F)V

    .line 1660556
    iget-object v0, p0, LX/AIx;->f:LX/4mU;

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {v0, p1}, LX/4mU;->f(F)V

    .line 1660557
    iget-object v0, p0, LX/AIx;->f:LX/4mU;

    new-instance p1, LX/AIv;

    invoke-direct {p1, p0}, LX/AIv;-><init>(LX/AIx;)V

    invoke-virtual {v0, p1}, LX/4mU;->a(LX/4mR;)V

    .line 1660558
    :goto_0
    iget-object v0, p0, LX/AIx;->d:LX/2qr;

    if-eqz v0, :cond_0

    .line 1660559
    iget-object v0, p0, LX/AIx;->d:LX/2qr;

    invoke-virtual {v0}, LX/2qr;->reset()V

    .line 1660560
    :cond_0
    iget-object v0, p0, LX/AIx;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1660561
    new-instance v0, LX/2qr;

    iget-object v1, p0, LX/AIx;->a:Landroid/view/View;

    invoke-direct {v0, v1, p2}, LX/2qr;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, LX/AIx;->d:LX/2qr;

    .line 1660562
    iget-object v0, p0, LX/AIx;->d:LX/2qr;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, LX/2qr;->setDuration(J)V

    .line 1660563
    iget-object v0, p0, LX/AIx;->a:Landroid/view/View;

    iget-object v1, p0, LX/AIx;->d:LX/2qr;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1660564
    :cond_1
    return-void

    .line 1660565
    :cond_2
    const/4 v1, 0x0

    .line 1660566
    iget-object v0, p0, LX/AIx;->e:LX/4mU;

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1660567
    iget-object v0, p0, LX/AIx;->f:LX/4mU;

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1660568
    iget-object v0, p0, LX/AIx;->f:LX/4mU;

    invoke-virtual {v0, v1}, LX/4mU;->f(F)V

    .line 1660569
    iget-object v0, p0, LX/AIx;->f:LX/4mU;

    new-instance v1, LX/AIw;

    invoke-direct {v1, p0}, LX/AIw;-><init>(LX/AIx;)V

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1660570
    goto :goto_0
.end method
