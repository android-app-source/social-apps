.class public LX/AEY;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/1Nt;

.field public final b:LX/2yI;

.field public final c:Landroid/content/Context;

.field private final d:LX/2yS;

.field private final e:LX/2yT;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2yI;LX/2yS;LX/2yT;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646775
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646776
    iput-object p1, p0, LX/AEY;->c:Landroid/content/Context;

    .line 1646777
    iput-object p2, p0, LX/AEY;->b:LX/2yI;

    .line 1646778
    iput-object p3, p0, LX/AEY;->d:LX/2yS;

    .line 1646779
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1646780
    const v1, 0x7f0a0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/AEY;->f:I

    .line 1646781
    const v1, 0x7f0a015d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/AEY;->g:I

    .line 1646782
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/LeadGenActionButton$LeadGenActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/LeadGenActionButton$LeadGenActionButtonPartDefinition;-><init>(LX/AEY;)V

    iput-object v0, p0, LX/AEY;->a:LX/1Nt;

    .line 1646783
    iput-object p4, p0, LX/AEY;->e:LX/2yT;

    .line 1646784
    return-void
.end method

.method public static a(LX/0QB;)LX/AEY;
    .locals 7

    .prologue
    .line 1646764
    const-class v1, LX/AEY;

    monitor-enter v1

    .line 1646765
    :try_start_0
    sget-object v0, LX/AEY;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646766
    sput-object v2, LX/AEY;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646767
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646768
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646769
    new-instance p0, LX/AEY;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const-class v4, LX/2yI;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2yI;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v5

    check-cast v5, LX/2yS;

    const-class v6, LX/2yT;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2yT;

    invoke-direct {p0, v3, v4, v5, v6}, LX/AEY;-><init>(Landroid/content/Context;LX/2yI;LX/2yS;LX/2yT;)V

    .line 1646770
    move-object v0, p0

    .line 1646771
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646772
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646773
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/AEY;Z)I
    .locals 1

    .prologue
    .line 1646763
    if-eqz p1, :cond_0

    iget v0, p0, LX/AEY;->f:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/AEY;->g:I

    goto :goto_0
.end method

.method public static b(Z)I
    .locals 1

    .prologue
    .line 1646785
    if-eqz p0, :cond_0

    const v0, 0x7f020248

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .prologue
    .line 1646761
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646762
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, 0x46a1c4a4

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646760
    iget-object v0, p0, LX/AEY;->a:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 5

    .prologue
    .line 1646745
    check-cast p2, LX/1Pq;

    .line 1646746
    invoke-static {p3}, LX/AEY;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1646747
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646748
    const/4 v0, 0x0

    .line 1646749
    :goto_0
    return-object v0

    .line 1646750
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v0

    .line 1646751
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1646752
    :goto_1
    iget-object v2, p0, LX/AEY;->e:LX/2yT;

    const/4 v3, 0x2

    iget-object v4, p0, LX/AEY;->d:LX/2yS;

    invoke-virtual {v2, p4, v3, v4}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v1

    .line 1646753
    iput-object v1, v2, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 1646754
    move-object v1, v2

    .line 1646755
    invoke-static {p0, v0}, LX/AEY;->a$redex0(LX/AEY;Z)I

    move-result v2

    invoke-virtual {v1, v2}, LX/AE0;->c(I)LX/AE0;

    move-result-object v1

    invoke-static {v0}, LX/AEY;->b(Z)I

    move-result v0

    invoke-virtual {v1, v0}, LX/AE0;->a(I)LX/AE0;

    move-result-object v0

    iget-object v1, p0, LX/AEY;->b:LX/2yI;

    iget-object v2, p0, LX/AEY;->c:Landroid/content/Context;

    invoke-virtual {v1, p3, v2, p2}, LX/2yI;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;)LX/B6B;

    move-result-object v1

    .line 1646756
    iput-object v1, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1646757
    move-object v0, v0

    .line 1646758
    goto :goto_0

    .line 1646759
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
