.class public final LX/A0T;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1605501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1605458
    if-nez p1, :cond_0

    .line 1605459
    :goto_0
    return v0

    .line 1605460
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    const/4 v2, 0x0

    .line 1605461
    if-nez v1, :cond_1

    .line 1605462
    :goto_1
    move v1, v2

    .line 1605463
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1605464
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605465
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1605466
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1605467
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1605468
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1605469
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1605470
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 1605471
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1605472
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1605473
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605474
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605475
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1605476
    const/4 v10, 0x7

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1605477
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1605478
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1605479
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1605480
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1605481
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1605482
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 1605483
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 1605484
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605485
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToAction;)I
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 1605502
    if-nez p1, :cond_0

    .line 1605503
    :goto_0
    return v0

    .line 1605504
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605505
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1605506
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1605507
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1605508
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1605509
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v6

    const/4 v12, 0x1

    const/4 v9, 0x0

    .line 1605510
    if-nez v6, :cond_1

    .line 1605511
    :goto_1
    move v6, v9

    .line 1605512
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605513
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605514
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u()Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    move-result-object v9

    invoke-static {p0, v9}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoneNumber;)I

    move-result v9

    .line 1605515
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1605516
    const/16 v11, 0xc

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1605517
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605518
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1605519
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->l()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1605520
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1605521
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1605522
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1605523
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1605524
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1605525
    const/16 v0, 0x8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1605526
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1605527
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1605528
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 1605529
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1605530
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 1605531
    :cond_1
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;->a()LX/0Px;

    move-result-object v10

    .line 1605532
    if-eqz v10, :cond_3

    .line 1605533
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    new-array v11, v7, [I

    move v8, v9

    .line 1605534
    :goto_2
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    if-ge v8, v7, :cond_2

    .line 1605535
    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    invoke-static {p0, v7}, LX/A0T;->c(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I

    move-result v7

    aput v7, v11, v8

    .line 1605536
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_2

    .line 1605537
    :cond_2
    invoke-virtual {p0, v11, v12}, LX/186;->a([IZ)I

    move-result v7

    .line 1605538
    :goto_3
    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1605539
    invoke-virtual {p0, v9, v7}, LX/186;->b(II)V

    .line 1605540
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1605541
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_3
    move v7, v9

    goto :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1605542
    if-nez p1, :cond_0

    .line 1605543
    :goto_0
    return v2

    .line 1605544
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1605545
    if-eqz v3, :cond_2

    .line 1605546
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1605547
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605548
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    const/4 v6, 0x0

    .line 1605549
    if-nez v0, :cond_3

    .line 1605550
    :goto_2
    move v0, v6

    .line 1605551
    aput v0, v4, v1

    .line 1605552
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605553
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1605554
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1605555
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605556
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605557
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1605558
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1605559
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605560
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1605561
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1605562
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 1605563
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1605564
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1605565
    if-nez p1, :cond_0

    .line 1605566
    :goto_0
    return v0

    .line 1605567
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605568
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1605569
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1605570
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605571
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1605572
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1605573
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoneNumber;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1605574
    if-nez p1, :cond_0

    .line 1605575
    :goto_0
    return v0

    .line 1605576
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605577
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1605578
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1605579
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1605580
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1605581
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605582
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1605583
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1605584
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1605585
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1605586
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1605587
    if-nez p1, :cond_0

    move v0, v6

    .line 1605588
    :goto_0
    return v0

    .line 1605589
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1605590
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605591
    const/4 v2, 0x6

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1605592
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;->a()I

    move-result v2

    invoke-virtual {p0, v6, v2, v6}, LX/186;->a(III)V

    .line 1605593
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605594
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;->k()Z

    move-result v2

    invoke-virtual {p0, v0, v2}, LX/186;->a(IZ)V

    .line 1605595
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605596
    const/4 v1, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;->n()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1605597
    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;->m()I

    move-result v1

    invoke-virtual {p0, v0, v1, v6}, LX/186;->a(III)V

    .line 1605598
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1605599
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLSearchElectionDate;)I
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1605600
    if-nez p1, :cond_0

    .line 1605601
    :goto_0
    return v2

    .line 1605602
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionDate;->a()LX/0Px;

    move-result-object v3

    .line 1605603
    if-eqz v3, :cond_2

    .line 1605604
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1605605
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605606
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;

    const/4 v7, 0x1

    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    .line 1605607
    if-nez v0, :cond_3

    .line 1605608
    :goto_2
    move v0, v9

    .line 1605609
    aput v0, v4, v1

    .line 1605610
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605611
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1605612
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1605613
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605614
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605615
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1605616
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->a()LX/0Px;

    move-result-object v12

    .line 1605617
    if-eqz v12, :cond_5

    .line 1605618
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v6

    new-array v13, v6, [I

    move v8, v9

    .line 1605619
    :goto_4
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v6

    if-ge v8, v6, :cond_4

    .line 1605620
    invoke-virtual {v12, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;

    invoke-static {p0, v6}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSearchElectionCandidate;)I

    move-result v6

    aput v6, v13, v8

    .line 1605621
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_4

    .line 1605622
    :cond_4
    invoke-virtual {p0, v13, v7}, LX/186;->a([IZ)I

    move-result v6

    .line 1605623
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1605624
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1605625
    const/4 v8, 0x5

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1605626
    invoke-virtual {p0, v9, v6}, LX/186;->b(II)V

    .line 1605627
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->j()D

    move-result-wide v8

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 1605628
    const/4 v6, 0x2

    invoke-virtual {p0, v6, v12}, LX/186;->b(II)V

    .line 1605629
    const/4 v6, 0x3

    invoke-virtual {p0, v6, v13}, LX/186;->b(II)V

    .line 1605630
    const/4 v7, 0x4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionRace;->m()D

    move-result-wide v8

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 1605631
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1605632
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_2

    :cond_5
    move v6, v9

    goto :goto_5
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;)I
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1605633
    if-nez p1, :cond_0

    .line 1605634
    :goto_0
    return v2

    .line 1605635
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;->a()LX/0Px;

    move-result-object v3

    .line 1605636
    if-eqz v3, :cond_2

    .line 1605637
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1605638
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605639
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;

    const/4 v6, 0x0

    .line 1605640
    if-nez v0, :cond_3

    .line 1605641
    :goto_2
    move v0, v6

    .line 1605642
    aput v0, v4, v1

    .line 1605643
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605644
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1605645
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605646
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1605647
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605648
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 1605649
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605650
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1605651
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605652
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605653
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1605654
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1605655
    const/4 v11, 0x7

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1605656
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->l()I

    move-result v11

    invoke-virtual {p0, v6, v11, v6}, LX/186;->a(III)V

    .line 1605657
    const/4 v11, 0x1

    invoke-virtual {p0, v11, v7}, LX/186;->b(II)V

    .line 1605658
    const/4 v7, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->m()Z

    move-result v11

    invoke-virtual {p0, v7, v11}, LX/186;->a(IZ)V

    .line 1605659
    const/4 v7, 0x3

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1605660
    const/4 v7, 0x4

    invoke-virtual {p0, v7, v9}, LX/186;->b(II)V

    .line 1605661
    const/4 v7, 0x5

    invoke-virtual {p0, v7, v10}, LX/186;->b(II)V

    .line 1605662
    const/4 v7, 0x6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionCandidateInfo;->o()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 1605663
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1605664
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1605486
    if-nez p1, :cond_0

    .line 1605487
    :goto_0
    return v2

    .line 1605488
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 1605489
    if-eqz v3, :cond_2

    .line 1605490
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1605491
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605492
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {p0, v0}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v0

    aput v0, v4, v1

    .line 1605493
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605494
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1605495
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1605496
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1605497
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605498
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 1605499
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605500
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public static a(LX/186;Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;)I
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1605069
    if-nez p1, :cond_0

    .line 1605070
    :goto_0
    return v2

    .line 1605071
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;->a()LX/0Px;

    move-result-object v3

    .line 1605072
    if-eqz v3, :cond_2

    .line 1605073
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1605074
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605075
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    const/4 v6, 0x0

    .line 1605076
    if-nez v0, :cond_3

    .line 1605077
    :goto_2
    move v0, v6

    .line 1605078
    aput v0, v4, v1

    .line 1605079
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605080
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1605081
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1605082
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605083
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605084
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1605085
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    move-result-object v7

    const/4 v8, 0x0

    .line 1605086
    if-nez v7, :cond_4

    .line 1605087
    :goto_4
    move v7, v8

    .line 1605088
    const/4 v8, 0x2

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1605089
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1605090
    const/4 v6, 0x1

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->b()Z

    move-result v7

    invoke-virtual {p0, v6, v7}, LX/186;->a(IZ)V

    .line 1605091
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1605092
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 1605093
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1605094
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1605095
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->c()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v11

    const/4 v12, 0x0

    .line 1605096
    if-nez v11, :cond_5

    .line 1605097
    :goto_5
    move v11, v12

    .line 1605098
    const/4 v12, 0x3

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1605099
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1605100
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v10}, LX/186;->b(II)V

    .line 1605101
    const/4 v8, 0x2

    invoke-virtual {p0, v8, v11}, LX/186;->b(II)V

    .line 1605102
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1605103
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4

    .line 1605104
    :cond_5
    invoke-virtual {v11}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 1605105
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1605106
    invoke-virtual {p0, v12, p1}, LX/186;->b(II)V

    .line 1605107
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 1605108
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_5
.end method

.method public static a(LX/186;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)I
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1605109
    if-nez p1, :cond_0

    .line 1605110
    :goto_0
    return v2

    .line 1605111
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v3

    .line 1605112
    if-eqz v3, :cond_2

    .line 1605113
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1605114
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605115
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    const/4 v6, 0x0

    .line 1605116
    if-nez v0, :cond_3

    .line 1605117
    :goto_2
    move v0, v6

    .line 1605118
    aput v0, v4, v1

    .line 1605119
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605120
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1605121
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1605122
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605123
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605124
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1605125
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v7

    const/4 v8, 0x0

    .line 1605126
    if-nez v7, :cond_4

    .line 1605127
    :goto_4
    move v7, v8

    .line 1605128
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1605129
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1605130
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1605131
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 1605132
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1605133
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 1605134
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1605135
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v0

    invoke-virtual {p0, v8, v0}, LX/186;->a(IZ)V

    .line 1605136
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1605137
    const/4 v8, 0x2

    invoke-virtual {p0, v8, p1}, LX/186;->b(II)V

    .line 1605138
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1605139
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4
.end method

.method public static a(LX/186;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 1605140
    if-nez p1, :cond_0

    .line 1605141
    :goto_0
    return v0

    .line 1605142
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;->a()LX/5uu;

    move-result-object v1

    const/4 v2, 0x0

    .line 1605143
    if-nez v1, :cond_1

    .line 1605144
    :goto_1
    move v1, v2

    .line 1605145
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1605146
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605147
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1605148
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1605149
    :cond_1
    invoke-interface {v1}, LX/5uu;->b()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v3

    const/4 v4, 0x0

    .line 1605150
    if-nez v3, :cond_2

    .line 1605151
    :goto_2
    move v3, v4

    .line 1605152
    invoke-interface {v1}, LX/5uu;->c()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v4

    const/4 v5, 0x0

    .line 1605153
    if-nez v4, :cond_3

    .line 1605154
    :goto_3
    move v4, v5

    .line 1605155
    invoke-interface {v1}, LX/5uu;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1605156
    invoke-interface {v1}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v6

    invoke-static {p0, v6}, LX/A0T;->a(LX/186;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)I

    move-result v6

    .line 1605157
    invoke-interface {v1}, LX/5uu;->bk_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605158
    invoke-interface {v1}, LX/5uu;->bl_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605159
    invoke-interface {v1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1605160
    invoke-interface {v1}, LX/5uu;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1605161
    invoke-interface {v1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1605162
    const/16 p1, 0x9

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1605163
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1605164
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1605165
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1605166
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1605167
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1605168
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 1605169
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 1605170
    const/4 v2, 0x7

    invoke-virtual {p0, v2, v10}, LX/186;->b(II)V

    .line 1605171
    const/16 v2, 0x8

    invoke-virtual {p0, v2, v11}, LX/186;->b(II)V

    .line 1605172
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605173
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1605174
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1605175
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1605176
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 1605177
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1605178
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1605179
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1605180
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605181
    const/4 v8, 0x3

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1605182
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 1605183
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v7}, LX/186;->b(II)V

    .line 1605184
    const/4 v5, 0x2

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;->c()Z

    move-result v6

    invoke-virtual {p0, v5, v6}, LX/186;->a(IZ)V

    .line 1605185
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1605186
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)LX/8d0;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1605187
    if-nez p0, :cond_1

    .line 1605188
    :cond_0
    :goto_0
    return-object v2

    .line 1605189
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1605190
    const/4 v1, 0x0

    .line 1605191
    if-nez p0, :cond_4

    .line 1605192
    :cond_2
    :goto_1
    move v1, v1

    .line 1605193
    if-eqz v1, :cond_0

    .line 1605194
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1605195
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1605196
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1605197
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1605198
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_3

    .line 1605199
    const-string v1, "SearchModelConversionHelper.getSearchResultsPage"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1605200
    :cond_3
    new-instance v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;

    invoke-direct {v2, v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1605201
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 1605202
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x25d6af

    if-ne v3, v4, :cond_2

    .line 1605203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1605204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    const/4 v4, 0x0

    .line 1605205
    if-nez v3, :cond_5

    .line 1605206
    :goto_2
    move v3, v4

    .line 1605207
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1605208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1605209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v6

    invoke-static {v0, v6}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToAction;)I

    move-result v6

    .line 1605210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gc()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v7

    const/4 v8, 0x0

    .line 1605211
    if-nez v7, :cond_8

    .line 1605212
    :goto_3
    move v7, v8

    .line 1605213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    const/4 v9, 0x0

    .line 1605214
    if-nez v8, :cond_9

    .line 1605215
    :goto_4
    move v8, v9

    .line 1605216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ku()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1605217
    const/16 v10, 0xe

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1605218
    const/4 v10, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aW()Z

    move-result v11

    invoke-virtual {v0, v10, v11}, LX/186;->a(IZ)V

    .line 1605219
    const/4 v10, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aY()Z

    move-result v11

    invoke-virtual {v0, v10, v11}, LX/186;->a(IZ)V

    .line 1605220
    const/4 v10, 0x3

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1605221
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1605222
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1605223
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1605224
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1605225
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1605226
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1605227
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1605228
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1605229
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1605230
    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lo()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1605231
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1605232
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1605233
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v5

    const/4 v6, 0x0

    .line 1605234
    if-nez v5, :cond_6

    .line 1605235
    :goto_5
    move v5, v6

    .line 1605236
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1605237
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1605238
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 1605239
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1605240
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    const/4 v8, 0x0

    .line 1605241
    if-nez v7, :cond_7

    .line 1605242
    :goto_6
    move v7, v8

    .line 1605243
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1605244
    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 1605245
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    .line 1605246
    invoke-virtual {v0, v6}, LX/186;->d(I)V

    goto :goto_5

    .line 1605247
    :cond_7
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1605248
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1605249
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v5

    invoke-virtual {v0, v8, v5, v8}, LX/186;->a(III)V

    .line 1605250
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v3}, LX/186;->b(II)V

    .line 1605251
    const/4 v3, 0x2

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v5

    invoke-virtual {v0, v3, v5, v8}, LX/186;->a(III)V

    .line 1605252
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 1605253
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto :goto_6

    .line 1605254
    :cond_8
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1605255
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;->a()I

    move-result v9

    invoke-virtual {v0, v8, v9, v8}, LX/186;->a(III)V

    .line 1605256
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 1605257
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 1605258
    :cond_9
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->l()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1605259
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1605260
    const/4 v12, 0x4

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1605261
    invoke-virtual {v0, v9, v10}, LX/186;->b(II)V

    .line 1605262
    const/4 v10, 0x1

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v12

    invoke-virtual {v0, v10, v12, v9}, LX/186;->a(III)V

    .line 1605263
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v11}, LX/186;->b(II)V

    .line 1605264
    const/4 v10, 0x3

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v11

    invoke-virtual {v0, v10, v11, v9}, LX/186;->a(III)V

    .line 1605265
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 1605266
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method public static a(Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;
    .locals 3

    .prologue
    .line 1605267
    if-nez p0, :cond_0

    .line 1605268
    const/4 v0, 0x0

    .line 1605269
    :goto_0
    return-object v0

    .line 1605270
    :cond_0
    new-instance v0, LX/4Wc;

    invoke-direct {v0}, LX/4Wc;-><init>()V

    .line 1605271
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->a()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v1

    .line 1605272
    if-nez v1, :cond_1

    .line 1605273
    const/4 v2, 0x0

    .line 1605274
    :goto_1
    move-object v1, v2

    .line 1605275
    iput-object v1, v0, LX/4Wc;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1605276
    new-instance v1, Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;-><init>(LX/4Wc;)V

    .line 1605277
    move-object v0, v1

    .line 1605278
    goto :goto_0

    .line 1605279
    :cond_1
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 1605280
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 1605281
    iput-object p0, v2, LX/173;->f:Ljava/lang/String;

    .line 1605282
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto :goto_1
.end method

.method public static a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1605283
    if-nez p0, :cond_0

    .line 1605284
    const/4 v0, 0x0

    .line 1605285
    :goto_0
    return-object v0

    .line 1605286
    :cond_0
    new-instance v3, LX/4Wh;

    invoke-direct {v3}, LX/4Wh;-><init>()V

    .line 1605287
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1605288
    iput-object v0, v3, LX/4Wh;->b:Ljava/lang/String;

    .line 1605289
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1605290
    iput-object v0, v3, LX/4Wh;->c:Ljava/lang/String;

    .line 1605291
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1605292
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1605293
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605294
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    invoke-static {v0}, LX/A0T;->a(Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1605295
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605296
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1605297
    iput-object v0, v3, LX/4Wh;->f:LX/0Px;

    .line 1605298
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->d()LX/0Px;

    move-result-object v0

    .line 1605299
    iput-object v0, v3, LX/4Wh;->i:LX/0Px;

    .line 1605300
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->e()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1605301
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1605302
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1605303
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;

    .line 1605304
    if-nez v0, :cond_5

    .line 1605305
    const/4 v4, 0x0

    .line 1605306
    :goto_3
    move-object v0, v4

    .line 1605307
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1605308
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1605309
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1605310
    iput-object v0, v3, LX/4Wh;->j:LX/0Px;

    .line 1605311
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->fK_()Z

    move-result v0

    .line 1605312
    iput-boolean v0, v3, LX/4Wh;->k:Z

    .line 1605313
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->fL_()Ljava/lang/String;

    move-result-object v0

    .line 1605314
    iput-object v0, v3, LX/4Wh;->l:Ljava/lang/String;

    .line 1605315
    invoke-virtual {v3}, LX/4Wh;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    goto/16 :goto_0

    .line 1605316
    :cond_5
    new-instance v4, LX/4Wj;

    invoke-direct {v4}, LX/4Wj;-><init>()V

    .line 1605317
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v5

    .line 1605318
    if-nez v5, :cond_6

    .line 1605319
    const/4 v6, 0x0

    .line 1605320
    :goto_4
    move-object v5, v6

    .line 1605321
    iput-object v5, v4, LX/4Wj;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1605322
    new-instance v5, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-direct {v5, v4}, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;-><init>(LX/4Wj;)V

    .line 1605323
    move-object v4, v5

    .line 1605324
    goto :goto_3

    .line 1605325
    :cond_6
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 1605326
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1605327
    iput-object v0, v6, LX/173;->f:Ljava/lang/String;

    .line 1605328
    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto :goto_4
.end method

.method public static a(LX/8dH;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 1605329
    if-nez p0, :cond_1

    .line 1605330
    :cond_0
    :goto_0
    return-object v2

    .line 1605331
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1605332
    const/4 v4, 0x0

    .line 1605333
    if-nez p0, :cond_3

    .line 1605334
    :goto_1
    move v1, v4

    .line 1605335
    if-eqz v1, :cond_0

    .line 1605336
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1605337
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1605338
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1605339
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1605340
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1605341
    const-string v1, "SearchModelConversionHelper.getSearchResultsEdgeNodeSeeMoreQuery"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1605342
    :cond_2
    new-instance v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    invoke-direct {v2, v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1605343
    :cond_3
    invoke-interface {p0}, LX/8dH;->a()LX/0Px;

    move-result-object v5

    .line 1605344
    if-eqz v5, :cond_5

    .line 1605345
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 1605346
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 1605347
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    const/4 v12, 0x1

    const/4 v9, 0x0

    .line 1605348
    if-nez v1, :cond_6

    .line 1605349
    :goto_3
    move v1, v9

    .line 1605350
    aput v1, v6, v3

    .line 1605351
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1605352
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {v0, v6, v1}, LX/186;->a([IZ)I

    move-result v1

    .line 1605353
    :goto_4
    invoke-interface {p0}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1605354
    invoke-interface {p0}, LX/8dH;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1605355
    invoke-interface {p0}, LX/8dH;->j()LX/8dG;

    move-result-object v6

    const/4 v7, 0x0

    .line 1605356
    if-nez v6, :cond_9

    .line 1605357
    :goto_5
    move v6, v7

    .line 1605358
    invoke-interface {p0}, LX/8dH;->fn_()LX/0Px;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->c(Ljava/util/List;)I

    move-result v7

    .line 1605359
    invoke-interface {p0}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v8

    invoke-static {v0, v8}, LX/A0T;->a(LX/186;Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;)I

    move-result v8

    .line 1605360
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1605361
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1605362
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1605363
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1605364
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1605365
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1605366
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1605367
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 1605368
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_5
    move v1, v4

    goto :goto_4

    .line 1605369
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v10

    .line 1605370
    if-eqz v10, :cond_8

    .line 1605371
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    new-array v11, v7, [I

    move v8, v9

    .line 1605372
    :goto_6
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    if-ge v8, v7, :cond_7

    .line 1605373
    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;

    invoke-static {v0, v7}, LX/A0T;->a(LX/186;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;)I

    move-result v7

    aput v7, v11, v8

    .line 1605374
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_6

    .line 1605375
    :cond_7
    invoke-virtual {v0, v11, v12}, LX/186;->a([IZ)I

    move-result v7

    .line 1605376
    :goto_7
    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1605377
    invoke-virtual {v0, v9, v7}, LX/186;->b(II)V

    .line 1605378
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 1605379
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto/16 :goto_3

    :cond_8
    move v7, v9

    goto :goto_7

    .line 1605380
    :cond_9
    invoke-interface {v6}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605381
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1605382
    invoke-virtual {v0, v7, v8}, LX/186;->b(II)V

    .line 1605383
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    .line 1605384
    invoke-virtual {v0, v7}, LX/186;->d(I)V

    goto/16 :goto_5
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1605385
    if-nez p1, :cond_0

    .line 1605386
    :goto_0
    return v2

    .line 1605387
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p0, v0}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v3

    .line 1605388
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1605389
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1605390
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1605391
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605392
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->o()LX/0Px;

    move-result-object v8

    .line 1605393
    if-eqz v8, :cond_2

    .line 1605394
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    new-array v9, v0, [I

    move v1, v2

    .line 1605395
    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605396
    invoke-virtual {v8, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;

    invoke-static {p0, v0}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;)I

    move-result v0

    aput v0, v9, v1

    .line 1605397
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605398
    :cond_1
    invoke-virtual {p0, v9, v11}, LX/186;->a([IZ)I

    move-result v0

    .line 1605399
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v1

    invoke-static {p0, v1}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I

    move-result v1

    .line 1605400
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605401
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1605402
    const/16 v10, 0xa

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1605403
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1605404
    invoke-virtual {p0, v11, v4}, LX/186;->b(II)V

    .line 1605405
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1605406
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1605407
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1605408
    const/4 v2, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1605409
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605410
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605411
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1605412
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1605413
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605414
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1605445
    if-nez p1, :cond_0

    .line 1605446
    :goto_0
    return v2

    .line 1605447
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1605448
    if-eqz v3, :cond_2

    .line 1605449
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1605450
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605451
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    invoke-static {p0, v0}, LX/A0T;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I

    move-result v0

    aput v0, v4, v1

    .line 1605452
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605453
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1605454
    :goto_2
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1605455
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605456
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605457
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public static c(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1605415
    if-nez p1, :cond_0

    .line 1605416
    :goto_0
    return v2

    .line 1605417
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p0, v0}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v3

    .line 1605418
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1605419
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1605420
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1605421
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1605422
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->o()LX/0Px;

    move-result-object v8

    .line 1605423
    if-eqz v8, :cond_2

    .line 1605424
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    new-array v9, v0, [I

    move v1, v2

    .line 1605425
    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1605426
    invoke-virtual {v8, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;

    invoke-static {p0, v0}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;)I

    move-result v0

    aput v0, v9, v1

    .line 1605427
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1605428
    :cond_1
    invoke-virtual {p0, v9, v11}, LX/186;->a([IZ)I

    move-result v0

    .line 1605429
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v1

    invoke-static {p0, v1}, LX/A0T;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I

    move-result v1

    .line 1605430
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1605431
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1605432
    const/16 v10, 0xa

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1605433
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1605434
    invoke-virtual {p0, v11, v4}, LX/186;->b(II)V

    .line 1605435
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1605436
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1605437
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1605438
    const/4 v2, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1605439
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1605440
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1605441
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1605442
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1605443
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1605444
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method
