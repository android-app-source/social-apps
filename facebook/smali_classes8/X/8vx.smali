.class public final enum LX/8vx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8vx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8vx;

.field public static final enum AddNoise:LX/8vx;

.field public static final enum Additive:LX/8vx;

.field public static final enum AlphaTest:LX/8vx;

.field public static final enum Blend_Mask:LX/8vx;

.field public static final enum Blend_Oldify:LX/8vx;

.field public static final enum Blend_Paint:LX/8vx;

.field public static final enum Blend_SoftLight:LX/8vx;

.field public static final enum Blinn:LX/8vx;

.field public static final enum Color:LX/8vx;

.field public static final enum Desaturate:LX/8vx;

.field public static final enum Grayscale:LX/8vx;

.field public static final enum Multiply:LX/8vx;

.field public static final enum NightVision:LX/8vx;

.field public static final enum Normal:LX/8vx;


# instance fields
.field private final text:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1421538
    new-instance v0, LX/8vx;

    const-string v1, "Normal"

    const-string v2, "normal"

    invoke-direct {v0, v1, v4, v2}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Normal:LX/8vx;

    .line 1421539
    new-instance v0, LX/8vx;

    const-string v1, "Color"

    const-string v2, "color"

    invoke-direct {v0, v1, v5, v2}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Color:LX/8vx;

    .line 1421540
    new-instance v0, LX/8vx;

    const-string v1, "Grayscale"

    const-string v2, "grayscale"

    invoke-direct {v0, v1, v6, v2}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Grayscale:LX/8vx;

    .line 1421541
    new-instance v0, LX/8vx;

    const-string v1, "Desaturate"

    const-string v2, "desaturate"

    invoke-direct {v0, v1, v7, v2}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Desaturate:LX/8vx;

    .line 1421542
    new-instance v0, LX/8vx;

    const-string v1, "AddNoise"

    const-string v2, "add_noise"

    invoke-direct {v0, v1, v8, v2}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->AddNoise:LX/8vx;

    .line 1421543
    new-instance v0, LX/8vx;

    const-string v1, "AlphaTest"

    const/4 v2, 0x5

    const-string v3, "alpha_test"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->AlphaTest:LX/8vx;

    .line 1421544
    new-instance v0, LX/8vx;

    const-string v1, "Additive"

    const/4 v2, 0x6

    const-string v3, "additive"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Additive:LX/8vx;

    .line 1421545
    new-instance v0, LX/8vx;

    const-string v1, "Multiply"

    const/4 v2, 0x7

    const-string v3, "multiply"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Multiply:LX/8vx;

    .line 1421546
    new-instance v0, LX/8vx;

    const-string v1, "Blinn"

    const/16 v2, 0x8

    const-string v3, "blinn"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Blinn:LX/8vx;

    .line 1421547
    new-instance v0, LX/8vx;

    const-string v1, "Blend_Mask"

    const/16 v2, 0x9

    const-string v3, "mask"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Blend_Mask:LX/8vx;

    .line 1421548
    new-instance v0, LX/8vx;

    const-string v1, "Blend_SoftLight"

    const/16 v2, 0xa

    const-string v3, "softlight"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Blend_SoftLight:LX/8vx;

    .line 1421549
    new-instance v0, LX/8vx;

    const-string v1, "Blend_Paint"

    const/16 v2, 0xb

    const-string v3, "paint"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Blend_Paint:LX/8vx;

    .line 1421550
    new-instance v0, LX/8vx;

    const-string v1, "Blend_Oldify"

    const/16 v2, 0xc

    const-string v3, "oldify"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->Blend_Oldify:LX/8vx;

    .line 1421551
    new-instance v0, LX/8vx;

    const-string v1, "NightVision"

    const/16 v2, 0xd

    const-string v3, "nightvision"

    invoke-direct {v0, v1, v2, v3}, LX/8vx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8vx;->NightVision:LX/8vx;

    .line 1421552
    const/16 v0, 0xe

    new-array v0, v0, [LX/8vx;

    sget-object v1, LX/8vx;->Normal:LX/8vx;

    aput-object v1, v0, v4

    sget-object v1, LX/8vx;->Color:LX/8vx;

    aput-object v1, v0, v5

    sget-object v1, LX/8vx;->Grayscale:LX/8vx;

    aput-object v1, v0, v6

    sget-object v1, LX/8vx;->Desaturate:LX/8vx;

    aput-object v1, v0, v7

    sget-object v1, LX/8vx;->AddNoise:LX/8vx;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8vx;->AlphaTest:LX/8vx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8vx;->Additive:LX/8vx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8vx;->Multiply:LX/8vx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8vx;->Blinn:LX/8vx;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8vx;->Blend_Mask:LX/8vx;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8vx;->Blend_SoftLight:LX/8vx;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8vx;->Blend_Paint:LX/8vx;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8vx;->Blend_Oldify:LX/8vx;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8vx;->NightVision:LX/8vx;

    aput-object v2, v0, v1

    sput-object v0, LX/8vx;->$VALUES:[LX/8vx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1421553
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1421554
    iput-object p3, p0, LX/8vx;->text:Ljava/lang/String;

    .line 1421555
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8vx;
    .locals 1

    .prologue
    .line 1421556
    const-class v0, LX/8vx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8vx;

    return-object v0
.end method

.method public static values()[LX/8vx;
    .locals 1

    .prologue
    .line 1421557
    sget-object v0, LX/8vx;->$VALUES:[LX/8vx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8vx;

    return-object v0
.end method


# virtual methods
.method public final getText()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonValue;
    .end annotation

    .prologue
    .line 1421558
    iget-object v0, p0, LX/8vx;->text:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1421559
    iget-object v0, p0, LX/8vx;->text:Ljava/lang/String;

    return-object v0
.end method
