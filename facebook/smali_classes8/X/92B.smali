.class public LX/92B;
.super LX/92A;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/92B;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1432048
    const-string v0, "minutiae_feelings_selector_time_to_fetch_end"

    const-string v1, "minutiae_feelings_selector_time_to_fetch_end_cached"

    const-string v2, "minutiae_feelings_selector_time_to_results_shown"

    const-string v3, "minutiae_feelings_selector_time_to_results_shown_cached"

    const-string v4, "minutiae_feelings_selector_fetch_time"

    const-string v5, "minutiae_feelings_selector_fetch_time_cached"

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92B;->a:Ljava/util/Map;

    .line 1432049
    const v0, 0xc50003

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0xc50008

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0xc50004

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0xc50009

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0xc50005

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v5, 0xc5000a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92B;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432050
    sget-object v0, LX/92B;->a:Ljava/util/Map;

    sget-object v1, LX/92B;->b:Ljava/util/Map;

    invoke-direct {p0, p1, v0, v1}, LX/92A;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Ljava/util/Map;Ljava/util/Map;)V

    .line 1432051
    return-void
.end method

.method public static a(LX/0QB;)LX/92B;
    .locals 4

    .prologue
    .line 1432052
    sget-object v0, LX/92B;->c:LX/92B;

    if-nez v0, :cond_1

    .line 1432053
    const-class v1, LX/92B;

    monitor-enter v1

    .line 1432054
    :try_start_0
    sget-object v0, LX/92B;->c:LX/92B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1432055
    if-eqz v2, :cond_0

    .line 1432056
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1432057
    new-instance p0, LX/92B;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/92B;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 1432058
    move-object v0, p0

    .line 1432059
    sput-object v0, LX/92B;->c:LX/92B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432060
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1432061
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432062
    :cond_1
    sget-object v0, LX/92B;->c:LX/92B;

    return-object v0

    .line 1432063
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1432064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
