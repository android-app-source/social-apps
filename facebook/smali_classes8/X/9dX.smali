.class public LX/9dX;
.super LX/9dK;
.source ""


# instance fields
.field private c:Landroid/widget/ImageView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Gc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1518234
    invoke-direct {p0, p1, p2}, LX/9dK;-><init>(Landroid/content/Context;LX/8Gc;)V

    .line 1518235
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 0

    .prologue
    .line 1518233
    return p1
.end method

.method public final a()Landroid/widget/ImageView;
    .locals 3

    .prologue
    .line 1518228
    iget-object v0, p0, LX/9dX;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1518229
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9dX;->c:Landroid/widget/ImageView;

    .line 1518230
    iget-object v0, p0, LX/9dX;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020402

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1518231
    iget-object v0, p0, LX/9dX;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1518232
    :cond_0
    iget-object v0, p0, LX/9dX;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final b()Landroid/widget/TextView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1518227
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 1518221
    const/4 v0, 0x0

    return v0
.end method

.method public final d()F
    .locals 2

    .prologue
    .line 1518226
    iget-object v0, p0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    return v0
.end method

.method public final e()LX/9dT;
    .locals 1

    .prologue
    .line 1518225
    sget-object v0, LX/9dT;->CLOCKWISE:LX/9dT;

    return-object v0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1518224
    const/4 v0, 0x0

    return v0
.end method

.method public final g()F
    .locals 2

    .prologue
    .line 1518223
    iget-object v0, p0, LX/9dX;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1518222
    const/4 v0, 0x0

    return v0
.end method
