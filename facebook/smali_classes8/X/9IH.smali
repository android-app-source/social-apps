.class public LX/9IH;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9IF;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9II;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462802
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9IH;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9II;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462799
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462800
    iput-object p1, p0, LX/9IH;->b:LX/0Ot;

    .line 1462801
    return-void
.end method

.method public static a(LX/0QB;)LX/9IH;
    .locals 4

    .prologue
    .line 1462788
    const-class v1, LX/9IH;

    monitor-enter v1

    .line 1462789
    :try_start_0
    sget-object v0, LX/9IH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462790
    sput-object v2, LX/9IH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462791
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462792
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462793
    new-instance v3, LX/9IH;

    const/16 p0, 0x1de8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9IH;-><init>(LX/0Ot;)V

    .line 1462794
    move-object v0, v3

    .line 1462795
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462796
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462797
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1462786
    invoke-static {}, LX/1dS;->b()V

    .line 1462787
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x317637c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1462803
    check-cast p6, LX/9IG;

    .line 1462804
    iget-object v1, p0, LX/9IH;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p6, LX/9IG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1462805
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1462806
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    .line 1462807
    invoke-static {v2}, LX/8jU;->a(Ljava/lang/String;)I

    move-result v2

    .line 1462808
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1462809
    iput v2, p5, LX/1no;->a:I

    .line 1462810
    iput v2, p5, LX/1no;->b:I

    .line 1462811
    const/16 v1, 0x1f

    const v2, 0x3ae4c035

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1462783
    iget-object v0, p0, LX/9IH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1462784
    new-instance v0, LX/9HE;

    invoke-direct {v0, p1}, LX/9HE;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1462785
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1462782
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1462778
    check-cast p3, LX/9IG;

    .line 1462779
    iget-object v0, p0, LX/9IH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/9HE;

    iget-object v0, p3, LX/9IG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1462780
    invoke-virtual {p2, v0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1462781
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1462776
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1462777
    const/16 v0, 0xf

    return v0
.end method
