.class public final LX/9xq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1593491
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1593492
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1593493
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1593494
    invoke-static {p0, p1}, LX/9xq;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1593495
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1593496
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1593485
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1593486
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1593487
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/9xq;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1593488
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1593489
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1593490
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1593497
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1593498
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593499
    :goto_0
    return v1

    .line 1593500
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593501
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1593502
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1593503
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1593504
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1593505
    const-string v4, "album"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1593506
    invoke-static {p0, p1}, LX/9xo;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1593507
    :cond_2
    const-string v4, "page"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1593508
    invoke-static {p0, p1}, LX/9xp;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1593509
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1593510
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1593511
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1593512
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1593474
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1593475
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593476
    if-eqz v0, :cond_0

    .line 1593477
    const-string v1, "album"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593478
    invoke-static {p0, v0, p2, p3}, LX/9xo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1593479
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593480
    if-eqz v0, :cond_1

    .line 1593481
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593482
    invoke-static {p0, v0, p2}, LX/9xp;->a(LX/15i;ILX/0nX;)V

    .line 1593483
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1593484
    return-void
.end method
