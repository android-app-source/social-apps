.class public LX/9hP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/Rect;

.field public final b:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1526628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526629
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9hP;->a:Landroid/graphics/Rect;

    .line 1526630
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9hP;->b:Landroid/graphics/Rect;

    .line 1526631
    return-void
.end method

.method private constructor <init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1526632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526633
    iput-object p1, p0, LX/9hP;->a:Landroid/graphics/Rect;

    .line 1526634
    iput-object p2, p0, LX/9hP;->b:Landroid/graphics/Rect;

    .line 1526635
    return-void
.end method

.method public static a(IILandroid/graphics/Rect;Landroid/widget/ImageView$ScaleType;)LX/9hP;
    .locals 9

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1526598
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v1, v0

    .line 1526599
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v2, v0

    .line 1526600
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    if-eq p3, v0, :cond_0

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    if-eq p3, v0, :cond_0

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    if-ne p3, v0, :cond_3

    .line 1526601
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1526602
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    if-ne p3, v3, :cond_2

    .line 1526603
    int-to-float v0, p0

    div-float v0, v1, v0

    int-to-float v1, p1

    div-float v1, v2, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1526604
    :cond_1
    :goto_0
    int-to-float v1, p0

    mul-float/2addr v1, v0

    .line 1526605
    int-to-float v2, p1

    mul-float/2addr v0, v2

    .line 1526606
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    div-float v3, v1, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 1526607
    int-to-float v3, v2

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 1526608
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    div-float v4, v0, v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 1526609
    int-to-float v4, v3

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 1526610
    new-instance v4, Landroid/graphics/Rect;

    iget v5, p2, Landroid/graphics/Rect;->left:I

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iget v6, p2, Landroid/graphics/Rect;->top:I

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    iget v7, p2, Landroid/graphics/Rect;->right:I

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    iget v8, p2, Landroid/graphics/Rect;->bottom:I

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1526611
    new-instance v5, LX/9hP;

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v2, v3, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {v5, v6, v4}, LX/9hP;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    return-object v5

    .line 1526612
    :cond_2
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    if-ne p3, v3, :cond_1

    .line 1526613
    int-to-float v0, p0

    div-float v0, v1, v0

    int-to-float v1, p1

    div-float v1, v2, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0

    .line 1526614
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not yet supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/widget/ImageView$ScaleType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/1af;Landroid/view/View;Landroid/graphics/Rect;)LX/9hP;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1526615
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526616
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526617
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1526618
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1526619
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1526620
    invoke-virtual {p0, v1}, LX/1af;->a(Landroid/graphics/RectF;)V

    .line 1526621
    aget v2, v0, v4

    int-to-float v2, v2

    aget v3, v0, v5

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 1526622
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1526623
    aget v3, v0, v4

    aget v0, v0, v5

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1526624
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1526625
    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1526626
    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    .line 1526627
    new-instance v2, LX/9hP;

    new-instance v3, Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    float-to-int v4, v4

    iget v5, v1, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    iget v6, v1, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    invoke-direct {v3, v4, v5, v6, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {v2, v3, v0}, LX/9hP;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    return-object v2
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;Landroid/widget/ImageView$ScaleType;)LX/9hP;
    .locals 2

    .prologue
    .line 1526595
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1526596
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1526597
    invoke-static {v0, v1, p1, p2}, LX/9hP;->a(IILandroid/graphics/Rect;Landroid/widget/ImageView$ScaleType;)LX/9hP;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/graphics/RectF;)LX/9hP;
    .locals 7

    .prologue
    .line 1526584
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1526585
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1526586
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/RectF;->offset(FF)V

    .line 1526587
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 1526588
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1526589
    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1526590
    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1526591
    iget v1, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1526592
    iget v1, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1526593
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1526594
    new-instance v1, LX/9hP;

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v4, p1, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget v5, p1, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget v6, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v6, v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {v1, v2, v0}, LX/9hP;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    return-object v1
.end method

.method public static a(Landroid/widget/ImageView;)LX/9hP;
    .locals 7

    .prologue
    .line 1526570
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526571
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526572
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1526573
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    .line 1526574
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1526575
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 1526576
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1526577
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    iget v3, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 1526578
    const/4 v3, 0x1

    aget v0, v0, v3

    int-to-float v0, v0

    iget v3, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 1526579
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v3, v1

    .line 1526580
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v2, v0

    .line 1526581
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1526582
    invoke-virtual {p0, v4}, Landroid/widget/ImageView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1526583
    new-instance v5, LX/9hP;

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v1, v0, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {v5, v6, v4}, LX/9hP;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    return-object v5
.end method

.method public static a(Lcom/facebook/drawee/view/DraweeView;)LX/9hP;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/view/DraweeView",
            "<",
            "LX/1af;",
            ">;)",
            "LX/9hP;"
        }
    .end annotation

    .prologue
    .line 1526565
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526566
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526567
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1526568
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/RectF;)V

    .line 1526569
    invoke-static {p0, v1}, LX/9hP;->a(Landroid/view/View;Landroid/graphics/RectF;)LX/9hP;

    move-result-object v0

    return-object v0
.end method
