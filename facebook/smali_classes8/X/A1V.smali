.class public final LX/A1V;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1610953
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1610954
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1610955
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1610956
    invoke-static {p0, p1}, LX/A1V;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1610957
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1610958
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1610959
    const/4 v13, 0x0

    .line 1610960
    const/4 v12, 0x0

    .line 1610961
    const/4 v11, 0x0

    .line 1610962
    const/4 v10, 0x0

    .line 1610963
    const/4 v9, 0x0

    .line 1610964
    const/4 v8, 0x0

    .line 1610965
    const/4 v7, 0x0

    .line 1610966
    const/4 v6, 0x0

    .line 1610967
    const/4 v5, 0x0

    .line 1610968
    const/4 v4, 0x0

    .line 1610969
    const/4 v3, 0x0

    .line 1610970
    const/4 v2, 0x0

    .line 1610971
    const/4 v1, 0x0

    .line 1610972
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1610973
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1610974
    const/4 v1, 0x0

    .line 1610975
    :goto_0
    return v1

    .line 1610976
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1610977
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_c

    .line 1610978
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1610979
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1610980
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1610981
    const-string v15, "entity"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1610982
    invoke-static/range {p0 .. p1}, LX/A1S;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1610983
    :cond_2
    const-string v15, "has_live_icon"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1610984
    const/4 v2, 0x1

    .line 1610985
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1610986
    :cond_3
    const-string v15, "image"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1610987
    invoke-static/range {p0 .. p1}, LX/A1T;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1610988
    :cond_4
    const-string v15, "is_badged"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1610989
    const/4 v1, 0x1

    .line 1610990
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1610991
    :cond_5
    const-string v15, "query"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1610992
    invoke-static/range {p0 .. p1}, LX/A1U;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1610993
    :cond_6
    const-string v15, "snippet"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1610994
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1610995
    :cond_7
    const-string v15, "subtitle_description"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1610996
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1610997
    :cond_8
    const-string v15, "subtitle_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1610998
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1610999
    :cond_9
    const-string v15, "suggestion_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1611000
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1611001
    :cond_a
    const-string v15, "title"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 1611002
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1611003
    :cond_b
    const-string v15, "tracking"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1611004
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1611005
    :cond_c
    const/16 v14, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1611006
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1611007
    if-eqz v2, :cond_d

    .line 1611008
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 1611009
    :cond_d
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1611010
    if-eqz v1, :cond_e

    .line 1611011
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->a(IZ)V

    .line 1611012
    :cond_e
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1611013
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1611014
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1611015
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1611016
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1611017
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1611018
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1611019
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1611020
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1611021
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1611022
    if-eqz v0, :cond_0

    .line 1611023
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611024
    invoke-static {p0, v0, p2}, LX/A1S;->a(LX/15i;ILX/0nX;)V

    .line 1611025
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1611026
    if-eqz v0, :cond_1

    .line 1611027
    const-string v1, "has_live_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611028
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1611029
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1611030
    if-eqz v0, :cond_3

    .line 1611031
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611032
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1611033
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1611034
    if-eqz v1, :cond_2

    .line 1611035
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611036
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611037
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1611038
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1611039
    if-eqz v0, :cond_4

    .line 1611040
    const-string v1, "is_badged"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611041
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1611042
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1611043
    if-eqz v0, :cond_8

    .line 1611044
    const-string v1, "query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611045
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1611046
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1611047
    if-eqz v1, :cond_5

    .line 1611048
    const-string v2, "query_function"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611049
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611050
    :cond_5
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1611051
    if-eqz v1, :cond_7

    .line 1611052
    const-string v2, "query_title"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611053
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1611054
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1611055
    if-eqz v2, :cond_6

    .line 1611056
    const-string v0, "text"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611057
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611058
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1611059
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1611060
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611061
    if-eqz v0, :cond_9

    .line 1611062
    const-string v1, "snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611063
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611064
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611065
    if-eqz v0, :cond_a

    .line 1611066
    const-string v1, "subtitle_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611067
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611068
    :cond_a
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611069
    if-eqz v0, :cond_b

    .line 1611070
    const-string v1, "subtitle_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611071
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611072
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611073
    if-eqz v0, :cond_c

    .line 1611074
    const-string v1, "suggestion_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611075
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611076
    :cond_c
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611077
    if-eqz v0, :cond_d

    .line 1611078
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611079
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611080
    :cond_d
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611081
    if-eqz v0, :cond_e

    .line 1611082
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611083
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611084
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1611085
    return-void
.end method
