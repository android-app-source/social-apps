.class public LX/9k8;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1530878
    const-string v0, "CREATE TABLE places_model (  _id INTEGER PRIMARY KEY,   content TEXT);"

    sput-object v0, LX/9k8;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530854
    iput-object p1, p0, LX/9k8;->b:Landroid/content/Context;

    .line 1530855
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 1530865
    new-instance v9, LX/9k7;

    iget-object v0, p0, LX/9k8;->b:Landroid/content/Context;

    invoke-direct {v9, v0}, LX/9k7;-><init>(Landroid/content/Context;)V

    .line 1530866
    invoke-virtual {v9}, LX/9k7;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1530867
    :try_start_0
    const-string v1, "places_model"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "content"

    aput-object v4, v2, v3

    const-string v3, " _id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1530868
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1530869
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v8

    .line 1530870
    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1530871
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1530872
    :cond_1
    invoke-virtual {v9}, LX/9k7;->close()V

    .line 1530873
    return-object v8

    .line 1530874
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1530875
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1530876
    :cond_2
    invoke-virtual {v9}, LX/9k7;->close()V

    throw v0

    .line 1530877
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 7

    .prologue
    .line 1530856
    new-instance v1, LX/9k7;

    iget-object v0, p0, LX/9k8;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, LX/9k7;-><init>(Landroid/content/Context;)V

    .line 1530857
    invoke-virtual {v1}, LX/9k7;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1530858
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1530859
    const-string v3, "_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1530860
    const-string v3, "content"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530861
    :try_start_0
    const-string v3, "places_model"

    const/4 v4, 0x0

    const/4 v5, 0x5

    const v6, -0x4986d566

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v0, v3, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, -0x365b68b1

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1530862
    invoke-virtual {v1}, LX/9k7;->close()V

    .line 1530863
    return-void

    .line 1530864
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/9k7;->close()V

    throw v0
.end method
