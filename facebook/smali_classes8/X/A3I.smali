.class public final LX/A3I;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1616093
    const/4 v8, 0x0

    .line 1616094
    const-wide/16 v6, 0x0

    .line 1616095
    const/4 v5, 0x0

    .line 1616096
    const/4 v4, 0x0

    .line 1616097
    const-wide/16 v2, 0x0

    .line 1616098
    const/4 v1, 0x0

    .line 1616099
    const/4 v0, 0x0

    .line 1616100
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1616101
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1616102
    const/4 v0, 0x0

    .line 1616103
    :goto_0
    return v0

    .line 1616104
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_7

    .line 1616105
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1616106
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1616107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1616108
    const-string v4, "candidates"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1616109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1616110
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_1

    .line 1616111
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_1

    .line 1616112
    invoke-static {p0, p1}, LX/A3F;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1616113
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1616114
    :cond_1
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1616115
    move v11, v0

    goto :goto_1

    .line 1616116
    :cond_2
    const-string v4, "polling_percentage"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1616117
    const/4 v0, 0x1

    .line 1616118
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1616119
    :cond_3
    const-string v4, "race_name"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1616120
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 1616121
    :cond_4
    const-string v4, "state_postal"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1616122
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 1616123
    :cond_5
    const-string v4, "total_electoral_votes"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1616124
    const/4 v0, 0x1

    .line 1616125
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto/16 :goto_1

    .line 1616126
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1616127
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1616128
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1616129
    if-eqz v1, :cond_8

    .line 1616130
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1616131
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1616132
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1616133
    if-eqz v6, :cond_9

    .line 1616134
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1616135
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v10, v5

    move v11, v8

    move-wide v8, v2

    move-wide v2, v6

    move v6, v0

    move v7, v4

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1616136
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1616137
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1616138
    if-eqz v0, :cond_1

    .line 1616139
    const-string v1, "candidates"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616140
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1616141
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1616142
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/A3F;->a(LX/15i;ILX/0nX;)V

    .line 1616143
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1616144
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1616145
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1616146
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 1616147
    const-string v2, "polling_percentage"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616148
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1616149
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1616150
    if-eqz v0, :cond_3

    .line 1616151
    const-string v1, "race_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616152
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1616153
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1616154
    if-eqz v0, :cond_4

    .line 1616155
    const-string v1, "state_postal"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616156
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1616157
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1616158
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 1616159
    const-string v2, "total_electoral_votes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616160
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1616161
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1616162
    return-void
.end method
