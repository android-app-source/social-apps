.class public final LX/AO2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/AO3;

.field public b:I

.field public c:LX/7S9;


# direct methods
.method public constructor <init>(LX/AO3;)V
    .locals 1

    .prologue
    .line 1668614
    iput-object p1, p0, LX/AO2;->a:LX/AO3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668615
    const/4 v0, -0x1

    iput v0, p0, LX/AO2;->b:I

    .line 1668616
    const/4 v0, 0x0

    iput-object v0, p0, LX/AO2;->c:LX/7S9;

    return-void
.end method

.method private a(Ljava/util/List;FFI)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7S9;",
            ">;FFI)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1668617
    iget-object v1, p0, LX/AO2;->c:LX/7S9;

    if-eqz v1, :cond_0

    .line 1668618
    iget-object v1, p0, LX/AO2;->c:LX/7S9;

    iget v1, v1, LX/7S9;->b:F

    sub-float/2addr v1, p2

    .line 1668619
    iget-object v2, p0, LX/AO2;->c:LX/7S9;

    iget v2, v2, LX/7S9;->c:F

    sub-float/2addr v2, p3

    .line 1668620
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x41200000    # 10.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    .line 1668621
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1668622
    new-instance v0, LX/7S9;

    sget-object v1, LX/7S8;->MOVE:LX/7S8;

    invoke-direct {v0, v1, p2, p3, p4}, LX/7S9;-><init>(LX/7S8;FFI)V

    iput-object v0, p0, LX/AO2;->c:LX/7S9;

    .line 1668623
    iget-object v0, p0, LX/AO2;->c:LX/7S9;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668624
    :cond_1
    return-void

    .line 1668625
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1668626
    iget-object v2, p0, LX/AO2;->a:LX/AO3;

    iget v2, v2, LX/AO3;->n:I

    if-nez v2, :cond_0

    .line 1668627
    :goto_0
    return v0

    .line 1668628
    :cond_0
    invoke-static {p2}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1668629
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 1668630
    packed-switch v2, :pswitch_data_0

    :cond_1
    :pswitch_0
    move v2, v0

    move v3, v0

    move v4, v0

    .line 1668631
    :goto_1
    if-eqz v3, :cond_2

    .line 1668632
    invoke-static {p2, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v3

    iput v3, p0, LX/AO2;->b:I

    .line 1668633
    new-instance v3, LX/7S9;

    sget-object v6, LX/7S8;->START:LX/7S8;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iget-object v9, p0, LX/AO2;->a:LX/AO3;

    iget v9, v9, LX/AO3;->n:I

    invoke-direct {v3, v6, v7, v8, v9}, LX/7S9;-><init>(LX/7S8;FFI)V

    iput-object v3, p0, LX/AO2;->c:LX/7S9;

    .line 1668634
    iget-object v3, p0, LX/AO2;->c:LX/7S9;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668635
    :cond_2
    if-eqz v4, :cond_4

    .line 1668636
    iget v3, p0, LX/AO2;->b:I

    invoke-static {p2, v3}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 1668637
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v4

    .line 1668638
    :goto_2
    if-ge v0, v4, :cond_3

    .line 1668639
    invoke-virtual {p2, v3, v0}, Landroid/view/MotionEvent;->getHistoricalX(II)F

    move-result v6

    .line 1668640
    invoke-virtual {p2, v3, v0}, Landroid/view/MotionEvent;->getHistoricalY(II)F

    move-result v7

    .line 1668641
    iget-object v8, p0, LX/AO2;->a:LX/AO3;

    iget v8, v8, LX/AO3;->n:I

    invoke-direct {p0, v5, v6, v7, v8}, LX/AO2;->a(Ljava/util/List;FFI)V

    .line 1668642
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1668643
    :pswitch_1
    iget v2, p0, LX/AO2;->b:I

    if-ne v2, v3, :cond_1

    move v2, v0

    move v3, v1

    move v4, v0

    .line 1668644
    goto :goto_1

    :pswitch_2
    move v2, v1

    move v3, v0

    move v4, v0

    .line 1668645
    goto :goto_1

    .line 1668646
    :pswitch_3
    invoke-static {p2}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1668647
    invoke-static {p2, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 1668648
    iget v3, p0, LX/AO2;->b:I

    if-ne v2, v3, :cond_1

    move v2, v1

    move v3, v0

    move v4, v0

    .line 1668649
    goto :goto_1

    .line 1668650
    :pswitch_4
    iget v2, p0, LX/AO2;->b:I

    if-eq v2, v3, :cond_1

    move v2, v0

    move v3, v0

    move v4, v1

    .line 1668651
    goto :goto_1

    .line 1668652
    :cond_3
    invoke-static {p2, v3}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1668653
    invoke-static {p2, v3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 1668654
    iget-object v4, p0, LX/AO2;->a:LX/AO3;

    iget v4, v4, LX/AO3;->n:I

    invoke-direct {p0, v5, v0, v3, v4}, LX/AO2;->a(Ljava/util/List;FFI)V

    .line 1668655
    :cond_4
    if-eqz v2, :cond_5

    .line 1668656
    const/4 v0, -0x1

    iput v0, p0, LX/AO2;->b:I

    .line 1668657
    const/4 v0, 0x0

    iput-object v0, p0, LX/AO2;->c:LX/7S9;

    .line 1668658
    new-instance v0, LX/7S9;

    sget-object v2, LX/7S8;->END:LX/7S8;

    invoke-direct {v0, v2}, LX/7S9;-><init>(LX/7S8;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668659
    :cond_5
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1668660
    iget-object v0, p0, LX/AO2;->a:LX/AO3;

    iget-object v0, v0, LX/AO3;->a:LX/6Jt;

    new-instance v2, LX/7Sg;

    invoke-direct {v2, v5}, LX/7Sg;-><init>(Ljava/util/List;)V

    iget-object v3, p0, LX/AO2;->a:LX/AO3;

    iget-object v3, v3, LX/AO3;->j:LX/7SB;

    invoke-virtual {v0, v2, v3}, LX/6Jt;->a(LX/7Sb;LX/6Jv;)V

    :cond_6
    move v0, v1

    .line 1668661
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
