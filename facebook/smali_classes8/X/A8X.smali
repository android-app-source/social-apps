.class public abstract LX/A8X;
.super LX/1OM;
.source ""

# interfaces
.implements LX/90h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/A8Y;",
        ">;",
        "LX/90h;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1627785
    invoke-direct {p0}, LX/1OM;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 1627784
    invoke-virtual {p0, p1, p2}, LX/A8X;->c(Landroid/view/ViewGroup;I)LX/A8Y;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1a1;I)V
    .locals 0

    .prologue
    .line 1627783
    check-cast p1, LX/A8Y;

    invoke-virtual {p0, p1, p2}, LX/A8X;->a(LX/A8Y;I)V

    return-void
.end method

.method public final a(LX/A8O;)V
    .locals 1

    .prologue
    .line 1627777
    new-instance v0, LX/A8W;

    invoke-direct {v0, p0, p1}, LX/A8W;-><init>(LX/A8X;LX/A8O;)V

    invoke-super {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 1627778
    return-void
.end method

.method public final a(LX/A8Y;I)V
    .locals 1

    .prologue
    .line 1627781
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0, v0, p2}, LX/A8X;->a(Landroid/view/View;I)V

    .line 1627782
    return-void
.end method

.method public abstract a(Landroid/view/View;I)V
.end method

.method public final c(Landroid/view/ViewGroup;I)LX/A8Y;
    .locals 2

    .prologue
    .line 1627780
    new-instance v0, LX/A8Y;

    invoke-virtual {p0, p1, p2}, LX/A8X;->d(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/A8Y;-><init>(Landroid/view/View;LX/A8X;)V

    return-object v0
.end method

.method public abstract d(Landroid/view/ViewGroup;I)Landroid/view/View;
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1627779
    invoke-virtual {p0}, LX/A8X;->getCount()I

    move-result v0

    return v0
.end method
