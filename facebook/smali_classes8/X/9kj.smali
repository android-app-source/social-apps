.class public final LX/9kj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9kk;


# direct methods
.method public constructor <init>(LX/9kk;)V
    .locals 0

    .prologue
    .line 1532106
    iput-object p1, p0, LX/9kj;->a:LX/9kk;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1532107
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1532108
    check-cast p1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;

    .line 1532109
    :try_start_0
    iget-object v0, p0, LX/9kj;->a:LX/9kk;

    const/4 v6, 0x0

    .line 1532110
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1532111
    invoke-virtual {p1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;->a()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v4, 0x310d57c8

    invoke-static {v3, v1, v6, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    .line 1532112
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1532113
    const-class v5, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-virtual {v4, v1, v6, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-static {v1}, LX/9kh;->a(Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;)Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1532114
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_0

    .line 1532115
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, v0, LX/9kk;->b:LX/0am;

    .line 1532116
    iget-object v0, p0, LX/9kj;->a:LX/9kk;

    invoke-virtual {v0}, LX/9kh;->b()V
    :try_end_0
    .catch LX/4BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 1532117
    :goto_2
    return-void

    :catch_0
    goto :goto_2
.end method
