.class public LX/9HW;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9HU;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9HX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1461387
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9HW;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9HX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1461388
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1461389
    iput-object p1, p0, LX/9HW;->b:LX/0Ot;

    .line 1461390
    return-void
.end method

.method public static a(LX/0QB;)LX/9HW;
    .locals 4

    .prologue
    .line 1461391
    const-class v1, LX/9HW;

    monitor-enter v1

    .line 1461392
    :try_start_0
    sget-object v0, LX/9HW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461393
    sput-object v2, LX/9HW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461394
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461395
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461396
    new-instance v3, LX/9HW;

    const/16 p0, 0x1dd3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9HW;-><init>(LX/0Ot;)V

    .line 1461397
    move-object v0, v3

    .line 1461398
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461399
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9HW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461400
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1461402
    check-cast p2, LX/9HV;

    .line 1461403
    iget-object v0, p0, LX/9HW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9HX;

    iget-object v1, p2, LX/9HV;->a:LX/1X1;

    iget-object v2, p2, LX/9HV;->b:LX/9FD;

    iget-object v3, p2, LX/9HV;->c:LX/9HA;

    .line 1461404
    iget-object v4, v0, LX/9HX;->a:LX/9CG;

    invoke-virtual {v4, p1}, LX/9CG;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1461405
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    .line 1461406
    invoke-virtual {v2, v3}, LX/9FD;->a(LX/9HA;)I

    move-result v5

    .line 1461407
    iget p0, v2, LX/9FD;->d:I

    move p0, p0

    .line 1461408
    invoke-static {p1, v1}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object p2

    invoke-interface {p2, v4}, LX/1Di;->a(LX/1dc;)LX/1Di;

    move-result-object v4

    const/4 p2, 0x4

    invoke-interface {v4, p2, v5}, LX/1Di;->e(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x5

    invoke-interface {v4, v5, p0}, LX/1Di;->e(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1461409
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1461410
    invoke-static {}, LX/1dS;->b()V

    .line 1461411
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/9HU;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1461412
    new-instance v1, LX/9HV;

    invoke-direct {v1, p0}, LX/9HV;-><init>(LX/9HW;)V

    .line 1461413
    sget-object v2, LX/9HW;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9HU;

    .line 1461414
    if-nez v2, :cond_0

    .line 1461415
    new-instance v2, LX/9HU;

    invoke-direct {v2}, LX/9HU;-><init>()V

    .line 1461416
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/9HU;->a$redex0(LX/9HU;LX/1De;IILX/9HV;)V

    .line 1461417
    move-object v1, v2

    .line 1461418
    move-object v0, v1

    .line 1461419
    return-object v0
.end method
