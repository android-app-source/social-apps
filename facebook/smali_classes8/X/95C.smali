.class public LX/95C;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/95B;

.field public final b:J

.field public final c:LX/95A;


# direct methods
.method public constructor <init>(LX/95B;JLX/95A;)V
    .locals 0
    .param p4    # LX/95A;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1436418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436419
    iput-object p1, p0, LX/95C;->a:LX/95B;

    .line 1436420
    iput-wide p2, p0, LX/95C;->b:J

    .line 1436421
    iput-object p4, p0, LX/95C;->c:LX/95A;

    .line 1436422
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1436423
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 1436424
    :cond_0
    :goto_0
    return v0

    .line 1436425
    :cond_1
    check-cast p1, LX/95C;

    .line 1436426
    iget-object v1, p0, LX/95C;->a:LX/95B;

    iget-object v2, p1, LX/95C;->a:LX/95B;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, LX/95C;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, LX/95C;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/95C;->c:LX/95A;

    iget-object v2, p1, LX/95C;->c:LX/95A;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1436417
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/95C;->a:LX/95B;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, LX/95C;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/95C;->c:LX/95A;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
