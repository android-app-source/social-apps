.class public LX/9fx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/0rq;

.field private final c:LX/0se;


# direct methods
.method public constructor <init>(LX/0tX;LX/0rq;LX/0se;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1522796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522797
    iput-object p1, p0, LX/9fx;->a:LX/0tX;

    .line 1522798
    iput-object p2, p0, LX/9fx;->b:LX/0rq;

    .line 1522799
    iput-object p3, p0, LX/9fx;->c:LX/0se;

    .line 1522800
    return-void
.end method

.method public static a(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522791
    new-instance v0, LX/5h5;

    invoke-direct {v0}, LX/5h5;-><init>()V

    move-object v1, v0

    .line 1522792
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 1522793
    invoke-static/range {v0 .. v7}, LX/9fx;->a(LX/9fx;LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1522794
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1522795
    iget-object v1, p0, LX/9fx;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/9fq;

    invoke-direct {v1, p0}, LX/9fq;-><init>(LX/9fx;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/9fx;LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 2

    .prologue
    .line 1522821
    const-string v0, "node_id"

    invoke-virtual {p1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1522822
    const-string v0, "before"

    invoke-virtual {p1, v0, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1522823
    const-string v0, "after"

    invoke-virtual {p1, v0, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1522824
    iget-object v0, p0, LX/9fx;->c:LX/0se;

    invoke-virtual {v0, p1}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 1522825
    if-lez p5, :cond_0

    .line 1522826
    const-string v0, "first"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1522827
    :cond_0
    if-lez p6, :cond_1

    .line 1522828
    const-string v0, "image_width"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1522829
    :cond_1
    if-lez p7, :cond_2

    .line 1522830
    const-string v0, "image_height"

    invoke-static {p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1522831
    :cond_2
    return-void
.end method

.method public static b(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522816
    new-instance v0, LX/5h4;

    invoke-direct {v0}, LX/5h4;-><init>()V

    move-object v1, v0

    .line 1522817
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 1522818
    invoke-static/range {v0 .. v7}, LX/9fx;->a(LX/9fx;LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1522819
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1522820
    iget-object v1, p0, LX/9fx;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/9fr;

    invoke-direct {v1, p0}, LX/9fr;-><init>(LX/9fx;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522832
    new-instance v0, LX/5h3;

    invoke-direct {v0}, LX/5h3;-><init>()V

    move-object v1, v0

    .line 1522833
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 1522834
    invoke-static/range {v0 .. v7}, LX/9fx;->a(LX/9fx;LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1522835
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1522836
    iget-object v1, p0, LX/9fx;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/9fs;

    invoke-direct {v1, p0}, LX/9fs;-><init>(LX/9fx;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522811
    new-instance v0, LX/5h2;

    invoke-direct {v0}, LX/5h2;-><init>()V

    move-object v1, v0

    .line 1522812
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 1522813
    invoke-static/range {v0 .. v7}, LX/9fx;->a(LX/9fx;LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1522814
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1522815
    iget-object v1, p0, LX/9fx;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/9ft;

    invoke-direct {v1, p0}, LX/9ft;-><init>(LX/9fx;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522806
    new-instance v0, LX/5h1;

    invoke-direct {v0}, LX/5h1;-><init>()V

    move-object v1, v0

    .line 1522807
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 1522808
    invoke-static/range {v0 .. v7}, LX/9fx;->a(LX/9fx;LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1522809
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1522810
    iget-object v1, p0, LX/9fx;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/9fu;

    invoke-direct {v1, p0}, LX/9fu;-><init>(LX/9fx;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522801
    new-instance v0, LX/5h0;

    invoke-direct {v0}, LX/5h0;-><init>()V

    move-object v1, v0

    .line 1522802
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 1522803
    invoke-static/range {v0 .. v7}, LX/9fx;->a(LX/9fx;LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1522804
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1522805
    iget-object v1, p0, LX/9fx;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/9fv;

    invoke-direct {v1, p0}, LX/9fv;-><init>(LX/9fx;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
