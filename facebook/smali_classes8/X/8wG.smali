.class public final LX/8wG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:[I

.field public final b:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TK;"
        }
    .end annotation
.end field

.field public final c:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([Ljava/lang/Object;[Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TK;[TV;)V"
        }
    .end annotation

    .prologue
    .line 1422154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422155
    invoke-static {p1}, LX/8wG;->a([Ljava/lang/Object;)[I

    move-result-object v0

    iput-object v0, p0, LX/8wG;->a:[I

    .line 1422156
    iget-object v0, p0, LX/8wG;->a:[I

    invoke-static {p1, v0}, LX/8wG;->a([Ljava/lang/Object;[I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/8wG;->b:[Ljava/lang/Object;

    .line 1422157
    iget-object v0, p0, LX/8wG;->a:[I

    invoke-static {p2, v0}, LX/8wG;->a([Ljava/lang/Object;[I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/8wG;->c:[Ljava/lang/Object;

    .line 1422158
    return-void
.end method

.method public synthetic constructor <init>([Ljava/lang/Object;[Ljava/lang/Object;B)V
    .locals 0

    .prologue
    .line 1422133
    invoke-direct {p0, p1, p2}, LX/8wG;-><init>([Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method private static a([Ljava/lang/Object;)[I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">([TK;)[I"
        }
    .end annotation

    .prologue
    .line 1422134
    array-length v2, p0

    .line 1422135
    new-array v3, v2, [I

    .line 1422136
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1422137
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1422138
    aget-object v5, p0, v1

    .line 1422139
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1422140
    if-nez v0, :cond_0

    .line 1422141
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1422142
    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1422143
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 1422144
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1422145
    :cond_1
    return-object v3
.end method

.method private static a([Ljava/lang/Object;[I)[Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">([TK;[I)[TK;"
        }
    .end annotation

    .prologue
    .line 1422146
    array-length v2, p0

    .line 1422147
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    .line 1422148
    const/4 v1, -0x1

    invoke-static {p1, v1}, Landroid/support/v7/widget/GridLayout;->a([II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 1422149
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1422150
    aget v3, p1, v1

    aget-object v4, p0, v1

    aput-object v4, v0, v3

    .line 1422151
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1422152
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 1422153
    iget-object v0, p0, LX/8wG;->c:[Ljava/lang/Object;

    iget-object v1, p0, LX/8wG;->a:[I

    aget v1, v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method
