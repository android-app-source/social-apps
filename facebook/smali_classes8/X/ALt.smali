.class public final LX/ALt;
.super Landroid/view/View;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/ZoomView;

.field private final b:Landroid/graphics/Paint;

.field private final c:I

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/ZoomView;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1665471
    iput-object p1, p0, LX/ALt;->a:Lcom/facebook/backstage/camera/ZoomView;

    .line 1665472
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1665473
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/ALt;->setWillNotDraw(Z)V

    .line 1665474
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/ALt;->b:Landroid/graphics/Paint;

    .line 1665475
    iget-object v0, p0, LX/ALt;->b:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1665476
    iget-object v0, p0, LX/ALt;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1665477
    iget-object v0, p0, LX/ALt;->b:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, LX/ALt;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1665478
    iget-object v0, p0, LX/ALt;->b:Landroid/graphics/Paint;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1665479
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0}, LX/ALt;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/ALt;->c:I

    .line 1665480
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/backstage/camera/ZoomView;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 1665470
    invoke-direct {p0, p1, p2}, LX/ALt;-><init>(Lcom/facebook/backstage/camera/ZoomView;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(FF)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1665467
    iget v0, p0, LX/ALt;->e:F

    iget v1, p0, LX/ALt;->d:F

    iget v2, p0, LX/ALt;->e:F

    sub-float/2addr v1, v2

    sub-float v2, p1, v3

    sub-float v3, p2, v3

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, LX/ALt;->f:F

    .line 1665468
    invoke-virtual {p0}, LX/ALt;->invalidate()V

    .line 1665469
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1665458
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1665459
    iget v0, p0, LX/ALt;->g:F

    iget v1, p0, LX/ALt;->h:F

    iget v2, p0, LX/ALt;->f:F

    iget-object v3, p0, LX/ALt;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1665460
    iget v0, p0, LX/ALt;->g:F

    iget v1, p0, LX/ALt;->h:F

    iget v2, p0, LX/ALt;->d:F

    iget-object v3, p0, LX/ALt;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1665461
    iget v0, p0, LX/ALt;->g:F

    iget v1, p0, LX/ALt;->h:F

    iget v2, p0, LX/ALt;->e:F

    iget-object v3, p0, LX/ALt;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1665462
    iget v1, p0, LX/ALt;->g:F

    iget v0, p0, LX/ALt;->h:F

    iget v2, p0, LX/ALt;->e:F

    sub-float v2, v0, v2

    iget v3, p0, LX/ALt;->g:F

    iget v0, p0, LX/ALt;->h:F

    iget v4, p0, LX/ALt;->d:F

    sub-float v4, v0, v4

    iget-object v5, p0, LX/ALt;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1665463
    iget v1, p0, LX/ALt;->g:F

    iget v0, p0, LX/ALt;->h:F

    iget v2, p0, LX/ALt;->e:F

    add-float/2addr v2, v0

    iget v3, p0, LX/ALt;->g:F

    iget v0, p0, LX/ALt;->h:F

    iget v4, p0, LX/ALt;->d:F

    add-float/2addr v4, v0

    iget-object v5, p0, LX/ALt;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1665464
    iget v0, p0, LX/ALt;->g:F

    iget v1, p0, LX/ALt;->e:F

    sub-float v1, v0, v1

    iget v2, p0, LX/ALt;->h:F

    iget v0, p0, LX/ALt;->g:F

    iget v3, p0, LX/ALt;->d:F

    sub-float v3, v0, v3

    iget v4, p0, LX/ALt;->h:F

    iget-object v5, p0, LX/ALt;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1665465
    iget v0, p0, LX/ALt;->g:F

    iget v1, p0, LX/ALt;->e:F

    add-float/2addr v1, v0

    iget v2, p0, LX/ALt;->h:F

    iget v0, p0, LX/ALt;->g:F

    iget v3, p0, LX/ALt;->d:F

    add-float/2addr v3, v0

    iget v4, p0, LX/ALt;->h:F

    iget-object v5, p0, LX/ALt;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1665466
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 1665452
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 1665453
    invoke-virtual {p0}, LX/ALt;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/ALt;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, LX/ALt;->c:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, LX/ALt;->d:F

    .line 1665454
    const/4 v0, 0x1

    const/high16 v1, 0x42400000    # 48.0f

    invoke-virtual {p0}, LX/ALt;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/ALt;->e:F

    .line 1665455
    invoke-virtual {p0}, LX/ALt;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, LX/ALt;->g:F

    .line 1665456
    invoke-virtual {p0}, LX/ALt;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, LX/ALt;->h:F

    .line 1665457
    return-void
.end method
