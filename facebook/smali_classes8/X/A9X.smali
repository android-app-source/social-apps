.class public final LX/A9X;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 1634817
    const/16 v23, 0x0

    .line 1634818
    const/16 v22, 0x0

    .line 1634819
    const/16 v21, 0x0

    .line 1634820
    const/16 v20, 0x0

    .line 1634821
    const/16 v19, 0x0

    .line 1634822
    const/16 v18, 0x0

    .line 1634823
    const/16 v17, 0x0

    .line 1634824
    const/16 v16, 0x0

    .line 1634825
    const/4 v15, 0x0

    .line 1634826
    const/4 v14, 0x0

    .line 1634827
    const/4 v13, 0x0

    .line 1634828
    const/4 v12, 0x0

    .line 1634829
    const/4 v11, 0x0

    .line 1634830
    const/4 v10, 0x0

    .line 1634831
    const/4 v9, 0x0

    .line 1634832
    const/4 v8, 0x0

    .line 1634833
    const/4 v7, 0x0

    .line 1634834
    const/4 v6, 0x0

    .line 1634835
    const/4 v5, 0x0

    .line 1634836
    const/4 v4, 0x0

    .line 1634837
    const/4 v3, 0x0

    .line 1634838
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 1634839
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1634840
    const/4 v3, 0x0

    .line 1634841
    :goto_0
    return v3

    .line 1634842
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1634843
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_10

    .line 1634844
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 1634845
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1634846
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 1634847
    const-string v25, "ad_accounts"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 1634848
    invoke-static/range {p0 .. p1}, LX/A9T;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 1634849
    :cond_2
    const-string v25, "boosted_cta_promotions"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 1634850
    invoke-static/range {p0 .. p1}, LX/AAE;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 1634851
    :cond_3
    const-string v25, "boosted_local_awareness_promotions"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 1634852
    invoke-static/range {p0 .. p1}, LX/AAA;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 1634853
    :cond_4
    const-string v25, "boosted_page_like_promotions"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 1634854
    invoke-static/range {p0 .. p1}, LX/AAC;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 1634855
    :cond_5
    const-string v25, "boosted_post_default_audience"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 1634856
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    goto :goto_1

    .line 1634857
    :cond_6
    const-string v25, "boosted_post_default_spec"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 1634858
    invoke-static/range {p0 .. p1}, LX/A9W;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1634859
    :cond_7
    const-string v25, "boosted_website_promotions"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 1634860
    invoke-static/range {p0 .. p1}, LX/AAL;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1634861
    :cond_8
    const-string v25, "budget_recommendations"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 1634862
    invoke-static/range {p0 .. p1}, LX/A9x;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1634863
    :cond_9
    const-string v25, "can_viewer_promote_for_page_likes"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 1634864
    const/4 v8, 0x1

    .line 1634865
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 1634866
    :cond_a
    const-string v25, "can_viewer_promote_website"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 1634867
    const/4 v7, 0x1

    .line 1634868
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 1634869
    :cond_b
    const-string v25, "default_duration_for_boosted_post"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 1634870
    const/4 v6, 0x1

    .line 1634871
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1634872
    :cond_c
    const-string v25, "has_boosted_posts"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 1634873
    const/4 v5, 0x1

    .line 1634874
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 1634875
    :cond_d
    const-string v25, "is_viewer_business_manager_admin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 1634876
    const/4 v4, 0x1

    .line 1634877
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 1634878
    :cond_e
    const-string v25, "last_used_targeting"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 1634879
    invoke-static/range {p0 .. p1}, LX/AAU;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1634880
    :cond_f
    const-string v25, "messaging_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 1634881
    const/4 v3, 0x1

    .line 1634882
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 1634883
    :cond_10
    const/16 v24, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1634884
    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634885
    const/16 v23, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634886
    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634887
    const/16 v21, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634888
    const/16 v20, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634889
    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634890
    const/16 v18, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634891
    const/16 v17, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634892
    if-eqz v8, :cond_11

    .line 1634893
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->a(IZ)V

    .line 1634894
    :cond_11
    if-eqz v7, :cond_12

    .line 1634895
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->a(IZ)V

    .line 1634896
    :cond_12
    if-eqz v6, :cond_13

    .line 1634897
    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13, v7}, LX/186;->a(III)V

    .line 1634898
    :cond_13
    if-eqz v5, :cond_14

    .line 1634899
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(IZ)V

    .line 1634900
    :cond_14
    if-eqz v4, :cond_15

    .line 1634901
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 1634902
    :cond_15
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1634903
    if-eqz v3, :cond_16

    .line 1634904
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1634905
    :cond_16
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1634906
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634907
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1634908
    if-eqz v0, :cond_0

    .line 1634909
    const-string v1, "ad_accounts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634910
    invoke-static {p0, v0, p2, p3}, LX/A9T;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634911
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634912
    if-eqz v0, :cond_1

    .line 1634913
    const-string v1, "boosted_cta_promotions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634914
    invoke-static {p0, v0, p2, p3}, LX/AAE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634915
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634916
    if-eqz v0, :cond_2

    .line 1634917
    const-string v1, "boosted_local_awareness_promotions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634918
    invoke-static {p0, v0, p2, p3}, LX/AAA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634919
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634920
    if-eqz v0, :cond_3

    .line 1634921
    const-string v1, "boosted_page_like_promotions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634922
    invoke-static {p0, v0, p2, p3}, LX/AAC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634923
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1634924
    if-eqz v0, :cond_4

    .line 1634925
    const-string v0, "boosted_post_default_audience"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634926
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634927
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634928
    if-eqz v0, :cond_7

    .line 1634929
    const-string v1, "boosted_post_default_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634930
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1634931
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634932
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1634933
    if-eqz v1, :cond_5

    .line 1634934
    const-string v3, "bid_amount"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634935
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1634936
    :cond_5
    invoke-virtual {p0, v0, v4}, LX/15i;->g(II)I

    move-result v1

    .line 1634937
    if-eqz v1, :cond_6

    .line 1634938
    const-string v1, "pacing_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634939
    invoke-virtual {p0, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634940
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634941
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634942
    if-eqz v0, :cond_8

    .line 1634943
    const-string v1, "boosted_website_promotions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634944
    invoke-static {p0, v0, p2, p3}, LX/AAL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634945
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634946
    if-eqz v0, :cond_9

    .line 1634947
    const-string v1, "budget_recommendations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634948
    invoke-static {p0, v0, p2, p3}, LX/A9x;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634949
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634950
    if-eqz v0, :cond_a

    .line 1634951
    const-string v1, "can_viewer_promote_for_page_likes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634952
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634953
    :cond_a
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634954
    if-eqz v0, :cond_b

    .line 1634955
    const-string v1, "can_viewer_promote_website"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634956
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634957
    :cond_b
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1634958
    if-eqz v0, :cond_c

    .line 1634959
    const-string v1, "default_duration_for_boosted_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634960
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1634961
    :cond_c
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634962
    if-eqz v0, :cond_d

    .line 1634963
    const-string v1, "has_boosted_posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634964
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634965
    :cond_d
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634966
    if-eqz v0, :cond_e

    .line 1634967
    const-string v1, "is_viewer_business_manager_admin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634968
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634969
    :cond_e
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634970
    if-eqz v0, :cond_f

    .line 1634971
    const-string v1, "last_used_targeting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634972
    invoke-static {p0, v0, p2, p3}, LX/AAU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634973
    :cond_f
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634974
    if-eqz v0, :cond_10

    .line 1634975
    const-string v1, "messaging_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634976
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634977
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634978
    return-void
.end method
