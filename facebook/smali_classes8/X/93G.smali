.class public final enum LX/93G;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/93G;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/93G;

.field public static final enum ACTIVITIES_TAB:LX/93G;

.field public static final enum FEELINGS_TAB:LX/93G;

.field public static final enum STICKERS_TAB:LX/93G;


# instance fields
.field public final mTitleBarResource:I

.field public final mTitleResource:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1433550
    new-instance v0, LX/93G;

    const-string v1, "FEELINGS_TAB"

    const v2, 0x7f08141e

    const v3, 0x7f08141c

    invoke-direct {v0, v1, v4, v2, v3}, LX/93G;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/93G;->FEELINGS_TAB:LX/93G;

    .line 1433551
    new-instance v0, LX/93G;

    const-string v1, "STICKERS_TAB"

    const v2, 0x7f08141f

    const v3, 0x7f08141d

    invoke-direct {v0, v1, v5, v2, v3}, LX/93G;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/93G;->STICKERS_TAB:LX/93G;

    .line 1433552
    new-instance v0, LX/93G;

    const-string v1, "ACTIVITIES_TAB"

    const v2, 0x7f081420

    const v3, 0x7f08141b

    invoke-direct {v0, v1, v6, v2, v3}, LX/93G;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/93G;->ACTIVITIES_TAB:LX/93G;

    .line 1433553
    const/4 v0, 0x3

    new-array v0, v0, [LX/93G;

    sget-object v1, LX/93G;->FEELINGS_TAB:LX/93G;

    aput-object v1, v0, v4

    sget-object v1, LX/93G;->STICKERS_TAB:LX/93G;

    aput-object v1, v0, v5

    sget-object v1, LX/93G;->ACTIVITIES_TAB:LX/93G;

    aput-object v1, v0, v6

    sput-object v0, LX/93G;->$VALUES:[LX/93G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1433554
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1433555
    iput p3, p0, LX/93G;->mTitleResource:I

    .line 1433556
    iput p4, p0, LX/93G;->mTitleBarResource:I

    .line 1433557
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/93G;
    .locals 1

    .prologue
    .line 1433558
    const-class v0, LX/93G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/93G;

    return-object v0
.end method

.method public static values()[LX/93G;
    .locals 1

    .prologue
    .line 1433559
    sget-object v0, LX/93G;->$VALUES:[LX/93G;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/93G;

    return-object v0
.end method
