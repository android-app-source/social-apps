.class public final LX/AE6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AE7;


# direct methods
.method public constructor <init>(LX/AE7;)V
    .locals 0

    .prologue
    .line 1646014
    iput-object p1, p0, LX/AE6;->a:LX/AE7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1646015
    iget-object v0, p0, LX/AE6;->a:LX/AE7;

    iget-object v0, v0, LX/AE7;->c:LX/AEA;

    iget-object v0, v0, LX/AEA;->b:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 1646016
    iget-object v0, p0, LX/AE6;->a:LX/AE7;

    iget-object v0, v0, LX/AE7;->c:LX/AEA;

    iget-object v0, v0, LX/AEA;->l:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646017
    iget-object v0, p0, LX/AE6;->a:LX/AE7;

    iget-object v0, v0, LX/AE7;->c:LX/AEA;

    iget-object v1, v0, LX/AEA;->c:LX/0bH;

    new-instance v2, LX/1Ne;

    iget-object v0, p0, LX/AE6;->a:LX/AE7;

    iget-object v0, v0, LX/AE7;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646018
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1646019
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1646020
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1646013
    return-void
.end method
