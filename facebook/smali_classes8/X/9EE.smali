.class public final enum LX/9EE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9EE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9EE;

.field public static final enum AGGREGATE_ORIGINAL_POST:LX/9EE;

.field public static final enum AGGREGATE_RESHARE:LX/9EE;

.field public static final enum AGGREGATE_TOP:LX/9EE;

.field public static final enum NONE:LX/9EE;

.field public static final enum ORIGINAL_POST:LX/9EE;

.field public static final enum RESHARE_TOP:LX/9EE;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1456657
    new-instance v0, LX/9EE;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/9EE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9EE;->NONE:LX/9EE;

    .line 1456658
    new-instance v0, LX/9EE;

    const-string v1, "RESHARE_TOP"

    invoke-direct {v0, v1, v4}, LX/9EE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9EE;->RESHARE_TOP:LX/9EE;

    .line 1456659
    new-instance v0, LX/9EE;

    const-string v1, "ORIGINAL_POST"

    invoke-direct {v0, v1, v5}, LX/9EE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9EE;->ORIGINAL_POST:LX/9EE;

    .line 1456660
    new-instance v0, LX/9EE;

    const-string v1, "AGGREGATE_TOP"

    invoke-direct {v0, v1, v6}, LX/9EE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9EE;->AGGREGATE_TOP:LX/9EE;

    .line 1456661
    new-instance v0, LX/9EE;

    const-string v1, "AGGREGATE_RESHARE"

    invoke-direct {v0, v1, v7}, LX/9EE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9EE;->AGGREGATE_RESHARE:LX/9EE;

    .line 1456662
    new-instance v0, LX/9EE;

    const-string v1, "AGGREGATE_ORIGINAL_POST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/9EE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9EE;->AGGREGATE_ORIGINAL_POST:LX/9EE;

    .line 1456663
    const/4 v0, 0x6

    new-array v0, v0, [LX/9EE;

    sget-object v1, LX/9EE;->NONE:LX/9EE;

    aput-object v1, v0, v3

    sget-object v1, LX/9EE;->RESHARE_TOP:LX/9EE;

    aput-object v1, v0, v4

    sget-object v1, LX/9EE;->ORIGINAL_POST:LX/9EE;

    aput-object v1, v0, v5

    sget-object v1, LX/9EE;->AGGREGATE_TOP:LX/9EE;

    aput-object v1, v0, v6

    sget-object v1, LX/9EE;->AGGREGATE_RESHARE:LX/9EE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/9EE;->AGGREGATE_ORIGINAL_POST:LX/9EE;

    aput-object v2, v0, v1

    sput-object v0, LX/9EE;->$VALUES:[LX/9EE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1456664
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9EE;
    .locals 1

    .prologue
    .line 1456665
    const-class v0, LX/9EE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9EE;

    return-object v0
.end method

.method public static values()[LX/9EE;
    .locals 1

    .prologue
    .line 1456666
    sget-object v0, LX/9EE;->$VALUES:[LX/9EE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9EE;

    return-object v0
.end method
