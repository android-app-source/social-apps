.class public final LX/AO6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AOB;


# direct methods
.method public constructor <init>(LX/AOB;)V
    .locals 0

    .prologue
    .line 1668713
    iput-object p1, p0, LX/AO6;->a:LX/AOB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x2ef0109e

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1668714
    iget-object v1, p0, LX/AO6;->a:LX/AOB;

    .line 1668715
    new-instance v3, Landroid/widget/EditText;

    iget-object v4, v1, LX/AOB;->f:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1668716
    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1668717
    invoke-virtual {v3}, Landroid/widget/EditText;->setSingleLine()V

    .line 1668718
    const v4, 0x7f082771

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1668719
    new-instance v4, LX/0ju;

    iget-object p0, v1, LX/AOB;->f:Landroid/content/Context;

    invoke-direct {v4, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v4

    const p0, 0x7f082772

    new-instance p1, LX/AO8;

    invoke-direct {p1, v1, v3}, LX/AO8;-><init>(LX/AOB;Landroid/widget/EditText;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    const p0, 0x7f082773

    new-instance p1, LX/AO7;

    invoke-direct {p1, v1, v3}, LX/AO7;-><init>(LX/AOB;Landroid/widget/EditText;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    .line 1668720
    invoke-virtual {v4}, LX/0ju;->b()LX/2EJ;

    move-result-object v4

    .line 1668721
    new-instance p0, LX/AO9;

    invoke-direct {p0, v1, v3, v4}, LX/AO9;-><init>(LX/AOB;Landroid/widget/EditText;LX/2EJ;)V

    invoke-virtual {v3, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1668722
    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 1668723
    iget-object v3, v1, LX/AOB;->f:Landroid/content/Context;

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 1668724
    const/4 v4, 0x2

    const/4 p0, 0x0

    invoke-virtual {v3, v4, p0}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 1668725
    const v1, 0x7fc94b6f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
