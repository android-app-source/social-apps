.class public final LX/9fN;
.super LX/9fL;
.source ""


# instance fields
.field public final synthetic m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

.field private n:LX/5iG;

.field private o:LX/1aX;

.field private p:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1521797
    iput-object p1, p0, LX/9fN;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    .line 1521798
    invoke-direct {p0, p1, p2}, LX/9fL;-><init>(Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;Landroid/view/View;)V

    .line 1521799
    const v0, 0x7f0d0d17

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    iput-object v0, p0, LX/9fN;->p:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    .line 1521800
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 1521801
    invoke-super {p0, p1}, LX/9fL;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    .line 1521802
    iget-object v0, p0, LX/9fN;->p:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1521803
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/9fL;->b(Z)LX/1af;

    move-result-object v0

    iget-object v1, p0, LX/9fN;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, LX/9fN;->o:LX/1aX;

    .line 1521804
    iget-object v0, p0, LX/9fN;->o:LX/1aX;

    iget-object v1, p0, LX/9fN;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->j:Landroid/net/Uri;

    invoke-virtual {p0, v1, v8}, LX/9fL;->a(Landroid/net/Uri;LX/33B;)LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 1521805
    iget-object v0, p0, LX/9fN;->p:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    iget-object v1, p0, LX/9fN;->o:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1521806
    :cond_0
    new-instance v0, LX/5iG;

    .line 1521807
    iget-object v1, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1521808
    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1521809
    invoke-direct {v0, v8, v1, v3}, LX/5iG;-><init>(LX/1aX;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/9fN;->n:LX/5iG;

    .line 1521810
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1521811
    invoke-virtual {p0, v2}, LX/9fL;->b(Z)LX/1af;

    move-result-object v5

    iget-object v6, p0, LX/9fN;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v6, v6, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-static {v5, v6}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v5

    .line 1521812
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p0, v6, v8}, LX/9fL;->a(Landroid/net/Uri;LX/33B;)LX/1aZ;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1aX;->a(LX/1aZ;)V

    .line 1521813
    iget-object v6, p0, LX/9fN;->n:LX/5iG;

    .line 1521814
    iget-object v7, p0, LX/9fN;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v7, v7, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->e:LX/9f1;

    invoke-virtual {v7}, LX/9f1;->a()LX/9d5;

    move-result-object v7

    .line 1521815
    new-instance v9, LX/3rL;

    iget v10, v7, LX/9d5;->u:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget p1, v7, LX/9d5;->v:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v9, v10, p1}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v7, v9

    .line 1521816
    invoke-static {v0, v7}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/3rL;)Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    invoke-virtual {v6, v0, v5}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/1aX;)V

    .line 1521817
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1521818
    :cond_1
    iget-object v0, p0, LX/9fN;->p:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    iget-object v1, p0, LX/9fN;->n:LX/5iG;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setSwipeableItem(LX/5iG;)V

    .line 1521819
    return-void
.end method
