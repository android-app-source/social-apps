.class public final LX/AHX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AFM;

.field public final synthetic c:LX/AHZ;


# direct methods
.method public constructor <init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V
    .locals 0

    .prologue
    .line 1655701
    iput-object p1, p0, LX/AHX;->c:LX/AHZ;

    iput-object p2, p0, LX/AHX;->a:Ljava/lang/String;

    iput-object p3, p0, LX/AHX;->b:LX/AFM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1655702
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "requestReplyThreadV1 Reply result not available"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1655703
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1655704
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1655705
    if-eqz p1, :cond_0

    .line 1655706
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1655707
    if-nez v0, :cond_1

    .line 1655708
    :cond_0
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "requestReplyThreadV1 onSucess null result."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655709
    :goto_0
    return-void

    .line 1655710
    :cond_1
    new-instance v1, LX/AHe;

    iget-object v0, p0, LX/AHX;->c:LX/AHZ;

    iget-object v2, v0, LX/AHZ;->d:Ljava/lang/String;

    iget-object v3, p0, LX/AHX;->a:Ljava/lang/String;

    .line 1655711
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1655712
    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel;

    invoke-direct {v1, v2, v3, v0}, LX/AHe;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel;)V

    .line 1655713
    iget-object v0, p0, LX/AHX;->b:LX/AFM;

    invoke-virtual {v1}, LX/AHa;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AFM;->a(Lcom/facebook/audience/model/ReplyThread;)V

    goto :goto_0
.end method
