.class public final LX/9nL;
.super LX/5p8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5p8",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/react/bridge/Callback;

.field public final synthetic b:LX/5pC;

.field public final synthetic c:LX/9nO;


# direct methods
.method public constructor <init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V
    .locals 0

    .prologue
    .line 1537101
    iput-object p1, p0, LX/9nL;->c:LX/9nO;

    iput-object p3, p0, LX/9nL;->a:Lcom/facebook/react/bridge/Callback;

    iput-object p4, p0, LX/9nL;->b:LX/5pC;

    invoke-direct {p0, p2}, LX/5p8;-><init>(LX/5pX;)V

    return-void
.end method

.method private varargs a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1537103
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    invoke-static {v0}, LX/9nO;->h(LX/9nO;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1537104
    iget-object v0, p0, LX/9nL;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2}, LX/9nH;->c(Ljava/lang/String;)LX/5pH;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1537105
    :cond_0
    :goto_0
    return-void

    .line 1537106
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x3e3498f3

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v0, v3

    .line 1537107
    :goto_1
    iget-object v1, p0, LX/9nL;->b:LX/5pC;

    invoke-interface {v1}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 1537108
    iget-object v1, p0, LX/9nL;->b:LX/5pC;

    invoke-interface {v1, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v1

    invoke-interface {v1}, LX/5pC;->size()I

    move-result v1

    const/4 v4, 0x2

    if-eq v1, v4, :cond_2

    .line 1537109
    const/4 v0, 0x0

    invoke-static {v0}, LX/9nH;->b(Ljava/lang/String;)LX/5pH;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1537110
    :try_start_1
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, 0x12fb45b2

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1537111
    :catch_0
    move-exception v0

    .line 1537112
    const-string v3, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537113
    if-nez v1, :cond_0

    .line 1537114
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto :goto_0

    .line 1537115
    :cond_2
    :try_start_2
    iget-object v1, p0, LX/9nL;->b:LX/5pC;

    invoke-interface {v1, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v1

    const/4 v4, 0x0

    invoke-interface {v1, v4}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1537116
    const/4 v0, 0x0

    invoke-static {v0}, LX/9nH;->a(Ljava/lang/String;)LX/5pH;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 1537117
    :try_start_3
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, 0x1f02e3d2

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 1537118
    :catch_1
    move-exception v0

    .line 1537119
    const-string v3, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537120
    if-nez v1, :cond_0

    .line 1537121
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto/16 :goto_0

    .line 1537122
    :cond_3
    :try_start_4
    iget-object v1, p0, LX/9nL;->b:LX/5pC;

    invoke-interface {v1, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v1, v4}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 1537123
    const/4 v0, 0x0

    invoke-static {v0}, LX/9nH;->b(Ljava/lang/String;)LX/5pH;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    .line 1537124
    :try_start_5
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, 0x3a3e7035

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 1537125
    :catch_2
    move-exception v0

    .line 1537126
    const-string v3, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537127
    if-nez v1, :cond_0

    .line 1537128
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto/16 :goto_0

    .line 1537129
    :cond_4
    :try_start_6
    iget-object v1, p0, LX/9nL;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-object v4, p0, LX/9nL;->b:LX/5pC;

    invoke-interface {v4, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/9nL;->b:LX/5pC;

    invoke-interface {v5, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, LX/9nG;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1537130
    const/4 v0, 0x0

    invoke-static {v0}, LX/9nH;->c(Ljava/lang/String;)LX/5pH;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v1

    .line 1537131
    :try_start_7
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, -0x48609456

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_0

    .line 1537132
    :catch_3
    move-exception v0

    .line 1537133
    const-string v3, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537134
    if-nez v1, :cond_0

    .line 1537135
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto/16 :goto_0

    .line 1537136
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 1537137
    :cond_6
    :try_start_8
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1537138
    :try_start_9
    iget-object v0, p0, LX/9nL;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x2c09d027

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    move-object v0, v2

    .line 1537139
    :cond_7
    :goto_2
    if-eqz v0, :cond_8

    .line 1537140
    iget-object v1, p0, LX/9nL;->a:Lcom/facebook/react/bridge/Callback;

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1537141
    :catch_4
    move-exception v0

    .line 1537142
    const-string v1, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537143
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    goto :goto_2

    .line 1537144
    :catch_5
    move-exception v0

    .line 1537145
    :try_start_a
    const-string v1, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537146
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v0

    .line 1537147
    :try_start_b
    iget-object v1, p0, LX/9nL;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v4, 0x36ff2a8d

    invoke-static {v1, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    goto :goto_2

    .line 1537148
    :catch_6
    move-exception v1

    .line 1537149
    const-string v4, "React"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537150
    if-nez v0, :cond_7

    .line 1537151
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    goto :goto_2

    .line 1537152
    :catchall_0
    move-exception v0

    .line 1537153
    :try_start_c
    iget-object v1, p0, LX/9nL;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v3, 0x6a382f0f

    invoke-static {v1, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_7

    .line 1537154
    :goto_3
    throw v0

    .line 1537155
    :catch_7
    move-exception v1

    .line 1537156
    const-string v3, "React"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537157
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto :goto_3

    .line 1537158
    :cond_8
    iget-object v0, p0, LX/9nL;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1537102
    invoke-direct {p0}, LX/9nL;->a()V

    return-void
.end method
