.class public final enum LX/9jK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9jK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9jK;

.field public static final enum CLOSED:LX/9jK;

.field public static final enum DUPLICATE:LX/9jK;

.field public static final enum INFO_INCORRECT:LX/9jK;

.field public static final enum NOT_PUBLIC:LX/9jK;

.field public static final enum OFFENSIVE:LX/9jK;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1529735
    new-instance v0, LX/9jK;

    const-string v1, "INFO_INCORRECT"

    const-string v2, "info_incorrect"

    invoke-direct {v0, v1, v3, v2}, LX/9jK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jK;->INFO_INCORRECT:LX/9jK;

    .line 1529736
    new-instance v0, LX/9jK;

    const-string v1, "OFFENSIVE"

    const-string v2, "offensive"

    invoke-direct {v0, v1, v4, v2}, LX/9jK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jK;->OFFENSIVE:LX/9jK;

    .line 1529737
    new-instance v0, LX/9jK;

    const-string v1, "CLOSED"

    const-string v2, "closed"

    invoke-direct {v0, v1, v5, v2}, LX/9jK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jK;->CLOSED:LX/9jK;

    .line 1529738
    new-instance v0, LX/9jK;

    const-string v1, "DUPLICATE"

    const-string v2, "duplicate"

    invoke-direct {v0, v1, v6, v2}, LX/9jK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jK;->DUPLICATE:LX/9jK;

    .line 1529739
    new-instance v0, LX/9jK;

    const-string v1, "NOT_PUBLIC"

    const-string v2, "not_public"

    invoke-direct {v0, v1, v7, v2}, LX/9jK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jK;->NOT_PUBLIC:LX/9jK;

    .line 1529740
    const/4 v0, 0x5

    new-array v0, v0, [LX/9jK;

    sget-object v1, LX/9jK;->INFO_INCORRECT:LX/9jK;

    aput-object v1, v0, v3

    sget-object v1, LX/9jK;->OFFENSIVE:LX/9jK;

    aput-object v1, v0, v4

    sget-object v1, LX/9jK;->CLOSED:LX/9jK;

    aput-object v1, v0, v5

    sget-object v1, LX/9jK;->DUPLICATE:LX/9jK;

    aput-object v1, v0, v6

    sget-object v1, LX/9jK;->NOT_PUBLIC:LX/9jK;

    aput-object v1, v0, v7

    sput-object v0, LX/9jK;->$VALUES:[LX/9jK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1529741
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1529742
    iput-object p3, p0, LX/9jK;->value:Ljava/lang/String;

    .line 1529743
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9jK;
    .locals 1

    .prologue
    .line 1529744
    const-class v0, LX/9jK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9jK;

    return-object v0
.end method

.method public static values()[LX/9jK;
    .locals 1

    .prologue
    .line 1529745
    sget-object v0, LX/9jK;->$VALUES:[LX/9jK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9jK;

    return-object v0
.end method
