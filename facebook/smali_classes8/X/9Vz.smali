.class public final LX/9Vz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/widget/ListView;

.field public final synthetic b:Lcom/facebook/widget/CustomLinearLayout;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;Lcom/facebook/widget/CustomLinearLayout;)V
    .locals 0

    .prologue
    .line 1500482
    iput-object p1, p0, LX/9Vz;->a:Landroid/widget/ListView;

    iput-object p2, p0, LX/9Vz;->b:Lcom/facebook/widget/CustomLinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 1500470
    iget-object v0, p0, LX/9Vz;->a:Landroid/widget/ListView;

    .line 1500471
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_2

    .line 1500472
    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1500473
    :goto_0
    iget-object v0, p0, LX/9Vz;->b:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1500474
    iget-object v1, p0, LX/9Vz;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    .line 1500475
    iget-object v2, p0, LX/9Vz;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1500476
    :goto_1
    return-void

    .line 1500477
    :cond_0
    iget-object v2, p0, LX/9Vz;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v2, p0, LX/9Vz;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 1500478
    iget-object v1, p0, LX/9Vz;->b:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0217b9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1500479
    :goto_2
    iget-object v0, p0, LX/9Vz;->b:Lcom/facebook/widget/CustomLinearLayout;

    invoke-static {v0}, LX/9W0;->a(Lcom/facebook/widget/CustomLinearLayout;)V

    goto :goto_1

    .line 1500480
    :cond_1
    iget-object v1, p0, LX/9Vz;->b:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, 0x106000d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1500481
    :cond_2
    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
