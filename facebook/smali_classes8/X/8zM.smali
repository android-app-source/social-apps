.class public final LX/8zM;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

.field private b:LX/8zD;

.field private c:LX/8z9;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zD;LX/8z9;)V
    .locals 0

    .prologue
    .line 1427161
    iput-object p1, p0, LX/8zM;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 1427162
    iput-object p2, p0, LX/8zM;->b:LX/8zD;

    .line 1427163
    iput-object p3, p0, LX/8zM;->c:LX/8z9;

    .line 1427164
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1427169
    iget-object v0, p0, LX/8zM;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v0, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->l:LX/8zL;

    if-nez v0, :cond_0

    .line 1427170
    :goto_0
    return-void

    .line 1427171
    :cond_0
    iget-object v0, p0, LX/8zM;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-static {v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->b(Lcom/facebook/composer/metatext/TagsTextViewContainer;)V

    .line 1427172
    iget-object v0, p0, LX/8zM;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v1, p0, LX/8zM;->b:LX/8zD;

    iget-object v2, p0, LX/8zM;->c:LX/8z9;

    const/4 v3, 0x0

    .line 1427173
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a$redex0(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zD;LX/8z9;I)V

    .line 1427174
    iget-object v0, p0, LX/8zM;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v0, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->l:LX/8zL;

    invoke-interface {v0}, LX/8zL;->a()V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1427165
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1427166
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1427167
    iget-object v0, p0, LX/8zM;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1427168
    return-void
.end method
