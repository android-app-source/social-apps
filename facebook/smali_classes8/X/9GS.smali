.class public final LX/9GS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9GT;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/9FA;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;LX/9GT;Ljava/lang/String;LX/9FA;)V
    .locals 0

    .prologue
    .line 1459933
    iput-object p1, p0, LX/9GS;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    iput-object p2, p0, LX/9GS;->a:LX/9GT;

    iput-object p3, p0, LX/9GS;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9GS;->c:LX/9FA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x7a2a9205

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1459934
    iget-object v0, p0, LX/9GS;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    iget-object v2, p0, LX/9GS;->a:LX/9GT;

    iget-object v2, v2, LX/9GT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v3, p0, LX/9GS;->a:LX/9GT;

    iget-object v3, v3, LX/9GT;->b:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/9GS;->b:Ljava/lang/String;

    .line 1459935
    if-nez v2, :cond_4

    .line 1459936
    const/4 v5, 0x0

    .line 1459937
    :goto_0
    move-object v2, v5

    .line 1459938
    iget-object v0, p0, LX/9GS;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    iget-object v3, p0, LX/9GS;->a:LX/9GT;

    iget-object v3, v3, LX/9GT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    iget-object v4, p0, LX/9GS;->a:LX/9GT;

    iget-object v4, v4, LX/9GT;->b:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/9GS;->b:Ljava/lang/String;

    .line 1459939
    if-nez v3, :cond_5

    .line 1459940
    const/4 v6, 0x0

    .line 1459941
    :goto_1
    move-object v3, v6

    .line 1459942
    iget-object v0, p0, LX/9GS;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    iget-object v4, v0, Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;->b:LX/8y0;

    iget-object v0, p0, LX/9GS;->a:LX/9GT;

    iget-object v5, v0, LX/9GT;->b:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-eqz v3, :cond_0

    iget-object v0, v3, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    :goto_2
    new-instance v6, LX/9GR;

    invoke-direct {v6, p0}, LX/9GR;-><init>(LX/9GS;)V

    .line 1459943
    new-instance v7, LX/4IJ;

    invoke-direct {v7}, LX/4IJ;-><init>()V

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v8

    .line 1459944
    const-string v9, "lightweight_rec_id"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459945
    move-object v7, v7

    .line 1459946
    invoke-static {}, LX/5HL;->c()LX/5HK;

    move-result-object v8

    .line 1459947
    const-string v9, "input"

    invoke-virtual {v8, v9, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1459948
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v0}, LX/8y0;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v7

    .line 1459949
    iget-object v8, v4, LX/8y0;->b:LX/1Ck;

    const-string v9, "place_list_remove_lightweight_rec"

    iget-object p1, v4, LX/8y0;->a:LX/0tX;

    invoke-virtual {p1, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v6}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object p1

    invoke-virtual {v8, v9, v7, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1459950
    iget-object v0, p0, LX/9GS;->c:LX/9FA;

    .line 1459951
    iget-boolean v4, v0, LX/9FA;->c:Z

    move v0, v4

    .line 1459952
    if-eqz v0, :cond_2

    .line 1459953
    if-nez v2, :cond_1

    .line 1459954
    const v0, -0x3d70b210

    invoke-static {v0, v1}, LX/02F;->a(II)V

    .line 1459955
    :goto_3
    return-void

    .line 1459956
    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 1459957
    :cond_1
    invoke-static {v2}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1459958
    iget-object v2, p0, LX/9GS;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;->e:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1459959
    :goto_4
    const v0, -0x59d3686e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_3

    .line 1459960
    :cond_2
    if-nez v3, :cond_3

    .line 1459961
    const v0, -0x33dcedd4    # -4.274808E7f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_3

    .line 1459962
    :cond_3
    iget-object v0, p0, LX/9GS;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;->f:LX/1K9;

    new-instance v2, LX/8q4;

    iget-object v4, v3, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, v3, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v4, v3}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_4

    :cond_4
    iget-object v5, v0, Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;->c:LX/189;

    invoke-virtual {v5, v2, v3, v4}, LX/189;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    goto/16 :goto_0

    :cond_5
    iget-object v6, v0, Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;->d:LX/20j;

    invoke-virtual {v6, v3, v5, v4}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;)LX/6PS;

    move-result-object v6

    goto/16 :goto_1
.end method
