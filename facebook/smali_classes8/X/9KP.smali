.class public final LX/9KP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1467925
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1467926
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467927
    :goto_0
    return v1

    .line 1467928
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467929
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1467930
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1467931
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1467932
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1467933
    const-string v4, "attachments"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1467934
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1467935
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1467936
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1467937
    invoke-static {p0, p1}, LX/9KO;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1467938
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1467939
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1467940
    goto :goto_1

    .line 1467941
    :cond_3
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1467942
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1467943
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1467944
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1467945
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1467946
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1467910
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1467911
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1467912
    if-eqz v0, :cond_1

    .line 1467913
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467914
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1467915
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1467916
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/9KO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1467917
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1467918
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1467919
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1467920
    if-eqz v0, :cond_2

    .line 1467921
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467922
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1467923
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1467924
    return-void
.end method
