.class public final LX/9q3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 1545882
    const/16 v17, 0x0

    .line 1545883
    const/16 v16, 0x0

    .line 1545884
    const/4 v15, 0x0

    .line 1545885
    const/4 v14, 0x0

    .line 1545886
    const/4 v13, 0x0

    .line 1545887
    const/4 v12, 0x0

    .line 1545888
    const/4 v9, 0x0

    .line 1545889
    const-wide/16 v10, 0x0

    .line 1545890
    const/4 v8, 0x0

    .line 1545891
    const/4 v7, 0x0

    .line 1545892
    const/4 v6, 0x0

    .line 1545893
    const/4 v5, 0x0

    .line 1545894
    const/4 v4, 0x0

    .line 1545895
    const/4 v3, 0x0

    .line 1545896
    const/4 v2, 0x0

    .line 1545897
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 1545898
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1545899
    const/4 v2, 0x0

    .line 1545900
    :goto_0
    return v2

    .line 1545901
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    if-eq v2, v0, :cond_c

    .line 1545902
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1545903
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1545904
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1545905
    const-string v19, "broadcast_network"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 1545906
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1545907
    :cond_1
    const-string v19, "clock"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1545908
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1545909
    :cond_2
    const-string v19, "first_team_object"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1545910
    invoke-static/range {p0 .. p1}, LX/9px;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1545911
    :cond_3
    const-string v19, "first_team_score"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1545912
    const/4 v2, 0x1

    .line 1545913
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v15, v7

    move v7, v2

    goto :goto_1

    .line 1545914
    :cond_4
    const-string v19, "has_match_started"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1545915
    const/4 v2, 0x1

    .line 1545916
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v14, v6

    move v6, v2

    goto/16 :goto_1

    .line 1545917
    :cond_5
    const-string v19, "id"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1545918
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1545919
    :cond_6
    const-string v19, "match_page"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1545920
    invoke-static/range {p0 .. p1}, LX/9py;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1545921
    :cond_7
    const-string v19, "scheduled_start_time"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1545922
    const/4 v2, 0x1

    .line 1545923
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1545924
    :cond_8
    const-string v19, "second_team_object"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1545925
    invoke-static/range {p0 .. p1}, LX/9q1;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1545926
    :cond_9
    const-string v19, "second_team_score"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1545927
    const/4 v2, 0x1

    .line 1545928
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move v10, v8

    move v8, v2

    goto/16 :goto_1

    .line 1545929
    :cond_a
    const-string v19, "winning_team"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1545930
    invoke-static/range {p0 .. p1}, LX/9q2;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1545931
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1545932
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1545933
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1545934
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1545935
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1545936
    if-eqz v7, :cond_d

    .line 1545937
    const/4 v2, 0x3

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v7}, LX/186;->a(III)V

    .line 1545938
    :cond_d
    if-eqz v6, :cond_e

    .line 1545939
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->a(IZ)V

    .line 1545940
    :cond_e
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1545941
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1545942
    if-eqz v3, :cond_f

    .line 1545943
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1545944
    :cond_f
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1545945
    if-eqz v8, :cond_10

    .line 1545946
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10, v3}, LX/186;->a(III)V

    .line 1545947
    :cond_10
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1545948
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v9

    move v9, v6

    move v6, v4

    move/from16 v21, v7

    move v7, v5

    move-wide v4, v10

    move/from16 v10, v21

    move v11, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1545949
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1545950
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1545951
    if-eqz v0, :cond_0

    .line 1545952
    const-string v1, "broadcast_network"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545953
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1545954
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1545955
    if-eqz v0, :cond_1

    .line 1545956
    const-string v1, "clock"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545957
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1545958
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545959
    if-eqz v0, :cond_2

    .line 1545960
    const-string v1, "first_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545961
    invoke-static {p0, v0, p2, p3}, LX/9px;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545962
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1545963
    if-eqz v0, :cond_3

    .line 1545964
    const-string v1, "first_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545965
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1545966
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1545967
    if-eqz v0, :cond_4

    .line 1545968
    const-string v1, "has_match_started"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545969
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1545970
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1545971
    if-eqz v0, :cond_5

    .line 1545972
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545973
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1545974
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545975
    if-eqz v0, :cond_6

    .line 1545976
    const-string v1, "match_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545977
    invoke-static {p0, v0, p2}, LX/9py;->a(LX/15i;ILX/0nX;)V

    .line 1545978
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1545979
    cmp-long v2, v0, v4

    if-eqz v2, :cond_7

    .line 1545980
    const-string v2, "scheduled_start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545981
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1545982
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545983
    if-eqz v0, :cond_8

    .line 1545984
    const-string v1, "second_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545985
    invoke-static {p0, v0, p2, p3}, LX/9q1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545986
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1545987
    if-eqz v0, :cond_9

    .line 1545988
    const-string v1, "second_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545989
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1545990
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545991
    if-eqz v0, :cond_a

    .line 1545992
    const-string v1, "winning_team"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545993
    invoke-static {p0, v0, p2}, LX/9q2;->a(LX/15i;ILX/0nX;)V

    .line 1545994
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1545995
    return-void
.end method
