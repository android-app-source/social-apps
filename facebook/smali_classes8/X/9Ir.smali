.class public LX/9Ir;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1463752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463753
    return-void
.end method

.method public static a(LX/1PT;)LX/21D;
    .locals 1

    .prologue
    .line 1463755
    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    invoke-static {v0}, LX/9Ir;->a(LX/1Qt;)LX/21D;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1Qt;)LX/21D;
    .locals 3

    .prologue
    .line 1463756
    sget-object v0, LX/9Iq;->a:[I

    invoke-virtual {p0}, LX/1Qt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1463757
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No ComposerSourceSurface for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1463758
    :pswitch_0
    sget-object v0, LX/21D;->NEWSFEED:LX/21D;

    .line 1463759
    :goto_0
    return-object v0

    .line 1463760
    :pswitch_1
    sget-object v0, LX/21D;->GOOD_FRIENDS:LX/21D;

    goto :goto_0

    .line 1463761
    :pswitch_2
    sget-object v0, LX/21D;->PERMALINK:LX/21D;

    goto :goto_0

    .line 1463762
    :pswitch_3
    sget-object v0, LX/21D;->TIMELINE:LX/21D;

    goto :goto_0

    .line 1463763
    :pswitch_4
    sget-object v0, LX/21D;->PAGE_FEED:LX/21D;

    goto :goto_0

    .line 1463764
    :pswitch_5
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    goto :goto_0

    .line 1463765
    :pswitch_6
    sget-object v0, LX/21D;->EVENT:LX/21D;

    goto :goto_0

    .line 1463766
    :pswitch_7
    sget-object v0, LX/21D;->SEARCH:LX/21D;

    goto :goto_0

    .line 1463767
    :pswitch_8
    sget-object v0, LX/21D;->ON_THIS_DAY_FEED:LX/21D;

    goto :goto_0

    .line 1463768
    :pswitch_9
    sget-object v0, LX/21D;->REACTION:LX/21D;

    goto :goto_0

    .line 1463769
    :pswitch_a
    sget-object v0, LX/21D;->PHOTOS_FEED:LX/21D;

    goto :goto_0

    .line 1463770
    :pswitch_b
    sget-object v0, LX/21D;->VIDEO_HOME:LX/21D;

    goto :goto_0

    .line 1463771
    :pswitch_c
    sget-object v0, LX/21D;->USER_REVIEWS_LIST:LX/21D;

    goto :goto_0

    .line 1463772
    :pswitch_d
    sget-object v0, LX/21D;->FUNDRAISER_PAGE:LX/21D;

    goto :goto_0

    .line 1463773
    :pswitch_e
    sget-object v0, LX/21D;->STORY_GALLERY_SURVEY:LX/21D;

    goto :goto_0

    .line 1463774
    :pswitch_f
    sget-object v0, LX/21D;->ELECTION_HUB:LX/21D;

    goto :goto_0

    .line 1463775
    :pswitch_10
    sget-object v0, LX/21D;->CURATED_COLLECTION:LX/21D;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public static b(LX/1PT;)LX/21D;
    .locals 1

    .prologue
    .line 1463754
    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    invoke-static {v0}, LX/9Ir;->a(LX/1Qt;)LX/21D;

    move-result-object v0

    return-object v0
.end method
