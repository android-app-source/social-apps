.class public final LX/9am;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/93q;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V
    .locals 0

    .prologue
    .line 1513984
    iput-object p1, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1513985
    if-nez p2, :cond_1

    iget-boolean v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v0, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->r:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_1

    .line 1513986
    iget-object v0, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    new-instance v1, LX/7lP;

    invoke-direct {v1, p1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    iget-object v2, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v2, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->r:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v1, v2}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v1

    invoke-virtual {v1}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    .line 1513987
    iput-object v1, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->q:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1513988
    iget-object v0, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v0, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->x:Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    iget-object v1, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v1, v1, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->q:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-virtual {v0, v3, v1}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1513989
    :cond_0
    :goto_0
    return-void

    .line 1513990
    :cond_1
    iget-object v0, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v0, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->x:Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    invoke-virtual {v0, v3, p1}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1513991
    iget-object v0, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    .line 1513992
    iput-object p1, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->q:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1513993
    if-eqz p2, :cond_0

    .line 1513994
    iget-object v0, p0, LX/9am;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v1, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1513995
    iput-object v1, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->r:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1513996
    goto :goto_0
.end method
