.class public final LX/AG2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 1651048
    const/16 v18, 0x0

    .line 1651049
    const-wide/16 v16, 0x0

    .line 1651050
    const/4 v15, 0x0

    .line 1651051
    const/4 v14, 0x0

    .line 1651052
    const/4 v13, 0x0

    .line 1651053
    const/4 v12, 0x0

    .line 1651054
    const/4 v11, 0x0

    .line 1651055
    const/4 v10, 0x0

    .line 1651056
    const/4 v9, 0x0

    .line 1651057
    const/4 v8, 0x0

    .line 1651058
    const/4 v7, 0x0

    .line 1651059
    const/4 v6, 0x0

    .line 1651060
    const/4 v5, 0x0

    .line 1651061
    const/4 v4, 0x0

    .line 1651062
    const/4 v3, 0x0

    .line 1651063
    const/4 v2, 0x0

    .line 1651064
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_13

    .line 1651065
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1651066
    const/4 v2, 0x0

    .line 1651067
    :goto_0
    return v2

    .line 1651068
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 1651069
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1651070
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1651071
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1651072
    const-string v6, "__type__"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "__typename"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1651073
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1651074
    :cond_2
    const-string v6, "created_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1651075
    const/4 v2, 0x1

    .line 1651076
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1651077
    :cond_3
    const-string v6, "height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1651078
    const/4 v2, 0x1

    .line 1651079
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v11, v2

    move/from16 v20, v6

    goto :goto_1

    .line 1651080
    :cond_4
    const-string v6, "image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1651081
    invoke-static/range {p0 .. p1}, LX/AFy;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1651082
    :cond_5
    const-string v6, "lowres"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1651083
    invoke-static/range {p0 .. p1}, LX/AFz;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1651084
    :cond_6
    const-string v6, "message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1651085
    invoke-static/range {p0 .. p1}, LX/AG0;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1651086
    :cond_7
    const-string v6, "owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1651087
    invoke-static/range {p0 .. p1}, LX/AG1;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1651088
    :cond_8
    const-string v6, "playable_duration"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1651089
    const/4 v2, 0x1

    .line 1651090
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v10, v2

    move v15, v6

    goto/16 :goto_1

    .line 1651091
    :cond_9
    const-string v6, "playable_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1651092
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1651093
    :cond_a
    const-string v6, "video_full_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1651094
    const/4 v2, 0x1

    .line 1651095
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move v13, v6

    goto/16 :goto_1

    .line 1651096
    :cond_b
    const-string v6, "width"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1651097
    const/4 v2, 0x1

    .line 1651098
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v12, v6

    goto/16 :goto_1

    .line 1651099
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1651100
    :cond_d
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1651101
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1651102
    if-eqz v3, :cond_e

    .line 1651103
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1651104
    :cond_e
    if-eqz v11, :cond_f

    .line 1651105
    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1651106
    :cond_f
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1651107
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1651108
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1651109
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1651110
    if-eqz v10, :cond_10

    .line 1651111
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 1651112
    :cond_10
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1651113
    if-eqz v9, :cond_11

    .line 1651114
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, LX/186;->a(III)V

    .line 1651115
    :cond_11
    if-eqz v8, :cond_12

    .line 1651116
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v3}, LX/186;->a(III)V

    .line 1651117
    :cond_12
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_13
    move/from16 v19, v14

    move/from16 v20, v15

    move v14, v9

    move v15, v10

    move v10, v4

    move v9, v3

    move v3, v6

    move/from16 v22, v8

    move v8, v2

    move/from16 v23, v5

    move-wide/from16 v4, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v12, v7

    move/from16 v11, v23

    move/from16 v7, v18

    move/from16 v18, v13

    move/from16 v13, v22

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1651118
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651119
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1651120
    if-eqz v0, :cond_0

    .line 1651121
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651122
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1651123
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1651124
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 1651125
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651126
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1651127
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1651128
    if-eqz v0, :cond_2

    .line 1651129
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651130
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1651131
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651132
    if-eqz v0, :cond_6

    .line 1651133
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651134
    const/4 v4, 0x0

    .line 1651135
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651136
    invoke-virtual {p0, v0, v4, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1651137
    if-eqz v1, :cond_3

    .line 1651138
    const-string v2, "height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651139
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1651140
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1651141
    if-eqz v1, :cond_4

    .line 1651142
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651143
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651144
    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1651145
    if-eqz v1, :cond_5

    .line 1651146
    const-string v2, "width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651147
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1651148
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651149
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651150
    if-eqz v0, :cond_a

    .line 1651151
    const-string v1, "lowres"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651152
    const/4 v4, 0x0

    .line 1651153
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651154
    invoke-virtual {p0, v0, v4, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1651155
    if-eqz v1, :cond_7

    .line 1651156
    const-string v2, "height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651157
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1651158
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1651159
    if-eqz v1, :cond_8

    .line 1651160
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651161
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651162
    :cond_8
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1651163
    if-eqz v1, :cond_9

    .line 1651164
    const-string v2, "width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651165
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1651166
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651167
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651168
    if-eqz v0, :cond_b

    .line 1651169
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651170
    invoke-static {p0, v0, p2}, LX/AG0;->a(LX/15i;ILX/0nX;)V

    .line 1651171
    :cond_b
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651172
    if-eqz v0, :cond_c

    .line 1651173
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651174
    invoke-static {p0, v0, p2, p3}, LX/AG1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1651175
    :cond_c
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1651176
    if-eqz v0, :cond_d

    .line 1651177
    const-string v1, "playable_duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651178
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1651179
    :cond_d
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651180
    if-eqz v0, :cond_e

    .line 1651181
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651182
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651183
    :cond_e
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1651184
    if-eqz v0, :cond_f

    .line 1651185
    const-string v1, "video_full_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651186
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1651187
    :cond_f
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1651188
    if-eqz v0, :cond_10

    .line 1651189
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651190
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1651191
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651192
    return-void
.end method
