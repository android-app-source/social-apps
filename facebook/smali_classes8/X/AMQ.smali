.class public final LX/AMQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AML;


# instance fields
.field public final synthetic a:LX/0Pz;

.field public final synthetic b:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final synthetic c:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final synthetic d:LX/ANr;

.field public final synthetic e:LX/AMR;


# direct methods
.method public constructor <init>(LX/AMR;LX/0Pz;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/ANr;)V
    .locals 0

    .prologue
    .line 1666170
    iput-object p1, p0, LX/AMQ;->e:LX/AMR;

    iput-object p2, p0, LX/AMQ;->a:LX/0Pz;

    iput-object p3, p0, LX/AMQ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p4, p0, LX/AMQ;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p5, p0, LX/AMQ;->d:LX/ANr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/AMT;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 1666171
    iget-object v0, p1, LX/AMT;->b:LX/AMS;

    move-object v0, v0

    .line 1666172
    sget-object v1, LX/AMS;->EFFECT:LX/AMS;

    if-ne v0, v1, :cond_0

    .line 1666173
    iget-object v0, p1, LX/AMT;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1666174
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 1666175
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, LX/AN3;

    invoke-direct {v2, v0, v1}, LX/AN3;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    move-object v0, v2

    .line 1666176
    if-eqz v0, :cond_0

    .line 1666177
    iget-object v1, p0, LX/AMQ;->a:LX/0Pz;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1666178
    :cond_0
    iget-object v0, p0, LX/AMQ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, LX/AMQ;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, LX/AMQ;->a:LX/0Pz;

    iget-object v3, p0, LX/AMQ;->d:LX/ANr;

    invoke-static {v0, v1, v2, v3}, LX/AMR;->b(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/0Pz;LX/ANr;)V

    .line 1666179
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1666180
    sget-object v0, LX/AMR;->a:Ljava/lang/String;

    const-string v1, "ShaderFilter asset download failed."

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1666181
    iget-object v0, p0, LX/AMQ;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1666182
    iget-object v0, p0, LX/AMQ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, LX/AMQ;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, LX/AMQ;->a:LX/0Pz;

    iget-object v3, p0, LX/AMQ;->d:LX/ANr;

    invoke-static {v0, v1, v2, v3}, LX/AMR;->b(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/0Pz;LX/ANr;)V

    .line 1666183
    return-void
.end method
