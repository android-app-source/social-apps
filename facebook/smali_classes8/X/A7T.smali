.class public LX/A7T;
.super LX/998;
.source ""

# interfaces
.implements LX/4Ba;
.implements LX/39D;
.implements LX/39E;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/1wz;

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z

.field public g:I

.field public h:I

.field public i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/drawers/DrawerStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0fR;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/drawers/DrawerDragListener;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field private o:Z

.field public final p:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1626180
    const-class v0, LX/A7T;

    sput-object v0, LX/A7T;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1626181
    invoke-direct {p0, p1}, LX/998;-><init>(Landroid/content/Context;)V

    .line 1626182
    iput v1, p0, LX/A7T;->c:I

    .line 1626183
    iput v1, p0, LX/A7T;->d:I

    .line 1626184
    iput-boolean v2, p0, LX/A7T;->e:Z

    .line 1626185
    iput-boolean v2, p0, LX/A7T;->f:Z

    .line 1626186
    iput v1, p0, LX/A7T;->g:I

    .line 1626187
    iput v1, p0, LX/A7T;->h:I

    .line 1626188
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/A7T;->i:Ljava/util/Set;

    .line 1626189
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/A7T;->j:Ljava/util/Set;

    .line 1626190
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/A7T;->k:Ljava/util/Set;

    .line 1626191
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/A7T;->l:Ljava/util/Set;

    .line 1626192
    iput-boolean v1, p0, LX/A7T;->m:Z

    .line 1626193
    iput-boolean v1, p0, LX/A7T;->n:Z

    .line 1626194
    iput-boolean v2, p0, LX/A7T;->o:Z

    .line 1626195
    new-instance v0, Lcom/facebook/ui/drawers/DrawerDraggableContentLayout$1;

    invoke-direct {v0, p0}, Lcom/facebook/ui/drawers/DrawerDraggableContentLayout$1;-><init>(LX/A7T;)V

    iput-object v0, p0, LX/A7T;->p:Ljava/lang/Runnable;

    .line 1626196
    const-class v0, LX/A7T;

    invoke-static {v0, p0}, LX/A7T;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1626197
    iget-object v0, p0, LX/A7T;->b:LX/1wz;

    const/4 v1, 0x2

    new-array v1, v1, [LX/31M;

    const/4 v2, 0x0

    sget-object p1, LX/31M;->LEFT:LX/31M;

    aput-object p1, v1, v2

    const/4 v2, 0x1

    sget-object p1, LX/31M;->RIGHT:LX/31M;

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LX/1wz;->a([LX/31M;)V

    .line 1626198
    iget-object v0, p0, LX/A7T;->b:LX/1wz;

    .line 1626199
    iput-object p0, v0, LX/1wz;->q:LX/4Ba;

    .line 1626200
    iget-object v0, p0, LX/A7T;->b:LX/1wz;

    .line 1626201
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 1626202
    iget-object v0, p0, LX/A7T;->b:LX/1wz;

    .line 1626203
    iput-object p0, v0, LX/1wz;->s:LX/39E;

    .line 1626204
    return-void
.end method

.method private a(LX/10e;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1626205
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v1

    .line 1626206
    sget-object v2, LX/A7S;->a:[I

    invoke-virtual {p1}, LX/10e;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v1, v0

    .line 1626207
    :goto_0
    iget-object v0, p0, LX/A7T;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fP;

    .line 1626208
    const/high16 p0, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1626209
    invoke-static {v0, p1, v6}, LX/0fP;->b(LX/0fP;LX/10e;Z)V

    .line 1626210
    invoke-static {v0, v5}, LX/0fP;->g(LX/0fP;Z)V

    .line 1626211
    invoke-static {v0, v5}, LX/0fP;->h(LX/0fP;Z)V

    .line 1626212
    sget-object v3, LX/A7M;->b:[I

    invoke-virtual {p1}, LX/10e;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 1626213
    :cond_0
    :goto_2
    goto :goto_1

    .line 1626214
    :pswitch_0
    iget v2, p0, LX/A7T;->d:I

    if-nez v2, :cond_1

    :goto_3
    move v1, v0

    .line 1626215
    goto :goto_0

    .line 1626216
    :cond_1
    int-to-float v0, v1

    neg-float v0, v0

    iget v1, p0, LX/A7T;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_3

    .line 1626217
    :pswitch_1
    iget v2, p0, LX/A7T;->c:I

    if-nez v2, :cond_2

    :goto_4
    move v1, v0

    goto :goto_0

    :cond_2
    int-to-float v0, v1

    iget v1, p0, LX/A7T;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_4

    .line 1626218
    :cond_3
    return-void

    .line 1626219
    :pswitch_2
    invoke-static {v0, v5}, LX/0fP;->e(LX/0fP;Z)V

    .line 1626220
    invoke-static {v0, v5}, LX/0fP;->f(LX/0fP;Z)V

    goto :goto_2

    .line 1626221
    :pswitch_3
    invoke-static {v0, v6}, LX/0fP;->e(LX/0fP;Z)V

    .line 1626222
    invoke-static {v0, v5}, LX/0fP;->f(LX/0fP;Z)V

    .line 1626223
    iget-object v3, v0, LX/0fP;->j:LX/A7O;

    if-eqz v3, :cond_0

    .line 1626224
    iget-object v3, v0, LX/0fP;->j:LX/A7O;

    invoke-static {v3, v6}, LX/A7O;->e(LX/A7O;Z)V

    .line 1626225
    iget-object v3, v0, LX/0fP;->j:LX/A7O;

    sub-float v4, p0, v1

    invoke-static {v3, v4}, LX/A7O;->a$redex0(LX/A7O;F)V

    goto :goto_2

    .line 1626226
    :pswitch_4
    invoke-static {v0, v5}, LX/0fP;->e(LX/0fP;Z)V

    .line 1626227
    invoke-static {v0, v6}, LX/0fP;->f(LX/0fP;Z)V

    .line 1626228
    iget-object v3, v0, LX/0fP;->k:LX/A7O;

    if-eqz v3, :cond_0

    .line 1626229
    iget-object v3, v0, LX/0fP;->k:LX/A7O;

    invoke-static {v3, v6}, LX/A7O;->e(LX/A7O;Z)V

    .line 1626230
    iget-object v3, v0, LX/0fP;->k:LX/A7O;

    sub-float v4, p0, v1

    invoke-static {v3, v4}, LX/A7O;->a$redex0(LX/A7O;F)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method private a(LX/10e;LX/31M;)V
    .locals 3
    .param p2    # LX/31M;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1626231
    sget-object v0, LX/10e;->CLOSED:LX/10e;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1626232
    sget-object v0, LX/A7S;->a:[I

    invoke-virtual {p1}, LX/10e;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1626233
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1626234
    goto :goto_0

    .line 1626235
    :pswitch_0
    iput v1, p0, LX/A7T;->h:I

    .line 1626236
    iget v0, p0, LX/A7T;->d:I

    neg-int v1, v0

    .line 1626237
    :cond_2
    :goto_2
    iput v1, p0, LX/A7T;->g:I

    goto :goto_1

    .line 1626238
    :pswitch_1
    iget v0, p0, LX/A7T;->c:I

    iput v0, p0, LX/A7T;->h:I

    goto :goto_2

    .line 1626239
    :pswitch_2
    sget-object v0, LX/A7S;->b:[I

    invoke-virtual {p2}, LX/31M;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 1626240
    :pswitch_3
    invoke-direct {p0}, LX/A7T;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, LX/A7T;->c:I

    :goto_3
    iput v0, p0, LX/A7T;->h:I

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    .line 1626241
    :pswitch_4
    iput v1, p0, LX/A7T;->h:I

    .line 1626242
    invoke-direct {p0}, LX/A7T;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, LX/A7T;->d:I

    neg-int v1, v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/A7T;

    invoke-static {p0}, LX/1wx;->b(LX/0QB;)LX/1wz;

    move-result-object p0

    check-cast p0, LX/1wz;

    iput-object p0, p1, LX/A7T;->b:LX/1wz;

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 1626243
    iget-object v0, p0, LX/A7T;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fR;

    .line 1626244
    if-eqz v3, :cond_0

    invoke-interface {v0}, LX/0fR;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v2

    .line 1626245
    :goto_1
    if-eqz v1, :cond_1

    invoke-interface {v0, p1}, LX/0fR;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_2
    move v1, v0

    .line 1626246
    goto :goto_0

    :cond_0
    move v3, v4

    .line 1626247
    goto :goto_1

    :cond_1
    move v0, v4

    .line 1626248
    goto :goto_2

    .line 1626249
    :cond_2
    if-nez v3, :cond_3

    move v0, v2

    :goto_3
    iput-boolean v0, p0, LX/A7T;->m:Z

    .line 1626250
    if-nez v1, :cond_4

    :goto_4
    iput-boolean v2, p0, LX/A7T;->n:Z

    .line 1626251
    return-void

    :cond_3
    move v0, v4

    .line 1626252
    goto :goto_3

    :cond_4
    move v2, v4

    .line 1626253
    goto :goto_4
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 1626254
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/A7T;->scrollTo(II)V

    .line 1626255
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1626256
    invoke-virtual {p0}, LX/998;->g()Z

    .line 1626257
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v1

    .line 1626258
    const/4 v0, 0x0

    .line 1626259
    if-eqz v1, :cond_0

    .line 1626260
    iget v0, p0, LX/A7T;->h:I

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1626261
    iget v2, p0, LX/A7T;->g:I

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 1626262
    if-ge v0, v2, :cond_2

    iget v0, p0, LX/A7T;->h:I

    .line 1626263
    :cond_0
    :goto_0
    if-eq v0, v1, :cond_4

    .line 1626264
    if-eqz p1, :cond_3

    .line 1626265
    invoke-direct {p0, v0}, LX/A7T;->e(I)V

    .line 1626266
    :cond_1
    :goto_1
    return-void

    .line 1626267
    :cond_2
    iget v0, p0, LX/A7T;->g:I

    goto :goto_0

    .line 1626268
    :cond_3
    invoke-direct {p0, v0}, LX/A7T;->b(I)V

    goto :goto_1

    .line 1626269
    :cond_4
    if-eqz p1, :cond_1

    .line 1626270
    invoke-static {p0}, LX/A7T;->l(LX/A7T;)V

    goto :goto_1
.end method

.method private b(II)Z
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1626273
    iget-object v0, p0, LX/A7T;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1626274
    :goto_0
    return v0

    .line 1626275
    :cond_0
    new-array v3, v4, [I

    .line 1626276
    invoke-virtual {p0, v3}, LX/A7T;->getLocationInWindow([I)V

    .line 1626277
    new-array v4, v4, [I

    .line 1626278
    iget-object v0, p0, LX/A7T;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1626279
    invoke-virtual {v0, v4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1626280
    aget v6, v4, v1

    aget v7, v3, v1

    sub-int/2addr v6, v7

    .line 1626281
    aget v7, v4, v2

    aget v8, v3, v2

    sub-int/2addr v7, v8

    .line 1626282
    new-instance v8, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v9, v6

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v7

    invoke-direct {v8, v6, v7, v9, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1626283
    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 1626284
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1626285
    goto :goto_0
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 1626271
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/998;->a(II)V

    .line 1626272
    return-void
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 1626359
    iget-boolean v0, p0, LX/A7T;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/A7T;->d:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, LX/A7T;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 1626360
    iget-boolean v0, p0, LX/A7T;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/A7T;->c:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, LX/A7T;->n:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(LX/A7T;)V
    .locals 7

    .prologue
    .line 1626316
    iget-object v0, p0, LX/A7T;->i:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/A7T;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1626317
    :cond_0
    :goto_0
    return-void

    .line 1626318
    :cond_1
    iget-object v0, p0, LX/A7T;->b:LX/1wz;

    invoke-virtual {v0}, LX/1wz;->b()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/998;->f()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1626319
    if-eqz v0, :cond_3

    .line 1626320
    invoke-virtual {p0}, LX/A7T;->getDrawerState()LX/10e;

    move-result-object v0

    invoke-direct {p0, v0}, LX/A7T;->a(LX/10e;)V

    goto :goto_0

    .line 1626321
    :cond_3
    iget-object v0, p0, LX/A7T;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fP;

    .line 1626322
    invoke-virtual {p0}, LX/A7T;->getDrawerState()LX/10e;

    move-result-object v2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1626323
    invoke-static {v0, v2, v5}, LX/0fP;->b(LX/0fP;LX/10e;Z)V

    .line 1626324
    sget-object v3, LX/A7M;->b:[I

    invoke-virtual {v2}, LX/10e;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1626325
    :cond_4
    :goto_3
    iget-object v3, v0, LX/0fP;->j:LX/A7O;

    if-eqz v3, :cond_5

    .line 1626326
    iget-object v3, v0, LX/0fP;->j:LX/A7O;

    invoke-static {v3, v5}, LX/A7O;->e(LX/A7O;Z)V

    .line 1626327
    :cond_5
    iget-object v3, v0, LX/0fP;->k:LX/A7O;

    if-eqz v3, :cond_6

    .line 1626328
    iget-object v3, v0, LX/0fP;->k:LX/A7O;

    invoke-static {v3, v5}, LX/A7O;->e(LX/A7O;Z)V

    .line 1626329
    :cond_6
    iget-object v3, v0, LX/0fP;->p:LX/A7R;

    if-eqz v3, :cond_7

    .line 1626330
    iget-object v3, v0, LX/0fP;->p:LX/A7R;

    .line 1626331
    iget-object v4, v3, LX/A7R;->a:LX/10e;

    if-ne v4, v2, :cond_9

    .line 1626332
    iget-object v4, v3, LX/A7R;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v5, 0x0

    const v2, -0x7ad9418b

    invoke-static {v4, v5, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1626333
    :goto_4
    const/4 v3, 0x0

    iput-object v3, v0, LX/0fP;->p:LX/A7R;

    .line 1626334
    :cond_7
    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    .line 1626335
    :pswitch_0
    invoke-static {v0, v5}, LX/0fP;->g(LX/0fP;Z)V

    .line 1626336
    invoke-static {v0, v5}, LX/0fP;->h(LX/0fP;Z)V

    .line 1626337
    invoke-static {v0, v5}, LX/0fP;->e(LX/0fP;Z)V

    .line 1626338
    invoke-static {v0, v5}, LX/0fP;->f(LX/0fP;Z)V

    .line 1626339
    iget-object v3, v0, LX/0fP;->i:LX/A7N;

    sget-object v4, LX/A7N;->ENSURE_BACKGROUND:LX/A7N;

    if-ne v3, v4, :cond_4

    .line 1626340
    iget-object v3, v0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v3, v5}, LX/A7T;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1626341
    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_4

    .line 1626342
    invoke-virtual {v3, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 1626343
    :pswitch_1
    invoke-static {v0, v6}, LX/0fP;->g(LX/0fP;Z)V

    .line 1626344
    invoke-static {v0, v5}, LX/0fP;->h(LX/0fP;Z)V

    .line 1626345
    invoke-static {v0, v6}, LX/0fP;->e(LX/0fP;Z)V

    .line 1626346
    invoke-static {v0, v5}, LX/0fP;->f(LX/0fP;Z)V

    goto :goto_3

    .line 1626347
    :pswitch_2
    invoke-static {v0, v5}, LX/0fP;->g(LX/0fP;Z)V

    .line 1626348
    invoke-static {v0, v6}, LX/0fP;->h(LX/0fP;Z)V

    .line 1626349
    invoke-static {v0, v5}, LX/0fP;->e(LX/0fP;Z)V

    .line 1626350
    invoke-static {v0, v6}, LX/0fP;->f(LX/0fP;Z)V

    goto :goto_3

    .line 1626351
    :cond_9
    invoke-virtual {v3}, LX/A7R;->c()V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private n()V
    .locals 8

    .prologue
    .line 1626352
    iget-object v0, p0, LX/A7T;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10h;

    .line 1626353
    iget-object v2, v0, LX/10h;->a:LX/0fK;

    iget-object v2, v2, LX/0fK;->n:LX/GgR;

    if-eqz v2, :cond_0

    .line 1626354
    iget-object v2, v0, LX/10h;->a:LX/0fK;

    iget-object v2, v2, LX/0fK;->n:LX/GgR;

    .line 1626355
    iget-object v3, v2, LX/GgR;->a:LX/10n;

    iget-object v4, v2, LX/GgR;->a:LX/10n;

    iget-object v4, v4, LX/10n;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    .line 1626356
    iput-wide v5, v3, LX/10n;->j:J

    .line 1626357
    :cond_0
    goto :goto_0

    .line 1626358
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1626312
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/A7T;->b(Z)V

    .line 1626313
    iget-object v0, p0, LX/A7T;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10h;

    .line 1626314
    invoke-virtual {v0}, LX/10h;->b()V

    goto :goto_0

    .line 1626315
    :cond_0
    return-void
.end method

.method public final a(IIZ)V
    .locals 0

    .prologue
    .line 1626305
    invoke-super {p0, p1, p2, p3}, LX/998;->a(IIZ)V

    .line 1626306
    if-eqz p3, :cond_1

    .line 1626307
    invoke-virtual {p0}, LX/A7T;->getHandler()Landroid/os/Handler;

    move-result-object p1

    .line 1626308
    if-eqz p1, :cond_0

    .line 1626309
    iget-object p2, p0, LX/A7T;->p:Ljava/lang/Runnable;

    const p3, -0x4cad2c77

    invoke-static {p1, p2, p3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1626310
    :cond_0
    :goto_0
    return-void

    .line 1626311
    :cond_1
    invoke-static {p0}, LX/A7T;->l(LX/A7T;)V

    goto :goto_0
.end method

.method public final a(LX/10e;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1626286
    invoke-virtual {p0}, LX/998;->g()Z

    .line 1626287
    iget-object v1, p0, LX/A7T;->b:LX/1wz;

    invoke-virtual {v1}, LX/1wz;->c()V

    .line 1626288
    sget-object v1, LX/10e;->CLOSED:LX/10e;

    if-eq p1, v1, :cond_0

    .line 1626289
    invoke-direct {p0, v0}, LX/A7T;->a(Z)V

    .line 1626290
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, LX/A7T;->a(LX/10e;LX/31M;)V

    .line 1626291
    :cond_0
    sget-object v1, LX/A7S;->a:[I

    invoke-virtual {p1}, LX/10e;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1626292
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 1626293
    if-eqz p2, :cond_4

    .line 1626294
    invoke-direct {p0, v0}, LX/A7T;->e(I)V

    .line 1626295
    invoke-virtual {p0}, LX/A7T;->getDrawerState()LX/10e;

    move-result-object v0

    .line 1626296
    sget-object v1, LX/10e;->CLOSED:LX/10e;

    if-ne v0, v1, :cond_3

    .line 1626297
    invoke-direct {p0, p1}, LX/A7T;->a(LX/10e;)V

    .line 1626298
    :cond_2
    :goto_1
    return-void

    .line 1626299
    :pswitch_0
    invoke-direct {p0}, LX/A7T;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1626300
    iget v0, p0, LX/A7T;->c:I

    goto :goto_0

    .line 1626301
    :pswitch_1
    invoke-direct {p0}, LX/A7T;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1626302
    iget v0, p0, LX/A7T;->d:I

    neg-int v0, v0

    goto :goto_0

    .line 1626303
    :cond_3
    invoke-direct {p0, v0}, LX/A7T;->a(LX/10e;)V

    goto :goto_1

    .line 1626304
    :cond_4
    invoke-direct {p0, v0}, LX/A7T;->b(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/31M;I)V
    .locals 2

    .prologue
    .line 1626168
    sget-object v0, LX/31M;->LEFT:LX/31M;

    if-ne p1, v0, :cond_0

    iget v0, p0, LX/A7T;->h:I

    .line 1626169
    :goto_0
    invoke-direct {p0, v0}, LX/A7T;->e(I)V

    .line 1626170
    iget-object v0, p0, LX/A7T;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10h;

    .line 1626171
    invoke-virtual {v0}, LX/10h;->b()V

    goto :goto_1

    .line 1626172
    :cond_0
    iget v0, p0, LX/A7T;->g:I

    goto :goto_0

    .line 1626173
    :cond_1
    return-void
.end method

.method public final a(FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1626174
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v2

    .line 1626175
    invoke-virtual {p0}, LX/A7T;->getDrawerState()LX/10e;

    move-result-object v3

    .line 1626176
    sget-object v4, LX/A7S;->a:[I

    invoke-virtual {v3}, LX/10e;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 1626177
    :cond_0
    :goto_0
    return v0

    .line 1626178
    :pswitch_0
    invoke-virtual {p0}, LX/A7T;->getWidth()I

    move-result v3

    sub-int v2, v3, v2

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1626179
    :pswitch_1
    neg-int v2, v2

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(FFLX/31M;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1626099
    invoke-direct {p0, v0}, LX/A7T;->a(Z)V

    .line 1626100
    invoke-virtual {p0}, LX/A7T;->getDrawerState()LX/10e;

    move-result-object v2

    invoke-direct {p0, v2, p3}, LX/A7T;->a(LX/10e;LX/31M;)V

    .line 1626101
    invoke-virtual {p0}, LX/998;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1626102
    invoke-direct {p0}, LX/A7T;->n()V

    .line 1626103
    :cond_0
    :goto_0
    return v0

    .line 1626104
    :cond_1
    invoke-virtual {p0}, LX/A7T;->getDrawerState()LX/10e;

    move-result-object v2

    .line 1626105
    sget-object v3, LX/10e;->CLOSED:LX/10e;

    if-ne v2, v3, :cond_6

    .line 1626106
    iget-boolean v2, p0, LX/A7T;->o:Z

    if-eqz v2, :cond_4

    sget-object v2, LX/31M;->LEFT:LX/31M;

    if-ne p3, v2, :cond_2

    invoke-direct {p0}, LX/A7T;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    sget-object v2, LX/31M;->RIGHT:LX/31M;

    if-ne p3, v2, :cond_3

    invoke-direct {p0}, LX/A7T;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    float-to-int v2, p1

    float-to-int v3, p2

    invoke-static {p0, p3, v2, v3}, LX/3BA;->a(Landroid/view/ViewGroup;LX/31M;II)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 1626107
    goto :goto_0

    .line 1626108
    :cond_5
    invoke-direct {p0}, LX/A7T;->n()V

    goto :goto_0

    .line 1626109
    :cond_6
    float-to-int v2, p1

    float-to-int v3, p2

    invoke-direct {p0, v2, v3}, LX/A7T;->b(II)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1626110
    :goto_1
    if-eqz v0, :cond_0

    .line 1626111
    invoke-direct {p0}, LX/A7T;->n()V

    goto :goto_0

    :cond_7
    move v0, v1

    .line 1626112
    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1626113
    iget-object v0, p0, LX/A7T;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1626114
    :cond_0
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 2

    .prologue
    .line 1626115
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v0

    float-to-int v1, p1

    sub-int/2addr v0, v1

    .line 1626116
    iget v1, p0, LX/A7T;->h:I

    if-le v0, v1, :cond_1

    .line 1626117
    iget v0, p0, LX/A7T;->h:I

    .line 1626118
    :cond_0
    :goto_0
    move v0, v0

    .line 1626119
    invoke-direct {p0, v0}, LX/A7T;->b(I)V

    .line 1626120
    return-void

    .line 1626121
    :cond_1
    iget v1, p0, LX/A7T;->g:I

    if-ge v0, v1, :cond_0

    .line 1626122
    iget v0, p0, LX/A7T;->g:I

    goto :goto_0
.end method

.method public final b(FF)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1626123
    invoke-virtual {p0}, LX/998;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1626124
    :cond_0
    :goto_0
    return v0

    .line 1626125
    :cond_1
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v1

    if-eqz v1, :cond_2

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-direct {p0, v1, v2}, LX/A7T;->b(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1626126
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(FF)Z
    .locals 1

    .prologue
    .line 1626127
    const/4 v0, 0x0

    return v0
.end method

.method public final canScrollHorizontally(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1626128
    if-lez p1, :cond_2

    invoke-direct {p0}, LX/A7T;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1626129
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v2

    iget v3, p0, LX/A7T;->c:I

    if-ge v2, v3, :cond_1

    .line 1626130
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1626131
    goto :goto_0

    .line 1626132
    :cond_2
    if-gez p1, :cond_3

    invoke-direct {p0}, LX/A7T;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1626133
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v2

    iget v3, p0, LX/A7T;->d:I

    neg-int v3, v3

    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1626134
    goto :goto_0
.end method

.method public final canScrollVertically(I)Z
    .locals 1

    .prologue
    .line 1626135
    const/4 v0, 0x0

    return v0
.end method

.method public final d(FF)V
    .locals 2

    .prologue
    .line 1626136
    invoke-virtual {p0}, LX/A7T;->getDrawerState()LX/10e;

    move-result-object v0

    sget-object v1, LX/10e;->CLOSED:LX/10e;

    if-eq v0, v1, :cond_0

    .line 1626137
    sget-object v0, LX/10e;->CLOSED:LX/10e;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/A7T;->a(LX/10e;Z)V

    .line 1626138
    :cond_0
    return-void
.end method

.method public getDrawerState()LX/10e;
    .locals 1

    .prologue
    .line 1626139
    invoke-virtual {p0}, LX/A7T;->getScrollX()I

    move-result v0

    .line 1626140
    if-lez v0, :cond_0

    .line 1626141
    sget-object v0, LX/10e;->SHOWING_RIGHT:LX/10e;

    .line 1626142
    :goto_0
    return-object v0

    .line 1626143
    :cond_0
    if-gez v0, :cond_1

    .line 1626144
    sget-object v0, LX/10e;->SHOWING_LEFT:LX/10e;

    goto :goto_0

    .line 1626145
    :cond_1
    sget-object v0, LX/10e;->CLOSED:LX/10e;

    goto :goto_0
.end method

.method public getLeftDrawerWidth()I
    .locals 1

    .prologue
    .line 1626146
    iget v0, p0, LX/A7T;->d:I

    return v0
.end method

.method public getRightDrawerWidth()I
    .locals 1

    .prologue
    .line 1626147
    iget v0, p0, LX/A7T;->c:I

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1626148
    iget-object v0, p0, LX/A7T;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x381b7c09

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1626149
    iget-object v1, p0, LX/A7T;->b:LX/1wz;

    invoke-virtual {v1, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x783ce8d7

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setLeftDrawerEnabled(Z)V
    .locals 1

    .prologue
    .line 1626150
    iget-boolean v0, p0, LX/A7T;->e:Z

    if-eq v0, p1, :cond_0

    .line 1626151
    iput-boolean p1, p0, LX/A7T;->e:Z

    .line 1626152
    if-nez p1, :cond_0

    .line 1626153
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/A7T;->b(Z)V

    .line 1626154
    :cond_0
    return-void
.end method

.method public setLeftDrawerWidth(I)V
    .locals 1

    .prologue
    .line 1626155
    iput p1, p0, LX/A7T;->d:I

    .line 1626156
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/A7T;->b(Z)V

    .line 1626157
    return-void
.end method

.method public setRightDrawerEnabled(Z)V
    .locals 1

    .prologue
    .line 1626158
    iget-boolean v0, p0, LX/A7T;->f:Z

    if-eq v0, p1, :cond_0

    .line 1626159
    iput-boolean p1, p0, LX/A7T;->f:Z

    .line 1626160
    if-nez p1, :cond_0

    .line 1626161
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/A7T;->b(Z)V

    .line 1626162
    :cond_0
    return-void
.end method

.method public setRightDrawerWidth(I)V
    .locals 1

    .prologue
    .line 1626163
    iput p1, p0, LX/A7T;->c:I

    .line 1626164
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/A7T;->b(Z)V

    .line 1626165
    return-void
.end method

.method public setSwipeToOpenEnabled(Z)V
    .locals 0

    .prologue
    .line 1626166
    iput-boolean p1, p0, LX/A7T;->o:Z

    .line 1626167
    return-void
.end method
