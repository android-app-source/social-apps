.class public LX/9nD;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/64w;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1536907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/64v;)LX/64v;
    .locals 4

    .prologue
    .line 1536896
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 1536897
    :try_start_0
    new-instance v0, LX/9nF;

    invoke-direct {v0}, LX/9nF;-><init>()V

    invoke-virtual {p0, v0}, LX/64v;->a(Ljavax/net/ssl/SSLSocketFactory;)LX/64v;

    .line 1536898
    new-instance v0, LX/64d;

    sget-object v1, LX/64e;->a:LX/64e;

    invoke-direct {v0, v1}, LX/64d;-><init>(LX/64e;)V

    const/4 v1, 0x1

    new-array v1, v1, [LX/658;

    const/4 v2, 0x0

    sget-object v3, LX/658;->TLS_1_2:LX/658;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/64d;->a([LX/658;)LX/64d;

    move-result-object v0

    invoke-virtual {v0}, LX/64d;->a()LX/64e;

    move-result-object v0

    .line 1536899
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1536900
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1536901
    sget-object v0, LX/64e;->b:LX/64e;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1536902
    sget-object v0, LX/64e;->c:LX/64e;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1536903
    invoke-static {v1}, LX/65A;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64v;->d:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1536904
    :cond_0
    :goto_0
    return-object p0

    .line 1536905
    :catch_0
    move-exception v0

    .line 1536906
    const-string v1, "OkHttpClientProvider"

    const-string v2, "Error while enabling TLS 1.2"

    invoke-static {v1, v2, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a()LX/64w;
    .locals 1

    .prologue
    .line 1536893
    sget-object v0, LX/9nD;->a:LX/64w;

    if-nez v0, :cond_0

    .line 1536894
    invoke-static {}, LX/9nD;->b()LX/64w;

    move-result-object v0

    sput-object v0, LX/9nD;->a:LX/64w;

    .line 1536895
    :cond_0
    sget-object v0, LX/9nD;->a:LX/64w;

    return-object v0
.end method

.method private static b()LX/64w;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1536891
    new-instance v0, LX/64v;

    invoke-direct {v0}, LX/64v;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/64v;->a(JLjava/util/concurrent/TimeUnit;)LX/64v;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/64v;->b(JLjava/util/concurrent/TimeUnit;)LX/64v;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/64v;->c(JLjava/util/concurrent/TimeUnit;)LX/64v;

    move-result-object v0

    new-instance v1, LX/9nE;

    invoke-direct {v1}, LX/9nE;-><init>()V

    invoke-virtual {v0, v1}, LX/64v;->a(LX/64g;)LX/64v;

    move-result-object v0

    .line 1536892
    invoke-static {v0}, LX/9nD;->a(LX/64v;)LX/64v;

    move-result-object v0

    invoke-virtual {v0}, LX/64v;->a()LX/64w;

    move-result-object v0

    return-object v0
.end method
