.class public final LX/9B0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1zt;

.field public final synthetic b:Z

.field public final synthetic c:I

.field public final synthetic d:LX/9B3;


# direct methods
.method public constructor <init>(LX/9B3;LX/1zt;ZI)V
    .locals 0

    .prologue
    .line 1451003
    iput-object p1, p0, LX/9B0;->d:LX/9B3;

    iput-object p2, p0, LX/9B0;->a:LX/1zt;

    iput-boolean p3, p0, LX/9B0;->b:Z

    iput p4, p0, LX/9B0;->c:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1451004
    iget-object v0, p0, LX/9B0;->d:LX/9B3;

    iget-object v0, v0, LX/9B3;->n:[Z

    iget-object v1, p0, LX/9B0;->d:LX/9B3;

    iget-object v2, p0, LX/9B0;->a:LX/1zt;

    invoke-static {v1, v2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v1

    aput-boolean v3, v0, v1

    .line 1451005
    iget-boolean v0, p0, LX/9B0;->b:Z

    if-eqz v0, :cond_0

    .line 1451006
    iget-object v0, p0, LX/9B0;->d:LX/9B3;

    iget-object v1, p0, LX/9B0;->a:LX/1zt;

    iget v2, p0, LX/9B0;->c:I

    invoke-virtual {v0, v1, v2, v3}, LX/9B3;->a(LX/1zt;IZ)V

    .line 1451007
    :cond_0
    iget-object v0, p0, LX/9B0;->d:LX/9B3;

    iget-object v1, p0, LX/9B0;->a:LX/1zt;

    invoke-static {v0, v1, p1}, LX/9B3;->a$redex0(LX/9B3;LX/1zt;Ljava/lang/Throwable;)V

    .line 1451008
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1450988
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1450989
    if-nez p1, :cond_0

    .line 1450990
    iget-object v0, p0, LX/9B0;->d:LX/9B3;

    iget-object v1, p0, LX/9B0;->a:LX/1zt;

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null feedback received"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, LX/9B3;->a$redex0(LX/9B3;LX/1zt;Ljava/lang/Throwable;)V

    .line 1450991
    :goto_0
    return-void

    .line 1450992
    :cond_0
    iget-object v0, p0, LX/9B0;->d:LX/9B3;

    iget-object v1, p0, LX/9B0;->a:LX/1zt;

    invoke-static {v0, p1, v1}, LX/9B3;->a$redex0(LX/9B3;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;)V

    .line 1450993
    iget-object v0, p0, LX/9B0;->d:LX/9B3;

    iget-object v0, v0, LX/9B3;->n:[Z

    iget-object v1, p0, LX/9B0;->d:LX/9B3;

    iget-object v2, p0, LX/9B0;->a:LX/1zt;

    invoke-static {v1, v2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    .line 1450994
    iget-object v0, p0, LX/9B0;->d:LX/9B3;

    iget-object v1, p0, LX/9B0;->a:LX/1zt;

    .line 1450995
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    if-eqz v2, :cond_1

    .line 1450996
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    .line 1450997
    iget-object v3, v2, LX/9BK;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    .line 1450998
    invoke-static {v3, v1}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->c$redex0(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/1zt;)V

    .line 1450999
    iget-object v0, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    iget-object p0, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->y:Landroid/util/SparseArray;

    .line 1451000
    iget v2, v1, LX/1zt;->e:I

    move v2, v2

    .line 1451001
    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {v0, p0}, LX/9BT;->b(I)V

    .line 1451002
    :cond_1
    goto :goto_0
.end method
