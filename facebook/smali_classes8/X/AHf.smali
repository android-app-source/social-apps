.class public LX/AHf;
.super LX/AHa;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1655855
    const-class v0, LX/AHf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AHf;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;)V
    .locals 0

    .prologue
    .line 1655856
    invoke-direct {p0, p1, p2}, LX/AHa;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655857
    iput-object p3, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    .line 1655858
    return-void
.end method


# virtual methods
.method public final b()Lcom/facebook/audience/model/AudienceControlData;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1655859
    iget-object v0, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;->l()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1655860
    const/4 v0, 0x0

    .line 1655861
    :goto_0
    return-object v0

    .line 1655862
    :cond_0
    iget-object v0, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;->l()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    .line 1655863
    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel;

    .line 1655864
    iget-object v5, p0, LX/AHa;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1655865
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1655866
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1655867
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v5, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    goto :goto_0

    .line 1655868
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1655869
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to retrieve thread owner."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/facebook/audience/model/Reply;
    .locals 4

    .prologue
    .line 1655870
    new-instance v1, LX/AHU;

    iget-object v0, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;->k()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel$EdgesModel;

    iget-object v2, p0, LX/AHa;->b:Ljava/lang/String;

    invoke-virtual {p0}, LX/AHf;->b()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, LX/AHU;-><init>(Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel$EdgesModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 1655871
    invoke-virtual {v1}, LX/AHR;->a()Lcom/facebook/audience/model/Reply;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1655872
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1655873
    iget-object v0, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;->k()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel$EdgesModel;

    .line 1655874
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 1655875
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    sget-object v6, LX/7h9;->TEXT:LX/7h9;

    invoke-virtual {v6}, LX/7h9;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 1655876
    if-nez v5, :cond_1

    const/4 v5, 0x0

    const/4 v9, 0x2

    .line 1655877
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    invoke-virtual {v7, v6, v9}, LX/15i;->j(II)I

    move-result v6

    int-to-float v6, v6

    .line 1655878
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    invoke-virtual {v8, v7, v5}, LX/15i;->j(II)I

    move-result v7

    int-to-float v7, v7

    .line 1655879
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    if-ne v6, v9, :cond_0

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v6

    if-ne v6, v9, :cond_0

    const/4 v5, 0x1

    :cond_0
    move v5, v5

    .line 1655880
    if-eqz v5, :cond_2

    .line 1655881
    :cond_1
    new-instance v5, LX/AHU;

    iget-object v6, p0, LX/AHa;->b:Ljava/lang/String;

    invoke-virtual {p0}, LX/AHf;->b()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v0, v6, v7}, LX/AHU;-><init>(Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$PostsModel$EdgesModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, LX/AHR;->a()Lcom/facebook/audience/model/Reply;

    move-result-object v0

    .line 1655882
    if-eqz v0, :cond_2

    .line 1655883
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1655884
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1655885
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 1655886
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1655887
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 1655888
    iget-boolean v5, v0, Lcom/facebook/audience/model/Reply;->c:Z

    move v5, v5

    .line 1655889
    if-nez v5, :cond_4

    .line 1655890
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1655891
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1655892
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1655893
    iget-object v0, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;->m()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1655894
    invoke-super {p0}, LX/AHa;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    if-eqz v0, :cond_1

    move v0, v1

    .line 1655895
    :goto_0
    if-nez v0, :cond_0

    .line 1655896
    sget-object v3, LX/AHf;->c:Ljava/lang/String;

    const-string v4, "Could not parse the thread participants for thread %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/AHf;->d:Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    invoke-virtual {v5}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;->j()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1655897
    :cond_0
    return v0

    :cond_1
    move v0, v2

    .line 1655898
    goto :goto_0
.end method
