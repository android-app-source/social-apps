.class public LX/ASG;
.super LX/AS0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData$ProvidesAudienceEducatorData;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPrivacyPillSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/AS0",
        "<TModelData;TDerivedData;TServices;>;"
    }
.end annotation


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;LX/0Ot;Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsDefaultPostPrivacyEnabled;
        .end annotation
    .end param
    .param p4    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673819
    invoke-direct {p0, p4, p5, p6}, LX/AS0;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V

    .line 1673820
    iput-object p1, p0, LX/ASG;->b:LX/0Ot;

    .line 1673821
    iput-object p2, p0, LX/ASG;->c:LX/0Or;

    .line 1673822
    iput-object p3, p0, LX/ASG;->d:LX/0Ot;

    .line 1673823
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1673800
    const/4 v0, 0x2

    return v0
.end method

.method public final a(LX/5L2;)LX/ASZ;
    .locals 2

    .prologue
    .line 1673816
    sget-object v0, LX/ASF;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1673817
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    :goto_0
    return-object v0

    .line 1673818
    :pswitch_0
    sget-object v0, LX/ASZ;->SHOW:LX/ASZ;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1673801
    iget-object v0, p0, LX/AS0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->c:Ljava/lang/String;

    move-object v2, v0

    .line 1673802
    iget-object v0, p0, LX/ASG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 1673803
    iget-object v1, p0, LX/ASG;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-virtual {v1, v6}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f081441

    .line 1673804
    :goto_0
    new-instance v3, LX/47x;

    invoke-direct {v3, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "__{TOKEN}__"

    aput-object v5, v4, v6

    invoke-virtual {v0, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    const-string v1, "__{TOKEN}__"

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0

    .line 1673805
    :cond_0
    iget-object v1, p0, LX/AS0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v1

    iget-object v3, v1, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->d:LX/AP5;

    .line 1673806
    if-eqz v3, :cond_1

    sget-object v1, LX/AP5;->NONE:LX/AP5;

    if-ne v3, v1, :cond_2

    .line 1673807
    :cond_1
    const v1, 0x7f081442

    .line 1673808
    :goto_1
    move v1, v1

    .line 1673809
    goto :goto_0

    .line 1673810
    :cond_2
    sget-object v1, LX/AP5;->TAGGEES:LX/AP5;

    if-ne v3, v1, :cond_3

    .line 1673811
    const v1, 0x7f081443

    goto :goto_1

    .line 1673812
    :cond_3
    sget-object v1, LX/AP5;->FRIENDS_OF_TAGGEES:LX/AP5;

    if-ne v3, v1, :cond_4

    .line 1673813
    const v1, 0x7f081444

    goto :goto_1

    .line 1673814
    :cond_4
    iget-object v1, p0, LX/ASG;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Unexpected tag expansion explanation type: "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/AP5;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1673815
    const v1, 0x7f081442

    goto :goto_1
.end method
