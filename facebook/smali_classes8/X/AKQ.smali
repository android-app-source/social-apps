.class public final LX/AKQ;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1663269
    const-class v1, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;

    const v0, 0x26d28b8a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "SnacksBroadcastQuery"

    const-string v6, "4c17277a4d317d6d5dbaf69828a999ac"

    const-string v7, "me"

    const-string v8, "10155247526441729"

    const-string v9, "10155259090206729"

    .line 1663270
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1663271
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1663272
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1663273
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1663274
    sparse-switch v0, :sswitch_data_0

    .line 1663275
    :goto_0
    return-object p1

    .line 1663276
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1663277
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1663278
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1663279
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1663280
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1663281
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1663282
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x16be9c4b -> :sswitch_1
        -0xdbacfae -> :sswitch_0
        -0x9c6725b -> :sswitch_2
        0x35e001 -> :sswitch_4
        0x683094a -> :sswitch_3
        0x38abfb24 -> :sswitch_5
        0x683ab666 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1663283
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 1663284
    :goto_1
    return v0

    .line 1663285
    :pswitch_1
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1663286
    :pswitch_4
    const/16 v0, 0x64

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1663287
    :pswitch_5
    const/16 v0, 0x1770

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1663288
    :pswitch_6
    const/16 v0, 0xa0

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
