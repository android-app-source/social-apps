.class public final LX/9Cm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/8q4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Ct;


# direct methods
.method public constructor <init>(LX/9Ct;)V
    .locals 0

    .prologue
    .line 1454376
    iput-object p1, p0, LX/9Cm;->a:LX/9Ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1454377
    check-cast p1, LX/8q4;

    .line 1454378
    iget-object v0, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v0

    .line 1454379
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1454380
    iget-object v0, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v0, v0, LX/9Ct;->f:LX/03V;

    sget-object v1, LX/9Ct;->a:Ljava/lang/String;

    const-string v2, "Updated comment must have a non null feedback"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1454381
    :goto_0
    return-void

    .line 1454382
    :cond_0
    iget-object v1, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1454383
    iget-object v1, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1454384
    :cond_1
    iget-object v1, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->h:LX/20j;

    iget-object v2, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v2, v2, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1454385
    iget-object v3, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v3, v3

    .line 1454386
    invoke-virtual {v1, v2, v3}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1454387
    iget-object v2, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v2, v2, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v2}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    .line 1454388
    iput-object v1, v2, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1454389
    move-object v1, v2

    .line 1454390
    iget-object v2, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v2, v2, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1454391
    iget-object v2, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v3, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v3, v3, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v3

    sget-object v4, LX/9Cs;->UPDATE:LX/9Cs;

    invoke-static {v2, v3, v0, v4}, LX/9Ct;->a$redex0(LX/9Ct;Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;Lcom/facebook/graphql/model/GraphQLComment;LX/9Cs;)Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v0

    .line 1454392
    iput-object v0, v1, LX/4Vu;->r:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    .line 1454393
    :cond_2
    invoke-virtual {v1}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1454394
    iget-object v1, p0, LX/9Cm;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
