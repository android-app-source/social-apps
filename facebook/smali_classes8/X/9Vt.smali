.class public final LX/9Vt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1500262
    const/4 v15, 0x0

    .line 1500263
    const/4 v14, 0x0

    .line 1500264
    const/4 v13, 0x0

    .line 1500265
    const/4 v12, 0x0

    .line 1500266
    const/4 v11, 0x0

    .line 1500267
    const/4 v10, 0x0

    .line 1500268
    const/4 v9, 0x0

    .line 1500269
    const/4 v8, 0x0

    .line 1500270
    const/4 v7, 0x0

    .line 1500271
    const/4 v6, 0x0

    .line 1500272
    const/4 v5, 0x0

    .line 1500273
    const/4 v4, 0x0

    .line 1500274
    const/4 v3, 0x0

    .line 1500275
    const/4 v2, 0x0

    .line 1500276
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1500277
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1500278
    const/4 v2, 0x0

    .line 1500279
    :goto_0
    return v2

    .line 1500280
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1500281
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_e

    .line 1500282
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1500283
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1500284
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1500285
    const-string v17, "__type__"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    const-string v17, "__typename"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1500286
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v15

    goto :goto_1

    .line 1500287
    :cond_3
    const-string v17, "already_completed"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1500288
    const/4 v3, 0x1

    .line 1500289
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 1500290
    :cond_4
    const-string v17, "completed_subtitle"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1500291
    invoke-static/range {p0 .. p1}, LX/9Vo;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1500292
    :cond_5
    const-string v17, "completed_title"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1500293
    invoke-static/range {p0 .. p1}, LX/9Vp;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1500294
    :cond_6
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1500295
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1500296
    :cond_7
    const-string v17, "negative_feedback_action_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1500297
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 1500298
    :cond_8
    const-string v17, "prefill"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1500299
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1500300
    :cond_9
    const-string v17, "subtitle"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1500301
    invoke-static/range {p0 .. p1}, LX/9Vq;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1500302
    :cond_a
    const-string v17, "target"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1500303
    invoke-static/range {p0 .. p1}, LX/9Vr;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1500304
    :cond_b
    const-string v17, "target_is_fixed"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 1500305
    const/4 v2, 0x1

    .line 1500306
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    goto/16 :goto_1

    .line 1500307
    :cond_c
    const-string v17, "title"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 1500308
    invoke-static/range {p0 .. p1}, LX/9Vs;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1500309
    :cond_d
    const-string v17, "url"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1500310
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1500311
    :cond_e
    const/16 v16, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1500312
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1500313
    if-eqz v3, :cond_f

    .line 1500314
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->a(IZ)V

    .line 1500315
    :cond_f
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1500316
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1500317
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1500318
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1500319
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1500320
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1500321
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1500322
    if-eqz v2, :cond_10

    .line 1500323
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 1500324
    :cond_10
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1500325
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1500326
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 1500327
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1500328
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1500329
    if-eqz v0, :cond_0

    .line 1500330
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500331
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1500332
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1500333
    if-eqz v0, :cond_1

    .line 1500334
    const-string v1, "already_completed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500335
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1500336
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1500337
    if-eqz v0, :cond_2

    .line 1500338
    const-string v1, "completed_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500339
    invoke-static {p0, v0, p2}, LX/9Vo;->a(LX/15i;ILX/0nX;)V

    .line 1500340
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1500341
    if-eqz v0, :cond_3

    .line 1500342
    const-string v1, "completed_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500343
    invoke-static {p0, v0, p2}, LX/9Vp;->a(LX/15i;ILX/0nX;)V

    .line 1500344
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1500345
    if-eqz v0, :cond_4

    .line 1500346
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500347
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1500348
    :cond_4
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1500349
    if-eqz v0, :cond_5

    .line 1500350
    const-string v0, "negative_feedback_action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500351
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1500352
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1500353
    if-eqz v0, :cond_6

    .line 1500354
    const-string v1, "prefill"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500355
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1500356
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1500357
    if-eqz v0, :cond_7

    .line 1500358
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500359
    invoke-static {p0, v0, p2}, LX/9Vq;->a(LX/15i;ILX/0nX;)V

    .line 1500360
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1500361
    if-eqz v0, :cond_8

    .line 1500362
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500363
    invoke-static {p0, v0, p2}, LX/9Vr;->a(LX/15i;ILX/0nX;)V

    .line 1500364
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1500365
    if-eqz v0, :cond_9

    .line 1500366
    const-string v1, "target_is_fixed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500367
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1500368
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1500369
    if-eqz v0, :cond_a

    .line 1500370
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500371
    invoke-static {p0, v0, p2}, LX/9Vs;->a(LX/15i;ILX/0nX;)V

    .line 1500372
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1500373
    if-eqz v0, :cond_b

    .line 1500374
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500375
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1500376
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1500377
    return-void
.end method
