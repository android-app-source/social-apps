.class public final LX/A5e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JX;


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 0

    .prologue
    .line 1621667
    iput-object p1, p0, LX/A5e;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1621668
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1621669
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1621670
    invoke-static {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->b(Lcom/facebook/tagging/model/TaggingProfile;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-result-object v3

    .line 1621671
    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1621672
    iget-object v4, p0, LX/A5e;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v4, v4, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621673
    iget-object v5, v4, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    move-object v4, v5

    .line 1621674
    iget-wide v8, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v6, v8

    .line 1621675
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1621676
    iget-object v0, p0, LX/A5e;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->O:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1621677
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1621678
    iget-object v1, p0, LX/A5e;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->I:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment$7$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment$7$1;-><init>(LX/A5e;LX/0Px;)V

    const v0, -0x375f75f3

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1621679
    return-void
.end method
