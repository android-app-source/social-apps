.class public final LX/9nK;
.super LX/5p8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5p8",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/react/bridge/Callback;

.field public final synthetic b:LX/5pC;

.field public final synthetic c:LX/9nO;


# direct methods
.method public constructor <init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V
    .locals 0

    .prologue
    .line 1537069
    iput-object p1, p0, LX/9nK;->c:LX/9nO;

    iput-object p3, p0, LX/9nK;->a:Lcom/facebook/react/bridge/Callback;

    iput-object p4, p0, LX/9nK;->b:LX/5pC;

    invoke-direct {p0, p2}, LX/5p8;-><init>(LX/5pX;)V

    return-void
.end method

.method private varargs a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1537070
    iget-object v0, p0, LX/9nK;->c:LX/9nO;

    invoke-static {v0}, LX/9nO;->h(LX/9nO;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1537071
    iget-object v0, p0, LX/9nK;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v2}, LX/9nH;->c(Ljava/lang/String;)LX/5pH;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1537072
    :goto_0
    return-void

    .line 1537073
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/9nK;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x64c44b21

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v0, v3

    .line 1537074
    :goto_1
    iget-object v1, p0, LX/9nK;->b:LX/5pC;

    invoke-interface {v1}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1537075
    iget-object v1, p0, LX/9nK;->b:LX/5pC;

    invoke-interface {v1}, LX/5pC;->size()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v4, 0x3e7

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1537076
    iget-object v4, p0, LX/9nK;->c:LX/9nO;

    iget-object v4, v4, LX/9nO;->a:LX/9nP;

    invoke-virtual {v4}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "catalystLocalStorage"

    invoke-static {v1}, LX/9nG;->a(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/9nK;->b:LX/5pC;

    invoke-static {v7, v0, v1}, LX/9nG;->a(LX/5pC;II)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1537077
    add-int/lit16 v0, v0, 0x3e7

    goto :goto_1

    .line 1537078
    :cond_1
    iget-object v0, p0, LX/9nK;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1537079
    :try_start_1
    iget-object v0, p0, LX/9nK;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x71565d7f

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v2

    .line 1537080
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 1537081
    iget-object v1, p0, LX/9nK;->a:Lcom/facebook/react/bridge/Callback;

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 1537082
    :catch_0
    move-exception v0

    .line 1537083
    const-string v1, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537084
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    goto :goto_2

    .line 1537085
    :catch_1
    move-exception v0

    .line 1537086
    :try_start_2
    const-string v1, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537087
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1537088
    :try_start_3
    iget-object v1, p0, LX/9nK;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v4, 0x789ed454

    invoke-static {v1, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 1537089
    :catch_2
    move-exception v1

    .line 1537090
    const-string v4, "React"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537091
    if-nez v0, :cond_2

    .line 1537092
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    goto :goto_2

    .line 1537093
    :catchall_0
    move-exception v0

    .line 1537094
    :try_start_4
    iget-object v1, p0, LX/9nK;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v3, 0x61aaf28b

    invoke-static {v1, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 1537095
    :goto_3
    throw v0

    .line 1537096
    :catch_3
    move-exception v1

    .line 1537097
    const-string v3, "React"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537098
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto :goto_3

    .line 1537099
    :cond_3
    iget-object v0, p0, LX/9nK;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1537100
    invoke-direct {p0}, LX/9nK;->a()V

    return-void
.end method
