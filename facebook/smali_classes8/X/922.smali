.class public final LX/922;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;)V
    .locals 0

    .prologue
    .line 1431857
    iput-object p1, p0, LX/922;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1431858
    iget-object v0, p0, LX/922;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->k:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 1431859
    iget-object v1, p0, LX/922;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    .line 1431860
    iget-object v2, v1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->j:LX/91H;

    .line 1431861
    iget-object v3, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1431862
    const-string p0, "session_id"

    invoke-virtual {v3, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object p0, v1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object p0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object p0

    iget-object p1, v1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object p1, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;->b()Ljava/lang/String;

    move-result-object p2

    move p4, p3

    .line 1431863
    const-string p5, "iconpicker_icon_selected"

    invoke-static {p5, v3}, LX/91H;->a(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p5

    invoke-virtual {p5, p0}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object p5

    invoke-virtual {p5, p1}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object p5

    .line 1431864
    iget-object p3, p5, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "icon_id"

    invoke-virtual {p3, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1431865
    move-object p5, p5

    .line 1431866
    invoke-virtual {p5, p4}, LX/8yQ;->c(I)LX/8yQ;

    move-result-object p5

    .line 1431867
    iget-object p3, p5, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p5, p3

    .line 1431868
    iget-object p3, v2, LX/91H;->a:LX/0Zb;

    invoke-interface {p3, p5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1431869
    iget-object v2, v1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {v2}, LX/2s1;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/2s1;

    move-result-object v2

    .line 1431870
    iput-object v0, v2, LX/2s1;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 1431871
    move-object v2, v2

    .line 1431872
    invoke-virtual {v2}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431873
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1431874
    const-string v3, "minutiae_object"

    iget-object p0, v1, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1431875
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 p0, -0x1

    invoke-virtual {v3, p0, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1431876
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1431877
    return-void
.end method
