.class public final LX/9bS;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9bT;


# direct methods
.method public constructor <init>(LX/9bT;)V
    .locals 0

    .prologue
    .line 1514931
    iput-object p1, p0, LX/9bS;->a:LX/9bT;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1514932
    check-cast p1, [LX/1FJ;

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1514933
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1514934
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1514935
    iget-object v2, p0, LX/9bS;->a:LX/9bT;

    iget-object v2, v2, LX/9bT;->e:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v2, v2, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->j:LX/9iU;

    .line 1514936
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/9iU;->a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    move-object v0, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1514937
    aget-object v1, p1, v4

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    .line 1514938
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/9bS;->a:LX/9bT;

    iget-object v0, v0, LX/9bT;->e:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->l:LX/03V;

    sget-object v2, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->c:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Profile Pic cannot be saved because bitmap is recycled"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1514939
    aget-object v0, p1, v4

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    :catch_0
    aget-object v0, p1, v4

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    aget-object v1, p1, v4

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1514940
    check-cast p1, Landroid/net/Uri;

    .line 1514941
    iget-object v0, p0, LX/9bS;->a:LX/9bT;

    iget-object v0, v0, LX/9bT;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1514942
    if-nez p1, :cond_0

    .line 1514943
    iget-object v0, p0, LX/9bS;->a:LX/9bT;

    iget-object v0, v0, LX/9bT;->b:Landroid/support/v4/app/FragmentActivity;

    .line 1514944
    const v2, 0x7f081200

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1514945
    :goto_0
    return-void

    .line 1514946
    :cond_0
    iget-object v0, p0, LX/9bS;->a:LX/9bT;

    iget-object v1, v0, LX/9bT;->e:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p0, LX/9bS;->a:LX/9bT;

    iget-wide v2, v0, LX/9bT;->c:J

    iget-object v0, p0, LX/9bS;->a:LX/9bT;

    iget-object v5, v0, LX/9bT;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, p0, LX/9bS;->a:LX/9bT;

    iget-wide v6, v0, LX/9bT;->d:J

    move-object v4, p1

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Landroid/app/Activity;J)V

    goto :goto_0
.end method
