.class public LX/9Hd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/9Hj;

.field public final b:LX/1zC;

.field public final c:LX/1Uf;

.field private final d:LX/8pY;

.field public final e:I


# direct methods
.method public constructor <init>(LX/9Hj;LX/1zC;LX/1Uf;LX/8pY;LX/0ad;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1461578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1461579
    iput-object p1, p0, LX/9Hd;->a:LX/9Hj;

    .line 1461580
    iput-object p2, p0, LX/9Hd;->b:LX/1zC;

    .line 1461581
    iput-object p3, p0, LX/9Hd;->c:LX/1Uf;

    .line 1461582
    iput-object p4, p0, LX/9Hd;->d:LX/8pY;

    .line 1461583
    sget v0, LX/0wn;->l:I

    const/4 v1, 0x0

    invoke-interface {p5, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/9Hd;->e:I

    .line 1461584
    return-void
.end method

.method public static a(LX/0QB;)LX/9Hd;
    .locals 9

    .prologue
    .line 1461585
    const-class v1, LX/9Hd;

    monitor-enter v1

    .line 1461586
    :try_start_0
    sget-object v0, LX/9Hd;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461587
    sput-object v2, LX/9Hd;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461588
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461589
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461590
    new-instance v3, LX/9Hd;

    invoke-static {v0}, LX/9Hj;->a(LX/0QB;)LX/9Hj;

    move-result-object v4

    check-cast v4, LX/9Hj;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v5

    check-cast v5, LX/1zC;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v6

    check-cast v6, LX/1Uf;

    invoke-static {v0}, LX/8pY;->b(LX/0QB;)LX/8pY;

    move-result-object v7

    check-cast v7, LX/8pY;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/9Hd;-><init>(LX/9Hj;LX/1zC;LX/1Uf;LX/8pY;LX/0ad;)V

    .line 1461591
    move-object v0, v3

    .line 1461592
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461593
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Hd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461594
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461595
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)Z
    .locals 1

    .prologue
    .line 1461596
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLComment;LX/1Pq;LX/9Fa;ZZ)V
    .locals 3
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "TE;",
            "LX/9Fa;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 1461597
    iget-boolean v0, p4, LX/9Fa;->a:Z

    move v0, v0

    .line 1461598
    if-nez v0, :cond_0

    if-eqz p5, :cond_0

    .line 1461599
    const/4 v0, 0x1

    .line 1461600
    iput-boolean v0, p4, LX/9Fa;->a:Z

    .line 1461601
    :cond_0
    if-nez p6, :cond_2

    .line 1461602
    invoke-interface {p3}, LX/1Pq;->iN_()V

    .line 1461603
    :cond_1
    :goto_0
    return-void

    .line 1461604
    :cond_2
    iget-boolean v0, p4, LX/9Fa;->b:Z

    move v0, v0

    .line 1461605
    if-eqz v0, :cond_3

    .line 1461606
    const/4 v0, 0x0

    .line 1461607
    iput-boolean v0, p4, LX/9Fa;->b:Z

    .line 1461608
    invoke-interface {p3}, LX/1Pq;->iN_()V

    goto :goto_0

    .line 1461609
    :cond_3
    invoke-static {p2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v0

    invoke-static {v0}, LX/9Hd;->a(Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1461610
    iget-object v0, p0, LX/9Hd;->d:LX/8pY;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/9Hc;

    invoke-direct {v2, p0, p4, p3}, LX/9Hc;-><init>(LX/9Hd;LX/9Fa;LX/1Pq;)V

    invoke-virtual {v0, v1, v2}, LX/8pY;->a(Ljava/lang/String;LX/3Wt;)V

    goto :goto_0
.end method
