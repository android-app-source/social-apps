.class public final LX/A9S;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 1634586
    const/16 v26, 0x0

    .line 1634587
    const/16 v25, 0x0

    .line 1634588
    const/16 v24, 0x0

    .line 1634589
    const/16 v23, 0x0

    .line 1634590
    const/16 v22, 0x0

    .line 1634591
    const/16 v21, 0x0

    .line 1634592
    const/16 v20, 0x0

    .line 1634593
    const/16 v19, 0x0

    .line 1634594
    const/16 v18, 0x0

    .line 1634595
    const/16 v17, 0x0

    .line 1634596
    const/16 v16, 0x0

    .line 1634597
    const/4 v15, 0x0

    .line 1634598
    const/4 v14, 0x0

    .line 1634599
    const/4 v13, 0x0

    .line 1634600
    const/4 v12, 0x0

    .line 1634601
    const/4 v11, 0x0

    .line 1634602
    const/4 v10, 0x0

    .line 1634603
    const/4 v9, 0x0

    .line 1634604
    const/4 v8, 0x0

    .line 1634605
    const/4 v7, 0x0

    .line 1634606
    const/4 v6, 0x0

    .line 1634607
    const/4 v5, 0x0

    .line 1634608
    const/4 v4, 0x0

    .line 1634609
    const/4 v3, 0x0

    .line 1634610
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 1634611
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1634612
    const/4 v3, 0x0

    .line 1634613
    :goto_0
    return v3

    .line 1634614
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1634615
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_16

    .line 1634616
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v27

    .line 1634617
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1634618
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    if-eqz v27, :cond_1

    .line 1634619
    const-string v28, "account_business_info"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 1634620
    invoke-static/range {p0 .. p1}, LX/A9R;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 1634621
    :cond_2
    const-string v28, "account_info"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 1634622
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto :goto_1

    .line 1634623
    :cond_3
    const-string v28, "account_status"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 1634624
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    goto :goto_1

    .line 1634625
    :cond_4
    const-string v28, "ads_currency"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 1634626
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 1634627
    :cond_5
    const-string v28, "audiences"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 1634628
    invoke-static/range {p0 .. p1}, LX/A9N;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 1634629
    :cond_6
    const-string v28, "balance"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 1634630
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1634631
    :cond_7
    const-string v28, "can_update_currency"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 1634632
    const/4 v5, 0x1

    .line 1634633
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 1634634
    :cond_8
    const-string v28, "checkout_available_balance"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 1634635
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1634636
    :cond_9
    const-string v28, "conversion_pixels"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 1634637
    invoke-static/range {p0 .. p1}, LX/AA1;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1634638
    :cond_a
    const-string v28, "has_funding_source"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 1634639
    const/4 v4, 0x1

    .line 1634640
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 1634641
    :cond_b
    const-string v28, "id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 1634642
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 1634643
    :cond_c
    const-string v28, "is_locked_into_checkout"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 1634644
    const/4 v3, 0x1

    .line 1634645
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 1634646
    :cond_d
    const-string v28, "legacy_account_id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 1634647
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1634648
    :cond_e
    const-string v28, "max_daily_budget"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 1634649
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1634650
    :cond_f
    const-string v28, "min_daily_budget"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 1634651
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1634652
    :cond_10
    const-string v28, "name"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 1634653
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1634654
    :cond_11
    const-string v28, "payment_info"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_12

    .line 1634655
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 1634656
    :cond_12
    const-string v28, "prepay_account_balance"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_13

    .line 1634657
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1634658
    :cond_13
    const-string v28, "spend_info"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_14

    .line 1634659
    invoke-static/range {p0 .. p1}, LX/A9O;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1634660
    :cond_14
    const-string v28, "stored_balance_status"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_15

    .line 1634661
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1634662
    :cond_15
    const-string v28, "timezone_info"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 1634663
    invoke-static/range {p0 .. p1}, LX/A9P;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1634664
    :cond_16
    const/16 v27, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1634665
    const/16 v27, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634666
    const/16 v26, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634667
    const/16 v25, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634668
    const/16 v24, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634669
    const/16 v23, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634670
    const/16 v22, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1634671
    if-eqz v5, :cond_17

    .line 1634672
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1634673
    :cond_17
    const/4 v5, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1634674
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1634675
    if-eqz v4, :cond_18

    .line 1634676
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1634677
    :cond_18
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1634678
    if-eqz v3, :cond_19

    .line 1634679
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->a(IZ)V

    .line 1634680
    :cond_19
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1634681
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1634682
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1634683
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1634684
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1634685
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1634686
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1634687
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1634688
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1634689
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 1634490
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634491
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634492
    if-eqz v0, :cond_1

    .line 1634493
    const-string v1, "account_business_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634494
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634495
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1634496
    if-eqz v1, :cond_0

    .line 1634497
    const-string v3, "business_country_code"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634498
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634499
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634500
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634501
    if-eqz v0, :cond_2

    .line 1634502
    const-string v1, "account_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634503
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634504
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1634505
    if-eqz v0, :cond_3

    .line 1634506
    const-string v0, "account_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634507
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634508
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634509
    if-eqz v0, :cond_4

    .line 1634510
    const-string v1, "ads_currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634511
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1634512
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634513
    if-eqz v0, :cond_5

    .line 1634514
    const-string v1, "audiences"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634515
    invoke-static {p0, v0, p2, p3}, LX/A9N;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634516
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634517
    if-eqz v0, :cond_6

    .line 1634518
    const-string v1, "balance"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634519
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1634520
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634521
    if-eqz v0, :cond_7

    .line 1634522
    const-string v1, "can_update_currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634523
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634524
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634525
    if-eqz v0, :cond_8

    .line 1634526
    const-string v1, "checkout_available_balance"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634527
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1634528
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634529
    if-eqz v0, :cond_a

    .line 1634530
    const-string v1, "conversion_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634531
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1634532
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 1634533
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/AA1;->a(LX/15i;ILX/0nX;)V

    .line 1634534
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1634535
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1634536
    :cond_a
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634537
    if-eqz v0, :cond_b

    .line 1634538
    const-string v1, "has_funding_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634539
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634540
    :cond_b
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634541
    if-eqz v0, :cond_c

    .line 1634542
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634543
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634544
    :cond_c
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634545
    if-eqz v0, :cond_d

    .line 1634546
    const-string v1, "is_locked_into_checkout"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634547
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634548
    :cond_d
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634549
    if-eqz v0, :cond_e

    .line 1634550
    const-string v1, "legacy_account_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634551
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634552
    :cond_e
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634553
    if-eqz v0, :cond_f

    .line 1634554
    const-string v1, "max_daily_budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634555
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1634556
    :cond_f
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634557
    if-eqz v0, :cond_10

    .line 1634558
    const-string v1, "min_daily_budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634559
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1634560
    :cond_10
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634561
    if-eqz v0, :cond_11

    .line 1634562
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634563
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634564
    :cond_11
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634565
    if-eqz v0, :cond_12

    .line 1634566
    const-string v1, "payment_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634567
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634568
    :cond_12
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634569
    if-eqz v0, :cond_13

    .line 1634570
    const-string v1, "prepay_account_balance"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634571
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1634572
    :cond_13
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634573
    if-eqz v0, :cond_14

    .line 1634574
    const-string v1, "spend_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634575
    invoke-static {p0, v0, p2, p3}, LX/A9O;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634576
    :cond_14
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634577
    if-eqz v0, :cond_15

    .line 1634578
    const-string v1, "stored_balance_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634579
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634580
    :cond_15
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634581
    if-eqz v0, :cond_16

    .line 1634582
    const-string v1, "timezone_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634583
    invoke-static {p0, v0, p2}, LX/BYT;->a(LX/15i;ILX/0nX;)V

    .line 1634584
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634585
    return-void
.end method
