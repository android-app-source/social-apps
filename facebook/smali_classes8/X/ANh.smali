.class public final LX/ANh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 0

    .prologue
    .line 1668339
    iput-object p1, p0, LX/ANh;->a:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1668340
    const/4 v0, 0x0

    .line 1668341
    iget-object v1, p0, LX/ANh;->a:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    iget-object v1, v1, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnTouchListener;

    .line 1668342
    invoke-interface {v0, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1668343
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 1668344
    goto :goto_0

    .line 1668345
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method
