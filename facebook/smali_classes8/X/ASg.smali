.class public LX/ASg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1674354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674355
    return-void
.end method

.method public static final a(Landroid/net/Uri;LX/4gF;)F
    .locals 1

    .prologue
    .line 1674353
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/ASg;->a(Landroid/net/Uri;LX/4gF;Lcom/facebook/ipc/media/MediaItem;)F

    move-result v0

    return v0
.end method

.method public static a(Landroid/net/Uri;LX/4gF;Lcom/facebook/ipc/media/MediaItem;)F
    .locals 3

    .prologue
    .line 1674356
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1674357
    sget-object v0, LX/ASf;->a:[I

    invoke-virtual {p1}, LX/4gF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1674358
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ComposerAttachmentViewUtility.getMediaAspectRatio: unknown media type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1674359
    :pswitch_0
    check-cast p2, Lcom/facebook/photos/base/media/VideoItem;

    const/4 v1, 0x0

    .line 1674360
    if-nez p2, :cond_0

    move v0, v1

    .line 1674361
    :goto_0
    const/high16 v2, 0x7fc00000    # NaNf

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 1674362
    :goto_1
    move v0, v0

    .line 1674363
    :goto_2
    return v0

    :pswitch_1
    check-cast p2, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-static {p0, p2}, LX/74d;->a(Landroid/net/Uri;Lcom/facebook/photos/base/media/PhotoItem;)F

    move-result v0

    goto :goto_2

    .line 1674364
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v0

    goto :goto_0

    .line 1674365
    :cond_1
    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1674366
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1674367
    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/74d;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 1674368
    const/16 v2, 0x12

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/74d;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 1674369
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_3

    .line 1674370
    :cond_2
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1674371
    const v0, 0x3faaaaab

    goto :goto_1

    .line 1674372
    :cond_3
    const/16 p1, 0x18

    invoke-virtual {v0, p1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/74d;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    .line 1674373
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1674374
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_3
    invoke-static {v2, v1, v0}, LX/74d;->a(III)F

    move-result v0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/ASg;
    .locals 1

    .prologue
    .line 1674350
    new-instance v0, LX/ASg;

    invoke-direct {v0}, LX/ASg;-><init>()V

    .line 1674351
    move-object v0, v0

    .line 1674352
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaItem;)F
    .locals 1

    .prologue
    .line 1674347
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    .line 1674348
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object p0

    invoke-static {v0, p0, p1}, LX/ASg;->a(Landroid/net/Uri;LX/4gF;Lcom/facebook/ipc/media/MediaItem;)F

    move-result p0

    move v0, p0

    .line 1674349
    return v0
.end method
