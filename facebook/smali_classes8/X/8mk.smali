.class public final LX/8mk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<[",
        "LX/1bf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8mj;

.field public final synthetic b:Lcom/facebook/stickers/ui/StickerDraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/ui/StickerDraweeView;LX/8mj;)V
    .locals 0

    .prologue
    .line 1399749
    iput-object p1, p0, LX/8mk;->b:Lcom/facebook/stickers/ui/StickerDraweeView;

    iput-object p2, p0, LX/8mk;->a:LX/8mj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([LX/1bf;)V
    .locals 2
    .param p1    # [LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1399750
    iget-object v0, p0, LX/8mk;->a:LX/8mj;

    iget-object v0, v0, LX/8mj;->a:Ljava/lang/String;

    iget-object v1, p0, LX/8mk;->b:Lcom/facebook/stickers/ui/StickerDraweeView;

    iget-object v1, v1, Lcom/facebook/stickers/ui/StickerDraweeView;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399751
    iget-object v0, p0, LX/8mk;->b:Lcom/facebook/stickers/ui/StickerDraweeView;

    iget-object v1, p0, LX/8mk;->a:LX/8mj;

    .line 1399752
    invoke-static {v0, p1, v1}, Lcom/facebook/stickers/ui/StickerDraweeView;->a$redex0(Lcom/facebook/stickers/ui/StickerDraweeView;[LX/1bf;LX/8mj;)V

    .line 1399753
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1399754
    sget-object v0, Lcom/facebook/stickers/ui/StickerDraweeView;->j:Ljava/lang/Class;

    const-string v1, "Error loading sticker %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/8mk;->a:LX/8mj;

    iget-object v4, v4, LX/8mj;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1399755
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1399756
    check-cast p1, [LX/1bf;

    invoke-direct {p0, p1}, LX/8mk;->a([LX/1bf;)V

    return-void
.end method
