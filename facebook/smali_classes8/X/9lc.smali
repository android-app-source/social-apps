.class public LX/9lc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1533068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1533069
    iput-object p1, p0, LX/9lc;->a:LX/0Zb;

    .line 1533070
    return-void
.end method

.method public static a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1533060
    const-string v0, "graphql_token"

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1533061
    iget-object v0, p0, LX/9lc;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1533062
    return-void
.end method

.method public static b(LX/0QB;)LX/9lc;
    .locals 2

    .prologue
    .line 1533063
    new-instance v1, LX/9lc;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/9lc;-><init>(LX/0Zb;)V

    .line 1533064
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1533065
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fb4a_rapid_reporting_opened"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1533066
    invoke-static {p0, v0, p1}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1533067
    return-void
.end method
