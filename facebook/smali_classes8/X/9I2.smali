.class public final LX/9I2;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9I3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1462441
    const-string v0, "CommentPlaceInfoAttachmentHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1462442
    if-ne p0, p1, :cond_1

    .line 1462443
    :cond_0
    :goto_0
    return v0

    .line 1462444
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1462445
    goto :goto_0

    .line 1462446
    :cond_3
    check-cast p1, LX/9I2;

    .line 1462447
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1462448
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1462449
    if-eq v2, v3, :cond_0

    .line 1462450
    iget-object v2, p0, LX/9I2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9I2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/9I2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1462451
    goto :goto_0

    .line 1462452
    :cond_5
    iget-object v2, p1, LX/9I2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1462453
    :cond_6
    iget-boolean v2, p0, LX/9I2;->b:Z

    iget-boolean v3, p1, LX/9I2;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1462454
    goto :goto_0

    .line 1462455
    :cond_7
    iget-object v2, p0, LX/9I2;->c:LX/1Pq;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/9I2;->c:LX/1Pq;

    iget-object v3, p1, LX/9I2;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1462456
    goto :goto_0

    .line 1462457
    :cond_8
    iget-object v2, p1, LX/9I2;->c:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
