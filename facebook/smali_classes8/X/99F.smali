.class public LX/99F;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field private a:LX/99E;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1447234
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    return-void
.end method

.method public constructor <init>(LX/99E;)V
    .locals 1

    .prologue
    .line 1447230
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1447231
    new-instance v0, LX/99E;

    invoke-direct {v0, p1}, LX/99E;-><init>(LX/99E;)V

    iput-object v0, p0, LX/99F;->a:LX/99E;

    .line 1447232
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1447233
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1447228
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1447229
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 1447227
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 1447226
    iget-object v0, p0, LX/99F;->a:LX/99E;

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1447225
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1447224
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1447235
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1447213
    sget-object v0, LX/03r;->TintDrawable:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1447214
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1447215
    const/16 v2, 0x1

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1447216
    const/16 v3, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 1447217
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1447218
    new-instance v0, LX/99E;

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1, v3, v2}, LX/99E;-><init>(Landroid/graphics/drawable/Drawable;ZI)V

    iput-object v0, p0, LX/99F;->a:LX/99E;

    .line 1447219
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1447220
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "getConstantState() should not return null during inflate"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1447221
    :cond_0
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1447222
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1447223
    return-void
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1447210
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 1447211
    invoke-virtual {p0}, LX/99F;->invalidateSelf()V

    .line 1447212
    :cond_0
    return-void
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1447208
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1447209
    return-object p0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1447206
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1447207
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 1447203
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 1447204
    invoke-virtual {p0, p2, p3, p4}, LX/99F;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 1447205
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1447201
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1447202
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1447195
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-boolean v0, v0, LX/99E;->b:Z

    if-nez v0, :cond_0

    .line 1447196
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1447197
    :cond_0
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1447198
    iget-object v0, p0, LX/99F;->a:LX/99E;

    iget-object v0, v0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 1447199
    invoke-virtual {p0, p2}, LX/99F;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 1447200
    :cond_0
    return-void
.end method
