.class public LX/A8g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private a:Z

.field private b:Z

.field public c:LX/4hC;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1628032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1628033
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/A8g;->a:Z

    .line 1628034
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1628035
    :cond_0
    invoke-static {p2}, LX/3Lz;->getInstance(Landroid/content/Context;)LX/3Lz;

    move-result-object v0

    .line 1628036
    new-instance p2, LX/4hC;

    invoke-direct {p2, p1, v0}, LX/4hC;-><init>(Ljava/lang/String;LX/3Lz;)V

    move-object v0, p2

    .line 1628037
    iput-object v0, p0, LX/A8g;->c:LX/4hC;

    .line 1628038
    return-void
.end method

.method public static a(LX/A8g;CZ)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1628025
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/A8g;->c:LX/4hC;

    .line 1628026
    const/4 p0, 0x1

    invoke-static {v0, p1, p0}, LX/4hC;->inputDigitWithOptionToRememberPosition(LX/4hC;CZ)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, LX/4hC;->currentOutput:Ljava/lang/String;

    .line 1628027
    iget-object p0, v0, LX/4hC;->currentOutput:Ljava/lang/String;

    move-object v0, p0

    .line 1628028
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/A8g;->c:LX/4hC;

    .line 1628029
    const/4 p0, 0x0

    invoke-static {v0, p1, p0}, LX/4hC;->inputDigitWithOptionToRememberPosition(LX/4hC;CZ)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, LX/4hC;->currentOutput:Ljava/lang/String;

    .line 1628030
    iget-object p0, v0, LX/4hC;->currentOutput:Ljava/lang/String;

    move-object v0, p0

    .line 1628031
    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1628022
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/A8g;->b:Z

    .line 1628023
    iget-object v0, p0, LX/A8g;->c:LX/4hC;

    invoke-virtual {v0}, LX/4hC;->clear()V

    .line 1628024
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;II)Z
    .locals 2

    .prologue
    .line 1628015
    move v0, p1

    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_1

    .line 1628016
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 1628017
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1628018
    const/4 v0, 0x1

    .line 1628019
    :goto_1
    return v0

    .line 1628020
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1628021
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1627979
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, LX/A8g;->b:Z

    if-eqz v2, :cond_2

    .line 1627980
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v0, p0, LX/A8g;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1627981
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 1627982
    goto :goto_0

    .line 1627983
    :cond_2
    :try_start_1
    iget-boolean v0, p0, LX/A8g;->a:Z

    if-nez v0, :cond_0

    .line 1627984
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v2, 0x0

    .line 1627985
    add-int/lit8 v7, v0, -0x1

    .line 1627986
    const/4 v3, 0x0

    .line 1627987
    iget-object v1, p0, LX/A8g;->c:LX/4hC;

    invoke-virtual {v1}, LX/4hC;->clear()V

    .line 1627988
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    move v4, v2

    move v1, v2

    move v5, v2

    .line 1627989
    :goto_2
    if-ge v4, v8, :cond_5

    .line 1627990
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    .line 1627991
    invoke-static {v6}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1627992
    if-eqz v5, :cond_3

    .line 1627993
    invoke-static {p0, v5, v1}, LX/A8g;->a(LX/A8g;CZ)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    move v1, v2

    :cond_3
    move v5, v6

    move-object v6, v3

    .line 1627994
    :goto_3
    if-ne v4, v7, :cond_4

    .line 1627995
    const/4 v1, 0x1

    .line 1627996
    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v6

    goto :goto_2

    .line 1627997
    :cond_5
    if-eqz v5, :cond_6

    .line 1627998
    invoke-static {p0, v5, v1}, LX/A8g;->a(LX/A8g;CZ)Ljava/lang/String;

    move-result-object v3

    .line 1627999
    :cond_6
    move-object v3, v3

    .line 1628000
    if-eqz v3, :cond_0

    .line 1628001
    iget-object v0, p0, LX/A8g;->c:LX/4hC;

    const/4 v1, 0x0

    .line 1628002
    iget-boolean v2, v0, LX/4hC;->ableToFormat:Z

    if-nez v2, :cond_a

    .line 1628003
    iget v1, v0, LX/4hC;->originalPosition:I

    .line 1628004
    :cond_7
    move v6, v1

    .line 1628005
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/A8g;->a:Z

    .line 1628006
    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 1628007
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1628008
    invoke-static {p1, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 1628009
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/A8g;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1628010
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    :try_start_2
    move-object v6, v3

    goto :goto_3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_a
    move v2, v1

    .line 1628011
    :goto_4
    iget v4, v0, LX/4hC;->positionToRemember:I

    if-ge v2, v4, :cond_7

    iget-object v4, v0, LX/4hC;->currentOutput:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 1628012
    iget-object v4, v0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    iget-object v5, v0, LX/4hC;->currentOutput:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_b

    .line 1628013
    add-int/lit8 v2, v2, 0x1

    .line 1628014
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1627971
    iget-boolean v0, p0, LX/A8g;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/A8g;->b:Z

    if-eqz v0, :cond_1

    .line 1627972
    :cond_0
    :goto_0
    return-void

    .line 1627973
    :cond_1
    if-lez p3, :cond_0

    invoke-static {p1, p2, p3}, LX/A8g;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1627974
    invoke-direct {p0}, LX/A8g;->a()V

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1627975
    iget-boolean v0, p0, LX/A8g;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/A8g;->b:Z

    if-eqz v0, :cond_1

    .line 1627976
    :cond_0
    :goto_0
    return-void

    .line 1627977
    :cond_1
    if-lez p4, :cond_0

    invoke-static {p1, p2, p4}, LX/A8g;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1627978
    invoke-direct {p0}, LX/A8g;->a()V

    goto :goto_0
.end method
