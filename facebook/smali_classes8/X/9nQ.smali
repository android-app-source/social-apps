.class public LX/9nQ;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "ToastAndroid"
.end annotation


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 1537301
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1537302
    return-void
.end method

.method public static synthetic a(LX/9nQ;)LX/5pY;
    .locals 1

    .prologue
    .line 1537299
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1537300
    return-object v0
.end method

.method public static synthetic b(LX/9nQ;)LX/5pY;
    .locals 1

    .prologue
    .line 1537285
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1537286
    return-object v0
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1537292
    invoke-static {}, LX/5ps;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1537293
    const-string v1, "SHORT"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537294
    const-string v1, "LONG"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537295
    const-string v1, "TOP"

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537296
    const-string v1, "BOTTOM"

    const/16 v2, 0x51

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537297
    const-string v1, "CENTER"

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537298
    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1537291
    const-string v0, "ToastAndroid"

    return-object v0
.end method

.method public show(Ljava/lang/String;I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1537289
    new-instance v0, Lcom/facebook/react/modules/toast/ToastModule$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/react/modules/toast/ToastModule$1;-><init>(LX/9nQ;Ljava/lang/String;I)V

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1537290
    return-void
.end method

.method public showWithGravity(Ljava/lang/String;II)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1537287
    new-instance v0, Lcom/facebook/react/modules/toast/ToastModule$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/react/modules/toast/ToastModule$2;-><init>(LX/9nQ;Ljava/lang/String;II)V

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1537288
    return-void
.end method
