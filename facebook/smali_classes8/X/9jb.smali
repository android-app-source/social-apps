.class public final LX/9jb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinSearchQuery;",
        "LX/9jN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9jo;

.field public final synthetic b:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;LX/9jo;)V
    .locals 0

    .prologue
    .line 1530204
    iput-object p1, p0, LX/9jb;->b:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    iput-object p2, p0, LX/9jb;->a:LX/9jo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1530205
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    .line 1530206
    iget-object v0, p0, LX/9jb;->a:LX/9jo;

    .line 1530207
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->b()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    move-result-object v3

    .line 1530208
    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v4

    .line 1530209
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 1530210
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1530211
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    .line 1530212
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$EdgesModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1530213
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1530214
    :cond_0
    new-instance v1, LX/9jN;

    invoke-direct {v1}, LX/9jN;-><init>()V

    .line 1530215
    iput-object p0, v1, LX/9jN;->a:Ljava/util/List;

    .line 1530216
    iput-object p0, v1, LX/9jN;->b:Ljava/util/List;

    .line 1530217
    iget-object v2, v0, LX/9jo;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1530218
    iput-object v2, v1, LX/9jN;->d:Ljava/lang/String;

    .line 1530219
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 1530220
    iput-object v2, v1, LX/9jN;->c:Ljava/lang/String;

    .line 1530221
    sget-object v2, LX/9jM;->TRADITIONAL:LX/9jM;

    .line 1530222
    iput-object v2, v1, LX/9jN;->g:LX/9jM;

    .line 1530223
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->c()Z

    move-result v2

    .line 1530224
    iput-boolean v2, v1, LX/9jN;->h:Z

    .line 1530225
    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->b()Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    move-result-object v2

    .line 1530226
    iput-object v2, v1, LX/9jN;->f:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    .line 1530227
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    move-result-object v2

    .line 1530228
    iput-object v2, v1, LX/9jN;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    .line 1530229
    move-object v0, v1

    .line 1530230
    return-object v0
.end method
