.class public final LX/9AM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1449855
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1449856
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449857
    :goto_0
    return v1

    .line 1449858
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449859
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1449860
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1449861
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1449862
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1449863
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1449864
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 1449865
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1449866
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1449867
    :cond_4
    const-string v6, "profile_badge"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1449868
    const/4 v5, 0x0

    .line 1449869
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 1449870
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449871
    :goto_2
    move v2, v5

    .line 1449872
    goto :goto_1

    .line 1449873
    :cond_5
    const-string v6, "profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1449874
    const/4 v5, 0x0

    .line 1449875
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_17

    .line 1449876
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449877
    :goto_3
    move v0, v5

    .line 1449878
    goto :goto_1

    .line 1449879
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1449880
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1449881
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1449882
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1449883
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1449884
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 1449885
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449886
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_a

    .line 1449887
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1449888
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1449889
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_9

    if-eqz v6, :cond_9

    .line 1449890
    const-string v7, "badge_icon"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1449891
    const/4 v6, 0x0

    .line 1449892
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_f

    .line 1449893
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449894
    :goto_5
    move v2, v6

    .line 1449895
    goto :goto_4

    .line 1449896
    :cond_a
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1449897
    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 1449898
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_b
    move v2, v5

    goto :goto_4

    .line 1449899
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449900
    :cond_d
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_e

    .line 1449901
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1449902
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1449903
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_d

    if-eqz v7, :cond_d

    .line 1449904
    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1449905
    const/4 v7, 0x0

    .line 1449906
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v8, :cond_13

    .line 1449907
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449908
    :goto_7
    move v2, v7

    .line 1449909
    goto :goto_6

    .line 1449910
    :cond_e
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1449911
    invoke-virtual {p1, v6, v2}, LX/186;->b(II)V

    .line 1449912
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_f
    move v2, v6

    goto :goto_6

    .line 1449913
    :cond_10
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449914
    :cond_11
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_12

    .line 1449915
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1449916
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1449917
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_11

    if-eqz v8, :cond_11

    .line 1449918
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 1449919
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_8

    .line 1449920
    :cond_12
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1449921
    invoke-virtual {p1, v7, v2}, LX/186;->b(II)V

    .line 1449922
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_7

    :cond_13
    move v2, v7

    goto :goto_8

    .line 1449923
    :cond_14
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449924
    :cond_15
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_16

    .line 1449925
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1449926
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1449927
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_15

    if-eqz v6, :cond_15

    .line 1449928
    const-string v7, "uri"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 1449929
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_9

    .line 1449930
    :cond_16
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1449931
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1449932
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_17
    move v0, v5

    goto :goto_9
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1449933
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1449934
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1449935
    if-eqz v0, :cond_0

    .line 1449936
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449937
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1449938
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1449939
    if-eqz v0, :cond_1

    .line 1449940
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449941
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1449942
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1449943
    if-eqz v0, :cond_5

    .line 1449944
    const-string v1, "profile_badge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449945
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1449946
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1449947
    if-eqz v1, :cond_4

    .line 1449948
    const-string v2, "badge_icon"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449949
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1449950
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449951
    if-eqz v2, :cond_3

    .line 1449952
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449953
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1449954
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1449955
    if-eqz v0, :cond_2

    .line 1449956
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449957
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1449958
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1449959
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1449960
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1449961
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1449962
    if-eqz v0, :cond_7

    .line 1449963
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449964
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1449965
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1449966
    if-eqz v1, :cond_6

    .line 1449967
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449968
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1449969
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1449970
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1449971
    return-void
.end method
