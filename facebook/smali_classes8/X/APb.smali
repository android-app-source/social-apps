.class public final LX/APb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/APd;


# direct methods
.method public constructor <init>(LX/APd;)V
    .locals 0

    .prologue
    .line 1670263
    iput-object p1, p0, LX/APb;->a:LX/APd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1670264
    iget-object v0, p0, LX/APb;->a:LX/APd;

    iget-object v0, v0, LX/APd;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670265
    iget-object v1, p0, LX/APb;->a:LX/APd;

    iget-object v2, v1, LX/APd;->b:LX/1RW;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0j2;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    .line 1670266
    iget-object v4, v2, LX/1RW;->a:LX/0Zb;

    const-string v5, "save_draft_from_more_option_menu"

    invoke-static {v2, v5}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "story_id"

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "media_count"

    invoke-virtual {v5, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "char_count"

    invoke-virtual {v5, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1670267
    iget-object v0, p0, LX/APb;->a:LX/APd;

    iget-object v0, v0, LX/APd;->d:LX/Hqa;

    .line 1670268
    iget-object v1, v0, LX/Hqa;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->u:LX/APY;

    sget-object v2, LX/8K5;->COMPOSER_POPOVER_MENU:LX/8K5;

    invoke-virtual {v1, v2}, LX/APY;->a(LX/8K5;)V

    .line 1670269
    iget-object v1, v0, LX/Hqa;->a:Lcom/facebook/composer/activity/ComposerFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/facebook/composer/activity/ComposerFragment;->f(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 1670270
    const/4 v0, 0x1

    return v0
.end method
