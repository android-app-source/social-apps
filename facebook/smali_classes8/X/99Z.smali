.class public LX/99Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Tn;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 1

    .prologue
    .line 1447387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1447388
    iput-object p1, p0, LX/99Z;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1447389
    iput-object p2, p0, LX/99Z;->b:LX/0Tn;

    .line 1447390
    const/4 v0, 0x0

    iput-object v0, p0, LX/99Z;->c:Ljava/lang/String;

    .line 1447391
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1447392
    monitor-enter p0

    if-nez p1, :cond_0

    .line 1447393
    :goto_0
    monitor-exit p0

    return-void

    .line 1447394
    :cond_0
    :try_start_0
    iput-object p1, p0, LX/99Z;->c:Ljava/lang/String;

    .line 1447395
    iget-object v0, p0, LX/99Z;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/99Z;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1447396
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
