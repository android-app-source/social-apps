.class public final enum LX/8oz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8oz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8oz;

.field public static final enum OTHER:LX/8oz;

.field public static final enum SCROLL:LX/8oz;

.field public static final enum UPDATE:LX/8oz;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1405097
    new-instance v0, LX/8oz;

    const-string v1, "SCROLL"

    invoke-direct {v0, v1, v2}, LX/8oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8oz;->SCROLL:LX/8oz;

    .line 1405098
    new-instance v0, LX/8oz;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v3}, LX/8oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8oz;->UPDATE:LX/8oz;

    .line 1405099
    new-instance v0, LX/8oz;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v4}, LX/8oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8oz;->OTHER:LX/8oz;

    .line 1405100
    const/4 v0, 0x3

    new-array v0, v0, [LX/8oz;

    sget-object v1, LX/8oz;->SCROLL:LX/8oz;

    aput-object v1, v0, v2

    sget-object v1, LX/8oz;->UPDATE:LX/8oz;

    aput-object v1, v0, v3

    sget-object v1, LX/8oz;->OTHER:LX/8oz;

    aput-object v1, v0, v4

    sput-object v0, LX/8oz;->$VALUES:[LX/8oz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1405101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8oz;
    .locals 1

    .prologue
    .line 1405102
    const-class v0, LX/8oz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8oz;

    return-object v0
.end method

.method public static values()[LX/8oz;
    .locals 1

    .prologue
    .line 1405103
    sget-object v0, LX/8oz;->$VALUES:[LX/8oz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8oz;

    return-object v0
.end method
