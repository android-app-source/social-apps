.class public LX/94d;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1435567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Z
    .locals 2

    .prologue
    .line 1435568
    if-eqz p0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1435569
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1435570
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1435571
    iget-boolean v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v0, v1

    .line 1435572
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1435573
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1435574
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1435575
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1435576
    invoke-static {v0}, LX/2cA;->c(LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1435577
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1435578
    invoke-static {v0}, LX/2cA;->e(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
