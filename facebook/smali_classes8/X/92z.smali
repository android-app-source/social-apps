.class public LX/92z;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/92z;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1433141
    const-string v0, "minutiae_tables"

    const/4 v1, 0x3

    new-instance v2, LX/92y;

    invoke-direct {v2}, LX/92y;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1433142
    iput-object p1, p0, LX/92z;->a:LX/0Zb;

    .line 1433143
    return-void
.end method

.method public static a(LX/0QB;)LX/92z;
    .locals 4

    .prologue
    .line 1433144
    sget-object v0, LX/92z;->b:LX/92z;

    if-nez v0, :cond_1

    .line 1433145
    const-class v1, LX/92z;

    monitor-enter v1

    .line 1433146
    :try_start_0
    sget-object v0, LX/92z;->b:LX/92z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1433147
    if-eqz v2, :cond_0

    .line 1433148
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1433149
    new-instance p0, LX/92z;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/92z;-><init>(LX/0Zb;)V

    .line 1433150
    move-object v0, p0

    .line 1433151
    sput-object v0, LX/92z;->b:LX/92z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1433152
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1433153
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1433154
    :cond_1
    sget-object v0, LX/92z;->b:LX/92z;

    return-object v0

    .line 1433155
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1433156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 1433157
    invoke-super {p0, p1}, LX/0Tv;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1433158
    iget-object v0, p0, LX/92z;->a:LX/0Zb;

    const-string v1, "minutiae_schema_clear_all_data"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1433159
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1433160
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1433161
    :cond_0
    return-void
.end method
