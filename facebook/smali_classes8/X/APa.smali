.class public final LX/APa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/APd;


# direct methods
.method public constructor <init>(LX/APd;)V
    .locals 0

    .prologue
    .line 1670238
    iput-object p1, p0, LX/APa;->a:LX/APd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v0, 0x1

    const v1, 0x238383c9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1670239
    new-instance v3, LX/5OM;

    iget-object v0, p0, LX/APa;->a:LX/APd;

    iget-object v0, v0, LX/APd;->a:Landroid/content/Context;

    invoke-direct {v3, v0}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1670240
    iget-object v0, p0, LX/APa;->a:LX/APd;

    iget-object v0, v0, LX/APd;->c:LX/0ad;

    sget-short v1, LX/1EB;->K:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1670241
    iget-object v0, p0, LX/APa;->a:LX/APd;

    .line 1670242
    invoke-virtual {v3}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 1670243
    iget-object v5, v0, LX/APd;->c:LX/0ad;

    sget-char v7, LX/1EB;->I:C

    const v8, 0x7f0814af

    iget-object v9, v0, LX/APd;->a:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-interface {v5, v7, v8, v9}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    .line 1670244
    new-instance v1, LX/APb;

    invoke-direct {v1, v0}, LX/APb;-><init>(LX/APd;)V

    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1670245
    iget-object v1, v0, LX/APd;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    move-object v5, v1

    .line 1670246
    check-cast v5, LX/0ik;

    invoke-interface {v5}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2zG;

    invoke-virtual {v5}, LX/2zG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2zG;

    check-cast v1, LX/5Qv;

    invoke-interface {v1}, LX/5Qv;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670247
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1670248
    :cond_1
    iget-object v0, p0, LX/APa;->a:LX/APd;

    iget-object v0, v0, LX/APd;->c:LX/0ad;

    sget-short v1, LX/1aO;->az:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1670249
    iget-object v0, p0, LX/APa;->a:LX/APd;

    .line 1670250
    invoke-virtual {v3}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 1670251
    iget-object v5, v0, LX/APd;->c:LX/0ad;

    sget-char v7, LX/1aO;->ay:C

    const v8, 0x7f08149e

    iget-object v9, v0, LX/APd;->a:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-interface {v5, v7, v8, v9}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    .line 1670252
    new-instance v1, LX/APc;

    invoke-direct {v1, v0}, LX/APc;-><init>(LX/APd;)V

    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1670253
    iget-object v1, v0, LX/APd;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    move-object v5, v1

    .line 1670254
    check-cast v5, LX/0ik;

    invoke-interface {v5}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2zG;

    invoke-virtual {v5}, LX/2zG;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2zG;

    check-cast v1, LX/5Qv;

    invoke-interface {v1}, LX/5Qv;->i()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1670255
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1670256
    :cond_3
    iget-object v0, p0, LX/APa;->a:LX/APd;

    iget-object v0, v0, LX/APd;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670257
    iput-boolean v4, v3, LX/0ht;->e:Z

    .line 1670258
    invoke-virtual {v3, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1670259
    iget-object v1, p0, LX/APa;->a:LX/APd;

    iget-object v4, v1, LX/APd;->b:LX/1RW;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0j2;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    .line 1670260
    iget-object v7, v4, LX/1RW;->a:LX/0Zb;

    const-string v8, "clicked_more_option_menu"

    invoke-static {v4, v8}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "story_id"

    invoke-virtual {v8, v9, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "media_count"

    invoke-virtual {v8, v9, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "char_count"

    invoke-virtual {v8, v9, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1670261
    invoke-virtual {v3}, LX/0ht;->d()V

    .line 1670262
    const v0, 0x284f11b5

    invoke-static {v6, v6, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
