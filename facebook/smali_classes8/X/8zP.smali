.class public final LX/8zP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8zR;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/8zZ;

.field public b:Ljava/lang/String;

.field public c:LX/8zS;

.field public d:Landroid/view/View$OnClickListener;

.field public e:LX/1OX;

.field public f:LX/8zz;

.field public final synthetic g:LX/8zR;


# direct methods
.method public constructor <init>(LX/8zR;)V
    .locals 1

    .prologue
    .line 1427377
    iput-object p1, p0, LX/8zP;->g:LX/8zR;

    .line 1427378
    move-object v0, p1

    .line 1427379
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1427380
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1427376
    const-string v0, "ActivityObjectListComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1427350
    if-ne p0, p1, :cond_1

    .line 1427351
    :cond_0
    :goto_0
    return v0

    .line 1427352
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1427353
    goto :goto_0

    .line 1427354
    :cond_3
    check-cast p1, LX/8zP;

    .line 1427355
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1427356
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1427357
    if-eq v2, v3, :cond_0

    .line 1427358
    iget-object v2, p0, LX/8zP;->a:LX/8zZ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8zP;->a:LX/8zZ;

    iget-object v3, p1, LX/8zP;->a:LX/8zZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1427359
    goto :goto_0

    .line 1427360
    :cond_5
    iget-object v2, p1, LX/8zP;->a:LX/8zZ;

    if-nez v2, :cond_4

    .line 1427361
    :cond_6
    iget-object v2, p0, LX/8zP;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/8zP;->b:Ljava/lang/String;

    iget-object v3, p1, LX/8zP;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1427362
    goto :goto_0

    .line 1427363
    :cond_8
    iget-object v2, p1, LX/8zP;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1427364
    :cond_9
    iget-object v2, p0, LX/8zP;->c:LX/8zS;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/8zP;->c:LX/8zS;

    iget-object v3, p1, LX/8zP;->c:LX/8zS;

    invoke-virtual {v2, v3}, LX/8zS;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1427365
    goto :goto_0

    .line 1427366
    :cond_b
    iget-object v2, p1, LX/8zP;->c:LX/8zS;

    if-nez v2, :cond_a

    .line 1427367
    :cond_c
    iget-object v2, p0, LX/8zP;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/8zP;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/8zP;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1427368
    goto :goto_0

    .line 1427369
    :cond_e
    iget-object v2, p1, LX/8zP;->d:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_d

    .line 1427370
    :cond_f
    iget-object v2, p0, LX/8zP;->e:LX/1OX;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/8zP;->e:LX/1OX;

    iget-object v3, p1, LX/8zP;->e:LX/1OX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1427371
    goto :goto_0

    .line 1427372
    :cond_11
    iget-object v2, p1, LX/8zP;->e:LX/1OX;

    if-nez v2, :cond_10

    .line 1427373
    :cond_12
    iget-object v2, p0, LX/8zP;->f:LX/8zz;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/8zP;->f:LX/8zz;

    iget-object v3, p1, LX/8zP;->f:LX/8zz;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1427374
    goto/16 :goto_0

    .line 1427375
    :cond_13
    iget-object v2, p1, LX/8zP;->f:LX/8zz;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
