.class public final LX/9ES;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/8py;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Ea;


# direct methods
.method public constructor <init>(LX/9Ea;)V
    .locals 0

    .prologue
    .line 1457037
    iput-object p1, p0, LX/9ES;->a:LX/9Ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1457038
    check-cast p1, LX/8py;

    .line 1457039
    iget-object v0, p0, LX/9ES;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->m:LX/9EZ;

    .line 1457040
    iget-object v1, v0, LX/9EZ;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->i:LX/20j;

    iget-object v2, v0, LX/9EZ;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457041
    iget-object v3, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v3, v3

    .line 1457042
    invoke-virtual {v1, v2, v3}, LX/20j;->e(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1457043
    iget-object v2, v0, LX/9EZ;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->s:LX/9EL;

    if-eqz v2, :cond_0

    .line 1457044
    iget-object v2, v0, LX/9EZ;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->s:LX/9EL;

    .line 1457045
    iget-object v3, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v3, v3

    .line 1457046
    iget-object v4, v2, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v4, :cond_0

    .line 1457047
    iget-object v5, v2, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p0, v2, LX/9EL;->b:LX/20j;

    iget-object v4, v2, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457048
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, p1

    .line 1457049
    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {p0, v4, v3}, LX/20j;->e(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    iput-object v4, v2, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457050
    iget-object v4, v2, LX/9EL;->a:LX/9CC;

    iget-object v5, v2, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v4, v5}, LX/9CC;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1457051
    :cond_0
    iget-object v2, v0, LX/9EZ;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->b:LX/0QK;

    invoke-interface {v2, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457052
    return-void
.end method
