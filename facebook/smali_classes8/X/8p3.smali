.class public LX/8p3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1405369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405370
    iput-object p1, p0, LX/8p3;->a:Landroid/content/Context;

    .line 1405371
    iput-object p2, p0, LX/8p3;->b:LX/03V;

    .line 1405372
    return-void
.end method

.method public static final a(LX/8of;ILcom/facebook/user/model/Name;)I
    .locals 8

    .prologue
    .line 1405373
    add-int/lit8 v0, p1, -0x1

    :goto_0
    if-ltz v0, :cond_4

    .line 1405374
    invoke-virtual {p0, v0}, LX/8of;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1405375
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1405376
    :sswitch_0
    add-int/lit8 v1, p1, -0x1

    const/4 v3, 0x0

    .line 1405377
    move v2, v1

    .line 1405378
    :goto_1
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1405379
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 1405380
    :cond_1
    if-nez p2, :cond_5

    .line 1405381
    add-int/lit8 v2, v2, 0x1

    .line 1405382
    :cond_2
    :goto_2
    move v1, v2

    .line 1405383
    invoke-virtual {p0, v1}, LX/8of;->a(I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1405384
    :goto_3
    :sswitch_1
    return v0

    .line 1405385
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1405386
    :sswitch_2
    add-int/lit8 v1, v0, 0x1

    if-ge v1, p1, :cond_0

    .line 1405387
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1405388
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 1405389
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v7

    .line 1405390
    if-nez v7, :cond_6

    .line 1405391
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1405392
    :cond_6
    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v1, 0x1

    invoke-interface {p0, v4, v5}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1405393
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1405394
    if-gtz v4, :cond_7

    .line 1405395
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1405396
    :cond_7
    add-int/lit8 v4, v4, -0x1

    move v5, v4

    move v4, v2

    :goto_4
    if-ltz v5, :cond_13

    .line 1405397
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v6

    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    if-eq v6, p1, :cond_b

    .line 1405398
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1405399
    add-int/lit8 v2, v4, 0x1

    goto :goto_2

    .line 1405400
    :cond_8
    if-nez v5, :cond_9

    .line 1405401
    add-int/lit8 v2, v4, 0x1

    goto :goto_2

    .line 1405402
    :cond_9
    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-nez v3, :cond_a

    .line 1405403
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1405404
    :cond_a
    add-int/lit8 v2, v4, 0x1

    goto :goto_2

    .line 1405405
    :cond_b
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1405406
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    const/16 p1, 0x20

    if-eq v6, p1, :cond_e

    .line 1405407
    if-nez v5, :cond_c

    .line 1405408
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 1405409
    :cond_c
    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-nez v3, :cond_d

    .line 1405410
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 1405411
    :cond_d
    add-int/lit8 v2, v4, 0x1

    goto/16 :goto_2

    :cond_e
    move v4, v2

    .line 1405412
    :cond_f
    add-int/lit8 v6, v2, -0x1

    .line 1405413
    if-gez v6, :cond_12

    .line 1405414
    if-nez v5, :cond_10

    move v2, v3

    .line 1405415
    goto/16 :goto_2

    .line 1405416
    :cond_10
    add-int/lit8 v2, v5, -0x1

    invoke-virtual {v7, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v2

    if-nez v2, :cond_11

    move v2, v3

    .line 1405417
    goto/16 :goto_2

    .line 1405418
    :cond_11
    add-int/lit8 v2, v4, 0x1

    goto/16 :goto_2

    .line 1405419
    :cond_12
    add-int/lit8 v2, v5, -0x1

    move v5, v2

    move v2, v6

    goto/16 :goto_4

    .line 1405420
    :cond_13
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v5, 0x40

    if-eq v3, v5, :cond_2

    .line 1405421
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v3

    if-nez v3, :cond_14

    .line 1405422
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 1405423
    :cond_14
    add-int/lit8 v2, v4, 0x1

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_2
        0x20 -> :sswitch_0
        0x40 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/8of;II)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1405424
    invoke-virtual {p0}, LX/8of;->length()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 1405425
    const-string v0, "_"

    .line 1405426
    :cond_0
    :goto_0
    return-object v0

    .line 1405427
    :cond_1
    if-lt p1, p2, :cond_2

    .line 1405428
    const-string v0, "_"

    goto :goto_0

    .line 1405429
    :cond_2
    invoke-virtual {p0, p1}, LX/8of;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1405430
    const-string v0, "_"

    goto :goto_0

    .line 1405431
    :cond_3
    if-lez p1, :cond_4

    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, LX/8of;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1405432
    const-string v0, "_"

    goto :goto_0

    .line 1405433
    :cond_4
    invoke-virtual {p0, p1}, LX/8of;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1405434
    :cond_5
    invoke-virtual {p0, p1, p2}, LX/8of;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1405435
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 1405436
    const-string v0, "_"

    goto :goto_0

    .line 1405437
    :sswitch_0
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0, p2}, LX/8of;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1405438
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_5

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x23 -> :sswitch_0
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public static b(LX/0QB;)LX/8p3;
    .locals 3

    .prologue
    .line 1405439
    new-instance v2, LX/8p3;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/8p3;-><init>(Landroid/content/Context;LX/03V;)V

    .line 1405440
    return-object v2
.end method


# virtual methods
.method public final a(LX/8of;IILjava/lang/CharSequence;)Z
    .locals 6

    .prologue
    .line 1405441
    const/16 v5, 0x40

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 1405442
    invoke-static {v1, v3, v2}, LX/8p3;->a(LX/8of;II)Ljava/lang/CharSequence;

    move-result-object p0

    .line 1405443
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p3

    const-string p4, "_"

    invoke-virtual {p3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 1405444
    iget-object p0, v0, LX/8p3;->b:LX/03V;

    const-string p1, "MentionsAutoCompleteTextView"

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "attempted to complete mention that isn\'t valid with start at : "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p3

    const-string p4, " in text: \'"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p3

    const-string p4, "\' with \'"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p3

    const-string p4, "\'."

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p1, p3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405445
    :cond_0
    :goto_0
    move v0, p2

    .line 1405446
    return v0

    .line 1405447
    :cond_1
    invoke-virtual {v1}, LX/8of;->length()I

    move-result p3

    .line 1405448
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result p0

    sub-int/2addr p3, p0

    .line 1405449
    invoke-virtual {v1, v3}, LX/8of;->charAt(I)C

    move-result p0

    if-ne p0, v5, :cond_2

    move p0, p1

    :goto_1
    sub-int p0, p3, p0

    .line 1405450
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result p3

    add-int/2addr p0, p3

    .line 1405451
    iget-object p3, v0, LX/8p3;->a:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f0c0028

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    if-gt p0, p3, :cond_0

    move p2, p1

    goto :goto_0

    :cond_2
    move p0, p2

    .line 1405452
    goto :goto_1
.end method
