.class public LX/8j6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8j6;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392150
    iput-object p1, p0, LX/8j6;->a:LX/0Zb;

    .line 1392151
    iput-object p2, p0, LX/8j6;->b:LX/0SG;

    .line 1392152
    return-void
.end method

.method public static a(LX/0QB;)LX/8j6;
    .locals 5

    .prologue
    .line 1392157
    sget-object v0, LX/8j6;->c:LX/8j6;

    if-nez v0, :cond_1

    .line 1392158
    const-class v1, LX/8j6;

    monitor-enter v1

    .line 1392159
    :try_start_0
    sget-object v0, LX/8j6;->c:LX/8j6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1392160
    if-eqz v2, :cond_0

    .line 1392161
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1392162
    new-instance p0, LX/8j6;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/8j6;-><init>(LX/0Zb;LX/0SG;)V

    .line 1392163
    move-object v0, p0

    .line 1392164
    sput-object v0, LX/8j6;->c:LX/8j6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1392165
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1392166
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1392167
    :cond_1
    sget-object v0, LX/8j6;->c:LX/8j6;

    return-object v0

    .line 1392168
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1392169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/8j6;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1392153
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "download_preview"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1392154
    const-string v1, "event_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1392155
    const-string v1, "timestamp"

    iget-object v2, p0, LX/8j6;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1392156
    return-object v0
.end method
