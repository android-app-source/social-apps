.class public abstract LX/AHR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1655612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1655613
    iput-object p1, p0, LX/AHR;->a:Ljava/lang/String;

    .line 1655614
    iput-object p2, p0, LX/AHR;->b:Ljava/lang/String;

    .line 1655615
    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/audience/model/Reply;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1655575
    invoke-virtual {p0}, LX/AHR;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1655576
    const/4 v0, 0x0

    .line 1655577
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/audience/model/Reply;->newBuilder()LX/7go;

    move-result-object v0

    invoke-virtual {p0}, LX/AHR;->c()Ljava/lang/String;

    move-result-object v1

    .line 1655578
    iput-object v1, v0, LX/7go;->g:Ljava/lang/String;

    .line 1655579
    move-object v0, v0

    .line 1655580
    invoke-virtual {p0}, LX/AHR;->d()Landroid/net/Uri;

    move-result-object v1

    .line 1655581
    iput-object v1, v0, LX/7go;->b:Landroid/net/Uri;

    .line 1655582
    move-object v0, v0

    .line 1655583
    invoke-virtual {p0}, LX/AHR;->e()Landroid/net/Uri;

    move-result-object v1

    .line 1655584
    iput-object v1, v0, LX/7go;->k:Landroid/net/Uri;

    .line 1655585
    move-object v0, v0

    .line 1655586
    invoke-virtual {p0}, LX/AHR;->f()Landroid/net/Uri;

    move-result-object v1

    .line 1655587
    iput-object v1, v0, LX/7go;->f:Landroid/net/Uri;

    .line 1655588
    move-object v0, v0

    .line 1655589
    invoke-virtual {p0}, LX/AHR;->g()Ljava/lang/String;

    move-result-object v1

    .line 1655590
    iput-object v1, v0, LX/7go;->d:Ljava/lang/String;

    .line 1655591
    move-object v0, v0

    .line 1655592
    invoke-virtual {p0}, LX/AHR;->h()J

    move-result-wide v2

    .line 1655593
    iput-wide v2, v0, LX/7go;->i:J

    .line 1655594
    move-object v0, v0

    .line 1655595
    invoke-virtual {p0}, LX/AHR;->i()J

    move-result-wide v2

    .line 1655596
    iput-wide v2, v0, LX/7go;->j:J

    .line 1655597
    move-object v0, v0

    .line 1655598
    invoke-virtual {p0}, LX/AHR;->j()F

    move-result v1

    .line 1655599
    iput v1, v0, LX/7go;->a:F

    .line 1655600
    move-object v0, v0

    .line 1655601
    invoke-virtual {p0}, LX/AHR;->k()Z

    move-result v1

    .line 1655602
    iput-boolean v1, v0, LX/7go;->c:Z

    .line 1655603
    move-object v0, v0

    .line 1655604
    invoke-virtual {p0}, LX/AHR;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/AHR;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, LX/AHR;->a(Ljava/lang/String;)Z

    move-result v1

    :goto_1
    move v1, v1

    .line 1655605
    if-eqz v1, :cond_1

    sget-object v1, LX/7gq;->SEEN:LX/7gq;

    :goto_2
    move-object v1, v1

    .line 1655606
    iput-object v1, v0, LX/7go;->h:LX/7gq;

    .line 1655607
    move-object v0, v0

    .line 1655608
    invoke-virtual {p0}, LX/AHR;->l()Ljava/lang/String;

    move-result-object v1

    .line 1655609
    iput-object v1, v0, LX/7go;->e:Ljava/lang/String;

    .line 1655610
    move-object v0, v0

    .line 1655611
    invoke-virtual {v0}, LX/7go;->a()Lcom/facebook/audience/model/Reply;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v1, LX/7gq;->SENT:LX/7gq;

    goto :goto_2

    :cond_2
    iget-object v1, p0, LX/AHR;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, LX/AHR;->a(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1655574
    iget-object v0, p0, LX/AHR;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AHR;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Landroid/net/Uri;
.end method

.method public abstract e()Landroid/net/Uri;
.end method

.method public abstract f()Landroid/net/Uri;
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()J
.end method

.method public abstract i()J
.end method

.method public abstract j()F
.end method

.method public abstract k()Z
.end method

.method public abstract l()Ljava/lang/String;
.end method
