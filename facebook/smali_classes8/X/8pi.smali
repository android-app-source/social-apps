.class public final LX/8pi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:LX/3H7;


# direct methods
.method public constructor <init>(LX/3H7;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 1

    .prologue
    .line 1406257
    iput-object p1, p0, LX/8pi;->d:LX/3H7;

    iput-object p2, p0, LX/8pi;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/8pi;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406258
    iget-object v0, p0, LX/8pi;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, LX/8pi;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1406259
    iget-object v0, p0, LX/8pi;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/8pi;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    const v2, -0x2e7a652d

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1406260
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1406252
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1406253
    iget-object v2, p0, LX/8pi;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406254
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406255
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2, v0}, LX/20j;->c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, LX/8pi;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406256
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1406250
    iget-object v0, p0, LX/8pi;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1406251
    return-void
.end method
