.class public final LX/A4K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$AllShareStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$LinkMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SummaryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$VideoShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1618372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;
    .locals 19

    .prologue
    .line 1618338
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1618339
    move-object/from16 v0, p0

    iget-object v3, v0, LX/A4K;->a:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$AllShareStoriesModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1618340
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1618341
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->d:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$ExternalUrlOwningProfileModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1618342
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1618343
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$InstantArticleModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1618344
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->g:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$LinkMediaModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1618345
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->h:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1618346
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->i:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1618347
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->j:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SummaryModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1618348
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->k:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$TitleModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1618349
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->l:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 1618350
    move-object/from16 v0, p0

    iget-object v4, v0, LX/A4K;->m:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$VideoShareModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1618351
    const/16 v4, 0xd

    invoke-virtual {v2, v4}, LX/186;->c(I)V

    .line 1618352
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, LX/186;->b(II)V

    .line 1618353
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/A4K;->b:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1618354
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1618355
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1618356
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1618357
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1618358
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1618359
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1618360
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1618361
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1618362
    const/16 v3, 0xa

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1618363
    const/16 v3, 0xb

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1618364
    const/16 v3, 0xc

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1618365
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1618366
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1618367
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1618368
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1618369
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1618370
    new-instance v3, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;-><init>(LX/15i;)V

    .line 1618371
    return-object v3
.end method
