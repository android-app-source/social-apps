.class public final LX/9WA;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/9WC;


# direct methods
.method public constructor <init>(LX/9WC;)V
    .locals 0

    .prologue
    .line 1500661
    iput-object p1, p0, LX/9WA;->a:LX/9WC;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1500658
    iget-object v0, p0, LX/9WA;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->g:LX/2EJ;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1500659
    iget-object v0, p0, LX/9WA;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->d:LX/03V;

    sget-object v1, LX/9WC;->b:Ljava/lang/String;

    const-string v2, "NFX message send action failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1500660
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1500649
    const/4 v2, 0x1

    .line 1500650
    iget-object v0, p0, LX/9WA;->a:LX/9WC;

    .line 1500651
    iput-boolean v2, v0, LX/9WC;->l:Z

    .line 1500652
    iget-object v0, p0, LX/9WA;->a:LX/9WC;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1500653
    iput-object v1, v0, LX/9WC;->B:Ljava/lang/Boolean;

    .line 1500654
    iget-object v0, p0, LX/9WA;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->g:LX/2EJ;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1500655
    iget-object v0, p0, LX/9WA;->a:LX/9WC;

    invoke-static {v0}, LX/9WC;->i(LX/9WC;)V

    .line 1500656
    iget-object v1, p0, LX/9WA;->a:LX/9WC;

    iget-object v0, p0, LX/9WA;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->h:Ljava/util/Stack;

    iget-object v2, p0, LX/9WA;->a:LX/9WC;

    iget-object v2, v2, LX/9WC;->h:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v2}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Vx;

    invoke-static {v1, v0}, LX/9WC;->a$redex0(LX/9WC;LX/9Vx;)V

    .line 1500657
    return-void
.end method
