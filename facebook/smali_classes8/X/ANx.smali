.class public LX/ANx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANo;


# instance fields
.field public final a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final b:Landroid/content/Context;

.field public final c:LX/6Jt;

.field public final d:LX/ANk;

.field private final e:Lcom/facebook/videocodec/effects/renderers/TextRenderer;

.field public final f:LX/6KY;

.field public g:LX/AOL;

.field public h:LX/ANv;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;LX/1FZ;LX/6Jt;)V
    .locals 2

    .prologue
    .line 1668580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668581
    new-instance v0, LX/ANw;

    invoke-direct {v0, p0}, LX/ANw;-><init>(LX/ANx;)V

    iput-object v0, p0, LX/ANx;->d:LX/ANk;

    .line 1668582
    iput-object p1, p0, LX/ANx;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668583
    iput-object p2, p0, LX/ANx;->b:Landroid/content/Context;

    .line 1668584
    new-instance v0, LX/6Jw;

    new-instance v1, LX/7Sq;

    invoke-direct {v1, p3}, LX/7Sq;-><init>(LX/1FZ;)V

    invoke-direct {v0, v1}, LX/6Jw;-><init>(LX/7Sq;)V

    iput-object v0, p0, LX/ANx;->e:Lcom/facebook/videocodec/effects/renderers/TextRenderer;

    .line 1668585
    new-instance v0, LX/6KY;

    iget-object v1, p0, LX/ANx;->e:Lcom/facebook/videocodec/effects/renderers/TextRenderer;

    invoke-direct {v0, v1}, LX/6KY;-><init>(LX/61B;)V

    iput-object v0, p0, LX/ANx;->f:LX/6KY;

    .line 1668586
    iput-object p4, p0, LX/ANx;->c:LX/6Jt;

    .line 1668587
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1668588
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668589
    iget-object v0, p0, LX/ANx;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/ANx;->g:LX/AOL;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668590
    return-void
.end method

.method public final b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668591
    iget-object v0, p0, LX/ANx;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668592
    return-void
.end method
