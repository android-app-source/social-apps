.class public final LX/90o;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1429432
    iput-object p1, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iput-object p2, p0, LX/90o;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1429433
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iget-object v1, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429434
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429435
    iget-object v2, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->r:Ljava/lang/String;

    .line 1429436
    const-string v3, "activity_picker_load_failed"

    invoke-static {v3, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    .line 1429437
    iget-object v4, v3, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v3, v4

    .line 1429438
    iget-object v4, v0, LX/919;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429439
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-static {v0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/Throwable;)V

    .line 1429440
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429441
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    invoke-virtual {v0}, LX/92C;->g()V

    .line 1429442
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1429443
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1429444
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    .line 1429445
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 1429446
    if-nez v0, :cond_0

    .line 1429447
    :goto_0
    return-void

    .line 1429448
    :cond_0
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->c()V

    .line 1429449
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    .line 1429450
    iget-boolean v1, v0, LX/90G;->c:Z

    move v0, v1

    .line 1429451
    if-nez v0, :cond_1

    .line 1429452
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iget-object v1, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429453
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429454
    iget-object v2, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->r:Ljava/lang/String;

    iget-object v3, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget v3, v3, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->z:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/90o;->a:Ljava/lang/String;

    .line 1429455
    const-string v5, "activity_picker_bootstrap_loaded"

    invoke-static {v5, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    .line 1429456
    iget-object v6, v5, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v5, v6

    .line 1429457
    iget-object v6, v0, LX/919;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429458
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    .line 1429459
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/90G;->c:Z

    .line 1429460
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1429461
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    .line 1429462
    iget-object v1, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    .line 1429463
    iget-object v2, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    invoke-interface {v2}, LX/5LG;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1429464
    iget-object v2, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    check-cast v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-static {v2}, LX/5Lb;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;)LX/5Lb;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1429465
    iput-object v3, v2, LX/5Lb;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1429466
    move-object v2, v2

    .line 1429467
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1429468
    iput-object v3, v2, LX/5Lb;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1429469
    move-object v2, v2

    .line 1429470
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1429471
    iput-object v3, v2, LX/5Lb;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1429472
    move-object v2, v2

    .line 1429473
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1429474
    iput-object v3, v2, LX/5Lb;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1429475
    move-object v2, v2

    .line 1429476
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1429477
    iput-object v3, v2, LX/5Lb;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1429478
    move-object v2, v2

    .line 1429479
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1429480
    iput-object v3, v2, LX/5Lb;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1429481
    move-object v2, v2

    .line 1429482
    invoke-virtual {v2}, LX/5Lb;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    .line 1429483
    :cond_2
    iget-object v1, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    iget-object v2, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->x:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    iget-object v4, p0, LX/90o;->a:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3, v4}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;Ljava/lang/String;LX/0ta;Ljava/lang/String;)V

    .line 1429484
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    iget-object v1, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->x:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0YN;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1429485
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    .line 1429486
    invoke-static {v0, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->b$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V

    .line 1429487
    :cond_3
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429488
    iget-object v0, p0, LX/90o;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-virtual {v0, v1}, LX/92C;->a(LX/0ta;)V

    goto/16 :goto_0
.end method
