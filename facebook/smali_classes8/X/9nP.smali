.class public LX/9nP;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source ""


# static fields
.field private static a:LX/9nP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/database/sqlite/SQLiteDatabase;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:J


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1537267
    const-string v0, "RKStorage"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 1537268
    const-wide/32 v0, 0x600000

    iput-wide v0, p0, LX/9nP;->d:J

    .line 1537269
    iput-object p1, p0, LX/9nP;->b:Landroid/content/Context;

    .line 1537270
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/9nP;
    .locals 2

    .prologue
    .line 1537274
    sget-object v0, LX/9nP;->a:LX/9nP;

    if-nez v0, :cond_0

    .line 1537275
    new-instance v0, LX/9nP;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9nP;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/9nP;->a:LX/9nP;

    .line 1537276
    :cond_0
    sget-object v0, LX/9nP;->a:LX/9nP;

    return-object v0
.end method

.method private declared-synchronized e()Z
    .locals 2

    .prologue
    .line 1537271
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/9nP;->f()V

    .line 1537272
    iget-object v0, p0, LX/9nP;->b:Landroid/content/Context;

    const-string v1, "RKStorage"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 1537273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()V
    .locals 1

    .prologue
    .line 1537238
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1537239
    iget-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1537240
    const/4 v0, 0x0

    iput-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1537241
    :cond_0
    monitor-exit p0

    return-void

    .line 1537242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1537249
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1537250
    :goto_0
    monitor-exit p0

    return v4

    .line 1537251
    :cond_0
    const/4 v1, 0x0

    .line 1537252
    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_1
    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 1537253
    if-lez v1, :cond_1

    .line 1537254
    :try_start_1
    invoke-direct {p0}, LX/9nP;->e()Z

    .line 1537255
    :cond_1
    invoke-virtual {p0}, LX/9nP;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1537256
    :cond_2
    :try_start_2
    iget-object v1, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v1, :cond_3

    .line 1537257
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1537258
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1537259
    :catch_0
    move-exception v0

    .line 1537260
    const-wide/16 v2, 0x1e

    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1537261
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1537262
    :catch_1
    :try_start_4
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_2

    .line 1537263
    :cond_3
    iget-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;

    iget-wide v2, p0, LX/9nP;->d:J

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->setMaximumSize(J)J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 1537264
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/9nP;->a()Z

    .line 1537265
    iget-object v0, p0, LX/9nP;->c:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1537266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 1537243
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/9nP;->d()V

    .line 1537244
    invoke-direct {p0}, LX/9nP;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1537245
    :cond_0
    monitor-exit p0

    return-void

    .line 1537246
    :catch_0
    :try_start_1
    invoke-direct {p0}, LX/9nP;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1537247
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Clearing and deleting database RKStorage failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1537248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    .line 1537235
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "catalystLocalStorage"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1537236
    monitor-exit p0

    return-void

    .line 1537237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 1537233
    const-string v0, "CREATE TABLE catalystLocalStorage (key TEXT PRIMARY KEY, value TEXT NOT NULL)"

    const v1, -0x5ff8e702

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7cb35801

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1537234
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1537229
    if-eq p2, p3, :cond_0

    .line 1537230
    invoke-direct {p0}, LX/9nP;->e()Z

    .line 1537231
    invoke-virtual {p0, p1}, LX/9nP;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1537232
    :cond_0
    return-void
.end method
