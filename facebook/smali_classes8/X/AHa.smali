.class public abstract LX/AHa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1655733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1655734
    iput-object p1, p0, LX/AHa;->b:Ljava/lang/String;

    .line 1655735
    iput-object p2, p0, LX/AHa;->a:Ljava/lang/String;

    .line 1655736
    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/audience/model/ReplyThread;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1655737
    invoke-virtual {p0}, LX/AHa;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1655738
    const/4 v0, 0x0

    .line 1655739
    :goto_0
    return-object v0

    .line 1655740
    :cond_0
    invoke-virtual {p0}, LX/AHa;->d()LX/0Px;

    move-result-object v0

    .line 1655741
    new-instance v1, LX/7gs;

    invoke-direct {v1}, LX/7gs;-><init>()V

    move-object v1, v1

    .line 1655742
    iget-object v2, p0, LX/AHa;->a:Ljava/lang/String;

    .line 1655743
    iput-object v2, v1, LX/7gs;->a:Ljava/lang/String;

    .line 1655744
    move-object v1, v1

    .line 1655745
    invoke-virtual {p0}, LX/AHa;->b()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v2

    .line 1655746
    iput-object v2, v1, LX/7gs;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1655747
    move-object v1, v1

    .line 1655748
    invoke-virtual {p0}, LX/AHa;->c()Lcom/facebook/audience/model/Reply;

    move-result-object v2

    .line 1655749
    iput-object v2, v1, LX/7gs;->f:Lcom/facebook/audience/model/Reply;

    .line 1655750
    move-object v1, v1

    .line 1655751
    iput-object v0, v1, LX/7gs;->e:LX/0Px;

    .line 1655752
    move-object v0, v1

    .line 1655753
    invoke-virtual {p0}, LX/AHa;->e()Z

    move-result v1

    .line 1655754
    iput-boolean v1, v0, LX/7gs;->c:Z

    .line 1655755
    move-object v0, v0

    .line 1655756
    invoke-virtual {v0}, LX/7gs;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract b()Lcom/facebook/audience/model/AudienceControlData;
.end method

.method public abstract c()Lcom/facebook/audience/model/Reply;
.end method

.method public abstract d()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Z
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 1655757
    iget-object v0, p0, LX/AHa;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AHa;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
