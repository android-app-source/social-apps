.class public final LX/9Ez;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:LX/9F0;


# direct methods
.method public constructor <init>(LX/9F0;)V
    .locals 0

    .prologue
    .line 1457825
    iput-object p1, p0, LX/9Ez;->a:LX/9F0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1457826
    iget-object v0, p0, LX/9Ez;->a:LX/9F0;

    invoke-virtual {v0}, LX/9F0;->d()V

    .line 1457827
    iget-object v0, p0, LX/9Ez;->a:LX/9F0;

    iget-object v0, v0, LX/9F0;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9D5;

    .line 1457828
    iget-object v1, v0, LX/9D5;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0if;

    .line 1457829
    sget-object v2, LX/0ig;->M:LX/0ih;

    move-object v2, v2

    .line 1457830
    const-string p0, "user_granted_audio_permission"

    invoke-virtual {v1, v2, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1457831
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1457832
    iget-object v0, p0, LX/9Ez;->a:LX/9F0;

    invoke-static {v0}, LX/9F0;->i(LX/9F0;)V

    .line 1457833
    iget-object v0, p0, LX/9Ez;->a:LX/9F0;

    iget-object v0, v0, LX/9F0;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9D5;

    .line 1457834
    iget-object p0, v0, LX/9D5;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0if;

    .line 1457835
    sget-object p1, LX/0ig;->M:LX/0ih;

    move-object p1, p1

    .line 1457836
    const-string p2, "user_denied_audio_permission"

    invoke-virtual {p0, p1, p2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1457837
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1457838
    iget-object v0, p0, LX/9Ez;->a:LX/9F0;

    invoke-static {v0}, LX/9F0;->i(LX/9F0;)V

    .line 1457839
    iget-object v0, p0, LX/9Ez;->a:LX/9F0;

    iget-object v0, v0, LX/9F0;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9D5;

    .line 1457840
    iget-object v1, v0, LX/9D5;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0if;

    .line 1457841
    sget-object v2, LX/0ig;->M:LX/0ih;

    move-object v2, v2

    .line 1457842
    const-string p0, "user_cancelled_permissions_dialog"

    invoke-virtual {v1, v2, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1457843
    return-void
.end method
