.class public final LX/9GW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLMedia;

.field public final synthetic b:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLMedia;)V
    .locals 0

    .prologue
    .line 1460006
    iput-object p1, p0, LX/9GW;->b:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    iput-object p2, p0, LX/9GW;->a:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0xc792382

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1460007
    iget-object v1, p0, LX/9GW;->a:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1460008
    iget-object v1, p0, LX/9GW;->b:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->e:LX/1nB;

    iget-object v2, p0, LX/9GW;->a:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    iget-object v2, p0, LX/9GW;->a:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1nB;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1460009
    iget-object v2, p0, LX/9GW;->b:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->h:LX/121;

    sget-object v3, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080e4a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/9GV;

    invoke-direct {v5, p0, v1, p1}, LX/9GV;-><init>(LX/9GW;Landroid/content/Intent;Landroid/view/View;)V

    invoke-virtual {v2, v3, v4, v5}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 1460010
    iget-object v1, p0, LX/9GW;->b:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->h:LX/121;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    iget-object v3, p0, LX/9GW;->b:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->g:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/121;->a(LX/0yY;LX/0gc;)V

    .line 1460011
    :cond_0
    const v1, 0x3360a759

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
