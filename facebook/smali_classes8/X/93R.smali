.class public final LX/93R;
.super LX/93Q;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLAlbum;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLAlbum;)V
    .locals 0
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1433800
    invoke-direct {p0, p1, p2, p3}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1433801
    iput-object p5, p0, LX/93R;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1433802
    iput-object p4, p0, LX/93R;->b:Landroid/content/res/Resources;

    .line 1433803
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1433779
    invoke-super {p0}, LX/93Q;->a()V

    .line 1433780
    iget-object v0, p0, LX/93R;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1433781
    new-instance v1, LX/7lN;

    invoke-direct {v1}, LX/7lN;-><init>()V

    iget-object v2, p0, LX/93R;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    .line 1433782
    iput-object v2, v1, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1433783
    move-object v1, v1

    .line 1433784
    iget-object v2, p0, LX/93R;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v2

    .line 1433785
    iput-object v2, v1, LX/7lN;->b:Ljava/lang/String;

    .line 1433786
    move-object v1, v1

    .line 1433787
    iget-object v2, p0, LX/93R;->b:Landroid/content/res/Resources;

    const v3, 0x7f08131e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1433788
    iput-object v2, v1, LX/7lN;->c:Ljava/lang/String;

    .line 1433789
    move-object v1, v1

    .line 1433790
    invoke-virtual {v1}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v1

    .line 1433791
    new-instance v2, LX/7lP;

    invoke-direct {v2}, LX/7lP;-><init>()V

    const/4 v3, 0x1

    .line 1433792
    iput-boolean v3, v2, LX/7lP;->a:Z

    .line 1433793
    move-object v2, v2

    .line 1433794
    new-instance v3, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LX/93R;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433795
    iput-object v3, v2, LX/7lP;->d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1433796
    move-object v0, v2

    .line 1433797
    invoke-virtual {v0, v1}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1433798
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1433799
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1433778
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "album:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/93R;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
