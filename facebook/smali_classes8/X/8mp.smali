.class public LX/8mp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

.field public c:Landroid/widget/GridView;

.field public d:LX/4m4;

.field public e:LX/8l0;

.field public f:F

.field public g:F

.field public h:Lcom/facebook/stickers/ui/StickerView;

.field public i:Lcom/facebook/stickers/model/Sticker;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;LX/8mf;Landroid/widget/GridView;LX/4m4;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/stickers/ui/IsStickerPreviewDialogEnabled;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;
        .annotation runtime Lcom/facebook/stickers/ui/ForStickerPreview;
        .end annotation
    .end param
    .param p4    # Landroid/widget/GridView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/4m4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1399963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1399964
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/8mp;->a:Z

    .line 1399965
    iput-object p2, p0, LX/8mp;->b:Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

    .line 1399966
    iput-object p5, p0, LX/8mp;->d:LX/4m4;

    .line 1399967
    iput-object p4, p0, LX/8mp;->c:Landroid/widget/GridView;

    .line 1399968
    iget-object v0, p0, LX/8mp;->c:Landroid/widget/GridView;

    new-instance v1, LX/8mm;

    invoke-direct {v1, p0}, LX/8mm;-><init>(LX/8mp;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1399969
    iget-object v0, p0, LX/8mp;->c:Landroid/widget/GridView;

    new-instance v1, LX/8mn;

    invoke-direct {v1, p0}, LX/8mn;-><init>(LX/8mp;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 1399970
    iget-object v0, p0, LX/8mp;->c:Landroid/widget/GridView;

    new-instance v1, LX/8mo;

    invoke-direct {v1, p0}, LX/8mo;-><init>(LX/8mp;)V

    .line 1399971
    new-instance p0, LX/8me;

    invoke-direct {p0, p3, v1}, LX/8me;-><init>(LX/8mf;Landroid/widget/AdapterView$OnItemClickListener;)V

    move-object v1, p0

    .line 1399972
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1399973
    return-void
.end method

.method public static a(LX/8mp;FF)Lcom/facebook/stickers/model/Sticker;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1399961
    invoke-direct {p0, p1, p2}, LX/8mp;->c(FF)I

    move-result v0

    .line 1399962
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    iget-object v2, p0, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v2}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v2

    add-int/2addr v0, v2

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/8mp;FF)Lcom/facebook/stickers/ui/StickerView;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1399974
    invoke-direct {p0, p1, p2}, LX/8mp;->c(FF)I

    move-result v0

    .line 1399975
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/ui/StickerView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(LX/8mp;Lcom/facebook/stickers/model/Sticker;)V
    .locals 6

    .prologue
    .line 1399914
    if-eqz p1, :cond_3

    .line 1399915
    iget-object v0, p0, LX/8mp;->i:Lcom/facebook/stickers/model/Sticker;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/8mp;->i:Lcom/facebook/stickers/model/Sticker;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1399916
    if-nez v0, :cond_3

    .line 1399917
    iput-object p1, p0, LX/8mp;->i:Lcom/facebook/stickers/model/Sticker;

    .line 1399918
    iget-object v0, p0, LX/8mp;->b:Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

    iget-object v1, p0, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1399919
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    .line 1399920
    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-eqz v3, :cond_5

    .line 1399921
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    .line 1399922
    :cond_0
    :goto_1
    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    invoke-static {}, LX/4eB;->newBuilder()LX/4eC;

    move-result-object v3

    const/4 p0, -0x1

    .line 1399923
    iput p0, v3, LX/4eC;->b:I

    .line 1399924
    move-object v3, v3

    .line 1399925
    invoke-virtual {v3}, LX/4eC;->l()LX/4eB;

    move-result-object v3

    .line 1399926
    iput-object v3, v2, LX/1bX;->e:LX/1bZ;

    .line 1399927
    move-object v2, v2

    .line 1399928
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    move-object v2, v2

    .line 1399929
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    if-nez v3, :cond_1

    .line 1399930
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030a9f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1399931
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1399932
    new-instance v4, LX/1Uo;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v5, LX/1Up;->c:LX/1Up;

    invoke-virtual {v4, v5}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v4

    new-instance v5, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0210ee

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    const/16 p1, 0x3e8

    invoke-direct {v5, p0, p1}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1399933
    iput-object v5, v4, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1399934
    move-object v4, v4

    .line 1399935
    invoke-virtual {v4}, LX/1Uo;->u()LX/1af;

    move-result-object v4

    move-object v4, v4

    .line 1399936
    invoke-virtual {v3, v4}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1399937
    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    .line 1399938
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 1399939
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1399940
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    new-instance v4, LX/8CT;

    invoke-direct {v4, v0}, LX/8CT;-><init>(Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;)V

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1399941
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    new-instance v4, LX/8CU;

    invoke-direct {v4, v0}, LX/8CU;-><init>(Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;)V

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1399942
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 1399943
    iget-object v4, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v4}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 1399944
    iget v4, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->f:I

    iget v5, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->f:I

    invoke-virtual {v3, v4, v5}, Landroid/view/Window;->setLayout(II)V

    .line 1399945
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 1399946
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020eb6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1399947
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    invoke-static {v3}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 1399948
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->b()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1399949
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 1399950
    :cond_2
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->e:LX/1bf;

    invoke-static {v3, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1399951
    iput-object v2, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->e:LX/1bf;

    .line 1399952
    invoke-static {v0}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d(Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;)V

    .line 1399953
    iget-object v3, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1399954
    iget-object v4, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->b:LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->o()LX/1Ad;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    new-instance v5, LX/8CV;

    invoke-direct {v5, v0, v2}, LX/8CV;-><init>(Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;LX/1bf;)V

    invoke-virtual {v4, v5}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    move-object v4, v4

    .line 1399955
    invoke-virtual {v3, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1399956
    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1399957
    :cond_5
    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    if-eqz v3, :cond_6

    .line 1399958
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    goto/16 :goto_1

    .line 1399959
    :cond_6
    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v3, :cond_0

    .line 1399960
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    goto/16 :goto_1
.end method

.method private c(FF)I
    .locals 3

    .prologue
    .line 1399906
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1399907
    iget-object v0, p0, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/ui/StickerView;

    .line 1399908
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1399909
    invoke-virtual {v0, v2}, Lcom/facebook/stickers/ui/StickerView;->getHitRect(Landroid/graphics/Rect;)V

    .line 1399910
    invoke-virtual {v0, p1, p2}, Lcom/facebook/stickers/ui/StickerView;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399911
    :goto_1
    return v1

    .line 1399912
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1399913
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method
