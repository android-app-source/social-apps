.class public final LX/9HZ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9Ha;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLComment;

.field public b:Z

.field public c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public d:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/9Ha;


# direct methods
.method public constructor <init>(LX/9Ha;)V
    .locals 1

    .prologue
    .line 1461457
    iput-object p1, p0, LX/9HZ;->e:LX/9Ha;

    .line 1461458
    move-object v0, p1

    .line 1461459
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1461460
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1461461
    const-string v0, "CommentBodyComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1461462
    if-ne p0, p1, :cond_1

    .line 1461463
    :cond_0
    :goto_0
    return v0

    .line 1461464
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1461465
    goto :goto_0

    .line 1461466
    :cond_3
    check-cast p1, LX/9HZ;

    .line 1461467
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1461468
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1461469
    if-eq v2, v3, :cond_0

    .line 1461470
    iget-object v2, p0, LX/9HZ;->a:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9HZ;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p1, LX/9HZ;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1461471
    goto :goto_0

    .line 1461472
    :cond_5
    iget-object v2, p1, LX/9HZ;->a:Lcom/facebook/graphql/model/GraphQLComment;

    if-nez v2, :cond_4

    .line 1461473
    :cond_6
    iget-boolean v2, p0, LX/9HZ;->b:Z

    iget-boolean v3, p1, LX/9HZ;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1461474
    goto :goto_0

    .line 1461475
    :cond_7
    iget-object v2, p0, LX/9HZ;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/9HZ;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v3, p1, LX/9HZ;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v2, v3}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1461476
    goto :goto_0

    .line 1461477
    :cond_9
    iget-object v2, p1, LX/9HZ;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-nez v2, :cond_8

    .line 1461478
    :cond_a
    iget-object v2, p0, LX/9HZ;->d:LX/1Pq;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/9HZ;->d:LX/1Pq;

    iget-object v3, p1, LX/9HZ;->d:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1461479
    goto :goto_0

    .line 1461480
    :cond_b
    iget-object v2, p1, LX/9HZ;->d:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
