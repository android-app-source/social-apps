.class public final LX/A3B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:D

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1615651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v13, 0x0

    const-wide/16 v4, 0x0

    const/4 v12, 0x0

    .line 1615652
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 1615653
    iget-object v2, p0, LX/A3B;->a:LX/0Px;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1615654
    iget-object v3, p0, LX/A3B;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1615655
    iget-object v3, p0, LX/A3B;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1615656
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1615657
    invoke-virtual {v0, v13, v2}, LX/186;->b(II)V

    .line 1615658
    iget-wide v2, p0, LX/A3B;->b:D

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1615659
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1615660
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1615661
    const/4 v7, 0x4

    iget-wide v8, p0, LX/A3B;->e:D

    move-object v6, v0

    move-wide v10, v4

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 1615662
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 1615663
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 1615664
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1615665
    invoke-virtual {v3, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1615666
    new-instance v2, LX/15i;

    move-object v4, v12

    move-object v5, v12

    move v6, v1

    move-object v7, v12

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1615667
    new-instance v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    invoke-direct {v0, v2}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;-><init>(LX/15i;)V

    .line 1615668
    return-object v0
.end method
