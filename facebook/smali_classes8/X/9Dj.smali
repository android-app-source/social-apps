.class public LX/9Dj;
.super LX/62O;
.source ""


# instance fields
.field private a:LX/1CW;

.field public b:LX/0Uh;

.field public c:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;LX/1CW;LX/0Uh;)V
    .locals 0
    .param p1    # Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1DI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456127
    invoke-direct {p0, p1, p2}, LX/62O;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    .line 1456128
    iput-object p3, p0, LX/9Dj;->a:LX/1CW;

    .line 1456129
    iput-object p4, p0, LX/9Dj;->b:LX/0Uh;

    .line 1456130
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/ServiceException;Landroid/content/res/Resources;LX/1DI;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1456095
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1456096
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_2

    .line 1456097
    iget-object v0, p0, LX/9Dj;->c:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    if-nez v0, :cond_0

    .line 1456098
    invoke-static {}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->newBuilder()LX/62Q;

    move-result-object v0

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    .line 1456099
    iput-object v1, v0, LX/62Q;->a:LX/1lD;

    .line 1456100
    move-object v0, v0

    .line 1456101
    const v1, 0x7f081119

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1456102
    iput-object v1, v0, LX/62Q;->b:Ljava/lang/String;

    .line 1456103
    move-object v0, v0

    .line 1456104
    const v1, 0x7f08111a

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1456105
    iput-object v1, v0, LX/62Q;->c:Ljava/lang/String;

    .line 1456106
    move-object v0, v0

    .line 1456107
    const v1, 0x7f020a88

    .line 1456108
    iput v1, v0, LX/62Q;->d:I

    .line 1456109
    move-object v0, v0

    .line 1456110
    invoke-virtual {v0}, LX/62Q;->a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-result-object v0

    iput-object v0, p0, LX/9Dj;->c:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1456111
    :cond_0
    iget-object v0, p0, LX/9Dj;->c:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1456112
    iget-object v1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    move-object v0, v1

    .line 1456113
    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/9Dj;->b:LX/0Uh;

    const/16 v1, 0x3b7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1456114
    iget-object v0, p0, LX/9Dj;->c:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    .line 1456115
    iput-object v1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    .line 1456116
    :cond_1
    iget-object v0, p0, LX/9Dj;->c:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-object v0, v0

    .line 1456117
    :goto_0
    iput-object v0, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1456118
    invoke-static {p0}, LX/62O;->e(LX/62O;)V

    .line 1456119
    return-void

    .line 1456120
    :cond_2
    iget-object v0, p0, LX/9Dj;->a:LX/1CW;

    invoke-virtual {v0, p1, v2, v2}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 1456121
    invoke-static {}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->newBuilder()LX/62Q;

    move-result-object v1

    sget-object v2, LX/1lD;->ERROR:LX/1lD;

    .line 1456122
    iput-object v2, v1, LX/62Q;->a:LX/1lD;

    .line 1456123
    move-object v1, v1

    .line 1456124
    iput-object v0, v1, LX/62Q;->b:Ljava/lang/String;

    .line 1456125
    move-object v0, v1

    .line 1456126
    invoke-virtual {v0}, LX/62Q;->a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-result-object v0

    goto :goto_0
.end method
