.class public LX/9I4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/3mL;

.field public final b:LX/9I8;


# direct methods
.method public constructor <init>(LX/3mL;LX/9I8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462502
    iput-object p1, p0, LX/9I4;->a:LX/3mL;

    .line 1462503
    iput-object p2, p0, LX/9I4;->b:LX/9I8;

    .line 1462504
    return-void
.end method

.method public static a(LX/0QB;)LX/9I4;
    .locals 5

    .prologue
    .line 1462490
    const-class v1, LX/9I4;

    monitor-enter v1

    .line 1462491
    :try_start_0
    sget-object v0, LX/9I4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462492
    sput-object v2, LX/9I4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462493
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462494
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462495
    new-instance p0, LX/9I4;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/9I8;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9I8;

    invoke-direct {p0, v3, v4}, LX/9I4;-><init>(LX/3mL;LX/9I8;)V

    .line 1462496
    move-object v0, p0

    .line 1462497
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462498
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9I4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462499
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462500
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
