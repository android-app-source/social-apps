.class public final LX/99x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/1CF;


# direct methods
.method public constructor <init>(LX/1CF;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1447955
    iput-object p1, p0, LX/99x;->d:LX/1CF;

    iput-object p2, p0, LX/99x;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/99x;->b:Ljava/lang/String;

    iput-object p4, p0, LX/99x;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1447956
    iget-object v0, p0, LX/99x;->d:LX/1CF;

    iget-object v0, v0, LX/1CF;->g:LX/03V;

    const-string v1, "optimistic_fetch_failure"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/99x;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", postId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/99x;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1447957
    iget-object v0, p0, LX/99x;->d:LX/1CF;

    iget-object v0, v0, LX/1CF;->m:LX/0qn;

    iget-object v1, p0, LX/99x;->a:Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1447958
    iget-object v0, p0, LX/99x;->d:LX/1CF;

    iget-object v0, v0, LX/1CF;->d:LX/0qm;

    iget-object v1, p0, LX/99x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qm;->a(Ljava/lang/String;)V

    .line 1447959
    iget-object v0, p0, LX/99x;->d:LX/1CF;

    iget-object v0, v0, LX/1CF;->d:LX/0qm;

    iget-object v1, p0, LX/99x;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qm;->b(Ljava/lang/String;)Z

    .line 1447960
    iget-object v0, p0, LX/99x;->d:LX/1CF;

    iget-object v0, v0, LX/1CF;->c:LX/1CH;

    invoke-interface {v0}, LX/1CH;->a()V

    .line 1447961
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1447962
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1447963
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1447964
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 1447965
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1447966
    iget-object v1, p0, LX/99x;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1447967
    invoke-static {v1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    .line 1447968
    iput-object v0, v2, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1447969
    move-object v2, v2

    .line 1447970
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v3

    .line 1447971
    iput-object v3, v2, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1447972
    move-object v2, v2

    .line 1447973
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object v0, v2

    .line 1447974
    iget-object v1, p0, LX/99x;->d:LX/1CF;

    iget-object v1, v1, LX/1CF;->m:LX/0qn;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v1, v0, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1447975
    iget-object v1, p0, LX/99x;->d:LX/1CF;

    iget-object v2, p0, LX/99x;->b:Ljava/lang/String;

    iget-object v3, p0, LX/99x;->c:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, LX/1CF;->a$redex0(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1447976
    iget-object v1, p0, LX/99x;->d:LX/1CF;

    iget-object v1, v1, LX/1CF;->c:LX/1CH;

    invoke-interface {v1, v0}, LX/1CH;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1447977
    iget-object v0, p0, LX/99x;->d:LX/1CF;

    iget-object v0, v0, LX/1CF;->c:LX/1CH;

    invoke-interface {v0}, LX/1CH;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1447978
    :goto_0
    return-void

    .line 1447979
    :catch_0
    move-exception v0

    .line 1447980
    invoke-virtual {p0, v0}, LX/99x;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
