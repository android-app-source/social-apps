.class public final LX/9YX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1506357
    const/16 v19, 0x0

    .line 1506358
    const/16 v18, 0x0

    .line 1506359
    const/16 v17, 0x0

    .line 1506360
    const/16 v16, 0x0

    .line 1506361
    const/4 v15, 0x0

    .line 1506362
    const/4 v14, 0x0

    .line 1506363
    const/4 v13, 0x0

    .line 1506364
    const/4 v12, 0x0

    .line 1506365
    const/4 v11, 0x0

    .line 1506366
    const/4 v10, 0x0

    .line 1506367
    const/4 v9, 0x0

    .line 1506368
    const/4 v8, 0x0

    .line 1506369
    const/4 v7, 0x0

    .line 1506370
    const/4 v6, 0x0

    .line 1506371
    const/4 v5, 0x0

    .line 1506372
    const/4 v4, 0x0

    .line 1506373
    const/4 v3, 0x0

    .line 1506374
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    .line 1506375
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1506376
    const/4 v3, 0x0

    .line 1506377
    :goto_0
    return v3

    .line 1506378
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1506379
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_13

    .line 1506380
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v20

    .line 1506381
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1506382
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    if-eqz v20, :cond_1

    .line 1506383
    const-string v21, "__type__"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_2

    const-string v21, "__typename"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1506384
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v19

    goto :goto_1

    .line 1506385
    :cond_3
    const-string v21, "action_type"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 1506386
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto :goto_1

    .line 1506387
    :cond_4
    const-string v21, "call_to_action"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1506388
    invoke-static/range {p0 .. p1}, LX/8FF;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1506389
    :cond_5
    const-string v21, "callout_text"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1506390
    invoke-static/range {p0 .. p1}, LX/9Yb;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1506391
    :cond_6
    const-string v21, "content_type"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1506392
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    goto/16 :goto_1

    .line 1506393
    :cond_7
    const-string v21, "cta_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1506394
    invoke-static/range {p0 .. p1}, LX/8En;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1506395
    :cond_8
    const-string v21, "description"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1506396
    invoke-static/range {p0 .. p1}, LX/9YS;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1506397
    :cond_9
    const-string v21, "name_propercase"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1506398
    invoke-static/range {p0 .. p1}, LX/9Yj;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1506399
    :cond_a
    const-string v21, "name_uppercase"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1506400
    invoke-static/range {p0 .. p1}, LX/9Yk;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1506401
    :cond_b
    const-string v21, "optional_id"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1506402
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 1506403
    :cond_c
    const-string v21, "page"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 1506404
    invoke-static/range {p0 .. p1}, LX/9YW;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1506405
    :cond_d
    const-string v21, "preview_action_type"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 1506406
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1506407
    :cond_e
    const-string v21, "preview_optional_id"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 1506408
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1506409
    :cond_f
    const-string v21, "preview_text"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 1506410
    invoke-static/range {p0 .. p1}, LX/9Yo;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1506411
    :cond_10
    const-string v21, "reaction_surface"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 1506412
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1506413
    :cond_11
    const-string v21, "tab_type"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_12

    .line 1506414
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto/16 :goto_1

    .line 1506415
    :cond_12
    const-string v21, "uri"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 1506416
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1506417
    :cond_13
    const/16 v20, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1506418
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1506419
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1506420
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1506421
    const/16 v17, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1506422
    const/16 v16, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1506423
    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1506424
    const/4 v14, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1506425
    const/4 v13, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1506426
    const/16 v12, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1506427
    const/16 v11, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1506428
    const/16 v10, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1506429
    const/16 v9, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1506430
    const/16 v8, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1506431
    const/16 v7, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1506432
    const/16 v6, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1506433
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1506434
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1506435
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1506436
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1506437
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1506438
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/9YX;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1506439
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1506440
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1506441
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1506442
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1506443
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1506444
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1506445
    invoke-static {p0, p1}, LX/9YX;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1506446
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1506447
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1506448
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1506449
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1506450
    if-eqz v0, :cond_0

    .line 1506451
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506452
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1506453
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1506454
    if-eqz v0, :cond_1

    .line 1506455
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506456
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506457
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506458
    if-eqz v0, :cond_2

    .line 1506459
    const-string v1, "call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506460
    invoke-static {p0, v0, p2, p3}, LX/8FF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1506461
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506462
    if-eqz v0, :cond_3

    .line 1506463
    const-string v1, "callout_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506464
    invoke-static {p0, v0, p2}, LX/9Yb;->a(LX/15i;ILX/0nX;)V

    .line 1506465
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1506466
    if-eqz v0, :cond_4

    .line 1506467
    const-string v0, "content_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506468
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506469
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506470
    if-eqz v0, :cond_5

    .line 1506471
    const-string v1, "cta_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506472
    invoke-static {p0, v0, p2, p3}, LX/8En;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1506473
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506474
    if-eqz v0, :cond_6

    .line 1506475
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506476
    invoke-static {p0, v0, p2}, LX/9YS;->a(LX/15i;ILX/0nX;)V

    .line 1506477
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506478
    if-eqz v0, :cond_7

    .line 1506479
    const-string v1, "name_propercase"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506480
    invoke-static {p0, v0, p2}, LX/9Yj;->a(LX/15i;ILX/0nX;)V

    .line 1506481
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506482
    if-eqz v0, :cond_8

    .line 1506483
    const-string v1, "name_uppercase"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506484
    invoke-static {p0, v0, p2}, LX/9Yk;->a(LX/15i;ILX/0nX;)V

    .line 1506485
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1506486
    if-eqz v0, :cond_9

    .line 1506487
    const-string v1, "optional_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506488
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506489
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506490
    if-eqz v0, :cond_a

    .line 1506491
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506492
    invoke-static {p0, v0, p2, p3}, LX/9YW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1506493
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1506494
    if-eqz v0, :cond_b

    .line 1506495
    const-string v1, "preview_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506496
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506497
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1506498
    if-eqz v0, :cond_c

    .line 1506499
    const-string v1, "preview_optional_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506500
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506501
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1506502
    if-eqz v0, :cond_d

    .line 1506503
    const-string v1, "preview_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506504
    invoke-static {p0, v0, p2}, LX/9Yo;->a(LX/15i;ILX/0nX;)V

    .line 1506505
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1506506
    if-eqz v0, :cond_e

    .line 1506507
    const-string v1, "reaction_surface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506508
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506509
    :cond_e
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1506510
    if-eqz v0, :cond_f

    .line 1506511
    const-string v0, "tab_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506512
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506513
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1506514
    if-eqz v0, :cond_10

    .line 1506515
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1506516
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1506517
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1506518
    return-void
.end method
