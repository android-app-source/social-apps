.class public final LX/9B2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9B3;

.field private b:LX/1zt;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(LX/9B3;LX/1zt;IZ)V
    .locals 0

    .prologue
    .line 1451025
    iput-object p1, p0, LX/9B2;->a:LX/9B3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1451026
    iput-object p2, p0, LX/9B2;->b:LX/1zt;

    .line 1451027
    iput p3, p0, LX/9B2;->c:I

    .line 1451028
    iput-boolean p4, p0, LX/9B2;->d:Z

    .line 1451029
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1451035
    iget-object v0, p0, LX/9B2;->a:LX/9B3;

    iget-object v0, v0, LX/9B3;->n:[Z

    iget-object v1, p0, LX/9B2;->a:LX/9B3;

    iget-object v2, p0, LX/9B2;->b:LX/1zt;

    invoke-static {v1, v2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    .line 1451036
    iget-object v0, p0, LX/9B2;->a:LX/9B3;

    iget-object v1, p0, LX/9B2;->b:LX/1zt;

    .line 1451037
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    if-eqz v2, :cond_0

    .line 1451038
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    .line 1451039
    iget-object v3, v2, LX/9BK;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object p0, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    iget-object v3, v2, LX/9BK;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v3, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->y:Landroid/util/SparseArray;

    .line 1451040
    iget v0, v1, LX/1zt;->e:I

    move v0, v0

    .line 1451041
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, LX/9BT;->b(I)V

    .line 1451042
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1451043
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1451044
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1451045
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1451046
    if-nez v0, :cond_0

    .line 1451047
    iget-object v0, p0, LX/9B2;->a:LX/9B3;

    iget-object v1, p0, LX/9B2;->b:LX/1zt;

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null feedback received"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, LX/9B3;->a$redex0(LX/9B3;LX/1zt;Ljava/lang/Throwable;)V

    .line 1451048
    :goto_0
    return-void

    .line 1451049
    :cond_0
    iget-object v1, p0, LX/9B2;->a:LX/9B3;

    iget-boolean v1, v1, LX/9B3;->s:Z

    if-eqz v1, :cond_1

    .line 1451050
    iget-object v1, p0, LX/9B2;->a:LX/9B3;

    iget-object v1, v1, LX/9B3;->e:LX/1My;

    iget-object v2, p0, LX/9B2;->a:LX/9B3;

    iget-object v3, p0, LX/9B2;->b:LX/1zt;

    invoke-static {v2, v3}, LX/9B3;->j(LX/9B3;LX/1zt;)LX/0TF;

    move-result-object v2

    iget-object v3, p0, LX/9B2;->a:LX/9B3;

    invoke-virtual {v3}, LX/9B3;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1451051
    :cond_1
    iget-object v1, p0, LX/9B2;->a:LX/9B3;

    iget-object v2, p0, LX/9B2;->b:LX/1zt;

    invoke-static {v1, v0, v2}, LX/9B3;->a$redex0(LX/9B3;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;)V

    .line 1451052
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1451053
    iget-object v1, p0, LX/9B2;->a:LX/9B3;

    iget-object v2, p0, LX/9B2;->b:LX/1zt;

    .line 1451054
    iput-object v2, v1, LX/9B3;->l:LX/1zt;

    .line 1451055
    iget-object v1, p0, LX/9B2;->a:LX/9B3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v0

    .line 1451056
    iput-object v0, v1, LX/9B3;->m:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 1451057
    :cond_2
    iget-object v0, p0, LX/9B2;->a:LX/9B3;

    iget-object v1, p0, LX/9B2;->b:LX/1zt;

    .line 1451058
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    if-eqz v2, :cond_3

    .line 1451059
    iget-object v2, v0, LX/9B3;->q:LX/9BK;

    .line 1451060
    iget-object v0, v2, LX/9BK;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-static {v0, v1}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->c$redex0(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/1zt;)V

    .line 1451061
    :cond_3
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1451030
    iget-object v0, p0, LX/9B2;->a:LX/9B3;

    iget-object v0, v0, LX/9B3;->n:[Z

    iget-object v1, p0, LX/9B2;->a:LX/9B3;

    iget-object v2, p0, LX/9B2;->b:LX/1zt;

    invoke-static {v1, v2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v1

    aput-boolean v3, v0, v1

    .line 1451031
    iget-boolean v0, p0, LX/9B2;->d:Z

    if-eqz v0, :cond_0

    .line 1451032
    iget-object v0, p0, LX/9B2;->a:LX/9B3;

    iget-object v1, p0, LX/9B2;->b:LX/1zt;

    iget v2, p0, LX/9B2;->c:I

    invoke-virtual {v0, v1, v2, v3}, LX/9B3;->a(LX/1zt;IZ)V

    .line 1451033
    :cond_0
    iget-object v0, p0, LX/9B2;->a:LX/9B3;

    iget-object v1, p0, LX/9B2;->b:LX/1zt;

    invoke-static {v0, v1, p1}, LX/9B3;->a$redex0(LX/9B3;LX/1zt;Ljava/lang/Throwable;)V

    .line 1451034
    return-void
.end method
