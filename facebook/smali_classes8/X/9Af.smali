.class public LX/9Af;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9Ae;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Adapter::",
        "LX/1OO;",
        ":",
        "LX/1Qr;",
        ">",
        "Ljava/lang/Object;",
        "LX/9Ae",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/1OO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TAdapter;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(LX/1OO;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TAdapter;)V"
        }
    .end annotation

    .prologue
    const/high16 v0, -0x80000000

    .line 1450294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1450295
    iput v0, p0, LX/9Af;->b:I

    .line 1450296
    iput v0, p0, LX/9Af;->c:I

    .line 1450297
    iput-object p1, p0, LX/9Af;->a:LX/1OO;

    .line 1450298
    return-void
.end method

.method public static c(LX/9Af;IILX/1Jt;)V
    .locals 3

    .prologue
    .line 1450276
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    invoke-static {p1, p2}, LX/9Af;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9Af;->a:LX/1OO;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1450277
    if-eqz v0, :cond_2

    .line 1450278
    invoke-direct {p0}, LX/9Af;->d()V

    .line 1450279
    :cond_1
    :goto_1
    return-void

    .line 1450280
    :cond_2
    invoke-interface {p3}, LX/1Jt;->a()I

    move-result v0

    .line 1450281
    sub-int v1, p1, v0

    .line 1450282
    sub-int v0, p2, v0

    .line 1450283
    if-gez v1, :cond_5

    if-gez v0, :cond_5

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 1450284
    if-eqz v2, :cond_3

    .line 1450285
    invoke-direct {p0}, LX/9Af;->d()V

    goto :goto_1

    .line 1450286
    :cond_3
    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1450287
    iget-object v2, p0, LX/9Af;->a:LX/1OO;

    invoke-interface {v2}, LX/1OP;->ij_()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1450288
    :try_start_0
    iget-object v0, p0, LX/9Af;->a:LX/1OO;

    check-cast v0, LX/1Qr;

    invoke-interface {v0, v1}, LX/1Qr;->h_(I)I

    move-result v0

    iput v0, p0, LX/9Af;->b:I

    .line 1450289
    iget-object v0, p0, LX/9Af;->a:LX/1OO;

    check-cast v0, LX/1Qr;

    invoke-interface {v0, v2}, LX/1Qr;->h_(I)I

    move-result v0

    iput v0, p0, LX/9Af;->c:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1450290
    iget v0, p0, LX/9Af;->b:I

    iget v1, p0, LX/9Af;->c:I

    if-gt v0, v1, :cond_6

    iget v0, p0, LX/9Af;->b:I

    invoke-static {p0, v0}, LX/9Af;->c(LX/9Af;I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, LX/9Af;->c:I

    invoke-static {p0, v0}, LX/9Af;->c(LX/9Af;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 1450291
    if-nez v0, :cond_1

    .line 1450292
    invoke-direct {p0}, LX/9Af;->d()V

    goto :goto_1

    .line 1450293
    :catch_0
    invoke-direct {p0}, LX/9Af;->d()V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static c(II)Z
    .locals 1

    .prologue
    .line 1450275
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/9Af;I)Z
    .locals 1

    .prologue
    .line 1450299
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/9Af;->a:LX/1OO;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 1450272
    iput v0, p0, LX/9Af;->b:I

    .line 1450273
    iput v0, p0, LX/9Af;->c:I

    .line 1450274
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1450271
    iget v0, p0, LX/9Af;->b:I

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1450259
    const/4 v1, 0x0

    .line 1450260
    invoke-static {p0, p1}, LX/9Af;->c(LX/9Af;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1450261
    :goto_0
    return-object v0

    .line 1450262
    :cond_0
    iget-object v0, p0, LX/9Af;->a:LX/1OO;

    check-cast v0, LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->m_(I)I

    move-result v0

    .line 1450263
    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_1

    move-object v0, v1

    .line 1450264
    goto :goto_0

    .line 1450265
    :cond_1
    iget-object v2, p0, LX/9Af;->a:LX/1OO;

    invoke-interface {v2, v0}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1450266
    instance-of v2, v0, LX/1Rk;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 1450267
    goto :goto_0

    .line 1450268
    :cond_2
    check-cast v0, LX/1Rk;

    .line 1450269
    iget-object v1, v0, LX/1Rk;->d:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v0, v1

    .line 1450270
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1450258
    iget v0, p0, LX/9Af;->c:I

    return v0
.end method
