.class public final LX/8uz;
.super LX/1cr;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field private final c:I

.field private final d:Landroid/graphics/Rect;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1415856
    iput-object p1, p0, LX/8uz;->b:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1415857
    invoke-direct {p0, p2}, LX/1cr;-><init>(Landroid/view/View;)V

    .line 1415858
    const/4 v0, 0x1

    iput v0, p0, LX/8uz;->c:I

    .line 1415859
    iput-object p3, p0, LX/8uz;->d:Landroid/graphics/Rect;

    .line 1415860
    iput-object p4, p0, LX/8uz;->e:Ljava/lang/String;

    .line 1415861
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 3

    .prologue
    .line 1415853
    iget-object v0, p0, LX/8uz;->d:Landroid/graphics/Rect;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1415854
    const/4 v0, 0x1

    .line 1415855
    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method public final a(ILX/3sp;)V
    .locals 1

    .prologue
    .line 1415848
    iget-object v0, p0, LX/8uz;->d:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 1415849
    iget-object v0, p0, LX/8uz;->e:Ljava/lang/String;

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 1415850
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1415851
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/3sp;->f(Z)V

    .line 1415852
    return-void
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1415862
    iget-object v0, p0, LX/8uz;->e:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1415863
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1415846
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1415847
    return-void
.end method

.method public final b(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1415843
    if-ne p1, v0, :cond_0

    .line 1415844
    iget-object v1, p0, LX/8uz;->b:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->q(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1415845
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
