.class public LX/8vW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8vV;


# instance fields
.field public a:LX/8vV;

.field private b:LX/8vj;


# direct methods
.method public constructor <init>(LX/8vj;)V
    .locals 0

    .prologue
    .line 1417091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1417092
    iput-object p1, p0, LX/8vW;->b:LX/8vj;

    .line 1417093
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ActionMode;IJZ)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1417094
    iget-object v1, p0, LX/8vW;->a:LX/8vV;

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-interface/range {v1 .. v6}, LX/8vV;->a(Landroid/view/ActionMode;IJZ)V

    .line 1417095
    iget-object v0, p0, LX/8vW;->b:LX/8vj;

    .line 1417096
    iget v1, v0, LX/8vj;->e:I

    move v0, v1

    .line 1417097
    if-nez v0, :cond_0

    .line 1417098
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 1417099
    :cond_0
    return-void
.end method

.method public final onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1417100
    iget-object v0, p0, LX/8vW;->a:LX/8vV;

    invoke-interface {v0, p1, p2}, LX/8vV;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1417101
    iget-object v1, p0, LX/8vW;->a:LX/8vV;

    invoke-interface {v1, p1, p2}, LX/8vV;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1417102
    iget-object v1, p0, LX/8vW;->b:LX/8vj;

    invoke-virtual {v1, v0}, LX/8vj;->setLongClickable(Z)V

    .line 1417103
    const/4 v0, 0x1

    .line 1417104
    :cond_0
    return v0
.end method

.method public final onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1417105
    iget-object v0, p0, LX/8vW;->a:LX/8vV;

    invoke-interface {v0, p1}, LX/8vV;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 1417106
    iget-object v0, p0, LX/8vW;->b:LX/8vj;

    const/4 v1, 0x0

    iput-object v1, v0, LX/8vj;->c:Ljava/lang/Object;

    .line 1417107
    iget-object v0, p0, LX/8vW;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->a()V

    .line 1417108
    iget-object v0, p0, LX/8vW;->b:LX/8vj;

    iput-boolean v2, v0, LX/8vj;->aj:Z

    .line 1417109
    iget-object v0, p0, LX/8vW;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vi;->o()V

    .line 1417110
    iget-object v0, p0, LX/8vW;->b:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->requestLayout()V

    .line 1417111
    iget-object v0, p0, LX/8vW;->b:LX/8vj;

    invoke-virtual {v0, v2}, LX/8vj;->setLongClickable(Z)V

    .line 1417112
    return-void
.end method

.method public final onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1417113
    iget-object v0, p0, LX/8vW;->a:LX/8vV;

    invoke-interface {v0, p1, p2}, LX/8vV;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
