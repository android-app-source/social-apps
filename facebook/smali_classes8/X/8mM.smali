.class public final LX/8mM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8ma;

.field public final synthetic b:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic c:Lcom/facebook/stickers/store/StickerStoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStoreFragment;LX/8ma;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1398787
    iput-object p1, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iput-object p2, p0, LX/8mM;->a:LX/8ma;

    iput-object p3, p0, LX/8mM;->b:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1398788
    iget-object v0, p0, LX/8mM;->a:LX/8ma;

    iget-object v1, p0, LX/8mM;->b:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v1}, LX/8ma;->add(Ljava/lang/Object;)V

    .line 1398789
    iget-object v0, p0, LX/8mM;->b:Lcom/facebook/stickers/model/StickerPack;

    .line 1398790
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 1398791
    iget-object v0, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4m4;

    invoke-virtual {v1, v0}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1398792
    iget-object v0, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    iget-object v1, p0, LX/8mM;->b:Lcom/facebook/stickers/model/StickerPack;

    .line 1398793
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1398794
    iget-object v2, p0, LX/8mM;->b:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1398795
    :goto_0
    iget-object v0, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->n:LX/1CX;

    iget-object v1, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v1

    iget-object v2, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 1398796
    iput-object v2, v1, LX/4mn;->b:Ljava/lang/String;

    .line 1398797
    move-object v1, v1

    .line 1398798
    const v2, 0x7f080039

    invoke-virtual {v1, v2}, LX/4mn;->b(I)LX/4mn;

    move-result-object v1

    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1398799
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    const-string v1, "Deleting downloaded sticker pack failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1398800
    iget-object v0, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->d:LX/03V;

    sget-object v1, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Deleting downloaded sticker pack failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1398801
    return-void

    .line 1398802
    :cond_0
    iget-object v0, p0, LX/8mM;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    iget-object v1, p0, LX/8mM;->b:Lcom/facebook/stickers/model/StickerPack;

    .line 1398803
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1398804
    iget-object v2, p0, LX/8mM;->b:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1398786
    return-void
.end method
