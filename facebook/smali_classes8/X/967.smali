.class public LX/967;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/189;

.field public final c:LX/20i;

.field public final d:LX/0Zb;

.field public final e:LX/17V;

.field private final f:LX/1Ck;

.field public final g:LX/3iV;

.field private final h:LX/1yp;

.field public final i:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1438226
    const-class v0, LX/967;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/967;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/189;LX/20i;LX/0Zb;LX/17V;LX/1Ck;LX/3iV;LX/1yp;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1438216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1438217
    iput-object p1, p0, LX/967;->b:LX/189;

    .line 1438218
    iput-object p2, p0, LX/967;->c:LX/20i;

    .line 1438219
    iput-object p3, p0, LX/967;->d:LX/0Zb;

    .line 1438220
    iput-object p4, p0, LX/967;->e:LX/17V;

    .line 1438221
    iput-object p5, p0, LX/967;->f:LX/1Ck;

    .line 1438222
    iput-object p6, p0, LX/967;->g:LX/3iV;

    .line 1438223
    iput-object p7, p0, LX/967;->h:LX/1yp;

    .line 1438224
    iput-object p8, p0, LX/967;->i:LX/03V;

    .line 1438225
    return-void
.end method

.method public static a(LX/0QB;)LX/967;
    .locals 1

    .prologue
    .line 1438215
    invoke-static {p0}, LX/967;->b(LX/0QB;)LX/967;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/util/Pair;
    .locals 5
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1438199
    if-nez p3, :cond_1

    .line 1438200
    :cond_0
    :goto_0
    return-object v0

    .line 1438201
    :cond_1
    const/4 v1, 0x0

    .line 1438202
    if-eqz p2, :cond_5

    .line 1438203
    invoke-static {p3}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 1438204
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_2

    const v3, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    if-eq v3, v4, :cond_6

    .line 1438205
    :cond_2
    :goto_1
    move-object v2, v1

    .line 1438206
    if-nez p4, :cond_3

    invoke-static {p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1438207
    :goto_2
    if-eqz v2, :cond_4

    .line 1438208
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1438209
    :cond_3
    invoke-virtual {p4, p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    goto :goto_2

    .line 1438210
    :cond_4
    if-nez p2, :cond_0

    .line 1438211
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, v1}, LX/967;->a(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 1438212
    :cond_5
    invoke-static {p3}, LX/1yp;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 1438213
    :cond_6
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v1, v2

    .line 1438214
    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/967;
    .locals 9

    .prologue
    .line 1438197
    new-instance v0, LX/967;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v1

    check-cast v1, LX/189;

    invoke-static {p0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v2

    check-cast v2, LX/20i;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v6

    check-cast v6, LX/3iV;

    invoke-static {p0}, LX/1yp;->a(LX/0QB;)LX/1yp;

    move-result-object v7

    check-cast v7, LX/1yp;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v0 .. v8}, LX/967;-><init>(LX/189;LX/20i;LX/0Zb;LX/17V;LX/1Ck;LX/3iV;LX/1yp;LX/03V;)V

    .line 1438198
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;ZJLjava/lang/String;Ljava/lang/String;LX/1L9;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;ZJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1438152
    if-nez p1, :cond_1

    .line 1438153
    :cond_0
    :goto_0
    return-void

    .line 1438154
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1438155
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1438156
    iget-object v2, p0, LX/967;->b:LX/189;

    iget-object v3, p0, LX/967;->c:LX/20i;

    invoke-virtual {v3}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v2, p1, v3, p2}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1438157
    invoke-virtual {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1438158
    invoke-virtual {p1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 1438159
    invoke-static {v3}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    .line 1438160
    if-nez v5, :cond_3

    .line 1438161
    iget-object v3, p0, LX/967;->i:LX/03V;

    sget-object v4, LX/967;->a:Ljava/lang/String;

    const-string v7, "Feedbackable should either be a FeedUnit or it\'s root should be a FeedUnit"

    invoke-virtual {v3, v4, v7}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1438162
    :cond_2
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1438163
    new-instance v4, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v7

    move-object/from16 v0, p6

    move-object/from16 v1, p5

    invoke-direct {v4, v7, v0, v1}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 1438164
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a()LX/5HA;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v2

    invoke-virtual {v7, v2}, LX/5HA;->a(Z)LX/5HA;

    move-result-object v2

    iget-object v7, p0, LX/967;->c:LX/20i;

    invoke-virtual {v7}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v7

    invoke-virtual {v2, v7}, LX/5HA;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/5HA;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/5HA;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/5HA;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/5HA;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/5HA;

    move-result-object v2

    invoke-virtual {v2}, LX/5HA;->a()Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    move-result-object v2

    .line 1438165
    iget-object v8, p0, LX/967;->f:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "task_key_set_like_"

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, LX/964;

    invoke-direct {v10, p0, v2}, LX/964;-><init>(LX/967;Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)V

    new-instance v2, LX/965;

    move-object v3, p0

    move-object/from16 v4, p7

    move v7, p2

    invoke-direct/range {v2 .. v7}, LX/965;-><init>(LX/967;LX/1L9;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-virtual {v8, v9, v10, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto/16 :goto_0

    .line 1438166
    :cond_3
    if-eqz p7, :cond_2

    .line 1438167
    move-object/from16 v0, p7

    invoke-interface {v0, v5}, LX/1L9;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;ZLjava/lang/String;Ljava/lang/String;LX/1L9;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1438186
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1438187
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v9

    .line 1438188
    :goto_0
    iget-object v3, p0, LX/967;->b:LX/189;

    move/from16 v0, p2

    invoke-virtual {v3, p1, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1438189
    invoke-static {v3}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    .line 1438190
    move-object/from16 v0, p5

    invoke-interface {v0, v5}, LX/1L9;->a(Ljava/lang/Object;)V

    .line 1438191
    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    .line 1438192
    iget-object v3, p0, LX/967;->d:LX/0Zb;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v9, v4, v1}, LX/17V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438193
    :cond_0
    invoke-static {}, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f()LX/5H5;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/5H5;->d(Ljava/lang/String;)LX/5H5;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/5H5;->c(Ljava/lang/String;)LX/5H5;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/5H5;->b(Ljava/lang/String;)LX/5H5;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, LX/5H5;->a(Z)LX/5H5;

    move-result-object v3

    sget-object v4, LX/967;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/5H5;->e(Ljava/lang/String;)LX/5H5;

    move-result-object v3

    invoke-virtual {v3}, LX/5H5;->g()Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    move-result-object v3

    .line 1438194
    iget-object v11, p0, LX/967;->f:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "set_notify_me_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iget-object v2, p0, LX/967;->g:LX/3iV;

    invoke-virtual {v2, v3}, LX/3iV;->a(Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v13

    new-instance v2, LX/966;

    move-object v3, p0

    move-object/from16 v4, p5

    move-object v6, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v10, p2

    invoke-direct/range {v2 .. v10}, LX/966;-><init>(LX/967;LX/1L9;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v11, v12, v13, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1438195
    return-void

    .line 1438196
    :cond_1
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/1L9;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1438168
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->f()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move/from16 v0, p7

    invoke-direct {p0, p1, v0, v1, v2}, LX/967;->a(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/util/Pair;

    move-result-object v2

    .line 1438169
    if-nez v2, :cond_0

    .line 1438170
    :goto_0
    return-void

    .line 1438171
    :cond_0
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1438172
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1438173
    iget-object v3, p0, LX/967;->b:LX/189;

    invoke-virtual {v3, v2, v1}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1438174
    if-nez v3, :cond_2

    .line 1438175
    const-string v3, "StoryMutationHelper"

    const-string v4, "Unable to toggle page like from action link: story type = %s, action link type = %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    if-nez v1, :cond_1

    const-string v1, "[null]"

    :goto_1
    aput-object v1, v5, v2

    invoke-static {v3, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v1

    goto :goto_1

    .line 1438176
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1438177
    invoke-static {v3}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 1438178
    if-eqz p8, :cond_3

    .line 1438179
    move-object/from16 v0, p8

    invoke-interface {v0, v2}, LX/1L9;->a(Ljava/lang/Object;)V

    .line 1438180
    :cond_3
    if-eqz p7, :cond_5

    invoke-static {v1}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    .line 1438181
    :goto_2
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a()LX/5H8;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5H8;->a(Ljava/lang/String;)LX/5H8;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/5H8;->a(Z)LX/5H8;

    move-result-object v1

    new-instance v2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-direct {v2, v4, v0, p4}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/5H8;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/5H8;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, LX/5H8;->b(Ljava/lang/String;)LX/5H8;

    move-result-object v1

    invoke-virtual {v1}, LX/5H8;->a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    move-result-object v1

    .line 1438182
    if-eqz p3, :cond_4

    .line 1438183
    iget-object v2, p0, LX/967;->d:LX/0Zb;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static {p3, v4, v5, p4}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438184
    :cond_4
    iget-object v8, p0, LX/967;->f:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "task_key_toggle_page_like"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, LX/962;

    invoke-direct {v10, p0, v1, v3}, LX/962;-><init>(LX/967;Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    new-instance v1, LX/963;

    move-object v2, p0

    move-object/from16 v3, p8

    move-object v4, p2

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, LX/963;-><init>(LX/967;LX/1L9;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)V

    invoke-virtual {v8, v9, v10, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto/16 :goto_0

    .line 1438185
    :cond_5
    invoke-static {v1}, LX/1yp;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    goto :goto_2
.end method
