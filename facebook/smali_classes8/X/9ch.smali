.class public final LX/9ch;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/9cX;",
        "LX/9cY;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;)V
    .locals 0

    .prologue
    .line 1516682
    iput-object p1, p0, LX/9ch;->a:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1516691
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1516690
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1516684
    check-cast p2, LX/9cY;

    .line 1516685
    iget-object v0, p0, LX/9ch;->a:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    .line 1516686
    iget-object p0, p2, LX/9cY;->a:LX/0Px;

    if-eqz p0, :cond_0

    iget-object p0, p2, LX/9cY;->a:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    if-lez p0, :cond_0

    iget-object p0, v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->c:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    iget-object p1, p2, LX/9cY;->a:LX/0Px;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p1

    if-eq p0, p1, :cond_0

    .line 1516687
    iget-object p0, p2, LX/9cY;->a:LX/0Px;

    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p0

    iput-object p0, v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->c:LX/0Px;

    .line 1516688
    const p0, -0x76549a0

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1516689
    :cond_0
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1516683
    return-void
.end method
