.class public final LX/9E3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:LX/5Gs;

.field public final synthetic c:I

.field public final synthetic d:LX/9E6;


# direct methods
.method public constructor <init>(LX/9E6;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;I)V
    .locals 0

    .prologue
    .line 1456470
    iput-object p1, p0, LX/9E3;->d:LX/9E6;

    iput-object p2, p0, LX/9E3;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/9E3;->b:LX/5Gs;

    iput p4, p0, LX/9E3;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1456471
    iget-object v0, p0, LX/9E3;->d:LX/9E6;

    iget-object v1, p0, LX/9E3;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, LX/9E3;->b:LX/5Gs;

    iget v3, p0, LX/9E3;->c:I

    .line 1456472
    invoke-static {v1}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v7

    .line 1456473
    sget-object v4, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    if-ne v2, v4, :cond_1

    .line 1456474
    invoke-static {v1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v4

    .line 1456475
    :goto_0
    move-object v6, v4

    .line 1456476
    iget-object v4, v0, LX/9E6;->c:LX/3H7;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    move-object v8, v2

    move v9, v3

    invoke-virtual/range {v4 .. v9}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;LX/21y;LX/5Gs;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1456477
    sget-object v5, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v7, v5}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/9E6;->f:LX/0tF;

    invoke-virtual {v5}, LX/0tF;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1456478
    new-instance v5, LX/9E5;

    invoke-direct {v5, v0}, LX/9E5;-><init>(LX/9E6;)V

    invoke-static {v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1456479
    :cond_0
    move-object v0, v4

    .line 1456480
    return-object v0

    :cond_1
    invoke-static {v1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
