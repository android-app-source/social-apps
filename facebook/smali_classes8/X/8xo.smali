.class public LX/8xo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public final d:LX/0iA;

.field public final e:LX/8xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1424776
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_REMOVE_PLACE_H_SCROLL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/8xo;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1424777
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_SEEKER_FEED_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/8xo;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1424778
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_COMMENTS_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/8xo;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;LX/8xm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424780
    iput-object p1, p0, LX/8xo;->d:LX/0iA;

    .line 1424781
    iput-object p2, p0, LX/8xo;->e:LX/8xm;

    .line 1424782
    return-void
.end method

.method public static a(ILandroid/view/View;)V
    .locals 3
    .param p0    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1424783
    new-instance v0, LX/0hs;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1424784
    const/4 v1, -0x1

    .line 1424785
    iput v1, v0, LX/0hs;->t:I

    .line 1424786
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1424787
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1424788
    invoke-virtual {v0, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1424789
    return-void
.end method

.method public static b(LX/0QB;)LX/8xo;
    .locals 7

    .prologue
    .line 1424790
    new-instance v2, LX/8xo;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    check-cast v0, LX/0iA;

    .line 1424791
    new-instance v6, LX/8xm;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-direct {v6, v1, v3, v4, v5}, LX/8xm;-><init>(Landroid/content/Context;LX/0So;LX/0hB;LX/0iA;)V

    .line 1424792
    move-object v1, v6

    .line 1424793
    check-cast v1, LX/8xm;

    invoke-direct {v2, v0, v1}, LX/8xo;-><init>(LX/0iA;LX/8xm;)V

    .line 1424794
    return-object v2
.end method
