.class public final LX/9DJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/DeferredConsumptionController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/DeferredConsumptionController;)V
    .locals 0

    .prologue
    .line 1455448
    iput-object p1, p0, LX/9DJ;->a:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x6b30bc80

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1455449
    iget-object v1, p0, LX/9DJ;->a:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    iget-object v1, v1, Lcom/facebook/feedback/ui/DeferredConsumptionController;->g:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1455450
    iget-object v1, p0, LX/9DJ;->a:Lcom/facebook/feedback/ui/DeferredConsumptionController;

    .line 1455451
    iget-object v2, v1, Lcom/facebook/feedback/ui/DeferredConsumptionController;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1455452
    iget-object p0, v1, Lcom/facebook/feedback/ui/DeferredConsumptionController;->h:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iget-object p1, v1, Lcom/facebook/feedback/ui/DeferredConsumptionController;->b:Ljava/util/Map;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    sget-object p1, Lcom/facebook/feedback/ui/DeferredConsumptionController;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 p2, 0x1

    new-array p2, p2, [LX/9Bw;

    const/4 p3, 0x0

    aput-object v1, p2, p3

    invoke-virtual {p0, v2, p1, p2}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a(Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/common/callercontext/CallerContext;[LX/9Bw;)V

    goto :goto_0

    .line 1455453
    :cond_0
    const/16 v1, 0x27

    const v2, -0x83e505c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
