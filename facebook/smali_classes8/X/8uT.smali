.class public final enum LX/8uT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8uT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8uT;

.field public static final enum CIRCLE:LX/8uT;

.field public static final enum FILLED_ROUND_RECT:LX/8uT;

.field public static final enum SQUARE:LX/8uT;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1414798
    new-instance v0, LX/8uT;

    const-string v1, "SQUARE"

    invoke-direct {v0, v1, v2}, LX/8uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8uT;->SQUARE:LX/8uT;

    new-instance v0, LX/8uT;

    const-string v1, "CIRCLE"

    invoke-direct {v0, v1, v3}, LX/8uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8uT;->CIRCLE:LX/8uT;

    new-instance v0, LX/8uT;

    const-string v1, "FILLED_ROUND_RECT"

    invoke-direct {v0, v1, v4}, LX/8uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8uT;->FILLED_ROUND_RECT:LX/8uT;

    .line 1414799
    const/4 v0, 0x3

    new-array v0, v0, [LX/8uT;

    sget-object v1, LX/8uT;->SQUARE:LX/8uT;

    aput-object v1, v0, v2

    sget-object v1, LX/8uT;->CIRCLE:LX/8uT;

    aput-object v1, v0, v3

    sget-object v1, LX/8uT;->FILLED_ROUND_RECT:LX/8uT;

    aput-object v1, v0, v4

    sput-object v0, LX/8uT;->$VALUES:[LX/8uT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1414795
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8uT;
    .locals 1

    .prologue
    .line 1414797
    const-class v0, LX/8uT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8uT;

    return-object v0
.end method

.method public static values()[LX/8uT;
    .locals 1

    .prologue
    .line 1414796
    sget-object v0, LX/8uT;->$VALUES:[LX/8uT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uT;

    return-object v0
.end method
