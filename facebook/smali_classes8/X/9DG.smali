.class public LX/9DG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final A:LX/1My;

.field public final B:LX/03V;

.field public final C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final D:LX/9F7;

.field private final E:LX/9EL;

.field public final F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field public final G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final H:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;"
        }
    .end annotation
.end field

.field private final I:LX/9DA;

.field public J:LX/0g8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:LX/21n;

.field public M:LX/9D7;

.field public N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public O:Landroid/support/v4/app/Fragment;

.field public P:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field

.field public Q:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public R:Ljava/lang/Long;

.field public S:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field public T:Z

.field public U:LX/9CO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:LX/9Dl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/9CC;

.field private final c:LX/9FA;

.field private final d:LX/9Ce;

.field private final e:LX/9Cy;

.field public final f:LX/9D1;

.field private final g:LX/9FO;

.field private final h:LX/9DS;

.field private final i:LX/9Da;

.field public final j:LX/9Do;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Dl;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/9Du;

.field public final m:LX/9CP;

.field public final n:LX/1Db;

.field private final o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;"
        }
    .end annotation
.end field

.field public final q:LX/0iA;

.field private final r:LX/2cm;

.field private final s:LX/0ad;

.field private final t:LX/20j;

.field private final u:LX/3iP;

.field public final v:Lcom/facebook/content/SecureContextHelper;

.field private final w:LX/0tF;

.field private final x:LX/9Im;

.field public final y:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1455201
    const-class v0, LX/9DG;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9DG;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;LX/9Cz;LX/9FO;LX/9Eb;LX/9EC;LX/9Da;LX/9Do;LX/0Ot;LX/9Du;LX/1Db;LX/0iA;LX/2cm;LX/0ad;LX/20j;LX/3iP;LX/9D7;Lcom/facebook/content/SecureContextHelper;LX/9CQ;LX/0tF;LX/9Im;LX/9CD;LX/9FB;LX/1My;LX/03V;LX/0Ot;LX/9F8;LX/9EM;LX/0Ot;LX/0Ot;)V
    .locals 11
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9Bj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/9Br;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p38    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/content/Context;",
            "LX/9Bj;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            "ZZZ",
            "LX/9Br;",
            "Z",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;",
            "LX/9Cz;",
            "LX/9FO;",
            "LX/9Eb;",
            "LX/9EC;",
            "LX/9Da;",
            "LX/9Do;",
            "LX/0Ot",
            "<",
            "LX/9Dl;",
            ">;",
            "LX/9Du;",
            "LX/1Db;",
            "LX/0iA;",
            "LX/2cm;",
            "LX/0ad;",
            "LX/20j;",
            "LX/3iP;",
            "LX/9D7;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/9CQ;",
            "LX/0tF;",
            "LX/9Im;",
            "LX/9CD;",
            "LX/9FB;",
            "LX/1My;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/9F8;",
            "LX/9EM;",
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1455252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1455253
    new-instance v2, LX/9D9;

    invoke-direct {v2, p0}, LX/9D9;-><init>(LX/9DG;)V

    iput-object v2, p0, LX/9DG;->H:LX/0TF;

    .line 1455254
    new-instance v2, LX/9DB;

    invoke-direct {v2, p0}, LX/9DB;-><init>(LX/9DG;)V

    iput-object v2, p0, LX/9DG;->I:LX/9DA;

    .line 1455255
    move-object/from16 v0, p30

    iput-object v0, p0, LX/9DG;->x:LX/9Im;

    .line 1455256
    new-instance v2, LX/9Ce;

    invoke-direct {v2}, LX/9Ce;-><init>()V

    iput-object v2, p0, LX/9DG;->d:LX/9Ce;

    .line 1455257
    iget-object v5, p0, LX/9DG;->d:LX/9Ce;

    new-instance v6, Lcom/facebook/feedback/ui/CommentsHelper$3;

    invoke-direct {v6, p0}, Lcom/facebook/feedback/ui/CommentsHelper$3;-><init>(LX/9DG;)V

    iget-object v7, p0, LX/9DG;->x:LX/9Im;

    move-object/from16 v2, p32

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p8

    invoke-virtual/range {v2 .. v10}, LX/9FB;->a(Landroid/content/Context;LX/9Bi;LX/9Ce;Ljava/lang/Runnable;LX/9Im;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)LX/9FA;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->c:LX/9FA;

    .line 1455258
    iget-object v2, p0, LX/9DG;->c:LX/9FA;

    move-object/from16 v0, p31

    move/from16 v1, p7

    invoke-virtual {v0, v2, p4, v1}, LX/9CD;->a(LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/9CC;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->b:LX/9CC;

    .line 1455259
    new-instance v2, LX/9D1;

    iget-object v3, p0, LX/9DG;->b:LX/9CC;

    move-object/from16 v0, p9

    invoke-direct {v2, v3, v0}, LX/9D1;-><init>(LX/9CC;LX/9Br;)V

    iput-object v2, p0, LX/9DG;->f:LX/9D1;

    .line 1455260
    iget-object v2, p0, LX/9DG;->f:LX/9D1;

    iget-object v3, p0, LX/9DG;->x:LX/9Im;

    invoke-virtual {v2, v3}, LX/9D1;->a(LX/9Im;)V

    .line 1455261
    iget-object v3, p0, LX/9DG;->c:LX/9FA;

    iget-object v4, p0, LX/9DG;->f:LX/9D1;

    iget-object v5, p0, LX/9DG;->d:LX/9Ce;

    move-object/from16 v2, p28

    move-object/from16 v6, p5

    move/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, LX/9CQ;->a(LX/9FA;LX/9D1;LX/9Ce;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Z)LX/9CP;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->m:LX/9CP;

    .line 1455262
    iget-object v2, p0, LX/9DG;->d:LX/9Ce;

    iget-object v3, p0, LX/9DG;->m:LX/9CP;

    invoke-virtual {v2, v3}, LX/9Ce;->a(LX/9CP;)V

    .line 1455263
    iget-object v2, p0, LX/9DG;->c:LX/9FA;

    iget-object v3, p0, LX/9DG;->m:LX/9CP;

    invoke-virtual {v2, v3}, LX/9FA;->a(LX/9CP;)V

    .line 1455264
    move-object/from16 v0, p13

    iput-object v0, p0, LX/9DG;->g:LX/9FO;

    .line 1455265
    move-object/from16 v0, p16

    iput-object v0, p0, LX/9DG;->i:LX/9Da;

    .line 1455266
    iget-object v2, p0, LX/9DG;->i:LX/9Da;

    iget-object v3, p0, LX/9DG;->I:LX/9DA;

    invoke-virtual {v2, v3}, LX/9Da;->a(LX/9DA;)V

    .line 1455267
    move-object/from16 v0, p17

    iput-object v0, p0, LX/9DG;->j:LX/9Do;

    .line 1455268
    move-object/from16 v0, p18

    iput-object v0, p0, LX/9DG;->k:LX/0Ot;

    .line 1455269
    move-object/from16 v0, p19

    iput-object v0, p0, LX/9DG;->l:LX/9Du;

    .line 1455270
    iput-object p1, p0, LX/9DG;->O:Landroid/support/v4/app/Fragment;

    .line 1455271
    move-object/from16 v0, p20

    iput-object v0, p0, LX/9DG;->n:LX/1Db;

    .line 1455272
    move-object/from16 v0, p21

    iput-object v0, p0, LX/9DG;->q:LX/0iA;

    .line 1455273
    move-object/from16 v0, p22

    iput-object v0, p0, LX/9DG;->r:LX/2cm;

    .line 1455274
    move-object/from16 v0, p23

    iput-object v0, p0, LX/9DG;->s:LX/0ad;

    .line 1455275
    move-object/from16 v0, p24

    iput-object v0, p0, LX/9DG;->t:LX/20j;

    .line 1455276
    move-object/from16 v0, p25

    iput-object v0, p0, LX/9DG;->u:LX/3iP;

    .line 1455277
    move-object/from16 v0, p26

    iput-object v0, p0, LX/9DG;->M:LX/9D7;

    .line 1455278
    move-object/from16 v0, p11

    iput-object v0, p0, LX/9DG;->y:LX/0QK;

    .line 1455279
    move/from16 v0, p10

    iput-boolean v0, p0, LX/9DG;->z:Z

    .line 1455280
    iget-object v2, p0, LX/9DG;->b:LX/9CC;

    move-object/from16 v0, p37

    invoke-virtual {v0, v2}, LX/9EM;->a(LX/9CC;)LX/9EL;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->E:LX/9EL;

    .line 1455281
    iget-object v5, p0, LX/9DG;->b:LX/9CC;

    move-object v2, p0

    move-object/from16 v3, p14

    move-object/from16 v4, p15

    move-object/from16 v6, p11

    move-object/from16 v7, p29

    invoke-direct/range {v2 .. v7}, LX/9DG;->a(LX/9Eb;LX/9EC;LX/9CC;LX/0QK;LX/0tF;)LX/9DS;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->h:LX/9DS;

    .line 1455282
    iget-object v2, p0, LX/9DG;->b:LX/9CC;

    iget-object v3, p0, LX/9DG;->f:LX/9D1;

    invoke-static {v2, v3}, LX/9Cz;->a(LX/9CC;LX/9D1;)LX/9Cy;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->e:LX/9Cy;

    .line 1455283
    const/4 v2, 0x1

    new-array v2, v2, [LX/21l;

    const/4 v3, 0x0

    iget-object v4, p0, LX/9DG;->i:LX/9Da;

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->o:Ljava/util/Set;

    .line 1455284
    const/4 v2, 0x2

    new-array v2, v2, [LX/21l;

    const/4 v3, 0x0

    iget-object v4, p0, LX/9DG;->m:LX/9CP;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/9DG;->c:LX/9FA;

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->p:Ljava/util/Set;

    .line 1455285
    iget-boolean v2, p0, LX/9DG;->z:Z

    if-nez v2, :cond_0

    .line 1455286
    iget-object v2, p0, LX/9DG;->o:Ljava/util/Set;

    iget-object v3, p0, LX/9DG;->h:LX/9DS;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1455287
    :cond_0
    move-object/from16 v0, p27

    iput-object v0, p0, LX/9DG;->v:Lcom/facebook/content/SecureContextHelper;

    .line 1455288
    move-object/from16 v0, p29

    iput-object v0, p0, LX/9DG;->w:LX/0tF;

    .line 1455289
    move-object/from16 v0, p33

    iput-object v0, p0, LX/9DG;->A:LX/1My;

    .line 1455290
    move-object/from16 v0, p34

    iput-object v0, p0, LX/9DG;->B:LX/03V;

    .line 1455291
    move-object/from16 v0, p35

    iput-object v0, p0, LX/9DG;->C:LX/0Ot;

    .line 1455292
    iget-object v2, p0, LX/9DG;->l:LX/9Du;

    iget-object v3, p0, LX/9DG;->f:LX/9D1;

    iget-object v4, p0, LX/9DG;->b:LX/9CC;

    move-object/from16 v0, p36

    invoke-virtual {v0, v2, v3, v4}, LX/9F8;->a(LX/9Du;LX/9D1;LX/9CC;)LX/9F7;

    move-result-object v2

    iput-object v2, p0, LX/9DG;->D:LX/9F7;

    .line 1455293
    move-object/from16 v0, p38

    iput-object v0, p0, LX/9DG;->F:LX/0Ot;

    .line 1455294
    iput-object p4, p0, LX/9DG;->Q:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1455295
    move-object/from16 v0, p39

    iput-object v0, p0, LX/9DG;->G:LX/0Ot;

    .line 1455296
    move-object/from16 v0, p5

    invoke-virtual {p0, v0}, LX/9DG;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455297
    return-void
.end method

.method private a(LX/9Eb;LX/9EC;LX/9CC;LX/0QK;LX/0tF;)LX/9DS;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9Eb;",
            "LX/9EC;",
            "LX/9CC;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;",
            "LX/0tF;",
            ")",
            "LX/9DS;"
        }
    .end annotation

    .prologue
    .line 1455249
    invoke-virtual {p5}, LX/0tF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455250
    iget-object v3, p0, LX/9DG;->f:LX/9D1;

    iget-object v4, p0, LX/9DG;->j:LX/9Do;

    iget-object v5, p0, LX/9DG;->l:LX/9Du;

    iget-object v6, p0, LX/9DG;->E:LX/9EL;

    move-object v0, p2

    move-object v1, p4

    move-object v2, p3

    invoke-virtual/range {v0 .. v6}, LX/9EC;->a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9EB;

    move-result-object v0

    .line 1455251
    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, LX/9DG;->f:LX/9D1;

    iget-object v4, p0, LX/9DG;->j:LX/9Do;

    iget-object v5, p0, LX/9DG;->l:LX/9Du;

    iget-object v6, p0, LX/9DG;->E:LX/9EL;

    move-object v0, p1

    move-object v1, p4

    move-object v2, p3

    invoke-virtual/range {v0 .. v6}, LX/9Eb;->a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9Ea;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;LX/1ry;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 8
    .param p0    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1455227
    if-eqz p1, :cond_4

    move v2, v1

    .line 1455228
    :goto_0
    if-eqz p0, :cond_1

    const/4 v4, 0x0

    .line 1455229
    if-eqz p0, :cond_0

    if-nez p1, :cond_5

    :cond_0
    move v3, v4

    .line 1455230
    :goto_1
    move v3, v3

    .line 1455231
    if-nez v3, :cond_2

    :cond_1
    move v0, v1

    .line 1455232
    :cond_2
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {p3}, LX/1ry;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_3

    .line 1455233
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1455234
    new-instance v1, LX/4Vv;

    invoke-direct {v1}, LX/4Vv;-><init>()V

    .line 1455235
    iput-object v0, v1, LX/4Vv;->c:LX/0Px;

    .line 1455236
    move-object v0, v1

    .line 1455237
    invoke-virtual {v0}, LX/4Vv;->a()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    .line 1455238
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    .line 1455239
    iput-object v0, v1, LX/3dM;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 1455240
    move-object v0, v1

    .line 1455241
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    .line 1455242
    :cond_3
    return-object p0

    :cond_4
    move v2, v0

    .line 1455243
    goto :goto_0

    .line 1455244
    :cond_5
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_2
    if-ge v5, v7, :cond_7

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1455245
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1455246
    const/4 v3, 0x1

    goto :goto_1

    .line 1455247
    :cond_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    :cond_7
    move v3, v4

    .line 1455248
    goto :goto_1
.end method

.method private a(Landroid/view/View;LX/0g8;)V
    .locals 3

    .prologue
    .line 1455202
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    .line 1455203
    invoke-static {v0}, LX/9CP;->E(LX/9CP;)V

    .line 1455204
    iput-object p2, v0, LX/9CP;->j:LX/0g8;

    .line 1455205
    if-eqz p2, :cond_1

    .line 1455206
    invoke-interface {p2}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v1

    .line 1455207
    invoke-static {v0}, LX/9CP;->B(LX/9CP;)V

    .line 1455208
    iget-object v2, v0, LX/9CP;->x:LX/4nF;

    if-nez v2, :cond_0

    .line 1455209
    new-instance v2, LX/9CL;

    invoke-direct {v2, v0}, LX/9CL;-><init>(LX/9CP;)V

    iput-object v2, v0, LX/9CP;->x:LX/4nF;

    .line 1455210
    :cond_0
    invoke-static {v0}, LX/9CP;->s(LX/9CP;)Z

    move-result v2

    invoke-static {v1, v2}, LX/9CP;->a(Landroid/view/View;Z)LX/4nG;

    move-result-object v2

    iput-object v2, v0, LX/9CP;->w:LX/4nG;

    .line 1455211
    iget-object v2, v0, LX/9CP;->w:LX/4nG;

    iget-object p2, v0, LX/9CP;->x:LX/4nF;

    invoke-virtual {v2, p2}, LX/4nG;->a(LX/4nF;)V

    .line 1455212
    :cond_1
    const v0, 0x7f0d0bdb

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    .line 1455213
    iget-object v1, p0, LX/9DG;->m:LX/9CP;

    .line 1455214
    invoke-static {v1}, LX/9CP;->G(LX/9CP;)V

    .line 1455215
    iput-object v0, v1, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    .line 1455216
    iget-object v2, v1, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    if-nez v2, :cond_4

    .line 1455217
    :goto_0
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    new-instance v1, LX/9DF;

    invoke-direct {v1, p0}, LX/9DF;-><init>(LX/9DG;)V

    .line 1455218
    iput-object v1, v0, LX/9CP;->B:LX/9DF;

    .line 1455219
    iget-object v2, v0, LX/9CP;->n:LX/21o;

    if-eqz v2, :cond_2

    .line 1455220
    iget-object v2, v0, LX/9CP;->n:LX/21o;

    invoke-interface {v2, v1}, LX/21o;->setMediaPickerListener(LX/9DF;)V

    .line 1455221
    :cond_2
    iget-object v0, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v0, :cond_3

    .line 1455222
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    iget-object v1, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0, v1}, LX/9CP;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455223
    :cond_3
    return-void

    .line 1455224
    :cond_4
    iget-object v2, v1, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    new-instance p1, LX/9CI;

    invoke-direct {p1, v1}, LX/9CI;-><init>(LX/9CP;)V

    .line 1455225
    iput-object p1, v2, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->h:LX/6Lk;

    .line 1455226
    goto :goto_0
.end method

.method private a(Lcom/facebook/ipc/media/MediaItem;LX/9CO;)V
    .locals 2

    .prologue
    .line 1455062
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    .line 1455063
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1455064
    :cond_0
    :goto_0
    return-void

    .line 1455065
    :cond_1
    sget-object v1, LX/9CM;->a:[I

    invoke-virtual {p2}, LX/9CO;->ordinal()I

    move-result p0

    aget v1, v1, p0

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1455066
    :pswitch_0
    iput-object p1, v0, LX/9CP;->C:Lcom/facebook/ipc/media/MediaItem;

    .line 1455067
    invoke-static {v0}, LX/9CP;->C(LX/9CP;)V

    goto :goto_0

    .line 1455068
    :pswitch_1
    iput-object p1, v0, LX/9CP;->D:Lcom/facebook/ipc/media/MediaItem;

    .line 1455069
    invoke-static {v0}, LX/9CP;->D(LX/9CP;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static n(LX/9DG;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1455200
    iget-boolean v1, p0, LX/9DG;->z:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9DG;->s:LX/0ad;

    sget-short v2, LX/0wn;->aI:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/9DG;->s:LX/0ad;

    sget-short v2, LX/0wn;->aH:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 5
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1455194
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1455195
    :cond_0
    return-object p1

    .line 1455196
    :cond_1
    iget-object v0, p0, LX/9DG;->u:LX/3iP;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iP;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1455197
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1455198
    iget-object v4, p0, LX/9DG;->t:LX/20j;

    invoke-virtual {v4, p1, v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    .line 1455199
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    .line 1455173
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1455174
    :cond_0
    :goto_0
    return-void

    .line 1455175
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1455176
    :sswitch_0
    const-string v0, "transliterated_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455177
    const-string v0, "transliterated_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1455178
    iget-object v1, p0, LX/9DG;->L:LX/21n;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/21n;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1455179
    :sswitch_1
    invoke-static {p1, p2, p3}, LX/9FO;->a(IILandroid/content/Intent;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    iget-object v1, p0, LX/9DG;->U:LX/9CO;

    invoke-direct {p0, v0, v1}, LX/9DG;->a(Lcom/facebook/ipc/media/MediaItem;LX/9CO;)V

    .line 1455180
    const/4 v0, 0x0

    iput-object v0, p0, LX/9DG;->U:LX/9CO;

    goto :goto_0

    .line 1455181
    :sswitch_2
    iget-object v0, p0, LX/9DG;->r:LX/2cm;

    invoke-virtual {v0, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1455182
    :sswitch_3
    const-string v3, "mention_user_id"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "mention_user_name"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1455183
    :cond_2
    :goto_1
    goto :goto_0

    .line 1455184
    :cond_3
    new-instance v3, LX/7Gq;

    invoke-direct {v3}, LX/7Gq;-><init>()V

    sget-object v4, LX/7Gr;->USER:LX/7Gr;

    .line 1455185
    iput-object v4, v3, LX/7Gq;->e:LX/7Gr;

    .line 1455186
    move-object v3, v3

    .line 1455187
    new-instance v4, Lcom/facebook/user/model/Name;

    const-string v5, "mention_user_name"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 1455188
    iput-object v4, v3, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 1455189
    move-object v4, v3

    .line 1455190
    :try_start_0
    const-string v3, "mention_user_id"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 1455191
    iput-wide v5, v4, LX/7Gq;->b:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1455192
    iget-object v3, p0, LX/9DG;->F:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    new-instance v5, Lcom/facebook/feedback/ui/CommentsHelper$5;

    invoke-direct {v5, p0, v4}, Lcom/facebook/feedback/ui/CommentsHelper$5;-><init>(LX/9DG;LX/7Gq;)V

    const-wide/16 v7, 0x64

    const v4, -0x4ec030be

    invoke-static {v3, v5, v7, v8, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1

    .line 1455193
    :catch_0
    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3ba -> :sswitch_1
        0x138a -> :sswitch_2
        0xb256 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1455153
    if-nez p1, :cond_0

    .line 1455154
    :goto_0
    return-void

    .line 1455155
    :cond_0
    const-string v0, "scrollingViewProxy"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1455156
    const-string v0, "scrollingViewProxy"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, LX/9DG;->K:Landroid/os/Parcelable;

    .line 1455157
    :cond_1
    const-string v0, "mediaItemDestination"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1455158
    const-string v0, "mediaItemDestination"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/9CO;

    iput-object v0, p0, LX/9DG;->U:LX/9CO;

    .line 1455159
    :cond_2
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    .line 1455160
    const-string p0, "composerManagerState"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1455161
    const-string p0, "composerManagerState"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, LX/9CN;

    iput-object p0, v0, LX/9CP;->E:LX/9CN;

    .line 1455162
    :cond_3
    invoke-static {v0}, LX/9CP;->O(LX/9CP;)Z

    move-result p0

    if-nez p0, :cond_4

    .line 1455163
    :goto_1
    goto :goto_0

    .line 1455164
    :cond_4
    const-string p0, "replyFeedback"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 1455165
    const-string p0, "replyingToAuthorName"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 1455166
    const-string p0, "replyingToAuthorId"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 1455167
    const-string p0, "replyingToCommentId"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 1455168
    const-string p0, "replyFeedback"

    invoke-static {p1, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p0, v0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455169
    const-string p0, "replyingToTopLevelComment"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p0

    iput-boolean p0, v0, LX/9CP;->H:Z

    .line 1455170
    const-string p0, "replyingToAuthorName"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, LX/9CP;->q:Ljava/lang/String;

    .line 1455171
    const-string p0, "replyingToAuthorId"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, LX/9CP;->r:Ljava/lang/String;

    .line 1455172
    const-string p0, "replyingToCommentId"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, LX/9CP;->p:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Landroid/view/View;LX/0g8;Landroid/widget/BaseAdapter;)V
    .locals 10

    .prologue
    .line 1455091
    iget-object v0, p0, LX/9DG;->x:LX/9Im;

    .line 1455092
    iget-boolean v1, v0, LX/9Im;->e:Z

    if-nez v1, :cond_8

    .line 1455093
    :goto_0
    iput-object p2, p0, LX/9DG;->J:LX/0g8;

    .line 1455094
    const v0, 0x7f0d009e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1455095
    iget-object v1, p0, LX/9DG;->j:LX/9Do;

    iget-object v2, p0, LX/9DG;->f:LX/9D1;

    invoke-virtual {v1, v0, v2}, LX/9Do;->a(Landroid/view/ViewStub;LX/9D1;)V

    .line 1455096
    const v0, 0x7f0d00ba

    invoke-static {p1, v0}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v1

    .line 1455097
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455098
    iget-object v0, p0, LX/9DG;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Dl;

    iput-object v0, p0, LX/9DG;->V:LX/9Dl;

    .line 1455099
    iget-object v2, p0, LX/9DG;->V:LX/9Dl;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iget-object v1, p0, LX/9DG;->f:LX/9D1;

    .line 1455100
    new-instance p3, LX/0zw;

    invoke-direct {p3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p3, v2, LX/9Dl;->c:LX/0zw;

    .line 1455101
    iput-object v1, v2, LX/9Dl;->b:LX/9D1;

    .line 1455102
    :cond_0
    const v0, 0x7f0d009f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1455103
    iget-object v1, p0, LX/9DG;->l:LX/9Du;

    iget-object v2, p0, LX/9DG;->f:LX/9D1;

    invoke-virtual {v1, v0, v2}, LX/9Du;->a(Landroid/view/ViewStub;LX/9D1;)V

    .line 1455104
    iget-object v0, p0, LX/9DG;->l:LX/9Du;

    iget-object v1, p0, LX/9DG;->j:LX/9Do;

    invoke-virtual {v0, v1}, LX/9Bh;->a(LX/9Bh;)V

    .line 1455105
    iget-object v0, p0, LX/9DG;->V:LX/9Dl;

    if-eqz v0, :cond_1

    .line 1455106
    iget-object v0, p0, LX/9DG;->l:LX/9Du;

    iget-object v1, p0, LX/9DG;->V:LX/9Dl;

    invoke-virtual {v0, v1}, LX/9Bh;->a(LX/9Bh;)V

    .line 1455107
    :cond_1
    iget-object v0, p0, LX/9DG;->f:LX/9D1;

    .line 1455108
    iput-object p2, v0, LX/9D1;->c:LX/0g8;

    .line 1455109
    iget-object v1, v0, LX/9D1;->c:LX/0g8;

    iget-object v2, v0, LX/9D1;->h:LX/9D0;

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 1455110
    iget-object v1, v0, LX/9D1;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1455111
    iget-object v1, v0, LX/9D1;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0fx;

    .line 1455112
    iget-object p3, v0, LX/9D1;->c:LX/0g8;

    invoke-interface {p3, v1}, LX/0g8;->b(LX/0fx;)V

    goto :goto_1

    .line 1455113
    :cond_2
    iget-object v1, v0, LX/9D1;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1455114
    :cond_3
    new-instance v0, LX/9DC;

    invoke-direct {v0, p0}, LX/9DC;-><init>(LX/9DG;)V

    invoke-interface {p2, v0}, LX/0g8;->a(LX/1St;)V

    .line 1455115
    invoke-direct {p0, p1, p2}, LX/9DG;->a(Landroid/view/View;LX/0g8;)V

    .line 1455116
    const v3, 0x7f0d009d

    invoke-static {p1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, LX/21n;

    iput-object v3, p0, LX/9DG;->L:LX/21n;

    .line 1455117
    iget-object v3, p0, LX/9DG;->L:LX/21n;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/21n;->setVisibility(I)V

    .line 1455118
    iget-object v3, p0, LX/9DG;->L:LX/21n;

    iget-object v4, p0, LX/9DG;->m:LX/9CP;

    invoke-interface {v3, v4}, LX/21n;->setCommentComposerManager(LX/9CP;)V

    .line 1455119
    iget-object v3, p0, LX/9DG;->L:LX/21n;

    new-instance v4, LX/9D8;

    invoke-direct {v4, p0}, LX/9D8;-><init>(LX/9DG;)V

    invoke-interface {v3, v4}, LX/21n;->setTransliterationClickListener(LX/9D8;)V

    .line 1455120
    iget-object v3, p0, LX/9DG;->p:Ljava/util/Set;

    iget-object v4, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1455121
    iget-object v3, p0, LX/9DG;->L:LX/21n;

    iget-boolean v4, p0, LX/9DG;->T:Z

    invoke-interface {v3, v4}, LX/21n;->setReshareButtonExperimentClicked(Z)V

    .line 1455122
    iget-object v3, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v3, :cond_4

    .line 1455123
    iget-object v3, p0, LX/9DG;->L:LX/21n;

    iget-object v4, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-interface {v3, v4}, LX/21n;->setFeedbackLoggingParams(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455124
    :cond_4
    iget-object v3, p0, LX/9DG;->S:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    if-eqz v3, :cond_5

    .line 1455125
    iget-object v3, p0, LX/9DG;->L:LX/21n;

    iget-object v4, p0, LX/9DG;->S:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-interface {v3, v4}, LX/21n;->setNotificationLogObject(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 1455126
    :cond_5
    iget-object v3, p0, LX/9DG;->R:Ljava/lang/Long;

    if-eqz v3, :cond_6

    .line 1455127
    iget-object v3, p0, LX/9DG;->L:LX/21n;

    iget-object v4, p0, LX/9DG;->R:Ljava/lang/Long;

    invoke-interface {v3, v4}, LX/21n;->setGroupIdForTagging(Ljava/lang/Long;)V

    .line 1455128
    :cond_6
    iget-object v5, p0, LX/9DG;->L:LX/21n;

    if-eqz v5, :cond_7

    iget-object v5, p0, LX/9DG;->Q:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v5, :cond_9

    .line 1455129
    :cond_7
    :goto_2
    iget-object v1, p0, LX/9DG;->M:LX/9D7;

    const v0, 0x7f0d00a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1455130
    iput-object v0, v1, LX/9D7;->c:Landroid/view/ViewStub;

    .line 1455131
    iget-object v0, p0, LX/9DG;->p:Ljava/util/Set;

    iget-object v1, p0, LX/9DG;->M:LX/9D7;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1455132
    return-void

    .line 1455133
    :cond_8
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455134
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455135
    iput-object p2, v0, LX/9Im;->f:LX/0g8;

    .line 1455136
    iput-object p3, v0, LX/9Im;->g:Landroid/widget/BaseAdapter;

    .line 1455137
    iget-object v1, v0, LX/9Im;->a:LX/9Ik;

    iget-object v2, v0, LX/9Im;->f:LX/0g8;

    .line 1455138
    iput-object v2, v1, LX/9Ik;->f:LX/0g8;

    .line 1455139
    iget-object v1, v0, LX/9Im;->f:LX/0g8;

    iget-object v2, v0, LX/9Im;->b:LX/0fx;

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 1455140
    iget-object v1, v0, LX/9Im;->g:Landroid/widget/BaseAdapter;

    iget-object v2, v0, LX/9Im;->d:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto/16 :goto_0

    .line 1455141
    :cond_9
    iget-object v5, p0, LX/9DG;->Q:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1455142
    iget-object v6, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v6

    .line 1455143
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455144
    if-eqz v5, :cond_7

    .line 1455145
    invoke-static {v5}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, LX/9DG;->G:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0W3;

    sget-wide v7, LX/0X5;->cv:J

    const/4 v9, 0x0

    invoke-interface {v6, v7, v8, v9}, LX/0W4;->a(JZ)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1455146
    invoke-static {v5}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v6

    .line 1455147
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455148
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455149
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1455150
    const-string v8, "groupCommerceProductItemID"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455151
    const-string v6, "tracking"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455152
    iget-object v5, p0, LX/9DG;->L:LX/21n;

    const-string v6, "GroupCommercePredictiveCommentsApp"

    const/4 v8, -0x1

    const/16 v9, 0x21

    invoke-interface {v5, v6, v7, v8, v9}, LX/21n;->a(Ljava/lang/String;Landroid/os/Bundle;II)V

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 3

    .prologue
    .line 1455076
    iput-object p1, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1455077
    iget-object v0, p0, LX/9DG;->b:LX/9CC;

    iget-object v1, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1455078
    iget-object v2, v0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v2, v1}, LX/9E8;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455079
    iget-object v2, v0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v2, v1}, LX/9E8;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455080
    iget-object v0, p0, LX/9DG;->x:LX/9Im;

    iget-object v1, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1455081
    iget-boolean v2, v0, LX/9Im;->e:Z

    if-nez v2, :cond_2

    .line 1455082
    :goto_0
    iget-object v0, p0, LX/9DG;->c:LX/9FA;

    invoke-virtual {v0, p1}, LX/9FA;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455083
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    if-eqz v0, :cond_0

    .line 1455084
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v0, p1}, LX/21n;->setFeedbackLoggingParams(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455085
    :cond_0
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    if-eqz v0, :cond_1

    .line 1455086
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    invoke-virtual {v0, p1}, LX/9CP;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455087
    :cond_1
    return-void

    .line 1455088
    :cond_2
    iget-object v2, v0, LX/9Im;->c:LX/9Ij;

    .line 1455089
    iput-object v1, v2, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1455090
    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 1

    .prologue
    .line 1455074
    sget-object v0, LX/9CO;->COMMENT_COMPOSER:LX/9CO;

    invoke-direct {p0, p1, v0}, LX/9DG;->a(Lcom/facebook/ipc/media/MediaItem;LX/9CO;)V

    .line 1455075
    return-void
.end method

.method public final a(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 1455070
    iput-object p1, p0, LX/9DG;->R:Ljava/lang/Long;

    .line 1455071
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    if-eqz v0, :cond_0

    .line 1455072
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v0, p1}, LX/21n;->setGroupIdForTagging(Ljava/lang/Long;)V

    .line 1455073
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1455298
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 1455299
    if-eqz p1, :cond_1

    iget-object v1, p0, LX/9DG;->t:LX/20j;

    .line 1455300
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1455301
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1, v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1455302
    iget-boolean v0, p0, LX/9DG;->z:Z

    if-eqz v0, :cond_0

    .line 1455303
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1455304
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455305
    new-instance v5, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v7, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v8, 0x0

    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v10

    move-object v6, v0

    invoke-direct/range {v5 .. v10}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 1455306
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1455307
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    .line 1455308
    iget-object v6, p0, LX/9DG;->A:LX/1My;

    iget-object v7, p0, LX/9DG;->H:LX/0TF;

    invoke-virtual {v6, v7, v4, v5}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1455309
    :cond_0
    iget-object v0, p0, LX/9DG;->E:LX/9EL;

    iget-object v1, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/9EL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1455310
    iget-object v0, p0, LX/9DG;->b:LX/9CC;

    iget-object v1, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1455311
    if-eqz v1, :cond_7

    .line 1455312
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1455313
    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455314
    :goto_1
    invoke-static {v3}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v3

    sget-object v4, LX/21y;->RANKED_ORDER:LX/21y;

    if-ne v3, v4, :cond_8

    .line 1455315
    iget-object v3, v0, LX/9CC;->b:LX/9E8;

    invoke-virtual {v3, v1}, LX/9E8;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1455316
    :goto_2
    iget-object v0, p0, LX/9DG;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21l;

    .line 1455317
    iget-object v3, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {v0, v3}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_3

    :cond_1
    move-object v0, v2

    .line 1455318
    goto :goto_0

    .line 1455319
    :cond_2
    iget-object v0, p0, LX/9DG;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21l;

    .line 1455320
    iget-object v1, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1455321
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v4

    .line 1455322
    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    :goto_5
    invoke-interface {v0, v1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_4

    :cond_3
    move-object v1, v2

    goto :goto_5

    .line 1455323
    :cond_4
    invoke-static {p0}, LX/9DG;->n(LX/9DG;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1455324
    iget-object v0, p0, LX/9DG;->D:LX/9F7;

    invoke-virtual {v0}, LX/9F7;->a()V

    .line 1455325
    :cond_5
    iget-object v0, p0, LX/9DG;->K:Landroid/os/Parcelable;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/9DG;->J:LX/0g8;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1455326
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1455327
    if-nez v0, :cond_9

    .line 1455328
    :cond_6
    :goto_6
    return-void

    .line 1455329
    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    .line 1455330
    :cond_8
    iget-object v3, v0, LX/9CC;->a:LX/9E8;

    invoke-virtual {v3, v1}, LX/9E8;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_2

    .line 1455331
    :cond_9
    iget-object v0, p0, LX/9DG;->J:LX/0g8;

    iget-object v1, p0, LX/9DG;->K:Landroid/os/Parcelable;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/os/Parcelable;)V

    .line 1455332
    const/4 v0, 0x0

    iput-object v0, p0, LX/9DG;->K:Landroid/os/Parcelable;

    goto :goto_6
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1454856
    iget-object v0, p0, LX/9DG;->e:LX/9Cy;

    .line 1454857
    iget-boolean p0, v0, LX/9Cy;->c:Z

    if-nez p0, :cond_0

    if-nez p1, :cond_1

    .line 1454858
    :cond_0
    :goto_0
    return-void

    .line 1454859
    :cond_1
    iget-object p0, v0, LX/9Cy;->b:LX/9D1;

    invoke-virtual {p0, p1}, LX/9D1;->a(Ljava/lang/String;)Z

    move-result p0

    iput-boolean p0, v0, LX/9Cy;->c:Z

    .line 1454860
    iget-boolean p0, v0, LX/9Cy;->c:Z

    if-eqz p0, :cond_0

    .line 1454861
    iget-object p0, v0, LX/9Cy;->a:LX/9CC;

    invoke-virtual {p0, p1}, LX/9CC;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(FF)Z
    .locals 5

    .prologue
    .line 1454862
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    const/4 v1, 0x0

    .line 1454863
    float-to-int v2, p1

    .line 1454864
    float-to-int v3, p2

    .line 1454865
    iget-object v4, v0, LX/9CP;->n:LX/21o;

    if-eqz v4, :cond_1

    iget-object v4, v0, LX/9CP;->n:LX/21o;

    invoke-interface {v4, v2, v3}, LX/21o;->a(II)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1454866
    :cond_0
    :goto_0
    move v0, v1

    .line 1454867
    return v0

    .line 1454868
    :cond_1
    iget-object v4, v0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    if-eqz v4, :cond_2

    .line 1454869
    iget-object v4, v0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object p0, v0, LX/9CP;->i:Landroid/graphics/Rect;

    invoke-virtual {v4, p0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1454870
    iget-object v4, v0, LX/9CP;->i:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1454871
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1454872
    iget-object v1, p0, LX/9DG;->L:LX/21n;

    if-nez v1, :cond_1

    .line 1454873
    :cond_0
    :goto_0
    return v0

    .line 1454874
    :cond_1
    invoke-virtual {p0}, LX/9DG;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1454875
    const/4 v0, 0x1

    goto :goto_0

    .line 1454876
    :cond_2
    iget-object v1, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v1}, LX/21n;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1454877
    iget-object v1, p0, LX/9DG;->s:LX/0ad;

    sget-char v2, LX/0wn;->t:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1454878
    const-string v2, "education_reminder"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "match_post_composer"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "education_nux"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1454879
    :cond_3
    const/4 v5, 0x1

    .line 1454880
    iget-object v0, p0, LX/9DG;->q:LX/0iA;

    sget-object v2, LX/3kg;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/3kg;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v6

    .line 1454881
    instance-of v0, v6, LX/3kg;

    if-nez v0, :cond_4

    const-string v0, "match_post_composer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1454882
    const/4 v0, 0x0

    .line 1454883
    :goto_1
    move v0, v0

    .line 1454884
    goto :goto_0

    .line 1454885
    :cond_4
    const-string v0, "match_post_composer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f081439    # 1.8088E38f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1454886
    :goto_2
    const-string v2, "match_post_composer"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const v2, 0x7f081431

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1454887
    :goto_3
    const v3, 0x7f08143f

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1454888
    const-string v4, "education_reminder"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1454889
    const v3, 0x7f08123d

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1454890
    :cond_5
    :goto_4
    const-string v4, "match_post_composer"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const v4, 0x7f08143d

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1454891
    :goto_5
    const-class v7, Landroid/app/Activity;

    invoke-static {p1, v7}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    .line 1454892
    const/4 v8, 0x0

    .line 1454893
    instance-of v9, v7, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v9, :cond_6

    move-object v8, v7

    .line 1454894
    check-cast v8, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v8

    .line 1454895
    const-string v9, "chromeless:content:fragment:tag"

    invoke-virtual {v8, v9}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v8

    .line 1454896
    :cond_6
    new-instance v9, Lcom/facebook/feedback/ui/CommentsHelper$8;

    invoke-direct {v9, p0, v8, v7}, Lcom/facebook/feedback/ui/CommentsHelper$8;-><init>(LX/9DG;Landroid/support/v4/app/Fragment;Landroid/app/Activity;)V

    move-object v7, v9

    .line 1454897
    new-instance v8, LX/0ju;

    invoke-direct {v8, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1454898
    invoke-virtual {v8, v5}, LX/0ju;->a(Z)LX/0ju;

    .line 1454899
    invoke-virtual {v8, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1454900
    invoke-virtual {v8, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1454901
    new-instance v0, LX/9DD;

    invoke-direct {v0, p0, v6, v1, v7}, LX/9DD;-><init>(LX/9DG;LX/0i1;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-virtual {v8, v3, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1454902
    const-string v0, "education_nux"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1454903
    new-instance v0, LX/9DE;

    invoke-direct {v0, p0, v1, v7}, LX/9DE;-><init>(LX/9DG;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-virtual {v8, v4, v0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1454904
    :cond_7
    invoke-virtual {v8}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    move v0, v5

    .line 1454905
    goto/16 :goto_1

    .line 1454906
    :cond_8
    const v0, 0x7f08123a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1454907
    :cond_9
    const v2, 0x7f08123b

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1454908
    :cond_a
    const-string v4, "education_nux"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1454909
    const v3, 0x7f08123c

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 1454910
    :cond_b
    const v4, 0x7f08123e

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_5
.end method

.method public final a(Lcom/facebook/tagging/model/TaggingProfile;)Z
    .locals 1

    .prologue
    .line 1454911
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    if-nez v0, :cond_0

    .line 1454912
    const/4 v0, 0x0

    .line 1454913
    :goto_0
    return v0

    .line 1454914
    :cond_0
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v0, p1}, LX/21n;->a(Lcom/facebook/tagging/model/TaggingProfile;)V

    .line 1454915
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1454916
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    const/4 v3, 0x0

    .line 1454917
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    if-nez v1, :cond_6

    .line 1454918
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/9CP;->k()V

    .line 1454919
    invoke-static {v0}, LX/9CP;->E(LX/9CP;)V

    .line 1454920
    invoke-static {v0}, LX/9CP;->B(LX/9CP;)V

    .line 1454921
    invoke-static {v0}, LX/9CP;->F(LX/9CP;)V

    .line 1454922
    invoke-static {v0}, LX/9CP;->G(LX/9CP;)V

    .line 1454923
    iget-object v1, v0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    if-eqz v1, :cond_1

    .line 1454924
    iget-object v1, v0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1454925
    iput-object v3, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    .line 1454926
    iput-object v3, v0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1454927
    :cond_1
    iput-object v3, v0, LX/9CP;->C:Lcom/facebook/ipc/media/MediaItem;

    .line 1454928
    iput-object v3, v0, LX/9CP;->D:Lcom/facebook/ipc/media/MediaItem;

    .line 1454929
    iput-object v3, v0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1454930
    iput-object v3, v0, LX/9CP;->q:Ljava/lang/String;

    .line 1454931
    iput-object v3, v0, LX/9CP;->r:Ljava/lang/String;

    .line 1454932
    iput-object v3, v0, LX/9CP;->p:Ljava/lang/String;

    .line 1454933
    sget-object v1, LX/9CN;->INACTIVE:LX/9CN;

    iput-object v1, v0, LX/9CP;->E:LX/9CN;

    .line 1454934
    invoke-static {v0}, LX/9CP;->P(LX/9CP;)V

    .line 1454935
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    if-eqz v0, :cond_2

    .line 1454936
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v0}, LX/21n;->i()V

    .line 1454937
    iget-object v0, p0, LX/9DG;->p:Ljava/util/Set;

    iget-object v1, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1454938
    iput-object v2, p0, LX/9DG;->L:LX/21n;

    .line 1454939
    :cond_2
    iget-object v0, p0, LX/9DG;->J:LX/0g8;

    if-eqz v0, :cond_3

    .line 1454940
    iget-object v0, p0, LX/9DG;->J:LX/0g8;

    invoke-interface {v0, v2}, LX/0g8;->a(LX/1St;)V

    .line 1454941
    iput-object v2, p0, LX/9DG;->J:LX/0g8;

    .line 1454942
    :cond_3
    iget-object v0, p0, LX/9DG;->x:LX/9Im;

    const/4 v3, 0x0

    .line 1454943
    iget-boolean v1, v0, LX/9Im;->e:Z

    if-nez v1, :cond_7

    .line 1454944
    :goto_1
    iget-object v0, p0, LX/9DG;->f:LX/9D1;

    const/4 v3, 0x0

    .line 1454945
    iget-object v1, v0, LX/9D1;->c:LX/0g8;

    if-eqz v1, :cond_4

    .line 1454946
    iget-object v1, v0, LX/9D1;->c:LX/0g8;

    iget-object v2, v0, LX/9D1;->h:LX/9D0;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 1454947
    iget-object v1, v0, LX/9D1;->c:LX/0g8;

    invoke-interface {v1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, LX/9D1;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v1, v2}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1454948
    iput-object v3, v0, LX/9D1;->c:LX/0g8;

    .line 1454949
    :cond_4
    iput-object v3, v0, LX/9D1;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1454950
    iget-object v0, p0, LX/9DG;->j:LX/9Do;

    .line 1454951
    const/4 v1, 0x0

    iput-object v1, v0, LX/9Do;->d:LX/0zw;

    .line 1454952
    iget-object v0, p0, LX/9DG;->V:LX/9Dl;

    if-eqz v0, :cond_5

    .line 1454953
    iget-object v0, p0, LX/9DG;->V:LX/9Dl;

    .line 1454954
    const/4 v1, 0x0

    iput-object v1, v0, LX/9Dl;->c:LX/0zw;

    .line 1454955
    :cond_5
    iget-object v0, p0, LX/9DG;->l:LX/9Du;

    .line 1454956
    const/4 v1, 0x0

    iput-object v1, v0, LX/9Du;->b:LX/0zw;

    .line 1454957
    iget-object v0, p0, LX/9DG;->p:Ljava/util/Set;

    iget-object v1, p0, LX/9DG;->M:LX/9D7;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1454958
    iget-object v0, p0, LX/9DG;->M:LX/9D7;

    const/4 v1, 0x0

    .line 1454959
    iput-object v1, v0, LX/9D7;->c:Landroid/view/ViewStub;

    .line 1454960
    iput-object v1, v0, LX/9D7;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1454961
    return-void

    .line 1454962
    :cond_6
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    invoke-interface {v1}, LX/21o;->getPendingComment()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v4

    .line 1454963
    if-eqz v4, :cond_0

    .line 1454964
    iget-object v1, v0, LX/9CP;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8pb;

    iget-object v5, v4, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LX/8pb;->a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Z

    goto/16 :goto_0

    .line 1454965
    :cond_7
    iget-object v1, v0, LX/9Im;->g:Landroid/widget/BaseAdapter;

    if-eqz v1, :cond_8

    .line 1454966
    iget-object v1, v0, LX/9Im;->g:Landroid/widget/BaseAdapter;

    iget-object v2, v0, LX/9Im;->d:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1454967
    :cond_8
    iget-object v1, v0, LX/9Im;->f:LX/0g8;

    if-eqz v1, :cond_9

    .line 1454968
    iget-object v1, v0, LX/9Im;->f:LX/0g8;

    iget-object v2, v0, LX/9Im;->b:LX/0fx;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 1454969
    :cond_9
    iget-object v1, v0, LX/9Im;->a:LX/9Ik;

    .line 1454970
    iput-object v3, v1, LX/9Ik;->f:LX/0g8;

    .line 1454971
    iput-object v3, v0, LX/9Im;->g:Landroid/widget/BaseAdapter;

    .line 1454972
    iput-object v3, v0, LX/9Im;->f:LX/0g8;

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1454973
    iget-object v0, p0, LX/9DG;->J:LX/0g8;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9DG;->w:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1454974
    const-string v0, "scrollingViewProxy"

    iget-object v1, p0, LX/9DG;->J:LX/0g8;

    invoke-interface {v1}, LX/0g8;->A()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1454975
    :cond_0
    const-string v0, "mediaItemDestination"

    iget-object v1, p0, LX/9DG;->U:LX/9CO;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1454976
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    .line 1454977
    const-string v1, "composerManagerState"

    iget-object p0, v0, LX/9CP;->E:LX/9CN;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1454978
    invoke-static {v0}, LX/9CP;->O(LX/9CP;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1454979
    :goto_0
    return-void

    .line 1454980
    :cond_1
    const-string v1, "replyFeedback"

    iget-object p0, v0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p1, v1, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1454981
    const-string v1, "replyingToTopLevelComment"

    iget-boolean p0, v0, LX/9CP;->H:Z

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1454982
    const-string v1, "replyingToAuthorName"

    iget-object p0, v0, LX/9CP;->q:Ljava/lang/String;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1454983
    const-string v1, "replyingToAuthorId"

    iget-object p0, v0, LX/9CP;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1454984
    const-string v1, "replyingToCommentId"

    iget-object p0, v0, LX/9CP;->p:Ljava/lang/String;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/tagging/model/TaggingProfile;)V
    .locals 0

    .prologue
    .line 1454985
    invoke-virtual {p0, p1}, LX/9DG;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    .line 1454986
    invoke-virtual {p0}, LX/9DG;->g()Z

    .line 1454987
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1454988
    iget-object v0, p0, LX/9DG;->x:LX/9Im;

    .line 1454989
    iget-boolean v2, v0, LX/9Im;->e:Z

    if-nez v2, :cond_1

    .line 1454990
    :goto_0
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    const/4 v2, 0x0

    .line 1454991
    iput-object v2, v0, LX/9CP;->y:LX/9D1;

    .line 1454992
    iput-object v2, v0, LX/9CP;->z:LX/9FA;

    .line 1454993
    iput-object v2, v0, LX/9CP;->A:LX/9Ce;

    .line 1454994
    iget-object v0, p0, LX/9DG;->d:LX/9Ce;

    .line 1454995
    iput-object v1, v0, LX/9Ce;->b:LX/9CP;

    .line 1454996
    iget-boolean v0, p0, LX/9DG;->z:Z

    if-nez v0, :cond_0

    .line 1454997
    iget-object v0, p0, LX/9DG;->h:LX/9DS;

    invoke-interface {v0}, LX/9DS;->a()V

    .line 1454998
    :cond_0
    iget-object v0, p0, LX/9DG;->D:LX/9F7;

    invoke-virtual {v0}, LX/9F7;->b()V

    .line 1454999
    iget-object v0, p0, LX/9DG;->i:LX/9Da;

    invoke-virtual {v0}, LX/9Da;->a()V

    .line 1455000
    iget-object v0, p0, LX/9DG;->i:LX/9Da;

    .line 1455001
    iput-object v1, v0, LX/9Da;->u:LX/9DA;

    .line 1455002
    iget-object v0, p0, LX/9DG;->f:LX/9D1;

    const/4 v2, 0x0

    .line 1455003
    iput-object v2, v0, LX/9D1;->i:Ljava/util/List;

    .line 1455004
    iput-object v2, v0, LX/9D1;->k:LX/9Im;

    .line 1455005
    iget-object v0, p0, LX/9DG;->b:LX/9CC;

    invoke-virtual {v0}, LX/62C;->dispose()V

    .line 1455006
    iget-object v0, p0, LX/9DG;->c:LX/9FA;

    const/4 v2, 0x0

    .line 1455007
    iput-object v2, v0, LX/9FA;->a:Landroid/content/Context;

    .line 1455008
    iput-object v2, v0, LX/9FA;->b:LX/9Bi;

    .line 1455009
    iput-object v2, v0, LX/9FA;->f:LX/9Ce;

    .line 1455010
    iput-object v2, v0, LX/9FA;->h:LX/1QR;

    .line 1455011
    iput-object v2, v0, LX/9FA;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1455012
    iput-object v2, v0, LX/9FA;->m:LX/9FD;

    .line 1455013
    iput-object v2, v0, LX/9FA;->o:LX/9Im;

    .line 1455014
    iput-object v2, v0, LX/9FA;->p:LX/9CP;

    .line 1455015
    iput-object v1, p0, LX/9DG;->K:Landroid/os/Parcelable;

    .line 1455016
    iput-object v1, p0, LX/9DG;->N:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1455017
    iput-object v1, p0, LX/9DG;->S:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1455018
    iput-object v1, p0, LX/9DG;->O:Landroid/support/v4/app/Fragment;

    .line 1455019
    iget-object v0, p0, LX/9DG;->A:LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1455020
    return-void

    .line 1455021
    :cond_1
    iget-object v2, v0, LX/9Im;->a:LX/9Ik;

    iget-object v3, v0, LX/9Im;->c:LX/9Ij;

    .line 1455022
    iget-object v0, v2, LX/9Ik;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1455023
    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1455024
    iget-object v0, p0, LX/9DG;->x:LX/9Im;

    .line 1455025
    iget-boolean v1, v0, LX/9Im;->e:Z

    if-nez v1, :cond_0

    .line 1455026
    :goto_0
    iget-object v0, p0, LX/9DG;->i:LX/9Da;

    invoke-virtual {v0}, LX/9Da;->a()V

    .line 1455027
    iget-object v0, p0, LX/9DG;->A:LX/1My;

    invoke-virtual {v0}, LX/1My;->d()V

    .line 1455028
    return-void

    .line 1455029
    :cond_0
    iget-object v1, v0, LX/9Im;->a:LX/9Ik;

    invoke-virtual {v1}, LX/9Ik;->c()V

    .line 1455030
    iget-object v1, v0, LX/9Im;->a:LX/9Ik;

    const/4 v2, 0x0

    .line 1455031
    iput-boolean v2, v1, LX/9Ik;->g:Z

    .line 1455032
    iget-object v1, v0, LX/9Im;->c:LX/9Ij;

    invoke-virtual {v1}, LX/9Ij;->a()V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1455033
    iget-object v0, p0, LX/9DG;->x:LX/9Im;

    .line 1455034
    iget-boolean v1, v0, LX/9Im;->e:Z

    if-nez v1, :cond_1

    .line 1455035
    :goto_0
    iget-object v1, p0, LX/9DG;->i:LX/9Da;

    iget-object v0, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    :goto_1
    invoke-virtual {v1, v0}, LX/9Da;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1455036
    iget-object v0, p0, LX/9DG;->A:LX/1My;

    invoke-virtual {v0}, LX/1My;->e()V

    .line 1455037
    return-void

    .line 1455038
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1455039
    :cond_1
    iget-object v1, v0, LX/9Im;->a:LX/9Ik;

    const/4 v2, 0x1

    .line 1455040
    iput-boolean v2, v1, LX/9Ik;->g:Z

    .line 1455041
    iget-object v1, v0, LX/9Im;->a:LX/9Ik;

    invoke-virtual {v1}, LX/9Ik;->b()V

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1455042
    iget-object v0, p0, LX/9DG;->e:LX/9Cy;

    .line 1455043
    iget-boolean v1, v0, LX/9Cy;->c:Z

    if-nez v1, :cond_1

    iget-object v1, v0, LX/9Cy;->b:LX/9D1;

    const/4 v2, 0x0

    .line 1455044
    iget-object p0, v1, LX/9D1;->c:LX/0g8;

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/9D1;->c:LX/0g8;

    invoke-interface {p0}, LX/0g8;->s()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-static {v1, p0, v2}, LX/9D1;->a(LX/9D1;II)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v1, v2

    .line 1455045
    if-eqz v1, :cond_1

    .line 1455046
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9Cy;->c:Z

    .line 1455047
    :cond_1
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1455048
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    if-eqz v0, :cond_0

    .line 1455049
    iget-object v0, p0, LX/9DG;->L:LX/21n;

    invoke-interface {v0}, LX/21n;->j()V

    .line 1455050
    const/4 v0, 0x1

    .line 1455051
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 7

    .prologue
    .line 1455052
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    .line 1455053
    iget-object v1, v0, LX/9CP;->g:LX/9FG;

    .line 1455054
    iget-object v2, v1, LX/9FG;->a:LX/0if;

    sget-object v3, LX/0ig;->j:LX/0ih;

    iget-wide v4, v1, LX/9FG;->b:J

    const-string v6, "entry_with_keyboard_shown"

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 1455055
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1455056
    iget-object v0, p0, LX/9DG;->m:LX/9CP;

    .line 1455057
    invoke-static {v0}, LX/9CP;->s(LX/9CP;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1455058
    invoke-virtual {v0}, LX/9CP;->j()V

    .line 1455059
    const/4 p0, 0x1

    .line 1455060
    :goto_0
    move v0, p0

    .line 1455061
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
