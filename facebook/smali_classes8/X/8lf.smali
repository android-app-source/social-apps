.class public final enum LX/8lf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8lf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8lf;

.field public static final enum JUMP_TO_VALUE:LX/8lf;

.field public static final enum SPRING_TO_VALUE:LX/8lf;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1397907
    new-instance v0, LX/8lf;

    const-string v1, "SPRING_TO_VALUE"

    invoke-direct {v0, v1, v2}, LX/8lf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lf;->SPRING_TO_VALUE:LX/8lf;

    .line 1397908
    new-instance v0, LX/8lf;

    const-string v1, "JUMP_TO_VALUE"

    invoke-direct {v0, v1, v3}, LX/8lf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lf;->JUMP_TO_VALUE:LX/8lf;

    .line 1397909
    const/4 v0, 0x2

    new-array v0, v0, [LX/8lf;

    sget-object v1, LX/8lf;->SPRING_TO_VALUE:LX/8lf;

    aput-object v1, v0, v2

    sget-object v1, LX/8lf;->JUMP_TO_VALUE:LX/8lf;

    aput-object v1, v0, v3

    sput-object v0, LX/8lf;->$VALUES:[LX/8lf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1397906
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8lf;
    .locals 1

    .prologue
    .line 1397905
    const-class v0, LX/8lf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8lf;

    return-object v0
.end method

.method public static values()[LX/8lf;
    .locals 1

    .prologue
    .line 1397904
    sget-object v0, LX/8lf;->$VALUES:[LX/8lf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8lf;

    return-object v0
.end method
