.class public final enum LX/99r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/99r;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/99r;

.field public static final enum Ego:LX/99r;

.field public static final enum Photo:LX/99r;

.field public static final enum PivotOnClick:LX/99r;

.field public static final enum PivotOnComment:LX/99r;

.field public static final enum PivotOnShare:LX/99r;

.field public static final enum Text:LX/99r;


# instance fields
.field private mLastImpressionLogTime:J

.field private final mType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1447829
    new-instance v0, LX/99r;

    const-string v1, "Text"

    const-string v2, "text"

    invoke-direct {v0, v1, v4, v2}, LX/99r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/99r;->Text:LX/99r;

    .line 1447830
    new-instance v0, LX/99r;

    const-string v1, "Photo"

    const-string v2, "photo"

    invoke-direct {v0, v1, v5, v2}, LX/99r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/99r;->Photo:LX/99r;

    .line 1447831
    new-instance v0, LX/99r;

    const-string v1, "Ego"

    const-string v2, "ego"

    invoke-direct {v0, v1, v6, v2}, LX/99r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/99r;->Ego:LX/99r;

    .line 1447832
    new-instance v0, LX/99r;

    const-string v1, "PivotOnClick"

    const-string v2, "pivot_on_click"

    invoke-direct {v0, v1, v7, v2}, LX/99r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/99r;->PivotOnClick:LX/99r;

    .line 1447833
    new-instance v0, LX/99r;

    const-string v1, "PivotOnComment"

    const-string v2, "pivot_on_comment"

    invoke-direct {v0, v1, v8, v2}, LX/99r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/99r;->PivotOnComment:LX/99r;

    .line 1447834
    new-instance v0, LX/99r;

    const-string v1, "PivotOnShare"

    const/4 v2, 0x5

    const-string v3, "pivot_on_share"

    invoke-direct {v0, v1, v2, v3}, LX/99r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/99r;->PivotOnShare:LX/99r;

    .line 1447835
    const/4 v0, 0x6

    new-array v0, v0, [LX/99r;

    sget-object v1, LX/99r;->Text:LX/99r;

    aput-object v1, v0, v4

    sget-object v1, LX/99r;->Photo:LX/99r;

    aput-object v1, v0, v5

    sget-object v1, LX/99r;->Ego:LX/99r;

    aput-object v1, v0, v6

    sget-object v1, LX/99r;->PivotOnClick:LX/99r;

    aput-object v1, v0, v7

    sget-object v1, LX/99r;->PivotOnComment:LX/99r;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/99r;->PivotOnShare:LX/99r;

    aput-object v2, v0, v1

    sput-object v0, LX/99r;->$VALUES:[LX/99r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1447825
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1447826
    const-wide/32 v0, -0x6ddd00

    iput-wide v0, p0, LX/99r;->mLastImpressionLogTime:J

    .line 1447827
    iput-object p3, p0, LX/99r;->mType:Ljava/lang/String;

    .line 1447828
    return-void
.end method

.method public static getEntryPoint(Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)LX/99r;
    .locals 2

    .prologue
    .line 1447821
    sget-object v0, LX/99q;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1447822
    sget-object v0, LX/99r;->PivotOnClick:LX/99r;

    :goto_0
    return-object v0

    .line 1447823
    :pswitch_0
    sget-object v0, LX/99r;->PivotOnComment:LX/99r;

    goto :goto_0

    .line 1447824
    :pswitch_1
    sget-object v0, LX/99r;->PivotOnShare:LX/99r;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private shouldThrottleLogging()Z
    .locals 2

    .prologue
    .line 1447818
    sget-object v0, LX/99q;->a:[I

    invoke-virtual {p0}, LX/99r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1447819
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1447820
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/99r;
    .locals 1

    .prologue
    .line 1447817
    const-class v0, LX/99r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/99r;

    return-object v0
.end method

.method public static values()[LX/99r;
    .locals 1

    .prologue
    .line 1447816
    sget-object v0, LX/99r;->$VALUES:[LX/99r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/99r;

    return-object v0
.end method


# virtual methods
.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1447808
    iget-object v0, p0, LX/99r;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public final shouldLogImpression()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 1447809
    invoke-direct {p0}, LX/99r;->shouldThrottleLogging()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1447810
    :goto_0
    return v0

    .line 1447811
    :cond_0
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 1447812
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 1447813
    iget-wide v4, p0, LX/99r;->mLastImpressionLogTime:J

    const-wide/32 v6, 0x6ddd00

    add-long/2addr v4, v6

    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    .line 1447814
    iput-wide v2, p0, LX/99r;->mLastImpressionLogTime:J

    goto :goto_0

    .line 1447815
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
