.class public LX/9J2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/9J2;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0if;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1464167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464168
    iput-object p1, p0, LX/9J2;->a:LX/0Zb;

    .line 1464169
    iput-object p2, p0, LX/9J2;->b:LX/0if;

    .line 1464170
    return-void
.end method

.method public static a(LX/0QB;)LX/9J2;
    .locals 5

    .prologue
    .line 1464199
    sget-object v0, LX/9J2;->d:LX/9J2;

    if-nez v0, :cond_1

    .line 1464200
    const-class v1, LX/9J2;

    monitor-enter v1

    .line 1464201
    :try_start_0
    sget-object v0, LX/9J2;->d:LX/9J2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1464202
    if-eqz v2, :cond_0

    .line 1464203
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1464204
    new-instance p0, LX/9J2;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/9J2;-><init>(LX/0Zb;LX/0if;)V

    .line 1464205
    move-object v0, p0

    .line 1464206
    sput-object v0, LX/9J2;->d:LX/9J2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1464207
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1464208
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1464209
    :cond_1
    sget-object v0, LX/9J2;->d:LX/9J2;

    return-object v0

    .line 1464210
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1464211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/9J2;LX/9J1;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # LX/9J1;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1464188
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/9J1;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1464189
    invoke-virtual {p1}, LX/9J1;->getModule()Ljava/lang/String;

    move-result-object v0

    .line 1464190
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1464191
    invoke-direct {p0}, LX/9J2;->c()Ljava/lang/String;

    move-result-object v0

    .line 1464192
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1464193
    const-string v0, "goodfriends_session"

    invoke-direct {p0}, LX/9J2;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1464194
    if-eqz p2, :cond_0

    .line 1464195
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1464196
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1464197
    :cond_0
    iget-object v0, p0, LX/9J2;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1464198
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1464185
    iget-object v0, p0, LX/9J2;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1464186
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9J2;->c:Ljava/lang/String;

    .line 1464187
    :cond_0
    iget-object v0, p0, LX/9J2;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/9J1;)V
    .locals 1

    .prologue
    .line 1464183
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/9J2;->a(LX/9J2;LX/9J1;Landroid/os/Bundle;)V

    .line 1464184
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1464180
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "goodfriends_session"

    iget-object v2, p0, LX/9J2;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1464181
    iget-object v1, p0, LX/9J2;->b:LX/0if;

    sget-object v2, LX/0ig;->aw:LX/0ih;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1464182
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1464175
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1464176
    const-string v1, "placeholder_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1464177
    sget-object v1, LX/9J1;->PLACEHOLDER_SHOWN:LX/9J1;

    invoke-static {p0, v1, v0}, LX/9J2;->a(LX/9J2;LX/9J1;Landroid/os/Bundle;)V

    .line 1464178
    const-string v0, "Placeholder Shown"

    invoke-virtual {p0, v0}, LX/9J2;->a(Ljava/lang/String;)V

    .line 1464179
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1464171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1464172
    const-string v1, "media_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1464173
    sget-object v1, LX/9J1;->PUBLISH_FROM_CAMERA_TAPPED:LX/9J1;

    invoke-static {p0, v1, v0}, LX/9J2;->a(LX/9J2;LX/9J1;Landroid/os/Bundle;)V

    .line 1464174
    return-void
.end method
