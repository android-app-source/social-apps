.class public final enum LX/94z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/94z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/94z;

.field public static final enum FAILED:LX/94z;

.field public static final enum NOT_STARTED:LX/94z;

.field public static final enum RUNNING:LX/94z;

.field public static final enum SUCCEEDED:LX/94z;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1436275
    new-instance v0, LX/94z;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LX/94z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94z;->NOT_STARTED:LX/94z;

    .line 1436276
    new-instance v0, LX/94z;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, LX/94z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94z;->RUNNING:LX/94z;

    .line 1436277
    new-instance v0, LX/94z;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v4}, LX/94z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94z;->SUCCEEDED:LX/94z;

    .line 1436278
    new-instance v0, LX/94z;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/94z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94z;->FAILED:LX/94z;

    .line 1436279
    const/4 v0, 0x4

    new-array v0, v0, [LX/94z;

    sget-object v1, LX/94z;->NOT_STARTED:LX/94z;

    aput-object v1, v0, v2

    sget-object v1, LX/94z;->RUNNING:LX/94z;

    aput-object v1, v0, v3

    sget-object v1, LX/94z;->SUCCEEDED:LX/94z;

    aput-object v1, v0, v4

    sget-object v1, LX/94z;->FAILED:LX/94z;

    aput-object v1, v0, v5

    sput-object v0, LX/94z;->$VALUES:[LX/94z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1436281
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/94z;
    .locals 1

    .prologue
    .line 1436282
    const-class v0, LX/94z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/94z;

    return-object v0
.end method

.method public static values()[LX/94z;
    .locals 1

    .prologue
    .line 1436280
    sget-object v0, LX/94z;->$VALUES:[LX/94z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/94z;

    return-object v0
.end method
