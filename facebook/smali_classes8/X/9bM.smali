.class public final LX/9bM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

.field public final synthetic b:Landroid/app/Activity;

.field public final synthetic c:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1514844
    iput-object p1, p0, LX/9bM;->c:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iput-object p2, p0, LX/9bM;->a:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    iput-object p3, p0, LX/9bM;->b:Landroid/app/Activity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1514856
    sget-object v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->c:Ljava/lang/Class;

    const-string v1, "Failed to fetch FacebookPhoto by fbid"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1514857
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1514845
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1514846
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;

    .line 1514847
    if-eqz v0, :cond_0

    .line 1514848
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1514849
    if-eqz v1, :cond_0

    .line 1514850
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1514851
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1514852
    :cond_0
    :goto_0
    return-void

    .line 1514853
    :cond_1
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v0, v1

    .line 1514854
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1514855
    iget-object v1, p0, LX/9bM;->c:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v2, p0, LX/9bM;->a:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    iget-object v3, p0, LX/9bM;->b:Landroid/app/Activity;

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    goto :goto_0
.end method
