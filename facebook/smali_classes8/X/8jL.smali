.class public LX/8jL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/8jJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1392333
    const-class v0, LX/8jL;

    sput-object v0, LX/8jL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/8jJ;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/client/IsAnimatedStickersEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/8jJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392335
    iput-object p1, p0, LX/8jL;->b:LX/0Or;

    .line 1392336
    iput-object p2, p0, LX/8jL;->c:LX/8jJ;

    .line 1392337
    return-void
.end method

.method public static a(LX/0QB;)LX/8jL;
    .locals 1

    .prologue
    .line 1392338
    invoke-static {p0}, LX/8jL;->b(LX/0QB;)LX/8jL;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/8jL;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 3

    .prologue
    .line 1392339
    iget-object v0, p0, LX/8jL;->c:LX/8jJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/8jJ;->a(Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1392340
    if-eqz v0, :cond_0

    .line 1392341
    new-instance v1, LX/8jK;

    invoke-direct {v1, p0}, LX/8jK;-><init>(LX/8jL;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1392342
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/8jL;
    .locals 3

    .prologue
    .line 1392343
    new-instance v1, LX/8jL;

    const/16 v0, 0x1578

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/8jJ;->b(LX/0QB;)LX/8jJ;

    move-result-object v0

    check-cast v0, LX/8jJ;

    invoke-direct {v1, v2, v0}, LX/8jL;-><init>(LX/0Or;LX/8jJ;)V

    .line 1392344
    return-object v1
.end method
