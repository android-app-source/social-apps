.class public LX/958;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1436388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436389
    return-void
.end method

.method public static a(LX/0QB;)LX/958;
    .locals 1

    .prologue
    .line 1436393
    new-instance v0, LX/958;

    invoke-direct {v0}, LX/958;-><init>()V

    .line 1436394
    move-object v0, v0

    .line 1436395
    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1436390
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1436391
    sget-object v0, LX/51s;->a:LX/51l;

    move-object v0, v0

    .line 1436392
    const-string v1, ":"

    invoke-interface {p0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1, v2}, LX/51l;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
