.class public final LX/95q;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

.field public final synthetic b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public final synthetic c:LX/1L9;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/5Gr;

.field public final synthetic g:LX/3iQ;


# direct methods
.method public constructor <init>(LX/3iQ;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;LX/5Gr;)V
    .locals 0

    .prologue
    .line 1437918
    iput-object p1, p0, LX/95q;->g:LX/3iQ;

    iput-object p2, p0, LX/95q;->a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iput-object p3, p0, LX/95q;->b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object p4, p0, LX/95q;->c:LX/1L9;

    iput-object p5, p0, LX/95q;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p6, p0, LX/95q;->e:Ljava/lang/String;

    iput-object p7, p0, LX/95q;->f:LX/5Gr;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 10

    .prologue
    .line 1437919
    sget-object v0, LX/6W6;->FAILURE:LX/6W6;

    .line 1437920
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 1437921
    sget-object v2, LX/1nY;->OTHER:LX/1nY;

    invoke-virtual {v1, v2}, LX/1nY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/95q;->a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v1, v1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/95q;->g:LX/3iQ;

    iget-object v1, v1, LX/3iQ;->o:LX/0Uh;

    const/16 v2, 0x398

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1437922
    sget-object v0, LX/6W6;->OFFLINE:LX/6W6;

    .line 1437923
    iget-object v1, p0, LX/95q;->g:LX/3iQ;

    iget-object v2, p0, LX/95q;->e:Ljava/lang/String;

    iget-object v3, p0, LX/95q;->a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v3, v3, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    iget-object v4, p0, LX/95q;->f:LX/5Gr;

    .line 1437924
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1437925
    const-string v6, "addPhotoAttachmentParams"

    invoke-static {}, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a()LX/6BG;

    move-result-object v7

    .line 1437926
    iput-object v2, v7, LX/6BG;->b:Ljava/lang/String;

    .line 1437927
    move-object v7, v7

    .line 1437928
    iput-object v3, v7, LX/6BG;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 1437929
    move-object v7, v7

    .line 1437930
    invoke-virtual {v7}, LX/6BG;->a()Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1437931
    new-instance v6, LX/4gv;

    invoke-direct {v6}, LX/4gv;-><init>()V

    .line 1437932
    iput-object v5, v6, LX/4gv;->h:Landroid/os/Bundle;

    .line 1437933
    move-object v5, v6

    .line 1437934
    const-string v6, "feed_add_photo"

    .line 1437935
    iput-object v6, v5, LX/4gv;->g:Ljava/lang/String;

    .line 1437936
    move-object v5, v5

    .line 1437937
    const-wide/16 v7, 0x0

    .line 1437938
    iput-wide v7, v5, LX/3G2;->c:J

    .line 1437939
    move-object v5, v5

    .line 1437940
    const/4 v6, 0x1

    .line 1437941
    iput v6, v5, LX/3G2;->e:I

    .line 1437942
    move-object v5, v5

    .line 1437943
    const/4 v6, 0x0

    .line 1437944
    iput-object v6, v5, LX/3G2;->b:Ljava/lang/String;

    .line 1437945
    move-object v5, v5

    .line 1437946
    sget-object v6, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v7, 0x1

    invoke-virtual {v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v7

    .line 1437947
    iput-wide v7, v5, LX/3G2;->d:J

    .line 1437948
    move-object v5, v5

    .line 1437949
    const/16 v6, 0x64

    .line 1437950
    iput v6, v5, LX/3G2;->f:I

    .line 1437951
    move-object v5, v5

    .line 1437952
    iput-object v2, v5, LX/3G2;->a:Ljava/lang/String;

    .line 1437953
    move-object v5, v5

    .line 1437954
    invoke-virtual {v5}, LX/3G2;->a()LX/3G3;

    move-result-object v5

    check-cast v5, LX/4gw;

    .line 1437955
    iget-object v6, v1, LX/3iQ;->q:LX/2xs;

    invoke-virtual {v4}, LX/5Gr;->a()Lcom/facebook/api/ufiservices/common/AddCommentParams;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2xs;->a(Lcom/facebook/api/ufiservices/common/AddCommentParams;)LX/399;

    move-result-object v6

    .line 1437956
    new-instance v7, LX/3G1;

    invoke-direct {v7}, LX/3G1;-><init>()V

    .line 1437957
    invoke-virtual {v7, v6}, LX/3G1;->a(LX/399;)LX/3G1;

    .line 1437958
    invoke-virtual {v7}, LX/3G1;->b()LX/3G4;

    move-result-object v6

    .line 1437959
    iput-object v6, v5, LX/4gw;->j:LX/3G4;

    .line 1437960
    iget-object v6, v1, LX/3iQ;->p:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/controller/mutation/CommentMutationHelper$2;

    invoke-direct {v7, v1, v5}, Lcom/facebook/controller/mutation/CommentMutationHelper$2;-><init>(LX/3iQ;LX/4gw;)V

    const v5, -0x160fb388

    invoke-static {v6, v7, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1437961
    :cond_0
    iget-object v1, p0, LX/95q;->g:LX/3iQ;

    iget-object v1, v1, LX/3iQ;->a:LX/3iR;

    iget-object v2, p0, LX/95q;->a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v2, v2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    iget-object v3, p0, LX/95q;->b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v1, v0, v2, v3}, LX/3iR;->a(LX/6W6;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1437962
    iget-object v0, p0, LX/95q;->c:LX/1L9;

    if-eqz v0, :cond_1

    .line 1437963
    iget-object v0, p0, LX/95q;->c:LX/1L9;

    iget-object v1, p0, LX/95q;->d:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-interface {v0, v1, p1}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1437964
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1437965
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1437966
    if-nez p1, :cond_1

    .line 1437967
    iget-object v0, p0, LX/95q;->g:LX/3iQ;

    iget-object v0, v0, LX/3iQ;->a:LX/3iR;

    sget-object v1, LX/6W6;->OFFLINE:LX/6W6;

    iget-object v2, p0, LX/95q;->a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v2, v2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    iget-object v3, p0, LX/95q;->b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0, v1, v2, v3}, LX/3iR;->a(LX/6W6;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1437968
    iget-object v0, p0, LX/95q;->c:LX/1L9;

    if-eqz v0, :cond_0

    .line 1437969
    iget-object v0, p0, LX/95q;->c:LX/1L9;

    iget-object v1, p0, LX/95q;->d:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-interface {v0, v1}, LX/1L9;->c(Ljava/lang/Object;)V

    .line 1437970
    :cond_0
    :goto_0
    return-void

    .line 1437971
    :cond_1
    iget-object v0, p0, LX/95q;->g:LX/3iQ;

    iget-object v0, v0, LX/3iQ;->a:LX/3iR;

    sget-object v1, LX/6W6;->SUCCESS:LX/6W6;

    iget-object v2, p0, LX/95q;->a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v2, v2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    iget-object v3, p0, LX/95q;->b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0, v1, v2, v3}, LX/3iR;->a(LX/6W6;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1437972
    iget-object v0, p0, LX/95q;->g:LX/3iQ;

    iget-object v0, v0, LX/3iQ;->l:LX/0ie;

    .line 1437973
    iget-object v1, v0, LX/0ie;->a:LX/0if;

    sget-object v2, LX/0ig;->ar:LX/0ih;

    sget-object v3, LX/8D1;->COMMENT:LX/8D1;

    invoke-virtual {v3}, LX/8D1;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1437974
    const/4 v2, 0x0

    .line 1437975
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    .line 1437976
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_6

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1437977
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1437978
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 1437979
    if-eqz v1, :cond_5

    .line 1437980
    const/4 v1, 0x1

    .line 1437981
    :goto_2
    move v1, v1

    .line 1437982
    move v0, v1

    .line 1437983
    if-eqz v0, :cond_4

    .line 1437984
    iget-object v0, p0, LX/95q;->g:LX/3iQ;

    iget-object v1, p0, LX/95q;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/95q;->a:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v2, v2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    iget-object v3, p0, LX/95q;->c:LX/1L9;

    .line 1437985
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1437986
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_3
    if-ge v5, v7, :cond_3

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1437987
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 1437988
    if-eqz v4, :cond_2

    .line 1437989
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1437990
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 1437991
    :cond_3
    new-instance v4, LX/95r;

    move-object v5, v0

    move-object v6, v1

    move-object v7, p1

    move-object v8, v2

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, LX/95r;-><init>(LX/3iQ;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;LX/1L9;)V

    .line 1437992
    iget-object v5, v0, LX/3iQ;->j:LX/3ib;

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, LX/3ib;->a(LX/0Px;LX/8Nj;)LX/8Np;

    move-result-object v4

    .line 1437993
    invoke-virtual {v4}, LX/8Np;->a()V

    .line 1437994
    goto/16 :goto_0

    .line 1437995
    :cond_4
    iget-object v0, p0, LX/95q;->c:LX/1L9;

    if-eqz v0, :cond_0

    .line 1437996
    iget-object v0, p0, LX/95q;->c:LX/1L9;

    invoke-interface {v0, p1}, LX/1L9;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1437997
    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_6
    move v1, v2

    .line 1437998
    goto :goto_2
.end method
