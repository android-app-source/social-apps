.class public final LX/8lr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/8ls;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/search/TrayStickerIdsLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/TrayStickerIdsLoader;)V
    .locals 0

    .prologue
    .line 1398217
    iput-object p1, p0, LX/8lr;->a:Lcom/facebook/stickers/search/TrayStickerIdsLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1398218
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1398219
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1398220
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1398221
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v1, v1

    .line 1398222
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1398223
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "sticker db not initialized, try again later."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1398224
    :cond_0
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v1

    .line 1398225
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1398226
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1398227
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v1, p0

    .line 1398228
    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1398229
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1398230
    :cond_1
    new-instance v0, LX/8ls;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8ls;-><init>(LX/0Px;)V

    return-object v0
.end method
