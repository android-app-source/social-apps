.class public LX/8u7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1414480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/text/Spanned;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 12

    .prologue
    .line 1414481
    const/4 v0, 0x0

    sget-object v1, LX/8ty;->a:LX/8tz;

    .line 1414482
    new-instance v2, LX/8u0;

    invoke-direct {v2}, LX/8u0;-><init>()V

    .line 1414483
    iput-object v1, v2, LX/8u0;->c:LX/8tv;

    .line 1414484
    move-object v2, v2

    .line 1414485
    new-instance v3, LX/8u1;

    invoke-direct {v3, v2}, LX/8u1;-><init>(LX/8u0;)V

    .line 1414486
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "<"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "fbhtml>"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "</fbhtml"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, ">"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1414487
    invoke-static {v2, v0, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    move-object v0, v2

    .line 1414488
    move-object v0, v0

    .line 1414489
    const/4 v1, 0x0

    .line 1414490
    if-nez v0, :cond_0

    .line 1414491
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1414492
    :goto_0
    move-object v0, v1

    .line 1414493
    return-object v0

    .line 1414494
    :cond_0
    if-nez p0, :cond_1

    .line 1414495
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1414496
    :cond_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1414497
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v4, Ljava/lang/Object;

    invoke-interface {p0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    .line 1414498
    new-instance v3, LX/8u6;

    invoke-direct {v3, p0}, LX/8u6;-><init>(Landroid/text/Spanned;)V

    invoke-static {v4, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1414499
    array-length v5, v4

    .line 1414500
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1414501
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    move v3, v1

    .line 1414502
    :goto_1
    if-ge v3, v5, :cond_4

    .line 1414503
    aget-object v8, v4, v3

    invoke-interface {p0, v8}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v8

    .line 1414504
    aget-object v9, v4, v3

    invoke-interface {p0, v9}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v9

    .line 1414505
    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v10

    .line 1414506
    const/4 v11, -0x1

    if-eq v10, v11, :cond_2

    .line 1414507
    add-int/2addr v9, v10

    sub-int/2addr v9, v8

    .line 1414508
    aget-object v11, v4, v3

    aget-object p1, v4, v3

    invoke-interface {p0, p1}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {v2, v11, v10, v9, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1414509
    :cond_2
    add-int/lit8 v9, v5, -0x1

    if-ge v3, v9, :cond_3

    .line 1414510
    add-int/lit8 v1, v3, 0x1

    aget-object v1, v4, v1

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 1414511
    sub-int/2addr v1, v8

    add-int/2addr v1, v10

    .line 1414512
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    move-object v1, v2

    .line 1414513
    goto :goto_0
.end method
