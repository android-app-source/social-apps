.class public LX/9A3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private a:LX/1nG;

.field private final b:Landroid/content/Context;

.field private final c:LX/0SG;

.field private final d:LX/8zJ;

.field private final e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0qn;

.field public final h:LX/9A6;

.field private final i:LX/0ad;

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLStory;

.field public o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:J

.field public q:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Lcom/facebook/user/model/User;

.field public v:Ljava/lang/String;

.field public w:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/1nG;LX/8zJ;LX/0qn;LX/9A6;LX/0ad;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;LX/0Px;)V
    .locals 1
    .param p8    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/1nG;",
            "LX/8zJ;",
            "LX/0qn;",
            "LX/9A6;",
            "LX/0ad;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1448437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1448438
    iput-object p1, p0, LX/9A3;->b:Landroid/content/Context;

    .line 1448439
    iput-object p2, p0, LX/9A3;->c:LX/0SG;

    .line 1448440
    iput-object p3, p0, LX/9A3;->a:LX/1nG;

    .line 1448441
    iput-object p4, p0, LX/9A3;->d:LX/8zJ;

    .line 1448442
    iput-object p5, p0, LX/9A3;->g:LX/0qn;

    .line 1448443
    invoke-virtual {p6, p10}, LX/9A6;->a(LX/0Px;)LX/9A6;

    move-result-object v0

    iput-object v0, p0, LX/9A3;->h:LX/9A6;

    .line 1448444
    iput-object p8, p0, LX/9A3;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1448445
    iput-object p9, p0, LX/9A3;->f:LX/0Px;

    .line 1448446
    iput-object p7, p0, LX/9A3;->i:LX/0ad;

    .line 1448447
    return-void
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1448448
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448449
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1448450
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1448451
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v6, v0}, LX/9A3;->b(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1448452
    if-eqz v0, :cond_0

    .line 1448453
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1448454
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1448455
    :cond_1
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;Lcom/facebook/graphql/model/GraphQLPlace;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 4

    .prologue
    .line 1448456
    if-eqz p0, :cond_0

    if-eqz p1, :cond_1

    .line 1448457
    :cond_0
    const/4 v0, 0x0

    .line 1448458
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/4Y6;

    invoke-direct {v0}, LX/4Y6;-><init>()V

    iget-wide v2, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->pageId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1448459
    iput-object v1, v0, LX/4Y6;->n:Ljava/lang/String;

    .line 1448460
    move-object v0, v0

    .line 1448461
    iget-object v1, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    .line 1448462
    iput-object v1, v0, LX/4Y6;->r:Ljava/lang/String;

    .line 1448463
    move-object v0, v0

    .line 1448464
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1448465
    const-wide/16 v2, 0x0

    cmp-long v0, p0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1448466
    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 1448467
    :cond_0
    const/4 v0, 0x0

    .line 1448468
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 1448469
    goto :goto_0

    .line 1448470
    :cond_2
    invoke-static {p3, v1, v1}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1448471
    new-instance v1, LX/25F;

    invoke-direct {v1}, LX/25F;-><init>()V

    .line 1448472
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1448473
    iput-object v2, v1, LX/25F;->C:Ljava/lang/String;

    .line 1448474
    iput-object p2, v1, LX/25F;->T:Ljava/lang/String;

    .line 1448475
    iput-object v0, v1, LX/25F;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448476
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x285feb

    invoke-direct {v0, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1448477
    iput-object v0, v1, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1448478
    invoke-virtual {v1}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 1448561
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    const/4 v1, 0x0

    .line 1448562
    iput-object v1, v0, LX/23u;->m:Ljava/lang/String;

    .line 1448563
    move-object v0, v0

    .line 1448564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->N()Z

    move-result v1

    .line 1448565
    iput-boolean v1, v0, LX/23u;->o:Z

    .line 1448566
    move-object v0, v0

    .line 1448567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    .line 1448568
    iput-wide v2, v0, LX/23u;->v:J

    .line 1448569
    move-object v0, v0

    .line 1448570
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object p2

    .line 1448571
    :cond_0
    iput-object p2, v0, LX/23u;->T:Ljava/lang/String;

    .line 1448572
    move-object v1, v0

    .line 1448573
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1448574
    :goto_0
    iput-object v0, v1, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1448575
    move-object v0, v1

    .line 1448576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    .line 1448577
    iput-object v1, v0, LX/23u;->t:Ljava/lang/String;

    .line 1448578
    move-object v0, v0

    .line 1448579
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1448580
    return-object v0

    .line 1448581
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 6
    .param p4    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/composer/minutiae/model/MinutiaeObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImageAtRange;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            ")",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;"
        }
    .end annotation

    .prologue
    .line 1448479
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1448480
    invoke-static {p1, p4}, LX/9A3;->a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 1448481
    if-eqz v0, :cond_0

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1448482
    :try_start_0
    new-instance v2, LX/1yN;

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v2, v3, v0}, LX/1yN;-><init>(II)V

    invoke-static {p2, v2}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object v0

    .line 1448483
    new-instance v2, LX/4Vo;

    invoke-direct {v2}, LX/4Vo;-><init>()V

    .line 1448484
    iget v3, v0, LX/1yL;->a:I

    move v3, v3

    .line 1448485
    iput v3, v2, LX/4Vo;->d:I

    .line 1448486
    move-object v2, v2

    .line 1448487
    iget v3, v0, LX/1yL;->b:I

    move v0, v3

    .line 1448488
    iput v0, v2, LX/4Vo;->c:I

    .line 1448489
    move-object v0, v2

    .line 1448490
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 1448491
    iput v2, v0, LX/4Vo;->b:I

    .line 1448492
    move-object v0, v0

    .line 1448493
    const/4 v2, 0x1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {p4, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 1448494
    new-instance v3, LX/4Ws;

    invoke-direct {v3}, LX/4Ws;-><init>()V

    invoke-static {v2, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1448495
    iput-object v2, v0, LX/4Vo;->e:LX/0Px;

    .line 1448496
    move-object v0, v0

    .line 1448497
    invoke-virtual {v0}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_1

    .line 1448498
    :cond_0
    :goto_0
    move-object v1, v1

    .line 1448499
    const/4 v0, 0x1

    new-array v0, v0, [LX/1y5;

    const/4 v2, 0x0

    invoke-static {p3}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1448500
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1448501
    invoke-static {v0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1448502
    :cond_1
    if-eqz p5, :cond_2

    .line 1448503
    invoke-static {p5}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/1y5;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1448504
    :cond_2
    if-eqz p6, :cond_3

    .line 1448505
    invoke-virtual {p6}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->e()LX/1y5;

    move-result-object v0

    .line 1448506
    if-eqz v0, :cond_3

    invoke-interface {v0}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1448507
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1448508
    :cond_3
    const/4 v5, -0x1

    .line 1448509
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1448510
    if-nez p2, :cond_5

    move-object v0, v4

    .line 1448511
    :goto_2
    move-object v2, v0

    .line 1448512
    if-eqz p8, :cond_4

    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1448513
    :try_start_1
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 1448514
    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v3

    .line 1448515
    iput-object v3, v0, LX/170;->o:Ljava/lang/String;

    .line 1448516
    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 1448517
    iput-object v3, v0, LX/170;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1448518
    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->A()Ljava/lang/String;

    move-result-object v3

    .line 1448519
    iput-object v3, v0, LX/170;->A:Ljava/lang/String;

    .line 1448520
    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 1448521
    iput-object v3, v0, LX/170;->B:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1448522
    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->G()Ljava/lang/String;

    move-result-object v3

    .line 1448523
    iput-object v3, v0, LX/170;->Y:Ljava/lang/String;

    .line 1448524
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, 0x3c68e4f

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1448525
    iput-object v3, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1448526
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    move-object v0, v0

    .line 1448527
    new-instance v3, LX/1yN;

    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p8}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {v3, v4, v5}, LX/1yN;-><init>(II)V

    invoke-static {p2, v3}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object v3

    invoke-static {v0, v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_0

    .line 1448528
    :cond_4
    :goto_3
    invoke-static {p2, v2, p7, v1}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0

    .line 1448529
    :catch_0
    move-exception v0

    .line 1448530
    const-string v3, "OptimisticPostStoryBuilder"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1448531
    :catch_1
    move-exception v0

    .line 1448532
    const-string v2, "OptimisticPostStoryBuilder"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1448533
    :cond_5
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object p0

    .line 1448534
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1y5;

    .line 1448535
    if-eqz v0, :cond_b

    .line 1448536
    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object p3

    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    :goto_5
    invoke-virtual {p2, p3, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .line 1448537
    :goto_6
    if-eq v3, v5, :cond_6

    .line 1448538
    if-eqz v0, :cond_7

    invoke-interface {v0}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p3

    if-nez p3, :cond_c

    .line 1448539
    :cond_7
    const/4 p3, 0x0

    .line 1448540
    :goto_7
    move-object p3, p3

    .line 1448541
    if-eqz p3, :cond_8

    .line 1448542
    :try_start_2
    new-instance p4, LX/1yN;

    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result p5

    invoke-direct {p4, v3, p5}, LX/1yN;-><init>(II)V

    invoke-static {p2, p4}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object p4

    invoke-static {p3, p4}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object p3

    invoke-interface {v4, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch LX/47A; {:try_start_2 .. :try_end_2} :catch_2

    .line 1448543
    :cond_8
    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1448544
    :cond_9
    const/4 v3, 0x0

    goto :goto_5

    .line 1448545
    :catch_2
    move-exception v0

    .line 1448546
    const-string v3, "OptimisticPostStoryBuilder"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-static {v3, p3, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_a
    move-object v0, v4

    .line 1448547
    goto/16 :goto_2

    :cond_b
    move v3, v5

    goto :goto_6

    :cond_c
    invoke-interface {v0}, LX/1y5;->e()Ljava/lang/String;

    move-result-object p3

    invoke-interface {v0}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p4

    invoke-static {p3, p4}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object p3

    goto :goto_7
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 5

    .prologue
    .line 1448548
    iget-object v0, p0, LX/9A3;->b:Landroid/content/Context;

    const v1, 0x7f081469

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1448549
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    .line 1448550
    iput-object v0, v1, LX/173;->f:Ljava/lang/String;

    .line 1448551
    move-object v0, v1

    .line 1448552
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1448553
    iget-object v1, p0, LX/9A3;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-eqz v1, :cond_1

    move-object p1, v0

    .line 1448554
    :cond_0
    :goto_0
    return-object p1

    .line 1448555
    :cond_1
    iget-object v1, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1448556
    iget-object v1, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0, v0, v0}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    goto :goto_0

    .line 1448557
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_0

    :cond_3
    move-object p1, v0

    .line 1448558
    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;LX/0Px;Lcom/facebook/graphql/model/GraphQLPlace;LX/0Px;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 1448559
    iget-object v1, p0, LX/9A3;->b:Landroid/content/Context;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/9A3;->a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPlace;LX/0Px;)Ljava/lang/String;

    move-result-object v2

    .line 1448560
    iget-object v1, p0, LX/9A3;->b:Landroid/content/Context;

    iget-object v8, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v9

    move-object v7, v9

    invoke-static/range {v0 .. v8}, LX/9A3;->a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPlace;LX/0Px;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 11
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;"
        }
    .end annotation

    .prologue
    .line 1448364
    iget-object v1, p0, LX/9A3;->d:LX/8zJ;

    new-instance v2, LX/8z6;

    invoke-direct {v2}, LX/8z6;-><init>()V

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, LX/8z6;->b(Ljava/lang/String;)LX/8z6;

    move-result-object v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, LX/8z6;->a(I)LX/8z6;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/8z6;->a(Z)LX/8z6;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/8z6;->b(Z)LX/8z6;

    move-result-object v0

    iget-object v2, p0, LX/9A3;->l:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0, v2}, LX/8z6;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/8z6;

    move-result-object v0

    iget-object v2, p0, LX/9A3;->q:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0, v2}, LX/8z6;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/8z6;

    move-result-object v0

    invoke-virtual {v0}, LX/8z6;->a()LX/8z5;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8zJ;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    .line 1448365
    iget-object v1, p0, LX/9A3;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/9A3;->a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v2

    .line 1448366
    const/4 v0, 0x0

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const-class v3, Landroid/net/Uri;

    invoke-virtual {v6, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/Uri;

    .line 1448367
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1448368
    array-length v4, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v0, v1

    .line 1448369
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 1448370
    invoke-virtual {v6, v5}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v8

    add-int/2addr v8, v7

    .line 1448371
    invoke-virtual {v6, v5}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v9

    add-int/2addr v7, v9

    .line 1448372
    new-instance v9, LX/4Wv;

    invoke-direct {v9}, LX/4Wv;-><init>()V

    sub-int/2addr v7, v8

    invoke-virtual {v9, v7}, LX/4Wv;->a(I)LX/4Wv;

    move-result-object v7

    invoke-virtual {v7, v8}, LX/4Wv;->b(I)LX/4Wv;

    move-result-object v7

    new-instance v8, LX/4W7;

    invoke-direct {v8}, LX/4W7;-><init>()V

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v5, v9, v10}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/4W7;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/4W7;

    move-result-object v5

    invoke-virtual {v5}, LX/4W7;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/4Wv;->a(Lcom/facebook/graphql/model/GraphQLEntityWithImage;)LX/4Wv;

    move-result-object v5

    invoke-virtual {v5}, LX/4Wv;->a()Lcom/facebook/graphql/model/GraphQLImageAtRange;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448373
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1448374
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1448375
    :cond_1
    iget-object v1, p0, LX/9A3;->b:Landroid/content/Context;

    iget-object v6, p0, LX/9A3;->q:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    iget-object v8, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v8}, LX/9A3;->a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;LX/0Px;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1448376
    iget-object v0, p0, LX/9A3;->i:LX/0ad;

    sget-short v1, LX/1RY;->p:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v0, :cond_1

    .line 1448377
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1448378
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/94g;->a(Ljava/lang/String;Landroid/text/style/ClickableSpan;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    move-object v0, v1

    .line 1448379
    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1448380
    if-eqz p4, :cond_0

    .line 1448381
    invoke-virtual {v0, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1448382
    :cond_0
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 1448383
    :cond_1
    iget-object v0, p0, LX/9A3;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-eqz v0, :cond_3

    .line 1448384
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0098

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    .line 1448385
    :cond_2
    :goto_0
    return-object p4

    .line 1448386
    :cond_3
    iget-object v0, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_8

    .line 1448387
    iget-object v0, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1448388
    iget-object v0, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 1448389
    :cond_4
    invoke-static {p5}, LX/7kz;->d(LX/0Px;)I

    move-result v0

    .line 1448390
    invoke-static {p5}, LX/7kz;->c(LX/0Px;)I

    move-result v1

    .line 1448391
    invoke-static {p5}, LX/7kz;->a(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1448392
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f009c

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    iget-object v4, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v8

    invoke-virtual {v0, v2, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 1448393
    :cond_5
    invoke-static {p5}, LX/7kz;->b(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1448394
    if-ne v0, v3, :cond_6

    .line 1448395
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f009d

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    iget-object v4, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v8

    invoke-virtual {v0, v2, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448396
    :cond_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0f009e

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    iget-object v4, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-virtual {v2, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448397
    :cond_7
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f009b

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    iget-object v4, p0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v8

    invoke-virtual {v1, v2, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448398
    :cond_8
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1448399
    invoke-static {p5}, LX/7kz;->c(LX/0Px;)I

    move-result v5

    .line 1448400
    invoke-static {p5}, LX/7kz;->d(LX/0Px;)I

    move-result v0

    .line 1448401
    const/4 v1, 0x0

    .line 1448402
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result p0

    move v7, v1

    move v2, v1

    :goto_1
    if-ge v7, p0, :cond_9

    invoke-virtual {p5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    .line 1448403
    invoke-static {v1}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1448404
    add-int/lit8 v1, v2, 0x1

    .line 1448405
    :goto_2
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v2, v1

    goto :goto_1

    .line 1448406
    :cond_9
    move v1, v2

    .line 1448407
    if-lez v1, :cond_a

    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v2

    if-le v2, v3, :cond_a

    move v2, v3

    .line 1448408
    :goto_3
    if-eqz v2, :cond_17

    .line 1448409
    add-int/2addr v0, v1

    move v1, v0

    .line 1448410
    :goto_4
    invoke-virtual {p5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1448411
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    :cond_a
    move v2, v4

    .line 1448412
    goto :goto_3

    .line 1448413
    :cond_b
    const v0, 0x7f081466

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object p4, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448414
    :cond_c
    if-nez v2, :cond_e

    invoke-virtual {p5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1448415
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v2

    sget-object v7, LX/4gF;->VIDEO:LX/4gF;

    if-ne v2, v7, :cond_19

    sget-object v2, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v2, 0x1

    :goto_5
    move v0, v2

    .line 1448416
    if-eqz v0, :cond_e

    .line 1448417
    if-nez p4, :cond_d

    .line 1448418
    const v0, 0x7f081467

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448419
    :cond_d
    const v0, 0x7f081468

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object p4, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448420
    :cond_e
    invoke-static {p5}, LX/7kz;->a(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1448421
    if-nez p4, :cond_f

    .line 1448422
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0096

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448423
    :cond_f
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0097

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    aput-object p4, v2, v8

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448424
    :cond_10
    invoke-static {p5}, LX/7kz;->b(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1448425
    if-nez p4, :cond_12

    .line 1448426
    if-ne v1, v3, :cond_11

    .line 1448427
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0092

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448428
    :cond_11
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0093

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v8

    invoke-virtual {v0, v2, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448429
    :cond_12
    if-ne v1, v3, :cond_13

    .line 1448430
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0094

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    aput-object p4, v2, v8

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448431
    :cond_13
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0095

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v8

    aput-object p4, v6, v9

    invoke-virtual {v0, v2, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448432
    :cond_14
    if-nez p4, :cond_15

    .line 1448433
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0099

    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v2

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v3

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448434
    :cond_15
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009a

    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v2

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    aput-object p4, v5, v3

    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v8

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_0

    .line 1448435
    :cond_16
    invoke-static {p4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1448436
    const/4 p4, 0x0

    goto/16 :goto_0

    :cond_17
    move v1, v0

    goto/16 :goto_4

    :cond_18
    move v1, v2

    goto/16 :goto_2

    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_5
.end method

.method private static a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPlace;LX/0Px;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1448130
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1448131
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_1

    .line 1448132
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1448133
    :goto_0
    if-eqz v0, :cond_3

    if-eqz p5, :cond_3

    .line 1448134
    const v1, 0x7f081465

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p6

    .line 1448135
    invoke-static/range {v0 .. v5}, LX/9A3;->a(LX/9A3;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1448136
    :cond_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_2

    .line 1448137
    const v1, 0x7f081461

    new-array v2, v6, [Ljava/lang/Object;

    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1448138
    :cond_2
    const v1, 0x7f081462

    new-array v2, v6, [Ljava/lang/Object;

    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {p1, p4}, LX/9A3;->a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1448139
    :cond_3
    if-eqz v0, :cond_4

    if-nez p5, :cond_4

    .line 1448140
    const v1, 0x7f081463

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1448141
    :cond_4
    if-nez v0, :cond_0

    if-eqz p5, :cond_0

    .line 1448142
    const v0, 0x7f081464

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_5
    move-object v0, v4

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1448143
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 1448144
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0091

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1448145
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1448146
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1448147
    iget-object v0, p0, LX/9A3;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-nez v0, :cond_8

    .line 1448148
    const/4 v0, 0x0

    .line 1448149
    :goto_0
    move-object v0, v0

    .line 1448150
    iget-object v1, p0, LX/9A3;->h:LX/9A6;

    const/16 v2, 0x200

    .line 1448151
    if-lez v2, :cond_a

    const/4 v3, 0x1

    :goto_1
    const-string v4, "Please enter a valid non-zero high side."

    invoke-static {v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1448152
    iput v2, v1, LX/9A6;->m:I

    .line 1448153
    move-object v1, v1

    .line 1448154
    const/16 v2, 0x140

    .line 1448155
    if-lez v2, :cond_b

    const/4 v3, 0x1

    :goto_2
    const-string v4, "Please enter a valid non-zero mid value."

    invoke-static {v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1448156
    iput v2, v1, LX/9A6;->n:I

    .line 1448157
    move-object v1, v1

    .line 1448158
    iget-object v2, p0, LX/9A3;->C:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1448159
    iput-object v2, v1, LX/9A6;->k:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1448160
    move-object v1, v1

    .line 1448161
    invoke-virtual {v1}, LX/9A6;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1448162
    if-nez v1, :cond_9

    const/4 v1, 0x0

    :goto_3
    move-object v1, v1

    .line 1448163
    iget-object v2, p0, LX/9A3;->A:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1448164
    invoke-static {v2}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 1448165
    const/4 v3, 0x0

    .line 1448166
    :goto_4
    move-object v2, v3

    .line 1448167
    iget-object v3, p0, LX/9A3;->A:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    const/4 v4, 0x0

    .line 1448168
    if-eqz v3, :cond_0

    iget-object v5, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v5, :cond_0

    iget-boolean v5, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    if-nez v5, :cond_d

    .line 1448169
    :cond_0
    :goto_5
    move-object v3, v4

    .line 1448170
    iget-object v4, p0, LX/9A3;->A:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    const/4 v8, 0x0

    const/16 v6, 0xc8

    .line 1448171
    if-eqz v4, :cond_1

    iget-object v5, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v5, :cond_1

    iget-boolean v5, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    if-eqz v5, :cond_f

    .line 1448172
    :cond_1
    :goto_6
    move-object v4, v8

    .line 1448173
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    if-nez v2, :cond_2

    if-nez v3, :cond_2

    if-nez v4, :cond_2

    .line 1448174
    const/4 v0, 0x0

    .line 1448175
    :goto_7
    return-object v0

    .line 1448176
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1448177
    if-eqz v0, :cond_3

    .line 1448178
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448179
    :cond_3
    if-eqz v1, :cond_4

    .line 1448180
    invoke-virtual {v5, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1448181
    :cond_4
    if-eqz v2, :cond_5

    .line 1448182
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448183
    :cond_5
    if-eqz v3, :cond_6

    .line 1448184
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448185
    :cond_6
    if-eqz v4, :cond_7

    .line 1448186
    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448187
    :cond_7
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_7

    :cond_8
    iget-object v0, p0, LX/9A3;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {v0}, LX/6XV;->a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    goto :goto_3

    .line 1448188
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1448189
    :cond_b
    const/4 v3, 0x0

    goto :goto_2

    .line 1448190
    :cond_c
    const-string v3, "https://www.facebook.com/safe_image.php?"

    .line 1448191
    const-string v4, "url"

    .line 1448192
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v5, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v5, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1448193
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1448194
    iput-object v3, v4, LX/2dc;->h:Ljava/lang/String;

    .line 1448195
    move-object v3, v4

    .line 1448196
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1448197
    new-instance v4, LX/4XB;

    invoke-direct {v4}, LX/4XB;-><init>()V

    .line 1448198
    iput-object v3, v4, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448199
    move-object v3, v4

    .line 1448200
    invoke-virtual {v3}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 1448201
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    iget-object v5, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1448202
    iput-object v5, v4, LX/39x;->w:Ljava/lang/String;

    .line 1448203
    move-object v4, v4

    .line 1448204
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1448205
    iput-object v5, v4, LX/39x;->p:LX/0Px;

    .line 1448206
    move-object v4, v4

    .line 1448207
    iput-object v3, v4, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1448208
    move-object v3, v4

    .line 1448209
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    goto/16 :goto_4

    .line 1448210
    :cond_d
    iget-object v5, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1448211
    new-instance v6, LX/4XR;

    invoke-direct {v6}, LX/4XR;-><init>()V

    new-instance v7, LX/3dL;

    invoke-direct {v7}, LX/3dL;-><init>()V

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    if-eqz v8, :cond_e

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 1448212
    :cond_e
    iput-object v4, v7, LX/3dL;->ag:Ljava/lang/String;

    .line 1448213
    move-object v4, v7

    .line 1448214
    invoke-virtual {v4}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1448215
    iput-object v4, v6, LX/4XR;->jb:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1448216
    move-object v4, v6

    .line 1448217
    new-instance v6, LX/23u;

    invoke-direct {v6}, LX/23u;-><init>()V

    invoke-virtual {v6}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 1448218
    iput-object v6, v4, LX/4XR;->cE:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1448219
    move-object v4, v4

    .line 1448220
    invoke-virtual {v4}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 1448221
    invoke-static {v5}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v5

    .line 1448222
    iput-object v4, v5, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1448223
    move-object v4, v5

    .line 1448224
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEME_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1448225
    iput-object v5, v4, LX/39x;->p:LX/0Px;

    .line 1448226
    move-object v4, v4

    .line 1448227
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto/16 :goto_5

    .line 1448228
    :cond_f
    iget-object v5, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    .line 1448229
    if-eqz v7, :cond_13

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_13

    .line 1448230
    invoke-static {v7}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v9

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/2dc;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;

    move-result-object p0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v5

    if-ge v5, v6, :cond_10

    move v5, v6

    .line 1448231
    :goto_8
    iput v5, p0, LX/2dc;->c:I

    .line 1448232
    move-object v5, p0

    .line 1448233
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    if-ge p0, v6, :cond_11

    .line 1448234
    :goto_9
    iput v6, v5, LX/2dc;->i:I

    .line 1448235
    move-object v5, v5

    .line 1448236
    invoke-virtual {v5}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1448237
    iput-object v5, v9, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448238
    move-object v5, v9

    .line 1448239
    invoke-virtual {v5}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 1448240
    :goto_a
    iget-object v6, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v6}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v6

    .line 1448241
    iput-object v5, v6, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1448242
    move-object v6, v6

    .line 1448243
    new-instance v7, LX/173;

    invoke-direct {v7}, LX/173;-><init>()V

    iget-object v5, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-eqz v5, :cond_12

    iget-object v5, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    .line 1448244
    :goto_b
    iput-object v5, v7, LX/173;->f:Ljava/lang/String;

    .line 1448245
    move-object v5, v7

    .line 1448246
    invoke-virtual {v5}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 1448247
    iput-object v5, v6, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1448248
    move-object v5, v6

    .line 1448249
    iget-object v6, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    .line 1448250
    iput-object v6, v5, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1448251
    move-object v5, v5

    .line 1448252
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v6, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 1448253
    iput-object v6, v5, LX/39x;->p:LX/0Px;

    .line 1448254
    move-object v5, v5

    .line 1448255
    invoke-virtual {v5}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v8

    goto/16 :goto_6

    .line 1448256
    :cond_10
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v5

    goto :goto_8

    :cond_11
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v6

    goto :goto_9

    :cond_12
    move-object v5, v8

    .line 1448257
    goto :goto_b

    :cond_13
    move-object v5, v7

    goto :goto_a
.end method

.method private static b(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLActor;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1448258
    const-wide/16 v2, 0x0

    cmp-long v0, p0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1448259
    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 1448260
    :cond_0
    const/4 v0, 0x0

    .line 1448261
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 1448262
    goto :goto_0

    .line 1448263
    :cond_2
    invoke-static {p3, v1, v1}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1448264
    new-instance v1, LX/3dL;

    invoke-direct {v1}, LX/3dL;-><init>()V

    .line 1448265
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1448266
    iput-object v2, v1, LX/3dL;->E:Ljava/lang/String;

    .line 1448267
    iput-object p2, v1, LX/3dL;->ag:Ljava/lang/String;

    .line 1448268
    iput-object v0, v1, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448269
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x285feb

    invoke-direct {v0, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1448270
    iput-object v0, v1, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1448271
    invoke-virtual {v1}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 2

    .prologue
    .line 1448272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/16z;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1448273
    :cond_0
    const/4 v0, 0x0

    .line 1448274
    :goto_0
    return-object v0

    .line 1448275
    :cond_1
    invoke-static {p1}, LX/4Zt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1448276
    sget-object v1, LX/16z;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-static {v0, v1}, LX/16z;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 1448277
    if-nez p1, :cond_0

    .line 1448278
    const/4 v0, 0x0

    .line 1448279
    :goto_0
    return-object v0

    .line 1448280
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v4

    .line 1448281
    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v0, 0x25d6af

    invoke-direct {v5, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1448282
    new-instance v0, LX/4Y6;

    invoke-direct {v0}, LX/4Y6;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    .line 1448283
    iput-object v1, v0, LX/4Y6;->n:Ljava/lang/String;

    .line 1448284
    move-object v0, v0

    .line 1448285
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1448286
    iput-object v1, v0, LX/4Y6;->r:Ljava/lang/String;

    .line 1448287
    move-object v0, v0

    .line 1448288
    iput-object v5, v0, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1448289
    move-object v6, v0

    .line 1448290
    new-instance v7, LX/4X8;

    invoke-direct {v7}, LX/4X8;-><init>()V

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v0

    .line 1448291
    :goto_1
    iput-wide v0, v7, LX/4X8;->b:D

    .line 1448292
    move-object v0, v7

    .line 1448293
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v2

    .line 1448294
    :cond_1
    iput-wide v2, v0, LX/4X8;->c:D

    .line 1448295
    move-object v0, v0

    .line 1448296
    invoke-virtual {v0}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 1448297
    iput-object v0, v6, LX/4Y6;->p:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 1448298
    move-object v0, v6

    .line 1448299
    iget-object v1, p0, LX/9A3;->a:LX/1nG;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1448300
    iput-object v1, v0, LX/4Y6;->P:Ljava/lang/String;

    .line 1448301
    move-object v0, v0

    .line 1448302
    new-instance v1, LX/4Xc;

    invoke-direct {v1}, LX/4Xc;-><init>()V

    invoke-virtual {v1}, LX/4Xc;->a()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v1

    .line 1448303
    iput-object v1, v0, LX/4Y6;->u:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 1448304
    move-object v0, v0

    .line 1448305
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method

.method private static b(Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 2

    .prologue
    .line 1448306
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448307
    new-instance v0, LX/4YL;

    invoke-direct {v0}, LX/4YL;-><init>()V

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->c:Ljava/lang/String;

    .line 1448308
    iput-object v1, v0, LX/4YL;->m:Ljava/lang/String;

    .line 1448309
    move-object v0, v0

    .line 1448310
    iget-object v1, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->d:Ljava/lang/String;

    .line 1448311
    iput-object v1, v0, LX/4YL;->h:Ljava/lang/String;

    .line 1448312
    move-object v0, v0

    .line 1448313
    invoke-virtual {v0}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1448314
    invoke-static {p0}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1448315
    const-string v0, ""

    .line 1448316
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1448317
    invoke-static {p1}, LX/7kz;->f(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9A3;->i:LX/0ad;

    invoke-static {v0}, LX/8Nv;->a(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 21

    .prologue
    .line 1448318
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->r:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448319
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->b:Landroid/content/Context;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448320
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->u:Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1448321
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9A3;->u:Lcom/facebook/user/model/User;

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/9A3;->u:Lcom/facebook/user/model/User;

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/9A3;->b(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1448322
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/9A3;->p:J

    cmp-long v2, v6, v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    move-object v8, v2

    .line 1448323
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->r:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    invoke-static {v2}, LX/9A3;->b(Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v12

    .line 1448324
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->f:LX/0Px;

    invoke-static {v2}, LX/9A3;->a(LX/0Px;)LX/0Px;

    move-result-object v5

    .line 1448325
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->l:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/9A3;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v6

    .line 1448326
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->m:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-static {v2, v6}, LX/9A3;->a(Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;Lcom/facebook/graphql/model/GraphQLPlace;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v13

    .line 1448327
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v14

    .line 1448328
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->h:LX/9A6;

    invoke-virtual {v2}, LX/9A6;->b()LX/0Px;

    move-result-object v7

    .line 1448329
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->q:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, LX/9A3;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;LX/0Px;Lcom/facebook/graphql/model/GraphQLPlace;LX/0Px;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1448330
    :goto_1
    const/4 v9, 0x0

    .line 1448331
    const/4 v3, 0x0

    .line 1448332
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->n:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v10, :cond_5

    .line 1448333
    if-nez v8, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1448334
    :goto_2
    const/4 v3, 0x0

    .line 1448335
    :goto_3
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v9, :cond_a

    new-instance v9, LX/2dc;

    invoke-direct {v9}, LX/2dc;-><init>()V

    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/2dc;->b(Ljava/lang/String;)LX/2dc;

    move-result-object v9

    invoke-virtual {v9}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 1448336
    :goto_4
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v10, :cond_b

    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    sget-object v11, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v10, v11}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v10

    if-nez v10, :cond_b

    new-instance v10, LX/4Z8;

    invoke-direct {v10}, LX/4Z8;-><init>()V

    move-object/from16 v0, p0

    iget-object v11, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v11}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v11

    invoke-virtual {v11}, LX/5RS;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/4Z8;->i(Ljava/lang/String;)LX/4Z8;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v11}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, LX/5MA;->a(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/4Z8;->a(Ljava/lang/String;)LX/4Z8;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v11}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, LX/5MA;->a(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/4Z8;->e(Ljava/lang/String;)LX/4Z8;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v11}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v11

    invoke-virtual {v11}, LX/5RY;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/4Z8;->g(Ljava/lang/String;)LX/4Z8;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/4Z8;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/4Z8;

    move-result-object v9

    invoke-virtual {v9}, LX/4Z8;->a()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v9

    .line 1448337
    :goto_5
    const/4 v10, 0x0

    .line 1448338
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/9A3;->p:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v11, v0, LX/9A3;->u:Lcom/facebook/user/model/User;

    invoke-virtual {v11}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    cmp-long v11, v16, v18

    if-nez v11, :cond_0

    .line 1448339
    sget-object v10, LX/16z;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1448340
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v11, :cond_c

    invoke-static {v7}, LX/7kz;->f(LX/0Px;)Z

    move-result v11

    if-eqz v11, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, LX/9A3;->b(LX/0Px;)Z

    move-result v11

    if-eqz v11, :cond_c

    :cond_1
    const/4 v11, 0x1

    .line 1448341
    :goto_6
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9A3;->c:LX/0SG;

    invoke-interface {v15}, LX/0SG;->a()J

    move-result-wide v16

    .line 1448342
    new-instance v15, LX/23u;

    invoke-direct {v15}, LX/23u;-><init>()V

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v15, v4}, LX/23u;->c(LX/0Px;)LX/23u;

    move-result-object v4

    invoke-virtual {v4, v11}, LX/23u;->b(Z)LX/23u;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v4, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v4, :cond_d

    invoke-static {v7}, LX/7kz;->f(LX/0Px;)Z

    move-result v4

    if-nez v4, :cond_d

    const/4 v4, 0x1

    :goto_7
    invoke-virtual {v11, v4}, LX/23u;->c(Z)LX/23u;

    move-result-object v4

    const-wide/16 v18, 0x3e8

    div-long v18, v16, v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, LX/23u;->a(J)LX/23u;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/23u;

    move-result-object v4

    invoke-virtual {v4, v13}, LX/23u;->b(Lcom/facebook/graphql/model/GraphQLPlace;)LX/23u;

    move-result-object v4

    invoke-virtual {v4, v14}, LX/23u;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/23u;

    move-result-object v4

    invoke-virtual {v4, v12}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)LX/23u;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/23u;->e(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/23u;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/23u;->g(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/23u;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/23u;

    move-result-object v2

    new-instance v3, LX/4ZV;

    invoke-direct {v3}, LX/4ZV;-><init>()V

    invoke-virtual {v3, v5}, LX/4ZV;->a(LX/0Px;)LX/4ZV;

    move-result-object v3

    invoke-virtual {v3}, LX/4ZV;->a()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLWithTagsConnection;)LX/23u;

    move-result-object v2

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, LX/23u;->b(J)LX/23u;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/23u;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/9A3;->n:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v3}, LX/23u;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, LX/9A3;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/23u;->f(LX/0Px;)LX/23u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->q:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->q:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->g()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    :goto_8
    invoke-virtual {v3, v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)LX/23u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->z:Ljava/lang/String;

    :goto_9
    invoke-virtual {v3, v2}, LX/23u;->f(Ljava/lang/String;)LX/23u;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/9A3;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/23u;->b(Ljava/lang/String;)LX/23u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->x:Ljava/lang/String;

    if-eqz v2, :cond_10

    new-instance v2, LX/4Yq;

    invoke-direct {v2}, LX/4Yq;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, LX/9A3;->x:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/4Yq;->b(Ljava/lang/String;)LX/4Yq;

    move-result-object v2

    invoke-virtual {v2}, LX/4Yq;->a()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v2

    :goto_a
    invoke-virtual {v3, v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLSticker;)LX/23u;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/9A3;->v:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/23u;->e(Ljava/lang/String;)LX/23u;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)LX/23u;

    move-result-object v2

    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1448343
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->x:Ljava/lang/String;

    invoke-static {v3, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 1448344
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9A3;->g:LX/0qn;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->w:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->w:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    :goto_b
    invoke-virtual {v4, v3, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1448345
    return-object v3

    .line 1448346
    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/9A3;->p:J

    move-object/from16 v0, p0

    iget-object v5, v0, LX/9A3;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/9A3;->t:Ljava/lang/String;

    invoke-static {v2, v3, v5, v6}, LX/9A3;->a(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    move-object v8, v2

    goto/16 :goto_0

    .line 1448347
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, LX/9A3;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPlace;LX/0Px;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto/16 :goto_1

    .line 1448348
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1448349
    :cond_5
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v10, :cond_6

    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    if-eqz v10, :cond_6

    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_6

    .line 1448350
    const/4 v2, 0x0

    .line 1448351
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 1448352
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    move-object/from16 v0, p0

    iget-object v10, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v10, :cond_8

    :cond_7
    move-object v3, v9

    .line 1448353
    goto/16 :goto_3

    .line 1448354
    :cond_8
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_12

    .line 1448355
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9A3;->i:LX/0ad;

    sget-short v10, LX/1RY;->p:S

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, LX/0ad;->a(SZ)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1448356
    const/4 v3, 0x0

    .line 1448357
    goto/16 :goto_3

    :cond_9
    move-object/from16 v20, v3

    move-object v3, v2

    move-object/from16 v2, v20

    .line 1448358
    goto/16 :goto_3

    .line 1448359
    :cond_a
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 1448360
    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 1448361
    :cond_c
    const/4 v11, 0x0

    goto/16 :goto_6

    .line 1448362
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 1448363
    :cond_11
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto/16 :goto_b

    :cond_12
    move-object v2, v3

    move-object v3, v9

    goto/16 :goto_3
.end method
