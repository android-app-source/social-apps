.class public final LX/A7a;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/edithistory/EditHistoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/ui/edithistory/EditHistoryFragment;)V
    .locals 0

    .prologue
    .line 1626384
    iput-object p1, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1626385
    iget-object v0, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    iget-object v0, v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1626386
    iget-object v0, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    iget-object v0, v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1626387
    iget-object v0, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    iget-object v0, v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchEditHistory"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1626388
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1626389
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1626390
    if-eqz p1, :cond_0

    .line 1626391
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1626392
    if-eqz v0, :cond_0

    .line 1626393
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1626394
    check-cast v0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel;->a()Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel$EditHistoryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1626395
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Got an empty result"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/A7a;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1626396
    :goto_0
    return-void

    .line 1626397
    :cond_1
    iget-object v0, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    iget-object v1, v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->j:LX/A7F;

    .line 1626398
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1626399
    check-cast v0, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel;->a()Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel$EditHistoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$FetchEditHistoryQueryModel$EditHistoryModel;->a()LX/0Px;

    move-result-object v0

    .line 1626400
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, LX/A7F;->a:Ljava/util/List;

    .line 1626401
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;

    .line 1626402
    iget-object v4, v1, LX/A7F;->a:Ljava/util/List;

    new-instance p1, LX/A7V;

    invoke-direct {p1, v2}, LX/A7V;-><init>(Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;)V

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1626403
    :cond_2
    const v2, -0x427e2316

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1626404
    iget-object v0, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    iget-object v0, v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1626405
    iget-object v0, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    iget-object v0, v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1626406
    iget-object v0, p0, LX/A7a;->a:Lcom/facebook/ui/edithistory/EditHistoryFragment;

    const/4 v1, 0x1

    .line 1626407
    iput-boolean v1, v0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->d:Z

    .line 1626408
    goto :goto_0
.end method
