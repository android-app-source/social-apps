.class public LX/9gu;
.super LX/9gr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9gr",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
        ">;",
        "Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;",
        "LX/5kD;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/9h9;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1525776
    const-class v0, LX/5kD;

    invoke-direct {p0, p1, v0, p2}, LX/9gr;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1525777
    iput-object p3, p0, LX/9gu;->b:LX/9h9;

    .line 1525778
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0gW;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1525779
    new-instance v0, LX/9g9;

    invoke-direct {v0}, LX/9g9;-><init>()V

    move-object v1, v0

    .line 1525780
    const-string v2, "ids"

    .line 1525781
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525782
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;->a:LX/0Px;

    invoke-virtual {v1, v2, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1525783
    iget-object v0, p0, LX/9gu;->b:LX/9h9;

    invoke-virtual {v0, v1}, LX/9h9;->a(LX/0gW;)LX/0gW;

    .line 1525784
    return-object v1
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
            ">;>;)",
            "LX/9fz",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525785
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1525786
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1525787
    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1525788
    if-eqz v0, :cond_0

    instance-of v3, v0, LX/5kD;

    if-eqz v3, :cond_0

    .line 1525789
    check-cast v0, LX/5kD;

    .line 1525790
    invoke-interface {v0}, LX/5kD;->e()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1525791
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1525792
    :cond_1
    new-instance v0, LX/9gt;

    invoke-direct {v0, p0}, LX/9gt;-><init>(LX/9gu;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1525793
    new-instance v0, LX/9fz;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    new-instance v2, LX/9gW;

    invoke-direct {v2}, LX/9gW;-><init>()V

    const/4 v3, 0x0

    .line 1525794
    iput-boolean v3, v2, LX/9gW;->c:Z

    .line 1525795
    move-object v2, v2

    .line 1525796
    invoke-virtual {v2}, LX/9gW;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/9fz;-><init>(LX/0Px;Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;)V

    return-object v0
.end method
