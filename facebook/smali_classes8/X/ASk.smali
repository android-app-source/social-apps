.class public final LX/ASk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;)V
    .locals 0

    .prologue
    .line 1674417
    iput-object p1, p0, LX/ASk;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x63ee3e2c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1674418
    iget-object v0, p0, LX/ASk;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    .line 1674419
    iget-object v2, v0, LX/ASh;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-interface {v2}, LX/1aZ;->f()Landroid/graphics/drawable/Animatable;

    move-result-object v2

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v0, v2

    .line 1674420
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1674421
    const v0, 0x3a751ef0

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1674422
    :goto_0
    return-void

    .line 1674423
    :cond_0
    sget-object v2, LX/ASl;->a:[I

    iget-object v3, p0, LX/ASk;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    iget-object v3, v3, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->l:LX/ASm;

    invoke-virtual {v3}, LX/ASm;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1674424
    :goto_1
    const v0, 0x7350af6a

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1674425
    :pswitch_0
    iget-object v2, p0, LX/ASk;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    sget-object v3, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    invoke-virtual {v2, v3}, LX/ASh;->setPlayButtonState(LX/6Wv;)V

    .line 1674426
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    .line 1674427
    iget-object v0, p0, LX/ASk;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    sget-object v2, LX/ASm;->RESUME_ON_CLICK:LX/ASm;

    .line 1674428
    iput-object v2, v0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->l:LX/ASm;

    .line 1674429
    goto :goto_1

    .line 1674430
    :pswitch_1
    iget-object v2, p0, LX/ASk;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    sget-object v3, LX/6Wv;->HIDDEN:LX/6Wv;

    invoke-virtual {v2, v3}, LX/ASh;->setPlayButtonState(LX/6Wv;)V

    .line 1674431
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1674432
    iget-object v0, p0, LX/ASk;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    sget-object v2, LX/ASm;->PAUSE_ON_CLICK:LX/ASm;

    .line 1674433
    iput-object v2, v0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->l:LX/ASm;

    .line 1674434
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
