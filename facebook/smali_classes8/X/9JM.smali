.class public final LX/9JM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Ljava/nio/channels/FileChannel;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/nio/channels/FileChannel;)V
    .locals 0

    .prologue
    .line 1464622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464623
    iput-object p1, p0, LX/9JM;->a:Ljava/io/File;

    .line 1464624
    iput-object p2, p0, LX/9JM;->b:Ljava/nio/channels/FileChannel;

    .line 1464625
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)I
    .locals 1

    .prologue
    .line 1464626
    iget-object v0, p0, LX/9JM;->b:Ljava/nio/channels/FileChannel;

    invoke-virtual {v0, p1}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1464627
    iget-object v0, p0, LX/9JM;->b:Ljava/nio/channels/FileChannel;

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 1464628
    return-void
.end method
