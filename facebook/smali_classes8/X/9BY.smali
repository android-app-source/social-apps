.class public abstract LX/9BY;
.super Landroid/view/View;
.source ""


# instance fields
.field public A:F

.field public B:F

.field public final C:Landroid/graphics/drawable/Drawable;

.field public D:I

.field public E:Landroid/graphics/drawable/Drawable;

.field public F:Landroid/graphics/drawable/Drawable;

.field public G:LX/20K;

.field public H:LX/210;

.field public I:Z

.field public J:I

.field public a:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Landroid/graphics/drawable/Drawable;

.field public final e:Landroid/graphics/Paint;

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:Ljava/lang/String;

.field public final q:Landroid/graphics/Rect;

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1451794
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1451795
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9BY;->q:Landroid/graphics/Rect;

    .line 1451796
    const/4 v0, -0x1

    iput v0, p0, LX/9BY;->D:I

    .line 1451797
    sget-object v0, LX/210;->ABOVE_FOOTER:LX/210;

    iput-object v0, p0, LX/9BY;->H:LX/210;

    .line 1451798
    const-class v0, LX/9BY;

    invoke-static {v0, p0}, LX/9BY;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1451799
    invoke-virtual {p0}, LX/9BY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1451800
    const v1, 0x7f0215e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/9BY;->C:Landroid/graphics/drawable/Drawable;

    .line 1451801
    iget-object v1, p0, LX/9BY;->C:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/9BY;->q:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1451802
    iget-object v1, p0, LX/9BY;->C:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    .line 1451803
    const v1, 0x7f0215e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/9BY;->d:Landroid/graphics/drawable/Drawable;

    .line 1451804
    const v1, 0x7f0b0fce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->g:I

    .line 1451805
    const v1, 0x7f0b0fcf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->h:I

    .line 1451806
    const v1, 0x7f0b0fd0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->i:I

    .line 1451807
    const v1, 0x7f0b0fcd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->j:I

    .line 1451808
    const v1, 0x7f0b0fcc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->f:I

    .line 1451809
    const v1, 0x7f0b00a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->k:I

    .line 1451810
    const v1, 0x7f0b0fd1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->l:I

    .line 1451811
    const v1, 0x7f0b0fd2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->m:I

    .line 1451812
    const v1, 0x7f0b0fd3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->n:I

    .line 1451813
    const v1, 0x7f0b0fd4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9BY;->o:I

    .line 1451814
    const v1, 0x7f081999

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9BY;->p:Ljava/lang/String;

    .line 1451815
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 1451816
    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1451817
    iget v1, p0, LX/9BY;->l:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1451818
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1451819
    invoke-virtual {p0}, LX/9BY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1451820
    move-object v0, v0

    .line 1451821
    iput-object v0, p0, LX/9BY;->e:Landroid/graphics/Paint;

    .line 1451822
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/9BY;

    invoke-static {p0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v1

    check-cast v1, LX/0hL;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v2

    check-cast v2, LX/0wM;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object v1, p1, LX/9BY;->a:LX/0hL;

    iput-object v2, p1, LX/9BY;->b:LX/0wM;

    iput-object p0, p1, LX/9BY;->c:LX/0hB;

    return-void
.end method


# virtual methods
.method public abstract a(I)Landroid/graphics/Rect;
.end method

.method public abstract a(FF)V
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1451823
    iget-object v0, p0, LX/9BY;->H:LX/210;

    sget-object v1, LX/210;->ABOVE_FOOTER:LX/210;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b(Landroid/view/MotionEvent;)Z
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()Z
.end method

.method public abstract getCurrentReaction()LX/1zt;
.end method

.method public abstract getFaceConfigs()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/9BW;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPointerPosition()LX/211;
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x46dc2d06

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1451791
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1451792
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/9BY;->I:Z

    .line 1451793
    const/16 v1, 0x2d

    const v2, 0x5030aae6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1db9952a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1451788
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1451789
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/9BY;->I:Z

    .line 1451790
    const/16 v1, 0x2d

    const v2, -0x527b4a85

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onWindowVisibilityChanged(I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x500c09b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1451784
    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    .line 1451785
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/9BY;->G:LX/20K;

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 1451786
    iget-object v1, p0, LX/9BY;->G:LX/20K;

    invoke-virtual {v1}, LX/20K;->c()V

    .line 1451787
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x28f6134c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public abstract setupReactionsImpl(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1zt;",
            ">;)V"
        }
    .end annotation
.end method
