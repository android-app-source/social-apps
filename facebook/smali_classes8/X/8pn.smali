.class public LX/8pn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pe;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Sh;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/1K9;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/6BK;

.field private g:I


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/1K9;LX/0Ot;LX/0Ot;LX/6BK;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/1K9;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/6BK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1406427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406428
    const/4 v0, 0x0

    iput v0, p0, LX/8pn;->g:I

    .line 1406429
    iput-object p1, p0, LX/8pn;->a:LX/0Sh;

    .line 1406430
    iput-object p2, p0, LX/8pn;->b:Ljava/util/concurrent/ExecutorService;

    .line 1406431
    iput-object p3, p0, LX/8pn;->c:LX/1K9;

    .line 1406432
    iput-object p4, p0, LX/8pn;->d:LX/0Ot;

    .line 1406433
    iput-object p5, p0, LX/8pn;->e:LX/0Ot;

    .line 1406434
    iput-object p6, p0, LX/8pn;->f:LX/6BK;

    .line 1406435
    return-void
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1406357
    if-eqz p2, :cond_2

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 1406358
    :goto_0
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;

    const/16 v2, 0x19

    const/4 v3, 0x0

    move-object v1, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0rS;)V

    .line 1406359
    iget-object v1, p0, LX/8pn;->f:LX/6BK;

    .line 1406360
    new-instance v2, LX/5BX;

    invoke-direct {v2}, LX/5BX;-><init>()V

    move-object v2, v2

    .line 1406361
    const-string v3, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "likers_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1406362
    if-eqz v0, :cond_1

    .line 1406363
    const-string v3, "feedback_id"

    .line 1406364
    iget-object v4, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1406365
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "max_likers"

    .line 1406366
    iget v5, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->b:I

    move v5, v5

    .line 1406367
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1406368
    iget-object v3, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1406369
    if-eqz v3, :cond_0

    .line 1406370
    const-string v3, "before_likers"

    .line 1406371
    iget-object v4, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1406372
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1406373
    :cond_0
    iget-object v3, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1406374
    if-eqz v3, :cond_1

    .line 1406375
    const-string v3, "after_likers"

    .line 1406376
    iget-object v4, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1406377
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1406378
    :cond_1
    iget-object v3, v1, LX/6BK;->c:LX/0se;

    invoke-virtual {v3, v2}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 1406379
    move-object v2, v2

    .line 1406380
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 1406381
    iput-object p4, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1406382
    move-object v2, v2

    .line 1406383
    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v3}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    .line 1406384
    iget-object v3, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->e:LX/0rS;

    move-object v3, v3

    .line 1406385
    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v3, v4, :cond_3

    .line 1406386
    sget-object v3, LX/0zS;->d:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 1406387
    :goto_1
    iget-object v3, v1, LX/6BK;->a:LX/0sZ;

    invoke-virtual {v3, v0, v2}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchNodeListParams;LX/0zO;)LX/1kt;

    move-result-object v3

    .line 1406388
    iput-object v3, v2, LX/0zO;->g:LX/1kt;

    .line 1406389
    move-object v0, v2

    .line 1406390
    return-object v0

    .line 1406391
    :cond_2
    sget-object v5, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    goto/16 :goto_0

    .line 1406392
    :cond_3
    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/8pn;
    .locals 13

    .prologue
    .line 1406413
    const-class v1, LX/8pn;

    monitor-enter v1

    .line 1406414
    :try_start_0
    sget-object v0, LX/8pn;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1406415
    sput-object v2, LX/8pn;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1406416
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406417
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1406418
    new-instance v3, LX/8pn;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v6

    check-cast v6, LX/1K9;

    const/16 v7, 0xafd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xafc

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 1406419
    new-instance v12, LX/6BK;

    invoke-static {v0}, LX/0sZ;->b(LX/0QB;)LX/0sZ;

    move-result-object v9

    check-cast v9, LX/0sZ;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v10

    check-cast v10, LX/0sa;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v11

    check-cast v11, LX/0se;

    invoke-direct {v12, v9, v10, v11}, LX/6BK;-><init>(LX/0sZ;LX/0sa;LX/0se;)V

    .line 1406420
    move-object v9, v12

    .line 1406421
    check-cast v9, LX/6BK;

    invoke-direct/range {v3 .. v9}, LX/8pn;-><init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/1K9;LX/0Ot;LX/0Ot;LX/6BK;)V

    .line 1406422
    move-object v0, v3

    .line 1406423
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1406424
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8pn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1406425
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1406426
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1406412
    new-instance v0, LX/8pm;

    invoke-direct {v0, p0}, LX/8pm;-><init>(LX/8pn;)V

    iget-object v1, p0, LX/8pn;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/89l;
    .locals 1

    .prologue
    .line 1406411
    sget-object v0, LX/89l;->LIKERS_FOR_FEEDBACK_ID:LX/89l;

    return-object v0
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;LX/0TF;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406401
    iget-object v0, p1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1406402
    invoke-direct {p0, v0, p4, p5, p6}, LX/8pn;->a(Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1406403
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1406404
    move-object v1, v0

    .line 1406405
    iget-object v0, p0, LX/8pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    new-instance v2, LX/8pl;

    invoke-direct {v2, p0, p3}, LX/8pl;-><init>(LX/8pn;LX/0TF;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1406406
    iget-object v4, v1, LX/0zO;->m:LX/0gW;

    move-object v4, v4

    .line 1406407
    iget-object v5, v4, LX/0gW;->f:Ljava/lang/String;

    move-object v4, v5

    .line 1406408
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, LX/8pn;->g:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/8pn;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1406409
    iget-object v1, p0, LX/8pn;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/8pn;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1406410
    return-void
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406396
    iget-object v0, p1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1406397
    invoke-direct {p0, v0, p3, p4, p5}, LX/8pn;->a(Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    .line 1406398
    iget-object v0, p0, LX/8pn;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1406399
    iget-object v1, p0, LX/8pn;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/8pn;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1406400
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1406395
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1406393
    iget-object v0, p0, LX/8pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1406394
    return-void
.end method
