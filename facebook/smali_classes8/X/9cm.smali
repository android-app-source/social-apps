.class public LX/9cm;
.super Lcom/facebook/stickers/ui/StickerGridViewAdapter;
.source ""


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:LX/8jY;

.field public c:Lcom/facebook/stickers/model/StickerPack;


# direct methods
.method public constructor <init>(LX/8jY;LX/8mv;LX/1Ad;LX/0Or;Landroid/content/Context;Ljava/lang/String;LX/7kc;)V
    .locals 7
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/abtest/IsAnimatedStickersInGridEnabled;
        .end annotation
    .end param
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/7kc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8jY;",
            "LX/8mv;",
            "LX/1Ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "LX/7kc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516777
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;-><init>(LX/8mv;LX/1Ad;LX/0Or;Landroid/content/Context;Ljava/lang/String;LX/7kc;)V

    .line 1516778
    iput-object p1, p0, LX/9cm;->b:LX/8jY;

    .line 1516779
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p5}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/9cm;->a:Landroid/os/Handler;

    .line 1516780
    return-void
.end method
