.class public final enum LX/8wK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8wK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8wK;

.field public static final enum ACCOUNT:LX/8wK;

.field public static final enum ACCOUNT_ERROR_CARD:LX/8wK;

.field public static final enum ADDRESS:LX/8wK;

.field public static final enum AD_CREATIVE:LX/8wK;

.field public static final enum AD_PREVIEW:LX/8wK;

.field public static final enum AYMT_CHANNEL:LX/8wK;

.field public static final enum BOOST_SLIDESHOW_INFO:LX/8wK;

.field public static final enum BOOST_TYPE:LX/8wK;

.field public static final enum BUDGET:LX/8wK;

.field public static final enum CALL_TO_ACTION:LX/8wK;

.field public static final enum CONVERSION_PIXEL:LX/8wK;

.field public static final enum DURATION:LX/8wK;

.field public static final enum DURATION_BUDGET:LX/8wK;

.field public static final enum ERROR_CARD:LX/8wK;

.field public static final enum FOOTER:LX/8wK;

.field public static final enum INFO_CARD:LX/8wK;

.field public static final enum INSIGHTS:LX/8wK;

.field public static final enum INSIGHTS_CLICKS:LX/8wK;

.field public static final enum INSIGHTS_ENGAGEMENT:LX/8wK;

.field public static final enum INSIGHTS_MESSAGES:LX/8wK;

.field public static final enum INSIGHTS_REACH:LX/8wK;

.field public static final enum INSIGHTS_SUMMARY:LX/8wK;

.field public static final enum OFFERS:LX/8wK;

.field public static final enum OVERVIEW:LX/8wK;

.field public static final enum PACING:LX/8wK;

.field public static final enum PHONE_NUMBER:LX/8wK;

.field public static final enum PROMOTION_DETAILS:LX/8wK;

.field public static final enum RESULTS:LX/8wK;

.field public static final enum SPACER:LX/8wK;

.field public static final enum TARGETING:LX/8wK;

.field public static final enum TARGETING_DESCRIPTION:LX/8wK;

.field public static final enum WEBSITE_URL:LX/8wK;


# instance fields
.field public final mServerType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1422626
    new-instance v0, LX/8wK;

    const-string v1, "ACCOUNT"

    const-string v2, "account"

    invoke-direct {v0, v1, v4, v2}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->ACCOUNT:LX/8wK;

    .line 1422627
    new-instance v0, LX/8wK;

    const-string v1, "ACCOUNT_ERROR_CARD"

    invoke-direct {v0, v1, v5}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->ACCOUNT_ERROR_CARD:LX/8wK;

    .line 1422628
    new-instance v0, LX/8wK;

    const-string v1, "AYMT_CHANNEL"

    invoke-direct {v0, v1, v6}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->AYMT_CHANNEL:LX/8wK;

    .line 1422629
    new-instance v0, LX/8wK;

    const-string v1, "AD_PREVIEW"

    const-string v2, "ad_preview"

    invoke-direct {v0, v1, v7, v2}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->AD_PREVIEW:LX/8wK;

    .line 1422630
    new-instance v0, LX/8wK;

    const-string v1, "AD_CREATIVE"

    invoke-direct {v0, v1, v8}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->AD_CREATIVE:LX/8wK;

    .line 1422631
    new-instance v0, LX/8wK;

    const-string v1, "PROMOTION_DETAILS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    .line 1422632
    new-instance v0, LX/8wK;

    const-string v1, "BOOST_TYPE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->BOOST_TYPE:LX/8wK;

    .line 1422633
    new-instance v0, LX/8wK;

    const-string v1, "BUDGET"

    const/4 v2, 0x7

    const-string v3, "budget"

    invoke-direct {v0, v1, v2, v3}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->BUDGET:LX/8wK;

    .line 1422634
    new-instance v0, LX/8wK;

    const-string v1, "DURATION"

    const/16 v2, 0x8

    const-string v3, "duration"

    invoke-direct {v0, v1, v2, v3}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->DURATION:LX/8wK;

    .line 1422635
    new-instance v0, LX/8wK;

    const-string v1, "DURATION_BUDGET"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->DURATION_BUDGET:LX/8wK;

    .line 1422636
    new-instance v0, LX/8wK;

    const-string v1, "FOOTER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->FOOTER:LX/8wK;

    .line 1422637
    new-instance v0, LX/8wK;

    const-string v1, "INFO_CARD"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->INFO_CARD:LX/8wK;

    .line 1422638
    new-instance v0, LX/8wK;

    const-string v1, "OVERVIEW"

    const/16 v2, 0xc

    const-string v3, "promotion_overview"

    invoke-direct {v0, v1, v2, v3}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->OVERVIEW:LX/8wK;

    .line 1422639
    new-instance v0, LX/8wK;

    const-string v1, "CONVERSION_PIXEL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->CONVERSION_PIXEL:LX/8wK;

    .line 1422640
    new-instance v0, LX/8wK;

    const-string v1, "ERROR_CARD"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->ERROR_CARD:LX/8wK;

    .line 1422641
    new-instance v0, LX/8wK;

    const-string v1, "INSIGHTS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->INSIGHTS:LX/8wK;

    .line 1422642
    new-instance v0, LX/8wK;

    const-string v1, "INSIGHTS_CLICKS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->INSIGHTS_CLICKS:LX/8wK;

    .line 1422643
    new-instance v0, LX/8wK;

    const-string v1, "INSIGHTS_ENGAGEMENT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->INSIGHTS_ENGAGEMENT:LX/8wK;

    .line 1422644
    new-instance v0, LX/8wK;

    const-string v1, "INSIGHTS_MESSAGES"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->INSIGHTS_MESSAGES:LX/8wK;

    .line 1422645
    new-instance v0, LX/8wK;

    const-string v1, "INSIGHTS_REACH"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->INSIGHTS_REACH:LX/8wK;

    .line 1422646
    new-instance v0, LX/8wK;

    const-string v1, "INSIGHTS_SUMMARY"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->INSIGHTS_SUMMARY:LX/8wK;

    .line 1422647
    new-instance v0, LX/8wK;

    const-string v1, "TARGETING"

    const/16 v2, 0x15

    const-string v3, "audience"

    invoke-direct {v0, v1, v2, v3}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->TARGETING:LX/8wK;

    .line 1422648
    new-instance v0, LX/8wK;

    const-string v1, "TARGETING_DESCRIPTION"

    const/16 v2, 0x16

    const-string v3, "audience_details"

    invoke-direct {v0, v1, v2, v3}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->TARGETING_DESCRIPTION:LX/8wK;

    .line 1422649
    new-instance v0, LX/8wK;

    const-string v1, "CALL_TO_ACTION"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->CALL_TO_ACTION:LX/8wK;

    .line 1422650
    new-instance v0, LX/8wK;

    const-string v1, "WEBSITE_URL"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->WEBSITE_URL:LX/8wK;

    .line 1422651
    new-instance v0, LX/8wK;

    const-string v1, "PHONE_NUMBER"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->PHONE_NUMBER:LX/8wK;

    .line 1422652
    new-instance v0, LX/8wK;

    const-string v1, "ADDRESS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->ADDRESS:LX/8wK;

    .line 1422653
    new-instance v0, LX/8wK;

    const-string v1, "RESULTS"

    const/16 v2, 0x1b

    const-string v3, "promotion_results"

    invoke-direct {v0, v1, v2, v3}, LX/8wK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wK;->RESULTS:LX/8wK;

    .line 1422654
    new-instance v0, LX/8wK;

    const-string v1, "SPACER"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->SPACER:LX/8wK;

    .line 1422655
    new-instance v0, LX/8wK;

    const-string v1, "PACING"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->PACING:LX/8wK;

    .line 1422656
    new-instance v0, LX/8wK;

    const-string v1, "OFFERS"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->OFFERS:LX/8wK;

    .line 1422657
    new-instance v0, LX/8wK;

    const-string v1, "BOOST_SLIDESHOW_INFO"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, LX/8wK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8wK;->BOOST_SLIDESHOW_INFO:LX/8wK;

    .line 1422658
    const/16 v0, 0x20

    new-array v0, v0, [LX/8wK;

    sget-object v1, LX/8wK;->ACCOUNT:LX/8wK;

    aput-object v1, v0, v4

    sget-object v1, LX/8wK;->ACCOUNT_ERROR_CARD:LX/8wK;

    aput-object v1, v0, v5

    sget-object v1, LX/8wK;->AYMT_CHANNEL:LX/8wK;

    aput-object v1, v0, v6

    sget-object v1, LX/8wK;->AD_PREVIEW:LX/8wK;

    aput-object v1, v0, v7

    sget-object v1, LX/8wK;->AD_CREATIVE:LX/8wK;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8wK;->BOOST_TYPE:LX/8wK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8wK;->BUDGET:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8wK;->DURATION:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8wK;->DURATION_BUDGET:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8wK;->FOOTER:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8wK;->INFO_CARD:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8wK;->OVERVIEW:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8wK;->CONVERSION_PIXEL:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8wK;->ERROR_CARD:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8wK;->INSIGHTS:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8wK;->INSIGHTS_CLICKS:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8wK;->INSIGHTS_ENGAGEMENT:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/8wK;->INSIGHTS_MESSAGES:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/8wK;->INSIGHTS_REACH:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/8wK;->INSIGHTS_SUMMARY:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/8wK;->TARGETING:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/8wK;->TARGETING_DESCRIPTION:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/8wK;->CALL_TO_ACTION:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/8wK;->WEBSITE_URL:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/8wK;->PHONE_NUMBER:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/8wK;->ADDRESS:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/8wK;->RESULTS:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/8wK;->SPACER:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/8wK;->PACING:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/8wK;->OFFERS:LX/8wK;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/8wK;->BOOST_SLIDESHOW_INFO:LX/8wK;

    aput-object v2, v0, v1

    sput-object v0, LX/8wK;->$VALUES:[LX/8wK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1422659
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1422660
    const/4 v0, 0x0

    iput-object v0, p0, LX/8wK;->mServerType:Ljava/lang/String;

    .line 1422661
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1422662
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1422663
    iput-object p3, p0, LX/8wK;->mServerType:Ljava/lang/String;

    .line 1422664
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8wK;
    .locals 1

    .prologue
    .line 1422665
    const-class v0, LX/8wK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8wK;

    return-object v0
.end method

.method public static values()[LX/8wK;
    .locals 1

    .prologue
    .line 1422666
    sget-object v0, LX/8wK;->$VALUES:[LX/8wK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8wK;

    return-object v0
.end method
