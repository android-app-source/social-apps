.class public LX/926;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1431979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1431980
    iput-object p1, p0, LX/926;->a:LX/0ad;

    .line 1431981
    return-void
.end method

.method public static a(LX/0QB;)LX/926;
    .locals 1

    .prologue
    .line 1431982
    invoke-static {p0}, LX/926;->b(LX/0QB;)LX/926;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/926;
    .locals 2

    .prologue
    .line 1431977
    new-instance v1, LX/926;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/926;-><init>(LX/0ad;)V

    .line 1431978
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1431971
    iget-object v0, p0, LX/926;->a:LX/0ad;

    sget-short v1, LX/8zO;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1431972
    if-eqz v0, :cond_0

    .line 1431973
    invoke-static {p2, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->a(Landroid/content/Context;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 1431974
    :goto_0
    const-string v1, "minutiae_configuration"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1431975
    return-object v0

    .line 1431976
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method
