.class public final LX/9ag;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V
    .locals 0

    .prologue
    .line 1513956
    iput-object p1, p0, LX/9ag;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x25706901

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1513946
    iget-object v1, p0, LX/9ag;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v1, v1, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->q:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-boolean v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v1, :cond_0

    .line 1513947
    iget-object v1, p0, LX/9ag;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    .line 1513948
    iget-object v3, v1, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->g:LX/9af;

    .line 1513949
    sget-object v4, LX/9ae;->ALBUM_PRIVACY_SELECTOR_OPENED:LX/9ae;

    invoke-static {v4}, LX/9af;->a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v3, v4}, LX/9af;->a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1513950
    iget-object v3, v1, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8RJ;

    sget-object v4, LX/8RI;->ALBUM_CREATOR_FRAGMENT:LX/8RI;

    invoke-virtual {v3, v4}, LX/8RJ;->a(LX/8RI;)V

    .line 1513951
    new-instance v3, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;

    invoke-direct {v3}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;-><init>()V

    .line 1513952
    new-instance v4, LX/93W;

    iget-object v5, v1, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->m:LX/8Q5;

    iget-object p0, v1, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->n:LX/93X;

    const/4 p1, 0x0

    invoke-direct {v4, v5, p0, p1}, LX/93W;-><init>(LX/8Q5;LX/93X;LX/8Rm;)V

    invoke-virtual {v3, v4}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->a(LX/93W;)V

    .line 1513953
    iget-object v4, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v4, v4

    .line 1513954
    const-string v5, "AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1513955
    :cond_0
    const v1, -0x44689577

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
