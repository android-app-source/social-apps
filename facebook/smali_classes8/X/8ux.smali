.class public final enum LX/8ux;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8ux;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8ux;

.field public static final enum BLUE:LX/8ux;

.field public static final enum RED:LX/8ux;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1415832
    new-instance v0, LX/8ux;

    const-string v1, "BLUE"

    invoke-direct {v0, v1, v2}, LX/8ux;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ux;->BLUE:LX/8ux;

    .line 1415833
    new-instance v0, LX/8ux;

    const-string v1, "RED"

    invoke-direct {v0, v1, v3}, LX/8ux;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ux;->RED:LX/8ux;

    .line 1415834
    const/4 v0, 0x2

    new-array v0, v0, [LX/8ux;

    sget-object v1, LX/8ux;->BLUE:LX/8ux;

    aput-object v1, v0, v2

    sget-object v1, LX/8ux;->RED:LX/8ux;

    aput-object v1, v0, v3

    sput-object v0, LX/8ux;->$VALUES:[LX/8ux;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1415835
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8ux;
    .locals 1

    .prologue
    .line 1415836
    const-class v0, LX/8ux;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8ux;

    return-object v0
.end method

.method public static values()[LX/8ux;
    .locals 1

    .prologue
    .line 1415837
    sget-object v0, LX/8ux;->$VALUES:[LX/8ux;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8ux;

    return-object v0
.end method
