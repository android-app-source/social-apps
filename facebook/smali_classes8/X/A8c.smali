.class public final LX/A8c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:LX/2Wj;


# direct methods
.method public constructor <init>(LX/2Wj;)V
    .locals 0

    .prologue
    .line 1627879
    iput-object p1, p0, LX/A8c;->a:LX/2Wj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 11

    .prologue
    .line 1627880
    iget-object v0, p0, LX/A8c;->a:LX/2Wj;

    const/4 v1, 0x1

    .line 1627881
    iget v2, v0, LX/2Wj;->u:I

    if-eq v2, v1, :cond_1

    .line 1627882
    :cond_0
    :goto_0
    move v0, v1

    .line 1627883
    return v0

    .line 1627884
    :cond_1
    invoke-static {v0}, LX/2Wj;->i(LX/2Wj;)V

    .line 1627885
    invoke-static {v0}, LX/2Wj;->h(LX/2Wj;)V

    .line 1627886
    const/4 v5, 0x0

    .line 1627887
    iget-object v4, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getParagraphAlignment(I)Landroid/text/Layout$Alignment;

    move-result-object v4

    .line 1627888
    iget-object v6, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v6, v5}, Landroid/text/Layout;->getParagraphDirection(I)I

    move-result v6

    .line 1627889
    invoke-virtual {v0}, LX/2Wj;->getWidth()I

    move-result v7

    invoke-virtual {v0}, LX/2Wj;->getPaddingLeft()I

    move-result v8

    invoke-virtual {v0}, LX/2Wj;->getPaddingRight()I

    move-result v9

    add-int/2addr v8, v9

    sub-int/2addr v7, v8

    .line 1627890
    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    if-ne v4, v8, :cond_6

    .line 1627891
    iget-object v4, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v4

    float-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v4, v8

    .line 1627892
    iget-object v8, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v8, v5}, Landroid/text/Layout;->getLineRight(I)F

    move-result v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    .line 1627893
    sub-int v9, v8, v4

    if-ge v9, v7, :cond_3

    .line 1627894
    add-int/2addr v4, v8

    div-int/lit8 v4, v4, 0x2

    div-int/lit8 v6, v7, 0x2

    sub-int/2addr v4, v6

    .line 1627895
    :cond_2
    :goto_1
    invoke-virtual {v0}, LX/2Wj;->getScrollX()I

    move-result v6

    if-eq v4, v6, :cond_9

    .line 1627896
    invoke-virtual {v0}, LX/2Wj;->getScrollY()I

    move-result v5

    invoke-virtual {v0, v4, v5}, LX/2Wj;->scrollTo(II)V

    .line 1627897
    const/4 v4, 0x1

    .line 1627898
    :goto_2
    move v2, v4

    .line 1627899
    const/4 v3, 0x2

    iput v3, v0, LX/2Wj;->u:I

    .line 1627900
    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 1627901
    :cond_3
    iget-object v9, v0, LX/2Wj;->j:LX/A8d;

    sget-object v10, LX/A8d;->LEADING:LX/A8d;

    if-ne v9, v10, :cond_5

    .line 1627902
    if-gez v6, :cond_2

    .line 1627903
    :cond_4
    sub-int v4, v8, v7

    .line 1627904
    goto :goto_1

    .line 1627905
    :cond_5
    if-gez v6, :cond_4

    goto :goto_1

    .line 1627906
    :cond_6
    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    if-ne v4, v8, :cond_7

    .line 1627907
    if-ltz v6, :cond_8

    .line 1627908
    iget-object v4, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v4

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v4, v6

    goto :goto_1

    .line 1627909
    :cond_7
    if-gez v6, :cond_8

    .line 1627910
    iget-object v4, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v4

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v4, v6

    goto :goto_1

    .line 1627911
    :cond_8
    iget-object v4, v0, LX/2Wj;->q:Landroid/text/Layout;

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineRight(I)F

    move-result v4

    float-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v4, v8

    .line 1627912
    sub-int/2addr v4, v7

    goto :goto_1

    :cond_9
    move v4, v5

    .line 1627913
    goto :goto_2
.end method
