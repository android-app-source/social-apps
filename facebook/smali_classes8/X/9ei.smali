.class public LX/9ei;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9eb;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/view/View$OnClickListener;

.field private final d:LX/9eh;

.field private final e:Landroid/content/Context;

.field private final f:Landroid/widget/FrameLayout;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

.field private final i:LX/8GT;

.field public j:LX/9dx;

.field private k:Landroid/view/View;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Lcom/facebook/resources/ui/FbTextView;

.field private n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

.field public o:Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

.field public p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

.field private q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1520283
    const-class v0, LX/9ei;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9ei;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/0am;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/content/Context;LX/8GT;)V
    .locals 4
    .param p1    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/editgallery/EditableOverlayContainerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/photos/editgallery/EditGalleryFragmentController;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/FrameLayout;",
            "Landroid/view/View;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;",
            "Lcom/facebook/photos/editgallery/RotatingPhotoViewController;",
            "Landroid/content/Context;",
            "LX/8GT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 1520261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1520262
    new-instance v0, LX/9ef;

    invoke-direct {v0, p0}, LX/9ef;-><init>(LX/9ei;)V

    iput-object v0, p0, LX/9ei;->b:Landroid/view/View$OnClickListener;

    .line 1520263
    new-instance v0, LX/9eg;

    invoke-direct {v0, p0}, LX/9eg;-><init>(LX/9ei;)V

    iput-object v0, p0, LX/9ei;->c:Landroid/view/View$OnClickListener;

    .line 1520264
    new-instance v0, LX/9eh;

    invoke-direct {v0, p0}, LX/9eh;-><init>(LX/9ei;)V

    iput-object v0, p0, LX/9ei;->d:LX/9eh;

    .line 1520265
    new-instance v0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;-><init>()V

    iput-object v0, p0, LX/9ei;->o:Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

    .line 1520266
    iput-object p1, p0, LX/9ei;->f:Landroid/widget/FrameLayout;

    .line 1520267
    iput-object p7, p0, LX/9ei;->e:Landroid/content/Context;

    .line 1520268
    iput-object p4, p0, LX/9ei;->g:Ljava/lang/String;

    .line 1520269
    iput-object p6, p0, LX/9ei;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520270
    iput-object p8, p0, LX/9ei;->i:LX/8GT;

    .line 1520271
    new-instance v0, LX/9dx;

    iget-object v1, p0, LX/9ei;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/9dx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9ei;->j:LX/9dx;

    .line 1520272
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    iget-object v1, p0, LX/9ei;->d:LX/9eh;

    .line 1520273
    iput-object v1, v0, LX/9dx;->g:LX/9eh;

    .line 1520274
    iput-object p2, p0, LX/9ei;->k:Landroid/view/View;

    .line 1520275
    iget-object v0, p0, LX/9ei;->k:Landroid/view/View;

    const v1, 0x7f0d0392

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1520276
    iget-object v0, p0, LX/9ei;->k:Landroid/view/View;

    const v1, 0x7f0d0394

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1520277
    iput-object p3, p0, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    .line 1520278
    iget-object v0, p0, LX/9ei;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/9ei;->j:LX/9dx;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1520279
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->i()V

    .line 1520280
    invoke-direct {p0}, LX/9ei;->o()V

    .line 1520281
    iput-object p5, p0, LX/9ei;->q:LX/0am;

    .line 1520282
    return-void
.end method

.method private o()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 1520237
    iget-object v0, p0, LX/9ei;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1520238
    iget-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->e:Landroid/content/Context;

    const v2, 0x7f082419

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1520239
    iget-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1520240
    iget-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1520241
    iget-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520242
    iget-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->e:Landroid/content/Context;

    const v2, 0x7f082423    # 1.8096264E38f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1520243
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->e:Landroid/content/Context;

    const v2, 0x7f082418

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1520244
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1520245
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1520246
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520247
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9ei;->e:Landroid/content/Context;

    const v2, 0x7f082422

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1520248
    iget-object v0, p0, LX/9ei;->k:Landroid/view/View;

    const v1, 0x7f0d0393

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1520249
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520250
    iget-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520251
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520252
    :cond_0
    iget-object v0, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520253
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1520254
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520255
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1520256
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520257
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1520258
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1520259
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520260
    :cond_1
    return-void
.end method

.method private p()V
    .locals 6

    .prologue
    .line 1520217
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->f()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1520218
    const/4 v1, 0x0

    .line 1520219
    :try_start_0
    iget-object v0, p0, LX/9ei;->i:LX/8GT;

    iget-object v2, p0, LX/9ei;->g:Ljava/lang/String;

    const-string v3, ".png"

    invoke-virtual {v0, v2, v3}, LX/8GT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1520220
    iget-object v2, p0, LX/9ei;->j:LX/9dx;

    const/4 v3, 0x2

    .line 1520221
    iget-object v4, v2, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v4, v3}, Lcom/facebook/drawingview/DrawingView;->a(I)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object v3, v4

    .line 1520222
    if-eqz v0, :cond_0

    if-nez v3, :cond_1

    .line 1520223
    :cond_0
    iget-object v0, p0, LX/9ei;->e:Landroid/content/Context;

    iget-object v2, p0, LX/9ei;->e:Landroid/content/Context;

    const v3, 0x7f082424

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1520224
    iget-object v0, p0, LX/9ei;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    .line 1520225
    :goto_0
    return-void

    .line 1520226
    :cond_1
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1520227
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1520228
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 1520229
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1520230
    iget-object v0, p0, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    iget-object v4, p0, LX/9ei;->h:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-virtual {v4, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Landroid/net/Uri;)I

    move-result v4

    int-to-float v4, v4

    const-string v5, "doodle"

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Landroid/net/Uri;IIFLjava/lang/String;)Lcom/facebook/photos/creativeediting/model/DoodleParams;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1520231
    iget-object v0, p0, LX/9ei;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    goto :goto_0

    .line 1520232
    :catch_0
    move-exception v0

    .line 1520233
    if-eqz v1, :cond_2

    .line 1520234
    :try_start_2
    invoke-static {v1}, LX/8GT;->a(Landroid/net/Uri;)V

    .line 1520235
    :cond_2
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1520236
    iget-object v0, p0, LX/9ei;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/9ei;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->invalidate()V

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1520216
    iget-object v0, p0, LX/9ei;->e:Landroid/content/Context;

    const v1, 0x7f082415

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 1520160
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520161
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    .line 1520162
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p0

    invoke-direct {v1, v2, p0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1520163
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1520164
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1520165
    iget-object v2, v0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawingview/DrawingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1520166
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V
    .locals 1

    .prologue
    .line 1520207
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1520208
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520209
    iput-object p1, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520210
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->a()V

    .line 1520211
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    .line 1520212
    const/4 p1, 0x0

    invoke-virtual {v0, p1}, LX/9dx;->setVisibility(I)V

    .line 1520213
    const/4 p1, 0x1

    invoke-virtual {v0, p1}, LX/9dx;->setEnabled(Z)V

    .line 1520214
    invoke-direct {p0}, LX/9ei;->o()V

    .line 1520215
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;)V
    .locals 1

    .prologue
    .line 1520205
    iget v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->c:I

    .line 1520206
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1520190
    iget-object v0, p0, LX/9ei;->o:Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

    iput-boolean p1, v0, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->b:Z

    .line 1520191
    iget-object v0, p0, LX/9ei;->q:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520192
    iget-object v0, p0, LX/9ei;->q:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c7;

    iget-object v1, p0, LX/9ei;->o:Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

    .line 1520193
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/9c4;->COMPOSER_TEXT_ON_PHOTOS:LX/9c4;

    invoke-virtual {p0}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "composer"

    .line 1520194
    iput-object p0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1520195
    move-object v2, v2

    .line 1520196
    sget-object p0, LX/9c5;->NUM_STROKES:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1520197
    iget p1, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->c:I

    move p1, p1

    .line 1520198
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->NUM_UNDOS:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1520199
    iget p1, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->d:I

    move p1, p1

    .line 1520200
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->NUM_RESETS:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    .line 1520201
    iget p1, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->e:I

    move p1, p1

    .line 1520202
    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->DOODLE_USED_COLOR_PICKER:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    iget-boolean p1, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->a:Z

    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/9c5;->ACCEPTED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    iget-boolean p1, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->b:Z

    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1520203
    invoke-static {v0, v2}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1520204
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1520185
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->i()V

    .line 1520186
    iget-object v0, p0, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520187
    iget-object v0, p0, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1520188
    iget-object v0, p0, LX/9ei;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1520189
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1520284
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1520153
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1520154
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1520155
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1520156
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1520157
    iget-object v0, p0, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1520158
    iget-object v0, p0, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setActionButtonEnabled(Z)V

    .line 1520159
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1520167
    iget-object v0, p0, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1520168
    iget-object v0, p0, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setActionButtonEnabled(Z)V

    .line 1520169
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1520170
    sget-object v0, LX/5Rr;->DOODLE:LX/5Rr;

    return-object v0
.end method

.method public final k()LX/9ek;
    .locals 1

    .prologue
    .line 1520171
    sget-object v0, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1520172
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    .line 1520173
    iget-boolean v1, v0, LX/9dx;->e:Z

    move v0, v1

    .line 1520174
    if-nez v0, :cond_0

    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
    .locals 4

    .prologue
    .line 1520175
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    .line 1520176
    iget-boolean v1, v0, LX/9dx;->e:Z

    move v0, v1

    .line 1520177
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520178
    invoke-direct {p0}, LX/9ei;->p()V

    .line 1520179
    :cond_0
    iget-object v0, p0, LX/9ei;->j:LX/9dx;

    invoke-virtual {v0}, LX/9dx;->a()V

    .line 1520180
    iget-object v0, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520181
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v2

    .line 1520182
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    iget-object v2, p0, LX/9ei;->p:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const-class v3, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    invoke-virtual {v2, v3}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->a(Ljava/lang/Class;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDoodleParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    .line 1520183
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520184
    iget-object v0, p0, LX/9ei;->n:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    return-object v0
.end method
