.class public LX/8sw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:Z

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:[F

.field private l:[F

.field private m:I

.field private n:F

.field private o:F

.field private p:LX/8sx;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public r:F

.field public s:F

.field public t:F

.field public u:F

.field private v:LX/8sv;

.field private w:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1411912
    const-class v0, LX/8sw;

    sput-object v0, LX/8sw;->c:Ljava/lang/Class;

    return-void
.end method

.method private a(F)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1411984
    iget v1, p0, LX/8sw;->f:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1411985
    iget v1, p0, LX/8sw;->f:F

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_1

    .line 1411986
    :cond_0
    :goto_0
    return v0

    .line 1411987
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 1411913
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1411914
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 1411915
    iget-boolean v4, p0, LX/8sw;->b:Z

    if-eqz v4, :cond_0

    .line 1411916
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 1411917
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    .line 1411918
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1411919
    :cond_1
    :goto_0
    iget-object v0, p0, LX/8sw;->v:LX/8sv;

    sget-object v1, LX/8sv;->INTERCEPTING:LX/8sv;

    if-ne v0, v1, :cond_f

    move v0, v2

    :goto_1
    return v0

    .line 1411920
    :pswitch_0
    iput v1, p0, LX/8sw;->g:F

    .line 1411921
    iput v0, p0, LX/8sw;->h:F

    .line 1411922
    iget v0, p0, LX/8sw;->g:F

    iput v0, p0, LX/8sw;->i:F

    .line 1411923
    iget v0, p0, LX/8sw;->h:F

    iput v0, p0, LX/8sw;->j:F

    .line 1411924
    sget-object v0, LX/8sv;->MONITORING:LX/8sv;

    iput-object v0, p0, LX/8sw;->v:LX/8sv;

    .line 1411925
    iput v10, p0, LX/8sw;->n:F

    .line 1411926
    iput v10, p0, LX/8sw;->o:F

    .line 1411927
    iget-object v0, p0, LX/8sw;->k:[F

    invoke-static {v0, v10}, Ljava/util/Arrays;->fill([FF)V

    .line 1411928
    iget-object v0, p0, LX/8sw;->l:[F

    invoke-static {v0, v10}, Ljava/util/Arrays;->fill([FF)V

    .line 1411929
    iget-boolean v0, p0, LX/8sw;->a:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/8sw;->g:F

    iget v1, p0, LX/8sw;->h:F

    const/4 v4, 0x1

    .line 1411930
    iget v5, p0, LX/8sw;->r:F

    iget v6, p0, LX/8sw;->s:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_12

    .line 1411931
    iget v5, p0, LX/8sw;->t:F

    iget v6, p0, LX/8sw;->u:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_10

    .line 1411932
    iget v5, p0, LX/8sw;->r:F

    cmpl-float v5, v0, v5

    if-lez v5, :cond_11

    iget v5, p0, LX/8sw;->s:F

    cmpg-float v5, v0, v5

    if-gez v5, :cond_11

    iget v5, p0, LX/8sw;->t:F

    cmpl-float v5, v1, v5

    if-lez v5, :cond_11

    iget v5, p0, LX/8sw;->u:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_11

    .line 1411933
    :cond_2
    :goto_2
    move v0, v4

    .line 1411934
    if-nez v0, :cond_1

    .line 1411935
    sget-object v0, LX/8sv;->DEFERRING:LX/8sv;

    iput-object v0, p0, LX/8sw;->v:LX/8sv;

    goto :goto_0

    .line 1411936
    :pswitch_1
    sget-object v4, LX/8su;->a:[I

    iget-object v5, p0, LX/8sw;->v:LX/8sv;

    invoke-virtual {v5}, LX/8sv;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 1411937
    :cond_3
    :goto_3
    :pswitch_2
    iput v1, p0, LX/8sw;->i:F

    .line 1411938
    iput v0, p0, LX/8sw;->j:F

    goto :goto_0

    .line 1411939
    :pswitch_3
    iget v4, p0, LX/8sw;->g:F

    sub-float v4, v1, v4

    .line 1411940
    iget v5, p0, LX/8sw;->h:F

    sub-float v5, v0, v5

    .line 1411941
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 1411942
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v7

    .line 1411943
    invoke-static {v11}, LX/01m;->b(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1411944
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "abs_dx="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " dx="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1411945
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "abs_dy="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1411946
    :cond_4
    sget-object v8, LX/8sv;->INTERCEPTING:LX/8sv;

    iput-object v8, p0, LX/8sw;->v:LX/8sv;

    .line 1411947
    iget-object v8, p0, LX/8sw;->q:Ljava/util/List;

    const/16 v9, 0xa

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    iget-object v8, p0, LX/8sw;->q:Ljava/util/List;

    const/16 v9, -0xa

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    :cond_5
    const/4 v8, 0x1

    :goto_4
    move v8, v8

    .line 1411948
    if-eqz v8, :cond_8

    iget v8, p0, LX/8sw;->d:F

    cmpl-float v8, v7, v8

    if-lez v8, :cond_8

    invoke-direct {p0, v6}, LX/8sw;->a(F)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1411949
    cmpl-float v4, v5, v10

    if-lez v4, :cond_7

    .line 1411950
    const/16 v4, 0xa

    iput v4, p0, LX/8sw;->w:I

    .line 1411951
    :cond_6
    :goto_5
    iget-object v4, p0, LX/8sw;->q:Ljava/util/List;

    iget v5, p0, LX/8sw;->w:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1411952
    sget-object v4, LX/8sv;->INTERCEPTING:LX/8sv;

    iput-object v4, p0, LX/8sw;->v:LX/8sv;

    goto/16 :goto_3

    .line 1411953
    :cond_7
    const/16 v4, -0xa

    iput v4, p0, LX/8sw;->w:I

    goto :goto_5

    .line 1411954
    :cond_8
    const/4 v5, 0x1

    .line 1411955
    iget-object v8, p0, LX/8sw;->q:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    iget-object v8, p0, LX/8sw;->q:Ljava/util/List;

    const/4 v9, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_15

    :cond_9
    :goto_6
    move v5, v5

    .line 1411956
    if-eqz v5, :cond_b

    iget v5, p0, LX/8sw;->e:F

    cmpl-float v5, v6, v5

    if-lez v5, :cond_b

    invoke-direct {p0, v7}, LX/8sw;->a(F)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1411957
    cmpl-float v4, v4, v10

    if-lez v4, :cond_a

    .line 1411958
    const/4 v4, -0x1

    iput v4, p0, LX/8sw;->w:I

    .line 1411959
    :goto_7
    invoke-static {v11}, LX/01m;->b(I)Z

    move-result v4

    if-eqz v4, :cond_6

    goto :goto_5

    .line 1411960
    :cond_a
    iput v2, p0, LX/8sw;->w:I

    goto :goto_7

    .line 1411961
    :cond_b
    sget-object v4, LX/8sv;->MONITORING:LX/8sv;

    iput-object v4, p0, LX/8sw;->v:LX/8sv;

    .line 1411962
    iput v3, p0, LX/8sw;->w:I

    goto :goto_5

    .line 1411963
    :cond_c
    iget-object v4, p0, LX/8sw;->v:LX/8sv;

    sget-object v5, LX/8sv;->INTERCEPTING:LX/8sv;

    if-ne v4, v5, :cond_3

    .line 1411964
    sget-object v4, LX/8sv;->DEFERRING:LX/8sv;

    iput-object v4, p0, LX/8sw;->v:LX/8sv;

    goto/16 :goto_3

    .line 1411965
    :pswitch_4
    iget v4, p0, LX/8sw;->i:F

    sub-float v4, v1, v4

    .line 1411966
    iget v5, p0, LX/8sw;->j:F

    sub-float v5, v0, v5

    .line 1411967
    iget v6, p0, LX/8sw;->n:F

    iget-object v7, p0, LX/8sw;->k:[F

    iget v8, p0, LX/8sw;->m:I

    aget v7, v7, v8

    sub-float v7, v4, v7

    add-float/2addr v6, v7

    iput v6, p0, LX/8sw;->n:F

    .line 1411968
    iget-object v6, p0, LX/8sw;->k:[F

    iget v7, p0, LX/8sw;->m:I

    aput v4, v6, v7

    .line 1411969
    iget v4, p0, LX/8sw;->m:I

    add-int/lit8 v4, v4, 0x1

    iget-object v6, p0, LX/8sw;->k:[F

    array-length v6, v6

    rem-int/2addr v4, v6

    iput v4, p0, LX/8sw;->m:I

    .line 1411970
    iget v4, p0, LX/8sw;->o:F

    iget-object v6, p0, LX/8sw;->l:[F

    iget v7, p0, LX/8sw;->m:I

    aget v6, v6, v7

    sub-float v6, v5, v6

    add-float/2addr v4, v6

    iput v4, p0, LX/8sw;->o:F

    .line 1411971
    iget-object v4, p0, LX/8sw;->l:[F

    iget v6, p0, LX/8sw;->m:I

    aput v5, v4, v6

    .line 1411972
    iget v4, p0, LX/8sw;->m:I

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, LX/8sw;->l:[F

    array-length v5, v5

    rem-int/2addr v4, v5

    iput v4, p0, LX/8sw;->m:I

    goto/16 :goto_3

    .line 1411973
    :pswitch_5
    iget-object v4, p0, LX/8sw;->v:LX/8sv;

    sget-object v5, LX/8sv;->DEFERRING:LX/8sv;

    if-eq v4, v5, :cond_d

    iget v4, p0, LX/8sw;->w:I

    if-nez v4, :cond_e

    .line 1411974
    :cond_d
    iget-object v4, p0, LX/8sw;->v:LX/8sv;

    sget-object v5, LX/8sv;->DEFERRING:LX/8sv;

    if-eq v4, v5, :cond_e

    .line 1411975
    iget-object v4, p0, LX/8sw;->p:LX/8sx;

    invoke-interface {v4, v1, v0}, LX/8sx;->onClick(FF)V

    .line 1411976
    :cond_e
    sget-object v0, LX/8sv;->MONITORING:LX/8sv;

    iput-object v0, p0, LX/8sw;->v:LX/8sv;

    .line 1411977
    iput v3, p0, LX/8sw;->w:I

    goto/16 :goto_0

    :cond_f
    move v0, v3

    .line 1411978
    goto/16 :goto_1

    .line 1411979
    :cond_10
    iget v5, p0, LX/8sw;->r:F

    cmpl-float v5, v0, v5

    if-lez v5, :cond_11

    iget v5, p0, LX/8sw;->s:F

    cmpg-float v5, v0, v5

    if-gez v5, :cond_11

    iget v5, p0, LX/8sw;->t:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_11

    iget v5, p0, LX/8sw;->u:F

    cmpl-float v5, v1, v5

    if-gtz v5, :cond_2

    .line 1411980
    :cond_11
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1411981
    :cond_12
    iget v5, p0, LX/8sw;->t:F

    iget v6, p0, LX/8sw;->u:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_13

    .line 1411982
    iget v5, p0, LX/8sw;->r:F

    cmpg-float v5, v0, v5

    if-gez v5, :cond_11

    iget v5, p0, LX/8sw;->s:F

    cmpl-float v5, v0, v5

    if-lez v5, :cond_11

    iget v5, p0, LX/8sw;->t:F

    cmpl-float v5, v1, v5

    if-lez v5, :cond_11

    iget v5, p0, LX/8sw;->u:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_11

    goto/16 :goto_2

    .line 1411983
    :cond_13
    iget v5, p0, LX/8sw;->r:F

    cmpg-float v5, v0, v5

    if-gez v5, :cond_11

    iget v5, p0, LX/8sw;->s:F

    cmpl-float v5, v0, v5

    if-lez v5, :cond_11

    iget v5, p0, LX/8sw;->t:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_11

    iget v5, p0, LX/8sw;->u:F

    cmpl-float v5, v1, v5

    if-lez v5, :cond_11

    goto/16 :goto_2

    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_4

    :cond_15
    const/4 v5, 0x0

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
