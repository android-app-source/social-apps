.class public final LX/91e;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/91f;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/91k;

.field public b:Ljava/lang/String;

.field public c:LX/91g;

.field public d:Landroid/view/View$OnClickListener;

.field public e:LX/1OX;

.field public f:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public g:LX/903;

.field public h:LX/8zz;

.field public final synthetic i:LX/91f;


# direct methods
.method public constructor <init>(LX/91f;)V
    .locals 1

    .prologue
    .line 1431236
    iput-object p1, p0, LX/91e;->i:LX/91f;

    .line 1431237
    move-object v0, p1

    .line 1431238
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1431239
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1431235
    const-string v0, "FeelingsListLayoutComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1431203
    if-ne p0, p1, :cond_1

    .line 1431204
    :cond_0
    :goto_0
    return v0

    .line 1431205
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1431206
    goto :goto_0

    .line 1431207
    :cond_3
    check-cast p1, LX/91e;

    .line 1431208
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1431209
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1431210
    if-eq v2, v3, :cond_0

    .line 1431211
    iget-object v2, p0, LX/91e;->a:LX/91k;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/91e;->a:LX/91k;

    iget-object v3, p1, LX/91e;->a:LX/91k;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1431212
    goto :goto_0

    .line 1431213
    :cond_5
    iget-object v2, p1, LX/91e;->a:LX/91k;

    if-nez v2, :cond_4

    .line 1431214
    :cond_6
    iget-object v2, p0, LX/91e;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/91e;->b:Ljava/lang/String;

    iget-object v3, p1, LX/91e;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1431215
    goto :goto_0

    .line 1431216
    :cond_8
    iget-object v2, p1, LX/91e;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1431217
    :cond_9
    iget-object v2, p0, LX/91e;->c:LX/91g;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/91e;->c:LX/91g;

    iget-object v3, p1, LX/91e;->c:LX/91g;

    invoke-virtual {v2, v3}, LX/91g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1431218
    goto :goto_0

    .line 1431219
    :cond_b
    iget-object v2, p1, LX/91e;->c:LX/91g;

    if-nez v2, :cond_a

    .line 1431220
    :cond_c
    iget-object v2, p0, LX/91e;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/91e;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/91e;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1431221
    goto :goto_0

    .line 1431222
    :cond_e
    iget-object v2, p1, LX/91e;->d:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_d

    .line 1431223
    :cond_f
    iget-object v2, p0, LX/91e;->e:LX/1OX;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/91e;->e:LX/1OX;

    iget-object v3, p1, LX/91e;->e:LX/1OX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1431224
    goto :goto_0

    .line 1431225
    :cond_11
    iget-object v2, p1, LX/91e;->e:LX/1OX;

    if-nez v2, :cond_10

    .line 1431226
    :cond_12
    iget-object v2, p0, LX/91e;->f:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/91e;->f:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, LX/91e;->f:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1431227
    goto/16 :goto_0

    .line 1431228
    :cond_14
    iget-object v2, p1, LX/91e;->f:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v2, :cond_13

    .line 1431229
    :cond_15
    iget-object v2, p0, LX/91e;->g:LX/903;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/91e;->g:LX/903;

    iget-object v3, p1, LX/91e;->g:LX/903;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 1431230
    goto/16 :goto_0

    .line 1431231
    :cond_17
    iget-object v2, p1, LX/91e;->g:LX/903;

    if-nez v2, :cond_16

    .line 1431232
    :cond_18
    iget-object v2, p0, LX/91e;->h:LX/8zz;

    if-eqz v2, :cond_19

    iget-object v2, p0, LX/91e;->h:LX/8zz;

    iget-object v3, p1, LX/91e;->h:LX/8zz;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1431233
    goto/16 :goto_0

    .line 1431234
    :cond_19
    iget-object v2, p1, LX/91e;->h:LX/8zz;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
