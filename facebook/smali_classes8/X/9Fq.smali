.class public final LX/9Fq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9Fr;

.field public final synthetic b:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;LX/9Fr;)V
    .locals 0

    .prologue
    .line 1459106
    iput-object p1, p0, LX/9Fq;->b:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    iput-object p2, p0, LX/9Fq;->a:LX/9Fr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x4594ab93

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1459097
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v2, p0, LX/9Fq;->a:LX/9Fr;

    iget-object v2, v2, LX/9Fr;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1459098
    iget-object v0, p0, LX/9Fq;->b:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;->e:LX/8xu;

    iget-object v3, p0, LX/9Fq;->a:LX/9Fr;

    iget-object v3, v3, LX/9Fr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/9Fq;->a:LX/9Fr;

    iget-object v4, v4, LX/9Fr;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    .line 1459099
    if-nez v3, :cond_1

    .line 1459100
    :cond_0
    :goto_0
    iget-object v0, p0, LX/9Fq;->b:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v3, p0, LX/9Fq;->b:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;->c:Landroid/content/Context;

    invoke-virtual {v0, v3, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1459101
    const v0, -0x7fabbf04

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1459102
    :cond_1
    iget-object v6, v0, LX/8xu;->b:LX/0Zb;

    const-string v7, "social_search_people_cards_messenger_click"

    const/4 p1, 0x0

    invoke-interface {v6, v7, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 1459103
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1459104
    invoke-static {v0, v3, v4, v6}, LX/8xu;->a(LX/8xu;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/0oG;)LX/0oG;

    move-result-object v6

    .line 1459105
    invoke-virtual {v6}, LX/0oG;->d()V

    goto :goto_0
.end method
