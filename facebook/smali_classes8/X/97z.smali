.class public final LX/97z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1443930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)LX/97z;
    .locals 2

    .prologue
    .line 1443925
    new-instance v0, LX/97z;

    invoke-direct {v0}, LX/97z;-><init>()V

    .line 1443926
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v1

    iput-object v1, v0, LX/97z;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    .line 1443927
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v1

    iput-object v1, v0, LX/97z;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    .line 1443928
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->a()I

    move-result v1

    iput v1, v0, LX/97z;->c:I

    .line 1443929
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1443911
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1443912
    iget-object v1, p0, LX/97z;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1443913
    iget-object v3, p0, LX/97z;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1443914
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1443915
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1443916
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1443917
    const/4 v1, 0x2

    iget v3, p0, LX/97z;->c:I

    invoke-virtual {v0, v1, v3, v6}, LX/186;->a(III)V

    .line 1443918
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1443919
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1443920
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1443921
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1443922
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1443923
    new-instance v1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    invoke-direct {v1, v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;-><init>(LX/15i;)V

    .line 1443924
    return-object v1
.end method
