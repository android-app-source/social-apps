.class public LX/AEN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4V2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4V2",
        "<",
        "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/826;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0SG;

.field private final d:Lcom/facebook/graphql/model/GraphQLNode;

.field private final e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Z


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0SG;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p4    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/826;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0SG;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1646475
    iput-object p1, p0, LX/AEN;->a:LX/0Or;

    .line 1646476
    iput-object p2, p0, LX/AEN;->b:LX/0Or;

    .line 1646477
    iput-object p3, p0, LX/AEN;->c:LX/0SG;

    .line 1646478
    iput-object p4, p0, LX/AEN;->d:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1646479
    iput-object p5, p0, LX/AEN;->g:Ljava/lang/String;

    .line 1646480
    iput-object p6, p0, LX/AEN;->e:Ljava/lang/String;

    .line 1646481
    iput-object p7, p0, LX/AEN;->f:Ljava/lang/String;

    .line 1646482
    iput-object p8, p0, LX/AEN;->h:Ljava/lang/String;

    .line 1646483
    iput-boolean p9, p0, LX/AEN;->i:Z

    .line 1646484
    return-void
.end method

.method private d()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;
    .locals 2

    .prologue
    .line 1646485
    new-instance v0, LX/40S;

    invoke-direct {v0}, LX/40S;-><init>()V

    iget-object v1, p0, LX/AEN;->d:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 1646486
    iput-object v1, v0, LX/40S;->b:Ljava/lang/String;

    .line 1646487
    move-object v1, v0

    .line 1646488
    iget-boolean v0, p0, LX/AEN;->i:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1646489
    :goto_0
    iput-object v0, v1, LX/40S;->d:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1646490
    move-object v0, v1

    .line 1646491
    invoke-virtual {v0}, LX/40S;->a()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1646492
    const-string v0, "LegacyCollectionSave"

    return-object v0
.end method

.method public final synthetic b()LX/0jT;
    .locals 1

    .prologue
    .line 1646493
    invoke-direct {p0}, LX/AEN;->d()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1646494
    new-instance v1, LX/5vM;

    invoke-direct {v1}, LX/5vM;-><init>()V

    iget-boolean v0, p0, LX/AEN;->i:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/5vL;->ADD:LX/5vL;

    .line 1646495
    :goto_0
    iput-object v0, v1, LX/5vM;->c:LX/5vL;

    .line 1646496
    move-object v0, v1

    .line 1646497
    iget-object v1, p0, LX/AEN;->d:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v1

    .line 1646498
    iput-object v1, v0, LX/5vM;->a:Ljava/lang/String;

    .line 1646499
    move-object v0, v0

    .line 1646500
    iget-object v1, p0, LX/AEN;->d:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 1646501
    iput-object v1, v0, LX/5vM;->b:Ljava/lang/String;

    .line 1646502
    move-object v0, v0

    .line 1646503
    iget-object v1, p0, LX/AEN;->g:Ljava/lang/String;

    .line 1646504
    iput-object v1, v0, LX/5vM;->l:Ljava/lang/String;

    .line 1646505
    move-object v0, v0

    .line 1646506
    iget-object v1, p0, LX/AEN;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/5vM;->c(Ljava/lang/String;)LX/5vM;

    move-result-object v0

    iget-object v1, p0, LX/AEN;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/5vM;->d(Ljava/lang/String;)LX/5vM;

    move-result-object v0

    iget-object v1, p0, LX/AEN;->h:Ljava/lang/String;

    .line 1646507
    iput-object v1, v0, LX/5vM;->k:Ljava/lang/String;

    .line 1646508
    move-object v2, v0

    .line 1646509
    iget-boolean v0, p0, LX/AEN;->i:Z

    if-eqz v0, :cond_0

    .line 1646510
    iget-object v0, p0, LX/AEN;->d:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    .line 1646511
    iput-object v0, v2, LX/5vM;->g:Ljava/lang/String;

    .line 1646512
    :cond_0
    iget-object v0, p0, LX/AEN;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/AEN;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1646513
    new-instance v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0}, LX/AEN;->d()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v1

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/AEN;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    return-object v0

    .line 1646514
    :cond_1
    sget-object v0, LX/5vL;->REMOVE:LX/5vL;

    goto :goto_0
.end method
