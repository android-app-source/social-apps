.class public final LX/ALz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/ui/ScalingToggleableEditText;)V
    .locals 0

    .prologue
    .line 1665684
    iput-object p1, p0, LX/ALz;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1665677
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 1665678
    iget-object v0, p0, LX/ALz;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    const/4 v1, 0x0

    .line 1665679
    iput-boolean v1, v0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->e:Z

    .line 1665680
    :cond_0
    iget-object v0, p0, LX/ALz;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    iget-boolean v0, v0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->e:Z

    if-nez v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 1665681
    iget-object v0, p0, LX/ALz;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    iget-object v0, v0, LX/AM1;->b:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1665682
    :cond_1
    iget-object v0, p0, LX/ALz;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    iget-object v0, v0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->c:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1665683
    return v2
.end method
