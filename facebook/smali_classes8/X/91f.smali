.class public LX/91f;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/91d;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/91h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1431240
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/91f;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/91h;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1431241
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1431242
    iput-object p1, p0, LX/91f;->b:LX/0Ot;

    .line 1431243
    return-void
.end method

.method public static a(LX/0QB;)LX/91f;
    .locals 4

    .prologue
    .line 1431244
    const-class v1, LX/91f;

    monitor-enter v1

    .line 1431245
    :try_start_0
    sget-object v0, LX/91f;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1431246
    sput-object v2, LX/91f;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1431247
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1431248
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1431249
    new-instance v3, LX/91f;

    const/16 p0, 0x19a5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/91f;-><init>(LX/0Ot;)V

    .line 1431250
    move-object v0, v3

    .line 1431251
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1431252
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1431253
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1431254
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1431255
    check-cast p2, LX/91e;

    .line 1431256
    iget-object v0, p0, LX/91f;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/91h;

    iget-object v2, p2, LX/91e;->a:LX/91k;

    iget-object v3, p2, LX/91e;->b:Ljava/lang/String;

    iget-object v4, p2, LX/91e;->c:LX/91g;

    iget-object v5, p2, LX/91e;->d:Landroid/view/View$OnClickListener;

    iget-object v6, p2, LX/91e;->e:LX/1OX;

    iget-object v7, p2, LX/91e;->f:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v8, p2, LX/91e;->g:LX/903;

    move-object v1, p1

    const/4 p2, 0x0

    const/4 v10, 0x0

    .line 1431257
    sget-object v9, LX/91g;->NETWORK_ERROR:LX/91g;

    if-ne v4, v9, :cond_0

    .line 1431258
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    iget-object v10, v0, LX/91h;->b:LX/91K;

    invoke-virtual {v10, v1}, LX/91K;->c(LX/1De;)LX/91J;

    move-result-object v10

    const p0, 0x7f08003b

    invoke-virtual {v10, p0}, LX/91J;->h(I)LX/91J;

    move-result-object v10

    invoke-virtual {v10, v5}, LX/91J;->a(Landroid/view/View$OnClickListener;)LX/91J;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v9

    .line 1431259
    :goto_0
    move-object v0, v9

    .line 1431260
    return-object v0

    .line 1431261
    :cond_0
    sget-object v9, LX/91g;->GENERIC_ERROR:LX/91g;

    if-ne v4, v9, :cond_1

    .line 1431262
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    iget-object v10, v0, LX/91h;->b:LX/91K;

    invoke-virtual {v10, v1}, LX/91K;->c(LX/1De;)LX/91J;

    move-result-object v10

    invoke-virtual {v10, v5}, LX/91J;->a(Landroid/view/View$OnClickListener;)LX/91J;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v9

    goto :goto_0

    .line 1431263
    :cond_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/4 p0, 0x4

    invoke-interface {v9, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    if-nez v7, :cond_3

    move-object v9, v10

    :goto_1
    invoke-interface {p0, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p0

    if-eqz v7, :cond_4

    move-object v9, v10

    :goto_2
    invoke-interface {p0, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/5K1;->c(LX/1De;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/5Jz;->a(LX/1OX;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/5Jz;->a(LX/5Je;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Di;->a(I)LX/1Di;

    move-result-object p0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-interface {p0, p1}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    invoke-interface {v9, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    sget-object p0, LX/91g;->LOADING:LX/91g;

    if-ne v4, p0, :cond_2

    invoke-static {v1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const/16 p0, 0x1e

    invoke-interface {v10, p0}, LX/1Di;->r(I)LX/1Di;

    move-result-object v10

    :cond_2
    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v9

    goto :goto_0

    :cond_3
    iget-object v9, v0, LX/91h;->a:LX/91P;

    invoke-virtual {v9, v1}, LX/91P;->c(LX/1De;)LX/91N;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/91N;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/91N;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/91N;->a(LX/903;)LX/91N;

    move-result-object v9

    goto :goto_1

    :cond_4
    invoke-static {v1}, LX/91W;->c(LX/1De;)LX/91U;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/91U;->a(Ljava/lang/CharSequence;)LX/91U;

    move-result-object v9

    .line 1431264
    const p1, 0x5dcaf977

    const/4 v0, 0x0

    invoke-static {v1, p1, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p1

    move-object p1, p1

    .line 1431265
    invoke-virtual {v9, p1}, LX/91U;->a(LX/1dQ;)LX/91U;

    move-result-object v9

    const/4 p1, 0x1

    invoke-virtual {v9, p1}, LX/91U;->h(I)LX/91U;

    move-result-object v9

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1431266
    invoke-static {}, LX/1dS;->b()V

    .line 1431267
    iget v0, p1, LX/1dQ;->b:I

    .line 1431268
    packed-switch v0, :pswitch_data_0

    .line 1431269
    :goto_0
    return-object v2

    .line 1431270
    :pswitch_0
    check-cast p2, LX/91c;

    .line 1431271
    iget-object v0, p2, LX/91c;->a:Ljava/lang/String;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1431272
    check-cast v1, LX/91e;

    .line 1431273
    iget-object p1, p0, LX/91f;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/91e;->h:LX/8zz;

    .line 1431274
    if-eqz p1, :cond_0

    .line 1431275
    invoke-interface {p1, v0}, LX/8zz;->a(Ljava/lang/String;)V

    .line 1431276
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5dcaf977
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/91d;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1431277
    new-instance v1, LX/91e;

    invoke-direct {v1, p0}, LX/91e;-><init>(LX/91f;)V

    .line 1431278
    sget-object v2, LX/91f;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/91d;

    .line 1431279
    if-nez v2, :cond_0

    .line 1431280
    new-instance v2, LX/91d;

    invoke-direct {v2}, LX/91d;-><init>()V

    .line 1431281
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/91d;->a$redex0(LX/91d;LX/1De;IILX/91e;)V

    .line 1431282
    move-object v1, v2

    .line 1431283
    move-object v0, v1

    .line 1431284
    return-object v0
.end method
