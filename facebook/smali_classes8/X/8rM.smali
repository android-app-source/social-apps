.class public LX/8rM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/03V;

.field public final b:[I

.field public final c:[I
    .annotation build Lcom/facebook/ufiservices/richtext/ListItemCounter$ListStyle;
    .end annotation
.end field

.field public d:I


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/16 v1, 0x40

    .line 1408792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1408793
    new-array v0, v1, [I

    iput-object v0, p0, LX/8rM;->b:[I

    .line 1408794
    new-array v0, v1, [I

    iput-object v0, p0, LX/8rM;->c:[I

    .line 1408795
    const/4 v0, -0x1

    iput v0, p0, LX/8rM;->d:I

    .line 1408796
    iput-object p1, p0, LX/8rM;->a:LX/03V;

    .line 1408797
    return-void
.end method


# virtual methods
.method public final a(ILcom/facebook/graphql/enums/GraphQLComposedBlockType;)V
    .locals 5

    .prologue
    .line 1408798
    iget v0, p0, LX/8rM;->d:I

    add-int/lit8 v0, v0, 0x1

    if-le p1, v0, :cond_0

    .line 1408799
    iget-object v0, p0, LX/8rM;->a:LX/03V;

    const-string v1, "ListItemCounter"

    const-string v2, "Trying to add depth %d when having only %d list depth."

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p0, LX/8rM;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1408800
    :cond_0
    const/16 v0, 0x40

    if-ge p1, v0, :cond_3

    :goto_0
    move v0, p1

    .line 1408801
    iget v1, p0, LX/8rM;->d:I

    if-le v0, v1, :cond_1

    .line 1408802
    :goto_1
    iget v1, p0, LX/8rM;->d:I

    if-le v0, v1, :cond_2

    .line 1408803
    iget v1, p0, LX/8rM;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/8rM;->d:I

    .line 1408804
    iget-object v1, p0, LX/8rM;->b:[I

    iget v2, p0, LX/8rM;->d:I

    const/4 v3, 0x1

    aput v3, v1, v2

    .line 1408805
    iget-object v1, p0, LX/8rM;->c:[I

    iget v2, p0, LX/8rM;->d:I

    .line 1408806
    iget v3, p0, LX/8rM;->d:I

    if-ge v3, v0, :cond_4

    .line 1408807
    const/4 v3, 0x0

    .line 1408808
    :goto_2
    move v3, v3

    .line 1408809
    aput v3, v1, v2

    goto :goto_1

    .line 1408810
    :cond_1
    iput v0, p0, LX/8rM;->d:I

    .line 1408811
    iget-object v0, p0, LX/8rM;->b:[I

    iget v1, p0, LX/8rM;->d:I

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 1408812
    :cond_2
    return-void

    :cond_3
    const/16 p1, 0x3f

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne p2, v3, :cond_5

    const/4 v3, 0x1

    goto :goto_2

    :cond_5
    const/4 v3, 0x2

    goto :goto_2
.end method

.method public final c()I
    .locals 5

    .prologue
    .line 1408813
    const/4 v1, 0x1

    .line 1408814
    iget v0, p0, LX/8rM;->d:I

    add-int/lit8 v0, v0, -0x1

    .line 1408815
    :goto_0
    if-ltz v0, :cond_0

    iget-object v2, p0, LX/8rM;->c:[I

    aget v2, v2, v0

    iget-object v3, p0, LX/8rM;->c:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    if-ne v2, v3, :cond_0

    .line 1408816
    add-int/lit8 v1, v1, 0x1

    .line 1408817
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1408818
    :cond_0
    return v1
.end method
