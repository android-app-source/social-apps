.class public final enum LX/8z2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8z2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8z2;

.field public static final enum BRANDED_CONTENT:LX/8z2;

.field public static final enum MINUTIAE:LX/8z2;

.field public static final enum PEOPLE:LX/8z2;

.field public static final enum PERSON:LX/8z2;

.field public static final enum PLACE:LX/8z2;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1426778
    new-instance v0, LX/8z2;

    const-string v1, "PERSON"

    invoke-direct {v0, v1, v2}, LX/8z2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8z2;->PERSON:LX/8z2;

    .line 1426779
    new-instance v0, LX/8z2;

    const-string v1, "PEOPLE"

    invoke-direct {v0, v1, v3}, LX/8z2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8z2;->PEOPLE:LX/8z2;

    .line 1426780
    new-instance v0, LX/8z2;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v4}, LX/8z2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8z2;->PLACE:LX/8z2;

    .line 1426781
    new-instance v0, LX/8z2;

    const-string v1, "MINUTIAE"

    invoke-direct {v0, v1, v5}, LX/8z2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8z2;->MINUTIAE:LX/8z2;

    .line 1426782
    new-instance v0, LX/8z2;

    const-string v1, "BRANDED_CONTENT"

    invoke-direct {v0, v1, v6}, LX/8z2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8z2;->BRANDED_CONTENT:LX/8z2;

    .line 1426783
    const/4 v0, 0x5

    new-array v0, v0, [LX/8z2;

    sget-object v1, LX/8z2;->PERSON:LX/8z2;

    aput-object v1, v0, v2

    sget-object v1, LX/8z2;->PEOPLE:LX/8z2;

    aput-object v1, v0, v3

    sget-object v1, LX/8z2;->PLACE:LX/8z2;

    aput-object v1, v0, v4

    sget-object v1, LX/8z2;->MINUTIAE:LX/8z2;

    aput-object v1, v0, v5

    sget-object v1, LX/8z2;->BRANDED_CONTENT:LX/8z2;

    aput-object v1, v0, v6

    sput-object v0, LX/8z2;->$VALUES:[LX/8z2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1426784
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8z2;
    .locals 1

    .prologue
    .line 1426785
    const-class v0, LX/8z2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8z2;

    return-object v0
.end method

.method public static values()[LX/8z2;
    .locals 1

    .prologue
    .line 1426786
    sget-object v0, LX/8z2;->$VALUES:[LX/8z2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8z2;

    return-object v0
.end method
