.class public final LX/9KE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1467356
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1467357
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467358
    :goto_0
    return v1

    .line 1467359
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467360
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1467361
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1467362
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1467363
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1467364
    const-string v5, "group_cover_photo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1467365
    const/4 v4, 0x0

    .line 1467366
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_9

    .line 1467367
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467368
    :goto_2
    move v3, v4

    .line 1467369
    goto :goto_1

    .line 1467370
    :cond_2
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1467371
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1467372
    :cond_3
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1467373
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1467374
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1467375
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1467376
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1467377
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1467378
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1467379
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467380
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 1467381
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1467382
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1467383
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 1467384
    const-string v6, "photo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1467385
    const/4 v5, 0x0

    .line 1467386
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_d

    .line 1467387
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467388
    :goto_4
    move v3, v5

    .line 1467389
    goto :goto_3

    .line 1467390
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1467391
    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1467392
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_9
    move v3, v4

    goto :goto_3

    .line 1467393
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467394
    :cond_b
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 1467395
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1467396
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1467397
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 1467398
    const-string v7, "image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1467399
    const/4 v6, 0x0

    .line 1467400
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_11

    .line 1467401
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467402
    :goto_6
    move v3, v6

    .line 1467403
    goto :goto_5

    .line 1467404
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1467405
    invoke-virtual {p1, v5, v3}, LX/186;->b(II)V

    .line 1467406
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_4

    :cond_d
    move v3, v5

    goto :goto_5

    .line 1467407
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467408
    :cond_f
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_10

    .line 1467409
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1467410
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1467411
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_f

    if-eqz v7, :cond_f

    .line 1467412
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1467413
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_7

    .line 1467414
    :cond_10
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1467415
    invoke-virtual {p1, v6, v3}, LX/186;->b(II)V

    .line 1467416
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_6

    :cond_11
    move v3, v6

    goto :goto_7
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1467417
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1467418
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1467419
    if-eqz v0, :cond_3

    .line 1467420
    const-string v1, "group_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467421
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1467422
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1467423
    if-eqz v1, :cond_2

    .line 1467424
    const-string v2, "photo"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467425
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1467426
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1467427
    if-eqz v2, :cond_1

    .line 1467428
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467429
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1467430
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1467431
    if-eqz v0, :cond_0

    .line 1467432
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467433
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1467434
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1467435
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1467436
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1467437
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1467438
    if-eqz v0, :cond_4

    .line 1467439
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467440
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1467441
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1467442
    if-eqz v0, :cond_5

    .line 1467443
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467444
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1467445
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1467446
    return-void
.end method
