.class public final LX/A7Q;
.super LX/A7P;
.source ""


# instance fields
.field public final synthetic a:LX/0fP;


# direct methods
.method public constructor <init>(LX/0fP;)V
    .locals 0

    .prologue
    .line 1626042
    iput-object p1, p0, LX/A7Q;->a:LX/0fP;

    invoke-direct {p0}, LX/A7P;-><init>()V

    return-void
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1626084
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    .line 1626085
    iget-object v1, v0, LX/0fP;->q:LX/10e;

    move-object v0, v1

    .line 1626086
    const-string v1, "drawer_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1626087
    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1626080
    const-string v0, "drawer_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1626081
    const-string v0, "drawer_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/10e;

    .line 1626082
    iget-object v1, p0, LX/A7Q;->a:LX/0fP;

    iget-object v1, v1, LX/0fP;->h:LX/A7T;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/A7T;->a(LX/10e;Z)V

    .line 1626083
    :cond_0
    return-void
.end method

.method public static g(LX/A7Q;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1626077
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->m:Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    .line 1626078
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, p0

    .line 1626079
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1626072
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    invoke-static {v0}, LX/0fP;->q(LX/0fP;)V

    .line 1626073
    invoke-static {p0}, LX/A7Q;->g(LX/A7Q;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, LX/A7Q;->d(Landroid/os/Bundle;)V

    .line 1626074
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->n:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 1626075
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1626076
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1626069
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    invoke-static {v0}, LX/0fP;->q(LX/0fP;)V

    .line 1626070
    invoke-direct {p0, p1}, LX/A7Q;->d(Landroid/os/Bundle;)V

    .line 1626071
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1626064
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    .line 1626065
    iget-object v1, v0, LX/0fP;->q:LX/10e;

    move-object v0, v1

    .line 1626066
    iget-object v1, p0, LX/A7Q;->a:LX/0fP;

    invoke-static {v1}, LX/0fP;->q(LX/0fP;)V

    .line 1626067
    iget-object v1, p0, LX/A7Q;->a:LX/0fP;

    iget-object v1, v1, LX/0fP;->h:LX/A7T;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/A7T;->a(LX/10e;Z)V

    .line 1626068
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1626062
    invoke-direct {p0, p1}, LX/A7Q;->c(Landroid/os/Bundle;)V

    .line 1626063
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1626054
    invoke-static {p0}, LX/A7Q;->g(LX/A7Q;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, LX/A7Q;->c(Landroid/os/Bundle;)V

    .line 1626055
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->n:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 1626056
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1626057
    :cond_0
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    iget-boolean v0, v0, LX/0fP;->s:Z

    if-eqz v0, :cond_1

    .line 1626058
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    const/4 v1, 0x0

    .line 1626059
    iput-boolean v1, v0, LX/0fP;->s:Z

    .line 1626060
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/ui/drawers/DrawerController$DrawerLifecycleListener$1;

    invoke-direct {v1, p0}, Lcom/facebook/ui/drawers/DrawerController$DrawerLifecycleListener$1;-><init>(LX/A7Q;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 1626061
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1626043
    iget-object v0, p0, LX/A7Q;->a:LX/0fP;

    .line 1626044
    iget-object v1, v0, LX/0fP;->j:LX/A7O;

    if-eqz v1, :cond_0

    .line 1626045
    iget-object v1, v0, LX/0fP;->j:LX/A7O;

    .line 1626046
    iget-object v2, v1, LX/A7O;->d:LX/0fM;

    move-object v1, v2

    .line 1626047
    invoke-static {v0, v1}, LX/0fP;->a(LX/0fP;LX/0fM;)V

    .line 1626048
    :cond_0
    iget-object v1, v0, LX/0fP;->k:LX/A7O;

    if-eqz v1, :cond_1

    .line 1626049
    iget-object v1, v0, LX/0fP;->k:LX/A7O;

    .line 1626050
    iget-object v2, v1, LX/A7O;->d:LX/0fM;

    move-object v1, v2

    .line 1626051
    invoke-static {v0, v1}, LX/0fP;->a(LX/0fP;LX/0fM;)V

    .line 1626052
    :cond_1
    invoke-super {p0}, LX/A7P;->d()V

    .line 1626053
    return-void
.end method
