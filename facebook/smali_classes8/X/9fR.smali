.class public final LX/9fR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9fH;


# instance fields
.field public final synthetic a:LX/9fU;


# direct methods
.method public constructor <init>(LX/9fU;)V
    .locals 0

    .prologue
    .line 1521908
    iput-object p1, p0, LX/9fR;->a:LX/9fU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1521901
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    iget-object v0, v0, LX/9fU;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cP;

    new-instance v1, LX/9cR;

    invoke-direct {v1}, LX/9cR;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1521902
    return-void
.end method

.method public final a(LX/362;)V
    .locals 2

    .prologue
    .line 1521893
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    const/4 v1, 0x1

    .line 1521894
    iput-boolean v1, v0, LX/9fU;->n:Z

    .line 1521895
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    iget-object v0, v0, LX/9fU;->q:LX/9f8;

    .line 1521896
    invoke-static {p1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, v1}, LX/9f8;->a(LX/5i7;)V

    .line 1521897
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1521903
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    const/4 v1, 0x1

    .line 1521904
    iput-boolean v1, v0, LX/9fU;->n:Z

    .line 1521905
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    iget-object v0, v0, LX/9fU;->q:LX/9f8;

    .line 1521906
    iget-object v1, p0, LX/9fR;->a:LX/9fU;

    iget-object v1, v1, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getSelectedItem()LX/362;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/9f8;->a(Ljava/lang/String;LX/5i7;)V

    .line 1521907
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1521898
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    .line 1521899
    iput-boolean p1, v0, LX/9fU;->n:Z

    .line 1521900
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1521888
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    const/4 v1, 0x1

    .line 1521889
    iput-boolean v1, v0, LX/9fU;->n:Z

    .line 1521890
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    iget-object v0, v0, LX/9fU;->q:LX/9f8;

    .line 1521891
    iget-object v1, p0, LX/9fR;->a:LX/9fU;

    iget-object v1, v1, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getSelectedItem()LX/362;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/9f8;->b(Ljava/lang/String;LX/5i7;)V

    .line 1521892
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1521883
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    const/4 v1, 0x1

    .line 1521884
    iput-boolean v1, v0, LX/9fU;->n:Z

    .line 1521885
    iget-object v0, p0, LX/9fR;->a:LX/9fU;

    iget-object v0, v0, LX/9fU;->q:LX/9f8;

    .line 1521886
    iget-object v1, p0, LX/9fR;->a:LX/9fU;

    iget-object v1, v1, LX/9fU;->j:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getSelectedItem()LX/362;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/9f8;->c(Ljava/lang/String;LX/5i7;)V

    .line 1521887
    return-void
.end method
