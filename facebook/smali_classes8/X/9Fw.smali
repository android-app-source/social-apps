.class public final LX/9Fw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9Fz;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;LX/9Fz;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1459187
    iput-object p1, p0, LX/9Fw;->d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    iput-object p2, p0, LX/9Fw;->a:LX/9Fz;

    iput-object p3, p0, LX/9Fw;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9Fw;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x546f8b9c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1459188
    iget-object v0, p0, LX/9Fw;->d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    iget-object v2, p0, LX/9Fw;->a:LX/9Fz;

    iget-object v2, v2, LX/9Fz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    iget-object v3, p0, LX/9Fw;->a:LX/9Fz;

    iget-object v3, v3, LX/9Fz;->b:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v4, p0, LX/9Fw;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->a(Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;)LX/6PS;

    move-result-object v2

    .line 1459189
    iget-object v0, p0, LX/9Fw;->d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    iget-object v3, v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->c:LX/8xq;

    iget-object v4, p0, LX/9Fw;->b:Ljava/lang/String;

    iget-object v5, p0, LX/9Fw;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_0
    new-instance v6, LX/9Fv;

    invoke-direct {v6, p0}, LX/9Fv;-><init>(LX/9Fw;)V

    .line 1459190
    new-instance v7, LX/4DR;

    invoke-direct {v7}, LX/4DR;-><init>()V

    invoke-virtual {v7, v4}, LX/4DR;->a(Ljava/lang/String;)LX/4DR;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4DR;->a(Ljava/lang/Boolean;)LX/4DR;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/4DR;->b(Ljava/lang/String;)LX/4DR;

    move-result-object v7

    .line 1459191
    invoke-static {}, LX/5Iz;->a()LX/5Ix;

    move-result-object v8

    .line 1459192
    const-string v9, "input"

    invoke-virtual {v8, v9, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1459193
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 1459194
    if-eqz v0, :cond_1

    .line 1459195
    new-instance v8, LX/5J5;

    invoke-direct {v8}, LX/5J5;-><init>()V

    .line 1459196
    if-eqz v0, :cond_0

    .line 1459197
    invoke-static {v0}, LX/5I9;->c(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    move-result-object v9

    .line 1459198
    iput-object v9, v8, LX/5J5;->a:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    .line 1459199
    :cond_0
    invoke-virtual {v8}, LX/5J5;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$PlaceListEditPendingProfileRecommendationMutationModel;

    move-result-object v8

    move-object v8, v8

    .line 1459200
    invoke-virtual {v7, v8}, LX/399;->a(LX/0jT;)LX/399;

    .line 1459201
    :cond_1
    iget-object v8, v3, LX/8xq;->b:LX/1Ck;

    const-string v9, "confirm_pending_person_card"

    iget-object p1, v3, LX/8xq;->a:LX/0tX;

    invoke-virtual {p1, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v6}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object p1

    invoke-virtual {v8, v9, v7, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1459202
    if-nez v2, :cond_3

    .line 1459203
    const v0, 0x6da58ba3

    invoke-static {v0, v1}, LX/02F;->a(II)V

    .line 1459204
    :goto_1
    return-void

    .line 1459205
    :cond_2
    iget-object v0, v2, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    goto :goto_0

    .line 1459206
    :cond_3
    iget-object v0, p0, LX/9Fw;->d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->h:LX/1K9;

    new-instance v3, LX/8q4;

    iget-object v4, v2, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, v2, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/1K9;->a(LX/1KJ;)V

    .line 1459207
    const v0, -0x5214668a

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_1
.end method
