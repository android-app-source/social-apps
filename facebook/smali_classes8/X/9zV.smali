.class public final LX/9zV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1601764
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1601765
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1601766
    :goto_0
    return v1

    .line 1601767
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1601768
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 1601769
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1601770
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1601771
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1601772
    const-string v8, "debug_info_native_template_view"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1601773
    invoke-static {p0, p1}, LX/5ex;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1601774
    :cond_2
    const-string v8, "logging_unit_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1601775
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1601776
    :cond_3
    const-string v8, "module_display_styles"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1601777
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1601778
    :cond_4
    const-string v8, "module_role"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1601779
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 1601780
    :cond_5
    const-string v8, "native_template_view"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1601781
    invoke-static {p0, p1}, LX/5ex;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1601782
    :cond_6
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1601783
    invoke-static {p0, p1}, LX/9zS;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1601784
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1601785
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1601786
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1601787
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1601788
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1601789
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1601790
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1601791
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 1601792
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1601793
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601794
    if-eqz v0, :cond_0

    .line 1601795
    const-string v1, "debug_info_native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601796
    invoke-static {p0, v0, p2, p3}, LX/5ex;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1601797
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1601798
    if-eqz v0, :cond_1

    .line 1601799
    const-string v1, "logging_unit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601800
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601801
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1601802
    if-eqz v0, :cond_2

    .line 1601803
    const-string v0, "module_display_styles"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601804
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1601805
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1601806
    if-eqz v0, :cond_3

    .line 1601807
    const-string v0, "module_role"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601808
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601809
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601810
    if-eqz v0, :cond_4

    .line 1601811
    const-string v1, "native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601812
    invoke-static {p0, v0, p2, p3}, LX/5ex;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1601813
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601814
    if-eqz v0, :cond_5

    .line 1601815
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601816
    invoke-static {p0, v0, p2, p3}, LX/9zS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1601817
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1601818
    return-void
.end method
