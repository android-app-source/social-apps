.class public LX/8pY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/8pW;

.field private c:LX/0tX;

.field private d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/03V;


# direct methods
.method public constructor <init>(LX/0Or;LX/8pW;LX/0tX;LX/1Ck;LX/03V;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/translation/annotations/IsCommentTranslationEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/8pW;",
            "LX/0tX;",
            "LX/1Ck;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1405986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405987
    iput-object p1, p0, LX/8pY;->a:LX/0Or;

    .line 1405988
    iput-object p2, p0, LX/8pY;->b:LX/8pW;

    .line 1405989
    iput-object p3, p0, LX/8pY;->c:LX/0tX;

    .line 1405990
    iput-object p4, p0, LX/8pY;->d:LX/1Ck;

    .line 1405991
    iput-object p5, p0, LX/8pY;->e:LX/03V;

    .line 1405992
    return-void
.end method

.method public static a$redex0(LX/8pY;Ljava/lang/Throwable;LX/3Wt;)V
    .locals 2

    .prologue
    .line 1405993
    iget-object v0, p0, LX/8pY;->e:LX/03V;

    const-string v1, "FetchCommentTranslationFailed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1405994
    invoke-interface {p2}, LX/3Wt;->hv_()V

    .line 1405995
    return-void
.end method

.method public static b(LX/0QB;)LX/8pY;
    .locals 6

    .prologue
    .line 1405996
    new-instance v0, LX/8pY;

    const/16 v1, 0x1589

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/8pW;->a(LX/0QB;)LX/8pW;

    move-result-object v2

    check-cast v2, LX/8pW;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct/range {v0 .. v5}, LX/8pY;-><init>(LX/0Or;LX/8pW;LX/0tX;LX/1Ck;LX/03V;)V

    .line 1405997
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/3Wt;)V
    .locals 2

    .prologue
    .line 1405998
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/8pY;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1405999
    :cond_0
    :goto_0
    return-void

    .line 1406000
    :cond_1
    iget-object v0, p0, LX/8pY;->b:LX/8pW;

    invoke-virtual {v0, p1}, LX/8pW;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1406001
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 1406002
    iget-object v1, p0, LX/8pY;->b:LX/8pW;

    invoke-virtual {v1, p1}, LX/8pW;->b(Ljava/lang/String;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1406003
    invoke-interface {p2, v0}, LX/3Wt;->a(Ljava/util/Map;)V

    goto :goto_0

    .line 1406004
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/8pY;->a(Ljava/util/List;LX/3Wt;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;LX/3Wt;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Wt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406005
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406006
    :goto_0
    return-void

    .line 1406007
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1406008
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1406009
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1406010
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1406011
    iget-object v3, p0, LX/8pY;->b:LX/8pW;

    invoke-virtual {v3, v0}, LX/8pW;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1406012
    iget-object v3, p0, LX/8pY;->b:LX/8pW;

    invoke-virtual {v3, v0}, LX/8pW;->b(Ljava/lang/String;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1406013
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 1406014
    :cond_2
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1406015
    invoke-interface {p2, v1}, LX/3Wt;->a(Ljava/util/Map;)V

    goto :goto_0

    .line 1406016
    :cond_3
    const-string v0, "fetch_comment_translation"

    .line 1406017
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1406018
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1406019
    goto :goto_2

    .line 1406020
    :cond_4
    move-object v0, v2

    .line 1406021
    new-instance v2, LX/7fL;

    invoke-direct {v2}, LX/7fL;-><init>()V

    move-object v2, v2

    .line 1406022
    const-string v3, "comment_ids"

    invoke-virtual {v2, v3, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1406023
    iget-object v3, p0, LX/8pY;->c:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    invoke-static {v2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1406024
    new-instance v3, LX/8pX;

    invoke-direct {v3, p0, p2, v1}, LX/8pX;-><init>(LX/8pY;LX/3Wt;Ljava/util/HashMap;)V

    .line 1406025
    iget-object v1, p0, LX/8pY;->d:LX/1Ck;

    invoke-static {v2}, LX/52F;->a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v2

    invoke-virtual {v1, v0, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto/16 :goto_0
.end method
