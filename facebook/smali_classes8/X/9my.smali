.class public LX/9my;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1536560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1536561
    return-void
.end method

.method public static a(Lcom/facebook/react/bridge/CatalystInstance;)V
    .locals 4

    .prologue
    .line 1536555
    invoke-interface {p0}, Lcom/facebook/react/bridge/CatalystInstance;->f()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5p4;

    .line 1536556
    instance-of v2, v0, LX/9mx;

    if-eqz v2, :cond_0

    .line 1536557
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cleaning data from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/5p4;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1536558
    check-cast v0, LX/9mx;

    invoke-interface {v0}, LX/9mx;->a()V

    goto :goto_0

    .line 1536559
    :cond_1
    return-void
.end method
