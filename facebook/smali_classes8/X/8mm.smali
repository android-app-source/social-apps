.class public final LX/8mm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/8mp;


# direct methods
.method public constructor <init>(LX/8mp;)V
    .locals 0

    .prologue
    .line 1399857
    iput-object p1, p0, LX/8mm;->a:LX/8mp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1399858
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 1399859
    iput v3, v2, LX/8mp;->f:F

    .line 1399860
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1399861
    iput v3, v2, LX/8mp;->g:F

    .line 1399862
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    .line 1399863
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v3, p0, LX/8mm;->a:LX/8mp;

    iget-object v4, p0, LX/8mm;->a:LX/8mp;

    iget v4, v4, LX/8mp;->f:F

    iget-object v5, p0, LX/8mm;->a:LX/8mp;

    iget v5, v5, LX/8mp;->g:F

    invoke-static {v3, v4, v5}, LX/8mp;->b(LX/8mp;FF)Lcom/facebook/stickers/ui/StickerView;

    move-result-object v3

    .line 1399864
    iput-object v3, v2, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    .line 1399865
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    if-eqz v2, :cond_0

    .line 1399866
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    invoke-virtual {v2}, Lcom/facebook/stickers/ui/StickerView;->c()V

    .line 1399867
    :cond_0
    :goto_0
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-boolean v2, v2, LX/8mp;->a:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->d:LX/4m4;

    sget-object v3, LX/4m4;->COMPOSER:LX/4m4;

    if-ne v2, v3, :cond_6

    .line 1399868
    :cond_1
    iget-object v0, p0, LX/8mm;->a:LX/8mp;

    iget-object v0, v0, LX/8mp;->b:Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

    invoke-virtual {v0}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->a()V

    move v0, v1

    .line 1399869
    :cond_2
    :goto_1
    return v0

    .line 1399870
    :cond_3
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    if-eqz v2, :cond_0

    .line 1399871
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v0, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v7, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_5

    .line 1399872
    :cond_4
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    invoke-virtual {v2}, Lcom/facebook/stickers/ui/StickerView;->d()V

    .line 1399873
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    .line 1399874
    iput-object v5, v2, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    .line 1399875
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    .line 1399876
    iput-object v5, v2, LX/8mp;->i:Lcom/facebook/stickers/model/Sticker;

    .line 1399877
    goto :goto_0

    .line 1399878
    :cond_5
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v3, p0, LX/8mm;->a:LX/8mp;

    iget v3, v3, LX/8mp;->f:F

    iget-object v4, p0, LX/8mm;->a:LX/8mp;

    iget v4, v4, LX/8mp;->g:F

    invoke-static {v2, v3, v4}, LX/8mp;->b(LX/8mp;FF)Lcom/facebook/stickers/ui/StickerView;

    move-result-object v2

    .line 1399879
    if-eqz v2, :cond_0

    iget-object v3, p0, LX/8mm;->a:LX/8mp;

    iget-object v3, v3, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    if-eq v3, v2, :cond_0

    .line 1399880
    iget-object v3, p0, LX/8mm;->a:LX/8mp;

    iget-object v3, v3, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    invoke-virtual {v3}, Lcom/facebook/stickers/ui/StickerView;->d()V

    .line 1399881
    iget-object v3, p0, LX/8mm;->a:LX/8mp;

    .line 1399882
    iput-object v2, v3, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    .line 1399883
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->h:Lcom/facebook/stickers/ui/StickerView;

    invoke-virtual {v2}, Lcom/facebook/stickers/ui/StickerView;->c()V

    .line 1399884
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    .line 1399885
    iput-object v5, v2, LX/8mp;->i:Lcom/facebook/stickers/model/Sticker;

    .line 1399886
    goto :goto_0

    .line 1399887
    :cond_6
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->b:Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

    invoke-virtual {v2}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->b()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1399888
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v0, :cond_7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v7, :cond_7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    .line 1399889
    :cond_7
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->b:Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

    invoke-virtual {v2}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->a()V

    .line 1399890
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v2, v1}, Landroid/widget/GridView;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_1

    .line 1399891
    :cond_8
    iget-object v1, p0, LX/8mm;->a:LX/8mp;

    iget v1, v1, LX/8mp;->f:F

    cmpg-float v1, v1, v6

    if-ltz v1, :cond_2

    iget-object v1, p0, LX/8mm;->a:LX/8mp;

    iget v1, v1, LX/8mp;->g:F

    cmpg-float v1, v1, v6

    if-ltz v1, :cond_2

    iget-object v1, p0, LX/8mm;->a:LX/8mp;

    iget v1, v1, LX/8mp;->f:F

    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v2}, Landroid/widget/GridView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    iget-object v1, p0, LX/8mm;->a:LX/8mp;

    iget v1, v1, LX/8mp;->g:F

    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, v2, LX/8mp;->c:Landroid/widget/GridView;

    invoke-virtual {v2}, Landroid/widget/GridView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    .line 1399892
    iget-object v1, p0, LX/8mm;->a:LX/8mp;

    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    iget v2, v2, LX/8mp;->f:F

    iget-object v3, p0, LX/8mm;->a:LX/8mp;

    iget v3, v3, LX/8mp;->g:F

    invoke-static {v1, v2, v3}, LX/8mp;->a(LX/8mp;FF)Lcom/facebook/stickers/model/Sticker;

    move-result-object v1

    .line 1399893
    iget-object v2, p0, LX/8mm;->a:LX/8mp;

    invoke-static {v2, v1}, LX/8mp;->b$redex0(LX/8mp;Lcom/facebook/stickers/model/Sticker;)V

    goto/16 :goto_1

    :cond_9
    move v0, v1

    .line 1399894
    goto/16 :goto_1
.end method
