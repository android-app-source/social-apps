.class public final LX/8zK;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:LX/8zD;

.field public final synthetic b:Landroid/text/SpannableStringBuilder;

.field public final synthetic c:Lcom/facebook/composer/metatext/TagsTextViewContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zD;Landroid/text/SpannableStringBuilder;)V
    .locals 0

    .prologue
    .line 1427152
    iput-object p1, p0, LX/8zK;->c:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iput-object p2, p0, LX/8zK;->a:LX/8zD;

    iput-object p3, p0, LX/8zK;->b:Landroid/text/SpannableStringBuilder;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private a(LX/1ln;)V
    .locals 5

    .prologue
    .line 1427153
    iget-object v0, p0, LX/8zK;->a:LX/8zD;

    iget-object v1, p0, LX/8zK;->c:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v1, v1, Lcom/facebook/composer/metatext/TagsTextViewContainer;->g:LX/8zD;

    invoke-virtual {v0, v1}, LX/8zD;->a(LX/8zD;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8zK;->c:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-boolean v0, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    if-nez v0, :cond_0

    .line 1427154
    iget-object v0, p0, LX/8zK;->c:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v0, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    .line 1427155
    iget-object v1, p0, LX/8zK;->c:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v1, v1, Lcom/facebook/composer/metatext/TagsTextViewContainer;->k:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, LX/8zK;->b:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, LX/8zK;->a:LX/8zD;

    iget-object v3, v3, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, LX/8zK;->c:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v4, v4, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->ascent:F

    invoke-static {v1, v2, p1, v3, v4}, LX/8zF;->a(Landroid/graphics/drawable/Drawable;Landroid/text/SpannableStringBuilder;LX/1ln;Landroid/net/Uri;F)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1427156
    iget-object v0, p0, LX/8zK;->c:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    const/4 v1, 0x1

    .line 1427157
    iput-boolean v1, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->h:Z

    .line 1427158
    :cond_0
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1427159
    check-cast p2, LX/1ln;

    invoke-direct {p0, p2}, LX/8zK;->a(LX/1ln;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1427160
    return-void
.end method
