.class public final LX/8tb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field public final synthetic a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;)V
    .locals 0

    .prologue
    .line 1413556
    iput-object p1, p0, LX/8tb;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 1413557
    iget-object v0, p0, LX/8tb;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iget-object v0, v0, LX/8tJ;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1413558
    iget-object v0, p0, LX/8tb;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    sget-object v1, LX/8tI;->PAUSED:LX/8tI;

    .line 1413559
    invoke-virtual {v0, v1}, LX/8tJ;->a(LX/8tI;)V

    .line 1413560
    iget-object v0, p0, LX/8tb;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iget-object v0, v0, LX/8tJ;->c:LX/8tN;

    if-eqz v0, :cond_0

    .line 1413561
    iget-object v0, p0, LX/8tb;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iget-object v0, v0, LX/8tJ;->c:LX/8tN;

    invoke-virtual {v0}, LX/8tN;->b()V

    .line 1413562
    :cond_0
    return-void
.end method
