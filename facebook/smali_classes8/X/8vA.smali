.class public final enum LX/8vA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8vA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8vA;

.field public static final enum FOLDER:LX/8vA;

.field public static final enum FRIENDLIST:LX/8vA;

.field public static final enum FRIENDS_EXCEPT:LX/8vA;

.field public static final enum FULL_CUSTOM:LX/8vA;

.field public static final enum GROUP_TAG:LX/8vA;

.field public static final enum GROUP_TOPIC:LX/8vA;

.field public static final enum INVITE:LX/8vA;

.field public static final enum LOADING:LX/8vA;

.field public static final enum PRIVACY:LX/8vA;

.field public static final enum PROFILE_CURATION_TAG:LX/8vA;

.field public static final enum SIMPLE:LX/8vA;

.field public static final enum SPECIFIC_FRIENDS:LX/8vA;

.field public static final enum SUGGESTION:LX/8vA;

.field public static final enum TAG_EXPANSION:LX/8vA;

.field public static final enum USER:LX/8vA;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1416694
    new-instance v0, LX/8vA;

    const-string v1, "PRIVACY"

    invoke-direct {v0, v1, v3}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->PRIVACY:LX/8vA;

    .line 1416695
    new-instance v0, LX/8vA;

    const-string v1, "USER"

    invoke-direct {v0, v1, v4}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->USER:LX/8vA;

    .line 1416696
    new-instance v0, LX/8vA;

    const-string v1, "FRIENDLIST"

    invoke-direct {v0, v1, v5}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->FRIENDLIST:LX/8vA;

    .line 1416697
    new-instance v0, LX/8vA;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v6}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->LOADING:LX/8vA;

    .line 1416698
    new-instance v0, LX/8vA;

    const-string v1, "SIMPLE"

    invoke-direct {v0, v1, v7}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->SIMPLE:LX/8vA;

    .line 1416699
    new-instance v0, LX/8vA;

    const-string v1, "SUGGESTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->SUGGESTION:LX/8vA;

    .line 1416700
    new-instance v0, LX/8vA;

    const-string v1, "TAG_EXPANSION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->TAG_EXPANSION:LX/8vA;

    .line 1416701
    new-instance v0, LX/8vA;

    const-string v1, "FULL_CUSTOM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->FULL_CUSTOM:LX/8vA;

    .line 1416702
    new-instance v0, LX/8vA;

    const-string v1, "FRIENDS_EXCEPT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    .line 1416703
    new-instance v0, LX/8vA;

    const-string v1, "SPECIFIC_FRIENDS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    .line 1416704
    new-instance v0, LX/8vA;

    const-string v1, "INVITE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->INVITE:LX/8vA;

    .line 1416705
    new-instance v0, LX/8vA;

    const-string v1, "FOLDER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->FOLDER:LX/8vA;

    .line 1416706
    new-instance v0, LX/8vA;

    const-string v1, "GROUP_TAG"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->GROUP_TAG:LX/8vA;

    .line 1416707
    new-instance v0, LX/8vA;

    const-string v1, "GROUP_TOPIC"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->GROUP_TOPIC:LX/8vA;

    .line 1416708
    new-instance v0, LX/8vA;

    const-string v1, "PROFILE_CURATION_TAG"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/8vA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8vA;->PROFILE_CURATION_TAG:LX/8vA;

    .line 1416709
    const/16 v0, 0xf

    new-array v0, v0, [LX/8vA;

    sget-object v1, LX/8vA;->PRIVACY:LX/8vA;

    aput-object v1, v0, v3

    sget-object v1, LX/8vA;->USER:LX/8vA;

    aput-object v1, v0, v4

    sget-object v1, LX/8vA;->FRIENDLIST:LX/8vA;

    aput-object v1, v0, v5

    sget-object v1, LX/8vA;->LOADING:LX/8vA;

    aput-object v1, v0, v6

    sget-object v1, LX/8vA;->SIMPLE:LX/8vA;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8vA;->SUGGESTION:LX/8vA;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8vA;->TAG_EXPANSION:LX/8vA;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8vA;->FULL_CUSTOM:LX/8vA;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8vA;->INVITE:LX/8vA;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8vA;->FOLDER:LX/8vA;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8vA;->GROUP_TAG:LX/8vA;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8vA;->GROUP_TOPIC:LX/8vA;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8vA;->PROFILE_CURATION_TAG:LX/8vA;

    aput-object v2, v0, v1

    sput-object v0, LX/8vA;->$VALUES:[LX/8vA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1416693
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8vA;
    .locals 1

    .prologue
    .line 1416692
    const-class v0, LX/8vA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8vA;

    return-object v0
.end method

.method public static values()[LX/8vA;
    .locals 1

    .prologue
    .line 1416691
    sget-object v0, LX/8vA;->$VALUES:[LX/8vA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8vA;

    return-object v0
.end method
