.class public LX/9Dl;
.super LX/9Bh;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field public b:LX/9D1;

.field public c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/feedback/ui/NewCommentsLoadingView;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0wW;

.field private e:LX/4mV;

.field private f:LX/0hB;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1456151
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/9Dl;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/0wW;LX/4mV;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456135
    invoke-direct {p0, p1, p2}, LX/9Bh;-><init>(LX/0wW;LX/4mV;)V

    .line 1456136
    iput-object p1, p0, LX/9Dl;->d:LX/0wW;

    .line 1456137
    iput-object p2, p0, LX/9Dl;->e:LX/4mV;

    .line 1456138
    iput-object p3, p0, LX/9Dl;->f:LX/0hB;

    .line 1456139
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 3

    .prologue
    .line 1456140
    invoke-virtual {p0}, LX/9Bh;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1456141
    iget-object v0, p0, LX/9Dl;->c:LX/0zw;

    if-eqz v0, :cond_0

    .line 1456142
    iget-object v0, p0, LX/9Dl;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v2, v1, p1

    iget-object v1, p0, LX/9Dl;->c:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedback/ui/NewCommentsLoadingView;

    invoke-virtual {v1}, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->setTranslationY(F)V

    .line 1456143
    :cond_0
    :goto_0
    return-void

    .line 1456144
    :cond_1
    invoke-super {p0, p1}, LX/9Bh;->a(F)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1456145
    invoke-virtual {p0}, LX/9Bh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/9Bh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1456146
    :cond_0
    const/4 v0, 0x0

    .line 1456147
    :goto_0
    return v0

    .line 1456148
    :cond_1
    iget-object v0, p0, LX/9Dl;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/NewCommentsLoadingView;

    iget-object v1, p0, LX/9Dl;->f:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/NewCommentsLoadingView;->setTranslationY(F)V

    .line 1456149
    invoke-super {p0}, LX/9Bh;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final f()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1456150
    iget-object v0, p0, LX/9Dl;->c:LX/0zw;

    return-object v0
.end method
