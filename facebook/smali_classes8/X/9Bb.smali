.class public final LX/9Bb;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/9Be;

.field private final b:LX/9Bd;


# direct methods
.method public constructor <init>(LX/9Be;LX/9Bd;)V
    .locals 0

    .prologue
    .line 1451860
    iput-object p1, p0, LX/9Bb;->a:LX/9Be;

    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 1451861
    iput-object p2, p0, LX/9Bb;->b:LX/9Bd;

    .line 1451862
    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 6

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1451863
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    iget v0, v0, LX/9BY;->r:I

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 1451864
    iget-object v1, p0, LX/9Bb;->a:LX/9Be;

    invoke-virtual {v1}, LX/9Be;->getHeight()I

    move-result v2

    .line 1451865
    int-to-float v1, v2

    iget-object v3, p0, LX/9Bb;->a:LX/9Be;

    iget v3, v3, LX/9BY;->k:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    sub-float/2addr v1, v3

    .line 1451866
    iget-object v3, p0, LX/9Bb;->a:LX/9Be;

    iget v3, v3, LX/9BY;->s:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    iget-object v3, p0, LX/9Bb;->a:LX/9Be;

    iget v3, v3, LX/9BY;->u:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    iget-object v3, p0, LX/9Bb;->a:LX/9Be;

    iget-object v3, v3, LX/9BY;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    .line 1451867
    iget-object v3, p0, LX/9Bb;->a:LX/9Be;

    invoke-virtual {v3}, LX/9BY;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1451868
    int-to-float v3, v2

    sub-float v1, v3, v1

    .line 1451869
    int-to-float v2, v2

    sub-float v0, v2, v0

    .line 1451870
    :cond_0
    sub-float/2addr v0, v1

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 1451871
    iget-object v1, p0, LX/9Bb;->b:LX/9Bd;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v2, v2

    .line 1451872
    iput v0, v1, LX/9Bd;->e:F

    .line 1451873
    iput v2, v1, LX/9Bd;->f:F

    .line 1451874
    iget-object v0, p0, LX/9Bb;->b:LX/9Bd;

    .line 1451875
    iget-object v1, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1451876
    const-wide v2, 0x406fe00000000000L    # 255.0

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v1, v2

    const/16 v2, 0xff

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1451877
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    invoke-virtual {v0}, LX/9Be;->invalidate()V

    .line 1451878
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1451879
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    iget-object v0, v0, LX/9Be;->H:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1451880
    :goto_0
    iget-object v2, p0, LX/9Bb;->a:LX/9Be;

    .line 1451881
    iget-object v3, v2, LX/9BY;->G:LX/20K;

    move-object v2, v3

    .line 1451882
    if-eqz v2, :cond_3

    iget-object v2, p0, LX/9Bb;->b:LX/9Bd;

    .line 1451883
    iget v3, v2, LX/9BW;->b:I

    move v2, v3

    .line 1451884
    if-ne v2, v0, :cond_3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v2, v3}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1451885
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    invoke-static {v0}, LX/9Be;->g(LX/9Be;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1451886
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    iget-object v0, v0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    iget-object v0, v0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1451887
    iget-object v3, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v3, v3

    .line 1451888
    instance-of v3, v3, LX/9Ug;

    if-eqz v3, :cond_0

    .line 1451889
    iget-object v3, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v3

    .line 1451890
    check-cast v0, LX/9Ug;

    invoke-virtual {v0}, LX/9Ug;->c()V

    .line 1451891
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1451892
    :cond_1
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    iget-object v0, v0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1451893
    :cond_2
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    .line 1451894
    iget-object v1, v0, LX/9BY;->G:LX/20K;

    move-object v0, v1

    .line 1451895
    invoke-virtual {v0}, LX/20K;->c()V

    .line 1451896
    iget-object v0, p0, LX/9Bb;->a:LX/9Be;

    iget-object v0, v0, LX/9Be;->M:LX/9BZ;

    invoke-virtual {v0, v4, v4}, LX/9BZ;->a(LX/9Bd;Landroid/graphics/Path;)V

    .line 1451897
    :cond_3
    return-void
.end method
