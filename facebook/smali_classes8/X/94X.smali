.class public final LX/94X;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public final c:LX/63W;

.field public final d:LX/94c;


# direct methods
.method public constructor <init>(LX/94Y;)V
    .locals 1

    .prologue
    .line 1435408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435409
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435410
    iget-object v0, p1, LX/94Y;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/94X;->a:Ljava/lang/String;

    .line 1435411
    iget-object v0, p1, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    iput-object v0, p0, LX/94X;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435412
    iget-object v0, p1, LX/94Y;->c:LX/63W;

    iput-object v0, p0, LX/94X;->c:LX/63W;

    .line 1435413
    iget-object v0, p1, LX/94Y;->d:LX/94c;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/94c;

    iput-object v0, p0, LX/94X;->d:LX/94c;

    .line 1435414
    return-void
.end method


# virtual methods
.method public final a()LX/94Y;
    .locals 2

    .prologue
    .line 1435415
    new-instance v0, LX/94Y;

    invoke-direct {v0, p0}, LX/94Y;-><init>(LX/94X;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1435416
    if-ne p0, p1, :cond_1

    .line 1435417
    :cond_0
    :goto_0
    return v0

    .line 1435418
    :cond_1
    instance-of v2, p1, LX/94X;

    if-nez v2, :cond_2

    move v0, v1

    .line 1435419
    goto :goto_0

    .line 1435420
    :cond_2
    check-cast p1, LX/94X;

    .line 1435421
    iget-object v2, p0, LX/94X;->a:Ljava/lang/String;

    iget-object v3, p1, LX/94X;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/94X;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    iget-object v3, p1, LX/94X;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/94X;->c:LX/63W;

    iget-object v3, p1, LX/94X;->c:LX/63W;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/94X;->d:LX/94c;

    iget-object v3, p1, LX/94X;->d:LX/94c;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1435422
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/94X;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/94X;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/94X;->c:LX/63W;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/94X;->d:LX/94c;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
