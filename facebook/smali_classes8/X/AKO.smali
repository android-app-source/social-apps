.class public LX/AKO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/AKO;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/audience/snacks/optimistic/SnacksUploadStoryOptimisticStore$NotifyChange;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1663215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663216
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/AKO;->b:Ljava/util/List;

    .line 1663217
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/AKO;->c:Ljava/util/List;

    .line 1663218
    iput-object p1, p0, LX/AKO;->a:LX/0Or;

    .line 1663219
    return-void
.end method

.method public static a(LX/0QB;)LX/AKO;
    .locals 4

    .prologue
    .line 1663202
    sget-object v0, LX/AKO;->d:LX/AKO;

    if-nez v0, :cond_1

    .line 1663203
    const-class v1, LX/AKO;

    monitor-enter v1

    .line 1663204
    :try_start_0
    sget-object v0, LX/AKO;->d:LX/AKO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1663205
    if-eqz v2, :cond_0

    .line 1663206
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1663207
    new-instance v3, LX/AKO;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/AKO;-><init>(LX/0Or;)V

    .line 1663208
    move-object v0, v3

    .line 1663209
    sput-object v0, LX/AKO;->d:LX/AKO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1663210
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1663211
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1663212
    :cond_1
    sget-object v0, LX/AKO;->d:LX/AKO;

    return-object v0

    .line 1663213
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1663214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized a(LX/AKO;Ljava/lang/String;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AKO;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1663195
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1663196
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1663197
    iget-object v2, v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1663198
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1663199
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1663200
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1663201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/AKO;Ljava/lang/String;Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
    .locals 1

    .prologue
    .line 1663190
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->a(Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)LX/AK6;

    move-result-object v0

    .line 1663191
    iput-object p1, v0, LX/AK6;->f:Ljava/lang/String;

    .line 1663192
    move-object v0, v0

    .line 1663193
    invoke-virtual {v0}, LX/AK6;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/AKO;Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)V
    .locals 1

    .prologue
    .line 1663187
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AKO;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1663188
    monitor-exit p0

    return-void

    .line 1663189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/AKO;Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
    .locals 5

    .prologue
    .line 1663156
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->newBuilder()LX/AK6;

    move-result-object v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1663157
    iput-object v1, v0, LX/AK6;->a:Ljava/lang/String;

    .line 1663158
    move-object v0, v0

    .line 1663159
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1663160
    iput-object v1, v0, LX/AK6;->d:Ljava/lang/String;

    .line 1663161
    move-object v1, v0

    .line 1663162
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getMediaType()LX/7gi;

    move-result-object v0

    sget-object v2, LX/7gi;->VIDEO:LX/7gi;

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1663163
    :goto_0
    iput-object v0, v1, LX/AK6;->i:Ljava/lang/String;

    .line 1663164
    move-object v0, v1

    .line 1663165
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1663166
    iput-object v1, v0, LX/AK6;->e:Ljava/lang/String;

    .line 1663167
    move-object v0, v0

    .line 1663168
    const/4 v1, 0x0

    .line 1663169
    iput v1, v0, LX/AK6;->g:I

    .line 1663170
    move-object v0, v0

    .line 1663171
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v2

    .line 1663172
    iput-wide v2, v0, LX/AK6;->h:J

    .line 1663173
    move-object v0, v0

    .line 1663174
    const/4 v1, 0x1

    .line 1663175
    iput-boolean v1, v0, LX/AK6;->b:Z

    .line 1663176
    move-object v0, v0

    .line 1663177
    iget-object v1, p0, LX/AKO;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1663178
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v2

    .line 1663179
    iget-object v3, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1663180
    invoke-virtual {v2, v3}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v1

    move-object v1, v1

    .line 1663181
    iput-object v1, v0, LX/AK6;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1663182
    move-object v0, v0

    .line 1663183
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1663184
    iput-object v1, v0, LX/AK6;->f:Ljava/lang/String;

    .line 1663185
    move-object v0, v0

    .line 1663186
    invoke-virtual {v0}, LX/AK6;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/AKO;)V
    .locals 5

    .prologue
    .line 1663151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AKO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJw;

    .line 1663152
    iget-object v2, v0, LX/AJw;->a:LX/AK0;

    const/4 v3, 0x1

    sget-object v4, LX/AJz;->OPTIMISTIC_UPDATE:LX/AJz;

    invoke-virtual {v2, v3, v4}, LX/AK0;->a(ZLX/AJz;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1663153
    goto :goto_0

    .line 1663154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1663155
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1663125
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1663126
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    move-object v0, v3

    .line 1663127
    invoke-static {p0, v0}, LX/AKO;->a(LX/AKO;Ljava/lang/String;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1663128
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1663129
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1663130
    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1663131
    iget-object v1, p0, LX/AKO;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1663132
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1663133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/AJw;)V
    .locals 1

    .prologue
    .line 1663147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AKO;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1663148
    iget-object v0, p0, LX/AKO;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1663149
    :cond_0
    monitor-exit p0

    return-void

    .line 1663150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/audience/model/UploadShot;)V
    .locals 2

    .prologue
    .line 1663140
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/AKO;->b(LX/AKO;Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v0

    .line 1663141
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1663142
    invoke-static {p0, v1}, LX/AKO;->a(LX/AKO;Ljava/lang/String;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1663143
    iget-object v1, p0, LX/AKO;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1663144
    invoke-static {p0}, LX/AKO;->b(LX/AKO;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1663145
    monitor-exit p0

    return-void

    .line 1663146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1663134
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p2}, LX/AKO;->a(LX/AKO;Ljava/lang/String;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v0

    .line 1663135
    if-eqz v0, :cond_0

    .line 1663136
    invoke-static {p0, p1, v0}, LX/AKO;->a(LX/AKO;Ljava/lang/String;Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v0

    invoke-static {p0, v0}, LX/AKO;->a(LX/AKO;Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)V

    .line 1663137
    :cond_0
    invoke-static {p0}, LX/AKO;->b(LX/AKO;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1663138
    monitor-exit p0

    return-void

    .line 1663139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
