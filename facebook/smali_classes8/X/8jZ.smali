.class public final LX/8jZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/facebook/stickers/data/StickerAssetDownloader;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/data/StickerAssetDownloader;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1392617
    iput-object p1, p0, LX/8jZ;->b:Lcom/facebook/stickers/data/StickerAssetDownloader;

    iput-object p2, p0, LX/8jZ;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1392618
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/8jZ;->a:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1392619
    :try_start_0
    iget-object v0, p0, LX/8jZ;->b:Lcom/facebook/stickers/data/StickerAssetDownloader;

    iget-object v0, v0, Lcom/facebook/stickers/data/StickerAssetDownloader;->d:LX/3ej;

    invoke-virtual {v0, p1, v1}, LX/3ej;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1392620
    const/4 v0, 0x0

    invoke-static {v1, v0}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 1392621
    const/4 v0, 0x0

    return-object v0

    .line 1392622
    :catchall_0
    move-exception v0

    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method
