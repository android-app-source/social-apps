.class public LX/AEM;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static u:LX/0Xm;


# instance fields
.field public final b:LX/1PJ;

.field public final c:Landroid/view/View$OnClickListener;

.field private final d:LX/1Sa;

.field private final e:LX/1Sj;

.field private final f:LX/03V;

.field public final g:Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;

.field private final h:LX/1Nt;

.field private final i:LX/0bH;

.field private final j:LX/2fk;

.field private final k:LX/2fh;

.field private final l:LX/14w;

.field private final m:LX/2yS;

.field private final n:LX/1Sl;

.field private final o:LX/0tX;

.field private final p:Ljava/util/concurrent/ExecutorService;

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/2fm;

.field private final s:LX/2yT;

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1646473
    new-instance v0, LX/AEJ;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/AEJ;-><init>(I)V

    sput-object v0, LX/AEM;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(LX/1PJ;LX/1Sa;LX/1Sj;LX/03V;Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;LX/2fh;LX/0bH;LX/2fk;LX/14w;LX/2yS;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;LX/2yT;LX/0Ot;)V
    .locals 3
    .param p13    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1PJ;",
            "LX/1Sa;",
            "LX/1Sj;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;",
            "LX/2fh;",
            "LX/0bH;",
            "LX/2fk;",
            "LX/14w;",
            "LX/2yS;",
            "LX/1Sl;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "LX/2fm;",
            "LX/2yT;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646452
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646453
    iput-object p1, p0, LX/AEM;->b:LX/1PJ;

    .line 1646454
    new-instance v1, LX/AEK;

    invoke-direct {v1, p0}, LX/AEK;-><init>(LX/AEM;)V

    iput-object v1, p0, LX/AEM;->c:Landroid/view/View$OnClickListener;

    .line 1646455
    iput-object p2, p0, LX/AEM;->d:LX/1Sa;

    .line 1646456
    iput-object p3, p0, LX/AEM;->e:LX/1Sj;

    .line 1646457
    iput-object p4, p0, LX/AEM;->f:LX/03V;

    .line 1646458
    iput-object p5, p0, LX/AEM;->g:Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;

    .line 1646459
    new-instance v1, Lcom/facebook/attachments/angora/actionbutton/CollectionSaveActionButton$CollectionsSaveActionButtonPartDefinition;

    invoke-direct {v1, p0}, Lcom/facebook/attachments/angora/actionbutton/CollectionSaveActionButton$CollectionsSaveActionButtonPartDefinition;-><init>(LX/AEM;)V

    iput-object v1, p0, LX/AEM;->h:LX/1Nt;

    .line 1646460
    iput-object p6, p0, LX/AEM;->k:LX/2fh;

    .line 1646461
    iput-object p7, p0, LX/AEM;->i:LX/0bH;

    .line 1646462
    iput-object p8, p0, LX/AEM;->j:LX/2fk;

    .line 1646463
    iput-object p9, p0, LX/AEM;->l:LX/14w;

    .line 1646464
    iput-object p10, p0, LX/AEM;->m:LX/2yS;

    .line 1646465
    iput-object p11, p0, LX/AEM;->n:LX/1Sl;

    .line 1646466
    iput-object p12, p0, LX/AEM;->o:LX/0tX;

    .line 1646467
    move-object/from16 v0, p13

    iput-object v0, p0, LX/AEM;->p:Ljava/util/concurrent/ExecutorService;

    .line 1646468
    move-object/from16 v0, p14

    iput-object v0, p0, LX/AEM;->q:LX/0Ot;

    .line 1646469
    move-object/from16 v0, p15

    iput-object v0, p0, LX/AEM;->r:LX/2fm;

    .line 1646470
    move-object/from16 v0, p16

    iput-object v0, p0, LX/AEM;->s:LX/2yT;

    .line 1646471
    move-object/from16 v0, p17

    iput-object v0, p0, LX/AEM;->t:LX/0Ot;

    .line 1646472
    return-void
.end method

.method private a(LX/1De;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 1646443
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/AEM;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v17

    .line 1646444
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    move v3, v2

    .line 1646445
    :goto_0
    if-eqz v3, :cond_1

    const v2, 0x7f02178c

    move/from16 v25, v2

    .line 1646446
    :goto_1
    if-eqz v3, :cond_2

    const v2, 0x7f0810f8

    move/from16 v24, v2

    .line 1646447
    :goto_2
    new-instance v2, LX/AEL;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/AEM;->d:LX/1Sa;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/AEM;->e:LX/1Sj;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/AEM;->f:LX/03V;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/AEM;->k:LX/2fh;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/AEM;->i:LX/0bH;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/AEM;->j:LX/2fk;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/AEM;->l:LX/14w;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/AEM;->n:LX/1Sl;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/AEM;->o:LX/0tX;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/AEM;->p:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/AEM;->q:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/AEM;->r:LX/2fm;

    move-object/from16 v16, v0

    const-string v18, "native_story"

    invoke-static/range {p3 .. p3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, LX/AEM;->c:Landroid/view/View$OnClickListener;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/AEM;->t:LX/0Ot;

    move-object/from16 v21, v0

    move-object/from16 v3, p0

    move-object/from16 v11, p1

    move-object/from16 v22, p2

    move-object/from16 v23, p3

    invoke-direct/range {v2 .. v23}, LX/AEL;-><init>(LX/AEM;LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;LX/0Ot;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1646448
    move-object/from16 v0, p0

    iget-object v3, v0, LX/AEM;->s:LX/2yT;

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, LX/AEM;->m:LX/2yS;

    move/from16 v0, p4

    invoke-virtual {v3, v0, v4, v5}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/AE0;->a(Z)LX/AE0;

    move-result-object v3

    move/from16 v0, v25

    invoke-virtual {v3, v0}, LX/AE0;->a(I)LX/AE0;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/AE0;->b(I)LX/AE0;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/AE0;->b(Ljava/lang/CharSequence;)LX/AE0;

    move-result-object v3

    sget-object v4, LX/AEM;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v4}, LX/AE0;->a(Landroid/util/SparseArray;)LX/AE0;

    move-result-object v3

    iget-object v2, v2, LX/2fo;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v2}, LX/AE0;->a(Landroid/view/View$OnClickListener;)LX/AE0;

    move-result-object v2

    return-object v2

    .line 1646449
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_0

    .line 1646450
    :cond_1
    const v2, 0x7f02178b

    move/from16 v25, v2

    goto/16 :goto_1

    .line 1646451
    :cond_2
    const v2, 0x7f0810f7

    move/from16 v24, v2

    goto/16 :goto_2
.end method

.method public static a(LX/0QB;)LX/AEM;
    .locals 3

    .prologue
    .line 1646435
    const-class v1, LX/AEM;

    monitor-enter v1

    .line 1646436
    :try_start_0
    sget-object v0, LX/AEM;->u:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646437
    sput-object v2, LX/AEM;->u:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646438
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646439
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/AEM;->b(LX/0QB;)LX/AEM;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646440
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646441
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/AEM;
    .locals 20

    .prologue
    .line 1646433
    new-instance v2, LX/AEM;

    invoke-static/range {p0 .. p0}, LX/1PJ;->a(LX/0QB;)LX/1PJ;

    move-result-object v3

    check-cast v3, LX/1PJ;

    invoke-static/range {p0 .. p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v4

    check-cast v4, LX/1Sa;

    invoke-static/range {p0 .. p0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v5

    check-cast v5, LX/1Sj;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {p0 .. p0}, Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;

    invoke-static/range {p0 .. p0}, LX/2fh;->a(LX/0QB;)LX/2fh;

    move-result-object v8

    check-cast v8, LX/2fh;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/2fk;->a(LX/0QB;)LX/2fk;

    move-result-object v10

    check-cast v10, LX/2fk;

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v11

    check-cast v11, LX/14w;

    invoke-static/range {p0 .. p0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v12

    check-cast v12, LX/2yS;

    invoke-static/range {p0 .. p0}, LX/1Sl;->a(LX/0QB;)LX/1Sl;

    move-result-object v13

    check-cast v13, LX/1Sl;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v14

    check-cast v14, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v15

    check-cast v15, Ljava/util/concurrent/ExecutorService;

    const/16 v16, 0x3279

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const-class v17, LX/2fm;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/2fm;

    const-class v18, LX/2yT;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/2yT;

    const/16 v19, 0x327a

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-direct/range {v2 .. v19}, LX/AEM;-><init>(LX/1PJ;LX/1Sa;LX/1Sj;LX/03V;Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;LX/2fh;LX/0bH;LX/2fk;LX/14w;LX/2yS;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;LX/2yT;LX/0Ot;)V

    .line 1646434
    return-object v2
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1646431
    const v0, -0x3625f733

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1646432
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646430
    iget-object v0, p0, LX/AEM;->h:LX/1Nt;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 1

    .prologue
    .line 1646429
    check-cast p2, LX/1Pq;

    invoke-direct {p0, p1, p2, p3, p4}, LX/AEM;->a(LX/1De;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;

    move-result-object v0

    return-object v0
.end method
