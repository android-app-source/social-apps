.class public final LX/A4V;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1619276
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1619277
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1619278
    :goto_0
    return v1

    .line 1619279
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1619280
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1619281
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1619282
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1619283
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1619284
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1619285
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 1619286
    :cond_3
    const-string v4, "profile_picture"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1619287
    invoke-static {p0, p1}, LX/A4U;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1619288
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1619289
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1619290
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1619291
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1619292
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1619293
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1619294
    if-eqz v0, :cond_0

    .line 1619295
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619296
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1619297
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1619298
    if-eqz v0, :cond_1

    .line 1619299
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1619300
    invoke-static {p0, v0, p2}, LX/A4U;->a(LX/15i;ILX/0nX;)V

    .line 1619301
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1619302
    return-void
.end method
