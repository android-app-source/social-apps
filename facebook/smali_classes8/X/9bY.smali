.class public LX/9bY;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/9bZ;",
        "LX/9bX;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9bY;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1515119
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1515120
    return-void
.end method

.method public static a(LX/0QB;)LX/9bY;
    .locals 3

    .prologue
    .line 1515121
    sget-object v0, LX/9bY;->a:LX/9bY;

    if-nez v0, :cond_1

    .line 1515122
    const-class v1, LX/9bY;

    monitor-enter v1

    .line 1515123
    :try_start_0
    sget-object v0, LX/9bY;->a:LX/9bY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1515124
    if-eqz v2, :cond_0

    .line 1515125
    :try_start_1
    new-instance v0, LX/9bY;

    invoke-direct {v0}, LX/9bY;-><init>()V

    .line 1515126
    move-object v0, v0

    .line 1515127
    sput-object v0, LX/9bY;->a:LX/9bY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1515128
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1515129
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1515130
    :cond_1
    sget-object v0, LX/9bY;->a:LX/9bY;

    return-object v0

    .line 1515131
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1515132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
