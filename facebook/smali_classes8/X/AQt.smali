.class public final LX/AQt;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:LX/AQu;


# direct methods
.method public constructor <init>(LX/AQu;)V
    .locals 0

    .prologue
    .line 1671927
    iput-object p1, p0, LX/AQt;->a:LX/AQu;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1671928
    iget-object v0, p0, LX/AQt;->a:LX/AQu;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1671929
    iput-object v1, v0, LX/AQu;->f:Ljava/lang/String;

    .line 1671930
    new-instance v4, Landroid/widget/Filter$FilterResults;

    invoke-direct {v4}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1671931
    iget-object v0, p0, LX/AQt;->a:LX/AQu;

    iget-object v0, v0, LX/AQu;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1671932
    iput v2, v4, Landroid/widget/Filter$FilterResults;->count:I

    .line 1671933
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v4, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1671934
    :goto_0
    return-object v4

    .line 1671935
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1671936
    iget-object v0, p0, LX/AQt;->a:LX/AQu;

    iget-object v0, v0, LX/AQu;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_4

    iget-object v0, p0, LX/AQt;->a:LX/AQu;

    iget-object v0, v0, LX/AQu;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1671937
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1671938
    const-string v1, "\\s"

    invoke-virtual {v7, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    move v1, v2

    :goto_2
    if-ge v1, v9, :cond_2

    aget-object v10, v8, v1

    .line 1671939
    iget-object v11, p0, LX/AQt;->a:LX/AQu;

    iget-object v11, v11, LX/AQu;->f:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, LX/AQt;->a:LX/AQu;

    iget-object v10, v10, LX/AQu;->f:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1671940
    :cond_1
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1671941
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1671942
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1671943
    :cond_4
    new-instance v0, LX/7lF;

    invoke-direct {v0}, LX/7lF;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1671944
    iput-object v1, v0, LX/7lF;->a:Ljava/lang/String;

    .line 1671945
    move-object v0, v0

    .line 1671946
    iget-object v1, p0, LX/AQt;->a:LX/AQu;

    iget-object v1, v1, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 1671947
    iput-object v1, v0, LX/7lF;->d:Ljava/lang/String;

    .line 1671948
    move-object v0, v0

    .line 1671949
    iget-object v1, p0, LX/AQt;->a:LX/AQu;

    iget-object v1, v1, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 1671950
    iput-object v1, v0, LX/7lF;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1671951
    move-object v0, v0

    .line 1671952
    iget-object v1, p0, LX/AQt;->a:LX/AQu;

    iget-object v1, v1, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->m()Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v1

    .line 1671953
    iput-object v1, v0, LX/7lF;->e:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1671954
    move-object v0, v0

    .line 1671955
    invoke-virtual {v0}, LX/7lF;->a()Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1671956
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v4, Landroid/widget/Filter$FilterResults;->count:I

    .line 1671957
    iput-object v5, v4, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 1671958
    if-eqz p2, :cond_0

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 1671959
    :cond_0
    iget-object v0, p0, LX/AQt;->a:LX/AQu;

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1671960
    iput-object v1, v0, LX/AQu;->g:Ljava/util/List;

    .line 1671961
    :goto_0
    return-void

    .line 1671962
    :cond_1
    iget-object v1, p0, LX/AQt;->a:LX/AQu;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1671963
    iput-object v0, v1, LX/AQu;->g:Ljava/util/List;

    .line 1671964
    goto :goto_0
.end method
