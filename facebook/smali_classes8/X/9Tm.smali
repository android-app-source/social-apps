.class public final enum LX/9Tm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Tm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Tm;

.field public static final enum ADD_CONTACTPOINT_ATTEMPT:LX/9Tm;

.field public static final enum ADD_CONTACTPOINT_COUNTRY_SELECTED:LX/9Tm;

.field public static final enum ADD_CONTACTPOINT_FAILURE:LX/9Tm;

.field public static final enum ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;

.field public static final enum ADD_CONTACTPOINT_SUCCESS:LX/9Tm;

.field public static final enum FACEWEB_ADD_CONTACTPOINT_CONFIRMED:LX/9Tm;

.field public static final enum FACEWEB_ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1496739
    new-instance v0, LX/9Tm;

    const-string v1, "ADD_CONTACTPOINT_FLOW_ENTER"

    const-string v2, "growth_add_contactpoint_flow_enter"

    invoke-direct {v0, v1, v4, v2}, LX/9Tm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tm;->ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;

    .line 1496740
    new-instance v0, LX/9Tm;

    const-string v1, "ADD_CONTACTPOINT_ATTEMPT"

    const-string v2, "growth_add_contactpoint_attempt"

    invoke-direct {v0, v1, v5, v2}, LX/9Tm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tm;->ADD_CONTACTPOINT_ATTEMPT:LX/9Tm;

    .line 1496741
    new-instance v0, LX/9Tm;

    const-string v1, "ADD_CONTACTPOINT_FAILURE"

    const-string v2, "growth_add_contactpoint_failure"

    invoke-direct {v0, v1, v6, v2}, LX/9Tm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tm;->ADD_CONTACTPOINT_FAILURE:LX/9Tm;

    .line 1496742
    new-instance v0, LX/9Tm;

    const-string v1, "ADD_CONTACTPOINT_SUCCESS"

    const-string v2, "growth_add_contactpoint_success"

    invoke-direct {v0, v1, v7, v2}, LX/9Tm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tm;->ADD_CONTACTPOINT_SUCCESS:LX/9Tm;

    .line 1496743
    new-instance v0, LX/9Tm;

    const-string v1, "ADD_CONTACTPOINT_COUNTRY_SELECTED"

    const-string v2, "growth_add_contactpoint_country_selected"

    invoke-direct {v0, v1, v8, v2}, LX/9Tm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tm;->ADD_CONTACTPOINT_COUNTRY_SELECTED:LX/9Tm;

    .line 1496744
    new-instance v0, LX/9Tm;

    const-string v1, "FACEWEB_ADD_CONTACTPOINT_FLOW_ENTER"

    const/4 v2, 0x5

    const-string v3, "growth_faceweb_add_contactpoint_flow_enter"

    invoke-direct {v0, v1, v2, v3}, LX/9Tm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tm;->FACEWEB_ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;

    .line 1496745
    new-instance v0, LX/9Tm;

    const-string v1, "FACEWEB_ADD_CONTACTPOINT_CONFIRMED"

    const/4 v2, 0x6

    const-string v3, "growth_faceweb_add_contactpoint_confirmed"

    invoke-direct {v0, v1, v2, v3}, LX/9Tm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tm;->FACEWEB_ADD_CONTACTPOINT_CONFIRMED:LX/9Tm;

    .line 1496746
    const/4 v0, 0x7

    new-array v0, v0, [LX/9Tm;

    sget-object v1, LX/9Tm;->ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;

    aput-object v1, v0, v4

    sget-object v1, LX/9Tm;->ADD_CONTACTPOINT_ATTEMPT:LX/9Tm;

    aput-object v1, v0, v5

    sget-object v1, LX/9Tm;->ADD_CONTACTPOINT_FAILURE:LX/9Tm;

    aput-object v1, v0, v6

    sget-object v1, LX/9Tm;->ADD_CONTACTPOINT_SUCCESS:LX/9Tm;

    aput-object v1, v0, v7

    sget-object v1, LX/9Tm;->ADD_CONTACTPOINT_COUNTRY_SELECTED:LX/9Tm;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9Tm;->FACEWEB_ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9Tm;->FACEWEB_ADD_CONTACTPOINT_CONFIRMED:LX/9Tm;

    aput-object v2, v0, v1

    sput-object v0, LX/9Tm;->$VALUES:[LX/9Tm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1496747
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1496748
    iput-object p3, p0, LX/9Tm;->mAnalyticsName:Ljava/lang/String;

    .line 1496749
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Tm;
    .locals 1

    .prologue
    .line 1496750
    const-class v0, LX/9Tm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Tm;

    return-object v0
.end method

.method public static values()[LX/9Tm;
    .locals 1

    .prologue
    .line 1496751
    sget-object v0, LX/9Tm;->$VALUES:[LX/9Tm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Tm;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1496752
    iget-object v0, p0, LX/9Tm;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1496753
    iget-object v0, p0, LX/9Tm;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
