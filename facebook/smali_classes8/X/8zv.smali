.class public final LX/8zv;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8zw;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/8zj;

.field public b:Ljava/lang/String;

.field public c:LX/8zx;

.field public d:Landroid/view/View$OnClickListener;

.field public e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public f:LX/903;

.field public g:LX/8zz;

.field public final synthetic h:LX/8zw;


# direct methods
.method public constructor <init>(LX/8zw;)V
    .locals 1

    .prologue
    .line 1428261
    iput-object p1, p0, LX/8zv;->h:LX/8zw;

    .line 1428262
    move-object v0, p1

    .line 1428263
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1428264
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1428265
    const-string v0, "MinutiaeVerbsListComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1428266
    if-ne p0, p1, :cond_1

    .line 1428267
    :cond_0
    :goto_0
    return v0

    .line 1428268
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1428269
    goto :goto_0

    .line 1428270
    :cond_3
    check-cast p1, LX/8zv;

    .line 1428271
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1428272
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1428273
    if-eq v2, v3, :cond_0

    .line 1428274
    iget-object v2, p0, LX/8zv;->a:LX/8zj;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8zv;->a:LX/8zj;

    iget-object v3, p1, LX/8zv;->a:LX/8zj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1428275
    goto :goto_0

    .line 1428276
    :cond_5
    iget-object v2, p1, LX/8zv;->a:LX/8zj;

    if-nez v2, :cond_4

    .line 1428277
    :cond_6
    iget-object v2, p0, LX/8zv;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/8zv;->b:Ljava/lang/String;

    iget-object v3, p1, LX/8zv;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1428278
    goto :goto_0

    .line 1428279
    :cond_8
    iget-object v2, p1, LX/8zv;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1428280
    :cond_9
    iget-object v2, p0, LX/8zv;->c:LX/8zx;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/8zv;->c:LX/8zx;

    iget-object v3, p1, LX/8zv;->c:LX/8zx;

    invoke-virtual {v2, v3}, LX/8zx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1428281
    goto :goto_0

    .line 1428282
    :cond_b
    iget-object v2, p1, LX/8zv;->c:LX/8zx;

    if-nez v2, :cond_a

    .line 1428283
    :cond_c
    iget-object v2, p0, LX/8zv;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/8zv;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/8zv;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1428284
    goto :goto_0

    .line 1428285
    :cond_e
    iget-object v2, p1, LX/8zv;->d:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_d

    .line 1428286
    :cond_f
    iget-object v2, p0, LX/8zv;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/8zv;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, LX/8zv;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1428287
    goto :goto_0

    .line 1428288
    :cond_11
    iget-object v2, p1, LX/8zv;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v2, :cond_10

    .line 1428289
    :cond_12
    iget-object v2, p0, LX/8zv;->f:LX/903;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/8zv;->f:LX/903;

    iget-object v3, p1, LX/8zv;->f:LX/903;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1428290
    goto/16 :goto_0

    .line 1428291
    :cond_14
    iget-object v2, p1, LX/8zv;->f:LX/903;

    if-nez v2, :cond_13

    .line 1428292
    :cond_15
    iget-object v2, p0, LX/8zv;->g:LX/8zz;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/8zv;->g:LX/8zz;

    iget-object v3, p1, LX/8zv;->g:LX/8zz;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1428293
    goto/16 :goto_0

    .line 1428294
    :cond_16
    iget-object v2, p1, LX/8zv;->g:LX/8zz;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
