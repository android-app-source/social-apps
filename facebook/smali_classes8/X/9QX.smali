.class public final LX/9QX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1487432
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1487433
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1487434
    :goto_0
    return v1

    .line 1487435
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1487436
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1487437
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1487438
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1487439
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1487440
    const-string v4, "ranges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1487441
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1487442
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1487443
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1487444
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1487445
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_c

    .line 1487446
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1487447
    :goto_3
    move v3, v4

    .line 1487448
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1487449
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1487450
    goto :goto_1

    .line 1487451
    :cond_3
    const-string v4, "text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1487452
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1487453
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1487454
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1487455
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1487456
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1487457
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_9

    .line 1487458
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1487459
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1487460
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_6

    if-eqz v9, :cond_6

    .line 1487461
    const-string v10, "length"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1487462
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v8, v6

    move v6, v5

    goto :goto_4

    .line 1487463
    :cond_7
    const-string v10, "offset"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1487464
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v5

    goto :goto_4

    .line 1487465
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1487466
    :cond_9
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1487467
    if-eqz v6, :cond_a

    .line 1487468
    invoke-virtual {p1, v4, v8, v4}, LX/186;->a(III)V

    .line 1487469
    :cond_a
    if-eqz v3, :cond_b

    .line 1487470
    invoke-virtual {p1, v5, v7, v4}, LX/186;->a(III)V

    .line 1487471
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_c
    move v3, v4

    move v6, v4

    move v7, v4

    move v8, v4

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1487472
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487473
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487474
    if-eqz v0, :cond_3

    .line 1487475
    const-string v1, "ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487476
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1487477
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1487478
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x0

    .line 1487479
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487480
    invoke-virtual {p0, v2, p3, p3}, LX/15i;->a(III)I

    move-result v3

    .line 1487481
    if-eqz v3, :cond_0

    .line 1487482
    const-string v4, "length"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487483
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1487484
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, p3}, LX/15i;->a(III)I

    move-result v3

    .line 1487485
    if-eqz v3, :cond_1

    .line 1487486
    const-string v4, "offset"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487487
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1487488
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487489
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1487490
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1487491
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487492
    if-eqz v0, :cond_4

    .line 1487493
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487494
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487495
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487496
    return-void
.end method
