.class public LX/AM1;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""

# interfaces
.implements LX/AL6;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final b:Landroid/view/View$OnTouchListener;

.field private final d:LX/AL7;

.field public e:Z

.field private f:LX/AMA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1665742
    const-class v0, LX/AM1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AM1;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665740
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AM1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665741
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665738
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AM1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665739
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1665727
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665728
    iput-boolean v1, p0, LX/AM1;->e:Z

    .line 1665729
    new-instance v0, LX/AM9;

    invoke-direct {v0, p0}, LX/AM9;-><init>(LX/AM1;)V

    iput-object v0, p0, LX/AM1;->b:Landroid/view/View$OnTouchListener;

    .line 1665730
    new-instance v0, LX/AL7;

    invoke-direct {v0}, LX/AL7;-><init>()V

    iput-object v0, p0, LX/AM1;->d:LX/AL7;

    .line 1665731
    iget-object v0, p0, LX/AM1;->d:LX/AL7;

    .line 1665732
    iput-object p0, v0, LX/AL7;->g:LX/AL6;

    .line 1665733
    invoke-virtual {p0, v1}, LX/AM1;->setFocusable(Z)V

    .line 1665734
    invoke-virtual {p0, v1}, LX/AM1;->setEnabled(Z)V

    .line 1665735
    invoke-virtual {p0, v1}, LX/AM1;->setFocusableInTouchMode(Z)V

    .line 1665736
    iget-object v0, p0, LX/AM1;->b:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, LX/AM1;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1665737
    return-void
.end method


# virtual methods
.method public final I_(I)V
    .locals 1

    .prologue
    .line 1665721
    if-nez p1, :cond_1

    .line 1665722
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AM1;->e:Z

    .line 1665723
    invoke-virtual {p0}, LX/AM1;->clearFocus()V

    .line 1665724
    iget-object v0, p0, LX/AM1;->f:LX/AMA;

    if-eqz v0, :cond_0

    .line 1665725
    :cond_0
    :goto_0
    return-void

    .line 1665726
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AM1;->e:Z

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1665743
    invoke-virtual {p0, v0}, LX/AM1;->setFocusable(Z)V

    .line 1665744
    invoke-virtual {p0, v0}, LX/AM1;->setEnabled(Z)V

    .line 1665745
    invoke-virtual {p0, v0}, LX/AM1;->setFocusableInTouchMode(Z)V

    .line 1665746
    invoke-virtual {p0}, LX/AM1;->requestFocus()Z

    .line 1665747
    invoke-virtual {p0}, LX/AM1;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 1665748
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1665715
    invoke-virtual {p0}, LX/AM1;->clearFocus()V

    .line 1665716
    invoke-virtual {p0}, LX/AM1;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1665717
    invoke-virtual {p0, v1}, LX/AM1;->setFocusable(Z)V

    .line 1665718
    invoke-virtual {p0, v1}, LX/AM1;->setEnabled(Z)V

    .line 1665719
    invoke-virtual {p0, v1}, LX/AM1;->setFocusableInTouchMode(Z)V

    .line 1665720
    return-void
.end method

.method public final b(I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1665713
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/AM1;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1665714
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x901b4c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1665710
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->onAttachedToWindow()V

    .line 1665711
    iget-object v2, p0, LX/AM1;->d:LX/AL7;

    invoke-virtual {p0}, LX/AM1;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v2, v0}, LX/AL7;->a(Landroid/app/Activity;)V

    .line 1665712
    const/16 v0, 0x2d

    const v2, 0x6a7318f8

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x65460d22

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1665707
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->onDetachedFromWindow()V

    .line 1665708
    iget-object v1, p0, LX/AM1;->d:LX/AL7;

    invoke-virtual {v1}, LX/AL7;->a()V

    .line 1665709
    const/16 v1, 0x2d

    const v2, -0x4c1be619

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setEventListener(LX/AMA;)V
    .locals 0

    .prologue
    .line 1665705
    iput-object p1, p0, LX/AM1;->f:LX/AMA;

    .line 1665706
    return-void
.end method
