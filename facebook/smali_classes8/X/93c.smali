.class public final LX/93c;
.super LX/93Q;
.source ""


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:LX/0tX;

.field public final d:Landroid/content/res/Resources;

.field public final e:LX/2rX;

.field private final f:Z


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;Ljava/lang/Long;Ljava/lang/String;LX/2rX;LX/0tX;Landroid/content/res/Resources;Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/2rX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1433925
    invoke-direct {p0, p1, p2, p3}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1433926
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/93c;->a:J

    .line 1433927
    iput-object p5, p0, LX/93c;->b:Ljava/lang/String;

    .line 1433928
    iput-object p6, p0, LX/93c;->e:LX/2rX;

    .line 1433929
    iput-object p7, p0, LX/93c;->c:LX/0tX;

    .line 1433930
    iput-object p8, p0, LX/93c;->d:Landroid/content/res/Resources;

    .line 1433931
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/93c;->f:Z

    .line 1433932
    return-void
.end method

.method public static a(LX/93c;LX/2rX;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;
    .locals 6

    .prologue
    .line 1433933
    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v0

    invoke-interface {v0}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/model/PrivacyType;->getByValue(Ljava/lang/String;)Lcom/facebook/graphql/model/PrivacyType;

    move-result-object v0

    .line 1433934
    sget-object v1, LX/93b;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/PrivacyType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1433935
    iget-object v0, p0, LX/93Q;->b:LX/03V;

    move-object v0, v0

    .line 1433936
    const-string v1, "composer_event_undefined_privacy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not determine event privacy, id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, LX/93c;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", privacyScopeType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v3

    invoke-interface {v3}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433937
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    :goto_0
    return-object v0

    .line 1433938
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    .line 1433939
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    .line 1433940
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static b$redex0(LX/93c;LX/2rX;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1433941
    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v0

    invoke-interface {v0}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/model/PrivacyType;->getByValue(Ljava/lang/String;)Lcom/facebook/graphql/model/PrivacyType;

    move-result-object v0

    .line 1433942
    sget-object v1, LX/93b;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/PrivacyType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1433943
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1433944
    :pswitch_0
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f08131d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1433945
    :pswitch_1
    iget-boolean v0, p0, LX/93c;->f:Z

    if-eqz v0, :cond_0

    .line 1433946
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f08132d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1433947
    :cond_0
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f08132c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1433948
    :pswitch_2
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f081334

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c(LX/93c;LX/2rX;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1433949
    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v0

    invoke-interface {v0}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/model/PrivacyType;->getByValue(Ljava/lang/String;)Lcom/facebook/graphql/model/PrivacyType;

    move-result-object v0

    .line 1433950
    sget-object v1, LX/93b;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/PrivacyType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1433951
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1433952
    :pswitch_0
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f081326

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1433953
    :pswitch_1
    iget-boolean v0, p0, LX/93c;->f:Z

    if-eqz v0, :cond_0

    .line 1433954
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f081325

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1433955
    :cond_0
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f081324

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1433956
    :pswitch_2
    iget-object v0, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v1, 0x7f081323

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1433957
    invoke-super {p0}, LX/93Q;->a()V

    .line 1433958
    const/4 v1, 0x1

    .line 1433959
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1433960
    iput-object v2, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1433961
    move-object v0, v0

    .line 1433962
    iget-object v2, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v3, 0x7f081337

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1433963
    iput-object v2, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1433964
    move-object v0, v0

    .line 1433965
    iget-object v2, p0, LX/93c;->d:Landroid/content/res/Resources;

    const v3, 0x7f081338

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1433966
    iput-object v2, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1433967
    move-object v0, v0

    .line 1433968
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1433969
    iget-object v2, p0, LX/93c;->e:LX/2rX;

    if-eqz v2, :cond_0

    .line 1433970
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    iget-object v2, p0, LX/93c;->e:LX/2rX;

    invoke-static {p0, v2}, LX/93c;->a(LX/93c;LX/2rX;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    .line 1433971
    iput-object v2, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1433972
    move-object v0, v0

    .line 1433973
    iget-object v2, p0, LX/93c;->e:LX/2rX;

    invoke-static {p0, v2}, LX/93c;->b$redex0(LX/93c;LX/2rX;)Ljava/lang/String;

    move-result-object v2

    .line 1433974
    iput-object v2, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1433975
    move-object v0, v0

    .line 1433976
    iget-object v2, p0, LX/93c;->e:LX/2rX;

    invoke-static {p0, v2}, LX/93c;->c(LX/93c;LX/2rX;)Ljava/lang/String;

    move-result-object v2

    .line 1433977
    iput-object v2, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1433978
    move-object v0, v0

    .line 1433979
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1433980
    :cond_0
    new-instance v2, LX/7lP;

    invoke-direct {v2}, LX/7lP;-><init>()V

    .line 1433981
    iput-boolean v1, v2, LX/7lP;->a:Z

    .line 1433982
    move-object v2, v2

    .line 1433983
    iget-object v3, p0, LX/93c;->e:LX/2rX;

    if-nez v3, :cond_2

    .line 1433984
    :goto_0
    iput-boolean v1, v2, LX/7lP;->b:Z

    .line 1433985
    move-object v1, v2

    .line 1433986
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    move-object v0, v0

    .line 1433987
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1433988
    iget-boolean v1, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v1, :cond_1

    .line 1433989
    :goto_1
    return-void

    .line 1433990
    :cond_1
    new-instance v1, LX/948;

    invoke-direct {v1}, LX/948;-><init>()V

    move-object v1, v1

    .line 1433991
    const-string v2, "event_id"

    iget-wide v4, p0, LX/93c;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1433992
    iget-object v2, p0, LX/93Q;->c:LX/1Ck;

    move-object v2, v2

    .line 1433993
    const-string v3, "fetch_event_data"

    iget-object v4, p0, LX/93c;->c:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v4, LX/93a;

    invoke-direct {v4, p0, v0}, LX/93a;-><init>(LX/93c;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1433994
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "event:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/93c;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
