.class public LX/APO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/01T;


# direct methods
.method public constructor <init>(LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670134
    iput-object p1, p0, LX/APO;->a:LX/01T;

    .line 1670135
    return-void
.end method

.method public static a(LX/0QB;)LX/APO;
    .locals 2

    .prologue
    .line 1670136
    new-instance v1, LX/APO;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    invoke-direct {v1, v0}, LX/APO;-><init>(LX/01T;)V

    .line 1670137
    move-object v0, v1

    .line 1670138
    return-object v0
.end method


# virtual methods
.method public final a(LX/2rw;)Z
    .locals 2

    .prologue
    .line 1670139
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1670140
    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/APO;->a:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 1670141
    :cond_0
    const/4 v0, 0x0

    .line 1670142
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
