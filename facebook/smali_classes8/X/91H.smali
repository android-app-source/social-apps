.class public LX/91H;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/91H;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430275
    iput-object p1, p0, LX/91H;->a:LX/0Zb;

    .line 1430276
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1430277
    new-instance v0, LX/8yQ;

    const-string v1, "minutiae_icon_picker"

    invoke-direct {v0, p0, p1, v1}, LX/8yQ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430278
    return-object v0
.end method

.method public static a(LX/0QB;)LX/91H;
    .locals 4

    .prologue
    .line 1430279
    sget-object v0, LX/91H;->b:LX/91H;

    if-nez v0, :cond_1

    .line 1430280
    const-class v1, LX/91H;

    monitor-enter v1

    .line 1430281
    :try_start_0
    sget-object v0, LX/91H;->b:LX/91H;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1430282
    if-eqz v2, :cond_0

    .line 1430283
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1430284
    new-instance p0, LX/91H;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/91H;-><init>(LX/0Zb;)V

    .line 1430285
    move-object v0, p0

    .line 1430286
    sput-object v0, LX/91H;->b:LX/91H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430287
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1430288
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1430289
    :cond_1
    sget-object v0, LX/91H;->b:LX/91H;

    return-object v0

    .line 1430290
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1430291
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
