.class public final enum LX/8m4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8m4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8m4;

.field public static final enum ALL:LX/8m4;

.field public static final enum FEATURED:LX/8m4;

.field public static final enum NON_FEATURED:LX/8m4;


# instance fields
.field private queryParam:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1398376
    new-instance v0, LX/8m4;

    const-string v1, "ALL"

    const-string v2, "all"

    invoke-direct {v0, v1, v3, v2}, LX/8m4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8m4;->ALL:LX/8m4;

    .line 1398377
    new-instance v0, LX/8m4;

    const-string v1, "FEATURED"

    const-string v2, "featured"

    invoke-direct {v0, v1, v4, v2}, LX/8m4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8m4;->FEATURED:LX/8m4;

    .line 1398378
    new-instance v0, LX/8m4;

    const-string v1, "NON_FEATURED"

    const-string v2, "non_featured"

    invoke-direct {v0, v1, v5, v2}, LX/8m4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8m4;->NON_FEATURED:LX/8m4;

    .line 1398379
    const/4 v0, 0x3

    new-array v0, v0, [LX/8m4;

    sget-object v1, LX/8m4;->ALL:LX/8m4;

    aput-object v1, v0, v3

    sget-object v1, LX/8m4;->FEATURED:LX/8m4;

    aput-object v1, v0, v4

    sget-object v1, LX/8m4;->NON_FEATURED:LX/8m4;

    aput-object v1, v0, v5

    sput-object v0, LX/8m4;->$VALUES:[LX/8m4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1398373
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1398374
    iput-object p3, p0, LX/8m4;->queryParam:Ljava/lang/String;

    .line 1398375
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8m4;
    .locals 1

    .prologue
    .line 1398372
    const-class v0, LX/8m4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8m4;

    return-object v0
.end method

.method public static values()[LX/8m4;
    .locals 1

    .prologue
    .line 1398371
    sget-object v0, LX/8m4;->$VALUES:[LX/8m4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8m4;

    return-object v0
.end method


# virtual methods
.method public final getQueryParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1398370
    iget-object v0, p0, LX/8m4;->queryParam:Ljava/lang/String;

    return-object v0
.end method
