.class public final LX/9ha;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/concurrent/Future",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/tagging/model/TaggingProfile;

.field public final synthetic c:Landroid/graphics/PointF;

.field public final synthetic d:LX/9hh;


# direct methods
.method public constructor <init>(LX/9hh;Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 1526757
    iput-object p1, p0, LX/9ha;->d:LX/9hh;

    iput-object p2, p0, LX/9ha;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9ha;->b:Lcom/facebook/tagging/model/TaggingProfile;

    iput-object p4, p0, LX/9ha;->c:Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1526758
    iget-object v0, p0, LX/9ha;->d:LX/9hh;

    iget-object v0, v0, LX/9hh;->e:LX/9fy;

    iget-object v1, p0, LX/9ha;->a:Ljava/lang/String;

    iget-object v2, p0, LX/9ha;->b:Lcom/facebook/tagging/model/TaggingProfile;

    iget-object v3, p0, LX/9ha;->c:Landroid/graphics/PointF;

    .line 1526759
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1526760
    new-instance v5, Lcom/facebook/photos/data/method/AddPhotoTagParams;

    invoke-direct {v5, v1, v2, v3}, Lcom/facebook/photos/data/method/AddPhotoTagParams;-><init>(Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)V

    .line 1526761
    const-string v6, "addPhotoTagParams"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1526762
    iget-object v5, v0, LX/9fy;->a:LX/0aG;

    const-string v6, "add_photo_tag"

    const p0, 0x2938548d

    invoke-static {v5, v6, v4, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    move-object v0, v4

    .line 1526763
    return-object v0
.end method
