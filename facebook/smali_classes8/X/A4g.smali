.class public final LX/A4g;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1619552
    const/16 v16, 0x0

    .line 1619553
    const-wide/16 v14, 0x0

    .line 1619554
    const/4 v13, 0x0

    .line 1619555
    const/4 v12, 0x0

    .line 1619556
    const/4 v11, 0x0

    .line 1619557
    const/4 v10, 0x0

    .line 1619558
    const/4 v9, 0x0

    .line 1619559
    const/4 v8, 0x0

    .line 1619560
    const/4 v7, 0x0

    .line 1619561
    const/4 v6, 0x0

    .line 1619562
    const/4 v5, 0x0

    .line 1619563
    const/4 v4, 0x0

    .line 1619564
    const/4 v3, 0x0

    .line 1619565
    const/4 v2, 0x0

    .line 1619566
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_10

    .line 1619567
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1619568
    const/4 v2, 0x0

    .line 1619569
    :goto_0
    return v2

    .line 1619570
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v7, :cond_e

    .line 1619571
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1619572
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1619573
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v19, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    if-eq v7, v0, :cond_0

    if-eqz v3, :cond_0

    .line 1619574
    const-string v7, "all_share_stories"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1619575
    invoke-static/range {p0 .. p1}, LX/A4T;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 1619576
    :cond_1
    const-string v7, "creation_time"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1619577
    const/4 v2, 0x1

    .line 1619578
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 1619579
    :cond_2
    const-string v7, "external_url"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1619580
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move/from16 v18, v3

    goto :goto_1

    .line 1619581
    :cond_3
    const-string v7, "external_url_owning_profile"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1619582
    invoke-static/range {p0 .. p1}, LX/A4V;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v17, v3

    goto :goto_1

    .line 1619583
    :cond_4
    const-string v7, "id"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1619584
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move/from16 v16, v3

    goto :goto_1

    .line 1619585
    :cond_5
    const-string v7, "instant_article"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1619586
    invoke-static/range {p0 .. p1}, LX/A4Y;->a(LX/15w;LX/186;)I

    move-result v3

    move v15, v3

    goto :goto_1

    .line 1619587
    :cond_6
    const-string v7, "link_media"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1619588
    invoke-static/range {p0 .. p1}, LX/A4a;->a(LX/15w;LX/186;)I

    move-result v3

    move v14, v3

    goto/16 :goto_1

    .line 1619589
    :cond_7
    const-string v7, "open_graph_node"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1619590
    invoke-static/range {p0 .. p1}, LX/A4b;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto/16 :goto_1

    .line 1619591
    :cond_8
    const-string v7, "source"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1619592
    invoke-static/range {p0 .. p1}, LX/A4c;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 1619593
    :cond_9
    const-string v7, "summary"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1619594
    invoke-static/range {p0 .. p1}, LX/A4d;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 1619595
    :cond_a
    const-string v7, "title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1619596
    invoke-static/range {p0 .. p1}, LX/A4e;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 1619597
    :cond_b
    const-string v7, "url"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1619598
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 1619599
    :cond_c
    const-string v7, "video_share"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1619600
    invoke-static/range {p0 .. p1}, LX/A4f;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 1619601
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1619602
    :cond_e
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1619603
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1619604
    if-eqz v2, :cond_f

    .line 1619605
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1619606
    :cond_f
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1619607
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1619608
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1619609
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1619610
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1619611
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1619612
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1619613
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1619614
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1619615
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1619616
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1619617
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v17, v12

    move/from16 v18, v13

    move v12, v7

    move v13, v8

    move v8, v3

    move/from16 v20, v6

    move/from16 v6, v16

    move/from16 v16, v11

    move/from16 v11, v20

    move/from16 v21, v9

    move v9, v4

    move/from16 v22, v5

    move-wide v4, v14

    move/from16 v14, v21

    move v15, v10

    move/from16 v10, v22

    goto/16 :goto_1
.end method
