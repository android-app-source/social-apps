.class public LX/AR0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/74n;

.field private final c:LX/03V;

.field private final d:LX/8LV;

.field private final e:LX/0SG;

.field public final f:LX/1EZ;


# direct methods
.method private constructor <init>(LX/74n;LX/03V;LX/8LV;LX/0SG;LX/1EZ;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1672075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672076
    iput-object p1, p0, LX/AR0;->b:LX/74n;

    .line 1672077
    iput-object p2, p0, LX/AR0;->c:LX/03V;

    .line 1672078
    iput-object p3, p0, LX/AR0;->d:LX/8LV;

    .line 1672079
    iput-object p4, p0, LX/AR0;->e:LX/0SG;

    .line 1672080
    iput-object p5, p0, LX/AR0;->f:LX/1EZ;

    .line 1672081
    return-void
.end method

.method private static a(LX/0jC;Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 3

    .prologue
    .line 1672082
    invoke-interface {p0}, LX/0jC;->getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v0

    .line 1672083
    if-nez v0, :cond_0

    .line 1672084
    :goto_0
    return-object p1

    :cond_0
    new-instance v1, LX/8LQ;

    invoke-direct {v1, p1}, LX/8LQ;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    const-string v2, "STORYLINE"

    .line 1672085
    iput-object v2, v1, LX/8LQ;->r:Ljava/lang/String;

    .line 1672086
    move-object v1, v1

    .line 1672087
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->getMoodId()Ljava/lang/String;

    move-result-object v0

    .line 1672088
    iput-object v0, v1, LX/8LQ;->ai:Ljava/lang/String;

    .line 1672089
    move-object v0, v1

    .line 1672090
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object p1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/AR0;
    .locals 6

    .prologue
    .line 1672091
    new-instance v0, LX/AR0;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v1

    check-cast v1, LX/74n;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v3

    check-cast v3, LX/8LV;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v5

    check-cast v5, LX/1EZ;

    invoke-direct/range {v0 .. v5}, LX/AR0;-><init>(LX/74n;LX/03V;LX/8LV;LX/0SG;LX/1EZ;)V

    .line 1672092
    const/16 v1, 0x12cb

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 1672093
    iput-object v1, v0, LX/AR0;->a:LX/0Or;

    .line 1672094
    return-object v0
.end method


# virtual methods
.method public final a(LX/0jI;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 38
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<DataProvider::",
            "LX/0jI;",
            ":",
            "LX/0io;",
            ":",
            "LX/0iw;",
            ":",
            "LX/0ix;",
            ":",
            "LX/0iy;",
            ":",
            "LX/0iz;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j2;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j8;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0iq;",
            ":",
            "LX/0ir;",
            ":",
            "LX/0jB;",
            ":",
            "LX/0jC;",
            ":",
            "LX/0jD;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0is;",
            ":",
            "LX/0ip;",
            ":",
            "LX/0jG;",
            ":",
            "LX/0jE;",
            ":",
            "LX/0jH;",
            ":",
            "LX/0jF;",
            ">(TDataProvider;)",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1672095
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR0;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 1672096
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    move-object/from16 v2, p1

    .line 1672097
    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-wide v0, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-wide/from16 v19, v0

    .line 1672098
    const/4 v3, 0x0

    move-object/from16 v2, p1

    .line 1672099
    check-cast v2, LX/0io;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->g(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    .line 1672100
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1672101
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/media/VideoItem;

    move-object v3, v2

    :cond_0
    move-object/from16 v2, p1

    .line 1672102
    check-cast v2, LX/0j2;

    invoke-interface {v2}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v2, p1

    .line 1672103
    check-cast v2, LX/0jD;

    invoke-interface {v2}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v9

    move-object/from16 v2, p1

    .line 1672104
    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v2, p1

    .line 1672105
    check-cast v2, LX/0jF;

    invoke-interface {v2}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v29

    move-object/from16 v2, p1

    .line 1672106
    check-cast v2, LX/0iz;

    invoke-interface {v2}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v30

    .line 1672107
    if-nez v3, :cond_1

    .line 1672108
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR0;->c:LX/03V;

    const-string v3, "video upload error"

    const-string v4, "Null VideoItem"

    invoke-static {v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672109
    const/4 v2, 0x0

    .line 1672110
    :goto_0
    return-object v2

    :cond_1
    move-object/from16 v2, p1

    .line 1672111
    check-cast v2, LX/0jB;

    invoke-interface {v2}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    if-eqz v2, :cond_3

    move-object/from16 v2, p1

    check-cast v2, LX/0jB;

    invoke-interface {v2}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v7

    .line 1672112
    :goto_1
    cmp-long v2, v10, v19

    if-eqz v2, :cond_2

    const-wide/16 v10, 0x0

    cmp-long v2, v19, v10

    if-gtz v2, :cond_6

    .line 1672113
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR0;->d:LX/8LV;

    move-object/from16 v4, p1

    check-cast v4, LX/0io;

    invoke-interface {v4}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7kq;->i(LX/0Px;)Landroid/os/Bundle;

    move-result-object v4

    move-object/from16 v6, p1

    check-cast v6, LX/0ip;

    invoke-interface {v6}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v6

    new-instance v8, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object/from16 v10, p1

    check-cast v10, LX/0iq;

    invoke-interface {v10}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    move-object/from16 v10, p1

    check-cast v10, LX/0j8;

    invoke-interface {v10}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v10

    move-object/from16 v12, p1

    check-cast v12, LX/0j8;

    invoke-interface {v12}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v13, p1

    check-cast v13, LX/0j8;

    invoke-interface {v13}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v13

    invoke-interface/range {p1 .. p1}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v15

    move-object/from16 v16, p1

    check-cast v16, LX/0ix;

    invoke-interface/range {v16 .. v16}, LX/0ix;->isUserSelectedTags()Z

    move-result v16

    move-object/from16 v17, p1

    check-cast v17, LX/0j8;

    invoke-interface/range {v17 .. v17}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v17

    if-eqz v17, :cond_4

    const/16 v17, 0x1

    :goto_2
    move-object/from16 v18, p1

    check-cast v18, LX/0j3;

    invoke-interface/range {v18 .. v18}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v19, p1

    check-cast v19, LX/0iw;

    invoke-interface/range {v19 .. v19}, LX/0iw;->isFeedOnlyPost()Z

    move-result v19

    move-object/from16 v20, p1

    check-cast v20, LX/0ir;

    invoke-interface/range {v20 .. v20}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v20

    move-object/from16 v21, p1

    check-cast v21, LX/0jH;

    invoke-interface/range {v21 .. v21}, LX/0jH;->getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v21

    move-object/from16 v22, p1

    check-cast v22, LX/0is;

    invoke-static/range {v22 .. v22}, LX/87S;->a(LX/0is;)LX/0Px;

    move-result-object v22

    move-object/from16 v23, p1

    check-cast v23, LX/0io;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/AR0;->e:LX/0SG;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, LX/AR5;->a(LX/0io;LX/0SG;)Lcom/facebook/audience/model/UploadShot;

    move-result-object v23

    move-object/from16 v24, p1

    check-cast v24, LX/0jG;

    invoke-interface/range {v24 .. v24}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v24

    if-eqz v24, :cond_5

    move-object/from16 v24, p1

    check-cast v24, LX/0jG;

    invoke-interface/range {v24 .. v24}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a()Ljava/lang/String;

    move-result-object v24

    :goto_3
    invoke-virtual/range {v2 .. v24}, LX/8LV;->a(Lcom/facebook/photos/base/media/VideoItem;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/0Px;JLjava/lang/String;ZLjava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 1672114
    :goto_4
    check-cast p1, LX/0jC;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/AR0;->a(LX/0jC;Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    .line 1672115
    :cond_3
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1672116
    :cond_4
    const/16 v17, 0x0

    goto :goto_2

    :cond_5
    const/16 v24, 0x0

    goto :goto_3

    :cond_6
    move-object/from16 v2, p1

    .line 1672117
    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    if-eqz v2, :cond_9

    move-object/from16 v2, p1

    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 1672118
    move-object/from16 v0, p0

    iget-object v0, v0, LX/AR0;->d:LX/8LV;

    move-object/from16 v16, v0

    move-object/from16 v2, p1

    check-cast v2, LX/0io;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->i(LX/0Px;)Landroid/os/Bundle;

    move-result-object v18

    move-object/from16 v2, p1

    check-cast v2, LX/0ip;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v22

    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v24

    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_7

    const/16 v26, 0x1

    :goto_5
    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v28

    move-object/from16 v2, p1

    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v32

    move-object/from16 v2, p1

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v2, p1

    check-cast v2, LX/0ir;

    invoke-interface {v2}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v34

    move-object/from16 v2, p1

    check-cast v2, LX/0jG;

    invoke-interface {v2}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    if-eqz v2, :cond_8

    move-object/from16 v2, p1

    check-cast v2, LX/0jG;

    invoke-interface {v2}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a()Ljava/lang/String;

    move-result-object v35

    :goto_6
    move-object/from16 v17, v3

    move-object/from16 v21, v5

    move-object/from16 v23, v7

    move-object/from16 v31, v14

    invoke-virtual/range {v16 .. v35}, LX/8LV;->a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JZLjava/lang/String;ZLX/5Rn;Ljava/lang/Long;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_4

    :cond_7
    const/16 v26, 0x0

    goto :goto_5

    :cond_8
    const/16 v35, 0x0

    goto :goto_6

    .line 1672119
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, LX/AR0;->d:LX/8LV;

    move-object/from16 v16, v0

    move-object/from16 v2, p1

    check-cast v2, LX/0io;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->i(LX/0Px;)Landroid/os/Bundle;

    move-result-object v18

    move-object/from16 v2, p1

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v2, p1

    check-cast v2, LX/0ip;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v23

    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v26

    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_a

    const/16 v28, 0x1

    :goto_7
    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v2, p1

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v30

    move-object/from16 v2, p1

    check-cast v2, LX/0jE;

    invoke-interface {v2}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v32

    move-object/from16 v2, p1

    check-cast v2, LX/0iy;

    invoke-interface {v2}, LX/0iy;->getMarketplaceId()J

    move-result-wide v33

    move-object/from16 v2, p1

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v2, p1

    check-cast v2, LX/0ir;

    invoke-interface {v2}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v36

    move-object/from16 v2, p1

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isGroupMemberBioPost()Z

    move-result v37

    move-object/from16 v17, v3

    move-object/from16 v22, v5

    move-object/from16 v24, v7

    move-object/from16 v25, v9

    move-object/from16 v31, v14

    invoke-virtual/range {v16 .. v37}, LX/8LV;->a(Lcom/facebook/photos/base/media/VideoItem;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;LX/0Px;JZLjava/lang/String;ZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Z)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_4

    :cond_a
    const/16 v28, 0x0

    goto :goto_7
.end method
