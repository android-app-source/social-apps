.class public final LX/A7O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/0fP;

.field public b:LX/10b;

.field public c:Lcom/facebook/widget/CustomFrameLayout;

.field public d:LX/0fM;

.field public e:I

.field public f:I

.field public g:Landroid/view/View;

.field public h:Landroid/view/View;

.field public i:Z

.field private j:Z

.field public k:Z

.field public l:Z


# direct methods
.method public constructor <init>(LX/0fP;LX/10b;LX/0fM;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1626018
    iput-object p1, p0, LX/A7O;->a:LX/0fP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1626019
    iput-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    .line 1626020
    iput-object v0, p0, LX/A7O;->d:LX/0fM;

    .line 1626021
    iput v1, p0, LX/A7O;->e:I

    .line 1626022
    iput v1, p0, LX/A7O;->f:I

    .line 1626023
    iput-object v0, p0, LX/A7O;->g:Landroid/view/View;

    .line 1626024
    iput-object v0, p0, LX/A7O;->h:Landroid/view/View;

    .line 1626025
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/A7O;->k:Z

    .line 1626026
    iput-boolean v2, p0, LX/A7O;->l:Z

    .line 1626027
    iput-object p2, p0, LX/A7O;->b:LX/10b;

    .line 1626028
    iput-object p3, p0, LX/A7O;->d:LX/0fM;

    .line 1626029
    iput-boolean v2, p0, LX/A7O;->i:Z

    .line 1626030
    iput-boolean p4, p0, LX/A7O;->j:Z

    .line 1626031
    return-void
.end method

.method public static a(LX/A7O;Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 1626009
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, p2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1626010
    sget-object v1, LX/A7M;->a:[I

    iget-object v2, p0, LX/A7O;->b:LX/10b;

    invoke-virtual {v2}, LX/10b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1626011
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1626012
    iget-object v0, p0, LX/A7O;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v0, p1}, LX/A7T;->addView(Landroid/view/View;)V

    .line 1626013
    return-void

    .line 1626014
    :pswitch_0
    const/4 v1, 0x3

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1626015
    neg-int v1, p2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 1626016
    :pswitch_1
    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1626017
    neg-int v1, p2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/view/View;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1625910
    if-nez p0, :cond_0

    .line 1625911
    :goto_0
    return-void

    .line 1625912
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1625913
    if-eqz v0, :cond_1

    .line 1625914
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1625915
    :cond_1
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 1625916
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1625917
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 1625918
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1625919
    :cond_3
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/A7O;F)V
    .locals 1

    .prologue
    .line 1626002
    iget-boolean v0, p0, LX/A7O;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/A7O;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1626003
    iget-object v0, p0, LX/A7O;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1626004
    :cond_0
    return-void
.end method

.method public static e(LX/A7O;Z)V
    .locals 2

    .prologue
    .line 1626005
    iget-boolean v0, p0, LX/A7O;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/A7O;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1626006
    iget-object v1, p0, LX/A7O;->h:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1626007
    :cond_0
    return-void

    .line 1626008
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1625992
    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomFrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1625993
    if-ne p1, v0, :cond_0

    .line 1625994
    :goto_1
    return-void

    .line 1625995
    :cond_0
    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    if-nez v0, :cond_1

    .line 1625996
    invoke-virtual {p0}, LX/A7O;->b()V

    .line 1625997
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/A7O;->b:LX/10b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " visibility changed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_2

    const-string v0, "VISIBLE"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1625998
    iget-object v1, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 1625999
    iget-object v0, p0, LX/A7O;->d:LX/0fM;

    invoke-virtual {v0, p1}, LX/0fM;->a(Z)V

    goto :goto_1

    .line 1626000
    :cond_2
    const-string v0, "INVISIBLE"

    goto :goto_2

    .line 1626001
    :cond_3
    const/4 v0, 0x4

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0fM;)Z
    .locals 1

    .prologue
    .line 1625991
    iget-object v0, p0, LX/A7O;->d:LX/0fM;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1625927
    iget-object v0, p0, LX/A7O;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->i:LX/A7N;

    sget-object v2, LX/A7N;->ENSURE_BACKGROUND:LX/A7N;

    if-ne v0, v2, :cond_0

    .line 1625928
    iget-object v0, p0, LX/A7O;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v0, v1}, LX/A7T;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1625929
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1625930
    iget-object v2, p0, LX/A7O;->a:LX/0fP;

    .line 1625931
    const/4 v3, 0x0

    .line 1625932
    invoke-static {v2, v0, v0, v3}, LX/0fP;->a$redex0(LX/0fP;Landroid/view/View;Landroid/view/View;Ljava/lang/Integer;)V

    .line 1625933
    :cond_0
    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    if-nez v0, :cond_2

    .line 1625934
    new-instance v0, Lcom/facebook/widget/CustomFrameLayout;

    iget-object v2, p0, LX/A7O;->a:LX/0fP;

    iget-object v2, v2, LX/0fP;->f:Landroid/app/Activity;

    invoke-direct {v0, v2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    .line 1625935
    iget-object v2, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    iget-object v0, p0, LX/A7O;->b:LX/10b;

    sget-object v3, LX/10b;->LEFT:LX/10b;

    if-ne v0, v3, :cond_b

    const v0, 0x7f0d01ba

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/widget/CustomFrameLayout;->setId(I)V

    .line 1625936
    iget-boolean v0, p0, LX/A7O;->j:Z

    if-eqz v0, :cond_1

    .line 1625937
    iget-object v2, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    iget-object v0, p0, LX/A7O;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hI;

    iget-object v3, p0, LX/A7O;->a:LX/0fP;

    iget-object v3, v3, LX/0fP;->f:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v0

    invoke-virtual {v2, v1, v0, v1, v1}, Lcom/facebook/widget/CustomFrameLayout;->setPadding(IIII)V

    .line 1625938
    :cond_1
    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 1625939
    :cond_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1625940
    iget-object v0, p0, LX/A7O;->a:LX/0fP;

    iget-object v0, v0, LX/0fP;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1625941
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1625942
    iget v5, p0, LX/A7O;->e:I

    if-ltz v5, :cond_d

    iget v5, p0, LX/A7O;->f:I

    if-ne v4, v5, :cond_d

    .line 1625943
    :cond_3
    :goto_1
    move v2, v3

    .line 1625944
    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomFrameLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 1625945
    iget-object v0, p0, LX/A7O;->d:LX/0fM;

    iget-object v3, p0, LX/A7O;->a:LX/0fP;

    iget-object v3, v3, LX/0fP;->f:Landroid/app/Activity;

    .line 1625946
    iget-object v5, v0, LX/0fM;->e:Landroid/view/View;

    if-nez v5, :cond_10

    const/4 v5, 0x1

    :goto_2
    const-string v6, "Root view has already been created."

    invoke-static {v5, v6}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1625947
    invoke-virtual {v0, v3}, LX/0fM;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v5

    iput-object v5, v0, LX/0fM;->e:Landroid/view/View;

    .line 1625948
    iget-object v5, v0, LX/0fM;->e:Landroid/view/View;

    move-object v0, v5

    .line 1625949
    const-string v3, "You must return a view when implementing DrawerContentController.onCreateView"

    invoke-static {v0, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625950
    iget-object v3, p0, LX/A7O;->a:LX/0fP;

    iget-object v4, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    const v5, 0x7f0a0724

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v0, v5}, LX/0fP;->a$redex0(LX/0fP;Landroid/view/View;Landroid/view/View;Ljava/lang/Integer;)V

    .line 1625951
    iget-object v3, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v3, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 1625952
    :cond_4
    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_c

    .line 1625953
    const/4 v0, 0x1

    .line 1625954
    iget-object v3, p0, LX/A7O;->a:LX/0fP;

    iget-object v3, v3, LX/0fP;->g:Landroid/widget/FrameLayout;

    iget-object v4, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v3, v4, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 1625955
    :goto_3
    iget-boolean v1, p0, LX/A7O;->k:Z

    if-eqz v1, :cond_6

    .line 1625956
    iget-object v1, p0, LX/A7O;->g:Landroid/view/View;

    if-nez v1, :cond_5

    .line 1625957
    new-instance v1, Landroid/view/View;

    iget-object v3, p0, LX/A7O;->a:LX/0fP;

    iget-object v3, v3, LX/0fP;->f:Landroid/app/Activity;

    invoke-direct {v1, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/A7O;->g:Landroid/view/View;

    .line 1625958
    iget-object v3, p0, LX/A7O;->g:Landroid/view/View;

    iget-object v1, p0, LX/A7O;->b:LX/10b;

    sget-object v4, LX/10b;->LEFT:LX/10b;

    if-ne v1, v4, :cond_11

    const v1, 0x7f020405

    :goto_4
    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1625959
    :cond_5
    iget-object v1, p0, LX/A7O;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_6

    .line 1625960
    iget-object v1, p0, LX/A7O;->a:LX/0fP;

    iget-object v1, v1, LX/0fP;->f:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b18af

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1625961
    iget-object v3, p0, LX/A7O;->g:Landroid/view/View;

    invoke-static {p0, v3, v1}, LX/A7O;->a(LX/A7O;Landroid/view/View;I)V

    .line 1625962
    :cond_6
    iget-boolean v1, p0, LX/A7O;->l:Z

    if-eqz v1, :cond_8

    .line 1625963
    iget-object v1, p0, LX/A7O;->h:Landroid/view/View;

    if-nez v1, :cond_7

    .line 1625964
    new-instance v1, Landroid/view/View;

    iget-object v3, p0, LX/A7O;->a:LX/0fP;

    iget-object v3, v3, LX/0fP;->f:Landroid/app/Activity;

    invoke-direct {v1, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/A7O;->h:Landroid/view/View;

    .line 1625965
    iget-object v1, p0, LX/A7O;->h:Landroid/view/View;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v4, -0x1000000

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1625966
    :cond_7
    iget-object v1, p0, LX/A7O;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_8

    .line 1625967
    iget-object v1, p0, LX/A7O;->h:Landroid/view/View;

    iget v3, p0, LX/A7O;->e:I

    invoke-static {p0, v1, v3}, LX/A7O;->a(LX/A7O;Landroid/view/View;I)V

    .line 1625968
    :cond_8
    if-eqz v2, :cond_9

    .line 1625969
    sget-object v1, LX/A7M;->a:[I

    iget-object v2, p0, LX/A7O;->b:LX/10b;

    invoke-virtual {v2}, LX/10b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1625970
    :cond_9
    :goto_5
    if-eqz v0, :cond_a

    .line 1625971
    iget-object v0, p0, LX/A7O;->d:LX/0fM;

    invoke-virtual {v0}, LX/0fM;->fW_()V

    .line 1625972
    :cond_a
    return-void

    .line 1625973
    :cond_b
    const v0, 0x7f0d01bb

    goto/16 :goto_0

    .line 1625974
    :pswitch_0
    iget-object v1, p0, LX/A7O;->a:LX/0fP;

    iget-object v1, v1, LX/0fP;->h:LX/A7T;

    iget v2, p0, LX/A7O;->e:I

    invoke-virtual {v1, v2}, LX/A7T;->setLeftDrawerWidth(I)V

    goto :goto_5

    .line 1625975
    :pswitch_1
    iget-object v1, p0, LX/A7O;->a:LX/0fP;

    iget-object v1, v1, LX/0fP;->h:LX/A7T;

    iget v2, p0, LX/A7O;->e:I

    invoke-virtual {v1, v2}, LX/A7T;->setRightDrawerWidth(I)V

    goto :goto_5

    :cond_c
    move v0, v1

    goto/16 :goto_3

    .line 1625976
    :cond_d
    iput v4, p0, LX/A7O;->f:I

    .line 1625977
    const v5, 0x7f0b18b0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1625978
    sub-int v0, v4, v0

    .line 1625979
    iget-object v4, p0, LX/A7O;->d:LX/0fM;

    iget-object v5, p0, LX/A7O;->a:LX/0fP;

    iget-object v5, v5, LX/0fP;->f:Landroid/app/Activity;

    invoke-virtual {v4, v5, v0}, LX/0fM;->a(Landroid/content/Context;I)I

    move-result v4

    .line 1625980
    if-ltz v4, :cond_e

    move v0, v2

    :goto_6
    const-string v5, "Drawer width cannot be less than 0"

    invoke-static {v0, v5}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1625981
    iget v0, p0, LX/A7O;->e:I

    if-eq v4, v0, :cond_3

    .line 1625982
    iput v4, p0, LX/A7O;->e:I

    .line 1625983
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, LX/A7O;->e:I

    const/4 v4, -0x1

    invoke-direct {v3, v0, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1625984
    iget-object v0, p0, LX/A7O;->b:LX/10b;

    sget-object v4, LX/10b;->LEFT:LX/10b;

    if-ne v0, v4, :cond_f

    const/4 v0, 0x3

    :goto_7
    iput v0, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1625985
    iget-object v0, p0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v3, v2

    .line 1625986
    goto/16 :goto_1

    :cond_e
    move v0, v3

    .line 1625987
    goto :goto_6

    .line 1625988
    :cond_f
    const/4 v0, 0x5

    goto :goto_7

    .line 1625989
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 1625990
    :cond_11
    const v1, 0x7f020406

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1625920
    iget-boolean v0, p0, LX/A7O;->i:Z

    move v0, v0

    .line 1625921
    if-eq p1, v0, :cond_0

    .line 1625922
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/A7O;->b:LX/10b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " focus changed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_1

    const-string v0, "FOCUSED"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1625923
    iput-boolean p1, p0, LX/A7O;->i:Z

    .line 1625924
    iget-object v0, p0, LX/A7O;->d:LX/0fM;

    invoke-virtual {v0, p1}, LX/0fM;->b(Z)V

    .line 1625925
    :cond_0
    return-void

    .line 1625926
    :cond_1
    const-string v0, "UNFOCUSED"

    goto :goto_0
.end method
