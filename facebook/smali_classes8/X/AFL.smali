.class public final LX/AFL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/AFM;


# direct methods
.method public constructor <init>(LX/AFM;)V
    .locals 0

    .prologue
    .line 1647589
    iput-object p1, p0, LX/AFL;->a:LX/AFM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/audience/model/ReplyThread;)V
    .locals 10

    .prologue
    .line 1647590
    iget-object v0, p0, LX/AFL;->a:LX/AFM;

    iget-object v0, v0, LX/AFM;->a:LX/AFO;

    iget-object v0, v0, LX/AFO;->l:LX/AFJ;

    new-instance v1, LX/AFK;

    invoke-direct {v1, p0}, LX/AFK;-><init>(LX/AFL;)V

    .line 1647591
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 1647592
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1647593
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v5, v3

    .line 1647594
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v3

    .line 1647595
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    .line 1647596
    iget-boolean v6, p1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    move v6, v6

    .line 1647597
    if-eqz v6, :cond_0

    .line 1647598
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1647599
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1647600
    :cond_0
    iget-object v8, v0, LX/AFJ;->a:LX/AFD;

    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    new-instance v2, LX/AFI;

    move-object v3, v0

    move-object v6, p1

    move-object v7, v1

    invoke-direct/range {v2 .. v7}, LX/AFI;-><init>(LX/AFJ;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/audience/model/ReplyThread;LX/AFK;)V

    invoke-virtual {v8, v9, v2}, LX/AFD;->a(Ljava/util/List;LX/AEs;)V

    .line 1647601
    return-void
.end method
