.class public final LX/9xc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1593132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1593133
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1593134
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1593135
    invoke-static {p0, p1}, LX/9xc;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1593136
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1593137
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1593126
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1593127
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1593128
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/9xc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1593129
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1593130
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1593131
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1593106
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1593107
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593108
    :goto_0
    return v1

    .line 1593109
    :cond_0
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1593110
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    .line 1593111
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1593112
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1593113
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1593114
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1593115
    const-string v7, "action"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1593116
    invoke-static {p0, p1}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1593117
    :cond_2
    const-string v7, "label"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1593118
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1593119
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1593120
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1593121
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1593122
    if-eqz v0, :cond_5

    .line 1593123
    invoke-virtual {p1, v2, v4, v1}, LX/186;->a(III)V

    .line 1593124
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1593125
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1593091
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1593092
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1593093
    if-eqz v0, :cond_0

    .line 1593094
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593095
    invoke-static {p0, v0, p2, p3}, LX/9tj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1593096
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1593097
    if-eqz v0, :cond_1

    .line 1593098
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593099
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1593100
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593101
    if-eqz v0, :cond_2

    .line 1593102
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593103
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1593104
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1593105
    return-void
.end method
