.class public final LX/923;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;)V
    .locals 0

    .prologue
    .line 1431878
    iput-object p1, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 1431879
    iget-object v0, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    iget v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->l:I

    if-eq v0, p2, :cond_1

    .line 1431880
    iget-object v0, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    iget v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->l:I

    if-nez v0, :cond_0

    .line 1431881
    iget-object v0, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->j:LX/91H;

    iget-object v1, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    .line 1431882
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v2

    .line 1431883
    const-string v2, "session_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    iget-object v3, v3, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->c:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, v3, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 1431884
    const-string p1, "iconpicker_first_scroll"

    invoke-static {p1, v1}, LX/91H;->a(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    invoke-virtual {p1, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    invoke-virtual {p1, v3}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    .line 1431885
    iget-object p3, p1, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p1, p3

    .line 1431886
    iget-object p3, v0, LX/91H;->a:LX/0Zb;

    invoke-interface {p3, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1431887
    :cond_0
    iget-object v0, p0, LX/923;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    .line 1431888
    iput p2, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->l:I

    .line 1431889
    :cond_1
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1431890
    return-void
.end method
