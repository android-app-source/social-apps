.class public LX/98v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1446565
    iput-object p1, p0, LX/98v;->a:LX/0Ot;

    .line 1446566
    iput-object p2, p0, LX/98v;->b:LX/0Ot;

    .line 1446567
    return-void
.end method


# virtual methods
.method public final clearUserData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1446568
    iget-object v0, p0, LX/98v;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1446569
    invoke-static {v0, v1, v1, v1}, LX/8xR;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/5pC;)V

    .line 1446570
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/8xS;->a(Landroid/content/Context;Z)V

    .line 1446571
    invoke-static {v0}, LX/9nP;->a(Landroid/content/Context;)LX/9nP;

    move-result-object v0

    invoke-virtual {v0}, LX/9nP;->c()V

    .line 1446572
    iget-object v0, p0, LX/98v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33u;

    .line 1446573
    invoke-virtual {v0}, LX/33u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446574
    iget-object v0, p0, LX/98v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33u;

    invoke-virtual {v0}, LX/33u;->b()V

    .line 1446575
    :cond_0
    return-void
.end method
