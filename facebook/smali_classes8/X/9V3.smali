.class public LX/9V3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9V3;


# instance fields
.field public a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1498902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498903
    iput-object p1, p0, LX/9V3;->a:LX/0Zb;

    .line 1498904
    return-void
.end method

.method public static a(LX/0QB;)LX/9V3;
    .locals 4

    .prologue
    .line 1498905
    sget-object v0, LX/9V3;->b:LX/9V3;

    if-nez v0, :cond_1

    .line 1498906
    const-class v1, LX/9V3;

    monitor-enter v1

    .line 1498907
    :try_start_0
    sget-object v0, LX/9V3;->b:LX/9V3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1498908
    if-eqz v2, :cond_0

    .line 1498909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1498910
    new-instance p0, LX/9V3;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/9V3;-><init>(LX/0Zb;)V

    .line 1498911
    move-object v0, p0

    .line 1498912
    sput-object v0, LX/9V3;->b:LX/9V3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1498913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1498914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1498915
    :cond_1
    sget-object v0, LX/9V3;->b:LX/9V3;

    return-object v0

    .line 1498916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1498917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
