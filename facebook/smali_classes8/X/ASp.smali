.class public LX/ASp;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:F

.field public b:F

.field public c:Landroid/view/View;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Landroid/widget/ImageView;

.field public f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1674533
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ASp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1674534
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1674535
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ASp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1674536
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1674515
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1674516
    const p1, 0x7f03156a

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1674517
    const p1, 0x7f0d0884

    invoke-virtual {p0, p1}, LX/ASp;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/ASp;->c:Landroid/view/View;

    .line 1674518
    iget-object p1, p0, LX/ASp;->c:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 1674519
    const p1, 0x7f0d0340

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, LX/ASp;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1674520
    const p1, 0x7f0d0a7e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/ASp;->e:Landroid/widget/ImageView;

    .line 1674521
    const p1, 0x7f0d1b2b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/ASp;->f:Landroid/view/View;

    .line 1674522
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1674529
    iget v0, p0, LX/ASp;->b:F

    move v1, v0

    .line 1674530
    invoke-virtual {p0}, LX/ASp;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    new-instance v2, LX/ATB;

    invoke-direct {v2, p1, p2}, LX/ATB;-><init>(II)V

    invoke-static {v1, v0, v2}, LX/ATC;->a(FLandroid/widget/FrameLayout$LayoutParams;LX/ATB;)LX/ATB;

    move-result-object v0

    .line 1674531
    iget v1, v0, LX/ATB;->a:I

    iget v0, v0, LX/ATB;->b:I

    invoke-super {p0, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1674532
    return-void
.end method

.method public setController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1674527
    iget-object v0, p0, LX/ASp;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1674528
    return-void
.end method

.method public setScale(F)V
    .locals 0

    .prologue
    .line 1674523
    iput p1, p0, LX/ASp;->a:F

    .line 1674524
    invoke-virtual {p0, p1}, LX/ASp;->setScaleX(F)V

    .line 1674525
    invoke-virtual {p0, p1}, LX/ASp;->setScaleY(F)V

    .line 1674526
    return-void
.end method
