.class public final LX/9f6;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520795
    iput-object p1, p0, LX/9f6;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1520796
    check-cast p2, LX/1ln;

    .line 1520797
    invoke-super {p0, p1, p2, p3}, LX/1cC;->a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V

    .line 1520798
    iget-object v0, p0, LX/9f6;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1520799
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 1520800
    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1520801
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-lez v1, :cond_0

    .line 1520802
    iget-object v1, p0, LX/9f6;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/graphics/Rect;)V

    .line 1520803
    iget-object v0, p0, LX/9f6;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520804
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1520805
    check-cast p2, LX/1ln;

    .line 1520806
    invoke-super {p0, p1, p2}, LX/1cC;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1520807
    iget-object v0, p0, LX/9f6;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1520808
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 1520809
    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1520810
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-lez v1, :cond_0

    .line 1520811
    iget-object v1, p0, LX/9f6;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1, v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/graphics/Rect;)V

    .line 1520812
    iget-object v0, p0, LX/9f6;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520813
    :cond_0
    return-void
.end method
