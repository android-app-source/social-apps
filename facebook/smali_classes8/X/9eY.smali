.class public final LX/9eY;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Landroid/graphics/RectF;

.field public final synthetic b:Z

.field public final synthetic c:Landroid/graphics/RectF;

.field public final synthetic d:LX/9ec;


# direct methods
.method public constructor <init>(LX/9ec;Landroid/graphics/RectF;ZLandroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 1519770
    iput-object p1, p0, LX/9eY;->d:LX/9ec;

    iput-object p2, p0, LX/9eY;->a:Landroid/graphics/RectF;

    iput-boolean p3, p0, LX/9eY;->b:Z

    iput-object p4, p0, LX/9eY;->c:Landroid/graphics/RectF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1519777
    iget-object v0, p0, LX/9eY;->d:LX/9ec;

    iget-object v0, v0, LX/9ec;->t:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082426

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1519778
    iget-object v0, p0, LX/9eY;->d:LX/9ec;

    iget-object v0, v0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/9eY;->c:Landroid/graphics/RectF;

    invoke-static {v1}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    iget-boolean v1, p0, LX/9eY;->b:Z

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setIsRotated(Z)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1519779
    iget-object v1, p0, LX/9eY;->d:LX/9ec;

    iget-object v1, v1, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->b()V

    .line 1519780
    iget-object v1, p0, LX/9eY;->d:LX/9ec;

    iget-object v1, v1, LX/9ec;->k:LX/9f4;

    invoke-virtual {v1, v0}, LX/9f4;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1519781
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1519771
    iget-object v0, p0, LX/9eY;->d:LX/9ec;

    invoke-static {v0}, LX/9ec;->x(LX/9ec;)V

    .line 1519772
    iget-object v0, p0, LX/9eY;->d:LX/9ec;

    iget-object v0, v0, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    iget-object v0, p0, LX/9eY;->d:LX/9ec;

    iget-object v0, v0, LX/9ec;->K:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9eY;->d:LX/9ec;

    iget-object v0, v0, LX/9ec;->K:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setEditedUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/9eY;->a:Landroid/graphics/RectF;

    invoke-static {v1}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    iget-boolean v1, p0, LX/9eY;->b:Z

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setIsRotated(Z)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1519773
    iget-object v1, p0, LX/9eY;->d:LX/9ec;

    iget-object v1, v1, LX/9ec;->d:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->b()V

    .line 1519774
    iget-object v1, p0, LX/9eY;->d:LX/9ec;

    iget-object v1, v1, LX/9ec;->k:LX/9f4;

    invoke-virtual {v1, v0}, LX/9f4;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1519775
    return-void

    .line 1519776
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
