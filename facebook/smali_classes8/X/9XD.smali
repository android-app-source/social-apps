.class public LX/9XD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/89z;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/9XD;


# instance fields
.field private final b:LX/0kx;

.field private final c:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1502483
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "native_newsfeed"

    sget-object v2, LX/89z;->NEWSFEED:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "bookmarks"

    sget-object v2, LX/89z;->BOOKMARK:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "story_view"

    sget-object v2, LX/89z;->PERMALINK:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "newsfeed_attached_story_view"

    sget-object v2, LX/89z;->PERMALINK:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "timeline"

    sget-object v2, LX/89z;->USER_TIMELINE:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "story_feedback_flyout"

    sget-object v2, LX/89z;->PERMALINK:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "pages_public_view"

    sget-object v2, LX/89z;->PAGE:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "pages_posts_by_others_module_name"

    sget-object v2, LX/89z;->PAGE:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "notifications"

    sget-object v2, LX/89z;->NOTIFICATION:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "event_permalink"

    sget-object v2, LX/89z;->EVENT:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "event_feed"

    sget-object v2, LX/89z;->EVENT:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "group_feed"

    sget-object v2, LX/89z;->GROUP:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "group_product_feed"

    sget-object v2, LX/89z;->GROUP:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "graph_search_results_page"

    sget-object v2, LX/89z;->SEARCH_RESULTS:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "graph_search_results_page_blended"

    sget-object v2, LX/89z;->SEARCH_RESULTS:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "graph_search_results_page_page"

    sget-object v2, LX/89z;->SEARCH_RESULTS:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "search_typeahead"

    sget-object v2, LX/89z;->SEARCH_TYPEAHEAD:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "pages_launchpoint"

    sget-object v2, LX/89z;->LAUNCH_POINT:LX/89z;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/9XD;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/0kx;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1502524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1502525
    iput-object p1, p0, LX/9XD;->b:LX/0kx;

    .line 1502526
    iput-object p2, p0, LX/9XD;->c:LX/0Zb;

    .line 1502527
    return-void
.end method

.method public static a(LX/0QB;)LX/9XD;
    .locals 5

    .prologue
    .line 1502511
    sget-object v0, LX/9XD;->d:LX/9XD;

    if-nez v0, :cond_1

    .line 1502512
    const-class v1, LX/9XD;

    monitor-enter v1

    .line 1502513
    :try_start_0
    sget-object v0, LX/9XD;->d:LX/9XD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1502514
    if-eqz v2, :cond_0

    .line 1502515
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1502516
    new-instance p0, LX/9XD;

    invoke-static {v0}, LX/0kx;->a(LX/0QB;)LX/0kx;

    move-result-object v3

    check-cast v3, LX/0kx;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/9XD;-><init>(LX/0kx;LX/0Zb;)V

    .line 1502517
    move-object v0, p0

    .line 1502518
    sput-object v0, LX/9XD;->d:LX/9XD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1502519
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1502520
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1502521
    :cond_1
    sget-object v0, LX/9XD;->d:LX/9XD;

    return-object v0

    .line 1502522
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1502523
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/89z;
    .locals 4

    .prologue
    .line 1502484
    iget-object v0, p0, LX/9XD;->b:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->c()LX/148;

    move-result-object v1

    .line 1502485
    if-eqz v1, :cond_0

    .line 1502486
    iget-object v0, v1, LX/148;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1502487
    if-eqz v0, :cond_0

    sget-object v0, LX/9XD;->a:Ljava/util/Map;

    .line 1502488
    iget-object v2, v1, LX/148;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1502489
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1502490
    :cond_0
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "pages_mobile_unmapped_referrer"

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1502491
    if-eqz v1, :cond_3

    .line 1502492
    const/4 v0, 0x1

    .line 1502493
    iget-object v3, v1, LX/148;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1502494
    if-eqz v3, :cond_1

    .line 1502495
    const/4 v0, 0x3

    .line 1502496
    :cond_1
    iget-object v3, v1, LX/148;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1502497
    if-eqz v3, :cond_2

    .line 1502498
    or-int/lit8 v0, v0, 0x4

    .line 1502499
    :cond_2
    const-string v3, "referrer_module_info"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1502500
    const-string v0, "referrer_module_tag"

    .line 1502501
    iget-object v3, v1, LX/148;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1502502
    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1502503
    const-string v0, "referrer_module_class_name"

    .line 1502504
    iget-object v3, v1, LX/148;->b:Ljava/lang/String;

    move-object v1, v3

    .line 1502505
    invoke-virtual {v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1502506
    :cond_3
    iget-object v0, p0, LX/9XD;->c:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502507
    sget-object v0, LX/89z;->UNKNOWN:LX/89z;

    .line 1502508
    :goto_0
    return-object v0

    :cond_4
    sget-object v0, LX/9XD;->a:Ljava/util/Map;

    .line 1502509
    iget-object v2, v1, LX/148;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1502510
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89z;

    goto :goto_0
.end method
