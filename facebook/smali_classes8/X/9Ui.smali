.class public final LX/9Ui;
.super LX/9Uh;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:Landroid/view/Choreographer;


# direct methods
.method public constructor <init>(LX/9Uf;II)V
    .locals 1

    .prologue
    .line 1498488
    invoke-direct {p0, p1, p2, p3}, LX/9Uh;-><init>(LX/9Uf;II)V

    .line 1498489
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, LX/9Ui;->a:Landroid/view/Choreographer;

    .line 1498490
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1498486
    iget-object v0, p0, LX/9Ui;->a:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1498487
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1498482
    iget-object v0, p0, LX/9Ui;->a:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1498483
    return-void
.end method

.method public final doFrame(J)V
    .locals 3

    .prologue
    .line 1498484
    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    invoke-virtual {p0, v0, v1}, LX/9Uh;->a(J)V

    .line 1498485
    return-void
.end method
