.class public LX/8xn;
.super LX/16T;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static c:Z

.field private static volatile d:LX/8xn;


# instance fields
.field public final b:LX/0iA;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1424775
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_REMOVE_PLACE_H_SCROLL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/8xn;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424756
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1424757
    iput-object p1, p0, LX/8xn;->b:LX/0iA;

    .line 1424758
    return-void
.end method

.method public static a(LX/0QB;)LX/8xn;
    .locals 4

    .prologue
    .line 1424762
    sget-object v0, LX/8xn;->d:LX/8xn;

    if-nez v0, :cond_1

    .line 1424763
    const-class v1, LX/8xn;

    monitor-enter v1

    .line 1424764
    :try_start_0
    sget-object v0, LX/8xn;->d:LX/8xn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1424765
    if-eqz v2, :cond_0

    .line 1424766
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1424767
    new-instance p0, LX/8xn;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-direct {p0, v3}, LX/8xn;-><init>(LX/0iA;)V

    .line 1424768
    move-object v0, p0

    .line 1424769
    sput-object v0, LX/8xn;->d:LX/8xn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1424770
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1424771
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1424772
    :cond_1
    sget-object v0, LX/8xn;->d:LX/8xn;

    return-object v0

    .line 1424773
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1424774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1424761
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1424760
    const-string v0, "4495"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1424759
    sget-object v0, LX/8xn;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
