.class public LX/9mU;
.super LX/9mP;
.source ""


# instance fields
.field private e:Landroid/widget/ProgressBar;

.field private f:Landroid/widget/TextView;

.field public g:Lcom/facebook/widget/FlowLayout;

.field public h:Landroid/widget/EditText;

.field public i:Landroid/view/View;

.field public j:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

.field public k:Lcom/facebook/rapidreporting/ui/DialogStateData;

.field public l:Landroid/widget/TextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Landroid/text/TextWatcher;

.field public final n:Landroid/view/View$OnFocusChangeListener;

.field public final o:Landroid/view/View$OnClickListener;

.field public final p:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/0wM;)V
    .locals 3

    .prologue
    .line 1535677
    const v0, 0x7f0310e9    # 1.7421667E38f

    invoke-direct {p0, p1, v0, p3, p5}, LX/9mP;-><init>(Landroid/content/Context;ILcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/0wM;)V

    .line 1535678
    new-instance v0, LX/9mQ;

    invoke-direct {v0, p0}, LX/9mQ;-><init>(LX/9mU;)V

    iput-object v0, p0, LX/9mU;->m:Landroid/text/TextWatcher;

    .line 1535679
    new-instance v0, LX/9mR;

    invoke-direct {v0, p0}, LX/9mR;-><init>(LX/9mU;)V

    iput-object v0, p0, LX/9mU;->n:Landroid/view/View$OnFocusChangeListener;

    .line 1535680
    new-instance v0, LX/9mS;

    invoke-direct {v0, p0}, LX/9mS;-><init>(LX/9mU;)V

    iput-object v0, p0, LX/9mU;->o:Landroid/view/View$OnClickListener;

    .line 1535681
    new-instance v0, LX/9mT;

    invoke-direct {v0, p0}, LX/9mT;-><init>(LX/9mU;)V

    iput-object v0, p0, LX/9mU;->p:Landroid/view/View$OnClickListener;

    .line 1535682
    iput-object p2, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535683
    iput-object p4, p0, LX/9mU;->j:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1535684
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/9mU;->e:Landroid/widget/ProgressBar;

    .line 1535685
    const v0, 0x7f0d0555

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9mU;->f:Landroid/widget/TextView;

    .line 1535686
    const v0, 0x7f0d2828

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FlowLayout;

    iput-object v0, p0, LX/9mU;->g:Lcom/facebook/widget/FlowLayout;

    .line 1535687
    const v0, 0x7f0d2829

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LX/9mU;->h:Landroid/widget/EditText;

    .line 1535688
    const v0, 0x7f0d282a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9mU;->i:Landroid/view/View;

    .line 1535689
    iget-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    const v1, 0x7f0824af

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1535690
    iget-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    const v1, 0x7f0209ae

    const v2, -0x6f6b64

    invoke-virtual {p0, v0, v1, v2}, LX/9mP;->a(Landroid/widget/TextView;II)V

    .line 1535691
    iget-object v0, p0, LX/9mU;->e:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1535692
    return-void
.end method

.method public static a$redex0(LX/9mU;Landroid/view/View;ZLcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;)V
    .locals 2

    .prologue
    .line 1535693
    invoke-virtual {p1, p2}, Landroid/view/View;->setSelected(Z)V

    .line 1535694
    if-eqz p2, :cond_0

    const-string v0, "other"

    invoke-virtual {p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535695
    iget-boolean v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->i:Z

    move v0, v1

    .line 1535696
    if-eqz v0, :cond_0

    .line 1535697
    iget-object v0, p0, LX/9mU;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1535698
    invoke-virtual {p0}, LX/9mU;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/9mU;->h:Landroid/widget/EditText;

    invoke-static {v0, v1}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 1535699
    :cond_0
    return-void
.end method

.method public static setTags(LX/9mU;I)V
    .locals 6

    .prologue
    .line 1535700
    :goto_0
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/ui/DialogStateData;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1535701
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535702
    iget-boolean v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->g:Z

    move v0, v1

    .line 1535703
    if-nez v0, :cond_1

    const/16 v0, 0x64

    if-lt p1, v0, :cond_1

    .line 1535704
    iget-object v0, p0, LX/9mU;->g:Lcom/facebook/widget/FlowLayout;

    const/4 p1, 0x0

    .line 1535705
    invoke-virtual {p0}, LX/9mU;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0310ec

    invoke-virtual {v1, v2, v0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1535706
    const v2, 0x7f020724

    const v3, -0x6f6b64

    invoke-virtual {p0, v1, v2, v3}, LX/9mP;->a(Landroid/widget/TextView;II)V

    .line 1535707
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1535708
    iget-object v2, p0, LX/9mU;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1535709
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1535710
    :cond_0
    return-void

    .line 1535711
    :cond_1
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/ui/DialogStateData;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;

    .line 1535712
    iget-object v1, p0, LX/9mU;->g:Lcom/facebook/widget/FlowLayout;

    const/4 v5, 0x0

    .line 1535713
    invoke-virtual {p0}, LX/9mU;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0310ec

    invoke-virtual {v2, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1535714
    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->j()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535715
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1535716
    iget-object v3, p0, LX/9mU;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1535717
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1535718
    move-object v1, v2

    .line 1535719
    iget-object v2, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535720
    iget-object v3, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    move-object v2, v3

    .line 1535721
    if-eqz v2, :cond_2

    iget-object v2, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535722
    iget-object v3, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    move-object v2, v3

    .line 1535723
    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1535724
    const/4 v2, 0x1

    invoke-static {p0, v1, v2, v0}, LX/9mU;->a$redex0(LX/9mU;Landroid/view/View;ZLcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;)V

    .line 1535725
    :cond_2
    const-string v2, "other"

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1535726
    iput-object v1, p0, LX/9mU;->l:Landroid/widget/TextView;

    .line 1535727
    :cond_3
    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 1535728
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535729
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    move-object v0, v1

    .line 1535730
    if-eqz v0, :cond_0

    .line 1535731
    iget-object v0, p0, LX/9mU;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1535732
    iget-object v0, p0, LX/9mU;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535733
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1535734
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535735
    iget-object v0, p0, LX/9mU;->e:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1535736
    :goto_0
    return-void

    .line 1535737
    :cond_0
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/ui/DialogStateData;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1535738
    iget-object v0, p0, LX/9mP;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/ui/DialogStateData;->o()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535739
    iget-object v4, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v4, :cond_8

    .line 1535740
    const/4 v4, 0x0

    .line 1535741
    :goto_1
    move-object v2, v4

    .line 1535742
    invoke-virtual {p0, v0, v1, v2}, LX/9mP;->a(Landroid/widget/TextView;Ljava/lang/String;LX/0Px;)V

    .line 1535743
    :cond_1
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/ui/DialogStateData;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/ui/DialogStateData;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1535744
    invoke-static {p0, v3}, LX/9mU;->setTags(LX/9mU;I)V

    .line 1535745
    :cond_2
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535746
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v1, :cond_a

    .line 1535747
    const/4 v1, 0x0

    .line 1535748
    :goto_2
    move-object v0, v1

    .line 1535749
    if-eqz v0, :cond_7

    invoke-interface {v0}, LX/3Sb;->a()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1535750
    const/4 v12, 0x2

    const v11, -0x6e685d

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1535751
    invoke-virtual {p0}, LX/9mU;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1981

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1535752
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1535753
    invoke-virtual {v2, v1, v1, v1, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1535754
    invoke-interface {v0}, LX/3Sb;->b()LX/2sN;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v5, v1, LX/1vs;->b:I

    .line 1535755
    const-class v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v4, v5, v9, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v6, -0x6401d751

    if-ne v1, v6, :cond_4

    .line 1535756
    iget-object v1, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535757
    iput-boolean v10, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->i:Z

    .line 1535758
    iget-object v1, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v4, v5, v10}, LX/15i;->h(II)Z

    move-result v6

    .line 1535759
    iput-boolean v6, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->j:Z

    .line 1535760
    iget-object v1, p0, LX/9mU;->h:Landroid/widget/EditText;

    iget-object v6, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535761
    iget-object v7, v6, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    move-object v6, v7

    .line 1535762
    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1535763
    const/4 v1, 0x4

    invoke-virtual {v4, v5, v1}, LX/15i;->g(II)I

    move-result v1

    iget-object v5, p0, LX/9mU;->h:Landroid/widget/EditText;

    invoke-virtual {v4, v1, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1535764
    iget-object v1, p0, LX/9mU;->h:Landroid/widget/EditText;

    iget-object v4, p0, LX/9mU;->m:Landroid/text/TextWatcher;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1535765
    iget-object v1, p0, LX/9mU;->h:Landroid/widget/EditText;

    iget-object v4, p0, LX/9mU;->n:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_3

    .line 1535766
    :cond_4
    const-class v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v4, v5, v9, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v6, 0x44644a19

    if-ne v1, v6, :cond_3

    .line 1535767
    new-instance v6, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/9mU;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v6, v1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1535768
    invoke-virtual {v6, v2}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1535769
    invoke-virtual {p0}, LX/9mU;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v7, 0x7f0a010e

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v6, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1535770
    invoke-virtual {v4, v5, v12}, LX/15i;->g(II)I

    move-result v1

    .line 1535771
    invoke-virtual {v4, v5, v12}, LX/15i;->g(II)I

    move-result v7

    const-class v8, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;

    invoke-virtual {v4, v7, v9, v8}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v7

    .line 1535772
    invoke-virtual {v4, v1, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    if-eqz v7, :cond_5

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_4
    invoke-static {v1}, Lcom/facebook/rapidreporting/ui/Range;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    invoke-virtual {p0, v6, v8, v1}, LX/9mP;->a(Landroid/widget/TextView;Ljava/lang/String;LX/0Px;)V

    .line 1535773
    const/4 v1, 0x3

    invoke-virtual {v4, v5, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const-string v4, "alert"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1535774
    const v1, 0x7f0207c5

    invoke-virtual {p0, v6, v1, v11}, LX/9mP;->a(Landroid/widget/TextView;II)V

    .line 1535775
    :goto_5
    iget-object v1, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    iget-object v4, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 1535776
    :cond_5
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1535777
    goto :goto_4

    .line 1535778
    :cond_6
    const v1, 0x7f0208ee

    invoke-virtual {p0, v6, v1, v11}, LX/9mP;->a(Landroid/widget/TextView;II)V

    goto :goto_5

    .line 1535779
    :cond_7
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535780
    iget-boolean v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    move v0, v1

    .line 1535781
    invoke-virtual {p0, v0}, LX/9mU;->a(Z)V

    goto/16 :goto_0

    .line 1535782
    :cond_8
    iget-object v4, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535783
    iget-object v5, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v5

    .line 1535784
    check-cast v4, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v4}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->p()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v6, 0x0

    const-class v7, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    invoke-virtual {v5, v4, v6, v7}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v4

    .line 1535785
    if-eqz v4, :cond_9

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    :goto_6
    invoke-static {v4}, Lcom/facebook/rapidreporting/ui/Range;->a(Ljava/util/List;)LX/0Px;

    move-result-object v4

    goto/16 :goto_1

    .line 1535786
    :cond_9
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1535787
    goto :goto_6

    :cond_a
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535788
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v0

    .line 1535789
    check-cast v1, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->n()LX/2uF;

    move-result-object v1

    goto/16 :goto_2
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 1535790
    if-eqz p1, :cond_1

    move v0, v1

    .line 1535791
    :goto_0
    iget-object v2, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1535792
    iget-object v2, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1535793
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1535794
    :cond_0
    iget-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    const v2, 0x7f0824b6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1535795
    iget-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1535796
    iget-object v0, p0, LX/9mU;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1535797
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1535798
    :goto_2
    iget-object v2, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1535799
    iget-object v2, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1535800
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1535801
    :cond_2
    iget-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    const v1, 0x7f0824af

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1535802
    iget-object v0, p0, LX/9mP;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1535803
    iget-object v0, p0, LX/9mP;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1535804
    :cond_3
    iget-object v0, p0, LX/9mU;->k:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535805
    iget-boolean v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->i:Z

    move v0, v1

    .line 1535806
    if-nez v0, :cond_4

    .line 1535807
    iget-object v0, p0, LX/9mU;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1535808
    iget-object v0, p0, LX/9mU;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1535809
    :cond_4
    iget-object v0, p0, LX/9mU;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1535810
    iget-object v0, p0, LX/9mU;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x11545bca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1535811
    invoke-super {p0}, LX/9mP;->onDetachedFromWindow()V

    .line 1535812
    invoke-virtual {p0}, LX/9mU;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/9mU;->h:Landroid/widget/EditText;

    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1535813
    const/16 v1, 0x2d

    const v2, 0x7a4a8831

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
