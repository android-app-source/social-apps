.class public LX/9h2;
.super LX/9gr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9gr",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;",
        "Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;",
        "LX/5kF;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/9h9;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1525943
    const-class v0, LX/5kF;

    invoke-direct {p0, p1, v0, p2}, LX/9gr;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1525944
    iput-object p3, p0, LX/9h2;->b:LX/9h9;

    .line 1525945
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0gW;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525946
    new-instance v0, LX/9gK;

    invoke-direct {v0}, LX/9gK;-><init>()V

    move-object v1, v0

    .line 1525947
    const-string v0, "after_cursor"

    invoke-virtual {v1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "first_count"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "id"

    .line 1525948
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525949
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1525950
    iget-object v0, p0, LX/9h2;->b:LX/9h9;

    invoke-virtual {v0, v1}, LX/9h9;->a(LX/0gW;)LX/0gW;

    .line 1525951
    return-object v1
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;",
            ">;)",
            "LX/9fz",
            "<",
            "LX/5kF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525952
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1525953
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525954
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;

    .line 1525955
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1525956
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v5

    invoke-interface {v5}, LX/5kE;->e()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v5

    invoke-interface {v5}, LX/5kE;->l()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v5

    invoke-interface {v5}, LX/5kE;->l()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1525957
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v5

    invoke-interface {v5}, LX/5kF;->m()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    .line 1525958
    if-eqz v5, :cond_2

    move v5, v6

    :goto_1
    if-eqz v5, :cond_5

    .line 1525959
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v5

    invoke-interface {v5}, LX/5kF;->m()LX/1vs;

    move-result-object v5

    iget-object p0, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1525960
    invoke-virtual {p0, v5, v7}, LX/15i;->g(II)I

    move-result v5

    if-eqz v5, :cond_4

    move v5, v6

    :goto_2
    if-eqz v5, :cond_7

    .line 1525961
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v5

    invoke-interface {v5}, LX/5kF;->m()LX/1vs;

    move-result-object v5

    iget-object p0, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {p0, v5, v7}, LX/15i;->g(II)I

    move-result v5

    .line 1525962
    invoke-virtual {p0, v5, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    :goto_3
    move v5, v6

    .line 1525963
    if-eqz v5, :cond_0

    .line 1525964
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1525965
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 1525966
    :cond_1
    new-instance v1, LX/9fz;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1525967
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525968
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/9fz;-><init>(LX/0Px;Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;)V

    return-object v1

    :cond_2
    move v5, v7

    goto :goto_1

    :cond_3
    move v5, v7

    goto :goto_1

    :cond_4
    move v5, v7

    goto :goto_2

    :cond_5
    move v5, v7

    goto :goto_2

    :cond_6
    move v6, v7

    goto :goto_3

    :cond_7
    move v6, v7

    goto :goto_3
.end method
