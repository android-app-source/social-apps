.class public LX/9iq;
.super LX/9iR;
.source ""


# instance fields
.field private a:LX/8Hv;

.field private b:LX/75F;


# direct methods
.method public constructor <init>(LX/8Hv;LX/75F;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1528633
    invoke-direct {p0}, LX/9iR;-><init>()V

    .line 1528634
    iput-object p1, p0, LX/9iq;->a:LX/8Hv;

    .line 1528635
    iput-object p2, p0, LX/9iq;->b:LX/75F;

    .line 1528636
    return-void
.end method

.method public static c(LX/0QB;)LX/9iq;
    .locals 3

    .prologue
    .line 1528637
    new-instance v2, LX/9iq;

    const-class v0, LX/8Hv;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/8Hv;

    invoke-static {p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v1

    check-cast v1, LX/75F;

    invoke-direct {v2, v0, v1}, LX/9iq;-><init>(LX/8Hv;LX/75F;)V

    .line 1528638
    return-object v2
.end method


# virtual methods
.method public final a(LX/74w;Landroid/content/Context;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/9iQ;
    .locals 4

    .prologue
    .line 1528639
    instance-of v0, p1, LX/74x;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1528640
    new-instance v1, LX/9ip;

    invoke-direct {v1, p2}, LX/9ip;-><init>(Landroid/content/Context;)V

    .line 1528641
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1528642
    iget-object v3, p0, LX/9iq;->b:LX/75F;

    move-object v0, p1

    check-cast v0, LX/74x;

    invoke-virtual {v3, v0}, LX/75F;->b(LX/74x;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1528643
    iget-object v3, p0, LX/9iq;->b:LX/75F;

    move-object v0, p1

    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {v3, v0}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528644
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1528645
    :cond_0
    if-eqz p3, :cond_3

    .line 1528646
    iget-object v0, p0, LX/9iq;->a:LX/8Hv;

    invoke-virtual {p3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1528647
    new-instance p2, LX/8Hu;

    invoke-static {v0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(LX/0QB;)Lcom/facebook/photos/imageprocessing/FiltersEngine;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-direct {p2, v3, v2, p0}, LX/8Hu;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/photos/imageprocessing/FiltersEngine;)V

    .line 1528648
    move-object v0, p2

    .line 1528649
    iput-object v0, v1, LX/9ip;->x:LX/33B;

    .line 1528650
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528651
    iput-object p3, v1, LX/9ip;->A:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1528652
    invoke-virtual {p3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1528653
    invoke-virtual {p3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    invoke-static {v2}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v2

    .line 1528654
    if-eqz v0, :cond_2

    .line 1528655
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1528656
    new-instance v3, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1528657
    :cond_1
    iput-object v0, v1, LX/9ip;->y:Landroid/net/Uri;

    .line 1528658
    :cond_2
    if-eqz v2, :cond_3

    .line 1528659
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528660
    iput-object v2, v1, LX/9ip;->s:Landroid/graphics/RectF;

    .line 1528661
    :cond_3
    check-cast p1, LX/74x;

    invoke-virtual {v1, p1}, LX/9ip;->a(LX/74x;)V

    .line 1528662
    return-object v1
.end method
