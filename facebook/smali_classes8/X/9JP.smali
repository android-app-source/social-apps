.class public final LX/9JP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:LX/9JO;

.field public b:Z


# direct methods
.method private constructor <init>(LX/9JO;Z)V
    .locals 0

    .prologue
    .line 1464686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464687
    iput-object p1, p0, LX/9JP;->a:LX/9JO;

    .line 1464688
    iput-boolean p2, p0, LX/9JP;->b:Z

    .line 1464689
    return-void
.end method

.method public static a(LX/9JN;)LX/9JP;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1464690
    iget v0, p0, LX/9JN;->c:I

    move v3, v0

    .line 1464691
    if-eqz v3, :cond_0

    const/4 v0, 0x5

    if-lt v3, v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464692
    iget-object v0, p0, LX/9JN;->a:LX/9JM;

    .line 1464693
    iget-object v4, v0, LX/9JM;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1464694
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, v0, LX/9JM;->a:Ljava/io/File;

    const/4 p0, 0x1

    invoke-direct {v4, v5, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 1464695
    new-instance v5, LX/9JO;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v4

    invoke-direct {v5, v4}, LX/9JO;-><init>(Ljava/nio/channels/FileChannel;)V

    move-object v0, v5

    .line 1464696
    move-object v0, v0

    .line 1464697
    iget-object v5, v0, LX/9JO;->a:Ljava/nio/channels/FileChannel;

    int-to-long v7, v3

    invoke-virtual {v5, v7, v8}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    .line 1464698
    new-instance v4, LX/9JP;

    if-nez v3, :cond_2

    :goto_1
    invoke-direct {v4, v0, v2}, LX/9JP;-><init>(LX/9JO;Z)V

    return-object v4

    :cond_1
    move v0, v1

    .line 1464699
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1464700
    goto :goto_1
.end method

.method public static a(Ljava/io/File;)LX/9JP;
    .locals 3

    .prologue
    .line 1464701
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1464702
    new-instance v1, LX/9JP;

    new-instance v2, LX/9JO;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-direct {v2, v0}, LX/9JO;-><init>(Ljava/nio/channels/FileChannel;)V

    const/4 v0, 0x1

    invoke-direct {v1, v2, v0}, LX/9JP;-><init>(LX/9JO;Z)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1464703
    iget-boolean v0, p0, LX/9JP;->b:Z

    if-ne v0, v2, :cond_0

    .line 1464704
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9JP;->b:Z

    .line 1464705
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1464706
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1464707
    const v1, 0x20161109

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1464708
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1464709
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464710
    iget-object v1, p0, LX/9JP;->a:LX/9JO;

    invoke-virtual {v1, v0}, LX/9JO;->a(Ljava/nio/ByteBuffer;)V

    .line 1464711
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464712
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1464713
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1464714
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1464715
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1464716
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464717
    iget-object v1, p0, LX/9JP;->a:LX/9JO;

    invoke-virtual {v1, v0}, LX/9JO;->a(Ljava/nio/ByteBuffer;)V

    .line 1464718
    iget-object v0, p0, LX/9JP;->a:LX/9JO;

    invoke-virtual {v0, p1}, LX/9JO;->a(Ljava/nio/ByteBuffer;)V

    .line 1464719
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1464720
    iget-object v0, p0, LX/9JP;->a:LX/9JO;

    invoke-virtual {v0}, LX/9JO;->close()V

    .line 1464721
    return-void
.end method
