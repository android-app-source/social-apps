.class public final LX/9DQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

.field public final synthetic d:LX/3iK;


# direct methods
.method public constructor <init>(LX/3iK;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V
    .locals 0

    .prologue
    .line 1455598
    iput-object p1, p0, LX/9DQ;->d:LX/3iK;

    iput-object p2, p0, LX/9DQ;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9DQ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p4, p0, LX/9DQ;->c:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1455599
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1455600
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->i:LX/3iM;

    iget-object v1, p0, LX/9DQ;->a:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1, v2}, LX/3iM;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1455601
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->a:LX/1K9;

    new-instance v1, LX/8px;

    iget-object v2, p0, LX/9DQ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8px;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1455602
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->i:LX/3iM;

    iget-object v1, p0, LX/9DQ;->a:Ljava/lang/String;

    iget-object v2, p0, LX/9DQ;->c:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    .line 1455603
    iget-object p0, v0, LX/3iM;->a:Ljava/util/Map;

    invoke-interface {p0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455604
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1455605
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1455606
    if-eqz p2, :cond_1

    .line 1455607
    iget-object v0, p2, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1455608
    sget-object v1, LX/1nY;->OTHER:LX/1nY;

    invoke-virtual {v0, v1}, LX/1nY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9DQ;->c:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v0, v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    .line 1455609
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->i:LX/3iM;

    iget-object v1, p0, LX/9DQ;->a:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1, v2}, LX/3iM;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1455610
    invoke-static {p1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v0

    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    iget-object v2, p0, LX/9DQ;->c:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v2, v2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    .line 1455611
    iput-object v2, v1, LX/3dM;->y:Ljava/lang/String;

    .line 1455612
    move-object v1, v1

    .line 1455613
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1455614
    iput-object v1, v0, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455615
    move-object v0, v0

    .line 1455616
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1455617
    iget-object v1, p0, LX/9DQ;->d:LX/3iK;

    iget-object v1, v1, LX/3iK;->k:LX/3iP;

    invoke-virtual {v1, v0}, LX/3iP;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1455618
    :goto_0
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->a:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/9DQ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1455619
    return-void

    .line 1455620
    :cond_0
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->i:LX/3iM;

    iget-object v1, p0, LX/9DQ;->a:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1, v2}, LX/3iM;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1455621
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->d:LX/3iL;

    invoke-virtual {v0, p2}, LX/3iL;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    goto :goto_0

    .line 1455622
    :cond_1
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->i:LX/3iM;

    iget-object v1, p0, LX/9DQ;->a:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1, v2}, LX/3iM;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1455623
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1455624
    iget-object v0, p0, LX/9DQ;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1455625
    invoke-static {p1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v0

    iget-object v1, p0, LX/9DQ;->a:Ljava/lang/String;

    .line 1455626
    iput-object v1, v0, LX/4Vu;->B:Ljava/lang/String;

    .line 1455627
    move-object v0, v0

    .line 1455628
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p1

    .line 1455629
    :cond_0
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->a:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/9DQ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1455630
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->i:LX/3iM;

    iget-object v1, p0, LX/9DQ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3iM;->b(Ljava/lang/String;)V

    .line 1455631
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1455632
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1455633
    invoke-static {p1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v0

    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    iget-object v2, p0, LX/9DQ;->c:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v2, v2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    .line 1455634
    iput-object v2, v1, LX/3dM;->y:Ljava/lang/String;

    .line 1455635
    move-object v1, v1

    .line 1455636
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1455637
    iput-object v1, v0, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455638
    move-object v0, v0

    .line 1455639
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1455640
    iget-object v1, p0, LX/9DQ;->d:LX/3iK;

    iget-object v1, v1, LX/3iK;->i:LX/3iM;

    iget-object v2, p0, LX/9DQ;->a:Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v1, v2, v3}, LX/3iM;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1455641
    iget-object v1, p0, LX/9DQ;->d:LX/3iK;

    iget-object v1, v1, LX/3iK;->k:LX/3iP;

    invoke-virtual {v1, v0}, LX/3iP;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1455642
    iget-object v0, p0, LX/9DQ;->d:LX/3iK;

    iget-object v0, v0, LX/3iK;->a:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/9DQ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1455643
    return-void
.end method
