.class public LX/9Ch;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1K9;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Vi;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLComment;

.field public e:Z

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0QK;LX/1K9;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/Void;",
            ">;",
            "LX/1K9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1454295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1454296
    iput-object p1, p0, LX/9Ch;->f:Ljava/lang/String;

    .line 1454297
    iput-object p2, p0, LX/9Ch;->c:LX/0QK;

    .line 1454298
    iput-object p3, p0, LX/9Ch;->a:LX/1K9;

    .line 1454299
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Ch;->b:Ljava/util/List;

    .line 1454300
    return-void
.end method

.method public static a(LX/9Ch;Ljava/lang/Class;LX/6Ve;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<",
            "Ljava/lang/String;",
            ">;>(",
            "Ljava/lang/Class",
            "<TE;>;",
            "LX/6Ve",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1454301
    iget-object v0, p0, LX/9Ch;->a:LX/1K9;

    iget-object v1, p0, LX/9Ch;->f:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, p2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    .line 1454302
    iget-object v1, p0, LX/9Ch;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1454303
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1454304
    iput-object p1, p0, LX/9Ch;->d:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1454305
    if-nez p1, :cond_1

    .line 1454306
    :cond_0
    :goto_0
    return-void

    .line 1454307
    :cond_1
    iget-boolean v0, p0, LX/9Ch;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1454308
    const-class v0, LX/8q4;

    new-instance p1, LX/9Cf;

    invoke-direct {p1, p0}, LX/9Cf;-><init>(LX/9Ch;)V

    invoke-static {p0, v0, p1}, LX/9Ch;->a(LX/9Ch;Ljava/lang/Class;LX/6Ve;)V

    .line 1454309
    const-class v0, LX/8py;

    new-instance p1, LX/9Cg;

    invoke-direct {p1, p0}, LX/9Cg;-><init>(LX/9Ch;)V

    invoke-static {p0, v0, p1}, LX/9Ch;->a(LX/9Ch;Ljava/lang/Class;LX/6Ve;)V

    .line 1454310
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9Ch;->e:Z

    .line 1454311
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454312
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {p0, p1}, LX/9Ch;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    return-void
.end method
