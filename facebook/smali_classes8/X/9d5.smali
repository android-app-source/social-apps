.class public LX/9d5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:LX/5iG;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public B:LX/5iG;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

.field public D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

.field public E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public H:LX/BFg;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public I:Z

.field public J:LX/9d9;

.field public K:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public L:LX/9dI;

.field public M:Z

.field public N:Z

.field public O:I

.field private final b:LX/9cr;

.field private final c:LX/9cu;

.field private final d:LX/9cw;

.field private final e:LX/9cy;

.field public final f:LX/9cz;

.field private final g:LX/9d0;

.field private final h:LX/9d1;

.field public final i:LX/9bz;

.field private final j:LX/8GN;

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/8GH;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/9dA;

.field public final m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

.field private final n:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

.field public final o:LX/8GP;

.field private final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5iG;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Z

.field public r:Ljava/lang/String;

.field public s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

.field private t:Landroid/net/Uri;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public u:I

.field public v:I

.field public w:Z

.field public x:Z

.field public volatile y:Z

.field public z:LX/5iG;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1517367
    const-class v0, LX/9d5;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9d5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;ZLX/9dA;LX/8GN;LX/9dJ;Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/8GP;)V
    .locals 3
    .param p1    # LX/9bz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1517340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1517341
    new-instance v0, LX/9ct;

    invoke-direct {v0, p0}, LX/9ct;-><init>(LX/9d5;)V

    iput-object v0, p0, LX/9d5;->b:LX/9cr;

    .line 1517342
    new-instance v0, LX/9cv;

    invoke-direct {v0, p0}, LX/9cv;-><init>(LX/9d5;)V

    iput-object v0, p0, LX/9d5;->c:LX/9cu;

    .line 1517343
    new-instance v0, LX/9cx;

    invoke-direct {v0, p0}, LX/9cx;-><init>(LX/9d5;)V

    iput-object v0, p0, LX/9d5;->d:LX/9cw;

    .line 1517344
    new-instance v0, LX/9cy;

    invoke-direct {v0, p0}, LX/9cy;-><init>(LX/9d5;)V

    iput-object v0, p0, LX/9d5;->e:LX/9cy;

    .line 1517345
    new-instance v0, LX/9cz;

    invoke-direct {v0, p0}, LX/9cz;-><init>(LX/9d5;)V

    iput-object v0, p0, LX/9d5;->f:LX/9cz;

    .line 1517346
    new-instance v0, LX/9d0;

    invoke-direct {v0, p0}, LX/9d0;-><init>(LX/9d5;)V

    iput-object v0, p0, LX/9d5;->g:LX/9d0;

    .line 1517347
    new-instance v0, LX/9d2;

    invoke-direct {v0, p0}, LX/9d2;-><init>(LX/9d5;)V

    iput-object v0, p0, LX/9d5;->h:LX/9d1;

    .line 1517348
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/9d5;->k:Ljava/util/Set;

    .line 1517349
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/9d5;->p:Ljava/util/ArrayList;

    .line 1517350
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1517351
    iput-object v0, p0, LX/9d5;->F:LX/0Px;

    .line 1517352
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9d5;->M:Z

    .line 1517353
    iput-object p1, p0, LX/9d5;->i:LX/9bz;

    .line 1517354
    iput-boolean p5, p0, LX/9d5;->q:Z

    .line 1517355
    iput-object p6, p0, LX/9d5;->l:LX/9dA;

    .line 1517356
    iput-object p7, p0, LX/9d5;->j:LX/8GN;

    .line 1517357
    iput-object p2, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1517358
    iput-object p3, p0, LX/9d5;->t:Landroid/net/Uri;

    .line 1517359
    iput-object p4, p0, LX/9d5;->r:Ljava/lang/String;

    .line 1517360
    iput-object p9, p0, LX/9d5;->n:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    .line 1517361
    iput-object p10, p0, LX/9d5;->o:LX/8GP;

    .line 1517362
    iget-object v0, p0, LX/9d5;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1517363
    iget-object v0, p0, LX/9d5;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1517364
    iget-object v0, p0, LX/9d5;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1517365
    iget-object v0, p0, LX/9d5;->h:LX/9d1;

    iget-boolean v1, p0, LX/9d5;->q:Z

    invoke-virtual {p8, v0, v1}, LX/9dJ;->a(LX/9d1;Z)LX/9dI;

    move-result-object v0

    iput-object v0, p0, LX/9d5;->L:LX/9dI;

    .line 1517366
    return-void
.end method

.method private a(LX/8G6;Ljava/lang/String;)LX/8G6;
    .locals 1
    .param p1    # LX/8G6;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517339
    new-instance v0, LX/9d3;

    invoke-direct {v0, p0, p2, p1}, LX/9d3;-><init>(LX/9d5;Ljava/lang/String;LX/8G6;)V

    return-object v0
.end method

.method public static a$redex0(LX/9d5;LX/5iG;I)V
    .locals 2

    .prologue
    .line 1517324
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517325
    iget-object v0, p0, LX/9d5;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5iG;

    .line 1517326
    if-eqz v0, :cond_0

    .line 1517327
    iget-object v1, v0, LX/5iG;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1517328
    iget-object v1, p1, LX/5iG;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1517329
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517330
    :goto_0
    return-void

    .line 1517331
    :cond_0
    invoke-static {p0, p1, p2}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1517332
    iget-object v0, p0, LX/9d5;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1517333
    iget-object v0, p1, LX/5iG;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1517334
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    if-eqz v0, :cond_1

    .line 1517335
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1517336
    :cond_1
    iget-object v0, p1, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1517337
    invoke-virtual {v0}, LX/1aX;->d()V

    goto :goto_1

    .line 1517338
    :cond_2
    goto :goto_0
.end method

.method public static b$redex0(LX/9d5;LX/5iG;I)V
    .locals 2

    .prologue
    .line 1517315
    if-nez p1, :cond_0

    .line 1517316
    :goto_0
    return-void

    .line 1517317
    :cond_0
    iget-object v0, p0, LX/9d5;->p:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1517318
    iget-object v0, p1, LX/5iG;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1517319
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    if-eqz v0, :cond_1

    .line 1517320
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1517321
    :cond_1
    iget-object v0, p1, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1517322
    invoke-virtual {v0}, LX/1aX;->f()V

    goto :goto_1

    .line 1517323
    :cond_2
    goto :goto_0
.end method

.method public static p(LX/9d5;)V
    .locals 2

    .prologue
    .line 1517296
    iget-object v0, p0, LX/9d5;->z:LX/5iG;

    if-eqz v0, :cond_1

    .line 1517297
    iget-object v0, p0, LX/9d5;->z:LX/5iG;

    .line 1517298
    iget-object v1, v0, LX/5iG;->e:Landroid/graphics/drawable/Drawable$Callback;

    move-object v0, v1

    .line 1517299
    if-nez v0, :cond_0

    .line 1517300
    iget-object v0, p0, LX/9d5;->z:LX/5iG;

    iget-object v1, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0, v1}, LX/5iG;->a(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1517301
    :cond_0
    iget-object v0, p0, LX/9d5;->z:LX/5iG;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/9d5;->a$redex0(LX/9d5;LX/5iG;I)V

    .line 1517302
    :cond_1
    iget-object v0, p0, LX/9d5;->A:LX/5iG;

    if-eqz v0, :cond_3

    .line 1517303
    iget-object v0, p0, LX/9d5;->A:LX/5iG;

    .line 1517304
    iget-object v1, v0, LX/5iG;->e:Landroid/graphics/drawable/Drawable$Callback;

    move-object v0, v1

    .line 1517305
    if-nez v0, :cond_2

    .line 1517306
    iget-object v0, p0, LX/9d5;->A:LX/5iG;

    iget-object v1, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0, v1}, LX/5iG;->a(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1517307
    :cond_2
    iget-object v0, p0, LX/9d5;->A:LX/5iG;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/9d5;->a$redex0(LX/9d5;LX/5iG;I)V

    .line 1517308
    :cond_3
    iget-object v0, p0, LX/9d5;->B:LX/5iG;

    if-eqz v0, :cond_5

    .line 1517309
    iget-object v0, p0, LX/9d5;->B:LX/5iG;

    .line 1517310
    iget-object v1, v0, LX/5iG;->e:Landroid/graphics/drawable/Drawable$Callback;

    move-object v0, v1

    .line 1517311
    if-nez v0, :cond_4

    .line 1517312
    iget-object v0, p0, LX/9d5;->B:LX/5iG;

    iget-object v1, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0, v1}, LX/5iG;->a(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1517313
    :cond_4
    iget-object v0, p0, LX/9d5;->B:LX/5iG;

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, LX/9d5;->a$redex0(LX/9d5;LX/5iG;I)V

    .line 1517314
    :cond_5
    return-void
.end method

.method public static t(LX/9d5;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1517293
    iget-object v1, p0, LX/9d5;->F:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9d5;->F:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    iget-object v1, p0, LX/9d5;->L:LX/9dI;

    .line 1517294
    iget-boolean p0, v1, LX/9dI;->B:Z

    move v1, p0

    .line 1517295
    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(LX/9d5;)Z
    .locals 1

    .prologue
    .line 1517292
    invoke-static {p0}, LX/9d5;->t(LX/9d5;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/9d5;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9d5;->z:LX/5iG;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9d5;->B:LX/5iG;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .param p1    # LX/0Px;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9dM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1517282
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1517283
    iget-object v0, p0, LX/9d5;->J:LX/9d9;

    if-nez v0, :cond_0

    .line 1517284
    iget-object v0, p0, LX/9d5;->g:LX/9d0;

    .line 1517285
    new-instance v1, LX/9d9;

    invoke-direct {v1, v0}, LX/9d9;-><init>(LX/9d0;)V

    .line 1517286
    move-object v0, v1

    .line 1517287
    iput-object v0, p0, LX/9d5;->J:LX/9d9;

    .line 1517288
    iget-object v0, p0, LX/9d5;->J:LX/9d9;

    invoke-virtual {p0, v0}, LX/9d5;->a(LX/8GH;)V

    .line 1517289
    :cond_0
    iget-object v0, p0, LX/9d5;->J:LX/9d9;

    .line 1517290
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    iput-object v1, v0, LX/9d9;->c:LX/0Px;

    .line 1517291
    :cond_1
    return-void
.end method

.method public final a(LX/0Px;LX/8G6;Ljava/lang/String;)V
    .locals 2
    .param p2    # LX/8G6;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;",
            "LX/8G6;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1517280
    iget-object v0, p0, LX/9d5;->n:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-direct {p0, p2, p3}, LX/9d5;->a(LX/8G6;Ljava/lang/String;)LX/8G6;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(LX/0Px;LX/8G6;)V

    .line 1517281
    return-void
.end method

.method public final a(LX/0Px;Ljava/lang/String;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1517264
    iget-boolean v0, p0, LX/9d5;->w:Z

    const-string v1, "You must bind this controller before setting its swipeable params list"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1517265
    iput-object p1, p0, LX/9d5;->F:LX/0Px;

    .line 1517266
    iget-object v0, p0, LX/9d5;->t:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9d5;->F:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517267
    iget-object v0, p0, LX/9d5;->j:LX/8GN;

    invoke-virtual {v0}, LX/8GN;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/9d5;->F:LX/0Px;

    .line 1517268
    :cond_0
    iget-object v0, p0, LX/9d5;->H:LX/BFg;

    if-eqz v0, :cond_1

    .line 1517269
    iget-object v0, p0, LX/9d5;->H:LX/BFg;

    iget-object v1, p0, LX/9d5;->F:LX/0Px;

    .line 1517270
    iget-object v2, v0, LX/BFg;->a:LX/BFj;

    iget-object v2, v2, LX/BFj;->d:LX/8GL;

    if-eqz v2, :cond_1

    .line 1517271
    iget-object v2, v0, LX/BFg;->a:LX/BFj;

    iget-object v2, v2, LX/BFj;->d:LX/8GL;

    iget-object v3, v0, LX/BFg;->a:LX/BFj;

    iget-boolean v3, v3, LX/BFj;->r:Z

    iget-object v4, v0, LX/BFg;->a:LX/BFj;

    iget-object v4, v4, LX/BFj;->j:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-virtual {v2, v3, v1, v4, p1}, LX/8GL;->a(ZLX/0Px;Ljava/lang/String;Z)V

    .line 1517272
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9d5;->I:Z

    .line 1517273
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1517274
    iget-object v0, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_2

    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, LX/9d5;->a(Ljava/lang/String;)V

    .line 1517275
    :goto_1
    return-void

    .line 1517276
    :cond_2
    iget-object v0, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517277
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1517278
    goto :goto_0

    .line 1517279
    :cond_3
    iget-object v0, p0, LX/9d5;->F:LX/0Px;

    invoke-static {v0, p2}, LX/8GN;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {p0, p2}, LX/9d5;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object p2

    goto :goto_2
.end method

.method public final a(LX/8GH;)V
    .locals 1

    .prologue
    .line 1517262
    iget-object v0, p0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1517263
    return-void
.end method

.method public final a(LX/Ar0;)V
    .locals 1
    .param p1    # LX/Ar0;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517259
    iget-object v0, p0, LX/9d5;->L:LX/9dI;

    .line 1517260
    iput-object p1, v0, LX/9dI;->x:LX/Ar0;

    .line 1517261
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517256
    iget-object v0, p0, LX/9d5;->L:LX/9dI;

    .line 1517257
    iput-object p1, v0, LX/9dI;->w:Landroid/view/View$OnClickListener;

    .line 1517258
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;IIZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1517076
    if-lez p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1517077
    if-lez p3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1517078
    iput p2, p0, LX/9d5;->u:I

    .line 1517079
    iput p3, p0, LX/9d5;->v:I

    .line 1517080
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iput-object v0, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1517081
    iget-object v0, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iget-object v2, p0, LX/9d5;->d:LX/9cw;

    .line 1517082
    iput-object v2, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->j:LX/9cw;

    .line 1517083
    iget-object v0, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iget-object v2, p0, LX/9d5;->c:LX/9cu;

    .line 1517084
    iput-object v2, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->i:LX/9cu;

    .line 1517085
    iget-object v0, p0, LX/9d5;->L:LX/9dI;

    iget-object v2, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iget-object v3, p0, LX/9d5;->c:LX/9cu;

    invoke-virtual {v0, v2, v3}, LX/9dI;->a(Landroid/view/View;LX/9cu;)V

    .line 1517086
    iput-boolean v1, p0, LX/9d5;->w:Z

    .line 1517087
    iget-object v0, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v1, p0, LX/9d5;->t:Landroid/net/Uri;

    iget v2, p0, LX/9d5;->u:I

    iget v3, p0, LX/9d5;->v:I

    iget-object v4, p0, LX/9d5;->b:LX/9cr;

    iget-object v6, p0, LX/9d5;->e:LX/9cy;

    move v5, p4

    .line 1517088
    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    .line 1517089
    iput v2, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->n:I

    .line 1517090
    iput v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->o:I

    .line 1517091
    iput-object v6, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    .line 1517092
    iput-boolean v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->j:Z

    .line 1517093
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1517094
    return-void

    :cond_1
    move v0, v2

    .line 1517095
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1517159
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517160
    iget-object v0, p0, LX/9d5;->F:LX/0Px;

    invoke-static {v0, p1}, LX/8GN;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    .line 1517161
    if-eqz v0, :cond_1

    .line 1517162
    iget-object v1, p0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517163
    iget-object v2, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517164
    iget-object v3, p0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517165
    iput-object v0, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517166
    iput-object v0, p0, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517167
    iget-object v0, p0, LX/9d5;->F:LX/0Px;

    iget-object v4, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v0, v4}, LX/8GN;->b(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517168
    iget-object v0, p0, LX/9d5;->F:LX/0Px;

    iget-object v4, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v0, v4}, LX/8GN;->a(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517169
    iget-object v0, p0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v3, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1517170
    :cond_0
    iget-object v0, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const/4 v3, 0x0

    .line 1517171
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    if-nez v1, :cond_2

    .line 1517172
    :cond_1
    :goto_0
    return-void

    .line 1517173
    :cond_2
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v1}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v1}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1517174
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->l:Z

    .line 1517175
    :goto_1
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    if-nez v1, :cond_4

    .line 1517176
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v1}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    invoke-static {v0, v3, v1, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    .line 1517177
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v1}, LX/9cy;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    invoke-static {v0, v3, v1, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    .line 1517178
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v1}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    invoke-static {v0, v3, v1, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    .line 1517179
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V

    goto :goto_0

    .line 1517180
    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->l:Z

    goto :goto_1

    .line 1517181
    :cond_4
    iput-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    .line 1517182
    iput-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    .line 1517183
    iput-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    .line 1517184
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    iget-object v2, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v2}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v3}, LX/9cy;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v4}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v4

    .line 1517185
    if-eqz v2, :cond_5

    .line 1517186
    iget-object v6, v1, LX/9dC;->c:LX/9dD;

    sget-object v5, LX/5jI;->FILTER:LX/5jI;

    .line 1517187
    iget-object v7, v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v7, v7

    .line 1517188
    invoke-virtual {v5, v7}, LX/5jI;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1517189
    iget-object v5, v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1517190
    :goto_2
    invoke-virtual {v6, v5}, LX/9dD;->a(Ljava/lang/String;)V

    .line 1517191
    :cond_5
    if-eqz v3, :cond_6

    .line 1517192
    iget-object v6, v1, LX/9dC;->d:LX/9dD;

    sget-object v5, LX/5jI;->FILTER:LX/5jI;

    .line 1517193
    iget-object v7, v3, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v7, v7

    .line 1517194
    invoke-virtual {v5, v7}, LX/5jI;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1517195
    iget-object v5, v3, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1517196
    :goto_3
    invoke-virtual {v6, v5}, LX/9dD;->a(Ljava/lang/String;)V

    .line 1517197
    :cond_6
    if-eqz v4, :cond_7

    .line 1517198
    iget-object v6, v1, LX/9dC;->e:LX/9dD;

    sget-object v5, LX/5jI;->FILTER:LX/5jI;

    .line 1517199
    iget-object v7, v4, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v7, v7

    .line 1517200
    invoke-virtual {v5, v7}, LX/5jI;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1517201
    iget-object v5, v4, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1517202
    :goto_4
    invoke-virtual {v6, v5}, LX/9dD;->a(Ljava/lang/String;)V

    .line 1517203
    :cond_7
    const/4 v2, 0x0

    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 1517204
    new-instance v1, LX/1Uo;

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v3, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v3}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    .line 1517205
    iput v2, v1, LX/1Uo;->d:I

    .line 1517206
    move-object v3, v1

    .line 1517207
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    if-nez v1, :cond_8

    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    if-nez v1, :cond_8

    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    if-nez v1, :cond_8

    .line 1517208
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->h(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)LX/1cC;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    .line 1517209
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->h(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)LX/1cC;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    .line 1517210
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->h(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)LX/1cC;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    .line 1517211
    :cond_8
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    if-nez v1, :cond_9

    .line 1517212
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    new-instance v5, LX/1o9;

    iget v6, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->n:I

    iget v7, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->o:I

    invoke-direct {v5, v6, v7}, LX/1o9;-><init>(II)V

    .line 1517213
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 1517214
    move-object v4, v4

    .line 1517215
    invoke-virtual {v4, p1}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    .line 1517216
    iget-object v6, v5, LX/9dC;->d:LX/9dD;

    move-object v5, v6

    .line 1517217
    iput-object v5, v4, LX/1bX;->j:LX/33B;

    .line 1517218
    move-object v4, v4

    .line 1517219
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    invoke-virtual {v1, v4}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1517220
    invoke-virtual {v3}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    invoke-static {v1, v4}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    .line 1517221
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1aX;->a(LX/1aZ;)V

    .line 1517222
    :cond_9
    iget-boolean v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->l:Z

    if-eqz v1, :cond_c

    .line 1517223
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    if-nez v1, :cond_a

    .line 1517224
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    new-instance v5, LX/1o9;

    iget v6, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->n:I

    iget v7, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->o:I

    invoke-direct {v5, v6, v7}, LX/1o9;-><init>(II)V

    .line 1517225
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 1517226
    move-object v4, v4

    .line 1517227
    invoke-virtual {v4, p1}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    .line 1517228
    iget-object v6, v5, LX/9dC;->c:LX/9dD;

    move-object v5, v6

    .line 1517229
    iput-object v5, v4, LX/1bX;->j:LX/33B;

    .line 1517230
    move-object v4, v4

    .line 1517231
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    invoke-virtual {v1, v4}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1517232
    invoke-virtual {v3}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    invoke-static {v1, v4}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    .line 1517233
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1aX;->a(LX/1aZ;)V

    .line 1517234
    :cond_a
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    if-nez v1, :cond_b

    .line 1517235
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    new-instance v5, LX/1o9;

    iget v6, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->n:I

    iget v7, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->o:I

    invoke-direct {v5, v6, v7}, LX/1o9;-><init>(II)V

    .line 1517236
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 1517237
    move-object v4, v4

    .line 1517238
    invoke-virtual {v4, p1}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    .line 1517239
    iget-object v6, v5, LX/9dC;->e:LX/9dD;

    move-object v5, v6

    .line 1517240
    iput-object v5, v4, LX/1bX;->j:LX/33B;

    .line 1517241
    move-object v4, v4

    .line 1517242
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    invoke-virtual {v1, v4}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1517243
    invoke-virtual {v3}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->c:Landroid/content/Context;

    invoke-static {v1, v3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    .line 1517244
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->e:LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1aX;->a(LX/1aZ;)V

    .line 1517245
    :cond_b
    :goto_5
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_d

    .line 1517246
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9cr;

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v4}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    invoke-static {v0, v3, v4, v5}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v5}, LX/9cy;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    invoke-static {v0, v4, v5, v6}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    iget-object v6, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v6}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    invoke-static {v0, v5, v6, v7}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v5

    invoke-interface {v1, v3, v4, v5}, LX/9cr;->a(LX/5iG;LX/5iG;LX/5iG;)V

    .line 1517247
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 1517248
    :cond_c
    iput-object p0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    .line 1517249
    iput-object p0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    .line 1517250
    iput-object p0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    .line 1517251
    iput-object p0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    goto :goto_5

    .line 1517252
    :cond_d
    goto/16 :goto_0

    .line 1517253
    :cond_e
    sget-object v5, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v5}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 1517254
    :cond_f
    sget-object v5, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v5}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    .line 1517255
    :cond_10
    sget-object v5, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v5}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4
.end method

.method public final a(ZLandroid/view/ViewParent;)V
    .locals 1

    .prologue
    .line 1517157
    iget-object v0, p0, LX/9d5;->L:LX/9dI;

    invoke-virtual {v0, p1, p2}, LX/9dI;->a(ZLandroid/view/ViewParent;)V

    .line 1517158
    return-void
.end method

.method public final a([Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1517154
    iget-object v0, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1517155
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    invoke-virtual {p0, p1}, LX/9dC;->a([Landroid/graphics/RectF;)V

    .line 1517156
    return-void
.end method

.method public final b(LX/0Px;LX/8G6;Ljava/lang/String;)V
    .locals 2
    .param p2    # LX/8G6;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;",
            "LX/8G6;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1517151
    new-instance v0, LX/8GG;

    invoke-direct {p0, p2, p3}, LX/9d5;->a(LX/8G6;Ljava/lang/String;)LX/8G6;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/8GG;-><init>(LX/0Px;LX/8G6;)V

    .line 1517152
    iget-object v1, p0, LX/9d5;->n:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(LX/0Px;LX/8G6;)V

    .line 1517153
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1517134
    iget-object v1, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setVisibility(I)V

    .line 1517135
    iget-boolean v0, p0, LX/9d5;->y:Z

    if-ne v0, p1, :cond_1

    .line 1517136
    :goto_1
    return-void

    .line 1517137
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 1517138
    :cond_1
    iput-boolean p1, p0, LX/9d5;->y:Z

    .line 1517139
    iget-object v0, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    if-eqz v0, :cond_2

    .line 1517140
    iget-object v0, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a(Z)V

    .line 1517141
    :cond_2
    if-eqz p1, :cond_3

    .line 1517142
    invoke-static {p0}, LX/9d5;->p(LX/9d5;)V

    .line 1517143
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9d5;->x:Z

    goto :goto_1

    .line 1517144
    :cond_3
    iget-object v0, p0, LX/9d5;->z:LX/5iG;

    if-eqz v0, :cond_4

    .line 1517145
    iget-object v0, p0, LX/9d5;->z:LX/5iG;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1517146
    :cond_4
    iget-object v0, p0, LX/9d5;->A:LX/5iG;

    if-eqz v0, :cond_5

    .line 1517147
    iget-object v0, p0, LX/9d5;->A:LX/5iG;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1517148
    :cond_5
    iget-object v0, p0, LX/9d5;->B:LX/5iG;

    if-eqz v0, :cond_6

    .line 1517149
    iget-object v0, p0, LX/9d5;->B:LX/5iG;

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1517150
    :cond_6
    goto :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1517131
    iget-object v0, p0, LX/9d5;->L:LX/9dI;

    .line 1517132
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/9dI;->B:Z

    .line 1517133
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1517128
    invoke-static {p0}, LX/9d5;->t(LX/9d5;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517129
    iget-object v0, p0, LX/9d5;->L:LX/9dI;

    invoke-virtual {v0}, LX/9dI;->i()V

    .line 1517130
    :cond_0
    return-void
.end method

.method public final f()Landroid/graphics/RectF;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1517127
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1517124
    iget-object v0, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    if-eqz v0, :cond_0

    .line 1517125
    iget-object v0, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->f()V

    .line 1517126
    :cond_0
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1517119
    invoke-virtual {p0}, LX/9d5;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1517120
    const/4 v0, 0x0

    .line 1517121
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517122
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1517123
    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final k()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1517116
    invoke-virtual {p0}, LX/9d5;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1517117
    const/4 v0, 0x0

    .line 1517118
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_0
.end method

.method public final l()LX/0Px;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1517113
    iget-object v0, p0, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_0

    .line 1517114
    const/4 v0, 0x0

    .line 1517115
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 1517097
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9d5;->w:Z

    .line 1517098
    iget-object v0, p0, LX/9d5;->J:LX/9d9;

    if-eqz v0, :cond_0

    .line 1517099
    iget-object v0, p0, LX/9d5;->J:LX/9d9;

    .line 1517100
    iget-object v1, p0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1517101
    :cond_0
    iget-object v0, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1517102
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1517103
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    invoke-virtual {v1}, LX/9dC;->d()V

    .line 1517104
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->i:Z

    .line 1517105
    iget-object v0, p0, LX/9d5;->L:LX/9dI;

    const/4 v2, 0x0

    .line 1517106
    iget-object v1, v0, LX/9dI;->u:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1517107
    iget-object v1, v0, LX/9dI;->p:LX/8Ge;

    invoke-virtual {v1}, LX/8Ge;->a()V

    .line 1517108
    iget-object v1, v0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1517109
    iput-object v2, v0, LX/9dI;->z:LX/9cu;

    .line 1517110
    iput-object v2, v0, LX/9dI;->u:Landroid/view/View;

    .line 1517111
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1517112
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1517096
    iget-boolean v0, p0, LX/9d5;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
