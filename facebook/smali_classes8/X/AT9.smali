.class public LX/AT9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ASn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j3;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "LX/ASn;"
    }
.end annotation


# instance fields
.field private final a:LX/ASg;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final d:LX/0ad;

.field private e:Lcom/facebook/composer/attachments/ComposerAttachment;

.field private f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;


# direct methods
.method public constructor <init>(LX/ASg;Landroid/content/Context;LX/0ad;LX/0il;)V
    .locals 2
    .param p4    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ASg;",
            "Landroid/content/Context;",
            "LX/0ad;",
            "TServices;)V"
        }
    .end annotation

    .prologue
    .line 1675277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1675278
    iput-object p1, p0, LX/AT9;->a:LX/ASg;

    .line 1675279
    iput-object p2, p0, LX/AT9;->b:Landroid/content/Context;

    .line 1675280
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AT9;->c:Ljava/lang/ref/WeakReference;

    .line 1675281
    iput-object p3, p0, LX/AT9;->d:LX/0ad;

    .line 1675282
    new-instance v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    iget-object v1, p0, LX/AT9;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    .line 1675283
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1675284
    const/4 v0, 0x0

    iput-object v0, p0, LX/AT9;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675285
    iget-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    .line 1675286
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1675287
    const/4 p0, 0x0

    .line 1675288
    iput p0, v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->d:F

    .line 1675289
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1675290
    iget-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    .line 1675291
    iput p1, v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->c:F

    .line 1675292
    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->setScaleX(F)V

    .line 1675293
    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->setScaleY(F)V

    .line 1675294
    iget-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->setAlpha(F)V

    .line 1675295
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1675322
    return-void
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 9

    .prologue
    .line 1675296
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    iput-object v0, p0, LX/AT9;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675297
    iget-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    iget-object v1, p0, LX/AT9;->a:LX/ASg;

    iget-object v2, p0, LX/AT9;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ASg;->a(Lcom/facebook/ipc/media/MediaItem;)F

    move-result v1

    .line 1675298
    iput v1, v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->d:F

    .line 1675299
    iget-object v1, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v0}, Lcom/facebook/photos/base/media/VideoItem;->s()Landroid/net/Uri;

    move-result-object v0

    const/4 v7, 0x1

    .line 1675300
    new-instance v3, LX/2oE;

    invoke-direct {v3}, LX/2oE;-><init>()V

    .line 1675301
    iput-object v2, v3, LX/2oE;->a:Landroid/net/Uri;

    .line 1675302
    move-object v3, v3

    .line 1675303
    sget-object v4, LX/097;->FROM_STREAM:LX/097;

    .line 1675304
    iput-object v4, v3, LX/2oE;->e:LX/097;

    .line 1675305
    move-object v3, v3

    .line 1675306
    invoke-virtual {v3}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v3

    .line 1675307
    new-instance v4, LX/2oH;

    invoke-direct {v4}, LX/2oH;-><init>()V

    invoke-virtual {v4, v3}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v3

    .line 1675308
    iput-boolean v7, v3, LX/2oH;->g:Z

    .line 1675309
    move-object v3, v3

    .line 1675310
    invoke-virtual {v3}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v3

    .line 1675311
    new-instance v4, LX/2pZ;

    invoke-direct {v4}, LX/2pZ;-><init>()V

    .line 1675312
    iput-object v3, v4, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1675313
    move-object v3, v4

    .line 1675314
    iget v4, v1, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->d:F

    float-to-double v5, v4

    .line 1675315
    iput-wide v5, v3, LX/2pZ;->e:D

    .line 1675316
    move-object v3, v3

    .line 1675317
    const-string v4, "CoverImageParamsKey"

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v3

    invoke-virtual {v3}, LX/2pZ;->b()LX/2pa;

    move-result-object v3

    .line 1675318
    iget-object v4, v1, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4, v3}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1675319
    iget-object v3, v1, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v3, v7, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1675320
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/data/MediaData;Z)V
    .locals 0

    .prologue
    .line 1675321
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1675274
    iget-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    return-object v0
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1675275
    iget-object v0, p0, LX/AT9;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1675276
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-static {v0}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1675273
    iget-object v0, p0, LX/AT9;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    return-object v0
.end method

.method public final c(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 1

    .prologue
    .line 1675271
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    iput-object v0, p0, LX/AT9;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675272
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1675270
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1675269
    return-void
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1675266
    iget-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    .line 1675267
    iget p0, v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->d:F

    move v0, p0

    .line 1675268
    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1675261
    iget-object v0, p0, LX/AT9;->f:Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;

    .line 1675262
    iget p0, v0, Lcom/facebook/composer/ui/underwood/TranscodedGifVideoAttachmentView;->c:F

    move v0, p0

    .line 1675263
    return v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1675265
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 1675264
    return-void
.end method
