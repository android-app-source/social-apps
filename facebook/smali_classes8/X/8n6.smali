.class public final LX/8n6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/tagging/model/TaggingProfile;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8n8;


# direct methods
.method public constructor <init>(LX/8n8;)V
    .locals 0

    .prologue
    .line 1400511
    iput-object p1, p0, LX/8n6;->a:LX/8n8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1400512
    check-cast p1, Lcom/facebook/tagging/model/TaggingProfile;

    check-cast p2, Lcom/facebook/tagging/model/TaggingProfile;

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 1400513
    instance-of v2, p1, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    if-eqz v2, :cond_1

    .line 1400514
    :cond_0
    :goto_0
    return v0

    .line 1400515
    :cond_1
    instance-of v2, p2, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    if-eqz v2, :cond_2

    move v0, v1

    .line 1400516
    goto :goto_0

    .line 1400517
    :cond_2
    sget-object v2, LX/8nE;->COMMENT_AUTHORS:LX/8nE;

    invoke-virtual {v2}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1400518
    iget-object v3, p1, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    move-object v3, v3

    .line 1400519
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1400520
    sget-object v0, LX/8nE;->COMMENT_AUTHORS:LX/8nE;

    invoke-virtual {v0}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1400521
    iget-object v2, p2, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1400522
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1400523
    goto :goto_0

    .line 1400524
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
