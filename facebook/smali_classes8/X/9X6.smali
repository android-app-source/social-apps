.class public final enum LX/9X6;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9X6;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9X6;

.field public static final enum EVENT_ADMIN_CONTACT_INBOX_TAPPED_BAN_USER:LX/9X6;

.field public static final enum EVENT_ADMIN_CONTACT_INBOX_TAPPED_ENTRY_POINT:LX/9X6;

.field public static final enum EVENT_ADMIN_CONTACT_INBOX_TAPPED_ONE_REQUEST:LX/9X6;

.field public static final enum EVENT_ADMIN_CONTACT_INBOX_TAPPED_PHONE_NUMBER:LX/9X6;

.field public static final enum EVENT_ADMIN_CONTACT_INBOX_TAPPED_SEND_EMAIL:LX/9X6;

.field public static final enum EVENT_ADMIN_CONTACT_INBOX_TOGGLE_RESPONDED:LX/9X6;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502318
    new-instance v0, LX/9X6;

    const-string v1, "EVENT_ADMIN_CONTACT_INBOX_TAPPED_ENTRY_POINT"

    const-string v2, "custom_cta_mobile_contact_us_tapped_all_requests"

    invoke-direct {v0, v1, v4, v2}, LX/9X6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_ENTRY_POINT:LX/9X6;

    .line 1502319
    new-instance v0, LX/9X6;

    const-string v1, "EVENT_ADMIN_CONTACT_INBOX_TAPPED_ONE_REQUEST"

    const-string v2, "custom_cta_mobile_contact_us_tapped_request"

    invoke-direct {v0, v1, v5, v2}, LX/9X6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_ONE_REQUEST:LX/9X6;

    .line 1502320
    new-instance v0, LX/9X6;

    const-string v1, "EVENT_ADMIN_CONTACT_INBOX_TAPPED_SEND_EMAIL"

    const-string v2, "custom_cta_mobile_contact_us_tapped_email"

    invoke-direct {v0, v1, v6, v2}, LX/9X6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_SEND_EMAIL:LX/9X6;

    .line 1502321
    new-instance v0, LX/9X6;

    const-string v1, "EVENT_ADMIN_CONTACT_INBOX_TAPPED_PHONE_NUMBER"

    const-string v2, "custom_cta_mobile_contact_us_tapped_phone"

    invoke-direct {v0, v1, v7, v2}, LX/9X6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_PHONE_NUMBER:LX/9X6;

    .line 1502322
    new-instance v0, LX/9X6;

    const-string v1, "EVENT_ADMIN_CONTACT_INBOX_TOGGLE_RESPONDED"

    const-string v2, "custom_cta_mobile_contact_us_toggle_responded"

    invoke-direct {v0, v1, v8, v2}, LX/9X6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TOGGLE_RESPONDED:LX/9X6;

    .line 1502323
    new-instance v0, LX/9X6;

    const-string v1, "EVENT_ADMIN_CONTACT_INBOX_TAPPED_BAN_USER"

    const/4 v2, 0x5

    const-string v3, "custom_cta_mobile_contact_us_tapped_ban_from_page"

    invoke-direct {v0, v1, v2, v3}, LX/9X6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_BAN_USER:LX/9X6;

    .line 1502324
    const/4 v0, 0x6

    new-array v0, v0, [LX/9X6;

    sget-object v1, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_ENTRY_POINT:LX/9X6;

    aput-object v1, v0, v4

    sget-object v1, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_ONE_REQUEST:LX/9X6;

    aput-object v1, v0, v5

    sget-object v1, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_SEND_EMAIL:LX/9X6;

    aput-object v1, v0, v6

    sget-object v1, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_PHONE_NUMBER:LX/9X6;

    aput-object v1, v0, v7

    sget-object v1, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TOGGLE_RESPONDED:LX/9X6;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_BAN_USER:LX/9X6;

    aput-object v2, v0, v1

    sput-object v0, LX/9X6;->$VALUES:[LX/9X6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502312
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502313
    iput-object p3, p0, LX/9X6;->mEventName:Ljava/lang/String;

    .line 1502314
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9X6;
    .locals 1

    .prologue
    .line 1502317
    const-class v0, LX/9X6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9X6;

    return-object v0
.end method

.method public static values()[LX/9X6;
    .locals 1

    .prologue
    .line 1502325
    sget-object v0, LX/9X6;->$VALUES:[LX/9X6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9X6;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502316
    iget-object v0, p0, LX/9X6;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502315
    sget-object v0, LX/9XC;->ADMIN_CONTACT_INBOX:LX/9XC;

    return-object v0
.end method
