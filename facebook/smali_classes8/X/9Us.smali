.class public LX/9Us;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Hk;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Hk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1498567
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Hk;
    .locals 4

    .prologue
    .line 1498568
    sget-object v0, LX/9Us;->a:LX/1Hk;

    if-nez v0, :cond_1

    .line 1498569
    const-class v1, LX/9Us;

    monitor-enter v1

    .line 1498570
    :try_start_0
    sget-object v0, LX/9Us;->a:LX/1Hk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1498571
    if-eqz v2, :cond_0

    .line 1498572
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1498573
    invoke-static {v0}, LX/9Ur;->a(LX/0QB;)LX/1Gf;

    move-result-object v3

    check-cast v3, LX/1Gf;

    invoke-static {v0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object p0

    check-cast p0, LX/1Ft;

    invoke-static {v3, p0}, LX/9V2;->a(LX/1Gf;LX/1Ft;)LX/1Hk;

    move-result-object v3

    move-object v0, v3

    .line 1498574
    sput-object v0, LX/9Us;->a:LX/1Hk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1498575
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1498576
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1498577
    :cond_1
    sget-object v0, LX/9Us;->a:LX/1Hk;

    return-object v0

    .line 1498578
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1498579
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1498580
    invoke-static {p0}, LX/9Ur;->a(LX/0QB;)LX/1Gf;

    move-result-object v0

    check-cast v0, LX/1Gf;

    invoke-static {p0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v1

    check-cast v1, LX/1Ft;

    invoke-static {v0, v1}, LX/9V2;->a(LX/1Gf;LX/1Ft;)LX/1Hk;

    move-result-object v0

    return-object v0
.end method
