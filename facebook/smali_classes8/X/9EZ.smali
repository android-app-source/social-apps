.class public final LX/9EZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/9Ea;


# direct methods
.method public constructor <init>(LX/9Ea;)V
    .locals 0

    .prologue
    .line 1457154
    iput-object p1, p0, LX/9EZ;->a:LX/9Ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static c(LX/9EZ;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 4

    .prologue
    .line 1457140
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1457141
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1457142
    :goto_0
    iget-object v1, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457143
    :cond_0
    :goto_1
    return-void

    .line 1457144
    :cond_1
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0, p1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1457145
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->i:LX/20j;

    iget-object v1, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, p1}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0

    .line 1457146
    :cond_2
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->s:LX/9EL;

    if-eqz v0, :cond_0

    .line 1457147
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->s:LX/9EL;

    .line 1457148
    iget-object v1, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_3

    .line 1457149
    iget-object v2, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, v0, LX/9EL;->b:LX/20j;

    iget-object v1, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457150
    iget-object p0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, p0

    .line 1457151
    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3, v1, p1}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iput-object v1, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457152
    iget-object v1, v0, LX/9EL;->a:LX/9CC;

    iget-object v2, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/9CC;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1457153
    :cond_3
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 7

    .prologue
    .line 1457111
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 1457112
    iget-object v1, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1, p1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->s:LX/9EL;

    if-eqz v0, :cond_2

    .line 1457113
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->s:LX/9EL;

    .line 1457114
    iget-object v1, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_0

    .line 1457115
    iget-object v2, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, v0, LX/9EL;->b:LX/20j;

    iget-object v1, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457116
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v4

    .line 1457117
    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3, v1, p1}, LX/20j;->c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iput-object v1, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457118
    iget-object v1, v0, LX/9EL;->a:LX/9CC;

    iget-object v2, v0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/9CC;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1457119
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 1457120
    if-nez p1, :cond_3

    .line 1457121
    :cond_1
    :goto_1
    return-void

    .line 1457122
    :cond_2
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->i:LX/20j;

    iget-object v1, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, p1}, LX/20j;->c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1457123
    iget-object v1, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1457124
    :cond_3
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->p:LX/1Ar;

    invoke-virtual {v0}, LX/1Ar;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1457125
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 1457126
    if-eqz v3, :cond_1

    .line 1457127
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 1457128
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 1457129
    if-eqz v0, :cond_4

    .line 1457130
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 1457131
    if-eqz v5, :cond_4

    .line 1457132
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    .line 1457133
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, -0x36bd2417

    if-ne v5, v6, :cond_4

    if-eqz v0, :cond_4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->NEW_YEARS:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v0, v5}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1457134
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->r:LX/0Zb;

    const-string v1, "factory_delights_nye_2017_comment_submitted"

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1457135
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1457136
    iget-object v0, p0, LX/9EZ;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->q:Lcom/facebook/delights/floating/DelightsFireworks;

    .line 1457137
    iget-object v1, v0, Lcom/facebook/delights/floating/DelightsFireworks;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/facebook/delights/floating/DelightsFireworks;->b(Landroid/content/Context;)V

    .line 1457138
    goto :goto_1

    .line 1457139
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method
