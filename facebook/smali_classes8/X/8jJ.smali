.class public LX/8jJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0aG;


# direct methods
.method public constructor <init>(LX/0Or;LX/0aG;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/data/CanSaveStickerAssetsToDisk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0aG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392326
    iput-object p1, p0, LX/8jJ;->a:LX/0Or;

    .line 1392327
    iput-object p2, p0, LX/8jJ;->b:LX/0aG;

    .line 1392328
    return-void
.end method

.method public static b(LX/0QB;)LX/8jJ;
    .locals 3

    .prologue
    .line 1392315
    new-instance v1, LX/8jJ;

    const/16 v0, 0x157e

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-direct {v1, v2, v0}, LX/8jJ;-><init>(LX/0Or;LX/0aG;)V

    .line 1392316
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/3e1;",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1392317
    iget-object v0, p0, LX/8jJ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1392318
    const/4 v0, 0x0

    .line 1392319
    :goto_0
    return-object v0

    .line 1392320
    :cond_0
    invoke-virtual {p2}, LX/3e1;->getDbName()Ljava/lang/String;

    .line 1392321
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1392322
    new-instance v0, Lcom/facebook/stickers/service/SaveStickerAssetParams;

    invoke-virtual {p2}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1, p3}, Lcom/facebook/stickers/service/SaveStickerAssetParams;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 1392323
    const-string v1, "SaveStickerAssetParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1392324
    iget-object v0, p0, LX/8jJ;->b:LX/0aG;

    const-string v1, "download_sticker_asset"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v5, -0x9ad5a0f

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    goto :goto_0
.end method
