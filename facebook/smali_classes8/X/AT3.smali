.class public LX/AT3;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1675081
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1675082
    return-void
.end method


# virtual methods
.method public final a(LX/0il;LX/ATL;Ljava/lang/String;)Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            "Services::",
            "LX/0il",
            "<TModelData;>;>(TServices;",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController",
            "<TModelData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1675083
    new-instance v0, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {p0}, LX/ASg;->a(LX/0QB;)LX/ASg;

    move-result-object v6

    check-cast v6, LX/ASg;

    invoke-static {p0}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v7

    check-cast v7, LX/7Dh;

    const/16 v1, 0x2d9

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v9}, Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;-><init>(LX/0il;LX/ATL;Ljava/lang/String;Landroid/content/Context;LX/1Ad;LX/ASg;LX/7Dh;LX/0Ot;LX/0W3;)V

    .line 1675084
    return-object v0
.end method
