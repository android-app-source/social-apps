.class public LX/8pf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pe;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1406241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406242
    iput-object p1, p0, LX/8pf;->a:LX/0Ot;

    .line 1406243
    return-void
.end method

.method public static a(LX/0QB;)LX/8pf;
    .locals 4

    .prologue
    .line 1406210
    const-class v1, LX/8pf;

    monitor-enter v1

    .line 1406211
    :try_start_0
    sget-object v0, LX/8pf;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1406212
    sput-object v2, LX/8pf;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1406213
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406214
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1406215
    new-instance v3, LX/8pf;

    const/16 p0, 0xafc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8pf;-><init>(LX/0Ot;)V

    .line 1406216
    move-object v0, v3

    .line 1406217
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1406218
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8pf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1406219
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1406220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/89l;
    .locals 1

    .prologue
    .line 1406240
    sget-object v0, LX/89l;->PROFILES:LX/89l;

    return-object v0
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;LX/0TF;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406231
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a()LX/0Px;

    move-result-object v7

    .line 1406232
    if-eqz v7, :cond_0

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1406233
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_0

    invoke-virtual {v7, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1406234
    iget-object v1, p0, LX/8pf;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    .line 1406235
    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v3

    .line 1406236
    new-instance v4, LX/8pd;

    invoke-direct {v4, p0, p3}, LX/8pd;-><init>(LX/8pf;LX/0TF;)V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2, v3}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1406237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 1406238
    invoke-virtual/range {v0 .. v5}, LX/8pf;->a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1406239
    return-void
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1406224
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a()LX/0Px;

    move-result-object v0

    .line 1406225
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1406226
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "No actors for profiles type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V

    .line 1406227
    :goto_0
    return-void

    .line 1406228
    :cond_1
    invoke-static {v3, v3, v2, v2}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 1406229
    new-instance v2, LX/44w;

    invoke-direct {v2, v0, v1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1406230
    invoke-virtual {p2, v2}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1406223
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1406221
    iget-object v0, p0, LX/8pf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1406222
    return-void
.end method
