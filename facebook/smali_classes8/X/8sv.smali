.class public final enum LX/8sv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8sv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8sv;

.field public static final enum DEFERRING:LX/8sv;

.field public static final enum INTERCEPTING:LX/8sv;

.field public static final enum MONITORING:LX/8sv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1411905
    new-instance v0, LX/8sv;

    const-string v1, "MONITORING"

    invoke-direct {v0, v1, v2}, LX/8sv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8sv;->MONITORING:LX/8sv;

    .line 1411906
    new-instance v0, LX/8sv;

    const-string v1, "INTERCEPTING"

    invoke-direct {v0, v1, v3}, LX/8sv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8sv;->INTERCEPTING:LX/8sv;

    .line 1411907
    new-instance v0, LX/8sv;

    const-string v1, "DEFERRING"

    invoke-direct {v0, v1, v4}, LX/8sv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8sv;->DEFERRING:LX/8sv;

    .line 1411908
    const/4 v0, 0x3

    new-array v0, v0, [LX/8sv;

    sget-object v1, LX/8sv;->MONITORING:LX/8sv;

    aput-object v1, v0, v2

    sget-object v1, LX/8sv;->INTERCEPTING:LX/8sv;

    aput-object v1, v0, v3

    sget-object v1, LX/8sv;->DEFERRING:LX/8sv;

    aput-object v1, v0, v4

    sput-object v0, LX/8sv;->$VALUES:[LX/8sv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1411909
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8sv;
    .locals 1

    .prologue
    .line 1411910
    const-class v0, LX/8sv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8sv;

    return-object v0
.end method

.method public static values()[LX/8sv;
    .locals 1

    .prologue
    .line 1411911
    sget-object v0, LX/8sv;->$VALUES:[LX/8sv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8sv;

    return-object v0
.end method
