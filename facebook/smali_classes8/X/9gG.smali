.class public final LX/9gG;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1523298
    const-class v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionComponentModel;

    const v0, -0x3a3a73b0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "MediaFetchFromReactionComponent"

    const-string v6, "95c05e19e5be92a7306afe4d37dedac7"

    const-string v7, "node"

    const-string v8, "10155211181701729"

    const-string v9, "10155259089396729"

    .line 1523299
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1523300
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1523301
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1523308
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1523309
    sparse-switch v0, :sswitch_data_0

    .line 1523310
    :goto_0
    return-object p1

    .line 1523311
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1523312
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1523313
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1523314
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1523315
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1523316
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1523317
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1523318
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1523319
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1523320
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1523321
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1523322
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1523323
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1523324
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1523325
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1523326
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1523327
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1523328
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1523329
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1523330
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1523331
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1523332
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_15
        -0x680de62a -> :sswitch_a
        -0x6326fdb3 -> :sswitch_f
        -0x51484e72 -> :sswitch_4
        -0x4620c181 -> :sswitch_14
        -0x4496acc9 -> :sswitch_b
        -0x43eadc34 -> :sswitch_12
        -0x421ba035 -> :sswitch_9
        -0x3c54de38 -> :sswitch_11
        -0x2c889631 -> :sswitch_10
        -0x2a0a3d40 -> :sswitch_6
        -0x1b87b280 -> :sswitch_e
        -0x12efdeb3 -> :sswitch_c
        -0x93a55fc -> :sswitch_7
        0xd1b -> :sswitch_8
        0x101fb19 -> :sswitch_1
        0xa1fa812 -> :sswitch_0
        0x214100e0 -> :sswitch_d
        0x3052e0ff -> :sswitch_2
        0x73a026b5 -> :sswitch_13
        0x7824fd38 -> :sswitch_3
        0x7c7626df -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1523302
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1523303
    :goto_1
    return v0

    .line 1523304
    :sswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "20"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1523305
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1523306
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1523307
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0x35 -> :sswitch_1
        0x63e -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
