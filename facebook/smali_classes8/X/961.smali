.class public LX/961;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/17V;

.field private final c:LX/20i;

.field private final d:LX/1Ck;

.field public final e:LX/3iV;

.field public final f:LX/25G;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17V;LX/20i;LX/1Ck;LX/3iV;LX/25G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1438081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1438082
    iput-object p1, p0, LX/961;->a:LX/0Zb;

    .line 1438083
    iput-object p2, p0, LX/961;->b:LX/17V;

    .line 1438084
    iput-object p3, p0, LX/961;->c:LX/20i;

    .line 1438085
    iput-object p4, p0, LX/961;->d:LX/1Ck;

    .line 1438086
    iput-object p5, p0, LX/961;->e:LX/3iV;

    .line 1438087
    iput-object p6, p0, LX/961;->f:LX/25G;

    .line 1438088
    return-void
.end method

.method public static b(LX/0QB;)LX/961;
    .locals 7

    .prologue
    .line 1438089
    new-instance v0, LX/961;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v2

    check-cast v2, LX/17V;

    invoke-static {p0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v3

    check-cast v3, LX/20i;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v5

    check-cast v5, LX/3iV;

    invoke-static {p0}, LX/25G;->b(LX/0QB;)LX/25G;

    move-result-object v6

    check-cast v6, LX/25G;

    invoke-direct/range {v0 .. v6}, LX/961;-><init>(LX/0Zb;LX/17V;LX/20i;LX/1Ck;LX/3iV;LX/25G;)V

    .line 1438090
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1L9;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            "LX/1L9",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1438091
    if-eqz p5, :cond_0

    .line 1438092
    invoke-interface {p5, p1}, LX/1L9;->a(Ljava/lang/Object;)V

    .line 1438093
    :cond_0
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a()LX/5HA;

    move-result-object v0

    .line 1438094
    iput-object p4, v0, LX/5HA;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1438095
    move-object v1, v0

    .line 1438096
    if-nez p3, :cond_1

    move-object v0, p1

    .line 1438097
    :goto_0
    iput-object v0, v1, LX/5HA;->a:Ljava/lang/String;

    .line 1438098
    move-object v0, v1

    .line 1438099
    iput-boolean p2, v0, LX/5HA;->b:Z

    .line 1438100
    move-object v0, v0

    .line 1438101
    iget-object v1, p0, LX/961;->c:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1438102
    iput-object v1, v0, LX/5HA;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1438103
    move-object v0, v0

    .line 1438104
    iput-object p3, v0, LX/5HA;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1438105
    move-object v0, v0

    .line 1438106
    invoke-virtual {v0}, LX/5HA;->a()Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    move-result-object v0

    .line 1438107
    iget-object v1, p0, LX/961;->d:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_key_toggle_post_like"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/95z;

    invoke-direct {v3, p0, p3, v0}, LX/95z;-><init>(LX/961;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)V

    new-instance v0, LX/960;

    invoke-direct {v0, p0, p5, p1}, LX/960;-><init>(LX/961;LX/1L9;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1438108
    return-void

    .line 1438109
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/162;",
            "Z",
            "LX/1L9",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1438110
    if-eqz p9, :cond_0

    .line 1438111
    move-object/from16 v0, p9

    invoke-interface {v0, p1}, LX/1L9;->a(Ljava/lang/Object;)V

    .line 1438112
    :cond_0
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a()LX/5H8;

    move-result-object v3

    invoke-virtual {v3, p1}, LX/5H8;->a(Ljava/lang/String;)LX/5H8;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/5H8;->a(Z)LX/5H8;

    move-result-object v3

    iget-object v4, p0, LX/961;->c:LX/20i;

    invoke-virtual {v4}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/5H8;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/5H8;

    move-result-object v3

    new-instance v4, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object/from16 v0, p7

    move-object/from16 v1, p5

    move-object/from16 v2, p4

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/5H8;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/5H8;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, LX/5H8;->b(Ljava/lang/String;)LX/5H8;

    move-result-object v3

    invoke-virtual {v3}, LX/5H8;->a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    move-result-object v3

    .line 1438113
    if-eqz p3, :cond_1

    .line 1438114
    iget-object v4, p0, LX/961;->a:LX/0Zb;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, p1, v5, v1}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438115
    :cond_1
    iget-object v10, p0, LX/961;->d:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "task_key_toggle_page_like"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v12, LX/95x;

    move/from16 v0, p8

    invoke-direct {v12, p0, v0, v3}, LX/95x;-><init>(LX/961;ZLcom/facebook/api/ufiservices/common/TogglePageLikeParams;)V

    new-instance v3, LX/95y;

    move-object v4, p0

    move-object/from16 v5, p9

    move-object v6, p1

    move-object/from16 v7, p3

    move v8, p2

    move-object/from16 v9, p4

    invoke-direct/range {v3 .. v9}, LX/95y;-><init>(LX/961;LX/1L9;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v10, v11, v12, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1438116
    return-void
.end method
