.class public final LX/91W;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/91W;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/91U;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/91b;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1431044
    const/4 v0, 0x0

    sput-object v0, LX/91W;->a:LX/91W;

    .line 1431045
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/91W;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1431041
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1431042
    new-instance v0, LX/91b;

    invoke-direct {v0}, LX/91b;-><init>()V

    iput-object v0, p0, LX/91W;->c:LX/91b;

    .line 1431043
    return-void
.end method

.method public static c(LX/1De;)LX/91U;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1431033
    new-instance v1, LX/91V;

    invoke-direct {v1}, LX/91V;-><init>()V

    .line 1431034
    sget-object v2, LX/91W;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/91U;

    .line 1431035
    if-nez v2, :cond_0

    .line 1431036
    new-instance v2, LX/91U;

    invoke-direct {v2}, LX/91U;-><init>()V

    .line 1431037
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/91U;->a$redex0(LX/91U;LX/1De;IILX/91V;)V

    .line 1431038
    move-object v1, v2

    .line 1431039
    move-object v0, v1

    .line 1431040
    return-object v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 1

    .prologue
    .line 1431027
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1431028
    if-nez v0, :cond_0

    .line 1431029
    const/4 v0, 0x0

    .line 1431030
    :goto_0
    return-object v0

    .line 1431031
    :cond_0
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1431032
    check-cast v0, LX/91V;

    iget-object v0, v0, LX/91V;->x:LX/1dQ;

    goto :goto_0
.end method

.method public static declared-synchronized q()LX/91W;
    .locals 2

    .prologue
    .line 1431023
    const-class v1, LX/91W;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/91W;->a:LX/91W;

    if-nez v0, :cond_0

    .line 1431024
    new-instance v0, LX/91W;

    invoke-direct {v0}, LX/91W;-><init>()V

    sput-object v0, LX/91W;->a:LX/91W;

    .line 1431025
    :cond_0
    sget-object v0, LX/91W;->a:LX/91W;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1431026
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1431021
    invoke-static {}, LX/1dS;->b()V

    .line 1431022
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 29

    .prologue
    const/16 v1, 0x8

    const/16 v2, 0x1e

    const v3, -0x31d48f62

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v28

    .line 1431018
    check-cast p6, LX/91V;

    .line 1431019
    move-object/from16 v0, p6

    iget-object v5, v0, LX/91V;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p6

    iget-object v6, v0, LX/91V;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p6

    iget-object v7, v0, LX/91V;->c:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p6

    iget v8, v0, LX/91V;->d:I

    move-object/from16 v0, p6

    iget v9, v0, LX/91V;->e:I

    move-object/from16 v0, p6

    iget v10, v0, LX/91V;->f:F

    move-object/from16 v0, p6

    iget v11, v0, LX/91V;->g:F

    move-object/from16 v0, p6

    iget v12, v0, LX/91V;->h:F

    move-object/from16 v0, p6

    iget v13, v0, LX/91V;->i:I

    move-object/from16 v0, p6

    iget-boolean v14, v0, LX/91V;->j:Z

    move-object/from16 v0, p6

    iget v15, v0, LX/91V;->k:I

    move-object/from16 v0, p6

    iget-object v0, v0, LX/91V;->l:Landroid/content/res/ColorStateList;

    move-object/from16 v16, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->m:I

    move/from16 v17, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/91V;->n:Landroid/content/res/ColorStateList;

    move-object/from16 v18, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->o:I

    move/from16 v19, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->p:I

    move/from16 v20, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->q:I

    move/from16 v21, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->r:F

    move/from16 v22, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->s:F

    move/from16 v23, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->t:I

    move/from16 v24, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/91V;->u:Landroid/graphics/Typeface;

    move-object/from16 v25, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/91V;->v:Landroid/text/Layout$Alignment;

    move-object/from16 v26, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/91V;->w:I

    move/from16 v27, v0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move/from16 v3, p4

    move-object/from16 v4, p5

    invoke-static/range {v1 .. v27}, LX/91b;->a(LX/1De;IILX/1no;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;I)V

    .line 1431020
    const/16 v1, 0x8

    const/16 v2, 0x1f

    const v3, 0x30ec3a28

    move/from16 v0, v28

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1431016
    new-instance v0, LX/91a;

    invoke-direct {v0, p1}, LX/91a;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1431017
    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1430945
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 1430946
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 1430947
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 1430948
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 1430949
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 1430950
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 1430951
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    .line 1430952
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v9

    .line 1430953
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v10

    .line 1430954
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v11

    .line 1430955
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v12

    .line 1430956
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v13

    .line 1430957
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v14

    .line 1430958
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v15

    .line 1430959
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v16

    .line 1430960
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v17

    .line 1430961
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v18

    move-object/from16 v1, p1

    .line 1430962
    invoke-static/range {v1 .. v18}, LX/91b;->a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V

    .line 1430963
    check-cast p2, LX/91V;

    .line 1430964
    invoke-virtual {v2}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1430965
    invoke-virtual {v2}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/91V;->c:Landroid/text/TextUtils$TruncateAt;

    .line 1430966
    :cond_0
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 1430967
    invoke-virtual {v3}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1430968
    invoke-virtual {v3}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->s:F

    .line 1430969
    :cond_1
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 1430970
    invoke-virtual {v4}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1430971
    invoke-virtual {v4}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->d:I

    .line 1430972
    :cond_2
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 1430973
    invoke-virtual {v5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1430974
    invoke-virtual {v5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->e:I

    .line 1430975
    :cond_3
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 1430976
    invoke-virtual {v6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1430977
    invoke-virtual {v6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move-object/from16 v0, p2

    iput-boolean v1, v0, LX/91V;->j:Z

    .line 1430978
    :cond_4
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 1430979
    invoke-virtual {v7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1430980
    invoke-virtual {v7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/91V;->a:Ljava/lang/CharSequence;

    .line 1430981
    :cond_5
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 1430982
    invoke-virtual {v8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1430983
    invoke-virtual {v8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/ColorStateList;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/91V;->l:Landroid/content/res/ColorStateList;

    .line 1430984
    :cond_6
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 1430985
    invoke-virtual {v9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1430986
    invoke-virtual {v9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->o:I

    .line 1430987
    :cond_7
    invoke-static {v9}, LX/1cy;->a(LX/1np;)V

    .line 1430988
    invoke-virtual {v10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1430989
    invoke-virtual {v10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->p:I

    .line 1430990
    :cond_8
    invoke-static {v10}, LX/1cy;->a(LX/1np;)V

    .line 1430991
    invoke-virtual {v11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 1430992
    invoke-virtual {v11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->q:I

    .line 1430993
    :cond_9
    invoke-static {v11}, LX/1cy;->a(LX/1np;)V

    .line 1430994
    invoke-virtual {v12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 1430995
    invoke-virtual {v12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout$Alignment;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/91V;->v:Landroid/text/Layout$Alignment;

    .line 1430996
    :cond_a
    invoke-static {v12}, LX/1cy;->a(LX/1np;)V

    .line 1430997
    invoke-virtual {v13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 1430998
    invoke-virtual {v13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->t:I

    .line 1430999
    :cond_b
    invoke-static {v13}, LX/1cy;->a(LX/1np;)V

    .line 1431000
    invoke-virtual {v14}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 1431001
    invoke-virtual {v14}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->f:F

    .line 1431002
    :cond_c
    invoke-static {v14}, LX/1cy;->a(LX/1np;)V

    .line 1431003
    invoke-virtual {v15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 1431004
    invoke-virtual {v15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->g:F

    .line 1431005
    :cond_d
    invoke-static {v15}, LX/1cy;->a(LX/1np;)V

    .line 1431006
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 1431007
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->h:F

    .line 1431008
    :cond_e
    invoke-static/range {v16 .. v16}, LX/1S3;->a(LX/1np;)V

    .line 1431009
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 1431010
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->i:I

    .line 1431011
    :cond_f
    invoke-static/range {v17 .. v17}, LX/1S3;->a(LX/1np;)V

    .line 1431012
    invoke-virtual/range {v18 .. v18}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 1431013
    invoke-virtual/range {v18 .. v18}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/91V;->w:I

    .line 1431014
    :cond_10
    invoke-static/range {v18 .. v18}, LX/1S3;->a(LX/1np;)V

    .line 1431015
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1430944
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 2

    .prologue
    .line 1430918
    check-cast p1, LX/91V;

    .line 1430919
    check-cast p2, LX/91V;

    .line 1430920
    iget-object v0, p1, LX/91V;->b:Ljava/lang/CharSequence;

    iget-object v1, p2, LX/91V;->b:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 1430921
    iget-object v1, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 1430922
    check-cast v1, Ljava/lang/CharSequence;

    .line 1430923
    iget-object p0, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 1430924
    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1430925
    invoke-static {v0}, LX/1cy;->a(LX/3lz;)V

    .line 1430926
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 26

    .prologue
    .line 1430941
    check-cast p3, LX/91V;

    move-object/from16 v2, p2

    .line 1430942
    check-cast v2, LX/91a;

    move-object/from16 v0, p3

    iget-object v3, v0, LX/91V;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    iget-object v4, v0, LX/91V;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    iget-object v5, v0, LX/91V;->c:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p3

    iget v6, v0, LX/91V;->d:I

    move-object/from16 v0, p3

    iget v7, v0, LX/91V;->e:I

    move-object/from16 v0, p3

    iget v8, v0, LX/91V;->f:F

    move-object/from16 v0, p3

    iget v9, v0, LX/91V;->g:F

    move-object/from16 v0, p3

    iget v10, v0, LX/91V;->h:F

    move-object/from16 v0, p3

    iget v11, v0, LX/91V;->i:I

    move-object/from16 v0, p3

    iget-boolean v12, v0, LX/91V;->j:Z

    move-object/from16 v0, p3

    iget v13, v0, LX/91V;->k:I

    move-object/from16 v0, p3

    iget-object v14, v0, LX/91V;->l:Landroid/content/res/ColorStateList;

    move-object/from16 v0, p3

    iget v15, v0, LX/91V;->m:I

    move-object/from16 v0, p3

    iget-object v0, v0, LX/91V;->n:Landroid/content/res/ColorStateList;

    move-object/from16 v16, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/91V;->o:I

    move/from16 v17, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/91V;->p:I

    move/from16 v18, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/91V;->q:I

    move/from16 v19, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/91V;->r:F

    move/from16 v20, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/91V;->s:F

    move/from16 v21, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/91V;->t:I

    move/from16 v22, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/91V;->u:Landroid/graphics/Typeface;

    move-object/from16 v23, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/91V;->v:Landroid/text/Layout$Alignment;

    move-object/from16 v24, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/91V;->w:I

    move/from16 v25, v0

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v25}, LX/91b;->a(LX/1De;LX/91a;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;I)V

    .line 1430943
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1430940
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1430936
    check-cast p2, LX/91a;

    .line 1430937
    const/4 p0, 0x0

    .line 1430938
    iput-object p0, p2, LX/91a;->c:LX/1dQ;

    .line 1430939
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1430933
    check-cast p2, LX/91a;

    .line 1430934
    iget-object p0, p2, LX/91a;->b:Landroid/text/TextWatcher;

    invoke-virtual {p2, p0}, LX/91a;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1430935
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1430930
    check-cast p2, LX/91a;

    .line 1430931
    iget-object p0, p2, LX/91a;->b:Landroid/text/TextWatcher;

    invoke-virtual {p2, p0}, LX/91a;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1430932
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1430929
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1430928
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1430927
    const/16 v0, 0xf

    return v0
.end method
