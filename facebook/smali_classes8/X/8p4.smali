.class public LX/8p4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1405453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405454
    iput-object p1, p0, LX/8p4;->a:LX/0ad;

    .line 1405455
    return-void
.end method

.method public static b(LX/0QB;)LX/8p4;
    .locals 2

    .prologue
    .line 1405456
    new-instance v1, LX/8p4;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/8p4;-><init>(LX/0ad;)V

    .line 1405457
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/tagging/model/TaggingProfile;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1405458
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1405459
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1405460
    :goto_0
    return v0

    .line 1405461
    :cond_0
    iget-object v0, p1, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1405462
    if-eqz v0, :cond_1

    .line 1405463
    iget-object v0, p1, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1405464
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    .line 1405465
    goto :goto_0

    .line 1405466
    :cond_2
    iget-object v0, p0, LX/8p4;->a:LX/0ad;

    sget-short v3, LX/8mx;->f:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1405467
    if-eqz v0, :cond_4

    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1405468
    :goto_1
    iget-object v3, p1, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v3, v3

    .line 1405469
    invoke-virtual {v3}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1405470
    array-length v5, v0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_6

    aget-object v6, v0, v3

    .line 1405471
    const/4 v7, 0x0

    .line 1405472
    array-length p1, v4

    move p0, v7

    :goto_3
    if-ge p0, p1, :cond_3

    aget-object p2, v4, p0

    .line 1405473
    invoke-virtual {p2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 1405474
    const/4 v7, 0x1

    .line 1405475
    :cond_3
    move v6, v7

    .line 1405476
    if-nez v6, :cond_5

    move v0, v2

    .line 1405477
    goto :goto_0

    .line 1405478
    :cond_4
    new-array v0, v1, [Ljava/lang/String;

    aput-object p2, v0, v2

    goto :goto_1

    .line 1405479
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    move v0, v1

    .line 1405480
    goto :goto_0

    .line 1405481
    :cond_7
    add-int/lit8 p0, p0, 0x1

    goto :goto_3
.end method
