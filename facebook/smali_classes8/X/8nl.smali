.class public final LX/8nl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1401877
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1401878
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401879
    :goto_0
    return v1

    .line 1401880
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401881
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1401882
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1401883
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1401884
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1401885
    const-string v4, "member_section"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1401886
    const/4 v3, 0x0

    .line 1401887
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 1401888
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401889
    :goto_2
    move v2, v3

    .line 1401890
    goto :goto_1

    .line 1401891
    :cond_2
    const-string v4, "non_member_section"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1401892
    const/4 v3, 0x0

    .line 1401893
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_12

    .line 1401894
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401895
    :goto_3
    move v0, v3

    .line 1401896
    goto :goto_1

    .line 1401897
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1401898
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1401899
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1401900
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1401901
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401902
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1401903
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1401904
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1401905
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1401906
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1401907
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1401908
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1401909
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1401910
    const/4 v5, 0x0

    .line 1401911
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 1401912
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401913
    :goto_6
    move v4, v5

    .line 1401914
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1401915
    :cond_7
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1401916
    goto :goto_4

    .line 1401917
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1401918
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1401919
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_9
    move v2, v3

    goto :goto_4

    .line 1401920
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401921
    :cond_b
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 1401922
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1401923
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1401924
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 1401925
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1401926
    invoke-static {p0, p1}, LX/8nj;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_7

    .line 1401927
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1401928
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1401929
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_6

    :cond_d
    move v4, v5

    goto :goto_7

    .line 1401930
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401931
    :cond_f
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_11

    .line 1401932
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1401933
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1401934
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_f

    if-eqz v4, :cond_f

    .line 1401935
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1401936
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1401937
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_10

    .line 1401938
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_10

    .line 1401939
    const/4 v5, 0x0

    .line 1401940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_16

    .line 1401941
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401942
    :goto_a
    move v4, v5

    .line 1401943
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 1401944
    :cond_10
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1401945
    goto :goto_8

    .line 1401946
    :cond_11
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1401947
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1401948
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_12
    move v0, v3

    goto :goto_8

    .line 1401949
    :cond_13
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1401950
    :cond_14
    :goto_b
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_15

    .line 1401951
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1401952
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1401953
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_14

    if-eqz v6, :cond_14

    .line 1401954
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1401955
    invoke-static {p0, p1}, LX/8nk;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_b

    .line 1401956
    :cond_15
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1401957
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1401958
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_a

    :cond_16
    move v4, v5

    goto :goto_b
.end method
