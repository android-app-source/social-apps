.class public final LX/9BE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V
    .locals 0

    .prologue
    .line 1451426
    iput-object p1, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1451427
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    if-eqz v0, :cond_2

    .line 1451428
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-boolean v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->C:Z

    if-nez v0, :cond_0

    .line 1451429
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    sget-object v1, LX/82l;->SWIPE:LX/82l;

    iget-object v2, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v2, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    invoke-static {v2, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/82m;->a(LX/82l;LX/1zt;)V

    .line 1451430
    :cond_0
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    invoke-static {v0, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v1

    .line 1451431
    iget v0, v1, LX/1zt;->e:I

    move v2, v0

    .line 1451432
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->z:Landroid/util/SparseArray;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1451433
    if-nez v0, :cond_1

    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-static {v0, p1}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a$redex0(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;I)I

    move-result v0

    if-lez v0, :cond_1

    .line 1451434
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    const v7, 0x820006

    .line 1451435
    iget-object v3, v0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v7, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1451436
    iget-object v3, v0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "reaction_tab_pos:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v7, p1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1451437
    iget-object v3, v0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v5, 0x2

    invoke-interface {v3, v7, p1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1451438
    :cond_1
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->z:Landroid/util/SparseArray;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1451439
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 1451440
    if-eqz v0, :cond_2

    .line 1451441
    iget-object v1, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    const v2, 0x7f0d124b

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1451442
    iput-object v0, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->A:Lcom/facebook/widget/listview/BetterListView;

    .line 1451443
    :cond_2
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    .line 1451444
    iput-boolean v4, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->C:Z

    .line 1451445
    return-void
.end method

.method public final a(IFI)V
    .locals 3

    .prologue
    .line 1451421
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    .line 1451422
    :goto_0
    return-void

    .line 1451423
    :cond_0
    iget-object v0, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    invoke-static {v0, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v0

    invoke-static {v0}, LX/9B7;->c(LX/1zt;)I

    move-result v0

    .line 1451424
    iget-object v1, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v1

    invoke-static {v1}, LX/9B7;->c(LX/1zt;)I

    move-result v1

    .line 1451425
    iget-object v2, p0, LX/9BE;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v2, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-static {p2, v0, v1}, LX/6Uc;->a(FII)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setUnderlineColor(I)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1451420
    return-void
.end method
