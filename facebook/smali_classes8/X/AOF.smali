.class public LX/AOF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANo;


# instance fields
.field public final a:LX/6Jt;

.field public final b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final c:Landroid/content/Context;

.field public final d:LX/ANk;

.field public e:LX/AOO;

.field public f:LX/6KY;

.field public g:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

.field public h:LX/AOD;


# direct methods
.method public constructor <init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;LX/7SL;)V
    .locals 2
    .param p1    # LX/6Jt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1668824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668825
    new-instance v0, LX/AOE;

    invoke-direct {v0, p0}, LX/AOE;-><init>(LX/AOF;)V

    iput-object v0, p0, LX/AOF;->d:LX/ANk;

    .line 1668826
    iput-object p1, p0, LX/AOF;->a:LX/6Jt;

    .line 1668827
    iput-object p2, p0, LX/AOF;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668828
    iput-object p3, p0, LX/AOF;->c:Landroid/content/Context;

    .line 1668829
    invoke-virtual {p4, p3}, LX/7SL;->a(Landroid/content/Context;)Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    move-result-object v0

    iput-object v0, p0, LX/AOF;->g:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    .line 1668830
    new-instance v0, LX/6KY;

    iget-object v1, p0, LX/AOF;->g:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    invoke-direct {v0, v1}, LX/6KY;-><init>(LX/61B;)V

    iput-object v0, p0, LX/AOF;->f:LX/6KY;

    .line 1668831
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1668819
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668820
    iget-object v0, p0, LX/AOF;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AOF;->e:LX/AOO;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668821
    return-void
.end method

.method public final b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668822
    iget-object v0, p0, LX/AOF;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668823
    return-void
.end method
