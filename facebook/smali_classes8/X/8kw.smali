.class public final LX/8kw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 1396949
    iput-object p1, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iput-object p2, p0, LX/8kw;->a:Ljava/util/Set;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1396950
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    const-string v1, "Fetching promoted sticker packs failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1396951
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v1, 0x0

    .line 1396952
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->r:LX/1Mv;

    .line 1396953
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v1, LX/8lJ;->a:LX/8lF;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1396954
    if-eqz v0, :cond_0

    .line 1396955
    const-string v1, "StickerKeyboardPopulatePacks"

    const v2, 0x4014144

    invoke-static {v0, v1, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1396956
    const-string v1, "StickerPackLoadForPopup"

    const v2, -0x2ec7930

    invoke-static {v0, v1, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1396957
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v1, LX/8lJ;->a:LX/8lF;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1396958
    :cond_0
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->l:LX/03V;

    sget-object v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fetching promoted sticker packs failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1396959
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1396960
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1396961
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v1, 0x0

    .line 1396962
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->r:LX/1Mv;

    .line 1396963
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1396964
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v1

    .line 1396965
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1396966
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1396967
    iget-object v4, p0, LX/8kw;->a:Ljava/util/Set;

    .line 1396968
    iget-object v5, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1396969
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1396970
    iget-object v4, v1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v4, v4

    .line 1396971
    iget-object v5, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v5, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    invoke-virtual {v4, v5}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1396972
    iget-boolean v4, v1, Lcom/facebook/stickers/model/StickerPack;->n:Z

    move v4, v4

    .line 1396973
    if-eqz v4, :cond_0

    .line 1396974
    iget-object v4, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v4, v4, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    new-instance v5, LX/8km;

    sget-object v6, LX/8kk;->PROMOTED:LX/8kk;

    invoke-direct {v5, v1, v6}, LX/8km;-><init>(Lcom/facebook/stickers/model/StickerPack;LX/8kk;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1396975
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1396976
    :cond_1
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    iget-object v1, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setItems(Ljava/util/List;)V

    .line 1396977
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v1, LX/8lJ;->a:LX/8lF;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1396978
    if-eqz v0, :cond_2

    .line 1396979
    const-string v1, "StickerKeyboardPopulatePacks"

    const v2, -0x1b11cace

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1396980
    const-string v1, "StickerPackLoadForPopup"

    const v2, -0x2381cc5a

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1396981
    iget-object v0, p0, LX/8kw;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v1, LX/8lJ;->a:LX/8lF;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1396982
    :cond_2
    return-void
.end method
