.class public final LX/AEb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/AEd;


# direct methods
.method public constructor <init>(LX/AEd;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1646816
    iput-object p1, p0, LX/AEb;->b:LX/AEd;

    iput-object p2, p0, LX/AEb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const v0, -0x3c30f330    # -414.1001f

    invoke-static {v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1646794
    iget-object v0, p0, LX/AEb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 1646795
    if-nez v5, :cond_0

    .line 1646796
    const v0, -0x7292dcfb

    invoke-static {v1, v1, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1646797
    :goto_0
    return-void

    .line 1646798
    :cond_0
    iget-object v0, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646799
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1646800
    iget-object v1, p0, LX/AEb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646801
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1646802
    move-object v2, v1

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646803
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v1

    .line 1646804
    invoke-static {v5}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1646805
    if-eqz v4, :cond_6

    .line 1646806
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646807
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1646808
    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    move v1, v3

    :goto_1
    move v5, v1

    move-object v1, v4

    move-object v4, v0

    .line 1646809
    :goto_2
    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1646810
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 1646811
    invoke-static {p1}, LX/7kh;->a(Landroid/view/View;)V

    .line 1646812
    :cond_2
    iget-object v0, p0, LX/AEb;->b:LX/AEd;

    iget-object v7, v0, LX/AEd;->b:LX/0bH;

    new-instance v0, LX/1Za;

    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    :goto_3
    if-eqz v5, :cond_5

    const-string v4, "sponsored_story"

    :goto_4
    const-string v5, "unknown"

    invoke-direct/range {v0 .. v5}, LX/1Za;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1646813
    const v0, 0x56186da1

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0

    .line 1646814
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1646815
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    const-string v4, "feed_story"

    goto :goto_4

    :cond_6
    move-object v4, v0

    move v8, v1

    move-object v1, v5

    move v5, v8

    goto :goto_2
.end method
