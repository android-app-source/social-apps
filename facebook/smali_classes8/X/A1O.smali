.class public final LX/A1O;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1610454
    const-class v1, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel;

    const v0, -0x48b78768

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "NullStateModuleQuery"

    const-string v6, "7cc57b3e547cd464689d2174c79d34b6"

    const-string v7, "graph_search_null_state"

    const-string v8, "10155069967941729"

    const-string v9, "10155259089401729"

    .line 1610455
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1610456
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1610457
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1610445
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1610446
    sparse-switch v0, :sswitch_data_0

    .line 1610447
    :goto_0
    return-object p1

    .line 1610448
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1610449
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1610450
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1610451
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1610452
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1610453
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x669b5620 -> :sswitch_1
        -0x356f97e5 -> :sswitch_2
        -0x33258c73 -> :sswitch_4
        0xea4d136 -> :sswitch_3
        0x714f9fb5 -> :sswitch_0
        0x757a316b -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1610440
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 1610441
    :goto_1
    return v0

    .line 1610442
    :pswitch_1
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1610443
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1610444
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
