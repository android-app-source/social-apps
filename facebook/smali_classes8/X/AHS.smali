.class public LX/AHS;
.super LX/AHR;
.source ""


# instance fields
.field private final c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1655631
    invoke-direct {p0, p2, p3}, LX/AHR;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655632
    iput-object p1, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    .line 1655633
    iput-object p4, p0, LX/AHS;->d:Ljava/util/Set;

    .line 1655634
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1655630
    iget-object v0, p0, LX/AHS;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1655627
    invoke-super {p0}, LX/AHR;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    if-eqz v0, :cond_0

    .line 1655628
    const/4 v0, 0x1

    move v0, v0

    .line 1655629
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1655626
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1655625
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7gp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1655624
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7gp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1655635
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1655636
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1655623
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 1655622
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/1lQ;->o(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 1655621
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final j()F
    .locals 2

    .prologue
    .line 1655618
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;->k()I

    move-result v0

    int-to-float v0, v0

    .line 1655619
    iget-object v1, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;->a()I

    move-result v1

    int-to-float v1, v1

    .line 1655620
    div-float v0, v1, v0

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1655617
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/AHR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1655616
    iget-object v0, p0, LX/AHS;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
