.class public LX/8qM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8qM;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1407045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407046
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8qM;->a:Z

    .line 1407047
    return-void
.end method

.method public static a(LX/0QB;)LX/8qM;
    .locals 3

    .prologue
    .line 1407048
    sget-object v0, LX/8qM;->b:LX/8qM;

    if-nez v0, :cond_1

    .line 1407049
    const-class v1, LX/8qM;

    monitor-enter v1

    .line 1407050
    :try_start_0
    sget-object v0, LX/8qM;->b:LX/8qM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1407051
    if-eqz v2, :cond_0

    .line 1407052
    :try_start_1
    new-instance v0, LX/8qM;

    invoke-direct {v0}, LX/8qM;-><init>()V

    .line 1407053
    move-object v0, v0

    .line 1407054
    sput-object v0, LX/8qM;->b:LX/8qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1407055
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1407056
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1407057
    :cond_1
    sget-object v0, LX/8qM;->b:LX/8qM;

    return-object v0

    .line 1407058
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1407059
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1407060
    if-nez p0, :cond_0

    .line 1407061
    const/4 v0, 0x0

    .line 1407062
    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method
