.class public final LX/98J;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1445468
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1445469
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1445470
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1445471
    invoke-static {p0, p1}, LX/98J;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1445472
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1445473
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1445474
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1445475
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1445476
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1445477
    const-string v2, "end"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445478
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1445479
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1445480
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 1445481
    const-string v2, "start"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445482
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1445483
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1445484
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1445462
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1445463
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1445464
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/98J;->a(LX/15i;ILX/0nX;)V

    .line 1445465
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1445466
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1445467
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1445444
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1445445
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1445446
    :goto_0
    return v1

    .line 1445447
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_3

    .line 1445448
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1445449
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1445450
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1445451
    const-string v11, "end"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1445452
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1445453
    :cond_1
    const-string v11, "start"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1445454
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v8

    move v6, v7

    goto :goto_1

    .line 1445455
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1445456
    :cond_3
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1445457
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 1445458
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1445459
    :cond_4
    if-eqz v6, :cond_5

    move-object v0, p1

    move v1, v7

    move-wide v2, v8

    .line 1445460
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1445461
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v6, v1

    move v0, v1

    move-wide v8, v4

    move-wide v2, v4

    goto :goto_1
.end method
