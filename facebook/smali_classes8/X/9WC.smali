.class public LX/9WC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public A:Ljava/lang/Boolean;

.field public B:Ljava/lang/Boolean;

.field public C:Z

.field public final D:Landroid/content/DialogInterface$OnClickListener;

.field public final E:Landroid/content/DialogInterface$OnClickListener;

.field private final F:Landroid/widget/AdapterView$OnItemClickListener;

.field private final G:Landroid/widget/AdapterView$OnItemClickListener;

.field public a:LX/0tX;

.field public c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/9WB;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/03V;

.field public e:LX/0Sh;

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8wj;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/2EJ;

.field public h:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/9Vx;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/9Vx;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/9WH;

.field public k:LX/9Vx;

.field public l:Z

.field public m:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

.field private n:LX/9WG;

.field public o:Landroid/widget/FrameLayout;

.field public p:Landroid/widget/LinearLayout;

.field public q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/9Ve;

.field public s:LX/9Vf;

.field public t:Lcom/facebook/content/SecureContextHelper;

.field public u:LX/9Vy;

.field public v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/lang/String;

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public y:Landroid/os/Bundle;

.field public z:LX/0SI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1500862
    const-class v0, LX/9WC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9WC;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Sh;LX/0Or;LX/0tX;LX/9Ve;LX/9Vf;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/1Ck;LX/0SI;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/8wj;",
            ">;",
            "LX/0tX;",
            "LX/9Ve;",
            "LX/9Vf;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Ck;",
            "LX/0SI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1500842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1500843
    iput-boolean v1, p0, LX/9WC;->l:Z

    .line 1500844
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/9WC;->A:Ljava/lang/Boolean;

    .line 1500845
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/9WC;->B:Ljava/lang/Boolean;

    .line 1500846
    iput-boolean v1, p0, LX/9WC;->C:Z

    .line 1500847
    new-instance v0, LX/9W2;

    invoke-direct {v0, p0}, LX/9W2;-><init>(LX/9WC;)V

    iput-object v0, p0, LX/9WC;->D:Landroid/content/DialogInterface$OnClickListener;

    .line 1500848
    new-instance v0, LX/9W3;

    invoke-direct {v0, p0}, LX/9W3;-><init>(LX/9WC;)V

    iput-object v0, p0, LX/9WC;->E:Landroid/content/DialogInterface$OnClickListener;

    .line 1500849
    new-instance v0, LX/9W4;

    invoke-direct {v0, p0}, LX/9W4;-><init>(LX/9WC;)V

    iput-object v0, p0, LX/9WC;->F:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1500850
    new-instance v0, LX/9W5;

    invoke-direct {v0, p0}, LX/9W5;-><init>(LX/9WC;)V

    iput-object v0, p0, LX/9WC;->G:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1500851
    iput-object p1, p0, LX/9WC;->d:LX/03V;

    .line 1500852
    iput-object p2, p0, LX/9WC;->e:LX/0Sh;

    .line 1500853
    iput-object p3, p0, LX/9WC;->f:LX/0Or;

    .line 1500854
    iput-object p4, p0, LX/9WC;->a:LX/0tX;

    .line 1500855
    iput-object p5, p0, LX/9WC;->r:LX/9Ve;

    .line 1500856
    iput-object p6, p0, LX/9WC;->s:LX/9Vf;

    .line 1500857
    iput-object p7, p0, LX/9WC;->t:Lcom/facebook/content/SecureContextHelper;

    .line 1500858
    iput-object p8, p0, LX/9WC;->v:LX/0Or;

    .line 1500859
    iput-object p9, p0, LX/9WC;->c:LX/1Ck;

    .line 1500860
    iput-object p10, p0, LX/9WC;->z:LX/0SI;

    .line 1500861
    return-void
.end method

.method public static a(LX/9WC;LX/9WI;)LX/9Vy;
    .locals 1

    .prologue
    .line 1500838
    iget-object v0, p0, LX/9WC;->g:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/9WG;->a(LX/9WI;Landroid/content/Context;)LX/9Vy;

    move-result-object v0

    .line 1500839
    iput-object v0, p0, LX/9WC;->u:LX/9Vy;

    .line 1500840
    invoke-direct {p0, v0}, LX/9WC;->a(LX/9Vy;)V

    .line 1500841
    return-object v0
.end method

.method private a(LX/9Vy;)V
    .locals 1

    .prologue
    .line 1500834
    iget-object v0, p0, LX/9WC;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1500835
    if-eqz p1, :cond_0

    .line 1500836
    iget-object v0, p0, LX/9WC;->o:Landroid/widget/FrameLayout;

    check-cast p1, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1500837
    :cond_0
    return-void
.end method

.method public static a(LX/9WC;LX/9WM;ILandroid/widget/ListView;)V
    .locals 3

    .prologue
    .line 1500809
    sget-object v0, LX/9To;->INITIATED:LX/9To;

    .line 1500810
    iput-object v0, p1, LX/9WM;->c:LX/9To;

    .line 1500811
    iget-object v0, p0, LX/9WC;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Vx;

    iput-object v0, p0, LX/9WC;->k:LX/9Vx;

    .line 1500812
    iget-object v0, p1, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    move-object v0, v0

    .line 1500813
    invoke-direct {p0, p1, p3}, LX/9WC;->a(LX/9WM;Landroid/widget/ListView;)V

    .line 1500814
    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v1, v2, :cond_2

    .line 1500815
    iget-object v1, p0, LX/9WC;->r:LX/9Ve;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 1500816
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "negativefeedback_open_message_composer"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500817
    invoke-static {v1, p1, v2}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500818
    sget-object v1, LX/9WI;->MESSAGE_COMPOSER:LX/9WI;

    invoke-static {p0, v1}, LX/9WC;->a(LX/9WC;LX/9WI;)LX/9Vy;

    move-result-object v1

    check-cast v1, LX/9WQ;

    .line 1500819
    const/16 p1, 0x8

    .line 1500820
    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->l()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1500821
    iget-object v2, v1, LX/9WQ;->c:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    invoke-virtual {v2, p1}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->setVisibility(I)V

    .line 1500822
    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1500823
    iget-object v2, v1, LX/9WQ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1500824
    :cond_0
    :goto_0
    iget-object v2, v1, LX/9WQ;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gm_()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1500825
    iget-object v1, p0, LX/9WC;->g:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1500826
    iget-object v1, p0, LX/9WC;->j:LX/9WH;

    .line 1500827
    iget-object v2, v1, LX/9WH;->c:Landroid/widget/Button;

    if-eqz v2, :cond_1

    .line 1500828
    iget-object v2, v1, LX/9WH;->c:Landroid/widget/Button;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1500829
    :cond_1
    iget-object v1, p0, LX/9WC;->j:LX/9WH;

    .line 1500830
    iget-object v2, v1, LX/9WH;->b:Landroid/widget/Button;

    if-eqz v2, :cond_2

    .line 1500831
    iget-object v2, v1, LX/9WH;->b:Landroid/widget/Button;

    const/16 p1, 0x8

    invoke-virtual {v2, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1500832
    :cond_2
    return-void

    .line 1500833
    :cond_3
    iget-object v2, v1, LX/9WQ;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(LX/9WC;Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1500759
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/9WC;->g:LX/2EJ;

    if-nez v0, :cond_1

    .line 1500760
    :cond_0
    :goto_0
    return-void

    .line 1500761
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/9WC;->C:Z

    .line 1500762
    iget-boolean v0, p0, LX/9WC;->l:Z

    if-eqz v0, :cond_3

    .line 1500763
    iget-object v0, p0, LX/9WC;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1500764
    iget-object v0, p0, LX/9WC;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Vx;

    iput-object v0, p0, LX/9WC;->k:LX/9Vx;

    .line 1500765
    :goto_1
    iget-object v0, p0, LX/9WC;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1500766
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1500767
    sget-object v0, LX/9WI;->GUIDED_ACTIONS:LX/9WI;

    invoke-static {p0, v0}, LX/9WC;->a(LX/9WC;LX/9WI;)LX/9Vy;

    move-result-object v0

    check-cast v0, LX/9WP;

    .line 1500768
    invoke-virtual {v0, p1}, LX/9WP;->a(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;)V

    .line 1500769
    :goto_2
    iget-object v0, p0, LX/9WC;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_a

    .line 1500770
    iget-object v0, p0, LX/9WC;->j:LX/9WH;

    .line 1500771
    iget-object v1, v0, LX/9WH;->a:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 1500772
    iget-object v1, v0, LX/9WH;->a:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1500773
    :cond_2
    :goto_3
    invoke-direct {p0, p1}, LX/9WC;->b(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;)V

    goto :goto_0

    .line 1500774
    :cond_3
    iget-object v0, p0, LX/9WC;->h:Ljava/util/Stack;

    iget-object v1, p0, LX/9WC;->k:LX/9Vx;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1500775
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1500776
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-eq v0, v1, :cond_6

    .line 1500777
    sget-object v0, LX/9WI;->GUIDED_ACTIONS:LX/9WI;

    invoke-static {p0, v0}, LX/9WC;->a(LX/9WC;LX/9WI;)LX/9Vy;

    move-result-object v0

    check-cast v0, LX/9WP;

    .line 1500778
    iget-object v1, p0, LX/9WC;->q:Ljava/util/Set;

    .line 1500779
    iput-object v1, v0, LX/9WP;->e:Ljava/util/Set;

    .line 1500780
    invoke-virtual {v0, p1}, LX/9WP;->a(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;)V

    .line 1500781
    iget-object v1, p0, LX/9WC;->G:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1500782
    iget-object v2, v0, LX/9WP;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1500783
    iget-object v0, p0, LX/9WC;->p:Landroid/widget/LinearLayout;

    new-instance v1, LX/9W8;

    invoke-direct {v1, p0}, LX/9W8;-><init>(LX/9WC;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1500784
    iget-object v0, p0, LX/9WC;->j:LX/9WH;

    invoke-virtual {v0}, LX/9WH;->g()V

    .line 1500785
    iget-object v0, p0, LX/9WC;->j:LX/9WH;

    .line 1500786
    iget-object v1, v0, LX/9WH;->b:Landroid/widget/Button;

    if-eqz v1, :cond_5

    .line 1500787
    iget-object v1, v0, LX/9WH;->b:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1500788
    :cond_5
    goto :goto_2

    .line 1500789
    :cond_6
    sget-object v0, LX/9WI;->FLOW_RESPONSES:LX/9WI;

    invoke-static {p0, v0}, LX/9WC;->a(LX/9WC;LX/9WI;)LX/9Vy;

    move-result-object v0

    check-cast v0, LX/9WT;

    .line 1500790
    const/4 v5, 0x0

    .line 1500791
    iget-object v1, v0, LX/9WT;->b:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_7

    .line 1500792
    iget-object v1, v0, LX/9WT;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->c()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1500793
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v5

    .line 1500794
    :goto_4
    if-ge v4, v7, :cond_9

    .line 1500795
    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    .line 1500796
    const-string v2, ""

    .line 1500797
    const-string v3, ""

    .line 1500798
    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->m()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TitleModel;

    move-result-object v8

    if-eqz v8, :cond_8

    .line 1500799
    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->m()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TitleModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1500800
    :cond_8
    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$SubtitleModel;

    move-result-object v8

    if-eqz v8, :cond_b

    .line 1500801
    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$SubtitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1500802
    :goto_5
    iget-object v3, v0, LX/9WT;->d:LX/9WR;

    new-instance v8, LX/9WS;

    invoke-direct {v8, v2, v1}, LX/9WS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, LX/9WR;->add(Ljava/lang/Object;)V

    .line 1500803
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    .line 1500804
    :cond_9
    invoke-virtual {v0, v5}, LX/9WT;->setProgressBarVisibility(Z)V

    .line 1500805
    iget-object v1, p0, LX/9WC;->F:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1500806
    iget-object v2, v0, LX/9WT;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1500807
    goto/16 :goto_2

    .line 1500808
    :cond_a
    iget-object v0, p0, LX/9WC;->j:LX/9WH;

    invoke-virtual {v0}, LX/9WH;->c()V

    goto/16 :goto_3

    :cond_b
    move-object v1, v3

    goto :goto_5
.end method

.method private a(LX/9WM;Landroid/widget/ListView;)V
    .locals 10

    .prologue
    .line 1500728
    iget-object v0, p1, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    move-object v9, v0

    .line 1500729
    iget-object v0, p0, LX/9WC;->g:LX/2EJ;

    if-nez v0, :cond_0

    .line 1500730
    :goto_0
    return-void

    .line 1500731
    :cond_0
    const-string v6, ""

    .line 1500732
    const-string v7, ""

    .line 1500733
    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1500734
    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 1500735
    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1500736
    :cond_1
    new-instance v0, LX/9Vx;

    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v4

    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gm_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->l()Z

    move-result v8

    invoke-direct/range {v0 .. v8}, LX/9Vx;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, LX/9WC;->k:LX/9Vx;

    .line 1500737
    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v0, v1, :cond_2

    .line 1500738
    iget-object v0, p0, LX/9WC;->h:Ljava/util/Stack;

    iget-object v1, p0, LX/9WC;->k:LX/9Vx;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1500739
    :cond_2
    sget-object v0, LX/9Tp;->c:LX/0Rf;

    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1500740
    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1500741
    if-nez v0, :cond_4

    .line 1500742
    :goto_1
    sget-object v0, LX/9To;->INITIAL:LX/9To;

    .line 1500743
    iput-object v0, p1, LX/9WM;->c:LX/9To;

    .line 1500744
    goto :goto_0

    .line 1500745
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1500746
    iget-object v0, p1, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    move-object v1, v0

    .line 1500747
    iget-object v0, p0, LX/9WC;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8wj;

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8wj;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1500748
    iget-object v2, p0, LX/9WC;->r:LX/9Ve;

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 1500749
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "negativefeedback_guided_action"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500750
    invoke-static {v2, v4, v3}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500751
    iget-object v2, p0, LX/9WC;->c:LX/1Ck;

    sget-object v3, LX/9WB;->ACTION:LX/9WB;

    new-instance v4, LX/9W9;

    invoke-direct {v4, p0, v1, p2, p1}, LX/9W9;-><init>(LX/9WC;Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;Landroid/widget/ListView;LX/9WM;)V

    invoke-static {v4}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v1

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1500752
    goto/16 :goto_0

    .line 1500753
    :cond_4
    iget-object v1, p0, LX/9WC;->r:LX/9Ve;

    invoke-virtual {v9}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 1500754
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "negativefeedback_redirect_action"

    invoke-direct {v3, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500755
    invoke-static {v1, v3, v2}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500756
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1500757
    const-string v0, "force_in_app_browser"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1500758
    iget-object v0, p0, LX/9WC;->t:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/9WC;->g:LX/2EJ;

    invoke-virtual {v2}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public static a$redex0(LX/9WC;LX/9Vx;)V
    .locals 8

    .prologue
    .line 1500863
    const/4 v0, 0x1

    .line 1500864
    iget-object v1, p0, LX/9WC;->u:LX/9Vy;

    if-eqz v1, :cond_0

    .line 1500865
    iget-object v1, p0, LX/9WC;->u:LX/9Vy;

    invoke-interface {v1, v0}, LX/9Vy;->setProgressBarVisibility(Z)V

    .line 1500866
    :cond_0
    iget-wide v4, p1, LX/9Vx;->b:J

    move-wide v0, v4

    .line 1500867
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1500868
    iget-object v0, p1, LX/9Vx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1500869
    iget-wide v4, p1, LX/9Vx;->b:J

    move-wide v2, v4

    .line 1500870
    new-instance v4, LX/9Vh;

    invoke-direct {v4}, LX/9Vh;-><init>()V

    .line 1500871
    const-string v5, "thread_id"

    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1500872
    const-string v5, "responsible_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1500873
    iget-object v5, p0, LX/9WC;->s:LX/9Vf;

    invoke-virtual {v5}, LX/9Vf;->a()V

    .line 1500874
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0x78

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 1500875
    iget-object v5, p0, LX/9WC;->z:LX/0SI;

    invoke-interface {v5}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    .line 1500876
    iput-object v5, v4, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1500877
    iget-object v5, p0, LX/9WC;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 1500878
    iget-object v5, p0, LX/9WC;->c:LX/1Ck;

    sget-object v6, LX/9WB;->FLOW_STEP:LX/9WB;

    new-instance v7, LX/9W7;

    invoke-direct {v7, p0}, LX/9W7;-><init>(LX/9WC;)V

    invoke-static {v7}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v7

    invoke-virtual {v5, v6, v4, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1500879
    :goto_0
    return-void

    .line 1500880
    :cond_1
    iget-object v0, p1, LX/9Vx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1500881
    new-instance v4, LX/9Vi;

    invoke-direct {v4}, LX/9Vi;-><init>()V

    .line 1500882
    const-string v5, "node_token"

    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1500883
    const-string v5, "negative_feedback_location"

    iget-object v6, p0, LX/9WC;->w:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1500884
    iget-object v5, p0, LX/9WC;->s:LX/9Vf;

    invoke-virtual {v5}, LX/9Vf;->a()V

    .line 1500885
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0x78

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 1500886
    iget-object v5, p0, LX/9WC;->z:LX/0SI;

    invoke-interface {v5}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    .line 1500887
    iput-object v5, v4, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1500888
    iget-object v5, p0, LX/9WC;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 1500889
    iget-object v5, p0, LX/9WC;->c:LX/1Ck;

    sget-object v6, LX/9WB;->FLOW_STEP:LX/9WB;

    new-instance v7, LX/9W6;

    invoke-direct {v7, p0}, LX/9W6;-><init>(LX/9WC;)V

    invoke-static {v7}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v7

    invoke-virtual {v5, v6, v4, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1500890
    goto :goto_0
.end method

.method public static a$redex0(LX/9WC;LX/9WJ;)V
    .locals 3

    .prologue
    .line 1500718
    sget-object v0, LX/9WI;->ERROR:LX/9WI;

    invoke-static {p0, v0}, LX/9WC;->a(LX/9WC;LX/9WI;)LX/9Vy;

    move-result-object v0

    check-cast v0, LX/9WK;

    .line 1500719
    sget-object v1, LX/9WJ;->NETWORK_ERROR:LX/9WJ;

    if-ne p1, v1, :cond_1

    .line 1500720
    iget-object v1, v0, LX/9WK;->a:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f08243a

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1500721
    iget-object v1, v0, LX/9WK;->b:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f08243b

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1500722
    :goto_0
    iget-object v0, p0, LX/9WC;->j:LX/9WH;

    const v1, 0x7f082434

    .line 1500723
    iget-object v2, v0, LX/9WH;->b:Landroid/widget/Button;

    if-eqz v2, :cond_0

    .line 1500724
    iget-object v2, v0, LX/9WH;->b:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(I)V

    .line 1500725
    :cond_0
    return-void

    .line 1500726
    :cond_1
    iget-object v1, v0, LX/9WK;->a:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f082438

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1500727
    iget-object v1, v0, LX/9WK;->b:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f082439

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;)V
    .locals 13

    .prologue
    .line 1500707
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    .line 1500708
    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v11, :cond_1

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    .line 1500709
    invoke-virtual {v8}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v4

    .line 1500710
    const-string v6, ""

    .line 1500711
    const-string v7, ""

    .line 1500712
    invoke-virtual {v8}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1500713
    invoke-virtual {v8}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 1500714
    invoke-virtual {v8}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->k()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TargetModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1500715
    :cond_0
    iget-object v12, p0, LX/9WC;->i:Ljava/util/ArrayList;

    new-instance v0, LX/9Vx;

    invoke-virtual {v8}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-virtual {v8}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gm_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->l()Z

    move-result v8

    invoke-direct/range {v0 .. v8}, LX/9Vx;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1500716
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 1500717
    :cond_1
    return-void
.end method

.method public static h(LX/9WC;)V
    .locals 6

    .prologue
    .line 1500689
    iget-object v0, p0, LX/9WC;->u:LX/9Vy;

    instance-of v0, v0, LX/9WP;

    if-eqz v0, :cond_2

    .line 1500690
    iget-object v0, p0, LX/9WC;->u:LX/9Vy;

    check-cast v0, LX/9WP;

    .line 1500691
    iget-object v1, v0, LX/9WP;->d:LX/9WL;

    move-object v2, v1

    .line 1500692
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, LX/9WL;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1500693
    invoke-virtual {v2, v1}, LX/9WL;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9WM;

    .line 1500694
    iget-object v3, v0, LX/9WM;->c:LX/9To;

    move-object v3, v3

    .line 1500695
    sget-object v4, LX/9To;->ASK_TO_CONFIRM:LX/9To;

    if-ne v3, v4, :cond_0

    .line 1500696
    sget-object v3, LX/9To;->INITIAL:LX/9To;

    .line 1500697
    iput-object v3, v0, LX/9WM;->c:LX/9To;

    .line 1500698
    iget-object v3, p0, LX/9WC;->r:LX/9Ve;

    .line 1500699
    iget-object v4, v0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    move-object v0, v4

    .line 1500700
    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1500701
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "negativefeedback_cancel_confirmation"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500702
    invoke-static {v3, v4, v0}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500703
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/9WC;->A:Ljava/lang/Boolean;

    .line 1500704
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1500705
    :cond_1
    const v0, -0x102a84ed

    invoke-static {v2, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1500706
    :cond_2
    return-void
.end method

.method public static i(LX/9WC;)V
    .locals 2

    .prologue
    .line 1500684
    iget-object v0, p0, LX/9WC;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Vx;

    .line 1500685
    iget-object v1, p0, LX/9WC;->x:Ljava/util/List;

    .line 1500686
    iget-object p0, v0, LX/9Vx;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1500687
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1500688
    return-void
.end method


# virtual methods
.method public final a(LX/9WH;)V
    .locals 4

    .prologue
    .line 1500673
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/9WC;->i:Ljava/util/ArrayList;

    .line 1500674
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/9WC;->h:Ljava/util/Stack;

    .line 1500675
    iput-object p1, p0, LX/9WC;->j:LX/9WH;

    .line 1500676
    new-instance v0, LX/9WG;

    invoke-direct {v0}, LX/9WG;-><init>()V

    iput-object v0, p0, LX/9WC;->n:LX/9WG;

    .line 1500677
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/9WC;->q:Ljava/util/Set;

    .line 1500678
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9WC;->x:Ljava/util/List;

    .line 1500679
    iget-object v0, p0, LX/9WC;->y:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9WC;->y:Landroid/os/Bundle;

    const-string v1, "analytics_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1500680
    iget-object v0, p0, LX/9WC;->r:LX/9Ve;

    iget-object v1, p0, LX/9WC;->y:Landroid/os/Bundle;

    const-string v2, "analytics_params"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1500681
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1500682
    iget-object p0, v0, LX/9Ve;->b:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1500683
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 10

    .prologue
    .line 1500668
    new-instance v0, LX/9Vx;

    const/4 v4, 0x0

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const/4 v8, 0x0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v8}, LX/9Vx;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, LX/9WC;->k:LX/9Vx;

    .line 1500669
    iget-object v0, p0, LX/9WC;->r:LX/9Ve;

    invoke-virtual {v0, p1, p4}, LX/9Ve;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500670
    iput-object p4, p0, LX/9WC;->w:Ljava/lang/String;

    .line 1500671
    iget-object v0, p0, LX/9WC;->k:LX/9Vx;

    invoke-static {p0, v0}, LX/9WC;->a$redex0(LX/9WC;LX/9Vx;)V

    .line 1500672
    return-void
.end method
