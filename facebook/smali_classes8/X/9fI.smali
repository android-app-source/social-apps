.class public LX/9fI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9eb;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

.field public final c:LX/9f1;

.field public final d:Landroid/view/ViewStub;

.field public final e:Lcom/facebook/resources/ui/FbTextView;

.field private final f:LX/9fO;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Gc;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0ad;

.field private final i:LX/8Gc;

.field private final j:Landroid/net/Uri;

.field public final k:Landroid/view/View;

.field public final l:Landroid/view/View;

.field public final m:Landroid/view/View;

.field public n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public o:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

.field private p:Z

.field private q:Z

.field private r:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

.field private s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;


# direct methods
.method public constructor <init>(Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;LX/9f1;LX/0am;Landroid/net/Uri;Landroid/view/View;Landroid/content/Context;LX/9fO;LX/0Or;LX/0ad;)V
    .locals 2
    .param p1    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/editgallery/EditableOverlayContainerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/9f1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewStub;",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Lcom/facebook/photos/editgallery/EditGallerySwipeableLayoutController;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/view/View;",
            "Landroid/content/Context;",
            "LX/9fO;",
            "LX/0Or",
            "<",
            "LX/8Gc;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1521680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1521681
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LX/9fI;->l:Landroid/view/View;

    .line 1521682
    iget-object v0, p0, LX/9fI;->l:Landroid/view/View;

    const v1, 0x7f0d0393

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9fI;->k:Landroid/view/View;

    .line 1521683
    iput-object p8, p0, LX/9fI;->a:Landroid/content/Context;

    .line 1521684
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, LX/9fI;->s:LX/0am;

    .line 1521685
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/9fI;->d:Landroid/view/ViewStub;

    .line 1521686
    iput-object p9, p0, LX/9fI;->f:LX/9fO;

    .line 1521687
    iget-object v0, p0, LX/9fI;->l:Landroid/view/View;

    const v1, 0x7f0d0392

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9fI;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1521688
    iput-object p3, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    .line 1521689
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, LX/9fI;->j:Landroid/net/Uri;

    .line 1521690
    iput-object p11, p0, LX/9fI;->h:LX/0ad;

    .line 1521691
    iget-object v0, p0, LX/9fI;->l:Landroid/view/View;

    const v1, 0x7f0d0394

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9fI;->m:Landroid/view/View;

    .line 1521692
    iput-object p10, p0, LX/9fI;->g:LX/0Or;

    .line 1521693
    iget-object v0, p0, LX/9fI;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Gc;

    iput-object v0, p0, LX/9fI;->i:LX/8Gc;

    .line 1521694
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iput-object v0, p0, LX/9fI;->r:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1521695
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9f1;

    iput-object v0, p0, LX/9fI;->c:LX/9f1;

    .line 1521696
    const/4 p1, 0x0

    .line 1521697
    invoke-static {p0}, LX/9fI;->q(LX/9fI;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1521698
    :goto_0
    return-void

    .line 1521699
    :cond_0
    iget-object v0, p0, LX/9fI;->c:LX/9f1;

    invoke-virtual {v0}, LX/9f1;->a()LX/9d5;

    move-result-object v0

    invoke-virtual {v0}, LX/9d5;->c()V

    .line 1521700
    iget-object v0, p0, LX/9fI;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/9fI;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1521701
    new-instance v0, LX/1P0;

    iget-object v1, p0, LX/9fI;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p1}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    .line 1521702
    iget-object v1, p0, LX/9fI;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1521703
    iget-object v0, p0, LX/9fI;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/62Z;

    iget-object p1, p0, LX/9fI;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a004f

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iget-object p2, p0, LX/9fI;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b0065

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    invoke-direct {v1, p1, p2}, LX/62Z;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    goto :goto_0
.end method

.method public static q(LX/9fI;)Z
    .locals 3

    .prologue
    .line 1521704
    iget-object v0, p0, LX/9fI;->h:LX/0ad;

    sget-short v1, LX/8Fn;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private u()V
    .locals 13

    .prologue
    .line 1521705
    invoke-static {p0}, LX/9fI;->q(LX/9fI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521706
    iget-object v0, p0, LX/9fI;->f:LX/9fO;

    iget-object v1, p0, LX/9fI;->a:Landroid/content/Context;

    iget-object v2, p0, LX/9fI;->j:Landroid/net/Uri;

    iget-object v3, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v4, p0, LX/9fI;->c:LX/9f1;

    .line 1521707
    new-instance v5, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v10

    check-cast v10, LX/1Ad;

    const-class v6, LX/9fQ;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/9fQ;

    invoke-static {v0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v12

    check-cast v12, LX/8GN;

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    invoke-direct/range {v5 .. v12}, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;LX/9f1;LX/1Ad;LX/9fQ;LX/8GN;)V

    .line 1521708
    move-object v0, v5

    .line 1521709
    iput-object v0, p0, LX/9fI;->o:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    .line 1521710
    iget-object v0, p0, LX/9fI;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/9fI;->o:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1521711
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1521712
    iget-object v0, p0, LX/9fI;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082416

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1521713
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1521714
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1521715
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521716
    iput-object p1, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521717
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9fI;->p:Z

    .line 1521718
    iget-object v0, p0, LX/9fI;->r:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setVisibility(I)V

    .line 1521719
    iget-object v0, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    .line 1521720
    iput-boolean v1, v0, LX/9dl;->j:Z

    .line 1521721
    invoke-static {p0}, LX/9fI;->q(LX/9fI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521722
    iget-object v0, p0, LX/9fI;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1521723
    :cond_0
    invoke-direct {p0}, LX/9fI;->u()V

    .line 1521724
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;)V
    .locals 1

    .prologue
    .line 1521725
    iget v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->e:I

    .line 1521726
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1521727
    iget-object v0, p0, LX/9fI;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1521728
    iget-object v0, p0, LX/9fI;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c7;

    iget-object v1, p0, LX/9fI;->c:LX/9f1;

    .line 1521729
    iget-object v2, v1, LX/9f1;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->aa:I

    move v2, v2

    .line 1521730
    iget-object v1, p0, LX/9fI;->c:LX/9f1;

    invoke-virtual {v1}, LX/9f1;->a()LX/9d5;

    move-result-object v1

    invoke-virtual {v1}, LX/9d5;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9fI;->c:LX/9f1;

    invoke-virtual {v1}, LX/9f1;->a()LX/9d5;

    move-result-object v1

    invoke-virtual {v1}, LX/9d5;->i()Ljava/lang/String;

    move-result-object v1

    .line 1521731
    :goto_0
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/9c4;->COMPOSER_FILTERS_IN_GALLERY:LX/9c4;

    invoke-virtual {p0}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "composer"

    .line 1521732
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1521733
    move-object v3, v3

    .line 1521734
    sget-object p0, LX/9c5;->NUM_OF_FILTER_SWIPES:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object p0, LX/9c5;->APPLIED_FILTER:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object p0, LX/9c5;->ACCEPTED:LX/9c5;

    invoke-virtual {p0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1521735
    invoke-static {v0, v3}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1521736
    :cond_0
    return-void

    .line 1521737
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1521670
    iget-boolean v0, p0, LX/9fI;->p:Z

    if-nez v0, :cond_0

    .line 1521671
    :goto_0
    return-void

    .line 1521672
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9fI;->p:Z

    .line 1521673
    iget-object v0, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x1

    .line 1521674
    iput-boolean v1, v0, LX/9dl;->j:Z

    .line 1521675
    iget-object v0, p0, LX/9fI;->l:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1521676
    invoke-static {p0}, LX/9fI;->q(LX/9fI;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1521677
    iget-object v0, p0, LX/9fI;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1521678
    :cond_1
    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1521679
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1521618
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1521619
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1521620
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1521621
    return-void
.end method

.method public final h()V
    .locals 5

    .prologue
    .line 1521622
    iget-object v0, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1521623
    iget-object v0, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setAlpha(F)V

    .line 1521624
    iget-object v0, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1521625
    iget-object v0, p0, LX/9fI;->i:LX/8Gc;

    invoke-virtual {v0}, LX/8Gc;->a()V

    .line 1521626
    iget-object v0, p0, LX/9fI;->i:LX/8Gc;

    iget-object v1, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/8Gc;->a(Landroid/view/View;I)V

    .line 1521627
    :cond_0
    const/16 v1, 0x8

    .line 1521628
    invoke-static {p0}, LX/9fI;->q(LX/9fI;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1521629
    const/4 v4, 0x0

    .line 1521630
    iget-object v0, p0, LX/9fI;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/9fI;->a:Landroid/content/Context;

    const v3, 0x7f082425

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1521631
    iget-object v0, p0, LX/9fI;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/9fI;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1521632
    iget-object v0, p0, LX/9fI;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/9fI;->a:Landroid/content/Context;

    const v3, 0x7f082425

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1521633
    iget-object v0, p0, LX/9fI;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1521634
    iget-object v0, p0, LX/9fI;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setClickable(Z)V

    .line 1521635
    :cond_1
    iget-object v0, p0, LX/9fI;->o:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    if-eqz v0, :cond_2

    .line 1521636
    iget-object v0, p0, LX/9fI;->c:LX/9f1;

    invoke-virtual {v0}, LX/9f1;->a()LX/9d5;

    move-result-object v0

    .line 1521637
    invoke-virtual {v0}, LX/9d5;->n()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1521638
    const/4 v2, 0x0

    .line 1521639
    :goto_0
    move-object v0, v2

    .line 1521640
    if-eqz v0, :cond_2

    .line 1521641
    iget-object v2, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521642
    iget-object v3, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    move-object v2, v3

    .line 1521643
    invoke-virtual {v2, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1521644
    iget-object v2, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521645
    iget-object v3, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    move-object v2, v3

    .line 1521646
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 1521647
    iget-object v3, p0, LX/9fI;->o:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    .line 1521648
    iput v0, v3, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->m:I

    .line 1521649
    iget-object v3, p0, LX/9fI;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    add-int/lit8 v4, v2, -0x1

    if-ne v0, v4, :cond_3

    :goto_1
    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 1521650
    :cond_2
    iget-object v0, p0, LX/9fI;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1521651
    iget-object v0, p0, LX/9fI;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1521652
    iget-object v0, p0, LX/9fI;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1521653
    return-void

    .line 1521654
    :cond_3
    add-int/lit8 v2, v2, -0x2

    if-ne v0, v2, :cond_4

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_5
    iget-object v2, v0, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1521655
    iget-object v0, p0, LX/9fI;->b:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->setVisibility(I)V

    .line 1521656
    iget-object v0, p0, LX/9fI;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1521657
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1521658
    sget-object v0, LX/5Rr;->FILTER:LX/5Rr;

    return-object v0
.end method

.method public final k()LX/9ek;
    .locals 1

    .prologue
    .line 1521659
    sget-object v0, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1521660
    iget-object v0, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521661
    iget-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1521662
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/9fI;->c:LX/9f1;

    invoke-virtual {v1}, LX/9f1;->a()LX/9d5;

    move-result-object v1

    invoke-virtual {v1}, LX/9d5;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/9fI;->q:Z

    .line 1521663
    iget-boolean v0, p0, LX/9fI;->q:Z

    return v0

    .line 1521664
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
    .locals 3

    .prologue
    .line 1521665
    iget-object v0, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1521666
    iget-object v2, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v2

    .line 1521667
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    iget-object v2, p0, LX/9fI;->c:LX/9f1;

    invoke-virtual {v2}, LX/9f1;->a()LX/9d5;

    move-result-object v2

    invoke-virtual {v2}, LX/9d5;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFilterName(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    iget-object v2, p0, LX/9fI;->c:LX/9f1;

    invoke-virtual {v2}, LX/9f1;->a()LX/9d5;

    move-result-object v2

    invoke-virtual {v2}, LX/9d5;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    .line 1521668
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1521669
    iget-object v0, p0, LX/9fI;->t:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    return-object v0
.end method
