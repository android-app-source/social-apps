.class public final LX/AFH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AEs;


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/AF9;

.field public final synthetic d:LX/AFJ;


# direct methods
.method public constructor <init>(LX/AFJ;Ljava/util/Set;LX/0Px;LX/AF9;)V
    .locals 0

    .prologue
    .line 1647488
    iput-object p1, p0, LX/AFH;->d:LX/AFJ;

    iput-object p2, p0, LX/AFH;->a:Ljava/util/Set;

    iput-object p3, p0, LX/AFH;->b:LX/0Px;

    iput-object p4, p0, LX/AFH;->c:LX/AF9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 13
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1647489
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1647490
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1647491
    iget-object v0, p0, LX/AFH;->a:Ljava/util/Set;

    invoke-static {v0, p1, v4, v5}, LX/AFJ;->b(Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 1647492
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1647493
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/model/ReplayableSession;

    .line 1647494
    iget-object v1, v0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    move-object v8, v1

    .line 1647495
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v3, v2

    :goto_0
    if-ge v3, v9, :cond_0

    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1647496
    invoke-static {v1, v4, v0}, LX/AFJ;->b(Ljava/lang/String;Ljava/util/Map;Lcom/facebook/audience/direct/model/ReplayableSession;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1647497
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1647498
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1647499
    :cond_2
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1647500
    iget-object v0, p0, LX/AFH;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_5

    iget-object v0, p0, LX/AFH;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7h0;

    .line 1647501
    iget-boolean v1, v0, LX/7h0;->h:Z

    move v1, v1

    .line 1647502
    if-eqz v1, :cond_3

    .line 1647503
    iget-object v1, v0, LX/7h0;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1647504
    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 1647505
    :goto_2
    new-instance v8, LX/7gx;

    invoke-direct {v8, v0}, LX/7gx;-><init>(LX/7h0;)V

    .line 1647506
    iput-boolean v1, v8, LX/7gx;->l:Z

    .line 1647507
    move-object v8, v8

    .line 1647508
    if-eqz v1, :cond_4

    .line 1647509
    iget-object v1, v0, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v1

    .line 1647510
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/model/ReplayableSession;

    .line 1647511
    iget-wide v11, v0, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    move-wide v0, v11

    .line 1647512
    :goto_3
    iput-wide v0, v8, LX/7gx;->m:J

    .line 1647513
    move-object v0, v8

    .line 1647514
    invoke-virtual {v0}, LX/7gx;->a()LX/7h0;

    move-result-object v0

    .line 1647515
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1647516
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 1647517
    goto :goto_2

    .line 1647518
    :cond_4
    const-wide/16 v0, -0x1

    goto :goto_3

    .line 1647519
    :cond_5
    iget-object v0, p0, LX/AFH;->c:LX/AF9;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1647520
    iget-object v2, v0, LX/AF9;->a:LX/AFA;

    iget-object v2, v2, LX/AFA;->b:LX/0gI;

    iget-object v2, v2, LX/0gI;->i:LX/1EY;

    new-instance v3, LX/AF8;

    invoke-direct {v3, v0}, LX/AF8;-><init>(LX/AF9;)V

    invoke-virtual {v2, v1, v3}, LX/1EY;->a(LX/0Px;LX/AF8;)V

    .line 1647521
    return-void
.end method
