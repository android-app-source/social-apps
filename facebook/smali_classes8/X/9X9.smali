.class public final enum LX/9X9;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9X9;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9X9;

.field public static final enum EVENT_PAGE_DETAILS_PARALLEL_FETCH_STARTED:LX/9X9;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1502373
    new-instance v0, LX/9X9;

    const-string v1, "EVENT_PAGE_DETAILS_PARALLEL_FETCH_STARTED"

    const-string v2, "page_header_parallel_fetch_started"

    invoke-direct {v0, v1, v3, v2}, LX/9X9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X9;->EVENT_PAGE_DETAILS_PARALLEL_FETCH_STARTED:LX/9X9;

    .line 1502374
    const/4 v0, 0x1

    new-array v0, v0, [LX/9X9;

    sget-object v1, LX/9X9;->EVENT_PAGE_DETAILS_PARALLEL_FETCH_STARTED:LX/9X9;

    aput-object v1, v0, v3

    sput-object v0, LX/9X9;->$VALUES:[LX/9X9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502375
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502376
    iput-object p3, p0, LX/9X9;->mEventName:Ljava/lang/String;

    .line 1502377
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9X9;
    .locals 1

    .prologue
    .line 1502378
    const-class v0, LX/9X9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9X9;

    return-object v0
.end method

.method public static values()[LX/9X9;
    .locals 1

    .prologue
    .line 1502379
    sget-object v0, LX/9X9;->$VALUES:[LX/9X9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9X9;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502380
    iget-object v0, p0, LX/9X9;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502381
    sget-object v0, LX/9XC;->TAP:LX/9XC;

    return-object v0
.end method
