.class public LX/9HX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/9CG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1461420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1461421
    return-void
.end method

.method public static a(LX/0QB;)LX/9HX;
    .locals 4

    .prologue
    .line 1461422
    const-class v1, LX/9HX;

    monitor-enter v1

    .line 1461423
    :try_start_0
    sget-object v0, LX/9HX;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461424
    sput-object v2, LX/9HX;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461425
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461426
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461427
    new-instance p0, LX/9HX;

    invoke-direct {p0}, LX/9HX;-><init>()V

    .line 1461428
    invoke-static {v0}, LX/9CG;->a(LX/0QB;)LX/9CG;

    move-result-object v3

    check-cast v3, LX/9CG;

    .line 1461429
    iput-object v3, p0, LX/9HX;->a:LX/9CG;

    .line 1461430
    move-object v0, p0

    .line 1461431
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461432
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9HX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461433
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461434
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
