.class public LX/917;
.super LX/3pF;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/90F;


# direct methods
.method public constructor <init>(LX/0gc;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1430147
    invoke-direct {p0, p1}, LX/3pF;-><init>(LX/0gc;)V

    .line 1430148
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1430149
    invoke-static {}, LX/93G;->values()[LX/93G;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1430150
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v4, v4, LX/93G;->mTitleResource:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1430151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1430152
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/917;->a:LX/0Px;

    .line 1430153
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1430144
    :try_start_0
    iget-object v0, p0, LX/917;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1430145
    :catch_0
    move-exception v0

    .line 1430146
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 1430129
    :try_start_0
    invoke-static {}, LX/93G;->values()[LX/93G;

    move-result-object v0

    aget-object v0, v0, p1

    .line 1430130
    sget-object v1, LX/916;->a:[I

    invoke-virtual {v0}, LX/93G;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1430131
    const-class v1, LX/917;

    const-string v2, "Unknown class for tab %s"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v0, p0, p1

    invoke-static {v1, v2, p0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1430132
    const-class v1, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    :goto_0
    move-object v0, v1

    .line 1430133
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1430134
    :catch_0
    move-exception v0

    .line 1430135
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1430136
    :pswitch_0
    const-class v1, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    goto :goto_0

    .line 1430137
    :pswitch_1
    const-class v1, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;

    goto :goto_0

    .line 1430138
    :pswitch_2
    const-class v1, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1430143
    invoke-static {}, LX/93G;->values()[LX/93G;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1430139
    instance-of v0, p3, LX/90F;

    if-eqz v0, :cond_0

    move-object v0, p3

    .line 1430140
    check-cast v0, LX/90F;

    iput-object v0, p0, LX/917;->b:LX/90F;

    .line 1430141
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/3pF;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1430142
    return-void
.end method
