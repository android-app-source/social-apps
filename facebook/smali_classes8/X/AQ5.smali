.class public final LX/AQ5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AQ4",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AQA;


# direct methods
.method public constructor <init>(LX/AQA;)V
    .locals 0

    .prologue
    .line 1670740
    iput-object p1, p0, LX/AQ5;->a:LX/AQA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1670741
    iget-object v0, p0, LX/AQ5;->a:LX/AQA;

    .line 1670742
    iget-object v1, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v0, v1

    .line 1670743
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1670744
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    .line 1670745
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1670746
    invoke-static {v1, v0}, LX/AQ3;->a(LX/0il;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 1670747
    sget-object v1, LX/AQ6;->a:[I

    iget-object v2, p0, LX/AQ5;->a:LX/AQA;

    .line 1670748
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1670749
    invoke-virtual {v2}, LX/B5j;->e()LX/2zG;

    move-result-object v2

    invoke-interface {v2}, LX/5RE;->I()LX/5RF;

    move-result-object v2

    invoke-virtual {v2}, LX/5RF;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1670750
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1670751
    :pswitch_0
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->x:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670752
    :pswitch_1
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->u:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670753
    :pswitch_2
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->r:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670754
    :pswitch_3
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->v:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670755
    :pswitch_4
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->s:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670756
    :pswitch_5
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->p:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670757
    :pswitch_6
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->t:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670758
    :pswitch_7
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->q:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670759
    :pswitch_8
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->o:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670760
    :pswitch_9
    iget-object v1, p0, LX/AQ5;->a:LX/AQA;

    iget-object v1, v1, LX/AQA;->a:LX/0ad;

    sget-char v2, LX/1EB;->w:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
