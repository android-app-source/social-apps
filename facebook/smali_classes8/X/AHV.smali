.class public final LX/AHV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AFM;

.field public final synthetic c:LX/AHZ;


# direct methods
.method public constructor <init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V
    .locals 0

    .prologue
    .line 1655683
    iput-object p1, p0, LX/AHV;->c:LX/AHZ;

    iput-object p2, p0, LX/AHV;->a:Ljava/lang/String;

    iput-object p3, p0, LX/AHV;->b:LX/AFM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1655684
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "Reply result not available"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1655685
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1655686
    check-cast p1, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel;

    .line 1655687
    if-nez p1, :cond_0

    .line 1655688
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "null result."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655689
    :goto_0
    return-void

    .line 1655690
    :cond_0
    new-instance v0, LX/AHf;

    iget-object v1, p0, LX/AHV;->c:LX/AHZ;

    iget-object v1, v1, LX/AHZ;->d:Ljava/lang/String;

    iget-object v2, p0, LX/AHV;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel;->a()Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/AHf;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel;)V

    .line 1655691
    iget-object v1, p0, LX/AHV;->b:LX/AFM;

    invoke-virtual {v0}, LX/AHa;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AFM;->a(Lcom/facebook/audience/model/ReplyThread;)V

    goto :goto_0
.end method
