.class public LX/9F0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# instance fields
.field public c:Lcom/facebook/graphql/model/GraphQLStory;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

.field public f:Landroid/support/v4/app/FragmentActivity;

.field public g:Z

.field public h:Z

.field public i:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8pb;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/3iG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/0i4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20q;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9Ck;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9D5;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/speech/SpeechRecognition;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1457889
    const-class v0, LX/9F0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9F0;->a:Ljava/lang/String;

    .line 1457890
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    sput-object v0, LX/9F0;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1457896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457897
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1457898
    iput-object v0, p0, LX/9F0;->l:LX/0Ot;

    .line 1457899
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1457900
    iput-object v0, p0, LX/9F0;->n:LX/0Ot;

    .line 1457901
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1457902
    iput-object v0, p0, LX/9F0;->o:LX/0Ot;

    .line 1457903
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1457904
    iput-object v0, p0, LX/9F0;->p:LX/0Ot;

    .line 1457905
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1457871
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1457872
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v6

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    move-object v3, p1

    move v5, v4

    move-object v7, v6

    move v8, v4

    move v9, v4

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;ZI)V

    goto :goto_0
.end method

.method public static a$redex0(LX/9F0;Lcom/facebook/speech/Result;)V
    .locals 2

    .prologue
    .line 1457891
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    if-nez v0, :cond_1

    .line 1457892
    :cond_0
    :goto_0
    return-void

    .line 1457893
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/speech/Result;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1457894
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1457895
    iget-object v1, p0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v1, v0}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static b(LX/9F0;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1457880
    iget-object v0, p0, LX/9F0;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/9F0;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457881
    return-void
.end method

.method public static e(LX/9F0;)V
    .locals 3

    .prologue
    .line 1457882
    iget-object v0, p0, LX/9F0;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/speech/SpeechRecognition;

    const-string v1, "comments"

    .line 1457883
    iput-object v1, v0, Lcom/facebook/speech/SpeechRecognition;->t:Ljava/lang/String;

    .line 1457884
    iget-object v2, v0, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-virtual {v2, v1}, Lcom/facebook/speech/SpeechRecognitionClient;->a(Ljava/lang/String;)V

    .line 1457885
    iget-object v0, p0, LX/9F0;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/speech/SpeechRecognition;

    new-instance v1, LX/9Ey;

    invoke-direct {v1, p0}, LX/9Ey;-><init>(LX/9F0;)V

    .line 1457886
    iput-object v1, v0, Lcom/facebook/speech/SpeechRecognition;->n:LX/9Ey;

    .line 1457887
    iget-object v2, v0, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    new-instance p0, LX/A5Q;

    invoke-direct {p0, v0}, LX/A5Q;-><init>(Lcom/facebook/speech/SpeechRecognition;)V

    invoke-virtual {v2, p0}, Lcom/facebook/speech/SpeechRecognitionClient;->a(Lcom/facebook/speech/SpeechRecognitionClient$ClientRecognitionListener;)V

    .line 1457888
    return-void
.end method

.method public static f(LX/9F0;)V
    .locals 1

    .prologue
    .line 1457876
    iget-object v0, p0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    if-nez v0, :cond_0

    .line 1457877
    :goto_0
    return-void

    .line 1457878
    :cond_0
    iget-object v0, p0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->b()V

    .line 1457879
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9F0;->g:Z

    goto :goto_0
.end method

.method public static g(LX/9F0;)V
    .locals 1

    .prologue
    .line 1457873
    iget-object v0, p0, LX/9F0;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1457874
    iget-object v0, p0, LX/9F0;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/speech/SpeechRecognition;

    invoke-virtual {v0}, Lcom/facebook/speech/SpeechRecognition;->b()V

    .line 1457875
    :cond_0
    return-void
.end method

.method public static i(LX/9F0;)V
    .locals 4

    .prologue
    .line 1457862
    iget-object v0, p0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1457863
    iget-object v1, p0, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, p0, LX/9F0;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20q;

    const/4 v2, 0x0

    .line 1457864
    invoke-virtual {v0}, LX/20q;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1457865
    :goto_0
    move-object v0, v2

    .line 1457866
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1457867
    return-void

    .line 1457868
    :cond_0
    iget-object v3, v0, LX/20q;->g:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 1457869
    iget-object v3, v0, LX/20q;->a:LX/0ad;

    sget-char p0, LX/0wn;->K:C

    invoke-interface {v3, p0, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/20q;->g:Ljava/lang/String;

    .line 1457870
    :cond_1
    iget-object v2, v0, LX/20q;->g:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final d()V
    .locals 6

    .prologue
    .line 1457844
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9F0;->g:Z

    .line 1457845
    iget-object v0, p0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->a(Ljava/lang/CharSequence;)V

    .line 1457846
    iget-object v0, p0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    .line 1457847
    iget-object v2, v0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, v0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20q;

    const/4 v3, 0x0

    .line 1457848
    invoke-virtual {v1}, LX/20q;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1457849
    :goto_0
    move-object v1, v3

    .line 1457850
    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1457851
    iget-object v1, v0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->p:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 1457852
    iget-object v1, v0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1457853
    iget-object v0, p0, LX/9F0;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/speech/SpeechRecognition;

    invoke-virtual {v0}, Lcom/facebook/speech/SpeechRecognition;->a()V

    .line 1457854
    iget-object v0, p0, LX/9F0;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9D5;

    .line 1457855
    iget-object v1, v0, LX/9D5;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0if;

    .line 1457856
    sget-object v2, LX/0ig;->M:LX/0ih;

    move-object v2, v2

    .line 1457857
    const-string v3, "speech_listening_event_started"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1457858
    return-void

    .line 1457859
    :cond_0
    iget-object v4, v1, LX/20q;->e:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 1457860
    iget-object v4, v1, LX/20q;->a:LX/0ad;

    sget-char v5, LX/0wn;->F:C

    invoke-interface {v4, v5, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, LX/20q;->e:Ljava/lang/String;

    .line 1457861
    :cond_1
    iget-object v3, v1, LX/20q;->e:Ljava/lang/String;

    goto :goto_0
.end method
