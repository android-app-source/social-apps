.class public final LX/91O;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/91P;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public b:LX/903;

.field public final synthetic c:LX/91P;


# direct methods
.method public constructor <init>(LX/91P;)V
    .locals 1

    .prologue
    .line 1430643
    iput-object p1, p0, LX/91O;->c:LX/91P;

    .line 1430644
    move-object v0, p1

    .line 1430645
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1430646
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1430647
    const-string v0, "MinutiaeSelectedObjectComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1430629
    if-ne p0, p1, :cond_1

    .line 1430630
    :cond_0
    :goto_0
    return v0

    .line 1430631
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1430632
    goto :goto_0

    .line 1430633
    :cond_3
    check-cast p1, LX/91O;

    .line 1430634
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1430635
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1430636
    if-eq v2, v3, :cond_0

    .line 1430637
    iget-object v2, p0, LX/91O;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/91O;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, LX/91O;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1430638
    goto :goto_0

    .line 1430639
    :cond_5
    iget-object v2, p1, LX/91O;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v2, :cond_4

    .line 1430640
    :cond_6
    iget-object v2, p0, LX/91O;->b:LX/903;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/91O;->b:LX/903;

    iget-object v3, p1, LX/91O;->b:LX/903;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1430641
    goto :goto_0

    .line 1430642
    :cond_7
    iget-object v2, p1, LX/91O;->b:LX/903;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
