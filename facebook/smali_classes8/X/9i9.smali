.class public final LX/9i9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8z9;


# instance fields
.field public final synthetic a:LX/9hC;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/9iA;


# direct methods
.method public constructor <init>(LX/9iA;LX/9hC;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1527276
    iput-object p1, p0, LX/9i9;->c:LX/9iA;

    iput-object p2, p0, LX/9i9;->a:LX/9hC;

    iput-object p3, p0, LX/9i9;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1527277
    iget-object v0, p0, LX/9i9;->c:LX/9iA;

    iget-object v1, p0, LX/9i9;->a:LX/9hC;

    invoke-interface {v1}, LX/9hC;->getCurrentMedia()LX/5kD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9iA;->d(LX/5kD;)LX/0Px;

    move-result-object v2

    .line 1527278
    if-nez v2, :cond_0

    .line 1527279
    :goto_0
    return-void

    .line 1527280
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1527281
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1527282
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1527283
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1527284
    :cond_1
    iget-object v0, p0, LX/9i9;->c:LX/9iA;

    iget-object v0, v0, LX/9iA;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8qT;

    iget-object v1, p0, LX/9i9;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, v3}, LX/8qT;->a(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1527264
    iget-object v0, p0, LX/9i9;->c:LX/9iA;

    iget-object v1, p0, LX/9i9;->a:LX/9hC;

    invoke-interface {v1}, LX/9hC;->getCurrentMedia()LX/5kD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9iA;->d(LX/5kD;)LX/0Px;

    move-result-object v4

    .line 1527265
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1527266
    :cond_0
    :goto_0
    return-void

    .line 1527267
    :cond_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1527268
    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget-wide v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v6, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-static {v5, v1, v6, v0}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1527269
    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    move v1, v2

    .line 1527270
    :goto_1
    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    if-ne v0, v2, :cond_3

    .line 1527271
    :goto_2
    if-eqz v1, :cond_4

    sget-object v0, LX/0ax;->bw:Ljava/lang/String;

    move-object v1, v0

    :goto_3
    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    iget-wide v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1527272
    iget-object v0, p0, LX/9i9;->c:LX/9iA;

    iget-object v0, v0, LX/9iA;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/9i9;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v5, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    goto :goto_0

    :cond_2
    move v1, v3

    .line 1527273
    goto :goto_1

    :cond_3
    move v2, v3

    .line 1527274
    goto :goto_2

    .line 1527275
    :cond_4
    if-eqz v2, :cond_5

    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    move-object v1, v0

    goto :goto_3

    :cond_5
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    move-object v1, v0

    goto :goto_3
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1527285
    iget-object v0, p0, LX/9i9;->a:LX/9hC;

    invoke-interface {v0}, LX/9hC;->getCurrentMedia()LX/5kD;

    move-result-object v0

    invoke-interface {v0}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v0

    .line 1527286
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1527287
    :cond_0
    :goto_0
    return-void

    .line 1527288
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1527289
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v4}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1527290
    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1527291
    iget-object v0, p0, LX/9i9;->c:LX/9iA;

    iget-object v0, v0, LX/9iA;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/9i9;->b:Landroid/content/Context;

    invoke-interface {v0, v3, v2, v1, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1527263
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1527261
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1527262
    return-void
.end method
