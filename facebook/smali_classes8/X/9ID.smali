.class public final LX/9ID;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;)V
    .locals 1

    .prologue
    .line 1462674
    iput-object p1, p0, LX/9ID;->b:Lcom/facebook/feedback/ui/rows/views/specs/CommentShareAttachmentComponent;

    .line 1462675
    move-object v0, p1

    .line 1462676
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1462677
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1462678
    const-string v0, "CommentShareAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1462679
    if-ne p0, p1, :cond_1

    .line 1462680
    :cond_0
    :goto_0
    return v0

    .line 1462681
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1462682
    goto :goto_0

    .line 1462683
    :cond_3
    check-cast p1, LX/9ID;

    .line 1462684
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1462685
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1462686
    if-eq v2, v3, :cond_0

    .line 1462687
    iget-object v2, p0, LX/9ID;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/9ID;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/9ID;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1462688
    goto :goto_0

    .line 1462689
    :cond_4
    iget-object v2, p1, LX/9ID;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
