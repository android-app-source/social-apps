.class public final LX/989;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1444997
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1444998
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1444999
    :goto_0
    return v1

    .line 1445000
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1445001
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1445002
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1445003
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1445004
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1445005
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1445006
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1445007
    :cond_2
    const-string v5, "location"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1445008
    invoke-static {p0, p1}, LX/988;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1445009
    :cond_3
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1445010
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1445011
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1445012
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1445013
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1445014
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1445015
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1445016
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1445017
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445018
    if-eqz v0, :cond_0

    .line 1445019
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445020
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445021
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445022
    if-eqz v0, :cond_3

    .line 1445023
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445024
    const-wide/16 v6, 0x0

    .line 1445025
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1445026
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1445027
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_1

    .line 1445028
    const-string v4, "latitude"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445029
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 1445030
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1445031
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_2

    .line 1445032
    const-string v4, "longitude"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445033
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 1445034
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1445035
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445036
    if-eqz v0, :cond_4

    .line 1445037
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445038
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445039
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1445040
    return-void
.end method
