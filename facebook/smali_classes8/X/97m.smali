.class public final LX/97m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "category"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "category"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$FriModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$MonModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedLocatedInModel$ParentPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$ThuModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$TueModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$WedModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1442225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1442226
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97m;
    .locals 4

    .prologue
    .line 1442227
    new-instance v0, LX/97m;

    invoke-direct {v0}, LX/97m;-><init>()V

    .line 1442228
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/97m;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1442229
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/97m;->b:LX/15i;

    iput v1, v0, LX/97m;->c:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1442230
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->r()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v1

    iput-object v1, v0, LX/97m;->d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    .line 1442231
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->e()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/97m;->e:LX/0Px;

    .line 1442232
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->gt_()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/97m;->f:LX/0Px;

    .line 1442233
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->s()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedLocatedInModel$ParentPlaceModel;

    move-result-object v1

    iput-object v1, v0, LX/97m;->g:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedLocatedInModel$ParentPlaceModel;

    .line 1442234
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->c()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/97m;->h:LX/15i;

    iput v1, v0, LX/97m;->i:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1442235
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/97m;->j:LX/0Px;

    .line 1442236
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/97m;->k:Ljava/lang/String;

    .line 1442237
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/97m;->l:LX/0Px;

    .line 1442238
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/97m;->m:Ljava/lang/String;

    .line 1442239
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->n()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/97m;->n:LX/0Px;

    .line 1442240
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->o()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/97m;->o:LX/0Px;

    .line 1442241
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->p()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/97m;->p:LX/0Px;

    .line 1442242
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/97m;->q:Ljava/lang/String;

    .line 1442243
    return-object v0

    .line 1442244
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1442245
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/15i;I)LX/97m;
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "category"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1442246
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/97m;->b:LX/15i;

    iput p2, p0, LX/97m;->c:I

    monitor-exit v1

    .line 1442247
    return-object p0

    .line 1442248
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;
    .locals 18

    .prologue
    .line 1442249
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1442250
    move-object/from16 v0, p0

    iget-object v2, v0, LX/97m;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1442251
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/97m;->b:LX/15i;

    move-object/from16 v0, p0

    iget v5, v0, LX/97m;->c:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v3, 0x31f7b0b9

    invoke-static {v4, v5, v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1442252
    move-object/from16 v0, p0

    iget-object v4, v0, LX/97m;->d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1442253
    move-object/from16 v0, p0

    iget-object v5, v0, LX/97m;->e:LX/0Px;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1442254
    move-object/from16 v0, p0

    iget-object v6, v0, LX/97m;->f:LX/0Px;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1442255
    move-object/from16 v0, p0

    iget-object v7, v0, LX/97m;->g:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedLocatedInModel$ParentPlaceModel;

    invoke-static {v1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1442256
    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_1
    move-object/from16 v0, p0

    iget-object v9, v0, LX/97m;->h:LX/15i;

    move-object/from16 v0, p0

    iget v10, v0, LX/97m;->i:I

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v8, 0x72e9906d

    invoke-static {v9, v10, v8}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;

    move-result-object v8

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1442257
    move-object/from16 v0, p0

    iget-object v9, v0, LX/97m;->j:LX/0Px;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 1442258
    move-object/from16 v0, p0

    iget-object v10, v0, LX/97m;->k:Ljava/lang/String;

    invoke-virtual {v1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1442259
    move-object/from16 v0, p0

    iget-object v11, v0, LX/97m;->l:LX/0Px;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 1442260
    move-object/from16 v0, p0

    iget-object v12, v0, LX/97m;->m:Ljava/lang/String;

    invoke-virtual {v1, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1442261
    move-object/from16 v0, p0

    iget-object v13, v0, LX/97m;->n:LX/0Px;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 1442262
    move-object/from16 v0, p0

    iget-object v14, v0, LX/97m;->o:LX/0Px;

    invoke-static {v1, v14}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v14

    .line 1442263
    move-object/from16 v0, p0

    iget-object v15, v0, LX/97m;->p:LX/0Px;

    invoke-static {v1, v15}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v15

    .line 1442264
    move-object/from16 v0, p0

    iget-object v0, v0, LX/97m;->q:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1442265
    const/16 v17, 0xf

    move/from16 v0, v17

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1442266
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1442267
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1442268
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1442269
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1442270
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1442271
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1442272
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1442273
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1442274
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1442275
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1442276
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1442277
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1442278
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1442279
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1442280
    const/16 v2, 0xe

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1442281
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1442282
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1442283
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1442284
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1442285
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1442286
    new-instance v2, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    invoke-direct {v2, v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;-><init>(LX/15i;)V

    .line 1442287
    return-object v2

    .line 1442288
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1442289
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public final b(LX/15i;I)LX/97m;
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1442290
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/97m;->h:LX/15i;

    iput p2, p0, LX/97m;->i:I

    monitor-exit v1

    .line 1442291
    return-object p0

    .line 1442292
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
