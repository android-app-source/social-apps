.class public LX/AEW;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/2yS;

.field private final b:LX/1vg;

.field public final c:Landroid/content/res/Resources;

.field private final d:LX/1Nt;

.field private final e:LX/3mF;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mG;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;

.field private final i:LX/2yT;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2yS;LX/1vg;Landroid/content/res/Resources;LX/3mF;LX/0Ot;LX/0Ot;LX/0ad;LX/2yT;)V
    .locals 2
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2yS;",
            "LX/1vg;",
            "Landroid/content/res/Resources;",
            "LX/3mF;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3mG;",
            ">;",
            "LX/0ad;",
            "LX/2yT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646676
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646677
    iput-object p2, p0, LX/AEW;->a:LX/2yS;

    .line 1646678
    iput-object p3, p0, LX/AEW;->b:LX/1vg;

    .line 1646679
    iput-object p4, p0, LX/AEW;->c:Landroid/content/res/Resources;

    .line 1646680
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;-><init>(LX/AEW;)V

    iput-object v0, p0, LX/AEW;->d:LX/1Nt;

    .line 1646681
    iput-object p5, p0, LX/AEW;->e:LX/3mF;

    .line 1646682
    iput-object p6, p0, LX/AEW;->f:LX/0Ot;

    .line 1646683
    iput-object p7, p0, LX/AEW;->g:LX/0Ot;

    .line 1646684
    iput-object p8, p0, LX/AEW;->h:LX/0ad;

    .line 1646685
    iput-object p9, p0, LX/AEW;->i:LX/2yT;

    .line 1646686
    return-void
.end method

.method public static a(LX/0QB;)LX/AEW;
    .locals 13

    .prologue
    .line 1646665
    const-class v1, LX/AEW;

    monitor-enter v1

    .line 1646666
    :try_start_0
    sget-object v0, LX/AEW;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646667
    sput-object v2, LX/AEW;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646668
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646669
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646670
    new-instance v3, LX/AEW;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v5

    check-cast v5, LX/2yS;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v6

    check-cast v6, LX/1vg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v8

    check-cast v8, LX/3mF;

    const/16 v9, 0x1430

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x94c

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    const-class v12, LX/2yT;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/2yT;

    invoke-direct/range {v3 .. v12}, LX/AEW;-><init>(Landroid/content/Context;LX/2yS;LX/1vg;Landroid/content/res/Resources;LX/3mF;LX/0Ot;LX/0Ot;LX/0ad;LX/2yT;)V

    .line 1646671
    move-object v0, v3

    .line 1646672
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646673
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646674
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646675
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/AEW;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;LX/AEU;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "LX/AEU;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1646687
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646688
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1646689
    if-nez v0, :cond_1

    .line 1646690
    :cond_0
    :goto_0
    return-void

    .line 1646691
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    .line 1646692
    if-eqz v4, :cond_0

    .line 1646693
    iget-boolean v0, p3, LX/AEU;->a:Z

    move v3, v0

    .line 1646694
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 1646695
    if-nez v3, :cond_2

    move v0, v1

    .line 1646696
    :goto_1
    iput-boolean v0, p3, LX/AEU;->a:Z

    .line 1646697
    new-array v0, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object p1, v0, v2

    invoke-interface {p2, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1646698
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1646699
    const-string v0, "feed_attachment_sponsored"

    move-object v1, v0

    .line 1646700
    :goto_2
    if-eqz v3, :cond_4

    iget-object v0, p0, LX/AEW;->e:LX/3mF;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v2

    const-string v4, "ALLOW_READD"

    invoke-virtual {v0, v2, v1, v4}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v6, v0

    .line 1646701
    :goto_3
    new-instance v0, LX/AER;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/AER;-><init>(LX/AEW;LX/AEU;ZLX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1646702
    iget-object v1, p0, LX/AEW;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v6, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1646703
    goto :goto_1

    .line 1646704
    :cond_3
    const-string v0, "feed_attachment"

    move-object v1, v0

    goto :goto_2

    .line 1646705
    :cond_4
    iget-object v2, p0, LX/AEW;->e:LX/3mF;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v4

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 1646706
    :goto_4
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-static {v4, v1, v5, v0}, LX/3mF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Ljava/lang/String;)LX/399;

    move-result-object v5

    .line 1646707
    iget-object v6, v2, LX/3mF;->a:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v6

    invoke-static {v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 1646708
    move-object v6, v0

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646664
    iget-object v0, p0, LX/AEW;->d:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 7

    .prologue
    .line 1646643
    check-cast p2, LX/1Pq;

    .line 1646644
    invoke-static {p3}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    move-object v0, p2

    .line 1646645
    check-cast v0, LX/1Pr;

    new-instance v2, LX/AET;

    invoke-direct {v2, v1}, LX/AET;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v0, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AEU;

    .line 1646646
    iget-boolean v1, v0, LX/AEU;->a:Z

    move v1, v1

    .line 1646647
    if-eqz v1, :cond_0

    .line 1646648
    const v3, 0x7f081b9a

    .line 1646649
    const v2, 0x7f020a52

    .line 1646650
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a00d2

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1646651
    :goto_0
    iget-object v4, p0, LX/AEW;->i:LX/2yT;

    const/4 v5, 0x2

    iget-object v6, p0, LX/AEW;->a:LX/2yS;

    invoke-virtual {v4, p4, v5, v6}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v4

    const/4 v5, 0x0

    .line 1646652
    iput-boolean v5, v4, LX/AE0;->l:Z

    .line 1646653
    move-object v4, v4

    .line 1646654
    invoke-virtual {p1, v3}, LX/1De;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1646655
    iput-object v3, v4, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 1646656
    move-object v3, v4

    .line 1646657
    invoke-virtual {v3, v1}, LX/AE0;->c(I)LX/AE0;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/AE0;->a(I)LX/AE0;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/AE0;->d(I)LX/AE0;

    move-result-object v1

    new-instance v2, LX/AEQ;

    invoke-direct {v2, p0, p3, p2, v0}, LX/AEQ;-><init>(LX/AEW;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;LX/AEU;)V

    .line 1646658
    iput-object v2, v1, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1646659
    move-object v0, v1

    .line 1646660
    return-object v0

    .line 1646661
    :cond_0
    const v3, 0x7f081b99

    .line 1646662
    const v2, 0x7f020ad5

    .line 1646663
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a033a

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0
.end method
