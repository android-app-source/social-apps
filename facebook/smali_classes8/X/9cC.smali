.class public final LX/9cC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:Ljava/lang/String;

.field public o:LX/5Rz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1516027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516028
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)V
    .locals 1

    .prologue
    .line 1516029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516030
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a:Z

    iput-boolean v0, p0, LX/9cC;->a:Z

    .line 1516031
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->b:Z

    iput-boolean v0, p0, LX/9cC;->b:Z

    .line 1516032
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->c:Z

    iput-boolean v0, p0, LX/9cC;->c:Z

    .line 1516033
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    iput v0, p0, LX/9cC;->d:I

    .line 1516034
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    iput v0, p0, LX/9cC;->e:I

    .line 1516035
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    iput v0, p0, LX/9cC;->f:I

    .line 1516036
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    iput v0, p0, LX/9cC;->g:I

    .line 1516037
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    iput v0, p0, LX/9cC;->h:I

    .line 1516038
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    iput v0, p0, LX/9cC;->i:I

    .line 1516039
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->j:I

    iput v0, p0, LX/9cC;->j:I

    .line 1516040
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->k:I

    iput v0, p0, LX/9cC;->k:I

    .line 1516041
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->l:I

    iput v0, p0, LX/9cC;->l:I

    .line 1516042
    iget v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->m:I

    iput v0, p0, LX/9cC;->m:I

    .line 1516043
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->n:Ljava/lang/String;

    iput-object v0, p0, LX/9cC;->n:Ljava/lang/String;

    .line 1516044
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->o:LX/5Rz;

    iput-object v0, p0, LX/9cC;->o:LX/5Rz;

    .line 1516045
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;
    .locals 2

    .prologue
    .line 1516046
    new-instance v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;-><init>(LX/9cC;)V

    return-object v0
.end method
