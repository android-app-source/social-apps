.class public final LX/A5g;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/Long;",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 0

    .prologue
    .line 1621712
    iput-object p1, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1621713
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->w:LX/03V;

    const-string v1, "FriendSuggestionsAndSelectorFragment contact fetch failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1621714
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621715
    const-string v1, "QueryFriends"

    .line 1621716
    iget-object v2, v0, LX/7kp;->b:LX/11i;

    sget-object p1, LX/7kp;->a:LX/7ko;

    invoke-interface {v2, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    .line 1621717
    if-eqz v2, :cond_0

    .line 1621718
    const p1, 0x75fd10ef

    invoke-static {v2, v1, p1}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1621719
    :cond_0
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    invoke-virtual {v0}, LX/7kp;->m()V

    .line 1621720
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1621721
    check-cast p1, LX/0P1;

    .line 1621722
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621723
    const-string v1, "UpdateContactList"

    invoke-static {v0, v1}, LX/7kp;->a(LX/7kp;Ljava/lang/String;)V

    .line 1621724
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    .line 1621725
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->F:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->U:Z

    if-eqz v1, :cond_1

    .line 1621726
    :cond_0
    :goto_0
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-virtual {p1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v1

    .line 1621727
    iget-object v2, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->F:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f08139a

    :goto_1
    move v2, v2

    .line 1621728
    sget-object v3, LX/A5i;->ALL_FRIENDS:LX/A5i;

    invoke-static {v0, v2, v3, v1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;ILX/A5i;LX/0Px;)V

    .line 1621729
    invoke-static {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->r(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    .line 1621730
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621731
    const-string v1, "UpdateContactList"

    invoke-static {v0, v1}, LX/7kp;->c(LX/7kp;Ljava/lang/String;)V

    .line 1621732
    iget-object v0, p0, LX/A5g;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    invoke-virtual {v0}, LX/7kp;->m()V

    .line 1621733
    return-void

    .line 1621734
    :cond_1
    iget-object v1, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->J:LX/A5m;

    new-instance v2, LX/A5W;

    invoke-direct {v2, v0, p1}, LX/A5W;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/0P1;)V

    new-instance v3, LX/A5X;

    invoke-direct {v3, v0}, LX/A5X;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    invoke-virtual {v1, v2, v3}, LX/A5m;->a(LX/0TF;LX/0TF;)V

    goto :goto_0

    :cond_2
    const v2, 0x7f081398

    goto :goto_1
.end method
