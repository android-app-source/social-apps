.class public final enum LX/93E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/93E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/93E;

.field public static final enum OBJECT_PICKER:LX/93E;

.field public static final enum UNKNOWN:LX/93E;

.field public static final enum VERB_PICKER:LX/93E;


# instance fields
.field private final mFragmentName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1433532
    new-instance v0, LX/93E;

    const-string v1, "VERB_PICKER"

    const-string v2, "Verb"

    invoke-direct {v0, v1, v3, v2}, LX/93E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/93E;->VERB_PICKER:LX/93E;

    .line 1433533
    new-instance v0, LX/93E;

    const-string v1, "OBJECT_PICKER"

    const-string v2, "Object"

    invoke-direct {v0, v1, v4, v2}, LX/93E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/93E;->OBJECT_PICKER:LX/93E;

    .line 1433534
    new-instance v0, LX/93E;

    const-string v1, "UNKNOWN"

    const-string v2, "Unknown"

    invoke-direct {v0, v1, v5, v2}, LX/93E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/93E;->UNKNOWN:LX/93E;

    .line 1433535
    const/4 v0, 0x3

    new-array v0, v0, [LX/93E;

    sget-object v1, LX/93E;->VERB_PICKER:LX/93E;

    aput-object v1, v0, v3

    sget-object v1, LX/93E;->OBJECT_PICKER:LX/93E;

    aput-object v1, v0, v4

    sget-object v1, LX/93E;->UNKNOWN:LX/93E;

    aput-object v1, v0, v5

    sput-object v0, LX/93E;->$VALUES:[LX/93E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1433536
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1433537
    iput-object p3, p0, LX/93E;->mFragmentName:Ljava/lang/String;

    .line 1433538
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/93E;
    .locals 1

    .prologue
    .line 1433539
    sget-object v0, LX/93E;->VERB_PICKER:LX/93E;

    invoke-virtual {v0}, LX/93E;->getFragmentName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1433540
    sget-object v0, LX/93E;->VERB_PICKER:LX/93E;

    .line 1433541
    :goto_0
    return-object v0

    .line 1433542
    :cond_0
    sget-object v0, LX/93E;->OBJECT_PICKER:LX/93E;

    invoke-virtual {v0}, LX/93E;->getFragmentName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1433543
    sget-object v0, LX/93E;->OBJECT_PICKER:LX/93E;

    goto :goto_0

    .line 1433544
    :cond_1
    sget-object v0, LX/93E;->UNKNOWN:LX/93E;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/93E;
    .locals 1

    .prologue
    .line 1433545
    const-class v0, LX/93E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/93E;

    return-object v0
.end method

.method public static values()[LX/93E;
    .locals 1

    .prologue
    .line 1433546
    sget-object v0, LX/93E;->$VALUES:[LX/93E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/93E;

    return-object v0
.end method


# virtual methods
.method public final getFragmentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1433547
    iget-object v0, p0, LX/93E;->mFragmentName:Ljava/lang/String;

    return-object v0
.end method
