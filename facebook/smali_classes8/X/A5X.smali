.class public final LX/A5X;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "LX/8vB;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 0

    .prologue
    .line 1621603
    iput-object p1, p0, LX/A5X;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1621604
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1621591
    check-cast p1, LX/0Px;

    .line 1621592
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1621593
    :cond_0
    :goto_0
    return-void

    .line 1621594
    :cond_1
    iget-object v0, p0, LX/A5X;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    .line 1621595
    iput-object p1, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->T:Ljava/util/List;

    .line 1621596
    iget-object v0, p0, LX/A5X;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    sget-object v1, LX/A5i;->GROUP_SUGGESTIONS:LX/A5i;

    invoke-virtual {v1}, LX/A5i;->ordinal()I

    move-result v1

    new-instance v2, LX/A5o;

    iget-object v3, p0, LX/A5X;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0813b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1}, LX/A5o;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, LX/8tB;->a(ILX/621;)V

    .line 1621597
    iget-object v0, p0, LX/A5X;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->C:LX/A5T;

    iget-object v1, p0, LX/A5X;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621598
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1621599
    iget-object v3, v0, LX/A5T;->a:LX/0Zb;

    const-string v4, "group_tag_suggestions_shown"

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8vB;

    .line 1621600
    iget-object v0, v2, LX/8vB;->f:Ljava/lang/String;

    move-object v2, v0

    .line 1621601
    invoke-static {v4, v2, v1}, LX/A5T;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1621602
    goto :goto_0
.end method
