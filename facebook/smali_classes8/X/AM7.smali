.class public LX/AM7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/GestureDetector;

.field public b:Z

.field public c:LX/AM2;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1665820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1665821
    new-instance v0, LX/AM6;

    invoke-direct {v0, p0}, LX/AM6;-><init>(LX/AM7;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1665822
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/AM5;

    invoke-direct {v1, p0}, LX/AM5;-><init>(LX/AM7;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/AM7;->a:Landroid/view/GestureDetector;

    .line 1665823
    iget-object v0, p0, LX/AM7;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, v2}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1665824
    iget-object v0, p0, LX/AM7;->a:Landroid/view/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 1665825
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AM7;->b:Z

    .line 1665826
    return-void
.end method
