.class public LX/9yN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:Landroid/os/Handler;

.field public final c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/9yM;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/0tX;Landroid/os/Handler;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1596115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1596116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9yN;->d:Ljava/util/HashMap;

    .line 1596117
    new-instance v0, Lcom/facebook/search/protocol/BatchedSearchLoader$1;

    invoke-direct {v0, p0}, Lcom/facebook/search/protocol/BatchedSearchLoader$1;-><init>(LX/9yN;)V

    iput-object v0, p0, LX/9yN;->f:Ljava/lang/Runnable;

    .line 1596118
    iput-object p1, p0, LX/9yN;->a:LX/0tX;

    .line 1596119
    iput-object p2, p0, LX/9yN;->b:Landroid/os/Handler;

    .line 1596120
    const/16 v0, 0x1e

    iput v0, p0, LX/9yN;->e:I

    .line 1596121
    iput-object p3, p0, LX/9yN;->c:Ljava/util/concurrent/Executor;

    .line 1596122
    return-void
.end method

.method public static a(LX/0QB;)LX/9yN;
    .locals 6

    .prologue
    .line 1596123
    const-class v1, LX/9yN;

    monitor-enter v1

    .line 1596124
    :try_start_0
    sget-object v0, LX/9yN;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1596125
    sput-object v2, LX/9yN;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1596126
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1596127
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1596128
    new-instance p0, LX/9yN;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v4, v5}, LX/9yN;-><init>(LX/0tX;Landroid/os/Handler;Ljava/util/concurrent/Executor;)V

    .line 1596129
    move-object v0, p0

    .line 1596130
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1596131
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9yN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1596132
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1596133
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1596134
    iget-object v0, p0, LX/9yN;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1596135
    return-void
.end method
