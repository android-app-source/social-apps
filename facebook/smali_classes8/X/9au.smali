.class public final enum LX/9au;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9au;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9au;

.field public static final enum ALBUMSTAB:LX/9au;

.field public static final enum COMPOSER:LX/9au;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1514152
    new-instance v0, LX/9au;

    const-string v1, "ALBUMSTAB"

    invoke-direct {v0, v1, v2}, LX/9au;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9au;->ALBUMSTAB:LX/9au;

    .line 1514153
    new-instance v0, LX/9au;

    const-string v1, "COMPOSER"

    invoke-direct {v0, v1, v3}, LX/9au;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9au;->COMPOSER:LX/9au;

    .line 1514154
    const/4 v0, 0x2

    new-array v0, v0, [LX/9au;

    sget-object v1, LX/9au;->ALBUMSTAB:LX/9au;

    aput-object v1, v0, v2

    sget-object v1, LX/9au;->COMPOSER:LX/9au;

    aput-object v1, v0, v3

    sput-object v0, LX/9au;->$VALUES:[LX/9au;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1514151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9au;
    .locals 1

    .prologue
    .line 1514149
    const-class v0, LX/9au;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9au;

    return-object v0
.end method

.method public static values()[LX/9au;
    .locals 1

    .prologue
    .line 1514150
    sget-object v0, LX/9au;->$VALUES:[LX/9au;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9au;

    return-object v0
.end method
