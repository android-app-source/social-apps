.class public final enum LX/8zS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8zS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8zS;

.field public static final enum GENERIC_ERROR:LX/8zS;

.field public static final enum LOADED:LX/8zS;

.field public static final enum LOADING:LX/8zS;

.field public static final enum NETWORK_ERROR:LX/8zS;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1427442
    new-instance v0, LX/8zS;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v2}, LX/8zS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8zS;->LOADED:LX/8zS;

    .line 1427443
    new-instance v0, LX/8zS;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/8zS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8zS;->LOADING:LX/8zS;

    .line 1427444
    new-instance v0, LX/8zS;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v4}, LX/8zS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8zS;->NETWORK_ERROR:LX/8zS;

    .line 1427445
    new-instance v0, LX/8zS;

    const-string v1, "GENERIC_ERROR"

    invoke-direct {v0, v1, v5}, LX/8zS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8zS;->GENERIC_ERROR:LX/8zS;

    .line 1427446
    const/4 v0, 0x4

    new-array v0, v0, [LX/8zS;

    sget-object v1, LX/8zS;->LOADED:LX/8zS;

    aput-object v1, v0, v2

    sget-object v1, LX/8zS;->LOADING:LX/8zS;

    aput-object v1, v0, v3

    sget-object v1, LX/8zS;->NETWORK_ERROR:LX/8zS;

    aput-object v1, v0, v4

    sget-object v1, LX/8zS;->GENERIC_ERROR:LX/8zS;

    aput-object v1, v0, v5

    sput-object v0, LX/8zS;->$VALUES:[LX/8zS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1427441
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8zS;
    .locals 1

    .prologue
    .line 1427447
    const-class v0, LX/8zS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8zS;

    return-object v0
.end method

.method public static values()[LX/8zS;
    .locals 1

    .prologue
    .line 1427440
    sget-object v0, LX/8zS;->$VALUES:[LX/8zS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8zS;

    return-object v0
.end method
