.class public final LX/96n;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1439635
    const-class v1, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;

    const v0, -0x1890a3e6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "GraphEditorQuestionsQuery"

    const-string v6, "a6a99171c84a3adceb54d41d4fc0c632"

    const-string v7, "mobile_graph_editor_cards"

    const-string v8, "10155215939356729"

    const-string v9, "10155259088976729"

    .line 1439636
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1439637
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1439638
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1439639
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1439640
    sparse-switch v0, :sswitch_data_0

    .line 1439641
    :goto_0
    return-object p1

    .line 1439642
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1439643
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1439644
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1439645
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1439646
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xa0d333f -> :sswitch_0
        0x101fb19 -> :sswitch_4
        0x294a4578 -> :sswitch_3
        0x3345d4af -> :sswitch_2
        0x3a4da320 -> :sswitch_1
    .end sparse-switch
.end method
