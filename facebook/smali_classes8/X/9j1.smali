.class public final LX/9j1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field public final synthetic a:LX/9j2;


# direct methods
.method public constructor <init>(LX/9j2;)V
    .locals 0

    .prologue
    .line 1529018
    iput-object p1, p0, LX/9j1;->a:LX/9j2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1529017
    const/4 v0, 0x0

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 1529016
    const/4 v0, 0x0

    return v0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 1529013
    iget-object v0, p0, LX/9j1;->a:LX/9j2;

    iget-boolean v0, v0, LX/9j2;->e:Z

    if-eqz v0, :cond_0

    .line 1529014
    iget-object v0, p0, LX/9j1;->a:LX/9j2;

    iget-object v0, v0, LX/9j2;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(Landroid/view/MotionEvent;)V

    .line 1529015
    :cond_0
    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 1528980
    const/4 v0, 0x0

    return v0
.end method

.method public final onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 1529012
    return-void
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1528981
    iget-object v0, p0, LX/9j1;->a:LX/9j2;

    iget-boolean v0, v0, LX/9j2;->d:Z

    if-eqz v0, :cond_1

    .line 1528982
    iget-object v0, p0, LX/9j1;->a:LX/9j2;

    iget-object v0, v0, LX/9j2;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(Landroid/view/MotionEvent;)V

    .line 1528983
    iget-object v0, p0, LX/9j1;->a:LX/9j2;

    iget-object v0, v0, LX/9j2;->c:LX/9iy;

    .line 1528984
    iget-object v1, v0, LX/9iy;->a:LX/9ij;

    invoke-virtual {v1}, LX/9ij;->bringToFront()V

    .line 1528985
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    invoke-virtual {v1}, LX/9j0;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1528986
    iget-object v1, v0, LX/9iy;->a:LX/9ij;

    invoke-virtual {v1}, LX/9ij;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1528987
    iget-object v1, v0, LX/9iy;->a:LX/9ij;

    .line 1528988
    iget-boolean v2, v1, LX/9ij;->n:Z

    move v1, v2

    .line 1528989
    if-eqz v1, :cond_3

    .line 1528990
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, v0, LX/9iy;->a:LX/9ij;

    invoke-virtual {v2}, LX/9ij;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object p0, v0, LX/9iy;->c:LX/9j0;

    iget p0, p0, LX/9j0;->b:F

    sub-float/2addr v2, p0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 1528991
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    iget-object v2, v0, LX/9iy;->b:Lcom/facebook/photos/base/tagging/Tag;

    invoke-virtual {v1, v2}, LX/9j0;->a(Lcom/facebook/photos/base/tagging/Tag;)V

    .line 1528992
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    iget-object v1, v1, LX/9j0;->g:LX/9il;

    iget-object v2, v0, LX/9iy;->b:Lcom/facebook/photos/base/tagging/Tag;

    invoke-virtual {v1, v2}, LX/9il;->b(Lcom/facebook/photos/base/tagging/Tag;)V

    .line 1528993
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 1528994
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1528995
    :cond_2
    iget-object v1, v0, LX/9iy;->a:LX/9ij;

    invoke-virtual {v1}, LX/9ij;->e()V

    .line 1528996
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    const/4 v2, 0x0

    .line 1528997
    iput-object v2, v1, LX/9j0;->f:LX/9ij;

    .line 1528998
    goto :goto_0

    .line 1528999
    :cond_3
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    invoke-virtual {v1}, LX/9j0;->a()V

    .line 1529000
    iget-object v1, v0, LX/9iy;->a:LX/9ij;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/9ij;->a(Z)V

    .line 1529001
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    iget-object v2, v0, LX/9iy;->a:LX/9ij;

    .line 1529002
    iput-object v2, v1, LX/9j0;->f:LX/9ij;

    .line 1529003
    goto :goto_0

    .line 1529004
    :cond_4
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    iget-object v1, v1, LX/9j0;->g:LX/9il;

    if-eqz v1, :cond_0

    .line 1529005
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    invoke-virtual {v1}, LX/9j0;->a()V

    .line 1529006
    iget-object v1, v0, LX/9iy;->c:LX/9j0;

    iget-object v1, v1, LX/9j0;->g:LX/9il;

    iget-object v2, v0, LX/9iy;->b:Lcom/facebook/photos/base/tagging/Tag;

    .line 1529007
    iget-object p0, v1, LX/9il;->a:LX/9ip;

    iget-object p0, p0, LX/9ip;->h:LX/BIm;

    if-eqz p0, :cond_5

    .line 1529008
    iget-object p0, v1, LX/9il;->a:LX/9ip;

    iget-object p0, p0, LX/9ip;->t:LX/8GZ;

    invoke-virtual {p0, v2}, LX/8GZ;->a(LX/362;)LX/362;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1529009
    if-eqz p0, :cond_6

    const/4 p0, 0x1

    :goto_2
    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 1529010
    :cond_5
    goto :goto_0

    .line 1529011
    :cond_6
    const/4 p0, 0x0

    goto :goto_2
.end method
