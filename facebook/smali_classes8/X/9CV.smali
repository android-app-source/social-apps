.class public final LX/9CV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:LX/9CW;

.field public final synthetic c:J

.field public final synthetic d:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Landroid/app/ProgressDialog;LX/9CW;J)V
    .locals 0

    .prologue
    .line 1454147
    iput-object p1, p0, LX/9CV;->d:LX/9Cd;

    iput-object p2, p0, LX/9CV;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, LX/9CV;->b:LX/9CW;

    iput-wide p4, p0, LX/9CV;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454148
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 1454149
    iget-object v0, p0, LX/9CV;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->d:LX/3iL;

    invoke-virtual {v0, p2}, LX/3iL;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1454150
    iget-object v0, p0, LX/9CV;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1454151
    iget-object v0, p0, LX/9CV;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->e:LX/0hx;

    iget-object v1, p0, LX/9CV;->d:LX/9Cd;

    iget-object v1, v1, LX/9Cd;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/9CV;->c:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 1454152
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1454153
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1454154
    iget-object v0, p0, LX/9CV;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1454155
    iget-object v0, p0, LX/9CV;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->b:LX/1K9;

    iget-object v1, p0, LX/9CV;->b:LX/9CW;

    invoke-interface {v1, p1}, LX/9CW;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/8pw;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1454156
    iget-object v0, p0, LX/9CV;->d:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->e:LX/0hx;

    iget-object v1, p0, LX/9CV;->d:LX/9Cd;

    iget-object v1, v1, LX/9Cd;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/9CV;->c:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 1454157
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454158
    return-void
.end method
