.class public final LX/9IZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9FA;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:LX/9Ga;

.field public final synthetic d:LX/9Gn;


# direct methods
.method public constructor <init>(LX/9FA;Lcom/facebook/graphql/model/GraphQLFeedback;LX/9Ga;LX/9Gn;)V
    .locals 0

    .prologue
    .line 1463298
    iput-object p1, p0, LX/9IZ;->a:LX/9FA;

    iput-object p2, p0, LX/9IZ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/9IZ;->c:LX/9Ga;

    iput-object p4, p0, LX/9IZ;->d:LX/9Gn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1463299
    iget-object v0, p0, LX/9IZ;->a:LX/9FA;

    new-instance v1, LX/9GZ;

    iget-object v2, p0, LX/9IZ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/9GZ;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9IZ;->c:LX/9Ga;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1463300
    iget-object v0, p0, LX/9IZ;->d:LX/9Gn;

    const/4 v1, 0x0

    .line 1463301
    iput-boolean v1, v0, LX/9Gn;->a:Z

    .line 1463302
    iget-object v0, p0, LX/9IZ;->a:LX/9FA;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1463303
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1463304
    invoke-direct {p0}, LX/9IZ;->b()V

    .line 1463305
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1463306
    invoke-direct {p0}, LX/9IZ;->b()V

    .line 1463307
    return-void
.end method
