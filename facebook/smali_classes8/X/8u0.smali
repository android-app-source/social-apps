.class public LX/8u0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/xml/sax/ContentHandler;


# instance fields
.field public a:Lorg/xml/sax/ContentHandler;

.field public b:Landroid/text/Editable;

.field public c:LX/8tv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1414378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final characters([CII)V
    .locals 2

    .prologue
    .line 1414369
    iget-object v0, p0, LX/8u0;->c:LX/8tv;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8u0;->c:LX/8tv;

    iget-object v1, p0, LX/8u0;->b:Landroid/text/Editable;

    invoke-virtual {v0, p1, p2, p3, v1}, LX/8tv;->a([CIILandroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414370
    :goto_0
    return-void

    .line 1414371
    :cond_0
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2, p3}, Lorg/xml/sax/ContentHandler;->characters([CII)V

    goto :goto_0
.end method

.method public final endDocument()V
    .locals 1

    .prologue
    .line 1414372
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->endDocument()V

    .line 1414373
    return-void
.end method

.method public final endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1414374
    const-string v0, "fbhtml"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1414375
    :cond_0
    :goto_0
    return-void

    .line 1414376
    :cond_1
    iget-object v0, p0, LX/8u0;->c:LX/8tv;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8u0;->c:LX/8tv;

    iget-object v1, p0, LX/8u0;->b:Landroid/text/Editable;

    invoke-virtual {v0, p2, v1}, LX/8tv;->a(Ljava/lang/String;Landroid/text/Editable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1414377
    :cond_2
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2, p3}, Lorg/xml/sax/ContentHandler;->endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final endPrefixMapping(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1414365
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1}, Lorg/xml/sax/ContentHandler;->endPrefixMapping(Ljava/lang/String;)V

    .line 1414366
    return-void
.end method

.method public final ignorableWhitespace([CII)V
    .locals 1

    .prologue
    .line 1414367
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2, p3}, Lorg/xml/sax/ContentHandler;->ignorableWhitespace([CII)V

    .line 1414368
    return-void
.end method

.method public final processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1414363
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2}, Lorg/xml/sax/ContentHandler;->processingInstruction(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414364
    return-void
.end method

.method public final setDocumentLocator(Lorg/xml/sax/Locator;)V
    .locals 1

    .prologue
    .line 1414361
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1}, Lorg/xml/sax/ContentHandler;->setDocumentLocator(Lorg/xml/sax/Locator;)V

    .line 1414362
    return-void
.end method

.method public final skippedEntity(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1414359
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1}, Lorg/xml/sax/ContentHandler;->skippedEntity(Ljava/lang/String;)V

    .line 1414360
    return-void
.end method

.method public final startDocument()V
    .locals 1

    .prologue
    .line 1414351
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->startDocument()V

    .line 1414352
    return-void
.end method

.method public final startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2

    .prologue
    .line 1414355
    const-string v0, "fbhtml"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1414356
    :cond_0
    :goto_0
    return-void

    .line 1414357
    :cond_1
    iget-object v0, p0, LX/8u0;->c:LX/8tv;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8u0;->c:LX/8tv;

    iget-object v1, p0, LX/8u0;->b:Landroid/text/Editable;

    invoke-virtual {v0, p2, p4, v1}, LX/8tv;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Landroid/text/Editable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1414358
    :cond_2
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/xml/sax/ContentHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0
.end method

.method public final startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1414353
    iget-object v0, p0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2}, Lorg/xml/sax/ContentHandler;->startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414354
    return-void
.end method
