.class public LX/8wn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1422950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422951
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1422952
    check-cast p1, Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;

    .line 1422953
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1422954
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "reportable_ent_token"

    iget-object v3, p1, Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422955
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "story_location"

    iget-object v3, p1, Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422956
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "action"

    iget-object v3, p1, Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;->a:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422957
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "undo"

    iget-object v3, p1, Lcom/facebook/api/reportable_entity/NegativeFeedbackActionOnReportableEntityMethod$Params;->d:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422958
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422959
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "negativeFeedbackActionOnReportableEntity"

    .line 1422960
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1422961
    move-object v1, v1

    .line 1422962
    const-string v2, "POST"

    .line 1422963
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1422964
    move-object v1, v1

    .line 1422965
    const-string v2, "reportable_ent_nfx_actions"

    .line 1422966
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1422967
    move-object v1, v1

    .line 1422968
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1422969
    move-object v0, v1

    .line 1422970
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1422971
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1422972
    move-object v0, v0

    .line 1422973
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1422974
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1422975
    const/4 v0, 0x0

    return-object v0
.end method
