.class public LX/9Uz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static volatile c:LX/9Uz;


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1498825
    const-class v0, LX/9Uz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9Uz;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1498826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498827
    invoke-static {}, LX/9Uz;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/9Uz;->a:Z

    .line 1498828
    return-void
.end method

.method public static a(LX/0QB;)LX/9Uz;
    .locals 3

    .prologue
    .line 1498829
    sget-object v0, LX/9Uz;->c:LX/9Uz;

    if-nez v0, :cond_1

    .line 1498830
    const-class v1, LX/9Uz;

    monitor-enter v1

    .line 1498831
    :try_start_0
    sget-object v0, LX/9Uz;->c:LX/9Uz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1498832
    if-eqz v2, :cond_0

    .line 1498833
    :try_start_1
    new-instance v0, LX/9Uz;

    invoke-direct {v0}, LX/9Uz;-><init>()V

    .line 1498834
    move-object v0, v0

    .line 1498835
    sput-object v0, LX/9Uz;->c:LX/9Uz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1498836
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1498837
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1498838
    :cond_1
    sget-object v0, LX/9Uz;->c:LX/9Uz;

    return-object v0

    .line 1498839
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1498840
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1498841
    const/4 v3, 0x0

    .line 1498842
    :try_start_0
    new-instance v2, LX/9VA;

    invoke-direct {v2}, LX/9VA;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1498843
    :try_start_1
    iget-object v1, v2, LX/9VA;->g:Landroid/opengl/EGLContext;

    if-eqz v1, :cond_3

    iget-object v1, v2, LX/9VA;->g:Landroid/opengl/EGLContext;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-eq v1, v3, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1498844
    if-nez v1, :cond_1

    .line 1498845
    sget-object v1, LX/9Uz;->b:Ljava/lang/String;

    const-string v3, "Could not create GLContext"

    invoke-static {v1, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1498846
    invoke-virtual {v2}, LX/9VA;->d()V

    .line 1498847
    :cond_0
    :goto_1
    return v0

    .line 1498848
    :cond_1
    invoke-virtual {v2}, LX/9VA;->d()V

    .line 1498849
    const/4 v0, 0x1

    goto :goto_1

    .line 1498850
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 1498851
    :goto_2
    :try_start_2
    sget-object v3, LX/9Uz;->b:Ljava/lang/String;

    const-string v4, "LivePhotosConfig.isSupported"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1498852
    if-eqz v2, :cond_0

    .line 1498853
    invoke-virtual {v2}, LX/9VA;->d()V

    goto :goto_1

    .line 1498854
    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_2

    .line 1498855
    invoke-virtual {v2}, LX/9VA;->d()V

    :cond_2
    throw v0

    .line 1498856
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1498857
    :catch_1
    move-exception v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
