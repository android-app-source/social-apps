.class public final LX/9mK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V
    .locals 0

    .prologue
    .line 1535393
    iput-object p1, p0, LX/9mK;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1535394
    iget-object v0, p0, LX/9mK;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    iget-object v0, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->m:LX/9lb;

    iget-object v1, p0, LX/9mK;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1535395
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v2, v2

    .line 1535396
    sget-object v3, LX/9lX;->a:[I

    .line 1535397
    iget-object v4, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    move-object v4, v4

    .line 1535398
    invoke-virtual {v4}, LX/9mN;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1535399
    :goto_0
    return-void

    .line 1535400
    :pswitch_0
    iget-object v3, v0, LX/9lb;->g:LX/9lc;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v4

    .line 1535401
    iget-object p0, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    move-object p0, p0

    .line 1535402
    iget-object p1, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    move-object v2, p1

    .line 1535403
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "fb4a_rapid_reporting_tapped_done"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535404
    const-string p2, "selected_tags"

    invoke-virtual {p1, p2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1535405
    const-string p2, "reason"

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1535406
    invoke-static {v3, p1, v4}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535407
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->c(Z)V

    .line 1535408
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v2, v2

    .line 1535409
    new-instance v3, LX/9li;

    invoke-direct {v3}, LX/9li;-><init>()V

    move-object v3, v3

    .line 1535410
    new-instance v4, LX/4Jb;

    invoke-direct {v4}, LX/4Jb;-><init>()V

    iget-object p0, v0, LX/9lb;->e:LX/0SI;

    invoke-interface {p0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p0

    .line 1535411
    iget-object p1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object p0, p1

    .line 1535412
    const-string p1, "actor_id"

    invoke-virtual {v4, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1535413
    move-object v4, v4

    .line 1535414
    invoke-virtual {v2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->l()Ljava/lang/String;

    move-result-object p0

    .line 1535415
    const-string p1, "rapid_reporting_prompt_node_token"

    invoke-virtual {v4, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1535416
    move-object v4, v4

    .line 1535417
    iget-object p0, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    move-object p0, p0

    .line 1535418
    const-string p1, "report_tags"

    invoke-virtual {v4, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1535419
    move-object v4, v4

    .line 1535420
    iget-object p0, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    move-object p0, p0

    .line 1535421
    const-string p1, "raw_text"

    invoke-virtual {v4, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1535422
    move-object v4, v4

    .line 1535423
    const-string p0, "input"

    invoke-virtual {v3, p0, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1535424
    iget-object v4, v0, LX/9lb;->b:LX/1Ck;

    sget-object p0, LX/9la;->METADATA_REPORT:LX/9la;

    iget-object p1, v0, LX/9lb;->f:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance p1, LX/9lS;

    invoke-direct {p1, v0, v2, v1}, LX/9lS;-><init>(LX/9lb;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    invoke-static {p1}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v2

    invoke-virtual {v4, p0, v3, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1535425
    goto :goto_0

    .line 1535426
    :pswitch_1
    iget-object v3, v0, LX/9lb;->g:LX/9lc;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v2

    .line 1535427
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "fb4a_rapid_reporting_confirmed_cancel"

    invoke-direct {v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535428
    invoke-static {v3, v4, v2}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535429
    invoke-static {v0, v1}, LX/9lb;->g(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    goto/16 :goto_0

    .line 1535430
    :pswitch_2
    iget-object v3, v0, LX/9lb;->g:LX/9lc;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v2

    .line 1535431
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "fb4a_rapid_reporting_tapped_go_to_video"

    invoke-direct {v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535432
    invoke-static {v3, v4, v2}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535433
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/9lb;->a(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Z)V

    .line 1535434
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
