.class public final LX/ANL;
.super LX/6Il;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 0

    .prologue
    .line 1667607
    iput-object p1, p0, LX/ANL;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {p0}, LX/6Il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1667608
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->b()V

    .line 1667609
    iget-object v0, p0, LX/ANL;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0, p1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/lang/Throwable;)V

    .line 1667610
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1667611
    iget-object v0, p0, LX/ANL;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    .line 1667612
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1667613
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v2, "cameracore_start_preview"

    const-string p0, "Fragment is no longer added"

    invoke-virtual {v1, v2, p0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667614
    :cond_0
    :goto_0
    return-void

    .line 1667615
    :cond_1
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Q:LX/ANf;

    sget-object v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    .line 1667616
    iput-object v2, v1, LX/ANf;->l:LX/6Ia;

    .line 1667617
    const/4 v1, 0x0

    move v1, v1

    .line 1667618
    if-nez v1, :cond_4

    .line 1667619
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667620
    :goto_1
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbImageButton;->setEnabled(Z)V

    .line 1667621
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    sget-object v2, LX/ANZ;->NONE:LX/ANZ;

    if-ne v1, v2, :cond_2

    .line 1667622
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667623
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    move-object v1, v2

    .line 1667624
    sget-object v2, Lcom/facebook/cameracore/ui/CaptureType;->PHOTO:Lcom/facebook/cameracore/ui/CaptureType;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/ANZ;->PHOTO:LX/ANZ;

    :goto_2
    iput-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1667625
    :cond_2
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    iget-object v2, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    invoke-static {v0, v1, v2}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/ANZ;LX/ANZ;)V

    .line 1667626
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Q:LX/ANf;

    iget-object v2, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    invoke-virtual {v1, v2}, LX/ANf;->a(LX/ANZ;)V

    .line 1667627
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 1667628
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->c:LX/0Px;

    move-object v1, v2

    .line 1667629
    sget-object v2, Lcom/facebook/cameracore/ui/CaptureType;->VIDEO:Lcom/facebook/cameracore/ui/CaptureType;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1667630
    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    goto :goto_0

    .line 1667631
    :cond_3
    sget-object v1, LX/ANZ;->VIDEO:LX/ANZ;

    goto :goto_2

    .line 1667632
    :cond_4
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667633
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Q:LX/ANf;

    invoke-virtual {v1}, LX/ANf;->d()V

    .line 1667634
    iget-object v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v2, LX/ANI;

    invoke-direct {v2, v0}, LX/ANI;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
