.class public final LX/A1B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1609908
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1609909
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1609910
    :goto_0
    return v1

    .line 1609911
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1609912
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1609913
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1609914
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1609915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1609916
    const-string v5, "logging_unit_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1609917
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1609918
    :cond_2
    const-string v5, "node"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1609919
    invoke-static {p0, p1}, LX/A1A;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1609920
    :cond_3
    const-string v5, "result_decoration"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1609921
    const/4 v4, 0x0

    .line 1609922
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_e

    .line 1609923
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1609924
    :goto_2
    move v0, v4

    .line 1609925
    goto :goto_1

    .line 1609926
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1609927
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1609928
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1609929
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1609930
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1609931
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1609932
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_d

    .line 1609933
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1609934
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1609935
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_7

    if-eqz v10, :cond_7

    .line 1609936
    const-string v11, "info_snippets"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1609937
    invoke-static {p0, p1}, LX/8g5;->b(LX/15w;LX/186;)I

    move-result v9

    goto :goto_3

    .line 1609938
    :cond_8
    const-string v11, "lineage_snippets"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1609939
    invoke-static {p0, p1}, LX/8g5;->b(LX/15w;LX/186;)I

    move-result v8

    goto :goto_3

    .line 1609940
    :cond_9
    const-string v11, "ordered_snippets"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1609941
    invoke-static {p0, p1}, LX/8g5;->b(LX/15w;LX/186;)I

    move-result v7

    goto :goto_3

    .line 1609942
    :cond_a
    const-string v11, "snippet_source"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1609943
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1609944
    :cond_b
    const-string v11, "social_snippet"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1609945
    invoke-static {p0, p1}, LX/8g5;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_3

    .line 1609946
    :cond_c
    const-string v11, "summary_snippet"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1609947
    invoke-static {p0, p1}, LX/8g5;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_3

    .line 1609948
    :cond_d
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1609949
    invoke-virtual {p1, v4, v9}, LX/186;->b(II)V

    .line 1609950
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v8}, LX/186;->b(II)V

    .line 1609951
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v7}, LX/186;->b(II)V

    .line 1609952
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v6}, LX/186;->b(II)V

    .line 1609953
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1609954
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1609955
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_e
    move v0, v4

    move v5, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    goto/16 :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1609956
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1609957
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1609958
    if-eqz v0, :cond_0

    .line 1609959
    const-string v1, "logging_unit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609960
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609961
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609962
    if-eqz v0, :cond_1

    .line 1609963
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609964
    invoke-static {p0, v0, p2, p3}, LX/A1A;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609965
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609966
    if-eqz v0, :cond_8

    .line 1609967
    const-string v1, "result_decoration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609968
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1609969
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1609970
    if-eqz v1, :cond_2

    .line 1609971
    const-string p1, "info_snippets"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609972
    invoke-static {p0, v1, p2, p3}, LX/8g5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609973
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1609974
    if-eqz v1, :cond_3

    .line 1609975
    const-string p1, "lineage_snippets"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609976
    invoke-static {p0, v1, p2, p3}, LX/8g5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609977
    :cond_3
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1609978
    if-eqz v1, :cond_4

    .line 1609979
    const-string p1, "ordered_snippets"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609980
    invoke-static {p0, v1, p2, p3}, LX/8g5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609981
    :cond_4
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1609982
    if-eqz v1, :cond_5

    .line 1609983
    const-string p1, "snippet_source"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609984
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609985
    :cond_5
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1609986
    if-eqz v1, :cond_6

    .line 1609987
    const-string p1, "social_snippet"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609988
    invoke-static {p0, v1, p2, p3}, LX/8g5;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609989
    :cond_6
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1609990
    if-eqz v1, :cond_7

    .line 1609991
    const-string p1, "summary_snippet"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609992
    invoke-static {p0, v1, p2, p3}, LX/8g5;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609993
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1609994
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1609995
    return-void
.end method
