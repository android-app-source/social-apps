.class public final LX/8wH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/8wH;


# instance fields
.field public final b:Z

.field public final c:LX/8wD;

.field public final d:LX/8vz;

.field public final e:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1422182
    const/high16 v0, -0x80000000

    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->a(I)LX/8wH;

    move-result-object v0

    sput-object v0, LX/8wH;->a:LX/8wH;

    return-void
.end method

.method private constructor <init>(ZIILX/8vz;F)V
    .locals 2

    .prologue
    .line 1422180
    new-instance v0, LX/8wD;

    add-int v1, p2, p3

    invoke-direct {v0, p2, v1}, LX/8wD;-><init>(II)V

    invoke-direct {p0, p1, v0, p4, p5}, LX/8wH;-><init>(ZLX/8wD;LX/8vz;F)V

    .line 1422181
    return-void
.end method

.method public synthetic constructor <init>(ZIILX/8vz;FB)V
    .locals 0

    .prologue
    .line 1422179
    invoke-direct/range {p0 .. p5}, LX/8wH;-><init>(ZIILX/8vz;F)V

    return-void
.end method

.method private constructor <init>(ZLX/8wD;LX/8vz;F)V
    .locals 0

    .prologue
    .line 1422173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422174
    iput-boolean p1, p0, LX/8wH;->b:Z

    .line 1422175
    iput-object p2, p0, LX/8wH;->c:LX/8wD;

    .line 1422176
    iput-object p3, p0, LX/8wH;->d:LX/8vz;

    .line 1422177
    iput p4, p0, LX/8wH;->e:F

    .line 1422178
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1422172
    iget-object v0, p0, LX/8wH;->d:LX/8vz;

    sget-object v1, Landroid/support/v7/widget/GridLayout;->k:LX/8vz;

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/8wH;->e:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final a(LX/8wD;)LX/8wH;
    .locals 4

    .prologue
    .line 1422159
    new-instance v0, LX/8wH;

    iget-boolean v1, p0, LX/8wH;->b:Z

    iget-object v2, p0, LX/8wH;->d:LX/8vz;

    iget v3, p0, LX/8wH;->e:F

    invoke-direct {v0, v1, p1, v2, v3}, LX/8wH;-><init>(ZLX/8wD;LX/8vz;F)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1422163
    if-ne p0, p1, :cond_1

    .line 1422164
    :cond_0
    :goto_0
    return v0

    .line 1422165
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1422166
    goto :goto_0

    .line 1422167
    :cond_3
    check-cast p1, LX/8wH;

    .line 1422168
    iget-object v2, p0, LX/8wH;->d:LX/8vz;

    iget-object v3, p1, LX/8wH;->d:LX/8vz;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1422169
    goto :goto_0

    .line 1422170
    :cond_4
    iget-object v2, p0, LX/8wH;->c:LX/8wD;

    iget-object v3, p1, LX/8wH;->c:LX/8wD;

    invoke-virtual {v2, v3}, LX/8wD;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1422171
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1422160
    iget-object v0, p0, LX/8wH;->c:LX/8wD;

    invoke-virtual {v0}, LX/8wD;->hashCode()I

    move-result v0

    .line 1422161
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/8wH;->d:LX/8vz;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1422162
    return v0
.end method
