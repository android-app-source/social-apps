.class public LX/8yQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/analytics/logger/HoneyClientEvent;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1425464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425465
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1425466
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1425467
    move-object v0, v0

    .line 1425468
    iput-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425469
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1425478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425479
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1425480
    iput-object p3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1425481
    move-object v0, v0

    .line 1425482
    iput-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425483
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "composer_session_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425484
    return-void
.end method


# virtual methods
.method public final a(I)LX/8yQ;
    .locals 2

    .prologue
    .line 1425476
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "number_of_results"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425477
    return-object p0
.end method

.method public final a(LX/0Px;)LX/8yQ;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/8yQ;"
        }
    .end annotation

    .prologue
    .line 1425470
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v2

    .line 1425471
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1425472
    invoke-static {v0}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 1425473
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1425474
    :cond_0
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "results"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425475
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1425462
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "action_type_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425463
    return-object p0
.end method

.method public final b(I)LX/8yQ;
    .locals 2

    .prologue
    .line 1425460
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "keystroke_count"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425461
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1425485
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "object_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425486
    return-object p0
.end method

.method public final c(I)LX/8yQ;
    .locals 2

    .prologue
    .line 1425458
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "display_position"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425459
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1425456
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "search_term"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425457
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1425448
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "typeahead_session_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425449
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1425454
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "request_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425455
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1425452
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "object_tracking_data"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425453
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1425450
    iget-object v0, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "typeahead_source"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1425451
    return-object p0
.end method
