.class public final enum LX/9Bg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Bg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Bg;

.field public static final enum HIDDEN:LX/9Bg;

.field public static final enum HIDING:LX/9Bg;

.field public static final enum REVEALING:LX/9Bg;

.field public static final enum SHOWN:LX/9Bg;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1452359
    new-instance v0, LX/9Bg;

    const-string v1, "REVEALING"

    invoke-direct {v0, v1, v2}, LX/9Bg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Bg;->REVEALING:LX/9Bg;

    .line 1452360
    new-instance v0, LX/9Bg;

    const-string v1, "HIDING"

    invoke-direct {v0, v1, v3}, LX/9Bg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Bg;->HIDING:LX/9Bg;

    .line 1452361
    new-instance v0, LX/9Bg;

    const-string v1, "SHOWN"

    invoke-direct {v0, v1, v4}, LX/9Bg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Bg;->SHOWN:LX/9Bg;

    .line 1452362
    new-instance v0, LX/9Bg;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v5}, LX/9Bg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Bg;->HIDDEN:LX/9Bg;

    .line 1452363
    const/4 v0, 0x4

    new-array v0, v0, [LX/9Bg;

    sget-object v1, LX/9Bg;->REVEALING:LX/9Bg;

    aput-object v1, v0, v2

    sget-object v1, LX/9Bg;->HIDING:LX/9Bg;

    aput-object v1, v0, v3

    sget-object v1, LX/9Bg;->SHOWN:LX/9Bg;

    aput-object v1, v0, v4

    sget-object v1, LX/9Bg;->HIDDEN:LX/9Bg;

    aput-object v1, v0, v5

    sput-object v0, LX/9Bg;->$VALUES:[LX/9Bg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1452364
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Bg;
    .locals 1

    .prologue
    .line 1452365
    const-class v0, LX/9Bg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Bg;

    return-object v0
.end method

.method public static values()[LX/9Bg;
    .locals 1

    .prologue
    .line 1452366
    sget-object v0, LX/9Bg;->$VALUES:[LX/9Bg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Bg;

    return-object v0
.end method
