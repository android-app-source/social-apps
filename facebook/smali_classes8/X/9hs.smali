.class public LX/9hs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9hs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527054
    return-void
.end method

.method public static a(LX/0QB;)LX/9hs;
    .locals 3

    .prologue
    .line 1527055
    sget-object v0, LX/9hs;->a:LX/9hs;

    if-nez v0, :cond_1

    .line 1527056
    const-class v1, LX/9hs;

    monitor-enter v1

    .line 1527057
    :try_start_0
    sget-object v0, LX/9hs;->a:LX/9hs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1527058
    if-eqz v2, :cond_0

    .line 1527059
    :try_start_1
    new-instance v0, LX/9hs;

    invoke-direct {v0}, LX/9hs;-><init>()V

    .line 1527060
    move-object v0, v0

    .line 1527061
    sput-object v0, LX/9hs;->a:LX/9hs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1527062
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1527063
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1527064
    :cond_1
    sget-object v0, LX/9hs;->a:LX/9hs;

    return-object v0

    .line 1527065
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1527066
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/9hk;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1527067
    new-instance v0, LX/9hk;

    new-instance v1, LX/9hn;

    invoke-direct {v1, p0, p1}, LX/9hn;-><init>(LX/9hs;Ljava/lang/String;)V

    invoke-direct {v0, p1, v1}, LX/9hk;-><init>(Ljava/lang/String;LX/9hl;)V

    return-object v0
.end method
