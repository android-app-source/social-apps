.class public final enum LX/9Wb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Wb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Wb;

.field public static final enum FB4A_BAN_USER_ACTIONS:LX/9Wb;

.field public static final enum PMA_BAN_USER_ACTIONS:LX/9Wb;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1501396
    new-instance v0, LX/9Wb;

    const-string v1, "FB4A_BAN_USER_ACTIONS"

    const-string v2, "android_pages_ban_actions"

    invoke-direct {v0, v1, v3, v2}, LX/9Wb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Wb;->FB4A_BAN_USER_ACTIONS:LX/9Wb;

    .line 1501397
    new-instance v0, LX/9Wb;

    const-string v1, "PMA_BAN_USER_ACTIONS"

    const-string v2, "android_pma_ban_actions"

    invoke-direct {v0, v1, v4, v2}, LX/9Wb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Wb;->PMA_BAN_USER_ACTIONS:LX/9Wb;

    .line 1501398
    const/4 v0, 0x2

    new-array v0, v0, [LX/9Wb;

    sget-object v1, LX/9Wb;->FB4A_BAN_USER_ACTIONS:LX/9Wb;

    aput-object v1, v0, v3

    sget-object v1, LX/9Wb;->PMA_BAN_USER_ACTIONS:LX/9Wb;

    aput-object v1, v0, v4

    sput-object v0, LX/9Wb;->$VALUES:[LX/9Wb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1501399
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1501400
    iput-object p3, p0, LX/9Wb;->mEventName:Ljava/lang/String;

    .line 1501401
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Wb;
    .locals 1

    .prologue
    .line 1501402
    const-class v0, LX/9Wb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Wb;

    return-object v0
.end method

.method public static values()[LX/9Wb;
    .locals 1

    .prologue
    .line 1501403
    sget-object v0, LX/9Wb;->$VALUES:[LX/9Wb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Wb;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1501404
    iget-object v0, p0, LX/9Wb;->mEventName:Ljava/lang/String;

    return-object v0
.end method
