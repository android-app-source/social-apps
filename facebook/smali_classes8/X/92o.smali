.class public LX/92o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/String;

.field private static final t:Ljava/lang/Object;


# instance fields
.field public a:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field public final d:LX/92N;

.field public final e:LX/92K;

.field public final f:LX/92w;

.field public final g:Ljava/util/concurrent/Executor;

.field public final h:LX/0TD;

.field public final i:LX/92u;

.field public final j:LX/92c;

.field private final k:LX/92Q;

.field public l:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/92m;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/92E;

.field public final n:LX/0Zb;

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:I

.field public r:LX/92n;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1432814
    const-string v0, "32"

    sput-object v0, LX/92o;->c:Ljava/lang/String;

    .line 1432815
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/92o;->t:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/92N;LX/92K;LX/92w;LX/92u;Ljava/util/concurrent/Executor;LX/0Zb;LX/0TD;LX/92c;LX/92Q;LX/1Ck;LX/92E;)V
    .locals 2
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p7    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1432728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432729
    const-string v0, "383634835006146"

    iput-object v0, p0, LX/92o;->b:Ljava/lang/String;

    .line 1432730
    iput-object p1, p0, LX/92o;->d:LX/92N;

    .line 1432731
    iput-object p2, p0, LX/92o;->e:LX/92K;

    .line 1432732
    iput-object p3, p0, LX/92o;->f:LX/92w;

    .line 1432733
    iput-object p4, p0, LX/92o;->i:LX/92u;

    .line 1432734
    iput-object p5, p0, LX/92o;->g:Ljava/util/concurrent/Executor;

    .line 1432735
    iput-object p7, p0, LX/92o;->h:LX/0TD;

    .line 1432736
    iput-object p8, p0, LX/92o;->j:LX/92c;

    .line 1432737
    iput-object p9, p0, LX/92o;->k:LX/92Q;

    .line 1432738
    iput-object p10, p0, LX/92o;->l:LX/1Ck;

    .line 1432739
    iput-object p11, p0, LX/92o;->m:LX/92E;

    .line 1432740
    iput-object p6, p0, LX/92o;->n:LX/0Zb;

    .line 1432741
    iput-boolean v1, p0, LX/92o;->s:Z

    .line 1432742
    iput-boolean v1, p0, LX/92o;->a:Z

    .line 1432743
    return-void
.end method

.method public static a(LX/0QB;)LX/92o;
    .locals 7

    .prologue
    .line 1432787
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1432788
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1432789
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1432790
    if-nez v1, :cond_0

    .line 1432791
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1432792
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1432793
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1432794
    sget-object v1, LX/92o;->t:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1432795
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1432796
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1432797
    :cond_1
    if-nez v1, :cond_4

    .line 1432798
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1432799
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1432800
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/92o;->b(LX/0QB;)LX/92o;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 1432801
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1432802
    if-nez v1, :cond_2

    .line 1432803
    sget-object v0, LX/92o;->t:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92o;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1432804
    :goto_1
    if-eqz v0, :cond_3

    .line 1432805
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1432806
    :goto_3
    check-cast v0, LX/92o;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1432807
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1432808
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1432809
    :catchall_1
    move-exception v0

    .line 1432810
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1432811
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1432812
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1432813
    :cond_2
    :try_start_8
    sget-object v0, LX/92o;->t:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92o;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static b(LX/92o;Z)LX/0w7;
    .locals 3

    .prologue
    .line 1432782
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 1432783
    new-instance v1, LX/0w7;

    invoke-direct {v1}, LX/0w7;-><init>()V

    const-string v2, "image_scale"

    if-nez v0, :cond_0

    sget-object v0, LX/0wB;->a:LX/0wC;

    :cond_0
    invoke-virtual {v1, v2, v0}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0w7;

    move-result-object v0

    const-string v1, "minutiae_image_size_large"

    sget-object v2, LX/92o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    move-result-object v0

    const-string v1, "dont_load_templates"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    move-result-object v0

    .line 1432784
    if-eqz p1, :cond_1

    .line 1432785
    const-string v1, "surface"

    const-string v2, "composer"

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    move-result-object v0

    const-string v1, "query_type"

    const-string v2, "VERB_ONLY"

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    move-result-object v0

    const-string v1, "place_id"

    iget-object v2, p0, LX/92o;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    move-result-object v0

    const-string v1, "query"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    move-result-object v0

    const-string v1, "session_id"

    iget v2, p0, LX/92o;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;

    move-result-object v0

    const-string v1, "is_prefetch"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0w7;

    move-result-object v0

    .line 1432786
    :cond_1
    return-object v0
.end method

.method public static b(LX/92o;LX/91s;)LX/8zq;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1432781
    new-instance v0, LX/92f;

    invoke-direct {v0, p0, p1}, LX/92f;-><init>(LX/92o;LX/91s;)V

    return-object v0
.end method

.method private static b(LX/0QB;)LX/92o;
    .locals 12

    .prologue
    .line 1432770
    new-instance v0, LX/92o;

    .line 1432771
    new-instance v3, LX/92N;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/92T;->b(LX/0QB;)LX/92T;

    move-result-object v2

    check-cast v2, LX/92T;

    invoke-direct {v3, v1, v2}, LX/92N;-><init>(LX/0tX;LX/92T;)V

    .line 1432772
    move-object v1, v3

    .line 1432773
    check-cast v1, LX/92N;

    .line 1432774
    new-instance v3, LX/92K;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {v3, v2}, LX/92K;-><init>(LX/0tX;)V

    .line 1432775
    move-object v2, v3

    .line 1432776
    check-cast v2, LX/92K;

    invoke-static {p0}, LX/92w;->a(LX/0QB;)LX/92w;

    move-result-object v3

    check-cast v3, LX/92w;

    .line 1432777
    new-instance v6, LX/92u;

    invoke-static {p0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v4

    check-cast v4, LX/0t2;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {v6, v4, v5}, LX/92u;-><init>(LX/0t2;LX/0W3;)V

    .line 1432778
    move-object v4, v6

    .line 1432779
    check-cast v4, LX/92u;

    invoke-static {p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {p0}, LX/92c;->a(LX/0QB;)LX/92c;

    move-result-object v8

    check-cast v8, LX/92c;

    invoke-static {p0}, LX/92Q;->a(LX/0QB;)LX/92Q;

    move-result-object v9

    check-cast v9, LX/92Q;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {p0}, LX/92E;->a(LX/0QB;)LX/92E;

    move-result-object v11

    check-cast v11, LX/92E;

    invoke-direct/range {v0 .. v11}, LX/92o;-><init>(LX/92N;LX/92K;LX/92w;LX/92u;Ljava/util/concurrent/Executor;LX/0Zb;LX/0TD;LX/92c;LX/92Q;LX/1Ck;LX/92E;)V

    .line 1432780
    return-object v0
.end method

.method public static c(LX/92o;LX/8zq;)LX/8zq;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1432769
    new-instance v0, LX/92g;

    invoke-direct {v0, p0, p1}, LX/92g;-><init>(LX/92o;LX/8zq;)V

    return-object v0
.end method

.method public static d(LX/92o;LX/8zq;)V
    .locals 4
    .param p0    # LX/92o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1432816
    iget-boolean v0, p0, LX/92o;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/92o;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/92o;->r:LX/92n;

    if-nez v0, :cond_0

    .line 1432817
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/92o;->a:Z

    .line 1432818
    :cond_0
    iget-object v0, p0, LX/92o;->l:LX/1Ck;

    sget-object v1, LX/92m;->ACTIVITIES:LX/92m;

    .line 1432819
    iget-object v2, p0, LX/92o;->h:LX/0TD;

    new-instance v3, LX/92k;

    invoke-direct {v3, p0}, LX/92k;-><init>(LX/92o;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1432820
    move-object v2, v2

    .line 1432821
    invoke-static {p0, p1}, LX/92o;->f(LX/92o;LX/8zq;)LX/0Vd;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1432822
    return-void
.end method

.method public static e(LX/92o;)I
    .locals 14

    .prologue
    .line 1432758
    iget-object v0, p0, LX/92o;->k:LX/92Q;

    const/16 v1, 0x9

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1432759
    if-ltz v1, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1432760
    iget-object v2, v0, LX/92Q;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/92F;->f:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v2, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 1432761
    iget-object v2, v0, LX/92Q;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-lez v2, :cond_3

    .line 1432762
    iget-object v2, v0, LX/92Q;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/92F;->f:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1432763
    :cond_0
    :goto_1
    move v0, v4

    .line 1432764
    if-eqz v0, :cond_1

    .line 1432765
    const/4 v0, 0x4

    .line 1432766
    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    move v2, v4

    .line 1432767
    goto :goto_0

    .line 1432768
    :cond_3
    iget-object v2, v0, LX/92Q;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    int-to-long v10, v1

    const-wide/32 v12, 0x5265c00

    mul-long/2addr v10, v12

    sub-long/2addr v8, v10

    cmp-long v2, v8, v6

    if-gez v2, :cond_0

    move v4, v3

    goto :goto_1
.end method

.method public static f(LX/92o;LX/8zq;)LX/0Vd;
    .locals 1
    .param p0    # LX/92o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8zq;",
            ")",
            "LX/0Vd",
            "<",
            "LX/92n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1432757
    new-instance v0, LX/92i;

    invoke-direct {v0, p0, p1}, LX/92i;-><init>(LX/92o;LX/8zq;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1432752
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/92o;->a:Z

    .line 1432753
    iget-boolean v0, p0, LX/92o;->s:Z

    if-eqz v0, :cond_0

    .line 1432754
    iget-object v0, p0, LX/92o;->l:LX/1Ck;

    sget-object v2, LX/92m;->ACTIVITIES:LX/92m;

    new-instance v3, LX/92h;

    invoke-direct {v3, p0}, LX/92h;-><init>(LX/92o;)V

    invoke-static {p0, v1}, LX/92o;->f(LX/92o;LX/8zq;)LX/0Vd;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1432755
    :goto_0
    return-void

    .line 1432756
    :cond_0
    invoke-static {p0, v1}, LX/92o;->d(LX/92o;LX/8zq;)V

    goto :goto_0
.end method

.method public final a(LX/8zq;)V
    .locals 3

    .prologue
    .line 1432748
    iget-object v0, p0, LX/92o;->m:LX/92E;

    .line 1432749
    const v1, 0x430004

    const-string v2, "minutiae_verb_picker_fetch_time"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432750
    invoke-static {p0, p1}, LX/92o;->d(LX/92o;LX/8zq;)V

    .line 1432751
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1432745
    iget-object v0, p0, LX/92o;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iget-object v0, p0, LX/92o;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/92o;->o:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1432746
    :cond_1
    :goto_0
    return-void

    .line 1432747
    :cond_2
    iput-object p1, p0, LX/92o;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1432744
    iget v0, p0, LX/92o;->q:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
