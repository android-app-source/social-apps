.class public LX/AMC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:D

.field public final b:D

.field private final c:D

.field private final d:D


# direct methods
.method public constructor <init>(DD)V
    .locals 11

    .prologue
    .line 1665963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1665964
    iput-wide p1, p0, LX/AMC;->c:D

    .line 1665965
    iput-wide p3, p0, LX/AMC;->d:D

    .line 1665966
    const-wide v0, 0x3ffb333333333333L    # 1.7

    div-double v0, p3, v0

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    invoke-static/range {v0 .. v5}, LX/AMC;->a(DDD)D

    move-result-wide v0

    .line 1665967
    const-wide/16 v2, 0x0

    const-wide v4, 0x3fe999999999999aL    # 0.8

    invoke-static/range {v0 .. v5}, LX/AMC;->b(DDD)D

    move-result-wide v8

    .line 1665968
    const-wide v0, 0x3ffb333333333333L    # 1.7

    div-double v0, p1, v0

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    invoke-static/range {v0 .. v5}, LX/AMC;->a(DDD)D

    move-result-wide v0

    .line 1665969
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    invoke-static/range {v0 .. v5}, LX/AMC;->b(DDD)D

    move-result-wide v0

    iput-wide v0, p0, LX/AMC;->a:D

    .line 1665970
    iget-wide v0, p0, LX/AMC;->a:D

    invoke-static {v0, v1}, LX/AMC;->d(D)D

    move-result-wide v4

    const-wide v6, 0x3f847ae147ae147bL    # 0.01

    move-wide v2, v8

    invoke-static/range {v2 .. v7}, LX/AMC;->d(DDD)D

    move-result-wide v0

    iput-wide v0, p0, LX/AMC;->b:D

    .line 1665971
    return-void
.end method

.method private static a(D)D
    .locals 6

    .prologue
    .line 1665962
    const-wide v0, 0x3f46f0068db8bac7L    # 7.0E-4

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {p0, p1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x3f9fbe76c8b43958L    # 0.031

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {p0, p1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide v2, 0x3fe47ae147ae147bL    # 0.64

    mul-double/2addr v2, p0

    add-double/2addr v0, v2

    const-wide v2, 0x3ff47ae147ae147bL    # 1.28

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private static a(DDD)D
    .locals 4

    .prologue
    .line 1665961
    sub-double v0, p0, p2

    sub-double v2, p4, p2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private static b(D)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 1665960
    const-wide v0, 0x3f0711947cfa26a2L    # 4.4E-5

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {p0, p1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x3f789374bc6a7efaL    # 0.006

    invoke-static {p0, p1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide v2, 0x3fd70a3d70a3d70aL    # 0.36

    mul-double/2addr v2, p0

    add-double/2addr v0, v2

    add-double/2addr v0, v6

    return-wide v0
.end method

.method private static b(DDD)D
    .locals 2

    .prologue
    .line 1665972
    sub-double v0, p4, p2

    mul-double/2addr v0, p0

    add-double/2addr v0, p2

    return-wide v0
.end method

.method private static c(D)D
    .locals 6

    .prologue
    .line 1665959
    const-wide v0, 0x3e9e32f0ee144531L    # 4.5E-7

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {p0, p1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x3f35c209246bf013L    # 3.32E-4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {p0, p1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide v2, 0x3fbb98c7e28240b8L    # 0.1078

    mul-double/2addr v2, p0

    add-double/2addr v0, v2

    const-wide v2, 0x40175c28f5c28f5cL    # 5.84

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private static c(DDD)D
    .locals 4

    .prologue
    .line 1665958
    mul-double v0, p0, p4

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, p0

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private static d(D)D
    .locals 9

    .prologue
    const-wide/high16 v6, 0x4046000000000000L    # 44.0

    const-wide/high16 v4, 0x4032000000000000L    # 18.0

    .line 1665950
    const-wide/16 v0, 0x0

    .line 1665951
    cmpg-double v2, p0, v4

    if-gtz v2, :cond_1

    .line 1665952
    invoke-static {p0, p1}, LX/AMC;->a(D)D

    move-result-wide v0

    .line 1665953
    :cond_0
    :goto_0
    return-wide v0

    .line 1665954
    :cond_1
    cmpl-double v2, p0, v4

    if-lez v2, :cond_2

    cmpg-double v2, p0, v6

    if-gtz v2, :cond_2

    .line 1665955
    invoke-static {p0, p1}, LX/AMC;->b(D)D

    move-result-wide v0

    goto :goto_0

    .line 1665956
    :cond_2
    cmpl-double v2, p0, v6

    if-lez v2, :cond_0

    .line 1665957
    invoke-static {p0, p1}, LX/AMC;->c(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method private static d(DDD)D
    .locals 7

    .prologue
    .line 1665949
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, p0

    mul-double v2, p0, p0

    sub-double/2addr v0, v2

    move-wide v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, LX/AMC;->c(DDD)D

    move-result-wide v0

    return-wide v0
.end method
