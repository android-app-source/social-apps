.class public LX/9gz;
.super LX/9gr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9gr",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForProfilePictureModel;",
        "Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;",
        "LX/5kD;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0se;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/0se;LX/03V;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1525890
    const-class v0, LX/5kD;

    invoke-direct {p0, p1, v0, p2}, LX/9gr;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1525891
    iput-object p3, p0, LX/9gz;->b:LX/0se;

    .line 1525892
    iput-object p4, p0, LX/9gz;->c:LX/03V;

    .line 1525893
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0gW;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForProfilePictureModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525894
    new-instance v0, LX/9gD;

    invoke-direct {v0}, LX/9gD;-><init>()V

    move-object v1, v0

    .line 1525895
    const-string v2, "profile_id"

    .line 1525896
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525897
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1525898
    iget-object v0, p0, LX/9gz;->b:LX/0se;

    invoke-virtual {v0, v1}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 1525899
    return-object v1
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForProfilePictureModel;",
            ">;)",
            "LX/9fz",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525871
    if-eqz p1, :cond_0

    .line 1525872
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525873
    if-eqz v0, :cond_0

    .line 1525874
    new-instance v1, LX/5kN;

    invoke-direct {v1}, LX/5kN;-><init>()V

    .line 1525875
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525876
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    .line 1525877
    iput-object v0, v1, LX/5kN;->D:Ljava/lang/String;

    .line 1525878
    move-object v1, v1

    .line 1525879
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525880
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForProfilePictureModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForProfilePictureModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 1525881
    iput-object v0, v1, LX/5kN;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1525882
    move-object v0, v1

    .line 1525883
    invoke-virtual {v0}, LX/5kN;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    .line 1525884
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1525885
    :goto_0
    new-instance v1, LX/9fz;

    new-instance v2, LX/9gW;

    invoke-direct {v2}, LX/9gW;-><init>()V

    invoke-virtual {v2}, LX/9gW;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/9fz;-><init>(LX/0Px;Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;)V

    return-object v1

    .line 1525886
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v1, v0

    .line 1525887
    iget-object v2, p0, LX/9gz;->c:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Result is null for profile id: "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1525888
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525889
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    move-object v0, v1

    goto :goto_0
.end method
