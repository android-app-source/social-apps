.class public final LX/8ks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8kh;",
        "LX/8ki;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 0

    .prologue
    .line 1396908
    iput-object p1, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2

    .prologue
    .line 1396904
    iget-object v0, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->x:LX/0Sg;

    invoke-virtual {v0, p2}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1396905
    iget-object v0, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const v1, 0x7f0d2159

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1396906
    iget-object v0, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    if-eqz v0, :cond_0

    .line 1396907
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1396909
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1396899
    check-cast p2, LX/8ki;

    .line 1396900
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    const-string v1, "fetchStickerMetadataWithLoader succeeded"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1396901
    iget-object v0, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const v1, 0x7f0d2159

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1396902
    iget-object v0, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->i:LX/0TD;

    new-instance v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView$4$1;

    invoke-direct {v1, p0, p2}, Lcom/facebook/stickers/keyboard/StickerKeyboardView$4$1;-><init>(LX/8ks;LX/8ki;)V

    const v2, 0x25bb2e05

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1396903
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1396892
    check-cast p2, Ljava/lang/Throwable;

    .line 1396893
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    const-string v1, "fetchStickerMetadataWithLoader failed"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1396894
    iget-object v0, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->l:LX/03V;

    sget-object v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fetch sticker metadata failed"

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1396895
    iget-object v0, p0, LX/8ks;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->H:LX/11i;

    sget-object v1, LX/8lJ;->a:LX/8lF;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1396896
    if-eqz v0, :cond_0

    .line 1396897
    const-string v1, "StickerPackLoadForPopup"

    const v2, 0x6c59909d

    invoke-static {v0, v1, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1396898
    :cond_0
    return-void
.end method
