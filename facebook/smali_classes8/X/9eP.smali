.class public LX/9eP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/9eQ;

.field public b:LX/9hP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/9hP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/animation/ValueAnimator;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private f:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method public constructor <init>(LX/9eQ;)V
    .locals 1

    .prologue
    .line 1519575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519576
    new-instance v0, LX/9eN;

    invoke-direct {v0, p0}, LX/9eN;-><init>(LX/9eP;)V

    iput-object v0, p0, LX/9eP;->e:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1519577
    new-instance v0, LX/9eO;

    invoke-direct {v0, p0}, LX/9eO;-><init>(LX/9eP;)V

    iput-object v0, p0, LX/9eP;->f:Landroid/animation/Animator$AnimatorListener;

    .line 1519578
    iput-object p1, p0, LX/9eP;->a:LX/9eQ;

    .line 1519579
    return-void
.end method

.method private a(LX/9hP;)LX/9hP;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1519589
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1519590
    iget-object v1, p0, LX/9eP;->a:LX/9eQ;

    invoke-virtual {v1, v0}, LX/9eQ;->getLocationOnScreen([I)V

    .line 1519591
    new-instance v1, LX/9hP;

    invoke-direct {v1}, LX/9hP;-><init>()V

    .line 1519592
    iget-object v2, v1, LX/9hP;->a:Landroid/graphics/Rect;

    iget-object v3, p1, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1519593
    iget-object v2, v1, LX/9hP;->b:Landroid/graphics/Rect;

    iget-object v3, p1, LX/9hP;->b:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1519594
    iget-object v2, v1, LX/9hP;->a:Landroid/graphics/Rect;

    aget v3, v0, v5

    neg-int v3, v3

    aget v4, v0, v6

    neg-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 1519595
    iget-object v2, v1, LX/9hP;->b:Landroid/graphics/Rect;

    aget v3, v0, v5

    neg-int v3, v3

    aget v0, v0, v6

    neg-int v0, v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1519596
    return-object v1
.end method

.method public static a(LX/9hP;LX/9hP;FLX/9hP;)V
    .locals 3

    .prologue
    .line 1519580
    iget-object v0, p0, LX/9hP;->b:Landroid/graphics/Rect;

    iget-object v1, p1, LX/9hP;->b:Landroid/graphics/Rect;

    iget-object v2, p3, LX/9hP;->b:Landroid/graphics/Rect;

    invoke-static {v0, v1, p2, v2}, LX/9eP;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;FLandroid/graphics/Rect;)V

    .line 1519581
    iget-object v0, p0, LX/9hP;->a:Landroid/graphics/Rect;

    iget-object v1, p1, LX/9hP;->a:Landroid/graphics/Rect;

    iget-object v2, p3, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-static {v0, v1, p2, v2}, LX/9eP;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;FLandroid/graphics/Rect;)V

    .line 1519582
    return-void
.end method

.method private static a(Landroid/graphics/Rect;Landroid/graphics/Rect;FLandroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 1519583
    iget v0, p0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    .line 1519584
    iget v1, p0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    .line 1519585
    iget v2, p0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    .line 1519586
    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    .line 1519587
    float-to-int v0, v0

    float-to-int v2, v2

    float-to-int v1, v1

    float-to-int v3, v3

    invoke-virtual {p3, v0, v2, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1519588
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1519552
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1519553
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1519554
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1519555
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1519556
    const/4 v0, 0x0

    iput-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    .line 1519557
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;LX/9hP;LX/9hP;LX/9eF;)V
    .locals 4
    .param p4    # LX/9eF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1519558
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1519559
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1519560
    :cond_0
    invoke-direct {p0, p2}, LX/9eP;->a(LX/9hP;)LX/9hP;

    move-result-object v0

    iput-object v0, p0, LX/9eP;->b:LX/9hP;

    .line 1519561
    invoke-direct {p0, p3}, LX/9eP;->a(LX/9hP;)LX/9hP;

    move-result-object v0

    iput-object v0, p0, LX/9eP;->c:LX/9hP;

    .line 1519562
    iget-object v0, p0, LX/9eP;->a:LX/9eQ;

    invoke-virtual {v0, p1}, LX/9eQ;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1519563
    iget-object v0, p0, LX/9eP;->a:LX/9eQ;

    iget-object v1, p0, LX/9eP;->b:LX/9hP;

    iget-object v2, p0, LX/9eP;->c:LX/9hP;

    invoke-virtual {v0, v1, v2}, LX/9eQ;->a(LX/9hP;LX/9hP;)V

    .line 1519564
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    .line 1519565
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1519566
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1519567
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/9eP;->e:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1519568
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/9eP;->f:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1519569
    if-eqz p4, :cond_1

    .line 1519570
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1519571
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1519572
    :cond_1
    iget-object v0, p0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1519573
    return-void

    .line 1519574
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
