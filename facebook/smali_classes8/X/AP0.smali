.class public LX/AP0;
.super Landroid/preference/PreferenceCategory;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1669917
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1669918
    return-void
.end method

.method public static a(LX/0QB;)LX/AP0;
    .locals 2

    .prologue
    .line 1669919
    new-instance v1, LX/AP0;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/AP0;-><init>(Landroid/content/Context;)V

    .line 1669920
    move-object v0, v1

    .line 1669921
    return-object v0
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 4

    .prologue
    .line 1669922
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1669923
    const-string v0, "Clash Management - Internal"

    invoke-virtual {p0, v0}, LX/AP0;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669924
    invoke-virtual {p0}, LX/AP0;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1669925
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1669926
    const-string v2, "Clash Management Settings"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1669927
    const-string v2, "View clash management settings"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1669928
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1669929
    invoke-virtual {p0, v1}, LX/AP0;->addPreference(Landroid/preference/Preference;)Z

    .line 1669930
    return-void
.end method
