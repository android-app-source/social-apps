.class public final LX/8nY;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8JX;

.field public final synthetic b:Ljava/lang/CharSequence;

.field public final synthetic c:LX/8nZ;


# direct methods
.method public constructor <init>(LX/8nZ;LX/8JX;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1401166
    iput-object p1, p0, LX/8nY;->c:LX/8nZ;

    iput-object p2, p0, LX/8nY;->a:LX/8JX;

    iput-object p3, p0, LX/8nY;->b:Ljava/lang/CharSequence;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1401182
    const-string v0, "fetch_groups_to_mentions"

    const-string v1, "Fail to load groups for mentions "

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401183
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1401167
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v10, 0x0

    .line 1401168
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1401169
    if-eqz p1, :cond_0

    .line 1401170
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401171
    if-nez v0, :cond_1

    .line 1401172
    :cond_0
    iget-object v0, p0, LX/8nY;->a:LX/8JX;

    iget-object v1, p0, LX/8nY;->b:Ljava/lang/CharSequence;

    invoke-static {v9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1401173
    :goto_0
    return-void

    .line 1401174
    :cond_1
    const/4 v0, 0x0

    move v8, v0

    .line 1401175
    :goto_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401176
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v8, v0, :cond_2

    .line 1401177
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401178
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v4

    .line 1401179
    iget-object v0, p0, LX/8nY;->c:LX/8nZ;

    iget-object v0, v0, LX/8nZ;->b:LX/3iT;

    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-virtual {v4}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v10, v10, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/facebook/tagging/graphql/protocol/FetchGroupsToMentionQueryModels$FetchGroupsToMentionQueryModel$GroupsModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/7Gr;->UNKNOWN:LX/7Gr;

    const-string v6, "groups_fetcher"

    sget-object v7, LX/8nE;->GROUPS:LX/8nE;

    invoke-virtual {v7}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1401180
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 1401181
    :cond_2
    iget-object v0, p0, LX/8nY;->a:LX/8JX;

    iget-object v1, p0, LX/8nY;->b:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v9}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    goto :goto_0
.end method
