.class public final LX/9mL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V
    .locals 0

    .prologue
    .line 1535435
    iput-object p1, p0, LX/9mL;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1535436
    iget-object v0, p0, LX/9mL;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    iget-object v0, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->m:LX/9lb;

    iget-object v1, p0, LX/9mL;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1535437
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v2, v2

    .line 1535438
    sget-object p0, LX/9lX;->a:[I

    .line 1535439
    iget-object p1, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    move-object p1, p1

    .line 1535440
    invoke-virtual {p1}, LX/9mN;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 1535441
    :goto_0
    return-void

    .line 1535442
    :pswitch_0
    iget-boolean p0, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    move p0, p0

    .line 1535443
    if-eqz p0, :cond_0

    .line 1535444
    iget-object v2, v0, LX/9lb;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1535445
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->c(Z)V

    goto :goto_0

    .line 1535446
    :cond_0
    iget-object p0, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    move-object p0, p0

    .line 1535447
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1535448
    iget-object p0, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    move-object p0, p0

    .line 1535449
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1535450
    iget-object p0, v0, LX/9lb;->g:LX/9lc;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v2

    .line 1535451
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "fb4a_rapid_reporting_tapped_cancel"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535452
    invoke-static {p0, p1, v2}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535453
    invoke-static {v0, v1}, LX/9lb;->g(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    goto :goto_0

    .line 1535454
    :cond_1
    sget-object p0, LX/9mN;->CANCEL_CONFIRM:LX/9mN;

    .line 1535455
    iget-object p1, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object p1, p1

    .line 1535456
    invoke-static {v1, v2, p0, p1}, LX/9lb;->a(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/DialogStateData;LX/9mN;LX/0gc;)V

    goto :goto_0

    .line 1535457
    :pswitch_1
    iget-object p0, v0, LX/9lb;->g:LX/9lc;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object p1

    .line 1535458
    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fb4a_rapid_reporting_aborted_cancel"

    invoke-direct {p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535459
    invoke-static {p0, p2, p1}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535460
    sget-object p0, LX/9mN;->FEEDBACK:LX/9mN;

    .line 1535461
    iget-object p1, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object p1, p1

    .line 1535462
    invoke-static {v1, v2, p0, p1}, LX/9lb;->a(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/DialogStateData;LX/9mN;LX/0gc;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
