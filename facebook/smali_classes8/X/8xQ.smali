.class public LX/8xQ;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "CurrentViewer"
.end annotation


# instance fields
.field private volatile a:Z

.field public final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;

.field public final e:Ljava/util/concurrent/Semaphore;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1424096
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1424097
    iput-boolean v1, p0, LX/8xQ;->a:Z

    .line 1424098
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/8xQ;->b:Ljava/util/Collection;

    .line 1424099
    new-instance v0, Lcom/facebook/catalyst/modules/fbauth/CurrentViewerModule$1;

    invoke-direct {v0, p0}, Lcom/facebook/catalyst/modules/fbauth/CurrentViewerModule$1;-><init>(LX/8xQ;)V

    iput-object v0, p0, LX/8xQ;->c:Ljava/lang/Runnable;

    .line 1424100
    new-instance v0, Lcom/facebook/catalyst/modules/fbauth/CurrentViewerModule$2;

    invoke-direct {v0, p0}, Lcom/facebook/catalyst/modules/fbauth/CurrentViewerModule$2;-><init>(LX/8xQ;)V

    iput-object v0, p0, LX/8xQ;->d:Ljava/lang/Runnable;

    .line 1424101
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, LX/8xQ;->e:Ljava/util/concurrent/Semaphore;

    .line 1424102
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1424103
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1424104
    const-string v0, "userFBID"

    .line 1424105
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 1424106
    invoke-static {v2}, LX/8xR;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424107
    const-string v2, "hasUser"

    .line 1424108
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1424109
    invoke-static {v0}, LX/8xR;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424110
    const-string v0, "isEmployee"

    .line 1424111
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 1424112
    invoke-static {v2}, LX/8xS;->a(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424113
    return-object v1

    .line 1424114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1424115
    invoke-super {p0}, LX/5pb;->f()V

    .line 1424116
    iget-object v0, p0, LX/8xQ;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1424117
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1424118
    const-string v0, "CurrentViewer"

    return-object v0
.end method

.method public logOut()V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1424119
    invoke-static {}, LX/5pe;->c()V

    .line 1424120
    iget-boolean v0, p0, LX/8xQ;->a:Z

    if-eqz v0, :cond_0

    .line 1424121
    :goto_0
    return-void

    .line 1424122
    :cond_0
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1424123
    const-string v1, "LoginPreferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "AccessToken"

    const/4 v5, 0x0

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1424124
    if-nez v0, :cond_1

    .line 1424125
    new-instance v0, LX/5p9;

    const-string v1, "You may not call CurrentViewer.logout() on a logged out user."

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1424126
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8xQ;->a:Z

    .line 1424127
    iget-object v0, p0, LX/8xQ;->c:Ljava/lang/Runnable;

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1424128
    :try_start_0
    iget-object v0, p0, LX/8xQ;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1424129
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1424130
    invoke-virtual {v0}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v0

    invoke-static {v0}, LX/9my;->a(Lcom/facebook/react/bridge/CatalystInstance;)V

    .line 1424131
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1424132
    invoke-static {v0, v3, v3, v3}, LX/8xR;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/5pC;)V

    .line 1424133
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1424134
    invoke-static {v0, v4}, LX/8xS;->a(Landroid/content/Context;Z)V

    .line 1424135
    iget-object v0, p0, LX/8xQ;->d:Ljava/lang/Runnable;

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1424136
    :catch_0
    move-exception v0

    .line 1424137
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Interrupted while waiting on logout listeners to be notified"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1424138
    :catchall_0
    move-exception v0

    .line 1424139
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1424140
    invoke-virtual {v1}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v1

    invoke-static {v1}, LX/9my;->a(Lcom/facebook/react/bridge/CatalystInstance;)V

    .line 1424141
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1424142
    invoke-static {v1, v3, v3, v3}, LX/8xR;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/5pC;)V

    .line 1424143
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1424144
    invoke-static {v1, v4}, LX/8xS;->a(Landroid/content/Context;Z)V

    throw v0
.end method

.method public setIsEmployee(Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1424145
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1424146
    invoke-static {v0, p1}, LX/8xS;->a(Landroid/content/Context;Z)V

    .line 1424147
    return-void
.end method
