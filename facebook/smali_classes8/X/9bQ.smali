.class public final LX/9bQ;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/io/InputStream;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1FJ;

.field public final synthetic b:LX/9bR;


# direct methods
.method public constructor <init>(LX/9bR;LX/1FJ;)V
    .locals 0

    .prologue
    .line 1514896
    iput-object p1, p0, LX/9bQ;->b:LX/9bR;

    iput-object p2, p0, LX/9bQ;->a:LX/1FJ;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1514897
    check-cast p1, [Ljava/io/InputStream;

    .line 1514898
    const/4 v0, 0x0

    aget-object v1, p1, v0

    .line 1514899
    :try_start_0
    iget-object v0, p0, LX/9bQ;->b:LX/9bR;

    iget-object v0, v0, LX/9bR;->f:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->j:LX/9iU;

    invoke-virtual {v0, v1}, LX/9iU;->a(Ljava/io/InputStream;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1514900
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1514901
    iget-object v1, p0, LX/9bQ;->a:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    .line 1514902
    :catch_0
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1514903
    iget-object v0, p0, LX/9bQ;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    const/4 v0, 0x0

    goto :goto_0

    .line 1514904
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1514905
    iget-object v1, p0, LX/9bQ;->a:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1514906
    check-cast p1, Landroid/net/Uri;

    .line 1514907
    iget-object v0, p0, LX/9bQ;->b:LX/9bR;

    iget-object v0, v0, LX/9bR;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1514908
    if-nez p1, :cond_0

    .line 1514909
    iget-object v0, p0, LX/9bQ;->b:LX/9bR;

    iget-object v0, v0, LX/9bR;->b:Landroid/support/v4/app/FragmentActivity;

    .line 1514910
    const v2, 0x7f081200

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1514911
    :goto_0
    return-void

    .line 1514912
    :cond_0
    iget-object v0, p0, LX/9bQ;->b:LX/9bR;

    iget-object v1, v0, LX/9bR;->f:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p0, LX/9bQ;->b:LX/9bR;

    iget-wide v2, v0, LX/9bR;->c:J

    iget-object v0, p0, LX/9bQ;->b:LX/9bR;

    iget-object v5, v0, LX/9bR;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, p0, LX/9bQ;->b:LX/9bR;

    iget-wide v6, v0, LX/9bR;->d:J

    move-object v4, p1

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Landroid/app/Activity;J)V

    goto :goto_0
.end method
