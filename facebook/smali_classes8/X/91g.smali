.class public final enum LX/91g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/91g;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/91g;

.field public static final enum GENERIC_ERROR:LX/91g;

.field public static final enum LOADED:LX/91g;

.field public static final enum LOADING:LX/91g;

.field public static final enum NETWORK_ERROR:LX/91g;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1431285
    new-instance v0, LX/91g;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v2}, LX/91g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/91g;->LOADED:LX/91g;

    .line 1431286
    new-instance v0, LX/91g;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/91g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/91g;->LOADING:LX/91g;

    .line 1431287
    new-instance v0, LX/91g;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v4}, LX/91g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/91g;->NETWORK_ERROR:LX/91g;

    .line 1431288
    new-instance v0, LX/91g;

    const-string v1, "GENERIC_ERROR"

    invoke-direct {v0, v1, v5}, LX/91g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/91g;->GENERIC_ERROR:LX/91g;

    .line 1431289
    const/4 v0, 0x4

    new-array v0, v0, [LX/91g;

    sget-object v1, LX/91g;->LOADED:LX/91g;

    aput-object v1, v0, v2

    sget-object v1, LX/91g;->LOADING:LX/91g;

    aput-object v1, v0, v3

    sget-object v1, LX/91g;->NETWORK_ERROR:LX/91g;

    aput-object v1, v0, v4

    sget-object v1, LX/91g;->GENERIC_ERROR:LX/91g;

    aput-object v1, v0, v5

    sput-object v0, LX/91g;->$VALUES:[LX/91g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1431290
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/91g;
    .locals 1

    .prologue
    .line 1431291
    const-class v0, LX/91g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/91g;

    return-object v0
.end method

.method public static values()[LX/91g;
    .locals 1

    .prologue
    .line 1431292
    sget-object v0, LX/91g;->$VALUES:[LX/91g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/91g;

    return-object v0
.end method
