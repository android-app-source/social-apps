.class public LX/9WV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/3fr;

.field public final b:LX/3iT;

.field public c:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1501222
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/9WV;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3fr;LX/3iT;LX/2RQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1501223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1501224
    iput-object p1, p0, LX/9WV;->a:LX/3fr;

    .line 1501225
    iput-object p2, p0, LX/9WV;->b:LX/3iT;

    .line 1501226
    iput-object p3, p0, LX/9WV;->c:LX/2RQ;

    .line 1501227
    return-void
.end method

.method public static a(LX/0QB;)LX/9WV;
    .locals 9

    .prologue
    .line 1501228
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1501229
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1501230
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1501231
    if-nez v1, :cond_0

    .line 1501232
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1501233
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1501234
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1501235
    sget-object v1, LX/9WV;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1501236
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1501237
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1501238
    :cond_1
    if-nez v1, :cond_4

    .line 1501239
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1501240
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1501241
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1501242
    new-instance p0, LX/9WV;

    invoke-static {v0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v1

    check-cast v1, LX/3fr;

    invoke-static {v0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v7

    check-cast v7, LX/3iT;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v8

    check-cast v8, LX/2RQ;

    invoke-direct {p0, v1, v7, v8}, LX/9WV;-><init>(LX/3fr;LX/3iT;LX/2RQ;)V

    .line 1501243
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1501244
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1501245
    if-nez v1, :cond_2

    .line 1501246
    sget-object v0, LX/9WV;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9WV;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1501247
    :goto_1
    if-eqz v0, :cond_3

    .line 1501248
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1501249
    :goto_3
    check-cast v0, LX/9WV;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1501250
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1501251
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1501252
    :catchall_1
    move-exception v0

    .line 1501253
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1501254
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1501255
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1501256
    :cond_2
    :try_start_8
    sget-object v0, LX/9WV;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9WV;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(LX/9WV;Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1501257
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    .line 1501258
    const/4 v1, 0x0

    .line 1501259
    :try_start_0
    iget-object v0, p0, LX/9WV;->a:LX/3fr;

    .line 1501260
    iget-object v2, p0, LX/9WV;->c:LX/2RQ;

    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1501261
    iput-object v3, v2, LX/2RR;->e:Ljava/lang/String;

    .line 1501262
    move-object v2, v2

    .line 1501263
    sget-object v3, LX/3Oq;->FRIENDS:LX/0Px;

    .line 1501264
    iput-object v3, v2, LX/2RR;->c:Ljava/util/Collection;

    .line 1501265
    move-object v2, v2

    .line 1501266
    sget-object v3, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 1501267
    iput-object v3, v2, LX/2RR;->n:LX/2RS;

    .line 1501268
    move-object v2, v2

    .line 1501269
    const/4 v3, 0x1

    .line 1501270
    iput-boolean v3, v2, LX/2RR;->o:Z

    .line 1501271
    move-object v2, v2

    .line 1501272
    move-object v2, v2

    .line 1501273
    sget-object v3, LX/0PH;->NAME:LX/0PH;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/3fr;->a(LX/2RR;Ljava/util/Set;)LX/6N1;

    move-result-object v1

    .line 1501274
    const-string v0, "contacts_db"

    .line 1501275
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1501276
    if-nez v1, :cond_2

    move-object v4, v12

    .line 1501277
    :goto_0
    move-object v0, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1501278
    if-eqz v1, :cond_0

    .line 1501279
    invoke-interface {v1}, LX/6N1;->close()V

    .line 1501280
    :cond_0
    return-object v0

    .line 1501281
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 1501282
    invoke-interface {v1}, LX/6N1;->close()V

    :cond_1
    throw v0

    .line 1501283
    :cond_2
    :goto_1
    invoke-interface {v1}, LX/6N1;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1501284
    invoke-interface {v1}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v9, v4

    check-cast v9, Lcom/facebook/contacts/graphql/Contact;

    .line 1501285
    iget-object v4, p0, LX/9WV;->b:LX/3iT;

    invoke-virtual {v9}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v5

    invoke-virtual {v9}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v9}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v9

    .line 1501286
    sget-object v10, LX/9WU;->a:[I

    invoke-virtual {v9}, LX/2RU;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 1501287
    sget-object v10, LX/7Gr;->UNKNOWN:LX/7Gr;

    :goto_2
    move-object v9, v10

    .line 1501288
    sget-object v10, LX/8nE;->FRIENDS:LX/8nE;

    invoke-virtual {v10}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v11

    move-object v10, v0

    invoke-virtual/range {v4 .. v11}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v4, v12

    .line 1501289
    goto :goto_0

    .line 1501290
    :pswitch_0
    sget-object v10, LX/7Gr;->USER:LX/7Gr;

    goto :goto_2

    .line 1501291
    :pswitch_1
    sget-object v10, LX/7Gr;->PAGE:LX/7Gr;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
