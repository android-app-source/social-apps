.class public LX/9hh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/0tX;

.field private final c:LX/0tc;

.field public final d:LX/9hs;

.field public final e:LX/9fy;

.field public final f:LX/0Uh;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0tc;LX/9hs;LX/0Or;LX/0Uh;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/0tc;",
            "LX/9hs;",
            "LX/0Or",
            "<",
            "LX/9fy;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1526818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526819
    iput-object p1, p0, LX/9hh;->a:Ljava/util/concurrent/ExecutorService;

    .line 1526820
    iput-object p2, p0, LX/9hh;->b:LX/0tX;

    .line 1526821
    iput-object p3, p0, LX/9hh;->c:LX/0tc;

    .line 1526822
    iput-object p4, p0, LX/9hh;->d:LX/9hs;

    .line 1526823
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fy;

    iput-object v0, p0, LX/9hh;->e:LX/9fy;

    .line 1526824
    iput-object p6, p0, LX/9hh;->f:LX/0Uh;

    .line 1526825
    return-void
.end method

.method public static a(LX/0QB;)LX/9hh;
    .locals 1

    .prologue
    .line 1526817
    invoke-static {p0}, LX/9hh;->b(LX/0QB;)LX/9hh;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;",
            "LX/9hk;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1526816
    iget-object v0, p0, LX/9hh;->c:LX/0tc;

    invoke-virtual {v0, p2}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/9hh;->a(Ljava/util/concurrent/Callable;LX/37X;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;[",
            "LX/4VT;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture;"
        }
    .end annotation

    .prologue
    .line 1526796
    iget-object v0, p0, LX/9hh;->c:LX/0tc;

    invoke-virtual {v0, p2}, LX/0tc;->a([LX/4VT;)LX/37X;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/9hh;->a(Ljava/util/concurrent/Callable;LX/37X;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/concurrent/Callable;LX/37X;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;",
            "LX/37X;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture;"
        }
    .end annotation

    .prologue
    .line 1526813
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 1526814
    iget-object v1, p0, LX/9hh;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/facebook/photos/mediagallery/mutation/MediaMutationGenerator$10;-><init>(LX/9hh;LX/37X;Ljava/util/concurrent/Callable;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v3, 0x7d631de1

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1526815
    return-object v0
.end method

.method public static b(LX/0QB;)LX/9hh;
    .locals 7

    .prologue
    .line 1526811
    new-instance v0, LX/9hh;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v3

    check-cast v3, LX/0tc;

    invoke-static {p0}, LX/9hs;->a(LX/0QB;)LX/9hs;

    move-result-object v4

    check-cast v4, LX/9hs;

    const/16 v5, 0x2e5f

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct/range {v0 .. v6}, LX/9hh;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0tc;LX/9hs;LX/0Or;LX/0Uh;)V

    .line 1526812
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1526805
    new-instance v0, LX/9hZ;

    invoke-direct {v0, p0, p1, p2}, LX/9hZ;-><init>(LX/9hh;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)V

    .line 1526806
    iget-object v1, p0, LX/9hh;->f:LX/0Uh;

    const/16 v2, 0x405

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1526807
    const/4 v1, 0x1

    new-array v1, v1, [LX/4VT;

    new-instance v2, LX/9hz;

    invoke-direct {v2, p1, p2}, LX/9hz;-><init>(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)V

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1526808
    :goto_0
    return-object v0

    .line 1526809
    :cond_0
    new-instance v1, LX/9hw;

    invoke-direct {v1, p1, p2}, LX/9hw;-><init>(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)V

    move-object v1, v1

    .line 1526810
    invoke-static {p0, v0, v1}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1526797
    new-instance v8, LX/9ha;

    invoke-direct {v8, p0, p1, p2, p3}, LX/9ha;-><init>(LX/9hh;Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)V

    .line 1526798
    iget-object v0, p0, LX/9hh;->f:LX/0Uh;

    const/16 v1, 0x405

    invoke-virtual {v0, v1, v10}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1526799
    const/4 v0, 0x1

    new-array v9, v0, [LX/4VT;

    new-instance v0, LX/9hS;

    invoke-virtual {p2}, Lcom/facebook/tagging/model/TaggingProfile;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 1526800
    iget-object v1, p2, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 1526801
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v3

    iget v1, p3, Landroid/graphics/PointF;->x:F

    float-to-double v4, v1

    iget v1, p3, Landroid/graphics/PointF;->y:F

    float-to-double v6, v1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LX/9hS;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)V

    aput-object v0, v9, v10

    invoke-static {p0, v8, v9}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1526802
    :goto_0
    return-object v0

    .line 1526803
    :cond_0
    new-instance v0, LX/9hu;

    invoke-direct {v0, p1, p2, p3}, LX/9hu;-><init>(Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)V

    move-object v0, v0

    .line 1526804
    invoke-static {p0, v8, v0}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
