.class public final LX/9RY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1491030
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1491031
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491032
    :goto_0
    return v1

    .line 1491033
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491034
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 1491035
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1491036
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1491037
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1491038
    const-string v8, "cover_photo"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1491039
    const/4 v7, 0x0

    .line 1491040
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_c

    .line 1491041
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491042
    :goto_2
    move v6, v7

    .line 1491043
    goto :goto_1

    .line 1491044
    :cond_2
    const-string v8, "group_members"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1491045
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1491046
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_19

    .line 1491047
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491048
    :goto_3
    move v5, v7

    .line 1491049
    goto :goto_1

    .line 1491050
    :cond_3
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1491051
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1491052
    :cond_4
    const-string v8, "name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1491053
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1491054
    :cond_5
    const-string v8, "viewer_join_state"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1491055
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 1491056
    :cond_6
    const-string v8, "visibility_sentence"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1491057
    const/4 v7, 0x0

    .line 1491058
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v8, :cond_1d

    .line 1491059
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491060
    :goto_4
    move v0, v7

    .line 1491061
    goto/16 :goto_1

    .line 1491062
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1491063
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1491064
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1491065
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1491066
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1491067
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1491068
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1491069
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1

    .line 1491070
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491071
    :cond_a
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1491072
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1491073
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1491074
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_a

    if-eqz v8, :cond_a

    .line 1491075
    const-string v9, "photo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1491076
    const/4 v8, 0x0

    .line 1491077
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_10

    .line 1491078
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491079
    :goto_6
    move v6, v8

    .line 1491080
    goto :goto_5

    .line 1491081
    :cond_b
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1491082
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1491083
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_c
    move v6, v7

    goto :goto_5

    .line 1491084
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491085
    :cond_e
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 1491086
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1491087
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1491088
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_e

    if-eqz v9, :cond_e

    .line 1491089
    const-string v10, "image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1491090
    const/4 v9, 0x0

    .line 1491091
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v10, :cond_14

    .line 1491092
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491093
    :goto_8
    move v6, v9

    .line 1491094
    goto :goto_7

    .line 1491095
    :cond_f
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1491096
    invoke-virtual {p1, v8, v6}, LX/186;->b(II)V

    .line 1491097
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_6

    :cond_10
    move v6, v8

    goto :goto_7

    .line 1491098
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491099
    :cond_12
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_13

    .line 1491100
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1491101
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1491102
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_12

    if-eqz v10, :cond_12

    .line 1491103
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 1491104
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_9

    .line 1491105
    :cond_13
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1491106
    invoke-virtual {p1, v9, v6}, LX/186;->b(II)V

    .line 1491107
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_8

    :cond_14
    move v6, v9

    goto :goto_9

    .line 1491108
    :cond_15
    :goto_a
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_17

    .line 1491109
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1491110
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1491111
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_15

    if-eqz v10, :cond_15

    .line 1491112
    const-string v11, "count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    .line 1491113
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v8

    goto :goto_a

    .line 1491114
    :cond_16
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_a

    .line 1491115
    :cond_17
    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1491116
    if-eqz v5, :cond_18

    .line 1491117
    invoke-virtual {p1, v7, v9, v7}, LX/186;->a(III)V

    .line 1491118
    :cond_18
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_3

    :cond_19
    move v5, v7

    move v9, v7

    goto :goto_a

    .line 1491119
    :cond_1a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1491120
    :cond_1b
    :goto_b
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_1c

    .line 1491121
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1491122
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1491123
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1b

    if-eqz v8, :cond_1b

    .line 1491124
    const-string v9, "text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1a

    .line 1491125
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_b

    .line 1491126
    :cond_1c
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1491127
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1491128
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_4

    :cond_1d
    move v0, v7

    goto :goto_b
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    .line 1491129
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1491130
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1491131
    if-eqz v0, :cond_3

    .line 1491132
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491133
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1491134
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1491135
    if-eqz v1, :cond_2

    .line 1491136
    const-string v3, "photo"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491137
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1491138
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1491139
    if-eqz v3, :cond_1

    .line 1491140
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491141
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1491142
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1491143
    if-eqz v0, :cond_0

    .line 1491144
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491145
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1491146
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1491147
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1491148
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1491149
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1491150
    if-eqz v0, :cond_5

    .line 1491151
    const-string v1, "group_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491152
    const/4 v1, 0x0

    .line 1491153
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1491154
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1491155
    if-eqz v1, :cond_4

    .line 1491156
    const-string v3, "count"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491157
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1491158
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1491159
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1491160
    if-eqz v0, :cond_6

    .line 1491161
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491162
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1491163
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1491164
    if-eqz v0, :cond_7

    .line 1491165
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491166
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1491167
    :cond_7
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1491168
    if-eqz v0, :cond_8

    .line 1491169
    const-string v0, "viewer_join_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491170
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1491171
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1491172
    if-eqz v0, :cond_a

    .line 1491173
    const-string v1, "visibility_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491174
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1491175
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1491176
    if-eqz v1, :cond_9

    .line 1491177
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1491178
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1491179
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1491180
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1491181
    return-void
.end method
