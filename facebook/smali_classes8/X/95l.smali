.class public final LX/95l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kR;


# instance fields
.field public final synthetic a:LX/95n;


# direct methods
.method public constructor <init>(LX/95n;)V
    .locals 0

    .prologue
    .line 1437485
    iput-object p1, p0, LX/95l;->a:LX/95n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2nf;)V
    .locals 2
    .param p1    # LX/2nf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDatabase"
    .end annotation

    .prologue
    .line 1437486
    iget-object v0, p0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1437487
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/95l;->a:LX/95n;

    iget-boolean v0, v0, LX/95n;->u:Z

    if-nez v0, :cond_1

    .line 1437488
    :cond_0
    :goto_0
    return-void

    .line 1437489
    :cond_1
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;-><init>(LX/95l;LX/2nf;)V

    .line 1437490
    iget-object v1, p0, LX/95l;->a:LX/95n;

    iget-object v1, v1, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1437491
    :try_start_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437492
    iget-object v0, p0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/95l;->a:LX/95n;

    iget-object v1, v1, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 1437493
    :cond_2
    iget-object v1, p0, LX/95l;->a:LX/95n;

    iget-object v1, v1, LX/95n;->t:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
