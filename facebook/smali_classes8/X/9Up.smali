.class public LX/9Up;
.super LX/3Jj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Jj",
        "<",
        "LX/3Jc;",
        "LX/9Uo;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1498543
    invoke-direct {p0}, LX/3Jj;-><init>()V

    .line 1498544
    return-void
.end method

.method private constructor <init>(Ljava/util/List;[[[F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3Jc;",
            ">;[[[F)V"
        }
    .end annotation

    .prologue
    .line 1498541
    invoke-direct {p0, p1, p2}, LX/3Jj;-><init>(Ljava/util/List;[[[F)V

    .line 1498542
    return-void
.end method

.method public static a(LX/3Jf;)LX/9Up;
    .locals 3

    .prologue
    .line 1498545
    iget-object v0, p0, LX/3Jf;->b:LX/3JT;

    move-object v0, v0

    .line 1498546
    sget-object v1, LX/3JT;->OPACITY:LX/3JT;

    if-eq v0, v1, :cond_0

    .line 1498547
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create a KeyFramedOpacity object from a non OPACITY animation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1498548
    :cond_0
    new-instance v0, LX/9Up;

    .line 1498549
    iget-object v1, p0, LX/3Jf;->c:Ljava/util/List;

    move-object v1, v1

    .line 1498550
    iget-object v2, p0, LX/3Jf;->d:[[[F

    move-object v2, v2

    .line 1498551
    invoke-direct {v0, v1, v2}, LX/9Up;-><init>(Ljava/util/List;[[[F)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V
    .locals 3

    .prologue
    .line 1498529
    check-cast p1, LX/3Jc;

    check-cast p2, LX/3Jc;

    check-cast p4, LX/9Uo;

    const/4 v2, 0x0

    .line 1498530
    if-nez p2, :cond_0

    .line 1498531
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 1498532
    aget v0, v0, v2

    .line 1498533
    iput v0, p4, LX/9Uo;->a:F

    .line 1498534
    :goto_0
    return-void

    .line 1498535
    :cond_0
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 1498536
    aget v0, v0, v2

    .line 1498537
    iget-object v1, p2, LX/3Jc;->b:[F

    move-object v1, v1

    .line 1498538
    aget v1, v1, v2

    invoke-static {v0, v1, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    .line 1498539
    iput v0, p4, LX/9Uo;->a:F

    .line 1498540
    goto :goto_0
.end method
