.class public final LX/A6c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/A6e;


# direct methods
.method public constructor <init>(LX/A6e;)V
    .locals 0

    .prologue
    .line 1624551
    iput-object p1, p0, LX/A6c;->a:LX/A6e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1624552
    iget-object v0, p0, LX/A6c;->a:LX/A6e;

    iget-object v0, v0, LX/A6e;->m:LX/A79;

    iget-object v1, p0, LX/A6c;->a:LX/A6e;

    iget v1, v1, LX/A6e;->f:I

    .line 1624553
    iget-object v2, v0, LX/A79;->b:LX/A78;

    const/4 v3, 0x0

    .line 1624554
    const-string v4, "transliteration_dictionary"

    invoke-static {v2, v4, v3, v1}, LX/A78;->a(LX/A78;Ljava/lang/String;II)I

    move-result v4

    .line 1624555
    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 1624556
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "transliteration_dictionary_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1624557
    invoke-static {v2, v4}, LX/A78;->a(LX/A78;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1624558
    :goto_0
    move-object v2, v4

    .line 1624559
    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v0, v2

    .line 1624560
    iget-object v1, p0, LX/A6c;->a:LX/A6e;

    iget-object v1, v1, LX/A6e;->k:LX/A77;

    iget-object v2, p0, LX/A6c;->a:LX/A6e;

    iget v2, v2, LX/A6e;->f:I

    invoke-virtual {v1, v2}, LX/A77;->a(I)Ljava/util/Map;

    move-result-object v1

    .line 1624561
    if-eqz v0, :cond_0

    .line 1624562
    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1624563
    :cond_0
    return-object v1

    :cond_1
    invoke-static {v0, v2}, LX/A79;->a(LX/A79;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method
