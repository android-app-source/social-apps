.class public LX/8yV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8yU;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/8yV;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8yW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1425928
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8yV;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8yW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425832
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1425833
    iput-object p1, p0, LX/8yV;->b:LX/0Ot;

    .line 1425834
    return-void
.end method

.method public static a(LX/0QB;)LX/8yV;
    .locals 4

    .prologue
    .line 1425915
    sget-object v0, LX/8yV;->c:LX/8yV;

    if-nez v0, :cond_1

    .line 1425916
    const-class v1, LX/8yV;

    monitor-enter v1

    .line 1425917
    :try_start_0
    sget-object v0, LX/8yV;->c:LX/8yV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1425918
    if-eqz v2, :cond_0

    .line 1425919
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1425920
    new-instance v3, LX/8yV;

    const/16 p0, 0x1937

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8yV;-><init>(LX/0Ot;)V

    .line 1425921
    move-object v0, v3

    .line 1425922
    sput-object v0, LX/8yV;->c:LX/8yV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1425923
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1425924
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1425925
    :cond_1
    sget-object v0, LX/8yV;->c:LX/8yV;

    return-object v0

    .line 1425926
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1425927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1425912
    check-cast p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    .line 1425913
    iget-object v0, p0, LX/8yV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8yW;

    iget-object v2, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget v3, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->b:I

    iget v4, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->c:I

    iget v5, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    iget v6, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->e:I

    iget-boolean v7, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->f:Z

    iget-object v8, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->g:Ljava/util/List;

    iget v9, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->h:I

    iget v10, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->i:I

    iget v11, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->j:I

    iget v12, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->k:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, LX/8yW;->a(LX/1De;Lcom/facebook/common/callercontext/CallerContext;IIIIZLjava/util/List;IIII)LX/1Dg;

    move-result-object v0

    .line 1425914
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1425910
    invoke-static {}, LX/1dS;->b()V

    .line 1425911
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/8yU;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1425902
    new-instance v1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;-><init>(LX/8yV;)V

    .line 1425903
    sget-object v2, LX/8yV;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8yU;

    .line 1425904
    if-nez v2, :cond_0

    .line 1425905
    new-instance v2, LX/8yU;

    invoke-direct {v2}, LX/8yU;-><init>()V

    .line 1425906
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/8yU;->a$redex0(LX/8yU;LX/1De;IILcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;)V

    .line 1425907
    move-object v1, v2

    .line 1425908
    move-object v0, v1

    .line 1425909
    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1425835
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1425836
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 1425837
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 1425838
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 1425839
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 1425840
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 1425841
    iget-object v0, p0, LX/8yV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p1

    const/4 v8, 0x0

    .line 1425842
    sget-object v7, LX/03r;->FbFacepileComponent:[I

    invoke-virtual {v0, v7, v8}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v9

    .line 1425843
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v10

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_6

    .line 1425844
    invoke-virtual {v9, v7}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result p0

    .line 1425845
    const/16 p1, 0x0

    if-ne p0, p1, :cond_1

    .line 1425846
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425847
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1425848
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1425849
    :cond_1
    const/16 p1, 0x1

    if-ne p0, p1, :cond_2

    .line 1425850
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425851
    iput-object p0, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1425852
    goto :goto_1

    .line 1425853
    :cond_2
    const/16 p1, 0x2

    if-ne p0, p1, :cond_3

    .line 1425854
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425855
    iput-object p0, v3, LX/1np;->a:Ljava/lang/Object;

    .line 1425856
    goto :goto_1

    .line 1425857
    :cond_3
    const/16 p1, 0x3

    if-ne p0, p1, :cond_4

    .line 1425858
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425859
    iput-object p0, v4, LX/1np;->a:Ljava/lang/Object;

    .line 1425860
    goto :goto_1

    .line 1425861
    :cond_4
    const/16 p1, 0x4

    if-ne p0, p1, :cond_5

    .line 1425862
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    .line 1425863
    iput-object p0, v5, LX/1np;->a:Ljava/lang/Object;

    .line 1425864
    goto :goto_1

    .line 1425865
    :cond_5
    const/16 p1, 0x5

    if-ne p0, p1, :cond_0

    .line 1425866
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425867
    iput-object p0, v6, LX/1np;->a:Ljava/lang/Object;

    .line 1425868
    goto :goto_1

    .line 1425869
    :cond_6
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    .line 1425870
    check-cast p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    .line 1425871
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425872
    if-eqz v0, :cond_7

    .line 1425873
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425874
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->b:I

    .line 1425875
    :cond_7
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1425876
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425877
    if-eqz v0, :cond_8

    .line 1425878
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425879
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->c:I

    .line 1425880
    :cond_8
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 1425881
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425882
    if-eqz v0, :cond_9

    .line 1425883
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425884
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    .line 1425885
    :cond_9
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 1425886
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425887
    if-eqz v0, :cond_a

    .line 1425888
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425889
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->e:I

    .line 1425890
    :cond_a
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 1425891
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425892
    if-eqz v0, :cond_b

    .line 1425893
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425894
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->f:Z

    .line 1425895
    :cond_b
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 1425896
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425897
    if-eqz v0, :cond_c

    .line 1425898
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425899
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->h:I

    .line 1425900
    :cond_c
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 1425901
    return-void
.end method
