.class public final LX/9We;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/Runnable;

.field public final synthetic d:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1501450
    iput-object p1, p0, LX/9We;->d:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    iput-object p2, p0, LX/9We;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9We;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9We;->c:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    .line 1501451
    iget-object v0, p0, LX/9We;->d:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    iget-object v0, v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1501452
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1501453
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1501454
    iget-object v0, p0, LX/9We;->d:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    iget-object v1, p0, LX/9We;->d:Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    iget-object v1, v1, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1501455
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1501456
    iget-object v2, p0, LX/9We;->a:Ljava/lang/String;

    iget-object v3, p0, LX/9We;->b:Ljava/lang/String;

    iget-object v4, p0, LX/9We;->c:Ljava/lang/Runnable;

    .line 1501457
    new-instance v5, LX/4BY;

    iget-object v6, v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->b:Landroid/content/Context;

    invoke-direct {v5, v6}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1501458
    iget-object v6, v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080fbc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1501459
    invoke-virtual {v5}, LX/4BY;->show()V

    .line 1501460
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1501461
    const-string v7, "blockUser"

    new-instance v8, Lcom/facebook/friends/methods/BlockUserMethod$Params;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-direct {v8, v9, v10, v11, v12}, Lcom/facebook/friends/methods/BlockUserMethod$Params;-><init>(JJ)V

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1501462
    iget-object v7, v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->d:LX/1Ck;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ban_user_"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_from_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "friending_block_user"

    .line 1501463
    new-instance v10, LX/9Wg;

    invoke-direct {v10, v0, v9, v6}, LX/9Wg;-><init>(Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object v6, v10

    .line 1501464
    new-instance v9, LX/9Wf;

    invoke-direct {v9, v0, v5, v3, v4}, LX/9Wf;-><init>(Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;LX/4BY;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-virtual {v7, v8, v6, v9}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1501465
    return-void
.end method
