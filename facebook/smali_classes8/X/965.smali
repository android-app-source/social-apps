.class public final LX/965;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Lcom/facebook/graphql/model/FeedUnit;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Z

.field public final synthetic e:LX/967;


# direct methods
.method public constructor <init>(LX/967;LX/1L9;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V
    .locals 0

    .prologue
    .line 1438131
    iput-object p1, p0, LX/965;->e:LX/967;

    iput-object p2, p0, LX/965;->a:LX/1L9;

    iput-object p3, p0, LX/965;->b:Lcom/facebook/graphql/model/FeedUnit;

    iput-object p4, p0, LX/965;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-boolean p5, p0, LX/965;->d:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1438132
    iget-object v0, p0, LX/965;->e:LX/967;

    iget-object v1, v0, LX/967;->b:LX/189;

    iget-object v2, p0, LX/965;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p0, LX/965;->e:LX/967;

    iget-object v0, v0, LX/967;->c:LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    iget-boolean v0, p0, LX/965;->d:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1438133
    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1438134
    if-nez v0, :cond_2

    .line 1438135
    iget-object v0, p0, LX/965;->e:LX/967;

    iget-object v0, v0, LX/967;->i:LX/03V;

    sget-object v1, LX/967;->a:Ljava/lang/String;

    const-string v2, "Feedbackable should either be a FeedUnit or it\'s root should be a FeedUnit"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1438136
    :cond_0
    :goto_1
    return-void

    .line 1438137
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1438138
    :cond_2
    iget-object v1, p0, LX/965;->a:LX/1L9;

    if-eqz v1, :cond_0

    .line 1438139
    iget-object v1, p0, LX/965;->a:LX/1L9;

    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1438140
    iget-object v0, p0, LX/965;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438141
    iget-object v0, p0, LX/965;->a:LX/1L9;

    iget-object v1, p0, LX/965;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0, v1}, LX/1L9;->b(Ljava/lang/Object;)V

    .line 1438142
    :cond_0
    return-void
.end method
