.class public LX/9c7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1515959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1515960
    iput-object p1, p0, LX/9c7;->a:LX/0Zb;

    .line 1515961
    return-void
.end method

.method public static a(LX/0QB;)LX/9c7;
    .locals 1

    .prologue
    .line 1515962
    invoke-static {p0}, LX/9c7;->b(LX/0QB;)LX/9c7;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1515963
    iget-object v0, p0, LX/9c7;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1515964
    iget-object v0, p0, LX/9c7;->b:Ljava/lang/String;

    .line 1515965
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1515966
    :cond_0
    iget-object v0, p0, LX/9c7;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1515967
    sget-object v0, LX/9c5;->MEDIA_ITEM_IDENTIFIER:LX/9c5;

    invoke-virtual {v0}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/9c7;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1515968
    :cond_1
    iget-object v0, p0, LX/9c7;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1515969
    return-void
.end method

.method public static b(LX/0QB;)LX/9c7;
    .locals 2

    .prologue
    .line 1515970
    new-instance v1, LX/9c7;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/9c7;-><init>(LX/0Zb;)V

    .line 1515971
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1515972
    iput-object p1, p0, LX/9c7;->b:Ljava/lang/String;

    .line 1515973
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1515974
    iput-object p1, p0, LX/9c7;->b:Ljava/lang/String;

    .line 1515975
    iput-object p2, p0, LX/9c7;->c:Ljava/lang/String;

    .line 1515976
    return-void
.end method
