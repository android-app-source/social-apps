.class public final LX/A82;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1627072
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1627073
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1627074
    if-eqz v0, :cond_0

    .line 1627075
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627076
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1627077
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1627078
    if-eqz v0, :cond_1

    .line 1627079
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627080
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1627081
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1627082
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1627083
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1627084
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1627085
    :goto_0
    return v1

    .line 1627086
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1627087
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1627088
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1627089
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1627090
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1627091
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1627092
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1627093
    :cond_2
    const-string v4, "name"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1627094
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1627095
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1627096
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1627097
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1627098
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 1627099
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1627100
    const-wide/16 v6, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 1627101
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_7

    .line 1627102
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1627103
    :goto_0
    move v1, v8

    .line 1627104
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1627105
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1627106
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, v13, :cond_4

    .line 1627107
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1627108
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1627109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v12, :cond_0

    .line 1627110
    const-string v13, "is_opted_in"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1627111
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v9

    move v11, v9

    move v9, v3

    goto :goto_1

    .line 1627112
    :cond_1
    const-string v13, "opted_in_time"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1627113
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    move v2, v3

    goto :goto_1

    .line 1627114
    :cond_2
    const-string v13, "owner_id"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1627115
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1627116
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1627117
    :cond_4
    const/4 v12, 0x3

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1627118
    if-eqz v9, :cond_5

    .line 1627119
    invoke-virtual {v0, v8, v11}, LX/186;->a(IZ)V

    .line 1627120
    :cond_5
    if-eqz v2, :cond_6

    move-object v2, v0

    .line 1627121
    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1627122
    :cond_6
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1627123
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_0

    :cond_7
    move v2, v8

    move v9, v8

    move v10, v8

    move-wide v4, v6

    move v11, v8

    goto :goto_1
.end method

.method public static a$redex0(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1627124
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1627125
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1627126
    if-eqz v0, :cond_0

    .line 1627127
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627128
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1627129
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1627130
    if-eqz v0, :cond_1

    .line 1627131
    const-string v1, "extended_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627132
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1627133
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1627134
    if-eqz v0, :cond_2

    .line 1627135
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627136
    invoke-static {p0, v0, p2}, LX/4i9;->a(LX/15i;ILX/0nX;)V

    .line 1627137
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1627138
    if-eqz v0, :cond_3

    .line 1627139
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627140
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1627141
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1627142
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1627143
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1627144
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1627145
    :goto_0
    return v1

    .line 1627146
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1627147
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1627148
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1627149
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1627150
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1627151
    const-string v4, "place_question_answer_label"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1627152
    invoke-static {p0, p1}, LX/96V;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1627153
    :cond_2
    const-string v4, "place_question_answer_value"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1627154
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1627155
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1627156
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1627157
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1627158
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1627159
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1627160
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1627161
    if-eqz v0, :cond_7

    .line 1627162
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627163
    const/4 v3, 0x0

    .line 1627164
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1627165
    invoke-virtual {p0, v0, v3}, LX/15i;->g(II)I

    move-result v1

    .line 1627166
    if-eqz v1, :cond_4

    .line 1627167
    const-string v2, "addressbook_contacts"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627168
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1627169
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1627170
    if-eqz v2, :cond_3

    .line 1627171
    const-string v4, "nodes"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627172
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1627173
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result p1

    if-ge v4, p1, :cond_2

    .line 1627174
    invoke-virtual {p0, v2, v4}, LX/15i;->q(II)I

    move-result p1

    .line 1627175
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1627176
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1627177
    if-eqz v1, :cond_0

    .line 1627178
    const-string p3, "record_id"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627179
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1627180
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1627181
    if-eqz v1, :cond_1

    .line 1627182
    const-string p3, "uuid"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627183
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1627184
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1627185
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1627186
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1627187
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1627188
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1627189
    if-eqz v1, :cond_5

    .line 1627190
    const-string v2, "is_in_sync"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627191
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1627192
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 1627193
    if-eqz v1, :cond_6

    .line 1627194
    const-string v2, "max_contacts_allowed"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1627195
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1627196
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1627197
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1627198
    return-void
.end method
