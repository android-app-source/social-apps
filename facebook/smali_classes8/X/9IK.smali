.class public final LX/9IK;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9IL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public b:Z

.field public final synthetic c:LX/9IL;


# direct methods
.method public constructor <init>(LX/9IL;)V
    .locals 1

    .prologue
    .line 1462847
    iput-object p1, p0, LX/9IK;->c:LX/9IL;

    .line 1462848
    move-object v0, p1

    .line 1462849
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1462850
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1462851
    const-string v0, "CommentVideoAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1462852
    if-ne p0, p1, :cond_1

    .line 1462853
    :cond_0
    :goto_0
    return v0

    .line 1462854
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1462855
    goto :goto_0

    .line 1462856
    :cond_3
    check-cast p1, LX/9IK;

    .line 1462857
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1462858
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1462859
    if-eq v2, v3, :cond_0

    .line 1462860
    iget-object v2, p0, LX/9IK;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9IK;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/9IK;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1462861
    goto :goto_0

    .line 1462862
    :cond_5
    iget-object v2, p1, LX/9IK;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_4

    .line 1462863
    :cond_6
    iget-boolean v2, p0, LX/9IK;->b:Z

    iget-boolean v3, p1, LX/9IK;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1462864
    goto :goto_0
.end method
