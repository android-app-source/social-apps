.class public final LX/95X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TTEdge;",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2jy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2jy",
            "<TTEdge;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2jy;LX/0Or;)V
    .locals 1
    .param p1    # LX/2jy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2jy",
            "<TTEdge;>;",
            "LX/0Or",
            "<",
            "LX/1My;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1436944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436945
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/95X;->b:Ljava/util/HashMap;

    .line 1436946
    iput-object p1, p0, LX/95X;->c:LX/2jy;

    .line 1436947
    iput-object p2, p0, LX/95X;->a:LX/0Or;

    .line 1436948
    new-instance v0, LX/95V;

    invoke-direct {v0, p0}, LX/95V;-><init>(LX/95X;)V

    invoke-interface {p1, v0}, LX/2jy;->a(LX/2kJ;)V

    .line 1436949
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/95X;Ljava/lang/Object;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEdge;)V"
        }
    .end annotation

    .prologue
    .line 1436950
    monitor-enter p0

    :try_start_0
    instance-of v2, p1, LX/0jT;

    if-nez v2, :cond_0

    .line 1436951
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Consistency resolution works only with GraphQLVisitableModel"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436952
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1436953
    :cond_0
    :try_start_1
    iget-object v2, p0, LX/95X;->b:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1436954
    :goto_0
    monitor-exit p0

    return-void

    .line 1436955
    :cond_1
    :try_start_2
    iget-object v2, p0, LX/95X;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1My;

    .line 1436956
    move-object v0, p1

    check-cast v0, LX/0jT;

    move-object v4, v0

    sget-object v5, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v6, 0x0

    invoke-static {p1}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    .line 1436957
    new-instance v4, LX/95W;

    invoke-direct {v4, p0, p1}, LX/95W;-><init>(LX/95X;Ljava/lang/Object;)V

    const-string v5, ""

    invoke-virtual {v3, v4, v5, v2}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1436958
    iget-object v2, p0, LX/95X;->b:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized b$redex0(LX/95X;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEdge;)V"
        }
    .end annotation

    .prologue
    .line 1436959
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/95X;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    .line 1436960
    if-eqz v0, :cond_0

    .line 1436961
    invoke-virtual {v0}, LX/1My;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436962
    :cond_0
    monitor-exit p0

    return-void

    .line 1436963
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 1436964
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/95X;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    .line 1436965
    invoke-virtual {v0}, LX/1My;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1436966
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1436967
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/95X;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1436968
    monitor-exit p0

    return-void
.end method
