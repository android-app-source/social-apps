.class public final LX/8kr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 0

    .prologue
    .line 1396871
    iput-object p1, p0, LX/8kr;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0xd78d23a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1396872
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1396873
    const-string v2, "com.facebook.orca.stickers.DOWNLOAD_QUEUED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1396874
    const-string v0, "stickerPack"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1396875
    iget-object v2, p0, LX/8kr;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1396876
    iget-object p0, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1396877
    iput-object v0, v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->u:Ljava/lang/String;

    .line 1396878
    :cond_0
    :goto_0
    const v0, 0x34b69208

    invoke-static {v0, v1}, LX/02F;->e(II)V

    return-void

    .line 1396879
    :cond_1
    const-string v2, "com.facebook.orca.stickers.STICKER_CONFIG_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1396880
    iget-object v0, p0, LX/8kr;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->y:LX/8kj;

    invoke-virtual {v0}, LX/8kj;->a()V

    .line 1396881
    iget-object v0, p0, LX/8kr;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-static {v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g$redex0(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    goto :goto_0
.end method
