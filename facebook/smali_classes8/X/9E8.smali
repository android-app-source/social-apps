.class public LX/9E8;
.super LX/99a;
.source ""

# interfaces
.implements LX/21l;
.implements LX/1Cw;


# instance fields
.field private final a:LX/9Di;

.field private final b:LX/9FA;

.field private final c:LX/0Sh;

.field private final d:LX/03V;

.field private final e:LX/9Dz;


# direct methods
.method public constructor <init>(LX/1Qq;LX/9Di;LX/9FA;LX/0Sh;LX/03V;LX/9Dz;)V
    .locals 0
    .param p1    # LX/1Qq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9Di;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9FA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1456540
    invoke-direct {p0, p1}, LX/99a;-><init>(LX/1Qq;)V

    .line 1456541
    iput-object p2, p0, LX/9E8;->a:LX/9Di;

    .line 1456542
    iput-object p3, p0, LX/9E8;->b:LX/9FA;

    .line 1456543
    iput-object p4, p0, LX/9E8;->c:LX/0Sh;

    .line 1456544
    iput-object p5, p0, LX/9E8;->d:LX/03V;

    .line 1456545
    iput-object p6, p0, LX/9E8;->e:LX/9Dz;

    .line 1456546
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 2

    .prologue
    .line 1456535
    invoke-virtual {p0, p1}, LX/99a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    .line 1456536
    iget-object v1, v0, LX/1Rk;->e:LX/0jW;

    move-object v1, v1

    .line 1456537
    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v1, :cond_0

    .line 1456538
    iget-object v1, v0, LX/1Rk;->e:LX/0jW;

    move-object v0, v1

    .line 1456539
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 1

    .prologue
    .line 1456547
    iget-object v0, p0, LX/9E8;->b:LX/9FA;

    invoke-virtual {v0, p1}, LX/9FA;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1456548
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1456523
    iget-object v0, p0, LX/9E8;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1456524
    iget-object v0, p0, LX/9E8;->d:LX/03V;

    const-string v1, "MultiRowCommentAdapter"

    const-string v2, "bind called on non-UI thread"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1456525
    :cond_0
    iget-object v0, p0, LX/9E8;->a:LX/9Di;

    invoke-virtual {v0, p1}, LX/9Di;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1456526
    iget-object v0, p0, LX/9E8;->e:LX/9Dz;

    .line 1456527
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, v0, LX/9Dz;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1456528
    iget-object v2, v0, LX/9Dz;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 1456529
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/ValueAnimator;

    .line 1456530
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1456531
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1456532
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    goto :goto_0

    .line 1456533
    :cond_1
    invoke-virtual {p0}, LX/99a;->notifyDataSetChanged()V

    .line 1456534
    return-void
.end method

.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 1

    .prologue
    .line 1456518
    iget-object v0, p0, LX/9E8;->b:LX/9FA;

    .line 1456519
    iput-object p1, v0, LX/9FA;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1456520
    iget-object p0, v0, LX/9FA;->e:LX/9Cd;

    .line 1456521
    iput-object p1, p0, LX/9Cd;->B:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1456522
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1456517
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, LX/9E8;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456510
    invoke-super {p0, p1, p2, p3}, LX/99a;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1456511
    invoke-virtual {p0, p1}, LX/9E8;->a(I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1456512
    if-nez v0, :cond_1

    .line 1456513
    :cond_0
    :goto_0
    return-object v1

    .line 1456514
    :cond_1
    iget-object v2, p0, LX/9E8;->e:LX/9Dz;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/9Dz;->a(Ljava/lang/String;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 1456515
    if-eqz v2, :cond_0

    instance-of v0, v1, LX/3Ws;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1456516
    check-cast v0, LX/3Ws;

    invoke-interface {v0, v2}, LX/3Ws;->a(Landroid/animation/ValueAnimator;)V

    goto :goto_0
.end method
