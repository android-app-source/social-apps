.class public LX/ASA;
.super LX/4or;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:LX/0aG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1673701
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1673702
    const-class v0, LX/ASA;

    invoke-static {v0, p0, p1}, LX/ASA;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1673703
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1673704
    new-array v2, v4, [LX/AS4;

    sget-object v0, LX/AS4;->FEED_ONLY_POST_NUX:LX/AS4;

    aput-object v0, v2, v1

    .line 1673705
    new-array v3, v4, [Ljava/lang/String;

    .line 1673706
    new-array v4, v4, [Ljava/lang/String;

    move v0, v1

    .line 1673707
    :goto_0
    if-gtz v0, :cond_0

    .line 1673708
    new-instance v5, Ljava/lang/StringBuilder;

    const-string p1, "Reset "

    invoke-direct {v5, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object p1, v2, v1

    iget-object p1, p1, LX/AS4;->description:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    .line 1673709
    aget-object v5, v2, v1

    invoke-virtual {v5}, LX/AS4;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 1673710
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1673711
    :cond_0
    const-string v0, "Composer Tip & Nux Settings"

    invoke-virtual {p0, v0}, LX/ASA;->setTitle(Ljava/lang/CharSequence;)V

    .line 1673712
    const-string v0, "List of Tip & Nux settings available to configure (on device only)."

    invoke-virtual {p0, v0}, LX/ASA;->setSummary(Ljava/lang/CharSequence;)V

    .line 1673713
    invoke-virtual {p0, v3}, LX/ASA;->setEntries([Ljava/lang/CharSequence;)V

    .line 1673714
    invoke-virtual {p0, v4}, LX/ASA;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1673715
    const-string v0, "Ignore the radio buttons. Options are not mutually exclusive."

    invoke-virtual {p0, v0}, LX/ASA;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 1673716
    const-string v0, "Ok"

    invoke-virtual {p0, v0}, LX/ASA;->setPositiveButtonText(Ljava/lang/CharSequence;)V

    .line 1673717
    const-string v0, "Cancel"

    invoke-virtual {p0, v0}, LX/ASA;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 1673718
    const-string v0, "ComposerTipPreference"

    invoke-virtual {p0, v0}, LX/ASA;->setKey(Ljava/lang/String;)V

    .line 1673719
    new-instance v0, LX/AS9;

    invoke-direct {v0, p0}, LX/AS9;-><init>(LX/ASA;)V

    invoke-virtual {p0, v0}, LX/ASA;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1673720
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/ASA;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object p0

    check-cast p0, LX/0aG;

    iput-object v0, p1, LX/ASA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, p1, LX/ASA;->b:LX/0aG;

    return-void
.end method
