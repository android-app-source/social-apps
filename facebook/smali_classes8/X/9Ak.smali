.class public LX/9Ak;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9Ak;


# instance fields
.field public a:LX/0uf;


# direct methods
.method public constructor <init>(LX/0uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1450433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1450434
    iput-object p1, p0, LX/9Ak;->a:LX/0uf;

    .line 1450435
    return-void
.end method

.method public static a(LX/0QB;)LX/9Ak;
    .locals 4

    .prologue
    .line 1450436
    sget-object v0, LX/9Ak;->b:LX/9Ak;

    if-nez v0, :cond_1

    .line 1450437
    const-class v1, LX/9Ak;

    monitor-enter v1

    .line 1450438
    :try_start_0
    sget-object v0, LX/9Ak;->b:LX/9Ak;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1450439
    if-eqz v2, :cond_0

    .line 1450440
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1450441
    new-instance p0, LX/9Ak;

    invoke-static {v0}, LX/0ts;->a(LX/0QB;)LX/0uf;

    move-result-object v3

    check-cast v3, LX/0uf;

    invoke-direct {p0, v3}, LX/9Ak;-><init>(LX/0uf;)V

    .line 1450442
    move-object v0, p0

    .line 1450443
    sput-object v0, LX/9Ak;->b:LX/9Ak;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1450444
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1450445
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1450446
    :cond_1
    sget-object v0, LX/9Ak;->b:LX/9Ak;

    return-object v0

    .line 1450447
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1450448
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)I
    .locals 6

    .prologue
    .line 1450449
    iget-object v0, p0, LX/9Ak;->a:LX/0uf;

    sget-wide v2, LX/0X5;->dc:J

    int-to-long v4, p1

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0uf;->a(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method
