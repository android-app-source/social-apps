.class public LX/AJt;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/AJZ;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/AJq;


# direct methods
.method public constructor <init>(LX/AJZ;LX/0Or;)V
    .locals 0
    .param p1    # LX/AJZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AJZ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1662334
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1662335
    iput-object p1, p0, LX/AJt;->a:LX/AJZ;

    .line 1662336
    iput-object p2, p0, LX/AJt;->b:LX/0Or;

    .line 1662337
    return-void
.end method

.method private f(I)I
    .locals 1

    .prologue
    .line 1662338
    invoke-direct {p0}, LX/AJt;->h()I

    move-result v0

    sub-int v0, p1, v0

    return v0
.end method

.method private h()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1662347
    iget-object v0, p0, LX/AJt;->a:LX/AJZ;

    .line 1662348
    iget-boolean v3, v0, LX/AJZ;->e:Z

    move v0, v3

    .line 1662349
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, LX/AJt;->a:LX/AJZ;

    .line 1662350
    iget-boolean p0, v3, LX/AJZ;->f:Z

    move v3, p0

    .line 1662351
    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1662339
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1662340
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1662341
    if-nez p2, :cond_0

    .line 1662342
    const v1, 0x7f031311

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1662343
    new-instance v0, LX/AJn;

    invoke-direct {v0, v1}, LX/AJn;-><init>(Landroid/view/View;)V

    .line 1662344
    :goto_0
    return-object v0

    .line 1662345
    :cond_0
    const v1, 0x7f031310

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1662346
    new-instance v0, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;

    invoke-direct {v0, v1}, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1662304
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1662305
    if-nez v0, :cond_2

    .line 1662306
    check-cast p1, LX/AJn;

    .line 1662307
    const v0, 0x7f020943

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 p0, 0x0

    .line 1662308
    iget-object v1, p1, LX/AJn;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1662309
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScaleX()F

    move-result v1

    cmpl-float v1, v1, p0

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScaleY()F

    move-result v1

    cmpl-float v1, v1, p0

    if-nez v1, :cond_1

    .line 1662310
    :cond_0
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, p2}, Landroid/view/View;->setScaleX(F)V

    .line 1662311
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, p2}, Landroid/view/View;->setScaleY(F)V

    .line 1662312
    :cond_1
    :goto_0
    return-void

    .line 1662313
    :cond_2
    check-cast p1, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;

    .line 1662314
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1662315
    iget-object v0, p0, LX/AJt;->a:LX/AJZ;

    invoke-direct {p0, p2}, LX/AJt;->f(I)I

    move-result v1

    const/4 p0, 0x0

    .line 1662316
    iget-object v2, v0, LX/AJZ;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_4

    move-object v2, p0

    .line 1662317
    :goto_1
    move-object v0, v2

    .line 1662318
    invoke-virtual {p1, v0}, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1662319
    :cond_3
    iget-object v0, p0, LX/AJt;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1662320
    :cond_4
    iget-object v2, v0, LX/AJZ;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1662321
    iget-object p2, v0, LX/AJZ;->a:Ljava/util/Map;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/audience/model/AudienceControlData;

    .line 1662322
    if-nez v2, :cond_5

    move-object v2, p0

    .line 1662323
    goto :goto_1

    .line 1662324
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_6

    .line 1662325
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1662326
    :cond_6
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getLowResProfileUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1662327
    iget-object v0, p0, LX/AJt;->a:LX/AJZ;

    .line 1662328
    iget-boolean v2, v0, LX/AJZ;->e:Z

    move v0, v2

    .line 1662329
    if-eqz v0, :cond_0

    .line 1662330
    invoke-virtual {p0, v1}, LX/1OM;->j_(I)V

    .line 1662331
    :goto_0
    iget-object v0, p0, LX/AJt;->c:LX/AJq;

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    invoke-interface {v0, v1}, LX/AJq;->a(I)V

    .line 1662332
    return-void

    .line 1662333
    :cond_0
    invoke-virtual {p0, v1}, LX/1OM;->d(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1662292
    iget-object v2, p0, LX/AJt;->a:LX/AJZ;

    .line 1662293
    iget-boolean v3, v2, LX/AJZ;->f:Z

    move v2, v3

    .line 1662294
    if-eqz v2, :cond_1

    .line 1662295
    iget-object v2, p0, LX/AJt;->a:LX/AJZ;

    .line 1662296
    iget-boolean v3, v2, LX/AJZ;->e:Z

    move v2, v3

    .line 1662297
    if-eqz v2, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, LX/1OM;->j_(I)V

    .line 1662298
    :goto_1
    iget-object v0, p0, LX/AJt;->c:LX/AJq;

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    invoke-interface {v0, v1}, LX/AJq;->a(I)V

    .line 1662299
    return-void

    :cond_0
    move v0, v1

    .line 1662300
    goto :goto_0

    .line 1662301
    :cond_1
    iget-object v2, p0, LX/AJt;->a:LX/AJZ;

    .line 1662302
    iget-boolean v3, v2, LX/AJZ;->e:Z

    move v2, v3

    .line 1662303
    if-eqz v2, :cond_2

    :goto_2
    invoke-virtual {p0, v0}, LX/1OM;->d(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 1662289
    invoke-direct {p0}, LX/AJt;->h()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, LX/1OM;->d(I)V

    .line 1662290
    iget-object v0, p0, LX/AJt;->c:LX/AJq;

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    invoke-interface {v0, v1}, LX/AJq;->a(I)V

    .line 1662291
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1662286
    invoke-direct {p0}, LX/AJt;->h()I

    move-result v0

    invoke-virtual {p0, v0}, LX/1OM;->j_(I)V

    .line 1662287
    iget-object v0, p0, LX/AJt;->c:LX/AJq;

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    invoke-interface {v0, v1}, LX/AJq;->a(I)V

    .line 1662288
    return-void
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 1662279
    iget-object v0, p0, LX/AJt;->a:LX/AJZ;

    .line 1662280
    iget-object v1, v0, LX/AJZ;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    move v0, v1

    .line 1662281
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJt;->a:LX/AJZ;

    .line 1662282
    iget-boolean v1, v0, LX/AJZ;->e:Z

    move v0, v1

    .line 1662283
    if-nez v0, :cond_0

    iget-object v0, p0, LX/AJt;->a:LX/AJZ;

    .line 1662284
    iget-boolean v1, v0, LX/AJZ;->f:Z

    move v0, v1

    .line 1662285
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1662269
    iget-object v1, p0, LX/AJt;->a:LX/AJZ;

    .line 1662270
    iget-boolean v2, v1, LX/AJZ;->e:Z

    move v1, v2

    .line 1662271
    iget-object v2, p0, LX/AJt;->a:LX/AJZ;

    .line 1662272
    iget-boolean p0, v2, LX/AJZ;->f:Z

    move v2, p0

    .line 1662273
    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 1662274
    const/4 v0, 0x0

    .line 1662275
    :cond_0
    :goto_0
    return v0

    .line 1662276
    :cond_1
    if-nez v1, :cond_2

    if-eqz v2, :cond_2

    if-eqz p1, :cond_0

    .line 1662277
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    if-eq p1, v0, :cond_0

    .line 1662278
    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1662266
    iget-object v0, p0, LX/AJt;->a:LX/AJZ;

    .line 1662267
    iget-object v1, v0, LX/AJZ;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    move v0, v1

    .line 1662268
    invoke-direct {p0}, LX/AJt;->h()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
