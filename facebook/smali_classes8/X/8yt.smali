.class public LX/8yt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:Landroid/graphics/Typeface;

.field public static final d:Landroid/graphics/Typeface;

.field public static final e:Landroid/text/Layout$Alignment;

.field private static final h:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/5Ph;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1426602
    const-string v0, "#FF141823"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/8yt;->a:I

    .line 1426603
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    sput v0, LX/8yt;->b:I

    .line 1426604
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 1426605
    sput-object v0, LX/8yt;->c:Landroid/graphics/Typeface;

    sput-object v0, LX/8yt;->d:Landroid/graphics/Typeface;

    .line 1426606
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/8yt;->e:Landroid/text/Layout$Alignment;

    .line 1426607
    new-instance v0, LX/8yp;

    invoke-direct {v0}, LX/8yp;-><init>()V

    sput-object v0, LX/8yt;->h:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1426608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426609
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1426610
    iput-object v0, p0, LX/8yt;->f:LX/0Ot;

    .line 1426611
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1426612
    iput-object v0, p0, LX/8yt;->g:LX/0Ot;

    .line 1426613
    return-void
.end method

.method public static a(LX/0QB;)LX/8yt;
    .locals 5

    .prologue
    .line 1426614
    const-class v1, LX/8yt;

    monitor-enter v1

    .line 1426615
    :try_start_0
    sget-object v0, LX/8yt;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1426616
    sput-object v2, LX/8yt;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1426617
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1426618
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1426619
    new-instance v3, LX/8yt;

    invoke-direct {v3}, LX/8yt;-><init>()V

    .line 1426620
    const/16 v4, 0x2eb

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xb19

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1426621
    iput-object v4, v3, LX/8yt;->f:LX/0Ot;

    iput-object p0, v3, LX/8yt;->g:LX/0Ot;

    .line 1426622
    move-object v0, v3

    .line 1426623
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1426624
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8yt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1426625
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1426626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/8yt;LX/3Ab;LX/8yr;ZLX/HJI;)Ljava/lang/CharSequence;
    .locals 11

    .prologue
    .line 1426627
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1426628
    invoke-interface {p1}, LX/3Ab;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ph;

    .line 1426629
    :try_start_0
    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/1yL;

    invoke-interface {v0}, LX/5Ph;->c()I

    move-result v7

    invoke-interface {v0}, LX/5Ph;->b()I

    move-result v8

    invoke-direct {v6, v7, v8}, LX/1yL;-><init>(II)V

    invoke-static {v5, v6}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1426630
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1426631
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    iget v8, p2, LX/8yr;->b:I

    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1426632
    new-instance v7, Landroid/text/style/AbsoluteSizeSpan;

    iget v8, p2, LX/8yr;->c:I

    invoke-direct {v7, v8}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1426633
    new-instance v7, LX/7Gs;

    const/4 v8, 0x0

    iget-object v9, p2, LX/8yr;->a:Landroid/graphics/Typeface;

    invoke-direct {v7, v8, v9}, LX/7Gs;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1426634
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 1426635
    iget v8, v5, LX/1yN;->a:I

    move v8, v8

    .line 1426636
    invoke-virtual {v5}, LX/1yN;->c()I

    move-result v9

    const/16 v10, 0x12

    invoke-interface {v2, v7, v8, v9, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 1426637
    :cond_0
    if-eqz p3, :cond_1

    .line 1426638
    const/4 v7, 0x0

    .line 1426639
    if-eqz p4, :cond_3

    .line 1426640
    new-instance v6, LX/8yq;

    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v7

    invoke-direct {v6, p4, v7}, LX/8yq;-><init>(LX/HJI;LX/1yA;)V

    .line 1426641
    :goto_2
    if-eqz v6, :cond_1

    .line 1426642
    iget v7, v5, LX/1yN;->a:I

    move v7, v7

    .line 1426643
    invoke-virtual {v5}, LX/1yN;->c()I

    move-result v8

    const/16 v9, 0x12

    invoke-interface {v2, v6, v7, v8, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1426644
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1426645
    :catch_0
    move-exception v0

    .line 1426646
    const-class v5, LX/8yt;

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1426647
    :cond_2
    return-object v2

    .line 1426648
    :cond_3
    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 1426649
    iget-object v6, p0, LX/8yt;->g:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1nG;

    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v8

    .line 1426650
    if-eqz v8, :cond_4

    .line 1426651
    new-instance v6, LX/8ys;

    invoke-direct {v6, p0, v8}, LX/8ys;-><init>(LX/8yt;Ljava/lang/String;)V

    goto :goto_2

    .line 1426652
    :cond_4
    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v6

    invoke-interface {v6}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 1426653
    new-instance v6, LX/8ys;

    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v7

    invoke-interface {v7}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, v7}, LX/8ys;-><init>(LX/8yt;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move-object v6, v7

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/1De;LX/3Ab;LX/HJI;Landroid/text/TextUtils$TruncateAt;ZLandroid/text/Layout$Alignment;IIZFIIIIIILandroid/graphics/Typeface;Landroid/graphics/Typeface;IIII)LX/1Dg;
    .locals 7
    .param p2    # LX/3Ab;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/HJI;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Landroid/text/TextUtils$TruncateAt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p6    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p17    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p18    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p19    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p20    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p21    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p22    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param

    .prologue
    .line 1426654
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1426655
    invoke-interface {p2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1426656
    if-nez p15, :cond_2

    .line 1426657
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1426658
    :goto_0
    if-nez p16, :cond_0

    move/from16 p16, v3

    .line 1426659
    :cond_0
    move-object/from16 v0, p18

    move-object/from16 v1, p17

    if-ne v0, v1, :cond_1

    .line 1426660
    move-object/from16 v0, p18

    move/from16 v1, p14

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object p18

    .line 1426661
    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    new-instance v5, LX/8yr;

    move-object/from16 v0, p18

    move/from16 v1, p12

    move/from16 v2, p16

    invoke-direct {v5, v0, v1, v2}, LX/8yr;-><init>(Landroid/graphics/Typeface;II)V

    move/from16 v0, p9

    invoke-static {p0, p2, v5, v0, p3}, LX/8yt;->a(LX/8yt;LX/3Ab;LX/8yr;ZLX/HJI;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->p(I)LX/1ne;

    move-result-object v3

    move/from16 v0, p13

    invoke-virtual {v3, v0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    move-object/from16 v0, p17

    invoke-virtual {v3, v0}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p7}, LX/1ne;->i(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    move/from16 v0, p11

    invoke-virtual {v3, v0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    move/from16 v0, p10

    invoke-virtual {v3, v0}, LX/1ne;->j(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    move/from16 v0, p20

    int-to-float v4, v0

    invoke-virtual {v3, v4}, LX/1ne;->d(F)LX/1ne;

    move-result-object v3

    move/from16 v0, p21

    int-to-float v4, v0

    invoke-virtual {v3, v4}, LX/1ne;->e(F)LX/1ne;

    move-result-object v3

    move/from16 v0, p19

    int-to-float v4, v0

    invoke-virtual {v3, v4}, LX/1ne;->c(F)LX/1ne;

    move-result-object v3

    move/from16 v0, p22

    invoke-virtual {v3, v0}, LX/1ne;->k(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    return-object v3

    :cond_2
    move/from16 v3, p15

    goto :goto_0
.end method
