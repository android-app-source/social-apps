.class public final LX/8po;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/8pq;


# direct methods
.method public constructor <init>(LX/8pq;LX/0TF;)V
    .locals 0

    .prologue
    .line 1406436
    iput-object p1, p0, LX/8po;->b:LX/8pq;

    iput-object p2, p0, LX/8po;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1406437
    iget-object v0, p0, LX/8po;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1406438
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1406439
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1406440
    if-eqz p1, :cond_0

    .line 1406441
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406442
    if-nez v0, :cond_1

    .line 1406443
    :cond_0
    iget-object v0, p0, LX/8po;->a:LX/0TF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1406444
    :goto_0
    return-void

    .line 1406445
    :cond_1
    iget-object v1, p0, LX/8po;->a:LX/0TF;

    .line 1406446
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406447
    check-cast v0, Ljava/util/List;

    invoke-static {v0}, LX/8pq;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
