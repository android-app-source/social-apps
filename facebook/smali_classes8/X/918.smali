.class public LX/918;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/918;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430155
    iput-object p1, p0, LX/918;->a:LX/0Zb;

    .line 1430156
    return-void
.end method

.method public static a(LX/0QB;)LX/918;
    .locals 4

    .prologue
    .line 1430157
    sget-object v0, LX/918;->b:LX/918;

    if-nez v0, :cond_1

    .line 1430158
    const-class v1, LX/918;

    monitor-enter v1

    .line 1430159
    :try_start_0
    sget-object v0, LX/918;->b:LX/918;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1430160
    if-eqz v2, :cond_0

    .line 1430161
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1430162
    new-instance p0, LX/918;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/918;-><init>(LX/0Zb;)V

    .line 1430163
    move-object v0, p0

    .line 1430164
    sput-object v0, LX/918;->b:LX/918;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430165
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1430166
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1430167
    :cond_1
    sget-object v0, LX/918;->b:LX/918;

    return-object v0

    .line 1430168
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1430169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1430170
    new-instance v0, LX/8yQ;

    const-string v1, "activities_selector"

    invoke-direct {v0, p0, p1, v1}, LX/8yQ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430171
    return-object v0
.end method


# virtual methods
.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1430172
    const-string v0, "activities_selector_objects_load_failed"

    invoke-static {v0, p1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    .line 1430173
    iget-object v1, v0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 1430174
    iget-object v1, p0, LX/918;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1430175
    return-void
.end method
