.class public LX/AOn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1FZ;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/68w;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/68w;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/68w;

.field private final e:LX/68w;

.field private final f:LX/68w;

.field private final g:LX/68w;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1FZ;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1669473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1669474
    iput-object p2, p0, LX/AOn;->a:LX/1FZ;

    .line 1669475
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AOn;->b:Ljava/util/Map;

    .line 1669476
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AOn;->c:Ljava/util/Map;

    .line 1669477
    invoke-static {p1}, LX/690;->a(Landroid/content/Context;)V

    .line 1669478
    const v0, 0x7f021645

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    iput-object v0, p0, LX/AOn;->d:LX/68w;

    .line 1669479
    const v0, 0x7f02164f

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    iput-object v0, p0, LX/AOn;->e:LX/68w;

    .line 1669480
    const v0, 0x7f021644

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    iput-object v0, p0, LX/AOn;->f:LX/68w;

    .line 1669481
    const v0, 0x7f02164e

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    iput-object v0, p0, LX/AOn;->g:LX/68w;

    .line 1669482
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFLX/1FZ;)LX/68w;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1669395
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-virtual {p4, v0, v1, v2}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1669396
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1669397
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1669398
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v1, p1, p2, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1669399
    invoke-static {v0}, LX/690;->a(Landroid/graphics/Bitmap;)LX/68w;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/AOn;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1669412
    const/4 v0, 0x0

    .line 1669413
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1669414
    :goto_1
    move v0, v0

    .line 1669415
    if-nez v0, :cond_1

    .line 1669416
    iget-object v0, p0, LX/AOn;->b:Ljava/util/Map;

    iget-object v1, p0, LX/AOn;->d:LX/68w;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1669417
    iget-object v0, p0, LX/AOn;->c:Ljava/util/Map;

    iget-object v1, p0, LX/AOn;->e:LX/68w;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1669418
    :goto_2
    return-void

    .line 1669419
    :cond_1
    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    .line 1669420
    if-nez v0, :cond_2

    .line 1669421
    iget-object v0, p0, LX/AOn;->b:Ljava/util/Map;

    iget-object v1, p0, LX/AOn;->d:LX/68w;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1669422
    iget-object v0, p0, LX/AOn;->c:Ljava/util/Map;

    iget-object v1, p0, LX/AOn;->e:LX/68w;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1669423
    :cond_2
    iget-object v1, p0, LX/AOn;->b:Ljava/util/Map;

    iget-object v2, p0, LX/AOn;->f:LX/68w;

    .line 1669424
    iget-object v3, v2, LX/68w;->a:Landroid/graphics/Bitmap;

    move-object v2, v3

    .line 1669425
    iget-object v3, v0, LX/68w;->a:Landroid/graphics/Bitmap;

    move-object v3, v3

    .line 1669426
    iget-object v4, p0, LX/AOn;->a:LX/1FZ;

    .line 1669427
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    .line 1669428
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    .line 1669429
    invoke-static {v2, v3, v5, v6, v4}, LX/AOn;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFLX/1FZ;)LX/68w;

    move-result-object v5

    move-object v2, v5

    .line 1669430
    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1669431
    iget-object v1, p0, LX/AOn;->c:Ljava/util/Map;

    iget-object v2, p0, LX/AOn;->g:LX/68w;

    .line 1669432
    iget-object v3, v2, LX/68w;->a:Landroid/graphics/Bitmap;

    move-object v2, v3

    .line 1669433
    iget-object v3, v0, LX/68w;->a:Landroid/graphics/Bitmap;

    move-object v0, v3

    .line 1669434
    iget-object v3, p0, LX/AOn;->a:LX/1FZ;

    .line 1669435
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 1669436
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40200000    # 2.5f

    div-float/2addr v5, v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    .line 1669437
    invoke-static {v2, v0, v4, v5, v3}, LX/AOn;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFLX/1FZ;)LX/68w;

    move-result-object v4

    move-object v0, v4

    .line 1669438
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1669439
    :sswitch_0
    const-string v2, "activity-recreation"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto/16 :goto_0

    :sswitch_1
    const-string v2, "airport"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0

    :sswitch_2
    const-string v2, "airport-terminal"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto/16 :goto_0

    :sswitch_3
    const-string v2, "arts"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto/16 :goto_0

    :sswitch_4
    const-string v2, "bank"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto/16 :goto_0

    :sswitch_5
    const-string v2, "bar-beergarden"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto/16 :goto_0

    :sswitch_6
    const-string v2, "breakfast-brunch"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto/16 :goto_0

    :sswitch_7
    const-string v2, "burgers"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto/16 :goto_0

    :sswitch_8
    const-string v2, "chinese"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto/16 :goto_0

    :sswitch_9
    const-string v2, "city"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x9

    goto/16 :goto_0

    :sswitch_a
    const-string v2, "cocktail-nightlife"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xa

    goto/16 :goto_0

    :sswitch_b
    const-string v2, "coffee"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v2, "deli-sandwich"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "delivery-takeaway"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "dessert"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "entertainment"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v2, "fastfood"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "home"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v2, "hotel"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v2, "italian"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v2, "lunch"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v2, "mexican"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v2, "outdoor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v2, "pizza"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v2, "professional-services"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v2, "ramen"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v2, "region"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v2, "restaurant"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v2, "shopping"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v2, "steak"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v2, "sushi"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v2, "thai"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v2, "winebar"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x20

    goto/16 :goto_0

    .line 1669440
    :pswitch_0
    const v0, 0x7f021635

    goto/16 :goto_1

    .line 1669441
    :pswitch_1
    const v0, 0x7f021636

    goto/16 :goto_1

    .line 1669442
    :pswitch_2
    const v0, 0x7f021637

    goto/16 :goto_1

    .line 1669443
    :pswitch_3
    const v0, 0x7f021638

    goto/16 :goto_1

    .line 1669444
    :pswitch_4
    const v0, 0x7f021639

    goto/16 :goto_1

    .line 1669445
    :pswitch_5
    const v0, 0x7f02163a

    goto/16 :goto_1

    .line 1669446
    :pswitch_6
    const v0, 0x7f02163b

    goto/16 :goto_1

    .line 1669447
    :pswitch_7
    const v0, 0x7f02163c

    goto/16 :goto_1

    .line 1669448
    :pswitch_8
    const v0, 0x7f02163d

    goto/16 :goto_1

    .line 1669449
    :pswitch_9
    const v0, 0x7f02163e

    goto/16 :goto_1

    .line 1669450
    :pswitch_a
    const v0, 0x7f02163f

    goto/16 :goto_1

    .line 1669451
    :pswitch_b
    const v0, 0x7f021640

    goto/16 :goto_1

    .line 1669452
    :pswitch_c
    const v0, 0x7f021641

    goto/16 :goto_1

    .line 1669453
    :pswitch_d
    const v0, 0x7f021642

    goto/16 :goto_1

    .line 1669454
    :pswitch_e
    const v0, 0x7f021643

    goto/16 :goto_1

    .line 1669455
    :pswitch_f
    const v0, 0x7f021646

    goto/16 :goto_1

    .line 1669456
    :pswitch_10
    const v0, 0x7f021647

    goto/16 :goto_1

    .line 1669457
    :pswitch_11
    const v0, 0x7f021648

    goto/16 :goto_1

    .line 1669458
    :pswitch_12
    const v0, 0x7f021649

    goto/16 :goto_1

    .line 1669459
    :pswitch_13
    const v0, 0x7f02164a

    goto/16 :goto_1

    .line 1669460
    :pswitch_14
    const v0, 0x7f02164b

    goto/16 :goto_1

    .line 1669461
    :pswitch_15
    const v0, 0x7f02164c

    goto/16 :goto_1

    .line 1669462
    :pswitch_16
    const v0, 0x7f02164d

    goto/16 :goto_1

    .line 1669463
    :pswitch_17
    const v0, 0x7f021650

    goto/16 :goto_1

    .line 1669464
    :pswitch_18
    const v0, 0x7f021651

    goto/16 :goto_1

    .line 1669465
    :pswitch_19
    const v0, 0x7f021652

    goto/16 :goto_1

    .line 1669466
    :pswitch_1a
    const v0, 0x7f021653

    goto/16 :goto_1

    .line 1669467
    :pswitch_1b
    const v0, 0x7f021654

    goto/16 :goto_1

    .line 1669468
    :pswitch_1c
    const v0, 0x7f021655

    goto/16 :goto_1

    .line 1669469
    :pswitch_1d
    const v0, 0x7f021656

    goto/16 :goto_1

    .line 1669470
    :pswitch_1e
    const v0, 0x7f021657

    goto/16 :goto_1

    .line 1669471
    :pswitch_1f
    const v0, 0x7f021658

    goto/16 :goto_1

    .line 1669472
    :pswitch_20
    const v0, 0x7f021659

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69a5b4c3 -> :sswitch_1b
        -0x6523aec0 -> :sswitch_6
        -0x56601c70 -> :sswitch_0
        -0x50c42034 -> :sswitch_b
        -0x4d5ab3a1 -> :sswitch_5
        -0x4ae6ea52 -> :sswitch_d
        -0x44f7db55 -> :sswitch_a
        -0x42475796 -> :sswitch_c
        -0x41f38404 -> :sswitch_16
        -0x3b1ba335 -> :sswitch_1
        -0x37b7d90c -> :sswitch_1a
        -0x19ea0ebc -> :sswitch_18
        -0x14880e98 -> :sswitch_1c
        0x2dd270 -> :sswitch_3
        0x2e063c -> :sswitch_4
        0x2e996b -> :sswitch_9
        0x30f4df -> :sswitch_11
        0x364d9c -> :sswitch_1f
        0x5edc1b4 -> :sswitch_12
        0x628c32a -> :sswitch_14
        0x65bdc88 -> :sswitch_17
        0x6743547 -> :sswitch_19
        0x68ad14e -> :sswitch_1d
        0x68b7b12 -> :sswitch_1e
        0xe40829e -> :sswitch_7
        0x1dcd7f88 -> :sswitch_f
        0x2c7c18ed -> :sswitch_8
        0x392cf0a7 -> :sswitch_15
        0x39c456ba -> :sswitch_10
        0x506fe36a -> :sswitch_20
        0x5cd2fb26 -> :sswitch_e
        0x601eed3e -> :sswitch_2
        0x7dea0c00 -> :sswitch_13
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/68w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669406
    if-nez p1, :cond_0

    .line 1669407
    iget-object v0, p0, LX/AOn;->d:LX/68w;

    .line 1669408
    :goto_0
    return-object v0

    .line 1669409
    :cond_0
    iget-object v0, p0, LX/AOn;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1669410
    invoke-static {p0, p1}, LX/AOn;->c(LX/AOn;Ljava/lang/String;)V

    .line 1669411
    :cond_1
    iget-object v0, p0, LX/AOn;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68w;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/68w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669400
    if-nez p1, :cond_0

    .line 1669401
    iget-object v0, p0, LX/AOn;->e:LX/68w;

    .line 1669402
    :goto_0
    return-object v0

    .line 1669403
    :cond_0
    iget-object v0, p0, LX/AOn;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1669404
    invoke-static {p0, p1}, LX/AOn;->c(LX/AOn;Ljava/lang/String;)V

    .line 1669405
    :cond_1
    iget-object v0, p0, LX/AOn;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68w;

    goto :goto_0
.end method
