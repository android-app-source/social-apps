.class public final LX/A5c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 0

    .prologue
    .line 1621626
    iput-object p1, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, -0x59966413

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1621627
    iget-object v1, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 1621628
    const v1, -0x3d8b19fd

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1621629
    :goto_0
    return-void

    .line 1621630
    :cond_0
    iget-object v1, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1621631
    iget-object v1, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    .line 1621632
    iget-object v1, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 1621633
    iget-object v1, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->requestFocus()Z

    .line 1621634
    iget-object v1, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v2, p0, LX/A5c;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v2, v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1621635
    iget-object v3, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1621636
    const v1, -0x2c8e099f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
