.class public final LX/98R;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1445759
    const/16 v17, 0x0

    .line 1445760
    const/16 v16, 0x0

    .line 1445761
    const/4 v15, 0x0

    .line 1445762
    const/4 v14, 0x0

    .line 1445763
    const/4 v13, 0x0

    .line 1445764
    const/4 v12, 0x0

    .line 1445765
    const/4 v11, 0x0

    .line 1445766
    const/4 v10, 0x0

    .line 1445767
    const/4 v9, 0x0

    .line 1445768
    const/4 v8, 0x0

    .line 1445769
    const/4 v7, 0x0

    .line 1445770
    const/4 v6, 0x0

    .line 1445771
    const/4 v5, 0x0

    .line 1445772
    const/4 v4, 0x0

    .line 1445773
    const/4 v3, 0x0

    .line 1445774
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 1445775
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1445776
    const/4 v3, 0x0

    .line 1445777
    :goto_0
    return v3

    .line 1445778
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1445779
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 1445780
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 1445781
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1445782
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 1445783
    const-string v19, "__type__"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    const-string v19, "__typename"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1445784
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v17

    goto :goto_1

    .line 1445785
    :cond_3
    const-string v19, "category"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1445786
    invoke-static/range {p0 .. p1}, LX/98A;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1445787
    :cond_4
    const-string v19, "city"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1445788
    invoke-static/range {p0 .. p1}, LX/989;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1445789
    :cond_5
    const-string v19, "fri"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1445790
    invoke-static/range {p0 .. p1}, LX/98F;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1445791
    :cond_6
    const-string v19, "mon"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1445792
    invoke-static/range {p0 .. p1}, LX/98G;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1445793
    :cond_7
    const-string v19, "parent_place"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1445794
    invoke-static/range {p0 .. p1}, LX/98M;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1445795
    :cond_8
    const-string v19, "photo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1445796
    invoke-static/range {p0 .. p1}, LX/98N;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1445797
    :cond_9
    const-string v19, "sat"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1445798
    invoke-static/range {p0 .. p1}, LX/98H;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1445799
    :cond_a
    const-string v19, "street"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 1445800
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1445801
    :cond_b
    const-string v19, "sun"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 1445802
    invoke-static/range {p0 .. p1}, LX/98I;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1445803
    :cond_c
    const-string v19, "text"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 1445804
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1445805
    :cond_d
    const-string v19, "thu"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 1445806
    invoke-static/range {p0 .. p1}, LX/98J;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1445807
    :cond_e
    const-string v19, "tue"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 1445808
    invoke-static/range {p0 .. p1}, LX/98K;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1445809
    :cond_f
    const-string v19, "wed"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 1445810
    invoke-static/range {p0 .. p1}, LX/98L;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1445811
    :cond_10
    const-string v19, "zip"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1445812
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1445813
    :cond_11
    const/16 v18, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1445814
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1445815
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1445816
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1445817
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1445818
    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1445819
    const/4 v13, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1445820
    const/4 v12, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1445821
    const/4 v11, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1445822
    const/16 v10, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1445823
    const/16 v9, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1445824
    const/16 v8, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1445825
    const/16 v7, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1445826
    const/16 v6, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1445827
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1445828
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1445829
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1445830
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1445831
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1445832
    if-eqz v0, :cond_0

    .line 1445833
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445834
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1445835
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445836
    if-eqz v0, :cond_1

    .line 1445837
    const-string v1, "category"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445838
    invoke-static {p0, v0, p2}, LX/98A;->a(LX/15i;ILX/0nX;)V

    .line 1445839
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445840
    if-eqz v0, :cond_2

    .line 1445841
    const-string v1, "city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445842
    invoke-static {p0, v0, p2, p3}, LX/989;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445843
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445844
    if-eqz v0, :cond_3

    .line 1445845
    const-string v1, "fri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445846
    invoke-static {p0, v0, p2, p3}, LX/98F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445847
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445848
    if-eqz v0, :cond_4

    .line 1445849
    const-string v1, "mon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445850
    invoke-static {p0, v0, p2, p3}, LX/98G;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445851
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445852
    if-eqz v0, :cond_5

    .line 1445853
    const-string v1, "parent_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445854
    invoke-static {p0, v0, p2}, LX/98M;->a(LX/15i;ILX/0nX;)V

    .line 1445855
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445856
    if-eqz v0, :cond_6

    .line 1445857
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445858
    invoke-static {p0, v0, p2, p3}, LX/98N;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445859
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445860
    if-eqz v0, :cond_7

    .line 1445861
    const-string v1, "sat"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445862
    invoke-static {p0, v0, p2, p3}, LX/98H;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445863
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445864
    if-eqz v0, :cond_8

    .line 1445865
    const-string v1, "street"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445866
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445867
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445868
    if-eqz v0, :cond_9

    .line 1445869
    const-string v1, "sun"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445870
    invoke-static {p0, v0, p2, p3}, LX/98I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445871
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445872
    if-eqz v0, :cond_a

    .line 1445873
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445874
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445875
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445876
    if-eqz v0, :cond_b

    .line 1445877
    const-string v1, "thu"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445878
    invoke-static {p0, v0, p2, p3}, LX/98J;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445879
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445880
    if-eqz v0, :cond_c

    .line 1445881
    const-string v1, "tue"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445882
    invoke-static {p0, v0, p2, p3}, LX/98K;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445883
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445884
    if-eqz v0, :cond_d

    .line 1445885
    const-string v1, "wed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445886
    invoke-static {p0, v0, p2, p3}, LX/98L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445887
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445888
    if-eqz v0, :cond_e

    .line 1445889
    const-string v1, "zip"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445890
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445891
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1445892
    return-void
.end method
