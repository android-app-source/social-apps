.class public abstract LX/A7F;
.super LX/1Cv;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/A7V;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/CharSequence;

.field public final c:LX/11S;

.field public final d:LX/1Uf;


# direct methods
.method public constructor <init>(LX/11S;LX/1Uf;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1625774
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1625775
    iput-object v0, p0, LX/A7F;->a:Ljava/util/List;

    .line 1625776
    iput-object v0, p0, LX/A7F;->b:Ljava/lang/CharSequence;

    .line 1625777
    iput-object p1, p0, LX/A7F;->c:LX/11S;

    .line 1625778
    iput-object p2, p0, LX/A7F;->d:LX/1Uf;

    .line 1625779
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1625765
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1625766
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1625767
    invoke-virtual {p0}, LX/A7F;->a()I

    move-result v1

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1625768
    new-instance v2, LX/A7X;

    invoke-direct {v2, p0}, LX/A7X;-><init>(LX/A7F;)V

    .line 1625769
    const v0, 0x7f0d0577

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LX/A7X;->a:Landroid/widget/TextView;

    .line 1625770
    const v0, 0x7f0d0578

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LX/A7X;->b:Landroid/widget/TextView;

    .line 1625771
    const v0, 0x7f0d1161

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LX/A7X;->c:Landroid/widget/TextView;

    .line 1625772
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1625773
    return-object v1
.end method

.method public a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 9

    .prologue
    .line 1625714
    check-cast p2, LX/A7V;

    .line 1625715
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A7X;

    .line 1625716
    iget-object v1, v0, LX/A7X;->b:Landroid/widget/TextView;

    const/4 p4, 0x1

    const/16 p1, 0x8

    const/4 v7, 0x0

    .line 1625717
    iget-object v2, p2, LX/A7V;->c:Ljava/lang/CharSequence;

    move-object v5, v2

    .line 1625718
    iget-object v2, p2, LX/A7V;->d:Ljava/lang/CharSequence;

    move-object v4, v2

    .line 1625719
    const v2, 0x7f0d0578

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1625720
    const v3, 0x7f0d1161

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1625721
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1625722
    const-string v4, ""

    .line 1625723
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1625724
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1625725
    iget-object v5, p2, LX/A7V;->a:Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;

    move-object v5, v5

    .line 1625726
    if-eqz v5, :cond_5

    .line 1625727
    invoke-virtual {v5}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->j()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    move-result-object v5

    .line 1625728
    if-eqz v5, :cond_5

    .line 1625729
    invoke-virtual {v5}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 1625730
    if-eqz v6, :cond_5

    .line 1625731
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1625732
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1625733
    iget-object v2, p0, LX/A7F;->d:LX/1Uf;

    invoke-virtual {v2, v5}, LX/1Uf;->a(LX/1eE;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1625734
    :goto_0
    iget-object v3, p0, LX/A7F;->b:Ljava/lang/CharSequence;

    if-eqz v3, :cond_0

    .line 1625735
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object v2, v3, v7

    iget-object v2, p0, LX/A7F;->b:Ljava/lang/CharSequence;

    aput-object v2, v3, p4

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1625736
    :cond_0
    iget-object v3, p0, LX/A7F;->d:LX/1Uf;

    new-instance v4, LX/A7W;

    invoke-direct {v4, p0, p2}, LX/A7W;-><init>(LX/A7F;LX/A7V;)V

    invoke-virtual {v3, v2, v4}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;

    move-result-object v3

    .line 1625737
    if-nez v3, :cond_1

    .line 1625738
    iput-boolean p4, p2, LX/A7V;->b:Z

    .line 1625739
    :cond_1
    iput-object v2, p2, LX/A7V;->c:Ljava/lang/CharSequence;

    .line 1625740
    iput-object v3, p2, LX/A7V;->d:Ljava/lang/CharSequence;

    .line 1625741
    move-object p5, v3

    move-object v3, v2

    move-object v2, p5

    .line 1625742
    :goto_1
    iget-boolean v4, p2, LX/A7V;->b:Z

    move v4, v4

    .line 1625743
    if-eqz v4, :cond_4

    :goto_2
    move-object v2, v3

    .line 1625744
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1625745
    iget-object v1, v0, LX/A7X;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1625746
    iget-object v1, v0, LX/A7X;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1625747
    iget-object v0, v0, LX/A7X;->a:Landroid/widget/TextView;

    .line 1625748
    iget-object v3, p2, LX/A7V;->e:Ljava/lang/String;

    move-object v3, v3

    .line 1625749
    if-nez v3, :cond_2

    .line 1625750
    iget-object v3, p0, LX/A7F;->c:LX/11S;

    sget-object v4, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    .line 1625751
    iget-object v5, p2, LX/A7V;->a:Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;

    move-object v5, v5

    .line 1625752
    invoke-virtual {v5}, Lcom/facebook/ui/edithistory/protocol/FetchEditHistoryGraphQLModels$EditActionFragmentModel;->k()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-interface {v3, v4, v5, v6}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    .line 1625753
    iput-object v3, p2, LX/A7V;->e:Ljava/lang/String;

    .line 1625754
    :cond_2
    move-object v1, v3

    .line 1625755
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1625756
    return-void

    .line 1625757
    :cond_3
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1625758
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v2, v4

    move-object v3, v5

    goto :goto_1

    :cond_4
    move-object v3, v2

    .line 1625759
    goto :goto_2

    :cond_5
    move-object v2, v4

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1625762
    iget-object v0, p0, LX/A7F;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1625763
    const/4 v0, 0x0

    .line 1625764
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/A7F;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1625761
    iget-object v0, p0, LX/A7F;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1625760
    int-to-long v0, p1

    return-wide v0
.end method
