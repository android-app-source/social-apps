.class public final LX/9V9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1498969
    iput-object p1, p0, LX/9V9;->a:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498970
    const-class v0, LX/9V9;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9V9;->b:Ljava/lang/String;

    .line 1498971
    iput-object p2, p0, LX/9V9;->c:Ljava/lang/String;

    .line 1498972
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1498965
    new-instance v0, LX/9V8;

    invoke-direct {v0, p0, p1}, LX/9V8;-><init>(LX/9V9;Ljava/io/InputStream;)V

    .line 1498966
    :try_start_0
    iget-object v1, p0, LX/9V9;->a:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    iget-object v1, v1, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->e:LX/1Hk;

    new-instance v2, LX/9V4;

    iget-object v3, p0, LX/9V9;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/9V4;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0}, LX/1Hk;->a(LX/1bh;LX/2uu;)LX/1gI;

    move-result-object v0

    .line 1498967
    invoke-static {v0}, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->b(LX/1gI;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1498968
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method
