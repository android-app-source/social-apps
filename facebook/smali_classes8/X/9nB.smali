.class public final LX/9nB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/9nC;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/9nC;)V
    .locals 3

    .prologue
    .line 1536843
    iput-object p1, p0, LX/9nB;->a:LX/9nC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1536844
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, LX/9nA;

    invoke-direct {v2, p0, p1}, LX/9nA;-><init>(LX/9nB;LX/9nC;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/9nB;->b:Landroid/os/Handler;

    .line 1536845
    return-void
.end method

.method public static c(LX/9nB;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1536846
    iget-object v0, p0, LX/9nB;->a:LX/9nC;

    invoke-static {v0}, LX/9nC;->b(LX/9nC;)Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->flush()V

    .line 1536847
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1536848
    sget-boolean v0, LX/9nC;->a:Z

    if-eqz v0, :cond_0

    .line 1536849
    iget-object v0, p0, LX/9nB;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1536850
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1536851
    iget-object v0, p0, LX/9nB;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1536852
    iget-object v0, p0, LX/9nB;->a:LX/9nC;

    new-instance v1, Lcom/facebook/react/modules/network/ForwardingCookieHandler$CookieSaver$2;

    invoke-direct {v1, p0}, Lcom/facebook/react/modules/network/ForwardingCookieHandler$CookieSaver$2;-><init>(LX/9nB;)V

    .line 1536853
    invoke-static {v0, v1}, LX/9nC;->a$redex0(LX/9nC;Ljava/lang/Runnable;)V

    .line 1536854
    return-void
.end method
