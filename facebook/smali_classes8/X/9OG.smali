.class public final LX/9OG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForumChildGroupsFragmentModel$CommunityForumChildGroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupContentSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupContentSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunitySuggestedGroupsFragmentModel$RelatedGroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerJoinCommunityContext"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1479074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1479075
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;
    .locals 17

    .prologue
    .line 1479076
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1479077
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9OG;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1479078
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9OG;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1479079
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OG;->c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForumChildGroupsFragmentModel$CommunityForumChildGroupsModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1479080
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OG;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1479081
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9OG;->e:LX/0Px;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1479082
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9OG;->f:LX/0Px;

    invoke-static {v1, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1479083
    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9OG;->g:LX/15i;

    move-object/from16 v0, p0

    iget v10, v0, LX/9OG;->h:I

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v8, -0x4959043f

    invoke-static {v9, v10, v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v8

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1479084
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9OG;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1479085
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9OG;->j:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1479086
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9OG;->k:Ljava/lang/String;

    invoke-virtual {v1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1479087
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9OG;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunitySuggestedGroupsFragmentModel$RelatedGroupsModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1479088
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9OG;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1479089
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9OG;->n:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;

    invoke-static {v1, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1479090
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9OG;->o:LX/2uF;

    invoke-static {v15, v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v15

    .line 1479091
    const/16 v16, 0xe

    move/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1479092
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1479093
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1479094
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1479095
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1479096
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1479097
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1479098
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1479099
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1479100
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1479101
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1479102
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1479103
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1479104
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1479105
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1479106
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1479107
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1479108
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1479109
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1479110
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1479111
    new-instance v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    invoke-direct {v2, v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;-><init>(LX/15i;)V

    .line 1479112
    return-object v2

    .line 1479113
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
