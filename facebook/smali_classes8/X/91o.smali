.class public LX/91o;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/91m;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/91p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1431412
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/91o;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/91p;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1431413
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1431414
    iput-object p1, p0, LX/91o;->b:LX/0Ot;

    .line 1431415
    return-void
.end method

.method public static a(LX/0QB;)LX/91o;
    .locals 4

    .prologue
    .line 1431416
    const-class v1, LX/91o;

    monitor-enter v1

    .line 1431417
    :try_start_0
    sget-object v0, LX/91o;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1431418
    sput-object v2, LX/91o;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1431419
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1431420
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1431421
    new-instance v3, LX/91o;

    const/16 p0, 0x19a7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/91o;-><init>(LX/0Ot;)V

    .line 1431422
    move-object v0, v3

    .line 1431423
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1431424
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1431425
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1431426
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLInterfaces$TaggableObjectEdge;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1431427
    const v0, -0x42ca3b4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1431428
    check-cast p2, LX/91n;

    .line 1431429
    iget-object v0, p0, LX/91o;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/91p;

    iget-object v1, p2, LX/91n;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-boolean v2, p2, LX/91n;->b:Z

    iget-object v3, p2, LX/91n;->c:Ljava/lang/String;

    .line 1431430
    if-eqz v3, :cond_1

    move-object v4, v3

    .line 1431431
    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1431432
    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    .line 1431433
    :cond_0
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p0

    iget-object p2, v0, LX/91p;->a:LX/91M;

    invoke-virtual {p2, p1}, LX/91M;->c(LX/1De;)LX/91L;

    move-result-object p2

    invoke-static {v4}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/91L;->a(Landroid/net/Uri;)LX/91L;

    move-result-object v4

    const p2, 0x7f0b1895

    invoke-virtual {v4, p2}, LX/91L;->i(I)LX/91L;

    move-result-object v4

    const p2, 0x7f0b1895

    invoke-virtual {v4, p2}, LX/91L;->k(I)LX/91L;

    move-result-object v4

    const/4 p2, 0x0

    invoke-virtual {v4, p2}, LX/91L;->m(I)LX/91L;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, LX/91L;->a(Ljava/lang/CharSequence;)LX/91L;

    move-result-object v4

    invoke-interface {p0, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 1431434
    const p0, -0x42ca3b4

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v1, p2, v0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1431435
    invoke-interface {v4, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    const p0, 0x7f020307

    invoke-interface {v4, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const p0, 0x7f0b1898

    invoke-interface {v4, v5, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a0720

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1431436
    return-object v0

    .line 1431437
    :cond_1
    const-string v4, ""

    goto/16 :goto_0

    .line 1431438
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1431439
    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1431440
    invoke-static {}, LX/1dS;->b()V

    .line 1431441
    iget v0, p1, LX/1dQ;->b:I

    .line 1431442
    packed-switch v0, :pswitch_data_0

    .line 1431443
    :goto_0
    return-object v3

    .line 1431444
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1431445
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1431446
    check-cast v2, LX/91n;

    .line 1431447
    iget-object v4, p0, LX/91o;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    iget-boolean v4, v2, LX/91n;->b:Z

    iget-object p1, v2, LX/91n;->d:LX/90B;

    iget-object p2, v2, LX/91n;->e:LX/90D;

    .line 1431448
    if-eqz p2, :cond_1

    if-eqz v4, :cond_1

    .line 1431449
    invoke-interface {p2, v0}, LX/90D;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V

    .line 1431450
    :cond_0
    :goto_1
    goto :goto_0

    .line 1431451
    :cond_1
    if-eqz p1, :cond_0

    .line 1431452
    invoke-interface {p1, v0}, LX/90B;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x42ca3b4
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/91m;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1431453
    new-instance v1, LX/91n;

    invoke-direct {v1, p0}, LX/91n;-><init>(LX/91o;)V

    .line 1431454
    sget-object v2, LX/91o;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/91m;

    .line 1431455
    if-nez v2, :cond_0

    .line 1431456
    new-instance v2, LX/91m;

    invoke-direct {v2}, LX/91m;-><init>()V

    .line 1431457
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/91m;->a$redex0(LX/91m;LX/1De;IILX/91n;)V

    .line 1431458
    move-object v1, v2

    .line 1431459
    move-object v0, v1

    .line 1431460
    return-object v0
.end method
