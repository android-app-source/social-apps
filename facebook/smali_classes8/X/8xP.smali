.class public LX/8xP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public b:LX/17W;

.field public c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public d:LX/1Uf;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(LX/17W;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424084
    iput-object p1, p0, LX/8xP;->b:LX/17W;

    .line 1424085
    iput-object p2, p0, LX/8xP;->d:LX/1Uf;

    .line 1424086
    return-void
.end method

.method public static b(LX/0QB;)LX/8xP;
    .locals 3

    .prologue
    .line 1424087
    new-instance v2, LX/8xP;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-static {p0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v1

    check-cast v1, LX/1Uf;

    invoke-direct {v2, v0, v1}, LX/8xP;-><init>(LX/17W;LX/1Uf;)V

    .line 1424088
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 2

    .prologue
    .line 1424052
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424053
    iput-object p1, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1424054
    iput-object p2, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1424055
    iget-object v0, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const v1, 0x7f0d05d7

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8xP;->e:Landroid/widget/TextView;

    .line 1424056
    iget-object v0, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const v1, 0x7f0d05d8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8xP;->f:Landroid/widget/TextView;

    .line 1424057
    iget-object v0, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const v1, 0x7f0d05d9

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8xP;->g:Landroid/widget/TextView;

    .line 1424058
    iget-object v0, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1424059
    iget-object v0, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v1, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1424060
    const/4 p1, -0x1

    .line 1424061
    iget-object v0, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1424062
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    move v0, v1

    .line 1424063
    if-nez v0, :cond_0

    iget-object v0, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1424064
    :cond_0
    iget-object v0, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const v1, 0x7f02093a

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayResource(I)V

    .line 1424065
    iget-object v0, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->c(II)V

    .line 1424066
    :cond_1
    :goto_0
    iget-object v0, p0, LX/8xP;->e:Landroid/widget/TextView;

    iget-object v1, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1424067
    const/16 p1, 0x8

    .line 1424068
    iget-object v0, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1424069
    iget-object v0, p0, LX/8xP;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1424070
    :goto_1
    iget-object v0, p0, LX/8xP;->g:Landroid/widget/TextView;

    iget-object v1, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VO;->y(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1424071
    iget-object v0, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424072
    new-instance v0, LX/8xO;

    invoke-direct {v0, p0}, LX/8xO;-><init>(LX/8xP;)V

    .line 1424073
    iget-object v1, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1424074
    iget-object v1, p0, LX/8xP;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1424075
    iget-object v1, p0, LX/8xP;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1424076
    iget-object v1, p0, LX/8xP;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1424077
    return-void

    .line 1424078
    :cond_2
    iget-object v0, p0, LX/8xP;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1424079
    :cond_3
    iget-object v0, p0, LX/8xP;->d:LX/1Uf;

    iget-object v1, p0, LX/8xP;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Uf;->a(LX/1eE;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1424080
    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-nez v1, :cond_4

    .line 1424081
    iget-object v0, p0, LX/8xP;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1424082
    :cond_4
    iget-object v1, p0, LX/8xP;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
