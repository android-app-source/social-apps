.class public final LX/8yl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8ym;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:I

.field public c:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1426447
    invoke-static {}, LX/8ym;->q()LX/8ym;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1426448
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1426446
    const-string v0, "ImageWithTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426449
    if-ne p0, p1, :cond_1

    .line 1426450
    :cond_0
    :goto_0
    return v0

    .line 1426451
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1426452
    goto :goto_0

    .line 1426453
    :cond_3
    check-cast p1, LX/8yl;

    .line 1426454
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1426455
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1426456
    if-eq v2, v3, :cond_0

    .line 1426457
    iget-object v2, p0, LX/8yl;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8yl;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/8yl;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1426458
    goto :goto_0

    .line 1426459
    :cond_5
    iget-object v2, p1, LX/8yl;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1426460
    :cond_6
    iget v2, p0, LX/8yl;->b:I

    iget v3, p1, LX/8yl;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1426461
    goto :goto_0

    .line 1426462
    :cond_7
    iget-object v2, p0, LX/8yl;->c:LX/1dc;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/8yl;->c:LX/1dc;

    iget-object v3, p1, LX/8yl;->c:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1426463
    goto :goto_0

    .line 1426464
    :cond_9
    iget-object v2, p1, LX/8yl;->c:LX/1dc;

    if-nez v2, :cond_8

    .line 1426465
    :cond_a
    iget-object v2, p0, LX/8yl;->d:LX/1dc;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/8yl;->d:LX/1dc;

    iget-object v3, p1, LX/8yl;->d:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1426466
    goto :goto_0

    .line 1426467
    :cond_c
    iget-object v2, p1, LX/8yl;->d:LX/1dc;

    if-nez v2, :cond_b

    .line 1426468
    :cond_d
    iget v2, p0, LX/8yl;->e:I

    iget v3, p1, LX/8yl;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1426469
    goto :goto_0
.end method
