.class public LX/9H0;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/20E;
.implements LX/20F;
.implements LX/3Wr;
.implements LX/3Ws;


# instance fields
.field public a:LX/9CG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/20P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:LX/9HK;

.field public final g:Lcom/facebook/widget/accessibility/AccessibleTextView;

.field private final h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

.field public final i:LX/20b;

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/20z;

.field public l:LX/21M;

.field private m:LX/20H;

.field public n:Z

.field public o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1460661
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9H0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1460662
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1460663
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/9H0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460664
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460665
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460666
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    iput-object v0, p0, LX/9H0;->m:LX/20H;

    .line 1460667
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/9H0;

    invoke-static {v0}, LX/9CG;->a(LX/0QB;)LX/9CG;

    move-result-object v3

    check-cast v3, LX/9CG;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v4

    check-cast v4, LX/1zf;

    invoke-static {v0}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object p2

    check-cast p2, LX/20K;

    const-class p3, LX/20P;

    invoke-interface {v0, p3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p3

    check-cast p3, LX/20P;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v0

    check-cast v0, LX/0tH;

    iput-object v3, v2, LX/9H0;->a:LX/9CG;

    iput-object v4, v2, LX/9H0;->b:LX/1zf;

    iput-object p2, v2, LX/9H0;->c:LX/20K;

    iput-object p3, v2, LX/9H0;->d:LX/20P;

    iput-object v0, v2, LX/9H0;->e:LX/0tH;

    .line 1460668
    const v0, 0x7f0302c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1460669
    iget-object v0, p0, LX/9H0;->a:LX/9CG;

    invoke-virtual {v0, p1}, LX/9CG;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/9H0;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1460670
    const v0, 0x7f0d09ba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/accessibility/AccessibleTextView;

    iput-object v0, p0, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    .line 1460671
    const v0, 0x7f0d09bb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    iput-object v0, p0, LX/9H0;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    .line 1460672
    iget-object v0, p0, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1460673
    iget-object v0, p0, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b08cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setTextSize(IF)V

    .line 1460674
    iget-object v0, p0, LX/9H0;->d:LX/20P;

    invoke-virtual {v0, p0}, LX/20P;->a(LX/20E;)LX/20b;

    move-result-object v0

    iput-object v0, p0, LX/9H0;->i:LX/20b;

    .line 1460675
    iget-object v0, p0, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    .line 1460676
    new-instance v1, LX/9Gy;

    invoke-direct {v1, p0}, LX/9Gy;-><init>(LX/9H0;)V

    move-object v1, v1

    .line 1460677
    invoke-virtual {v0, v1}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1460678
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9H0;->f:LX/9HK;

    .line 1460679
    return-void
.end method

.method public static a$redex0(LX/9H0;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1460680
    iget-object v0, p0, LX/9H0;->m:LX/20H;

    sget-object v1, LX/20H;->REACTIONS:LX/20H;

    if-eq v0, v1, :cond_0

    .line 1460681
    const/4 v0, 0x0

    .line 1460682
    :goto_0
    return v0

    .line 1460683
    :cond_0
    iget-object v0, p0, LX/9H0;->c:LX/20K;

    invoke-virtual {v0, p0}, LX/20K;->a(LX/20F;)V

    .line 1460684
    iget-object v0, p0, LX/9H0;->c:LX/20K;

    iget-object v1, p0, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v0, v1, p1}, LX/20K;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 1460685
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1460686
    iget-object v0, p0, LX/9H0;->c:LX/20K;

    invoke-virtual {v0}, LX/20K;->a()V

    .line 1460687
    return-void
.end method

.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1460688
    iget-object v0, p0, LX/9H0;->f:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1460689
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460690
    invoke-virtual {p0}, LX/9H0;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0, p1}, LX/9CG;->a(Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V

    .line 1460691
    return-void
.end method

.method public final a(Landroid/view/View;LX/1zt;)V
    .locals 2

    .prologue
    .line 1460692
    iget-object v0, p0, LX/9H0;->l:LX/21M;

    if-eqz v0, :cond_0

    sget-object v0, LX/1zt;->c:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 1460693
    :cond_0
    :goto_0
    return-void

    .line 1460694
    :cond_1
    iget-object v0, p0, LX/9H0;->l:LX/21M;

    const/4 v1, 0x0

    invoke-interface {v0, p0, p2, v1}, LX/21M;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 1460695
    sget-object v0, LX/20H;->REACTIONS:LX/20H;

    invoke-virtual {p0, v0}, LX/9H0;->setMode(LX/20H;)V

    .line 1460696
    invoke-static {p0, p2}, LX/9H0;->a$redex0(LX/9H0;Landroid/view/MotionEvent;)Z

    .line 1460697
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1460658
    if-eqz p1, :cond_0

    sget-object v0, LX/20H;->REACTIONS:LX/20H;

    :goto_0
    invoke-virtual {p0, v0}, LX/9H0;->setMode(LX/20H;)V

    .line 1460659
    return-void

    .line 1460660
    :cond_0
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1460698
    iget-object v0, p0, LX/9H0;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a(Z)V

    .line 1460699
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1460656
    iget-object v0, p0, LX/9H0;->f:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1460657
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460629
    iget-object v0, p0, LX/9H0;->f:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1460653
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1460654
    iget-object v0, p0, LX/9H0;->f:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1460655
    return-void
.end method

.method public getDockTheme()LX/20I;
    .locals 1

    .prologue
    .line 1460652
    sget-object v0, LX/20I;->LIGHT:LX/20I;

    return-object v0
.end method

.method public getInteractionLogger()LX/20z;
    .locals 1

    .prologue
    .line 1460651
    iget-object v0, p0, LX/9H0;->k:LX/20z;

    return-object v0
.end method

.method public getSupportedReactions()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1460648
    iget-object v0, p0, LX/9H0;->j:LX/0Px;

    if-eqz v0, :cond_0

    .line 1460649
    iget-object v0, p0, LX/9H0;->j:LX/0Px;

    .line 1460650
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9H0;->b:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->d()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1460644
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1460645
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    invoke-virtual {p0, v0}, LX/9H0;->setMode(LX/20H;)V

    .line 1460646
    invoke-virtual {p0}, LX/9H0;->a()V

    .line 1460647
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460643
    invoke-static {p0, p1}, LX/9H0;->a$redex0(LX/9H0;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x3f5fa2c9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1460642
    invoke-static {p0, p1}, LX/9H0;->a$redex0(LX/9H0;Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x5c772ba6

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setMode(LX/20H;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1460630
    sget-object v0, LX/9Gz;->a:[I

    invoke-virtual {p1}, LX/20H;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1460631
    :cond_0
    :goto_0
    iput-object p1, p0, LX/9H0;->m:LX/20H;

    .line 1460632
    return-void

    .line 1460633
    :pswitch_0
    iget-object v0, p0, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setVisibility(I)V

    .line 1460634
    iget-object v0, p0, LX/9H0;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v3}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 1460635
    invoke-virtual {p0}, LX/9H0;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1460636
    invoke-virtual {p0}, LX/9H0;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 1460637
    :pswitch_1
    iget-object v0, p0, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setVisibility(I)V

    .line 1460638
    iget-object v0, p0, LX/9H0;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 1460639
    iget-object v0, p0, LX/9H0;->h:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a(Z)V

    .line 1460640
    invoke-virtual {p0}, LX/9H0;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1460641
    invoke-virtual {p0}, LX/9H0;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
