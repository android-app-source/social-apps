.class public final LX/8pk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1406272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;)Lcom/facebook/graphql/model/GraphQLSeenByConnection;
    .locals 7

    .prologue
    .line 1406273
    if-nez p0, :cond_0

    .line 1406274
    const/4 v0, 0x0

    .line 1406275
    :goto_0
    return-object v0

    .line 1406276
    :cond_0
    new-instance v2, LX/4Yj;

    invoke-direct {v2}, LX/4Yj;-><init>()V

    .line 1406277
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->a()I

    move-result v0

    .line 1406278
    iput v0, v2, LX/4Yj;->b:I

    .line 1406279
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1406280
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1406281
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1406282
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DefaultSeenByActorModel;

    .line 1406283
    if-nez v0, :cond_3

    .line 1406284
    const/4 v4, 0x0

    .line 1406285
    :goto_2
    move-object v0, v4

    .line 1406286
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1406287
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1406288
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1406289
    iput-object v0, v2, LX/4Yj;->c:LX/0Px;

    .line 1406290
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->c()LX/0us;

    move-result-object v0

    .line 1406291
    if-nez v0, :cond_5

    .line 1406292
    const/4 v1, 0x0

    .line 1406293
    :goto_3
    move-object v0, v1

    .line 1406294
    iput-object v0, v2, LX/4Yj;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1406295
    invoke-virtual {v2}, LX/4Yj;->a()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    goto :goto_0

    .line 1406296
    :cond_3
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    .line 1406297
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DefaultSeenByActorModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 1406298
    iput-object v5, v4, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1406299
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DefaultSeenByActorModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 1406300
    iput-object v5, v4, LX/3dL;->E:Ljava/lang/String;

    .line 1406301
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DefaultSeenByActorModel;->k()Z

    move-result v5

    .line 1406302
    iput-boolean v5, v4, LX/3dL;->T:Z

    .line 1406303
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DefaultSeenByActorModel;->l()Z

    move-result v5

    .line 1406304
    iput-boolean v5, v4, LX/3dL;->V:Z

    .line 1406305
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DefaultSeenByActorModel;->v_()Ljava/lang/String;

    move-result-object v5

    .line 1406306
    iput-object v5, v4, LX/3dL;->ag:Ljava/lang/String;

    .line 1406307
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DefaultSeenByActorModel;->c()LX/1Fb;

    move-result-object v5

    .line 1406308
    if-nez v5, :cond_4

    .line 1406309
    const/4 v6, 0x0

    .line 1406310
    :goto_4
    move-object v5, v6

    .line 1406311
    iput-object v5, v4, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1406312
    invoke-virtual {v4}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    goto :goto_2

    .line 1406313
    :cond_4
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 1406314
    invoke-interface {v5}, LX/1Fb;->a()I

    move-result v0

    .line 1406315
    iput v0, v6, LX/2dc;->c:I

    .line 1406316
    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1406317
    iput-object v0, v6, LX/2dc;->h:Ljava/lang/String;

    .line 1406318
    invoke-interface {v5}, LX/1Fb;->c()I

    move-result v0

    .line 1406319
    iput v0, v6, LX/2dc;->i:I

    .line 1406320
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto :goto_4

    .line 1406321
    :cond_5
    new-instance v1, LX/17L;

    invoke-direct {v1}, LX/17L;-><init>()V

    .line 1406322
    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    .line 1406323
    iput-object v3, v1, LX/17L;->c:Ljava/lang/String;

    .line 1406324
    invoke-interface {v0}, LX/0us;->b()Z

    move-result v3

    .line 1406325
    iput-boolean v3, v1, LX/17L;->d:Z

    .line 1406326
    invoke-interface {v0}, LX/0us;->c()Z

    move-result v3

    .line 1406327
    iput-boolean v3, v1, LX/17L;->e:Z

    .line 1406328
    invoke-interface {v0}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v3

    .line 1406329
    iput-object v3, v1, LX/17L;->f:Ljava/lang/String;

    .line 1406330
    invoke-virtual {v1}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    goto :goto_3
.end method
