.class public LX/AS7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3l6;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field public final b:LX/3kp;

.field private final c:Landroid/content/Context;

.field private d:LX/0hs;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;LX/3kp;Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673685
    iput-object p1, p0, LX/AS7;->a:Landroid/view/ViewGroup;

    .line 1673686
    iput-object p2, p0, LX/AS7;->b:LX/3kp;

    .line 1673687
    iput-object p3, p0, LX/AS7;->c:Landroid/content/Context;

    .line 1673688
    iget-object v0, p0, LX/AS7;->b:LX/3kp;

    sget-object v1, LX/0dp;->n:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1673689
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)LX/ASZ;
    .locals 2

    .prologue
    .line 1673681
    sget-object v0, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/AS7;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AS7;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d0ae1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1673682
    sget-object v0, LX/ASZ;->SHOW:LX/ASZ;

    .line 1673683
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1673678
    iget-object v0, p0, LX/AS7;->d:LX/0hs;

    if-nez v0, :cond_0

    .line 1673679
    :goto_0
    return-void

    .line 1673680
    :cond_0
    iget-object v0, p0, LX/AS7;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1673677
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 1673667
    iget-object v0, p0, LX/AS7;->a:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 1673668
    :cond_0
    :goto_0
    return-void

    .line 1673669
    :cond_1
    iget-object v0, p0, LX/AS7;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d0ae1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1673670
    if-eqz v0, :cond_0

    .line 1673671
    iget-object v1, p0, LX/AS7;->d:LX/0hs;

    if-nez v1, :cond_2

    .line 1673672
    new-instance v1, LX/0hs;

    iget-object v2, p0, LX/AS7;->c:Landroid/content/Context;

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/AS7;->d:LX/0hs;

    .line 1673673
    iget-object v1, p0, LX/AS7;->d:LX/0hs;

    new-instance v2, LX/AS6;

    invoke-direct {v2, p0}, LX/AS6;-><init>(LX/AS7;)V

    .line 1673674
    iput-object v2, v1, LX/0ht;->H:LX/2dD;

    .line 1673675
    iget-object v1, p0, LX/AS7;->d:LX/0hs;

    const v2, 0x7f081498

    invoke-virtual {v1, v2}, LX/0hs;->b(I)V

    .line 1673676
    :cond_2
    iget-object v1, p0, LX/AS7;->d:LX/0hs;

    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1673662
    iget-object v0, p0, LX/AS7;->d:LX/0hs;

    if-nez v0, :cond_0

    .line 1673663
    const/4 v0, 0x0

    .line 1673664
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/AS7;->d:LX/0hs;

    .line 1673665
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 1673666
    goto :goto_0
.end method
