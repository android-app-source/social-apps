.class public LX/8zX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/91M;

.field public b:F

.field public c:F

.field public d:F

.field public e:Landroid/net/Uri;

.field public f:Landroid/net/Uri;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/91M;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1427598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1427599
    iput-object p1, p0, LX/8zX;->a:LX/91M;

    .line 1427600
    return-void
.end method

.method public static a(LX/0QB;)LX/8zX;
    .locals 4

    .prologue
    .line 1427601
    const-class v1, LX/8zX;

    monitor-enter v1

    .line 1427602
    :try_start_0
    sget-object v0, LX/8zX;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1427603
    sput-object v2, LX/8zX;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1427604
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1427605
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1427606
    new-instance p0, LX/8zX;

    invoke-static {v0}, LX/91M;->a(LX/0QB;)LX/91M;

    move-result-object v3

    check-cast v3, LX/91M;

    invoke-direct {p0, v3}, LX/8zX;-><init>(LX/91M;)V

    .line 1427607
    move-object v0, p0

    .line 1427608
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1427609
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1427610
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1427611
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/8zX;LX/5LG;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;ZLX/1De;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1427612
    iput-object v1, p0, LX/8zX;->g:Ljava/lang/String;

    .line 1427613
    if-eqz p3, :cond_1

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1427614
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v1

    move-object v3, v0

    move-object v0, v1

    .line 1427615
    :goto_0
    invoke-virtual {p4}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1895

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 1427616
    invoke-virtual {p4}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b1896

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 1427617
    invoke-virtual {p4}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b1897

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    iput v6, p0, LX/8zX;->d:F

    .line 1427618
    iput-object v1, p0, LX/8zX;->f:Landroid/net/Uri;

    .line 1427619
    if-eqz v3, :cond_4

    .line 1427620
    iput-object v3, p0, LX/8zX;->e:Landroid/net/Uri;

    .line 1427621
    iput v4, p0, LX/8zX;->b:F

    .line 1427622
    :goto_1
    iget-object v1, p0, LX/8zX;->e:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 1427623
    iput-object v0, p0, LX/8zX;->e:Landroid/net/Uri;

    .line 1427624
    iput v4, p0, LX/8zX;->b:F

    .line 1427625
    :cond_0
    return-void

    .line 1427626
    :cond_1
    invoke-interface {p1}, LX/5LG;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1427627
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_0

    .line 1427628
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ah_()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1427629
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ah_()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8zX;->g:Ljava/lang/String;

    .line 1427630
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1427631
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1427632
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1427633
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v3, v1

    move-object v8, v0

    move-object v0, v2

    move-object v2, v8

    goto/16 :goto_0

    .line 1427634
    :cond_4
    iput-object v2, p0, LX/8zX;->e:Landroid/net/Uri;

    .line 1427635
    iput v5, p0, LX/8zX;->b:F

    .line 1427636
    const/4 v1, 0x0

    iput v1, p0, LX/8zX;->d:F

    goto :goto_1

    :cond_5
    move-object v2, v0

    move-object v3, v1

    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method
