.class public final LX/ALk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/graphics/Bitmap;

.field public final synthetic b:LX/ALl;


# direct methods
.method public constructor <init>(LX/ALl;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1665380
    iput-object p1, p0, LX/ALk;->b:LX/ALl;

    iput-object p2, p0, LX/ALk;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1665374
    iget-object v0, p0, LX/ALk;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1665375
    iget-object v0, p0, LX/ALk;->b:LX/ALl;

    iget-object v0, v0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->m:LX/AM1;

    invoke-virtual {v0}, LX/AM1;->destroyDrawingCache()V

    .line 1665376
    iget-object v0, p0, LX/ALk;->b:LX/ALl;

    iget-object v0, v0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->m:LX/AM1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AM1;->setDrawingCacheEnabled(Z)V

    .line 1665377
    iget-object v0, p0, LX/ALk;->b:LX/ALl;

    iget-object v0, v0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->o:LX/ALp;

    if-eqz v0, :cond_0

    .line 1665378
    :cond_0
    iget-object v0, p0, LX/ALk;->b:LX/ALl;

    iget-object v0, v0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->h:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1665379
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1665367
    iget-object v0, p0, LX/ALk;->b:LX/ALl;

    iget-object v0, v0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->h:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1665368
    sget-object v0, LX/ALq;->d:Ljava/lang/String;

    const-string v1, "Failed to decode media to bitmap or video file"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1665369
    iget-object v1, p0, LX/ALk;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1665370
    iget-object v1, p0, LX/ALk;->b:LX/ALl;

    iget-object v1, v1, LX/ALl;->a:LX/ALq;

    iget-object v1, v1, LX/ALq;->m:LX/AM1;

    invoke-virtual {v1}, LX/AM1;->destroyDrawingCache()V

    .line 1665371
    iget-object v1, p0, LX/ALk;->b:LX/ALl;

    iget-object v1, v1, LX/ALl;->a:LX/ALq;

    iget-object v1, v1, LX/ALq;->m:LX/AM1;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/AM1;->setDrawingCacheEnabled(Z)V

    .line 1665372
    iget-object v1, p0, LX/ALk;->b:LX/ALl;

    iget-object v1, v1, LX/ALl;->a:LX/ALq;

    iget-object v1, v1, LX/ALq;->c:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 1665373
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1665366
    invoke-direct {p0}, LX/ALk;->a()V

    return-void
.end method
