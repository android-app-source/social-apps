.class public LX/AMR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/AMZ;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1666221
    const-class v0, LX/AMR;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AMR;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AMZ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1666216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1666217
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1666218
    iput-object v0, p0, LX/AMR;->c:LX/0Ot;

    .line 1666219
    iput-object p1, p0, LX/AMR;->b:LX/AMZ;

    .line 1666220
    return-void
.end method

.method public static b(LX/0QB;)LX/AMR;
    .locals 4

    .prologue
    .line 1666209
    new-instance v1, LX/AMR;

    .line 1666210
    new-instance v3, LX/AMZ;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-direct {v3, v0, v2}, LX/AMZ;-><init>(LX/0tX;LX/1Ck;)V

    .line 1666211
    move-object v0, v3

    .line 1666212
    check-cast v0, LX/AMZ;

    invoke-direct {v1, v0}, LX/AMR;-><init>(LX/AMZ;)V

    .line 1666213
    const/16 v0, 0x1884

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    .line 1666214
    iput-object v0, v1, LX/AMR;->c:LX/0Ot;

    .line 1666215
    return-object v1
.end method

.method public static b(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/0Pz;LX/ANr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "LX/0Pz",
            "<",
            "LX/AN3;",
            ">;",
            "Lcom/facebook/cameracore/assets/ShaderFilterAssetManager$Callback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1666184
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_1

    .line 1666185
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 1666186
    if-lez v0, :cond_0

    .line 1666187
    sget-object v1, LX/AMR;->a:Ljava/lang/String;

    const-string v2, "%d downloads failed"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1666188
    :cond_0
    invoke-virtual {p2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1666189
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    new-instance v2, LX/AN3;

    const-string v3, "None"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LX/AN3;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1666190
    iget-object v2, p3, LX/ANr;->a:LX/AOD;

    .line 1666191
    iput-object v1, v2, LX/AOD;->c:LX/0Px;

    .line 1666192
    iget-object v1, p3, LX/ANr;->b:LX/AOF;

    .line 1666193
    iget-object v2, v1, LX/AOF;->e:LX/AOO;

    iget-object v3, v1, LX/AOF;->h:LX/AOD;

    .line 1666194
    iget-object v1, v3, LX/AOD;->c:LX/0Px;

    move-object v3, v1

    .line 1666195
    invoke-virtual {v2, v3}, LX/AOH;->a(LX/0Px;)V

    .line 1666196
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(ZLX/ANr;)V
    .locals 10

    .prologue
    .line 1666197
    iget-object v0, p0, LX/AMR;->b:LX/AMZ;

    new-instance v1, LX/AMP;

    invoke-direct {v1, p0, p2}, LX/AMP;-><init>(LX/AMR;LX/ANr;)V

    .line 1666198
    if-eqz p1, :cond_0

    sget-object v2, LX/0zS;->d:LX/0zS;

    .line 1666199
    :goto_0
    iget-object v3, v0, LX/AMZ;->a:LX/0tX;

    .line 1666200
    new-instance v6, LX/AMu;

    invoke-direct {v6}, LX/AMu;-><init>()V

    move-object v6, v6

    .line 1666201
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    const-wide/16 v8, 0x384

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    const/4 v7, 0x1

    .line 1666202
    iput-boolean v7, v6, LX/0zO;->p:Z

    .line 1666203
    move-object v6, v6

    .line 1666204
    move-object v2, v6

    .line 1666205
    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1666206
    iget-object v3, v0, LX/AMZ;->b:LX/1Ck;

    sget-object v4, LX/AMY;->FETCH_DOWNLOADABLE_SHADER_FILTERS:LX/AMY;

    new-instance v5, LX/AMX;

    invoke-direct {v5, v0, v1}, LX/AMX;-><init>(LX/AMZ;LX/AMP;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1666207
    return-void

    .line 1666208
    :cond_0
    sget-object v2, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method
