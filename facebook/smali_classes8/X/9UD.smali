.class public LX/9UD;
.super LX/9UC;
.source ""


# instance fields
.field public c:LX/9Tr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9Tr",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Set;LX/2Rd;LX/0ad;LX/0Rf;)V
    .locals 1
    .param p6    # LX/0Rf;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/user/names/Normalizer;",
            "LX/0ad;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1497714
    invoke-direct {p0, p1, p2, p4, p5}, LX/9UC;-><init>(Landroid/content/Context;Landroid/database/Cursor;LX/2Rd;LX/0ad;)V

    .line 1497715
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1497716
    iput-object v0, p0, LX/9UD;->k:LX/0Rf;

    .line 1497717
    new-instance v0, LX/9Tr;

    invoke-direct {v0, p3}, LX/9Tr;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/9UD;->c:LX/9Tr;

    .line 1497718
    iput-object p6, p0, LX/9UD;->k:LX/0Rf;

    .line 1497719
    return-void
.end method


# virtual methods
.method public a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1497720
    invoke-super/range {p0 .. p5}, LX/9UC;->a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1497721
    invoke-virtual {p0, p1, p2}, LX/9UC;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497722
    iget-object v2, p0, LX/9UD;->c:LX/9Tr;

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1497723
    const v4, 0x7f0d092e

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 1497724
    iget-object v5, v2, LX/9Tr;->a:Ljava/util/Collection;

    invoke-interface {v5, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1497725
    iget-object v2, p0, LX/9UD;->k:LX/0Rf;

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1497726
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1497727
    :goto_0
    return-object v1

    .line 1497728
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/model/FacebookProfile;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1497729
    invoke-super {p0, p1}, LX/9UC;->a(Lcom/facebook/ipc/model/FacebookProfile;)Landroid/view/View;

    move-result-object v0

    .line 1497730
    const p0, 0x7f0d275e

    invoke-virtual {v0, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/view/ViewStub;

    invoke-virtual {p0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1497731
    move-object v0, v0

    .line 1497732
    return-object v0
.end method
