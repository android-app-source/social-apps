.class public final LX/9ht;
.super LX/9hl;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:D

.field public final synthetic d:D


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)V
    .locals 0

    .prologue
    .line 1527068
    iput-object p2, p0, LX/9ht;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9ht;->b:Ljava/lang/String;

    iput-wide p4, p0, LX/9ht;->c:D

    iput-wide p6, p0, LX/9ht;->d:D

    invoke-direct {p0, p1}, LX/9hl;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .locals 8

    .prologue
    .line 1527069
    iget-object v0, p0, LX/9ht;->a:Ljava/lang/String;

    iget-object v1, p0, LX/9ht;->b:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/9hj;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1527070
    const-string v0, "MediaVisitorTagAdd"

    const-string v1, "MG.Tag added multiple times for same user"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527071
    :goto_0
    return-object p1

    :cond_0
    invoke-static {p1}, LX/5kN;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)LX/5kN;

    move-result-object v0

    iget-object v2, p0, LX/9ht;->a:Ljava/lang/String;

    iget-object v3, p0, LX/9ht;->b:Ljava/lang/String;

    iget-wide v4, p0, LX/9ht;->c:D

    iget-wide v6, p0, LX/9ht;->d:D

    move-object v1, p1

    invoke-static/range {v1 .. v7}, LX/9hj;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;Ljava/lang/String;Ljava/lang/String;DD)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    .line 1527072
    iput-object v1, v0, LX/5kN;->ak:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    .line 1527073
    move-object v0, v0

    .line 1527074
    invoke-virtual {v0}, LX/5kN;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object p1

    goto :goto_0
.end method
