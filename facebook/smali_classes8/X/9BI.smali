.class public final LX/9BI;
.super LX/0gG;
.source ""

# interfaces
.implements LX/6Ue;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/44w",
            "<",
            "LX/1zt;",
            "LX/55h",
            "<",
            "LX/9Al;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/44w",
            "<",
            "LX/1zt;",
            "LX/55h",
            "<",
            "LX/9Al;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1451615
    iput-object p1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1451616
    iput-object p2, p0, LX/9BI;->b:Ljava/util/List;

    .line 1451617
    return-void
.end method

.method private a(ILX/1zt;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1451584
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    invoke-virtual {v0, p2}, LX/9B3;->d(LX/1zt;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1451585
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    const v6, 0x820008

    .line 1451586
    iget-object v3, v0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v6, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1451587
    iget-object v3, v0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "reaction_tab_pos:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v6, p1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1451588
    :cond_0
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-boolean v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->D:Z

    if-eqz v0, :cond_6

    .line 1451589
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1451590
    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/9BI;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/9BI;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    iget-object v0, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, LX/1zt;

    .line 1451591
    iget v3, v0, LX/1zt;->e:I

    move v0, v3

    .line 1451592
    if-ne v0, v1, :cond_4

    move v0, v1

    .line 1451593
    :goto_0
    iget v3, p2, LX/1zt;->e:I

    move v3, v3

    .line 1451594
    if-nez v3, :cond_5

    move v3, v1

    .line 1451595
    :goto_1
    iget-object v4, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v4, v4, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    if-nez v0, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    invoke-static {p0, p1}, LX/9BI;->e(LX/9BI;I)I

    move-result v0

    const/4 v6, 0x1

    .line 1451596
    invoke-static {v4, p2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v7

    .line 1451597
    iget-object v5, v4, LX/9B3;->o:LX/0v6;

    if-eqz v5, :cond_3

    iget-object v5, v4, LX/9B3;->n:[Z

    aget-boolean v5, v5, v7

    if-nez v5, :cond_3

    invoke-virtual {v4}, LX/9B3;->c()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_7

    .line 1451598
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v2

    .line 1451599
    goto :goto_0

    :cond_5
    move v3, v2

    .line 1451600
    goto :goto_1

    .line 1451601
    :cond_6
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    invoke-static {p0, p1}, LX/9BI;->e(LX/9BI;I)I

    move-result v2

    invoke-virtual {v0, p2, v2, v1}, LX/9B3;->a(LX/1zt;IZ)V

    goto :goto_2

    .line 1451602
    :cond_7
    if-lez v0, :cond_8

    move v5, v6

    :goto_3
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 1451603
    iget-object v5, v4, LX/9B3;->n:[Z

    aput-boolean v6, v5, v7

    .line 1451604
    invoke-static {v4}, LX/9B3;->f(LX/9B3;)Z

    .line 1451605
    invoke-static {v1}, LX/9B3;->a(Z)LX/0rS;

    move-result-object v12

    .line 1451606
    iget-object v5, v4, LX/9B3;->k:[LX/55h;

    aget-object v5, v5, v7

    .line 1451607
    iget-object v6, v5, LX/55h;->e:Ljava/lang/String;

    move-object v11, v6

    .line 1451608
    new-instance v5, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;

    invoke-virtual {v4}, LX/9B3;->c()Ljava/lang/String;

    move-result-object v6

    const/4 v10, 0x0

    move-object v7, p2

    move v8, v2

    move v9, v0

    invoke-direct/range {v5 .. v12}, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;-><init>(Ljava/lang/String;LX/1zt;ZILjava/lang/String;Ljava/lang/String;LX/0rS;)V

    .line 1451609
    iget-object v6, v4, LX/9B3;->g:LX/3HA;

    invoke-static {}, LX/5Cb;->a()LX/5Ca;

    move-result-object v7

    iget-object v8, v4, LX/9B3;->r:LX/9BJ;

    invoke-virtual {v8}, LX/9BJ;->b()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    invoke-virtual {v6, v7, v5, v8}, LX/3HA;->a(LX/0gW;Lcom/facebook/api/ufiservices/common/FetchReactionsParams;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v5

    .line 1451610
    iget-boolean v6, v4, LX/9B3;->s:Z

    .line 1451611
    iput-boolean v6, v5, LX/0zO;->p:Z

    .line 1451612
    iget-object v6, v4, LX/9B3;->o:LX/0v6;

    invoke-virtual {v6, v5}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v5

    .line 1451613
    iget-object v6, v4, LX/9B3;->i:Ljava/util/concurrent/Executor;

    invoke-virtual {v5, v6}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v5

    new-instance v6, LX/9B2;

    invoke-direct {v6, v4, p2, v0, v1}, LX/9B2;-><init>(LX/9B3;LX/1zt;IZ)V

    invoke-virtual {v5, v6}, LX/0zX;->a(LX/0rl;)LX/0zi;

    goto :goto_2

    .line 1451614
    :cond_8
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public static a$redex0(LX/9BI;I)LX/1zt;
    .locals 1

    .prologue
    .line 1451581
    invoke-virtual {p0}, LX/9BI;->b()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 1451582
    sget-object v0, LX/1zt;->c:LX/1zt;

    move-object v0, v0

    .line 1451583
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9BI;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    iget-object v0, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, LX/1zt;

    goto :goto_0
.end method

.method private static e(LX/9BI;I)I
    .locals 1

    .prologue
    .line 1451493
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->E:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->F:I

    goto :goto_0
.end method


# virtual methods
.method public final A_(I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1451576
    invoke-static {p0, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v0

    .line 1451577
    invoke-static {v0}, LX/9B3;->e(LX/1zt;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    :goto_0
    move-object v0, p0

    .line 1451578
    return-object v0

    .line 1451579
    :cond_0
    iget-object p0, v0, LX/1zt;->k:LX/1zx;

    invoke-interface {p0}, LX/1zx;->a()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    move-object p0, p0

    .line 1451580
    goto :goto_0
.end method

.method public final G_(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1451575
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->k:LX/9B7;

    invoke-static {p0, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v1

    iget-object v2, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v2, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->x:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v2}, LX/9B7;->a(LX/1zt;Ljava/util/HashMap;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1451530
    invoke-static {p0, p2}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v3

    .line 1451531
    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-ne v1, p2, :cond_4

    .line 1451532
    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->z:Landroid/util/SparseArray;

    .line 1451533
    iget v4, v3, LX/1zt;->e:I

    move v4, v4

    .line 1451534
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1451535
    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    const v7, 0x820006

    .line 1451536
    iget-object v4, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v4, v7, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1451537
    iget-object v4, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "reaction_tab_pos:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v7, p2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1451538
    move v1, v0

    .line 1451539
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 1451540
    iget-object v0, p0, LX/9BI;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, LX/55h;

    .line 1451541
    iget-object v5, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-static {v5, v0}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->b(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/55h;)LX/1Cw;

    move-result-object v5

    .line 1451542
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 1451543
    iget-object v6, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-virtual {v6}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->c()I

    move-result v6

    invoke-virtual {v4, v6, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 1451544
    const v6, 0x7f0d124b

    invoke-static {v4, v6}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/widget/listview/BetterListView;

    .line 1451545
    new-instance v7, LX/9BG;

    invoke-direct {v7, p0, v0, v3}, LX/9BG;-><init>(LX/9BI;LX/55h;LX/1zt;)V

    invoke-virtual {v6, v7}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1451546
    invoke-virtual {v6, v5}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1451547
    new-instance v7, LX/9BH;

    invoke-direct {v7, p0, v5, v3}, LX/9BH;-><init>(LX/9BI;LX/1Cw;LX/1zt;)V

    move-object v7, v7

    .line 1451548
    invoke-virtual {v6, v7}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1451549
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1451550
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1451551
    if-eqz v0, :cond_5

    invoke-virtual {v0}, LX/55h;->c()I

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 1451552
    if-eqz v5, :cond_3

    .line 1451553
    invoke-direct {p0, p2, v3}, LX/9BI;->a(ILX/1zt;)V

    .line 1451554
    const v1, 0x7f0d124a

    invoke-static {v4, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1451555
    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1451556
    :cond_0
    :goto_2
    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-boolean v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->I:Z

    if-eqz v1, :cond_2

    .line 1451557
    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    .line 1451558
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1451559
    const/4 v3, 0x0

    move v5, v3

    :goto_3
    invoke-virtual {v0}, LX/55h;->c()I

    move-result v3

    if-ge v5, v3, :cond_1

    .line 1451560
    invoke-virtual {v0, v5}, LX/55h;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9Al;

    .line 1451561
    iget-object v7, v3, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v3, v7

    .line 1451562
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1451563
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 1451564
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 1451565
    iget-object v3, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v3, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->p:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1451566
    iget-object v5, v3, Lcom/facebook/ufiservices/flyout/ProfileListParams;->j:LX/8s1;

    move-object v3, v5

    .line 1451567
    invoke-virtual {v1, v0, v3}, LX/82m;->a(LX/0Px;LX/8s1;)V

    .line 1451568
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    .line 1451569
    iput-boolean v2, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->I:Z

    .line 1451570
    :cond_2
    return-object v4

    .line 1451571
    :cond_3
    if-eqz v1, :cond_0

    .line 1451572
    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    .line 1451573
    iget-object v3, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x820006

    const/4 v6, 0x2

    invoke-interface {v3, v5, p2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1451574
    goto :goto_2

    :cond_4
    move v1, v2

    goto/16 :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1451527
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    const-string v1, "ReactorsListBatchFetch"

    .line 1451528
    new-instance p0, LX/0v6;

    invoke-direct {p0, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    iput-object p0, v0, LX/9B3;->o:LX/0v6;

    .line 1451529
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1451525
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1451526
    return-void
.end method

.method public final a(Landroid/widget/TextView;I)V
    .locals 6

    .prologue
    .line 1451510
    invoke-static {p0, p2}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v1

    .line 1451511
    instance-of v0, p1, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1451512
    check-cast v0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;

    .line 1451513
    invoke-static {v1}, LX/9B7;->c(LX/1zt;)I

    move-result v2

    .line 1451514
    iput v2, v0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->b:I

    .line 1451515
    iget-object v2, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v2, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->k:LX/9B7;

    invoke-virtual {v2}, LX/9B7;->b()I

    move-result v2

    .line 1451516
    iput v2, v0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->c:I

    .line 1451517
    :cond_0
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->k:LX/9B7;

    .line 1451518
    invoke-static {v1}, LX/9B7;->c(LX/1zt;)I

    move-result v2

    invoke-virtual {v0}, LX/9B7;->b()I

    move-result v3

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 p2, 0x0

    .line 1451519
    new-array v4, v1, [[I

    new-array v5, v0, [I

    const p0, 0x10100a1

    aput p0, v5, p2

    aput-object v5, v4, p2

    new-array v5, p2, [I

    aput-object v5, v4, v0

    .line 1451520
    new-array v5, v1, [I

    aput v2, v5, p2

    aput v3, v5, v0

    .line 1451521
    new-instance p0, Landroid/content/res/ColorStateList;

    invoke-direct {p0, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v2, p0

    .line 1451522
    move-object v0, v2

    .line 1451523
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1451524
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1451509
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1451508
    iget-object v0, p0, LX/9BI;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1451502
    iget-object v0, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    .line 1451503
    iget-object p0, v0, LX/9B3;->o:LX/0v6;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/9B3;->o:LX/0v6;

    .line 1451504
    iget-object p1, p0, LX/0v6;->b:Ljava/util/List;

    move-object p0, p1

    .line 1451505
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    .line 1451506
    iget-object p0, v0, LX/9B3;->d:LX/0tX;

    iget-object p1, v0, LX/9B3;->o:LX/0v6;

    invoke-virtual {p0, p1}, LX/0tX;->a(LX/0v6;)V

    .line 1451507
    :cond_0
    return-void
.end method

.method public final k_(I)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 1451494
    invoke-static {p0, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v0

    .line 1451495
    invoke-static {v0}, LX/9B3;->e(LX/1zt;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->k:LX/9B7;

    iget-object v2, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v2, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->x:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2}, LX/9B7;->a(LX/1zt;Ljava/util/HashMap;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1451496
    :goto_0
    return-object v0

    .line 1451497
    :cond_0
    iget-object v1, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->w:Landroid/content/res/Resources;

    const v2, 0x7f08199a

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1451498
    iget v5, v0, LX/1zt;->e:I

    move v5, v5

    .line 1451499
    iget-object v6, p0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v6, v6, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->x:Ljava/util/HashMap;

    invoke-static {v5, v6}, LX/9B7;->b(ILjava/util/HashMap;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 1451500
    iget-object v5, v0, LX/1zt;->f:Ljava/lang/String;

    move-object v0, v5

    .line 1451501
    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
