.class public final LX/ALO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ALN;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/CameraView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CameraView;)V
    .locals 0

    .prologue
    .line 1664789
    iput-object p1, p0, LX/ALO;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1664790
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1664791
    iget-object v0, p0, LX/ALO;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v0, p1}, Lcom/facebook/backstage/camera/CameraView;->setFlash(Lcom/facebook/backstage/camera/CameraView;Z)V

    .line 1664792
    :goto_0
    return-void

    .line 1664793
    :cond_0
    iget-object v0, p0, LX/ALO;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/backstage/ui/ToggleTextButton;->setToggleState(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
