.class public final LX/8kL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1396027
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1396028
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1396029
    :goto_0
    return v1

    .line 1396030
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1396031
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1396032
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1396033
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1396034
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1396035
    const-string v6, "animated_image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1396036
    invoke-static {p0, p1}, LX/8kN;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1396037
    :cond_2
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1396038
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1396039
    :cond_3
    const-string v6, "pack"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1396040
    invoke-static {p0, p1}, LX/8kK;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1396041
    :cond_4
    const-string v6, "thread_image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1396042
    invoke-static {p0, p1}, LX/8kN;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1396043
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1396044
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1396045
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1396046
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1396047
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1396048
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1396021
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1396022
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1396023
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/8kL;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1396024
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1396025
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1396026
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1396015
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1396016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1396017
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1396018
    invoke-static {p0, p1}, LX/8kL;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1396019
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1396020
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1395996
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395997
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1395998
    if-eqz v0, :cond_0

    .line 1395999
    const-string v1, "animated_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396000
    invoke-static {p0, v0, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396001
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1396002
    if-eqz v0, :cond_1

    .line 1396003
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396004
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1396005
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396006
    if-eqz v0, :cond_2

    .line 1396007
    const-string v1, "pack"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396008
    invoke-static {p0, v0, p2}, LX/8kK;->a(LX/15i;ILX/0nX;)V

    .line 1396009
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396010
    if-eqz v0, :cond_3

    .line 1396011
    const-string v1, "thread_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396012
    invoke-static {p0, v0, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396013
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1396014
    return-void
.end method
