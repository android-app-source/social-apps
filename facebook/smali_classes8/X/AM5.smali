.class public final LX/AM5;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/AM7;


# direct methods
.method public constructor <init>(LX/AM7;)V
    .locals 0

    .prologue
    .line 1665811
    iput-object p1, p0, LX/AM5;->a:LX/AM7;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 1665803
    iget-object v0, p0, LX/AM5;->a:LX/AM7;

    iget-object v0, v0, LX/AM7;->c:LX/AM2;

    if-eqz v0, :cond_0

    .line 1665804
    iget-object v0, p0, LX/AM5;->a:LX/AM7;

    iget-object v0, v0, LX/AM7;->c:LX/AM2;

    .line 1665805
    iget-object v1, v0, LX/AM2;->a:LX/ALb;

    .line 1665806
    iget-object v2, v1, LX/ALb;->e:LX/AM3;

    sget-object v3, LX/AM3;->PRESSING:LX/AM3;

    invoke-virtual {v2, v3}, LX/AM3;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1665807
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1665808
    :cond_1
    sget-object v2, LX/AM3;->PRESSING:LX/AM3;

    iput-object v2, v1, LX/ALb;->e:LX/AM3;

    .line 1665809
    iget-object v2, v1, LX/ALb;->c:LX/8YK;

    iget-object v3, v1, LX/ALb;->a:LX/8YL;

    invoke-virtual {v2, v3}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 1665810
    iget-object v2, v1, LX/ALb;->c:LX/8YK;

    const-wide v4, 0x3fe999999999999aL    # 0.8

    invoke-virtual {v2, v4, v5}, LX/8YK;->b(D)LX/8YK;

    goto :goto_0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 1665802
    return-void
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1665794
    iget-object v0, p0, LX/AM5;->a:LX/AM7;

    iget-object v0, v0, LX/AM7;->c:LX/AM2;

    if-eqz v0, :cond_0

    .line 1665795
    iget-object v0, p0, LX/AM5;->a:LX/AM7;

    iget-object v0, v0, LX/AM7;->c:LX/AM2;

    invoke-virtual {v0}, LX/AM2;->c()V

    .line 1665796
    :cond_0
    iget-object v0, p0, LX/AM5;->a:LX/AM7;

    iget-object v0, v0, LX/AM7;->c:LX/AM2;

    if-eqz v0, :cond_2

    .line 1665797
    iget-object v0, p0, LX/AM5;->a:LX/AM7;

    iget-object v0, v0, LX/AM7;->c:LX/AM2;

    .line 1665798
    iget-object p0, v0, LX/AM2;->a:LX/ALb;

    iget-object p0, p0, LX/ALb;->f:LX/AM8;

    if-eqz p0, :cond_1

    .line 1665799
    iget-object p0, v0, LX/AM2;->a:LX/ALb;

    iget-object p0, p0, LX/ALb;->f:LX/AM8;

    invoke-virtual {p0}, LX/AM8;->a()V

    .line 1665800
    :cond_1
    const/4 v0, 0x1

    .line 1665801
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
