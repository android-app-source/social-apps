.class public final LX/9tr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1564073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;LX/5Ph;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 1564023
    if-nez p1, :cond_0

    .line 1564024
    :goto_0
    return v0

    .line 1564025
    :cond_0
    invoke-interface {p1}, LX/5Ph;->a()LX/1yA;

    move-result-object v1

    const/4 v4, 0x0

    .line 1564026
    if-nez v1, :cond_1

    .line 1564027
    :goto_1
    move v1, v4

    .line 1564028
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1564029
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1564030
    const/4 v1, 0x1

    invoke-interface {p1}, LX/5Ph;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1564031
    const/4 v1, 0x2

    invoke-interface {p1}, LX/5Ph;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1564032
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1564033
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1564034
    :cond_1
    invoke-interface {v1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    .line 1564035
    invoke-interface {v1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1564036
    invoke-interface {v1}, LX/1yA;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1564037
    invoke-interface {v1}, LX/1yA;->n()LX/1yP;

    move-result-object v2

    .line 1564038
    if-nez v2, :cond_4

    .line 1564039
    const/4 v3, 0x0

    .line 1564040
    :goto_2
    move v8, v3

    .line 1564041
    invoke-interface {v1}, LX/1yA;->o()LX/0Px;

    move-result-object v9

    .line 1564042
    if-eqz v9, :cond_3

    .line 1564043
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    new-array v10, v2, [I

    move v3, v4

    .line 1564044
    :goto_3
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 1564045
    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    const/4 v11, 0x0

    .line 1564046
    if-nez v2, :cond_5

    .line 1564047
    :goto_4
    move v2, v11

    .line 1564048
    aput v2, v10, v3

    .line 1564049
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 1564050
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p0, v10, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 1564051
    :goto_5
    invoke-interface {v1}, LX/1yA;->w_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1564052
    invoke-interface {v1}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1564053
    const/16 v10, 0xf

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1564054
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 1564055
    const/4 v4, 0x5

    invoke-virtual {p0, v4, v6}, LX/186;->b(II)V

    .line 1564056
    const/16 v4, 0x9

    invoke-virtual {p0, v4, v7}, LX/186;->b(II)V

    .line 1564057
    const/16 v4, 0xa

    invoke-virtual {p0, v4, v8}, LX/186;->b(II)V

    .line 1564058
    const/16 v4, 0xc

    invoke-virtual {p0, v4, v2}, LX/186;->b(II)V

    .line 1564059
    const/16 v2, 0xd

    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1564060
    const/16 v2, 0xe

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 1564061
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1564062
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_3
    move v2, v4

    goto :goto_5

    .line 1564063
    :cond_4
    invoke-interface {v2}, LX/1yP;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1564064
    const/4 v8, 0x5

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1564065
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v3}, LX/186;->b(II)V

    .line 1564066
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1564067
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_2

    .line 1564068
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1564069
    const/4 v13, 0x1

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1564070
    invoke-virtual {p0, v11, v12}, LX/186;->b(II)V

    .line 1564071
    invoke-virtual {p0}, LX/186;->d()I

    move-result v11

    .line 1564072
    invoke-virtual {p0, v11}, LX/186;->d(I)V

    goto :goto_4
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 13

    .prologue
    .line 1563889
    if-nez p0, :cond_0

    .line 1563890
    const/4 v0, 0x0

    .line 1563891
    :goto_0
    return-object v0

    .line 1563892
    :cond_0
    new-instance v2, LX/4Vu;

    invoke-direct {v2}, LX/4Vu;-><init>()V

    .line 1563893
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1563894
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1563895
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1563896
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;

    .line 1563897
    if-nez v0, :cond_3

    .line 1563898
    const/4 v4, 0x0

    .line 1563899
    :goto_2
    move-object v0, v4

    .line 1563900
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1563901
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1563902
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1563903
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 1563904
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v0

    .line 1563905
    if-nez v0, :cond_7

    .line 1563906
    const/4 v1, 0x0

    .line 1563907
    :goto_3
    move-object v0, v1

    .line 1563908
    iput-object v0, v2, LX/4Vu;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1563909
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->c()LX/3Ab;

    move-result-object v0

    invoke-static {v0}, LX/9tr;->a(LX/3Ab;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1563910
    iput-object v0, v2, LX/4Vu;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1563911
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->d()J

    move-result-wide v0

    .line 1563912
    iput-wide v0, v2, LX/4Vu;->n:J

    .line 1563913
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->e()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;

    move-result-object v0

    .line 1563914
    if-nez v0, :cond_9

    .line 1563915
    const/4 v1, 0x0

    .line 1563916
    :goto_4
    move-object v0, v1

    .line 1563917
    iput-object v0, v2, LX/4Vu;->o:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 1563918
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->hn_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v0

    .line 1563919
    if-nez v0, :cond_a

    .line 1563920
    const/4 v1, 0x0

    .line 1563921
    :goto_5
    move-object v0, v1

    .line 1563922
    iput-object v0, v2, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1563923
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->hm_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v0

    .line 1563924
    if-nez v0, :cond_c

    .line 1563925
    const/4 v1, 0x0

    .line 1563926
    :goto_6
    move-object v0, v1

    .line 1563927
    iput-object v0, v2, LX/4Vu;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1563928
    invoke-virtual {v2}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    goto :goto_0

    .line 1563929
    :cond_3
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 1563930
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->a()LX/3Ab;

    move-result-object v5

    invoke-static {v5}, LX/9tr;->a(LX/3Ab;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 1563931
    iput-object v5, v4, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1563932
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->b()LX/1U8;

    move-result-object v5

    .line 1563933
    if-nez v5, :cond_4

    .line 1563934
    const/4 v6, 0x0

    .line 1563935
    :goto_7
    move-object v5, v6

    .line 1563936
    iput-object v5, v4, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1563937
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1563938
    iput-object v5, v4, LX/39x;->l:Ljava/lang/String;

    .line 1563939
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel$SourceModel;

    move-result-object v5

    .line 1563940
    if-nez v5, :cond_6

    .line 1563941
    const/4 v6, 0x0

    .line 1563942
    :goto_8
    move-object v5, v6

    .line 1563943
    iput-object v5, v4, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1563944
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->e()LX/0Px;

    move-result-object v5

    .line 1563945
    iput-object v5, v4, LX/39x;->p:LX/0Px;

    .line 1563946
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->ho_()Ljava/lang/String;

    move-result-object v5

    .line 1563947
    iput-object v5, v4, LX/39x;->t:Ljava/lang/String;

    .line 1563948
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel;->hp_()Ljava/lang/String;

    move-result-object v5

    .line 1563949
    iput-object v5, v4, LX/39x;->w:Ljava/lang/String;

    .line 1563950
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto/16 :goto_2

    .line 1563951
    :cond_4
    new-instance v6, LX/4XB;

    invoke-direct {v6}, LX/4XB;-><init>()V

    .line 1563952
    invoke-interface {v5}, LX/1U8;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1563953
    iput-object v7, v6, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1563954
    invoke-interface {v5}, LX/1U8;->c()LX/1f8;

    move-result-object v7

    .line 1563955
    if-nez v7, :cond_5

    .line 1563956
    const/4 v8, 0x0

    .line 1563957
    :goto_9
    move-object v7, v8

    .line 1563958
    iput-object v7, v6, LX/4XB;->H:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 1563959
    invoke-interface {v5}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v7

    .line 1563960
    iput-object v7, v6, LX/4XB;->T:Ljava/lang/String;

    .line 1563961
    invoke-interface {v5}, LX/1U8;->e()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/9tr;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1563962
    iput-object v7, v6, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1563963
    invoke-interface {v5}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/9tr;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1563964
    iput-object v7, v6, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1563965
    invoke-interface {v5}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/9tr;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1563966
    iput-object v7, v6, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1563967
    invoke-interface {v5}, LX/1U8;->j()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/9tr;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1563968
    iput-object v7, v6, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1563969
    invoke-virtual {v6}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    goto :goto_7

    .line 1563970
    :cond_5
    new-instance v8, LX/4ZN;

    invoke-direct {v8}, LX/4ZN;-><init>()V

    .line 1563971
    invoke-interface {v7}, LX/1f8;->a()D

    move-result-wide v10

    .line 1563972
    iput-wide v10, v8, LX/4ZN;->b:D

    .line 1563973
    invoke-interface {v7}, LX/1f8;->b()D

    move-result-wide v10

    .line 1563974
    iput-wide v10, v8, LX/4ZN;->c:D

    .line 1563975
    invoke-virtual {v8}, LX/4ZN;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v8

    goto :goto_9

    .line 1563976
    :cond_6
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 1563977
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AttachmentsModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 1563978
    iput-object v7, v6, LX/173;->f:Ljava/lang/String;

    .line 1563979
    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto/16 :goto_8

    .line 1563980
    :cond_7
    new-instance v1, LX/3dL;

    invoke-direct {v1}, LX/3dL;-><init>()V

    .line 1563981
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 1563982
    iput-object v3, v1, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1563983
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 1563984
    iput-object v3, v1, LX/3dL;->E:Ljava/lang/String;

    .line 1563985
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 1563986
    iput-object v3, v1, LX/3dL;->ag:Ljava/lang/String;

    .line 1563987
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;->e()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel$ProfilePictureModel;

    move-result-object v3

    .line 1563988
    if-nez v3, :cond_8

    .line 1563989
    const/4 v4, 0x0

    .line 1563990
    :goto_a
    move-object v3, v4

    .line 1563991
    iput-object v3, v1, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1563992
    invoke-virtual {v1}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    goto/16 :goto_3

    .line 1563993
    :cond_8
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 1563994
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1563995
    iput-object v0, v4, LX/2dc;->h:Ljava/lang/String;

    .line 1563996
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto :goto_a

    .line 1563997
    :cond_9
    new-instance v1, LX/4W5;

    invoke-direct {v1}, LX/4W5;-><init>()V

    .line 1563998
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$EditHistoryModel;->a()I

    move-result v3

    .line 1563999
    iput v3, v1, LX/4W5;->b:I

    .line 1564000
    invoke-virtual {v1}, LX/4W5;->a()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v1

    goto/16 :goto_4

    .line 1564001
    :cond_a
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    .line 1564002
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->b()Z

    move-result v3

    invoke-virtual {v1, v3}, LX/3dM;->c(Z)LX/3dM;

    .line 1564003
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->c()Z

    move-result v3

    invoke-virtual {v1, v3}, LX/3dM;->g(Z)LX/3dM;

    .line 1564004
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->d()Z

    move-result v3

    invoke-virtual {v1, v3}, LX/3dM;->j(Z)LX/3dM;

    .line 1564005
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 1564006
    iput-object v3, v1, LX/3dM;->y:Ljava/lang/String;

    .line 1564007
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->hq_()Ljava/lang/String;

    move-result-object v3

    .line 1564008
    iput-object v3, v1, LX/3dM;->D:Ljava/lang/String;

    .line 1564009
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->hr_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel$LikersModel;

    move-result-object v3

    .line 1564010
    if-nez v3, :cond_b

    .line 1564011
    const/4 v4, 0x0

    .line 1564012
    :goto_b
    move-object v3, v4

    .line 1564013
    invoke-virtual {v1, v3}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1564014
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto/16 :goto_5

    .line 1564015
    :cond_b
    new-instance v4, LX/3dN;

    invoke-direct {v4}, LX/3dN;-><init>()V

    .line 1564016
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel$LikersModel;->a()I

    move-result v0

    .line 1564017
    iput v0, v4, LX/3dN;->b:I

    .line 1564018
    invoke-virtual {v4}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    goto :goto_b

    .line 1564019
    :cond_c
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    .line 1564020
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 1564021
    iput-object v3, v1, LX/3dM;->D:Ljava/lang/String;

    .line 1564022
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto/16 :goto_6
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1563878
    if-nez p0, :cond_0

    .line 1563879
    const/4 v0, 0x0

    .line 1563880
    :goto_0
    return-object v0

    .line 1563881
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1563882
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1563883
    iput v1, v0, LX/2dc;->c:I

    .line 1563884
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1563885
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1563886
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1563887
    iput v1, v0, LX/2dc;->i:I

    .line 1563888
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 6

    .prologue
    .line 1563854
    if-nez p0, :cond_0

    .line 1563855
    const/4 v0, 0x0

    .line 1563856
    :goto_0
    return-object v0

    .line 1563857
    :cond_0
    new-instance v2, LX/4Z6;

    invoke-direct {v2}, LX/4Z6;-><init>()V

    .line 1563858
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1563859
    iput-object v0, v2, LX/4Z6;->b:Ljava/lang/String;

    .line 1563860
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1563861
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1563862
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1563863
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;

    .line 1563864
    if-nez v0, :cond_3

    .line 1563865
    const/4 v4, 0x0

    .line 1563866
    :goto_2
    move-object v0, v4

    .line 1563867
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1563868
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1563869
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1563870
    iput-object v0, v2, LX/4Z6;->c:LX/0Px;

    .line 1563871
    :cond_2
    invoke-virtual {v2}, LX/4Z6;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    goto :goto_0

    .line 1563872
    :cond_3
    new-instance v4, LX/4Vm;

    invoke-direct {v4}, LX/4Vm;-><init>()V

    .line 1563873
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->a()I

    move-result v5

    .line 1563874
    iput v5, v4, LX/4Vm;->b:I

    .line 1563875
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v5

    .line 1563876
    iput-object v5, v4, LX/4Vm;->c:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 1563877
    invoke-virtual {v4}, LX/4Vm;->a()Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    move-result-object v4

    goto :goto_2
.end method

.method public static a(LX/3Ab;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 12

    .prologue
    .line 1563789
    if-nez p0, :cond_0

    .line 1563790
    const/4 v0, 0x0

    .line 1563791
    :goto_0
    return-object v0

    .line 1563792
    :cond_0
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 1563793
    invoke-interface {p0}, LX/3Ab;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1563794
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1563795
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, LX/3Ab;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1563796
    invoke-interface {p0}, LX/3Ab;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ph;

    .line 1563797
    if-nez v0, :cond_3

    .line 1563798
    const/4 v4, 0x0

    .line 1563799
    :goto_2
    move-object v0, v4

    .line 1563800
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1563801
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1563802
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1563803
    iput-object v0, v2, LX/173;->e:LX/0Px;

    .line 1563804
    :cond_2
    invoke-interface {p0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    .line 1563805
    iput-object v0, v2, LX/173;->f:Ljava/lang/String;

    .line 1563806
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 1563807
    :cond_3
    new-instance v4, LX/4W6;

    invoke-direct {v4}, LX/4W6;-><init>()V

    .line 1563808
    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v5

    .line 1563809
    if-nez v5, :cond_4

    .line 1563810
    const/4 v6, 0x0

    .line 1563811
    :goto_3
    move-object v5, v6

    .line 1563812
    iput-object v5, v4, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1563813
    invoke-interface {v0}, LX/5Ph;->b()I

    move-result v5

    .line 1563814
    iput v5, v4, LX/4W6;->c:I

    .line 1563815
    invoke-interface {v0}, LX/5Ph;->c()I

    move-result v5

    .line 1563816
    iput v5, v4, LX/4W6;->d:I

    .line 1563817
    invoke-virtual {v4}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v4

    goto :goto_2

    .line 1563818
    :cond_4
    new-instance v8, LX/170;

    invoke-direct {v8}, LX/170;-><init>()V

    .line 1563819
    invoke-interface {v5}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 1563820
    iput-object v6, v8, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1563821
    invoke-interface {v5}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v6

    .line 1563822
    iput-object v6, v8, LX/170;->o:Ljava/lang/String;

    .line 1563823
    invoke-interface {v5}, LX/1yA;->v_()Ljava/lang/String;

    move-result-object v6

    .line 1563824
    iput-object v6, v8, LX/170;->A:Ljava/lang/String;

    .line 1563825
    invoke-interface {v5}, LX/1yA;->n()LX/1yP;

    move-result-object v6

    .line 1563826
    if-nez v6, :cond_7

    .line 1563827
    const/4 v7, 0x0

    .line 1563828
    :goto_4
    move-object v6, v7

    .line 1563829
    iput-object v6, v8, LX/170;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1563830
    invoke-interface {v5}, LX/1yA;->o()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 1563831
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1563832
    const/4 v6, 0x0

    move v7, v6

    :goto_5
    invoke-interface {v5}, LX/1yA;->o()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_5

    .line 1563833
    invoke-interface {v5}, LX/1yA;->o()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    .line 1563834
    if-nez v6, :cond_8

    .line 1563835
    const/4 v10, 0x0

    .line 1563836
    :goto_6
    move-object v6, v10

    .line 1563837
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1563838
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_5

    .line 1563839
    :cond_5
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1563840
    iput-object v6, v8, LX/170;->N:LX/0Px;

    .line 1563841
    :cond_6
    invoke-interface {v5}, LX/1yA;->w_()Ljava/lang/String;

    move-result-object v6

    .line 1563842
    iput-object v6, v8, LX/170;->X:Ljava/lang/String;

    .line 1563843
    invoke-interface {v5}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v6

    .line 1563844
    iput-object v6, v8, LX/170;->Y:Ljava/lang/String;

    .line 1563845
    invoke-virtual {v8}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    goto :goto_3

    .line 1563846
    :cond_7
    new-instance v7, LX/4XY;

    invoke-direct {v7}, LX/4XY;-><init>()V

    .line 1563847
    invoke-interface {v6}, LX/1yP;->e()Ljava/lang/String;

    move-result-object v9

    .line 1563848
    iput-object v9, v7, LX/4XY;->ag:Ljava/lang/String;

    .line 1563849
    invoke-virtual {v7}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    goto :goto_4

    .line 1563850
    :cond_8
    new-instance v10, LX/4YY;

    invoke-direct {v10}, LX/4YY;-><init>()V

    .line 1563851
    invoke-virtual {v6}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a()Ljava/lang/String;

    move-result-object v11

    .line 1563852
    iput-object v11, v10, LX/4YY;->d:Ljava/lang/String;

    .line 1563853
    invoke-virtual {v10}, LX/4YY;->a()Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    move-result-object v10

    goto :goto_6
.end method

.method public static a(LX/5sd;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 9

    .prologue
    .line 1563720
    if-nez p0, :cond_0

    .line 1563721
    const/4 v0, 0x0

    .line 1563722
    :goto_0
    return-object v0

    .line 1563723
    :cond_0
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 1563724
    invoke-interface {p0}, LX/5sd;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1563725
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1563726
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, LX/5sd;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1563727
    invoke-interface {p0}, LX/5sd;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;

    .line 1563728
    if-nez v0, :cond_3

    .line 1563729
    const/4 v4, 0x0

    .line 1563730
    :goto_2
    move-object v0, v4

    .line 1563731
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1563732
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1563733
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1563734
    iput-object v0, v2, LX/173;->c:LX/0Px;

    .line 1563735
    :cond_2
    invoke-interface {p0}, LX/5sd;->a()Ljava/lang/String;

    move-result-object v0

    .line 1563736
    iput-object v0, v2, LX/173;->f:Ljava/lang/String;

    .line 1563737
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 1563738
    :cond_3
    new-instance v4, LX/4Wv;

    invoke-direct {v4}, LX/4Wv;-><init>()V

    .line 1563739
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v5

    .line 1563740
    if-nez v5, :cond_4

    .line 1563741
    const/4 v6, 0x0

    .line 1563742
    :goto_3
    move-object v5, v6

    .line 1563743
    iput-object v5, v4, LX/4Wv;->b:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    .line 1563744
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->b()I

    move-result v5

    .line 1563745
    iput v5, v4, LX/4Wv;->c:I

    .line 1563746
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->c()I

    move-result v5

    .line 1563747
    iput v5, v4, LX/4Wv;->d:I

    .line 1563748
    invoke-virtual {v4}, LX/4Wv;->a()Lcom/facebook/graphql/model/GraphQLImageAtRange;

    move-result-object v4

    goto :goto_2

    .line 1563749
    :cond_4
    new-instance v6, LX/4W7;

    invoke-direct {v6}, LX/4W7;-><init>()V

    .line 1563750
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1563751
    iput-object v7, v6, LX/4W7;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1563752
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;->b()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;

    move-result-object v7

    .line 1563753
    if-nez v7, :cond_5

    .line 1563754
    const/4 v8, 0x0

    .line 1563755
    :goto_4
    move-object v7, v8

    .line 1563756
    iput-object v7, v6, LX/4W7;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1563757
    invoke-virtual {v6}, LX/4W7;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v6

    goto :goto_3

    .line 1563758
    :cond_5
    new-instance v8, LX/2dc;

    invoke-direct {v8}, LX/2dc;-><init>()V

    .line 1563759
    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1563760
    iput-object v5, v8, LX/2dc;->h:Ljava/lang/String;

    .line 1563761
    invoke-virtual {v8}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    goto :goto_4
.end method

.method public static a(LX/5sZ;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1563762
    if-nez p0, :cond_1

    .line 1563763
    :cond_0
    :goto_0
    return-object v2

    .line 1563764
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1563765
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1563766
    if-nez p0, :cond_3

    .line 1563767
    :goto_1
    move v1, v1

    .line 1563768
    if-eqz v1, :cond_0

    .line 1563769
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1563770
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1563771
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1563772
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1563773
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1563774
    const-string v1, "ReactionConversionHelper.getLinkableUtilAddLinksGraphQL"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1563775
    :cond_2
    new-instance v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1563776
    :cond_3
    invoke-interface {p0}, LX/5sZ;->b()LX/0Px;

    move-result-object v4

    .line 1563777
    if-eqz v4, :cond_5

    .line 1563778
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v3

    new-array v5, v3, [I

    move v3, v1

    .line 1563779
    :goto_2
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 1563780
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5Ph;

    invoke-static {v0, v1}, LX/9tr;->a(LX/186;LX/5Ph;)I

    move-result v1

    aput v1, v5, v3

    .line 1563781
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1563782
    :cond_4
    invoke-virtual {v0, v5, v6}, LX/186;->a([IZ)I

    move-result v1

    .line 1563783
    :cond_5
    invoke-interface {p0}, LX/5sZ;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1563784
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1563785
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1563786
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1563787
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1563788
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method
