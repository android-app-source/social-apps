.class public final LX/9dt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Ne;


# instance fields
.field public final synthetic a:LX/9dx;


# direct methods
.method public constructor <init>(LX/9dx;)V
    .locals 0

    .prologue
    .line 1518832
    iput-object p1, p0, LX/9dt;->a:LX/9dx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1518833
    iget-object v0, p0, LX/9dt;->a:LX/9dx;

    .line 1518834
    iput-boolean v2, v0, LX/9dx;->e:Z

    .line 1518835
    iget-object v0, p0, LX/9dt;->a:LX/9dx;

    iget-object v0, v0, LX/9dx;->g:LX/9eh;

    const/4 v4, 0x0

    .line 1518836
    iget-object v1, v0, LX/9eh;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->o:Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

    .line 1518837
    iget v3, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->c:I

    .line 1518838
    iget-object v1, v0, LX/9eh;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->j:LX/9dx;

    .line 1518839
    iget-boolean v3, v1, LX/9dx;->f:Z

    move v1, v3

    .line 1518840
    if-eqz v1, :cond_0

    .line 1518841
    iget-object v1, v0, LX/9eh;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->o:Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/facebook/photos/creativeediting/analytics/DoodleOnPhotosLoggingParams;->a:Z

    .line 1518842
    :cond_0
    iget-object v1, v0, LX/9eh;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->j:LX/9dx;

    invoke-virtual {v1}, LX/9dx;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1518843
    iget-object v1, v0, LX/9eh;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1518844
    iget-object v1, v0, LX/9eh;->a:LX/9ei;

    iget-object v1, v1, LX/9ei;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1518845
    :cond_1
    iget-object v0, p0, LX/9dt;->a:LX/9dx;

    iget-object v1, p0, LX/9dt;->a:LX/9dx;

    iget-object v1, v1, LX/9dx;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-static {v0, v2, v1}, LX/9dx;->a$redex0(LX/9dx;ILandroid/view/View;)V

    .line 1518846
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1518847
    iget-object v0, p0, LX/9dt;->a:LX/9dx;

    const/4 v1, 0x0

    iget-object v2, p0, LX/9dt;->a:LX/9dx;

    iget-object v2, v2, LX/9dx;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-static {v0, v1, v2}, LX/9dx;->a$redex0(LX/9dx;ILandroid/view/View;)V

    .line 1518848
    return-void
.end method
