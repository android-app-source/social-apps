.class public abstract LX/93H;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:Landroid/widget/TextView;

.field public k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/drawee/view/DraweeView;

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:Landroid/graphics/drawable/Drawable;

.field public o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:Landroid/graphics/drawable/Drawable;

.field public u:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1433560
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1433561
    const/4 p1, 0x0

    .line 1433562
    invoke-virtual {p0}, LX/93H;->getLayoutResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1433563
    const-class v0, LX/93H;

    invoke-static {v0, p0}, LX/93H;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1433564
    invoke-virtual {p0}, LX/93H;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1433565
    const v1, 0x7f0b0c58

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/93H;->p:I

    .line 1433566
    const v1, 0x7f0b0c59

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/93H;->q:I

    .line 1433567
    const v1, 0x7f0b0c5a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/93H;->r:I

    .line 1433568
    const v1, 0x7f0b0c5b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/93H;->s:I

    .line 1433569
    const v1, 0x7f020f12

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/93H;->t:Landroid/graphics/drawable/Drawable;

    .line 1433570
    const v1, 0x7f02145d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/93H;->m:Landroid/graphics/drawable/Drawable;

    .line 1433571
    const v1, 0x7f02145d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/93H;->n:Landroid/graphics/drawable/Drawable;

    .line 1433572
    iget-object v0, p0, LX/93H;->m:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, LX/93H;->p:I

    iget v3, p0, LX/93H;->p:I

    invoke-direct {v1, p1, p1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1433573
    iget-object v0, p0, LX/93H;->n:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, LX/93H;->q:I

    iget v3, p0, LX/93H;->q:I

    invoke-direct {v1, p1, p1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1433574
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1433575
    const v0, 0x7f0d0aa7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/93H;->j:Landroid/widget/TextView;

    .line 1433576
    const v0, 0x7f0d0ab4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getOptionalView(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/93H;->k:LX/0am;

    .line 1433577
    const v0, 0x7f0d0ab5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, LX/93H;->l:Lcom/facebook/drawee/view/DraweeView;

    .line 1433578
    iget v0, p0, LX/93H;->p:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1433579
    iget v0, p0, LX/93H;->p:I

    invoke-virtual {p0, v0}, LX/93H;->setMinimumHeight(I)V

    .line 1433580
    invoke-virtual {p0}, LX/93H;->d()V

    .line 1433581
    return-void
.end method

.method public static a(LX/93H;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;I)V
    .locals 4
    .param p0    # LX/93H;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1433596
    if-nez p1, :cond_0

    .line 1433597
    :goto_0
    return-void

    .line 1433598
    :cond_0
    iget v0, p0, LX/93H;->p:I

    sub-int/2addr v0, p3

    div-int/lit8 v0, v0, 0x2

    .line 1433599
    invoke-virtual {p0}, LX/93H;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, LX/93H;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, LX/93H;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, LX/93H;->setPadding(IIII)V

    .line 1433600
    iget v1, p0, LX/93H;->s:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1433601
    invoke-virtual {p0, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1433602
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1433603
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1433604
    invoke-virtual {p0, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1433591
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1433592
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1433593
    :goto_0
    return-void

    .line 1433594
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1433595
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/93H;

    const/16 p0, 0x509

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    iput-object v1, p1, LX/93H;->o:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Z)LX/93H;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1433605
    iget-object v2, p0, LX/93H;->j:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, LX/93H;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f021a25

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v1, v1, v0, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1433606
    return-object p0

    :cond_0
    move-object v0, v1

    .line 1433607
    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1433583
    iget-object v0, p0, LX/93H;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1433584
    iget-object v0, p0, LX/93H;->k:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1433585
    iget-object v0, p0, LX/93H;->k:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1433586
    :cond_0
    iget-object v0, p0, LX/93H;->l:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 1433587
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/93H;->a(Z)LX/93H;

    .line 1433588
    const v0, 0x7f020309

    invoke-virtual {p0, v0}, LX/93H;->setBackgroundResource(I)V

    .line 1433589
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/93H;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1433590
    return-void
.end method

.method public getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1433582
    const-string v0, "composer"

    return-object v0
.end method

.method public abstract getLayoutResourceId()I
.end method
