.class public final LX/8yd;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8ye;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I

.field public final synthetic d:LX/8ye;


# direct methods
.method public constructor <init>(LX/8ye;)V
    .locals 1

    .prologue
    .line 1426239
    iput-object p1, p0, LX/8yd;->d:LX/8ye;

    .line 1426240
    move-object v0, p1

    .line 1426241
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1426242
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1426243
    const-string v0, "FbProgressBarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426244
    if-ne p0, p1, :cond_1

    .line 1426245
    :cond_0
    :goto_0
    return v0

    .line 1426246
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1426247
    goto :goto_0

    .line 1426248
    :cond_3
    check-cast p1, LX/8yd;

    .line 1426249
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1426250
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1426251
    if-eq v2, v3, :cond_0

    .line 1426252
    iget-object v2, p0, LX/8yd;->a:LX/1dc;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8yd;->a:LX/1dc;

    iget-object v3, p1, LX/8yd;->a:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1426253
    goto :goto_0

    .line 1426254
    :cond_5
    iget-object v2, p1, LX/8yd;->a:LX/1dc;

    if-nez v2, :cond_4

    .line 1426255
    :cond_6
    iget v2, p0, LX/8yd;->b:I

    iget v3, p1, LX/8yd;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1426256
    goto :goto_0

    .line 1426257
    :cond_7
    iget v2, p0, LX/8yd;->c:I

    iget v3, p1, LX/8yd;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1426258
    goto :goto_0
.end method
