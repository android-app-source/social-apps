.class public LX/8ya;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8ya;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1426066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426067
    return-void
.end method

.method public static a(LX/0QB;)LX/8ya;
    .locals 3

    .prologue
    .line 1426068
    sget-object v0, LX/8ya;->a:LX/8ya;

    if-nez v0, :cond_1

    .line 1426069
    const-class v1, LX/8ya;

    monitor-enter v1

    .line 1426070
    :try_start_0
    sget-object v0, LX/8ya;->a:LX/8ya;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1426071
    if-eqz v2, :cond_0

    .line 1426072
    :try_start_1
    new-instance v0, LX/8ya;

    invoke-direct {v0}, LX/8ya;-><init>()V

    .line 1426073
    move-object v0, v0

    .line 1426074
    sput-object v0, LX/8ya;->a:LX/8ya;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1426075
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1426076
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1426077
    :cond_1
    sget-object v0, LX/8ya;->a:LX/8ya;

    return-object v0

    .line 1426078
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1426079
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
