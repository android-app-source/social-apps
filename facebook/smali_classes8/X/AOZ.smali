.class public LX/AOZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANo;


# instance fields
.field public final a:LX/6Jt;

.field public final b:LX/BVQ;

.field private final c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private final d:LX/AOX;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "LX/7So;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;LX/BVQ;)V
    .locals 4
    .param p1    # LX/6Jt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1669074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1669075
    iput-object p1, p0, LX/AOZ;->a:LX/6Jt;

    .line 1669076
    iput-object p4, p0, LX/AOZ;->b:LX/BVQ;

    .line 1669077
    iput-object p3, p0, LX/AOZ;->f:Landroid/content/Context;

    .line 1669078
    iput-object p2, p0, LX/AOZ;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1669079
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1669080
    new-instance v1, Landroid/util/Pair;

    const-string v3, "Off"

    new-instance p1, LX/7So;

    invoke-direct {p1}, LX/7So;-><init>()V

    invoke-direct {v1, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669081
    new-instance v1, Landroid/util/Pair;

    const-string v3, "Crayon"

    new-instance p1, LX/7So;

    const p2, 0x7f070018

    invoke-static {p0, p2}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p2

    const p4, 0x7f070019

    invoke-static {p0, p4}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p1, p2, p4}, LX/7So;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669082
    new-instance v1, Landroid/util/Pair;

    const-string v3, "DarkMatter"

    new-instance p1, LX/7So;

    const p2, 0x7f07001d

    invoke-static {p0, p2}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p2

    const p4, 0x7f07001e

    invoke-static {p0, p4}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p1, p2, p4}, LX/7So;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669083
    new-instance v1, Landroid/util/Pair;

    const-string v3, "Fingerprint"

    new-instance p1, LX/7So;

    const p2, 0x7f07002f

    invoke-static {p0, p2}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p2

    const p4, 0x7f070030

    invoke-static {p0, p4}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p1, p2, p4}, LX/7So;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669084
    new-instance v1, Landroid/util/Pair;

    const-string v3, "Gothic"

    new-instance p1, LX/7So;

    const p2, 0x7f070031

    invoke-static {p0, p2}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p2

    const p4, 0x7f070032

    invoke-static {p0, p4}, LX/AOZ;->a(LX/AOZ;I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p1, p2, p4}, LX/7So;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669085
    move-object v0, v0

    .line 1669086
    iput-object v0, p0, LX/AOZ;->e:Ljava/util/List;

    .line 1669087
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    move v1, v2

    .line 1669088
    :goto_0
    iget-object v0, p0, LX/AOZ;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1669089
    iget-object v0, p0, LX/AOZ;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669090
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1669091
    :cond_0
    new-instance v0, LX/AOX;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    new-instance v3, LX/AOY;

    invoke-direct {v3, p0}, LX/AOY;-><init>(LX/AOZ;)V

    invoke-direct {v0, p3, v1, v2, v3}, LX/AOX;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    iput-object v0, p0, LX/AOZ;->d:LX/AOX;

    .line 1669092
    return-void
.end method

.method public static a(LX/AOZ;I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 1669093
    iget-object v0, p0, LX/AOZ;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 1669094
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 1669095
    new-instance v2, Ljava/io/File;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1669096
    const v1, 0x8000

    new-array v3, v1, [B

    .line 1669097
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x0

    .line 1669098
    :goto_0
    :try_start_1
    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 1669099
    const/4 v6, 0x0

    invoke-virtual {v4, v3, v6, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1669100
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1669101
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    .line 1669102
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1669103
    :goto_3
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1669104
    :cond_0
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1669105
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    goto :goto_3

    :catch_2
    move-exception v3

    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1669106
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1669107
    iget-object v0, p0, LX/AOZ;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AOZ;->d:LX/AOX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1669108
    return-void
.end method

.method public final b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1669109
    iget-object v0, p0, LX/AOZ;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1669110
    return-void
.end method
