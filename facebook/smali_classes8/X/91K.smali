.class public LX/91K;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/91J;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1430421
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/91K;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/minutiae/common/GenericErrorComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430375
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1430376
    iput-object p1, p0, LX/91K;->b:LX/0Ot;

    .line 1430377
    return-void
.end method

.method public static a(LX/0QB;)LX/91K;
    .locals 4

    .prologue
    .line 1430410
    const-class v1, LX/91K;

    monitor-enter v1

    .line 1430411
    :try_start_0
    sget-object v0, LX/91K;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1430412
    sput-object v2, LX/91K;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1430413
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430414
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1430415
    new-instance v3, LX/91K;

    const/16 p0, 0x199f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/91K;-><init>(LX/0Ot;)V

    .line 1430416
    move-object v0, v3

    .line 1430417
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1430418
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430419
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1430420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1430409
    const v0, 0x5533b2e4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1430397
    check-cast p2, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    .line 1430398
    iget-object v0, p0, LX/91K;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->a:Ljava/lang/CharSequence;

    iget-object v1, p2, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->b:LX/1dc;

    iget-object v2, p2, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->c:Ljava/lang/CharSequence;

    iget v3, p2, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->d:I

    const/4 p0, 0x2

    const/16 p2, 0x8

    .line 1430399
    if-nez v2, :cond_0

    .line 1430400
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080040

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1430401
    :cond_0
    if-nez v0, :cond_1

    .line 1430402
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08003c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1430403
    :cond_1
    if-nez v1, :cond_2

    .line 1430404
    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v4

    const v5, 0x7f021120

    invoke-virtual {v4, v5}, LX/1nm;->h(I)LX/1nm;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 1430405
    :cond_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v5

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b189a

    invoke-interface {v5, p2, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b189c

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a0051

    invoke-virtual {v5, v6}, LX/1ne;->m(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b189b

    invoke-interface {v5, p2, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const p0, 0x7f021121

    invoke-virtual {v6, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const p0, 0x7f0b189d

    invoke-interface {v6, p2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const p0, 0x7f0b18a0

    invoke-virtual {v6, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const p0, 0x7f0a0052

    invoke-virtual {v6, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 p0, 0x4

    const p2, 0x7f0b189f

    invoke-interface {v6, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1430406
    const v5, 0x5533b2e4

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1430407
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1430408
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1430386
    invoke-static {}, LX/1dS;->b()V

    .line 1430387
    iget v0, p1, LX/1dQ;->b:I

    .line 1430388
    packed-switch v0, :pswitch_data_0

    .line 1430389
    :goto_0
    return-object v2

    .line 1430390
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1430391
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1430392
    check-cast v1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    .line 1430393
    iget-object p1, p0, LX/91K;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;->g:Landroid/view/View$OnClickListener;

    .line 1430394
    if-eqz p1, :cond_0

    .line 1430395
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1430396
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5533b2e4
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/91J;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1430378
    new-instance v1, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;-><init>(LX/91K;)V

    .line 1430379
    sget-object v2, LX/91K;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/91J;

    .line 1430380
    if-nez v2, :cond_0

    .line 1430381
    new-instance v2, LX/91J;

    invoke-direct {v2}, LX/91J;-><init>()V

    .line 1430382
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/91J;->a$redex0(LX/91J;LX/1De;IILcom/facebook/composer/minutiae/common/GenericErrorComponent$GenericErrorComponentImpl;)V

    .line 1430383
    move-object v1, v2

    .line 1430384
    move-object v0, v1

    .line 1430385
    return-object v0
.end method
