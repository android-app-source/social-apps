.class public final LX/8iR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field public final synthetic a:Lcom/facebook/sounds/configurator/AudioConfigurator;


# direct methods
.method public constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 0

    .prologue
    .line 1391510
    iput-object p1, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1391494
    iget-object v0, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v0, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    if-nez v0, :cond_1

    .line 1391495
    :cond_0
    :goto_0
    return v3

    .line 1391496
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v0, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1391497
    :goto_1
    const/4 v2, 0x0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1391498
    iget-object v1, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391499
    iget v2, v1, LX/8iO;->c:F

    move v1, v2

    .line 1391500
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    .line 1391501
    iget-object v1, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391502
    iput v0, v1, LX/8iO;->c:F

    .line 1391503
    iget-object v0, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391504
    invoke-static {v0, v1}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;LX/8iO;)V

    .line 1391505
    iget-object v0, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-static {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->e(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391506
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 1391507
    iget-object v0, p0, LX/8iR;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    .line 1391508
    invoke-static {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->c$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391509
    goto :goto_0

    :catch_0
    move v0, v1

    goto :goto_1
.end method
