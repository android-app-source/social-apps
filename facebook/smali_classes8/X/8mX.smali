.class public final enum LX/8mX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8mX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8mX;

.field public static final enum AVAILABLE:LX/8mX;

.field public static final enum FEATURED:LX/8mX;

.field public static final enum OWNED:LX/8mX;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1398935
    new-instance v0, LX/8mX;

    const-string v1, "FEATURED"

    invoke-direct {v0, v1, v2}, LX/8mX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8mX;->FEATURED:LX/8mX;

    .line 1398936
    new-instance v0, LX/8mX;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v3}, LX/8mX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8mX;->AVAILABLE:LX/8mX;

    .line 1398937
    new-instance v0, LX/8mX;

    const-string v1, "OWNED"

    invoke-direct {v0, v1, v4}, LX/8mX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8mX;->OWNED:LX/8mX;

    .line 1398938
    const/4 v0, 0x3

    new-array v0, v0, [LX/8mX;

    sget-object v1, LX/8mX;->FEATURED:LX/8mX;

    aput-object v1, v0, v2

    sget-object v1, LX/8mX;->AVAILABLE:LX/8mX;

    aput-object v1, v0, v3

    sget-object v1, LX/8mX;->OWNED:LX/8mX;

    aput-object v1, v0, v4

    sput-object v0, LX/8mX;->$VALUES:[LX/8mX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1398939
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8mX;
    .locals 1

    .prologue
    .line 1398940
    const-class v0, LX/8mX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8mX;

    return-object v0
.end method

.method public static values()[LX/8mX;
    .locals 1

    .prologue
    .line 1398941
    sget-object v0, LX/8mX;->$VALUES:[LX/8mX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8mX;

    return-object v0
.end method
