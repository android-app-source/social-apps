.class public final LX/9Gy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/9H0;


# direct methods
.method public constructor <init>(LX/9H0;)V
    .locals 0

    .prologue
    .line 1460627
    iput-object p1, p0, LX/9Gy;->a:LX/9H0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 1460599
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-nez v1, :cond_0

    .line 1460600
    iget-object v1, p0, LX/9Gy;->a:LX/9H0;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 1460601
    iput v2, v1, LX/9H0;->o:I

    .line 1460602
    iget-object v1, p0, LX/9Gy;->a:LX/9H0;

    iget-object v2, p0, LX/9Gy;->a:LX/9H0;

    .line 1460603
    const/4 v4, 0x0

    .line 1460604
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    .line 1460605
    :goto_0
    move v3, v3

    .line 1460606
    move v2, v3

    .line 1460607
    iput-boolean v2, v1, LX/9H0;->n:Z

    .line 1460608
    :cond_0
    iget-object v1, p0, LX/9Gy;->a:LX/9H0;

    iget v1, v1, LX/9H0;->o:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, v0, :cond_1

    .line 1460609
    :goto_1
    return v0

    .line 1460610
    :cond_1
    iget-object v0, p0, LX/9Gy;->a:LX/9H0;

    iget-boolean v0, v0, LX/9H0;->n:Z

    if-eqz v0, :cond_2

    .line 1460611
    iget-object v0, p0, LX/9Gy;->a:LX/9H0;

    iget-object v0, v0, LX/9H0;->i:LX/20b;

    iget-object v1, p0, LX/9Gy;->a:LX/9H0;

    iget-object v1, v1, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v0, v1, p2}, LX/20b;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1460612
    :cond_2
    iget-object v0, p0, LX/9Gy;->a:LX/9H0;

    .line 1460613
    invoke-static {v0, p2}, LX/9H0;->a$redex0(LX/9H0;Landroid/view/MotionEvent;)Z

    move-result v1

    move v0, v1

    .line 1460614
    goto :goto_1

    .line 1460615
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    iget-object v5, v2, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v5}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getTotalPaddingLeft()I

    move-result v5

    sub-int/2addr v3, v5

    .line 1460616
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    iget-object v6, v2, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v6}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getTotalPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    .line 1460617
    iget-object v6, v2, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v6}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getScrollX()I

    move-result v6

    add-int/2addr v3, v6

    .line 1460618
    iget-object v6, v2, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v6}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getScrollY()I

    move-result v6

    add-int/2addr v5, v6

    .line 1460619
    iget-object v6, v2, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v6}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v6

    .line 1460620
    iget-object v7, v2, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v7}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v7

    .line 1460621
    invoke-virtual {v6, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v5

    .line 1460622
    int-to-float v3, v3

    invoke-virtual {v6, v5, v3}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v3

    .line 1460623
    add-int/lit8 v5, v3, -0x3

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1460624
    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    add-int/lit8 v3, v3, 0x3

    invoke-static {v6, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1460625
    const-class v6, LX/8qw;

    invoke-virtual {v7, v5, v3, v6}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [LX/8qw;

    .line 1460626
    if-eqz v3, :cond_4

    array-length v3, v3

    if-lez v3, :cond_4

    const/4 v3, 0x1

    goto/16 :goto_0

    :cond_4
    move v3, v4

    goto/16 :goto_0
.end method
