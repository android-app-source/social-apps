.class public final LX/910;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8zz;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)V
    .locals 0

    .prologue
    .line 1429992
    iput-object p1, p0, LX/910;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1429993
    iget-object v0, p0, LX/910;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->d:LX/8zs;

    .line 1429994
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1429995
    const/4 v1, 0x0

    .line 1429996
    :goto_0
    if-nez v1, :cond_1

    .line 1429997
    iget-object v1, v0, LX/8zs;->b:LX/8zj;

    iget-object v2, v0, LX/8zs;->h:LX/0Px;

    invoke-virtual {v1, v2}, LX/8zj;->a(LX/0Px;)V

    .line 1429998
    :goto_1
    return-void

    .line 1429999
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1430000
    :cond_1
    iget-object v2, v0, LX/8zs;->h:LX/0Px;

    .line 1430001
    iget-object v3, v0, LX/8zs;->a:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/8zs;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1430002
    iget-object v2, v0, LX/8zs;->b:LX/8zj;

    .line 1430003
    iget-object v3, v2, LX/8zj;->a:LX/0Px;

    move-object v2, v3

    .line 1430004
    :cond_2
    iput-object v1, v0, LX/8zs;->a:Ljava/lang/String;

    .line 1430005
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1430006
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/92e;

    .line 1430007
    iget-object p0, v2, LX/92e;->a:LX/5LG;

    move-object p0, p0

    .line 1430008
    if-eqz p0, :cond_3

    .line 1430009
    iget-object p0, v2, LX/92e;->a:LX/5LG;

    move-object p0, p0

    .line 1430010
    invoke-interface {p0}, LX/5LG;->n()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 1430011
    iget-object p0, v2, LX/92e;->a:LX/5LG;

    move-object p0, p0

    .line 1430012
    invoke-interface {p0}, LX/5LG;->n()Ljava/lang/String;

    move-result-object p0

    iget-object p1, v0, LX/8zs;->f:Ljava/util/Locale;

    invoke-virtual {p0, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1430013
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1430014
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1430015
    iget-object v2, v0, LX/8zs;->b:LX/8zj;

    invoke-virtual {v2, v1}, LX/8zj;->a(LX/0Px;)V

    goto :goto_1
.end method
