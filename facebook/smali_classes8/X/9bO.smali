.class public final LX/9bO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/FragmentActivity;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Landroid/support/v4/app/FragmentActivity;J)V
    .locals 1

    .prologue
    .line 1514874
    iput-object p1, p0, LX/9bO;->c:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iput-object p2, p0, LX/9bO;->a:Landroid/support/v4/app/FragmentActivity;

    iput-wide p3, p0, LX/9bO;->b:J

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1514875
    sget-object v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->c:Ljava/lang/Class;

    const-string v1, "Failed to fetch FacebookPhoto by fbid"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1514876
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1514877
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1514878
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;

    .line 1514879
    if-eqz v0, :cond_0

    .line 1514880
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1514881
    if-eqz v1, :cond_0

    .line 1514882
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1514883
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1514884
    :cond_0
    :goto_0
    return-void

    .line 1514885
    :cond_1
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v0, v1

    .line 1514886
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1514887
    iget-object v1, p0, LX/9bO;->c:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v2, p0, LX/9bO;->a:Landroid/support/v4/app/FragmentActivity;

    iget-wide v4, p0, LX/9bO;->b:J

    invoke-virtual {v1, v0, v2, v4, v5}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Landroid/support/v4/app/FragmentActivity;J)V

    goto :goto_0
.end method
