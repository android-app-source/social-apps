.class public LX/ALB;
.super LX/5fj;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1664632
    const-class v0, LX/ALB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ALB;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1664633
    invoke-direct {p0}, LX/5fj;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/5fS;LX/5fO;LX/5fO;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1664634
    invoke-virtual {p1}, LX/5fS;->B()Ljava/util/List;

    move-result-object v3

    .line 1664635
    new-instance v4, Ljava/util/HashSet;

    invoke-virtual {p1}, LX/5fS;->C()Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1664636
    invoke-virtual {p1}, LX/5fS;->D()Ljava/util/List;

    move-result-object v0

    .line 1664637
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1664638
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1664639
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1664640
    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1664641
    :cond_0
    new-instance v0, LX/AL9;

    invoke-direct {v0, p0}, LX/AL9;-><init>(LX/ALB;)V

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1664642
    sget-object v0, LX/5fO;->HIGH:LX/5fO;

    invoke-virtual {p2, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1664643
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    move-object v1, v0

    .line 1664644
    :goto_0
    iget v0, v1, Landroid/hardware/Camera$Size;->height:I

    int-to-float v0, v0

    iget v6, v1, Landroid/hardware/Camera$Size;->width:I

    int-to-float v6, v6

    div-float v6, v0, v6

    .line 1664645
    if-eqz v3, :cond_8

    .line 1664646
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664647
    iget v7, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-float v7, v7

    iget v8, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    .line 1664648
    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    sub-float/2addr v7, v6

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const v8, 0x358637bd    # 1.0E-6f

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1

    .line 1664649
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1664650
    :cond_2
    sget-object v0, LX/5fO;->MEDIUM:LX/5fO;

    invoke-virtual {p2, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1664651
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    move-object v0, v2

    .line 1664652
    :cond_3
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_4

    .line 1664653
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664654
    iget v7, v0, Landroid/hardware/Camera$Size;->width:I

    iget v8, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v7, v8

    const/high16 v8, 0x200000

    if-gt v7, v8, :cond_3

    :cond_4
    move-object v1, v0

    .line 1664655
    goto :goto_0

    :cond_5
    sget-object v0, LX/5fO;->LOW:LX/5fO;

    invoke-virtual {p2, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1664656
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    move-object v0, v2

    .line 1664657
    :cond_6
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_7

    .line 1664658
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664659
    iget v7, v0, Landroid/hardware/Camera$Size;->width:I

    iget v8, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v7, v8

    const/high16 v8, 0x300000

    if-gt v7, v8, :cond_6

    :cond_7
    move-object v1, v0

    goto :goto_0

    .line 1664660
    :cond_8
    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1664661
    :cond_9
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_a

    .line 1664662
    sget-object v0, LX/ALB;->a:Ljava/lang/String;

    const-string v1, "could not find a size that matches picture aspect ratio."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1664663
    invoke-super {p0, p1, p2, p3}, LX/5fj;->a(LX/5fS;LX/5fO;LX/5fO;)V

    .line 1664664
    :goto_2
    return-void

    .line 1664665
    :cond_a
    new-instance v0, LX/ALA;

    invoke-direct {v0, p0}, LX/ALA;-><init>(LX/ALB;)V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1664666
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_b

    .line 1664667
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    move-object v2, v0

    .line 1664668
    :cond_b
    sget-object v0, LX/5fO;->HIGH:LX/5fO;

    invoke-virtual {p3, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1664669
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664670
    :cond_c
    :goto_3
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p1, v2, v0}, LX/5fS;->a(II)V

    .line 1664671
    iget v0, v1, Landroid/hardware/Camera$Size;->width:I

    iget v1, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p1, v0, v1}, LX/5fS;->b(II)V

    goto :goto_2

    .line 1664672
    :cond_d
    sget-object v0, LX/5fO;->MEDIUM:LX/5fO;

    invoke-virtual {p3, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1664673
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664674
    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v3

    div-int/lit8 v3, v0, 0x2

    .line 1664675
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v9, v0

    move-object v0, v2

    move v2, v9

    .line 1664676
    :cond_e
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_c

    .line 1664677
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664678
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v4, v6

    if-gt v4, v3, :cond_e

    goto :goto_3

    .line 1664679
    :cond_f
    sget-object v0, LX/5fO;->LOW:LX/5fO;

    invoke-virtual {p3, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1664680
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664681
    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v3

    div-int/lit8 v3, v0, 0x3

    .line 1664682
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v9, v0

    move-object v0, v2

    move v2, v9

    .line 1664683
    :cond_10
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_c

    .line 1664684
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1664685
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v4, v6

    if-gt v4, v3, :cond_10

    goto :goto_3

    :cond_11
    move-object v0, v2

    goto :goto_3

    :cond_12
    move-object v1, v2

    goto/16 :goto_0
.end method
