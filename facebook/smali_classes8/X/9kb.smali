.class public final enum LX/9kb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9kb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9kb;

.field public static final enum NO_LOGGER:LX/9kb;

.field public static final enum PLACE_CREATION_LOGGER:LX/9kb;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1532017
    new-instance v0, LX/9kb;

    const-string v1, "NO_LOGGER"

    invoke-direct {v0, v1, v2}, LX/9kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9kb;->NO_LOGGER:LX/9kb;

    .line 1532018
    new-instance v0, LX/9kb;

    const-string v1, "PLACE_CREATION_LOGGER"

    invoke-direct {v0, v1, v3}, LX/9kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9kb;->PLACE_CREATION_LOGGER:LX/9kb;

    .line 1532019
    const/4 v0, 0x2

    new-array v0, v0, [LX/9kb;

    sget-object v1, LX/9kb;->NO_LOGGER:LX/9kb;

    aput-object v1, v0, v2

    sget-object v1, LX/9kb;->PLACE_CREATION_LOGGER:LX/9kb;

    aput-object v1, v0, v3

    sput-object v0, LX/9kb;->$VALUES:[LX/9kb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1532020
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9kb;
    .locals 1

    .prologue
    .line 1532021
    const-class v0, LX/9kb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9kb;

    return-object v0
.end method

.method public static values()[LX/9kb;
    .locals 1

    .prologue
    .line 1532022
    sget-object v0, LX/9kb;->$VALUES:[LX/9kb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9kb;

    return-object v0
.end method
