.class public final LX/AQL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V
    .locals 0

    .prologue
    .line 1671259
    iput-object p1, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1671260
    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    .line 1671261
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v1

    .line 1671262
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1671263
    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v2, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->i:LX/AQO;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1671264
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1671265
    check-cast p1, LX/0Px;

    .line 1671266
    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    .line 1671267
    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1671268
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1671269
    :cond_1
    :goto_0
    return-void

    .line 1671270
    :cond_2
    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->h:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;

    .line 1671271
    iput-object p1, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;->d:LX/0Px;

    .line 1671272
    iget-object v0, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/AQL;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v1, v1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->h:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsListAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
