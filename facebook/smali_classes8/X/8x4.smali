.class public LX/8x4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1423466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1423467
    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 1423468
    packed-switch p0, :pswitch_data_0

    .line 1423469
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown badge type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1423470
    :pswitch_0
    const v0, 0x7f020ad9

    .line 1423471
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f020ad7

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/1De;II)LX/1Dg;
    .locals 6
    .param p1    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    const/16 v5, 0x10

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 1423472
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-static {p1}, LX/8x4;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4, v3}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3, v3}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v0

    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const v3, 0x7f020ad8

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Di;->c(I)LX/1Di;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a0048

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004b

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    goto :goto_0
.end method

.method private static a()LX/8x4;
    .locals 1

    .prologue
    .line 1423473
    new-instance v0, LX/8x4;

    invoke-direct {v0}, LX/8x4;-><init>()V

    .line 1423474
    return-object v0
.end method

.method public static a(LX/0QB;)LX/8x4;
    .locals 3

    .prologue
    .line 1423475
    const-class v1, LX/8x4;

    monitor-enter v1

    .line 1423476
    :try_start_0
    sget-object v0, LX/8x4;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1423477
    sput-object v2, LX/8x4;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1423478
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423479
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    invoke-static {}, LX/8x4;->a()LX/8x4;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1423480
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8x4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1423481
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1423482
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
