.class public final enum LX/A6N;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/A6N;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/A6N;

.field public static final enum BIGRAM:LX/A6N;

.field public static final enum UNIGRAM:LX/A6N;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1624304
    new-instance v0, LX/A6N;

    const-string v1, "UNIGRAM"

    invoke-direct {v0, v1, v2}, LX/A6N;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A6N;->UNIGRAM:LX/A6N;

    .line 1624305
    new-instance v0, LX/A6N;

    const-string v1, "BIGRAM"

    invoke-direct {v0, v1, v3}, LX/A6N;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A6N;->BIGRAM:LX/A6N;

    .line 1624306
    const/4 v0, 0x2

    new-array v0, v0, [LX/A6N;

    sget-object v1, LX/A6N;->UNIGRAM:LX/A6N;

    aput-object v1, v0, v2

    sget-object v1, LX/A6N;->BIGRAM:LX/A6N;

    aput-object v1, v0, v3

    sput-object v0, LX/A6N;->$VALUES:[LX/A6N;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1624307
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOrdinal(I)LX/A6N;
    .locals 1

    .prologue
    .line 1624308
    invoke-static {}, LX/A6N;->values()[LX/A6N;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/A6N;
    .locals 1

    .prologue
    .line 1624309
    const-class v0, LX/A6N;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/A6N;

    return-object v0
.end method

.method public static values()[LX/A6N;
    .locals 1

    .prologue
    .line 1624310
    sget-object v0, LX/A6N;->$VALUES:[LX/A6N;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/A6N;

    return-object v0
.end method
