.class public final LX/8yh;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8yi;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:F

.field public b:I

.field public c:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1426347
    invoke-static {}, LX/8yi;->q()LX/8yi;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1426348
    const/4 v0, 0x0

    iput v0, p0, LX/8yh;->b:I

    .line 1426349
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1426350
    const-string v0, "FractionalRatingBarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426351
    if-ne p0, p1, :cond_1

    .line 1426352
    :cond_0
    :goto_0
    return v0

    .line 1426353
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1426354
    goto :goto_0

    .line 1426355
    :cond_3
    check-cast p1, LX/8yh;

    .line 1426356
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1426357
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1426358
    if-eq v2, v3, :cond_0

    .line 1426359
    iget v2, p0, LX/8yh;->a:F

    iget v3, p1, LX/8yh;->a:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 1426360
    goto :goto_0

    .line 1426361
    :cond_4
    iget v2, p0, LX/8yh;->b:I

    iget v3, p1, LX/8yh;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1426362
    goto :goto_0

    .line 1426363
    :cond_5
    iget-object v2, p0, LX/8yh;->c:LX/1dc;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/8yh;->c:LX/1dc;

    iget-object v3, p1, LX/8yh;->c:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    .line 1426364
    goto :goto_0

    .line 1426365
    :cond_7
    iget-object v2, p1, LX/8yh;->c:LX/1dc;

    if-nez v2, :cond_6

    .line 1426366
    :cond_8
    iget-object v2, p0, LX/8yh;->d:LX/1dc;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/8yh;->d:LX/1dc;

    iget-object v3, p1, LX/8yh;->d:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 1426367
    goto :goto_0

    .line 1426368
    :cond_a
    iget-object v2, p1, LX/8yh;->d:LX/1dc;

    if-nez v2, :cond_9

    .line 1426369
    :cond_b
    iget-object v2, p0, LX/8yh;->e:LX/1dc;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/8yh;->e:LX/1dc;

    iget-object v3, p1, LX/8yh;->e:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1426370
    goto :goto_0

    .line 1426371
    :cond_c
    iget-object v2, p1, LX/8yh;->e:LX/1dc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
