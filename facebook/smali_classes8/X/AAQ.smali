.class public final LX/AAQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 1637587
    const/16 v20, 0x0

    .line 1637588
    const/16 v19, 0x0

    .line 1637589
    const/16 v18, 0x0

    .line 1637590
    const/16 v17, 0x0

    .line 1637591
    const/16 v16, 0x0

    .line 1637592
    const/4 v15, 0x0

    .line 1637593
    const/4 v14, 0x0

    .line 1637594
    const/4 v13, 0x0

    .line 1637595
    const/4 v12, 0x0

    .line 1637596
    const/4 v11, 0x0

    .line 1637597
    const/4 v10, 0x0

    .line 1637598
    const/4 v9, 0x0

    .line 1637599
    const/4 v8, 0x0

    .line 1637600
    const/4 v7, 0x0

    .line 1637601
    const/4 v6, 0x0

    .line 1637602
    const/4 v5, 0x0

    .line 1637603
    const/4 v4, 0x0

    .line 1637604
    const/4 v3, 0x0

    .line 1637605
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 1637606
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1637607
    const/4 v3, 0x0

    .line 1637608
    :goto_0
    return v3

    .line 1637609
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1637610
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_a

    .line 1637611
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 1637612
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1637613
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 1637614
    const-string v22, "linkClicks"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 1637615
    const/4 v11, 0x1

    .line 1637616
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto :goto_1

    .line 1637617
    :cond_2
    const-string v22, "messaging_reply"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 1637618
    const/4 v10, 0x1

    .line 1637619
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto :goto_1

    .line 1637620
    :cond_3
    const-string v22, "organic_reach"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1637621
    const/4 v9, 0x1

    .line 1637622
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto :goto_1

    .line 1637623
    :cond_4
    const-string v22, "otherClicks"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 1637624
    const/4 v8, 0x1

    .line 1637625
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto :goto_1

    .line 1637626
    :cond_5
    const-string v22, "paid_reach"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 1637627
    const/4 v7, 0x1

    .line 1637628
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto :goto_1

    .line 1637629
    :cond_6
    const-string v22, "photoViews"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 1637630
    const/4 v6, 0x1

    .line 1637631
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto :goto_1

    .line 1637632
    :cond_7
    const-string v22, "totalClicks"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 1637633
    const/4 v5, 0x1

    .line 1637634
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 1637635
    :cond_8
    const-string v22, "total_reach"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 1637636
    const/4 v4, 0x1

    .line 1637637
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1637638
    :cond_9
    const-string v22, "videoPlays"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1637639
    const/4 v3, 0x1

    .line 1637640
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 1637641
    :cond_a
    const/16 v21, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1637642
    if-eqz v11, :cond_b

    .line 1637643
    const/4 v11, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v11, v1, v2}, LX/186;->a(III)V

    .line 1637644
    :cond_b
    if-eqz v10, :cond_c

    .line 1637645
    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v10, v1, v11}, LX/186;->a(III)V

    .line 1637646
    :cond_c
    if-eqz v9, :cond_d

    .line 1637647
    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 1637648
    :cond_d
    if-eqz v8, :cond_e

    .line 1637649
    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 1637650
    :cond_e
    if-eqz v7, :cond_f

    .line 1637651
    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 1637652
    :cond_f
    if-eqz v6, :cond_10

    .line 1637653
    const/4 v6, 0x5

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15, v7}, LX/186;->a(III)V

    .line 1637654
    :cond_10
    if-eqz v5, :cond_11

    .line 1637655
    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14, v6}, LX/186;->a(III)V

    .line 1637656
    :cond_11
    if-eqz v4, :cond_12

    .line 1637657
    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v5}, LX/186;->a(III)V

    .line 1637658
    :cond_12
    if-eqz v3, :cond_13

    .line 1637659
    const/16 v3, 0x8

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12, v4}, LX/186;->a(III)V

    .line 1637660
    :cond_13
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1637661
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1637662
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637663
    if-eqz v0, :cond_0

    .line 1637664
    const-string v1, "linkClicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637665
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637666
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637667
    if-eqz v0, :cond_1

    .line 1637668
    const-string v1, "messaging_reply"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637669
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637670
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637671
    if-eqz v0, :cond_2

    .line 1637672
    const-string v1, "organic_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637673
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637674
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637675
    if-eqz v0, :cond_3

    .line 1637676
    const-string v1, "otherClicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637677
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637678
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637679
    if-eqz v0, :cond_4

    .line 1637680
    const-string v1, "paid_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637681
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637682
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637683
    if-eqz v0, :cond_5

    .line 1637684
    const-string v1, "photoViews"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637685
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637686
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637687
    if-eqz v0, :cond_6

    .line 1637688
    const-string v1, "totalClicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637689
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637690
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637691
    if-eqz v0, :cond_7

    .line 1637692
    const-string v1, "total_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637693
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637694
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637695
    if-eqz v0, :cond_8

    .line 1637696
    const-string v1, "videoPlays"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637697
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637698
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1637699
    return-void
.end method
