.class public LX/8rk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/8rW;

.field public final c:LX/2hX;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/5Ou;

.field public final f:LX/17W;

.field private final g:LX/0W9;

.field private final h:LX/0ad;

.field public final i:Lcom/facebook/content/SecureContextHelper;

.field public final j:LX/0hy;

.field public k:LX/36N;

.field public l:Landroid/view/View;

.field private m:Landroid/widget/TextView;

.field public n:LX/8rg;

.field public o:LX/8s1;

.field private p:Z

.field public q:LX/82m;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/ipc/feed/ViewPermalinkParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8rW;LX/2hX;LX/17W;LX/5Ou;LX/0Or;LX/0ad;LX/0W9;Lcom/facebook/content/SecureContextHelper;LX/0hy;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/8rW;",
            "LX/2hX;",
            "LX/17W;",
            "LX/5Ou;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            "LX/0W9;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0hy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1409079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409080
    iput-object p1, p0, LX/8rk;->a:Landroid/content/Context;

    .line 1409081
    iput-object p2, p0, LX/8rk;->b:LX/8rW;

    .line 1409082
    iput-object p3, p0, LX/8rk;->c:LX/2hX;

    .line 1409083
    iput-object p4, p0, LX/8rk;->f:LX/17W;

    .line 1409084
    iput-object p5, p0, LX/8rk;->e:LX/5Ou;

    .line 1409085
    iput-object p6, p0, LX/8rk;->d:LX/0Or;

    .line 1409086
    iput-object p7, p0, LX/8rk;->h:LX/0ad;

    .line 1409087
    iput-object p8, p0, LX/8rk;->g:LX/0W9;

    .line 1409088
    iput-object p9, p0, LX/8rk;->i:Lcom/facebook/content/SecureContextHelper;

    .line 1409089
    iput-object p10, p0, LX/8rk;->j:LX/0hy;

    .line 1409090
    return-void
.end method

.method public static a(LX/8rk;Landroid/view/View;)V
    .locals 9

    .prologue
    .line 1409091
    iget-object v0, p0, LX/8rk;->e:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1409092
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v1}, LX/36N;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1409093
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1409094
    const-string v2, "is_from_fb4a"

    const/4 p1, 0x1

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1409095
    iget-object v2, p0, LX/8rk;->f:LX/17W;

    iget-object p1, p0, LX/8rk;->a:Landroid/content/Context;

    invoke-virtual {v2, p1, v0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 1409096
    :goto_0
    return-void

    .line 1409097
    :cond_0
    sget-object v0, LX/8s1;->ACTIVITY_RESULT:LX/8s1;

    iget-object v1, p0, LX/8rk;->o:LX/8s1;

    invoke-virtual {v0, v1}, LX/8s1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1409098
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1409099
    if-nez v0, :cond_2

    .line 1409100
    :goto_1
    goto :goto_0

    .line 1409101
    :cond_1
    iget-object v2, p0, LX/8rk;->j:LX/0hy;

    iget-object v3, p0, LX/8rk;->r:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-interface {v2, v3}, LX/0hz;->a(Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;)Landroid/content/Intent;

    move-result-object v3

    .line 1409102
    new-instance v2, LX/7Gq;

    invoke-direct {v2}, LX/7Gq;-><init>()V

    sget-object v4, LX/7Gr;->USER:LX/7Gr;

    .line 1409103
    iput-object v4, v2, LX/7Gq;->e:LX/7Gr;

    .line 1409104
    move-object v2, v2

    .line 1409105
    new-instance v4, Lcom/facebook/user/model/Name;

    iget-object v5, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v5}, LX/36N;->v_()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 1409106
    iput-object v4, v2, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 1409107
    move-object v4, v2

    .line 1409108
    const/4 v2, 0x0

    .line 1409109
    :try_start_0
    iget-object v5, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v5}, LX/36N;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1409110
    iput-wide v6, v4, LX/7Gq;->b:J

    .line 1409111
    move-object v4, v4

    .line 1409112
    invoke-virtual {v4}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1409113
    :goto_2
    const-string v4, "autofill_mention_tagging_profile"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1409114
    const-string v2, "show_keyboard_on_first_load"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1409115
    iget-object v2, p0, LX/8rk;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1409116
    goto :goto_0

    .line 1409117
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1409118
    const-string v2, "mention_user_id"

    iget-object v3, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v3}, LX/36N;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1409119
    const-string v2, "mention_user_name"

    iget-object v3, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v3}, LX/36N;->v_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1409120
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1409121
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :catch_0
    goto :goto_2
.end method

.method public static a$redex0(LX/8rk;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;II)V
    .locals 7
    .param p0    # LX/8rk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1409122
    if-eqz p1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1409123
    :cond_0
    iget-object v0, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1409124
    iget-object v0, p0, LX/8rk;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1409125
    iget-object v0, p0, LX/8rk;->l:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 1409126
    :goto_0
    return-void

    .line 1409127
    :cond_1
    iget-object v0, p0, LX/8rk;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1409128
    iget-object v1, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1409129
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1409130
    iget-object v1, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-static {v1, v4, v4, v4, v4}, LX/4lM;->b(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1409131
    iget-object v1, p0, LX/8rk;->m:Landroid/widget/TextView;

    const v2, 0x7f080f85

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1409132
    :goto_1
    iget-object v0, p0, LX/8rk;->n:LX/8rg;

    iget-object v1, p0, LX/8rk;->l:Landroid/view/View;

    iget-object v2, p0, LX/8rk;->o:LX/8s1;

    invoke-interface {v0, v1, p1, v2}, LX/8rg;->a(Landroid/view/View;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/8s1;)V

    .line 1409133
    iget-object v0, p0, LX/8rk;->l:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 1409134
    iget-object v0, p0, LX/8rk;->l:Landroid/view/View;

    new-instance v1, LX/8rh;

    invoke-direct {v1, p0, p1}, LX/8rh;-><init>(LX/8rk;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1409135
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-lez p3, :cond_3

    .line 1409136
    const v1, 0x7f0f00f4

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/8rk;->g:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 1409137
    iget-object v2, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1409138
    const v1, 0x7f021537

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1409139
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1409140
    iget-object v1, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-static {v1, v0, v4, v4, v4}, LX/4lM;->b(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1409141
    :cond_3
    if-lez p2, :cond_4

    .line 1409142
    const v1, 0x7f0f005c

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1409143
    iget-object v1, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-static {v1, v4, v4, v4, v4}, LX/4lM;->b(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1409144
    iget-object v1, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1409145
    :cond_4
    iget-object v0, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/8rk;
    .locals 11

    .prologue
    .line 1409146
    new-instance v0, LX/8rk;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/8rW;->a(LX/0QB;)LX/8rW;

    move-result-object v2

    check-cast v2, LX/8rW;

    invoke-static {p0}, LX/2hX;->b(LX/0QB;)LX/2hX;

    move-result-object v3

    check-cast v3, LX/2hX;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {p0}, LX/5Ou;->a(LX/0QB;)LX/5Ou;

    move-result-object v5

    check-cast v5, LX/5Ou;

    const/16 v6, 0x15e7

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v8

    check-cast v8, LX/0W9;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v10

    check-cast v10, LX/0hy;

    invoke-direct/range {v0 .. v10}, LX/8rk;-><init>(Landroid/content/Context;LX/8rW;LX/2hX;LX/17W;LX/5Ou;LX/0Or;LX/0ad;LX/0W9;Lcom/facebook/content/SecureContextHelper;LX/0hy;)V

    .line 1409147
    return-object v0
.end method


# virtual methods
.method public final a()LX/8rj;
    .locals 2

    .prologue
    .line 1409148
    new-instance v0, LX/8rj;

    invoke-direct {v0, p0}, LX/8rj;-><init>(LX/8rk;)V

    return-object v0
.end method

.method public final a(LX/36N;LX/8s1;ZLcom/facebook/ipc/feed/ViewPermalinkParams;LX/82m;)V
    .locals 7
    .param p4    # Lcom/facebook/ipc/feed/ViewPermalinkParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/82m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    .line 1409149
    iput-object p1, p0, LX/8rk;->k:LX/36N;

    .line 1409150
    iput-object p2, p0, LX/8rk;->o:LX/8s1;

    .line 1409151
    iput-object p4, p0, LX/8rk;->r:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    .line 1409152
    iput-object p5, p0, LX/8rk;->q:LX/82m;

    .line 1409153
    iget-object v0, p0, LX/8rk;->k:LX/36N;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8rk;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8rk;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v1}, LX/36N;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1409154
    :cond_0
    iget-object v0, p0, LX/8rk;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1409155
    iget-object v0, p0, LX/8rk;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1409156
    :goto_0
    return-void

    .line 1409157
    :cond_1
    if-eqz p3, :cond_2

    .line 1409158
    iget-object v0, p0, LX/8rk;->b:LX/8rW;

    iget-object v1, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v1}, LX/36N;->e()Ljava/lang/String;

    move-result-object v1

    .line 1409159
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 1409160
    iget-object v5, v0, LX/8rW;->a:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v3, v5

    .line 1409161
    move-object v0, v3

    .line 1409162
    if-eqz v0, :cond_2

    iget-object v1, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v1}, LX/36N;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    if-eq v1, v0, :cond_2

    .line 1409163
    iget-object v1, p0, LX/8rk;->k:LX/36N;

    invoke-static {v1}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->a(LX/36N;)Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;

    move-result-object v1

    invoke-static {v1}, LX/8rm;->a(Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;)LX/8rm;

    move-result-object v1

    .line 1409164
    iput-object v0, v1, LX/8rm;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1409165
    move-object v0, v1

    .line 1409166
    invoke-virtual {v0}, LX/8rm;->a()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;

    move-result-object v0

    iput-object v0, p0, LX/8rk;->k:LX/36N;

    .line 1409167
    :cond_2
    iget-object v0, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v0}, LX/36N;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v0, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v0}, LX/36N;->b()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v0}, LX/36N;->b()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;->a()I

    move-result v0

    :goto_1
    iget-object v2, p0, LX/8rk;->k:LX/36N;

    invoke-interface {v2}, LX/36N;->k()I

    move-result v2

    invoke-static {p0, v1, v0, v2}, LX/8rk;->a$redex0(LX/8rk;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;II)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/8rg;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 1409168
    iput-object p1, p0, LX/8rk;->n:LX/8rg;

    .line 1409169
    iput-object p2, p0, LX/8rk;->l:Landroid/view/View;

    .line 1409170
    iput-object p3, p0, LX/8rk;->m:Landroid/widget/TextView;

    .line 1409171
    iget-object v0, p0, LX/8rk;->h:LX/0ad;

    sget-short v1, LX/1sg;->k:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/8rk;->p:Z

    .line 1409172
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1409173
    iget-object v0, p0, LX/8rk;->c:LX/2hX;

    .line 1409174
    iput-boolean p1, v0, LX/2hY;->d:Z

    .line 1409175
    return-void
.end method
