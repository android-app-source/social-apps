.class public final LX/9nI;
.super LX/5p8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5p8",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/react/bridge/Callback;

.field public final synthetic b:LX/5pC;

.field public final synthetic c:LX/9nO;


# direct methods
.method public constructor <init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V
    .locals 0

    .prologue
    .line 1536976
    iput-object p1, p0, LX/9nI;->c:LX/9nO;

    iput-object p3, p0, LX/9nI;->a:Lcom/facebook/react/bridge/Callback;

    iput-object p4, p0, LX/9nI;->b:LX/5pC;

    invoke-direct {p0, p2}, LX/5p8;-><init>(LX/5pX;)V

    return-void
.end method

.method private varargs a()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1536977
    iget-object v0, p0, LX/9nI;->c:LX/9nO;

    invoke-static {v0}, LX/9nO;->h(LX/9nO;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536978
    iget-object v0, p0, LX/9nI;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v14, [Ljava/lang/Object;

    invoke-static {v5}, LX/9nH;->c(Ljava/lang/String;)LX/5pH;

    move-result-object v2

    aput-object v2, v1, v9

    aput-object v5, v1, v13

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1536979
    :goto_0
    return-void

    .line 1536980
    :cond_0
    new-array v2, v14, [Ljava/lang/String;

    const-string v0, "key"

    aput-object v0, v2, v9

    const-string v0, "value"

    aput-object v0, v2, v13

    .line 1536981
    invoke-static {}, LX/5pt;->a()Ljava/util/HashSet;

    move-result-object v10

    .line 1536982
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v11

    move v8, v9

    .line 1536983
    :goto_1
    iget-object v0, p0, LX/9nI;->b:LX/5pC;

    invoke-interface {v0}, LX/5pC;->size()I

    move-result v0

    if-ge v8, v0, :cond_5

    .line 1536984
    iget-object v0, p0, LX/9nI;->b:LX/5pC;

    invoke-interface {v0}, LX/5pC;->size()I

    move-result v0

    sub-int/2addr v0, v8

    const/16 v1, 0x3e7

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 1536985
    iget-object v0, p0, LX/9nI;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "catalystLocalStorage"

    invoke-static {v12}, LX/9nG;->a(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/9nI;->b:LX/5pC;

    invoke-static {v4, v8, v12}, LX/9nG;->a(LX/5pC;II)[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1536986
    invoke-virtual {v10}, Ljava/util/HashSet;->clear()V

    .line 1536987
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget-object v3, p0, LX/9nI;->b:LX/5pC;

    invoke-interface {v3}, LX/5pC;->size()I

    move-result v3

    if-eq v0, v3, :cond_1

    move v0, v8

    .line 1536988
    :goto_2
    add-int v3, v8, v12

    if-ge v0, v3, :cond_1

    .line 1536989
    iget-object v3, p0, LX/9nI;->b:LX/5pC;

    invoke-interface {v3, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1536990
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1536991
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1536992
    :cond_2
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v0

    .line 1536993
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1536994
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1536995
    invoke-interface {v11, v0}, LX/5pD;->a(LX/5pD;)V

    .line 1536996
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1536997
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 1536998
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1536999
    invoke-virtual {v10}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1537000
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v3

    .line 1537001
    invoke-interface {v3, v0}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1537002
    invoke-interface {v3}, LX/5pD;->pushNull()V

    .line 1537003
    invoke-interface {v11, v3}, LX/5pD;->a(LX/5pD;)V

    goto :goto_3

    .line 1537004
    :catch_0
    move-exception v0

    .line 1537005
    :try_start_1
    const-string v2, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537006
    iget-object v2, p0, LX/9nI;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    const/4 v4, 0x0

    aput-object v4, v3, v0

    invoke-interface {v2, v3}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1537007
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1537008
    :cond_4
    invoke-virtual {v10}, Ljava/util/HashSet;->clear()V

    .line 1537009
    add-int/lit16 v0, v8, 0x3e7

    move v8, v0

    goto/16 :goto_1

    .line 1537010
    :cond_5
    iget-object v0, p0, LX/9nI;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v14, [Ljava/lang/Object;

    aput-object v5, v1, v9

    aput-object v11, v1, v13

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1537011
    invoke-direct {p0}, LX/9nI;->a()V

    return-void
.end method
