.class public final LX/920;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public final synthetic b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 0

    .prologue
    .line 1431785
    iput-object p1, p0, LX/920;->b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    iput-object p2, p0, LX/920;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1431767
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1431768
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 1431769
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Unexpected null results from Graphql"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/920;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1431770
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1431771
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 1431772
    iget-object v1, p0, LX/920;->b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_2
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, LX/920;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431773
    invoke-static {v1, v0, v2}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->a$redex0(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;Ljava/util/ArrayList;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 1431774
    return-void

    .line 1431775
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1431776
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1431777
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1431778
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1431779
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-virtual {v3, v0, v1, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 1431780
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_5

    move v0, v1

    goto :goto_1

    .line 1431781
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1431782
    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1

    .line 1431783
    :cond_6
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1431784
    goto :goto_2
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1431760
    iget-object v0, p0, LX/920;->b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1431761
    iget-object v0, p0, LX/920;->b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->r:Lcom/facebook/widget/error/GenericErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 1431762
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 1431763
    iget-object v0, p0, LX/920;->b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->r:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->a()V

    .line 1431764
    :goto_0
    iget-object v0, p0, LX/920;->b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->r:Lcom/facebook/widget/error/GenericErrorView;

    new-instance v1, LX/91z;

    invoke-direct {v1, p0}, LX/91z;-><init>(LX/920;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1431765
    return-void

    .line 1431766
    :cond_0
    iget-object v0, p0, LX/920;->b:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->r:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    goto :goto_0
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1431759
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/920;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
