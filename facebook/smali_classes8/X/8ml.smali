.class public final LX/8ml;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/stickers/model/Sticker;",
        "[",
        "LX/1bf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8mj;

.field public final synthetic b:Lcom/facebook/stickers/ui/StickerDraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/ui/StickerDraweeView;LX/8mj;)V
    .locals 0

    .prologue
    .line 1399758
    iput-object p1, p0, LX/8ml;->b:Lcom/facebook/stickers/ui/StickerDraweeView;

    iput-object p2, p0, LX/8ml;->a:LX/8mj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/stickers/model/Sticker;)[LX/1bf;
    .locals 2
    .param p1    # Lcom/facebook/stickers/model/Sticker;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1399759
    if-nez p1, :cond_0

    .line 1399760
    const/4 v0, 0x0

    .line 1399761
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/8ml;->b:Lcom/facebook/stickers/ui/StickerDraweeView;

    iget-object v1, p0, LX/8ml;->a:LX/8mj;

    invoke-static {v0, p1, v1}, Lcom/facebook/stickers/ui/StickerDraweeView;->a(Lcom/facebook/stickers/ui/StickerDraweeView;Lcom/facebook/stickers/model/Sticker;LX/8mj;)[LX/1bf;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1399757
    check-cast p1, Lcom/facebook/stickers/model/Sticker;

    invoke-direct {p0, p1}, LX/8ml;->a(Lcom/facebook/stickers/model/Sticker;)[LX/1bf;

    move-result-object v0

    return-object v0
.end method
