.class public LX/AE0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/ApI;

.field public final b:LX/Aph;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I
    .annotation build Lcom/facebook/fig/components/attachment/annotations/FigAttachmentFooterActionType;
    .end annotation
.end field

.field private final e:LX/2yS;

.field public f:Ljava/lang/CharSequence;

.field public g:Ljava/lang/CharSequence;

.field public h:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private k:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public l:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private m:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private n:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public o:Landroid/view/View$OnClickListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZILX/2yS;LX/ApI;LX/Aph;LX/0Or;)V
    .locals 0
    .param p1    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigAttachmentFooterActionType;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2yS;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "LX/2yS;",
            "LX/ApI;",
            "LX/Aph;",
            "LX/0Or",
            "<",
            "LX/1vg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1645846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1645847
    if-eqz p1, :cond_0

    const/4 p2, 0x1

    :cond_0
    iput p2, p0, LX/AE0;->d:I

    .line 1645848
    iput-object p3, p0, LX/AE0;->e:LX/2yS;

    .line 1645849
    iput-object p4, p0, LX/AE0;->a:LX/ApI;

    .line 1645850
    iput-object p5, p0, LX/AE0;->b:LX/Aph;

    .line 1645851
    iput-object p6, p0, LX/AE0;->c:LX/0Or;

    .line 1645852
    return-void
.end method

.method public static c(LX/AE0;LX/1De;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<+",
            "LX/2yS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1645853
    iget-object v0, p0, LX/AE0;->e:LX/2yS;

    invoke-virtual {v0, p1}, LX/2yS;->c(LX/1De;)LX/AE2;

    move-result-object v0

    iget-object v1, p0, LX/AE0;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, LX/AE2;->a(Ljava/lang/CharSequence;)LX/AE2;

    move-result-object v0

    iget-boolean v1, p0, LX/AE0;->l:Z

    .line 1645854
    iget-object v2, v0, LX/AE2;->a:LX/AE1;

    iput-boolean v1, v2, LX/AE1;->f:Z

    .line 1645855
    move-object v1, v0

    .line 1645856
    iget-boolean v0, p0, LX/AE0;->l:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0b1169

    .line 1645857
    :goto_0
    iget-object v2, v1, LX/AE2;->a:LX/AE1;

    invoke-virtual {v1, v0}, LX/1Dp;->e(I)I

    move-result v3

    iput v3, v2, LX/AE1;->i:I

    .line 1645858
    move-object v0, v1

    .line 1645859
    iget-object v1, p0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/AE2;->a(Landroid/view/View$OnClickListener;)LX/AE2;

    move-result-object v0

    iget-object v1, p0, LX/AE0;->g:Ljava/lang/CharSequence;

    .line 1645860
    iget-object v2, v0, LX/AE2;->a:LX/AE1;

    iput-object v1, v2, LX/AE1;->g:Ljava/lang/CharSequence;

    .line 1645861
    move-object v0, v0

    .line 1645862
    iget-object v1, p0, LX/AE0;->i:Landroid/util/SparseArray;

    .line 1645863
    iget-object v2, v0, LX/AE2;->a:LX/AE1;

    iput-object v1, v2, LX/AE1;->j:Landroid/util/SparseArray;

    .line 1645864
    move-object v1, v0

    .line 1645865
    iget-object v0, p0, LX/AE0;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1645866
    iget-object v0, p0, LX/AE0;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1645867
    iget-object v0, p0, LX/AE0;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    iget-object v2, p0, LX/AE0;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    iget-object v2, p0, LX/AE0;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1645868
    iget-object v2, v1, LX/AE2;->a:LX/AE1;

    iput-object v0, v2, LX/AE1;->d:LX/1dc;

    .line 1645869
    :cond_0
    :goto_1
    iget-object v0, p0, LX/AE0;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1645870
    iget-object v0, p0, LX/AE0;->k:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1645871
    iget-object v2, v1, LX/AE2;->a:LX/AE1;

    invoke-virtual {v1, v0}, LX/1Dp;->e(I)I

    move-result v3

    iput v3, v2, LX/AE1;->h:I

    .line 1645872
    :cond_1
    iget-object v0, p0, LX/AE0;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1645873
    iget-object v0, p0, LX/AE0;->m:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, LX/AE2;->h(I)LX/AE2;

    .line 1645874
    :cond_2
    iget-object v0, p0, LX/AE0;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1645875
    iget-object v0, p0, LX/AE0;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, LX/AE2;->j(I)LX/AE2;

    .line 1645876
    :cond_3
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 1645877
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 1645878
    :cond_5
    iget-object v0, p0, LX/AE0;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1645879
    iget-object v2, v1, LX/AE2;->a:LX/AE1;

    invoke-virtual {v1, v0}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v3

    iput-object v3, v2, LX/AE1;->d:LX/1dc;

    .line 1645880
    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/AE0;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1645883
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/AE0;->h:Ljava/lang/Integer;

    .line 1645884
    return-object p0
.end method

.method public final a(Landroid/util/SparseArray;)LX/AE0;
    .locals 0

    .prologue
    .line 1645881
    iput-object p1, p0, LX/AE0;->i:Landroid/util/SparseArray;

    .line 1645882
    return-object p0
.end method

.method public final a(Landroid/view/View$OnClickListener;)LX/AE0;
    .locals 0

    .prologue
    .line 1645844
    iput-object p1, p0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1645845
    return-object p0
.end method

.method public final a(Z)LX/AE0;
    .locals 0

    .prologue
    .line 1645842
    iput-boolean p1, p0, LX/AE0;->l:Z

    .line 1645843
    return-object p0
.end method

.method public final b(I)LX/AE0;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1645832
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/AE0;->j:Ljava/lang/Integer;

    .line 1645833
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/AE0;
    .locals 0

    .prologue
    .line 1645834
    iput-object p1, p0, LX/AE0;->g:Ljava/lang/CharSequence;

    .line 1645835
    return-object p0
.end method

.method public final c(I)LX/AE0;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1645836
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/AE0;->m:Ljava/lang/Integer;

    .line 1645837
    return-object p0
.end method

.method public final d(I)LX/AE0;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1645838
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/AE0;->n:Ljava/lang/Integer;

    .line 1645839
    return-object p0
.end method

.method public final e(I)LX/AE0;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1645840
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/AE0;->k:Ljava/lang/Integer;

    .line 1645841
    return-object p0
.end method
