.class public LX/9bK;
.super LX/556;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/556",
        "<",
        "LX/9bJ;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:[I

.field private final c:LX/9bJ;


# direct methods
.method public constructor <init>(LX/9bJ;)V
    .locals 1
    .param p1    # LX/9bJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1514799
    invoke-direct {p0, p1}, LX/556;-><init>(Landroid/view/View;)V

    .line 1514800
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/9bK;->b:[I

    .line 1514801
    iput-object p1, p0, LX/9bK;->c:LX/9bJ;

    .line 1514802
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 8

    .prologue
    .line 1514803
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    float-to-int v1, p1

    const/4 v2, -0x1

    .line 1514804
    int-to-double v4, v1

    iget-wide v6, v0, LX/9bJ;->a:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 1514805
    invoke-virtual {v0}, LX/9bJ;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1514806
    const/4 v2, 0x3

    .line 1514807
    :cond_0
    :goto_0
    move v0, v2

    .line 1514808
    return v0

    .line 1514809
    :cond_1
    iget-object v3, v0, LX/9bJ;->d:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v3, :cond_0

    sget-object v2, LX/9bE;->LEFT:LX/9bE;

    invoke-virtual {v2}, LX/9bE;->ordinal()I

    move-result v2

    goto :goto_0

    .line 1514810
    :cond_2
    iget-object v3, v0, LX/9bJ;->e:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v3, :cond_0

    sget-object v2, LX/9bE;->RIGHT:LX/9bE;

    invoke-virtual {v2}, LX/9bE;->ordinal()I

    move-result v2

    goto :goto_0
.end method

.method public final a(ILX/3sp;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1514811
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    iget-object v1, p0, LX/9bK;->b:[I

    invoke-virtual {v0, v1}, LX/9bJ;->getLocationOnScreen([I)V

    .line 1514812
    iget-object v0, p0, LX/9bK;->b:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1514813
    iget-object v1, p0, LX/9bK;->b:[I

    aget v1, v1, v4

    .line 1514814
    sget-object v2, LX/9bE;->LEFT:LX/9bE;

    invoke-virtual {v2}, LX/9bE;->ordinal()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 1514815
    iget-object v2, p0, LX/9bK;->c:LX/9bJ;

    sget-object v3, LX/9bE;->LEFT:LX/9bE;

    invoke-virtual {v2, v3, v0, v1}, LX/9bJ;->a(LX/9bE;II)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Landroid/graphics/Rect;)V

    .line 1514816
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    sget-object v1, LX/9bE;->LEFT:LX/9bE;

    invoke-virtual {v0, v1}, LX/9bJ;->a(LX/9bE;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 1514817
    :cond_0
    :goto_0
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    invoke-virtual {p2, v0}, LX/3sp;->d(Landroid/view/View;)V

    .line 1514818
    invoke-virtual {p2, v4}, LX/3sp;->c(Z)V

    .line 1514819
    invoke-virtual {p2, v4}, LX/3sp;->a(Z)V

    .line 1514820
    invoke-virtual {p2, v4}, LX/3sp;->f(Z)V

    .line 1514821
    invoke-virtual {p2, v4}, LX/3sp;->h(Z)V

    .line 1514822
    return-void

    .line 1514823
    :cond_1
    sget-object v2, LX/9bE;->RIGHT:LX/9bE;

    invoke-virtual {v2}, LX/9bE;->ordinal()I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 1514824
    iget-object v2, p0, LX/9bK;->c:LX/9bJ;

    sget-object v3, LX/9bE;->RIGHT:LX/9bE;

    invoke-virtual {v2, v3, v0, v1}, LX/9bJ;->a(LX/9bE;II)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Landroid/graphics/Rect;)V

    .line 1514825
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    sget-object v1, LX/9bE;->RIGHT:LX/9bE;

    invoke-virtual {v0, v1}, LX/9bJ;->a(LX/9bE;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1514826
    :cond_2
    const/4 v2, 0x3

    if-ne v2, p1, :cond_0

    .line 1514827
    iget-object v2, p0, LX/9bK;->c:LX/9bJ;

    sget-object v3, LX/9bE;->LEFT:LX/9bE;

    invoke-virtual {v2, v3, v0, v1}, LX/9bJ;->a(LX/9bE;II)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Landroid/graphics/Rect;)V

    .line 1514828
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    .line 1514829
    invoke-virtual {v0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0811dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1514830
    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(LX/3sp;)V
    .locals 2

    .prologue
    .line 1514831
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    .line 1514832
    iget-object v1, v0, LX/9bJ;->d:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1514833
    if-eqz v0, :cond_0

    .line 1514834
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    sget-object v1, LX/9bE;->LEFT:LX/9bE;

    invoke-virtual {v1}, LX/9bE;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/3sp;->c(Landroid/view/View;I)V

    .line 1514835
    :cond_0
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    .line 1514836
    iget-object v1, v0, LX/9bJ;->e:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1514837
    if-eqz v0, :cond_1

    .line 1514838
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    sget-object v1, LX/9bE;->RIGHT:LX/9bE;

    invoke-virtual {v1}, LX/9bE;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/3sp;->c(Landroid/view/View;I)V

    .line 1514839
    :cond_1
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    invoke-virtual {v0}, LX/9bJ;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1514840
    iget-object v0, p0, LX/9bK;->c:LX/9bJ;

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, LX/3sp;->c(Landroid/view/View;I)V

    .line 1514841
    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
