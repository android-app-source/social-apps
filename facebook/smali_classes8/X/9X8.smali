.class public final enum LX/9X8;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9X8;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9X8;

.field public static final enum EVENT_ADMIN_CLICK_PROMOTE:LX/9X8;

.field public static final enum EVENT_ADMIN_CLOSE_ADD_SERVICES_CARD:LX/9X8;

.field public static final enum EVENT_ADMIN_INVITE_OTHERS:LX/9X8;

.field public static final enum EVENT_ADMIN_NUX_SHOP_CLOSE:LX/9X8;

.field public static final enum EVENT_ADMIN_NUX_SHOP_START:LX/9X8;

.field public static final enum EVENT_ADMIN_PANEL_INIT:LX/9X8;

.field public static final enum EVENT_ADMIN_PIN_STORY:LX/9X8;

.field public static final enum EVENT_ADMIN_SHARE_PHOTO:LX/9X8;

.field public static final enum EVENT_ADMIN_SHOW_ADMIN_PANEL:LX/9X8;

.field public static final enum EVENT_ADMIN_SHOW_PUBLIC_VIEW:LX/9X8;

.field public static final enum EVENT_ADMIN_START_ADD_SERVICES_CARD:LX/9X8;

.field public static final enum EVENT_ADMIN_UNPIN_STORY:LX/9X8;

.field public static final enum EVENT_ADMIN_UPSELL_MESSAGE:LX/9X8;

.field public static final enum EVENT_ADMIN_UPSELL_NOTIF:LX/9X8;

.field public static final enum EVENT_ADMIN_VIEW_POSTS_BY_OTHERS:LX/9X8;

.field public static final enum EVENT_ADMIN_VIEW_TIMELINE:LX/9X8;

.field public static final enum EVENT_ADMIN_WRITE_POST:LX/9X8;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502355
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_PANEL_INIT"

    const-string v2, "admin_panel_initialization"

    invoke-direct {v0, v1, v4, v2}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_PANEL_INIT:LX/9X8;

    .line 1502356
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_SHOW_ADMIN_PANEL"

    const-string v2, "admin_panel_show_admin_panel"

    invoke-direct {v0, v1, v5, v2}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_SHOW_ADMIN_PANEL:LX/9X8;

    .line 1502357
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_SHOW_PUBLIC_VIEW"

    const-string v2, "admin_panel_show_public_view"

    invoke-direct {v0, v1, v6, v2}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_SHOW_PUBLIC_VIEW:LX/9X8;

    .line 1502358
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_WRITE_POST"

    const-string v2, "admin_panel_write_post"

    invoke-direct {v0, v1, v7, v2}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_WRITE_POST:LX/9X8;

    .line 1502359
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_SHARE_PHOTO"

    const-string v2, "admin_panel_share_photo"

    invoke-direct {v0, v1, v8, v2}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_SHARE_PHOTO:LX/9X8;

    .line 1502360
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_VIEW_TIMELINE"

    const/4 v2, 0x5

    const-string v3, "admin_panel_view_timeline"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_VIEW_TIMELINE:LX/9X8;

    .line 1502361
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_INVITE_OTHERS"

    const/4 v2, 0x6

    const-string v3, "admin_panel_invite_others"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_INVITE_OTHERS:LX/9X8;

    .line 1502362
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_VIEW_POSTS_BY_OTHERS"

    const/4 v2, 0x7

    const-string v3, "admin_panel_view_posts_by_others"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_VIEW_POSTS_BY_OTHERS:LX/9X8;

    .line 1502363
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_UPSELL_NOTIF"

    const/16 v2, 0x8

    const-string v3, "admin_panel_upsell_notifications_row"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_UPSELL_NOTIF:LX/9X8;

    .line 1502364
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_UPSELL_MESSAGE"

    const/16 v2, 0x9

    const-string v3, "admin_panel_upsell_messages_row"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_UPSELL_MESSAGE:LX/9X8;

    .line 1502365
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_CLICK_PROMOTE"

    const/16 v2, 0xa

    const-string v3, "admin_click_promote_page"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_CLICK_PROMOTE:LX/9X8;

    .line 1502366
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_START_ADD_SERVICES_CARD"

    const/16 v2, 0xb

    const-string v3, "page_service_card_start_now"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_START_ADD_SERVICES_CARD:LX/9X8;

    .line 1502367
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_CLOSE_ADD_SERVICES_CARD"

    const/16 v2, 0xc

    const-string v3, "page_service_card_cross_out"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_CLOSE_ADD_SERVICES_CARD:LX/9X8;

    .line 1502368
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_NUX_SHOP_START"

    const/16 v2, 0xd

    const-string v3, "page_nux_shop_start"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_NUX_SHOP_START:LX/9X8;

    .line 1502369
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_NUX_SHOP_CLOSE"

    const/16 v2, 0xe

    const-string v3, "page_nux_shop_cross_out"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_NUX_SHOP_CLOSE:LX/9X8;

    .line 1502370
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_PIN_STORY"

    const/16 v2, 0xf

    const-string v3, "android_page_pin_post"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_PIN_STORY:LX/9X8;

    .line 1502371
    new-instance v0, LX/9X8;

    const-string v1, "EVENT_ADMIN_UNPIN_STORY"

    const/16 v2, 0x10

    const-string v3, "android_page_unpin_post"

    invoke-direct {v0, v1, v2, v3}, LX/9X8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X8;->EVENT_ADMIN_UNPIN_STORY:LX/9X8;

    .line 1502372
    const/16 v0, 0x11

    new-array v0, v0, [LX/9X8;

    sget-object v1, LX/9X8;->EVENT_ADMIN_PANEL_INIT:LX/9X8;

    aput-object v1, v0, v4

    sget-object v1, LX/9X8;->EVENT_ADMIN_SHOW_ADMIN_PANEL:LX/9X8;

    aput-object v1, v0, v5

    sget-object v1, LX/9X8;->EVENT_ADMIN_SHOW_PUBLIC_VIEW:LX/9X8;

    aput-object v1, v0, v6

    sget-object v1, LX/9X8;->EVENT_ADMIN_WRITE_POST:LX/9X8;

    aput-object v1, v0, v7

    sget-object v1, LX/9X8;->EVENT_ADMIN_SHARE_PHOTO:LX/9X8;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9X8;->EVENT_ADMIN_VIEW_TIMELINE:LX/9X8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9X8;->EVENT_ADMIN_INVITE_OTHERS:LX/9X8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9X8;->EVENT_ADMIN_VIEW_POSTS_BY_OTHERS:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9X8;->EVENT_ADMIN_UPSELL_NOTIF:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9X8;->EVENT_ADMIN_UPSELL_MESSAGE:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9X8;->EVENT_ADMIN_CLICK_PROMOTE:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9X8;->EVENT_ADMIN_START_ADD_SERVICES_CARD:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9X8;->EVENT_ADMIN_CLOSE_ADD_SERVICES_CARD:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9X8;->EVENT_ADMIN_NUX_SHOP_START:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9X8;->EVENT_ADMIN_NUX_SHOP_CLOSE:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9X8;->EVENT_ADMIN_PIN_STORY:LX/9X8;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9X8;->EVENT_ADMIN_UNPIN_STORY:LX/9X8;

    aput-object v2, v0, v1

    sput-object v0, LX/9X8;->$VALUES:[LX/9X8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502353
    iput-object p3, p0, LX/9X8;->mEventName:Ljava/lang/String;

    .line 1502354
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9X8;
    .locals 1

    .prologue
    .line 1502351
    const-class v0, LX/9X8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9X8;

    return-object v0
.end method

.method public static values()[LX/9X8;
    .locals 1

    .prologue
    .line 1502348
    sget-object v0, LX/9X8;->$VALUES:[LX/9X8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9X8;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502350
    iget-object v0, p0, LX/9X8;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502349
    sget-object v0, LX/9XC;->ADMIN:LX/9XC;

    return-object v0
.end method
