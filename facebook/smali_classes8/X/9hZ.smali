.class public final LX/9hZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/concurrent/Future",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

.field public final synthetic c:LX/9hh;


# direct methods
.method public constructor <init>(LX/9hh;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)V
    .locals 0

    .prologue
    .line 1526750
    iput-object p1, p0, LX/9hZ;->c:LX/9hh;

    iput-object p2, p0, LX/9hZ;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9hZ;->b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1526751
    iget-object v0, p0, LX/9hZ;->c:LX/9hh;

    iget-object v0, v0, LX/9hh;->e:LX/9fy;

    iget-object v1, p0, LX/9hZ;->a:Ljava/lang/String;

    iget-object v2, p0, LX/9hZ;->b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    .line 1526752
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1526753
    new-instance v4, Lcom/facebook/photos/data/method/DeletePhotoTagParams;

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v4, v1, v5, p0}, Lcom/facebook/photos/data/method/DeletePhotoTagParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1526754
    const-string v5, "deletePhotoTagParams"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1526755
    iget-object v4, v0, LX/9fy;->a:LX/0aG;

    const-string v5, "delete_photo_tag"

    const p0, 0x7a13d89

    invoke-static {v4, v5, v3, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    move-object v0, v3

    .line 1526756
    return-object v0
.end method
