.class public final LX/ACY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V
    .locals 0

    .prologue
    .line 1642836
    iput-object p1, p0, LX/ACY;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1642837
    iget-object v0, p0, LX/ACY;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1642838
    iget-object v1, p0, LX/ACY;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1642839
    iget-object v1, p0, LX/ACY;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->b(LX/8QL;)V

    .line 1642840
    :goto_0
    return-void

    .line 1642841
    :cond_0
    iget-object v1, p0, LX/ACY;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(LX/8QL;)V

    goto :goto_0
.end method
