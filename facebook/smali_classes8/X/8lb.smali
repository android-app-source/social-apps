.class public final LX/8lb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8lo;",
        "LX/8lp;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V
    .locals 0

    .prologue
    .line 1397841
    iput-object p1, p0, LX/8lb;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1397842
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1397843
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1397844
    check-cast p2, LX/8lp;

    .line 1397845
    iget-object v0, p0, LX/8lb;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    sget-object v1, LX/8lg;->WAIT_FOR_TAGGED_STICKERS:LX/8lg;

    invoke-virtual {v0, v1}, LX/8lg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1397846
    iget-object v0, p0, LX/8lb;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v1, p2, LX/8lp;->a:LX/0Px;

    new-instance v2, LX/8la;

    invoke-direct {v2, p0}, LX/8la;-><init>(LX/8lb;)V

    invoke-static {v0, v1, v2}, Lcom/facebook/stickers/search/StickerSearchContainer;->a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;LX/0Px;LX/8lZ;)V

    .line 1397847
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1397848
    check-cast p2, Ljava/lang/Throwable;

    .line 1397849
    iget-object v0, p0, LX/8lb;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    sget-object v1, LX/8lg;->WAIT_FOR_TAGGED_STICKERS:LX/8lg;

    invoke-virtual {v0, v1}, LX/8lg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1397850
    iget-object v0, p0, LX/8lb;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    sget-object v1, LX/8lg;->ERROR:LX/8lg;

    invoke-static {v0, v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1397851
    :cond_0
    iget-object v0, p0, LX/8lb;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->s:LX/03V;

    sget-object v1, Lcom/facebook/stickers/search/StickerSearchContainer;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tagged stickers loading failed"

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1397852
    return-void
.end method
