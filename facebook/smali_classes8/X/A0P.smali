.class public LX/A0P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1604988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1604989
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1604990
    check-cast p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;

    .line 1604991
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1604992
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1604993
    const-string v1, "user_input"

    .line 1604994
    iget-object v2, p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1604995
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1604996
    const-string v1, "selected_type"

    .line 1604997
    iget-object v2, p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1604998
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1604999
    const-string v1, "selected_text"

    .line 1605000
    iget-object v2, p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1605001
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1605002
    const-string v1, "selected_semantic"

    .line 1605003
    iget-object v2, p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1605004
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1605005
    const-string v1, "timestamp"

    .line 1605006
    iget-wide v6, p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->e:J

    move-wide v2, v6

    .line 1605007
    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 1605008
    const-string v1, "skip_activity_log"

    .line 1605009
    iget-boolean v2, p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->g:Z

    move v2, v2

    .line 1605010
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1605011
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "search_type"

    .line 1605012
    iget-object v3, p1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;->f:LX/A0S;

    move-object v3, v3

    .line 1605013
    invoke-virtual {v3}, LX/A0S;->getParamName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1605014
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1605015
    new-instance v0, LX/14N;

    const-string v1, "mobilesearchactivitylog"

    const-string v2, "POST"

    const-string v3, "method/mobilesearchactivitylog"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1605016
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1605017
    const/4 v0, 0x0

    return-object v0
.end method
