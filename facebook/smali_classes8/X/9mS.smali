.class public final LX/9mS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9mU;


# direct methods
.method public constructor <init>(LX/9mU;)V
    .locals 0

    .prologue
    .line 1535660
    iput-object p1, p0, LX/9mS;->a:LX/9mU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x2

    const v0, -0x2cf6d481

    invoke-static {v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1535661
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;

    .line 1535662
    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1535663
    const-string v4, "other"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/9mS;->a:LX/9mU;

    iget-object v4, v4, LX/9mU;->h:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1535664
    const v0, 0x1b051cd4

    invoke-static {v5, v5, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1535665
    :goto_0
    return-void

    .line 1535666
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1535667
    :goto_1
    iget-object v4, p0, LX/9mS;->a:LX/9mU;

    iget-object v4, v4, LX/9mU;->j:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-virtual {v4, v3, v1}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->a(Ljava/lang/String;Z)V

    .line 1535668
    iget-object v3, p0, LX/9mS;->a:LX/9mU;

    invoke-static {v3, p1, v1, v0}, LX/9mU;->a$redex0(LX/9mU;Landroid/view/View;ZLcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;)V

    .line 1535669
    const v0, 0x7fbbc4cd

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1535670
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
