.class public final LX/A94;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "associatedAdCampaigns"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "associatedAdCampaigns"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "displayLabels"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "targetingSentences"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1629737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1629738
    return-void
.end method


# virtual methods
.method public final a(LX/15i;I)LX/A94;
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "associatedAdCampaigns"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1629739
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/A94;->a:LX/15i;

    iput p2, p0, LX/A94;->b:I

    monitor-exit v1

    .line 1629740
    return-object p0

    .line 1629741
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v12, 0x0

    const/4 v2, 0x0

    .line 1629742
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1629743
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v3, p0, LX/A94;->a:LX/15i;

    iget v5, p0, LX/A94;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v1, 0x62f008b1

    invoke-static {v3, v5, v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1629744
    iget-object v3, p0, LX/A94;->c:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1629745
    iget-object v5, p0, LX/A94;->d:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1629746
    iget-object v6, p0, LX/A94;->f:LX/2uF;

    invoke-static {v6, v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v6

    .line 1629747
    iget-object v7, p0, LX/A94;->g:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1629748
    iget-object v8, p0, LX/A94;->h:LX/0Px;

    invoke-virtual {v0, v8}, LX/186;->c(Ljava/util/List;)I

    move-result v8

    .line 1629749
    iget-object v9, p0, LX/A94;->j:LX/2uF;

    invoke-static {v9, v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v9

    .line 1629750
    iget-object v10, p0, LX/A94;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1629751
    const/16 v11, 0xa

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1629752
    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 1629753
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1629754
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1629755
    const/4 v1, 0x3

    iget-boolean v3, p0, LX/A94;->e:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1629756
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1629757
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1629758
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1629759
    const/4 v1, 0x7

    iget-boolean v3, p0, LX/A94;->i:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1629760
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1629761
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1629762
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1629763
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1629764
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1629765
    invoke-virtual {v1, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1629766
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1629767
    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-direct {v1, v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;-><init>(LX/15i;)V

    .line 1629768
    return-object v1

    .line 1629769
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
