.class public final LX/AJN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/93q;


# instance fields
.field public final synthetic a:LX/AJQ;


# direct methods
.method public constructor <init>(LX/AJQ;)V
    .locals 0

    .prologue
    .line 1661069
    iput-object p1, p0, LX/AJN;->a:LX/AJQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V
    .locals 2

    .prologue
    .line 1661070
    iget-object v0, p0, LX/AJN;->a:LX/AJQ;

    .line 1661071
    iput-object p1, v0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1661072
    iget-object v0, p0, LX/AJN;->a:LX/AJQ;

    iget-object v0, v0, LX/AJQ;->e:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1661073
    iget-object v0, p0, LX/AJN;->a:LX/AJQ;

    .line 1661074
    iget-object v1, v0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-nez v1, :cond_1

    .line 1661075
    :cond_0
    :goto_0
    iget-object v0, p0, LX/AJN;->a:LX/AJQ;

    iget-object v0, v0, LX/AJQ;->b:LX/AJA;

    iget-object v1, p0, LX/AJN;->a:LX/AJQ;

    iget-object v1, v1, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, v1}, LX/AJA;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1661076
    return-void

    .line 1661077
    :cond_1
    new-instance v1, LX/8QV;

    iget-object p1, v0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object p1, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v1, p1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    iget-object p1, v0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object p1, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object p1, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {p1, p2}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v1

    invoke-virtual {v1}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AJQ;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    goto :goto_0
.end method
