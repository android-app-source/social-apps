.class public LX/9n0;
.super LX/5p5;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKExceptionsManager"
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:LX/5qI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1536601
    const-string v0, "(?:^|[/\\\\])(\\d+\\.js)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/9n0;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(LX/5qI;)V
    .locals 0

    .prologue
    .line 1536598
    invoke-direct {p0}, LX/5p5;-><init>()V

    .line 1536599
    iput-object p1, p0, LX/9n0;->b:LX/5qI;

    .line 1536600
    return-void
.end method

.method private static a(LX/5pG;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1536593
    const-string v0, "file"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "file"

    invoke-interface {p0, v0}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "file"

    invoke-interface {p0, v0}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v0

    sget-object v1, Lcom/facebook/react/bridge/ReadableType;->String:Lcom/facebook/react/bridge/ReadableType;

    if-ne v0, v1, :cond_0

    .line 1536594
    sget-object v0, LX/9n0;->a:Ljava/util/regex/Pattern;

    const-string v1, "file"

    invoke-interface {p0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1536595
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1536596
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1536597
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;LX/5pC;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1536584
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ", stack:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1536585
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1536586
    invoke-interface {p1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    .line 1536587
    const-string v3, "methodName"

    invoke-interface {v2, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, LX/9n0;->a(LX/5pG;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "lineNumber"

    invoke-interface {v2, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1536588
    const-string v3, "column"

    invoke-interface {v2, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "column"

    invoke-interface {v2, v3}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "column"

    invoke-interface {v2, v3}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v3

    sget-object v4, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    if-ne v3, v4, :cond_0

    .line 1536589
    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "column"

    invoke-interface {v2, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1536590
    :cond_0
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1536591
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1536592
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/5pC;I)V
    .locals 2

    .prologue
    .line 1536602
    iget-object v0, p0, LX/9n0;->b:LX/5qI;

    invoke-interface {v0}, LX/5qI;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536603
    new-instance v0, LX/5pp;

    invoke-static {p1, p2}, LX/9n0;->a(Ljava/lang/String;LX/5pC;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1536604
    :cond_0
    return-void
.end method


# virtual methods
.method public dismissRedbox()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536583
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1536576
    const-string v0, "RKExceptionsManager"

    return-object v0
.end method

.method public reportFatalException(Ljava/lang/String;LX/5pC;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536581
    invoke-direct {p0, p1, p2, p3}, LX/9n0;->a(Ljava/lang/String;LX/5pC;I)V

    .line 1536582
    return-void
.end method

.method public reportSoftException(Ljava/lang/String;LX/5pC;I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536578
    iget-object v0, p0, LX/9n0;->b:LX/5qI;

    invoke-interface {v0}, LX/5qI;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536579
    const-string v0, "React"

    invoke-static {p1, p2}, LX/9n0;->a(Ljava/lang/String;LX/5pC;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536580
    :cond_0
    return-void
.end method

.method public updateExceptionMessage(Ljava/lang/String;LX/5pC;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536577
    return-void
.end method
