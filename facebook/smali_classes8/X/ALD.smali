.class public final LX/ALD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "LX/5fM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ALH;


# direct methods
.method public constructor <init>(LX/ALH;)V
    .locals 0

    .prologue
    .line 1664700
    iput-object p1, p0, LX/ALD;->a:LX/ALH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1664701
    iget-object v0, p0, LX/ALD;->a:LX/ALH;

    iget-object v0, v0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    iget-object v1, p0, LX/ALD;->a:LX/ALH;

    iget-object v1, v1, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v1}, Lcom/facebook/optic/CameraPreviewView;->getCameraFacing()LX/5fM;

    move-result-object v1

    .line 1664702
    iput-object v1, v0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 1664703
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1664704
    iget-object v0, p0, LX/ALD;->a:LX/ALH;

    iget-object v0, v0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08283f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 1664705
    iget-object v0, p0, LX/ALD;->a:LX/ALH;

    iget-object v0, v0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->d()V

    .line 1664706
    sget-object v0, Lcom/facebook/backstage/camera/CameraView;->f:Ljava/lang/String;

    const-string v1, "Failed to flip the camera"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1664707
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1664708
    invoke-direct {p0}, LX/ALD;->a()V

    return-void
.end method
