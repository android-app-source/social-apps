.class public final LX/8p1;
.super Landroid/text/Editable$Factory;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V
    .locals 0

    .prologue
    .line 1405111
    iput-object p1, p0, LX/8p1;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-direct {p0}, Landroid/text/Editable$Factory;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;B)V
    .locals 0

    .prologue
    .line 1405110
    invoke-direct {p0, p1}, LX/8p1;-><init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V

    return-void
.end method


# virtual methods
.method public final newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;
    .locals 3

    .prologue
    .line 1405108
    iget-object v0, p0, LX/8p1;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v0, v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->f:LX/3iT;

    new-instance v1, LX/8p0;

    invoke-direct {v1, p0}, LX/8p0;-><init>(LX/8p1;)V

    iget-object v2, p0, LX/8p1;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-static {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getMentionsColorSpanProvider(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)LX/8oa;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, LX/8of;->a(Ljava/lang/CharSequence;LX/3iT;LX/8oi;LX/8oa;)LX/8of;

    move-result-object v0

    .line 1405109
    return-object v0
.end method
