.class public final LX/A7R;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/10e;

.field public b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/10e;)V
    .locals 1

    .prologue
    .line 1626088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1626089
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10e;

    iput-object v0, p0, LX/A7R;->a:LX/10e;

    .line 1626090
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/A7R;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1626091
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 1626092
    iget-object v0, p0, LX/A7R;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 1626093
    return-void
.end method
