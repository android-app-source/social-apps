.class public final LX/90p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/92P;

.field public final synthetic b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;LX/92P;)V
    .locals 0

    .prologue
    .line 1429489
    iput-object p1, p0, LX/90p;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iput-object p2, p0, LX/90p;->a:LX/92P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1429490
    iget-object v0, p0, LX/90p;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->i:LX/92t;

    iget-object v1, p0, LX/90p;->a:LX/92P;

    invoke-virtual {v0, v1}, LX/92t;->a(LX/92P;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
