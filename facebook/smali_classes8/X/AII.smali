.class public final LX/AII;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1658220
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1658221
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1658222
    :goto_0
    return v1

    .line 1658223
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1658224
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1658225
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1658226
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1658227
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1658228
    const-string v4, "backstage_whitelisted_friends"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1658229
    const/4 v3, 0x0

    .line 1658230
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 1658231
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1658232
    :goto_2
    move v2, v3

    .line 1658233
    goto :goto_1

    .line 1658234
    :cond_2
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1658235
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1658236
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1658237
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1658238
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1658239
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1658240
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1658241
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1658242
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1658243
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1658244
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1658245
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1658246
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1658247
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1658248
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1658249
    invoke-static {p0, p1}, LX/AIH;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1658250
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1658251
    :cond_7
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1658252
    goto :goto_3

    .line 1658253
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1658254
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1658255
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1658256
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1658257
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1658258
    if-eqz v0, :cond_2

    .line 1658259
    const-string v1, "backstage_whitelisted_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1658260
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1658261
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1658262
    if-eqz v1, :cond_1

    .line 1658263
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1658264
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1658265
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1658266
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v0

    invoke-static {p0, v0, p2}, LX/AIH;->a(LX/15i;ILX/0nX;)V

    .line 1658267
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1658268
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1658269
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1658270
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1658271
    if-eqz v0, :cond_3

    .line 1658272
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1658273
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1658274
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1658275
    return-void
.end method
