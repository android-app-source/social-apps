.class public final LX/A3g;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1616785
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1616786
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1616787
    :goto_0
    return v1

    .line 1616788
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1616789
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1616790
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1616791
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1616792
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1616793
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1616794
    :cond_1
    const-string v7, "icon_image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1616795
    invoke-static {p0, p1}, LX/A3e;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1616796
    :cond_2
    const-string v7, "mood_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1616797
    invoke-static {p0, p1}, LX/A3f;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1616798
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1616799
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1616800
    if-eqz v0, :cond_5

    .line 1616801
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1616802
    :cond_5
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1616803
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1616804
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1616805
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1616806
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1616807
    if-eqz v0, :cond_0

    .line 1616808
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616809
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1616810
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1616811
    if-eqz v0, :cond_1

    .line 1616812
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616813
    invoke-static {p0, v0, p2}, LX/A3e;->a(LX/15i;ILX/0nX;)V

    .line 1616814
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1616815
    if-eqz v0, :cond_2

    .line 1616816
    const-string v1, "mood_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1616817
    invoke-static {p0, v0, p2}, LX/A3f;->a(LX/15i;ILX/0nX;)V

    .line 1616818
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1616819
    return-void
.end method
