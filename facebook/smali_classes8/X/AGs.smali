.class public final LX/AGs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0gN;


# direct methods
.method public constructor <init>(LX/0gN;)V
    .locals 0

    .prologue
    .line 1653281
    iput-object p1, p0, LX/AGs;->a:LX/0gN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1653282
    sget-object v0, LX/0gN;->a:Ljava/lang/String;

    const-string v1, "Failed to query inbox badge count"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1653283
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1653284
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1653285
    if-nez p1, :cond_1

    .line 1653286
    :cond_0
    :goto_0
    return-void

    .line 1653287
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1653288
    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->j()I

    .line 1653289
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1653290
    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->j()I

    move-result v0

    .line 1653291
    iget-object v1, p0, LX/AGs;->a:LX/0gN;

    iget-object v1, v1, LX/0gN;->g:LX/AEx;

    if-eqz v1, :cond_0

    .line 1653292
    iget-object v1, p0, LX/AGs;->a:LX/0gN;

    iget-object v1, v1, LX/0gN;->g:LX/AEx;

    invoke-virtual {v1, v0}, LX/AEx;->a(I)V

    goto :goto_0
.end method
