.class public LX/AQ0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field public static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field public static final d:Ljava/util/regex/Pattern;

.field public static final e:Ljava/util/regex/Pattern;

.field public static final f:Ljava/util/regex/Pattern;

.field public static final g:Ljava/util/regex/Pattern;

.field public static final h:Ljava/util/regex/Pattern;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 1670657
    const-string v0, "(?:(?:(?:(?:ht|f)tps?)://(?:(?:(?:[\\.:\\-_%@]|[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])*[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])|\\[(?:(?:[A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})\\])(?::\\d+){0,1})|(?:(?:(?:(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])[.]){3}(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9]))(?::\\d+){0,1})|(?:\\[(?:(?:[A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})\\](?::\\d+){0,1})|(?:(?:\\b)www\\d{0,3}[.](?:(?:(?:[\\.:\\-_%@]|[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])*[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])|\\[(?:(?:[A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})\\])(?:[.][a-z]{2,4})(?::\\d+){0,1})|(?:(?:(?:(?:[\\.:\\-_%@]|[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])*[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])|\\[(?:(?:[A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})\\])(?:[.][a-z]{2,4})(?::\\d+){0,1}(?=[/?#]))|(?:(?:(?:(?:[\\.:\\-_%@]|[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])*[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])|\\[(?:(?:[A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})\\])[.](com|pr|org|net|edu|gov|uk|fm|ly)))"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->a:Ljava/util/regex/Pattern;

    .line 1670658
    const-string v0, "^\\[[0-9]{1,4}:[0-9]{1,4}:[A-Fa-f0-9]{1,4}\\]"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->b:Ljava/util/regex/Pattern;

    .line 1670659
    const-string v0, "((?:(?:(?:ht|f)tps?)://(?:(?:(?:[\\.:\\-_%@]|[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])*[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])|\\[(?:(?:[A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})\\])(?::\\d+){0,1})|(?:(?:\\b)www\\d{0,3}[.](?:(?:(?:[\\.:\\-_%@]|[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])*[^\\s!\"#%&\'\\(\\)\\*,\\-\\./:;<>\\?@\\[\\\\\\]\\^_`\\{\\|\\}\\u2000-\\u206F\\u00ab\\u00bb\\uff08\\uff09])|\\[(?:(?:[A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4})\\])(?:[.][a-z]{2,4})(?::\\d+){0,1}))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->c:Ljava/util/regex/Pattern;

    .line 1670660
    const-string v0, "[/#?]"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->d:Ljava/util/regex/Pattern;

    .line 1670661
    const-string v0, "[\\s\\(\\)<>\\?#]"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->e:Ljava/util/regex/Pattern;

    .line 1670662
    const-string v0, "[\\s!\"#%&\'\\(\\)\\*,\\./:;<>\\?@\\[\\\\\\]\\^`\\{\\|\\}\\u00ab\\u00bb\\u2000-\\u206F\\uff08\\uff09]"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->f:Ljava/util/regex/Pattern;

    .line 1670663
    const-string v0, "\\s\\(\\)<>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->g:Ljava/util/regex/Pattern;

    .line 1670664
    const-string v0, "[\\s\'\";]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/AQ0;->h:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1670708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/AQ0;Ljava/lang/CharSequence;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;"
        }
    .end annotation

    .prologue
    .line 1670709
    invoke-static {p0, p1, p2}, LX/AQ0;->b(LX/AQ0;Ljava/lang/CharSequence;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    .line 1670710
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1670704
    if-eqz p0, :cond_0

    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1670705
    sget-object v0, LX/AQ0;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1670706
    :cond_0
    :goto_0
    return-object p0

    .line 1670707
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static b(LX/AQ0;Ljava/lang/CharSequence;LX/0Px;)Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1670665
    sget-object v0, LX/AQ0;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1670666
    :cond_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1670667
    const/16 p0, 0x28

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1670668
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v8

    .line 1670669
    const-string v0, "["

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Character;->codePointBefore(Ljava/lang/CharSequence;I)I

    move-result v0

    const/16 v2, 0x40

    if-ne v0, v2, :cond_2

    .line 1670670
    sget-object v0, LX/AQ0;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1670671
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1670672
    const/4 v0, 0x0

    .line 1670673
    :goto_0
    move-object v0, v0

    .line 1670674
    if-eqz v0, :cond_0

    .line 1670675
    invoke-virtual {p2, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1670676
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1670677
    :cond_2
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    .line 1670678
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ne v5, v0, :cond_3

    .line 1670679
    invoke-static {v8}, LX/AQ0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670680
    :cond_3
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 1670681
    invoke-static {p1, v5}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->appendCodePoint(I)Ljava/lang/StringBuffer;

    .line 1670682
    sget-object v0, LX/AQ0;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1670683
    invoke-static {v8}, LX/AQ0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670684
    :cond_4
    invoke-static {p1, v5, v7}, Ljava/lang/Character;->offsetByCodePoints(Ljava/lang/CharSequence;II)I

    move-result v0

    move v2, v3

    move v4, v5

    move v6, v3

    .line 1670685
    :goto_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-ge v0, v10, :cond_9

    .line 1670686
    invoke-static {p1, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v10

    .line 1670687
    invoke-virtual {v9, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1670688
    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->appendCodePoint(I)Ljava/lang/StringBuffer;

    .line 1670689
    if-nez v2, :cond_a

    .line 1670690
    if-ne v10, p0, :cond_6

    .line 1670691
    add-int/lit8 v6, v6, 0x1

    move v2, v7

    .line 1670692
    :cond_5
    :goto_3
    invoke-static {p1, v0, v7}, Ljava/lang/Character;->offsetByCodePoints(Ljava/lang/CharSequence;II)I

    move-result v0

    goto :goto_2

    .line 1670693
    :cond_6
    sget-object v10, LX/AQ0;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-nez v10, :cond_7

    sget-object v10, LX/AQ0;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-nez v10, :cond_8

    :cond_7
    move v4, v0

    .line 1670694
    goto :goto_3

    .line 1670695
    :cond_8
    sget-object v10, LX/AQ0;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1670696
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, v4, v7}, Ljava/lang/Character;->offsetByCodePoints(Ljava/lang/CharSequence;II)I

    move-result v2

    invoke-interface {p1, v5, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/AQ0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1670697
    :cond_a
    if-ne v10, p0, :cond_b

    .line 1670698
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1670699
    :cond_b
    const/16 v11, 0x29

    if-ne v10, v11, :cond_c

    .line 1670700
    add-int/lit8 v6, v6, -0x1

    .line 1670701
    if-nez v6, :cond_5

    move v2, v3

    move v4, v0

    .line 1670702
    goto :goto_3

    .line 1670703
    :cond_c
    sget-object v10, LX/AQ0;->g:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-nez v10, :cond_9

    goto :goto_3
.end method
