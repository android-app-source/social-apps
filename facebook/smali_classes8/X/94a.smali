.class public final LX/94a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1435458
    iput-object p1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iput-object p2, p0, LX/94a;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1435459
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-boolean v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->h:Z

    if-nez v1, :cond_0

    .line 1435460
    const/4 v0, 0x0

    .line 1435461
    :goto_0
    return v0

    .line 1435462
    :cond_0
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-object v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    if-nez v1, :cond_1

    .line 1435463
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    new-instance v2, LX/0hs;

    iget-object v3, p0, LX/94a;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1435464
    iput-object v2, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    .line 1435465
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-object v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    const/16 v2, 0x7da

    .line 1435466
    iput v2, v1, LX/0ht;->s:I

    .line 1435467
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-object v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0ht;->b(F)V

    .line 1435468
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-object v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 1435469
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-object v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    iget-object v2, p0, LX/94a;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b104c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1435470
    iput v2, v1, LX/0hs;->r:I

    .line 1435471
    :cond_1
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-object v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    iget-object v2, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    invoke-virtual {v2}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1435472
    iget-object v1, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    iget-object v1, v1, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->i:LX/0hs;

    iget-object v2, p0, LX/94a;->b:Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    invoke-virtual {v1, v2}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method
