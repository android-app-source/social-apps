.class public final LX/9Ek;
.super LX/451;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/451",
        "<",
        "LX/6BE",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/flyout/FeedbackParams;

.field public final synthetic b:[LX/9Bw;

.field public final synthetic c:Lcom/facebook/feedback/ui/SingletonFeedbackController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)V
    .locals 0

    .prologue
    .line 1457335
    iput-object p1, p0, LX/9Ek;->c:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iput-object p2, p0, LX/9Ek;->a:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    iput-object p3, p0, LX/9Ek;->b:[LX/9Bw;

    invoke-direct {p0}, LX/451;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1457336
    check-cast p1, LX/6BE;

    .line 1457337
    iget-object v0, p1, LX/6BE;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1457338
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457339
    if-nez v0, :cond_1

    .line 1457340
    :cond_0
    return-void

    .line 1457341
    :cond_1
    invoke-static {v0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v1

    .line 1457342
    iget-object v2, p0, LX/9Ek;->a:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1457343
    iget-object v3, v2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v2, v3

    .line 1457344
    if-eqz v2, :cond_2

    .line 1457345
    iget-object v3, p0, LX/9Ek;->c:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iget-object v3, v3, Lcom/facebook/feedback/ui/SingletonFeedbackController;->e:LX/9Ah;

    .line 1457346
    iget-object v4, v3, LX/9Ah;->a:LX/0Uh;

    const/16 v5, 0x392

    const/4 p1, 0x0

    invoke-virtual {v4, v5, p1}, LX/0Uh;->a(IZ)Z

    move-result v4

    move v3, v4

    .line 1457347
    if-eqz v3, :cond_2

    iget-object v3, p0, LX/9Ek;->c:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    invoke-static {v3, v1, v2}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a$redex0(Lcom/facebook/feedback/ui/SingletonFeedbackController;LX/0Px;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1457348
    :cond_2
    iget-object v2, p0, LX/9Ek;->b:[LX/9Bw;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1457349
    invoke-interface {v4, v0}, LX/9Bw;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1457350
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1457351
    return-void
.end method
