.class public final enum LX/ACs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ACs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ACs;

.field public static final enum INTEREST:LX/ACs;

.field public static final enum LOCATION:LX/ACs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1643211
    new-instance v0, LX/ACs;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v2}, LX/ACs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ACs;->LOCATION:LX/ACs;

    .line 1643212
    new-instance v0, LX/ACs;

    const-string v1, "INTEREST"

    invoke-direct {v0, v1, v3}, LX/ACs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ACs;->INTEREST:LX/ACs;

    .line 1643213
    const/4 v0, 0x2

    new-array v0, v0, [LX/ACs;

    sget-object v1, LX/ACs;->LOCATION:LX/ACs;

    aput-object v1, v0, v2

    sget-object v1, LX/ACs;->INTEREST:LX/ACs;

    aput-object v1, v0, v3

    sput-object v0, LX/ACs;->$VALUES:[LX/ACs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1643214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ACs;
    .locals 1

    .prologue
    .line 1643215
    const-class v0, LX/ACs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ACs;

    return-object v0
.end method

.method public static values()[LX/ACs;
    .locals 1

    .prologue
    .line 1643216
    sget-object v0, LX/ACs;->$VALUES:[LX/ACs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ACs;

    return-object v0
.end method
