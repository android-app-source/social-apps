.class public final LX/A8L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/A8A;


# instance fields
.field public final synthetic a:LX/A8N;


# direct methods
.method public constructor <init>(LX/A8N;)V
    .locals 0

    .prologue
    .line 1627524
    iput-object p1, p0, LX/A8L;->a:LX/A8N;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1627527
    iget-object v0, p0, LX/A8L;->a:LX/A8N;

    iget-object v0, v0, LX/A8N;->b:LX/A8D;

    invoke-virtual {v0}, LX/0ht;->e()V

    .line 1627528
    iget-object v0, p0, LX/A8L;->a:LX/A8N;

    .line 1627529
    iget-object v1, v0, LX/A8N;->a:LX/A7r;

    if-eqz v1, :cond_0

    .line 1627530
    iget-object v1, v0, LX/A8N;->a:LX/A7r;

    .line 1627531
    iget-object v2, v1, LX/A7r;->a:LX/A7t;

    iget-object v2, v2, LX/A7t;->h:LX/4Jx;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1627532
    const-string v0, "client_mutation_id"

    invoke-virtual {v2, v0, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627533
    iget-object v2, v1, LX/A7r;->a:LX/A7t;

    iget-object p0, v2, LX/A7t;->h:LX/4Jx;

    iget-object v2, v1, LX/A7r;->a:LX/A7t;

    iget-object v2, v2, LX/A7t;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1627534
    const-string v0, "actor_id"

    invoke-virtual {p0, v0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627535
    iget-object v2, v1, LX/A7r;->a:LX/A7t;

    iget-object v2, v2, LX/A7t;->h:LX/4Jx;

    iget-object p0, v1, LX/A7r;->a:LX/A7t;

    iget p0, p0, LX/A7t;->i:I

    add-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1627536
    const-string v0, "score"

    invoke-virtual {v2, v0, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1627537
    iget-object v2, v1, LX/A7r;->a:LX/A7t;

    iget-object v2, v2, LX/A7t;->h:LX/4Jx;

    iget-object p0, v1, LX/A7r;->a:LX/A7t;

    iget-object p0, p0, LX/A7t;->j:Ljava/lang/String;

    .line 1627538
    const-string v0, "text_feedback"

    invoke-virtual {v2, v0, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627539
    new-instance v2, LX/A7w;

    invoke-direct {v2}, LX/A7w;-><init>()V

    move-object v2, v2

    .line 1627540
    const-string p0, "input"

    iget-object v0, v1, LX/A7r;->a:LX/A7t;

    iget-object v0, v0, LX/A7t;->h:LX/4Jx;

    invoke-virtual {v2, p0, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1627541
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1627542
    iget-object p0, v1, LX/A7r;->a:LX/A7t;

    iget-object p0, p0, LX/A7t;->b:LX/0tX;

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1627543
    new-instance p0, LX/A7q;

    invoke-direct {p0, v1}, LX/A7q;-><init>(LX/A7r;)V

    iget-object v0, v1, LX/A7r;->a:LX/A7t;

    iget-object v0, v0, LX/A7t;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, p0, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1627544
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1627525
    iget-object v0, p0, LX/A8L;->a:LX/A8N;

    iget-object v0, v0, LX/A8N;->b:LX/A8D;

    invoke-virtual {v0}, LX/0ht;->f()V

    .line 1627526
    return-void
.end method
