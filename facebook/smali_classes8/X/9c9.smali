.class public LX/9c9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Zb;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516002
    iput-object p1, p0, LX/9c9;->b:Ljava/lang/String;

    .line 1516003
    iput-object p2, p0, LX/9c9;->a:LX/0Zb;

    .line 1516004
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1516005
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1516006
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516007
    iget-object v0, p0, LX/9c9;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1516008
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "composer_creative_editing_tools"

    invoke-direct {v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "creative_editing"

    .line 1516009
    iput-object v3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1516010
    move-object v0, v0

    .line 1516011
    iget-object v3, p0, LX/9c9;->b:Ljava/lang/String;

    .line 1516012
    iput-object v3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1516013
    move-object v0, v0

    .line 1516014
    sget-object v3, LX/9c8;->MEDIA_ITEM_IDENTIFIER:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->NUMBER_OF_UG_ENTRIES:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->NUMBER_OF_STICKERS_ENTRIES:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->NUMBER_OF_STICKER_ON_PHOTO:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->j:I

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->NUMBER_OF_TEXT_ENTRIES:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->NUMBER_OF_TEXT_ON_PHOTO:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->k:I

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->NUMBER_OF_CROP_ENTRIES:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->NUMBER_OF_DOODLE_ON_PHOTO:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->l:I

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->IS_PHOTO_CROPPED:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a:Z

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v3, LX/9c8;->IS_PHOTO_ROTATED:LX/9c8;

    invoke-virtual {v3}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v3

    iget v4, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->m:I

    if-eqz v4, :cond_2

    :goto_2
    invoke-virtual {v0, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/9c8;->NUMBER_OF_FILTER_ENTRIES:LX/9c8;

    invoke-virtual {v1}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v1

    iget v2, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/9c8;->NUMBER_OF_SWIPES:LX/9c8;

    invoke-virtual {v1}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v1

    iget v2, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/9c8;->APPLIED_FILTER_NAME:LX/9c8;

    invoke-virtual {v1}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/9c8;->IS_PHOTO_DELETED:LX/9c8;

    invoke-virtual {v1}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/9c8;->IS_PHOTO_PUBLISHED:LX/9c8;

    invoke-virtual {v1}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->c:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/9c8;->Entry_POINT:LX/9c8;

    invoke-virtual {v1}, LX/9c8;->getParamKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->o:LX/5Rz;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1516015
    iget-object v1, p0, LX/9c9;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1516016
    return-void

    :cond_0
    move v0, v2

    .line 1516017
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1516018
    goto/16 :goto_1

    :cond_2
    move v1, v2

    .line 1516019
    goto :goto_2
.end method
