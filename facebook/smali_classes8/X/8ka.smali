.class public final LX/8ka;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8kb;


# direct methods
.method public constructor <init>(LX/8kb;)V
    .locals 0

    .prologue
    .line 1396538
    iput-object p1, p0, LX/8ka;->a:LX/8kb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x3e476095

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1396539
    iget-object v1, p0, LX/8ka;->a:LX/8kb;

    iget-object v1, v1, LX/8kb;->d:LX/8l8;

    if-eqz v1, :cond_3

    .line 1396540
    iget-object v1, p0, LX/8ka;->a:LX/8kb;

    iget-object v1, v1, LX/8kb;->d:LX/8l8;

    .line 1396541
    iget-object v3, v1, LX/8l8;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v3, v3, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->g:LX/8j6;

    iget-object v4, v1, LX/8l8;->a:LX/8km;

    iget-object v4, v4, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1396542
    iget-object v5, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1396543
    const-string v5, "cancel_button_pressed"

    invoke-static {v3, v5}, LX/8j6;->d(LX/8j6;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1396544
    const-string v6, "pack_id"

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396545
    iget-object v6, v3, LX/8j6;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1396546
    iget-object v3, v1, LX/8l8;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v3, v3, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    if-eqz v3, :cond_3

    .line 1396547
    iget-object v3, v1, LX/8l8;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v3, v3, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    iget-object v4, v1, LX/8l8;->a:LX/8km;

    iget-object v4, v4, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1396548
    iget-object v5, v3, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1396549
    iget-object v6, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    .line 1396550
    iget-object v7, v6, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    move-object v6, v7

    .line 1396551
    iget-object v7, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1396552
    const/4 v8, -0x1

    .line 1396553
    iget-object v9, v6, LX/8CZ;->c:LX/8CX;

    if-nez v9, :cond_4

    .line 1396554
    :cond_0
    :goto_0
    move v6, v8

    .line 1396555
    if-ltz v6, :cond_2

    .line 1396556
    iget-object v7, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v7, :cond_1

    .line 1396557
    iget-object v7, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1396558
    iget-object v8, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    .line 1396559
    iget-object v9, v8, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v8, v9

    .line 1396560
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1396561
    const/4 v7, 0x0

    iput-object v7, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->E:Lcom/facebook/stickers/model/StickerPack;

    .line 1396562
    :cond_1
    iget-object v7, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-virtual {v7, v6}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c(I)V

    .line 1396563
    iget-object v7, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->D:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1396564
    :cond_2
    iget-object v5, v3, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1396565
    iget-object v6, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->C:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1396566
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1396567
    const-string v7, "stickerPack"

    invoke-virtual {v6, v7, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1396568
    iget-object v7, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->o:LX/0aG;

    const-string v8, "add_closed_download_preview_sticker_pack"

    const v9, 0x67d5449a

    invoke-static {v7, v8, v6, v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    .line 1396569
    new-instance v7, LX/8kv;

    invoke-direct {v7, v5, v4}, LX/8kv;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;Lcom/facebook/stickers/model/StickerPack;)V

    .line 1396570
    invoke-static {v6, v7}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v8

    iput-object v8, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->s:LX/1Mv;

    .line 1396571
    iget-object v8, v5, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->i:LX/0TD;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1396572
    :cond_3
    const v1, -0x15740942

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1396573
    :cond_4
    iget-object v9, v6, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result p0

    .line 1396574
    const/4 v9, 0x0

    :goto_1
    if-ge v9, p0, :cond_0

    .line 1396575
    iget-object p1, v6, LX/8CZ;->c:LX/8CX;

    iget-object v1, v6, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, LX/8CX;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    move v8, v9

    .line 1396576
    goto :goto_0

    .line 1396577
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method
