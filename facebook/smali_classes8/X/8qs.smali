.class public LX/8qs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3iM;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0tH;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/3iM;LX/0Or;LX/0tH;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3iM;",
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;",
            "LX/0tH;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1408025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1408026
    iput-object p1, p0, LX/8qs;->a:LX/3iM;

    .line 1408027
    iput-object p2, p0, LX/8qs;->b:LX/0Or;

    .line 1408028
    iput-object p3, p0, LX/8qs;->c:LX/0tH;

    .line 1408029
    iput-object p4, p0, LX/8qs;->d:LX/0ad;

    .line 1408030
    return-void
.end method

.method public static a(LX/0QB;)LX/8qs;
    .locals 1

    .prologue
    .line 1408006
    invoke-static {p0}, LX/8qs;->b(LX/0QB;)LX/8qs;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/8qs;LX/8qr;Lcom/facebook/graphql/model/GraphQLComment;Z)LX/8r1;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1408009
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->T()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8qs;->d:LX/0ad;

    sget-short v2, LX/0wn;->u:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v5, 0x1

    .line 1408010
    :goto_0
    iget-object v1, p0, LX/8qs;->d:LX/0ad;

    sget-short v2, LX/0wn;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v6

    .line 1408011
    iget-object v1, p0, LX/8qs;->d:LX/0ad;

    sget-short v2, LX/0wn;->d:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v7

    .line 1408012
    iget-object v1, p0, LX/8qs;->d:LX/0ad;

    sget-short v2, LX/0wn;->e:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v8

    .line 1408013
    invoke-static {p2}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1408014
    new-instance v0, LX/8r4;

    iget-object v2, p0, LX/8qs;->a:LX/3iM;

    iget-object v3, p0, LX/8qs;->b:LX/0Or;

    move-object v1, p1

    move v4, p3

    invoke-direct/range {v0 .. v8}, LX/8r4;-><init>(LX/8qr;LX/3iM;LX/0Or;ZZZZZ)V

    .line 1408015
    :goto_1
    return-object v0

    :cond_0
    move v5, v0

    .line 1408016
    goto :goto_0

    .line 1408017
    :cond_1
    new-instance v0, LX/8r1;

    iget-object v2, p0, LX/8qs;->a:LX/3iM;

    iget-object v3, p0, LX/8qs;->b:LX/0Or;

    move-object v1, p1

    move v4, p3

    invoke-direct/range {v0 .. v8}, LX/8r1;-><init>(LX/8qr;LX/3iM;LX/0Or;ZZZZZ)V

    goto :goto_1
.end method

.method public static a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 1408018
    invoke-static {p1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v0

    .line 1408019
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1408020
    if-eqz v0, :cond_0

    .line 1408021
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1408022
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1408023
    const/4 v1, 0x1

    const/4 p0, 0x0

    invoke-static {v0, v1, p0}, LX/0YN;->a(Ljava/lang/CharSequence;ZZ)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v0, v1

    .line 1408024
    return-object v0
.end method

.method public static b(LX/0QB;)LX/8qs;
    .locals 5

    .prologue
    .line 1408007
    new-instance v3, LX/8qs;

    invoke-static {p0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v0

    check-cast v0, LX/3iM;

    const/16 v1, 0x2e4

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v1

    check-cast v1, LX/0tH;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v3, v0, v4, v1, v2}, LX/8qs;-><init>(LX/3iM;LX/0Or;LX/0tH;LX/0ad;)V

    .line 1408008
    return-object v3
.end method


# virtual methods
.method public final a(LX/8qr;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8qr;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1408004
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/8qs;->a(LX/8qs;LX/8qr;Lcom/facebook/graphql/model/GraphQLComment;Z)LX/8r1;

    move-result-object v0

    .line 1408005
    invoke-virtual {v0, p2, p3}, LX/8r1;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
