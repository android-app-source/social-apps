.class public LX/AEI;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/8pa;

.field public final c:LX/1Ck;

.field public final d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final e:Landroid/content/Context;

.field private final f:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final g:LX/1CX;

.field private final h:LX/0kb;

.field public final i:LX/0Zb;

.field private final j:LX/2yS;

.field private final k:LX/1Nt;

.field private final l:LX/2yT;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/content/res/Resources;LX/8pa;LX/1Ck;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1CX;LX/0kb;LX/0Zb;LX/2yS;LX/2yT;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646351
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646352
    iput-object p1, p0, LX/AEI;->e:Landroid/content/Context;

    .line 1646353
    iput-object p2, p0, LX/AEI;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1646354
    iput-object p3, p0, LX/AEI;->a:Landroid/content/res/Resources;

    .line 1646355
    iput-object p4, p0, LX/AEI;->b:LX/8pa;

    .line 1646356
    iput-object p5, p0, LX/AEI;->c:LX/1Ck;

    .line 1646357
    iput-object p6, p0, LX/AEI;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1646358
    iput-object p7, p0, LX/AEI;->g:LX/1CX;

    .line 1646359
    iput-object p8, p0, LX/AEI;->h:LX/0kb;

    .line 1646360
    iput-object p9, p0, LX/AEI;->i:LX/0Zb;

    .line 1646361
    iput-object p10, p0, LX/AEI;->j:LX/2yS;

    .line 1646362
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/ClaimCouponActionButton$ClaimCouponActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/ClaimCouponActionButton$ClaimCouponActionButtonPartDefinition;-><init>(LX/AEI;)V

    iput-object v0, p0, LX/AEI;->k:LX/1Nt;

    .line 1646363
    iput-object p11, p0, LX/AEI;->l:LX/2yT;

    .line 1646364
    return-void
.end method

.method public static a(LX/0QB;)LX/AEI;
    .locals 15

    .prologue
    .line 1646365
    const-class v1, LX/AEI;

    monitor-enter v1

    .line 1646366
    :try_start_0
    sget-object v0, LX/AEI;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646367
    sput-object v2, LX/AEI;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646368
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646369
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646370
    new-instance v3, LX/AEI;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    .line 1646371
    new-instance v9, LX/8pa;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static {v0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v8

    check-cast v8, LX/3iV;

    invoke-direct {v9, v7, v8}, LX/8pa;-><init>(LX/0aG;LX/3iV;)V

    .line 1646372
    move-object v7, v9

    .line 1646373
    check-cast v7, LX/8pa;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v9

    check-cast v9, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v10

    check-cast v10, LX/1CX;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v11

    check-cast v11, LX/0kb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v13

    check-cast v13, LX/2yS;

    const-class v14, LX/2yT;

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/2yT;

    invoke-direct/range {v3 .. v14}, LX/AEI;-><init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/content/res/Resources;LX/8pa;LX/1Ck;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1CX;LX/0kb;LX/0Zb;LX/2yS;LX/2yT;)V

    .line 1646374
    move-object v0, v3

    .line 1646375
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646376
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646377
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646378
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLCoupon;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1646379
    invoke-static {p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1646380
    if-nez v0, :cond_0

    .line 1646381
    const/4 v0, 0x0

    .line 1646382
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/AEI;Lcom/facebook/graphql/model/GraphQLCoupon;LX/162;Z)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1646383
    new-instance v0, LX/AEG;

    invoke-direct {v0, p0, p1, p2, p3}, LX/AEG;-><init>(LX/AEI;Lcom/facebook/graphql/model/GraphQLCoupon;LX/162;Z)V

    return-object v0
.end method

.method public static a$redex0(LX/AEI;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1646384
    iget-object v0, p0, LX/AEI;->h:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080039

    .line 1646385
    :goto_0
    iget-object v1, p0, LX/AEI;->g:LX/1CX;

    iget-object v2, p0, LX/AEI;->e:Landroid/content/Context;

    invoke-static {v2}, LX/4mm;->a(Landroid/content/Context;)LX/4mn;

    move-result-object v2

    .line 1646386
    iput-object p1, v2, LX/4mn;->b:Ljava/lang/String;

    .line 1646387
    move-object v2, v2

    .line 1646388
    invoke-virtual {v2, v0}, LX/4mn;->b(I)LX/4mn;

    move-result-object v0

    invoke-virtual {v0}, LX/4mn;->l()LX/4mm;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1646389
    return-void

    .line 1646390
    :cond_0
    const v0, 0x7f08003a

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLCoupon;)I
    .locals 1

    .prologue
    .line 1646391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCoupon;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1646392
    const v0, 0x7f08101c

    .line 1646393
    :goto_0
    return v0

    .line 1646394
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCoupon;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1646395
    const v0, 0x7f08101b

    goto :goto_0

    .line 1646396
    :cond_1
    const v0, 0x7f08101a

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLCoupon;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1646397
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCoupon;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "resend"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "claim"

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646398
    iget-object v0, p0, LX/AEI;->k:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 1646399
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646400
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646401
    invoke-static {v0}, LX/AEI;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v0

    .line 1646402
    if-nez v0, :cond_0

    .line 1646403
    const/4 v0, 0x0

    .line 1646404
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/AEI;->l:LX/2yT;

    const/4 v2, 0x2

    iget-object v3, p0, LX/AEI;->j:LX/2yS;

    invoke-virtual {v1, p4, v2, v3}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v1

    const v2, 0x7f020850

    invoke-virtual {v1, v2}, LX/AE0;->a(I)LX/AE0;

    move-result-object v1

    const/4 v2, 0x1

    .line 1646405
    iput-boolean v2, v1, LX/AE0;->l:Z

    .line 1646406
    move-object v1, v1

    .line 1646407
    invoke-static {v0}, LX/AEI;->c(Lcom/facebook/graphql/model/GraphQLCoupon;)I

    move-result v2

    invoke-virtual {p1, v2}, LX/1De;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1646408
    iput-object v2, v1, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 1646409
    move-object v1, v1

    .line 1646410
    invoke-static {p3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-static {p3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-static {v3}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    invoke-static {p0, v0, v2, v3}, LX/AEI;->a$redex0(LX/AEI;Lcom/facebook/graphql/model/GraphQLCoupon;LX/162;Z)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 1646411
    iput-object v0, v1, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1646412
    move-object v0, v1

    .line 1646413
    goto :goto_0
.end method
