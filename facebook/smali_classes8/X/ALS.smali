.class public final LX/ALS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5fY;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/CameraView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CameraView;)V
    .locals 0

    .prologue
    .line 1664828
    iput-object p1, p0, LX/ALS;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/5fd;Landroid/graphics/Point;)V
    .locals 3

    .prologue
    .line 1664829
    sget-object v0, LX/ALF;->a:[I

    invoke-virtual {p1}, LX/5fd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1664830
    :goto_0
    return-void

    .line 1664831
    :pswitch_0
    iget-object v0, p0, LX/ALS;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->k:Lcom/facebook/backstage/camera/FocusView;

    iget v1, p2, Landroid/graphics/Point;->x:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/backstage/camera/FocusView;->a(II)V

    .line 1664832
    iget-object v0, p0, LX/ALS;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->k:Lcom/facebook/backstage/camera/FocusView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/FocusView;->d()V

    goto :goto_0

    .line 1664833
    :pswitch_1
    iget-object v0, p0, LX/ALS;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->k:Lcom/facebook/backstage/camera/FocusView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/FocusView;->a()V

    goto :goto_0

    .line 1664834
    :pswitch_2
    iget-object v0, p0, LX/ALS;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->k:Lcom/facebook/backstage/camera/FocusView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/FocusView;->b()V

    goto :goto_0

    .line 1664835
    :pswitch_3
    iget-object v0, p0, LX/ALS;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->k:Lcom/facebook/backstage/camera/FocusView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/FocusView;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
