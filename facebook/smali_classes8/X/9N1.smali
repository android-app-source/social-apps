.class public interface abstract LX/9N1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9Kq;
.implements LX/9Mt;
.implements LX/9Mu;
.implements LX/9Mv;
.implements LX/9Mw;
.implements LX/9Mx;
.implements LX/9My;
.implements LX/9Mz;
.implements LX/9N0;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Forwarder;
    processor = "com.facebook.dracula.transformer.Transformer"
    to = "FetchGroupInformation$"
.end annotation


# virtual methods
.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public abstract e()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract hP_()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract hQ_()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()Z
.end method

.method public abstract n()LX/175;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()Z
.end method

.method public abstract p()Z
.end method

.method public abstract q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()I
.end method

.method public abstract s()Z
.end method

.method public abstract t()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract v()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
