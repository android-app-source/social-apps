.class public final LX/8yq;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field private final a:LX/HJI;

.field private final b:LX/1yA;


# direct methods
.method public constructor <init>(LX/HJI;LX/1yA;)V
    .locals 0

    .prologue
    .line 1426588
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 1426589
    iput-object p1, p0, LX/8yq;->a:LX/HJI;

    .line 1426590
    iput-object p2, p0, LX/8yq;->b:LX/1yA;

    .line 1426591
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1426579
    iget-object v0, p0, LX/8yq;->a:LX/HJI;

    if-eqz v0, :cond_1

    .line 1426580
    iget-object v0, p0, LX/8yq;->a:LX/HJI;

    iget-object v1, p0, LX/8yq;->b:LX/1yA;

    .line 1426581
    iget-object v2, v0, LX/HJI;->a:LX/HJL;

    iget-object v2, v2, LX/HJL;->e:LX/1nG;

    invoke-virtual {v2, v1}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v2

    .line 1426582
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1426583
    invoke-interface {v1}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v2

    .line 1426584
    :cond_0
    iget-object p0, v0, LX/HJI;->a:LX/HJL;

    iget-object p0, p0, LX/HJL;->b:LX/17W;

    iget-object p1, v0, LX/HJI;->a:LX/HJL;

    iget-object p1, p1, LX/HJL;->a:Landroid/content/Context;

    invoke-virtual {p0, p1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1426585
    :cond_1
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1426586
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1426587
    return-void
.end method
