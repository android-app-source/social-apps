.class public LX/A5P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/A5P;


# instance fields
.field private final a:LX/2O6;


# direct methods
.method public constructor <init>(LX/2O6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1621062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621063
    iput-object p1, p0, LX/A5P;->a:LX/2O6;

    .line 1621064
    return-void
.end method

.method public static a(LX/0QB;)LX/A5P;
    .locals 4

    .prologue
    .line 1621049
    sget-object v0, LX/A5P;->b:LX/A5P;

    if-nez v0, :cond_1

    .line 1621050
    const-class v1, LX/A5P;

    monitor-enter v1

    .line 1621051
    :try_start_0
    sget-object v0, LX/A5P;->b:LX/A5P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1621052
    if-eqz v2, :cond_0

    .line 1621053
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1621054
    new-instance p0, LX/A5P;

    invoke-static {v0}, LX/2O5;->a(LX/0QB;)LX/2O6;

    move-result-object v3

    check-cast v3, LX/2O6;

    invoke-direct {p0, v3}, LX/A5P;-><init>(LX/2O6;)V

    .line 1621055
    move-object v0, p0

    .line 1621056
    sput-object v0, LX/A5P;->b:LX/A5P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1621057
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1621058
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1621059
    :cond_1
    sget-object v0, LX/A5P;->b:LX/A5P;

    return-object v0

    .line 1621060
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1621061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1621047
    iget-object v0, p0, LX/A5P;->a:LX/2O6;

    invoke-virtual {v0}, LX/2O6;->invalidate()V

    .line 1621048
    return-void
.end method
