.class public final LX/9UT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/profilelist/ProfileListDynamicAdapter$SortProfilesTask;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/profilelist/ProfileListDynamicAdapter$SortProfilesTask;)V
    .locals 0

    .prologue
    .line 1498026
    iput-object p1, p0, LX/9UT;->a:Lcom/facebook/katana/activity/profilelist/ProfileListDynamicAdapter$SortProfilesTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1498027
    check-cast p1, Lcom/facebook/ipc/model/FacebookProfile;

    check-cast p2, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1498028
    iget-object v0, p1, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    iget-object v1, p2, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
