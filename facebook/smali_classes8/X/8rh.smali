.class public final LX/8rh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic b:LX/8rk;


# direct methods
.method public constructor <init>(LX/8rk;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 1409033
    iput-object p1, p0, LX/8rh;->b:LX/8rk;

    iput-object p2, p0, LX/8rh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x7360f80

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1409034
    iget-object v1, p0, LX/8rh;->b:LX/8rk;

    iget-object v2, p0, LX/8rh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1409035
    iget-object v4, v1, LX/8rk;->n:LX/8rg;

    instance-of v4, v4, LX/8rv;

    if-eqz v4, :cond_1

    iget-object v4, v1, LX/8rk;->o:LX/8s1;

    invoke-static {v4}, LX/8s1;->isCommentMentionSupported(LX/8s1;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v4, v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1409036
    invoke-static {v1, p1}, LX/8rk;->a(LX/8rk;Landroid/view/View;)V

    .line 1409037
    :goto_0
    iget-object v1, p0, LX/8rh;->b:LX/8rk;

    iget-object v2, p0, LX/8rh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1409038
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    if-eqz v4, :cond_0

    if-nez v2, :cond_2

    .line 1409039
    :cond_0
    :goto_1
    const v1, -0x4ce4b421

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1409040
    :cond_1
    iget-object v4, v1, LX/8rk;->l:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 1409041
    iget-object v5, v1, LX/8rk;->c:LX/2hX;

    iget-object v4, v1, LX/8rk;->k:LX/36N;

    invoke-interface {v4}, LX/36N;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iget-object v4, v1, LX/8rk;->k:LX/36N;

    invoke-interface {v4}, LX/36N;->v_()Ljava/lang/String;

    move-result-object v8

    sget-object v9, LX/2h7;->PROFILE_BROWSER:LX/2h7;

    move-object v10, v2

    invoke-virtual/range {v5 .. v10}, LX/2hX;->a(JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 1409042
    :cond_2
    sget-object v4, LX/8ri;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 1409043
    :pswitch_0
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    .line 1409044
    const-string v5, "add_friend_click"

    const-string v1, "friend_button"

    invoke-static {v4, v5, v1}, LX/82m;->a(LX/82m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1409045
    goto :goto_1

    .line 1409046
    :pswitch_1
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    .line 1409047
    const-string v5, "cancel_friend_click"

    const-string v1, "friend_button"

    invoke-static {v4, v5, v1}, LX/82m;->a(LX/82m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1409048
    goto :goto_1

    .line 1409049
    :pswitch_2
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    if-nez v4, :cond_3

    .line 1409050
    :goto_2
    goto :goto_1

    .line 1409051
    :cond_3
    iget-object v4, v1, LX/8rk;->o:LX/8s1;

    invoke-static {v4}, LX/8s1;->isCommentMentionSupported(LX/8s1;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v1, LX/8rk;->e:LX/5Ou;

    invoke-virtual {v4}, LX/5Ou;->a()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1409052
    :cond_4
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    .line 1409053
    const-string v5, "unfriend_click"

    const-string v1, "friend_button"

    invoke-static {v4, v5, v1}, LX/82m;->a(LX/82m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1409054
    goto :goto_2

    .line 1409055
    :cond_5
    iget-object v4, v1, LX/8rk;->e:LX/5Ou;

    invoke-virtual {v4}, LX/5Ou;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1409056
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    .line 1409057
    const-string v5, "message_click"

    const-string v1, "mention_button"

    invoke-static {v4, v5, v1}, LX/82m;->a(LX/82m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1409058
    goto :goto_2

    .line 1409059
    :cond_6
    iget-object v4, v1, LX/8rk;->e:LX/5Ou;

    invoke-virtual {v4}, LX/5Ou;->c()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1409060
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    .line 1409061
    const-string v5, "comment_click"

    const-string v1, "mention_button"

    invoke-static {v4, v5, v1}, LX/82m;->a(LX/82m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1409062
    goto :goto_2

    .line 1409063
    :cond_7
    iget-object v4, v1, LX/8rk;->q:LX/82m;

    .line 1409064
    const-string v5, "comment_click"

    const-string v1, "comment_button"

    invoke-static {v4, v5, v1}, LX/82m;->a(LX/82m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1409065
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
