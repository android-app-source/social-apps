.class public LX/99a;
.super LX/1UF;
.source ""

# interfaces
.implements LX/1Qq;


# instance fields
.field public final a:LX/1Qq;


# direct methods
.method public constructor <init>(LX/1Qq;)V
    .locals 0

    .prologue
    .line 1447415
    invoke-direct {p0, p1}, LX/1UF;-><init>(LX/1Qr;)V

    .line 1447416
    iput-object p1, p0, LX/99a;->a:LX/1Qq;

    .line 1447417
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1447418
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0, p1, p2}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 1447419
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 1447420
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1447421
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1447423
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1447422
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1447424
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1447414
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1447407
    invoke-virtual {p0, p1}, LX/99a;->getItemViewType(I)I

    move-result v4

    .line 1447408
    if-nez p2, :cond_1

    .line 1447409
    invoke-virtual {p0, v4, p3}, LX/99a;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1447410
    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "createView() shall not return null value!"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1447411
    :goto_1
    invoke-virtual {p0, p1}, LX/99a;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, p0

    move v1, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/99a;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 1447412
    return-object v3

    .line 1447413
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v3, p2

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1447397
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1447406
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 1447405
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1447404
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 1447402
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 1447403
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1447400
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1447401
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1447398
    iget-object v0, p0, LX/99a;->a:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1447399
    return-void
.end method
