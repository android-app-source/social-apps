.class public final LX/9AL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    .line 1449824
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1449825
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1449826
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1449827
    const/4 v2, 0x0

    .line 1449828
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1449829
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449830
    :goto_1
    move v1, v2

    .line 1449831
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1449832
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1449833
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1449834
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1449835
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1449836
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1449837
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1449838
    const-string v4, "style_list"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1449839
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_2

    .line 1449840
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1449841
    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1449842
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1449843
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1449844
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1449845
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 1449846
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1449847
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 1449848
    if-eqz v2, :cond_0

    .line 1449849
    const-string v2, "style_list"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449850
    invoke-virtual {p0, v1, p3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1449851
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1449852
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1449853
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1449854
    return-void
.end method
