.class public LX/AFO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile n:LX/AFO;


# instance fields
.field public final b:LX/AFM;

.field public final c:LX/AHZ;

.field public final d:LX/1El;

.field public final e:LX/7gh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7gh",
            "<",
            "Lcom/facebook/audience/direct/data/ReplyDataProvider$ReplyThreadObserver;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/0SG;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/AFP;

.field public final k:LX/AEv;

.field public final l:LX/AFJ;

.field private final m:LX/AF1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1647688
    const-class v0, LX/AFO;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AFO;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AHZ;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/1El;LX/AFP;LX/AEv;LX/AFJ;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647674
    new-instance v0, LX/AFM;

    invoke-direct {v0, p0}, LX/AFM;-><init>(LX/AFO;)V

    iput-object v0, p0, LX/AFO;->b:LX/AFM;

    .line 1647675
    new-instance v0, LX/7gh;

    invoke-direct {v0}, LX/7gh;-><init>()V

    iput-object v0, p0, LX/AFO;->e:LX/7gh;

    .line 1647676
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AFO;->i:Ljava/util/Map;

    .line 1647677
    new-instance v0, LX/AFN;

    invoke-direct {v0, p0}, LX/AFN;-><init>(LX/AFO;)V

    iput-object v0, p0, LX/AFO;->m:LX/AF1;

    .line 1647678
    iput-object p1, p0, LX/AFO;->c:LX/AHZ;

    .line 1647679
    iput-object p2, p0, LX/AFO;->f:Ljava/util/concurrent/ExecutorService;

    .line 1647680
    iput-object p3, p0, LX/AFO;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1647681
    iput-object p4, p0, LX/AFO;->h:LX/0SG;

    .line 1647682
    iput-object p5, p0, LX/AFO;->d:LX/1El;

    .line 1647683
    iput-object p6, p0, LX/AFO;->j:LX/AFP;

    .line 1647684
    iput-object p7, p0, LX/AFO;->k:LX/AEv;

    .line 1647685
    iput-object p8, p0, LX/AFO;->l:LX/AFJ;

    .line 1647686
    iget-object v0, p0, LX/AFO;->d:LX/1El;

    iget-object v1, p0, LX/AFO;->m:LX/AF1;

    invoke-virtual {v0, v1}, LX/1El;->a(LX/AF1;)V

    .line 1647687
    return-void
.end method

.method public static a(LX/0QB;)LX/AFO;
    .locals 3

    .prologue
    .line 1647629
    sget-object v0, LX/AFO;->n:LX/AFO;

    if-nez v0, :cond_1

    .line 1647630
    const-class v1, LX/AFO;

    monitor-enter v1

    .line 1647631
    :try_start_0
    sget-object v0, LX/AFO;->n:LX/AFO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1647632
    if-eqz v2, :cond_0

    .line 1647633
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/AFO;->b(LX/0QB;)LX/AFO;

    move-result-object v0

    sput-object v0, LX/AFO;->n:LX/AFO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1647634
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1647635
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1647636
    :cond_1
    sget-object v0, LX/AFO;->n:LX/AFO;

    return-object v0

    .line 1647637
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1647638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/AFO;LX/0zS;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1647656
    iget-object v0, p0, LX/AFO;->c:LX/AHZ;

    iget-object v1, p0, LX/AFO;->b:LX/AFM;

    .line 1647657
    iget-object v2, v0, LX/AHZ;->f:LX/0gK;

    invoke-virtual {v2}, LX/0gK;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1647658
    new-instance v6, LX/AFi;

    invoke-direct {v6}, LX/AFi;-><init>()V

    move-object v6, v6

    .line 1647659
    const-string v7, "3"

    const-string v8, "4"

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1647660
    const-string v7, "2"

    invoke-virtual {v6, v7, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1647661
    const-string v7, "0"

    const/16 v8, 0x32

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1647662
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v6, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    const-wide/16 v8, 0x1c20

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    .line 1647663
    iget-object v7, v0, LX/AHZ;->c:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    move-object v3, v6

    .line 1647664
    new-instance v4, LX/AHY;

    invoke-direct {v4, v0, p2, v1}, LX/AHY;-><init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V

    iget-object v5, v0, LX/AHZ;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1647665
    :goto_0
    return-void

    .line 1647666
    :cond_0
    new-instance v6, LX/AFh;

    invoke-direct {v6}, LX/AFh;-><init>()V

    move-object v6, v6

    .line 1647667
    const-string v7, "2"

    const-string v8, "4"

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1647668
    const-string v7, "4"

    invoke-virtual {v6, v7, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1647669
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v6, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    const-wide/16 v8, 0x1c20

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    .line 1647670
    iget-object v7, v0, LX/AHZ;->c:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    move-object v3, v6

    .line 1647671
    new-instance v4, LX/AHX;

    invoke-direct {v4, v0, p2, v1}, LX/AHX;-><init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V

    iget-object v5, v0, LX/AHZ;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1647672
    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/AFO;
    .locals 15

    .prologue
    .line 1647647
    new-instance v0, LX/AFO;

    .line 1647648
    new-instance v9, LX/AHZ;

    invoke-static {p0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v10

    check-cast v10, LX/0gX;

    const/16 v11, 0x15e7

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v13

    check-cast v13, LX/0tX;

    invoke-static {p0}, LX/0gK;->b(LX/0QB;)LX/0gK;

    move-result-object v14

    check-cast v14, LX/0gK;

    invoke-direct/range {v9 .. v14}, LX/AHZ;-><init>(LX/0gX;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0gK;)V

    .line 1647649
    move-object v1, v9

    .line 1647650
    check-cast v1, LX/AHZ;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/1El;->a(LX/0QB;)LX/1El;

    move-result-object v5

    check-cast v5, LX/1El;

    .line 1647651
    new-instance v6, LX/AFP;

    invoke-direct {v6}, LX/AFP;-><init>()V

    .line 1647652
    move-object v6, v6

    .line 1647653
    move-object v6, v6

    .line 1647654
    check-cast v6, LX/AFP;

    invoke-static {p0}, LX/AEv;->a(LX/0QB;)LX/AEv;

    move-result-object v7

    check-cast v7, LX/AEv;

    invoke-static {p0}, LX/AFJ;->a(LX/0QB;)LX/AFJ;

    move-result-object v8

    check-cast v8, LX/AFJ;

    invoke-direct/range {v0 .. v8}, LX/AFO;-><init>(LX/AHZ;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/1El;LX/AFP;LX/AEv;LX/AFJ;)V

    .line 1647655
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1647645
    sget-object v0, LX/0zS;->e:LX/0zS;

    invoke-static {p0, v0, p1}, LX/AFO;->a(LX/AFO;LX/0zS;Ljava/lang/String;)V

    .line 1647646
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1647639
    iget-object v0, p0, LX/AFO;->j:LX/AFP;

    invoke-virtual {v0, p1}, LX/AFP;->a(Ljava/lang/String;)Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    .line 1647640
    if-eqz v0, :cond_0

    .line 1647641
    iget-object v1, p0, LX/AFO;->b:LX/AFM;

    invoke-virtual {v1, v0}, LX/AFM;->a(Lcom/facebook/audience/model/ReplyThread;)V

    .line 1647642
    :goto_0
    return-void

    .line 1647643
    :cond_0
    sget-object v0, LX/0zS;->b:LX/0zS;

    invoke-static {p0, v0, p1}, LX/AFO;->a(LX/AFO;LX/0zS;Ljava/lang/String;)V

    .line 1647644
    sget-object v0, LX/0zS;->d:LX/0zS;

    invoke-static {p0, v0, p1}, LX/AFO;->a(LX/AFO;LX/0zS;Ljava/lang/String;)V

    goto :goto_0
.end method
