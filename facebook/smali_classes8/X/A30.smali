.class public final LX/A30;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 1615110
    const-wide/16 v18, 0x0

    .line 1615111
    const/16 v17, 0x0

    .line 1615112
    const/16 v16, 0x0

    .line 1615113
    const/4 v15, 0x0

    .line 1615114
    const/4 v14, 0x0

    .line 1615115
    const/4 v13, 0x0

    .line 1615116
    const/4 v12, 0x0

    .line 1615117
    const-wide/16 v10, 0x0

    .line 1615118
    const-wide/16 v8, 0x0

    .line 1615119
    const/4 v7, 0x0

    .line 1615120
    const/4 v6, 0x0

    .line 1615121
    const/4 v5, 0x0

    .line 1615122
    const/4 v4, 0x0

    .line 1615123
    const/4 v3, 0x0

    .line 1615124
    const/4 v2, 0x0

    .line 1615125
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_11

    .line 1615126
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1615127
    const/4 v2, 0x0

    .line 1615128
    :goto_0
    return v2

    .line 1615129
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_c

    .line 1615130
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1615131
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1615132
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1615133
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1615134
    const/4 v2, 0x1

    .line 1615135
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1615136
    :cond_1
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1615137
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 1615138
    :cond_2
    const-string v6, "image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1615139
    invoke-static/range {p0 .. p1}, LX/A2n;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 1615140
    :cond_3
    const-string v6, "is_on_sale"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1615141
    const/4 v2, 0x1

    .line 1615142
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v20, v6

    goto :goto_1

    .line 1615143
    :cond_4
    const-string v6, "item_price"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1615144
    invoke-static/range {p0 .. p1}, LX/A2o;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1615145
    :cond_5
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1615146
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1615147
    :cond_6
    const-string v6, "parent_story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1615148
    invoke-static/range {p0 .. p1}, LX/A2w;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1615149
    :cond_7
    const-string v6, "product_latitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1615150
    const/4 v2, 0x1

    .line 1615151
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 1615152
    :cond_8
    const-string v6, "product_longitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1615153
    const/4 v2, 0x1

    .line 1615154
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v14, v6

    goto/16 :goto_1

    .line 1615155
    :cond_9
    const-string v6, "sale_price"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1615156
    invoke-static/range {p0 .. p1}, LX/A2x;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1615157
    :cond_a
    const-string v6, "seller"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1615158
    invoke-static/range {p0 .. p1}, LX/A2z;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1615159
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1615160
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1615161
    if-eqz v3, :cond_d

    .line 1615162
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1615163
    :cond_d
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1615164
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1615165
    if-eqz v10, :cond_e

    .line 1615166
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1615167
    :cond_e
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1615168
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1615169
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1615170
    if-eqz v9, :cond_f

    .line 1615171
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1615172
    :cond_f
    if-eqz v8, :cond_10

    .line 1615173
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1615174
    :cond_10
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1615175
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1615176
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v20, v15

    move/from16 v21, v16

    move/from16 v22, v17

    move-wide/from16 v16, v10

    move v11, v6

    move v10, v4

    move-wide/from16 v23, v8

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v18

    move/from16 v19, v14

    move/from16 v18, v13

    move v13, v12

    move-wide/from16 v14, v23

    move v12, v7

    goto/16 :goto_1
.end method
