.class public final LX/91y;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;)V
    .locals 0

    .prologue
    .line 1431748
    iput-object p1, p0, LX/91y;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 1431749
    iget-object v0, p0, LX/91y;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "minutiae_object"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431750
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1431751
    const-string v2, "minutiae_object"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1431752
    iget-object v0, p0, LX/91y;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1431753
    iget-object v0, p0, LX/91y;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerActivity;->finish()V

    .line 1431754
    return-void
.end method
