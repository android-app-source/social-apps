.class public final LX/A9v;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1636467
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 1636468
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1636469
    :goto_0
    return v1

    .line 1636470
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_8

    .line 1636471
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1636472
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1636473
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 1636474
    const-string v12, "amount_offset"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1636475
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v10, v3

    move v3, v2

    goto :goto_1

    .line 1636476
    :cond_1
    const-string v12, "budget"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1636477
    invoke-static {p0, p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1636478
    :cond_2
    const-string v12, "estimate_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1636479
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 1636480
    :cond_3
    const-string v12, "estimated_reach"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1636481
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 1636482
    :cond_4
    const-string v12, "reach_interval"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1636483
    invoke-static {p0, p1}, LX/AA9;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1636484
    :cond_5
    const-string v12, "secondary_estimate_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1636485
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1636486
    :cond_6
    const-string v12, "secondary_reach_interval"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1636487
    invoke-static {p0, p1}, LX/AA9;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1636488
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1636489
    :cond_8
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1636490
    if-eqz v3, :cond_9

    .line 1636491
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 1636492
    :cond_9
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 1636493
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1636494
    if-eqz v0, :cond_a

    .line 1636495
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 1636496
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1636497
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1636498
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1636499
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1636500
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636501
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1636502
    if-eqz v0, :cond_0

    .line 1636503
    const-string v1, "amount_offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636504
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1636505
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636506
    if-eqz v0, :cond_1

    .line 1636507
    const-string v1, "budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636508
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1636509
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1636510
    if-eqz v0, :cond_2

    .line 1636511
    const-string v0, "estimate_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636512
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636513
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1636514
    if-eqz v0, :cond_3

    .line 1636515
    const-string v1, "estimated_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636516
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1636517
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636518
    if-eqz v0, :cond_4

    .line 1636519
    const-string v1, "reach_interval"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636520
    invoke-static {p0, v0, p2}, LX/AA9;->a(LX/15i;ILX/0nX;)V

    .line 1636521
    :cond_4
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1636522
    if-eqz v0, :cond_5

    .line 1636523
    const-string v0, "secondary_estimate_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636524
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636525
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636526
    if-eqz v0, :cond_6

    .line 1636527
    const-string v1, "secondary_reach_interval"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636528
    invoke-static {p0, v0, p2}, LX/AA9;->a(LX/15i;ILX/0nX;)V

    .line 1636529
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636530
    return-void
.end method
