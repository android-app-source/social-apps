.class public final LX/AL5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:LX/AL7;


# direct methods
.method public constructor <init>(LX/AL7;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1664579
    iput-object p1, p0, LX/AL5;->b:LX/AL7;

    iput-object p2, p0, LX/AL5;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 1664580
    iget-object v0, p0, LX/AL5;->b:LX/AL7;

    iget-object v0, v0, LX/AL7;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1664581
    iget-object v0, p0, LX/AL5;->b:LX/AL7;

    iget-object v1, p0, LX/AL5;->a:Landroid/app/Activity;

    iget-object v2, p0, LX/AL5;->b:LX/AL7;

    iget-object v2, v2, LX/AL7;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/AL7;->a$redex0(LX/AL7;Landroid/app/Activity;Landroid/os/IBinder;)V

    .line 1664582
    return-void
.end method
