.class public LX/9Ag;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Jt;


# instance fields
.field private final a:LX/1UQ;


# direct methods
.method public constructor <init>(LX/1UQ;)V
    .locals 0

    .prologue
    .line 1450300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1450301
    iput-object p1, p0, LX/9Ag;->a:LX/1UQ;

    .line 1450302
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1450303
    iget-object v0, p0, LX/9Ag;->a:LX/1UQ;

    invoke-virtual {v0}, LX/1UQ;->b()I

    move-result v2

    .line 1450304
    if-ltz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "firstPosition is invalid"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1450305
    if-nez v2, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 1450306
    goto :goto_0

    .line 1450307
    :cond_1
    add-int/lit8 v1, v2, -0x1

    goto :goto_1
.end method
