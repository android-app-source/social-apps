.class public LX/92C;
.super LX/92A;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/92C;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1432092
    const-string v0, "minutiae_object_picker_time_to_fetch_end"

    const-string v1, "minutiae_object_picker_time_to_fetch_end_cached"

    const-string v2, "minutiae_object_picker_time_to_results_shown"

    const-string v3, "minutiae_object_picker_time_to_results_shown_cached"

    const-string v4, "minutiae_object_picker_fetch_time"

    const-string v5, "minutiae_object_picker_fetch_time_cached"

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92C;->a:Ljava/util/Map;

    .line 1432093
    const v0, 0x420003

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x420008

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x420004

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x420009

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x420005

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v5, 0x42000a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92C;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432090
    sget-object v0, LX/92C;->a:Ljava/util/Map;

    sget-object v1, LX/92C;->b:Ljava/util/Map;

    invoke-direct {p0, p1, v0, v1}, LX/92A;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Ljava/util/Map;Ljava/util/Map;)V

    .line 1432091
    return-void
.end method

.method public static a(LX/0QB;)LX/92C;
    .locals 4

    .prologue
    .line 1432077
    sget-object v0, LX/92C;->c:LX/92C;

    if-nez v0, :cond_1

    .line 1432078
    const-class v1, LX/92C;

    monitor-enter v1

    .line 1432079
    :try_start_0
    sget-object v0, LX/92C;->c:LX/92C;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1432080
    if-eqz v2, :cond_0

    .line 1432081
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1432082
    new-instance p0, LX/92C;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/92C;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 1432083
    move-object v0, p0

    .line 1432084
    sput-object v0, LX/92C;->c:LX/92C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432085
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1432086
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432087
    :cond_1
    sget-object v0, LX/92C;->c:LX/92C;

    return-object v0

    .line 1432088
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1432089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1432094
    const v0, 0x420001

    const-string v1, "minutiae_object_picker_time_to_init"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432095
    const v0, 0x420002

    const-string v1, "minutiae_object_picker_time_to_fetch_start"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432096
    const v0, 0x420003

    const-string v1, "minutiae_object_picker_time_to_fetch_end"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432097
    const v0, 0x420004

    const-string v1, "minutiae_object_picker_time_to_results_shown"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432098
    const v0, 0x420007

    const-string v1, "minutiae_object_picker_time_to_search_shown"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432099
    return-void
.end method

.method public final a(LX/0ta;)V
    .locals 2

    .prologue
    .line 1432073
    const v0, 0x420003

    const-string v1, "minutiae_object_picker_time_to_fetch_end"

    invoke-virtual {p0, v0, v1, p1}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1432074
    const v0, 0x420005

    const-string v1, "minutiae_object_picker_fetch_time"

    invoke-virtual {p0, v0, v1, p1}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1432075
    const v0, 0x420006

    const-string v1, "minutiae_object_picker_rendering_time"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432076
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1432071
    const v0, 0x42000b

    const-string v1, "minutiae_object_picker_time_to_scroll_load"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432072
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1432069
    const v0, 0x42000b

    const-string v1, "minutiae_object_picker_time_to_scroll_load"

    invoke-virtual {p0, v0, v1}, LX/92A;->b(ILjava/lang/String;)V

    .line 1432070
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1432065
    const v0, 0x420003

    const-string v1, "minutiae_object_picker_time_to_fetch_end"

    invoke-virtual {p0, v0, v1}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432066
    const v0, 0x420004

    const-string v1, "minutiae_object_picker_time_to_results_shown"

    invoke-virtual {p0, v0, v1}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432067
    const v0, 0x420005

    const-string v1, "minutiae_object_picker_fetch_time"

    invoke-virtual {p0, v0, v1}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432068
    return-void
.end method
