.class public final LX/AT4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9ii;


# instance fields
.field public final synthetic a:LX/9ij;

.field public final synthetic b:LX/AT6;


# direct methods
.method public constructor <init>(LX/AT6;LX/9ij;)V
    .locals 0

    .prologue
    .line 1675140
    iput-object p1, p0, LX/AT4;->b:LX/AT6;

    iput-object p2, p0, LX/AT4;->a:LX/9ij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/tagging/Tag;)V
    .locals 3

    .prologue
    .line 1675141
    iget-object v0, p0, LX/AT4;->a:LX/9ij;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675142
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675143
    iget-object v0, p0, LX/AT4;->b:LX/AT6;

    iget-object v0, v0, LX/AT6;->h:LX/8GZ;

    invoke-virtual {v0, p1}, LX/8GZ;->a(LX/362;)LX/362;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1675144
    if-nez v0, :cond_1

    .line 1675145
    :cond_0
    :goto_0
    return-void

    .line 1675146
    :cond_1
    iget-object v1, p0, LX/AT4;->b:LX/AT6;

    iget-object v1, v1, LX/AT6;->k:LX/75Q;

    iget-object v2, p0, LX/AT4;->b:LX/AT6;

    iget-object v2, v2, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v1, v2, v0}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/base/tagging/Tag;)V

    .line 1675147
    iget-object v0, p0, LX/AT4;->b:LX/AT6;

    iget-object v0, v0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    iget-object v1, p0, LX/AT4;->b:LX/AT6;

    iget-object v1, v1, LX/AT6;->g:LX/9ic;

    iget-object v2, p0, LX/AT4;->b:LX/AT6;

    iget-object v2, v2, LX/AT6;->m:Ljava/util/List;

    invoke-virtual {v1, v2}, LX/9ic;->a(Ljava/util/List;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setFaceBoxes(Ljava/util/Collection;)V

    .line 1675148
    iget-object v0, p0, LX/AT4;->b:LX/AT6;

    iget-object v0, v0, LX/AT6;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/AT4;->a:LX/9ij;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1675149
    iget-object v0, p0, LX/AT4;->b:LX/AT6;

    iget-object v0, v0, LX/AT6;->a:Ljava/util/HashMap;

    iget-object v1, p0, LX/AT4;->a:LX/9ij;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675150
    iget-object v0, p0, LX/AT4;->b:LX/AT6;

    iget-object v0, v0, LX/AT6;->b:LX/8Jm;

    iget-object v1, p0, LX/AT4;->b:LX/AT6;

    iget-object v1, v1, LX/AT6;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, LX/8Jm;->a(Ljava/util/Map;)V

    .line 1675151
    iget-object v0, p0, LX/AT4;->b:LX/AT6;

    iget-object v0, v0, LX/AT6;->j:LX/Hqr;

    if-eqz v0, :cond_0

    .line 1675152
    iget-object v0, p0, LX/AT4;->b:LX/AT6;

    iget-object v0, v0, LX/AT6;->j:LX/Hqr;

    invoke-virtual {v0}, LX/Hqr;->a()V

    goto :goto_0
.end method
