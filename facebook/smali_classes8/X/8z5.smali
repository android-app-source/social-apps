.class public final LX/8z5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public final b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public final c:Ljava/lang/String;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:Z

.field public final g:Z

.field public final h:LX/8z9;

.field public final i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;


# direct methods
.method private constructor <init>(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0Px;IZZLX/8z9;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;IZZ",
            "LX/8z9;",
            "Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1426873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426874
    iput-object p1, p0, LX/8z5;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1426875
    iput-object p2, p0, LX/8z5;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1426876
    iput-object p3, p0, LX/8z5;->c:Ljava/lang/String;

    .line 1426877
    iput-object p4, p0, LX/8z5;->d:LX/0Px;

    .line 1426878
    iput p5, p0, LX/8z5;->e:I

    .line 1426879
    iput-boolean p6, p0, LX/8z5;->f:Z

    .line 1426880
    iput-boolean p7, p0, LX/8z5;->g:Z

    .line 1426881
    iput-object p8, p0, LX/8z5;->h:LX/8z9;

    .line 1426882
    iput-object p9, p0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1426883
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0Px;IZZLX/8z9;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;B)V
    .locals 0

    .prologue
    .line 1426884
    invoke-direct/range {p0 .. p9}, LX/8z5;-><init>(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0Px;IZZLX/8z9;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1426885
    iget-object v0, p0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    if-nez v0, :cond_0

    .line 1426886
    iget v0, p0, LX/8z5;->e:I

    .line 1426887
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/8z5;->e:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1426888
    iget-object v0, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/8z5;->e:I

    .line 1426889
    :goto_0
    if-nez v0, :cond_1

    .line 1426890
    const/4 v0, 0x0

    .line 1426891
    :goto_1
    return-object v0

    .line 1426892
    :cond_0
    iget v0, p0, LX/8z5;->e:I

    iget-object v1, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 1426893
    :cond_1
    const v1, 0x7f0f0091

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1426894
    iget-object v0, p0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    if-nez v0, :cond_0

    .line 1426895
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1426896
    iget v0, p0, LX/8z5;->e:I

    if-nez v0, :cond_1

    .line 1426897
    const/4 v0, 0x0

    .line 1426898
    :goto_0
    move-object v0, v0

    .line 1426899
    :goto_1
    return-object v0

    :cond_0
    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1426900
    iget v0, p0, LX/8z5;->e:I

    if-nez v0, :cond_5

    .line 1426901
    iget-object v0, p0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1426902
    iget-object v1, v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1426903
    :goto_2
    move-object v0, v0

    .line 1426904
    goto :goto_1

    .line 1426905
    :cond_1
    iget-object v0, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1426906
    const v0, 0x7f0f0091

    iget v1, p0, LX/8z5;->e:I

    new-array v2, v3, [Ljava/lang/Object;

    iget v3, p0, LX/8z5;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1426907
    :cond_2
    iget v0, p0, LX/8z5;->e:I

    if-ne v0, v3, :cond_3

    .line 1426908
    iget-object v0, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1426909
    :cond_3
    iget v0, p0, LX/8z5;->e:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v1, :cond_4

    .line 1426910
    const v0, 0x7f081461

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1426911
    :cond_4
    const v0, 0x7f081462

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, p1}, LX/8z5;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1426912
    :cond_5
    iget-object v0, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1426913
    const v0, 0x7f081462

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1426914
    iget-object v5, v2, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    move-object v2, v5

    .line 1426915
    aput-object v2, v1, v3

    invoke-virtual {p0, p1}, LX/8z5;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1426916
    :cond_6
    iget v0, p0, LX/8z5;->e:I

    if-ne v0, v4, :cond_7

    .line 1426917
    const v0, 0x7f081461

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1426918
    iget-object v5, v2, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    move-object v2, v5

    .line 1426919
    aput-object v2, v1, v3

    iget-object v2, p0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1426920
    :cond_7
    const v0, 0x7f081462

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1426921
    iget-object v5, v2, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    move-object v2, v5

    .line 1426922
    aput-object v2, v1, v3

    invoke-virtual {p0, p1}, LX/8z5;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method
