.class public final LX/9Cw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/9Cx;


# direct methods
.method public constructor <init>(LX/9Cx;)V
    .locals 0

    .prologue
    .line 1454542
    iput-object p1, p0, LX/9Cw;->a:LX/9Cx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, 0x17f8eba9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1454543
    const-string v0, "CommentInlineBannerListener.SET_INPUT_TEXT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1454544
    const-string v0, "extra_input_text"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1454545
    const-string v0, "extra_react_tag"

    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1454546
    iget-object v0, p0, LX/9Cw;->a:LX/9Cx;

    iget-object v0, v0, LX/9Cx;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21n;

    .line 1454547
    if-nez v0, :cond_0

    .line 1454548
    const/16 v0, 0x27

    const v2, -0x7005c12b

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1454549
    :goto_0
    return-void

    .line 1454550
    :cond_0
    invoke-interface {v0}, LX/21n;->getInlineReactBannerRootTag()I

    move-result v4

    .line 1454551
    if-eq v4, v3, :cond_1

    .line 1454552
    const v0, 0x549776e1

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 1454553
    :cond_1
    new-instance v3, Lcom/facebook/feedback/ui/CommentInlineBannerListener$CommentInlineBannerActionReceiver$1;

    invoke-direct {v3, p0, v0, v2}, Lcom/facebook/feedback/ui/CommentInlineBannerListener$CommentInlineBannerActionReceiver$1;-><init>(LX/9Cw;LX/21n;Ljava/lang/String;)V

    invoke-static {v3}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1454554
    :cond_2
    const v0, 0x4377c80a

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0
.end method
