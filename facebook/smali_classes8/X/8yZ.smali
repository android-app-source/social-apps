.class public LX/8yZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8yX;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/8yZ;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8ya;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1426029
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8yZ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8ya;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1426030
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1426031
    iput-object p1, p0, LX/8yZ;->b:LX/0Ot;

    .line 1426032
    return-void
.end method

.method public static a(LX/0QB;)LX/8yZ;
    .locals 4

    .prologue
    .line 1426033
    sget-object v0, LX/8yZ;->c:LX/8yZ;

    if-nez v0, :cond_1

    .line 1426034
    const-class v1, LX/8yZ;

    monitor-enter v1

    .line 1426035
    :try_start_0
    sget-object v0, LX/8yZ;->c:LX/8yZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1426036
    if-eqz v2, :cond_0

    .line 1426037
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1426038
    new-instance v3, LX/8yZ;

    const/16 p0, 0x1939

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8yZ;-><init>(LX/0Ot;)V

    .line 1426039
    move-object v0, v3

    .line 1426040
    sput-object v0, LX/8yZ;->c:LX/8yZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1426041
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1426042
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1426043
    :cond_1
    sget-object v0, LX/8yZ;->c:LX/8yZ;

    return-object v0

    .line 1426044
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1426045
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1426046
    check-cast p2, LX/8yY;

    .line 1426047
    iget-object v0, p0, LX/8yZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget v0, p2, LX/8yY;->a:I

    const/4 v3, 0x2

    const/4 p2, 0x1

    .line 1426048
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1426049
    const v2, 0x7f0b1533

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1426050
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020700

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    .line 1426051
    const/16 p0, 0x63

    if-le v0, p0, :cond_0

    .line 1426052
    const-string p0, "\u221e"

    .line 1426053
    :goto_0
    move-object p0, p0

    .line 1426054
    invoke-virtual {v3, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    const/4 p0, -0x1

    invoke-virtual {v3, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->p(I)LX/1ne;

    move-result-object v1

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1426055
    return-object v0

    :cond_0
    const-string p0, "+%d"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1426056
    invoke-static {}, LX/1dS;->b()V

    .line 1426057
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/8yX;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1426058
    new-instance v1, LX/8yY;

    invoke-direct {v1, p0}, LX/8yY;-><init>(LX/8yZ;)V

    .line 1426059
    sget-object v2, LX/8yZ;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8yX;

    .line 1426060
    if-nez v2, :cond_0

    .line 1426061
    new-instance v2, LX/8yX;

    invoke-direct {v2}, LX/8yX;-><init>()V

    .line 1426062
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/8yX;->a$redex0(LX/8yX;LX/1De;IILX/8yY;)V

    .line 1426063
    move-object v1, v2

    .line 1426064
    move-object v0, v1

    .line 1426065
    return-object v0
.end method
