.class public final LX/92i;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/92n;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8zq;

.field public final synthetic b:LX/92o;


# direct methods
.method public constructor <init>(LX/92o;LX/8zq;)V
    .locals 0

    .prologue
    .line 1432540
    iput-object p1, p0, LX/92i;->b:LX/92o;

    iput-object p2, p0, LX/92i;->a:LX/8zq;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1432541
    iget-object v0, p0, LX/92i;->b:LX/92o;

    iget-object v0, v0, LX/92o;->m:LX/92E;

    .line 1432542
    const v1, 0x430003

    const-string v2, "minutiae_verb_picker_time_to_fetch_end"

    invoke-virtual {v0, v1, v2}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432543
    const v1, 0x43000b

    const-string v2, "minutiae_verb_picker_time_to_verbs_shown"

    invoke-virtual {v0, v1, v2}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432544
    const v1, 0x430004

    const-string v2, "minutiae_verb_picker_fetch_time"

    invoke-virtual {v0, v1, v2}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432545
    const v1, 0x430006

    const-string v2, "minutiae_verb_picker_tti"

    invoke-virtual {v0, v1, v2}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432546
    iget-object v0, p0, LX/92i;->a:LX/8zq;

    if-eqz v0, :cond_0

    .line 1432547
    iget-object v0, p0, LX/92i;->a:LX/8zq;

    invoke-interface {v0, p1}, LX/8zq;->a(Ljava/lang/Throwable;)V

    .line 1432548
    :cond_0
    iget-object v0, p0, LX/92i;->b:LX/92o;

    iget-boolean v0, v0, LX/92o;->s:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/92i;->b:LX/92o;

    iget-boolean v0, v0, LX/92o;->a:Z

    if-nez v0, :cond_1

    .line 1432549
    iget-object v0, p0, LX/92i;->b:LX/92o;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/92o;->a:Z

    .line 1432550
    iget-object v0, p0, LX/92i;->b:LX/92o;

    iget-object v1, p0, LX/92i;->a:LX/8zq;

    invoke-static {v0, v1}, LX/92o;->d(LX/92o;LX/8zq;)V

    .line 1432551
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1432552
    check-cast p1, LX/92n;

    .line 1432553
    iget-object v0, p0, LX/92i;->b:LX/92o;

    .line 1432554
    iput-object p1, v0, LX/92o;->r:LX/92n;

    .line 1432555
    iget-object v0, p0, LX/92i;->b:LX/92o;

    iget-boolean v0, v0, LX/92o;->p:Z

    if-eqz v0, :cond_0

    .line 1432556
    iget-object v0, p0, LX/92i;->b:LX/92o;

    invoke-static {v0}, LX/92o;->e(LX/92o;)I

    move-result v0

    .line 1432557
    if-lez v0, :cond_0

    .line 1432558
    iget-object v1, p0, LX/92i;->b:LX/92o;

    iget-object v1, v1, LX/92o;->j:LX/92c;

    iget-object v2, p0, LX/92i;->b:LX/92o;

    iget-object v2, v2, LX/92o;->r:LX/92n;

    iget-object v2, v2, LX/92n;->a:LX/0Px;

    iget-object v3, p0, LX/92i;->b:LX/92o;

    iget-object v3, v3, LX/92o;->o:Ljava/lang/String;

    .line 1432559
    iget-object v4, v1, LX/92c;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v4, v0, :cond_3

    .line 1432560
    :cond_0
    iget-object v0, p0, LX/92i;->b:LX/92o;

    iget-object v1, p1, LX/92n;->b:LX/0ta;

    const/4 v4, 0x0

    .line 1432561
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v1, v2, :cond_5

    .line 1432562
    iget-object v2, v0, LX/92o;->n:LX/0Zb;

    const-string v3, "minutiae_verbs_fetcher_cache_miss"

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 1432563
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1432564
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 1432565
    :cond_1
    :goto_0
    iget-object v0, p0, LX/92i;->a:LX/8zq;

    if-eqz v0, :cond_2

    .line 1432566
    iget-object v0, p0, LX/92i;->a:LX/8zq;

    iget-object v1, p1, LX/92n;->a:LX/0Px;

    iget-object v2, p1, LX/92n;->b:LX/0ta;

    invoke-interface {v0, v1, v2}, LX/8zq;->a(LX/0Px;LX/0ta;)V

    .line 1432567
    :cond_2
    return-void

    .line 1432568
    :cond_3
    sget-object v4, LX/92c;->d:LX/1sm;

    invoke-virtual {v4, v2, v0}, LX/1sm;->a(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v4

    .line 1432569
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1432570
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/92e;

    .line 1432571
    iget-object v7, v4, LX/92e;->a:LX/5LG;

    move-object v4, v7

    .line 1432572
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v4, v7, v3, v8, v9}, LX/92I;->a(LX/5LG;ILjava/lang/String;ZLandroid/location/Location;)LX/92H;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1432573
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/92H;

    .line 1432574
    iget-object v6, v1, LX/92c;->c:Ljava/util/List;

    iget-object v7, v1, LX/92c;->a:LX/92Z;

    invoke-virtual {v7, v4}, LX/92Z;->a(LX/92H;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v4, v7}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1432575
    :cond_5
    iget-object v2, v0, LX/92o;->n:LX/0Zb;

    const-string v3, "minutiae_verbs_fetcher_cache_hit"

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 1432576
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1432577
    invoke-virtual {v2}, LX/0oG;->d()V

    goto :goto_0
.end method
