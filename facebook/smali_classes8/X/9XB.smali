.class public final enum LX/9XB;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9XB;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9XB;

.field public static final enum EVENT_CHECKIN_SUCCESS:LX/9XB;

.field public static final enum EVENT_CITY_HUB_PYML_MODULE_LIKE_SUCCESS:LX/9XB;

.field public static final enum EVENT_CITY_HUB_PYML_MODULE_RENDER_SUCCESS:LX/9XB;

.field public static final enum EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_SUCCESS:LX/9XB;

.field public static final enum EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_SUCCESS:LX/9XB;

.field public static final enum EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_SUCCESS:LX/9XB;

.field public static final enum EVENT_FB_EVENT_STATUS_SUCCESS:LX/9XB;

.field public static final enum EVENT_LIKE_SUCCESS:LX/9XB;

.field public static final enum EVENT_NETWORK_LOADED_BEFORE_CACHE:LX/9XB;

.field public static final enum EVENT_PAGE_ADD_TO_FAVORITES_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_DETAILS_LOADED:LX/9XB;

.field public static final enum EVENT_PAGE_EDIT_REVIEW_PRIVACY_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_FOLLOW_REGULAR_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_FOLLOW_SEE_FIRST_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_FOLLOW_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_INFO_LOADED:LX/9XB;

.field public static final enum EVENT_PAGE_RATING_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_RECOMMENDATION_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_REMOVE_FROM_FAVORITES_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_SAVE_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_UNFOLLOW_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_UNSAVE_SUCCESS:LX/9XB;

.field public static final enum EVENT_PAGE_VISIT:LX/9XB;

.field public static final enum EVENT_PLACE_DELETE_REVIEW_SUCCESS:LX/9XB;

.field public static final enum EVENT_PLACE_EDIT_REVIEW_SUCCESS:LX/9XB;

.field public static final enum EVENT_PLACE_REPORT_SUCCESS:LX/9XB;

.field public static final enum EVENT_PLACE_SAVE_SUCCESS:LX/9XB;

.field public static final enum EVENT_PLACE_UNSAVE_SUCCESS:LX/9XB;

.field public static final enum EVENT_RECOMMENDATION_LIKE_SUCCESS:LX/9XB;

.field public static final enum EVENT_RECOMMENDATION_UNLIKE_SUCCESS:LX/9XB;

.field public static final enum EVENT_SECTION_LOADED:LX/9XB;

.field public static final enum EVENT_SHARE_PAGE_SUCCESS:LX/9XB;

.field public static final enum EVENT_SUGGEST_EDIT_SUCCESS:LX/9XB;

.field public static final enum EVENT_UNLIKE_SUCCESS:LX/9XB;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502421
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_VISIT"

    const-string v2, "visit_page"

    invoke-direct {v0, v1, v4, v2}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_VISIT:LX/9XB;

    .line 1502422
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_DETAILS_LOADED"

    const-string v2, "page_load_successful"

    invoke-direct {v0, v1, v5, v2}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_DETAILS_LOADED:LX/9XB;

    .line 1502423
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_NETWORK_LOADED_BEFORE_CACHE"

    const-string v2, "page_network_before_cache"

    invoke-direct {v0, v1, v6, v2}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_NETWORK_LOADED_BEFORE_CACHE:LX/9XB;

    .line 1502424
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_INFO_LOADED"

    const-string v2, "info_page_load_successful"

    invoke-direct {v0, v1, v7, v2}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_INFO_LOADED:LX/9XB;

    .line 1502425
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_RATING_SUCCESS"

    const-string v2, "page_rating_successful"

    invoke-direct {v0, v1, v8, v2}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_RATING_SUCCESS:LX/9XB;

    .line 1502426
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PLACE_REPORT_SUCCESS"

    const/4 v2, 0x5

    const-string v3, "place_report_successful"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PLACE_REPORT_SUCCESS:LX/9XB;

    .line 1502427
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_RECOMMENDATION_SUCCESS"

    const/4 v2, 0x6

    const-string v3, "page_recommendation_successful"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_RECOMMENDATION_SUCCESS:LX/9XB;

    .line 1502428
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_EDIT_REVIEW_PRIVACY_SUCCESS"

    const/4 v2, 0x7

    const-string v3, "page_edit_review_privacy_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_EDIT_REVIEW_PRIVACY_SUCCESS:LX/9XB;

    .line 1502429
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PLACE_EDIT_REVIEW_SUCCESS"

    const/16 v2, 0x8

    const-string v3, "place_edit_review_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PLACE_EDIT_REVIEW_SUCCESS:LX/9XB;

    .line 1502430
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PLACE_DELETE_REVIEW_SUCCESS"

    const/16 v2, 0x9

    const-string v3, "place_delete_review_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PLACE_DELETE_REVIEW_SUCCESS:LX/9XB;

    .line 1502431
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_SUGGEST_EDIT_SUCCESS"

    const/16 v2, 0xa

    const-string v3, "page_suggest_edit_successful"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_SUGGEST_EDIT_SUCCESS:LX/9XB;

    .line 1502432
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_UNLIKE_SUCCESS"

    const/16 v2, 0xb

    const-string v3, "page_unlike_successful"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_UNLIKE_SUCCESS:LX/9XB;

    .line 1502433
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_LIKE_SUCCESS"

    const/16 v2, 0xc

    const-string v3, "page_like_successful"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_LIKE_SUCCESS:LX/9XB;

    .line 1502434
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_SHARE_PAGE_SUCCESS"

    const/16 v2, 0xd

    const-string v3, "page_share_successful"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_SHARE_PAGE_SUCCESS:LX/9XB;

    .line 1502435
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_CHECKIN_SUCCESS"

    const/16 v2, 0xe

    const-string v3, "page_checkin_successful"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_CHECKIN_SUCCESS:LX/9XB;

    .line 1502436
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_FB_EVENT_STATUS_SUCCESS"

    const/16 v2, 0xf

    const-string v3, "page_event_status_update_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_FB_EVENT_STATUS_SUCCESS:LX/9XB;

    .line 1502437
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_RECOMMENDATION_LIKE_SUCCESS"

    const/16 v2, 0x10

    const-string v3, "recommendation_like_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_RECOMMENDATION_LIKE_SUCCESS:LX/9XB;

    .line 1502438
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_RECOMMENDATION_UNLIKE_SUCCESS"

    const/16 v2, 0x11

    const-string v3, "recommendation_unlike_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_RECOMMENDATION_UNLIKE_SUCCESS:LX/9XB;

    .line 1502439
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_SECTION_LOADED"

    const/16 v2, 0x12

    const-string v3, "section_loaded"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_SECTION_LOADED:LX/9XB;

    .line 1502440
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PLACE_SAVE_SUCCESS"

    const/16 v2, 0x13

    const-string v3, "place_save_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PLACE_SAVE_SUCCESS:LX/9XB;

    .line 1502441
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PLACE_UNSAVE_SUCCESS"

    const/16 v2, 0x14

    const-string v3, "place_unsave_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PLACE_UNSAVE_SUCCESS:LX/9XB;

    .line 1502442
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_SAVE_SUCCESS"

    const/16 v2, 0x15

    const-string v3, "page_save_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_SAVE_SUCCESS:LX/9XB;

    .line 1502443
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_UNSAVE_SUCCESS"

    const/16 v2, 0x16

    const-string v3, "page_unsave_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_UNSAVE_SUCCESS:LX/9XB;

    .line 1502444
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_FOLLOW_SUCCESS"

    const/16 v2, 0x17

    const-string v3, "page_follow_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_FOLLOW_SUCCESS:LX/9XB;

    .line 1502445
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_UNFOLLOW_SUCCESS"

    const/16 v2, 0x18

    const-string v3, "page_unfollow_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_UNFOLLOW_SUCCESS:LX/9XB;

    .line 1502446
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_FOLLOW_SEE_FIRST_SUCCESS"

    const/16 v2, 0x19

    const-string v3, "page_follow_see_first_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_FOLLOW_SEE_FIRST_SUCCESS:LX/9XB;

    .line 1502447
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_FOLLOW_REGULAR_SUCCESS"

    const/16 v2, 0x1a

    const-string v3, "page_follow_regular_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_FOLLOW_REGULAR_SUCCESS:LX/9XB;

    .line 1502448
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_ADD_TO_FAVORITES_SUCCESS"

    const/16 v2, 0x1b

    const-string v3, "page_add_to_favorites_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_ADD_TO_FAVORITES_SUCCESS:LX/9XB;

    .line 1502449
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_PAGE_REMOVE_FROM_FAVORITES_SUCCESS"

    const/16 v2, 0x1c

    const-string v3, "page_remove_from_favorites_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_PAGE_REMOVE_FROM_FAVORITES_SUCCESS:LX/9XB;

    .line 1502450
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_SUCCESS"

    const/16 v2, 0x1d

    const-string v3, "city_hub_social_module_like_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_SUCCESS:LX/9XB;

    .line 1502451
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_SUCCESS"

    const/16 v2, 0x1e

    const-string v3, "city_hub_social_module_fetch_pages_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_SUCCESS:LX/9XB;

    .line 1502452
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_SUCCESS"

    const/16 v2, 0x1f

    const-string v3, "city_hub_social_module_render_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_SUCCESS:LX/9XB;

    .line 1502453
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_CITY_HUB_PYML_MODULE_LIKE_SUCCESS"

    const/16 v2, 0x20

    const-string v3, "city_hub_pyml_module_like_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_CITY_HUB_PYML_MODULE_LIKE_SUCCESS:LX/9XB;

    .line 1502454
    new-instance v0, LX/9XB;

    const-string v1, "EVENT_CITY_HUB_PYML_MODULE_RENDER_SUCCESS"

    const/16 v2, 0x21

    const-string v3, "city_hub_pyml_module_render_success"

    invoke-direct {v0, v1, v2, v3}, LX/9XB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XB;->EVENT_CITY_HUB_PYML_MODULE_RENDER_SUCCESS:LX/9XB;

    .line 1502455
    const/16 v0, 0x22

    new-array v0, v0, [LX/9XB;

    sget-object v1, LX/9XB;->EVENT_PAGE_VISIT:LX/9XB;

    aput-object v1, v0, v4

    sget-object v1, LX/9XB;->EVENT_PAGE_DETAILS_LOADED:LX/9XB;

    aput-object v1, v0, v5

    sget-object v1, LX/9XB;->EVENT_NETWORK_LOADED_BEFORE_CACHE:LX/9XB;

    aput-object v1, v0, v6

    sget-object v1, LX/9XB;->EVENT_PAGE_INFO_LOADED:LX/9XB;

    aput-object v1, v0, v7

    sget-object v1, LX/9XB;->EVENT_PAGE_RATING_SUCCESS:LX/9XB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9XB;->EVENT_PLACE_REPORT_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9XB;->EVENT_PAGE_RECOMMENDATION_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9XB;->EVENT_PAGE_EDIT_REVIEW_PRIVACY_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9XB;->EVENT_PLACE_EDIT_REVIEW_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9XB;->EVENT_PLACE_DELETE_REVIEW_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9XB;->EVENT_SUGGEST_EDIT_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9XB;->EVENT_UNLIKE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9XB;->EVENT_LIKE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9XB;->EVENT_SHARE_PAGE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9XB;->EVENT_CHECKIN_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9XB;->EVENT_FB_EVENT_STATUS_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9XB;->EVENT_RECOMMENDATION_LIKE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9XB;->EVENT_RECOMMENDATION_UNLIKE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9XB;->EVENT_SECTION_LOADED:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9XB;->EVENT_PLACE_SAVE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/9XB;->EVENT_PLACE_UNSAVE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/9XB;->EVENT_PAGE_SAVE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/9XB;->EVENT_PAGE_UNSAVE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/9XB;->EVENT_PAGE_FOLLOW_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/9XB;->EVENT_PAGE_UNFOLLOW_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/9XB;->EVENT_PAGE_FOLLOW_SEE_FIRST_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/9XB;->EVENT_PAGE_FOLLOW_REGULAR_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/9XB;->EVENT_PAGE_ADD_TO_FAVORITES_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/9XB;->EVENT_PAGE_REMOVE_FROM_FAVORITES_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/9XB;->EVENT_CITY_HUB_PYML_MODULE_LIKE_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/9XB;->EVENT_CITY_HUB_PYML_MODULE_RENDER_SUCCESS:LX/9XB;

    aput-object v2, v0, v1

    sput-object v0, LX/9XB;->$VALUES:[LX/9XB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502456
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502457
    iput-object p3, p0, LX/9XB;->mEventName:Ljava/lang/String;

    .line 1502458
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9XB;
    .locals 1

    .prologue
    .line 1502459
    const-class v0, LX/9XB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9XB;

    return-object v0
.end method

.method public static values()[LX/9XB;
    .locals 1

    .prologue
    .line 1502460
    sget-object v0, LX/9XB;->$VALUES:[LX/9XB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9XB;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502461
    iget-object v0, p0, LX/9XB;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502462
    sget-object v0, LX/9XC;->NETWORK_SUCCESS:LX/9XC;

    return-object v0
.end method
