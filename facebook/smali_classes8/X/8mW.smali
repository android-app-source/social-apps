.class public final LX/8mW;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/stickers/store/StickerStoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1398934
    iput-object p1, p0, LX/8mW;->b:Lcom/facebook/stickers/store/StickerStoreFragment;

    iput-object p2, p0, LX/8mW;->a:Ljava/util/List;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 1398915
    iget v0, p2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    move v0, v0

    .line 1398916
    packed-switch v0, :pswitch_data_0

    .line 1398917
    :goto_0
    return-void

    .line 1398918
    :pswitch_0
    iget-object v0, p0, LX/8mW;->b:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, p0, LX/8mW;->b:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-static {v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->p(Lcom/facebook/stickers/store/StickerStoreFragment;)LX/0Px;

    move-result-object v1

    .line 1398919
    invoke-static {v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->b$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V

    .line 1398920
    goto :goto_0

    .line 1398921
    :pswitch_1
    iget-object v0, p0, LX/8mW;->b:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    .line 1398922
    iget-boolean v1, v0, LX/8ma;->d:Z

    move v0, v1

    .line 1398923
    if-eqz v0, :cond_1

    .line 1398924
    iget-object v0, p0, LX/8mW;->b:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, p0, LX/8mW;->b:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStoreFragment;->y:LX/8ma;

    .line 1398925
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object p1

    .line 1398926
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, LX/8ma;->getCount()I

    move-result p2

    if-ge v2, p2, :cond_0

    .line 1398927
    invoke-virtual {v1, v2}, LX/8ma;->getItem(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1398928
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1398929
    :cond_0
    move-object v1, p1

    .line 1398930
    invoke-static {v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->e(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V

    .line 1398931
    :cond_1
    iget-object v0, p0, LX/8mW;->b:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, p0, LX/8mW;->a:Ljava/util/List;

    .line 1398932
    invoke-static {v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->c$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V

    .line 1398933
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
