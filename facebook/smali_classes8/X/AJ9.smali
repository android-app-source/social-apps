.class public final LX/AJ9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V
    .locals 0

    .prologue
    .line 1660683
    iput-object p1, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x2

    const v0, -0x79844cf0

    invoke-static {v6, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1660684
    iget-object v1, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    invoke-static {v1}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->k(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V

    .line 1660685
    iget-object v1, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->a$redex0(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;ZZ)V

    .line 1660686
    iget-object v1, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v1, v1, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    invoke-virtual {v1}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->b()V

    .line 1660687
    iget-object v1, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v1, v1, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->t:LX/AJS;

    iget-object v2, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    invoke-virtual {v2}, LX/AJZ;->e()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v3, v3, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->r:LX/AJQ;

    invoke-virtual {v3}, LX/AJQ;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v3

    iget-object v4, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v4, v4, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660688
    iget-boolean v5, v4, LX/AJZ;->f:Z

    move v4, v5

    .line 1660689
    iget-object v5, p0, LX/AJ9;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v5, v5, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660690
    iget-boolean p0, v5, LX/AJZ;->e:Z

    move v5, p0

    .line 1660691
    invoke-static {v1, v2, v4, v5}, LX/AJS;->a(LX/AJS;LX/0Px;ZZ)V

    .line 1660692
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1660693
    const-string v7, "extra_selected_audience"

    invoke-static {v2, v3, v4, v5}, LX/AJS;->b(LX/0Px;Lcom/facebook/privacy/model/SelectablePrivacyData;ZZ)Lcom/facebook/audience/model/SharesheetSelectedAudience;

    move-result-object p0

    invoke-virtual {v8, v7, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1660694
    iget-object p0, v1, LX/AJS;->b:LX/AJK;

    if-nez v3, :cond_1

    const/4 v7, 0x0

    .line 1660695
    :goto_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1660696
    invoke-static {p0, v3}, LX/AJK;->a(LX/AJK;Landroid/os/Bundle;)V

    .line 1660697
    sget-object p1, LX/AJJ;->DID_SHARE_TO_FEED:LX/AJJ;

    invoke-virtual {p1}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v5, :cond_5

    const-string p1, "1"

    :goto_1
    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660698
    sget-object p1, LX/AJJ;->DID_SHARE_DIRECT:LX/AJJ;

    invoke-virtual {p1}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_6

    const-string p1, "0"

    :goto_2
    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660699
    sget-object p1, LX/AJJ;->DID_CHANGE_FEED_PRIVACY:LX/AJJ;

    invoke-virtual {p1}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v4

    iget-boolean p1, p0, LX/AJK;->a:Z

    if-eqz p1, :cond_7

    const-string p1, "1"

    :goto_3
    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660700
    if-eqz v5, :cond_3

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1660701
    const-string p1, "168541146923029"

    .line 1660702
    invoke-static {p0, v3, v7}, LX/AJK;->a(LX/AJK;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1660703
    :goto_4
    const-string v4, "extra_sharesheet_integration_point_id"

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660704
    move-object p1, v3

    .line 1660705
    if-eqz p1, :cond_0

    .line 1660706
    if-eqz v5, :cond_2

    .line 1660707
    const-string v3, "try_show_survey_on_result_extra_data"

    invoke-virtual {v8, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1660708
    const-string v3, "try_show_survey_on_result_integration_point_id"

    const-string v4, "extra_sharesheet_integration_point_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1660709
    :cond_0
    :goto_5
    iget-object v7, v1, LX/AJS;->d:LX/0he;

    .line 1660710
    sget-object p0, LX/0hf;->SHARESHEET:LX/0hf;

    iput-object p0, v7, LX/0he;->j:LX/0hf;

    .line 1660711
    sget-object p0, LX/7gU;->CLICK:LX/7gU;

    iput-object p0, v7, LX/0he;->i:LX/7gU;

    .line 1660712
    iget-object v7, v1, LX/AJS;->c:Landroid/app/Activity;

    const/4 p0, -0x1

    invoke-virtual {v7, p0, v8}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1660713
    iget-object v7, v1, LX/AJS;->c:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->finish()V

    .line 1660714
    iget-object v7, v1, LX/AJS;->c:Landroid/app/Activity;

    const/high16 v8, 0x10a0000

    const p0, 0x7f040015

    invoke-virtual {v7, v8, p0}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1660715
    const v1, -0x4a860d1

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1660716
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/privacy/model/SelectablePrivacyData;->d()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1660717
    :cond_2
    const-string v3, "extra_sharesheet_survey_data"

    invoke-virtual {v8, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1660718
    const-string v3, "extra_sharesheet_integration_point_id"

    const-string v4, "extra_sharesheet_integration_point_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_5

    .line 1660719
    :cond_3
    if-nez v5, :cond_4

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 1660720
    const-string p1, "1307235905961814"

    .line 1660721
    invoke-static {v3, v2}, LX/AJK;->a(Landroid/os/Bundle;LX/0Px;)V

    goto :goto_4

    .line 1660722
    :cond_4
    const-string p1, "896560460448915"

    .line 1660723
    invoke-static {p0, v3, v7}, LX/AJK;->a(LX/AJK;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1660724
    invoke-static {v3, v2}, LX/AJK;->a(Landroid/os/Bundle;LX/0Px;)V

    .line 1660725
    goto :goto_4

    .line 1660726
    :cond_5
    const-string p1, "0"

    goto/16 :goto_1

    .line 1660727
    :cond_6
    const-string p1, "1"

    goto/16 :goto_2

    .line 1660728
    :cond_7
    const-string p1, "0"

    goto/16 :goto_3
.end method
