.class public LX/AT6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/9ij;",
            "LX/8Jk;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/8Jm;

.field public final c:Landroid/widget/FrameLayout;

.field public final d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

.field public final e:Landroid/content/Context;

.field public final f:LX/ASb;

.field public final g:LX/9ic;

.field public final h:LX/8GZ;

.field public final i:LX/ATV;

.field public final j:LX/Hqr;

.field public final k:LX/75Q;

.field private l:Z

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/photos/base/media/PhotoItem;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;Lcom/facebook/photos/tagging/shared/FaceBoxesView;LX/ATV;LX/Hqr;Landroid/content/Context;LX/9ic;LX/8GZ;LX/ASb;LX/8Jn;LX/75Q;)V
    .locals 2
    .param p1    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/tagging/shared/FaceBoxesView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/ATV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Hqr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1675161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1675162
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AT6;->l:Z

    .line 1675163
    iput-object p1, p0, LX/AT6;->c:Landroid/widget/FrameLayout;

    .line 1675164
    iput-object p2, p0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    .line 1675165
    iget-object v0, p0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    .line 1675166
    new-instance v1, LX/AT5;

    invoke-direct {v1, p0}, LX/AT5;-><init>(LX/AT6;)V

    move-object v1, v1

    .line 1675167
    iput-object v1, v0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->i:LX/8JH;

    .line 1675168
    iput-object p3, p0, LX/AT6;->i:LX/ATV;

    .line 1675169
    iput-object p4, p0, LX/AT6;->j:LX/Hqr;

    .line 1675170
    iput-object p5, p0, LX/AT6;->e:Landroid/content/Context;

    .line 1675171
    iput-object p6, p0, LX/AT6;->g:LX/9ic;

    .line 1675172
    iput-object p7, p0, LX/AT6;->h:LX/8GZ;

    .line 1675173
    iput-object p8, p0, LX/AT6;->f:LX/ASb;

    .line 1675174
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/AT6;->a:Ljava/util/HashMap;

    .line 1675175
    iget-object v0, p0, LX/AT6;->c:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {p9, v0, v1}, LX/8Jn;->a(Landroid/view/View;F)LX/8Jm;

    move-result-object v0

    iput-object v0, p0, LX/AT6;->b:LX/8Jm;

    .line 1675176
    iget-object v0, p0, LX/AT6;->b:LX/8Jm;

    iget-object v1, p0, LX/AT6;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, LX/8Jm;->a(Ljava/util/Map;)V

    .line 1675177
    iput-object p10, p0, LX/AT6;->k:LX/75Q;

    .line 1675178
    return-void
.end method

.method public static a(LX/AT6;)V
    .locals 13

    .prologue
    .line 1675179
    iget-object v0, p0, LX/AT6;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9ij;

    .line 1675180
    iget-object v2, p0, LX/AT6;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 1675181
    :cond_0
    iget-object v0, p0, LX/AT6;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1675182
    iget-object v0, p0, LX/AT6;->k:LX/75Q;

    iget-object v1, p0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0, v1}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;)Ljava/util/List;

    move-result-object v1

    .line 1675183
    iget-object v0, p0, LX/AT6;->h:LX/8GZ;

    invoke-virtual {v0, v1}, LX/8GZ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 1675184
    if-eqz v2, :cond_1

    if-nez v1, :cond_2

    .line 1675185
    :cond_1
    :goto_1
    return-void

    .line 1675186
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1675187
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1675188
    iget-object v5, p0, LX/AT6;->h:LX/8GZ;

    invoke-virtual {v5, v0}, LX/8GZ;->c(LX/362;)LX/362;

    move-result-object v5

    if-nez v5, :cond_3

    .line 1675189
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1675190
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1675191
    iget-object v4, p0, LX/AT6;->k:LX/75Q;

    iget-object v5, p0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v4, v5, v0}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/base/tagging/Tag;)V

    goto :goto_3

    .line 1675192
    :cond_5
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_6

    iget-object v0, p0, LX/AT6;->j:LX/Hqr;

    if-eqz v0, :cond_6

    .line 1675193
    iget-object v0, p0, LX/AT6;->j:LX/Hqr;

    invoke-virtual {v0}, LX/Hqr;->a()V

    .line 1675194
    :cond_6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1675195
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1675196
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v0, Lcom/facebook/photos/base/tagging/Tag;->d:Z

    .line 1675197
    const/4 v9, 0x1

    const/4 v12, -0x2

    .line 1675198
    new-instance v6, LX/9ij;

    iget-object v7, p0, LX/AT6;->e:Landroid/content/Context;

    sget-object v10, LX/9ih;->FIRST_NAME:LX/9ih;

    move-object v8, v0

    move v11, v9

    invoke-direct/range {v6 .. v11}, LX/9ij;-><init>(Landroid/content/Context;Lcom/facebook/photos/base/tagging/Tag;ZLX/9ih;Z)V

    .line 1675199
    iget-object v7, v6, LX/9ij;->h:Landroid/widget/TextView;

    move-object v7, v7

    .line 1675200
    iget-object v8, p0, LX/AT6;->e:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b004c

    invoke-static {v8, v9}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1675201
    new-instance v7, LX/AT4;

    invoke-direct {v7, p0, v6}, LX/AT4;-><init>(LX/AT6;LX/9ij;)V

    move-object v7, v7

    .line 1675202
    iput-object v7, v6, LX/9ij;->j:LX/9ii;

    .line 1675203
    iget-object v7, p0, LX/AT6;->e:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->density:F

    .line 1675204
    const/high16 v8, 0x40800000    # 4.0f

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    float-to-int v7, v7

    .line 1675205
    iget-object v8, v6, LX/9ij;->h:Landroid/widget/TextView;

    invoke-virtual {v8, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1675206
    iget v8, v6, LX/9ij;->v:I

    add-int/2addr v8, v7

    iput v8, v6, LX/9ij;->v:I

    .line 1675207
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v7, v12, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1675208
    iget-object v8, p0, LX/AT6;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v8, v6, v7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1675209
    iget-object v7, p0, LX/AT6;->a:Ljava/util/HashMap;

    new-instance v8, LX/8Jk;

    .line 1675210
    iget-object v9, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v9, v9

    .line 1675211
    invoke-interface {v9}, Lcom/facebook/photos/base/tagging/TagTarget;->e()Landroid/graphics/PointF;

    move-result-object v9

    .line 1675212
    iget-object v10, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v10, v10

    .line 1675213
    invoke-interface {v10}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v10

    invoke-direct {v8, v9, v10}, LX/8Jk;-><init>(Landroid/graphics/PointF;Landroid/graphics/RectF;)V

    invoke-virtual {v7, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675214
    goto :goto_4

    .line 1675215
    :cond_7
    iget-object v0, p0, LX/AT6;->b:LX/8Jm;

    iget-object v1, p0, LX/AT6;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, LX/8Jm;->a(Ljava/util/Map;)V

    .line 1675216
    iget-object v0, p0, LX/AT6;->c:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/facebook/composer/ui/underwood/TaggingController$1;

    invoke-direct {v1, p0}, Lcom/facebook/composer/ui/underwood/TaggingController$1;-><init>(LX/AT6;)V

    invoke-static {v0, v1}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    goto/16 :goto_1
.end method

.method public static b(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 1675217
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675218
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1675219
    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, p1, p0, v1}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1675220
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/PhotoItem;)V
    .locals 0

    .prologue
    .line 1675221
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675222
    iput-object p1, p0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675223
    return-void
.end method

.method public final b(Landroid/graphics/RectF;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1675224
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675225
    iget-object v0, p0, LX/AT6;->f:LX/ASb;

    iget-object v1, p0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0, v1}, LX/ASb;->a(Lcom/facebook/ipc/media/MediaItem;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v1

    invoke-static {v0, p1, v1}, LX/9ic;->a(Ljava/util/List;Landroid/graphics/RectF;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
