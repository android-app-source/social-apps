.class public LX/9iM;
.super LX/0gG;
.source ""


# instance fields
.field private a:I

.field private b:LX/Duv;

.field private c:LX/9iR;

.field public d:LX/9iJ;


# direct methods
.method public constructor <init>(LX/Duv;LX/9iR;)V
    .locals 1

    .prologue
    .line 1527718
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1527719
    iput-object p1, p0, LX/9iM;->b:LX/Duv;

    .line 1527720
    iput-object p2, p0, LX/9iM;->c:LX/9iR;

    .line 1527721
    const/4 v0, -0x1

    iput v0, p0, LX/9iM;->a:I

    .line 1527722
    return-void
.end method

.method private a(I)LX/74w;
    .locals 1

    .prologue
    .line 1527723
    iget-object v0, p0, LX/9iM;->b:LX/Duv;

    .line 1527724
    iget-object p0, v0, LX/Duv;->a:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/74w;

    move-object v0, p0

    .line 1527725
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1527711
    invoke-direct {p0, p2}, LX/9iM;->a(I)LX/74w;

    move-result-object v0

    .line 1527712
    iget-object v1, p0, LX/9iM;->c:LX/9iR;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/9iR;->a(LX/74w;Landroid/content/Context;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/9iQ;

    move-result-object v0

    .line 1527713
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9iQ;->setTag(Ljava/lang/Object;)V

    .line 1527714
    check-cast p1, Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 1527715
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1527716
    check-cast p1, Landroid/support/v4/view/ViewPager;

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 1527717
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1527710
    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1527707
    iget-object v0, p0, LX/9iM;->b:LX/Duv;

    .line 1527708
    iget-object p0, v0, LX/Duv;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move v0, p0

    .line 1527709
    return v0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1527704
    iget-object v0, p0, LX/9iM;->d:LX/9iJ;

    if-eqz v0, :cond_0

    .line 1527705
    iget-object v0, p0, LX/9iM;->d:LX/9iJ;

    invoke-interface {v0}, LX/9iJ;->a()V

    .line 1527706
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1527701
    iget v0, p0, LX/9iM;->a:I

    if-ne v0, p2, :cond_0

    .line 1527702
    :goto_0
    return-void

    .line 1527703
    :cond_0
    iput p2, p0, LX/9iM;->a:I

    goto :goto_0
.end method
