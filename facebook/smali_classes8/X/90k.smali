.class public final LX/90k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V
    .locals 0

    .prologue
    .line 1429375
    iput-object p1, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 9

    .prologue
    .line 1429376
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    .line 1429377
    iget v1, v0, LX/90G;->b:I

    move v0, v1

    .line 1429378
    if-eq v0, p2, :cond_1

    .line 1429379
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    invoke-virtual {v0}, LX/90G;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429380
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iget-object v1, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429381
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1429382
    iget-object v2, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->r:Ljava/lang/String;

    .line 1429383
    const-string p1, "activity_picker_first_scroll"

    invoke-static {p1, v1}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    invoke-virtual {p1, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    .line 1429384
    iget-object p4, p1, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p1, p4

    .line 1429385
    iget-object p4, v0, LX/919;->a:LX/0Zb;

    invoke-interface {p4, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429386
    :cond_0
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    .line 1429387
    iput p2, v0, LX/90G;->b:I

    .line 1429388
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    if-eqz v0, :cond_1

    .line 1429389
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->p:Lcom/facebook/ui/search/SearchEditText;

    .line 1429390
    invoke-static {v0}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 1429391
    :cond_1
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v0}, LX/90j;->a()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 1429392
    :cond_2
    :goto_0
    return-void

    .line 1429393
    :cond_3
    iget-object v1, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    invoke-virtual {v1}, LX/90j;->a()I

    move-result v1

    .line 1429394
    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    if-lt v1, p2, :cond_4

    add-int v2, p2, p3

    if-ge v1, v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1429395
    move v0, v2

    .line 1429396
    if-eqz v0, :cond_2

    .line 1429397
    iget-object v0, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->u:LX/90j;

    iget-object v1, p0, LX/90k;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    .line 1429398
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1429399
    iget-object v3, v0, LX/90j;->i:LX/92X;

    iget-object v5, v0, LX/90j;->m:Ljava/lang/String;

    iget-object v4, v0, LX/90j;->h:LX/5Lv;

    .line 1429400
    invoke-virtual {v4}, LX/5Lv;->a()Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, v4, LX/5Lv;->c:LX/0ut;

    invoke-interface {v7}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v7

    :goto_2
    move-object v7, v7

    .line 1429401
    new-instance v8, LX/90e;

    invoke-direct {v8, v0, v6}, LX/90e;-><init>(LX/90j;Ljava/lang/String;)V

    move-object v4, v1

    invoke-virtual/range {v3 .. v8}, LX/92X;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)Z

    .line 1429402
    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1429403
    return-void
.end method
