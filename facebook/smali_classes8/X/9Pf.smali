.class public final LX/9Pf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1485405
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1485406
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485407
    :goto_0
    return v1

    .line 1485408
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485409
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1485410
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1485411
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485412
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1485413
    const-string v3, "photo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1485414
    const/4 v2, 0x0

    .line 1485415
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1485416
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485417
    :goto_2
    move v0, v2

    .line 1485418
    goto :goto_1

    .line 1485419
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1485420
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1485421
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1485422
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485423
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 1485424
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1485425
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485426
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 1485427
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1485428
    const/4 v3, 0x0

    .line 1485429
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_b

    .line 1485430
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485431
    :goto_4
    move v0, v3

    .line 1485432
    goto :goto_3

    .line 1485433
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1485434
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1485435
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 1485436
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485437
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_a

    .line 1485438
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1485439
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485440
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_9

    if-eqz v4, :cond_9

    .line 1485441
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1485442
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 1485443
    :cond_a
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1485444
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1485445
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_4

    :cond_b
    move v0, v3

    goto :goto_5
.end method
