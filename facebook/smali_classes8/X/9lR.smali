.class public final LX/9lR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/rapidreporting/ui/DialogStateData;

.field public final synthetic b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

.field public final synthetic c:LX/9mU;

.field public final synthetic d:LX/9lb;


# direct methods
.method public constructor <init>(LX/9lb;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9mU;)V
    .locals 0

    .prologue
    .line 1532856
    iput-object p1, p0, LX/9lR;->d:LX/9lb;

    iput-object p2, p0, LX/9lR;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iput-object p3, p0, LX/9lR;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    iput-object p4, p0, LX/9lR;->c:LX/9mU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1532857
    iget-object v0, p0, LX/9lR;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iget-object v1, p0, LX/9lR;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1532858
    iput-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    .line 1532859
    iget-object v0, p0, LX/9lR;->c:LX/9mU;

    invoke-virtual {v0}, LX/9mU;->a()V

    .line 1532860
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1532861
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1532862
    if-eqz p1, :cond_0

    .line 1532863
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1532864
    if-eqz v0, :cond_0

    .line 1532865
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1532866
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1532867
    iget-object v1, p0, LX/9lR;->d:LX/9lb;

    .line 1532868
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1532869
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/9lb;->a$redex0(LX/9lb;Ljava/lang/String;)V

    .line 1532870
    iget-object v0, p0, LX/9lR;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1532871
    iput-object p1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1532872
    :goto_0
    iget-object v0, p0, LX/9lR;->c:LX/9mU;

    invoke-virtual {v0}, LX/9mU;->a()V

    .line 1532873
    iget-object v0, p0, LX/9lR;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->b()V

    .line 1532874
    return-void

    .line 1532875
    :cond_0
    iget-object v0, p0, LX/9lR;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iget-object v1, p0, LX/9lR;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1532876
    iput-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    .line 1532877
    iget-object v0, p0, LX/9lR;->d:LX/9lb;

    iget-object v0, v0, LX/9lb;->d:LX/03V;

    sget-object v1, LX/9lb;->a:Ljava/lang/String;

    const-string v2, "RapidReporting GraphQL call to fetch RapidReportingPrompt returned successfully but returned no RapidReportingPrompt"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
