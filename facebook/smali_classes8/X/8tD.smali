.class public LX/8tD;
.super LX/3Mm;
.source ""


# instance fields
.field private final a:Z

.field private final b:LX/8RK;

.field private final c:LX/8vE;


# direct methods
.method public constructor <init>(LX/0Zr;LX/8RK;LX/8vE;Z)V
    .locals 0
    .param p2    # LX/8RK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/8vE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412613
    invoke-direct {p0, p1}, LX/3Mm;-><init>(LX/0Zr;)V

    .line 1412614
    iput-boolean p4, p0, LX/8tD;->a:Z

    .line 1412615
    iput-object p2, p0, LX/8tD;->b:LX/8RK;

    .line 1412616
    iput-object p3, p0, LX/8tD;->c:LX/8vE;

    .line 1412617
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;LX/39y;)V
    .locals 1

    .prologue
    .line 1412618
    iget-object v0, p0, LX/8tD;->c:LX/8vE;

    .line 1412619
    iget-object p0, p2, LX/39y;->a:Ljava/lang/Object;

    if-eqz p0, :cond_0

    .line 1412620
    iget-object p1, v0, LX/8vE;->a:LX/8tB;

    iget-object p0, p2, LX/39y;->a:Ljava/lang/Object;

    check-cast p0, Ljava/util/List;

    iput-object p0, p1, LX/8tB;->d:Ljava/util/List;

    .line 1412621
    :goto_0
    iget-object p0, v0, LX/8vE;->a:LX/8tB;

    const p1, 0x36f2f8e2

    invoke-static {p0, p1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1412622
    return-void

    .line 1412623
    :cond_0
    iget-object p0, v0, LX/8vE;->a:LX/8tB;

    iget-object p1, v0, LX/8vE;->a:LX/8tB;

    iget-object p1, p1, LX/8tB;->c:Ljava/util/List;

    iput-object p1, p0, LX/8tB;->d:Ljava/util/List;

    goto :goto_0
.end method

.method public b(Ljava/lang/CharSequence;)LX/39y;
    .locals 8

    .prologue
    .line 1412624
    new-instance v2, LX/39y;

    invoke-direct {v2}, LX/39y;-><init>()V

    .line 1412625
    iget-object v0, p0, LX/8tD;->c:LX/8vE;

    invoke-virtual {v0}, LX/8vE;->a()Ljava/util/List;

    move-result-object v0

    .line 1412626
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v2, LX/39y;->b:I

    .line 1412627
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1412628
    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 1412629
    :goto_0
    return-object v0

    .line 1412630
    :cond_0
    iget-object v1, p0, LX/8tD;->b:LX/8RK;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, LX/8RK;->a(Ljava/lang/String;)V

    .line 1412631
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1412632
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1412633
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1412634
    instance-of v1, v0, LX/622;

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 1412635
    check-cast v1, LX/622;

    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v1

    .line 1412636
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1412637
    check-cast v1, LX/8QL;

    .line 1412638
    iget-object v7, p0, LX/8tD;->b:LX/8RK;

    invoke-interface {v7, v1}, LX/8RK;->a(LX/8QL;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1412639
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1412640
    :cond_3
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v1

    goto :goto_2

    .line 1412641
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1412642
    iget-boolean v5, p0, LX/8tD;->a:Z

    if-nez v5, :cond_5

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1412643
    :cond_5
    new-instance v5, LX/623;

    invoke-interface {v0}, LX/621;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0, v1}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1412644
    :cond_6
    iput-object v3, v2, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 1412645
    goto :goto_0
.end method
