.class public LX/AQz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1672020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;LX/AQy;Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;Landroid/text/TextWatcher;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1672021
    iget-object v0, p2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-object v0, v0

    .line 1672022
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->STARTED_JOB:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-eq v0, v1, :cond_0

    .line 1672023
    iget-object v0, p2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-object v0, v0

    .line 1672024
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->GRADUATED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-ne v0, v1, :cond_2

    .line 1672025
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/AQy;->setIsTitleEditable(Z)V

    .line 1672026
    :goto_0
    iget-object v0, p2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1672027
    invoke-interface {p1, v0}, LX/AQy;->setTitle(Ljava/lang/String;)V

    .line 1672028
    invoke-interface {p1, p3}, LX/AQy;->a(Landroid/text/TextWatcher;)V

    .line 1672029
    iget-object v0, p2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object v1, v0

    .line 1672030
    if-nez v1, :cond_3

    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1, v0}, LX/AQy;->setIconUri(Ljava/lang/String;)V

    .line 1672031
    if-eqz v1, :cond_1

    .line 1672032
    const v0, 0x7f0b0b6e

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-interface {p1, v0}, LX/AQy;->setIconSize(I)V

    .line 1672033
    :cond_1
    invoke-interface {p1, p4}, LX/AQy;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1672034
    return-void

    .line 1672035
    :cond_2
    const/4 v0, 0x1

    invoke-interface {p1, v0}, LX/AQy;->setIsTitleEditable(Z)V

    goto :goto_0

    .line 1672036
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
