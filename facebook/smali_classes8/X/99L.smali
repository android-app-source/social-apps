.class public LX/99L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/99L;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/0Px",
            "<",
            "LX/99K;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1447261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1447262
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/99L;->a:Ljava/util/Map;

    .line 1447263
    invoke-direct {p0}, LX/99L;->a()V

    .line 1447264
    return-void
.end method

.method public static a(LX/99K;LX/0Px;)F
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/99I;",
            ">(",
            "LX/99K;",
            "LX/0Px",
            "<TT;>;)F"
        }
    .end annotation

    .prologue
    const/high16 v11, 0x40a00000    # 5.0f

    const/4 v3, 0x0

    const/high16 v10, 0x41c80000    # 25.0f

    .line 1447265
    iget-object v0, p0, LX/99K;->b:LX/0Px;

    move-object v0, v0

    .line 1447266
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    .line 1447267
    const/4 v0, 0x0

    move v4, v0

    move v5, v3

    :goto_0
    if-ge v4, v6, :cond_6

    .line 1447268
    iget-object v0, p0, LX/99K;->b:LX/0Px;

    move-object v0, v0

    .line 1447269
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/99J;

    .line 1447270
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/99I;

    invoke-interface {v1}, LX/99I;->a()I

    move-result v7

    .line 1447271
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/99I;

    invoke-interface {v1}, LX/99I;->b()I

    move-result v8

    .line 1447272
    iget v1, v0, LX/99J;->c:I

    mul-int/2addr v1, v8

    int-to-float v1, v1

    int-to-float v2, v7

    div-float/2addr v1, v2

    iget v2, v0, LX/99J;->d:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 1447273
    iget v1, v0, LX/99J;->c:I

    int-to-float v1, v1

    int-to-float v2, v8

    mul-float/2addr v1, v2

    int-to-float v2, v7

    div-float/2addr v1, v2

    .line 1447274
    iget v2, v0, LX/99J;->d:I

    int-to-float v2, v2

    sub-float v2, v1, v2

    div-float v1, v2, v1

    move v2, v1

    .line 1447275
    :goto_1
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    sub-float v1, v5, v1

    .line 1447276
    if-ne v7, v8, :cond_1

    .line 1447277
    mul-float v0, v2, v11

    sub-float v0, v1, v0

    .line 1447278
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v5, v0

    goto :goto_0

    .line 1447279
    :cond_0
    iget v1, v0, LX/99J;->d:I

    mul-int/2addr v1, v7

    int-to-float v1, v1

    int-to-float v2, v8

    div-float/2addr v1, v2

    iget v2, v0, LX/99J;->c:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_8

    .line 1447280
    iget v1, v0, LX/99J;->d:I

    int-to-float v1, v1

    int-to-float v2, v7

    mul-float/2addr v1, v2

    int-to-float v2, v8

    div-float/2addr v1, v2

    .line 1447281
    iget v2, v0, LX/99J;->c:I

    int-to-float v2, v2

    sub-float v2, v1, v2

    div-float v1, v2, v1

    move v2, v1

    goto :goto_1

    .line 1447282
    :cond_1
    iget v5, v0, LX/99J;->c:I

    iget v9, v0, LX/99J;->d:I

    if-le v5, v9, :cond_3

    .line 1447283
    if-le v7, v8, :cond_2

    .line 1447284
    mul-float v0, v2, v10

    add-float/2addr v0, v1

    goto :goto_2

    .line 1447285
    :cond_2
    mul-float v0, v2, v10

    sub-float v0, v1, v0

    goto :goto_2

    .line 1447286
    :cond_3
    iget v5, v0, LX/99J;->c:I

    iget v9, v0, LX/99J;->d:I

    if-ge v5, v9, :cond_5

    .line 1447287
    if-ge v7, v8, :cond_4

    .line 1447288
    mul-float v0, v2, v10

    add-float/2addr v0, v1

    goto :goto_2

    .line 1447289
    :cond_4
    mul-float v0, v2, v10

    sub-float v0, v1, v0

    goto :goto_2

    .line 1447290
    :cond_5
    iget v5, v0, LX/99J;->c:I

    iget v0, v0, LX/99J;->d:I

    if-ne v5, v0, :cond_7

    .line 1447291
    mul-float v0, v2, v11

    sub-float v0, v1, v0

    goto :goto_2

    .line 1447292
    :cond_6
    mul-int/lit16 v0, v6, 0xc8

    int-to-float v0, v0

    add-float/2addr v0, v5

    .line 1447293
    return v0

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/99L;
    .locals 3

    .prologue
    .line 1447294
    sget-object v0, LX/99L;->b:LX/99L;

    if-nez v0, :cond_1

    .line 1447295
    const-class v1, LX/99L;

    monitor-enter v1

    .line 1447296
    :try_start_0
    sget-object v0, LX/99L;->b:LX/99L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1447297
    if-eqz v2, :cond_0

    .line 1447298
    :try_start_1
    new-instance v0, LX/99L;

    invoke-direct {v0}, LX/99L;-><init>()V

    .line 1447299
    move-object v0, v0

    .line 1447300
    sput-object v0, LX/99L;->b:LX/99L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447301
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1447302
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1447303
    :cond_1
    sget-object v0, LX/99L;->b:LX/99L;

    return-object v0

    .line 1447304
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1447305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 15

    .prologue
    const/4 v14, 0x6

    const/4 v13, 0x4

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x3

    .line 1447306
    iget-object v0, p0, LX/99L;->a:Ljava/util/Map;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, LX/99H;

    sget-object v3, LX/99G;->USER:LX/99G;

    sget-object v4, LX/99G;->PAGE:LX/99G;

    invoke-static {v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-direct {v2, v3}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v2, v11, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v10, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2}, LX/99H;->a()LX/99K;

    move-result-object v2

    new-instance v3, LX/99H;

    sget-object v4, LX/99G;->USER:LX/99G;

    sget-object v5, LX/99G;->PAGE:LX/99G;

    invoke-static {v4, v5}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {v3, v4}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v3, v11, v11, v14, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v11, v10, v14, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3}, LX/99H;->a()LX/99K;

    move-result-object v3

    new-instance v4, LX/99H;

    sget-object v5, LX/99G;->USER:LX/99G;

    sget-object v6, LX/99G;->PAGE:LX/99G;

    invoke-static {v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-direct {v4, v5}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v4, v11, v11, v10, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4, v10, v11, v10, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4}, LX/99H;->a()LX/99K;

    move-result-object v4

    new-instance v5, LX/99H;

    sget-object v6, LX/99G;->USER:LX/99G;

    invoke-static {v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v6

    invoke-direct {v5, v6}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v5, v11, v11, v14, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v5

    invoke-virtual {v5, v11, v12, v14, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v5

    invoke-virtual {v5}, LX/99H;->a()LX/99K;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1447307
    iget-object v7, p0, LX/99L;->a:Ljava/util/Map;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, LX/99H;

    sget-object v1, LX/99G;->USER:LX/99G;

    sget-object v2, LX/99G;->PAGE:LX/99G;

    invoke-static {v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {v0, v1}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v0, v11, v11, v14, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v0

    invoke-virtual {v0, v11, v10, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v0

    invoke-virtual {v0, v10, v10, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v0

    invoke-virtual {v0}, LX/99H;->a()LX/99K;

    move-result-object v0

    new-instance v1, LX/99H;

    sget-object v2, LX/99G;->USER:LX/99G;

    sget-object v3, LX/99G;->PAGE:LX/99G;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-direct {v1, v2}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v1, v11, v11, v10, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v1

    invoke-virtual {v1, v10, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v1

    invoke-virtual {v1, v10, v10, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v1

    invoke-virtual {v1}, LX/99H;->a()LX/99K;

    move-result-object v1

    new-instance v2, LX/99H;

    sget-object v3, LX/99G;->PAGE:LX/99G;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-direct {v2, v3}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v2, v11, v11, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v12, v11, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v13, v11, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2}, LX/99H;->a()LX/99K;

    move-result-object v2

    new-instance v3, LX/99H;

    sget-object v4, LX/99G;->USER:LX/99G;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {v3, v4}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v3, v11, v11, v14, v13}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v11, v13, v10, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v10, v13, v10, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3}, LX/99H;->a()LX/99K;

    move-result-object v3

    new-instance v4, LX/99H;

    sget-object v5, LX/99G;->USER:LX/99G;

    invoke-static {v5}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-direct {v4, v5}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v4, v11, v11, v13, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4, v13, v11, v12, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4, v13, v10, v12, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4}, LX/99H;->a()LX/99K;

    move-result-object v4

    new-instance v5, LX/99H;

    sget-object v6, LX/99G;->USER:LX/99G;

    invoke-static {v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v6

    invoke-direct {v5, v6}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v5, v11, v11, v14, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v5

    invoke-virtual {v5, v11, v12, v14, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v5

    invoke-virtual {v5, v11, v13, v14, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v5

    invoke-virtual {v5}, LX/99H;->a()LX/99K;

    move-result-object v5

    new-instance v6, LX/99H;

    sget-object v9, LX/99G;->USER:LX/99G;

    invoke-static {v9}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v9

    invoke-direct {v6, v9}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v6, v11, v11, v12, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v6

    invoke-virtual {v6, v12, v11, v12, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v6

    invoke-virtual {v6, v13, v11, v12, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v6

    invoke-virtual {v6}, LX/99H;->a()LX/99K;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1447308
    iget-object v0, p0, LX/99L;->a:Ljava/util/Map;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, LX/99H;

    sget-object v3, LX/99G;->USER:LX/99G;

    sget-object v4, LX/99G;->PAGE:LX/99G;

    invoke-static {v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-direct {v2, v3}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v2, v11, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v10, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v11, v10, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v10, v10, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2}, LX/99H;->a()LX/99K;

    move-result-object v2

    new-instance v3, LX/99H;

    sget-object v4, LX/99G;->USER:LX/99G;

    sget-object v5, LX/99G;->PAGE:LX/99G;

    invoke-static {v4, v5}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {v3, v4}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v3, v11, v11, v14, v13}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v11, v13, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v12, v13, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v13, v13, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3}, LX/99H;->a()LX/99K;

    move-result-object v3

    new-instance v4, LX/99H;

    sget-object v5, LX/99G;->USER:LX/99G;

    sget-object v6, LX/99G;->PAGE:LX/99G;

    invoke-static {v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-direct {v4, v5}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v4, v11, v11, v13, v14}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4, v13, v11, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4, v13, v12, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4, v13, v13, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v4

    invoke-virtual {v4}, LX/99H;->a()LX/99K;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1447309
    iget-object v0, p0, LX/99L;->a:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, LX/99H;

    sget-object v3, LX/99G;->USER:LX/99G;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-direct {v2, v3}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v2, v11, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v11, v10, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v10, v11, v10, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v10, v12, v10, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2, v10, v13, v10, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v2

    invoke-virtual {v2}, LX/99H;->a()LX/99K;

    move-result-object v2

    new-instance v3, LX/99H;

    sget-object v4, LX/99G;->USER:LX/99G;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {v3, v4}, LX/99H;-><init>(Ljava/util/EnumSet;)V

    invoke-virtual {v3, v11, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v10, v11, v10, v10}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v11, v10, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v12, v10, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3, v13, v10, v12, v12}, LX/99H;->a(IIII)LX/99H;

    move-result-object v3

    invoke-virtual {v3}, LX/99H;->a()LX/99K;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1447310
    return-void
.end method
