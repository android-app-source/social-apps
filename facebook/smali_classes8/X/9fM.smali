.class public final LX/9fM;
.super LX/9fL;
.source ""


# instance fields
.field public final synthetic m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

.field private n:Lcom/facebook/drawee/view/DraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1521785
    iput-object p1, p0, LX/9fM;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    .line 1521786
    invoke-direct {p0, p1, p2}, LX/9fL;-><init>(Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;Landroid/view/View;)V

    .line 1521787
    const v0, 0x7f0d0d16

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, LX/9fM;->n:Lcom/facebook/drawee/view/DraweeView;

    .line 1521788
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 4

    .prologue
    .line 1521789
    invoke-super {p0, p1}, LX/9fL;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    .line 1521790
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    .line 1521791
    iget-object v1, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1521792
    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFilterName(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1521793
    iget-object v1, p0, LX/9fM;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->h:LX/9fP;

    sget-object v2, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/9fP;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/9ek;I)LX/8Hx;

    move-result-object v0

    .line 1521794
    iget-object v1, p0, LX/9fM;->n:Lcom/facebook/drawee/view/DraweeView;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/9fL;->b(Z)LX/1af;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1521795
    iget-object v1, p0, LX/9fM;->n:Lcom/facebook/drawee/view/DraweeView;

    iget-object v2, p0, LX/9fM;->m:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->j:Landroid/net/Uri;

    invoke-virtual {p0, v2, v0}, LX/9fL;->a(Landroid/net/Uri;LX/33B;)LX/1aZ;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1521796
    return-void
.end method
