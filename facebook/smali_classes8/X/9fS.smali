.class public final LX/9fS;
.super LX/9cS;
.source ""


# instance fields
.field public final synthetic a:LX/9fU;


# direct methods
.method public constructor <init>(LX/9fU;)V
    .locals 0

    .prologue
    .line 1521909
    iput-object p1, p0, LX/9fS;->a:LX/9fU;

    invoke-direct {p0}, LX/9cS;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1521910
    iget-object v0, p0, LX/9fS;->a:LX/9fU;

    iget-object v0, v0, LX/9fU;->k:LX/9cg;

    const/4 p0, 0x1

    const/4 v2, 0x0

    .line 1521911
    iget-object v1, v0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->getCount()I

    move-result v1

    if-le v1, p0, :cond_0

    .line 1521912
    iget-object v1, v0, LX/9cg;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, p0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1521913
    iget-object v1, v0, LX/9cg;->f:Lit/sephiroth/android/library/widget/HListView;

    invoke-virtual {v1, v2}, LX/8vi;->setSelection(I)V

    .line 1521914
    :cond_0
    iget-object v1, v0, LX/9cg;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 1521915
    invoke-virtual {v0, v2}, LX/9cg;->setVisibility(I)V

    .line 1521916
    new-instance v1, LX/9cf;

    invoke-direct {v1, v0}, LX/9cf;-><init>(LX/9cg;)V

    invoke-virtual {v0, v1}, LX/9cg;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1521917
    iget-object v1, v0, LX/9cg;->d:LX/9c3;

    iget-object v2, v0, LX/9cg;->n:Ljava/lang/String;

    .line 1521918
    iget-object p0, v1, LX/9c3;->b:LX/11i;

    sget-object p1, LX/9c3;->a:LX/9c1;

    invoke-interface {p0, p1, v2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object p0

    .line 1521919
    if-nez p0, :cond_1

    .line 1521920
    :goto_0
    return-void

    .line 1521921
    :cond_1
    sget-object p1, LX/9c2;->STICKER_PICKER_OPENED:LX/9c2;

    iget-object p1, p1, LX/9c2;->name:Ljava/lang/String;

    const v0, -0x1169258

    invoke-static {p0, p1, v0}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method
