.class public LX/9B7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1zf;

.field private final b:LX/154;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/1zf;LX/154;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1451331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1451332
    iput-object p1, p0, LX/9B7;->a:LX/1zf;

    .line 1451333
    iput-object p2, p0, LX/9B7;->b:LX/154;

    .line 1451334
    iput-object p3, p0, LX/9B7;->c:Landroid/content/Context;

    .line 1451335
    return-void
.end method

.method public static a(LX/0QB;)LX/9B7;
    .locals 6

    .prologue
    .line 1451343
    const-class v1, LX/9B7;

    monitor-enter v1

    .line 1451344
    :try_start_0
    sget-object v0, LX/9B7;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1451345
    sput-object v2, LX/9B7;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1451346
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1451347
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1451348
    new-instance p0, LX/9B7;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v3

    check-cast v3, LX/1zf;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v4

    check-cast v4, LX/154;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/9B7;-><init>(LX/1zf;LX/154;Landroid/content/Context;)V

    .line 1451349
    move-object v0, p0

    .line 1451350
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1451351
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9B7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1451352
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1451353
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(ILjava/util/HashMap;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1451342
    if-eqz p1, :cond_0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1zt;)I
    .locals 1

    .prologue
    .line 1451354
    invoke-static {p0}, LX/9B3;->e(LX/1zt;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, -0xa76f01

    :goto_0
    return v0

    .line 1451355
    :cond_0
    iget v0, p0, LX/1zt;->g:I

    move v0, v0

    .line 1451356
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1451341
    iget-object v0, p0, LX/9B7;->a:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->c()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1zt;Ljava/util/HashMap;)Ljava/lang/CharSequence;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1zt;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 1451337
    iget-object v0, p0, LX/9B7;->b:LX/154;

    .line 1451338
    iget v1, p1, LX/1zt;->e:I

    move v1, v1

    .line 1451339
    invoke-static {v1, p2}, LX/9B7;->b(ILjava/util/HashMap;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1451340
    invoke-static {p1}, LX/9B3;->e(LX/1zt;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/9B7;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081993

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1451336
    iget-object v0, p0, LX/9B7;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a05bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method
