.class public LX/9k7;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530843
    const-string v0, "places.db"

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 1530844
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 1530845
    sget-object v0, LX/9k8;->a:Ljava/lang/String;

    const p0, 0x521f134d

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x25343607

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1530846
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1530847
    invoke-virtual {p0, p1, p2, p3}, LX/9k7;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 1530848
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1530849
    const/4 p0, 0x1

    if-ne p3, p0, :cond_0

    .line 1530850
    const-string p0, "DROP TABLE IF EXISTS places_model"

    const p2, -0x4b8f548c

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, -0x689b7067

    invoke-static {p0}, LX/03h;->a(I)V

    .line 1530851
    sget-object p0, LX/9k8;->a:Ljava/lang/String;

    const p2, -0x40294ace

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0xc288f91

    invoke-static {p0}, LX/03h;->a(I)V

    .line 1530852
    :cond_0
    return-void
.end method
