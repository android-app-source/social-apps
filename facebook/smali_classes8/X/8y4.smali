.class public LX/8y4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425100
    iput-object p1, p0, LX/8y4;->a:LX/0tX;

    .line 1425101
    iput-object p2, p0, LX/8y4;->b:LX/1Ck;

    .line 1425102
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$PlaceListRemoveConfirmedProfileRecommendationMutationModel;
    .locals 8

    .prologue
    .line 1425103
    new-instance v0, LX/5J6;

    invoke-direct {v0}, LX/5J6;-><init>()V

    .line 1425104
    if-eqz p0, :cond_0

    .line 1425105
    invoke-static {p0}, LX/5I9;->c(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    move-result-object v1

    .line 1425106
    iput-object v1, v0, LX/5J6;->a:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    .line 1425107
    :cond_0
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1425108
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1425109
    iget-object v3, v0, LX/5J6;->a:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1425110
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1425111
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1425112
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1425113
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1425114
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1425115
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1425116
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1425117
    new-instance v3, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$PlaceListRemoveConfirmedProfileRecommendationMutationModel;

    invoke-direct {v3, v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$PlaceListRemoveConfirmedProfileRecommendationMutationModel;-><init>(LX/15i;)V

    .line 1425118
    move-object v0, v3

    .line 1425119
    return-object v0
.end method
