.class public final LX/AQE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AQF;


# direct methods
.method public constructor <init>(LX/AQF;)V
    .locals 0

    .prologue
    .line 1671057
    iput-object p1, p0, LX/AQE;->a:LX/AQF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1671056
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1671042
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1671043
    if-eqz p1, :cond_0

    .line 1671044
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671045
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1671046
    :goto_1
    return-void

    .line 1671047
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671048
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1671049
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1671050
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671051
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1671052
    new-instance v5, LX/0XI;

    invoke-direct {v5}, LX/0XI;-><init>()V

    iget-object v0, p0, LX/AQE;->a:LX/AQF;

    iget-object v0, v0, LX/AQF;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v5, v0}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v0

    .line 1671053
    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, LX/15i;->j(II)I

    move-result v5

    invoke-virtual {v3, v4, v1}, LX/15i;->j(II)I

    move-result v1

    invoke-virtual {v3, v4, v2}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {v0, v5, v1, v2}, LX/0XI;->a(III)LX/0XI;

    .line 1671054
    iget-object v1, p0, LX/AQE;->a:LX/AQF;

    iget-object v1, v1, LX/AQF;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0WJ;->a(Lcom/facebook/user/model/User;)V

    goto :goto_1

    .line 1671055
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
