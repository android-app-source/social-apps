.class public final LX/8io;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/8ip;


# direct methods
.method public constructor <init>(LX/8ip;)V
    .locals 0

    .prologue
    .line 1391980
    iput-object p1, p0, LX/8io;->a:LX/8ip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1391981
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-boolean v2, v2, LX/8ip;->d:Z

    if-eqz v2, :cond_5

    .line 1391982
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-object v2, v2, LX/8ip;->c:Landroid/view/GestureDetector;

    invoke-virtual {v2, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1391983
    if-nez v2, :cond_3

    .line 1391984
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_4

    .line 1391985
    :cond_0
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-object v2, v2, LX/8ip;->f:LX/8ik;

    if-eqz v2, :cond_1

    .line 1391986
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-object v2, v2, LX/8ip;->f:LX/8ik;

    invoke-virtual {v2}, LX/8ik;->b()V

    .line 1391987
    :cond_1
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-boolean v2, v2, LX/8ip;->i:Z

    if-nez v2, :cond_2

    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-object v2, v2, LX/8ip;->e:LX/8ir;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/8io;->a:LX/8ip;

    invoke-static {v2, p2}, LX/8ip;->a$redex0(LX/8ip;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-boolean v2, v2, LX/8ip;->j:Z

    if-nez v2, :cond_2

    .line 1391988
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    iget-object v2, v2, LX/8ip;->e:LX/8ir;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    invoke-virtual {v2}, LX/8ir;->a()V

    .line 1391989
    :cond_2
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    .line 1391990
    iput-boolean v1, v2, LX/8ip;->i:Z

    .line 1391991
    :cond_3
    :goto_0
    return v0

    .line 1391992
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 1391993
    iget-object v4, p0, LX/8io;->a:LX/8ip;

    iget-boolean v4, v4, LX/8ip;->h:Z

    if-eqz v4, :cond_3

    sget v4, LX/8ip;->a:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    .line 1391994
    iget-object v2, p0, LX/8io;->a:LX/8ip;

    .line 1391995
    iput-boolean v1, v2, LX/8ip;->h:Z

    .line 1391996
    iget-object v1, p0, LX/8io;->a:LX/8ip;

    iget-object v1, v1, LX/8ip;->c:Landroid/view/GestureDetector;

    invoke-virtual {v1}, Landroid/view/GestureDetector;->isLongpressEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/8io;->a:LX/8ip;

    invoke-static {v1, p2}, LX/8ip;->a$redex0(LX/8ip;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1391997
    iget-object v1, p0, LX/8io;->a:LX/8ip;

    invoke-static {v1}, LX/8ip;->b(LX/8ip;)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1391998
    goto :goto_0
.end method
