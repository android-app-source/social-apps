.class public final LX/8pj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/3H7;


# direct methods
.method public constructor <init>(LX/3H7;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1406271
    iput-object p1, p0, LX/8pj;->c:LX/3H7;

    iput-object p2, p0, LX/8pj;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/8pj;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1406261
    iget-object v0, p0, LX/8pj;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1406262
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1406263
    check-cast p1, Ljava/util/List;

    .line 1406264
    iget-object v0, p0, LX/8pj;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406265
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406266
    if-eqz v0, :cond_1

    .line 1406267
    invoke-static {v1, v0}, LX/20j;->c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 1406268
    goto :goto_0

    .line 1406269
    :cond_0
    iget-object v0, p0, LX/8pj;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x6c46625a

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1406270
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method
