.class public abstract LX/AGp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/audience/model/AudienceControlData;

.field public final b:Lcom/facebook/audience/model/AudienceControlData;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V
    .locals 0

    .prologue
    .line 1653183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1653184
    iput-object p1, p0, LX/AGp;->a:Lcom/facebook/audience/model/AudienceControlData;

    .line 1653185
    iput-object p2, p0, LX/AGp;->b:Lcom/facebook/audience/model/AudienceControlData;

    .line 1653186
    return-void
.end method


# virtual methods
.method public a()LX/7h0;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1653148
    invoke-virtual {p0}, LX/AGp;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1653149
    const/4 v0, 0x0

    .line 1653150
    :goto_0
    return-object v0

    .line 1653151
    :cond_0
    new-instance v0, LX/7gx;

    invoke-direct {v0}, LX/7gx;-><init>()V

    .line 1653152
    invoke-virtual {p0}, LX/AGp;->k()Ljava/lang/String;

    move-result-object v1

    .line 1653153
    iput-object v1, v0, LX/7gx;->i:Ljava/lang/String;

    .line 1653154
    move-object v0, v0

    .line 1653155
    invoke-virtual {p0}, LX/AGp;->j()LX/7gy;

    move-result-object v1

    .line 1653156
    iput-object v1, v0, LX/7gx;->b:LX/7gy;

    .line 1653157
    move-object v0, v0

    .line 1653158
    invoke-virtual {p0}, LX/AGp;->i()J

    move-result-wide v2

    .line 1653159
    iput-wide v2, v0, LX/7gx;->c:J

    .line 1653160
    move-object v0, v0

    .line 1653161
    invoke-virtual {p0}, LX/AGp;->h()Landroid/net/Uri;

    move-result-object v1

    .line 1653162
    iput-object v1, v0, LX/7gx;->e:Landroid/net/Uri;

    .line 1653163
    move-object v0, v0

    .line 1653164
    invoke-virtual {p0}, LX/AGp;->g()Landroid/net/Uri;

    move-result-object v1

    .line 1653165
    iput-object v1, v0, LX/7gx;->f:Landroid/net/Uri;

    .line 1653166
    move-object v0, v0

    .line 1653167
    invoke-virtual {p0}, LX/AGp;->f()Landroid/net/Uri;

    move-result-object v1

    .line 1653168
    iput-object v1, v0, LX/7gx;->g:Landroid/net/Uri;

    .line 1653169
    move-object v0, v0

    .line 1653170
    invoke-virtual {p0}, LX/AGp;->e()LX/7gz;

    move-result-object v1

    .line 1653171
    iput-object v1, v0, LX/7gx;->a:LX/7gz;

    .line 1653172
    move-object v0, v0

    .line 1653173
    invoke-virtual {p0}, LX/AGp;->d()Z

    move-result v1

    .line 1653174
    iput-boolean v1, v0, LX/7gx;->h:Z

    .line 1653175
    move-object v0, v0

    .line 1653176
    invoke-virtual {p0}, LX/AGp;->c()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v1

    .line 1653177
    iput-object v1, v0, LX/7gx;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1653178
    move-object v0, v0

    .line 1653179
    invoke-virtual {p0}, LX/AGp;->b()Ljava/lang/String;

    move-result-object v1

    .line 1653180
    iput-object v1, v0, LX/7gx;->k:Ljava/lang/String;

    .line 1653181
    move-object v0, v0

    .line 1653182
    invoke-virtual {v0}, LX/7gx;->a()LX/7h0;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Lcom/facebook/audience/model/AudienceControlData;
.end method

.method public abstract d()Z
.end method

.method public abstract e()LX/7gz;
.end method

.method public abstract f()Landroid/net/Uri;
.end method

.method public abstract g()Landroid/net/Uri;
.end method

.method public abstract h()Landroid/net/Uri;
.end method

.method public abstract i()J
.end method

.method public abstract j()LX/7gy;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 1653147
    iget-object v0, p0, LX/AGp;->a:Lcom/facebook/audience/model/AudienceControlData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AGp;->b:Lcom/facebook/audience/model/AudienceControlData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
