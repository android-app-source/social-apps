.class public final LX/9Ef;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;)V
    .locals 0

    .prologue
    .line 1457278
    iput-object p1, p0, LX/9Ef;->a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x51b040f1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457279
    iget-object v1, p0, LX/9Ef;->a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    iget-object v1, v1, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 1457280
    iget-object v2, v1, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1457281
    iget-object v2, v1, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 1457282
    :goto_0
    move-object v1, v2

    .line 1457283
    iget-object v2, p0, LX/9Ef;->a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    iget-object v2, v2, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->s:LX/9D8;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1457284
    new-instance v4, Landroid/content/Intent;

    iget-object v5, v2, LX/9D8;->a:LX/9DG;

    iget-object v5, v5, LX/9DG;->O:Landroid/support/v4/app/Fragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class p0, Lcom/facebook/transliteration/TransliterationActivity;

    invoke-direct {v4, v5, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1457285
    const-string v5, "composer_text"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1457286
    const-string v5, "entry_point"

    const-string p0, "comments"

    invoke-virtual {v4, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1457287
    iget-object v5, v2, LX/9D8;->a:LX/9DG;

    iget-object v5, v5, LX/9DG;->v:Lcom/facebook/content/SecureContextHelper;

    const/4 p0, 0x1

    iget-object p1, v2, LX/9D8;->a:LX/9DG;

    iget-object p1, p1, LX/9DG;->O:Landroid/support/v4/app/Fragment;

    invoke-interface {v5, v4, p0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1457288
    const v1, 0x1c326adb

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    iget-object v2, v1, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    goto :goto_0
.end method
