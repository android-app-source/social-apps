.class public final LX/8v5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/user/tiles/UserTileDrawableController;

.field private final b:LX/1FZ;

.field public c:LX/8v8;

.field public d:Landroid/text/TextPaint;

.field public e:Landroid/content/res/Resources;

.field public f:I

.field private g:Ljava/lang/Integer;

.field public h:Landroid/content/res/ColorStateList;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Ljava/lang/Integer;

.field private n:Ljava/lang/Integer;

.field private o:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1FZ;)V
    .locals 0

    .prologue
    .line 1416527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1416528
    iput-object p1, p0, LX/8v5;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1416529
    iput-object p2, p0, LX/8v5;->b:LX/1FZ;

    .line 1416530
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/8v6;
    .locals 18

    .prologue
    .line 1416531
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->c:LX/8v8;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1416532
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->d:Landroid/text/TextPaint;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1416533
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1416534
    move-object/from16 v0, p0

    iget v1, v0, LX/8v5;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1416535
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->h:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8v5;->h:Landroid/content/res/ColorStateList;

    .line 1416536
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1416537
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1416538
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 1416539
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 1416540
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->c:LX/8v8;

    invoke-virtual {v1}, LX/8QK;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_6

    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8v5;->l:Landroid/graphics/drawable/Drawable;

    .line 1416541
    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 1416542
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 1416543
    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    .line 1416544
    :goto_8
    new-instance v1, LX/8v6;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8v5;->c:LX/8v8;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8v5;->d:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8v5;->e:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget v6, v0, LX/8v5;->f:I

    move-object/from16 v0, p0

    iget-object v15, v0, LX/8v5;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8v5;->b:LX/1FZ;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v1 .. v17}, LX/8v6;-><init>(LX/8v8;Landroid/text/TextPaint;Landroid/content/res/Resources;Landroid/content/res/ColorStateList;IIIIILandroid/graphics/drawable/Drawable;IIZLcom/facebook/user/tiles/UserTileDrawableController;LX/1FZ;B)V

    .line 1416545
    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/8uk;->a(Landroid/content/Context;)V

    .line 1416546
    return-object v1

    .line 1416547
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a0a6c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    goto/16 :goto_0

    .line 1416548
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    goto/16 :goto_1

    .line 1416549
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b01bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    goto/16 :goto_2

    .line 1416550
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b01be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    goto/16 :goto_3

    .line 1416551
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b01bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    goto/16 :goto_4

    .line 1416552
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f021976

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    goto/16 :goto_5

    .line 1416553
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b01c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    goto/16 :goto_6

    .line 1416554
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b01c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v13

    goto/16 :goto_7

    .line 1416555
    :cond_9
    const/4 v14, 0x0

    goto/16 :goto_8
.end method
