.class public final LX/8rY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1408923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)LX/36N;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1408924
    if-nez p0, :cond_1

    .line 1408925
    :cond_0
    :goto_0
    return-object v2

    .line 1408926
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1408927
    const/4 v1, 0x0

    .line 1408928
    if-nez p0, :cond_4

    .line 1408929
    :cond_2
    :goto_1
    move v1, v1

    .line 1408930
    if-eqz v1, :cond_0

    .line 1408931
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1408932
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1408933
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1408934
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1408935
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_3

    .line 1408936
    const-string v1, "ProfileListConverter.getProfileListFriendingControllerGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1408937
    :cond_3
    new-instance v2, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1408938
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 1408939
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x285feb

    if-ne v3, v4, :cond_2

    .line 1408940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1408941
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1408942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v5

    const/4 v6, 0x0

    .line 1408943
    if-nez v5, :cond_5

    .line 1408944
    :goto_2
    move v5, v6

    .line 1408945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1408946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v7

    const/4 v8, 0x0

    .line 1408947
    if-nez v7, :cond_6

    .line 1408948
    :goto_3
    move v7, v8

    .line 1408949
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1408950
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1408951
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1408952
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1408953
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1408954
    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v4

    invoke-virtual {v0, v3, v4, v1}, LX/186;->a(III)V

    .line 1408955
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1408956
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1408957
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1408958
    :cond_5
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1408959
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;->a()I

    move-result v7

    invoke-virtual {v0, v6, v7, v6}, LX/186;->a(III)V

    .line 1408960
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    .line 1408961
    invoke-virtual {v0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 1408962
    :cond_6
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1408963
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->j()I

    move-result v9

    invoke-virtual {v0, v8, v9, v8}, LX/186;->a(III)V

    .line 1408964
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 1408965
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto :goto_3
.end method
