.class public final enum LX/A6W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/A6W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/A6W;

.field public static final enum BACK_BUTTON:LX/A6W;

.field public static final enum COMMENT_POSTED:LX/A6W;

.field public static final enum DICTIONARY_LOAD_FAILED:LX/A6W;

.field public static final enum DICTIONARY_MODIFICATION_FAILED:LX/A6W;

.field public static final enum DOWNLOAD_FAILED:LX/A6W;

.field public static final enum FINISHED:LX/A6W;

.field public static final enum HELP_CANCELLED:LX/A6W;

.field public static final enum HELP_OPENED:LX/A6W;

.field public static final enum HELP_USED:LX/A6W;

.field public static final enum MORE_CANCELLED:LX/A6W;

.field public static final enum MORE_OPENED:LX/A6W;

.field public static final enum OPENED:LX/A6W;

.field public static final enum PREFERENCE_CHANGED:LX/A6W;

.field public static final enum WORD_TRANSLITERATED:LX/A6W;


# instance fields
.field public final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1624350
    new-instance v0, LX/A6W;

    const-string v1, "BACK_BUTTON"

    const-string v2, "transliterator_back_pressed"

    invoke-direct {v0, v1, v4, v2}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->BACK_BUTTON:LX/A6W;

    .line 1624351
    new-instance v0, LX/A6W;

    const-string v1, "DICTIONARY_LOAD_FAILED"

    const-string v2, "transliterator_dictionary_load_failed"

    invoke-direct {v0, v1, v5, v2}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->DICTIONARY_LOAD_FAILED:LX/A6W;

    .line 1624352
    new-instance v0, LX/A6W;

    const-string v1, "DICTIONARY_MODIFICATION_FAILED"

    const-string v2, "transliterator_dictionary_mod_failed"

    invoke-direct {v0, v1, v6, v2}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->DICTIONARY_MODIFICATION_FAILED:LX/A6W;

    .line 1624353
    new-instance v0, LX/A6W;

    const-string v1, "DOWNLOAD_FAILED"

    const-string v2, "transliterator_download_failed"

    invoke-direct {v0, v1, v7, v2}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->DOWNLOAD_FAILED:LX/A6W;

    .line 1624354
    new-instance v0, LX/A6W;

    const-string v1, "FINISHED"

    const-string v2, "transliterator_finished"

    invoke-direct {v0, v1, v8, v2}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->FINISHED:LX/A6W;

    .line 1624355
    new-instance v0, LX/A6W;

    const-string v1, "HELP_CANCELLED"

    const/4 v2, 0x5

    const-string v3, "transliterator_help_cancelled"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->HELP_CANCELLED:LX/A6W;

    .line 1624356
    new-instance v0, LX/A6W;

    const-string v1, "HELP_OPENED"

    const/4 v2, 0x6

    const-string v3, "transliterator_help_opened"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->HELP_OPENED:LX/A6W;

    .line 1624357
    new-instance v0, LX/A6W;

    const-string v1, "HELP_USED"

    const/4 v2, 0x7

    const-string v3, "transliterator_help_used"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->HELP_USED:LX/A6W;

    .line 1624358
    new-instance v0, LX/A6W;

    const-string v1, "OPENED"

    const/16 v2, 0x8

    const-string v3, "transliterator_opened"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->OPENED:LX/A6W;

    .line 1624359
    new-instance v0, LX/A6W;

    const-string v1, "MORE_CANCELLED"

    const/16 v2, 0x9

    const-string v3, "transliterator_more_cancelled"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->MORE_CANCELLED:LX/A6W;

    .line 1624360
    new-instance v0, LX/A6W;

    const-string v1, "MORE_OPENED"

    const/16 v2, 0xa

    const-string v3, "transliterator_more_opened"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->MORE_OPENED:LX/A6W;

    .line 1624361
    new-instance v0, LX/A6W;

    const-string v1, "WORD_TRANSLITERATED"

    const/16 v2, 0xb

    const-string v3, "transliterator_word_transliterated"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->WORD_TRANSLITERATED:LX/A6W;

    .line 1624362
    new-instance v0, LX/A6W;

    const-string v1, "COMMENT_POSTED"

    const/16 v2, 0xc

    const-string v3, "transliterator_comment_posted"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->COMMENT_POSTED:LX/A6W;

    .line 1624363
    new-instance v0, LX/A6W;

    const-string v1, "PREFERENCE_CHANGED"

    const/16 v2, 0xd

    const-string v3, "transliterator_preference_changed"

    invoke-direct {v0, v1, v2, v3}, LX/A6W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A6W;->PREFERENCE_CHANGED:LX/A6W;

    .line 1624364
    const/16 v0, 0xe

    new-array v0, v0, [LX/A6W;

    sget-object v1, LX/A6W;->BACK_BUTTON:LX/A6W;

    aput-object v1, v0, v4

    sget-object v1, LX/A6W;->DICTIONARY_LOAD_FAILED:LX/A6W;

    aput-object v1, v0, v5

    sget-object v1, LX/A6W;->DICTIONARY_MODIFICATION_FAILED:LX/A6W;

    aput-object v1, v0, v6

    sget-object v1, LX/A6W;->DOWNLOAD_FAILED:LX/A6W;

    aput-object v1, v0, v7

    sget-object v1, LX/A6W;->FINISHED:LX/A6W;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/A6W;->HELP_CANCELLED:LX/A6W;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/A6W;->HELP_OPENED:LX/A6W;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/A6W;->HELP_USED:LX/A6W;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/A6W;->OPENED:LX/A6W;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/A6W;->MORE_CANCELLED:LX/A6W;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/A6W;->MORE_OPENED:LX/A6W;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/A6W;->WORD_TRANSLITERATED:LX/A6W;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/A6W;->COMMENT_POSTED:LX/A6W;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/A6W;->PREFERENCE_CHANGED:LX/A6W;

    aput-object v2, v0, v1

    sput-object v0, LX/A6W;->$VALUES:[LX/A6W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1624347
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1624348
    iput-object p3, p0, LX/A6W;->eventName:Ljava/lang/String;

    .line 1624349
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/A6W;
    .locals 1

    .prologue
    .line 1624366
    const-class v0, LX/A6W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/A6W;

    return-object v0
.end method

.method public static values()[LX/A6W;
    .locals 1

    .prologue
    .line 1624365
    sget-object v0, LX/A6W;->$VALUES:[LX/A6W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/A6W;

    return-object v0
.end method
