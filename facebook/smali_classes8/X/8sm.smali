.class public LX/8sm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8sj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:J

.field private d:Ljava/lang/Runnable;

.field public e:Landroid/animation/Animator;

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(LX/0SG;Ljava/lang/Runnable;JLX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "Ljava/lang/Runnable;",
            "J",
            "LX/0Px",
            "<",
            "LX/8sj;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1411773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411774
    iput-boolean v0, p0, LX/8sm;->f:Z

    .line 1411775
    iput-boolean v0, p0, LX/8sm;->g:Z

    .line 1411776
    iput-object p2, p0, LX/8sm;->d:Ljava/lang/Runnable;

    .line 1411777
    iput-wide p3, p0, LX/8sm;->c:J

    .line 1411778
    iput-object p5, p0, LX/8sm;->b:LX/0Px;

    .line 1411779
    iput-object p1, p0, LX/8sm;->a:LX/0SG;

    .line 1411780
    return-void
.end method

.method public static a(LX/8sm;I)J
    .locals 6

    .prologue
    .line 1411781
    const-wide/16 v2, 0x0

    .line 1411782
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/8sm;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1411783
    if-ne v1, p1, :cond_0

    .line 1411784
    :goto_1
    return-wide v2

    .line 1411785
    :cond_0
    iget-object v0, p0, LX/8sm;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8sj;

    invoke-interface {v0}, LX/8sj;->a()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 1411786
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1411787
    :cond_1
    const-wide v2, 0x7fffffffffffffffL

    goto :goto_1
.end method

.method public static a$redex0(LX/8sm;IF)V
    .locals 6

    .prologue
    .line 1411788
    iget-object v0, p0, LX/8sm;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8sj;

    .line 1411789
    invoke-interface {v0, p2}, LX/8sj;->a(F)Landroid/animation/Animator;

    move-result-object v2

    .line 1411790
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, p2

    invoke-interface {v0}, LX/8sj;->a()J

    move-result-wide v4

    long-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-long v4, v3

    .line 1411791
    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1411792
    move-object v0, v2

    .line 1411793
    new-instance v1, LX/8sl;

    invoke-direct {v1, p0, p1}, LX/8sl;-><init>(LX/8sm;I)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1411794
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1411795
    iput-object v0, p0, LX/8sm;->e:Landroid/animation/Animator;

    .line 1411796
    return-void
.end method

.method public static c$redex0(LX/8sm;)V
    .locals 1

    .prologue
    .line 1411797
    iget-object v0, p0, LX/8sm;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1411798
    iget-object v0, p0, LX/8sm;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1411799
    const/4 v0, 0x0

    iput-object v0, p0, LX/8sm;->d:Ljava/lang/Runnable;

    .line 1411800
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 1411801
    iget-boolean v0, p0, LX/8sm;->f:Z

    if-eqz v0, :cond_0

    .line 1411802
    :goto_0
    return-void

    .line 1411803
    :cond_0
    iget-object v0, p0, LX/8sm;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1411804
    iget-wide v2, p0, LX/8sm;->c:J

    sub-long v2, v0, v2

    .line 1411805
    const/4 v8, 0x0

    move v9, v8

    :goto_1
    iget-object v8, p0, LX/8sm;->b:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_3

    .line 1411806
    invoke-static {p0, v9}, LX/8sm;->a(LX/8sm;I)J

    move-result-wide v10

    .line 1411807
    iget-object v8, p0, LX/8sm;->b:LX/0Px;

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/8sj;

    invoke-interface {v8}, LX/8sj;->a()J

    move-result-wide v12

    add-long/2addr v12, v10

    .line 1411808
    cmp-long v8, v10, v2

    if-gtz v8, :cond_2

    cmp-long v8, v2, v12

    if-gez v8, :cond_2

    .line 1411809
    :goto_2
    move v1, v9

    .line 1411810
    if-gez v1, :cond_1

    .line 1411811
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8sm;->f:Z

    .line 1411812
    iget-object v0, p0, LX/8sm;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 1411813
    :cond_1
    iget-object v0, p0, LX/8sm;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8sj;

    invoke-interface {v0}, LX/8sj;->a()J

    move-result-wide v4

    .line 1411814
    invoke-static {p0, v1}, LX/8sm;->a(LX/8sm;I)J

    move-result-wide v6

    sub-long/2addr v2, v6

    long-to-float v0, v2

    long-to-float v2, v4

    div-float/2addr v0, v2

    .line 1411815
    invoke-static {p0, v1, v0}, LX/8sm;->a$redex0(LX/8sm;IF)V

    goto :goto_0

    .line 1411816
    :cond_2
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_1

    .line 1411817
    :cond_3
    const/4 v9, -0x1

    goto :goto_2
.end method
