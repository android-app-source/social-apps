.class public LX/9IE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:LX/1Uf;

.field public final c:LX/17W;


# direct methods
.method public constructor <init>(LX/1nu;LX/1Uf;LX/17W;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1462722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462723
    iput-object p1, p0, LX/9IE;->a:LX/1nu;

    .line 1462724
    iput-object p2, p0, LX/9IE;->b:LX/1Uf;

    .line 1462725
    iput-object p3, p0, LX/9IE;->c:LX/17W;

    .line 1462726
    return-void
.end method

.method public static a(LX/0QB;)LX/9IE;
    .locals 6

    .prologue
    .line 1462727
    const-class v1, LX/9IE;

    monitor-enter v1

    .line 1462728
    :try_start_0
    sget-object v0, LX/9IE;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462729
    sput-object v2, LX/9IE;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462732
    new-instance p0, LX/9IE;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-direct {p0, v3, v4, v5}, LX/9IE;-><init>(LX/1nu;LX/1Uf;LX/17W;)V

    .line 1462733
    move-object v0, p0

    .line 1462734
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462735
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462736
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
