.class public final LX/AMB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/ui/VideoView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/ui/VideoView;)V
    .locals 0

    .prologue
    .line 1665869
    iput-object p1, p0, LX/AMB;->a:Lcom/facebook/backstage/ui/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 6

    .prologue
    .line 1665870
    iget-object v0, p0, LX/AMB;->a:Lcom/facebook/backstage/ui/VideoView;

    iget-object v0, v0, Lcom/facebook/backstage/ui/VideoView;->c:Landroid/view/TextureView;

    iget-object v1, p0, LX/AMB;->a:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v3

    iget-object v4, p0, LX/AMB;->a:Lcom/facebook/backstage/ui/VideoView;

    iget v4, v4, Lcom/facebook/backstage/ui/VideoView;->e:I

    iget-object v5, p0, LX/AMB;->a:Lcom/facebook/backstage/ui/VideoView;

    iget-boolean v5, v5, Lcom/facebook/backstage/ui/VideoView;->f:Z

    invoke-static {v1, v2, v3, v4, v5}, Lcom/facebook/backstage/ui/VideoView;->a(Lcom/facebook/backstage/ui/VideoView;IIIZ)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    .line 1665871
    iget-object v0, p0, LX/AMB;->a:Lcom/facebook/backstage/ui/VideoView;

    iget-boolean v0, v0, Lcom/facebook/backstage/ui/VideoView;->h:Z

    if-eqz v0, :cond_0

    .line 1665872
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 1665873
    :cond_0
    iget-object v0, p0, LX/AMB;->a:Lcom/facebook/backstage/ui/VideoView;

    iget-object v0, v0, Lcom/facebook/backstage/ui/VideoView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1665874
    return-void
.end method
