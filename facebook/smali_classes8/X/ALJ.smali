.class public final LX/ALJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "LX/5fl;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ALM;


# direct methods
.method public constructor <init>(LX/ALM;)V
    .locals 0

    .prologue
    .line 1664756
    iput-object p1, p0, LX/ALJ;->a:LX/ALM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/5fl;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1664744
    iget-object v0, p0, LX/ALJ;->a:LX/ALM;

    iget-object v0, v0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->x:LX/ALU;

    if-eqz v0, :cond_0

    .line 1664745
    new-instance v0, LX/7gj;

    sget-object v1, LX/7gi;->VIDEO:LX/7gi;

    const/4 v2, 0x0

    .line 1664746
    iget-object v3, p1, LX/5fl;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1664747
    iget-object v5, p0, LX/ALJ;->a:LX/ALM;

    iget-object v5, v5, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v5}, Lcom/facebook/backstage/camera/CameraView;->i(Lcom/facebook/backstage/camera/CameraView;)Z

    move-result v6

    iget-object v5, p0, LX/ALJ;->a:LX/ALM;

    iget-object v5, v5, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v5, v5, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v5}, Lcom/facebook/optic/CameraPreviewView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget-object v7, p0, LX/ALJ;->a:LX/ALM;

    iget-object v7, v7, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v7, v7, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v7}, Lcom/facebook/optic/CameraPreviewView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float v7, v5, v7

    move v5, v4

    invoke-direct/range {v0 .. v7}, LX/7gj;-><init>(LX/7gi;Landroid/graphics/Bitmap;Ljava/lang/String;IIZF)V

    .line 1664748
    iget-object v1, p0, LX/ALJ;->a:LX/ALM;

    iget-object v1, v1, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v2, v1, Lcom/facebook/backstage/camera/CameraView;->e:LX/1Eo;

    iget-object v1, p0, LX/ALJ;->a:LX/ALM;

    iget-object v1, v1, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v1}, Lcom/facebook/backstage/camera/CameraView;->f(Lcom/facebook/backstage/camera/CameraView;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/7gQ;->CAMERA_VIDEO_CAPTURE_FRONT:LX/7gQ;

    :goto_0
    invoke-virtual {v2, v1}, LX/1Eo;->a(LX/7gQ;)V

    .line 1664749
    iget-object v1, p0, LX/ALJ;->a:LX/ALM;

    iget-object v1, v1, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->x:LX/ALU;

    invoke-interface {v1, v0}, LX/ALU;->a(LX/7gj;)V

    .line 1664750
    :cond_0
    return-void

    .line 1664751
    :cond_1
    sget-object v1, LX/7gQ;->CAMERA_VIDEO_CAPTURE:LX/7gQ;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1664752
    iget-object v0, p0, LX/ALJ;->a:LX/ALM;

    iget-object v0, v0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082842

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 1664753
    sget-object v0, Lcom/facebook/backstage/camera/CameraView;->f:Ljava/lang/String;

    const-string v1, "Failed to finish recording a video"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1664754
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1664755
    check-cast p1, LX/5fl;

    invoke-direct {p0, p1}, LX/ALJ;->a(LX/5fl;)V

    return-void
.end method
