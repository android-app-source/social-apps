.class public final LX/8jO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic b:Lcom/facebook/stickers/client/StickerDownloadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1392367
    iput-object p1, p0, LX/8jO;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iput-object p2, p0, LX/8jO;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1392368
    invoke-super {p0}, LX/0Vd;->dispose()V

    .line 1392369
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->b:Ljava/lang/Class;

    const-string v1, "Add sticker pack operation for pack %s cancelled."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/8jO;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1392370
    iget-object v5, v3, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1392371
    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1392372
    iget-object v0, p0, LX/8jO;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, p0, LX/8jO;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-static {v0, v4, v1}, Lcom/facebook/stickers/client/StickerDownloadManager;->a$redex0(Lcom/facebook/stickers/client/StickerDownloadManager;ZLcom/facebook/stickers/model/StickerPack;)V

    .line 1392373
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1392374
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->b:Ljava/lang/Class;

    const-string v1, "Unable to add sticker pack %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/8jO;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1392375
    iget-object v5, v3, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1392376
    aput-object v3, v2, v4

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1392377
    iget-object v0, p0, LX/8jO;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, p0, LX/8jO;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-static {v0, v4, v1}, Lcom/facebook/stickers/client/StickerDownloadManager;->a$redex0(Lcom/facebook/stickers/client/StickerDownloadManager;ZLcom/facebook/stickers/model/StickerPack;)V

    .line 1392378
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1392379
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.orca.stickers.ADD_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1392380
    const-string v1, "stickerPack"

    iget-object v2, p0, LX/8jO;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1392381
    iget-object v1, p0, LX/8jO;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, v1, Lcom/facebook/stickers/client/StickerDownloadManager;->e:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1392382
    iget-object v0, p0, LX/8jO;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, p0, LX/8jO;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1392383
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1392384
    const-string v3, "stickerPack"

    invoke-virtual {v5, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1392385
    iget-object v3, v0, Lcom/facebook/stickers/client/StickerDownloadManager;->c:LX/0aG;

    const-string v4, "download_sticker_pack_assets"

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v7, Lcom/facebook/stickers/client/StickerDownloadManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v8, -0x9833bf2

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    .line 1392386
    new-instance v4, LX/8jP;

    invoke-direct {v4, v0, v1}, LX/8jP;-><init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V

    invoke-interface {v3, v4}, LX/1MF;->setOnProgressListener(LX/4An;)LX/1MF;

    .line 1392387
    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 1392388
    new-instance v4, LX/8jQ;

    invoke-direct {v4, v0, v1}, LX/8jQ;-><init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V

    .line 1392389
    iget-object v5, v0, Lcom/facebook/stickers/client/StickerDownloadManager;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1392390
    iget-object v5, v0, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    .line 1392391
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1392392
    invoke-static {v3, v4}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v3

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1392393
    return-void
.end method
