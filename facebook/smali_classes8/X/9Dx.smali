.class public LX/9Dx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public e:Lcom/facebook/graphql/model/GraphQLComment;

.field public f:LX/9Dw;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Dx;->a:Ljava/util/List;

    .line 1456325
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/9Dx;->b:Ljava/util/Set;

    .line 1456326
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/9Dx;->c:Ljava/util/Set;

    .line 1456327
    const/4 v0, 0x0

    iput v0, p0, LX/9Dx;->d:I

    .line 1456328
    return-void
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1456320
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1456321
    :cond_0
    const/4 v0, 0x0

    .line 1456322
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {p0}, LX/36l;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLName;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
