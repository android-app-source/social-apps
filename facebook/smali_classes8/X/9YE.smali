.class public final LX/9YE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel$AllPhonesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field public y:Z

.field public z:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1504012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)LX/9YE;
    .locals 2

    .prologue
    .line 1504068
    new-instance v0, LX/9YE;

    invoke-direct {v0}, LX/9YE;-><init>()V

    .line 1504069
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->G()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    .line 1504070
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->H()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    .line 1504071
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->d()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->c:LX/0Px;

    .line 1504072
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->d:Z

    .line 1504073
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->gC_()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->e:Z

    .line 1504074
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->gD_()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->f:Z

    .line 1504075
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->j()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->g:Z

    .line 1504076
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->h:Z

    .line 1504077
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->i:Z

    .line 1504078
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->j:Z

    .line 1504079
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->k:Z

    .line 1504080
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->l:Ljava/lang/String;

    .line 1504081
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->m:Z

    .line 1504082
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->I()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1504083
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->o:Ljava/lang/String;

    .line 1504084
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->p:Ljava/lang/String;

    .line 1504085
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->q:Z

    .line 1504086
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->J()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->r:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;

    .line 1504087
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->K()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->s:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1504088
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->w()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->t:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1504089
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->L()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->u:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    .line 1504090
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->M()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->v:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 1504091
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->z()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->w:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1504092
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->x:Z

    .line 1504093
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->B()Z

    move-result v1

    iput-boolean v1, v0, LX/9YE;->y:Z

    .line 1504094
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->z:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1504095
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->D()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->A:Ljava/lang/String;

    .line 1504096
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->B:LX/0Px;

    .line 1504097
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->F()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, v0, LX/9YE;->C:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1504098
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
    .locals 20

    .prologue
    .line 1504013
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1504014
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9YE;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1504015
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9YE;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AdminInfoModel;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1504016
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9YE;->c:LX/0Px;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1504017
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9YE;->l:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1504018
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9YE;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1504019
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9YE;->o:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1504020
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9YE;->p:Ljava/lang/String;

    invoke-virtual {v1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1504021
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9YE;->r:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1504022
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9YE;->s:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1504023
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9YE;->t:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-virtual {v1, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1504024
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9YE;->u:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1504025
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9YE;->v:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1504026
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9YE;->w:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v1, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 1504027
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9YE;->z:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v1, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1504028
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9YE;->A:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1504029
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9YE;->B:LX/0Px;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v17

    .line 1504030
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9YE;->C:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 1504031
    const/16 v19, 0x1d

    move/from16 v0, v19

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1504032
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1504033
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1504034
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1504035
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->d:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504036
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->e:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504037
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->f:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504038
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->g:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504039
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->h:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504040
    const/16 v2, 0x8

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->i:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504041
    const/16 v2, 0x9

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->j:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504042
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->k:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504043
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1504044
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->m:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504045
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1504046
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1504047
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1504048
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->q:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504049
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1504050
    const/16 v2, 0x12

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1504051
    const/16 v2, 0x13

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1504052
    const/16 v2, 0x14

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1504053
    const/16 v2, 0x15

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1504054
    const/16 v2, 0x16

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1504055
    const/16 v2, 0x17

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->x:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504056
    const/16 v2, 0x18

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9YE;->y:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1504057
    const/16 v2, 0x19

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1504058
    const/16 v2, 0x1a

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1504059
    const/16 v2, 0x1b

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1504060
    const/16 v2, 0x1c

    move/from16 v0, v18

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1504061
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1504062
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1504063
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1504064
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1504065
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1504066
    new-instance v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-direct {v2, v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;-><init>(LX/15i;)V

    .line 1504067
    return-object v2
.end method
