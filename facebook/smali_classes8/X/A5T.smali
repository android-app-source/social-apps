.class public LX/A5T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/A5T;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1621444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621445
    iput-object p1, p0, LX/A5T;->a:LX/0Zb;

    .line 1621446
    return-void
.end method

.method public static a(LX/0QB;)LX/A5T;
    .locals 4

    .prologue
    .line 1621447
    sget-object v0, LX/A5T;->b:LX/A5T;

    if-nez v0, :cond_1

    .line 1621448
    const-class v1, LX/A5T;

    monitor-enter v1

    .line 1621449
    :try_start_0
    sget-object v0, LX/A5T;->b:LX/A5T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1621450
    if-eqz v2, :cond_0

    .line 1621451
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1621452
    new-instance p0, LX/A5T;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/A5T;-><init>(LX/0Zb;)V

    .line 1621453
    move-object v0, p0

    .line 1621454
    sput-object v0, LX/A5T;->b:LX/A5T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1621455
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1621456
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1621457
    :cond_1
    sget-object v0, LX/A5T;->b:LX/A5T;

    return-object v0

    .line 1621458
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1621459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1621460
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1621461
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1621462
    move-object v0, v0

    .line 1621463
    const-string v1, "suggestion_source"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1621464
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1621465
    iput-object p5, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1621466
    move-object v0, v0

    .line 1621467
    const-string v1, "tracking_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "is_place_to_people"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "has_initially_tagged_friends"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "has_tag_suggestions"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;Ljava/lang/String;ZZZLjava/util/List;Ljava/lang/String;)V
    .locals 7
    .param p6    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/8QL;",
            ">;",
            "Ljava/lang/String;",
            "ZZZ",
            "Ljava/util/List",
            "<",
            "LX/8vB;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1621468
    iget-object v6, p0, LX/A5T;->a:LX/0Zb;

    const-string v0, "friend_suggestions_done"

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p7

    invoke-static/range {v0 .. v5}, LX/A5T;->a(Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v6, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1621469
    if-eqz p6, :cond_2

    invoke-interface {p6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1621470
    const/4 v1, 0x0

    .line 1621471
    invoke-interface {p6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vB;

    .line 1621472
    iget-object v2, v0, LX/8vB;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1621473
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v3

    move v0, v1

    .line 1621474
    :goto_0
    invoke-interface {p6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1621475
    invoke-interface {p6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1621476
    invoke-virtual {v3, v0}, LX/162;->c(I)LX/162;

    .line 1621477
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1621478
    :cond_1
    invoke-virtual {v3}, LX/0lF;->e()I

    move-result v0

    if-lez v0, :cond_3

    .line 1621479
    iget-object v0, p0, LX/A5T;->a:LX/0Zb;

    const-string v1, "group_tag_suggestions_selected"

    invoke-static {v1, v2, p7}, LX/A5T;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "indices_selected"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1621480
    :cond_2
    :goto_1
    return-void

    .line 1621481
    :cond_3
    iget-object v0, p0, LX/A5T;->a:LX/0Zb;

    const-string v1, "group_tag_suggestions_not_selected"

    invoke-static {v1, v2, p7}, LX/A5T;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1
.end method
