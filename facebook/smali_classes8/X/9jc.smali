.class public final enum LX/9jc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9jc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9jc;

.field public static final enum NEARBY_LOCATION:LX/9jc;

.field public static final enum NEARBY_PLACES:LX/9jc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1530231
    new-instance v0, LX/9jc;

    const-string v1, "NEARBY_LOCATION"

    invoke-direct {v0, v1, v2}, LX/9jc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jc;->NEARBY_LOCATION:LX/9jc;

    .line 1530232
    new-instance v0, LX/9jc;

    const-string v1, "NEARBY_PLACES"

    invoke-direct {v0, v1, v3}, LX/9jc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jc;->NEARBY_PLACES:LX/9jc;

    .line 1530233
    const/4 v0, 0x2

    new-array v0, v0, [LX/9jc;

    sget-object v1, LX/9jc;->NEARBY_LOCATION:LX/9jc;

    aput-object v1, v0, v2

    sget-object v1, LX/9jc;->NEARBY_PLACES:LX/9jc;

    aput-object v1, v0, v3

    sput-object v0, LX/9jc;->$VALUES:[LX/9jc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1530235
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9jc;
    .locals 1

    .prologue
    .line 1530236
    const-class v0, LX/9jc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9jc;

    return-object v0
.end method

.method public static values()[LX/9jc;
    .locals 1

    .prologue
    .line 1530234
    sget-object v0, LX/9jc;->$VALUES:[LX/9jc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9jc;

    return-object v0
.end method
