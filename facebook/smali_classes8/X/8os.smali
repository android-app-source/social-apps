.class public LX/8os;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/tagging/protocol/FetchGroupMembersParams;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:LX/6Wc;

.field public static final b:LX/6Wc;

.field public static final c:LX/6Wc;

.field public static final d:LX/6Wc;

.field public static final e:LX/6Wc;

.field public static final f:LX/6Wc;

.field public static final g:LX/6Wc;

.field public static final h:LX/6Wc;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1404954
    new-instance v0, LX/6Wc;

    const-string v1, "id"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->a:LX/6Wc;

    .line 1404955
    new-instance v0, LX/6Wc;

    const-string v1, "first_name"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->b:LX/6Wc;

    .line 1404956
    new-instance v0, LX/6Wc;

    const-string v1, "gid"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->c:LX/6Wc;

    .line 1404957
    new-instance v0, LX/6Wc;

    const-string v1, "last_name"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->d:LX/6Wc;

    .line 1404958
    new-instance v0, LX/6Wc;

    const-string v1, "name"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->e:LX/6Wc;

    .line 1404959
    new-instance v0, LX/6Wc;

    const-string v1, "pic_square"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->f:LX/6Wc;

    .line 1404960
    new-instance v0, LX/6Wc;

    const-string v1, "type"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->g:LX/6Wc;

    .line 1404961
    new-instance v0, LX/6Wc;

    const-string v1, "uid"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/8os;->h:LX/6Wc;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1404962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1404963
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 13

    .prologue
    .line 1404964
    check-cast p1, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;

    .line 1404965
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1404966
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "query"

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1404967
    invoke-static {}, LX/6Wo;->a()LX/6We;

    move-result-object v6

    new-array v7, v11, [LX/6Wc;

    sget-object v8, LX/8os;->h:LX/6Wc;

    aput-object v8, v7, v10

    invoke-virtual {v6, v7}, LX/6Wd;->a([LX/6Wc;)LX/6Wk;

    move-result-object v6

    sget-object v7, LX/6Wn;->GROUP_MEMBER:LX/6Wn;

    invoke-virtual {v6, v7}, LX/6Wd;->a(LX/6Wn;)LX/6Wi;

    move-result-object v6

    sget-object v7, LX/8os;->c:LX/6Wc;

    iget-wide v8, p1, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->a:J

    .line 1404968
    new-instance v12, LX/6Wg;

    invoke-direct {v12, v8, v9}, LX/6Wg;-><init>(J)V

    invoke-static {v7, v12}, LX/6Wb;->a(LX/6Wb;LX/6Wb;)LX/6Wa;

    move-result-object v12

    move-object v7, v12

    .line 1404969
    invoke-virtual {v6, v7}, LX/6Wd;->a(LX/6WX;)LX/6Wl;

    move-result-object v6

    .line 1404970
    sget-object v7, LX/8os;->h:LX/6Wc;

    invoke-virtual {v7, v6}, LX/6Wb;->a(LX/6Wd;)LX/6Wa;

    move-result-object v6

    .line 1404971
    iget-object v7, p1, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->b:Ljava/lang/String;

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1404972
    check-cast v6, LX/6Wa;

    sget-object v7, LX/8os;->b:LX/6Wc;

    iget-object v8, p1, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->b:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/6Wb;->b(Ljava/lang/String;)LX/6Wa;

    move-result-object v7

    sget-object v8, LX/8os;->d:LX/6Wc;

    iget-object v9, p1, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, LX/6Wb;->b(Ljava/lang/String;)LX/6Wa;

    move-result-object v8

    .line 1404973
    new-instance v9, LX/6Wh;

    invoke-direct {v9, v7, v8}, LX/6Wh;-><init>(LX/6WX;LX/6WX;)V

    move-object v7, v9

    .line 1404974
    invoke-virtual {v6, v7}, LX/6Wa;->a(LX/6WX;)LX/6WZ;

    move-result-object v6

    .line 1404975
    :cond_0
    invoke-static {}, LX/6Wo;->a()LX/6We;

    move-result-object v7

    new-array v8, v11, [LX/6Wc;

    sget-object v9, LX/8os;->h:LX/6Wc;

    aput-object v9, v8, v10

    invoke-virtual {v7, v8}, LX/6Wd;->a([LX/6Wc;)LX/6Wk;

    move-result-object v7

    sget-object v8, LX/6Wn;->USER:LX/6Wn;

    invoke-virtual {v7, v8}, LX/6Wd;->a(LX/6Wn;)LX/6Wi;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/6Wd;->a(LX/6WX;)LX/6Wl;

    move-result-object v6

    .line 1404976
    invoke-static {}, LX/6Wo;->a()LX/6We;

    move-result-object v7

    const/4 v8, 0x4

    new-array v8, v8, [LX/6Wc;

    sget-object v9, LX/8os;->a:LX/6Wc;

    aput-object v9, v8, v10

    sget-object v9, LX/8os;->e:LX/6Wc;

    aput-object v9, v8, v11

    const/4 v9, 0x2

    sget-object v10, LX/8os;->f:LX/6Wc;

    aput-object v10, v8, v9

    const/4 v9, 0x3

    sget-object v10, LX/8os;->g:LX/6Wc;

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, LX/6Wd;->a([LX/6Wc;)LX/6Wk;

    move-result-object v7

    sget-object v8, LX/6Wn;->PROFILE:LX/6Wn;

    invoke-virtual {v7, v8}, LX/6Wd;->a(LX/6Wn;)LX/6Wi;

    move-result-object v7

    sget-object v8, LX/8os;->a:LX/6Wc;

    invoke-virtual {v8, v6}, LX/6Wb;->a(LX/6Wd;)LX/6Wa;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/6Wd;->a(LX/6WX;)LX/6Wl;

    move-result-object v6

    iget v7, p1, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;->c:I

    .line 1404977
    new-instance v8, LX/6Wj;

    invoke-direct {v8, v6}, LX/6Wj;-><init>(LX/6Wd;)V

    .line 1404978
    const-string v9, " LIMIT "

    invoke-static {v8, v9}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1404979
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1404980
    move-object v6, v8

    .line 1404981
    invoke-virtual {v6}, LX/6Wj;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 1404982
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1404983
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "JSON"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1404984
    new-instance v0, LX/14N;

    const-string v1, "groupMembers"

    const-string v2, "GET"

    const-string v3, "method/fql.query"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1404985
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 1404986
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 1404987
    invoke-virtual {v0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1404988
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 1404989
    :cond_0
    const-class v1, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0, v1}, LX/15w;->b(Ljava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
