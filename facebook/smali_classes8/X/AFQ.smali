.class public LX/AFQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/17Y;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0hg;

.field public final d:LX/0gI;


# direct methods
.method public constructor <init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0hg;LX/0gI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647758
    iput-object p1, p0, LX/AFQ;->a:LX/17Y;

    .line 1647759
    iput-object p2, p0, LX/AFQ;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1647760
    iput-object p3, p0, LX/AFQ;->c:LX/0hg;

    .line 1647761
    iput-object p4, p0, LX/AFQ;->d:LX/0gI;

    .line 1647762
    return-void
.end method

.method public static a(LX/0QB;)LX/AFQ;
    .locals 1

    .prologue
    .line 1647756
    invoke-static {p0}, LX/AFQ;->b(LX/0QB;)LX/AFQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1647763
    const v0, 0x7f040016

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1647764
    return-void
.end method

.method public static b(LX/0QB;)LX/AFQ;
    .locals 5

    .prologue
    .line 1647754
    new-instance v4, LX/AFQ;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0hg;->a(LX/0QB;)LX/0hg;

    move-result-object v2

    check-cast v2, LX/0hg;

    invoke-static {p0}, LX/0gI;->a(LX/0QB;)LX/0gI;

    move-result-object v3

    check-cast v3, LX/0gI;

    invoke-direct {v4, v0, v1, v2, v3}, LX/AFQ;-><init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0hg;LX/0gI;)V

    .line 1647755
    return-object v4
.end method


# virtual methods
.method public final a(LX/AFW;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 1647710
    iget-object v0, p1, LX/AFW;->a:LX/7h0;

    move-object v1, v0

    .line 1647711
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "item can\'t be null"

    invoke-static {v0, v2}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1647712
    iget-object v0, v1, LX/7h0;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1647713
    iget-object v1, p1, LX/AFW;->b:LX/0Px;

    move-object v1, v1

    .line 1647714
    sget-object v2, LX/0ax;->gv:Ljava/lang/String;

    const-string v3, "{id}"

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 1647715
    iget-object v3, p0, LX/AFQ;->a:LX/17Y;

    invoke-interface {v3, p2, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1647716
    const-string v3, "extra_reply_threads"

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1647717
    move-object v1, v2

    .line 1647718
    iget-object v0, p1, LX/AFW;->a:LX/7h0;

    move-object v0, v0

    .line 1647719
    iget-boolean v2, v0, LX/7h0;->l:Z

    move v0, v2

    .line 1647720
    if-eqz v0, :cond_1

    .line 1647721
    const/4 v2, 0x1

    .line 1647722
    invoke-static {}, Lcom/facebook/audience/model/ReplyThreadConfig;->newBuilder()LX/7gu;

    move-result-object v0

    .line 1647723
    iput-boolean v2, v0, LX/7gu;->b:Z

    .line 1647724
    move-object v0, v0

    .line 1647725
    iput-boolean v2, v0, LX/7gu;->c:Z

    .line 1647726
    move-object v0, v0

    .line 1647727
    const/4 v2, 0x0

    .line 1647728
    iput-boolean v2, v0, LX/7gu;->a:Z

    .line 1647729
    move-object v0, v0

    .line 1647730
    sget-object v2, LX/7gw;->DIRECT:LX/7gw;

    .line 1647731
    iput-object v2, v0, LX/7gu;->d:LX/7gw;

    .line 1647732
    move-object v0, v0

    .line 1647733
    invoke-virtual {v0}, LX/7gu;->a()Lcom/facebook/audience/model/ReplyThreadConfig;

    move-result-object v0

    move-object v0, v0

    .line 1647734
    :goto_1
    const-string v2, "extra_reply_thread_config"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1647735
    const-class v0, Landroid/app/Activity;

    invoke-static {p2, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1647736
    iget-object v2, p0, LX/AFQ;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1647737
    invoke-static {v0}, LX/AFQ;->a(Landroid/app/Activity;)V

    .line 1647738
    return-void

    .line 1647739
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1647740
    :cond_1
    const/4 v3, 0x0

    .line 1647741
    invoke-static {}, Lcom/facebook/audience/model/ReplyThreadConfig;->newBuilder()LX/7gu;

    move-result-object v0

    .line 1647742
    iput-boolean v3, v0, LX/7gu;->b:Z

    .line 1647743
    move-object v0, v0

    .line 1647744
    const/4 v2, 0x1

    .line 1647745
    iput-boolean v2, v0, LX/7gu;->c:Z

    .line 1647746
    move-object v0, v0

    .line 1647747
    iput-boolean v3, v0, LX/7gu;->a:Z

    .line 1647748
    move-object v0, v0

    .line 1647749
    sget-object v2, LX/7gw;->DIRECT:LX/7gw;

    .line 1647750
    iput-object v2, v0, LX/7gu;->d:LX/7gw;

    .line 1647751
    move-object v0, v0

    .line 1647752
    invoke-virtual {v0}, LX/7gu;->a()Lcom/facebook/audience/model/ReplyThreadConfig;

    move-result-object v0

    move-object v0, v0

    .line 1647753
    goto :goto_1
.end method
