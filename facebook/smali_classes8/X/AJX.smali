.class public LX/AJX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:[I

.field private static final g:Ljava/lang/Object;


# instance fields
.field private final c:LX/0tX;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:[Z

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1661224
    const-class v0, LX/AJX;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AJX;->a:Ljava/lang/String;

    .line 1661225
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/AJX;->b:[I

    .line 1661226
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/AJX;->g:Ljava/lang/Object;

    return-void

    .line 1661227
    :array_0
    .array-data 4
        0x14
        0x1388
    .end array-data
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1661274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1661275
    sget-object v0, LX/AJX;->b:[I

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, LX/AJX;->e:[Z

    .line 1661276
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1661277
    iput-object v0, p0, LX/AJX;->f:LX/0Px;

    .line 1661278
    iput-object p1, p0, LX/AJX;->c:LX/0tX;

    .line 1661279
    iput-object p2, p0, LX/AJX;->d:Ljava/util/concurrent/Executor;

    .line 1661280
    return-void
.end method

.method public static a(LX/0QB;)LX/AJX;
    .locals 8

    .prologue
    .line 1661245
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1661246
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1661247
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1661248
    if-nez v1, :cond_0

    .line 1661249
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1661250
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1661251
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1661252
    sget-object v1, LX/AJX;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1661253
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1661254
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1661255
    :cond_1
    if-nez v1, :cond_4

    .line 1661256
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1661257
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1661258
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1661259
    new-instance p0, LX/AJX;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v1, v7}, LX/AJX;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    .line 1661260
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1661261
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1661262
    if-nez v1, :cond_2

    .line 1661263
    sget-object v0, LX/AJX;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJX;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1661264
    :goto_1
    if-eqz v0, :cond_3

    .line 1661265
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1661266
    :goto_3
    check-cast v0, LX/AJX;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1661267
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1661268
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1661269
    :catchall_1
    move-exception v0

    .line 1661270
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1661271
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1661272
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1661273
    :cond_2
    :try_start_8
    sget-object v0, LX/AJX;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJX;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/AJX;LX/0zS;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zS;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/16 v3, 0x64

    .line 1661236
    new-instance v0, LX/AJa;

    invoke-direct {v0}, LX/AJa;-><init>()V

    move-object v0, v0

    .line 1661237
    const-string v1, "0"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1661238
    const-string v1, "1"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1661239
    const-string v1, "2"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1661240
    const-string v1, "3"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1661241
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1661242
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1661243
    move-object v0, v0

    .line 1661244
    iget-object v1, p0, LX/AJX;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/AJX;ZLX/AJF;I)V
    .locals 3

    .prologue
    .line 1661233
    if-eqz p1, :cond_0

    sget-object v0, LX/0zS;->b:LX/0zS;

    :goto_0
    sget-object v1, LX/AJX;->b:[I

    aget v1, v1, p3

    invoke-static {p0, v0, v1}, LX/AJX;->a(LX/AJX;LX/0zS;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/AJW;

    invoke-direct {v1, p0, p3, p2}, LX/AJW;-><init>(LX/AJX;ILX/AJF;)V

    iget-object v2, p0, LX/AJX;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1661234
    return-void

    .line 1661235
    :cond_0
    sget-object v0, LX/0zS;->d:LX/0zS;

    goto :goto_0
.end method

.method public static c(LX/AJX;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1661228
    iget-object v2, p0, LX/AJX;->e:[Z

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-boolean v4, v2, v1

    .line 1661229
    if-nez v4, :cond_0

    .line 1661230
    :goto_1
    return v0

    .line 1661231
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1661232
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method
