.class public LX/8ov;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:LX/1zC;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;LX/1zC;)V
    .locals 0

    .prologue
    .line 1405011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405012
    iput-object p1, p0, LX/8ov;->a:Landroid/widget/EditText;

    .line 1405013
    iput-object p2, p0, LX/8ov;->b:LX/1zC;

    .line 1405014
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1405015
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1405016
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1405017
    if-lez p4, :cond_0

    instance-of v0, p1, Landroid/text/Editable;

    if-eqz v0, :cond_0

    .line 1405018
    iget-object v0, p0, LX/8ov;->b:LX/1zC;

    check-cast p1, Landroid/text/Editable;

    iget-object v1, p0, LX/8ov;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTextSize()F

    move-result v1

    float-to-int v1, v1

    .line 1405019
    new-instance p0, LX/1zS;

    invoke-direct {p0}, LX/1zS;-><init>()V

    .line 1405020
    iput v1, p0, LX/1zS;->a:I

    .line 1405021
    iput p2, p0, LX/1zS;->b:I

    .line 1405022
    iput p4, p0, LX/1zS;->c:I

    .line 1405023
    const/4 p3, 0x0

    iput-boolean p3, p0, LX/1zS;->d:Z

    .line 1405024
    const/16 p3, 0x1

    if-eqz p3, :cond_1

    .line 1405025
    invoke-static {v0, p1, p0}, LX/1zC;->a(LX/1zC;Landroid/text/Editable;LX/1zS;)Z

    .line 1405026
    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0, p1, p0}, LX/1zC;->b(LX/1zC;Landroid/text/Editable;LX/1zS;)Z

    goto :goto_0
.end method
