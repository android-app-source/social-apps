.class public final enum LX/8uu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8uu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8uu;

.field public static final enum DIALPAD:LX/8uu;

.field public static final enum TEXT_NO_SUGGESTIONS:LX/8uu;


# instance fields
.field public final drawableResourceId:I

.field public final inputTypeFlags:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1415829
    new-instance v0, LX/8uu;

    const-string v1, "TEXT_NO_SUGGESTIONS"

    const v2, 0xa0001

    const v3, 0x7f020d91

    invoke-direct {v0, v1, v4, v2, v3}, LX/8uu;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/8uu;->TEXT_NO_SUGGESTIONS:LX/8uu;

    .line 1415830
    new-instance v0, LX/8uu;

    const-string v1, "DIALPAD"

    const v2, 0x20003

    const v3, 0x7f020d90

    invoke-direct {v0, v1, v5, v2, v3}, LX/8uu;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/8uu;->DIALPAD:LX/8uu;

    .line 1415831
    const/4 v0, 0x2

    new-array v0, v0, [LX/8uu;

    sget-object v1, LX/8uu;->TEXT_NO_SUGGESTIONS:LX/8uu;

    aput-object v1, v0, v4

    sget-object v1, LX/8uu;->DIALPAD:LX/8uu;

    aput-object v1, v0, v5

    sput-object v0, LX/8uu;->$VALUES:[LX/8uu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1415825
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1415826
    iput p3, p0, LX/8uu;->inputTypeFlags:I

    .line 1415827
    iput p4, p0, LX/8uu;->drawableResourceId:I

    .line 1415828
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8uu;
    .locals 1

    .prologue
    .line 1415824
    const-class v0, LX/8uu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8uu;

    return-object v0
.end method

.method public static values()[LX/8uu;
    .locals 1

    .prologue
    .line 1415823
    sget-object v0, LX/8uu;->$VALUES:[LX/8uu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uu;

    return-object v0
.end method
