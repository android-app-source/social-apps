.class public final LX/95R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/AutoCloseable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi",
        "ImprovedNewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/AutoCloseable;"
    }
.end annotation


# instance fields
.field private final a:LX/95S;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95S",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/95S;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95S",
            "<TTEdge;TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 1436857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436858
    iput-object p1, p0, LX/95R;->a:LX/95S;

    .line 1436859
    return-void
.end method


# virtual methods
.method public final a()LX/2kW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation

    .prologue
    .line 1436860
    iget-object v0, p0, LX/95R;->a:LX/95S;

    invoke-virtual {v0}, LX/95S;->b()LX/2kW;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1436861
    iget-object v0, p0, LX/95R;->a:LX/95S;

    invoke-virtual {v0, p0}, LX/95S;->a(LX/95R;)V

    .line 1436862
    return-void
.end method
