.class public final LX/9m2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1534687
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1534688
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1534689
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1534690
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1534691
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1534692
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1534693
    :goto_1
    move v1, v2

    .line 1534694
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1534695
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1534696
    :cond_1
    const-string v10, "is_optional"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1534697
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    .line 1534698
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_8

    .line 1534699
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1534700
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1534701
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_2

    if-eqz v9, :cond_2

    .line 1534702
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1534703
    :cond_3
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_2

    .line 1534704
    :cond_4
    const-string v10, "message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1534705
    const/4 v9, 0x0

    .line 1534706
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v10, :cond_10

    .line 1534707
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1534708
    :goto_3
    move v6, v9

    .line 1534709
    goto :goto_2

    .line 1534710
    :cond_5
    const-string v10, "message_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1534711
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 1534712
    :cond_6
    const-string v10, "placeholder_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1534713
    const/4 v9, 0x0

    .line 1534714
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v10, :cond_14

    .line 1534715
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1534716
    :goto_4
    move v4, v9

    .line 1534717
    goto :goto_2

    .line 1534718
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1534719
    :cond_8
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1534720
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1534721
    if-eqz v1, :cond_9

    .line 1534722
    invoke-virtual {p1, v3, v7}, LX/186;->a(IZ)V

    .line 1534723
    :cond_9
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1534724
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1534725
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1534726
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_2

    .line 1534727
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1534728
    :cond_c
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_f

    .line 1534729
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1534730
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1534731
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_c

    if-eqz v11, :cond_c

    .line 1534732
    const-string v12, "ranges"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 1534733
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1534734
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_d

    .line 1534735
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_d

    .line 1534736
    invoke-static {p0, p1}, LX/9m1;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1534737
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1534738
    :cond_d
    invoke-static {v10, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v10

    move v10, v10

    .line 1534739
    goto :goto_5

    .line 1534740
    :cond_e
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1534741
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_5

    .line 1534742
    :cond_f
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1534743
    invoke-virtual {p1, v9, v10}, LX/186;->b(II)V

    .line 1534744
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v6}, LX/186;->b(II)V

    .line 1534745
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_3

    :cond_10
    move v6, v9

    move v10, v9

    goto :goto_5

    .line 1534746
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1534747
    :cond_12
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_13

    .line 1534748
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1534749
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1534750
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_12

    if-eqz v10, :cond_12

    .line 1534751
    const-string v11, "text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 1534752
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_7

    .line 1534753
    :cond_13
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1534754
    invoke-virtual {p1, v9, v4}, LX/186;->b(II)V

    .line 1534755
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_4

    :cond_14
    move v4, v9

    goto :goto_7
.end method
