.class public LX/9H3;
.super LX/9Gx;
.source ""


# instance fields
.field public b:Landroid/view/View;

.field public c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1460802
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9H3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1460803
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460804
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/9H3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460805
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460806
    invoke-direct {p0, p1, p2, p3}, LX/9Gx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460807
    const v0, 0x7f0302d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1460808
    const v0, 0x7f0d09d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9H3;->b:Landroid/view/View;

    .line 1460809
    const v0, 0x7f0d09d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9H3;->c:Landroid/view/View;

    .line 1460810
    return-void
.end method
