.class public LX/9jZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/9jZ;


# instance fields
.field public final a:LX/0SG;

.field private final b:Landroid/content/res/Resources;

.field public final c:LX/9jj;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/content/res/Resources;LX/9jj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529987
    iput-object p1, p0, LX/9jZ;->a:LX/0SG;

    .line 1529988
    iput-object p2, p0, LX/9jZ;->b:Landroid/content/res/Resources;

    .line 1529989
    iput-object p3, p0, LX/9jZ;->c:LX/9jj;

    .line 1529990
    return-void
.end method

.method public static a(LX/0QB;)LX/9jZ;
    .locals 6

    .prologue
    .line 1529991
    sget-object v0, LX/9jZ;->d:LX/9jZ;

    if-nez v0, :cond_1

    .line 1529992
    const-class v1, LX/9jZ;

    monitor-enter v1

    .line 1529993
    :try_start_0
    sget-object v0, LX/9jZ;->d:LX/9jZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1529994
    if-eqz v2, :cond_0

    .line 1529995
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1529996
    new-instance p0, LX/9jZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/9jj;->b(LX/0QB;)LX/9jj;

    move-result-object v5

    check-cast v5, LX/9jj;

    invoke-direct {p0, v3, v4, v5}, LX/9jZ;-><init>(LX/0SG;Landroid/content/res/Resources;LX/9jj;)V

    .line 1529997
    move-object v0, p0

    .line 1529998
    sput-object v0, LX/9jZ;->d:LX/9jZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1529999
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1530000
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1530001
    :cond_1
    sget-object v0, LX/9jZ;->d:LX/9jZ;

    return-object v0

    .line 1530002
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1530003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/9jo;)LX/0zO;
    .locals 12

    .prologue
    .line 1530004
    new-instance v2, LX/4DI;

    invoke-direct {v2}, LX/4DI;-><init>()V

    .line 1530005
    iget-object v3, p1, LX/9jo;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1530006
    invoke-virtual {v2, v3}, LX/4DI;->a(Ljava/lang/String;)LX/4DI;

    .line 1530007
    iget-object v6, p1, LX/9jo;->b:Landroid/location/Location;

    move-object v8, v6

    .line 1530008
    if-eqz v8, :cond_1

    .line 1530009
    new-instance v6, LX/3Aj;

    invoke-direct {v6}, LX/3Aj;-><init>()V

    invoke-virtual {v8}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    invoke-virtual {v8}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    invoke-virtual {v8}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    move-result-object v9

    .line 1530010
    iget-boolean v6, p1, LX/9jo;->d:Z

    move v6, v6

    .line 1530011
    if-eqz v6, :cond_5

    const-wide/16 v6, 0x0

    :goto_0
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/3Aj;->e(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    .line 1530012
    invoke-virtual {v8}, Landroid/location/Location;->hasSpeed()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1530013
    invoke-virtual {v8}, Landroid/location/Location;->getSpeed()F

    move-result v7

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 1530014
    :cond_0
    invoke-virtual {v2, v6}, LX/4DI;->a(LX/3Aj;)LX/4DI;

    .line 1530015
    :cond_1
    iget-object v3, p1, LX/9jo;->g:LX/9jC;

    move-object v3, v3

    .line 1530016
    invoke-virtual {v3}, LX/9jC;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1530017
    const-string v4, "composer_entrypoint"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530018
    iget-object v3, p1, LX/9jo;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1530019
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1530020
    iget-object v3, p0, LX/9jZ;->c:LX/9jj;

    invoke-virtual {v3, v2}, LX/9jj;->a(LX/4DI;)Z

    .line 1530021
    :cond_2
    invoke-static {}, LX/5m3;->a()LX/5lz;

    move-result-object v3

    .line 1530022
    const-string v4, "query"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    const-string v4, "num_results"

    const/16 v5, 0xf

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v4, "search_context"

    .line 1530023
    sget-object v5, LX/9jY;->a:[I

    .line 1530024
    iget-object v6, p1, LX/9jo;->c:LX/9jG;

    move-object v6, v6

    .line 1530025
    invoke-virtual {v6}, LX/9jG;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1530026
    const-string v5, "CHECKIN"

    :goto_1
    move-object v5, v5

    .line 1530027
    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v4, "fetch_address"

    const-string v5, "true"

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1530028
    iget-object v2, p1, LX/9jo;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1530029
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1530030
    const-string v2, "story_id"

    .line 1530031
    iget-object v4, p1, LX/9jo;->e:Ljava/lang/String;

    move-object v4, v4

    .line 1530032
    invoke-virtual {v3, v2, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1530033
    :cond_3
    iget-object v2, p1, LX/9jo;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1530034
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1530035
    const-string v2, "comment_id"

    .line 1530036
    iget-object v4, p1, LX/9jo;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1530037
    invoke-virtual {v3, v2, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1530038
    :cond_4
    move-object v0, v3

    .line 1530039
    invoke-static {}, LX/5m3;->a()LX/5lz;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1530040
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 1530041
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    return-object v0

    .line 1530042
    :cond_5
    iget-object v6, p0, LX/9jZ;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v8}, Landroid/location/Location;->getTime()J

    move-result-wide v10

    sub-long/2addr v6, v10

    long-to-double v6, v6

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v6, v10

    const-wide v10, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v10

    goto/16 :goto_0

    .line 1530043
    :pswitch_0
    const-string v5, "COMPOSER"

    goto :goto_1

    .line 1530044
    :pswitch_1
    const-string v5, "FORSALE_POST"

    goto :goto_1

    .line 1530045
    :pswitch_2
    const-string v5, "NON_GEOHUB_PLACES"

    goto :goto_1

    .line 1530046
    :pswitch_3
    const-string v5, "SOCIAL_SEARCH_COMMENT"

    goto :goto_1

    .line 1530047
    :pswitch_4
    const-string v5, "SOCIAL_SEARCH_CONVERSION"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
