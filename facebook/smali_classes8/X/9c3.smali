.class public LX/9c3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/9c1;

.field private static volatile e:LX/9c3;


# instance fields
.field public final b:LX/11i;

.field public final c:LX/0So;

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1515783
    new-instance v0, LX/9c1;

    invoke-direct {v0}, LX/9c1;-><init>()V

    sput-object v0, LX/9c3;->a:LX/9c1;

    return-void
.end method

.method public constructor <init>(LX/11i;LX/0So;LX/0W3;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1515784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1515785
    iput-object p1, p0, LX/9c3;->b:LX/11i;

    .line 1515786
    iput-object p2, p0, LX/9c3;->c:LX/0So;

    .line 1515787
    sget-wide v0, LX/0X5;->ie:J

    const/4 v2, 0x4

    invoke-interface {p3, v0, v1, v2}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/9c3;->d:I

    .line 1515788
    return-void
.end method

.method public static a(LX/0QB;)LX/9c3;
    .locals 6

    .prologue
    .line 1515789
    sget-object v0, LX/9c3;->e:LX/9c3;

    if-nez v0, :cond_1

    .line 1515790
    const-class v1, LX/9c3;

    monitor-enter v1

    .line 1515791
    :try_start_0
    sget-object v0, LX/9c3;->e:LX/9c3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1515792
    if-eqz v2, :cond_0

    .line 1515793
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1515794
    new-instance p0, LX/9c3;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/9c3;-><init>(LX/11i;LX/0So;LX/0W3;)V

    .line 1515795
    move-object v0, p0

    .line 1515796
    sput-object v0, LX/9c3;->e:LX/9c3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1515797
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1515798
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1515799
    :cond_1
    sget-object v0, LX/9c3;->e:LX/9c3;

    return-object v0

    .line 1515800
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1515801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/9c2;)V
    .locals 3

    .prologue
    .line 1515802
    iget-object v0, p0, LX/9c3;->b:LX/11i;

    sget-object v1, LX/9c3;->a:LX/9c1;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 1515803
    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 1515804
    :cond_0
    :goto_0
    return-void

    .line 1515805
    :cond_1
    iget-object v1, p2, LX/9c2;->name:Ljava/lang/String;

    const v2, -0xef7167

    invoke-static {v0, v1, v2}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 1515806
    iget-object v0, p0, LX/9c3;->b:LX/11i;

    sget-object v1, LX/9c3;->a:LX/9c1;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 1515807
    if-nez v0, :cond_0

    .line 1515808
    :goto_0
    return-void

    .line 1515809
    :cond_0
    const-string v0, "numOfAttachments"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "composerSessionId"

    const-string v4, "failed"

    const-string v5, "false"

    move-object v3, p1

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    .line 1515810
    iget-object v0, p0, LX/9c3;->b:LX/11i;

    sget-object v1, LX/9c3;->a:LX/9c1;

    iget-object v2, p0, LX/9c3;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 1515811
    iget-object v0, p0, LX/9c3;->b:LX/11i;

    sget-object v1, LX/9c3;->a:LX/9c1;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 1515812
    if-nez v0, :cond_0

    .line 1515813
    :goto_0
    return-void

    .line 1515814
    :cond_0
    const-string v0, "numOfAttachments"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "composerSessionId"

    const-string v4, "cancelled"

    const-string v5, "true"

    move-object v3, p1

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    .line 1515815
    iget-object v0, p0, LX/9c3;->b:LX/11i;

    sget-object v1, LX/9c3;->a:LX/9c1;

    iget-object v2, p0, LX/9c3;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    goto :goto_0
.end method
