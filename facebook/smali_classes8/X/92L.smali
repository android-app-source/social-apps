.class public final LX/92L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;",
        ">;",
        "LX/92n;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/92N;


# direct methods
.method public constructor <init>(LX/92N;)V
    .locals 0

    .prologue
    .line 1432249
    iput-object p1, p0, LX/92L;->a:LX/92N;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1432231
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1432232
    iget-object v0, p0, LX/92L;->a:LX/92N;

    .line 1432233
    if-nez p1, :cond_0

    .line 1432234
    const/4 v1, 0x0

    .line 1432235
    :goto_0
    move-object v0, v1

    .line 1432236
    return-object v0

    .line 1432237
    :cond_0
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1432238
    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;

    .line 1432239
    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;->a()LX/0Px;

    move-result-object v1

    new-instance v2, LX/92M;

    invoke-direct {v2, v0}, LX/92M;-><init>(LX/92N;)V

    invoke-static {v1, v2}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v2

    .line 1432240
    new-instance v1, LX/92n;

    .line 1432241
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1432242
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1432243
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1432244
    new-instance v2, LX/92e;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/5LG;

    invoke-direct {v2, p0}, LX/92e;-><init>(LX/5LG;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1432245
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    move-object v3, p0

    .line 1432246
    move-object v2, v3

    .line 1432247
    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 1432248
    invoke-direct {v1, v2, v3}, LX/92n;-><init>(LX/0Px;LX/0ta;)V

    goto :goto_0
.end method
