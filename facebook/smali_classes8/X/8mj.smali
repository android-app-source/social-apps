.class public final LX/8mj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/1Ai;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/33B;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:I

.field public final f:Z

.field public final g:Z

.field public final h:Z


# direct methods
.method public constructor <init>(LX/8mi;)V
    .locals 1

    .prologue
    .line 1399739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1399740
    iget-object v0, p1, LX/8mi;->f:Ljava/lang/String;

    iput-object v0, p0, LX/8mj;->a:Ljava/lang/String;

    .line 1399741
    iget v0, p1, LX/8mi;->b:I

    iput v0, p0, LX/8mj;->e:I

    .line 1399742
    iget-boolean v0, p1, LX/8mi;->c:Z

    iput-boolean v0, p0, LX/8mj;->h:Z

    .line 1399743
    iget-boolean v0, p1, LX/8mi;->d:Z

    iput-boolean v0, p0, LX/8mj;->f:Z

    .line 1399744
    iget-boolean v0, p1, LX/8mi;->e:Z

    iput-boolean v0, p0, LX/8mj;->g:Z

    .line 1399745
    iget-object v0, p1, LX/8mi;->g:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, LX/8mj;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1399746
    iget-object v0, p1, LX/8mi;->h:LX/1Ai;

    iput-object v0, p0, LX/8mj;->c:LX/1Ai;

    .line 1399747
    iget-object v0, p1, LX/8mi;->i:LX/33B;

    iput-object v0, p0, LX/8mj;->d:LX/33B;

    .line 1399748
    return-void
.end method
