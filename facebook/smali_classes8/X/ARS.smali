.class public LX/ARS;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# instance fields
.field public final a:LX/AR2;

.field public final b:LX/AR7;

.field public final c:LX/0Uh;

.field public final d:LX/BJ7;


# direct methods
.method public constructor <init>(LX/B5j;Landroid/content/Context;LX/AR2;LX/AR7;LX/0Uh;LX/BJ7;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Landroid/content/Context;",
            "LX/AR2;",
            "LX/AR7;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/BJ7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672950
    invoke-direct {p0, p2, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1672951
    iput-object p3, p0, LX/ARS;->a:LX/AR2;

    .line 1672952
    iput-object p4, p0, LX/ARS;->b:LX/AR7;

    .line 1672953
    iput-object p5, p0, LX/ARS;->c:LX/0Uh;

    .line 1672954
    iput-object p6, p0, LX/ARS;->d:LX/BJ7;

    .line 1672955
    return-void
.end method


# virtual methods
.method public final V()LX/ARN;
    .locals 1

    .prologue
    .line 1672949
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final Z()LX/ARN;
    .locals 1

    .prologue
    .line 1672948
    new-instance v0, LX/ARP;

    invoke-direct {v0, p0}, LX/ARP;-><init>(LX/ARS;)V

    return-object v0
.end method

.method public final aC()LX/ARN;
    .locals 1

    .prologue
    .line 1672947
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1

    .prologue
    .line 1672946
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1

    .prologue
    .line 1672945
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ad()LX/ARN;
    .locals 1

    .prologue
    .line 1672944
    new-instance v0, LX/ARQ;

    invoke-direct {v0, p0}, LX/ARQ;-><init>(LX/ARS;)V

    return-object v0
.end method

.method public final ae()LX/ARN;
    .locals 1

    .prologue
    .line 1672937
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final af()LX/ARN;
    .locals 1

    .prologue
    .line 1672943
    new-instance v0, LX/ARO;

    invoke-direct {v0, p0}, LX/ARO;-><init>(LX/ARS;)V

    return-object v0
.end method

.method public final ag()LX/ARN;
    .locals 1

    .prologue
    .line 1672942
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aj()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1672941
    new-instance v0, LX/ARM;

    invoke-direct {v0, p0}, LX/ARM;-><init>(LX/ARS;)V

    return-object v0
.end method

.method public final al()LX/ARN;
    .locals 1

    .prologue
    .line 1672940
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1

    .prologue
    .line 1672939
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final i()LX/ARN;
    .locals 1

    .prologue
    .line 1672938
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method
