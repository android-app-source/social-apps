.class public final LX/95e;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1437129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437130
    return-void
.end method

.method public static a(LX/2nj;LX/3DP;LX/5Mb;Ljava/lang/String;)Lcom/facebook/graphql/cursor/edgestore/PageInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2nj;",
            "LX/3DP;",
            "LX/5Mb",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/graphql/cursor/edgestore/PageInfo;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1437131
    iget-object v1, p2, LX/5Mb;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p2, LX/5Mb;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p2, LX/5Mb;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1437132
    if-eqz v1, :cond_0

    .line 1437133
    const/4 v0, 0x0

    .line 1437134
    :goto_1
    return-object v0

    .line 1437135
    :cond_0
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    .line 1437136
    iget-object v2, p0, LX/2nj;->c:LX/2nk;

    move-object v2, v2

    .line 1437137
    if-ne v1, v2, :cond_1

    .line 1437138
    const/4 v3, 0x1

    .line 1437139
    iget-boolean v0, p2, LX/5Mb;->f:Z

    move v4, v0

    .line 1437140
    :goto_2
    iget-object v0, p2, LX/5Mb;->c:Ljava/lang/String;

    move-object v1, v0

    .line 1437141
    iget-object v0, p2, LX/5Mb;->d:Ljava/lang/String;

    move-object v2, v0

    .line 1437142
    iget-object v0, p2, LX/5Mb;->b:LX/0Px;

    move-object v0, v0

    .line 1437143
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v0

    goto :goto_1

    .line 1437144
    :cond_1
    sget-object v1, LX/3DP;->FIRST:LX/3DP;

    if-ne v1, p1, :cond_2

    .line 1437145
    iget-boolean v1, p2, LX/5Mb;->f:Z

    move v4, v1

    .line 1437146
    move v3, v0

    goto :goto_2

    .line 1437147
    :cond_2
    sget-object v1, LX/3DP;->LAST:LX/3DP;

    if-ne v1, p1, :cond_3

    .line 1437148
    iget-boolean v1, p2, LX/5Mb;->e:Z

    move v3, v1

    .line 1437149
    move v4, v0

    .line 1437150
    goto :goto_2

    .line 1437151
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported Order:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/3DP;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/2nj;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1437152
    sget-object v0, LX/95d;->a:[I

    .line 1437153
    iget-object v1, p0, LX/2nj;->c:LX/2nk;

    move-object v1, v1

    .line 1437154
    invoke-virtual {v1}, LX/2nk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1437155
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1437156
    :pswitch_0
    invoke-static {p1, p2}, LX/2nU;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1437157
    :goto_0
    return-object v0

    .line 1437158
    :pswitch_1
    check-cast p0, LX/2nl;

    .line 1437159
    iget-object v0, p0, LX/2nl;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1437160
    invoke-static {v0}, LX/2nU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1437161
    :pswitch_2
    check-cast p0, LX/2nl;

    .line 1437162
    iget-object v0, p0, LX/2nl;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1437163
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x18

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1437164
    invoke-static {v0}, LX/2nU;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1437165
    invoke-static {v0}, LX/2nU;->e(Ljava/lang/String;)I

    move-result v2

    .line 1437166
    const-string p0, "%s%08x"

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1437167
    goto :goto_0

    .line 1437168
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
