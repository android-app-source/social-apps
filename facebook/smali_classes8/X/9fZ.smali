.class public LX/9fZ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/photos/editgallery/TextEditController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1522349
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1522350
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Ljava/lang/String;Landroid/widget/FrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/9fP;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/9f8;LX/0am;)Lcom/facebook/photos/editgallery/TextEditController;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/ui/titlebar/Fb4aTitleBar;",
            "Ljava/lang/String;",
            "Landroid/widget/FrameLayout;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Ljava/lang/String;",
            "LX/9fP;",
            "Lcom/facebook/photos/editgallery/RotatingPhotoViewController;",
            "LX/9f8;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;)",
            "Lcom/facebook/photos/editgallery/TextEditController;"
        }
    .end annotation

    .prologue
    .line 1522351
    new-instance v0, Lcom/facebook/photos/editgallery/TextEditController;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v11

    check-cast v11, LX/1Ad;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    invoke-static {p0}, LX/8GT;->a(LX/0QB;)LX/8GT;

    move-result-object v13

    check-cast v13, LX/8GT;

    invoke-static {p0}, LX/8Gc;->b(LX/0QB;)LX/8Gc;

    move-result-object v14

    check-cast v14, LX/8Gc;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v14}, Lcom/facebook/photos/editgallery/TextEditController;-><init>(Landroid/net/Uri;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Ljava/lang/String;Landroid/widget/FrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Ljava/lang/String;LX/9fP;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/9f8;LX/0am;LX/1Ad;Landroid/content/Context;LX/8GT;LX/8Gc;)V

    .line 1522352
    return-object v0
.end method
