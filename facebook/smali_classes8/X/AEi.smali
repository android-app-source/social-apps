.class public LX/AEi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0kL;

.field private final c:LX/0Zb;

.field private final d:LX/17Q;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final g:LX/B9y;

.field private final h:LX/14x;


# direct methods
.method public constructor <init>(LX/0Or;LX/0kL;LX/B9y;Lcom/facebook/intent/feed/IFeedIntentBuilder;Landroid/content/Context;LX/17Q;LX/0Zb;LX/14x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0kL;",
            "LX/B9y;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "Landroid/content/Context;",
            "LX/17Q;",
            "LX/0Zb;",
            "LX/14x;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1646979
    iput-object p1, p0, LX/AEi;->a:LX/0Or;

    .line 1646980
    iput-object p2, p0, LX/AEi;->b:LX/0kL;

    .line 1646981
    iput-object p3, p0, LX/AEi;->g:LX/B9y;

    .line 1646982
    iput-object p4, p0, LX/AEi;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1646983
    iput-object p5, p0, LX/AEi;->e:Landroid/content/Context;

    .line 1646984
    iput-object p6, p0, LX/AEi;->d:LX/17Q;

    .line 1646985
    iput-object p7, p0, LX/AEi;->c:LX/0Zb;

    .line 1646986
    iput-object p8, p0, LX/AEi;->h:LX/14x;

    .line 1646987
    return-void
.end method

.method public static a(LX/0QB;)LX/AEi;
    .locals 12

    .prologue
    .line 1646988
    const-class v1, LX/AEi;

    monitor-enter v1

    .line 1646989
    :try_start_0
    sget-object v0, LX/AEi;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646990
    sput-object v2, LX/AEi;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646991
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646992
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646993
    new-instance v3, LX/AEi;

    const/16 v4, 0x19e

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {v0}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v6

    check-cast v6, LX/B9y;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v9

    check-cast v9, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v11

    check-cast v11, LX/14x;

    invoke-direct/range {v3 .. v11}, LX/AEi;-><init>(LX/0Or;LX/0kL;LX/B9y;Lcom/facebook/intent/feed/IFeedIntentBuilder;Landroid/content/Context;LX/17Q;LX/0Zb;LX/14x;)V

    .line 1646994
    move-object v0, v3

    .line 1646995
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646996
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646997
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646998
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/AEi;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1646999
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1647000
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1647001
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1647002
    if-nez v1, :cond_1

    .line 1647003
    :cond_0
    :goto_0
    return-void

    .line 1647004
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v8

    .line 1647005
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-nez v2, :cond_3

    const/4 v6, 0x0

    .line 1647006
    :goto_1
    invoke-static {v1}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v3

    .line 1647007
    if-eqz v8, :cond_0

    if-eqz v6, :cond_0

    .line 1647008
    iget-object v1, p0, LX/AEi;->h:LX/14x;

    invoke-static {v1}, LX/AEg;->a(LX/14x;)Z

    move-result v9

    .line 1647009
    if-eqz v9, :cond_4

    .line 1647010
    iget-object v0, p0, LX/AEi;->g:LX/B9y;

    iget-object v2, p0, LX/AEi;->e:Landroid/content/Context;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v7, "page"

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, LX/B9y;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1647011
    :goto_2
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1647012
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    if-nez v1, :cond_5

    .line 1647013
    :cond_2
    const/4 v1, 0x0

    .line 1647014
    :goto_3
    move-object v0, v1

    .line 1647015
    iget-object v1, p0, LX/AEi;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1647016
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 1647017
    :cond_4
    sget-object v1, LX/0ax;->bm:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1647018
    iget-object v1, p0, LX/AEi;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/AEi;->e:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_2

    .line 1647019
    :cond_5
    if-eqz v9, :cond_6

    const-string v1, "messenger"

    .line 1647020
    :goto_4
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "message_cta_click"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "redirect_type"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "page_id"

    invoke-virtual {v1, v2, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "post_id"

    invoke-virtual {v1, v2, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 1647021
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1647022
    move-object v1, v1

    .line 1647023
    goto :goto_3

    .line 1647024
    :cond_6
    const-string v1, "mtouch"

    goto :goto_4
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/AEh;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/AEh;"
        }
    .end annotation

    .prologue
    .line 1647025
    new-instance v0, LX/AEh;

    invoke-direct {v0, p0, p1}, LX/AEh;-><init>(LX/AEi;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method
