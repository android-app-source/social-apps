.class public final LX/9zY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1601933
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 1601934
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1601935
    :goto_0
    return v1

    .line 1601936
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1601937
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1601938
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1601939
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1601940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1601941
    const-string v9, "filter_type_sets"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1601942
    invoke-static {p0, p1}, LX/5v9;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1601943
    :cond_2
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1601944
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1601945
    :cond_3
    const-string v9, "logging_unit_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1601946
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1601947
    :cond_4
    const-string v9, "modules"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1601948
    invoke-static {p0, p1}, LX/9zW;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1601949
    :cond_5
    const-string v9, "post_search_intent_log"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1601950
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1601951
    :cond_6
    const-string v9, "query_function"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1601952
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1601953
    :cond_7
    const-string v9, "query_title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1601954
    invoke-static {p0, p1}, LX/9zX;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 1601955
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1601956
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1601957
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1601958
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1601959
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1601960
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1601961
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1601962
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1601963
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1601964
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1601965
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601966
    if-eqz v0, :cond_0

    .line 1601967
    const-string v1, "filter_type_sets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601968
    invoke-static {p0, v0, p2, p3}, LX/5v9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1601969
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1601970
    if-eqz v0, :cond_1

    .line 1601971
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601972
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601973
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1601974
    if-eqz v0, :cond_2

    .line 1601975
    const-string v1, "logging_unit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601976
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601977
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601978
    if-eqz v0, :cond_3

    .line 1601979
    const-string v1, "modules"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601980
    invoke-static {p0, v0, p2, p3}, LX/9zW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1601981
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1601982
    if-eqz v0, :cond_4

    .line 1601983
    const-string v1, "post_search_intent_log"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601984
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601985
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1601986
    if-eqz v0, :cond_5

    .line 1601987
    const-string v1, "query_function"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601988
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1601989
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1601990
    if-eqz v0, :cond_6

    .line 1601991
    const-string v1, "query_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1601992
    invoke-static {p0, v0, p2}, LX/9zX;->a(LX/15i;ILX/0nX;)V

    .line 1601993
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1601994
    return-void
.end method
