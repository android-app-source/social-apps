.class public LX/8pq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pe;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Sh;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0rq;

.field private final f:LX/0sa;


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0rq;LX/0sa;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/0rq;",
            "LX/0sa;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1406452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406453
    iput-object p1, p0, LX/8pq;->a:LX/0Sh;

    .line 1406454
    iput-object p2, p0, LX/8pq;->b:Ljava/util/concurrent/ExecutorService;

    .line 1406455
    iput-object p3, p0, LX/8pq;->c:LX/0Ot;

    .line 1406456
    iput-object p4, p0, LX/8pq;->d:LX/0Ot;

    .line 1406457
    iput-object p5, p0, LX/8pq;->e:LX/0rq;

    .line 1406458
    iput-object p6, p0, LX/8pq;->f:LX/0sa;

    .line 1406459
    return-void
.end method

.method private a(LX/0Px;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0zO",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1406460
    new-instance v0, LX/8r8;

    invoke-direct {v0}, LX/8r8;-><init>()V

    move-object v0, v0

    .line 1406461
    const-string v1, "profile_ids"

    invoke-virtual {v0, v1, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_pic_media_type"

    iget-object v2, p0, LX/8pq;->e:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->b()LX/0wF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/8r8;

    .line 1406462
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/8pq;
    .locals 10

    .prologue
    .line 1406463
    const-class v1, LX/8pq;

    monitor-enter v1

    .line 1406464
    :try_start_0
    sget-object v0, LX/8pq;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1406465
    sput-object v2, LX/8pq;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1406466
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406467
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1406468
    new-instance v3, LX/8pq;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0xafd

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xafc

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v8

    check-cast v8, LX/0rq;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v9

    check-cast v9, LX/0sa;

    invoke-direct/range {v3 .. v9}, LX/8pq;-><init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0rq;LX/0sa;)V

    .line 1406469
    move-object v0, v3

    .line 1406470
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1406471
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8pq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1406472
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1406473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;",
            ">;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1406474
    invoke-static {p1}, LX/0tX;->c(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1406475
    new-instance v1, LX/8pp;

    invoke-direct {v1, p0}, LX/8pp;-><init>(LX/8pq;)V

    iget-object v2, p0, LX/8pq;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1406476
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1406477
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;

    .line 1406478
    invoke-virtual {v0}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7, v7}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1406479
    invoke-virtual {v0}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    .line 1406480
    new-instance v5, LX/3dL;

    invoke-direct {v5}, LX/3dL;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;->l()Ljava/lang/String;

    move-result-object v6

    .line 1406481
    iput-object v6, v5, LX/3dL;->E:Ljava/lang/String;

    .line 1406482
    move-object v5, v5

    .line 1406483
    invoke-virtual {v0}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;->m()Ljava/lang/String;

    move-result-object v6

    .line 1406484
    iput-object v6, v5, LX/3dL;->ag:Ljava/lang/String;

    .line 1406485
    move-object v5, v5

    .line 1406486
    iput-object v3, v5, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1406487
    move-object v3, v5

    .line 1406488
    iput-object v4, v3, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1406489
    move-object v3, v3

    .line 1406490
    invoke-virtual {v0}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 1406491
    iput-object v0, v3, LX/3dL;->B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1406492
    move-object v0, v3

    .line 1406493
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1406494
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1406495
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a()LX/89l;
    .locals 1

    .prologue
    .line 1406496
    sget-object v0, LX/89l;->PROFILES_BY_IDS:LX/89l;

    return-object v0
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;LX/0TF;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406497
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/8pq;->a(LX/0Px;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1406498
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1406499
    move-object v1, v0

    .line 1406500
    iget-object v0, p0, LX/8pq;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    new-instance v2, LX/8po;

    invoke-direct {v2, p0, p3}, LX/8po;-><init>(LX/8pq;LX/0TF;)V

    .line 1406501
    iget-object v3, v1, LX/0zO;->m:LX/0gW;

    move-object v3, v3

    .line 1406502
    iget-object p1, v3, LX/0gW;->f:Ljava/lang/String;

    move-object v3, p1

    .line 1406503
    invoke-virtual {v0, v1, v2, v3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1406504
    iget-object v1, p0, LX/8pq;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/8pq;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1406505
    return-void
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406506
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/8pq;->a(LX/0Px;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1406507
    iget-object v0, p0, LX/8pq;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1406508
    iget-object v1, p0, LX/8pq;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/8pq;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1406509
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1406510
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1406511
    iget-object v0, p0, LX/8pq;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1406512
    return-void
.end method
