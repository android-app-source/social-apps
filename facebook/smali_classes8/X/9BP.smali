.class public final enum LX/9BP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9BP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9BP;

.field public static final enum BLINK:LX/9BP;

.field public static final enum THROB:LX/9BP;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1451687
    new-instance v0, LX/9BP;

    const-string v1, "THROB"

    invoke-direct {v0, v1, v2}, LX/9BP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9BP;->THROB:LX/9BP;

    .line 1451688
    new-instance v0, LX/9BP;

    const-string v1, "BLINK"

    invoke-direct {v0, v1, v3}, LX/9BP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9BP;->BLINK:LX/9BP;

    .line 1451689
    const/4 v0, 0x2

    new-array v0, v0, [LX/9BP;

    sget-object v1, LX/9BP;->THROB:LX/9BP;

    aput-object v1, v0, v2

    sget-object v1, LX/9BP;->BLINK:LX/9BP;

    aput-object v1, v0, v3

    sput-object v0, LX/9BP;->$VALUES:[LX/9BP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1451690
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9BP;
    .locals 1

    .prologue
    .line 1451691
    const-class v0, LX/9BP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9BP;

    return-object v0
.end method

.method public static values()[LX/9BP;
    .locals 1

    .prologue
    .line 1451692
    sget-object v0, LX/9BP;->$VALUES:[LX/9BP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9BP;

    return-object v0
.end method
