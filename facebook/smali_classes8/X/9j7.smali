.class public LX/9j7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/9j7;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final b:LX/0id;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529215
    iput-object p1, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1529216
    iput-object p2, p0, LX/9j7;->b:LX/0id;

    .line 1529217
    return-void
.end method

.method public static a(LX/0QB;)LX/9j7;
    .locals 5

    .prologue
    .line 1529190
    sget-object v0, LX/9j7;->c:LX/9j7;

    if-nez v0, :cond_1

    .line 1529191
    const-class v1, LX/9j7;

    monitor-enter v1

    .line 1529192
    :try_start_0
    sget-object v0, LX/9j7;->c:LX/9j7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1529193
    if-eqz v2, :cond_0

    .line 1529194
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1529195
    new-instance p0, LX/9j7;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v4

    check-cast v4, LX/0id;

    invoke-direct {p0, v3, v4}, LX/9j7;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;)V

    .line 1529196
    move-object v0, p0

    .line 1529197
    sput-object v0, LX/9j7;->c:LX/9j7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1529198
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1529199
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1529200
    :cond_1
    sget-object v0, LX/9j7;->c:LX/9j7;

    return-object v0

    .line 1529201
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1529202
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/9j7;ILX/9jG;)V
    .locals 2

    .prologue
    .line 1529211
    iget-object v1, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v0, LX/9jG;->CHECKIN:LX/9jG;

    invoke-virtual {p2, v0}, LX/9jG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "checkin_button"

    :goto_0
    invoke-interface {v1, p1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1529212
    return-void

    .line 1529213
    :cond_0
    const-string v0, "location_pin"

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1529203
    iget-object v0, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x150005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1529204
    iget-object v0, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x150006

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1529205
    iget-object v0, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x150016

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1529206
    iget-object v0, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x150017

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1529207
    iget-object v0, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x150018

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1529208
    iget-object v0, p0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x150019

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1529209
    iget-object v0, p0, LX/9j7;->b:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 1529210
    return-void
.end method
