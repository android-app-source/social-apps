.class public final LX/99C;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/99A;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1447020
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/99C;->a:Ljava/util/List;

    .line 1447021
    new-instance v0, LX/99B;

    invoke-direct {v0}, LX/99B;-><init>()V

    sput-object v0, LX/99C;->b:LX/99A;

    return-void
.end method

.method public static a(Landroid/content/res/Resources;ILX/99A;)V
    .locals 3

    .prologue
    .line 1447022
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1447023
    instance-of v0, v1, Lcom/facebook/fbui/drawable/NetworkDrawable;

    if-nez v0, :cond_0

    .line 1447024
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The drawable ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is not a Network Drawable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v1

    .line 1447025
    check-cast v0, Lcom/facebook/fbui/drawable/NetworkDrawable;

    .line 1447026
    iget-object v2, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-virtual {v2}, LX/999;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1447027
    if-nez v2, :cond_1

    .line 1447028
    sget-object v2, LX/99C;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1447029
    iput-object p2, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->e:LX/99A;

    .line 1447030
    invoke-static {v0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->d(Lcom/facebook/fbui/drawable/NetworkDrawable;)V

    .line 1447031
    :goto_1
    return-void

    .line 1447032
    :cond_1
    invoke-interface {p2, p1}, LX/99A;->a(I)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
