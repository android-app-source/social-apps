.class public final LX/9gC;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPostedPhotosModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1523180
    const-class v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPostedPhotosModel;

    const v0, -0x5f553cd    # -1.800074E35f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "MediaFetchForPostedPhotos"

    const-string v6, "977b01d425e89bcecf86c592f5363452"

    const-string v7, "page"

    const-string v8, "10155211181646729"

    const-string v9, "10155259089286729"

    .line 1523181
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1523182
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1523183
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1523190
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1523191
    sparse-switch v0, :sswitch_data_0

    .line 1523192
    :goto_0
    return-object p1

    .line 1523193
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1523194
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1523195
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1523196
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1523197
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1523198
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1523199
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1523200
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1523201
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1523202
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1523203
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1523204
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1523205
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1523206
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1523207
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1523208
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1523209
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1523210
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1523211
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1523212
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1523213
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1523214
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1523215
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_16
        -0x680de62a -> :sswitch_d
        -0x6326fdb3 -> :sswitch_b
        -0x51484e72 -> :sswitch_7
        -0x4620c181 -> :sswitch_2
        -0x4496acc9 -> :sswitch_e
        -0x43eadc34 -> :sswitch_13
        -0x421ba035 -> :sswitch_c
        -0x3c54de38 -> :sswitch_12
        -0x2c889631 -> :sswitch_11
        -0x2a0a3d40 -> :sswitch_8
        -0x1b87b280 -> :sswitch_a
        -0x1673cbea -> :sswitch_5
        -0x12efdeb3 -> :sswitch_f
        -0x93a55fc -> :sswitch_9
        0x101fb19 -> :sswitch_4
        0xa1fa812 -> :sswitch_3
        0x214100e0 -> :sswitch_10
        0x3052e0ff -> :sswitch_0
        0x73a026b5 -> :sswitch_14
        0x7824fd38 -> :sswitch_6
        0x7c7626df -> :sswitch_1
        0x7e07ec78 -> :sswitch_15
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1523184
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1523185
    :goto_1
    return v0

    .line 1523186
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_1
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1523187
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1523188
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1523189
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
