.class public LX/APD;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/0ad;

.field public final d:LX/0Uh;

.field private final e:LX/7Dh;

.field public final f:LX/1kZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1670065
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    sget-object v1, LX/2rw;->EVENT:LX/2rw;

    sget-object v2, LX/2rw;->GROUP:LX/2rw;

    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    sget-object v4, LX/2rw;->MARKETPLACE:LX/2rw;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/APD;->a:LX/0Rf;

    .line 1670066
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    sget-object v1, LX/APD;->a:LX/0Rf;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    sget-object v1, LX/2rw;->USER:LX/2rw;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    sput-object v0, LX/APD;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Uh;LX/7Dh;LX/1kZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670068
    iput-object p1, p0, LX/APD;->c:LX/0ad;

    .line 1670069
    iput-object p2, p0, LX/APD;->d:LX/0Uh;

    .line 1670070
    iput-object p3, p0, LX/APD;->e:LX/7Dh;

    .line 1670071
    iput-object p4, p0, LX/APD;->f:LX/1kZ;

    .line 1670072
    return-void
.end method

.method public static a(LX/0QB;)LX/APD;
    .locals 1

    .prologue
    .line 1670063
    invoke-static {p0}, LX/APD;->b(LX/0QB;)LX/APD;

    move-result-object v0

    return-object v0
.end method

.method private a()Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1670064
    iget-object v0, p0, LX/APD;->c:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/1EB;->E:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    return v0
.end method

.method public static b(LX/0QB;)LX/APD;
    .locals 5

    .prologue
    .line 1670061
    new-instance v4, LX/APD;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v2

    check-cast v2, LX/7Dh;

    invoke-static {p0}, LX/1kZ;->a(LX/0QB;)LX/1kZ;

    move-result-object v3

    check-cast v3, LX/1kZ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/APD;-><init>(LX/0ad;LX/0Uh;LX/7Dh;LX/1kZ;)V

    .line 1670062
    return-object v4
.end method


# virtual methods
.method public final a(LX/ARN;LX/2rw;ZZZZZZLX/0Px;)Z
    .locals 4
    .param p1    # LX/ARN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ARN;",
            "LX/2rw;",
            "ZZZZZZ",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1670052
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/ARN;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-eqz p4, :cond_1

    if-eqz p3, :cond_2

    iget-object v1, p0, LX/APD;->c:LX/0ad;

    sget-short v2, LX/1EB;->n:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1670053
    :cond_1
    :goto_0
    return v0

    .line 1670054
    :cond_2
    invoke-static {p9}, LX/7kq;->w(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/APD;->e:LX/7Dh;

    invoke-virtual {v1}, LX/7Dh;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670055
    :cond_3
    if-eqz p7, :cond_4

    iget-object v1, p0, LX/APD;->c:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/1EB;->P:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670056
    :cond_4
    if-eqz p8, :cond_5

    iget-object v1, p0, LX/APD;->f:LX/1kZ;

    invoke-virtual {v1}, LX/1kZ;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670057
    :cond_5
    if-eqz p5, :cond_6

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    invoke-virtual {v1, p2}, LX/2rw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670058
    :cond_6
    if-eqz p5, :cond_7

    if-eqz p6, :cond_1

    .line 1670059
    :cond_7
    if-eqz p5, :cond_8

    invoke-direct {p0}, LX/APD;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1670060
    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(ZLX/2rw;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1670046
    if-nez p1, :cond_1

    .line 1670047
    :cond_0
    :goto_0
    return v0

    .line 1670048
    :cond_1
    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-ne p2, v1, :cond_2

    if-eqz p3, :cond_0

    .line 1670049
    :cond_2
    iget-object v1, p0, LX/APD;->c:LX/0ad;

    sget-short v2, LX/1EB;->C:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/APD;->b:LX/0Rf;

    .line 1670050
    :goto_1
    invoke-virtual {v0, p2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1670051
    :cond_3
    sget-object v0, LX/APD;->a:LX/0Rf;

    goto :goto_1
.end method
