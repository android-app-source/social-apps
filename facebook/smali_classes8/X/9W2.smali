.class public final LX/9W2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/9WC;


# direct methods
.method public constructor <init>(LX/9WC;)V
    .locals 0

    .prologue
    .line 1500490
    iput-object p1, p0, LX/9W2;->a:LX/9WC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1500491
    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 1500492
    :goto_0
    return-void

    .line 1500493
    :cond_0
    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->u:LX/9Vy;

    instance-of v0, v0, LX/9WQ;

    if-eqz v0, :cond_1

    .line 1500494
    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->g:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->u:LX/9Vy;

    check-cast v0, LX/9WQ;

    invoke-static {v1, v0}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1500495
    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->r:LX/9Ve;

    iget-object v1, p0, LX/9W2;->a:LX/9WC;

    iget-object v1, v1, LX/9WC;->k:LX/9Vx;

    .line 1500496
    iget-object p1, v1, LX/9Vx;->a:Ljava/lang/String;

    move-object v1, p1

    .line 1500497
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "negativefeedback_cancel_message_composer"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500498
    invoke-static {v0, p1, v1}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500499
    :goto_1
    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    .line 1500500
    iput-boolean v2, v0, LX/9WC;->l:Z

    .line 1500501
    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->c:LX/1Ck;

    sget-object v1, LX/9WB;->FLOW_STEP:LX/9WB;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1500502
    iget-object v1, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->h:Ljava/util/Stack;

    iget-object v2, p0, LX/9W2;->a:LX/9WC;

    iget-object v2, v2, LX/9WC;->h:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v2}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Vx;

    invoke-static {v1, v0}, LX/9WC;->a$redex0(LX/9WC;LX/9Vx;)V

    goto :goto_0

    .line 1500503
    :cond_1
    iget-object v0, p0, LX/9W2;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->r:LX/9Ve;

    iget-object v1, p0, LX/9W2;->a:LX/9WC;

    iget-object v1, v1, LX/9WC;->k:LX/9Vx;

    .line 1500504
    iget-object p1, v1, LX/9Vx;->a:Ljava/lang/String;

    move-object v1, p1

    .line 1500505
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "negativefeedback_went_back"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500506
    invoke-static {v0, p1, v1}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500507
    goto :goto_1
.end method
