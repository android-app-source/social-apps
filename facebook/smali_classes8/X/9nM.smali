.class public final LX/9nM;
.super LX/5p8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5p8",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/react/bridge/Callback;

.field public final synthetic b:LX/9nO;


# direct methods
.method public constructor <init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 1537159
    iput-object p1, p0, LX/9nM;->b:LX/9nO;

    iput-object p3, p0, LX/9nM;->a:Lcom/facebook/react/bridge/Callback;

    invoke-direct {p0, p2}, LX/5p8;-><init>(LX/5pX;)V

    return-void
.end method

.method private varargs a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1537160
    iget-object v0, p0, LX/9nM;->b:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1537161
    iget-object v0, p0, LX/9nM;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v5}, LX/9nH;->c(Ljava/lang/String;)LX/5pH;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1537162
    :goto_0
    return-void

    .line 1537163
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/9nM;->b:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->d()V

    .line 1537164
    iget-object v0, p0, LX/9nM;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1537165
    :catch_0
    move-exception v0

    .line 1537166
    const-string v1, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537167
    iget-object v1, p0, LX/9nM;->a:Lcom/facebook/react/bridge/Callback;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1537168
    invoke-direct {p0}, LX/9nM;->a()V

    return-void
.end method
