.class public LX/9Do;
.super LX/9Bh;
.source ""


# instance fields
.field public a:LX/9D1;

.field public b:LX/9Dx;

.field public c:Lcom/facebook/graphql/model/GraphQLComment;

.field public d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/feedback/ui/NewCommentsPillView;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0wW;LX/4mV;LX/9Dx;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456229
    invoke-direct {p0, p1, p2}, LX/9Bh;-><init>(LX/0wW;LX/4mV;)V

    .line 1456230
    new-instance v0, LX/9Dm;

    invoke-direct {v0, p0}, LX/9Dm;-><init>(LX/9Do;)V

    iput-object v0, p0, LX/9Do;->e:Landroid/view/View$OnClickListener;

    .line 1456231
    iput-object p3, p0, LX/9Do;->b:LX/9Dx;

    .line 1456232
    return-void
.end method

.method public static a(LX/0QB;)LX/9Do;
    .locals 4

    .prologue
    .line 1456222
    new-instance v3, LX/9Do;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-static {p0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v1

    check-cast v1, LX/4mV;

    .line 1456223
    new-instance v2, LX/9Dx;

    invoke-direct {v2}, LX/9Dx;-><init>()V

    .line 1456224
    move-object v2, v2

    .line 1456225
    move-object v2, v2

    .line 1456226
    check-cast v2, LX/9Dx;

    invoke-direct {v3, v0, v1, v2}, LX/9Do;-><init>(LX/0wW;LX/4mV;LX/9Dx;)V

    .line 1456227
    move-object v0, v3

    .line 1456228
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;LX/9D1;)V
    .locals 2

    .prologue
    .line 1456161
    new-instance v0, LX/0zw;

    invoke-direct {v0, p1}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, LX/9Do;->d:LX/0zw;

    .line 1456162
    iput-object p2, p0, LX/9Do;->a:LX/9D1;

    .line 1456163
    iget-object v0, p0, LX/9Do;->a:LX/9D1;

    if-nez v0, :cond_0

    .line 1456164
    :goto_0
    return-void

    .line 1456165
    :cond_0
    iget-object v0, p0, LX/9Do;->a:LX/9D1;

    new-instance v1, LX/9Dn;

    invoke-direct {v1, p0}, LX/9Dn;-><init>(LX/9Do;)V

    invoke-virtual {v0, v1}, LX/9D1;->a(LX/0fx;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 9

    .prologue
    .line 1456175
    if-nez p1, :cond_0

    .line 1456176
    :goto_0
    return-void

    .line 1456177
    :cond_0
    iput-object p1, p0, LX/9Do;->c:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456178
    iget-object v0, p0, LX/9Do;->a:LX/9D1;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9Do;->a:LX/9D1;

    invoke-virtual {v0}, LX/9D1;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1456179
    iget-object v0, p0, LX/9Do;->a:LX/9D1;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9D1;->a(Ljava/lang/String;)Z

    goto :goto_0

    .line 1456180
    :cond_1
    iget-object v0, p0, LX/9Do;->b:LX/9Dx;

    .line 1456181
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-nez v1, :cond_4

    .line 1456182
    :cond_2
    :goto_1
    iget-object v0, p0, LX/9Do;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/NewCommentsPillView;

    iget-object v2, p0, LX/9Do;->b:LX/9Dx;

    iget-object v1, p0, LX/9Do;->d:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedback/ui/NewCommentsPillView;

    invoke-virtual {v1}, Lcom/facebook/feedback/ui/NewCommentsPillView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v7, 0x2

    const/4 p1, 0x1

    const/4 v8, 0x0

    .line 1456183
    iget-object v3, v2, LX/9Dx;->e:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v3, :cond_3

    iget-object v3, v2, LX/9Dx;->e:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-nez v3, :cond_c

    :cond_3
    const/4 v3, 0x0

    move-object v4, v3

    .line 1456184
    :goto_2
    sget-object v3, LX/9Dv;->a:[I

    iget-object v5, v2, LX/9Dx;->f:LX/9Dw;

    invoke-virtual {v5}, LX/9Dw;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 1456185
    const v3, 0x7f081232

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    move-object v1, v3

    .line 1456186
    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/NewCommentsPillView;->setPillText(Ljava/lang/CharSequence;)V

    .line 1456187
    iget-object v0, p0, LX/9Do;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/NewCommentsPillView;

    iget-object v1, p0, LX/9Do;->b:LX/9Dx;

    .line 1456188
    iget-object v2, v1, LX/9Dx;->f:LX/9Dw;

    sget-object v3, LX/9Dw;->NO_FRIENDS:LX/9Dw;

    if-ne v2, v3, :cond_e

    .line 1456189
    const/4 v2, 0x0

    .line 1456190
    :goto_4
    move-object v1, v2

    .line 1456191
    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/NewCommentsPillView;->setPillProfilePictures(Ljava/util/List;)V

    .line 1456192
    iget-object v0, p0, LX/9Do;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/NewCommentsPillView;

    iget-object v1, p0, LX/9Do;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/NewCommentsPillView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1456193
    invoke-virtual {p0}, LX/9Bh;->a()Z

    goto/16 :goto_0

    .line 1456194
    :cond_4
    iget v1, v0, LX/9Dx;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/9Dx;->d:I

    .line 1456195
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->T()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1456196
    iget-object v1, v0, LX/9Dx;->b:Ljava/util/Set;

    invoke-static {p1}, LX/9Dx;->b(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1456197
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    .line 1456198
    :cond_5
    :goto_5
    iput-object p1, v0, LX/9Dx;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456199
    :goto_6
    iget-object v1, v0, LX/9Dx;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1456200
    sget-object v1, LX/9Dw;->NO_FRIENDS:LX/9Dw;

    iput-object v1, v0, LX/9Dx;->f:LX/9Dw;

    .line 1456201
    :goto_7
    goto/16 :goto_1

    .line 1456202
    :cond_6
    iget-object v1, v0, LX/9Dx;->c:Ljava/util/Set;

    invoke-static {p1}, LX/9Dx;->b(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1456203
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 1456204
    iget-object v2, v0, LX/9Dx;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1456205
    iget-object v2, v0, LX/9Dx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_8

    .line 1456206
    iget-object v2, v0, LX/9Dx;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1456207
    :cond_8
    iget-object v2, v0, LX/9Dx;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1456208
    :cond_9
    iget-object v1, v0, LX/9Dx;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    iget-object v1, v0, LX/9Dx;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1456209
    sget-object v1, LX/9Dw;->ONE_FRIEND_ONLY:LX/9Dw;

    iput-object v1, v0, LX/9Dx;->f:LX/9Dw;

    goto :goto_7

    .line 1456210
    :cond_a
    iget-object v1, v0, LX/9Dx;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    iget-object v1, v0, LX/9Dx;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1456211
    sget-object v1, LX/9Dw;->TWO_FRIENDS_ONLY:LX/9Dw;

    iput-object v1, v0, LX/9Dx;->f:LX/9Dw;

    goto :goto_7

    .line 1456212
    :cond_b
    sget-object v1, LX/9Dw;->FRIEND_AND_OTHERS:LX/9Dw;

    iput-object v1, v0, LX/9Dx;->f:LX/9Dw;

    goto :goto_7

    .line 1456213
    :cond_c
    iget-object v3, v2, LX/9Dx;->e:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v3}, LX/9Dx;->b(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_2

    .line 1456214
    :pswitch_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0089

    iget v5, v2, LX/9Dx;->d:I

    new-array v6, p1, [Ljava/lang/Object;

    iget v7, v2, LX/9Dx;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 1456215
    :pswitch_1
    const v3, 0x7f081237

    new-array v5, p1, [Ljava/lang/Object;

    aput-object v4, v5, v8

    invoke-virtual {v1, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 1456216
    :pswitch_2
    iget-object v3, v2, LX/9Dx;->b:Ljava/util/Set;

    iget-object v5, v2, LX/9Dx;->b:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 1456217
    aget-object v5, v3, v8

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    aget-object v3, v3, p1

    .line 1456218
    :goto_8
    const v5, 0x7f081238

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v4, v6, v8

    aput-object v3, v6, p1

    invoke-virtual {v1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 1456219
    :cond_d
    aget-object v3, v3, v8

    goto :goto_8

    .line 1456220
    :pswitch_3
    iget-object v3, v2, LX/9Dx;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    iget-object v5, v2, LX/9Dx;->c:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    add-int/2addr v3, v5

    add-int/lit8 v3, v3, -0x1

    .line 1456221
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f008a

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, p1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    :cond_e
    iget-object v2, v1, LX/9Dx;->a:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1456167
    invoke-super {p0}, LX/9Bh;->b()V

    .line 1456168
    iget-object v0, p0, LX/9Do;->b:LX/9Dx;

    .line 1456169
    const/4 p0, 0x0

    iput p0, v0, LX/9Dx;->d:I

    .line 1456170
    iget-object p0, v0, LX/9Dx;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 1456171
    iget-object p0, v0, LX/9Dx;->b:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->clear()V

    .line 1456172
    iget-object p0, v0, LX/9Dx;->c:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->clear()V

    .line 1456173
    const/4 p0, 0x0

    iput-object p0, v0, LX/9Dx;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456174
    return-void
.end method

.method public final f()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1456166
    iget-object v0, p0, LX/9Do;->d:LX/0zw;

    return-object v0
.end method
