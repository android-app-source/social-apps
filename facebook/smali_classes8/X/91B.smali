.class public final enum LX/91B;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/91B;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/91B;

.field public static final enum IS_FROM_COMPOSER:LX/91B;

.field public static final enum PACK_ID:LX/91B;

.field public static final enum STICKER_ID:LX/91B;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1430208
    new-instance v0, LX/91B;

    const-string v1, "IS_FROM_COMPOSER"

    const-string v2, "is_from_composer"

    invoke-direct {v0, v1, v3, v2}, LX/91B;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91B;->IS_FROM_COMPOSER:LX/91B;

    .line 1430209
    new-instance v0, LX/91B;

    const-string v1, "STICKER_ID"

    const-string v2, "sticker_id"

    invoke-direct {v0, v1, v4, v2}, LX/91B;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91B;->STICKER_ID:LX/91B;

    .line 1430210
    new-instance v0, LX/91B;

    const-string v1, "PACK_ID"

    const-string v2, "pack_id"

    invoke-direct {v0, v1, v5, v2}, LX/91B;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91B;->PACK_ID:LX/91B;

    .line 1430211
    const/4 v0, 0x3

    new-array v0, v0, [LX/91B;

    sget-object v1, LX/91B;->IS_FROM_COMPOSER:LX/91B;

    aput-object v1, v0, v3

    sget-object v1, LX/91B;->STICKER_ID:LX/91B;

    aput-object v1, v0, v4

    sget-object v1, LX/91B;->PACK_ID:LX/91B;

    aput-object v1, v0, v5

    sput-object v0, LX/91B;->$VALUES:[LX/91B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1430212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1430213
    iput-object p3, p0, LX/91B;->name:Ljava/lang/String;

    .line 1430214
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/91B;
    .locals 1

    .prologue
    .line 1430215
    const-class v0, LX/91B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/91B;

    return-object v0
.end method

.method public static values()[LX/91B;
    .locals 1

    .prologue
    .line 1430216
    sget-object v0, LX/91B;->$VALUES:[LX/91B;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/91B;

    return-object v0
.end method


# virtual methods
.method public final getParamKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1430217
    iget-object v0, p0, LX/91B;->name:Ljava/lang/String;

    return-object v0
.end method
