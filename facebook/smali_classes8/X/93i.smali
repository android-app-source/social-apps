.class public final LX/93i;
.super LX/93Q;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:J

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0ad;

.field public final e:Z


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;LX/0tX;Landroid/content/res/Resources;Ljava/lang/Long;LX/0ad;Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434132
    invoke-direct {p0, p1, p2, p3}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1434133
    iput-object p4, p0, LX/93i;->a:LX/0tX;

    .line 1434134
    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/93i;->b:J

    .line 1434135
    iput-object p5, p0, LX/93i;->c:Landroid/content/res/Resources;

    .line 1434136
    iput-object p7, p0, LX/93i;->d:LX/0ad;

    .line 1434137
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/93i;->e:Z

    .line 1434138
    return-void
.end method

.method public static a(LX/93i;Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1434120
    invoke-virtual {p1}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->go_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1434121
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1434122
    if-nez v0, :cond_0

    .line 1434123
    iget-object v0, p0, LX/93i;->c:Landroid/content/res/Resources;

    const v1, 0x7f08133b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1434124
    :goto_0
    return-object v0

    .line 1434125
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1434126
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1434127
    if-nez v0, :cond_1

    .line 1434128
    iget-object v0, p0, LX/93i;->c:Landroid/content/res/Resources;

    const v1, 0x7f08133b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1434129
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1434130
    iget-object v0, p0, LX/93i;->c:Landroid/content/res/Resources;

    const v1, 0x7f08133a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1434131
    :cond_2
    iget-object v1, p0, LX/93i;->c:Landroid/content/res/Resources;

    const v2, 0x7f081339

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/93i;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZZ)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;
    .locals 6

    .prologue
    .line 1434139
    if-eqz p1, :cond_0

    .line 1434140
    sget-object v0, LX/93h;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1434141
    :cond_0
    iget-object v0, p0, LX/93Q;->b:LX/03V;

    move-object v0, v0

    .line 1434142
    const-string v1, "composer_group_undefined_privacy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not determine group privacy, id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, LX/93i;->b:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", visibility="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    :goto_0
    return-object v0

    .line 1434144
    :pswitch_0
    if-eqz p3, :cond_1

    .line 1434145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->WORK_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    .line 1434146
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    .line 1434147
    :pswitch_1
    if-eqz p2, :cond_2

    .line 1434148
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    .line 1434149
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1434096
    invoke-super {p0}, LX/93Q;->a()V

    .line 1434097
    const/4 v3, 0x1

    .line 1434098
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434099
    iput-object v1, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434100
    move-object v0, v0

    .line 1434101
    iget-object v1, p0, LX/93i;->c:Landroid/content/res/Resources;

    const v2, 0x7f081335

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1434102
    iput-object v1, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1434103
    move-object v0, v0

    .line 1434104
    iget-object v1, p0, LX/93i;->c:Landroid/content/res/Resources;

    const v2, 0x7f081336

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1434105
    iput-object v1, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1434106
    move-object v0, v0

    .line 1434107
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434108
    new-instance v1, LX/7lP;

    invoke-direct {v1}, LX/7lP;-><init>()V

    .line 1434109
    iput-boolean v3, v1, LX/7lP;->a:Z

    .line 1434110
    move-object v1, v1

    .line 1434111
    iput-boolean v3, v1, LX/7lP;->b:Z

    .line 1434112
    move-object v1, v1

    .line 1434113
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    move-object v0, v0

    .line 1434114
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434115
    new-instance v1, LX/94D;

    invoke-direct {v1}, LX/94D;-><init>()V

    move-object v1, v1

    .line 1434116
    const-string v2, "group_id"

    iget-wide v4, p0, LX/93i;->b:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1434117
    iget-object v2, p0, LX/93Q;->c:LX/1Ck;

    move-object v2, v2

    .line 1434118
    const-string v3, "fetch_group_data"

    iget-object v4, p0, LX/93i;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v4, LX/93g;

    invoke-direct {v4, p0, v0}, LX/93g;-><init>(LX/93i;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1434119
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1434095
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "group:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/93i;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
