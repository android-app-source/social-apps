.class public LX/ALC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0hB;


# direct methods
.method public constructor <init>(LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1664686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1664687
    iput-object p1, p0, LX/ALC;->a:LX/0hB;

    .line 1664688
    return-void
.end method

.method public static b(LX/0QB;)LX/ALC;
    .locals 2

    .prologue
    .line 1664689
    new-instance v1, LX/ALC;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-direct {v1, v0}, LX/ALC;-><init>(LX/0hB;)V

    .line 1664690
    return-object v1
.end method


# virtual methods
.method public final a(F)I
    .locals 3

    .prologue
    .line 1664691
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1664692
    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_0

    .line 1664693
    iget-object v1, p0, LX/ALC;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    .line 1664694
    :goto_0
    move v0, v1

    .line 1664695
    return v0

    .line 1664696
    :cond_0
    iget-object v1, p0, LX/ALC;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    .line 1664697
    iget-object v2, p0, LX/ALC;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->d()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    .line 1664698
    iget-object v1, p0, LX/ALC;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    .line 1664699
    :cond_1
    float-to-int v1, v1

    goto :goto_0
.end method
