.class public final LX/9eG;
.super LX/9eF;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 0

    .prologue
    .line 1519343
    iput-object p1, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-direct {p0}, LX/9eF;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 1519344
    iget-object v0, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->ANIMATE_OUT:LX/9eK;

    if-eq v0, v1, :cond_0

    .line 1519345
    const-string v0, "illegal animationEnd state: %s, safe dismiss: %s"

    iget-object v1, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v1}, LX/9eK;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    .line 1519346
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v2, v3

    .line 1519347
    invoke-virtual {v2}, LX/0k3;->p()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1519348
    iget-object v0, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1519349
    :cond_0
    iget-object v0, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    if-eqz v0, :cond_1

    .line 1519350
    iget-object v0, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v1, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    const/4 v2, 0x0

    iget-object v3, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v3}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 1519351
    :cond_1
    iget-object v0, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1519352
    return-void
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 1519353
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    const v1, 0x3f7851ec    # 0.97f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 1519354
    iget-object v0, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v1, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    const/4 v2, 0x0

    iget-object v3, p0, LX/9eG;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v3}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 1519355
    :cond_0
    return-void
.end method
