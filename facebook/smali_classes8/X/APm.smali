.class public LX/APm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j6;",
        ":",
        "LX/0j3;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPostCompositionOverlayShowing;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesCanSubmit;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPostCompositionViewSupported;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginPublishButtonTextGetter;",
        ":",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginTitleGetter;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0in",
        "<TPluginData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/view/View$OnClickListener;

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final c:LX/ARv;

.field public final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/controller/ComposerFb4aTitleBarController$Delegate;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0h5;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/content/Context;

.field public final h:LX/APz;

.field public final i:LX/APv;

.field public j:LX/0ad;

.field public k:LX/6WS;

.field public l:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewStub;LX/0il;LX/ARv;LX/Hqz;Landroid/content/Context;LX/APz;LX/APw;LX/0ad;)V
    .locals 2
    .param p1    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/ARv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Hqz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewStub;",
            "TServices;",
            "Lcom/facebook/composer/controller/ComposerMenuCreator;",
            "Lcom/facebook/composer/controller/ComposerFb4aTitleBarController$Delegate;",
            "Landroid/content/Context;",
            "LX/APz;",
            "LX/APw;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670424
    new-instance v0, LX/APi;

    invoke-direct {v0, p0}, LX/APi;-><init>(LX/APm;)V

    iput-object v0, p0, LX/APm;->a:Landroid/view/View$OnClickListener;

    .line 1670425
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/APm;->f:Ljava/util/List;

    .line 1670426
    iput-object p5, p0, LX/APm;->g:Landroid/content/Context;

    .line 1670427
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APm;->b:Ljava/lang/ref/WeakReference;

    .line 1670428
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARv;

    iput-object v0, p0, LX/APm;->c:LX/ARv;

    .line 1670429
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APm;->d:Ljava/lang/ref/WeakReference;

    .line 1670430
    iput-object p6, p0, LX/APm;->h:LX/APz;

    .line 1670431
    new-instance p3, LX/APv;

    check-cast p2, LX/0il;

    invoke-static {p7}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const/16 v1, 0x259

    invoke-static {p7, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p4

    invoke-static {p7}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {p3, p2, v0, p4, v1}, LX/APv;-><init>(LX/0il;Landroid/content/res/Resources;LX/0Ot;LX/0ad;)V

    .line 1670432
    move-object v0, p3

    .line 1670433
    iput-object v0, p0, LX/APm;->i:LX/APv;

    .line 1670434
    iput-object p8, p0, LX/APm;->j:LX/0ad;

    .line 1670435
    const v0, 0x7f030317

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1670436
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1670437
    check-cast v0, LX/0h5;

    .line 1670438
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/APm;->e:Ljava/lang/ref/WeakReference;

    .line 1670439
    new-instance v1, LX/APj;

    invoke-direct {v1, p0}, LX/APj;-><init>(LX/APm;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1670440
    new-instance v1, LX/APk;

    invoke-direct {v1, p0}, LX/APk;-><init>(LX/APm;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1670441
    const v1, 0x7f030318

    invoke-interface {v0, v1}, LX/0h5;->d_(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    .line 1670442
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/APm;->l:Ljava/lang/ref/WeakReference;

    .line 1670443
    invoke-direct {p0}, LX/APm;->b()V

    .line 1670444
    return-void
.end method

.method private b()V
    .locals 10

    .prologue
    .line 1670351
    const/4 v5, 0x0

    .line 1670352
    iget-object v0, p0, LX/APm;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, LX/0il;

    .line 1670353
    iget-object v0, p0, LX/APm;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;

    .line 1670354
    iget-object v0, p0, LX/APm;->h:LX/APz;

    move-object v1, v4

    check-cast v1, LX/0in;

    invoke-interface {v1}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AQ9;

    check-cast v1, LX/AQ9;

    .line 1670355
    iget-object v2, v1, LX/AQ9;->g:LX/AQ4;

    move-object v1, v2

    .line 1670356
    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0j6;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j6;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j6;

    check-cast v4, LX/0j4;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, LX/APz;->a(LX/AQ4;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setText(Ljava/lang/CharSequence;)V

    .line 1670357
    iget-object v0, p0, LX/APm;->c:LX/ARv;

    const/4 v3, 0x0

    .line 1670358
    iget-object v1, v0, LX/ARv;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    move-object v2, v1

    .line 1670359
    check-cast v2, LX/0ik;

    invoke-interface {v2}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2zG;

    .line 1670360
    iget-object v4, v2, LX/2zG;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/APN;

    invoke-static {v2}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v8

    invoke-static {v2}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v7

    if-eqz v7, :cond_6

    const/4 v7, 0x1

    :goto_0
    invoke-static {v2}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v9

    .line 1670361
    iget-object v4, v9, LX/AQ9;->D:LX/ARN;

    move-object v9, v4

    .line 1670362
    invoke-static {v8, v7, v9}, LX/APN;->c(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ZLX/ARN;)LX/03R;

    move-result-object v4

    .line 1670363
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-eq v4, v2, :cond_7

    .line 1670364
    invoke-virtual {v4}, LX/03R;->asBoolean()Z

    move-result v4

    .line 1670365
    :goto_1
    move v4, v4

    .line 1670366
    move v2, v4

    .line 1670367
    if-nez v2, :cond_3

    move v1, v3

    .line 1670368
    :goto_2
    move v0, v1

    .line 1670369
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/APm;->j:LX/0ad;

    sget-short v1, LX/1EB;->ar:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1670370
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setShouldShowTriangle(Z)V

    .line 1670371
    iget-object v0, p0, LX/APm;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1670372
    :goto_3
    iget-object v0, p0, LX/APm;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670373
    invoke-static {p0}, LX/APm;->d(LX/APm;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, LX/APm;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081490

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1670374
    :goto_4
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    .line 1670375
    iput-object v1, v2, LX/108;->g:Ljava/lang/String;

    .line 1670376
    move-object v1, v2

    .line 1670377
    const/4 v2, 0x1

    .line 1670378
    iput-boolean v2, v1, LX/108;->q:Z

    .line 1670379
    move-object v1, v1

    .line 1670380
    const/4 v2, -0x2

    .line 1670381
    iput v2, v1, LX/108;->h:I

    .line 1670382
    move-object v1, v1

    .line 1670383
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->a()Z

    move-result v0

    .line 1670384
    iput-boolean v0, v1, LX/108;->d:Z

    .line 1670385
    move-object v0, v1

    .line 1670386
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1670387
    iget-object v1, p0, LX/APm;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/APm;->f:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670388
    :cond_0
    iget-object v1, p0, LX/APm;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1670389
    iget-object v1, p0, LX/APm;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1670390
    iget-object v0, p0, LX/APm;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iget-object v1, p0, LX/APm;->f:Ljava/util/List;

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1670391
    :cond_1
    return-void

    .line 1670392
    :cond_2
    invoke-virtual {v6, v5}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setShouldShowTriangle(Z)V

    .line 1670393
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Lcom/facebook/composer/ui/titlebar/FbTitleViewWithTriangle;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 1670394
    :cond_3
    invoke-static {v0}, LX/ARv;->c(LX/ARv;)LX/0P1;

    move-result-object v2

    .line 1670395
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v1, v4, :cond_4

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-static {v2}, LX/ARv;->a(LX/0P1;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    goto/16 :goto_2

    :cond_5
    move v1, v3

    goto/16 :goto_2

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1670396
    :cond_7
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v2, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq v4, v2, :cond_8

    .line 1670397
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1670398
    :cond_8
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowTargetSelection()Z

    move-result v4

    goto/16 :goto_1

    .line 1670399
    :cond_9
    iget-object v1, p0, LX/APm;->i:LX/APv;

    const/4 v4, 0x0

    .line 1670400
    iget-object v2, v1, LX/APv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 1670401
    sget-object v5, LX/APu;->a:[I

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j3;

    check-cast v3, LX/0jF;

    invoke-interface {v3}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v3

    invoke-virtual {v3}, LX/5Rn;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_0

    .line 1670402
    iget-object v2, v1, LX/APv;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v3, "composer_get_titlebar_button_spec"

    const-string v5, "Publish Mode not set."

    invoke-virtual {v2, v3, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 1670403
    :cond_a
    :goto_5
    move-object v1, v2

    .line 1670404
    goto/16 :goto_4

    :pswitch_0
    move-object v3, v2

    .line 1670405
    check-cast v3, LX/0in;

    invoke-interface {v3}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AQ9;

    .line 1670406
    iget-object v5, v3, LX/AQ9;->e:LX/AQ4;

    move-object v3, v5

    .line 1670407
    if-eqz v3, :cond_b

    .line 1670408
    check-cast v2, LX/0in;

    invoke-interface {v2}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AQ9;

    .line 1670409
    iget-object v3, v2, LX/AQ9;->e:LX/AQ4;

    move-object v2, v3

    .line 1670410
    invoke-interface {v2}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_5

    .line 1670411
    :cond_b
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j3;

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isGroupMemberBioPost()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1670412
    iget-object v2, v1, LX/APv;->b:Landroid/content/res/Resources;

    const v3, 0x7f081402

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 1670413
    :cond_c
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j3;

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1670414
    iget-object v2, v1, LX/APv;->b:Landroid/content/res/Resources;

    const v3, 0x7f081401

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 1670415
    :cond_d
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j3;

    check-cast v3, LX/0j4;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1670416
    iget-object v2, v1, LX/APv;->b:Landroid/content/res/Resources;

    const v3, 0x7f0813fc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 1670417
    :cond_e
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v2

    sget-object v3, LX/2rt;->SHARE:LX/2rt;

    if-ne v2, v3, :cond_f

    .line 1670418
    iget-object v2, v1, LX/APv;->d:LX/0ad;

    sget-char v3, LX/1EB;->am:C

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1670419
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1670420
    :cond_f
    iget-object v2, v1, LX/APv;->b:Landroid/content/res/Resources;

    const v3, 0x7f0813f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 1670421
    :pswitch_1
    iget-object v2, v1, LX/APv;->b:Landroid/content/res/Resources;

    const v3, 0x7f081452

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 1670422
    :pswitch_2
    iget-object v2, v1, LX/APv;->b:Landroid/content/res/Resources;

    const v3, 0x7f081454

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static d(LX/APm;)Z
    .locals 2

    .prologue
    .line 1670349
    iget-object v0, p0, LX/APm;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 1670350
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2zG;

    check-cast v1, LX/2zG;

    invoke-virtual {v1}, LX/2zG;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j6;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 1670344
    sget-object v0, LX/APl;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1670345
    :goto_0
    return-void

    .line 1670346
    :pswitch_0
    invoke-direct {p0}, LX/APm;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1670347
    invoke-direct {p0}, LX/APm;->b()V

    .line 1670348
    return-void
.end method
