.class public final LX/9YA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1503757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;
    .locals 20

    .prologue
    .line 1503758
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1503759
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9YA;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1503760
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9YA;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1503761
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9YA;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1503762
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9YA;->d:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1503763
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9YA;->e:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    invoke-virtual {v1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1503764
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9YA;->f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-static {v1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1503765
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9YA;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1503766
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9YA;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1503767
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9YA;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1503768
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9YA;->j:Ljava/lang/String;

    invoke-virtual {v1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1503769
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9YA;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1503770
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9YA;->l:Ljava/lang/String;

    invoke-virtual {v1, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1503771
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9YA;->m:Ljava/lang/String;

    invoke-virtual {v1, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1503772
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9YA;->n:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    invoke-static {v1, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1503773
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9YA;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1503774
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9YA;->p:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1503775
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9YA;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1503776
    const/16 v19, 0x11

    move/from16 v0, v19

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1503777
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1503778
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1503779
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1503780
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1503781
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1503782
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1503783
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1503784
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1503785
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1503786
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1503787
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1503788
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1503789
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1503790
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1503791
    const/16 v2, 0xe

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1503792
    const/16 v2, 0xf

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1503793
    const/16 v2, 0x10

    move/from16 v0, v18

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1503794
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1503795
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1503796
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1503797
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1503798
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1503799
    new-instance v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-direct {v2, v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;-><init>(LX/15i;)V

    .line 1503800
    return-object v2
.end method
