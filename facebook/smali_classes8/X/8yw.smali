.class public final LX/8yw;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/8yw;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8yu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/8yx;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1426740
    const/4 v0, 0x0

    sput-object v0, LX/8yw;->a:LX/8yw;

    .line 1426741
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8yw;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1426737
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1426738
    new-instance v0, LX/8yx;

    invoke-direct {v0}, LX/8yx;-><init>()V

    iput-object v0, p0, LX/8yw;->c:LX/8yx;

    .line 1426739
    return-void
.end method

.method public static c(LX/1De;)LX/8yu;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1426729
    new-instance v1, LX/8yv;

    invoke-direct {v1}, LX/8yv;-><init>()V

    .line 1426730
    sget-object v2, LX/8yw;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8yu;

    .line 1426731
    if-nez v2, :cond_0

    .line 1426732
    new-instance v2, LX/8yu;

    invoke-direct {v2}, LX/8yu;-><init>()V

    .line 1426733
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/8yu;->a$redex0(LX/8yu;LX/1De;IILX/8yv;)V

    .line 1426734
    move-object v1, v2

    .line 1426735
    move-object v0, v1

    .line 1426736
    return-object v0
.end method

.method public static declared-synchronized q()LX/8yw;
    .locals 2

    .prologue
    .line 1426716
    const-class v1, LX/8yw;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/8yw;->a:LX/8yw;

    if-nez v0, :cond_0

    .line 1426717
    new-instance v0, LX/8yw;

    invoke-direct {v0}, LX/8yw;-><init>()V

    sput-object v0, LX/8yw;->a:LX/8yw;

    .line 1426718
    :cond_0
    sget-object v0, LX/8yw;->a:LX/8yw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1426719
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1426727
    invoke-static {}, LX/1dS;->b()V

    .line 1426728
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1426721
    check-cast p4, LX/8yv;

    .line 1426722
    iget-object v3, p4, LX/8yv;->a:LX/1X1;

    iget-object v4, p4, LX/8yv;->b:LX/0QR;

    iget v5, p4, LX/8yv;->c:I

    move-object v0, p1

    move v1, p2

    move v2, p3

    .line 1426723
    invoke-static {v0, v1, v2, v3, v5}, LX/8yx;->a(LX/1De;IILX/1X1;I)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1426724
    :goto_0
    invoke-static {v0, v3}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1426725
    return-object v0

    .line 1426726
    :cond_0
    invoke-interface {v4}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1X1;

    move-object v3, p0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1426720
    const/4 v0, 0x1

    return v0
.end method
