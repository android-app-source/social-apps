.class public final enum LX/9c2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9c2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9c2;

.field public static final enum ATTACHMENT_ADDED:LX/9c2;

.field public static final enum ATTACHMENT_REMOVED:LX/9c2;

.field public static final enum ENTER_STICKER_STORE:LX/9c2;

.field public static final enum STICKER_ADDED:LX/9c2;

.field public static final enum STICKER_EDIT_FLOW:LX/9c2;

.field public static final enum STICKER_PICKER_CLOSED:LX/9c2;

.field public static final enum STICKER_PICKER_OPENED:LX/9c2;

.field public static final enum STICKER_REMOVED:LX/9c2;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1515774
    new-instance v0, LX/9c2;

    const-string v1, "STICKER_EDIT_FLOW"

    const-string v2, "sticker_edit_flow"

    invoke-direct {v0, v1, v4, v2}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->STICKER_EDIT_FLOW:LX/9c2;

    .line 1515775
    new-instance v0, LX/9c2;

    const-string v1, "STICKER_REMOVED"

    const-string v2, "sticker_removed"

    invoke-direct {v0, v1, v5, v2}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->STICKER_REMOVED:LX/9c2;

    .line 1515776
    new-instance v0, LX/9c2;

    const-string v1, "STICKER_ADDED"

    const-string v2, "sticker_added"

    invoke-direct {v0, v1, v6, v2}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->STICKER_ADDED:LX/9c2;

    .line 1515777
    new-instance v0, LX/9c2;

    const-string v1, "STICKER_PICKER_OPENED"

    const-string v2, "sticker_picker_opened"

    invoke-direct {v0, v1, v7, v2}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->STICKER_PICKER_OPENED:LX/9c2;

    .line 1515778
    new-instance v0, LX/9c2;

    const-string v1, "STICKER_PICKER_CLOSED"

    const-string v2, "sticker_picker_closed"

    invoke-direct {v0, v1, v8, v2}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->STICKER_PICKER_CLOSED:LX/9c2;

    .line 1515779
    new-instance v0, LX/9c2;

    const-string v1, "ENTER_STICKER_STORE"

    const/4 v2, 0x5

    const-string v3, "enter_sticker_store"

    invoke-direct {v0, v1, v2, v3}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->ENTER_STICKER_STORE:LX/9c2;

    .line 1515780
    new-instance v0, LX/9c2;

    const-string v1, "ATTACHMENT_ADDED"

    const/4 v2, 0x6

    const-string v3, "attachment_added"

    invoke-direct {v0, v1, v2, v3}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->ATTACHMENT_ADDED:LX/9c2;

    .line 1515781
    new-instance v0, LX/9c2;

    const-string v1, "ATTACHMENT_REMOVED"

    const/4 v2, 0x7

    const-string v3, "attachment_removed"

    invoke-direct {v0, v1, v2, v3}, LX/9c2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c2;->ATTACHMENT_REMOVED:LX/9c2;

    .line 1515782
    const/16 v0, 0x8

    new-array v0, v0, [LX/9c2;

    sget-object v1, LX/9c2;->STICKER_EDIT_FLOW:LX/9c2;

    aput-object v1, v0, v4

    sget-object v1, LX/9c2;->STICKER_REMOVED:LX/9c2;

    aput-object v1, v0, v5

    sget-object v1, LX/9c2;->STICKER_ADDED:LX/9c2;

    aput-object v1, v0, v6

    sget-object v1, LX/9c2;->STICKER_PICKER_OPENED:LX/9c2;

    aput-object v1, v0, v7

    sget-object v1, LX/9c2;->STICKER_PICKER_CLOSED:LX/9c2;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9c2;->ENTER_STICKER_STORE:LX/9c2;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9c2;->ATTACHMENT_ADDED:LX/9c2;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9c2;->ATTACHMENT_REMOVED:LX/9c2;

    aput-object v2, v0, v1

    sput-object v0, LX/9c2;->$VALUES:[LX/9c2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1515769
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1515770
    iput-object p3, p0, LX/9c2;->name:Ljava/lang/String;

    .line 1515771
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9c2;
    .locals 1

    .prologue
    .line 1515773
    const-class v0, LX/9c2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9c2;

    return-object v0
.end method

.method public static values()[LX/9c2;
    .locals 1

    .prologue
    .line 1515772
    sget-object v0, LX/9c2;->$VALUES:[LX/9c2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9c2;

    return-object v0
.end method
