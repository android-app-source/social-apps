.class public final enum LX/9XG;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9XG;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9XG;

.field public static final enum EVENT_GLOBAL_PAGE_REDIRECTION:LX/9XG;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1502601
    new-instance v0, LX/9XG;

    const-string v1, "EVENT_GLOBAL_PAGE_REDIRECTION"

    const-string v2, "android_global_page_redirection"

    invoke-direct {v0, v1, v3, v2}, LX/9XG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XG;->EVENT_GLOBAL_PAGE_REDIRECTION:LX/9XG;

    .line 1502602
    const/4 v0, 0x1

    new-array v0, v0, [LX/9XG;

    sget-object v1, LX/9XG;->EVENT_GLOBAL_PAGE_REDIRECTION:LX/9XG;

    aput-object v1, v0, v3

    sput-object v0, LX/9XG;->$VALUES:[LX/9XG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502603
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502604
    iput-object p3, p0, LX/9XG;->mEventName:Ljava/lang/String;

    .line 1502605
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9XG;
    .locals 1

    .prologue
    .line 1502606
    const-class v0, LX/9XG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9XG;

    return-object v0
.end method

.method public static values()[LX/9XG;
    .locals 1

    .prologue
    .line 1502607
    sget-object v0, LX/9XG;->$VALUES:[LX/9XG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9XG;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502608
    iget-object v0, p0, LX/9XG;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502609
    sget-object v0, LX/9XC;->REDIRECTION:LX/9XC;

    return-object v0
.end method
