.class public final LX/9er;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520651
    iput-object p1, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1520640
    iget-object v0, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    if-eqz v0, :cond_0

    .line 1520641
    iget-object v0, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    iget-object v1, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-interface {v0, v1}, LX/9el;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1520642
    :cond_0
    iget-object v0, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    .line 1520643
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1520644
    check-cast p1, Landroid/net/Uri;

    .line 1520645
    iget-object v0, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    .line 1520646
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520647
    iget-object v0, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    if-eqz v0, :cond_0

    .line 1520648
    iget-object v0, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    iget-object v1, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-interface {v0, v1}, LX/9el;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1520649
    :cond_0
    iget-object v0, p0, LX/9er;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    .line 1520650
    return-void
.end method
