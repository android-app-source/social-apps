.class public final LX/ATN;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/ATO;


# direct methods
.method public constructor <init>(LX/ATO;)V
    .locals 0

    .prologue
    .line 1675554
    iput-object p1, p0, LX/ATN;->a:LX/ATO;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 1675555
    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->b:Ljava/util/List;

    iget-object v1, p0, LX/ATN;->a:LX/ATO;

    iget v1, v1, LX/ATO;->I:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-interface {v0, v1}, LX/ASn;->a(F)V

    .line 1675556
    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    invoke-static {v0}, LX/ATO;->t(LX/ATO;)V

    .line 1675557
    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->z:LX/ATl;

    invoke-virtual {v0}, LX/ATl;->requestLayout()V

    .line 1675558
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 3

    .prologue
    .line 1675559
    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    iget-boolean v0, v0, LX/ATO;->Q:Z

    if-eqz v0, :cond_0

    .line 1675560
    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    iget-object v1, v0, LX/ATO;->B:LX/ASe;

    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->a:Ljava/util/List;

    iget-object v2, p0, LX/ATN;->a:LX/ATO;

    iget v2, v2, LX/ATO;->I:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-interface {v1, v0}, LX/ASe;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1675561
    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->l:LX/ATQ;

    .line 1675562
    iget-object v1, v0, LX/ATQ;->a:LX/73w;

    const/4 v0, 0x0

    .line 1675563
    sget-object v2, LX/74R;->COMPOSER_ATTACHMENT_REMOVED:LX/74R;

    invoke-static {v1, v2, v0, v0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1675564
    :cond_0
    iget-object v0, p0, LX/ATN;->a:LX/ATO;

    const/4 v1, -0x1

    .line 1675565
    iput v1, v0, LX/ATO;->I:I

    .line 1675566
    return-void
.end method
