.class public final LX/9UF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;)V
    .locals 0

    .prologue
    .line 1497740
    iput-object p1, p0, LX/9UF;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1497750
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1497749
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1497741
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1497742
    iget-object v1, p0, LX/9UF;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    .line 1497743
    iput-object v0, v1, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->j:Ljava/lang/String;

    .line 1497744
    iget-object v0, p0, LX/9UF;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    check-cast v0, LX/9UD;

    iget-object v0, v0, LX/9UC;->h:Landroid/widget/Filter;

    iget-object v1, p0, LX/9UF;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1497745
    iget-object v0, p0, LX/9UF;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->k:LX/9UZ;

    .line 1497746
    iget-object v1, v0, LX/9UZ;->a:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v1

    .line 1497747
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setFastScrollEnabled(Z)V

    .line 1497748
    return-void
.end method
