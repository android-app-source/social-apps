.class public LX/9BL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3sI;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9BL;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1451622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1451623
    return-void
.end method

.method public static a(LX/0QB;)LX/9BL;
    .locals 3

    .prologue
    .line 1451624
    sget-object v0, LX/9BL;->a:LX/9BL;

    if-nez v0, :cond_1

    .line 1451625
    const-class v1, LX/9BL;

    monitor-enter v1

    .line 1451626
    :try_start_0
    sget-object v0, LX/9BL;->a:LX/9BL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1451627
    if-eqz v2, :cond_0

    .line 1451628
    :try_start_1
    new-instance v0, LX/9BL;

    invoke-direct {v0}, LX/9BL;-><init>()V

    .line 1451629
    move-object v0, v0

    .line 1451630
    sput-object v0, LX/9BL;->a:LX/9BL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1451631
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1451632
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1451633
    :cond_1
    sget-object v0, LX/9BL;->a:LX/9BL;

    return-object v0

    .line 1451634
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1451635
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;F)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1451636
    const/high16 v0, -0x40800000    # -1.0f

    cmpg-float v0, p2, v0

    if-ltz v0, :cond_0

    .line 1451637
    cmpg-float v0, p2, v1

    if-gtz v0, :cond_0

    .line 1451638
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1451639
    mul-float/2addr v0, v1

    sub-float v0, v1, v0

    .line 1451640
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1451641
    :goto_0
    return-void

    .line 1451642
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method
