.class public final enum LX/9ad;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9ad;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9ad;

.field public static final enum CANCELLATION:LX/9ad;

.field public static final enum NO_ALBUM_TITLE:LX/9ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1513907
    new-instance v0, LX/9ad;

    const-string v1, "CANCELLATION"

    invoke-direct {v0, v1, v2}, LX/9ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9ad;->CANCELLATION:LX/9ad;

    .line 1513908
    new-instance v0, LX/9ad;

    const-string v1, "NO_ALBUM_TITLE"

    invoke-direct {v0, v1, v3}, LX/9ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9ad;->NO_ALBUM_TITLE:LX/9ad;

    .line 1513909
    const/4 v0, 0x2

    new-array v0, v0, [LX/9ad;

    sget-object v1, LX/9ad;->CANCELLATION:LX/9ad;

    aput-object v1, v0, v2

    sget-object v1, LX/9ad;->NO_ALBUM_TITLE:LX/9ad;

    aput-object v1, v0, v3

    sput-object v0, LX/9ad;->$VALUES:[LX/9ad;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1513910
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9ad;
    .locals 1

    .prologue
    .line 1513911
    const-class v0, LX/9ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9ad;

    return-object v0
.end method

.method public static values()[LX/9ad;
    .locals 1

    .prologue
    .line 1513912
    sget-object v0, LX/9ad;->$VALUES:[LX/9ad;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9ad;

    return-object v0
.end method
