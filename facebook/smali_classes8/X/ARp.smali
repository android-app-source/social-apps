.class public final LX/ARp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Hqt;

.field public final synthetic b:LX/2rw;

.field public final synthetic c:LX/ARv;


# direct methods
.method public constructor <init>(LX/ARv;LX/Hqt;LX/2rw;)V
    .locals 0

    .prologue
    .line 1673312
    iput-object p1, p0, LX/ARp;->c:LX/ARv;

    iput-object p2, p0, LX/ARp;->a:LX/Hqt;

    iput-object p3, p0, LX/ARp;->b:LX/2rw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 1673313
    iget-object v1, p0, LX/ARp;->a:LX/Hqt;

    iget-object v2, p0, LX/ARp;->b:LX/2rw;

    iget-object v0, p0, LX/ARp;->c:LX/ARv;

    iget-object v0, v0, LX/ARv;->v:LX/0P1;

    iget-object v3, p0, LX/ARp;->b:LX/2rw;

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARo;

    iget-object v0, v0, LX/ARo;->e:Ljava/lang/Class;

    const/4 p0, 0x0

    .line 1673314
    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    if-eq v2, v3, :cond_0

    .line 1673315
    iget-object v3, v1, LX/Hqt;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v3, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    sget-object v4, LX/5Rn;->NORMAL:LX/5Rn;

    invoke-virtual {v3, v4}, LX/0jL;->a(LX/5Rn;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3, p0}, LX/0jL;->a(Ljava/lang/Long;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3}, LX/0jL;->a()V

    .line 1673316
    :cond_0
    sget-object v3, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v2, v3, :cond_2

    .line 1673317
    iget-object v3, v1, LX/Hqt;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq v3, v4, :cond_1

    .line 1673318
    iget-object v3, v1, LX/Hqt;->a:Lcom/facebook/composer/activity/ComposerFragment;

    sget-object v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v3, v4, p0}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)V

    .line 1673319
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1673320
    :cond_2
    if-nez v0, :cond_3

    .line 1673321
    iget-object v3, v1, LX/Hqt;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->aG:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    const-string v4, "no_activity_for_composer_target_selection"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Couldn\'t find activity for target type: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1673322
    :cond_3
    new-instance v3, Landroid/content/Intent;

    iget-object v4, v1, LX/Hqt;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1673323
    iget-object v4, v1, LX/Hqt;->a:Lcom/facebook/composer/activity/ComposerFragment;

    const/4 p0, 0x3

    invoke-virtual {v4, v3, p0}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
