.class public final enum LX/AMS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AMS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AMS;

.field public static final enum EFFECT:LX/AMS;

.field public static final enum SUPPORT:LX/AMS;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1666222
    new-instance v0, LX/AMS;

    const-string v1, "EFFECT"

    invoke-direct {v0, v1, v2}, LX/AMS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AMS;->EFFECT:LX/AMS;

    .line 1666223
    new-instance v0, LX/AMS;

    const-string v1, "SUPPORT"

    invoke-direct {v0, v1, v3}, LX/AMS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AMS;->SUPPORT:LX/AMS;

    .line 1666224
    const/4 v0, 0x2

    new-array v0, v0, [LX/AMS;

    sget-object v1, LX/AMS;->EFFECT:LX/AMS;

    aput-object v1, v0, v2

    sget-object v1, LX/AMS;->SUPPORT:LX/AMS;

    aput-object v1, v0, v3

    sput-object v0, LX/AMS;->$VALUES:[LX/AMS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1666225
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AMS;
    .locals 1

    .prologue
    .line 1666226
    const-class v0, LX/AMS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AMS;

    return-object v0
.end method

.method public static values()[LX/AMS;
    .locals 1

    .prologue
    .line 1666227
    sget-object v0, LX/AMS;->$VALUES:[LX/AMS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AMS;

    return-object v0
.end method
