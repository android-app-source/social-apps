.class public final LX/9W7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowForMessageThreadQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9WC;


# direct methods
.method public constructor <init>(LX/9WC;)V
    .locals 0

    .prologue
    .line 1500611
    iput-object p1, p0, LX/9W7;->a:LX/9WC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1500612
    iget-object v0, p0, LX/9W7;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->s:LX/9Vf;

    invoke-virtual {v0}, LX/9Vf;->c()V

    .line 1500613
    iget-object v0, p0, LX/9W7;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->d:LX/03V;

    sget-object v1, LX/9WC;->b:Ljava/lang/String;

    const-string v2, "NFX flow step fetch failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1500614
    iget-object v0, p0, LX/9W7;->a:LX/9WC;

    sget-object v1, LX/9WJ;->NETWORK_ERROR:LX/9WJ;

    invoke-static {v0, v1}, LX/9WC;->a$redex0(LX/9WC;LX/9WJ;)V

    .line 1500615
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1500616
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1500617
    iget-object v0, p0, LX/9W7;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->s:LX/9Vf;

    invoke-virtual {v0}, LX/9Vf;->b()V

    .line 1500618
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1500619
    if-eqz v0, :cond_0

    .line 1500620
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1500621
    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowForMessageThreadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowForMessageThreadQueryModel;->a()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1500622
    :cond_0
    iget-object v0, p0, LX/9W7;->a:LX/9WC;

    sget-object v1, LX/9WJ;->DATA_ERROR:LX/9WJ;

    invoke-static {v0, v1}, LX/9WC;->a$redex0(LX/9WC;LX/9WJ;)V

    .line 1500623
    :goto_0
    return-void

    .line 1500624
    :cond_1
    iget-object v1, p0, LX/9W7;->a:LX/9WC;

    .line 1500625
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1500626
    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowForMessageThreadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackFlowForMessageThreadQueryModel;->a()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;

    move-result-object v0

    invoke-static {v1, v0}, LX/9WC;->a(LX/9WC;Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;)V

    goto :goto_0
.end method
