.class public final LX/9OQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Z

.field public B:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPostTopics"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPostTopics"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "learningCourseUnit"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "learningCourseUnit"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Z

.field public u:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "suggestedPurpose"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "suggestedPurpose"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1480250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1480251
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;
    .locals 4

    .prologue
    .line 1480252
    new-instance v0, LX/9OQ;

    invoke-direct {v0}, LX/9OQ;-><init>()V

    .line 1480253
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 1480254
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->b:Z

    .line 1480255
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->c:Z

    .line 1480256
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->d:Z

    .line 1480257
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    .line 1480258
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    .line 1480259
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1480260
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    .line 1480261
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    .line 1480262
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/9OQ;->j:LX/15i;

    iput v1, v0, LX/9OQ;->k:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1480263
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->l:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1480264
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->m:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1480265
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->n:Ljava/lang/String;

    .line 1480266
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->o:Z

    .line 1480267
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/9OQ;->p:LX/15i;

    iput v1, v0, LX/9OQ;->q:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1480268
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1480269
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->s:Z

    .line 1480270
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->t:Z

    .line 1480271
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->u:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    .line 1480272
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->v:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1480273
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iput-object v2, v0, LX/9OQ;->w:LX/15i;

    iput v1, v0, LX/9OQ;->x:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1480274
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->y:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    .line 1480275
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r()I

    move-result v1

    iput v1, v0, LX/9OQ;->z:I

    .line 1480276
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->s()Z

    move-result v1

    iput-boolean v1, v0, LX/9OQ;->A:Z

    .line 1480277
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->B:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    .line 1480278
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->C:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1480279
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->D:Ljava/lang/String;

    .line 1480280
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->E:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    .line 1480281
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1480282
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v1

    iput-object v1, v0, LX/9OQ;->G:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 1480283
    return-object v0

    .line 1480284
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1480285
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1480286
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;
    .locals 27

    .prologue
    .line 1480287
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1480288
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1480289
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OQ;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    invoke-static {v3, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1480290
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9OQ;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    invoke-static {v3, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1480291
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9OQ;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    invoke-static {v3, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1480292
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9OQ;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    invoke-static {v3, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1480293
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9OQ;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    invoke-static {v3, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1480294
    sget-object v10, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9OQ;->j:LX/15i;

    move-object/from16 v0, p0

    iget v12, v0, LX/9OQ;->k:I

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v10, -0x4d1fbe34

    invoke-static {v11, v12, v10}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v10

    invoke-static {v3, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1480295
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9OQ;->l:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    invoke-static {v3, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1480296
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9OQ;->m:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    invoke-static {v3, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1480297
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9OQ;->n:Ljava/lang/String;

    invoke-virtual {v3, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1480298
    sget-object v14, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v14

    :try_start_1
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9OQ;->p:LX/15i;

    move-object/from16 v0, p0

    iget v0, v0, LX/9OQ;->q:I

    move/from16 v16, v0

    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v14, 0x722fec04

    move/from16 v0, v16

    invoke-static {v15, v0, v14}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v14

    invoke-static {v3, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1480299
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9OQ;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-static {v3, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1480300
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->u:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1480301
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->v:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1480302
    sget-object v18, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v18

    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->w:LX/15i;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9OQ;->x:I

    move/from16 v20, v0

    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v18, 0x6da0c94

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1480303
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->y:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1480304
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->B:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1480305
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->C:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    .line 1480306
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->D:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 1480307
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->E:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1480308
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    .line 1480309
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9OQ;->G:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    .line 1480310
    const/16 v26, 0x1e

    move/from16 v0, v26

    invoke-virtual {v3, v0}, LX/186;->c(I)V

    .line 1480311
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0, v4}, LX/186;->b(II)V

    .line 1480312
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9OQ;->b:Z

    move/from16 v26, v0

    move/from16 v0, v26

    invoke-virtual {v3, v4, v0}, LX/186;->a(IZ)V

    .line 1480313
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9OQ;->c:Z

    move/from16 v26, v0

    move/from16 v0, v26

    invoke-virtual {v3, v4, v0}, LX/186;->a(IZ)V

    .line 1480314
    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9OQ;->d:Z

    move/from16 v26, v0

    move/from16 v0, v26

    invoke-virtual {v3, v4, v0}, LX/186;->a(IZ)V

    .line 1480315
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v5}, LX/186;->b(II)V

    .line 1480316
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 1480317
    const/4 v4, 0x6

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 1480318
    const/4 v4, 0x7

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 1480319
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 1480320
    const/16 v4, 0x9

    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 1480321
    const/16 v4, 0xa

    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 1480322
    const/16 v4, 0xb

    invoke-virtual {v3, v4, v12}, LX/186;->b(II)V

    .line 1480323
    const/16 v4, 0xc

    invoke-virtual {v3, v4, v13}, LX/186;->b(II)V

    .line 1480324
    const/16 v4, 0xd

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9OQ;->o:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1480325
    const/16 v4, 0xe

    invoke-virtual {v3, v4, v14}, LX/186;->b(II)V

    .line 1480326
    const/16 v4, 0xf

    invoke-virtual {v3, v4, v15}, LX/186;->b(II)V

    .line 1480327
    const/16 v4, 0x10

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9OQ;->s:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1480328
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9OQ;->t:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1480329
    const/16 v4, 0x12

    move/from16 v0, v16

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480330
    const/16 v4, 0x13

    move/from16 v0, v17

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480331
    const/16 v4, 0x14

    move/from16 v0, v18

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480332
    const/16 v4, 0x15

    move/from16 v0, v19

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480333
    const/16 v4, 0x16

    move-object/from16 v0, p0

    iget v5, v0, LX/9OQ;->z:I

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, LX/186;->a(III)V

    .line 1480334
    const/16 v4, 0x17

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/9OQ;->A:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 1480335
    const/16 v4, 0x18

    move/from16 v0, v20

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480336
    const/16 v4, 0x19

    move/from16 v0, v21

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480337
    const/16 v4, 0x1a

    move/from16 v0, v22

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480338
    const/16 v4, 0x1b

    move/from16 v0, v23

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480339
    const/16 v4, 0x1c

    move/from16 v0, v24

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480340
    const/16 v4, 0x1d

    move/from16 v0, v25

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 1480341
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 1480342
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 1480343
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1480344
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1480345
    new-instance v3, LX/15i;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1480346
    new-instance v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {v4, v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;-><init>(LX/15i;)V

    .line 1480347
    return-object v4

    .line 1480348
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 1480349
    :catchall_1
    move-exception v3

    :try_start_4
    monitor-exit v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3

    .line 1480350
    :catchall_2
    move-exception v3

    :try_start_5
    monitor-exit v18
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v3
.end method
