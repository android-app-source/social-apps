.class public LX/9hy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMedia;",
        "LX/4XC;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1527100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527101
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/9hy;->a:Ljava/lang/String;

    .line 1527102
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527103
    iget-object v0, p0, LX/9hy;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1527104
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMedia;

    check-cast p2, LX/4XC;

    .line 1527105
    iget-object v0, p0, LX/9hy;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1527106
    :goto_0
    return-void

    .line 1527107
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/4XC;->a(Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527108
    const-class v0, Lcom/facebook/graphql/model/GraphQLMedia;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1527109
    const-string v0, "RejectPlaceMutatingVisitor"

    return-object v0
.end method
