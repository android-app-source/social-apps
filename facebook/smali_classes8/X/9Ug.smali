.class public LX/9Ug;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements LX/9Uc;
.implements LX/9Uf;


# instance fields
.field public final a:LX/3K3;

.field private final b:Landroid/graphics/Paint;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Ue;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/Matrix;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/9Uh;

.field public final f:Landroid/graphics/Matrix;

.field public final g:Landroid/graphics/Matrix;

.field public final h:Landroid/graphics/Matrix;

.field private i:I

.field private j:I

.field private k:F

.field private l:F

.field private m:F

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/9Ud;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/20u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/9Uk;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1498399
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1498400
    new-instance v0, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    .line 1498401
    iput-boolean v1, p0, LX/9Ug;->o:Z

    .line 1498402
    iget-object v0, p1, LX/9Uk;->a:LX/3K3;

    move-object v0, v0

    .line 1498403
    iput-object v0, p0, LX/9Ug;->a:LX/3K3;

    .line 1498404
    iget-object v0, p1, LX/9Uk;->c:LX/9Uj;

    move-object v0, v0

    .line 1498405
    iget-object v2, v0, LX/9Uj;->b:Ljava/util/Map;

    move-object v0, v2

    .line 1498406
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/9Ug;->n:Ljava/util/Map;

    .line 1498407
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9Ug;->f:Landroid/graphics/Matrix;

    .line 1498408
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    .line 1498409
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9Ug;->h:Landroid/graphics/Matrix;

    .line 1498410
    iget-object v0, p0, LX/9Ug;->a:LX/3K3;

    .line 1498411
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1498412
    if-eqz v2, :cond_3

    .line 1498413
    new-instance v2, LX/9Ui;

    .line 1498414
    iget v3, v0, LX/3K3;->a:I

    move v3, v3

    .line 1498415
    iget v4, v0, LX/3K3;->b:I

    move v4, v4

    .line 1498416
    invoke-direct {v2, p0, v3, v4}, LX/9Ui;-><init>(LX/9Uf;II)V

    .line 1498417
    :goto_2
    move-object v0, v2

    .line 1498418
    iput-object v0, p0, LX/9Ug;->e:LX/9Uh;

    .line 1498419
    iget-object v0, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1498420
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1498421
    iget-object v0, p0, LX/9Ug;->a:LX/3K3;

    .line 1498422
    iget-object v2, v0, LX/3K3;->c:Ljava/util/List;

    move-object v0, v2

    .line 1498423
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_1

    .line 1498424
    new-instance v5, LX/9Ue;

    iget-object v0, p0, LX/9Ug;->a:LX/3K3;

    .line 1498425
    iget-object v6, v0, LX/3K3;->c:Ljava/util/List;

    move-object v0, v6

    .line 1498426
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3K1;

    invoke-direct {v5, p0, v0}, LX/9Ue;-><init>(LX/9Ug;LX/3K1;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1498427
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1498428
    :cond_0
    iget-object v0, p1, LX/9Uk;->c:LX/9Uj;

    move-object v0, v0

    .line 1498429
    iget-object v2, v0, LX/9Uj;->b:Ljava/util/Map;

    move-object v0, v2

    .line 1498430
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 1498431
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/9Ug;->c:Ljava/util/List;

    .line 1498432
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/9Ug;->d:Landroid/util/SparseArray;

    .line 1498433
    iget-object v0, p0, LX/9Ug;->a:LX/3K3;

    .line 1498434
    iget-object v2, v0, LX/3K3;->d:Ljava/util/List;

    move-object v2, v2

    .line 1498435
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    :goto_4
    if-ge v1, v3, :cond_2

    .line 1498436
    iget-object v4, p0, LX/9Ug;->d:Landroid/util/SparseArray;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jl;

    .line 1498437
    iget v5, v0, LX/3Jl;->a:I

    move v0, v5

    .line 1498438
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v4, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1498439
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1498440
    :cond_2
    iget v0, p1, LX/9Uk;->b:I

    move v0, v0

    .line 1498441
    iget-object v6, p0, LX/9Ug;->e:LX/9Uh;

    .line 1498442
    const/16 v7, 0x3e8

    div-int/2addr v7, v0

    int-to-long v7, v7

    iput-wide v7, v6, LX/9Uh;->g:J

    .line 1498443
    return-void

    :cond_3
    new-instance v2, Lcom/facebook/keyframes/KeyframesDrawableAnimationCallback$RunnableFaceAnimationCallback;

    .line 1498444
    iget v3, v0, LX/3K3;->a:I

    move v3, v3

    .line 1498445
    iget v4, v0, LX/3K3;->b:I

    move v4, v4

    .line 1498446
    invoke-direct {v2, p0, v3, v4}, Lcom/facebook/keyframes/KeyframesDrawableAnimationCallback$RunnableFaceAnimationCallback;-><init>(LX/9Uf;II)V

    goto/16 :goto_2

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method private a(Landroid/graphics/Canvas;LX/9Ua;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 1498394
    iget-object v0, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    invoke-virtual {p2, v0}, LX/9Ua;->a(Landroid/graphics/Matrix;)V

    .line 1498395
    iget-object v0, p2, LX/9Ua;->a:Landroid/graphics/Path;

    move-object v0, v0

    .line 1498396
    invoke-virtual {p1, v0, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1498397
    iget-object v0, p0, LX/9Ug;->h:Landroid/graphics/Matrix;

    invoke-virtual {p2, v0}, LX/9Ua;->a(Landroid/graphics/Matrix;)V

    .line 1498398
    return-void
.end method

.method private b(F)V
    .locals 11

    .prologue
    .line 1498307
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9Ug;->o:Z

    .line 1498308
    iget-object v0, p0, LX/9Ug;->a:LX/3K3;

    iget-object v1, p0, LX/9Ug;->d:Landroid/util/SparseArray;

    const/4 v6, 0x0

    .line 1498309
    iget-object v2, v0, LX/3K3;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    move v7, v6

    .line 1498310
    :goto_0
    if-ge v7, v8, :cond_3

    .line 1498311
    iget-object v2, v0, LX/3K3;->d:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Jl;

    .line 1498312
    iget v3, v2, LX/3Jl;->a:I

    move v3, v3

    .line 1498313
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Matrix;

    .line 1498314
    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 1498315
    invoke-virtual {v2}, LX/3Jl;->d()LX/9Ul;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1498316
    invoke-virtual {v2}, LX/3Jl;->d()LX/9Ul;

    move-result-object v4

    invoke-virtual {v4, p1, v3}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 1498317
    :cond_0
    iget-object v4, v2, LX/3Jl;->c:Ljava/util/List;

    move-object v4, v4

    .line 1498318
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    move v5, v6

    .line 1498319
    :goto_1
    if-ge v5, v9, :cond_1

    .line 1498320
    iget-object v4, v2, LX/3Jl;->c:Ljava/util/List;

    move-object v4, v4

    .line 1498321
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3Jf;

    .line 1498322
    iget-object v10, v4, LX/3Jf;->f:LX/3Jj;

    move-object v4, v10

    .line 1498323
    invoke-virtual {v4, p1, v3}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 1498324
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 1498325
    :cond_1
    iget v4, v2, LX/3Jl;->b:I

    move v4, v4

    .line 1498326
    if-lez v4, :cond_2

    .line 1498327
    iget v4, v2, LX/3Jl;->b:I

    move v2, v4

    .line 1498328
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1498329
    :cond_2
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_0

    .line 1498330
    :cond_3
    const/4 v0, 0x0

    iget-object v1, p0, LX/9Ug;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_6

    .line 1498331
    iget-object v0, p0, LX/9Ug;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ue;

    .line 1498332
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    .line 1498333
    iget v4, v3, LX/3K1;->h:F

    move v3, v4

    .line 1498334
    cmpg-float v3, p1, v3

    if-ltz v3, :cond_4

    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    .line 1498335
    iget v4, v3, LX/3K1;->i:F

    move v3, v4

    .line 1498336
    cmpl-float v3, p1, v3

    if-lez v3, :cond_7

    .line 1498337
    :cond_4
    const/4 v3, 0x0

    iput-boolean v3, v0, LX/9Ue;->k:Z

    .line 1498338
    :cond_5
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1498339
    :cond_6
    return-void

    .line 1498340
    :cond_7
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/9Ue;->k:Z

    .line 1498341
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    iget-object v4, v0, LX/9Ue;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4, p1}, LX/3K1;->a(Landroid/graphics/Matrix;F)V

    .line 1498342
    iget-object v3, v0, LX/9Ue;->b:LX/9Ug;

    iget-object v3, v3, LX/9Ug;->d:Landroid/util/SparseArray;

    iget-object v4, v0, LX/9Ue;->c:LX/3K1;

    .line 1498343
    iget v5, v4, LX/3K1;->l:I

    move v4, v5

    .line 1498344
    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Matrix;

    .line 1498345
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1498346
    iget-object v4, v0, LX/9Ue;->h:Landroid/graphics/Matrix;

    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1498347
    :cond_8
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    .line 1498348
    iget-object v4, v3, LX/3K1;->r:LX/3K2;

    move-object v3, v4

    .line 1498349
    invoke-static {v0}, LX/9Ue;->m(LX/9Ue;)Z

    move-result v4

    if-nez v4, :cond_5

    if-eqz v3, :cond_5

    .line 1498350
    iget-object v4, v0, LX/9Ue;->d:LX/9Ua;

    invoke-virtual {v4}, LX/9Ua;->b()V

    .line 1498351
    iget-object v4, v0, LX/9Ue;->d:LX/9Ua;

    invoke-virtual {v3, p1, v4}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 1498352
    iget-object v3, v0, LX/9Ue;->d:LX/9Ua;

    iget-object v4, v0, LX/9Ue;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, LX/9Ua;->a(Landroid/graphics/Matrix;)V

    .line 1498353
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    iget-object v4, v0, LX/9Ue;->f:LX/9Uq;

    .line 1498354
    if-nez v4, :cond_c

    .line 1498355
    :cond_9
    :goto_4
    iget-object v3, v0, LX/9Ue;->f:LX/9Uq;

    iget-object v4, v0, LX/9Ue;->h:Landroid/graphics/Matrix;

    .line 1498356
    iget-object v5, v0, LX/9Ue;->j:[F

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1498357
    iget-object v5, v0, LX/9Ue;->j:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget-object v6, v0, LX/9Ue;->j:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    add-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move v4, v5

    .line 1498358
    iget v5, v3, LX/9Uq;->a:F

    mul-float/2addr v5, v4

    iput v5, v3, LX/9Uq;->a:F

    .line 1498359
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    iget-object v4, v0, LX/9Ue;->g:LX/9Uo;

    .line 1498360
    if-eqz v4, :cond_a

    iget-object v5, v3, LX/3K1;->o:LX/3Jf;

    if-nez v5, :cond_d

    .line 1498361
    :cond_a
    :goto_5
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    .line 1498362
    iget-object v4, v3, LX/3K1;->p:LX/3kN;

    move-object v3, v4

    .line 1498363
    if-eqz v3, :cond_b

    .line 1498364
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    invoke-static {v0, v3}, LX/9Ue;->a(LX/9Ue;LX/3K1;)V

    .line 1498365
    :cond_b
    iget-object v3, v0, LX/9Ue;->l:[Landroid/graphics/Shader;

    if-nez v3, :cond_e

    .line 1498366
    const/4 v3, 0x0

    .line 1498367
    :goto_6
    move-object v3, v3

    .line 1498368
    iput-object v3, v0, LX/9Ue;->m:Landroid/graphics/Shader;

    .line 1498369
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    .line 1498370
    iget-object v4, v3, LX/3K1;->n:LX/3K1;

    move-object v3, v4

    .line 1498371
    if-eqz v3, :cond_5

    .line 1498372
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    .line 1498373
    iget-object v4, v3, LX/3K1;->n:LX/3K1;

    move-object v3, v4

    .line 1498374
    iget-object v4, v0, LX/9Ue;->i:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4, p1}, LX/3K1;->a(Landroid/graphics/Matrix;F)V

    .line 1498375
    iget-object v3, v0, LX/9Ue;->e:LX/9Ua;

    invoke-virtual {v3}, LX/9Ua;->b()V

    .line 1498376
    iget-object v3, v0, LX/9Ue;->c:LX/3K1;

    .line 1498377
    iget-object v4, v3, LX/3K1;->n:LX/3K1;

    move-object v3, v4

    .line 1498378
    iget-object v4, v3, LX/3K1;->r:LX/3K2;

    move-object v3, v4

    .line 1498379
    iget-object v4, v0, LX/9Ue;->e:LX/9Ua;

    invoke-virtual {v3, p1, v4}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 1498380
    iget-object v3, v0, LX/9Ue;->e:LX/9Ua;

    iget-object v4, v0, LX/9Ue;->i:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, LX/9Ua;->a(Landroid/graphics/Matrix;)V

    goto/16 :goto_3

    .line 1498381
    :cond_c
    iget v5, v3, LX/3K1;->g:F

    .line 1498382
    iput v5, v4, LX/9Uq;->a:F

    .line 1498383
    iget-object v5, v3, LX/3K1;->a:LX/3Jf;

    if-eqz v5, :cond_9

    .line 1498384
    iget-object v5, v3, LX/3K1;->a:LX/3Jf;

    .line 1498385
    iget-object v3, v5, LX/3Jf;->f:LX/3Jj;

    move-object v5, v3

    .line 1498386
    invoke-virtual {v5, p1, v4}, LX/3Jj;->a(FLjava/lang/Object;)V

    goto :goto_4

    .line 1498387
    :cond_d
    iget-object v5, v3, LX/3K1;->o:LX/3Jf;

    .line 1498388
    iget-object v3, v5, LX/3Jf;->f:LX/3Jj;

    move-object v5, v3

    .line 1498389
    invoke-virtual {v5, p1, v4}, LX/3Jj;->a(FLjava/lang/Object;)V

    goto :goto_5

    .line 1498390
    :cond_e
    iget-object v3, v0, LX/9Ue;->b:LX/9Ug;

    iget-object v3, v3, LX/9Ug;->a:LX/3K3;

    .line 1498391
    iget v4, v3, LX/3K3;->b:I

    move v3, v4

    .line 1498392
    int-to-float v3, v3

    div-float v3, p1, v3

    iget-object v4, v0, LX/9Ue;->l:[Landroid/graphics/Shader;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1498393
    iget-object v4, v0, LX/9Ue;->l:[Landroid/graphics/Shader;

    aget-object v3, v4, v3

    goto :goto_6
.end method

.method private b(FFLX/9Ub;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1498293
    iget v0, p0, LX/9Ug;->l:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, LX/9Ug;->m:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    .line 1498294
    :goto_0
    return-void

    .line 1498295
    :cond_0
    iget-object v0, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    iget v1, p0, LX/9Ug;->k:F

    iget v2, p0, LX/9Ug;->k:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1498296
    cmpl-float v0, p1, v3

    if-nez v0, :cond_1

    cmpl-float v0, p2, v3

    if-nez v0, :cond_1

    .line 1498297
    iput v3, p0, LX/9Ug;->l:F

    .line 1498298
    iput v3, p0, LX/9Ug;->m:F

    .line 1498299
    iget-object v0, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/9Ug;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    goto :goto_0

    .line 1498300
    :cond_1
    sget-object v0, LX/9Ub;->UP:LX/9Ub;

    if-ne p3, v0, :cond_2

    iget v0, p0, LX/9Ug;->j:I

    int-to-float v0, v0

    .line 1498301
    :goto_1
    iget-object v1, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    iget v2, p0, LX/9Ug;->i:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, LX/9Ug;->j:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, p1, p1, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1498302
    iget-object v1, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    iget v2, p0, LX/9Ug;->i:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, p2, p2, v2, v0}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1498303
    iput p1, p0, LX/9Ug;->l:F

    .line 1498304
    iput p2, p0, LX/9Ug;->m:F

    .line 1498305
    iget-object v0, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/9Ug;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    goto :goto_0

    .line 1498306
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1498288
    iget-object v0, p0, LX/9Ug;->e:LX/9Uh;

    .line 1498289
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/9Uh;->e:Z

    .line 1498290
    invoke-virtual {v0}, LX/9Uh;->b()V

    .line 1498291
    invoke-virtual {v0}, LX/9Uh;->a()V

    .line 1498292
    return-void
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1498285
    invoke-direct {p0, p1}, LX/9Ug;->b(F)V

    .line 1498286
    invoke-virtual {p0}, LX/9Ug;->invalidateSelf()V

    .line 1498287
    return-void
.end method

.method public final a(FFLX/9Ub;)V
    .locals 0

    .prologue
    .line 1498283
    invoke-direct {p0, p1, p2, p3}, LX/9Ug;->b(FFLX/9Ub;)V

    .line 1498284
    return-void
.end method

.method public final a(LX/20u;)V
    .locals 1

    .prologue
    .line 1498447
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/9Ug;->p:Ljava/lang/ref/WeakReference;

    .line 1498448
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1498280
    iget-object v0, p0, LX/9Ug;->e:LX/9Uh;

    .line 1498281
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/9Uh;->e:Z

    .line 1498282
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1498274
    iget-object v0, p0, LX/9Ug;->p:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 1498275
    :cond_0
    :goto_0
    return-void

    .line 1498276
    :cond_1
    iget-object v0, p0, LX/9Ug;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20u;

    .line 1498277
    if-eqz v0, :cond_0

    .line 1498278
    invoke-interface {v0}, LX/20u;->a()V

    .line 1498279
    iget-object v0, p0, LX/9Ug;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1498208
    invoke-virtual {p0}, LX/9Ug;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    .line 1498209
    iget v0, v3, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v2, v3, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1498210
    iget-object v0, p0, LX/9Ug;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_9

    .line 1498211
    iget-object v0, p0, LX/9Ug;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ue;

    .line 1498212
    iget-boolean v5, v0, LX/9Ue;->k:Z

    move v5, v5

    .line 1498213
    if-eqz v5, :cond_2

    .line 1498214
    invoke-virtual {v0}, LX/9Ue;->k()LX/9Ud;

    move-result-object v5

    .line 1498215
    iget-object v6, v0, LX/9Ue;->h:Landroid/graphics/Matrix;

    iget-object v7, v0, LX/9Ue;->b:LX/9Ug;

    iget-object v7, v7, LX/9Ug;->f:Landroid/graphics/Matrix;

    if-ne v6, v7, :cond_a

    .line 1498216
    const/4 v6, 0x0

    .line 1498217
    :goto_1
    move-object v6, v6

    .line 1498218
    if-eqz v5, :cond_4

    iget-object v7, v5, LX/9Ud;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_4

    if-eqz v6, :cond_4

    .line 1498219
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1498220
    iget-object v0, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1498221
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1498222
    iget-object v0, v5, LX/9Ud;->b:Landroid/graphics/Matrix;

    if-eqz v0, :cond_3

    iget-object v0, v5, LX/9Ud;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 1498223
    :goto_2
    if-eqz v0, :cond_0

    .line 1498224
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1498225
    iget-object v6, v5, LX/9Ud;->b:Landroid/graphics/Matrix;

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1498226
    :cond_0
    iget-object v5, v5, LX/9Ud;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1498227
    if-eqz v0, :cond_1

    .line 1498228
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1498229
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1498230
    :cond_2
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1498231
    goto :goto_2

    .line 1498232
    :cond_4
    iget-object v5, v0, LX/9Ue;->d:LX/9Ua;

    move-object v5, v5

    .line 1498233
    if-eqz v5, :cond_2

    .line 1498234
    iget-object v6, v5, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->isEmpty()Z

    move-result v6

    move v6, v6

    .line 1498235
    if-nez v6, :cond_2

    .line 1498236
    iget-object v6, v0, LX/9Ue;->e:LX/9Ua;

    move-object v6, v6

    .line 1498237
    if-eqz v6, :cond_5

    .line 1498238
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1498239
    iget-object v6, v0, LX/9Ue;->e:LX/9Ua;

    move-object v6, v6

    .line 1498240
    sget-object v7, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    .line 1498241
    iget-object v8, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    invoke-virtual {v6, v8}, LX/9Ua;->a(Landroid/graphics/Matrix;)V

    .line 1498242
    iget-object v8, v6, LX/9Ua;->a:Landroid/graphics/Path;

    move-object v8, v8

    .line 1498243
    invoke-virtual {p1, v8, v7}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 1498244
    iget-object v8, p0, LX/9Ug;->h:Landroid/graphics/Matrix;

    invoke-virtual {v6, v8}, LX/9Ua;->a(Landroid/graphics/Matrix;)V

    .line 1498245
    :cond_5
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1498246
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, LX/9Ue;->i()Landroid/graphics/Paint$Cap;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1498247
    invoke-virtual {v0}, LX/9Ue;->h()I

    move-result v6

    if-eqz v6, :cond_6

    .line 1498248
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1498249
    iget-object v6, v0, LX/9Ue;->m:Landroid/graphics/Shader;

    move-object v6, v6

    .line 1498250
    if-nez v6, :cond_8

    .line 1498251
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, LX/9Ue;->h()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1498252
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, LX/9Ue;->e()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1498253
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v5, v6}, LX/9Ug;->a(Landroid/graphics/Canvas;LX/9Ua;Landroid/graphics/Paint;)V

    .line 1498254
    :cond_6
    :goto_4
    invoke-virtual {v0}, LX/9Ue;->g()I

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v0}, LX/9Ue;->d()F

    move-result v6

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_7

    .line 1498255
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, LX/9Ue;->g()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1498256
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, LX/9Ue;->e()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1498257
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1498258
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, LX/9Ue;->d()F

    move-result v7

    iget v8, p0, LX/9Ug;->k:F

    mul-float/2addr v7, v8

    iget v8, p0, LX/9Ug;->l:F

    mul-float/2addr v7, v8

    iget v8, p0, LX/9Ug;->m:F

    mul-float/2addr v7, v8

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1498259
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v5, v6}, LX/9Ug;->a(Landroid/graphics/Canvas;LX/9Ua;Landroid/graphics/Paint;)V

    .line 1498260
    :cond_7
    iget-object v5, v0, LX/9Ue;->e:LX/9Ua;

    move-object v0, v5

    .line 1498261
    if-eqz v0, :cond_2

    .line 1498262
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_3

    .line 1498263
    :cond_8
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    .line 1498264
    iget-object v7, v0, LX/9Ue;->m:Landroid/graphics/Shader;

    move-object v7, v7

    .line 1498265
    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1498266
    iget-object v6, p0, LX/9Ug;->b:Landroid/graphics/Paint;

    .line 1498267
    iget-object v7, p0, LX/9Ug;->g:Landroid/graphics/Matrix;

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1498268
    iget-object v7, v5, LX/9Ua;->a:Landroid/graphics/Path;

    move-object v7, v7

    .line 1498269
    invoke-virtual {p1, v7, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1498270
    iget-object v7, p0, LX/9Ug;->h:Landroid/graphics/Matrix;

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1498271
    goto :goto_4

    .line 1498272
    :cond_9
    iget v0, v3, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, v3, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1498273
    return-void

    :cond_a
    iget-object v6, v0, LX/9Ue;->h:Landroid/graphics/Matrix;

    goto/16 :goto_1
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1498207
    const/4 v0, -0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 1498206
    return-void
.end method

.method public final setBounds(IIII)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1498192
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1498193
    sub-int v0, p3, p1

    iput v0, p0, LX/9Ug;->i:I

    .line 1498194
    sub-int v0, p4, p2

    iput v0, p0, LX/9Ug;->j:I

    .line 1498195
    iget v0, p0, LX/9Ug;->i:I

    int-to-float v0, v0

    iget-object v1, p0, LX/9Ug;->a:LX/3K3;

    .line 1498196
    iget-object v2, v1, LX/3K3;->e:[F

    move-object v1, v2

    .line 1498197
    const/4 v2, 0x0

    aget v1, v1, v2

    div-float/2addr v0, v1

    .line 1498198
    iget v1, p0, LX/9Ug;->j:I

    int-to-float v1, v1

    iget-object v2, p0, LX/9Ug;->a:LX/3K3;

    .line 1498199
    iget-object v3, v2, LX/3K3;->e:[F

    move-object v2, v3

    .line 1498200
    const/4 v3, 0x1

    aget v2, v2, v3

    div-float/2addr v1, v2

    .line 1498201
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/9Ug;->k:F

    .line 1498202
    sget-object v0, LX/9Ub;->UP:LX/9Ub;

    invoke-direct {p0, v4, v4, v0}, LX/9Ug;->b(FFLX/9Ub;)V

    .line 1498203
    iget-boolean v0, p0, LX/9Ug;->o:Z

    if-nez v0, :cond_0

    .line 1498204
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/9Ug;->b(F)V

    .line 1498205
    :cond_0
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 1498191
    return-void
.end method
