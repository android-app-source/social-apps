.class public final LX/9ji;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Landroid/net/wifi/ScanResult;",
        "Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifiObject;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/9jj;


# direct methods
.method public constructor <init>(LX/9jj;J)V
    .locals 0

    .prologue
    .line 1530338
    iput-object p1, p0, LX/9ji;->b:LX/9jj;

    iput-wide p2, p0, LX/9ji;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1530339
    check-cast p1, Landroid/net/wifi/ScanResult;

    .line 1530340
    new-instance v0, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifiObject;

    iget-object v1, p1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iget-object v2, p1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget v3, p1, Landroid/net/wifi/ScanResult;->level:I

    iget v4, p1, Landroid/net/wifi/ScanResult;->frequency:I

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v6, p0, LX/9ji;->a:J

    invoke-static {p1, v6, v7}, LX/2zX;->a(Landroid/net/wifi/ScanResult;J)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifiObject;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Long;)V

    return-object v0
.end method
