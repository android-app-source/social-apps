.class public final LX/A9g;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1635487
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1635488
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635489
    :goto_0
    return v1

    .line 1635490
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635491
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 1635492
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1635493
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635494
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1635495
    const-string v8, "body_text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1635496
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1635497
    :cond_2
    const-string v8, "creative_image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1635498
    const/4 v7, 0x0

    .line 1635499
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_c

    .line 1635500
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635501
    :goto_2
    move v5, v7

    .line 1635502
    goto :goto_1

    .line 1635503
    :cond_3
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1635504
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1635505
    :cond_4
    const-string v8, "image_hash"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1635506
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1635507
    :cond_5
    const-string v8, "object_story_spec"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1635508
    invoke-static {p0, p1}, LX/A9d;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1635509
    :cond_6
    const-string v8, "promoted_object"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1635510
    invoke-static {p0, p1}, LX/A9f;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1635511
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1635512
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1635513
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1635514
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1635515
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1635516
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1635517
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1635518
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1

    .line 1635519
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635520
    :cond_a
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1635521
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1635522
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635523
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_a

    if-eqz v8, :cond_a

    .line 1635524
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1635525
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1635526
    :cond_b
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1635527
    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 1635528
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_c
    move v5, v7

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1635529
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635530
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1635531
    if-eqz v0, :cond_0

    .line 1635532
    const-string v1, "body_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635533
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635534
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635535
    if-eqz v0, :cond_2

    .line 1635536
    const-string v1, "creative_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635537
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635538
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635539
    if-eqz v1, :cond_1

    .line 1635540
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635541
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635542
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635543
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1635544
    if-eqz v0, :cond_3

    .line 1635545
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635546
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635547
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1635548
    if-eqz v0, :cond_4

    .line 1635549
    const-string v1, "image_hash"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635550
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635551
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635552
    if-eqz v0, :cond_5

    .line 1635553
    const-string v1, "object_story_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635554
    invoke-static {p0, v0, p2, p3}, LX/A9d;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635555
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635556
    if-eqz v0, :cond_6

    .line 1635557
    const-string v1, "promoted_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635558
    invoke-static {p0, v0, p2, p3}, LX/A9f;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635559
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635560
    return-void
.end method
