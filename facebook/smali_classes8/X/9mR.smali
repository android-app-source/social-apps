.class public final LX/9mR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/9mU;


# direct methods
.method public constructor <init>(LX/9mU;)V
    .locals 0

    .prologue
    .line 1535651
    iput-object p1, p0, LX/9mR;->a:LX/9mU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 1535652
    iget-object v0, p0, LX/9mR;->a:LX/9mU;

    iget-object v0, v0, LX/9mU;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1535653
    if-eqz p2, :cond_0

    .line 1535654
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    mul-int/lit8 v1, v1, 0x3

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1535655
    iget-object v1, p0, LX/9mR;->a:LX/9mU;

    iget-object v1, v1, LX/9mU;->i:Landroid/view/View;

    const v2, 0x7f0a0103

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1535656
    :goto_0
    iget-object v1, p0, LX/9mR;->a:LX/9mU;

    iget-object v1, v1, LX/9mU;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1535657
    return-void

    .line 1535658
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    div-int/lit8 v1, v1, 0x3

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1535659
    iget-object v1, p0, LX/9mR;->a:LX/9mU;

    iget-object v1, v1, LX/9mU;->i:Landroid/view/View;

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method
