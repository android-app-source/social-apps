.class public LX/ADz;
.super Lcom/facebook/attachments/angora/AngoraAttachmentView;
.source ""


# static fields
.field public static final c:LX/1Cz;


# instance fields
.field public final e:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1645824
    new-instance v0, LX/ADy;

    invoke-direct {v0}, LX/ADy;-><init>()V

    sput-object v0, LX/ADz;->c:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1645825
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ADz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1645826
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1645827
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ADz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1645828
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1645829
    const v0, 0x7f0311d3

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 1645830
    const v0, 0x7f0d29e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/ADz;->e:Landroid/widget/TextView;

    .line 1645831
    return-void
.end method
