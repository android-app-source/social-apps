.class public final LX/92R;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/92S;


# direct methods
.method public constructor <init>(LX/92S;)V
    .locals 0

    .prologue
    .line 1432286
    iput-object p1, p0, LX/92R;->a:LX/92S;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1432287
    iget-object v0, p0, LX/92R;->a:LX/92S;

    iget-object v0, v0, LX/92S;->b:LX/03V;

    const-string v1, "minutiae_verb_background_fetch_failed"

    const-string v2, "Minutiae verbs background fetch failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1432288
    iget-object v0, p0, LX/92R;->a:LX/92S;

    iget-object v0, v0, LX/92S;->c:LX/919;

    .line 1432289
    iget-object v1, v0, LX/919;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "activity_picker_background_fetch_failed"

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1432290
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1432291
    iget-object v0, p0, LX/92R;->a:LX/92S;

    iget-object v0, v0, LX/92S;->c:LX/919;

    .line 1432292
    iget-object v1, v0, LX/919;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "activity_picker_background_fetch_success"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1432293
    return-void
.end method
