.class public final enum LX/9VZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9VZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9VZ;

.field public static final enum ACTION_LINK:LX/9VZ;

.field public static final enum ACTIVE_NOW_LIGHTWEIGHT_ACTIONS:LX/9VZ;

.field public static final enum COMPOSER_AUDIO_CLIP_TAB:LX/9VZ;

.field public static final enum COMPOSER_CONTENT_SEARCH:LX/9VZ;

.field public static final enum COMPOSER_EVENT_TAB:LX/9VZ;

.field public static final enum COMPOSER_HOT_LIKE:LX/9VZ;

.field public static final enum COMPOSER_KEYBOARD:LX/9VZ;

.field public static final enum COMPOSER_LIGHTWEIGHT_ACTIONS_TAB:LX/9VZ;

.field public static final enum COMPOSER_LIVE_LOCATION_TAB:LX/9VZ;

.field public static final enum COMPOSER_LOCATION_TAB:LX/9VZ;

.field public static final enum COMPOSER_MEDIA_TRAY_TAB:LX/9VZ;

.field public static final enum COMPOSER_MONTAGE_CAM_TAB:LX/9VZ;

.field public static final enum COMPOSER_PAYMENT_TAB:LX/9VZ;

.field public static final enum COMPOSER_QUICK_CAM_TAB:LX/9VZ;

.field public static final enum COMPOSER_SAMPLE_CONTENT_PAGE:LX/9VZ;

.field public static final enum COMPOSER_STICKER_TAB:LX/9VZ;

.field public static final enum COMPOSER_TEXT_TAB:LX/9VZ;

.field public static final enum FORWARD:LX/9VZ;

.field public static final enum GROUP_EVENT_CREATE:LX/9VZ;

.field public static final enum MEDIA_PICKER_GALLERY:LX/9VZ;

.field public static final enum MEDIA_VIEWER_EDITOR:LX/9VZ;

.field public static final enum MONTAGE:LX/9VZ;

.field public static final enum NEW_MESSAGE:LX/9VZ;

.field public static final enum NULL_STATE:LX/9VZ;

.field public static final enum NUX:LX/9VZ;

.field public static final enum PEOPLE_LIST_LIGHTWEIGHT_ACTIONS:LX/9VZ;

.field public static final enum PIC_HEAD:LX/9VZ;

.field public static final enum PLATFORM_APP:LX/9VZ;

.field public static final enum PLATFORM_POSTBACK:LX/9VZ;

.field public static final enum QUICK_REPLY:LX/9VZ;

.field public static final enum RTC_LONG_CLICK:LX/9VZ;

.field public static final enum RTC_VOICEMAIL:LX/9VZ;

.field public static final enum RTC_VOIP_QUICKRESPONSE:LX/9VZ;

.field public static final enum SHARE:LX/9VZ;

.field public static final enum TRUSTED_APP_INTENT:LX/9VZ;

.field public static final enum UNKNOWN:LX/9VZ;


# instance fields
.field private mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1499458
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_AUDIO_CLIP_TAB"

    const-string v2, "composer_audio_clip_tab"

    invoke-direct {v0, v1, v4, v2}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_AUDIO_CLIP_TAB:LX/9VZ;

    .line 1499459
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_EVENT_TAB"

    const-string v2, "composer_event_tab"

    invoke-direct {v0, v1, v5, v2}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_EVENT_TAB:LX/9VZ;

    .line 1499460
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_LOCATION_TAB"

    const-string v2, "composer_location_tab"

    invoke-direct {v0, v1, v6, v2}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_LOCATION_TAB:LX/9VZ;

    .line 1499461
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_LIVE_LOCATION_TAB"

    const-string v2, "composer_live_location_tab"

    invoke-direct {v0, v1, v7, v2}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_LIVE_LOCATION_TAB:LX/9VZ;

    .line 1499462
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_MEDIA_TRAY_TAB"

    const-string v2, "composer_media_tray_tab"

    invoke-direct {v0, v1, v8, v2}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_MEDIA_TRAY_TAB:LX/9VZ;

    .line 1499463
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_PAYMENT_TAB"

    const/4 v2, 0x5

    const-string v3, "composer_payment_tab"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_PAYMENT_TAB:LX/9VZ;

    .line 1499464
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_QUICK_CAM_TAB"

    const/4 v2, 0x6

    const-string v3, "composer_quick_cam_tab"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_QUICK_CAM_TAB:LX/9VZ;

    .line 1499465
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_MONTAGE_CAM_TAB"

    const/4 v2, 0x7

    const-string v3, "composer_montage_cam_tab"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_MONTAGE_CAM_TAB:LX/9VZ;

    .line 1499466
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_STICKER_TAB"

    const/16 v2, 0x8

    const-string v3, "composer_sticker_tab"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_STICKER_TAB:LX/9VZ;

    .line 1499467
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_LIGHTWEIGHT_ACTIONS_TAB"

    const/16 v2, 0x9

    const-string v3, "composer_lighweight_actions_tab"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_LIGHTWEIGHT_ACTIONS_TAB:LX/9VZ;

    .line 1499468
    new-instance v0, LX/9VZ;

    const-string v1, "PEOPLE_LIST_LIGHTWEIGHT_ACTIONS"

    const/16 v2, 0xa

    const-string v3, "people_lwa"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->PEOPLE_LIST_LIGHTWEIGHT_ACTIONS:LX/9VZ;

    .line 1499469
    new-instance v0, LX/9VZ;

    const-string v1, "ACTIVE_NOW_LIGHTWEIGHT_ACTIONS"

    const/16 v2, 0xb

    const-string v3, "active_now_lwa"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->ACTIVE_NOW_LIGHTWEIGHT_ACTIONS:LX/9VZ;

    .line 1499470
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_TEXT_TAB"

    const/16 v2, 0xc

    const-string v3, "composer_text_tab"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_TEXT_TAB:LX/9VZ;

    .line 1499471
    new-instance v0, LX/9VZ;

    const-string v1, "NULL_STATE"

    const/16 v2, 0xd

    const-string v3, "null_state"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->NULL_STATE:LX/9VZ;

    .line 1499472
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_CONTENT_SEARCH"

    const/16 v2, 0xe

    const-string v3, "composer_content_search"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_CONTENT_SEARCH:LX/9VZ;

    .line 1499473
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_HOT_LIKE"

    const/16 v2, 0xf

    const-string v3, "composer_hot_like"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_HOT_LIKE:LX/9VZ;

    .line 1499474
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_KEYBOARD"

    const/16 v2, 0x10

    const-string v3, "composer_keyboard"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_KEYBOARD:LX/9VZ;

    .line 1499475
    new-instance v0, LX/9VZ;

    const-string v1, "COMPOSER_SAMPLE_CONTENT_PAGE"

    const/16 v2, 0x11

    const-string v3, "composer_sample_content_page"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->COMPOSER_SAMPLE_CONTENT_PAGE:LX/9VZ;

    .line 1499476
    new-instance v0, LX/9VZ;

    const-string v1, "ACTION_LINK"

    const/16 v2, 0x12

    const-string v3, "action_link"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->ACTION_LINK:LX/9VZ;

    .line 1499477
    new-instance v0, LX/9VZ;

    const-string v1, "FORWARD"

    const/16 v2, 0x13

    const-string v3, "forward"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->FORWARD:LX/9VZ;

    .line 1499478
    new-instance v0, LX/9VZ;

    const-string v1, "MEDIA_PICKER_GALLERY"

    const/16 v2, 0x14

    const-string v3, "media_picker_gallery"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->MEDIA_PICKER_GALLERY:LX/9VZ;

    .line 1499479
    new-instance v0, LX/9VZ;

    const-string v1, "MEDIA_VIEWER_EDITOR"

    const/16 v2, 0x15

    const-string v3, "media_viewer_editor"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->MEDIA_VIEWER_EDITOR:LX/9VZ;

    .line 1499480
    new-instance v0, LX/9VZ;

    const-string v1, "MONTAGE"

    const/16 v2, 0x16

    const-string v3, "montage"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->MONTAGE:LX/9VZ;

    .line 1499481
    new-instance v0, LX/9VZ;

    const-string v1, "NEW_MESSAGE"

    const/16 v2, 0x17

    const-string v3, "new_message"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->NEW_MESSAGE:LX/9VZ;

    .line 1499482
    new-instance v0, LX/9VZ;

    const-string v1, "GROUP_EVENT_CREATE"

    const/16 v2, 0x18

    const-string v3, "group_event_create"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->GROUP_EVENT_CREATE:LX/9VZ;

    .line 1499483
    new-instance v0, LX/9VZ;

    const-string v1, "NUX"

    const/16 v2, 0x19

    const-string v3, "nux"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->NUX:LX/9VZ;

    .line 1499484
    new-instance v0, LX/9VZ;

    const-string v1, "PLATFORM_APP"

    const/16 v2, 0x1a

    const-string v3, "platform_app"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->PLATFORM_APP:LX/9VZ;

    .line 1499485
    new-instance v0, LX/9VZ;

    const-string v1, "PLATFORM_POSTBACK"

    const/16 v2, 0x1b

    const-string v3, "platform_postback"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->PLATFORM_POSTBACK:LX/9VZ;

    .line 1499486
    new-instance v0, LX/9VZ;

    const-string v1, "PIC_HEAD"

    const/16 v2, 0x1c

    const-string v3, "pic_head"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->PIC_HEAD:LX/9VZ;

    .line 1499487
    new-instance v0, LX/9VZ;

    const-string v1, "QUICK_REPLY"

    const/16 v2, 0x1d

    const-string v3, "quick_reply"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->QUICK_REPLY:LX/9VZ;

    .line 1499488
    new-instance v0, LX/9VZ;

    const-string v1, "RTC_VOICEMAIL"

    const/16 v2, 0x1e

    const-string v3, "rtc_voicemail"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->RTC_VOICEMAIL:LX/9VZ;

    .line 1499489
    new-instance v0, LX/9VZ;

    const-string v1, "RTC_VOIP_QUICKRESPONSE"

    const/16 v2, 0x1f

    const-string v3, "rtc_voip_quickresponse"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->RTC_VOIP_QUICKRESPONSE:LX/9VZ;

    .line 1499490
    new-instance v0, LX/9VZ;

    const-string v1, "RTC_LONG_CLICK"

    const/16 v2, 0x20

    const-string v3, "rtc_long_click"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->RTC_LONG_CLICK:LX/9VZ;

    .line 1499491
    new-instance v0, LX/9VZ;

    const-string v1, "SHARE"

    const/16 v2, 0x21

    const-string v3, "share"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->SHARE:LX/9VZ;

    .line 1499492
    new-instance v0, LX/9VZ;

    const-string v1, "TRUSTED_APP_INTENT"

    const/16 v2, 0x22

    const-string v3, "trusted_app_intent"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->TRUSTED_APP_INTENT:LX/9VZ;

    .line 1499493
    new-instance v0, LX/9VZ;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x23

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/9VZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9VZ;->UNKNOWN:LX/9VZ;

    .line 1499494
    const/16 v0, 0x24

    new-array v0, v0, [LX/9VZ;

    sget-object v1, LX/9VZ;->COMPOSER_AUDIO_CLIP_TAB:LX/9VZ;

    aput-object v1, v0, v4

    sget-object v1, LX/9VZ;->COMPOSER_EVENT_TAB:LX/9VZ;

    aput-object v1, v0, v5

    sget-object v1, LX/9VZ;->COMPOSER_LOCATION_TAB:LX/9VZ;

    aput-object v1, v0, v6

    sget-object v1, LX/9VZ;->COMPOSER_LIVE_LOCATION_TAB:LX/9VZ;

    aput-object v1, v0, v7

    sget-object v1, LX/9VZ;->COMPOSER_MEDIA_TRAY_TAB:LX/9VZ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9VZ;->COMPOSER_PAYMENT_TAB:LX/9VZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9VZ;->COMPOSER_QUICK_CAM_TAB:LX/9VZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9VZ;->COMPOSER_MONTAGE_CAM_TAB:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9VZ;->COMPOSER_STICKER_TAB:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9VZ;->COMPOSER_LIGHTWEIGHT_ACTIONS_TAB:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9VZ;->PEOPLE_LIST_LIGHTWEIGHT_ACTIONS:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9VZ;->ACTIVE_NOW_LIGHTWEIGHT_ACTIONS:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9VZ;->COMPOSER_TEXT_TAB:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9VZ;->NULL_STATE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9VZ;->COMPOSER_CONTENT_SEARCH:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9VZ;->COMPOSER_HOT_LIKE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9VZ;->COMPOSER_KEYBOARD:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9VZ;->COMPOSER_SAMPLE_CONTENT_PAGE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9VZ;->ACTION_LINK:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9VZ;->FORWARD:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/9VZ;->MEDIA_PICKER_GALLERY:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/9VZ;->MEDIA_VIEWER_EDITOR:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/9VZ;->MONTAGE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/9VZ;->NEW_MESSAGE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/9VZ;->GROUP_EVENT_CREATE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/9VZ;->NUX:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/9VZ;->PLATFORM_APP:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/9VZ;->PLATFORM_POSTBACK:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/9VZ;->PIC_HEAD:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/9VZ;->QUICK_REPLY:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/9VZ;->RTC_VOICEMAIL:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/9VZ;->RTC_VOIP_QUICKRESPONSE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/9VZ;->RTC_LONG_CLICK:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/9VZ;->SHARE:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/9VZ;->TRUSTED_APP_INTENT:LX/9VZ;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/9VZ;->UNKNOWN:LX/9VZ;

    aput-object v2, v0, v1

    sput-object v0, LX/9VZ;->$VALUES:[LX/9VZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1499498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1499499
    iput-object p3, p0, LX/9VZ;->mValue:Ljava/lang/String;

    .line 1499500
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9VZ;
    .locals 1

    .prologue
    .line 1499497
    const-class v0, LX/9VZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9VZ;

    return-object v0
.end method

.method public static values()[LX/9VZ;
    .locals 1

    .prologue
    .line 1499496
    sget-object v0, LX/9VZ;->$VALUES:[LX/9VZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9VZ;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1499495
    iget-object v0, p0, LX/9VZ;->mValue:Ljava/lang/String;

    return-object v0
.end method
