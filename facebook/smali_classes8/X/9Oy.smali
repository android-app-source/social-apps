.class public final LX/9Oy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "image"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "image"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1483141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1483142
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;
    .locals 13

    .prologue
    .line 1483108
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1483109
    iget-object v1, p0, LX/9Oy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1483110
    iget-object v2, p0, LX/9Oy;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1483111
    iget-object v3, p0, LX/9Oy;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1483112
    iget-object v4, p0, LX/9Oy;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1483113
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v6, p0, LX/9Oy;->e:LX/15i;

    iget v7, p0, LX/9Oy;->f:I

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v5, 0x7e98ef71

    invoke-static {v6, v7, v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1483114
    iget-object v6, p0, LX/9Oy;->g:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1483115
    iget-object v7, p0, LX/9Oy;->i:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1483116
    iget-object v8, p0, LX/9Oy;->j:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1483117
    iget-object v9, p0, LX/9Oy;->k:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1483118
    iget-object v10, p0, LX/9Oy;->l:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1483119
    iget-object v11, p0, LX/9Oy;->m:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1483120
    const/16 v12, 0xc

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1483121
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 1483122
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1483123
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1483124
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1483125
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1483126
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1483127
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/9Oy;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1483128
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1483129
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1483130
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1483131
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1483132
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1483133
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1483134
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1483135
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1483136
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1483137
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1483138
    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-direct {v1, v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;-><init>(LX/15i;)V

    .line 1483139
    return-object v1

    .line 1483140
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
