.class public LX/9V5;
.super LX/3AP;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9V5;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V
    .locals 10
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1498930
    const-string v3, "live_photo"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    .line 1498931
    return-void
.end method

.method public static a(LX/0QB;)LX/9V5;
    .locals 12

    .prologue
    .line 1498932
    sget-object v0, LX/9V5;->a:LX/9V5;

    if-nez v0, :cond_1

    .line 1498933
    const-class v1, LX/9V5;

    monitor-enter v1

    .line 1498934
    :try_start_0
    sget-object v0, LX/9V5;->a:LX/9V5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1498935
    if-eqz v2, :cond_0

    .line 1498936
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1498937
    new-instance v3, LX/9V5;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v5

    check-cast v5, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v8

    check-cast v8, LX/1Gk;

    invoke-static {v0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v9

    check-cast v9, LX/1Gl;

    invoke-static {v0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v10

    check-cast v10, LX/1Go;

    invoke-static {v0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v11

    check-cast v11, LX/13t;

    invoke-direct/range {v3 .. v11}, LX/9V5;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    .line 1498938
    move-object v0, v3

    .line 1498939
    sput-object v0, LX/9V5;->a:LX/9V5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1498940
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1498941
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1498942
    :cond_1
    sget-object v0, LX/9V5;->a:LX/9V5;

    return-object v0

    .line 1498943
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1498944
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
