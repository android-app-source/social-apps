.class public final LX/AEl;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2fo;


# direct methods
.method public constructor <init>(LX/2fo;)V
    .locals 0

    .prologue
    .line 1647035
    iput-object p1, p0, LX/AEl;->a:LX/2fo;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1647033
    iget-object v0, p0, LX/AEl;->a:LX/2fo;

    iget-object v0, v0, LX/2fo;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNw;

    invoke-virtual {v0}, LX/BNw;->a()V

    .line 1647034
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1647036
    iget-object v0, p0, LX/AEl;->a:LX/2fo;

    iget-object v0, v0, LX/2fo;->b:LX/03V;

    iget-object v1, p0, LX/AEl;->a:LX/2fo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to add item to Timeline Collection due to server exception %s."

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1647037
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1647032
    invoke-direct {p0}, LX/AEl;->a()V

    return-void
.end method
