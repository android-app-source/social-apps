.class public final LX/9FV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;)V
    .locals 0

    .prologue
    .line 1458610
    iput-object p1, p0, LX/9FV;->a:Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x2142f344

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1458611
    check-cast p1, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;

    .line 1458612
    iget-object v1, p1, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v1, v1

    .line 1458613
    if-nez v1, :cond_0

    .line 1458614
    const v1, -0x2943b4d4

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1458615
    :goto_0
    return-void

    .line 1458616
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1458617
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1458618
    :goto_1
    const v1, 0x483b7b15

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1458619
    :cond_1
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_1
.end method
