.class public LX/9af;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1513929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1513930
    const-string v0, ""

    iput-object v0, p0, LX/9af;->c:Ljava/lang/String;

    .line 1513931
    iput-boolean v1, p0, LX/9af;->d:Z

    .line 1513932
    iput-boolean v1, p0, LX/9af;->e:Z

    .line 1513933
    iput-object p1, p0, LX/9af;->a:LX/0Zb;

    .line 1513934
    return-void
.end method

.method public static a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1513935
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/9ae;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1513936
    const-string v0, "album_creator"

    .line 1513937
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1513938
    iget-object v0, p0, LX/9af;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1513939
    iget-object v0, p0, LX/9af;->b:Ljava/lang/String;

    .line 1513940
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1513941
    :cond_0
    const-string v0, "qe_group_name"

    iget-object v1, p0, LX/9af;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1513942
    iget-object v0, p0, LX/9af;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1513943
    return-void
.end method

.method public static b(LX/0QB;)LX/9af;
    .locals 2

    .prologue
    .line 1513944
    new-instance v1, LX/9af;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/9af;-><init>(LX/0Zb;)V

    .line 1513945
    return-object v1
.end method
