.class public LX/9cg;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9ci;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9cq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9c3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/9c7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lit/sephiroth/android/library/widget/HListView;

.field public g:Landroid/support/v4/view/ViewPager;

.field public h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

.field public i:LX/9cp;

.field public j:Landroid/database/DataSetObserver;

.field public k:Landroid/view/inputmethod/InputMethodManager;

.field public l:Landroid/view/animation/TranslateAnimation;

.field public m:Landroid/view/animation/TranslateAnimation;

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 1516653
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1516654
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/9cg;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const-class v4, LX/9ci;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9ci;

    const-class v5, LX/9cq;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9cq;

    invoke-static {v0}, LX/9c3;->a(LX/0QB;)LX/9c3;

    move-result-object v6

    check-cast v6, LX/9c3;

    invoke-static {v0}, LX/9c7;->b(LX/0QB;)LX/9c7;

    move-result-object v0

    check-cast v0, LX/9c7;

    iput-object v3, v2, LX/9cg;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v2, LX/9cg;->b:LX/9ci;

    iput-object v5, v2, LX/9cg;->c:LX/9cq;

    iput-object v6, v2, LX/9cg;->d:LX/9c3;

    iput-object v0, v2, LX/9cg;->e:LX/9c7;

    .line 1516655
    const v0, 0x7f0310fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1516656
    const v0, 0x7f0d285c

    invoke-virtual {p0, v0}, LX/9cg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lit/sephiroth/android/library/widget/HListView;

    iput-object v0, p0, LX/9cg;->f:Lit/sephiroth/android/library/widget/HListView;

    .line 1516657
    iget-object v0, p0, LX/9cg;->f:Lit/sephiroth/android/library/widget/HListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/8vj;->setChoiceMode(I)V

    .line 1516658
    iget-object v0, p0, LX/9cg;->f:Lit/sephiroth/android/library/widget/HListView;

    invoke-virtual {p0}, LX/9cg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1935

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lit/sephiroth/android/library/widget/HListView;->setDividerWidth(I)V

    .line 1516659
    invoke-virtual {p0}, LX/9cg;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, LX/9cg;->k:Landroid/view/inputmethod/InputMethodManager;

    .line 1516660
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, LX/9cg;->l:Landroid/view/animation/TranslateAnimation;

    .line 1516661
    iget-object v0, p0, LX/9cg;->l:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1516662
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, LX/9cg;->m:Landroid/view/animation/TranslateAnimation;

    .line 1516663
    iget-object v0, p0, LX/9cg;->m:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1516664
    iget-object v0, p0, LX/9cg;->m:Landroid/view/animation/TranslateAnimation;

    new-instance v1, LX/9ca;

    invoke-direct {v1, p0}, LX/9ca;-><init>(LX/9cg;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1516665
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 4

    .prologue
    .line 1516666
    invoke-virtual {p0}, LX/9cg;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1516667
    invoke-virtual {p0}, LX/9cg;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1516668
    iget-object v0, p0, LX/9cg;->m:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0, v0}, LX/9cg;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1516669
    iget-object v0, p0, LX/9cg;->d:LX/9c3;

    iget-object v1, p0, LX/9cg;->n:Ljava/lang/String;

    .line 1516670
    iget-object v2, v0, LX/9c3;->b:LX/11i;

    sget-object v3, LX/9c3;->a:LX/9c1;

    invoke-interface {v2, v3, v1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v2

    .line 1516671
    if-nez v2, :cond_0

    .line 1516672
    :goto_0
    return-void

    .line 1516673
    :cond_0
    sget-object v3, LX/9c2;->STICKER_PICKER_CLOSED:LX/9c2;

    iget-object v3, v3, LX/9c2;->name:Ljava/lang/String;

    const p0, -0x66e9da02

    invoke-static {v2, v3, p0}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1516674
    iget-object v0, p0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    if-nez v0, :cond_0

    .line 1516675
    :goto_0
    return-void

    .line 1516676
    :cond_0
    iget-object v0, p0, LX/9cg;->h:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    .line 1516677
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->d:LX/9cZ;

    new-instance v2, LX/9ch;

    invoke-direct {v2, v0}, LX/9ch;-><init>(Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;)V

    .line 1516678
    iput-object v2, v1, LX/9cZ;->e:LX/3Mb;

    .line 1516679
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->d:LX/9cZ;

    new-instance v2, LX/9cX;

    const/4 p0, 0x0

    invoke-direct {v2, p0}, LX/9cX;-><init>(Z)V

    invoke-virtual {v1, v2}, LX/9cZ;->a(LX/9cX;)V

    .line 1516680
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->d:LX/9cZ;

    new-instance v2, LX/9cX;

    const/4 p0, 0x1

    invoke-direct {v2, p0}, LX/9cX;-><init>(Z)V

    invoke-virtual {v1, v2}, LX/9cZ;->a(LX/9cX;)V

    .line 1516681
    goto :goto_0
.end method
