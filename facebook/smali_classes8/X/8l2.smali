.class public final LX/8l2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerPackPageView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V
    .locals 0

    .prologue
    .line 1397311
    iput-object p1, p0, LX/8l2;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x79aead59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397297
    iget-object v1, p0, LX/8l2;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    .line 1397298
    const-string v2, "stickerPack"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/model/StickerPack;

    .line 1397299
    iget-object p0, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->r:Lcom/facebook/stickers/model/StickerPack;

    if-eqz p0, :cond_0

    .line 1397300
    iget-object p0, v2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, p0

    .line 1397301
    iget-object p0, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->r:Lcom/facebook/stickers/model/StickerPack;

    .line 1397302
    iget-object p1, p0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object p0, p1

    .line 1397303
    invoke-static {v2, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1397304
    :cond_0
    :goto_0
    const/16 v1, 0x27

    const v2, 0x255bb780

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1397305
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string p0, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1397306
    iget-object v2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->k:Landroid/widget/ProgressBar;

    const-string p0, "progress"

    const/4 p1, 0x0

    invoke-virtual {p2, p0, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    invoke-virtual {v2, p0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 1397307
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string p0, "com.facebook.orca.stickers.DOWNLOAD_SUCCESS"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1397308
    iget-object v2, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->c:LX/8jY;

    new-instance p0, LX/8jW;

    iget-object p1, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->r:Lcom/facebook/stickers/model/StickerPack;

    .line 1397309
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object p1, v1

    .line 1397310
    invoke-direct {p0, p1}, LX/8jW;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, p0}, LX/8jY;->a(LX/8jW;)V

    goto :goto_0
.end method
