.class public final LX/ACv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ACu;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;)V
    .locals 0

    .prologue
    .line 1643224
    iput-object p1, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(FF)V
    .locals 2

    .prologue
    .line 1643225
    iget-object v0, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->e:Lcom/facebook/resources/ui/FbTextView;

    float-to-int v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1643226
    iget-object v0, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    .line 1643227
    invoke-static {v0}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b$redex0(Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;)V

    .line 1643228
    return-void
.end method

.method public final b(FF)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1643229
    iget-object v0, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget v0, v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c:I

    float-to-int v3, p1

    if-ne v0, v3, :cond_0

    iget-object v0, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget v0, v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->d:I

    float-to-int v3, p2

    if-eq v0, v3, :cond_2

    :cond_0
    move v0, v2

    .line 1643230
    :goto_0
    iget-object v3, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    float-to-int v4, p1

    .line 1643231
    iput v4, v3, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c:I

    .line 1643232
    iget-object v3, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    float-to-int v4, p2

    .line 1643233
    iput v4, v3, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->d:I

    .line 1643234
    iget-object v3, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget-object v3, v3, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    invoke-virtual {v3}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->requestFocus()Z

    .line 1643235
    iget-object v3, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget-object v3, v3, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->g:Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    iget-object v4, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080a50

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget v7, v7, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    iget-object v1, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget v1, v1, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1643236
    iget-object v1, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->h:LX/ACw;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 1643237
    iget-object v0, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->h:LX/ACw;

    iget-object v1, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget v1, v1, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c:I

    iget-object v2, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iget v2, v2, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->d:I

    invoke-interface {v0, v1, v2}, LX/ACw;->a(II)V

    .line 1643238
    :cond_1
    iget-object v0, p0, LX/ACv;->a:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->sendAccessibilityEvent(I)V

    .line 1643239
    return-void

    :cond_2
    move v0, v1

    .line 1643240
    goto :goto_0
.end method
