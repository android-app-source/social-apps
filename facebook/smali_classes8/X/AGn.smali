.class public final LX/AGn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AF7;

.field public final synthetic b:LX/AGo;


# direct methods
.method public constructor <init>(LX/AGo;LX/AF7;)V
    .locals 0

    .prologue
    .line 1653111
    iput-object p1, p0, LX/AGn;->b:LX/AGo;

    iput-object p2, p0, LX/AGn;->a:LX/AF7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1653112
    sget-object v0, LX/AGo;->a:Ljava/lang/String;

    const-string v1, "failed to get direct share threads"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1653113
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1653114
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1653115
    iget-object v0, p0, LX/AGn;->a:LX/AF7;

    if-nez v0, :cond_0

    .line 1653116
    :goto_0
    return-void

    .line 1653117
    :cond_0
    if-nez p1, :cond_1

    .line 1653118
    iget-object v0, p0, LX/AGn;->a:LX/AF7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AF7;->a(LX/0Px;)V

    goto :goto_0

    .line 1653119
    :cond_1
    const/4 v2, 0x0

    .line 1653120
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1653121
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1653122
    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 1653123
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v5

    .line 1653124
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1653125
    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v5

    .line 1653126
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1653127
    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v1, v3, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v5

    .line 1653128
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1653129
    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_4

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel;

    .line 1653130
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1653131
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v9

    invoke-virtual {v8, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v8

    .line 1653132
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v1, v2

    .line 1653133
    :goto_2
    if-ge v1, v10, :cond_3

    invoke-virtual {v9, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    .line 1653134
    new-instance v11, LX/AGq;

    invoke-direct {v11, v0, v8, v5}, LX/AGq;-><init>(Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V

    .line 1653135
    invoke-virtual {v11}, LX/AGp;->a()LX/7h0;

    move-result-object v0

    .line 1653136
    if-eqz v0, :cond_2

    .line 1653137
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1653138
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1653139
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1653140
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1653141
    iget-object v1, p0, LX/AGn;->a:LX/AF7;

    invoke-virtual {v1, v0}, LX/AF7;->a(LX/0Px;)V

    goto/16 :goto_0
.end method
