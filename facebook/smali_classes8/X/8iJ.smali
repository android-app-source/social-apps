.class public final enum LX/8iJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8iJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8iJ;

.field public static final enum SENTRY_BLOCK:LX/8iJ;

.field public static final enum SENTRY_COMMENT_DELETE_BLOCK:LX/8iJ;

.field public static final enum SENTRY_COMMENT_DISPLAY_BLOCK:LX/8iJ;

.field public static final enum SENTRY_COMMENT_EDIT_BLOCK:LX/8iJ;

.field public static final enum SENTRY_COMMENT_LIKE_BLOCK:LX/8iJ;

.field public static final enum SENTRY_COMMENT_POST_BLOCK:LX/8iJ;

.field public static final enum SENTRY_LIKE_BLOCK:LX/8iJ;

.field public static final enum SENTRY_PROFILE_LIST_BLOCK:LX/8iJ;

.field public static final enum SENTRY_PROFILE_PICTURE_UPDATE_BLOCK:LX/8iJ;

.field public static final enum SENTRY_REMOVE_TAG_BLOCK:LX/8iJ;


# instance fields
.field private final mTitleId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1391390
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_BLOCK"

    const v2, 0x7f080049

    invoke-direct {v0, v1, v4, v2}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_BLOCK:LX/8iJ;

    .line 1391391
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_LIKE_BLOCK"

    const v2, 0x7f08004a

    invoke-direct {v0, v1, v5, v2}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_LIKE_BLOCK:LX/8iJ;

    .line 1391392
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_COMMENT_LIKE_BLOCK"

    const v2, 0x7f08004c

    invoke-direct {v0, v1, v6, v2}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_COMMENT_LIKE_BLOCK:LX/8iJ;

    .line 1391393
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_COMMENT_DELETE_BLOCK"

    const v2, 0x7f08004d

    invoke-direct {v0, v1, v7, v2}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_COMMENT_DELETE_BLOCK:LX/8iJ;

    .line 1391394
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_COMMENT_POST_BLOCK"

    const v2, 0x7f08004b

    invoke-direct {v0, v1, v8, v2}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_COMMENT_POST_BLOCK:LX/8iJ;

    .line 1391395
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_COMMENT_EDIT_BLOCK"

    const/4 v2, 0x5

    const v3, 0x7f08004e

    invoke-direct {v0, v1, v2, v3}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_COMMENT_EDIT_BLOCK:LX/8iJ;

    .line 1391396
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_COMMENT_DISPLAY_BLOCK"

    const/4 v2, 0x6

    const v3, 0x7f08004f

    invoke-direct {v0, v1, v2, v3}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_COMMENT_DISPLAY_BLOCK:LX/8iJ;

    .line 1391397
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_REMOVE_TAG_BLOCK"

    const/4 v2, 0x7

    const v3, 0x7f080050

    invoke-direct {v0, v1, v2, v3}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_REMOVE_TAG_BLOCK:LX/8iJ;

    .line 1391398
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_PROFILE_PICTURE_UPDATE_BLOCK"

    const/16 v2, 0x8

    const v3, 0x7f080051

    invoke-direct {v0, v1, v2, v3}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_PROFILE_PICTURE_UPDATE_BLOCK:LX/8iJ;

    .line 1391399
    new-instance v0, LX/8iJ;

    const-string v1, "SENTRY_PROFILE_LIST_BLOCK"

    const/16 v2, 0x9

    const v3, 0x7f080052

    invoke-direct {v0, v1, v2, v3}, LX/8iJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8iJ;->SENTRY_PROFILE_LIST_BLOCK:LX/8iJ;

    .line 1391400
    const/16 v0, 0xa

    new-array v0, v0, [LX/8iJ;

    sget-object v1, LX/8iJ;->SENTRY_BLOCK:LX/8iJ;

    aput-object v1, v0, v4

    sget-object v1, LX/8iJ;->SENTRY_LIKE_BLOCK:LX/8iJ;

    aput-object v1, v0, v5

    sget-object v1, LX/8iJ;->SENTRY_COMMENT_LIKE_BLOCK:LX/8iJ;

    aput-object v1, v0, v6

    sget-object v1, LX/8iJ;->SENTRY_COMMENT_DELETE_BLOCK:LX/8iJ;

    aput-object v1, v0, v7

    sget-object v1, LX/8iJ;->SENTRY_COMMENT_POST_BLOCK:LX/8iJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8iJ;->SENTRY_COMMENT_EDIT_BLOCK:LX/8iJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8iJ;->SENTRY_COMMENT_DISPLAY_BLOCK:LX/8iJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8iJ;->SENTRY_REMOVE_TAG_BLOCK:LX/8iJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8iJ;->SENTRY_PROFILE_PICTURE_UPDATE_BLOCK:LX/8iJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8iJ;->SENTRY_PROFILE_LIST_BLOCK:LX/8iJ;

    aput-object v2, v0, v1

    sput-object v0, LX/8iJ;->$VALUES:[LX/8iJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1391384
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1391385
    iput p3, p0, LX/8iJ;->mTitleId:I

    .line 1391386
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8iJ;
    .locals 1

    .prologue
    .line 1391389
    const-class v0, LX/8iJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8iJ;

    return-object v0
.end method

.method public static values()[LX/8iJ;
    .locals 1

    .prologue
    .line 1391388
    sget-object v0, LX/8iJ;->$VALUES:[LX/8iJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8iJ;

    return-object v0
.end method


# virtual methods
.method public final getTitleId()I
    .locals 1

    .prologue
    .line 1391387
    iget v0, p0, LX/8iJ;->mTitleId:I

    return v0
.end method
