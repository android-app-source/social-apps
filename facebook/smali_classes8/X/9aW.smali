.class public LX/9aW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9aT;


# static fields
.field public static final a:LX/9aW;

.field public static final b:LX/9aW;


# instance fields
.field private final c:Ljava/util/Random;

.field private final d:F

.field private final e:F


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1513809
    new-instance v0, LX/9aW;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v1}, LX/9aW;-><init>(FF)V

    sput-object v0, LX/9aW;->a:LX/9aW;

    .line 1513810
    new-instance v0, LX/9aW;

    const/high16 v1, 0x43b40000    # 360.0f

    invoke-direct {v0, v2, v1}, LX/9aW;-><init>(FF)V

    sput-object v0, LX/9aW;->b:LX/9aW;

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1

    .prologue
    .line 1513811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1513812
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/9aW;->c:Ljava/util/Random;

    .line 1513813
    iput p1, p0, LX/9aW;->d:F

    .line 1513814
    iput p2, p0, LX/9aW;->e:F

    .line 1513815
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 3

    .prologue
    .line 1513816
    iget v0, p0, LX/9aW;->d:F

    iget v1, p0, LX/9aW;->e:F

    iget-object v2, p0, LX/9aW;->c:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    invoke-static {v0, v1, v2}, LX/0yq;->a(FFF)F

    move-result v0

    return v0
.end method
