.class public final LX/8wt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:LX/8wv;


# direct methods
.method public constructor <init>(LX/8wv;)V
    .locals 0

    .prologue
    .line 1423047
    iput-object p1, p0, LX/8wt;->a:LX/8wv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1423046
    return-void
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 1423044
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1, p2}, LX/2qW;->c(FF)V

    .line 1423045
    return-void
.end method

.method public final a(F)Z
    .locals 1

    .prologue
    .line 1423048
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1}, LX/2qW;->a(F)V

    .line 1423049
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    invoke-virtual {v0}, LX/8wv;->p()V

    .line 1423050
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1423038
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->a:LX/1xG;

    iget-object v1, p0, LX/8wt;->a:LX/8wv;

    iget-object v1, v1, LX/8wv;->r:Ljava/lang/String;

    iget-object v2, p0, LX/8wt;->a:LX/8wv;

    iget-object v2, v2, LX/8wv;->p:LX/7Dj;

    invoke-virtual {v0, v1, v2}, LX/1xG;->h(Ljava/lang/String;LX/7Dj;)V

    .line 1423039
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h()V

    .line 1423040
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    const/4 v1, 0x0

    .line 1423041
    iput-boolean v1, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1423042
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->f()V

    .line 1423043
    return-void
.end method

.method public final b(FF)V
    .locals 2

    .prologue
    .line 1423036
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2qW;->d(FF)V

    .line 1423037
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 1423034
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->a:LX/1xG;

    iget-object v1, p0, LX/8wt;->a:LX/8wv;

    iget-object v1, v1, LX/8wv;->r:Ljava/lang/String;

    iget-object v2, p0, LX/8wt;->a:LX/8wv;

    iget-object v2, v2, LX/8wv;->p:LX/7Dj;

    invoke-virtual {v0, v1, v2}, LX/1xG;->g(Ljava/lang/String;LX/7Dj;)V

    .line 1423035
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1423032
    iget-object v0, p0, LX/8wt;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->g()V

    .line 1423033
    return-void
.end method
