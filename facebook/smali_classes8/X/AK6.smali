.class public final LX/AK6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lcom/facebook/audience/model/AudienceControlData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1662651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662652
    const-string v0, ""

    iput-object v0, p0, LX/AK6;->a:Ljava/lang/String;

    .line 1662653
    const-string v0, ""

    iput-object v0, p0, LX/AK6;->e:Ljava/lang/String;

    .line 1662654
    const-string v0, ""

    iput-object v0, p0, LX/AK6;->f:Ljava/lang/String;

    .line 1662655
    return-void
.end method

.method public constructor <init>(Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)V
    .locals 4

    .prologue
    .line 1662656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662657
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1662658
    instance-of v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    if-eqz v0, :cond_0

    .line 1662659
    check-cast p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662660
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    iput-object v0, p0, LX/AK6;->a:Ljava/lang/String;

    .line 1662661
    iget-boolean v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    iput-boolean v0, p0, LX/AK6;->b:Z

    .line 1662662
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, LX/AK6;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662663
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    iput-object v0, p0, LX/AK6;->d:Ljava/lang/String;

    .line 1662664
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    iput-object v0, p0, LX/AK6;->e:Ljava/lang/String;

    .line 1662665
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    iput-object v0, p0, LX/AK6;->f:Ljava/lang/String;

    .line 1662666
    iget v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    iput v0, p0, LX/AK6;->g:I

    .line 1662667
    iget-wide v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    iput-wide v0, p0, LX/AK6;->h:J

    .line 1662668
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    iput-object v0, p0, LX/AK6;->i:Ljava/lang/String;

    .line 1662669
    :goto_0
    return-void

    .line 1662670
    :cond_0
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1662671
    iput-object v0, p0, LX/AK6;->a:Ljava/lang/String;

    .line 1662672
    iget-boolean v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    move v0, v0

    .line 1662673
    iput-boolean v0, p0, LX/AK6;->b:Z

    .line 1662674
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v0

    .line 1662675
    iput-object v0, p0, LX/AK6;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662676
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1662677
    iput-object v0, p0, LX/AK6;->d:Ljava/lang/String;

    .line 1662678
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1662679
    iput-object v0, p0, LX/AK6;->e:Ljava/lang/String;

    .line 1662680
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1662681
    iput-object v0, p0, LX/AK6;->f:Ljava/lang/String;

    .line 1662682
    iget v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    move v0, v0

    .line 1662683
    iput v0, p0, LX/AK6;->g:I

    .line 1662684
    iget-wide v2, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    move-wide v0, v2

    .line 1662685
    iput-wide v0, p0, LX/AK6;->h:J

    .line 1662686
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1662687
    iput-object v0, p0, LX/AK6;->i:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
    .locals 2

    .prologue
    .line 1662688
    new-instance v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    invoke-direct {v0, p0}, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;-><init>(LX/AK6;)V

    return-object v0
.end method
