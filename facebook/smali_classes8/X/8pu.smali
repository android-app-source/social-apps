.class public final LX/8pu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;",
        ">;",
        "LX/44w",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/model/GraphQLActor;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPageInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8pv;


# direct methods
.method public constructor <init>(LX/8pv;)V
    .locals 0

    .prologue
    .line 1406543
    iput-object p1, p0, LX/8pu;->a:LX/8pv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1406544
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1406545
    if-nez p1, :cond_0

    .line 1406546
    const/4 v0, 0x0

    .line 1406547
    :goto_0
    return-object v0

    .line 1406548
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406549
    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1406550
    const/4 v1, 0x0

    .line 1406551
    if-nez v0, :cond_2

    .line 1406552
    :cond_1
    :goto_1
    move-object v0, v1

    .line 1406553
    goto :goto_0

    .line 1406554
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object p0

    .line 1406555
    if-eqz p0, :cond_1

    .line 1406556
    invoke-static {p0}, LX/8pk;->a(Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;)Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object p0

    .line 1406557
    new-instance v1, LX/44w;

    invoke-static {p0}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLSeenByConnection;)LX/0Px;

    move-result-object p1

    .line 1406558
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;-><init>()V

    :goto_2
    move-object p0, v0

    .line 1406559
    invoke-direct {v1, p1, p0}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_2
.end method
