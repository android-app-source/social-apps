.class public final LX/ALl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/ALq;


# direct methods
.method public constructor <init>(LX/ALq;)V
    .locals 0

    .prologue
    .line 1665381
    iput-object p1, p0, LX/ALl;->a:LX/ALq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x2

    const/4 v4, 0x0

    const v0, -0x597e1cca

    invoke-static {v10, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 1665382
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->m:LX/AM1;

    invoke-virtual {v0}, LX/AM1;->b()V

    .line 1665383
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->m:LX/AM1;

    invoke-virtual {v0}, LX/AM1;->clearFocus()V

    .line 1665384
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->m:LX/AM1;

    invoke-virtual {v0, v1}, LX/AM1;->setDrawingCacheEnabled(Z)V

    .line 1665385
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->m:LX/AM1;

    invoke-virtual {v0}, LX/AM1;->buildDrawingCache()V

    .line 1665386
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->m:LX/AM1;

    invoke-virtual {v0}, LX/AM1;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1665387
    iget-object v9, p0, LX/ALl;->a:LX/ALq;

    new-instance v0, LX/7gj;

    sget-object v1, LX/7gi;->PHOTO:LX/7gi;

    const-string v3, ""

    iget-object v5, p0, LX/ALl;->a:LX/ALq;

    invoke-virtual {v5}, LX/ALq;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, LX/ALl;->a:LX/ALq;

    invoke-virtual {v6}, LX/ALq;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v7, v5, v6

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v7}, LX/7gj;-><init>(LX/7gi;Landroid/graphics/Bitmap;Ljava/lang/String;IIZF)V

    .line 1665388
    iput-object v0, v9, LX/ALq;->n:LX/7gj;

    .line 1665389
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->n:LX/7gj;

    iget-object v1, p0, LX/ALl;->a:LX/ALq;

    iget-object v1, v1, LX/ALq;->m:LX/AM1;

    invoke-virtual {v1}, LX/AM1;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1665390
    iput-object v1, v0, LX/7gj;->l:Ljava/lang/String;

    .line 1665391
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 1665392
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    invoke-static {v0}, LX/ALq;->b(LX/ALq;)V

    .line 1665393
    iget-object v0, p0, LX/ALl;->a:LX/ALq;

    iget-object v0, v0, LX/ALq;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/ALk;

    invoke-direct {v1, p0, v2}, LX/ALk;-><init>(LX/ALl;Landroid/graphics/Bitmap;)V

    iget-object v2, p0, LX/ALl;->a:LX/ALq;

    iget-object v2, v2, LX/ALq;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1665394
    const v0, 0x286df869    # 1.3210008E-14f

    invoke-static {v10, v10, v0, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
