.class public final LX/AQq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V
    .locals 0

    .prologue
    .line 1671862
    iput-object p1, p0, LX/AQq;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1671861
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1671860
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1671854
    iget-object v0, p0, LX/AQq;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v2, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->h:Landroid/view/View;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1671855
    iget-object v0, p0, LX/AQq;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->j:LX/AQu;

    if-eqz v0, :cond_0

    .line 1671856
    iget-object v0, p0, LX/AQq;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->j:LX/AQu;

    invoke-virtual {v0}, LX/AQu;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v2, p0, LX/AQq;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v2, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->p:Landroid/widget/Filter$FilterListener;

    invoke-virtual {v0, p1, v2}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    .line 1671857
    iget-object v0, p0, LX/AQq;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    .line 1671858
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1671859
    goto :goto_0
.end method
