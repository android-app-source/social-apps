.class public final LX/937;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/93E;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/93G;


# instance fields
.field public c:LX/939;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/93E;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/93G;

.field public n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/93E;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1433273
    new-instance v0, LX/93A;

    invoke-direct {v0}, LX/93A;-><init>()V

    .line 1433274
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1433275
    sput-object v0, LX/937;->a:LX/0Px;

    .line 1433276
    new-instance v0, LX/93B;

    invoke-direct {v0}, LX/93B;-><init>()V

    .line 1433277
    sget-object v0, LX/93G;->FEELINGS_TAB:LX/93G;

    move-object v0, v0

    .line 1433278
    sput-object v0, LX/937;->b:LX/93G;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1433279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433280
    sget-object v0, LX/937;->a:LX/0Px;

    iput-object v0, p0, LX/937;->f:LX/0Px;

    .line 1433281
    sget-object v0, LX/937;->b:LX/93G;

    iput-object v0, p0, LX/937;->m:LX/93G;

    .line 1433282
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V
    .locals 1

    .prologue
    .line 1433283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433284
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1433285
    instance-of v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    if-eqz v0, :cond_0

    .line 1433286
    check-cast p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1433287
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    iput-object v0, p0, LX/937;->c:LX/939;

    .line 1433288
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, LX/937;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1433289
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, LX/937;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1433290
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    iput-object v0, p0, LX/937;->f:LX/0Px;

    .line 1433291
    iget-boolean v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    iput-boolean v0, p0, LX/937;->g:Z

    .line 1433292
    iget-boolean v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    iput-boolean v0, p0, LX/937;->h:Z

    .line 1433293
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    iput-object v0, p0, LX/937;->i:Ljava/lang/String;

    .line 1433294
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    iput-object v0, p0, LX/937;->j:Ljava/lang/String;

    .line 1433295
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, LX/937;->k:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1433296
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    iput-object v0, p0, LX/937;->l:Ljava/lang/String;

    .line 1433297
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    iput-object v0, p0, LX/937;->m:LX/93G;

    .line 1433298
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    iput-object v0, p0, LX/937;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 1433299
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    iput-object v0, p0, LX/937;->o:LX/93E;

    .line 1433300
    :goto_0
    return-void

    .line 1433301
    :cond_0
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->b:LX/939;

    move-object v0, v0

    .line 1433302
    iput-object v0, p0, LX/937;->c:LX/939;

    .line 1433303
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1433304
    iput-object v0, p0, LX/937;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1433305
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v0

    .line 1433306
    iput-object v0, p0, LX/937;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1433307
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->e:LX/0Px;

    move-object v0, v0

    .line 1433308
    iput-object v0, p0, LX/937;->f:LX/0Px;

    .line 1433309
    iget-boolean v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->f:Z

    move v0, v0

    .line 1433310
    iput-boolean v0, p0, LX/937;->g:Z

    .line 1433311
    iget-boolean v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->g:Z

    move v0, v0

    .line 1433312
    iput-boolean v0, p0, LX/937;->h:Z

    .line 1433313
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1433314
    iput-object v0, p0, LX/937;->i:Ljava/lang/String;

    .line 1433315
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1433316
    iput-object v0, p0, LX/937;->j:Ljava/lang/String;

    .line 1433317
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v0

    .line 1433318
    iput-object v0, p0, LX/937;->k:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1433319
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1433320
    iput-object v0, p0, LX/937;->l:Ljava/lang/String;

    .line 1433321
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->l:LX/93G;

    move-object v0, v0

    .line 1433322
    iput-object v0, p0, LX/937;->m:LX/93G;

    .line 1433323
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-object v0, v0

    .line 1433324
    iput-object v0, p0, LX/937;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 1433325
    iget-object v0, p1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v0

    .line 1433326
    iput-object v0, p0, LX/937;->o:LX/93E;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
    .locals 2

    .prologue
    .line 1433327
    new-instance v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;-><init>(LX/937;)V

    return-object v0
.end method
