.class public final LX/ALg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/PreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/PreviewView;)V
    .locals 0

    .prologue
    .line 1665197
    iput-object p1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const v0, -0x9a10ef9

    invoke-static {v4, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1665198
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    iget-object v2, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v2, v2, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v2}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1665199
    iput-object v2, v1, LX/7gj;->l:Ljava/lang/String;

    .line 1665200
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    invoke-virtual {v1}, LX/7gj;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1665201
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    const-string v2, ""

    .line 1665202
    iput-object v2, v1, LX/7gj;->i:Ljava/lang/String;

    .line 1665203
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v1}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1665204
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1665205
    iget-object v2, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v2, v2, Lcom/facebook/backstage/camera/PreviewView;->f:LX/1Eo;

    sget-object v3, LX/7gQ;->CAMERA_CAMERA_ADD_CAPTION:LX/7gQ;

    invoke-virtual {v2, v3}, LX/1Eo;->a(LX/7gQ;)V

    .line 1665206
    :cond_0
    iget-object v2, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v2, v2, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v2}, LX/AM1;->b()V

    .line 1665207
    iget-object v2, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v2, v2, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v2}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->clearFocus()V

    .line 1665208
    iget-object v2, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v2, v2, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v2, v5}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setCursorVisible(Z)V

    .line 1665209
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1665210
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setVisibility(I)V

    .line 1665211
    :cond_1
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v6}, Landroid/widget/FrameLayout;->setDrawingCacheEnabled(Z)V

    .line 1665212
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->buildDrawingCache()V

    .line 1665213
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    iget-object v2, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v2, v2, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1665214
    iput-object v2, v1, LX/7gj;->c:Landroid/graphics/Bitmap;

    .line 1665215
    :cond_2
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1665216
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    invoke-static {v1}, Lcom/facebook/backstage/camera/PreviewView;->g(Lcom/facebook/backstage/camera/PreviewView;)V

    .line 1665217
    iget-object v1, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/ALf;

    invoke-direct {v2, p0}, LX/ALf;-><init>(LX/ALg;)V

    iget-object v3, p0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v3, v3, Lcom/facebook/backstage/camera/PreviewView;->b:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1665218
    const v1, -0x2aa159a0

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
