.class public LX/9ee;
.super LX/9e3;
.source ""


# instance fields
.field public a:LX/9cH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Landroid/graphics/Rect;

.field public c:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 1520122
    invoke-direct {p0, p1, p2}, LX/9e3;-><init>(Landroid/content/Context;Z)V

    .line 1520123
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9ee;->b:Landroid/graphics/Rect;

    .line 1520124
    const-class v0, LX/9ee;

    invoke-static {v0, p0}, LX/9ee;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1520125
    iget-object v0, p0, LX/9ee;->a:LX/9cH;

    iget-object p1, p0, LX/9ee;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, LX/9cH;->a(Landroid/graphics/Rect;)Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iput-object v0, p0, LX/9ee;->c:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1520126
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/9ee;

    const-class p0, LX/9cH;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9cH;

    iput-object v1, p1, LX/9ee;->a:LX/9cH;

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1520117
    invoke-super {p0, p1}, LX/9e3;->onDraw(Landroid/graphics/Canvas;)V

    .line 1520118
    iget-object v0, p0, LX/9e3;->c:Landroid/graphics/RectF;

    move-object v0, v0

    .line 1520119
    iget-object v1, p0, LX/9ee;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1520120
    iget-object v0, p0, LX/9ee;->c:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iget-object v1, p0, LX/9ee;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1520121
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1520116
    invoke-super {p0, p1}, LX/9e3;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9ee;->c:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
