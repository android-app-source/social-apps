.class public final LX/90e;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/90j;


# direct methods
.method public constructor <init>(LX/90j;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1429279
    iput-object p1, p0, LX/90e;->b:LX/90j;

    iput-object p2, p0, LX/90e;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1429294
    iget-object v0, p0, LX/90e;->b:LX/90j;

    iget-object v0, v0, LX/90j;->j:LX/03V;

    const-string v1, "minutiae_taggable_object_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1429295
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1429280
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1429281
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1429282
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    .line 1429283
    iget-object v1, p0, LX/90e;->b:LX/90j;

    iget-object v1, v1, LX/90j;->h:LX/5Lv;

    iget-object v2, p0, LX/90e;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/5Lv;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;Ljava/lang/String;)V

    .line 1429284
    iget-object v1, p0, LX/90e;->b:LX/90j;

    iget-object v1, v1, LX/90j;->g:LX/92C;

    .line 1429285
    const v2, 0x42000b

    const-string v3, "minutiae_object_picker_time_to_scroll_load"

    .line 1429286
    iget-object p1, v1, LX/92A;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {p1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result p1

    move v2, p1

    .line 1429287
    move v1, v2

    .line 1429288
    if-eqz v1, :cond_1

    .line 1429289
    iget-object v0, p0, LX/90e;->b:LX/90j;

    iget-object v0, v0, LX/90j;->g:LX/92C;

    invoke-virtual {v0}, LX/92C;->d()V

    .line 1429290
    :cond_0
    :goto_0
    iget-object v0, p0, LX/90e;->b:LX/90j;

    const v1, 0x78a824b7

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1429291
    return-void

    .line 1429292
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1429293
    iget-object v1, p0, LX/90e;->b:LX/90j;

    iget-object v1, v1, LX/90j;->k:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    new-instance v2, LX/90d;

    invoke-direct {v2, p0}, LX/90d;-><init>(LX/90e;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
