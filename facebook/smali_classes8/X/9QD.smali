.class public final LX/9QD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 44

    .prologue
    .line 1486748
    const/16 v40, 0x0

    .line 1486749
    const/16 v39, 0x0

    .line 1486750
    const/16 v38, 0x0

    .line 1486751
    const/16 v37, 0x0

    .line 1486752
    const/16 v36, 0x0

    .line 1486753
    const/16 v35, 0x0

    .line 1486754
    const/16 v34, 0x0

    .line 1486755
    const/16 v33, 0x0

    .line 1486756
    const/16 v32, 0x0

    .line 1486757
    const/16 v31, 0x0

    .line 1486758
    const/16 v30, 0x0

    .line 1486759
    const/16 v29, 0x0

    .line 1486760
    const/16 v28, 0x0

    .line 1486761
    const/16 v27, 0x0

    .line 1486762
    const/16 v26, 0x0

    .line 1486763
    const/16 v25, 0x0

    .line 1486764
    const/16 v24, 0x0

    .line 1486765
    const/16 v23, 0x0

    .line 1486766
    const/16 v22, 0x0

    .line 1486767
    const/16 v21, 0x0

    .line 1486768
    const/16 v20, 0x0

    .line 1486769
    const/16 v19, 0x0

    .line 1486770
    const/16 v18, 0x0

    .line 1486771
    const/16 v17, 0x0

    .line 1486772
    const/16 v16, 0x0

    .line 1486773
    const/4 v15, 0x0

    .line 1486774
    const/4 v14, 0x0

    .line 1486775
    const/4 v13, 0x0

    .line 1486776
    const/4 v12, 0x0

    .line 1486777
    const/4 v11, 0x0

    .line 1486778
    const/4 v10, 0x0

    .line 1486779
    const/4 v9, 0x0

    .line 1486780
    const/4 v8, 0x0

    .line 1486781
    const/4 v7, 0x0

    .line 1486782
    const/4 v6, 0x0

    .line 1486783
    const/4 v5, 0x0

    .line 1486784
    const/4 v4, 0x0

    .line 1486785
    const/4 v3, 0x0

    .line 1486786
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    .line 1486787
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1486788
    const/4 v3, 0x0

    .line 1486789
    :goto_0
    return v3

    .line 1486790
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1486791
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1f

    .line 1486792
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v41

    .line 1486793
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1486794
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_1

    if-eqz v41, :cond_1

    .line 1486795
    const-string v42, "admin_aware_group"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_2

    .line 1486796
    invoke-static/range {p0 .. p1}, LX/9QY;->a(LX/15w;LX/186;)I

    move-result v40

    goto :goto_1

    .line 1486797
    :cond_2
    const-string v42, "can_viewer_change_cover_photo"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_3

    .line 1486798
    const/4 v10, 0x1

    .line 1486799
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 1486800
    :cond_3
    const-string v42, "can_viewer_pin_post"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_4

    .line 1486801
    const/4 v9, 0x1

    .line 1486802
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 1486803
    :cond_4
    const-string v42, "can_viewer_report"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_5

    .line 1486804
    const/4 v8, 0x1

    .line 1486805
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 1486806
    :cond_5
    const-string v42, "communityFields"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_6

    .line 1486807
    invoke-static/range {p0 .. p1}, LX/9Q5;->a(LX/15w;LX/186;)I

    move-result v36

    goto :goto_1

    .line 1486808
    :cond_6
    const-string v42, "group_admined_page_members"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_7

    .line 1486809
    invoke-static/range {p0 .. p1}, LX/9Qb;->a(LX/15w;LX/186;)I

    move-result v35

    goto :goto_1

    .line 1486810
    :cond_7
    const-string v42, "group_configs"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_8

    .line 1486811
    invoke-static/range {p0 .. p1}, LX/5A9;->a(LX/15w;LX/186;)I

    move-result v34

    goto :goto_1

    .line 1486812
    :cond_8
    const-string v42, "group_member_profiles"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_9

    .line 1486813
    invoke-static/range {p0 .. p1}, LX/9Qm;->a(LX/15w;LX/186;)I

    move-result v33

    goto/16 :goto_1

    .line 1486814
    :cond_9
    const-string v42, "group_pinned_stories"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_a

    .line 1486815
    invoke-static/range {p0 .. p1}, LX/9Qd;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 1486816
    :cond_a
    const-string v42, "group_post_topics"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_b

    .line 1486817
    invoke-static/range {p0 .. p1}, LX/9QG;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 1486818
    :cond_b
    const-string v42, "group_purposes"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_c

    .line 1486819
    invoke-static/range {p0 .. p1}, LX/9Tf;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 1486820
    :cond_c
    const-string v42, "group_sell_config"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_d

    .line 1486821
    invoke-static/range {p0 .. p1}, LX/9LH;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 1486822
    :cond_d
    const-string v42, "id"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_e

    .line 1486823
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 1486824
    :cond_e
    const-string v42, "is_viewer_unconfirmed"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_f

    .line 1486825
    const/4 v7, 0x1

    .line 1486826
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 1486827
    :cond_f
    const-string v42, "learning_course_unit"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_10

    .line 1486828
    invoke-static/range {p0 .. p1}, LX/9Qp;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1486829
    :cond_10
    const-string v42, "prefill_group_welcome_prompt_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_11

    .line 1486830
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 1486831
    :cond_11
    const-string v42, "should_show_notif_settings_transition_nux"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_12

    .line 1486832
    const/4 v6, 0x1

    .line 1486833
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 1486834
    :cond_12
    const-string v42, "should_show_rooms_secondary_nux"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_13

    .line 1486835
    const/4 v5, 0x1

    .line 1486836
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 1486837
    :cond_13
    const-string v42, "subgroupsFields"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_14

    .line 1486838
    invoke-static/range {p0 .. p1}, LX/9QC;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1486839
    :cond_14
    const-string v42, "subscribe_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_15

    .line 1486840
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    goto/16 :goto_1

    .line 1486841
    :cond_15
    const-string v42, "suggested_purpose"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_16

    .line 1486842
    invoke-static/range {p0 .. p1}, LX/9Tg;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1486843
    :cond_16
    const-string v42, "tips_channel"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_17

    .line 1486844
    invoke-static/range {p0 .. p1}, LX/9Qg;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1486845
    :cond_17
    const-string v42, "unread_count"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_18

    .line 1486846
    const/4 v4, 0x1

    .line 1486847
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto/16 :goto_1

    .line 1486848
    :cond_18
    const-string v42, "user_might_be_selling"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_19

    .line 1486849
    const/4 v3, 0x1

    .line 1486850
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 1486851
    :cond_19
    const-string v42, "viewer_added_by"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1a

    .line 1486852
    invoke-static/range {p0 .. p1}, LX/9Qh;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1486853
    :cond_1a
    const-string v42, "viewer_admin_type"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1b

    .line 1486854
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    goto/16 :goto_1

    .line 1486855
    :cond_1b
    const-string v42, "viewer_invite_message"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1c

    .line 1486856
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1486857
    :cond_1c
    const-string v42, "viewer_invite_to_group"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1d

    .line 1486858
    invoke-static/range {p0 .. p1}, LX/9Qj;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1486859
    :cond_1d
    const-string v42, "viewer_join_state"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1e

    .line 1486860
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto/16 :goto_1

    .line 1486861
    :cond_1e
    const-string v42, "viewer_post_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_0

    .line 1486862
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    goto/16 :goto_1

    .line 1486863
    :cond_1f
    const/16 v41, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1486864
    const/16 v41, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1486865
    if-eqz v10, :cond_20

    .line 1486866
    const/4 v10, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1486867
    :cond_20
    if-eqz v9, :cond_21

    .line 1486868
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1486869
    :cond_21
    if-eqz v8, :cond_22

    .line 1486870
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1486871
    :cond_22
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486872
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486873
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486874
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486875
    const/16 v8, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486876
    const/16 v8, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486877
    const/16 v8, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486878
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486879
    const/16 v8, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1486880
    if-eqz v7, :cond_23

    .line 1486881
    const/16 v7, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1486882
    :cond_23
    const/16 v7, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1486883
    const/16 v7, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1486884
    if-eqz v6, :cond_24

    .line 1486885
    const/16 v6, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1486886
    :cond_24
    if-eqz v5, :cond_25

    .line 1486887
    const/16 v5, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1486888
    :cond_25
    const/16 v5, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1486889
    const/16 v5, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1486890
    const/16 v5, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1486891
    const/16 v5, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1486892
    if-eqz v4, :cond_26

    .line 1486893
    const/16 v4, 0x16

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 1486894
    :cond_26
    if-eqz v3, :cond_27

    .line 1486895
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1486896
    :cond_27
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1486897
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1486898
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1486899
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1486900
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1486901
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1486902
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
