.class public final enum LX/AJz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AJz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AJz;

.field public static final enum LOAD_UI:LX/AJz;

.field public static final enum OPTIMISTIC_UPDATE:LX/AJz;

.field public static final enum PREFETCH:LX/AJz;

.field public static final enum UNKNOWN:LX/AJz;

.field public static final enum USER_PULL_TO_REFRESH:LX/AJz;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1662445
    new-instance v0, LX/AJz;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/AJz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AJz;->UNKNOWN:LX/AJz;

    .line 1662446
    new-instance v0, LX/AJz;

    const-string v1, "LOAD_UI"

    invoke-direct {v0, v1, v3}, LX/AJz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AJz;->LOAD_UI:LX/AJz;

    .line 1662447
    new-instance v0, LX/AJz;

    const-string v1, "PREFETCH"

    invoke-direct {v0, v1, v4}, LX/AJz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AJz;->PREFETCH:LX/AJz;

    .line 1662448
    new-instance v0, LX/AJz;

    const-string v1, "OPTIMISTIC_UPDATE"

    invoke-direct {v0, v1, v5}, LX/AJz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AJz;->OPTIMISTIC_UPDATE:LX/AJz;

    .line 1662449
    new-instance v0, LX/AJz;

    const-string v1, "USER_PULL_TO_REFRESH"

    invoke-direct {v0, v1, v6}, LX/AJz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AJz;->USER_PULL_TO_REFRESH:LX/AJz;

    .line 1662450
    const/4 v0, 0x5

    new-array v0, v0, [LX/AJz;

    sget-object v1, LX/AJz;->UNKNOWN:LX/AJz;

    aput-object v1, v0, v2

    sget-object v1, LX/AJz;->LOAD_UI:LX/AJz;

    aput-object v1, v0, v3

    sget-object v1, LX/AJz;->PREFETCH:LX/AJz;

    aput-object v1, v0, v4

    sget-object v1, LX/AJz;->OPTIMISTIC_UPDATE:LX/AJz;

    aput-object v1, v0, v5

    sget-object v1, LX/AJz;->USER_PULL_TO_REFRESH:LX/AJz;

    aput-object v1, v0, v6

    sput-object v0, LX/AJz;->$VALUES:[LX/AJz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1662451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AJz;
    .locals 1

    .prologue
    .line 1662452
    const-class v0, LX/AJz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AJz;

    return-object v0
.end method

.method public static values()[LX/AJz;
    .locals 1

    .prologue
    .line 1662453
    sget-object v0, LX/AJz;->$VALUES:[LX/AJz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AJz;

    return-object v0
.end method
