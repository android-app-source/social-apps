.class public final LX/91V;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/91W;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/text/TextUtils$TruncateAt;

.field public d:I

.field public e:I

.field public f:F

.field public g:F

.field public h:F

.field public i:I

.field public j:Z

.field public k:I

.field public l:Landroid/content/res/ColorStateList;

.field public m:I

.field public n:Landroid/content/res/ColorStateList;

.field public o:I

.field public p:I

.field public q:I

.field public r:F

.field public s:F

.field public t:I

.field public u:Landroid/graphics/Typeface;

.field public v:Landroid/text/Layout$Alignment;

.field public w:I

.field public x:LX/1dQ;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x1000000

    .line 1430842
    invoke-static {}, LX/91W;->q()LX/91W;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1430843
    const/high16 v0, -0x80000000

    iput v0, p0, LX/91V;->d:I

    .line 1430844
    const v0, 0x7fffffff

    iput v0, p0, LX/91V;->e:I

    .line 1430845
    const v0, -0x777778

    iput v0, p0, LX/91V;->i:I

    .line 1430846
    iput v1, p0, LX/91V;->k:I

    .line 1430847
    const v0, -0x333334

    iput v0, p0, LX/91V;->m:I

    .line 1430848
    iput v1, p0, LX/91V;->o:I

    .line 1430849
    const/16 v0, 0xd

    iput v0, p0, LX/91V;->q:I

    .line 1430850
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/91V;->s:F

    .line 1430851
    sget v0, LX/91b;->a:I

    iput v0, p0, LX/91V;->t:I

    .line 1430852
    sget-object v0, LX/91b;->b:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/91V;->u:Landroid/graphics/Typeface;

    .line 1430853
    sget-object v0, LX/91b;->c:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LX/91V;->v:Landroid/text/Layout$Alignment;

    .line 1430854
    const v0, 0x800013

    iput v0, p0, LX/91V;->w:I

    .line 1430855
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1430856
    const-string v0, "SearchEditText"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1430857
    if-ne p0, p1, :cond_1

    .line 1430858
    :cond_0
    :goto_0
    return v0

    .line 1430859
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1430860
    goto :goto_0

    .line 1430861
    :cond_3
    check-cast p1, LX/91V;

    .line 1430862
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1430863
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1430864
    if-eq v2, v3, :cond_0

    .line 1430865
    iget-object v2, p0, LX/91V;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/91V;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/91V;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1430866
    goto :goto_0

    .line 1430867
    :cond_5
    iget-object v2, p1, LX/91V;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1430868
    :cond_6
    iget-object v2, p0, LX/91V;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/91V;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/91V;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1430869
    goto :goto_0

    .line 1430870
    :cond_8
    iget-object v2, p1, LX/91V;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1430871
    :cond_9
    iget-object v2, p0, LX/91V;->c:Landroid/text/TextUtils$TruncateAt;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/91V;->c:Landroid/text/TextUtils$TruncateAt;

    iget-object v3, p1, LX/91V;->c:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/text/TextUtils$TruncateAt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1430872
    goto :goto_0

    .line 1430873
    :cond_b
    iget-object v2, p1, LX/91V;->c:Landroid/text/TextUtils$TruncateAt;

    if-nez v2, :cond_a

    .line 1430874
    :cond_c
    iget v2, p0, LX/91V;->d:I

    iget v3, p1, LX/91V;->d:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1430875
    goto :goto_0

    .line 1430876
    :cond_d
    iget v2, p0, LX/91V;->e:I

    iget v3, p1, LX/91V;->e:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1430877
    goto :goto_0

    .line 1430878
    :cond_e
    iget v2, p0, LX/91V;->f:F

    iget v3, p1, LX/91V;->f:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_f

    move v0, v1

    .line 1430879
    goto :goto_0

    .line 1430880
    :cond_f
    iget v2, p0, LX/91V;->g:F

    iget v3, p1, LX/91V;->g:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_10

    move v0, v1

    .line 1430881
    goto :goto_0

    .line 1430882
    :cond_10
    iget v2, p0, LX/91V;->h:F

    iget v3, p1, LX/91V;->h:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_11

    move v0, v1

    .line 1430883
    goto/16 :goto_0

    .line 1430884
    :cond_11
    iget v2, p0, LX/91V;->i:I

    iget v3, p1, LX/91V;->i:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1430885
    goto/16 :goto_0

    .line 1430886
    :cond_12
    iget-boolean v2, p0, LX/91V;->j:Z

    iget-boolean v3, p1, LX/91V;->j:Z

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1430887
    goto/16 :goto_0

    .line 1430888
    :cond_13
    iget v2, p0, LX/91V;->k:I

    iget v3, p1, LX/91V;->k:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 1430889
    goto/16 :goto_0

    .line 1430890
    :cond_14
    iget-object v2, p0, LX/91V;->l:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/91V;->l:Landroid/content/res/ColorStateList;

    iget-object v3, p1, LX/91V;->l:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 1430891
    goto/16 :goto_0

    .line 1430892
    :cond_16
    iget-object v2, p1, LX/91V;->l:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_15

    .line 1430893
    :cond_17
    iget v2, p0, LX/91V;->m:I

    iget v3, p1, LX/91V;->m:I

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 1430894
    goto/16 :goto_0

    .line 1430895
    :cond_18
    iget-object v2, p0, LX/91V;->n:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1a

    iget-object v2, p0, LX/91V;->n:Landroid/content/res/ColorStateList;

    iget-object v3, p1, LX/91V;->n:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 1430896
    goto/16 :goto_0

    .line 1430897
    :cond_1a
    iget-object v2, p1, LX/91V;->n:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_19

    .line 1430898
    :cond_1b
    iget v2, p0, LX/91V;->o:I

    iget v3, p1, LX/91V;->o:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 1430899
    goto/16 :goto_0

    .line 1430900
    :cond_1c
    iget v2, p0, LX/91V;->p:I

    iget v3, p1, LX/91V;->p:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 1430901
    goto/16 :goto_0

    .line 1430902
    :cond_1d
    iget v2, p0, LX/91V;->q:I

    iget v3, p1, LX/91V;->q:I

    if-eq v2, v3, :cond_1e

    move v0, v1

    .line 1430903
    goto/16 :goto_0

    .line 1430904
    :cond_1e
    iget v2, p0, LX/91V;->r:F

    iget v3, p1, LX/91V;->r:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_1f

    move v0, v1

    .line 1430905
    goto/16 :goto_0

    .line 1430906
    :cond_1f
    iget v2, p0, LX/91V;->s:F

    iget v3, p1, LX/91V;->s:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_20

    move v0, v1

    .line 1430907
    goto/16 :goto_0

    .line 1430908
    :cond_20
    iget v2, p0, LX/91V;->t:I

    iget v3, p1, LX/91V;->t:I

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 1430909
    goto/16 :goto_0

    .line 1430910
    :cond_21
    iget-object v2, p0, LX/91V;->u:Landroid/graphics/Typeface;

    if-eqz v2, :cond_23

    iget-object v2, p0, LX/91V;->u:Landroid/graphics/Typeface;

    iget-object v3, p1, LX/91V;->u:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    :cond_22
    move v0, v1

    .line 1430911
    goto/16 :goto_0

    .line 1430912
    :cond_23
    iget-object v2, p1, LX/91V;->u:Landroid/graphics/Typeface;

    if-nez v2, :cond_22

    .line 1430913
    :cond_24
    iget-object v2, p0, LX/91V;->v:Landroid/text/Layout$Alignment;

    if-eqz v2, :cond_26

    iget-object v2, p0, LX/91V;->v:Landroid/text/Layout$Alignment;

    iget-object v3, p1, LX/91V;->v:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, Landroid/text/Layout$Alignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    :cond_25
    move v0, v1

    .line 1430914
    goto/16 :goto_0

    .line 1430915
    :cond_26
    iget-object v2, p1, LX/91V;->v:Landroid/text/Layout$Alignment;

    if-nez v2, :cond_25

    .line 1430916
    :cond_27
    iget v2, p0, LX/91V;->w:I

    iget v3, p1, LX/91V;->w:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1430917
    goto/16 :goto_0
.end method
