.class public final LX/9a4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1512666
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1512667
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1512668
    :goto_0
    return v1

    .line 1512669
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1512670
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1512671
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1512672
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1512673
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1512674
    const-string v5, "catalog_items"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1512675
    const/4 v4, 0x0

    .line 1512676
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 1512677
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1512678
    :goto_2
    move v3, v4

    .line 1512679
    goto :goto_1

    .line 1512680
    :cond_2
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1512681
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1512682
    :cond_3
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1512683
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1512684
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1512685
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1512686
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1512687
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1512688
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1512689
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1512690
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1512691
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1512692
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1512693
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 1512694
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1512695
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1512696
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_8

    .line 1512697
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_8

    .line 1512698
    const/4 v6, 0x0

    .line 1512699
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_e

    .line 1512700
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1512701
    :goto_5
    move v5, v6

    .line 1512702
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1512703
    :cond_8
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1512704
    goto :goto_3

    .line 1512705
    :cond_9
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1512706
    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1512707
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_a
    move v3, v4

    goto :goto_3

    .line 1512708
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1512709
    :cond_c
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_d

    .line 1512710
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1512711
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1512712
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_c

    if-eqz v7, :cond_c

    .line 1512713
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1512714
    invoke-static {p0, p1}, LX/9a3;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_6

    .line 1512715
    :cond_d
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1512716
    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1512717
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_e
    move v5, v6

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1512718
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1512719
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1512720
    if-eqz v0, :cond_3

    .line 1512721
    const-string v1, "catalog_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512722
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1512723
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1512724
    if-eqz v1, :cond_2

    .line 1512725
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512726
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1512727
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1512728
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1512729
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1512730
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1512731
    if-eqz v4, :cond_0

    .line 1512732
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512733
    invoke-static {p0, v4, p2, p3}, LX/9a3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1512734
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1512735
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1512736
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1512737
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1512738
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1512739
    if-eqz v0, :cond_4

    .line 1512740
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512741
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1512742
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1512743
    if-eqz v0, :cond_5

    .line 1512744
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512745
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1512746
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1512747
    return-void
.end method
