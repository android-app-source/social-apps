.class public final enum LX/8ur;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8ur;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8ur;

.field public static final enum CLEAR:LX/8ur;

.field public static final enum INPUT_TYPE_SWITCH:LX/8ur;

.field public static final enum NONE:LX/8ur;


# instance fields
.field private final mAccessibilityTextResId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1415806
    new-instance v0, LX/8ur;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3, v3}, LX/8ur;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8ur;->NONE:LX/8ur;

    .line 1415807
    new-instance v0, LX/8ur;

    const-string v1, "CLEAR"

    const v2, 0x7f0801a9

    invoke-direct {v0, v1, v4, v2}, LX/8ur;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8ur;->CLEAR:LX/8ur;

    .line 1415808
    new-instance v0, LX/8ur;

    const-string v1, "INPUT_TYPE_SWITCH"

    const v2, 0x7f0801aa

    invoke-direct {v0, v1, v5, v2}, LX/8ur;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8ur;->INPUT_TYPE_SWITCH:LX/8ur;

    .line 1415809
    const/4 v0, 0x3

    new-array v0, v0, [LX/8ur;

    sget-object v1, LX/8ur;->NONE:LX/8ur;

    aput-object v1, v0, v3

    sget-object v1, LX/8ur;->CLEAR:LX/8ur;

    aput-object v1, v0, v4

    sget-object v1, LX/8ur;->INPUT_TYPE_SWITCH:LX/8ur;

    aput-object v1, v0, v5

    sput-object v0, LX/8ur;->$VALUES:[LX/8ur;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1415803
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1415804
    iput p3, p0, LX/8ur;->mAccessibilityTextResId:I

    .line 1415805
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8ur;
    .locals 1

    .prologue
    .line 1415810
    const-class v0, LX/8ur;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8ur;

    return-object v0
.end method

.method public static values()[LX/8ur;
    .locals 1

    .prologue
    .line 1415802
    sget-object v0, LX/8ur;->$VALUES:[LX/8ur;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8ur;

    return-object v0
.end method


# virtual methods
.method public final getAccessibilityText()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1415801
    iget v0, p0, LX/8ur;->mAccessibilityTextResId:I

    return v0
.end method
