.class public LX/9WR;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/9WS;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1501180
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1501181
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/9WR;->a:Landroid/view/LayoutInflater;

    .line 1501182
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1501183
    iget-object v0, p0, LX/9WR;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030bdb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 1501184
    invoke-virtual {p0, p1}, LX/9WR;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9WS;

    .line 1501185
    iget-object v2, v1, LX/9WS;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1501186
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1501187
    iget-object v2, v1, LX/9WS;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1501188
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1501189
    return-object v0
.end method
