.class public final LX/9IX;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9IY;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public b:LX/5Gs;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:LX/9FA;

.field public f:LX/9Fi;

.field public final synthetic g:LX/9IY;


# direct methods
.method public constructor <init>(LX/9IY;)V
    .locals 1

    .prologue
    .line 1463216
    iput-object p1, p0, LX/9IX;->g:LX/9IY;

    .line 1463217
    move-object v0, p1

    .line 1463218
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1463219
    const/4 v0, -0x1

    iput v0, p0, LX/9IX;->c:I

    .line 1463220
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1463215
    const-string v0, "LoadMoreCommentsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1463221
    if-ne p0, p1, :cond_1

    .line 1463222
    :cond_0
    :goto_0
    return v0

    .line 1463223
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1463224
    goto :goto_0

    .line 1463225
    :cond_3
    check-cast p1, LX/9IX;

    .line 1463226
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1463227
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1463228
    if-eq v2, v3, :cond_0

    .line 1463229
    iget-object v2, p0, LX/9IX;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9IX;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p1, LX/9IX;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1463230
    goto :goto_0

    .line 1463231
    :cond_5
    iget-object v2, p1, LX/9IX;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v2, :cond_4

    .line 1463232
    :cond_6
    iget-object v2, p0, LX/9IX;->b:LX/5Gs;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/9IX;->b:LX/5Gs;

    iget-object v3, p1, LX/9IX;->b:LX/5Gs;

    invoke-virtual {v2, v3}, LX/5Gs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1463233
    goto :goto_0

    .line 1463234
    :cond_8
    iget-object v2, p1, LX/9IX;->b:LX/5Gs;

    if-nez v2, :cond_7

    .line 1463235
    :cond_9
    iget v2, p0, LX/9IX;->c:I

    iget v3, p1, LX/9IX;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1463236
    goto :goto_0

    .line 1463237
    :cond_a
    iget-object v2, p0, LX/9IX;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/9IX;->d:Ljava/lang/String;

    iget-object v3, p1, LX/9IX;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1463238
    goto :goto_0

    .line 1463239
    :cond_c
    iget-object v2, p1, LX/9IX;->d:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 1463240
    :cond_d
    iget-object v2, p0, LX/9IX;->e:LX/9FA;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/9IX;->e:LX/9FA;

    iget-object v3, p1, LX/9IX;->e:LX/9FA;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1463241
    goto :goto_0

    .line 1463242
    :cond_f
    iget-object v2, p1, LX/9IX;->e:LX/9FA;

    if-nez v2, :cond_e

    .line 1463243
    :cond_10
    iget-object v2, p0, LX/9IX;->f:LX/9Fi;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/9IX;->f:LX/9Fi;

    iget-object v3, p1, LX/9IX;->f:LX/9Fi;

    invoke-virtual {v2, v3}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1463244
    goto :goto_0

    .line 1463245
    :cond_11
    iget-object v2, p1, LX/9IX;->f:LX/9Fi;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
