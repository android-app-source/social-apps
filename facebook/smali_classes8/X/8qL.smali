.class public final LX/8qL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public b:Lcom/facebook/graphql/model/GraphQLComment;

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:J

.field public l:Lcom/facebook/ipc/media/MediaItem;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:LX/21y;

.field public p:Z

.field public q:Z

.field public r:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1406888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/8qL;
    .locals 2

    .prologue
    .line 1406850
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    .line 1406851
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v1, v1

    .line 1406852
    iput-object v1, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406853
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->b:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v1, v1

    .line 1406854
    iput-object v1, v0, LX/8qL;->b:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1406855
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v1

    .line 1406856
    iput-object v1, v0, LX/8qL;->d:Ljava/lang/String;

    .line 1406857
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v1

    .line 1406858
    iput-object v1, v0, LX/8qL;->e:Ljava/lang/String;

    .line 1406859
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1406860
    iput-object v1, v0, LX/8qL;->f:Ljava/lang/String;

    .line 1406861
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 1406862
    iput-object v1, v0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1406863
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h:Z

    move v1, v1

    .line 1406864
    iput-boolean v1, v0, LX/8qL;->h:Z

    .line 1406865
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->i:Z

    move v1, v1

    .line 1406866
    iput-boolean v1, v0, LX/8qL;->i:Z

    .line 1406867
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    move v1, v1

    .line 1406868
    iput-boolean v1, v0, LX/8qL;->j:Z

    .line 1406869
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v1, v1

    .line 1406870
    invoke-virtual {v0, v1}, LX/8qL;->a(Ljava/lang/Long;)LX/8qL;

    .line 1406871
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->l:Lcom/facebook/ipc/media/MediaItem;

    move-object v1, v1

    .line 1406872
    iput-object v1, v0, LX/8qL;->l:Lcom/facebook/ipc/media/MediaItem;

    .line 1406873
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1406874
    iput-object v1, v0, LX/8qL;->m:Ljava/lang/String;

    .line 1406875
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->n:Ljava/lang/String;

    move-object v1, v1

    .line 1406876
    iput-object v1, v0, LX/8qL;->n:Ljava/lang/String;

    .line 1406877
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->o:LX/21y;

    move-object v1, v1

    .line 1406878
    iput-object v1, v0, LX/8qL;->o:LX/21y;

    .line 1406879
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->p:Z

    move v1, v1

    .line 1406880
    iput-boolean v1, v0, LX/8qL;->p:Z

    .line 1406881
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v1, v1

    .line 1406882
    iput-boolean v1, v0, LX/8qL;->q:Z

    .line 1406883
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->r:Z

    move v1, v1

    .line 1406884
    iput-boolean v1, v0, LX/8qL;->r:Z

    .line 1406885
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 1406886
    iput-object v1, v0, LX/8qL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1406887
    return-object v0
.end method


# virtual methods
.method public final a(LX/21y;)LX/8qL;
    .locals 0

    .prologue
    .line 1406848
    iput-object p1, p0, LX/8qL;->o:LX/21y;

    .line 1406849
    return-object p0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/8qL;
    .locals 0

    .prologue
    .line 1406846
    iput-object p1, p0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1406847
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/8qL;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/8qL;"
        }
    .end annotation

    .prologue
    .line 1406844
    iput-object p1, p0, LX/8qL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1406845
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/8qL;
    .locals 0

    .prologue
    .line 1406842
    iput-object p1, p0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406843
    return-object p0
.end method

.method public final a(Ljava/lang/Long;)LX/8qL;
    .locals 2

    .prologue
    .line 1406839
    if-eqz p1, :cond_0

    .line 1406840
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/8qL;->k:J

    .line 1406841
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/8qL;
    .locals 0

    .prologue
    .line 1406837
    iput-object p1, p0, LX/8qL;->d:Ljava/lang/String;

    .line 1406838
    return-object p0
.end method

.method public final a(Z)LX/8qL;
    .locals 0

    .prologue
    .line 1406889
    iput-boolean p1, p0, LX/8qL;->h:Z

    .line 1406890
    return-object p0
.end method

.method public final a()Lcom/facebook/ufiservices/flyout/FeedbackParams;
    .locals 2

    .prologue
    .line 1406835
    new-instance v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-direct {v0, p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;-><init>(LX/8qL;)V

    .line 1406836
    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/8qL;
    .locals 0

    .prologue
    .line 1406833
    iput-object p1, p0, LX/8qL;->e:Ljava/lang/String;

    .line 1406834
    return-object p0
.end method

.method public final b(Z)LX/8qL;
    .locals 0

    .prologue
    .line 1406831
    iput-boolean p1, p0, LX/8qL;->i:Z

    .line 1406832
    return-object p0
.end method

.method public final c(Z)LX/8qL;
    .locals 0

    .prologue
    .line 1406829
    iput-boolean p1, p0, LX/8qL;->j:Z

    .line 1406830
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/8qL;
    .locals 0

    .prologue
    .line 1406827
    iput-object p1, p0, LX/8qL;->m:Ljava/lang/String;

    .line 1406828
    return-object p0
.end method

.method public final d(Z)LX/8qL;
    .locals 0

    .prologue
    .line 1406825
    iput-boolean p1, p0, LX/8qL;->p:Z

    .line 1406826
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/8qL;
    .locals 0

    .prologue
    .line 1406823
    iput-object p1, p0, LX/8qL;->n:Ljava/lang/String;

    .line 1406824
    return-object p0
.end method

.method public final e(Z)LX/8qL;
    .locals 0

    .prologue
    .line 1406821
    iput-boolean p1, p0, LX/8qL;->q:Z

    .line 1406822
    return-object p0
.end method

.method public final f(Z)LX/8qL;
    .locals 0

    .prologue
    .line 1406819
    iput-boolean p1, p0, LX/8qL;->r:Z

    .line 1406820
    return-object p0
.end method
