.class public LX/92Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/92Q;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0SG;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432270
    iput-object p1, p0, LX/92Q;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1432271
    iput-object p2, p0, LX/92Q;->b:LX/0SG;

    .line 1432272
    return-void
.end method

.method public static a(LX/0QB;)LX/92Q;
    .locals 5

    .prologue
    .line 1432273
    sget-object v0, LX/92Q;->c:LX/92Q;

    if-nez v0, :cond_1

    .line 1432274
    const-class v1, LX/92Q;

    monitor-enter v1

    .line 1432275
    :try_start_0
    sget-object v0, LX/92Q;->c:LX/92Q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1432276
    if-eqz v2, :cond_0

    .line 1432277
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1432278
    new-instance p0, LX/92Q;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/92Q;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 1432279
    move-object v0, p0

    .line 1432280
    sput-object v0, LX/92Q;->c:LX/92Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432281
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1432282
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432283
    :cond_1
    sget-object v0, LX/92Q;->c:LX/92Q;

    return-object v0

    .line 1432284
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1432285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
