.class public final LX/9Ev;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/SpeechCommentDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V
    .locals 0

    .prologue
    .line 1457614
    iput-object p1, p0, LX/9Ev;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x15dd8d65

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457615
    iget-object v1, p0, LX/9Ev;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    iget-object v1, v1, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    if-nez v1, :cond_0

    .line 1457616
    const v1, 0x4c284a98    # 4.4116576E7f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1457617
    :goto_0
    return-void

    .line 1457618
    :cond_0
    iget-object v1, p0, LX/9Ev;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    iget-object v1, v1, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    iget-object v2, p0, LX/9Ev;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v2}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->k()Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x1

    .line 1457619
    invoke-static {v1}, LX/9F0;->g(LX/9F0;)V

    .line 1457620
    iget-object v3, v1, LX/9F0;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3, v2}, LX/9F0;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v4

    .line 1457621
    iget-object v3, v1, LX/9F0;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457622
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1457623
    if-eqz v5, :cond_2

    .line 1457624
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1457625
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1457626
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1457627
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    :goto_1
    move v3, v5

    .line 1457628
    if-nez v3, :cond_1

    .line 1457629
    iget-object v3, v1, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    const v4, 0x7f081268

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1457630
    :goto_2
    iget-object v1, p0, LX/9Ev;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->a(Ljava/lang/CharSequence;)V

    .line 1457631
    const v1, 0x2b66282c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1457632
    :cond_1
    iget-object v3, v1, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    sget-object v5, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-static {v3, v5}, LX/82k;->a(Landroid/content/Context;LX/1EO;)Ljava/lang/String;

    move-result-object v3

    .line 1457633
    new-instance v5, LX/21A;

    invoke-direct {v5}, LX/21A;-><init>()V

    const-string v6, "speech_dialog"

    .line 1457634
    iput-object v6, v5, LX/21A;->c:Ljava/lang/String;

    .line 1457635
    move-object v5, v5

    .line 1457636
    iput-object v3, v5, LX/21A;->d:Ljava/lang/String;

    .line 1457637
    move-object v3, v5

    .line 1457638
    invoke-virtual {v3}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v5

    .line 1457639
    iget-object v3, v1, LX/9F0;->m:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9Ck;

    iget-object v6, v1, LX/9F0;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1457640
    invoke-static {v3}, LX/9Ck;->a(LX/9Ck;)V

    .line 1457641
    iget-object v8, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 1457642
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v9

    .line 1457643
    invoke-static {v9}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457644
    iget-object v8, v3, LX/9Ck;->b:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1K9;

    const-class p1, LX/8px;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v9

    new-instance v2, LX/9Cj;

    invoke-direct {v2, v3, v6}, LX/9Cj;-><init>(LX/9Ck;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v8, p1, v9, v2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v8

    iput-object v8, v3, LX/9Ck;->a:LX/6Vi;

    .line 1457645
    iget-object v3, v1, LX/9F0;->j:LX/3iG;

    sget-object v6, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v3, v6}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v3

    iget-object v6, v1, LX/9F0;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    invoke-virtual {v3, v4, v6, v5}, LX/3iK;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1457646
    iput-boolean v7, v1, LX/9F0;->h:Z

    .line 1457647
    iget-object v3, v1, LX/9F0;->i:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8pb;

    iget-object v4, v4, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/8pb;->a(Ljava/lang/String;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    .line 1457648
    iget-object v3, v1, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v3}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1457649
    iget-object v3, v1, LX/9F0;->n:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9D5;

    .line 1457650
    iget-object v4, v3, LX/9D5;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0if;

    .line 1457651
    sget-object v5, LX/0ig;->M:LX/0ih;

    move-object v5, v5

    .line 1457652
    const-string v6, "user_posted_comment"

    invoke-virtual {v4, v5, v6}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1457653
    goto/16 :goto_2

    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_1
.end method
