.class public final LX/ABQ;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1640557
    const-class v1, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;

    const v0, 0x693a08f5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchAvailableAudiencesQuery"

    const-string v6, "6eb3185e745f68f933ca2338f5aecd03"

    const-string v7, "node"

    const-string v8, "10155069964691729"

    const-string v9, "10155259087126729"

    .line 1640558
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1640559
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1640560
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1640561
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1640562
    sparse-switch v0, :sswitch_data_0

    .line 1640563
    :goto_0
    return-object p1

    .line 1640564
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1640565
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1640566
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1640567
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1640568
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1640569
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1640570
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1640571
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1640572
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x73d66cad -> :sswitch_2
        -0x46beb116 -> :sswitch_4
        -0x4651e685 -> :sswitch_7
        -0x438a99f9 -> :sswitch_6
        -0x2fe52f35 -> :sswitch_0
        -0x197a9d8e -> :sswitch_5
        0x5f559782 -> :sswitch_8
        0x72068209 -> :sswitch_1
        0x732c0b14 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1640573
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1640574
    :goto_1
    return v0

    .line 1640575
    :pswitch_0
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_1
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :pswitch_3
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :pswitch_4
    const-string v2, "8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 1640576
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1640577
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1640578
    :pswitch_7
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1640579
    :pswitch_8
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1640580
    :pswitch_9
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
