.class public final LX/9Q5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1486351
    const/4 v15, 0x0

    .line 1486352
    const/4 v14, 0x0

    .line 1486353
    const/4 v13, 0x0

    .line 1486354
    const/4 v12, 0x0

    .line 1486355
    const/4 v11, 0x0

    .line 1486356
    const/4 v10, 0x0

    .line 1486357
    const/4 v9, 0x0

    .line 1486358
    const/4 v8, 0x0

    .line 1486359
    const/4 v7, 0x0

    .line 1486360
    const/4 v6, 0x0

    .line 1486361
    const/4 v5, 0x0

    .line 1486362
    const/4 v4, 0x0

    .line 1486363
    const/4 v3, 0x0

    .line 1486364
    const/4 v2, 0x0

    .line 1486365
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1486366
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1486367
    const/4 v2, 0x0

    .line 1486368
    :goto_0
    return v2

    .line 1486369
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1486370
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 1486371
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1486372
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1486373
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1486374
    const-string v17, "community_creation_unit"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1486375
    invoke-static/range {p0 .. p1}, LX/9PV;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1486376
    :cond_2
    const-string v17, "community_forsale_stories"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1486377
    invoke-static/range {p0 .. p1}, LX/9Pd;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1486378
    :cond_3
    const-string v17, "community_forum_child_groups"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1486379
    invoke-static/range {p0 .. p1}, LX/9Pe;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1486380
    :cond_4
    const-string v17, "community_news_collection"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1486381
    invoke-static/range {p0 .. p1}, LX/9Q0;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1486382
    :cond_5
    const-string v17, "community_nux_answers"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1486383
    invoke-static/range {p0 .. p1}, LX/9Q3;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1486384
    :cond_6
    const-string v17, "community_nux_questions"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1486385
    invoke-static/range {p0 .. p1}, LX/9Q4;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1486386
    :cond_7
    const-string v17, "group_content_sections"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1486387
    invoke-static/range {p0 .. p1}, LX/9Q6;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1486388
    :cond_8
    const-string v17, "group_events"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1486389
    invoke-static/range {p0 .. p1}, LX/9PX;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1486390
    :cond_9
    const-string v17, "group_trending_stories"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1486391
    invoke-static/range {p0 .. p1}, LX/9Q8;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1486392
    :cond_a
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1486393
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1486394
    :cond_b
    const-string v17, "related_groups"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 1486395
    invoke-static/range {p0 .. p1}, LX/9Q7;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1486396
    :cond_c
    const-string v17, "suggested_members"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 1486397
    invoke-static/range {p0 .. p1}, LX/9Pt;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1486398
    :cond_d
    const-string v17, "viewer_child_groups"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 1486399
    invoke-static/range {p0 .. p1}, LX/9Q9;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1486400
    :cond_e
    const-string v17, "viewer_join_community_context"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1486401
    invoke-static/range {p0 .. p1}, LX/9QB;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1486402
    :cond_f
    const/16 v16, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1486403
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1486404
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1486405
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1486406
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1486407
    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1486408
    const/4 v11, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1486409
    const/4 v10, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1486410
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1486411
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1486412
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1486413
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1486414
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1486415
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1486416
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1486417
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1486418
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1486419
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486420
    if-eqz v0, :cond_0

    .line 1486421
    const-string v1, "community_creation_unit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486422
    invoke-static {p0, v0, p2, p3}, LX/9PV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486423
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486424
    if-eqz v0, :cond_1

    .line 1486425
    const-string v1, "community_forsale_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486426
    invoke-static {p0, v0, p2, p3}, LX/9Pd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486427
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486428
    if-eqz v0, :cond_2

    .line 1486429
    const-string v1, "community_forum_child_groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486430
    invoke-static {p0, v0, p2, p3}, LX/9Pe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486431
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486432
    if-eqz v0, :cond_3

    .line 1486433
    const-string v1, "community_news_collection"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486434
    invoke-static {p0, v0, p2, p3}, LX/9Q0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486435
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486436
    if-eqz v0, :cond_4

    .line 1486437
    const-string v1, "community_nux_answers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486438
    invoke-static {p0, v0, p2, p3}, LX/9Q3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486439
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486440
    if-eqz v0, :cond_5

    .line 1486441
    const-string v1, "community_nux_questions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486442
    invoke-static {p0, v0, p2, p3}, LX/9Q4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486443
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486444
    if-eqz v0, :cond_6

    .line 1486445
    const-string v1, "group_content_sections"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486446
    invoke-static {p0, v0, p2, p3}, LX/9Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486447
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486448
    if-eqz v0, :cond_7

    .line 1486449
    const-string v1, "group_events"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486450
    invoke-static {p0, v0, p2, p3}, LX/9PX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486451
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486452
    if-eqz v0, :cond_8

    .line 1486453
    const-string v1, "group_trending_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486454
    invoke-static {p0, v0, p2, p3}, LX/9Q8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486455
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1486456
    if-eqz v0, :cond_9

    .line 1486457
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486458
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1486459
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486460
    if-eqz v0, :cond_a

    .line 1486461
    const-string v1, "related_groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486462
    invoke-static {p0, v0, p2, p3}, LX/9Q7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486463
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486464
    if-eqz v0, :cond_b

    .line 1486465
    const-string v1, "suggested_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486466
    invoke-static {p0, v0, p2, p3}, LX/9Pt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486467
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486468
    if-eqz v0, :cond_c

    .line 1486469
    const-string v1, "viewer_child_groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486470
    invoke-static {p0, v0, p2, p3}, LX/9Q9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486471
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1486472
    if-eqz v0, :cond_d

    .line 1486473
    const-string v1, "viewer_join_community_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486474
    invoke-static {p0, v0, p2, p3}, LX/9QB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1486475
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1486476
    return-void
.end method
