.class public final LX/ANa;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

.field private b:I

.field private c:I

.field private d:F

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 1

    .prologue
    .line 1667759
    iput-object p1, p0, LX/ANa;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 1667760
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ANa;->e:Z

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1667772
    iget-boolean v1, p0, LX/ANa;->e:Z

    if-nez v1, :cond_0

    .line 1667773
    :goto_0
    return v0

    .line 1667774
    :cond_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    .line 1667775
    iget v2, p0, LX/ANa;->d:F

    sub-float/2addr v1, v2

    iget-object v2, p0, LX/ANa;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    .line 1667776
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string p1, "window"

    invoke-virtual {v3, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 1667777
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    move v2, v3

    .line 1667778
    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, LX/ANa;->c:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, LX/ANa;->b:I

    add-int/2addr v1, v2

    .line 1667779
    iget v2, p0, LX/ANa;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1667780
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v1, v0}, LX/6Ia;->a(I)V

    .line 1667781
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1667761
    sget-object v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v2}, LX/6Ia;->k()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1667762
    :cond_0
    :goto_0
    return v0

    .line 1667763
    :cond_1
    const/4 v2, 0x0

    .line 1667764
    :try_start_0
    sget-object v3, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v3}, LX/6Ia;->a()LX/6IP;
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1667765
    :goto_1
    if-eqz v2, :cond_2

    invoke-interface {v2}, LX/6IP;->h()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1667766
    :cond_2
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->i()I

    move-result v0

    iput v0, p0, LX/ANa;->b:I

    .line 1667767
    invoke-interface {v2}, LX/6IP;->g()I

    move-result v0

    iput v0, p0, LX/ANa;->c:I

    .line 1667768
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, LX/ANa;->d:F

    .line 1667769
    iput-boolean v1, p0, LX/ANa;->e:Z

    move v0, v1

    .line 1667770
    goto :goto_0

    .line 1667771
    :catch_0
    goto :goto_1
.end method
