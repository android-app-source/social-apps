.class public final LX/ARu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/ARt;)V
    .locals 1

    .prologue
    .line 1673345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673346
    iget-boolean v0, p1, LX/ARt;->a:Z

    iput-boolean v0, p0, LX/ARu;->a:Z

    .line 1673347
    iget-object v0, p1, LX/ARt;->b:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/ARu;->b:LX/0Rf;

    .line 1673348
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1673349
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(isHidden="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, LX/ARu;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deactivateReasons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/ARu;->b:LX/0Rf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
