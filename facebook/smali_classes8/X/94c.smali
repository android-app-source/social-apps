.class public LX/94c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/94b;

.field public final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/94b;)V
    .locals 1

    .prologue
    .line 1435561
    const-string v0, ""

    invoke-direct {p0, p1, v0}, LX/94c;-><init>(LX/94b;Ljava/lang/String;)V

    .line 1435562
    return-void
.end method

.method private constructor <init>(LX/94b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1435563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435564
    iput-object p1, p0, LX/94c;->a:LX/94b;

    .line 1435565
    iput-object p2, p0, LX/94c;->b:Ljava/lang/String;

    .line 1435566
    return-void
.end method

.method public static c()LX/94c;
    .locals 2

    .prologue
    .line 1435560
    new-instance v0, LX/94c;

    sget-object v1, LX/94b;->DEFAULT:LX/94b;

    invoke-direct {v0, v1}, LX/94c;-><init>(LX/94b;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1435554
    if-ne p0, p1, :cond_1

    .line 1435555
    :cond_0
    :goto_0
    return v0

    .line 1435556
    :cond_1
    instance-of v2, p1, LX/94c;

    if-nez v2, :cond_2

    move v0, v1

    .line 1435557
    goto :goto_0

    .line 1435558
    :cond_2
    check-cast p1, LX/94c;

    .line 1435559
    iget-object v2, p0, LX/94c;->a:LX/94b;

    iget-object v3, p1, LX/94c;->a:LX/94b;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/94c;->b:Ljava/lang/String;

    iget-object v3, p1, LX/94c;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1435553
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/94c;->a:LX/94b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/94c;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
