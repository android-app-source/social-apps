.class public final LX/9k2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/ipc/model/PageTopic;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9k4;


# direct methods
.method public constructor <init>(LX/9k4;)V
    .locals 0

    .prologue
    .line 1530726
    iput-object p1, p0, LX/9k2;->a:LX/9k4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1530727
    check-cast p1, Lcom/facebook/ipc/model/PageTopic;

    check-cast p2, Lcom/facebook/ipc/model/PageTopic;

    .line 1530728
    iget v0, p2, Lcom/facebook/ipc/model/PageTopic;->pageCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/facebook/ipc/model/PageTopic;->pageCount:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
