.class public LX/8t0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1412031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1412024
    const/high16 v0, 0x3f000000    # 0.5f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, p1, v0, v1}, LX/8t0;->a(Landroid/view/View;IFF)V

    .line 1412025
    return-void
.end method

.method private static a(Landroid/view/View;IFF)V
    .locals 1

    .prologue
    .line 1412026
    if-nez p1, :cond_1

    .line 1412027
    invoke-virtual {p0, p2}, Landroid/view/View;->setAlpha(F)V

    .line 1412028
    :cond_0
    :goto_0
    return-void

    .line 1412029
    :cond_1
    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 1412030
    :cond_2
    invoke-virtual {p0, p3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public static a(Landroid/view/ViewGroup;I)V
    .locals 2

    .prologue
    .line 1412006
    const/high16 v0, 0x3f000000    # 0.5f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, p1, v0, v1}, LX/8t0;->a(Landroid/view/ViewGroup;IFF)V

    .line 1412007
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;IFF)V
    .locals 3

    .prologue
    .line 1412016
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1412017
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1412018
    instance-of v2, v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    if-nez v2, :cond_0

    .line 1412019
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 1412020
    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0, p1, p2, p3}, LX/8t0;->a(Landroid/view/ViewGroup;IFF)V

    .line 1412021
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1412022
    :cond_0
    invoke-static {v0, p1, p2, p3}, LX/8t0;->a(Landroid/view/View;IFF)V

    goto :goto_1

    .line 1412023
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;I)V
    .locals 4

    .prologue
    .line 1412008
    const/16 v0, 0x7f

    const/16 v1, 0xff

    .line 1412009
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getController()LX/1aZ;

    move-result-object v2

    .line 1412010
    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/1aZ;->c()LX/1aY;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/1aZ;->c()LX/1aY;

    move-result-object v3

    invoke-interface {v3}, LX/1aY;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1412011
    :cond_0
    :goto_0
    return-void

    .line 1412012
    :cond_1
    if-nez p1, :cond_2

    .line 1412013
    invoke-interface {v2}, LX/1aZ;->c()LX/1aY;

    move-result-object v2

    invoke-interface {v2}, LX/1aY;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    .line 1412014
    :cond_2
    const/4 v3, 0x1

    if-eq p1, v3, :cond_3

    const/4 v3, 0x3

    if-ne p1, v3, :cond_0

    .line 1412015
    :cond_3
    invoke-interface {v2}, LX/1aZ;->c()LX/1aY;

    move-result-object v2

    invoke-interface {v2}, LX/1aY;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method
