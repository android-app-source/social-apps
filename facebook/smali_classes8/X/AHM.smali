.class public final LX/AHM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1655377
    const/4 v11, 0x0

    .line 1655378
    const/4 v10, 0x0

    .line 1655379
    const/4 v9, 0x0

    .line 1655380
    const/4 v8, 0x0

    .line 1655381
    const/4 v5, 0x0

    .line 1655382
    const-wide/16 v6, 0x0

    .line 1655383
    const/4 v4, 0x0

    .line 1655384
    const/4 v3, 0x0

    .line 1655385
    const/4 v2, 0x0

    .line 1655386
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 1655387
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1655388
    const/4 v2, 0x0

    .line 1655389
    :goto_0
    return v2

    .line 1655390
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 1655391
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1655392
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1655393
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v14, :cond_0

    if-eqz v2, :cond_0

    .line 1655394
    const-string v6, "backstage_message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1655395
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 1655396
    :cond_1
    const-string v6, "backstage_post_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1655397
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 1655398
    :cond_2
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1655399
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto :goto_1

    .line 1655400
    :cond_3
    const-string v6, "post_media"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1655401
    invoke-static/range {p0 .. p1}, LX/AHL;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto :goto_1

    .line 1655402
    :cond_4
    const-string v6, "seen_by_users"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1655403
    invoke-static/range {p0 .. p1}, LX/AHP;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1655404
    :cond_5
    const-string v6, "time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1655405
    const/4 v2, 0x1

    .line 1655406
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1655407
    :cond_6
    const-string v6, "timezone_offset_seconds"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1655408
    const/4 v2, 0x1

    .line 1655409
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v9, v6

    goto/16 :goto_1

    .line 1655410
    :cond_7
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1655411
    :cond_8
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1655412
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1655413
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1655414
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1655415
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1655416
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1655417
    if-eqz v3, :cond_9

    .line 1655418
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1655419
    :cond_9
    if-eqz v8, :cond_a

    .line 1655420
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 1655421
    :cond_a
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v10

    move v13, v11

    move v10, v8

    move v11, v9

    move v8, v2

    move v9, v4

    move-wide v15, v6

    move v7, v5

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1655422
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1655423
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1655424
    if-eqz v0, :cond_0

    .line 1655425
    const-string v1, "backstage_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655426
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1655427
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1655428
    if-eqz v0, :cond_1

    .line 1655429
    const-string v1, "backstage_post_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655430
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1655431
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1655432
    if-eqz v0, :cond_2

    .line 1655433
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655434
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1655435
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1655436
    if-eqz v0, :cond_3

    .line 1655437
    const-string v1, "post_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655438
    invoke-static {p0, v0, p2, p3}, LX/AHL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1655439
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1655440
    if-eqz v0, :cond_4

    .line 1655441
    const-string v1, "seen_by_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655442
    invoke-static {p0, v0, p2, p3}, LX/AHP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1655443
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1655444
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 1655445
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655446
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1655447
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1655448
    if-eqz v0, :cond_6

    .line 1655449
    const-string v1, "timezone_offset_seconds"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655450
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1655451
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1655452
    return-void
.end method
