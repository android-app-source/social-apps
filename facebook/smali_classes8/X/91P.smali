.class public LX/91P;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/91N;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/91Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1430648
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/91P;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/91Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430649
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1430650
    iput-object p1, p0, LX/91P;->b:LX/0Ot;

    .line 1430651
    return-void
.end method

.method public static a(LX/0QB;)LX/91P;
    .locals 4

    .prologue
    .line 1430652
    const-class v1, LX/91P;

    monitor-enter v1

    .line 1430653
    :try_start_0
    sget-object v0, LX/91P;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1430654
    sput-object v2, LX/91P;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1430655
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430656
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1430657
    new-instance v3, LX/91P;

    const/16 p0, 0x19a3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/91P;-><init>(LX/0Ot;)V

    .line 1430658
    move-object v0, v3

    .line 1430659
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1430660
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430661
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1430662
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1430663
    check-cast p2, LX/91O;

    .line 1430664
    iget-object v0, p0, LX/91P;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/91Q;

    iget-object v1, p2, LX/91O;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    const/4 p2, 0x0

    .line 1430665
    iget-object v2, v0, LX/91Q;->c:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, LX/2xv;->i(I)LX/2xv;

    move-result-object v2

    const v3, 0x7f02081c

    invoke-virtual {v2, v3}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 1430666
    new-instance v3, LX/8z6;

    invoke-direct {v3}, LX/8z6;-><init>()V

    .line 1430667
    iput-boolean p2, v3, LX/8z6;->g:Z

    .line 1430668
    move-object v3, v3

    .line 1430669
    iput-boolean p2, v3, LX/8z6;->f:Z

    .line 1430670
    move-object v3, v3

    .line 1430671
    iput-object v1, v3, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1430672
    move-object v3, v3

    .line 1430673
    invoke-virtual {v3}, LX/8z6;->a()LX/8z5;

    move-result-object v3

    .line 1430674
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    iget-object p0, v0, LX/91Q;->a:LX/91M;

    invoke-virtual {p0, p1}, LX/91M;->c(LX/1De;)LX/91L;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/91L;->a(Landroid/net/Uri;)LX/91L;

    move-result-object p0

    const p2, 0x7f0b1891

    invoke-virtual {p0, p2}, LX/91L;->i(I)LX/91L;

    move-result-object p0

    const p2, 0x7f0b1892

    invoke-virtual {p0, p2}, LX/91L;->k(I)LX/91L;

    move-result-object p0

    .line 1430675
    const p2, 0x4aa1495b    # 5285037.5f

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1430676
    iget-object v1, p0, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput-object p2, v1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->h:LX/1dQ;

    .line 1430677
    move-object p0, p0

    .line 1430678
    iget-object p2, v0, LX/91Q;->b:LX/8zJ;

    invoke-virtual {p2, v3}, LX/8zJ;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/91L;->a(Ljava/lang/CharSequence;)LX/91L;

    move-result-object v3

    .line 1430679
    const p0, 0x4aa14b0d    # 5285254.5f

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1430680
    iget-object p2, v3, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput-object p0, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->j:LX/1dQ;

    .line 1430681
    move-object v3, v3

    .line 1430682
    iget-object p0, v3, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput-object v2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->n:LX/1dc;

    .line 1430683
    move-object v2, v3

    .line 1430684
    const v3, 0x4aa1478a    # 5284805.0f

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1430685
    iget-object p0, v2, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput-object v3, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->i:LX/1dQ;

    .line 1430686
    move-object v2, v2

    .line 1430687
    const v3, 0x7f0823ce

    .line 1430688
    iget-object p0, v2, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    invoke-virtual {v2, v3}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->s:Ljava/lang/CharSequence;

    .line 1430689
    move-object v2, v2

    .line 1430690
    const/4 v3, 0x1

    .line 1430691
    iget-object p0, v2, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput-boolean v3, p0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->t:Z

    .line 1430692
    move-object v2, v2

    .line 1430693
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, LX/91L;->m(I)LX/91L;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1890

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0720

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1430694
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1430695
    invoke-static {}, LX/1dS;->b()V

    .line 1430696
    iget v0, p1, LX/1dQ;->b:I

    .line 1430697
    sparse-switch v0, :sswitch_data_0

    .line 1430698
    :goto_0
    return-object v2

    .line 1430699
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1430700
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1430701
    check-cast v1, LX/91O;

    .line 1430702
    iget-object p1, p0, LX/91P;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/91O;->b:LX/903;

    .line 1430703
    if-eqz p1, :cond_0

    .line 1430704
    invoke-interface {p1}, LX/903;->b()V

    .line 1430705
    :cond_0
    goto :goto_0

    .line 1430706
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1430707
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1430708
    check-cast v1, LX/91O;

    .line 1430709
    iget-object p1, p0, LX/91P;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/91O;->b:LX/903;

    .line 1430710
    if-eqz p1, :cond_1

    .line 1430711
    invoke-interface {p1}, LX/903;->a()V

    .line 1430712
    :cond_1
    goto :goto_0

    .line 1430713
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 1430714
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1430715
    check-cast v1, LX/91O;

    .line 1430716
    iget-object p1, p0, LX/91P;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/91O;->b:LX/903;

    .line 1430717
    if-eqz p1, :cond_2

    .line 1430718
    invoke-interface {p1}, LX/903;->c()V

    .line 1430719
    :cond_2
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4aa1478a -> :sswitch_1
        0x4aa1495b -> :sswitch_0
        0x4aa14b0d -> :sswitch_2
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/91N;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1430720
    new-instance v1, LX/91O;

    invoke-direct {v1, p0}, LX/91O;-><init>(LX/91P;)V

    .line 1430721
    sget-object v2, LX/91P;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/91N;

    .line 1430722
    if-nez v2, :cond_0

    .line 1430723
    new-instance v2, LX/91N;

    invoke-direct {v2}, LX/91N;-><init>()V

    .line 1430724
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/91N;->a$redex0(LX/91N;LX/1De;IILX/91O;)V

    .line 1430725
    move-object v1, v2

    .line 1430726
    move-object v0, v1

    .line 1430727
    return-object v0
.end method
