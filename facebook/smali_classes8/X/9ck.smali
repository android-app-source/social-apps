.class public LX/9ck;
.super Landroid/widget/GridView;
.source ""


# static fields
.field public static final a:LX/4m4;


# instance fields
.field private b:LX/9cm;

.field public c:LX/9cn;

.field public d:LX/8mq;

.field public e:LX/8mp;

.field private f:LX/7kc;

.field public g:Lcom/facebook/stickers/model/StickerPack;

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9cP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1516766
    sget-object v0, LX/4m4;->COMPOSER:LX/4m4;

    sput-object v0, LX/9ck;->a:LX/4m4;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1516727
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 1516728
    const/4 p1, 0x0

    .line 1516729
    const-class v0, LX/9ck;

    invoke-static {v0, p0}, LX/9ck;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1516730
    iget-object v0, p0, LX/9ck;->d:LX/8mq;

    sget-object v1, LX/9ck;->a:LX/4m4;

    invoke-virtual {v0, p0, v1}, LX/8mq;->a(Landroid/widget/GridView;LX/4m4;)LX/8mp;

    move-result-object v0

    iput-object v0, p0, LX/9ck;->e:LX/8mp;

    .line 1516731
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/9ck;->setStretchMode(I)V

    .line 1516732
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LX/9ck;->setNumColumns(I)V

    .line 1516733
    invoke-virtual {p0}, LX/9ck;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-int v0, v0

    invoke-virtual {p0}, LX/9ck;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1940

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/2addr v0, v1

    .line 1516734
    invoke-virtual {p0, v0}, LX/9ck;->setColumnWidth(I)V

    .line 1516735
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/9ck;->setClickable(Z)V

    .line 1516736
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, LX/9ck;->setGravity(I)V

    .line 1516737
    invoke-virtual {p0, p1, p1, p1, p1}, LX/9ck;->setPadding(IIII)V

    .line 1516738
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/9ck;

    const-class v1, LX/8mq;

    invoke-interface {v3, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8mq;

    const-class v2, LX/9cn;

    invoke-interface {v3, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9cn;

    const/16 p0, 0x2e29

    invoke-static {v3, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    iput-object v1, p1, LX/9ck;->d:LX/8mq;

    iput-object v2, p1, LX/9ck;->c:LX/9cn;

    iput-object v3, p1, LX/9ck;->h:LX/0Ot;

    return-void
.end method

.method private b()V
    .locals 12

    .prologue
    .line 1516739
    new-instance v0, LX/7kd;

    invoke-virtual {p0}, LX/9ck;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, LX/4m4;->COMPOSER:LX/4m4;

    invoke-static {v2}, LX/8kW;->a(LX/4m4;)LX/7kb;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7kd;-><init>(Landroid/content/res/Resources;LX/7kb;)V

    invoke-virtual {p0}, LX/9ck;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/9ck;->getHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/7kd;->a(IIZ)LX/7kc;

    move-result-object v0

    iput-object v0, p0, LX/9ck;->f:LX/7kc;

    .line 1516740
    iget-object v0, p0, LX/9ck;->c:LX/9cn;

    invoke-virtual {p0}, LX/9ck;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, ""

    iget-object v3, p0, LX/9ck;->f:LX/7kc;

    .line 1516741
    new-instance v4, LX/9cm;

    invoke-static {v0}, LX/8jY;->b(LX/0QB;)LX/8jY;

    move-result-object v5

    check-cast v5, LX/8jY;

    invoke-static {v0}, LX/8mv;->b(LX/0QB;)LX/8mv;

    move-result-object v6

    check-cast v6, LX/8mv;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    const/16 v8, 0x1572

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    move-object v9, v1

    move-object v10, v2

    move-object v11, v3

    invoke-direct/range {v4 .. v11}, LX/9cm;-><init>(LX/8jY;LX/8mv;LX/1Ad;LX/0Or;Landroid/content/Context;Ljava/lang/String;LX/7kc;)V

    .line 1516742
    move-object v0, v4

    .line 1516743
    iput-object v0, p0, LX/9ck;->b:LX/9cm;

    .line 1516744
    iget-object v0, p0, LX/9ck;->b:LX/9cm;

    .line 1516745
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1516746
    invoke-virtual {v0, v1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a(LX/0Px;)V

    .line 1516747
    iget-object v0, p0, LX/9ck;->f:LX/7kc;

    .line 1516748
    iget v1, v0, LX/7kc;->a:I

    move v0, v1

    .line 1516749
    invoke-virtual {p0, v0}, LX/9ck;->setNumColumns(I)V

    .line 1516750
    iget-object v0, p0, LX/9ck;->e:LX/8mp;

    new-instance v1, LX/9cj;

    invoke-direct {v1, p0}, LX/9cj;-><init>(LX/9ck;)V

    .line 1516751
    iput-object v1, v0, LX/8mp;->e:LX/8l0;

    .line 1516752
    iget-object v0, p0, LX/9ck;->b:LX/9cm;

    iget-object v1, p0, LX/9ck;->g:Lcom/facebook/stickers/model/StickerPack;

    .line 1516753
    iput-object v1, v0, LX/9cm;->c:Lcom/facebook/stickers/model/StickerPack;

    .line 1516754
    iget-object v2, v0, LX/9cm;->b:LX/8jY;

    invoke-virtual {v2}, LX/8jY;->a()V

    .line 1516755
    iget-object v2, v0, LX/9cm;->b:LX/8jY;

    new-instance v3, LX/9cl;

    invoke-direct {v3, v0}, LX/9cl;-><init>(LX/9cm;)V

    .line 1516756
    iput-object v3, v2, LX/8jY;->e:LX/3Mb;

    .line 1516757
    iget-object v2, v0, LX/9cm;->b:LX/8jY;

    new-instance v3, LX/8jW;

    iget-object v1, v0, LX/9cm;->c:Lcom/facebook/stickers/model/StickerPack;

    .line 1516758
    iget-object v0, v1, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v1, v0

    .line 1516759
    invoke-direct {v3, v1}, LX/8jW;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v3}, LX/8jY;->a(LX/8jW;)V

    .line 1516760
    iget-object v0, p0, LX/9ck;->b:LX/9cm;

    invoke-virtual {p0, v0}, LX/9ck;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1516761
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1516762
    invoke-super/range {p0 .. p5}, Landroid/widget/GridView;->onLayout(ZIIII)V

    .line 1516763
    iget-object v0, p0, LX/9ck;->f:LX/7kc;

    if-nez v0, :cond_0

    .line 1516764
    invoke-direct {p0}, LX/9ck;->b()V

    .line 1516765
    :cond_0
    return-void
.end method
