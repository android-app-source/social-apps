.class public final enum LX/8tm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8tm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8tm;

.field public static final enum ANIMATING:LX/8tm;

.field public static final enum AT_REST:LX/8tm;

.field public static final enum BEING_DRAGGED:LX/8tm;

.field public static final enum DISMISSING:LX/8tm;

.field public static final enum NEEDS_REVEAL:LX/8tm;

.field public static final enum REVEALING:LX/8tm;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1413821
    new-instance v0, LX/8tm;

    const-string v1, "AT_REST"

    invoke-direct {v0, v1, v3}, LX/8tm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tm;->AT_REST:LX/8tm;

    .line 1413822
    new-instance v0, LX/8tm;

    const-string v1, "ANIMATING"

    invoke-direct {v0, v1, v4}, LX/8tm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tm;->ANIMATING:LX/8tm;

    .line 1413823
    new-instance v0, LX/8tm;

    const-string v1, "BEING_DRAGGED"

    invoke-direct {v0, v1, v5}, LX/8tm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tm;->BEING_DRAGGED:LX/8tm;

    .line 1413824
    new-instance v0, LX/8tm;

    const-string v1, "DISMISSING"

    invoke-direct {v0, v1, v6}, LX/8tm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tm;->DISMISSING:LX/8tm;

    .line 1413825
    new-instance v0, LX/8tm;

    const-string v1, "NEEDS_REVEAL"

    invoke-direct {v0, v1, v7}, LX/8tm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tm;->NEEDS_REVEAL:LX/8tm;

    .line 1413826
    new-instance v0, LX/8tm;

    const-string v1, "REVEALING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8tm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tm;->REVEALING:LX/8tm;

    .line 1413827
    const/4 v0, 0x6

    new-array v0, v0, [LX/8tm;

    sget-object v1, LX/8tm;->AT_REST:LX/8tm;

    aput-object v1, v0, v3

    sget-object v1, LX/8tm;->ANIMATING:LX/8tm;

    aput-object v1, v0, v4

    sget-object v1, LX/8tm;->BEING_DRAGGED:LX/8tm;

    aput-object v1, v0, v5

    sget-object v1, LX/8tm;->DISMISSING:LX/8tm;

    aput-object v1, v0, v6

    sget-object v1, LX/8tm;->NEEDS_REVEAL:LX/8tm;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8tm;->REVEALING:LX/8tm;

    aput-object v2, v0, v1

    sput-object v0, LX/8tm;->$VALUES:[LX/8tm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1413830
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8tm;
    .locals 1

    .prologue
    .line 1413829
    const-class v0, LX/8tm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8tm;

    return-object v0
.end method

.method public static values()[LX/8tm;
    .locals 1

    .prologue
    .line 1413828
    sget-object v0, LX/8tm;->$VALUES:[LX/8tm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8tm;

    return-object v0
.end method
