.class public final LX/ALP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5fb;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/CameraView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CameraView;)V
    .locals 0

    .prologue
    .line 1664817
    iput-object p1, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1664804
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    const/4 v1, 0x1

    .line 1664805
    iput-boolean v1, v0, Lcom/facebook/backstage/camera/CameraView;->y:Z

    .line 1664806
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1664807
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->d()V

    .line 1664808
    :cond_0
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1664809
    invoke-virtual {v0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1664810
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08283d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 1664811
    sget-object v0, Lcom/facebook/backstage/camera/CameraView;->f:Ljava/lang/String;

    const-string v1, "Flash not ready."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1664812
    :cond_1
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/ToggleTextButton;->setVisibility(I)V

    .line 1664813
    :goto_0
    return-void

    .line 1664814
    :cond_2
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v0}, Lcom/facebook/backstage/camera/CameraView;->h(Lcom/facebook/backstage/camera/CameraView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1664815
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/ToggleTextButton;->setVisibility(I)V

    .line 1664816
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v1}, Lcom/facebook/backstage/camera/CameraView;->g(Lcom/facebook/backstage/camera/CameraView;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/backstage/camera/CameraView;->setFlash(Lcom/facebook/backstage/camera/CameraView;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1664799
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    const/4 v1, 0x1

    .line 1664800
    iput-boolean v1, v0, Lcom/facebook/backstage/camera/CameraView;->y:Z

    .line 1664801
    iget-object v0, p0, LX/ALP;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08283e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 1664802
    sget-object v0, Lcom/facebook/backstage/camera/CameraView;->f:Ljava/lang/String;

    const-string v1, "Failed to initialize the camera"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1664803
    return-void
.end method
