.class public LX/9IN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/support/v4/app/FragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1nB;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/9IQ;

.field public final f:LX/0yI;

.field public final g:LX/0ad;


# direct methods
.method public constructor <init>(LX/9IQ;LX/0yI;LX/0ad;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1462914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462915
    iput-object p1, p0, LX/9IN;->e:LX/9IQ;

    .line 1462916
    iput-object p2, p0, LX/9IN;->f:LX/0yI;

    .line 1462917
    iput-object p3, p0, LX/9IN;->g:LX/0ad;

    .line 1462918
    return-void
.end method

.method public static a(LX/0QB;)LX/9IN;
    .locals 7

    .prologue
    .line 1462919
    const-class v1, LX/9IN;

    monitor-enter v1

    .line 1462920
    :try_start_0
    sget-object v0, LX/9IN;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462921
    sput-object v2, LX/9IN;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462922
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462923
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462924
    new-instance v6, LX/9IN;

    invoke-static {v0}, LX/9IQ;->a(LX/0QB;)LX/9IQ;

    move-result-object v3

    check-cast v3, LX/9IQ;

    invoke-static {v0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v4

    check-cast v4, LX/0yI;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {v6, v3, v4, v5}, LX/9IN;-><init>(LX/9IQ;LX/0yI;LX/0ad;)V

    .line 1462925
    const/16 v3, 0xbd8

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x455

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x36

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0xbc6

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1462926
    iput-object v3, v6, LX/9IN;->a:LX/0Or;

    iput-object v4, v6, LX/9IN;->b:LX/0Or;

    iput-object v5, v6, LX/9IN;->c:LX/0Or;

    iput-object p0, v6, LX/9IN;->d:LX/0Or;

    .line 1462927
    move-object v0, v6

    .line 1462928
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462929
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462930
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462931
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/0yI;LX/0ad;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1462932
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v1

    sget-object v2, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1462933
    :goto_0
    return v0

    :cond_0
    sget-short v1, LX/0wn;->aQ:S

    invoke-interface {p1, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method
