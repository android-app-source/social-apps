.class public final enum LX/ALY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ALY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ALY;

.field public static final enum NONE:LX/ALY;

.field public static final enum PRESSING:LX/ALY;

.field public static final enum UNPRESSING:LX/ALY;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1665012
    new-instance v0, LX/ALY;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/ALY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ALY;->NONE:LX/ALY;

    .line 1665013
    new-instance v0, LX/ALY;

    const-string v1, "PRESSING"

    invoke-direct {v0, v1, v3}, LX/ALY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ALY;->PRESSING:LX/ALY;

    .line 1665014
    new-instance v0, LX/ALY;

    const-string v1, "UNPRESSING"

    invoke-direct {v0, v1, v4}, LX/ALY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ALY;->UNPRESSING:LX/ALY;

    .line 1665015
    const/4 v0, 0x3

    new-array v0, v0, [LX/ALY;

    sget-object v1, LX/ALY;->NONE:LX/ALY;

    aput-object v1, v0, v2

    sget-object v1, LX/ALY;->PRESSING:LX/ALY;

    aput-object v1, v0, v3

    sget-object v1, LX/ALY;->UNPRESSING:LX/ALY;

    aput-object v1, v0, v4

    sput-object v0, LX/ALY;->$VALUES:[LX/ALY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1665016
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ALY;
    .locals 1

    .prologue
    .line 1665017
    const-class v0, LX/ALY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ALY;

    return-object v0
.end method

.method public static values()[LX/ALY;
    .locals 1

    .prologue
    .line 1665018
    sget-object v0, LX/ALY;->$VALUES:[LX/ALY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ALY;

    return-object v0
.end method
