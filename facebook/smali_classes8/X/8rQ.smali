.class public LX/8rQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/8rI;

.field public final b:Landroid/content/Context;

.field public final c:LX/8rL;

.field public final d:LX/8rO;

.field public final e:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8rI;Landroid/content/Context;LX/8rL;LX/8rO;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1408845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1408846
    iput-object p1, p0, LX/8rQ;->a:LX/8rI;

    .line 1408847
    iput-object p2, p0, LX/8rQ;->b:Landroid/content/Context;

    .line 1408848
    iput-object p3, p0, LX/8rQ;->c:LX/8rL;

    .line 1408849
    iput-object p4, p0, LX/8rQ;->d:LX/8rO;

    .line 1408850
    iget-object v0, p0, LX/8rQ;->b:Landroid/content/Context;

    const v1, 0x7f0a009a

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/8rQ;->e:I

    .line 1408851
    return-void
.end method

.method public static a(LX/8rQ;Landroid/text/SpannableStringBuilder;I)V
    .locals 4
    .param p1    # Landroid/text/SpannableStringBuilder;
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1408852
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1408853
    :goto_0
    return-void

    .line 1408854
    :cond_0
    invoke-static {p1}, LX/33Q;->a(Landroid/text/Editable;)Landroid/text/Editable;

    .line 1408855
    iget-object v0, p0, LX/8rQ;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1408856
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1408857
    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v1, v0}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method
