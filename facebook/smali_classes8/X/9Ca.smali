.class public final LX/9Ca;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic b:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1454175
    iput-object p1, p0, LX/9Ca;->b:LX/9Cd;

    iput-object p2, p0, LX/9Ca;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1454176
    iget-object v0, p0, LX/9Ca;->b:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->j:LX/8DU;

    iget-object v1, p0, LX/9Ca;->a:Lcom/facebook/graphql/model/GraphQLComment;

    const/4 v4, 0x0

    .line 1454177
    iget-object v2, v0, LX/8DU;->b:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 1454178
    iget-object v2, v0, LX/8DU;->a:LX/2L1;

    invoke-virtual {v2}, LX/2L1;->a()LX/0Px;

    move-result-object v6

    .line 1454179
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_2

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3G3;

    .line 1454180
    instance-of v3, v2, LX/3G4;

    if-eqz v3, :cond_1

    .line 1454181
    check-cast v2, LX/3G4;

    .line 1454182
    iget-object v3, v2, LX/3G4;->h:Ljava/lang/Class;

    const-class p0, LX/58m;

    if-ne v3, p0, :cond_1

    .line 1454183
    iget-object v3, v2, LX/3G4;->j:LX/0jT;

    check-cast v3, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    .line 1454184
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1454185
    iget-object v3, v0, LX/8DU;->a:LX/2L1;

    iget-object v2, v2, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, LX/2L1;->a(Ljava/lang/String;)V

    .line 1454186
    const/4 v2, 0x1

    .line 1454187
    :goto_1
    move v0, v2

    .line 1454188
    if-nez v0, :cond_0

    .line 1454189
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The comment could not be found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1454190
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 1454191
    :cond_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_0

    :cond_2
    move v2, v4

    .line 1454192
    goto :goto_1
.end method
