.class public LX/8im;
.super Landroid/widget/TextView;
.source ""


# static fields
.field public static final a:LX/8YL;


# instance fields
.field public b:LX/8YL;

.field public c:LX/8YL;

.field public d:LX/8YK;

.field public e:LX/8ip;

.field public f:LX/8ij;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1391953
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v0

    sput-object v0, LX/8im;->a:LX/8YL;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    .line 1391937
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1391938
    sget-object v0, LX/8ij;->NONE:LX/8ij;

    iput-object v0, p0, LX/8im;->f:LX/8ij;

    .line 1391939
    new-instance v1, LX/0wU;

    const-wide/high16 v3, 0x4034000000000000L    # 20.0

    const-wide/16 v5, 0x0

    invoke-direct {v1, v3, v4, v5, v6}, LX/0wU;-><init>(DD)V

    .line 1391940
    iget-wide v8, v1, LX/0wU;->b:D

    move-wide v3, v8

    .line 1391941
    iget-wide v8, v1, LX/0wU;->c:D

    move-wide v1, v8

    .line 1391942
    invoke-static {v3, v4, v1, v2}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v1

    iput-object v1, p0, LX/8im;->b:LX/8YL;

    .line 1391943
    new-instance v1, LX/0wU;

    const-wide/high16 v3, 0x4014000000000000L    # 5.0

    const-wide/high16 v5, 0x4024000000000000L    # 10.0

    invoke-direct {v1, v3, v4, v5, v6}, LX/0wU;-><init>(DD)V

    .line 1391944
    iget-wide v8, v1, LX/0wU;->b:D

    move-wide v3, v8

    .line 1391945
    iget-wide v8, v1, LX/0wU;->c:D

    move-wide v1, v8

    .line 1391946
    invoke-static {v3, v4, v1, v2}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v1

    iput-object v1, p0, LX/8im;->c:LX/8YL;

    .line 1391947
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v1

    invoke-virtual {v1}, LX/8YH;->a()LX/8YK;

    move-result-object v1

    new-instance v2, LX/8il;

    invoke-direct {v2, p0}, LX/8il;-><init>(LX/8im;)V

    invoke-virtual {v1, v2}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    move-result-object v1

    iget-object v2, p0, LX/8im;->b:LX/8YL;

    invoke-virtual {v1, v2}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v1

    sget-object v2, LX/8im;->a:LX/8YL;

    invoke-virtual {v1, v2}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v1

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v3, v4}, LX/8YK;->a(D)LX/8YK;

    move-result-object v1

    iput-object v1, p0, LX/8im;->d:LX/8YK;

    .line 1391948
    new-instance v1, LX/8ip;

    invoke-virtual {p0}, LX/8im;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p0}, LX/8ip;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v1, p0, LX/8im;->e:LX/8ip;

    .line 1391949
    new-instance v1, LX/8ik;

    invoke-direct {v1, p0}, LX/8ik;-><init>(LX/8im;)V

    .line 1391950
    iget-object v2, p0, LX/8im;->e:LX/8ip;

    .line 1391951
    iput-object v1, v2, LX/8ip;->f:LX/8ik;

    .line 1391952
    return-void
.end method

.method public static synthetic a(LX/8im;Z)V
    .locals 0

    .prologue
    .line 1391936
    invoke-super {p0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 1391923
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1391924
    if-eqz v0, :cond_1

    .line 1391925
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {p0, v0}, LX/8im;->setAlpha(F)V

    .line 1391926
    invoke-super {p0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1391927
    :goto_2
    return-void

    .line 1391928
    :cond_0
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_1

    .line 1391929
    :cond_1
    new-instance v0, Lcom/facebook/sounds/configurator/SpringTextView$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/sounds/configurator/SpringTextView$1;-><init>(LX/8im;Z)V

    invoke-virtual {p0, v0}, LX/8im;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLongPressEnabled(Z)V
    .locals 1

    .prologue
    .line 1391933
    iget-object v0, p0, LX/8im;->e:LX/8ip;

    .line 1391934
    iget-object p0, v0, LX/8ip;->c:Landroid/view/GestureDetector;

    invoke-virtual {p0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1391935
    return-void
.end method

.method public setOnLongPressListener(LX/8iq;)V
    .locals 1

    .prologue
    .line 1391930
    iget-object v0, p0, LX/8im;->e:LX/8ip;

    .line 1391931
    iput-object p1, v0, LX/8ip;->g:LX/8iq;

    .line 1391932
    return-void
.end method
