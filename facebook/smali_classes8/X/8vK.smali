.class public final LX/8vK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1416867
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1416868
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1416869
    :goto_0
    return v1

    .line 1416870
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1416871
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1416872
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1416873
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1416874
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1416875
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1416876
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1416877
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1416878
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1416879
    :cond_4
    const-string v5, "translated_body_for_viewer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1416880
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1416881
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1416882
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1416883
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1416884
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1416885
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1416886
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1416887
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1416888
    :pswitch_0
    const-string v0, "broadcast_photo_publish_and_return"

    .line 1416889
    :goto_0
    return-object v0

    .line 1416890
    :pswitch_1
    const-string v0, "call_edit_post"

    goto :goto_0

    .line 1416891
    :pswitch_2
    const-string v0, "call_publish_photo"

    goto :goto_0

    .line 1416892
    :pswitch_3
    const-string v0, "call_publish_post"

    goto :goto_0

    .line 1416893
    :pswitch_4
    const-string v0, "call_publish_share"

    goto :goto_0

    .line 1416894
    :pswitch_5
    const-string v0, "delete_story_from_db_complete"

    goto :goto_0

    .line 1416895
    :pswitch_6
    const-string v0, "delete_story_from_db_failure"

    goto :goto_0

    .line 1416896
    :pswitch_7
    const-string v0, "delete_story_from_db_start"

    goto :goto_0

    .line 1416897
    :pswitch_8
    const-string v0, "delete_story_postponed_for_video"

    goto :goto_0

    .line 1416898
    :pswitch_9
    const-string v0, "mark_posting_complete"

    goto :goto_0

    .line 1416899
    :pswitch_a
    const-string v0, "mark_posting_failure_no_story"

    goto :goto_0

    .line 1416900
    :pswitch_b
    const-string v0, "mark_posting_failure_not_in_cache"

    goto :goto_0

    .line 1416901
    :pswitch_c
    const-string v0, "mark_publish_complete"

    goto :goto_0

    .line 1416902
    :pswitch_d
    const-string v0, "mark_publish_failure_no_story"

    goto :goto_0

    .line 1416903
    :pswitch_e
    const-string v0, "mark_publish_failure_not_in_cache"

    goto :goto_0

    .line 1416904
    :pswitch_f
    const-string v0, "publish_offline_post"

    goto :goto_0

    .line 1416905
    :pswitch_10
    const-string v0, "save_pending_story_with_optimistic_posting_unsupported"

    goto :goto_0

    .line 1416906
    :pswitch_11
    const-string v0, "save_story_to_db_complete"

    goto :goto_0

    .line 1416907
    :pswitch_12
    const-string v0, "save_story_to_db_failure"

    goto :goto_0

    .line 1416908
    :pswitch_13
    const-string v0, "save_story_to_db_start"

    goto :goto_0

    .line 1416909
    :pswitch_14
    const-string v0, "start_request_pending_story_missing"

    goto :goto_0

    .line 1416910
    :pswitch_15
    const-string v0, "story_expired"

    goto :goto_0

    .line 1416911
    :pswitch_16
    const-string v0, "update_existing_story_in_db_start"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1416912
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1416913
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1416914
    if-eqz v0, :cond_0

    .line 1416915
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416916
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1416917
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1416918
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1416919
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1416920
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1416921
    if-eqz v0, :cond_1

    .line 1416922
    const-string v1, "commenters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416923
    const/4 v1, 0x0

    .line 1416924
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1416925
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1416926
    if-eqz v1, :cond_0

    .line 1416927
    const-string p3, "count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416928
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1416929
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1416930
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1416931
    if-eqz v0, :cond_2

    .line 1416932
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416933
    invoke-static {p0, v0, p2}, LX/7fV;->a(LX/15i;ILX/0nX;)V

    .line 1416934
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1416935
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1416936
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1416937
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1416938
    :goto_0
    return v1

    .line 1416939
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1416940
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1416941
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1416942
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1416943
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1416944
    const-string v3, "uri"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1416945
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1416946
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1416947
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1416948
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1416949
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1416950
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1416951
    if-eqz v0, :cond_4

    .line 1416952
    const-string v1, "collection_product_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416953
    const/4 v1, 0x0

    .line 1416954
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1416955
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1416956
    if-eqz v1, :cond_0

    .line 1416957
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416958
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1416959
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1416960
    if-eqz v1, :cond_3

    .line 1416961
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416962
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1416963
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1416964
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1416965
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1416966
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1416967
    if-eqz v4, :cond_1

    .line 1416968
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416969
    invoke-static {p0, v4, p2, p3}, LX/7in;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1416970
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1416971
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1416972
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1416973
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1416974
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1416975
    if-eqz v0, :cond_5

    .line 1416976
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416977
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1416978
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1416979
    if-eqz v0, :cond_6

    .line 1416980
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1416981
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1416982
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1416983
    return-void
.end method
