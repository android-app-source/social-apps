.class public final LX/9g7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1523042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1523043
    :try_start_0
    new-instance v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    invoke-direct {v0, p1}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;-><init>(Landroid/os/Parcel;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1523044
    :catch_0
    move-exception v0

    .line 1523045
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1523046
    new-array v0, p1, [Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    return-object v0
.end method
