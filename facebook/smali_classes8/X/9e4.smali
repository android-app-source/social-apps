.class public final enum LX/9e4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9e4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9e4;

.field public static final enum NORMAL:LX/9e4;

.field public static final enum ROTATED_CCW:LX/9e4;

.field public static final enum ROTATED_CW:LX/9e4;

.field public static final enum UPSIDE_DOWN:LX/9e4;


# instance fields
.field public final rotation:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1519147
    new-instance v0, LX/9e4;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3, v3}, LX/9e4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9e4;->NORMAL:LX/9e4;

    .line 1519148
    new-instance v0, LX/9e4;

    const-string v1, "ROTATED_CW"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v4, v2}, LX/9e4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9e4;->ROTATED_CW:LX/9e4;

    .line 1519149
    new-instance v0, LX/9e4;

    const-string v1, "UPSIDE_DOWN"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v5, v2}, LX/9e4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9e4;->UPSIDE_DOWN:LX/9e4;

    .line 1519150
    new-instance v0, LX/9e4;

    const-string v1, "ROTATED_CCW"

    const/16 v2, 0x10e

    invoke-direct {v0, v1, v6, v2}, LX/9e4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9e4;->ROTATED_CCW:LX/9e4;

    .line 1519151
    const/4 v0, 0x4

    new-array v0, v0, [LX/9e4;

    sget-object v1, LX/9e4;->NORMAL:LX/9e4;

    aput-object v1, v0, v3

    sget-object v1, LX/9e4;->ROTATED_CW:LX/9e4;

    aput-object v1, v0, v4

    sget-object v1, LX/9e4;->UPSIDE_DOWN:LX/9e4;

    aput-object v1, v0, v5

    sget-object v1, LX/9e4;->ROTATED_CCW:LX/9e4;

    aput-object v1, v0, v6

    sput-object v0, LX/9e4;->$VALUES:[LX/9e4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1519144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1519145
    iput p3, p0, LX/9e4;->rotation:I

    .line 1519146
    return-void
.end method

.method public static valueOf(I)LX/9e4;
    .locals 1

    .prologue
    .line 1519152
    rem-int/lit16 v0, p0, 0x168

    sparse-switch v0, :sswitch_data_0

    .line 1519153
    sget-object v0, LX/9e4;->NORMAL:LX/9e4;

    :goto_0
    return-object v0

    .line 1519154
    :sswitch_0
    sget-object v0, LX/9e4;->ROTATED_CW:LX/9e4;

    goto :goto_0

    .line 1519155
    :sswitch_1
    sget-object v0, LX/9e4;->UPSIDE_DOWN:LX/9e4;

    goto :goto_0

    .line 1519156
    :sswitch_2
    sget-object v0, LX/9e4;->ROTATED_CCW:LX/9e4;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/9e4;
    .locals 1

    .prologue
    .line 1519143
    const-class v0, LX/9e4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9e4;

    return-object v0
.end method

.method public static values()[LX/9e4;
    .locals 1

    .prologue
    .line 1519142
    sget-object v0, LX/9e4;->$VALUES:[LX/9e4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9e4;

    return-object v0
.end method


# virtual methods
.method public final isRatioChanged()Z
    .locals 1

    .prologue
    .line 1519141
    sget-object v0, LX/9e4;->ROTATED_CCW:LX/9e4;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/9e4;->ROTATED_CW:LX/9e4;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
