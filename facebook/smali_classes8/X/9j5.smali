.class public LX/9j5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static u:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0So;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/9jG;

.field public g:LX/9jN;

.field public h:Landroid/location/Location;

.field public i:Z

.field public j:I

.field public k:Ljava/lang/String;

.field public l:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:J

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0So;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1529063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529064
    const-string v0, ""

    iput-object v0, p0, LX/9j5;->c:Ljava/lang/String;

    .line 1529065
    const-string v0, ""

    iput-object v0, p0, LX/9j5;->d:Ljava/lang/String;

    .line 1529066
    const-string v0, ""

    iput-object v0, p0, LX/9j5;->e:Ljava/lang/String;

    .line 1529067
    sget-object v0, LX/9jG;->CHECKIN:LX/9jG;

    iput-object v0, p0, LX/9j5;->f:LX/9jG;

    .line 1529068
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/9j5;->l:Ljava/util/HashSet;

    .line 1529069
    iput-boolean v1, p0, LX/9j5;->m:Z

    .line 1529070
    iput-boolean v1, p0, LX/9j5;->n:Z

    .line 1529071
    iput-boolean v1, p0, LX/9j5;->o:Z

    .line 1529072
    iput-boolean v1, p0, LX/9j5;->p:Z

    .line 1529073
    iput-boolean v1, p0, LX/9j5;->q:Z

    .line 1529074
    iput-boolean v1, p0, LX/9j5;->r:Z

    .line 1529075
    const-class v0, LX/0Zb;

    invoke-static {v0, p1}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v0, p0, LX/9j5;->a:LX/0Zb;

    .line 1529076
    const-class v0, LX/0So;

    invoke-static {v0, p2}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    iput-object v0, p0, LX/9j5;->b:LX/0So;

    .line 1529077
    return-void
.end method

.method public static a(LX/0QB;)LX/9j5;
    .locals 5

    .prologue
    .line 1529147
    const-class v1, LX/9j5;

    monitor-enter v1

    .line 1529148
    :try_start_0
    sget-object v0, LX/9j5;->u:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1529149
    sput-object v2, LX/9j5;->u:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1529150
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1529151
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1529152
    new-instance p0, LX/9j5;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/9j5;-><init>(LX/0Zb;LX/0So;)V

    .line 1529153
    move-object v0, p0

    .line 1529154
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1529155
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9j5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1529156
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1529157
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 12

    .prologue
    .line 1529118
    iget-object v0, p0, LX/9j5;->g:LX/9jN;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9j5;->g:LX/9jN;

    .line 1529119
    iget-object v1, v0, LX/9jN;->b:Ljava/util/List;

    move-object v0, v1

    .line 1529120
    if-eqz v0, :cond_1

    .line 1529121
    const-string v0, "results_list_id"

    iget-object v1, p0, LX/9j5;->g:LX/9jN;

    .line 1529122
    iget-object v2, v1, LX/9jN;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1529123
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529124
    const-string v0, "results_from_cache"

    iget-object v1, p0, LX/9j5;->g:LX/9jN;

    .line 1529125
    iget-boolean v2, v1, LX/9jN;->e:Z

    move v1, v2

    .line 1529126
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529127
    const-string v0, "results_fetched"

    iget-object v1, p0, LX/9j5;->g:LX/9jN;

    .line 1529128
    iget-object v2, v1, LX/9jN;->b:Ljava/util/List;

    move-object v1, v2

    .line 1529129
    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-virtual {v6}, LX/0mC;->b()LX/162;

    move-result-object v7

    .line 1529130
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1529131
    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v7, v10, v11}, LX/162;->b(J)LX/162;

    goto :goto_0

    .line 1529132
    :cond_0
    move-object v1, v7

    .line 1529133
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529134
    const-string v1, "is_historical_list"

    iget-object v0, p0, LX/9j5;->g:LX/9jN;

    .line 1529135
    iget-object v2, v0, LX/9jN;->g:LX/9jM;

    move-object v0, v2

    .line 1529136
    sget-object v2, LX/9jM;->RECENT:LX/9jM;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529137
    const-string v0, "list_type"

    iget-object v1, p0, LX/9j5;->g:LX/9jN;

    .line 1529138
    iget-object v2, v1, LX/9jN;->g:LX/9jM;

    move-object v1, v2

    .line 1529139
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529140
    :cond_1
    iget-object v0, p0, LX/9j5;->h:Landroid/location/Location;

    if-eqz v0, :cond_2

    .line 1529141
    const-string v0, "user_longitude"

    iget-object v1, p0, LX/9j5;->h:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529142
    const-string v0, "user_latitude"

    iget-object v1, p0, LX/9j5;->h:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529143
    const-string v0, "location_accuracy"

    iget-object v1, p0, LX/9j5;->h:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529144
    const-string v0, "location_age_ms"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, LX/9j5;->h:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1529145
    :cond_2
    return-object p1

    .line 1529146
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 5

    .prologue
    .line 1529100
    iget-wide v0, p0, LX/9j5;->s:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1529101
    iget-object v0, p0, LX/9j5;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/9j5;->s:J

    .line 1529102
    :cond_0
    iget-object v0, p0, LX/9j5;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/9j5;->s:J

    sub-long/2addr v0, v2

    .line 1529103
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/9j5;->d:Ljava/lang/String;

    .line 1529104
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1529105
    move-object v2, v2

    .line 1529106
    const-string v3, "place_picker"

    .line 1529107
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1529108
    move-object v2, v2

    .line 1529109
    const-string v3, "query"

    iget-object v4, p0, LX/9j5;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "search_type"

    iget-object v4, p0, LX/9j5;->f:LX/9jG;

    invoke-virtual {v4}, LX/9jG;->toLegacyString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "place_picker_session_id"

    iget-object v4, p0, LX/9j5;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "milliseconds_since_start"

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "entry_from_checkin"

    iget-boolean v2, p0, LX/9j5;->i:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "has_tti_error"

    iget-boolean v2, p0, LX/9j5;->r:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "user_enabled_location_providers"

    iget-object v2, p0, LX/9j5;->t:Ljava/util/List;

    .line 1529110
    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-virtual {v3}, LX/0mC;->b()LX/162;

    move-result-object v4

    .line 1529111
    if-nez v2, :cond_1

    move-object v3, v4

    .line 1529112
    :goto_0
    move-object v2, v3

    .line 1529113
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "suggestion_mechanism"

    iget-object v2, p0, LX/9j5;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1529114
    return-object v0

    .line 1529115
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1529116
    invoke-virtual {v4, v3}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    :cond_2
    move-object v3, v4

    .line 1529117
    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 1529158
    iget-object v0, p0, LX/9j5;->g:LX/9jN;

    .line 1529159
    iget-object v1, v0, LX/9jN;->b:Ljava/util/List;

    move-object v0, v1

    .line 1529160
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    .line 1529161
    iget-object v0, p0, LX/9j5;->g:LX/9jN;

    .line 1529162
    iget-object v1, v0, LX/9jN;->b:Ljava/util/List;

    move-object v0, v1

    .line 1529163
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 p2, v0, -0x1

    .line 1529164
    :cond_0
    :goto_0
    if-gt p1, p2, :cond_1

    .line 1529165
    iget-object v0, p0, LX/9j5;->l:Ljava/util/HashSet;

    iget-object v1, p0, LX/9j5;->g:LX/9jN;

    .line 1529166
    iget-object v2, v1, LX/9jN;->b:Ljava/util/List;

    move-object v1, v2

    .line 1529167
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1529168
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1529169
    :cond_1
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1529087
    const-string v0, "has_saved_instance_state"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1529088
    :goto_0
    return-void

    .line 1529089
    :cond_0
    const-string v0, "has_results_loaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/9j5;->p:Z

    .line 1529090
    const-string v0, "has_past_places_in_main_list_loaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/9j5;->q:Z

    .line 1529091
    const-string v0, "has_location_been_received"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/9j5;->o:Z

    .line 1529092
    const-string v0, "has_typed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/9j5;->n:Z

    .line 1529093
    const-string v0, "has_scrolled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/9j5;->m:Z

    .line 1529094
    const-string v0, "has_tti_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/9j5;->r:Z

    .line 1529095
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9j5;->c:Ljava/lang/String;

    .line 1529096
    const-string v0, "composer_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9j5;->d:Ljava/lang/String;

    .line 1529097
    const-string v0, "place_picker_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9j5;->e:Ljava/lang/String;

    .line 1529098
    const-string v0, "start_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/9j5;->s:J

    .line 1529099
    const-string v0, "device_orientation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9j5;->j:I

    goto :goto_0
.end method

.method public final b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 5

    .prologue
    .line 1529081
    iget-object v0, p0, LX/9j5;->g:LX/9jN;

    if-nez v0, :cond_0

    .line 1529082
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call onNearbyListChanged first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1529083
    :cond_0
    iget-object v0, p0, LX/9j5;->a:LX/0Zb;

    const-string v1, "place_picker_place_picked"

    invoke-static {p0, v1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "results_seen"

    iget-object v3, p0, LX/9j5;->l:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "selected_row"

    iget-object v3, p0, LX/9j5;->g:LX/9jN;

    .line 1529084
    iget-object v4, v3, LX/9jN;->b:Ljava/util/List;

    move-object v3, v4

    .line 1529085
    invoke-interface {v3, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {p0, v1}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1529086
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1529079
    iget-object v0, p0, LX/9j5;->a:LX/0Zb;

    const-string v1, "place_picker_nonintrusive_error_gms_upsell_result"

    invoke-static {p0, v1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "gms_upsell_result"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1529080
    return-void
.end method

.method public final z()Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;
    .locals 6

    .prologue
    .line 1529078
    new-instance v0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    iget-object v1, p0, LX/9j5;->e:Ljava/lang/String;

    iget-object v2, p0, LX/9j5;->d:Ljava/lang/String;

    iget-wide v4, p0, LX/9j5;->s:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0
.end method
