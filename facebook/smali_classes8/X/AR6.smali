.class public LX/AR6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesPrivacyOverride;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0j5;",
        ":",
        "LX/0jB;",
        ":",
        "LX/0j9;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/AR1;

.field private final c:LX/0gd;

.field private final d:Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/0SG;

.field public final h:LX/8LV;

.field public final i:LX/1EZ;

.field private final j:LX/74n;


# direct methods
.method public constructor <init>(LX/0il;LX/AR1;LX/0gd;LX/0SG;LX/8LV;LX/1EZ;LX/74n;)V
    .locals 7
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AR1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/AR1;",
            "LX/0gd;",
            "LX/0SG;",
            "LX/8LV;",
            "LX/1EZ;",
            "LX/74n;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672386
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    iput-object v0, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    .line 1672387
    iput-object p2, p0, LX/AR6;->b:LX/AR1;

    .line 1672388
    iput-object p3, p0, LX/AR6;->c:LX/0gd;

    .line 1672389
    iget-object v0, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672390
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0io;

    check-cast p1, LX/0j3;

    invoke-interface {p1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0io;

    check-cast p1, LX/0ip;

    invoke-interface {p1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object p1

    invoke-static {p2, p1}, LX/94f;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1672391
    const/4 v0, 0x0

    .line 1672392
    :goto_0
    move-object v0, v0

    .line 1672393
    iput-object v0, p0, LX/AR6;->d:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1672394
    iget-object v0, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672395
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0io;

    check-cast p1, LX/0j3;

    invoke-interface {p1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0io;

    check-cast p1, LX/0jD;

    invoke-interface {p1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object p1

    invoke-static {p2, p1}, LX/94f;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0Px;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 1672396
    const/4 v0, 0x0

    .line 1672397
    :goto_1
    move-object v0, v0

    .line 1672398
    iput-object v0, p0, LX/AR6;->e:LX/0Rf;

    .line 1672399
    iget-object v1, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 1672400
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-static {v3, v2}, LX/94f;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1672401
    const/4 v1, 0x0

    .line 1672402
    :goto_2
    move-object v0, v1

    .line 1672403
    iput-object v0, p0, LX/AR6;->f:Ljava/lang/String;

    .line 1672404
    iput-object p4, p0, LX/AR6;->g:LX/0SG;

    .line 1672405
    iput-object p5, p0, LX/AR6;->h:LX/8LV;

    .line 1672406
    iput-object p6, p0, LX/AR6;->i:LX/1EZ;

    .line 1672407
    iput-object p7, p0, LX/AR6;->j:LX/74n;

    .line 1672408
    return-void

    .line 1672409
    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0io;

    check-cast p1, LX/0ip;

    invoke-interface {p1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object p1

    if-nez p1, :cond_1

    .line 1672410
    sget-object v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->b:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    goto :goto_0

    .line 1672411
    :cond_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->d()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jD;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    goto :goto_1

    .line 1672412
    :cond_3
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v2, v3, v5

    if-nez v2, :cond_4

    .line 1672413
    const-string v1, "0"

    goto :goto_2

    .line 1672414
    :cond_4
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method private a(LX/0ge;)V
    .locals 2

    .prologue
    .line 1672531
    iget-object v0, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672532
    iget-object v1, p0, LX/AR6;->c:LX/0gd;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1672533
    return-void
.end method

.method private b(Z)Lcom/facebook/composer/publish/common/EditPostParams;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1672468
    iget-object v0, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672469
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1672470
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1672471
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1672472
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1672473
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    .line 1672474
    iget-wide v10, v1, Lcom/facebook/ipc/media/MediaIdKey;->b:J

    move-wide v8, v10

    .line 1672475
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1672476
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1672477
    :cond_1
    invoke-static {}, Lcom/facebook/composer/publish/common/EditPostParams;->newBuilder()Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setComposerSessionId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLegacyApiStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setLegacyStoryApiId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j2;

    invoke-interface {v1}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setMessage(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setStoryId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCacheId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    move-object v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setCacheIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    iget-object v3, p0, LX/AR6;->d:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v1, v3}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setMinutiaeTag(Lcom/facebook/ipc/composer/model/MinutiaeTag;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    iget-object v1, p0, LX/AR6;->e:LX/0Rf;

    if-nez v1, :cond_8

    move-object v1, v2

    :goto_2
    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setTaggedIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    iget-object v3, p0, LX/AR6;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setPlaceTag(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0jE;

    invoke-interface {v1}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    .line 1672478
    iget-object v5, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1672479
    if-nez v5, :cond_d

    const/4 v5, 0x0

    :goto_3
    move-object v1, v5

    .line 1672480
    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setPrivacy(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setShouldPublishUnpublishedContent(Z)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setSourceType(LX/21D;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    iget-object v3, p0, LX/AR6;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v1, v6, v7}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setOriginalPostTime(J)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v6, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {v3, v6, v7}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setTargetId(J)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v1

    invoke-virtual {v3, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setHasMediaFbIds(Z)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    sget-object v5, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v1, v5}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v2

    :cond_2
    invoke-virtual {v3, v2}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setRichTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v2

    .line 1672481
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    .line 1672482
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    .line 1672483
    if-nez v3, :cond_9

    if-eqz v1, :cond_9

    .line 1672484
    sget-object v1, Lcom/facebook/composer/publish/common/LinkEdit;->a:Lcom/facebook/composer/publish/common/LinkEdit;

    invoke-virtual {v2, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setLinkEdit(Lcom/facebook/composer/publish/common/LinkEdit;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    .line 1672485
    :cond_3
    :goto_4
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1672486
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setMediaFbIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    .line 1672487
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    .line 1672488
    if-eqz v1, :cond_4

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1672489
    :cond_4
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1672490
    :goto_5
    move-object v1, v3

    .line 1672491
    invoke-virtual {v2, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setMediaCaptions(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    .line 1672492
    :cond_5
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v1

    .line 1672493
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jB;

    invoke-interface {v0}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    .line 1672494
    if-nez v0, :cond_b

    if-eqz v1, :cond_b

    .line 1672495
    sget-object v0, Lcom/facebook/composer/publish/common/StickerEdit;->a:Lcom/facebook/composer/publish/common/StickerEdit;

    invoke-virtual {v2, v0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setStickerEdit(Lcom/facebook/composer/publish/common/StickerEdit;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    .line 1672496
    :cond_6
    :goto_6
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v0

    return-object v0

    .line 1672497
    :cond_7
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCacheId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    goto/16 :goto_1

    :cond_8
    iget-object v1, p0, LX/AR6;->e:LX/0Rf;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto/16 :goto_2

    .line 1672498
    :cond_9
    if-eqz v3, :cond_3

    iget-object v5, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz v1, :cond_a

    iget-object v5, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v5, :cond_a

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    iget-object v5, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1672499
    :cond_a
    iget-object v1, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/composer/publish/common/LinkEdit;->a(Ljava/lang/String;)Lcom/facebook/composer/publish/common/LinkEdit;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setLinkEdit(Lcom/facebook/composer/publish/common/LinkEdit;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    goto/16 :goto_4

    .line 1672500
    :cond_b
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 1672501
    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1672502
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/publish/common/StickerEdit;->a(Ljava/lang/String;)Lcom/facebook/composer/publish/common/StickerEdit;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setStickerEdit(Lcom/facebook/composer/publish/common/StickerEdit;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    goto :goto_6

    :cond_d
    invoke-virtual {v5}, Lcom/facebook/privacy/model/SelectablePrivacyData;->d()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    .line 1672503
    :cond_e
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1672504
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_7
    if-ge v4, v6, :cond_f

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1672505
    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v3

    .line 1672506
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1672507
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_7

    .line 1672508
    :cond_f
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    goto/16 :goto_5
.end method

.method private e()V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1672509
    iget-object v0, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672510
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1672511
    :cond_0
    :goto_0
    return-void

    .line 1672512
    :cond_1
    sget-object v1, LX/0ge;->COMPOSER_EDIT_TAGS:LX/0ge;

    invoke-direct {p0, v1}, LX/AR6;->a(LX/0ge;)V

    .line 1672513
    iget-object v1, p0, LX/AR6;->d:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    if-eqz v1, :cond_2

    .line 1672514
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-nez v1, :cond_4

    .line 1672515
    sget-object v1, LX/0ge;->COMPOSER_EDIT_REMOVE_MINUTIAE_TAG:LX/0ge;

    invoke-direct {p0, v1}, LX/AR6;->a(LX/0ge;)V

    .line 1672516
    :cond_2
    :goto_1
    iget-object v1, p0, LX/AR6;->e:LX/0Rf;

    if-eqz v1, :cond_3

    .line 1672517
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0jD;

    invoke-interface {v1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1672518
    sget-object v1, LX/0ge;->COMPOSER_EDIT_REMOVE_WITH_TAG:LX/0ge;

    invoke-direct {p0, v1}, LX/AR6;->a(LX/0ge;)V

    .line 1672519
    :cond_3
    :goto_2
    iget-object v1, p0, LX/AR6;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1672520
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_8

    .line 1672521
    sget-object v0, LX/0ge;->COMPOSER_EDIT_REMOVE_PLACE_TAG:LX/0ge;

    invoke-direct {p0, v0}, LX/AR6;->a(LX/0ge;)V

    goto :goto_0

    .line 1672522
    :cond_4
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getMinutiaeObjectTag()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-nez v1, :cond_5

    .line 1672523
    sget-object v1, LX/0ge;->COMPOSER_EDIT_ADD_MINUTIAE_TAG:LX/0ge;

    invoke-direct {p0, v1}, LX/AR6;->a(LX/0ge;)V

    goto :goto_1

    .line 1672524
    :cond_5
    sget-object v1, LX/0ge;->COMPOSER_EDIT_CHANGE_MINUTIAE_TAG:LX/0ge;

    invoke-direct {p0, v1}, LX/AR6;->a(LX/0ge;)V

    goto :goto_1

    .line 1672525
    :cond_6
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1672526
    sget-object v1, LX/0ge;->COMPOSER_EDIT_ADD_WITH_TAG:LX/0ge;

    invoke-direct {p0, v1}, LX/AR6;->a(LX/0ge;)V

    goto :goto_2

    .line 1672527
    :cond_7
    sget-object v1, LX/0ge;->COMPOSER_EDIT_CHANGE_WITH_TAG:LX/0ge;

    invoke-direct {p0, v1}, LX/AR6;->a(LX/0ge;)V

    goto :goto_2

    .line 1672528
    :cond_8
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-nez v0, :cond_9

    .line 1672529
    sget-object v0, LX/0ge;->COMPOSER_EDIT_ADD_PLACE_TAG:LX/0ge;

    invoke-direct {p0, v0}, LX/AR6;->a(LX/0ge;)V

    goto/16 :goto_0

    .line 1672530
    :cond_9
    sget-object v0, LX/0ge;->COMPOSER_EDIT_CHANGE_PLACE_TAG:LX/0ge;

    invoke-direct {p0, v0}, LX/AR6;->a(LX/0ge;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1672467
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/AR6;->a(Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Landroid/content/Intent;
    .locals 14

    .prologue
    .line 1672415
    invoke-direct {p0}, LX/AR6;->e()V

    .line 1672416
    invoke-direct {p0, p1}, LX/AR6;->b(Z)Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v0

    .line 1672417
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1672418
    const-string v2, "publishEditPostParamsKey"

    .line 1672419
    iget-object v4, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    .line 1672420
    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, LX/0j3;

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1672421
    :cond_0
    :goto_0
    move-object v3, v0

    .line 1672422
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1672423
    iget-object v2, p0, LX/AR6;->b:LX/AR1;

    invoke-virtual {v2}, LX/AR1;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1672424
    const-string v2, "extra_optimistic_feed_story"

    .line 1672425
    iget-object v3, p0, LX/AR6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    .line 1672426
    iget-object v4, p0, LX/AR6;->b:LX/AR1;

    invoke-virtual {v4}, LX/AR1;->b()LX/9A3;

    move-result-object v5

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j3;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getStoryId()Ljava/lang/String;

    move-result-object v4

    .line 1672427
    iput-object v4, v5, LX/9A3;->v:Ljava/lang/String;

    .line 1672428
    move-object v5, v5

    .line 1672429
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j3;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLegacyApiStoryId()Ljava/lang/String;

    move-result-object v4

    .line 1672430
    iput-object v4, v5, LX/9A3;->z:Ljava/lang/String;

    .line 1672431
    move-object v4, v5

    .line 1672432
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 1672433
    iput-object v5, v4, LX/9A3;->w:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 1672434
    move-object v5, v4

    .line 1672435
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0iq;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    .line 1672436
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v3

    .line 1672437
    if-eqz v4, :cond_1

    iget-boolean v6, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    if-eqz v6, :cond_1

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v4, :cond_2

    :cond_1
    if-eqz v3, :cond_2

    invoke-interface {v3}, LX/1oU;->b()LX/1Fd;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1672438
    new-instance v4, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    invoke-interface {v3}, LX/1oU;->b()LX/1Fd;

    move-result-object v6

    invoke-interface {v6}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3}, LX/1oU;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v6, v3}, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672439
    iput-object v4, v5, LX/9A3;->r:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1672440
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v3

    .line 1672441
    iput-object v3, v5, LX/9A3;->y:Ljava/lang/String;

    .line 1672442
    invoke-virtual {v5}, LX/9A3;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    move-object v0, v3

    .line 1672443
    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1672444
    :cond_3
    return-object v1

    .line 1672445
    :cond_4
    const/4 v7, 0x0

    .line 1672446
    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    invoke-interface {v5}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v6

    .line 1672447
    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    invoke-interface {v4}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v8

    .line 1672448
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 1672449
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 1672450
    const/4 v4, 0x0

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v11

    move v5, v4

    :goto_1
    if-ge v5, v11, :cond_5

    .line 1672451
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ipc/media/MediaItem;

    .line 1672452
    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v12

    invoke-static {v12}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1672453
    invoke-virtual {v9, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1672454
    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1672455
    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v12

    sget-object v13, LX/4gF;->VIDEO:LX/4gF;

    if-ne v12, v13, :cond_8

    sget-object v12, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_8

    const/4 v12, 0x1

    :goto_2
    move v4, v12

    .line 1672456
    if-eqz v4, :cond_7

    .line 1672457
    const/4 v4, 0x1

    .line 1672458
    :goto_3
    add-int/lit8 v5, v5, 0x1

    move v7, v4

    goto :goto_1

    .line 1672459
    :cond_5
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1672460
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1672461
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1672462
    invoke-static {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->a(Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v4

    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setIsPhotoContainer(Z)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v10

    .line 1672463
    if-eqz v7, :cond_6

    iget-object v4, p0, LX/AR6;->h:LX/8LV;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v10}, LX/8LV;->a(LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v4

    .line 1672464
    :goto_4
    iget-object v5, p0, LX/AR6;->i:LX/1EZ;

    invoke-virtual {v5, v4}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1672465
    const-string v4, "is_uploading_media"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 1672466
    :cond_6
    iget-object v4, p0, LX/AR6;->h:LX/8LV;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object v9, v10

    invoke-virtual/range {v4 .. v9}, LX/8LV;->a(LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v4

    goto :goto_4

    :cond_7
    move v4, v7

    goto :goto_3

    :cond_8
    const/4 v12, 0x0

    goto :goto_2
.end method
