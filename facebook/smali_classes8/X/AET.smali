.class public final LX/AET;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/AEU;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1646627
    const-class v0, LX/AET;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AET;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1646628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1646629
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/AET;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/AET;->b:Ljava/lang/String;

    .line 1646630
    return-void

    .line 1646631
    :cond_0
    sget-object v0, LX/AET;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1646632
    new-instance v0, LX/AEU;

    invoke-direct {v0}, LX/AEU;-><init>()V

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1646633
    iget-object v0, p0, LX/AET;->b:Ljava/lang/String;

    return-object v0
.end method
