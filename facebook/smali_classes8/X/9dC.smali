.class public LX/9dC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/photos/imageprocessing/FiltersEngine;

.field private final b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ImageTransformExecutorService;
    .end annotation
.end field

.field public c:LX/9dD;

.field public d:LX/9dD;

.field public e:LX/9dD;

.field public volatile f:Z

.field public volatile g:Z

.field public volatile h:Z

.field public i:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/8Ht;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:[Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final k:LX/9dB;


# direct methods
.method public constructor <init>(LX/0TD;Lcom/facebook/photos/imageprocessing/FiltersEngine;LX/0Or;)V
    .locals 2
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ImageTransformExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "Lcom/facebook/photos/imageprocessing/FiltersEngine;",
            "LX/0Or",
            "<",
            "LX/9dD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1517679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1517680
    const/4 v0, 0x0

    iput-object v0, p0, LX/9dC;->j:[Landroid/graphics/RectF;

    .line 1517681
    new-instance v0, LX/9dB;

    invoke-direct {v0, p0}, LX/9dB;-><init>(LX/9dC;)V

    iput-object v0, p0, LX/9dC;->k:LX/9dB;

    .line 1517682
    iput-object p1, p0, LX/9dC;->b:LX/0TD;

    .line 1517683
    iput-object p2, p0, LX/9dC;->a:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1517684
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9dD;

    iput-object v0, p0, LX/9dC;->c:LX/9dD;

    .line 1517685
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9dD;

    iput-object v0, p0, LX/9dC;->d:LX/9dD;

    .line 1517686
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9dD;

    iput-object v0, p0, LX/9dC;->e:LX/9dD;

    .line 1517687
    iget-object v0, p0, LX/9dC;->c:LX/9dD;

    iget-object v1, p0, LX/9dC;->k:LX/9dB;

    .line 1517688
    iput-object v1, v0, LX/9dD;->f:LX/9dB;

    .line 1517689
    iget-object v0, p0, LX/9dC;->d:LX/9dD;

    iget-object v1, p0, LX/9dC;->k:LX/9dB;

    .line 1517690
    iput-object v1, v0, LX/9dD;->f:LX/9dB;

    .line 1517691
    iget-object v0, p0, LX/9dC;->e:LX/9dD;

    iget-object v1, p0, LX/9dC;->k:LX/9dB;

    .line 1517692
    iput-object v1, v0, LX/9dD;->f:LX/9dB;

    .line 1517693
    return-void
.end method

.method public static g$redex0(LX/9dC;)V
    .locals 4

    .prologue
    .line 1517725
    monitor-enter p0

    .line 1517726
    :try_start_0
    iget-object v0, p0, LX/9dC;->i:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    .line 1517727
    iget-object v1, p0, LX/9dC;->j:[Landroid/graphics/RectF;

    .line 1517728
    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    .line 1517729
    :cond_0
    if-eqz v0, :cond_1

    .line 1517730
    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1517731
    :cond_1
    monitor-exit p0

    .line 1517732
    :goto_0
    return-void

    .line 1517733
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517734
    iget-object v2, p0, LX/9dC;->b:LX/0TD;

    new-instance v3, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeablePostprocessorManager$2;

    invoke-direct {v3, p0, v1, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeablePostprocessorManager$2;-><init>(LX/9dC;[Landroid/graphics/RectF;LX/1FJ;)V

    const v0, -0x521bb20c

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 1517735
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static h(LX/9dC;)V
    .locals 1

    .prologue
    .line 1517721
    iget-object v0, p0, LX/9dC;->c:LX/9dD;

    invoke-virtual {v0}, LX/4fO;->c()V

    .line 1517722
    iget-object v0, p0, LX/9dC;->d:LX/9dD;

    invoke-virtual {v0}, LX/4fO;->c()V

    .line 1517723
    iget-object v0, p0, LX/9dC;->e:LX/9dD;

    invoke-virtual {v0}, LX/4fO;->c()V

    .line 1517724
    return-void
.end method


# virtual methods
.method public final a([Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1517736
    monitor-enter p0

    .line 1517737
    :try_start_0
    iput-object p1, p0, LX/9dC;->j:[Landroid/graphics/RectF;

    .line 1517738
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517739
    invoke-static {p0}, LX/9dC;->g$redex0(LX/9dC;)V

    .line 1517740
    return-void

    .line 1517741
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1517708
    iget-object v0, p0, LX/9dC;->c:LX/9dD;

    invoke-virtual {v0}, LX/9dD;->d()V

    .line 1517709
    iget-object v0, p0, LX/9dC;->d:LX/9dD;

    invoke-virtual {v0}, LX/9dD;->d()V

    .line 1517710
    iget-object v0, p0, LX/9dC;->e:LX/9dD;

    invoke-virtual {v0}, LX/9dD;->d()V

    .line 1517711
    iput-boolean v1, p0, LX/9dC;->f:Z

    .line 1517712
    iput-boolean v1, p0, LX/9dC;->g:Z

    .line 1517713
    iput-boolean v1, p0, LX/9dC;->h:Z

    .line 1517714
    monitor-enter p0

    .line 1517715
    :try_start_0
    iget-object v0, p0, LX/9dC;->i:LX/1FJ;

    .line 1517716
    const/4 v1, 0x0

    iput-object v1, p0, LX/9dC;->i:LX/1FJ;

    .line 1517717
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517718
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1517719
    return-void

    .line 1517720
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1517694
    iget-object v0, p0, LX/9dC;->c:LX/9dD;

    invoke-virtual {v0}, LX/9dD;->e()V

    .line 1517695
    iget-object v0, p0, LX/9dC;->d:LX/9dD;

    invoke-virtual {v0}, LX/9dD;->e()V

    .line 1517696
    iget-object v0, p0, LX/9dC;->e:LX/9dD;

    invoke-virtual {v0}, LX/9dD;->e()V

    .line 1517697
    monitor-enter p0

    .line 1517698
    :try_start_0
    iget-object v0, p0, LX/9dC;->i:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    .line 1517699
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517700
    if-eqz v0, :cond_0

    .line 1517701
    iget-object v1, p0, LX/9dC;->c:LX/9dD;

    invoke-virtual {v1, v0}, LX/9dD;->a(LX/1FJ;)V

    .line 1517702
    iget-object v1, p0, LX/9dC;->d:LX/9dD;

    invoke-virtual {v1, v0}, LX/9dD;->a(LX/1FJ;)V

    .line 1517703
    iget-object v1, p0, LX/9dC;->e:LX/9dD;

    invoke-virtual {v1, v0}, LX/9dD;->a(LX/1FJ;)V

    .line 1517704
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1517705
    :goto_0
    return-void

    .line 1517706
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1517707
    :cond_0
    invoke-static {p0}, LX/9dC;->h(LX/9dC;)V

    goto :goto_0
.end method
