.class public LX/AHT;
.super LX/AHR;
.source ""


# instance fields
.field private final c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1655657
    invoke-direct {p0, p2, p3}, LX/AHR;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655658
    iput-object p1, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    .line 1655659
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1655651
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel;

    .line 1655652
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1655653
    const/4 v0, 0x1

    .line 1655654
    :goto_1
    return v0

    .line 1655655
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1655656
    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1655650
    invoke-super {p0}, LX/AHR;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1655649
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1655648
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7gp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1655647
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7gp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1655637
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1655638
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7gp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1655646
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 1655645
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/1lQ;->o(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 1655644
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->o()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, LX/1lQ;->o(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final j()F
    .locals 4

    .prologue
    .line 1655641
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    int-to-float v0, v0

    .line 1655642
    iget-object v1, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->j(II)I

    move-result v1

    int-to-float v1, v1

    .line 1655643
    div-float v0, v1, v0

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1655640
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/AHR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1655639
    iget-object v0, p0, LX/AHT;->c:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
