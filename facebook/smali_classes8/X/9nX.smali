.class public final enum LX/9nX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9nX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9nX;

.field public static final enum DASHED:LX/9nX;

.field public static final enum DOTTED:LX/9nX;

.field public static final enum SOLID:LX/9nX;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1537399
    new-instance v0, LX/9nX;

    const-string v1, "SOLID"

    invoke-direct {v0, v1, v2}, LX/9nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9nX;->SOLID:LX/9nX;

    .line 1537400
    new-instance v0, LX/9nX;

    const-string v1, "DASHED"

    invoke-direct {v0, v1, v3}, LX/9nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9nX;->DASHED:LX/9nX;

    .line 1537401
    new-instance v0, LX/9nX;

    const-string v1, "DOTTED"

    invoke-direct {v0, v1, v4}, LX/9nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9nX;->DOTTED:LX/9nX;

    .line 1537402
    const/4 v0, 0x3

    new-array v0, v0, [LX/9nX;

    sget-object v1, LX/9nX;->SOLID:LX/9nX;

    aput-object v1, v0, v2

    sget-object v1, LX/9nX;->DASHED:LX/9nX;

    aput-object v1, v0, v3

    sget-object v1, LX/9nX;->DOTTED:LX/9nX;

    aput-object v1, v0, v4

    sput-object v0, LX/9nX;->$VALUES:[LX/9nX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1537398
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9nX;
    .locals 1

    .prologue
    .line 1537391
    const-class v0, LX/9nX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9nX;

    return-object v0
.end method

.method public static values()[LX/9nX;
    .locals 1

    .prologue
    .line 1537397
    sget-object v0, LX/9nX;->$VALUES:[LX/9nX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9nX;

    return-object v0
.end method


# virtual methods
.method public final getPathEffect(F)Landroid/graphics/PathEffect;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x40400000    # 3.0f

    .line 1537392
    sget-object v0, LX/9nW;->a:[I

    invoke-virtual {p0}, LX/9nX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1537393
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1537394
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1537395
    :pswitch_1
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x4

    new-array v1, v1, [F

    mul-float v2, p1, v3

    aput v2, v1, v5

    mul-float v2, p1, v3

    aput v2, v1, v6

    mul-float v2, p1, v3

    aput v2, v1, v7

    const/4 v2, 0x3

    mul-float/2addr v3, p1

    aput v3, v1, v2

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    goto :goto_0

    .line 1537396
    :pswitch_2
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x4

    new-array v1, v1, [F

    aput p1, v1, v5

    aput p1, v1, v6

    aput p1, v1, v7

    const/4 v2, 0x3

    aput p1, v1, v2

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
