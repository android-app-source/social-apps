.class public final LX/90V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;)V
    .locals 0

    .prologue
    .line 1428925
    iput-object p1, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1428926
    iget-object v0, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->p(Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;)LX/90u;

    move-result-object v0

    invoke-interface {v0}, LX/90u;->b()V

    .line 1428927
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1428928
    iget-object v1, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v1}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1428929
    const-string v1, "extra_place"

    iget-object v2, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428930
    iget-object v3, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v2, v3

    .line 1428931
    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1428932
    iget-object v1, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->s:LX/91G;

    sget-object v2, LX/91E;->SKIP:LX/91E;

    iget-object v3, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    iget-object v3, v3, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v3}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v3

    iget-object v4, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    iget-object v4, v4, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->q:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428933
    iget-object v5, v4, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v4, v5

    .line 1428934
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    iget-object v5, v5, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->E:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/91G;->a(LX/91E;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 1428935
    :cond_0
    iget-object v1, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1428936
    iget-object v0, p0, LX/90V;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTagPickerActivity;->finish()V

    .line 1428937
    return-void
.end method
