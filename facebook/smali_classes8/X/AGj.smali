.class public final LX/AGj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 1652995
    const/4 v14, 0x0

    .line 1652996
    const/4 v13, 0x0

    .line 1652997
    const/4 v12, 0x0

    .line 1652998
    const/4 v11, 0x0

    .line 1652999
    const/4 v10, 0x0

    .line 1653000
    const/4 v9, 0x0

    .line 1653001
    const/4 v8, 0x0

    .line 1653002
    const/4 v5, 0x0

    .line 1653003
    const-wide/16 v6, 0x0

    .line 1653004
    const/4 v4, 0x0

    .line 1653005
    const/4 v3, 0x0

    .line 1653006
    const/4 v2, 0x0

    .line 1653007
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 1653008
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1653009
    const/4 v2, 0x0

    .line 1653010
    :goto_0
    return v2

    .line 1653011
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v6, v0, :cond_b

    .line 1653012
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1653013
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1653014
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_0

    if-eqz v6, :cond_0

    .line 1653015
    const-string v16, "all_direct_users"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 1653016
    invoke-static/range {p0 .. p1}, LX/AIW;->a(LX/15w;LX/186;)I

    move-result v6

    move v15, v6

    goto :goto_1

    .line 1653017
    :cond_1
    const-string v16, "direct_messages"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 1653018
    invoke-static/range {p0 .. p1}, LX/AGi;->a(LX/15w;LX/186;)I

    move-result v6

    move v14, v6

    goto :goto_1

    .line 1653019
    :cond_2
    const-string v16, "direct_reactions"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 1653020
    invoke-static/range {p0 .. p1}, LX/AIY;->a(LX/15w;LX/186;)I

    move-result v6

    move v13, v6

    goto :goto_1

    .line 1653021
    :cond_3
    const-string v16, "id"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 1653022
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v12, v6

    goto :goto_1

    .line 1653023
    :cond_4
    const-string v16, "is_seen_by_viewer"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 1653024
    const/4 v3, 0x1

    .line 1653025
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v6

    goto :goto_1

    .line 1653026
    :cond_5
    const-string v16, "latest_message"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 1653027
    invoke-static/range {p0 .. p1}, LX/AIU;->a(LX/15w;LX/186;)I

    move-result v6

    move v10, v6

    goto/16 :goto_1

    .line 1653028
    :cond_6
    const-string v16, "root_message"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 1653029
    invoke-static/range {p0 .. p1}, LX/AIU;->a(LX/15w;LX/186;)I

    move-result v6

    move v9, v6

    goto/16 :goto_1

    .line 1653030
    :cond_7
    const-string v16, "seen_direct_users"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 1653031
    invoke-static/range {p0 .. p1}, LX/AIa;->a(LX/15w;LX/186;)I

    move-result v6

    move v7, v6

    goto/16 :goto_1

    .line 1653032
    :cond_8
    const-string v16, "time"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 1653033
    const/4 v2, 0x1

    .line 1653034
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 1653035
    :cond_9
    const-string v16, "url"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1653036
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v8, v6

    goto/16 :goto_1

    .line 1653037
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1653038
    :cond_b
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1653039
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 1653040
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 1653041
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 1653042
    const/4 v6, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 1653043
    if-eqz v3, :cond_c

    .line 1653044
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 1653045
    :cond_c
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1653046
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1653047
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1653048
    if-eqz v2, :cond_d

    .line 1653049
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1653050
    :cond_d
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1653051
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v4

    move-wide/from16 v18, v6

    move v7, v5

    move-wide/from16 v4, v18

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1652952
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1652953
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652954
    if-eqz v0, :cond_0

    .line 1652955
    const-string v1, "all_direct_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652956
    invoke-static {p0, v0, p2, p3}, LX/AIW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652957
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652958
    if-eqz v0, :cond_1

    .line 1652959
    const-string v1, "direct_messages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652960
    invoke-static {p0, v0, p2, p3}, LX/AGi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652961
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652962
    if-eqz v0, :cond_2

    .line 1652963
    const-string v1, "direct_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652964
    invoke-static {p0, v0, p2, p3}, LX/AIY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652965
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1652966
    if-eqz v0, :cond_3

    .line 1652967
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652968
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1652969
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1652970
    if-eqz v0, :cond_4

    .line 1652971
    const-string v1, "is_seen_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652972
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1652973
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652974
    if-eqz v0, :cond_5

    .line 1652975
    const-string v1, "latest_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652976
    invoke-static {p0, v0, p2, p3}, LX/AIU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652977
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652978
    if-eqz v0, :cond_6

    .line 1652979
    const-string v1, "root_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652980
    invoke-static {p0, v0, p2, p3}, LX/AIU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652981
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652982
    if-eqz v0, :cond_7

    .line 1652983
    const-string v1, "seen_direct_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652984
    invoke-static {p0, v0, p2, p3}, LX/AIa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652985
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1652986
    cmp-long v2, v0, v2

    if-eqz v2, :cond_8

    .line 1652987
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652988
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1652989
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1652990
    if-eqz v0, :cond_9

    .line 1652991
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652992
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1652993
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1652994
    return-void
.end method
