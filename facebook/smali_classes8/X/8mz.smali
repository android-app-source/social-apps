.class public final LX/8mz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8my;


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;)V
    .locals 0

    .prologue
    .line 1400253
    iput-object p1, p0, LX/8mz;->a:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1400254
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 1400255
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/8mz;->a:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-object v1, v1, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->j:LX/8my;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/8mz;->a:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-object v1, v1, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->j:LX/8my;

    invoke-interface {v1, p1, p2}, LX/8my;->a(J)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, LX/8mz;->a:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-object v1, v1, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->g:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
