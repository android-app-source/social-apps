.class public final LX/9LN;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Z

.field public final synthetic c:LX/9LP;


# direct methods
.method public constructor <init>(LX/9LP;Lcom/facebook/graphql/model/GraphQLStory;Z)V
    .locals 0

    .prologue
    .line 1470716
    iput-object p1, p0, LX/9LN;->c:LX/9LP;

    iput-object p2, p0, LX/9LN;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-boolean p3, p0, LX/9LN;->b:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1470717
    iget-object v0, p0, LX/9LN;->c:LX/9LP;

    iget-object v0, v0, LX/9LP;->h:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1470718
    iget-object v0, p0, LX/9LN;->c:LX/9LP;

    iget-object v0, v0, LX/9LP;->e:LX/0bH;

    new-instance v1, LX/1Ne;

    iget-object v2, p0, LX/9LN;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v1, v2}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1470719
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1470720
    iget-object v0, p0, LX/9LN;->c:LX/9LP;

    iget-object v0, v0, LX/9LP;->h:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1470721
    iget-object v0, p0, LX/9LN;->c:LX/9LP;

    iget-object v1, p0, LX/9LN;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, LX/9LN;->b:Z

    .line 1470722
    iget-object v3, v0, LX/9LP;->c:LX/3iV;

    invoke-virtual {v3, v1, v2}, LX/3iV;->a(Ljava/lang/String;Z)V

    .line 1470723
    iget-object v3, v0, LX/9LP;->d:LX/0jU;

    invoke-virtual {v3, v1}, LX/0jU;->c(Ljava/lang/String;)LX/0Px;

    move-result-object v5

    .line 1470724
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1470725
    iget-object v7, v0, LX/9LP;->d:LX/0jU;

    .line 1470726
    invoke-virtual {v7, v3}, LX/0jU;->d(Ljava/lang/String;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1470727
    if-nez p0, :cond_2

    .line 1470728
    :cond_0
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1470729
    :cond_1
    return-void

    .line 1470730
    :cond_2
    iget-object p1, v7, LX/0jU;->g:LX/189;

    invoke-virtual {p1, p0, v2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    .line 1470731
    if-eqz p0, :cond_0

    .line 1470732
    invoke-static {v7, v3}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object p1

    iget-object p1, p1, LX/18D;->d:Ljava/lang/String;

    invoke-static {v7, v3}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v1

    iget-object v1, v1, LX/18D;->e:Ljava/lang/String;

    invoke-static {v7, p0, p1, v1}, LX/0jU;->a(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
