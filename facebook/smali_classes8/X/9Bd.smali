.class public final LX/9Bd;
.super LX/9BW;
.source ""


# instance fields
.field public final synthetic b:LX/9Be;

.field public final c:LX/0wd;

.field private final d:LX/9Bb;

.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field private i:Z


# direct methods
.method public constructor <init>(LX/9Be;ILX/1zt;)V
    .locals 6

    .prologue
    .line 1451905
    iput-object p1, p0, LX/9Bd;->b:LX/9Be;

    .line 1451906
    invoke-direct {p0, p1, p2, p3}, LX/9BW;-><init>(LX/9BY;ILX/1zt;)V

    .line 1451907
    iget-object v0, p1, LX/9Be;->D:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    new-instance v1, LX/9Bc;

    invoke-direct {v1, p1, p0}, LX/9Bc;-><init>(LX/9Be;LX/9Bd;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iget v1, p1, LX/9BY;->A:F

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/9Bd;->c:LX/0wd;

    .line 1451908
    new-instance v0, LX/9Bb;

    invoke-direct {v0, p1, p0}, LX/9Bb;-><init>(LX/9Be;LX/9Bd;)V

    iput-object v0, p0, LX/9Bd;->d:LX/9Bb;

    .line 1451909
    iget-object v0, p1, LX/9Be;->O:LX/4lV;

    iget-object v1, p0, LX/9Bd;->d:LX/9Bb;

    .line 1451910
    iget-object v2, v0, LX/4lV;->a:LX/0wW;

    invoke-virtual {v2}, LX/0wW;->a()LX/0wd;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v2

    iget-object v3, v0, LX/4lV;->f:LX/0wT;

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v2

    .line 1451911
    iget-object v3, v0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1451912
    iget-object v2, v0, LX/4lV;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1451913
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    .line 1451914
    iget-boolean v0, p0, LX/9Bd;->i:Z

    if-ne v0, p1, :cond_0

    .line 1451915
    :goto_0
    return-void

    .line 1451916
    :cond_0
    if-eqz p1, :cond_1

    .line 1451917
    iget-object v0, p0, LX/9Bd;->b:LX/9Be;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/9Be;->performHapticFeedback(I)Z

    .line 1451918
    iget-object v0, p0, LX/9Bd;->b:LX/9Be;

    .line 1451919
    iget v1, p0, LX/9BW;->b:I

    move v1, v1

    .line 1451920
    if-ltz v1, :cond_1

    .line 1451921
    iget-object v2, v0, LX/9Be;->E:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3RX;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reactions_dock_select_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v1, 0x1

    const/4 v5, 0x6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1451922
    :cond_1
    iget-object v0, p0, LX/9Bd;->b:LX/9Be;

    .line 1451923
    iget-object v1, v0, LX/9Be;->F:LX/0tH;

    invoke-virtual {v1}, LX/0tH;->f()LX/1zu;

    move-result-object v1

    sget-object v2, LX/1zu;->VECTOR_ANIMATED_SELECTED:LX/1zu;

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1451924
    if-eqz v0, :cond_2

    .line 1451925
    iget-object v0, p0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 1451926
    instance-of v0, v0, LX/9Ug;

    if-eqz v0, :cond_2

    .line 1451927
    if-eqz p1, :cond_3

    .line 1451928
    iget-object v0, p0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 1451929
    check-cast v0, LX/9Ug;

    invoke-virtual {v0}, LX/9Ug;->a()V

    .line 1451930
    :cond_2
    :goto_2
    iput-boolean p1, p0, LX/9Bd;->i:Z

    goto :goto_0

    .line 1451931
    :cond_3
    iget-object v0, p0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 1451932
    check-cast v0, LX/9Ug;

    invoke-virtual {v0}, LX/9Ug;->c()V

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final i()F
    .locals 3

    .prologue
    .line 1451933
    iget-object v0, p0, LX/9Bd;->b:LX/9Be;

    iget v0, v0, LX/9BY;->z:F

    const/high16 v1, 0x40100000    # 2.25f

    iget-object v2, p0, LX/9Bd;->b:LX/9Be;

    iget v2, v2, LX/9BY;->z:F

    sub-float/2addr v1, v2

    iget v2, p0, LX/9Bd;->g:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final j()F
    .locals 3

    .prologue
    .line 1451934
    iget-object v0, p0, LX/9Bd;->b:LX/9Be;

    iget v0, v0, LX/9BY;->z:F

    iget-object v1, p0, LX/9Bd;->b:LX/9Be;

    iget v1, v1, LX/9BY;->B:F

    iget-object v2, p0, LX/9Bd;->b:LX/9Be;

    iget v2, v2, LX/9BY;->z:F

    sub-float/2addr v1, v2

    iget v2, p0, LX/9Bd;->g:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method
