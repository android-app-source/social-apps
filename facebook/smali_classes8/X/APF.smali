.class public LX/APF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/01T;


# direct methods
.method public constructor <init>(LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670075
    iput-object p1, p0, LX/APF;->a:LX/01T;

    .line 1670076
    return-void
.end method

.method public static a(LX/0QB;)LX/APF;
    .locals 1

    .prologue
    .line 1670077
    invoke-static {p0}, LX/APF;->b(LX/0QB;)LX/APF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/APF;
    .locals 2

    .prologue
    .line 1670078
    new-instance v1, LX/APF;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    invoke-direct {v1, v0}, LX/APF;-><init>(LX/01T;)V

    .line 1670079
    return-object v1
.end method


# virtual methods
.method public final a(LX/2rw;ZZZLX/ARN;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1670080
    if-eqz p4, :cond_1

    move p3, v0

    .line 1670081
    :cond_0
    :goto_0
    return p3

    .line 1670082
    :cond_1
    if-eqz p5, :cond_2

    invoke-interface {p5}, LX/ARN;->a()Z

    move-result v1

    if-nez v1, :cond_2

    move p3, v0

    .line 1670083
    goto :goto_0

    .line 1670084
    :cond_2
    if-nez p2, :cond_0

    .line 1670085
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1670086
    sget-object p2, LX/APE;->a:[I

    invoke-virtual {p1}, LX/2rw;->ordinal()I

    move-result p3

    aget p2, p2, p3

    packed-switch p2, :pswitch_data_0

    move v0, v1

    .line 1670087
    :cond_3
    :goto_1
    :pswitch_0
    move p3, v0

    .line 1670088
    goto :goto_0

    .line 1670089
    :pswitch_1
    iget-object p2, p0, LX/APF;->a:LX/01T;

    sget-object p3, LX/01T;->PAA:LX/01T;

    if-ne p2, p3, :cond_3

    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
