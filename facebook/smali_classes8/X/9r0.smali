.class public final LX/9r0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1551380
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1551381
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1551382
    :goto_0
    return v1

    .line 1551383
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1551384
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1551385
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1551386
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1551387
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1551388
    const-string v5, "footer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1551389
    invoke-static {p0, p1}, LX/9qz;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1551390
    :cond_2
    const-string v5, "subtitle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1551391
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1551392
    :cond_3
    const-string v5, "title"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1551393
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1551394
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1551395
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1551396
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1551397
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1551398
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1551399
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1551400
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1551401
    if-eqz v0, :cond_0

    .line 1551402
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551403
    invoke-static {p0, v0, p2, p3}, LX/9qz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1551404
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1551405
    if-eqz v0, :cond_1

    .line 1551406
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551407
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1551408
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1551409
    if-eqz v0, :cond_2

    .line 1551410
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551411
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1551412
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1551413
    return-void
.end method
