.class public LX/8u3;
.super LX/8tw;
.source ""


# instance fields
.field public a:I

.field private final b:[I

.field private final c:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x64

    .line 1414407
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/8tw;-><init>(Ljava/lang/String;)V

    .line 1414408
    const/4 v0, -0x1

    iput v0, p0, LX/8u3;->a:I

    .line 1414409
    new-array v0, v1, [I

    iput-object v0, p0, LX/8u3;->b:[I

    .line 1414410
    new-array v0, v1, [I

    iput-object v0, p0, LX/8u3;->c:[I

    .line 1414411
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1414412
    iget v0, p0, LX/8u3;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/8u3;->a:I

    .line 1414413
    iget v0, p0, LX/8u3;->a:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 1414414
    :goto_0
    return-void

    .line 1414415
    :cond_0
    iget-object v0, p0, LX/8u3;->c:[I

    iget v1, p0, LX/8u3;->a:I

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 1414416
    iget-object v0, p0, LX/8u3;->b:[I

    iget v1, p0, LX/8u3;->a:I

    aput p1, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;IILandroid/text/Editable;)V
    .locals 3

    .prologue
    .line 1414417
    iget v0, p0, LX/8u3;->a:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    iget v0, p0, LX/8u3;->a:I

    .line 1414418
    :goto_0
    iget-object v1, p0, LX/8u3;->c:[I

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    .line 1414419
    invoke-static {p4}, LX/33Q;->a(Landroid/text/Editable;)Landroid/text/Editable;

    .line 1414420
    new-instance v1, LX/8uD;

    invoke-direct {v1}, LX/8uD;-><init>()V

    iget-object v2, p0, LX/8u3;->b:[I

    aget v2, v2, v0

    .line 1414421
    iput v2, v1, LX/8uD;->g:I

    .line 1414422
    move-object v1, v1

    .line 1414423
    iget-object v2, p0, LX/8u3;->c:[I

    aget v0, v2, v0

    .line 1414424
    iput v0, v1, LX/8uD;->h:I

    .line 1414425
    move-object v0, v1

    .line 1414426
    invoke-interface {p4}, Landroid/text/Editable;->length()I

    move-result v1

    const/16 v2, 0x21

    invoke-interface {p4, v0, p2, v1, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1414427
    return-void

    .line 1414428
    :cond_0
    const/16 v0, 0x63

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/text/Editable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1414429
    const-string v1, "ol"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ul"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1414430
    :cond_0
    iget v1, p0, LX/8u3;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/8u3;->a:I

    .line 1414431
    :goto_0
    return v0

    .line 1414432
    :cond_1
    const-string v1, "li"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1414433
    invoke-virtual {p0, p2, p0}, LX/8tw;->b(Landroid/text/Editable;Ljava/lang/Object;)V

    goto :goto_0

    .line 1414434
    :cond_2
    invoke-super {p0, p1, p2}, LX/8tw;->a(Ljava/lang/String;Landroid/text/Editable;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lorg/xml/sax/Attributes;Landroid/text/Editable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1414435
    const-string v1, "ol"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1414436
    sget v1, LX/8uD;->e:I

    invoke-direct {p0, v1}, LX/8u3;->a(I)V

    .line 1414437
    :goto_0
    return v0

    .line 1414438
    :cond_0
    const-string v1, "ul"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1414439
    sget v1, LX/8uD;->d:I

    invoke-direct {p0, v1}, LX/8u3;->a(I)V

    goto :goto_0

    .line 1414440
    :cond_1
    const-string v1, "li"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1414441
    invoke-static {p3}, LX/33Q;->a(Landroid/text/Editable;)Landroid/text/Editable;

    .line 1414442
    invoke-static {p3, p0}, LX/8tw;->a(Landroid/text/Editable;Ljava/lang/Object;)V

    goto :goto_0

    .line 1414443
    :cond_2
    invoke-super {p0, p1, p2, p3}, LX/8tw;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Landroid/text/Editable;)Z

    move-result v0

    goto :goto_0
.end method
