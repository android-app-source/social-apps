.class public final LX/9qw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1551148
    const/4 v10, 0x0

    .line 1551149
    const/4 v9, 0x0

    .line 1551150
    const/4 v8, 0x0

    .line 1551151
    const/4 v7, 0x0

    .line 1551152
    const/4 v6, 0x0

    .line 1551153
    const/4 v5, 0x0

    .line 1551154
    const/4 v4, 0x0

    .line 1551155
    const/4 v3, 0x0

    .line 1551156
    const/4 v2, 0x0

    .line 1551157
    const/4 v1, 0x0

    .line 1551158
    const/4 v0, 0x0

    .line 1551159
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1551160
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1551161
    const/4 v0, 0x0

    .line 1551162
    :goto_0
    return v0

    .line 1551163
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1551164
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 1551165
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1551166
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1551167
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1551168
    const-string v12, "can_viewer_like"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1551169
    const/4 v1, 0x1

    .line 1551170
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1551171
    :cond_2
    const-string v12, "cover_photo"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1551172
    invoke-static {p0, p1}, LX/5tN;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1551173
    :cond_3
    const-string v12, "does_viewer_like"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1551174
    const/4 v0, 0x1

    .line 1551175
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1551176
    :cond_4
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1551177
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1551178
    :cond_5
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1551179
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1551180
    :cond_6
    const-string v12, "page_call_to_action"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1551181
    invoke-static {p0, p1}, LX/8FF;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1551182
    :cond_7
    const-string v12, "page_likers"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1551183
    invoke-static {p0, p1}, LX/9qu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1551184
    :cond_8
    const-string v12, "profile_picture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1551185
    invoke-static {p0, p1}, LX/9qv;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1551186
    :cond_9
    const-string v12, "top_category_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1551187
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 1551188
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1551189
    if-eqz v1, :cond_b

    .line 1551190
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 1551191
    :cond_b
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1551192
    if-eqz v0, :cond_c

    .line 1551193
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 1551194
    :cond_c
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1551195
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1551196
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1551197
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1551198
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1551199
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1551200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1551201
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1551202
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1551203
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/9qw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1551204
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1551205
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1551206
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1551207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1551208
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1551209
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1551210
    invoke-static {p0, p1}, LX/9qw;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1551211
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1551212
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1551213
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1551214
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1551215
    if-eqz v0, :cond_0

    .line 1551216
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551217
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1551218
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1551219
    if-eqz v0, :cond_1

    .line 1551220
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551221
    invoke-static {p0, v0, p2, p3}, LX/5tN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1551222
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1551223
    if-eqz v0, :cond_2

    .line 1551224
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551225
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1551226
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1551227
    if-eqz v0, :cond_3

    .line 1551228
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551229
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1551230
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1551231
    if-eqz v0, :cond_4

    .line 1551232
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551233
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1551234
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1551235
    if-eqz v0, :cond_5

    .line 1551236
    const-string v1, "page_call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551237
    invoke-static {p0, v0, p2, p3}, LX/8FF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1551238
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1551239
    if-eqz v0, :cond_6

    .line 1551240
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551241
    invoke-static {p0, v0, p2}, LX/9qu;->a(LX/15i;ILX/0nX;)V

    .line 1551242
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1551243
    if-eqz v0, :cond_7

    .line 1551244
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551245
    invoke-static {p0, v0, p2}, LX/9qv;->a(LX/15i;ILX/0nX;)V

    .line 1551246
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1551247
    if-eqz v0, :cond_8

    .line 1551248
    const-string v1, "top_category_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1551249
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1551250
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1551251
    return-void
.end method
