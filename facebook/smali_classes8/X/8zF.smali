.class public LX/8zF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 1427028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1427029
    iput-object p1, p0, LX/8zF;->a:Landroid/content/res/Resources;

    .line 1427030
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/text/SpannableStringBuilder;LX/1ln;Landroid/net/Uri;F)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1427021
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Landroid/net/Uri;

    invoke-virtual {p1, v4, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/Uri;

    .line 1427022
    array-length v1, v0

    if-lez v1, :cond_0

    aget-object v1, v0, v4

    invoke-virtual {v1, p3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1427023
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1427024
    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v1, v2

    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 1427025
    float-to-int v2, v2

    float-to-int v1, v1

    invoke-virtual {p0, v4, v4, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1427026
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    aget-object v2, v0, v4

    invoke-virtual {p1, v2}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    aget-object v0, v0, v4

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    const/16 v3, 0x21

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1427027
    :cond_0
    return-object p1
.end method
