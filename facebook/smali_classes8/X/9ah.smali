.class public final LX/9ah;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V
    .locals 0

    .prologue
    .line 1513957
    iput-object p1, p0, LX/9ah;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x661b3347

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1513958
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v1

    sget-object v2, LX/9jG;->ALBUM_CREATOR:LX/9jG;

    .line 1513959
    iput-object v2, v1, LX/9jF;->q:LX/9jG;

    .line 1513960
    move-object v1, v1

    .line 1513961
    iget-object v2, p0, LX/9ah;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v2, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v2, :cond_0

    .line 1513962
    iget-object v2, p0, LX/9ah;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v2, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    .line 1513963
    iput-object v2, v1, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1513964
    :cond_0
    iget-object v2, p0, LX/9ah;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    iget-object v2, v2, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/9ah;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v1

    invoke-static {v3, v1}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v1

    const/16 v3, 0x65

    iget-object v4, p0, LX/9ah;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1513965
    const v1, 0x3387628f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
