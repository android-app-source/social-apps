.class public final LX/8k6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1395525
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1395526
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395527
    :goto_0
    return v1

    .line 1395528
    :cond_0
    const-string v11, "is_featured"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1395529
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    .line 1395530
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 1395531
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1395532
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395533
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1395534
    const-string v11, "color_code"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1395535
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1395536
    :cond_2
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1395537
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1395538
    :cond_3
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1395539
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1395540
    :cond_4
    const-string v11, "order"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1395541
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1395542
    :cond_5
    const-string v11, "thumbnail_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1395543
    const/4 v10, 0x0

    .line 1395544
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v11, :cond_e

    .line 1395545
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395546
    :goto_2
    move v4, v10

    .line 1395547
    goto :goto_1

    .line 1395548
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1395549
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1395550
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1395551
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1395552
    if-eqz v3, :cond_8

    .line 1395553
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 1395554
    :cond_8
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1395555
    if-eqz v0, :cond_9

    .line 1395556
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 1395557
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1395558
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1

    .line 1395559
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395560
    :cond_c
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 1395561
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1395562
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395563
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_c

    if-eqz v11, :cond_c

    .line 1395564
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1395565
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1395566
    :cond_d
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1395567
    invoke-virtual {p1, v10, v4}, LX/186;->b(II)V

    .line 1395568
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_2

    :cond_e
    move v4, v10

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1395569
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395570
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1395571
    if-eqz v0, :cond_0

    .line 1395572
    const-string v1, "color_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395573
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1395574
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1395575
    if-eqz v0, :cond_1

    .line 1395576
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395577
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1395578
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1395579
    if-eqz v0, :cond_2

    .line 1395580
    const-string v1, "is_featured"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395581
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1395582
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1395583
    if-eqz v0, :cond_3

    .line 1395584
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395585
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1395586
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1395587
    if-eqz v0, :cond_4

    .line 1395588
    const-string v1, "order"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395589
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1395590
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1395591
    if-eqz v0, :cond_6

    .line 1395592
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395593
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395594
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1395595
    if-eqz v1, :cond_5

    .line 1395596
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395597
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1395598
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395599
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395600
    return-void
.end method
