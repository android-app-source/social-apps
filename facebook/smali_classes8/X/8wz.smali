.class public LX/8wz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8wx;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8x0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1423373
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8wz;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8x0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1423370
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1423371
    iput-object p1, p0, LX/8wz;->b:LX/0Ot;

    .line 1423372
    return-void
.end method

.method private a(LX/1De;II)LX/8wx;
    .locals 2

    .prologue
    .line 1423369
    new-instance v0, LX/8wy;

    invoke-direct {v0, p0}, LX/8wy;-><init>(LX/8wz;)V

    invoke-static {p1, p2, p3, v0}, LX/8wz;->a(LX/1De;IILX/8wy;)LX/8wx;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1De;IILX/8wy;)LX/8wx;
    .locals 1

    .prologue
    .line 1423364
    sget-object v0, LX/8wz;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8wx;

    .line 1423365
    if-nez v0, :cond_0

    .line 1423366
    new-instance v0, LX/8wx;

    invoke-direct {v0}, LX/8wx;-><init>()V

    .line 1423367
    :cond_0
    invoke-static {v0, p0, p1, p2, p3}, LX/8wx;->a$redex0(LX/8wx;LX/1De;IILX/8wy;)V

    .line 1423368
    return-object v0
.end method

.method public static a(LX/0QB;)LX/8wz;
    .locals 3

    .prologue
    .line 1423356
    const-class v1, LX/8wz;

    monitor-enter v1

    .line 1423357
    :try_start_0
    sget-object v0, LX/8wz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1423358
    sput-object v2, LX/8wz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1423359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/8wz;->b(LX/0QB;)LX/8wz;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1423361
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8wz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1423362
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1423363
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1dQ;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1423349
    new-instance v0, LX/8ww;

    invoke-direct {v0}, LX/8ww;-><init>()V

    .line 1423350
    iput-object p1, v0, LX/8ww;->a:Landroid/view/View;

    .line 1423351
    iput p2, v0, LX/8ww;->b:I

    .line 1423352
    iget-object v1, p0, LX/1dQ;->a:LX/1X1;

    .line 1423353
    iget-object p1, v1, LX/1X1;->e:LX/1S3;

    move-object v1, p1

    .line 1423354
    invoke-virtual {v1, p0, v0}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1423355
    return-void
.end method

.method private a(Landroid/view/View;LX/1De;LX/1X1;)V
    .locals 1

    .prologue
    .line 1423374
    check-cast p3, LX/8wy;

    .line 1423375
    iget-object v0, p0, LX/8wz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget v0, p3, LX/8wy;->a:I

    invoke-static {p1, v0, p2}, LX/8x0;->onClick(Landroid/view/View;ILX/1De;)V

    .line 1423376
    return-void
.end method

.method private static b(LX/0QB;)LX/8wz;
    .locals 2

    .prologue
    .line 1423347
    new-instance v0, LX/8wz;

    const/16 v1, 0x17a0

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8wz;-><init>(LX/0Ot;)V

    .line 1423348
    return-object v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 1

    .prologue
    .line 1423341
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1423342
    if-nez v0, :cond_0

    .line 1423343
    const/4 v0, 0x0

    .line 1423344
    :goto_0
    return-object v0

    .line 1423345
    :cond_0
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1423346
    check-cast v0, LX/8wy;

    iget-object v0, v0, LX/8wy;->c:LX/1dQ;

    goto :goto_0
.end method

.method public static onClick(LX/1De;LX/1De;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1423340
    const v0, -0x31df57b4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;LX/1De;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1423339
    const v0, -0x31df57b4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1423336
    check-cast p2, LX/8wy;

    .line 1423337
    iget-object v0, p0, LX/8wz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8x0;

    iget v1, p2, LX/8wy;->a:I

    iget v2, p2, LX/8wy;->b:I

    invoke-virtual {v0, p1, v1, v2}, LX/8x0;->a(LX/1De;II)LX/1Dg;

    move-result-object v0

    .line 1423338
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1423330
    invoke-static {}, LX/1dS;->b()V

    .line 1423331
    iget v0, p1, LX/1dQ;->b:I

    .line 1423332
    packed-switch v0, :pswitch_data_0

    .line 1423333
    :goto_0
    return-object v3

    .line 1423334
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1423335
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v1, v0, v2}, LX/8wz;->a(Landroid/view/View;LX/1De;LX/1X1;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x31df57b4
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/8wx;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1423329
    invoke-direct {p0, p1, v0, v0}, LX/8wz;->a(LX/1De;II)LX/8wx;

    move-result-object v0

    return-object v0
.end method
