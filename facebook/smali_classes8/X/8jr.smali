.class public final LX/8jr;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$SearchTaggedStickersQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1392820
    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$SearchTaggedStickersQueryModel;

    const v0, -0x3def245

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "SearchTaggedStickersQuery"

    const-string v6, "4a98a7f24c39bc3bd08185135eda2abd"

    const-string v7, "sticker_search"

    const-string v8, "10155093174986729"

    const-string v9, "10155259090061729"

    .line 1392821
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1392822
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1392823
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1392810
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1392811
    sparse-switch v0, :sswitch_data_0

    .line 1392812
    :goto_0
    return-object p1

    .line 1392813
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1392814
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1392815
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1392816
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1392817
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1392818
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1392819
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x46c3012f -> :sswitch_1
        0x6234bbb -> :sswitch_3
        0x1b7d0371 -> :sswitch_6
        0x1df56d39 -> :sswitch_2
        0x2161bcaf -> :sswitch_0
        0x73a026b5 -> :sswitch_4
        0x763c4507 -> :sswitch_5
    .end sparse-switch
.end method
