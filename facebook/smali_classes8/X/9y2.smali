.class public final LX/9y2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1593858
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1593859
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593860
    :goto_0
    return v1

    .line 1593861
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593862
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1593863
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1593864
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1593865
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1593866
    const-string v6, "album_cover_photo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1593867
    invoke-static {p0, p1}, LX/9y0;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1593868
    :cond_2
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1593869
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1593870
    :cond_3
    const-string v6, "photo_items"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1593871
    invoke-static {p0, p1}, LX/9y1;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1593872
    :cond_4
    const-string v6, "title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1593873
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1593874
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1593875
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1593876
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1593877
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1593878
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1593879
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1593880
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1593881
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593882
    if-eqz v0, :cond_0

    .line 1593883
    const-string v1, "album_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593884
    invoke-static {p0, v0, p2, p3}, LX/9y0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1593885
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1593886
    if-eqz v0, :cond_1

    .line 1593887
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593888
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1593889
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593890
    if-eqz v0, :cond_2

    .line 1593891
    const-string v1, "photo_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593892
    invoke-static {p0, v0, p2}, LX/9y1;->a(LX/15i;ILX/0nX;)V

    .line 1593893
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593894
    if-eqz v0, :cond_3

    .line 1593895
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593896
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1593897
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1593898
    return-void
.end method
