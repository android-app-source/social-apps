.class public final LX/966;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Lcom/facebook/graphql/model/FeedUnit;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Z

.field public final synthetic h:LX/967;


# direct methods
.method public constructor <init>(LX/967;LX/1L9;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1438143
    iput-object p1, p0, LX/966;->h:LX/967;

    iput-object p2, p0, LX/966;->a:LX/1L9;

    iput-object p3, p0, LX/966;->b:Lcom/facebook/graphql/model/FeedUnit;

    iput-object p4, p0, LX/966;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p5, p0, LX/966;->d:Ljava/lang/String;

    iput-object p6, p0, LX/966;->e:Ljava/lang/String;

    iput-object p7, p0, LX/966;->f:Ljava/lang/String;

    iput-boolean p8, p0, LX/966;->g:Z

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 1438144
    iget-object v0, p0, LX/966;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1438145
    iget-object v1, p0, LX/966;->a:LX/1L9;

    invoke-interface {v1, v0, p1}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438146
    iget-object v0, p0, LX/966;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/966;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1438147
    iget-object v0, p0, LX/966;->h:LX/967;

    iget-object v0, v0, LX/967;->d:LX/0Zb;

    .line 1438148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/966;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_fail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/966;->f:Ljava/lang/String;

    iget-boolean v3, p0, LX/966;->g:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/966;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, LX/17V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438149
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1438150
    iget-object v0, p0, LX/966;->a:LX/1L9;

    iget-object v1, p0, LX/966;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0, v1}, LX/1L9;->b(Ljava/lang/Object;)V

    .line 1438151
    return-void
.end method
