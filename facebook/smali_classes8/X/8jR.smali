.class public final LX/8jR;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/8jS;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/client/StickerToPackMetadataLoader;LX/8jS;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1392498
    iput-object p1, p0, LX/8jR;->c:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    iput-object p2, p0, LX/8jR;->a:LX/8jS;

    iput-object p3, p0, LX/8jR;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1392499
    iget-object v0, p0, LX/8jR;->c:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    iget-object v1, p0, LX/8jR;->a:LX/8jS;

    const-string v2, "fetchStickerPacksAsync failed"

    iget-object v3, p0, LX/8jR;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1392500
    sget-object p0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a:Ljava/lang/Class;

    invoke-static {p0, v2, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1392501
    invoke-virtual {v3, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1392502
    iget-object p0, v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    if-eqz p0, :cond_0

    .line 1392503
    iget-object p0, v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    invoke-interface {p0, v1, p1}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1392504
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1392505
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1392506
    iget-object v0, p0, LX/8jR;->c:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    const/4 v1, 0x0

    .line 1392507
    iput-object v1, v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->f:LX/1Mv;

    .line 1392508
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1392509
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v1, v1

    .line 1392510
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1392511
    invoke-virtual {v0, v1}, Lcom/facebook/stickers/service/FetchStickerPacksResult;->a(Lcom/facebook/stickers/model/StickerPack;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8m2;

    .line 1392512
    new-instance v2, LX/8jT;

    invoke-direct {v2, v1, v0}, LX/8jT;-><init>(Lcom/facebook/stickers/model/StickerPack;LX/8m2;)V

    .line 1392513
    iget-object v0, p0, LX/8jR;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x627cd977

    invoke-static {v0, v2, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1392514
    iget-object v0, p0, LX/8jR;->c:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    iget-object v0, v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    if-eqz v0, :cond_0

    .line 1392515
    iget-object v0, p0, LX/8jR;->c:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    iget-object v0, v0, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    iget-object v1, p0, LX/8jR;->a:LX/8jS;

    invoke-interface {v0, v1, v2}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1392516
    :cond_0
    return-void
.end method
