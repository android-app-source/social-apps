.class public LX/8nC;
.super LX/8nB;
.source ""


# instance fields
.field public final a:LX/8cT;

.field public final b:LX/3iT;

.field private final c:LX/3Qc;

.field public final d:I


# direct methods
.method public constructor <init>(LX/8cT;LX/3iT;LX/3Qc;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400638
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1400639
    iput-object p1, p0, LX/8nC;->a:LX/8cT;

    .line 1400640
    iput-object p2, p0, LX/8nC;->b:LX/3iT;

    .line 1400641
    const/16 v0, 0x3e8

    iput v0, p0, LX/8nC;->d:I

    .line 1400642
    iput-object p3, p0, LX/8nC;->c:LX/3Qc;

    .line 1400643
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 1

    .prologue
    .line 1400644
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, LX/8nB;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1400645
    :cond_0
    :goto_0
    return-void

    .line 1400646
    :cond_1
    iget-object v0, p0, LX/8nC;->c:LX/3Qc;

    invoke-virtual {v0}, LX/3Qc;->a()V

    .line 1400647
    iget-object v0, p0, LX/8nC;->a:LX/8cT;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    iget p3, p0, LX/8nC;->d:I

    invoke-virtual {v0, p2, p3}, LX/8cT;->a(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance p2, LX/8nA;

    invoke-direct {p2, p0, p8, p1}, LX/8nA;-><init>(LX/8nC;LX/8JX;Ljava/lang/CharSequence;)V

    invoke-static {v0, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1400648
    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1400649
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400650
    const-string v0, "bootstrap"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400651
    const-string v0, "@"

    return-object v0
.end method
