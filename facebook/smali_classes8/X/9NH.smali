.class public final LX/9NH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$MessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ToModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1475065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1475066
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1475067
    iget-object v1, p0, LX/9NH;->a:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1475068
    iget-object v2, p0, LX/9NH;->b:LX/0Px;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1475069
    iget-object v3, p0, LX/9NH;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1475070
    iget-object v4, p0, LX/9NH;->e:Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1475071
    iget-object v4, p0, LX/9NH;->f:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1475072
    iget-object v4, p0, LX/9NH;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$MessageModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1475073
    iget-object v4, p0, LX/9NH;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ToModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1475074
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1475075
    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 1475076
    invoke-virtual {v0, v12, v2}, LX/186;->b(II)V

    .line 1475077
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1475078
    const/4 v1, 0x3

    iget-wide v2, p0, LX/9NH;->d:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1475079
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1475080
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1475081
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1475082
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1475083
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1475084
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1475085
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1475086
    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1475087
    new-instance v0, LX/15i;

    move-object v2, v10

    move-object v3, v10

    move v4, v12

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1475088
    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;-><init>(LX/15i;)V

    .line 1475089
    return-object v1
.end method
