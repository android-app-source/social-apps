.class public final LX/8mU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/store/StickerStoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V
    .locals 0

    .prologue
    .line 1398836
    iput-object p1, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 1398837
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1398838
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v1

    .line 1398839
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1398840
    iget-object v1, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 1398841
    iget-object v1, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 1398842
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_1

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1398843
    iget-object v3, v1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v6, v3

    .line 1398844
    iget-object v3, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v3, v3, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/4m4;

    invoke-virtual {v6, v3}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1398845
    iget-object v3, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v3, v3, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1398846
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1398847
    invoke-virtual {v3, v6, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1398848
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 1398849
    :cond_0
    iget-object v3, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v3, v3, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1398850
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1398851
    invoke-virtual {v3, v6, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1398852
    :cond_1
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    .line 1398853
    iput-boolean v7, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->E:Z

    .line 1398854
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    .line 1398855
    invoke-static {v0, v7}, Lcom/facebook/stickers/store/StickerStoreFragment;->a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V

    .line 1398856
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->m:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1398857
    if-eqz v0, :cond_2

    .line 1398858
    const-string v1, "StickerFetchingStickerPacks"

    iget-object v3, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v3, v3, Lcom/facebook/stickers/store/StickerStoreFragment;->l:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x25a795d7

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1398859
    :cond_2
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->m:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1398860
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1398861
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    const-string v1, "Fetching downloaded sticker packs failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1398862
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->d:LX/03V;

    sget-object v1, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Fetching downloaded sticker packs failed"

    invoke-virtual {v0, v1, v3, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1398863
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    const/4 v1, 0x1

    .line 1398864
    invoke-static {v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Z)V

    .line 1398865
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->m:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1398866
    if-eqz v0, :cond_0

    .line 1398867
    const-string v1, "StickerFetchingStickerPacks"

    iget-object v3, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v3, v3, Lcom/facebook/stickers/store/StickerStoreFragment;->l:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x1ba831c2

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1398868
    :cond_0
    iget-object v0, p0, LX/8mU;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->m:LX/11i;

    sget-object v1, LX/8lJ;->c:LX/8lH;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1398869
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1398870
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/8mU;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
