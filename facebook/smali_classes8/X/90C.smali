.class public final LX/90C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/90B;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V
    .locals 0

    .prologue
    .line 1428439
    iput-object p1, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V
    .locals 12

    .prologue
    .line 1428440
    iget-object v0, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    invoke-virtual {v0, p1}, LX/91v;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 1428441
    iget-object v1, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "minutiae_object"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1428442
    iget-object v0, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1428443
    iget-object v0, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->r:LX/91D;

    iget-object v1, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-static {v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1428444
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1428445
    iget-object v2, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    .line 1428446
    iget-object v3, v2, LX/91v;->g:LX/5LG;

    move-object v2, v3

    .line 1428447
    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v4, v4, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    .line 1428448
    iget-object v5, v4, LX/91k;->a:LX/0Px;

    if-eqz v5, :cond_0

    iget-object v5, v4, LX/91k;->a:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    :goto_0
    move v4, v5

    .line 1428449
    iget-object v5, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v5, v5, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    invoke-virtual {v5}, LX/91k;->m()LX/0Px;

    move-result-object v5

    iget-object v6, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v6, v6, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    .line 1428450
    iget v7, v6, LX/90G;->a:I

    move v6, v7

    .line 1428451
    iget-object v7, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v7, v7, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    .line 1428452
    iget-object v8, v7, LX/91k;->a:LX/0Px;

    invoke-virtual {v8, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v8

    move v7, v8

    .line 1428453
    iget-object v8, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v8, v8, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    invoke-virtual {v8}, LX/91v;->d()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget v9, v9, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/90C;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v10, v10, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    .line 1428454
    iget-object v11, v10, LX/91k;->b:Ljava/util/Map;

    invoke-interface {v11, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    move-object v10, v11

    .line 1428455
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ag_()Ljava/lang/String;

    move-result-object v11

    .line 1428456
    const-string p0, "feeling_selector_object_selected"

    invoke-static {p0, v1}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/8yQ;->b(I)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/8yQ;->c(I)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v9}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v10}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v11}, LX/8yQ;->g(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    .line 1428457
    iget-object p1, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p0, p1

    .line 1428458
    iget-object p1, v0, LX/91D;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428459
    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
