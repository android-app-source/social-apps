.class public final LX/9lT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/399;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Js1;

.field public final synthetic b:Lcom/facebook/rapidreporting/ui/DialogStateData;

.field public final synthetic c:LX/9lb;


# direct methods
.method public constructor <init>(LX/9lb;LX/Js1;Lcom/facebook/rapidreporting/ui/DialogStateData;)V
    .locals 0

    .prologue
    .line 1532911
    iput-object p1, p0, LX/9lT;->c:LX/9lb;

    iput-object p2, p0, LX/9lT;->a:LX/Js1;

    iput-object p3, p0, LX/9lT;->b:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1532912
    iget-object v0, p0, LX/9lT;->a:LX/Js1;

    iget-object v1, p0, LX/9lT;->b:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1532913
    invoke-static {v0}, LX/Js1;->a(LX/Js1;)Ljava/util/List;

    move-result-object v2

    .line 1532914
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1532915
    :cond_0
    const/4 v2, 0x0

    .line 1532916
    :goto_0
    move-object v0, v2

    .line 1532917
    return-object v0

    .line 1532918
    :cond_1
    new-instance v3, LX/4K0;

    invoke-direct {v3}, LX/4K0;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/ui/DialogStateData;->l()Ljava/lang/String;

    move-result-object v4

    .line 1532919
    const-string v0, "rapid_reporting_prompt_node_token"

    invoke-virtual {v3, v0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532920
    move-object v3, v3

    .line 1532921
    iget-object v4, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v4, :cond_2

    .line 1532922
    const/4 v4, 0x0

    .line 1532923
    :goto_1
    move-object v4, v4

    .line 1532924
    const-string p0, "srt_job_id"

    invoke-virtual {v3, p0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532925
    move-object v3, v3

    .line 1532926
    const-string v4, "tincan_thread_data"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1532927
    move-object v2, v3

    .line 1532928
    new-instance v3, LX/Js2;

    invoke-direct {v3}, LX/Js2;-><init>()V

    move-object v3, v3

    .line 1532929
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1532930
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    goto :goto_0

    :cond_2
    iget-object v4, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1532931
    iget-object p0, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, p0

    .line 1532932
    check-cast v4, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v4}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v4

    iget-object p0, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v0, 0x2

    invoke-virtual {p0, v4, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method
