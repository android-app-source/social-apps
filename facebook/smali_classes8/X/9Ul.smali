.class public LX/9Ul;
.super LX/3Jj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Jj",
        "<",
        "LX/3Jc;",
        "Landroid/graphics/Matrix;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;[[[F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3Jc;",
            ">;[[[F)V"
        }
    .end annotation

    .prologue
    .line 1498508
    invoke-direct {p0, p1, p2}, LX/3Jj;-><init>(Ljava/util/List;[[[F)V

    .line 1498509
    return-void
.end method


# virtual methods
.method public final a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1498510
    check-cast p1, LX/3Jc;

    check-cast p2, LX/3Jc;

    check-cast p4, Landroid/graphics/Matrix;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1498511
    if-nez p2, :cond_0

    .line 1498512
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 1498513
    aget v0, v0, v2

    neg-float v0, v0

    .line 1498514
    iget-object v1, p1, LX/3Jc;->b:[F

    move-object v1, v1

    .line 1498515
    aget v1, v1, v3

    neg-float v1, v1

    invoke-virtual {p4, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1498516
    :goto_0
    return-void

    .line 1498517
    :cond_0
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 1498518
    aget v0, v0, v2

    .line 1498519
    iget-object v1, p2, LX/3Jc;->b:[F

    move-object v1, v1

    .line 1498520
    aget v1, v1, v2

    invoke-static {v0, v1, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    neg-float v0, v0

    .line 1498521
    iget-object v1, p1, LX/3Jc;->b:[F

    move-object v1, v1

    .line 1498522
    aget v1, v1, v3

    .line 1498523
    iget-object v2, p2, LX/3Jc;->b:[F

    move-object v2, v2

    .line 1498524
    aget v2, v2, v3

    invoke-static {v1, v2, p3}, LX/3Jj;->a(FFF)F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p4, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0
.end method
