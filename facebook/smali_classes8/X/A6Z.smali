.class public final LX/A6Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/A6x;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/A6e;


# direct methods
.method public constructor <init>(LX/A6e;)V
    .locals 0

    .prologue
    .line 1624422
    iput-object p1, p0, LX/A6Z;->a:LX/A6e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1624423
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1624424
    check-cast p1, LX/A6x;

    .line 1624425
    iget-object v0, p0, LX/A6Z;->a:LX/A6e;

    iget-object v0, v0, LX/A6e;->a:LX/A6N;

    sget-object v1, LX/A6N;->UNIGRAM:LX/A6N;

    if-ne v0, v1, :cond_1

    .line 1624426
    check-cast p1, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;

    .line 1624427
    iget-object v0, p0, LX/A6Z;->a:LX/A6e;

    iget v1, p1, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;->mVersion:I

    .line 1624428
    iput v1, v0, LX/A6e;->h:I

    .line 1624429
    iget-object v0, p0, LX/A6Z;->a:LX/A6e;

    iget-object v0, v0, LX/A6e;->i:LX/A6r;

    check-cast v0, LX/A6y;

    .line 1624430
    iput-object p1, v0, LX/A6y;->b:Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;

    .line 1624431
    iget-object v1, v0, LX/A6y;->b:Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;

    .line 1624432
    const/4 v2, 0x0

    .line 1624433
    iget-object v3, v1, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;->mModel:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v3, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1624434
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v3, v2

    .line 1624435
    goto :goto_0

    .line 1624436
    :cond_0
    move v1, v3

    .line 1624437
    iput v1, v0, LX/A6y;->a:I

    .line 1624438
    iget-object v0, p0, LX/A6Z;->a:LX/A6e;

    const/4 v1, 0x1

    .line 1624439
    iput-boolean v1, v0, LX/A6e;->b:Z

    .line 1624440
    :cond_1
    return-void
.end method
