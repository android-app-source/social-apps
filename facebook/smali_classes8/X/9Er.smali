.class public LX/9Er;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/17W;

.field private final c:LX/0wM;

.field public final d:LX/8yJ;

.field private final e:LX/1Uf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;LX/0wM;LX/8yJ;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1457538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457539
    iput-object p1, p0, LX/9Er;->a:Landroid/content/Context;

    .line 1457540
    iput-object p2, p0, LX/9Er;->b:LX/17W;

    .line 1457541
    iput-object p3, p0, LX/9Er;->c:LX/0wM;

    .line 1457542
    iput-object p4, p0, LX/9Er;->d:LX/8yJ;

    .line 1457543
    iput-object p5, p0, LX/9Er;->e:LX/1Uf;

    .line 1457544
    return-void
.end method

.method public static b(LX/0QB;)LX/9Er;
    .locals 6

    .prologue
    .line 1457545
    new-instance v0, LX/9Er;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {p0}, LX/8yJ;->b(LX/0QB;)LX/8yJ;

    move-result-object v4

    check-cast v4, LX/8yJ;

    invoke-static {p0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-direct/range {v0 .. v5}, LX/9Er;-><init>(Landroid/content/Context;LX/17W;LX/0wM;LX/8yJ;LX/1Uf;)V

    .line 1457546
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1457547
    new-instance v0, LX/9Eq;

    invoke-direct {v0, p0, p1, p2, p3}, LX/9Eq;-><init>(LX/9Er;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPage;I)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1457548
    new-instance v1, LX/3DI;

    invoke-direct {v1}, LX/3DI;-><init>()V

    .line 1457549
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->U()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1457550
    iget-object v0, p0, LX/9Er;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b004e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1457551
    iget-object v2, p0, LX/9Er;->c:LX/0wM;

    const v3, 0x7f0209e1

    const v4, -0xa76f01

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1457552
    invoke-virtual {v2, v5, v5, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1457553
    new-instance v0, LX/34T;

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1457554
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->U()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3DI;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1457555
    const-string v2, " "

    invoke-virtual {v1, v2}, LX/3DI;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1457556
    const-string v2, " "

    invoke-virtual {v1, v2}, LX/3DI;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1457557
    invoke-virtual {v1}, LX/3DI;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1}, LX/3DI;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v0, v2, v3, v4}, LX/3DI;->setSpan(Ljava/lang/Object;III)V

    .line 1457558
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 1457559
    iget-object v0, p0, LX/9Er;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->Z()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/8hv;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1457560
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1457561
    invoke-virtual {v1, v0}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 1457562
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->ab()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1457563
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->ab()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, LX/9Er;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00e4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1, v0, v2, v5}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 1457564
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1457565
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, LX/9Er;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00e4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1, v0, v2, v5}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 1457566
    :cond_3
    return-object v1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1457567
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v0

    .line 1457568
    if-nez v0, :cond_0

    .line 1457569
    const-string v0, ""

    .line 1457570
    :goto_0
    return-object v0

    .line 1457571
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1457572
    if-nez v3, :cond_1

    .line 1457573
    const-string v0, ""

    goto :goto_0

    .line 1457574
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1457575
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1457576
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1457577
    if-eqz v0, :cond_3

    .line 1457578
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    .line 1457579
    if-eqz v6, :cond_3

    .line 1457580
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1457581
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1457582
    const/4 v7, 0x1

    .line 1457583
    :goto_2
    move v6, v7

    .line 1457584
    if-nez v6, :cond_3

    .line 1457585
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1457586
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1457587
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1457588
    iget-object v0, p0, LX/9Er;->e:LX/1Uf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4, v2}, LX/1Uf;->a(LX/1eE;ZLX/0lF;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0

    .line 1457589
    :cond_5
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1457590
    iget-object v1, p0, LX/9Er;->a:Landroid/content/Context;

    const v2, 0x7f08124a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1457591
    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1457592
    const-string v1, ", "

    const/4 v8, 0x0

    .line 1457593
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1457594
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1457595
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    .line 1457596
    if-eqz v2, :cond_6

    .line 1457597
    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1457598
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v7, 0x1

    invoke-direct {v2, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v6}, Landroid/text/SpannableString;->length()I

    move-result v7

    invoke-virtual {v6, v2, v8, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1457599
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1457600
    :cond_7
    move-object v2, v3

    .line 1457601
    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/CharSequence;Ljava/util/Collection;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    :cond_8
    const/4 v7, 0x0

    goto :goto_2
.end method
