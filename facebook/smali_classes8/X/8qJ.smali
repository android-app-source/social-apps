.class public final LX/8qJ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;)V
    .locals 0

    .prologue
    .line 1406664
    iput-object p1, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1406665
    iget-object v0, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a$redex0(Lcom/facebook/ufiservices/flyout/EditCommentFragment;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1406666
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1406667
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1406668
    iget-object v0, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 1406669
    iget-object v0, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v1, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v1, v1, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->i:LX/20j;

    iget-object v2, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v2, v2, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1, v2, p1}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1406670
    iput-object v1, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406671
    :cond_0
    if-eqz p1, :cond_1

    .line 1406672
    iget-object v0, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->g:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/8qJ;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v2, v2, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1406673
    :cond_1
    return-void
.end method
