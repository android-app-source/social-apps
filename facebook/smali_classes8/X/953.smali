.class public final LX/953;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/contacts/upload/ContactsUploadStateListener;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/contacts/upload/ContactsUploadStateListener;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1436369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436370
    iput-object p1, p0, LX/953;->a:LX/0QB;

    .line 1436371
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1436372
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/953;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1436373
    packed-switch p2, :pswitch_data_0

    .line 1436374
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1436375
    :pswitch_0
    new-instance v0, LX/JuP;

    invoke-direct {v0}, LX/JuP;-><init>()V

    .line 1436376
    const/16 v1, 0x2b2d

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0xac0

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1436377
    iput-object v1, v0, LX/JuP;->a:LX/0Ot;

    iput-object p0, v0, LX/JuP;->b:LX/0Ot;

    .line 1436378
    move-object v0, v0

    .line 1436379
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1436380
    const/4 v0, 0x1

    return v0
.end method
