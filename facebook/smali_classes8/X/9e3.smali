.class public LX/9e3;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:I

.field public b:Landroid/graphics/Paint;

.field public c:Landroid/graphics/RectF;

.field public d:F

.field public e:F

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 1519106
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1519107
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/9e3;->d:F

    .line 1519108
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/9e3;->e:F

    .line 1519109
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9e3;->f:Z

    .line 1519110
    iput-boolean p2, p0, LX/9e3;->f:Z

    .line 1519111
    iget-boolean v0, p0, LX/9e3;->f:Z

    const/4 p2, 0x1

    .line 1519112
    if-eqz v0, :cond_0

    .line 1519113
    invoke-virtual {p0}, LX/9e3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a073c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/9e3;->a:I

    .line 1519114
    :goto_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, LX/9e3;->b:Landroid/graphics/Paint;

    .line 1519115
    if-eqz v0, :cond_1

    .line 1519116
    iget-object v1, p0, LX/9e3;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/9e3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a00d2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1519117
    invoke-virtual {p0}, LX/9e3;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 1519118
    iget-object v2, p0, LX/9e3;->b:Landroid/graphics/Paint;

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1519119
    :goto_1
    iget-object v1, p0, LX/9e3;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1519120
    const/4 v1, 0x0

    invoke-static {p0, p2, v1}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1519121
    return-void

    .line 1519122
    :cond_0
    invoke-virtual {p0}, LX/9e3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a073b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/9e3;->a:I

    goto :goto_0

    .line 1519123
    :cond_1
    iget-object v1, p0, LX/9e3;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/9e3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a0048

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1519124
    iget-object v1, p0, LX/9e3;->b:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_1
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1519125
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1519126
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1519127
    iget-object v0, p0, LX/9e3;->c:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    .line 1519128
    iget v0, p0, LX/9e3;->a:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1519129
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1519130
    iget-object v0, p0, LX/9e3;->c:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9e3;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1519131
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1519132
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 1519133
    if-eqz p1, :cond_0

    iget v0, p0, LX/9e3;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1519134
    sub-int v0, p5, p3

    .line 1519135
    sub-int v1, p4, p2

    .line 1519136
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, LX/9e3;->d:F

    mul-float/2addr v2, v3

    .line 1519137
    int-to-float v1, v1

    iget v3, p0, LX/9e3;->e:F

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    div-float/2addr v1, v4

    .line 1519138
    int-to-float v0, v0

    sub-float/2addr v0, v2

    div-float/2addr v0, v4

    .line 1519139
    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, LX/9e3;->e:F

    mul-float/2addr v4, v2

    add-float/2addr v4, v1

    add-float/2addr v2, v0

    invoke-direct {v3, v1, v0, v4, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v3, p0, LX/9e3;->c:Landroid/graphics/RectF;

    .line 1519140
    :cond_0
    return-void
.end method
