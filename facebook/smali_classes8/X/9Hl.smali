.class public final LX/9Hl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:LX/1Pr;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1Pr;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1461839
    iput-object p1, p0, LX/9Hl;->d:Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    iput-object p2, p0, LX/9Hl;->a:LX/1Pr;

    iput-object p3, p0, LX/9Hl;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p4, p0, LX/9Hl;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1461840
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1461841
    const v1, 0x7f0d3264

    if-ne v0, v1, :cond_1

    .line 1461842
    iget-object v0, p0, LX/9Hl;->a:LX/1Pr;

    check-cast v0, LX/9FA;

    iget-object v1, p0, LX/9Hl;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9Hl;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2}, LX/9FA;->c(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1461843
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1461844
    :cond_1
    const v1, 0x7f0d3265

    if-ne v0, v1, :cond_0

    .line 1461845
    iget-object v0, p0, LX/9Hl;->a:LX/1Pr;

    check-cast v0, LX/9FA;

    iget-object v1, p0, LX/9Hl;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9Hl;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2}, LX/9FA;->d(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method
