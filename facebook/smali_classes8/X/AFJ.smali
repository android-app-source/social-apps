.class public LX/AFJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/AFJ;


# instance fields
.field public final a:LX/AFD;

.field public final b:LX/0SG;

.field public c:LX/AF4;


# direct methods
.method public constructor <init>(LX/AFE;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647576
    const-string v0, "snacks_inbox_replay_session_cache_table"

    invoke-virtual {p1, v0}, LX/AFE;->a(Ljava/lang/String;)LX/AFD;

    move-result-object v0

    iput-object v0, p0, LX/AFJ;->a:LX/AFD;

    .line 1647577
    iput-object p2, p0, LX/AFJ;->b:LX/0SG;

    .line 1647578
    return-void
.end method

.method public static a(LX/0QB;)LX/AFJ;
    .locals 5

    .prologue
    .line 1647558
    sget-object v0, LX/AFJ;->d:LX/AFJ;

    if-nez v0, :cond_1

    .line 1647559
    const-class v1, LX/AFJ;

    monitor-enter v1

    .line 1647560
    :try_start_0
    sget-object v0, LX/AFJ;->d:LX/AFJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1647561
    if-eqz v2, :cond_0

    .line 1647562
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1647563
    new-instance p0, LX/AFJ;

    const-class v3, LX/AFE;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/AFE;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/AFJ;-><init>(LX/AFE;LX/0SG;)V

    .line 1647564
    move-object v0, p0

    .line 1647565
    sput-object v0, LX/AFJ;->d:LX/AFJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1647566
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1647567
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1647568
    :cond_1
    sget-object v0, LX/AFJ;->d:LX/AFJ;

    return-object v0

    .line 1647569
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1647570
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/audience/direct/model/CachedReplayedModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/audience/direct/model/ReplayableSession;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1647579
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1647580
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1647581
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcel;

    .line 1647582
    sget-object p1, Lcom/facebook/audience/direct/model/ReplayableSession;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {p1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/audience/direct/model/ReplayableSession;

    move-object v0, p1

    .line 1647583
    invoke-interface {p3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1647584
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcel;

    .line 1647585
    sget-object p1, Lcom/facebook/audience/direct/model/CachedReplayedModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {p1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/audience/direct/model/CachedReplayedModel;

    move-object v0, p1

    .line 1647586
    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1647587
    :cond_1
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/util/Map;Lcom/facebook/audience/direct/model/ReplayableSession;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/audience/direct/model/CachedReplayedModel;",
            ">;",
            "Lcom/facebook/audience/direct/model/ReplayableSession;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1647571
    invoke-interface {p1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/model/CachedReplayedModel;

    .line 1647572
    iget-wide v4, v0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->a:J

    move-wide v0, v4

    .line 1647573
    iget-wide v4, p2, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    move-wide v2, v4

    .line 1647574
    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/audience/model/AudienceControlData;LX/0Px;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/audience/model/AudienceControlData;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1647548
    iget-object v0, p0, LX/AFJ;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1647549
    iget-object v2, p0, LX/AFJ;->a:LX/AFD;

    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    .line 1647550
    new-instance v4, LX/AFY;

    invoke-direct {v4}, LX/AFY;-><init>()V

    move-object v4, v4

    .line 1647551
    iput-wide v0, v4, LX/AFY;->a:J

    .line 1647552
    move-object v4, v4

    .line 1647553
    iput-object p2, v4, LX/AFY;->b:LX/0Px;

    .line 1647554
    move-object v4, v4

    .line 1647555
    new-instance v5, Lcom/facebook/audience/direct/model/ReplayableSession;

    invoke-direct {v5, v4}, Lcom/facebook/audience/direct/model/ReplayableSession;-><init>(LX/AFY;)V

    move-object v4, v5

    .line 1647556
    invoke-virtual {v2, v3, v4}, LX/AFD;->a(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1647557
    return-wide v0
.end method
