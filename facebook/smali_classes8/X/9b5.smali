.class public final LX/9b5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/FbEditText;

.field public final synthetic b:LX/2EJ;

.field public final synthetic c:LX/9b8;


# direct methods
.method public constructor <init>(LX/9b8;Lcom/facebook/resources/ui/FbEditText;LX/2EJ;)V
    .locals 0

    .prologue
    .line 1514267
    iput-object p1, p0, LX/9b5;->c:LX/9b8;

    iput-object p2, p0, LX/9b5;->a:Lcom/facebook/resources/ui/FbEditText;

    iput-object p3, p0, LX/9b5;->b:LX/2EJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x2

    const/4 v5, 0x1

    const v0, -0x5f85a7c

    invoke-static {v3, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1514268
    iget-object v1, p0, LX/9b5;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1514269
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1514270
    iget-object v1, p0, LX/9b5;->c:LX/9b8;

    invoke-virtual {v1}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0811e3

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1514271
    const v1, 0x625e159e

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1514272
    :goto_0
    return-void

    .line 1514273
    :cond_0
    iget-object v2, p0, LX/9b5;->c:LX/9b8;

    iget-object v2, v2, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1514274
    iget-object v1, p0, LX/9b5;->b:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->dismiss()V

    .line 1514275
    const v1, -0x67d76415

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1514276
    :cond_1
    iget-object v2, p0, LX/9b5;->c:LX/9b8;

    new-instance v3, LX/4BY;

    iget-object v4, p0, LX/9b5;->c:LX/9b8;

    invoke-virtual {v4}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v3, v2, LX/9b8;->c:LX/4BY;

    .line 1514277
    iget-object v2, p0, LX/9b5;->c:LX/9b8;

    iget-object v2, v2, LX/9b8;->c:LX/4BY;

    .line 1514278
    iput v6, v2, LX/4BY;->d:I

    .line 1514279
    iget-object v2, p0, LX/9b5;->c:LX/9b8;

    iget-object v2, v2, LX/9b8;->c:LX/4BY;

    iget-object v3, p0, LX/9b5;->c:LX/9b8;

    invoke-virtual {v3}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0811e2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1514280
    iget-object v2, p0, LX/9b5;->c:LX/9b8;

    iget-object v2, v2, LX/9b8;->c:LX/4BY;

    invoke-virtual {v2, v5}, LX/4BY;->a(Z)V

    .line 1514281
    iget-object v2, p0, LX/9b5;->c:LX/9b8;

    iget-object v2, v2, LX/9b8;->c:LX/4BY;

    invoke-virtual {v2, v6}, LX/4BY;->setCancelable(Z)V

    .line 1514282
    iget-object v2, p0, LX/9b5;->c:LX/9b8;

    iget-object v2, v2, LX/9b8;->c:LX/4BY;

    invoke-virtual {v2}, LX/4BY;->show()V

    .line 1514283
    new-instance v2, LX/9b4;

    invoke-direct {v2, p0, v1}, LX/9b4;-><init>(LX/9b5;Ljava/lang/String;)V

    .line 1514284
    iget-object v3, p0, LX/9b5;->c:LX/9b8;

    iget-object v3, v3, LX/9b8;->g:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "tasks-renamePhotoAlbum:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/9b5;->c:LX/9b8;

    iget-object v5, v5, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/9b5;->c:LX/9b8;

    iget-object v5, v5, LX/9b8;->f:LX/9fy;

    iget-object v6, p0, LX/9b5;->c:LX/9b8;

    iget-object v6, v6, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v6

    const/4 v10, 0x0

    .line 1514285
    move-object v7, v5

    move-object v8, v6

    move-object v9, v1

    move-object v11, v10

    move-object v12, v10

    move-object v13, v10

    invoke-static/range {v7 .. v13}, LX/9fy;->a(LX/9fy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v1, v7

    .line 1514286
    invoke-virtual {v3, v4, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1514287
    iget-object v1, p0, LX/9b5;->b:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->dismiss()V

    .line 1514288
    const v1, -0x2fca218

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
