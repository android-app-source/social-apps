.class public final LX/AKd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AK1;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/AKe;


# direct methods
.method public constructor <init>(LX/AKe;LX/AK1;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1663997
    iput-object p1, p0, LX/AKd;->c:LX/AKe;

    iput-object p2, p0, LX/AKd;->a:LX/AK1;

    iput-object p3, p0, LX/AKd;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1663998
    sget-object v0, LX/AKe;->a:Ljava/lang/String;

    const-string v1, "failed to get direct share threads"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1663999
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664000
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1664001
    iget-object v0, p0, LX/AKd;->a:LX/AK1;

    if-nez v0, :cond_0

    .line 1664002
    :goto_0
    return-void

    .line 1664003
    :cond_0
    if-eqz p1, :cond_1

    .line 1664004
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1664005
    if-nez v0, :cond_2

    .line 1664006
    :cond_1
    goto :goto_0

    .line 1664007
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1664008
    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;

    .line 1664009
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1664010
    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel$EdgesModel;

    .line 1664011
    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, LX/AKd;->c:LX/AKe;

    iget-object v1, v1, LX/AKe;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1664012
    iget-object p1, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p1

    .line 1664013
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1664014
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$FBSnacksStoryDetailsModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1664015
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1664016
    :cond_4
    iget-object v0, p0, LX/AKd;->a:LX/AK1;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/AKd;->b:Ljava/lang/String;

    .line 1664017
    iget-object v3, v0, LX/AK1;->a:LX/AK2;

    .line 1664018
    iget-object v4, v3, LX/AK2;->c:LX/7gh;

    invoke-virtual {v4}, LX/7gh;->a()Ljava/util/List;

    move-result-object v4

    .line 1664019
    iget-object v5, v3, LX/AK2;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/audience/snacks/data/SnacksStoryDetailProvider$2;

    invoke-direct {v0, v3, v4, v2, v1}, Lcom/facebook/audience/snacks/data/SnacksStoryDetailProvider$2;-><init>(LX/AK2;Ljava/util/List;Ljava/lang/String;LX/0Px;)V

    const v4, -0x15456f83

    invoke-static {v5, v0, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1664020
    goto/16 :goto_0
.end method
