.class public LX/9d9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8GH;


# instance fields
.field public a:LX/9dM;

.field private final b:LX/9d0;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/9dM;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(LX/9d0;)V
    .locals 0
    .param p1    # LX/9d0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1517635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1517636
    iput-object p1, p0, LX/9d9;->b:LX/9d0;

    .line 1517637
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517638
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;ZI)V
    .locals 2
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517639
    iget-object v0, p0, LX/9d9;->a:LX/9dM;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9d9;->a:LX/9dM;

    invoke-interface {v0}, LX/9dM;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517640
    iget-object v0, p0, LX/9d9;->b:LX/9d0;

    iget-object v1, p0, LX/9d9;->a:LX/9dM;

    invoke-interface {v1}, LX/9dM;->d()LX/9dK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9d0;->a(LX/9dK;)V

    .line 1517641
    iget-object v0, p0, LX/9d9;->a:LX/9dM;

    invoke-interface {v0}, LX/9dM;->b()V

    .line 1517642
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1517643
    if-nez p1, :cond_1

    .line 1517644
    :cond_0
    :goto_0
    return-void

    .line 1517645
    :cond_1
    iget-object v0, p0, LX/9d9;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/9d9;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9dM;

    .line 1517646
    iget-boolean v3, p0, LX/9d9;->e:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, LX/9d9;->d:Z

    if-nez v3, :cond_2

    invoke-interface {v0}, LX/9dM;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1517647
    iput-object v0, p0, LX/9d9;->a:LX/9dM;

    .line 1517648
    iget-object v1, p0, LX/9d9;->b:LX/9d0;

    invoke-interface {v0}, LX/9dM;->d()LX/9dK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9d0;->a(LX/9dK;)V

    .line 1517649
    invoke-interface {v0}, LX/9dM;->b()V

    .line 1517650
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9d9;->e:Z

    goto :goto_0

    .line 1517651
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517652
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9d9;->d:Z

    .line 1517653
    return-void
.end method

.method public final c(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517654
    return-void
.end method
