.class public final LX/A9c;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1635215
    const/4 v12, 0x0

    .line 1635216
    const/4 v11, 0x0

    .line 1635217
    const/4 v10, 0x0

    .line 1635218
    const/4 v9, 0x0

    .line 1635219
    const/4 v8, 0x0

    .line 1635220
    const/4 v7, 0x0

    .line 1635221
    const/4 v6, 0x0

    .line 1635222
    const/4 v5, 0x0

    .line 1635223
    const/4 v4, 0x0

    .line 1635224
    const/4 v3, 0x0

    .line 1635225
    const/4 v2, 0x0

    .line 1635226
    const/4 v1, 0x0

    .line 1635227
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1635228
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1635229
    const/4 v1, 0x0

    .line 1635230
    :goto_0
    return v1

    .line 1635231
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1635232
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 1635233
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1635234
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1635235
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1635236
    const-string v14, "associated_ad_campaigns"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1635237
    invoke-static/range {p0 .. p1}, LX/A9b;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1635238
    :cond_2
    const-string v14, "audience_code"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1635239
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    goto :goto_1

    .line 1635240
    :cond_3
    const-string v14, "audience_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1635241
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1635242
    :cond_4
    const-string v14, "can_mobile_client_edit"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1635243
    const/4 v2, 0x1

    .line 1635244
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1635245
    :cond_5
    const-string v14, "display_labels"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1635246
    invoke-static/range {p0 .. p1}, LX/A9a;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1635247
    :cond_6
    const-string v14, "display_name"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1635248
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1635249
    :cond_7
    const-string v14, "editable_fields"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1635250
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1635251
    :cond_8
    const-string v14, "is_last_used"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1635252
    const/4 v1, 0x1

    .line 1635253
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v5

    goto/16 :goto_1

    .line 1635254
    :cond_9
    const-string v14, "targeting_sentences"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1635255
    invoke-static/range {p0 .. p1}, LX/AAX;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1635256
    :cond_a
    const-string v14, "targeting_spec"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1635257
    invoke-static/range {p0 .. p1}, LX/AAU;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1635258
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1635259
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1635260
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1635261
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1635262
    if-eqz v2, :cond_c

    .line 1635263
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 1635264
    :cond_c
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1635265
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1635266
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1635267
    if-eqz v1, :cond_d

    .line 1635268
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 1635269
    :cond_d
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1635270
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1635271
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x1

    .line 1635272
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635273
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635274
    if-eqz v0, :cond_5

    .line 1635275
    const-string v1, "associated_ad_campaigns"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635276
    const/4 v1, 0x0

    .line 1635277
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635278
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1635279
    if-eqz v1, :cond_0

    .line 1635280
    const-string v4, "count"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635281
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1635282
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1635283
    if-eqz v1, :cond_4

    .line 1635284
    const-string v4, "edges"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635285
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1635286
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 1635287
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1635288
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635289
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1635290
    if-eqz v6, :cond_2

    .line 1635291
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635292
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635293
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1635294
    if-eqz v0, :cond_1

    .line 1635295
    const-string v5, "name"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635296
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635297
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635298
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635299
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1635300
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1635301
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635302
    :cond_5
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1635303
    if-eqz v0, :cond_6

    .line 1635304
    const-string v0, "audience_code"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635305
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635306
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1635307
    if-eqz v0, :cond_7

    .line 1635308
    const-string v1, "audience_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635309
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635310
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1635311
    if-eqz v0, :cond_8

    .line 1635312
    const-string v1, "can_mobile_client_edit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635313
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1635314
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635315
    if-eqz v0, :cond_9

    .line 1635316
    const-string v1, "display_labels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635317
    invoke-static {p0, v0, p2, p3}, LX/A9a;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635318
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1635319
    if-eqz v0, :cond_a

    .line 1635320
    const-string v1, "display_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635321
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635322
    :cond_a
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1635323
    if-eqz v0, :cond_b

    .line 1635324
    const-string v0, "editable_fields"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635325
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1635326
    :cond_b
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1635327
    if-eqz v0, :cond_c

    .line 1635328
    const-string v1, "is_last_used"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635329
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1635330
    :cond_c
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635331
    if-eqz v0, :cond_d

    .line 1635332
    const-string v1, "targeting_sentences"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635333
    invoke-static {p0, v0, p2, p3}, LX/AAX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635334
    :cond_d
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635335
    if-eqz v0, :cond_e

    .line 1635336
    const-string v1, "targeting_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635337
    invoke-static {p0, v0, p2, p3}, LX/AAU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635338
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635339
    return-void
.end method
