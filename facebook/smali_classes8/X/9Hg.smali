.class public LX/9Hg;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasCommentActions;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasLoggingParams;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasInlineReplyActions;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9Hg",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1461720
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1461721
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/9Hg;->b:LX/0Zi;

    .line 1461722
    iput-object p1, p0, LX/9Hg;->a:LX/0Ot;

    .line 1461723
    return-void
.end method

.method public static a(LX/1De;Landroid/graphics/drawable/Drawable;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/graphics/drawable/Drawable;",
            ")",
            "LX/1dQ",
            "<",
            "LX/48J;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461719
    const v0, 0x16f9bc30

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/9Hg;
    .locals 4

    .prologue
    .line 1461708
    const-class v1, LX/9Hg;

    monitor-enter v1

    .line 1461709
    :try_start_0
    sget-object v0, LX/9Hg;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461710
    sput-object v2, LX/9Hg;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461711
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461712
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461713
    new-instance v3, LX/9Hg;

    const/16 p0, 0x1dd8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9Hg;-><init>(LX/0Ot;)V

    .line 1461714
    move-object v0, v3

    .line 1461715
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461716
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Hg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461717
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/48C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461707
    const v0, 0x38c14d73

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461658
    const v0, 0x393ef995

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461706
    const v0, 0x393ef995

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1461703
    check-cast p2, LX/9Hf;

    .line 1461704
    iget-object v0, p0, LX/9Hg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    iget-object v2, p2, LX/9Hf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/9Hf;->b:LX/9FD;

    iget-object v4, p2, LX/9Hf;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v5, p2, LX/9Hf;->d:LX/1Pr;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FD;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1Pr;)LX/1Dg;

    move-result-object v0

    .line 1461705
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1461659
    invoke-static {}, LX/1dS;->b()V

    .line 1461660
    iget v0, p1, LX/1dQ;->b:I

    .line 1461661
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1461662
    :goto_0
    return-object v0

    .line 1461663
    :sswitch_0
    check-cast p2, LX/48J;

    .line 1461664
    iget-object v2, p2, LX/48J;->b:Landroid/view/MotionEvent;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 1461665
    iget-object p1, p0, LX/9Hg;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1461666
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    .line 1461667
    :goto_1
    :pswitch_0
    const/4 p1, 0x0

    move p1, p1

    .line 1461668
    move v0, p1

    .line 1461669
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1461670
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1461671
    iget-object v2, p0, LX/9Hg;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    .line 1461672
    const/4 p0, 0x1

    move v2, p0

    .line 1461673
    move v0, v2

    .line 1461674
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1461675
    :sswitch_2
    check-cast p2, LX/48C;

    .line 1461676
    iget-object v0, p2, LX/48C;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1461677
    check-cast v1, LX/9Hf;

    .line 1461678
    iget-object v2, p0, LX/9Hg;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    iget-object v3, v1, LX/9Hf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/9Hf;->d:LX/1Pr;

    invoke-virtual {v2, v0, v3, p1}, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)Z

    move-result v2

    .line 1461679
    move v0, v2

    .line 1461680
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1461681
    :sswitch_3
    check-cast p2, LX/3Ae;

    .line 1461682
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1461683
    iget-object p1, p0, LX/9Hg;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    const/4 v3, 0x0

    .line 1461684
    iget-object p2, p1, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;->k:LX/8qo;

    invoke-static {v0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;

    move-result-object p0

    invoke-interface {p2, v2, p0, v3, v3}, LX/8qo;->a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;)V

    .line 1461685
    move-object v0, v1

    .line 1461686
    goto :goto_0

    .line 1461687
    :sswitch_4
    check-cast p2, LX/3Ae;

    .line 1461688
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1461689
    check-cast v2, LX/9Hf;

    .line 1461690
    iget-object v3, p0, LX/9Hg;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;

    iget-object v4, v2, LX/9Hf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v2, LX/9Hf;->d:LX/1Pr;

    .line 1461691
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1461692
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1461693
    invoke-static {v4}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p2

    .line 1461694
    new-instance p0, LX/6WS;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1461695
    const v2, 0x7f110035

    invoke-virtual {p0, v2}, LX/5OM;->b(I)V

    .line 1461696
    new-instance v2, LX/9Hl;

    invoke-direct {v2, v3, v5, p1, p2}, LX/9Hl;-><init>(Lcom/facebook/feedback/ui/rows/views/specs/CommentComponentSpec;LX/1Pr;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1461697
    iput-object v2, p0, LX/5OM;->p:LX/5OO;

    .line 1461698
    invoke-virtual {p0, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1461699
    move-object v0, v1

    .line 1461700
    goto/16 :goto_0

    .line 1461701
    :pswitch_1
    invoke-static {v0}, LX/9CG;->a(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 1461702
    :pswitch_2
    invoke-static {v0}, LX/9CG;->b(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4f67f4ec -> :sswitch_4
        -0x455704a9 -> :sswitch_3
        0x16f9bc30 -> :sswitch_0
        0x38c14d73 -> :sswitch_2
        0x393ef995 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
