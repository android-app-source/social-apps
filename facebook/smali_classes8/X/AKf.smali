.class public final LX/AKf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AJy;

.field public final synthetic b:LX/AKg;


# direct methods
.method public constructor <init>(LX/AKg;LX/AJy;)V
    .locals 0

    .prologue
    .line 1664027
    iput-object p1, p0, LX/AKf;->b:LX/AKg;

    iput-object p2, p0, LX/AKf;->a:LX/AJy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1664028
    sget-object v0, LX/AKg;->a:Ljava/lang/String;

    const-string v1, "failed to get direct share threads"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1664029
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1664030
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1664031
    iget-object v0, p0, LX/AKf;->a:LX/AJy;

    if-nez v0, :cond_0

    .line 1664032
    :goto_0
    return-void

    .line 1664033
    :cond_0
    if-nez p1, :cond_1

    .line 1664034
    goto :goto_0

    .line 1664035
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1664036
    check-cast v0, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;

    .line 1664037
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v1

    .line 1664038
    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->j()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;

    move-result-object v2

    const/4 v5, 0x0

    .line 1664039
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1664040
    if-eqz v2, :cond_4

    .line 1664041
    invoke-virtual {v2}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    :goto_1
    if-ge v6, v9, :cond_4

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel$EdgesModel;

    .line 1664042
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->l()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v10

    .line 1664043
    invoke-virtual {v3}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$FriendsStoriesBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v4, v5

    :goto_2
    if-ge v4, v12, :cond_3

    invoke-virtual {v11, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    .line 1664044
    new-instance p1, LX/AKP;

    invoke-virtual {v3}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v3

    invoke-direct {p1, v3, v1, v10}, LX/AKP;-><init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V

    .line 1664045
    invoke-virtual {p1}, LX/AKP;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v3

    .line 1664046
    if-eqz v3, :cond_2

    .line 1664047
    invoke-virtual {p1}, LX/AKP;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1664048
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1664049
    :cond_3
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto/16 :goto_1

    .line 1664050
    :cond_4
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1664051
    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel;->o()Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;

    move-result-object v0

    const/4 v5, 0x0

    .line 1664052
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1664053
    if-eqz v0, :cond_7

    .line 1664054
    invoke-virtual {v0}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    :goto_3
    if-ge v6, v9, :cond_7

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$EdgesModel;

    .line 1664055
    invoke-virtual {v3}, Lcom/facebook/audience/snacks/protocol/SnacksBroadcastQueryModels$SnacksBroadcastQueryModel$StoryBucketModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v4, v5

    :goto_4
    if-ge v4, v11, :cond_6

    invoke-virtual {v10, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    .line 1664056
    new-instance v12, LX/AKP;

    invoke-virtual {v3}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v3

    invoke-direct {v12, v3, v1, v1}, LX/AKP;-><init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V

    .line 1664057
    invoke-virtual {v12}, LX/AKP;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v3

    .line 1664058
    if-eqz v3, :cond_5

    .line 1664059
    invoke-virtual {v12}, LX/AKP;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1664060
    :cond_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    .line 1664061
    :cond_6
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_3

    .line 1664062
    :cond_7
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 1664063
    iget-object v1, p0, LX/AKf;->a:LX/AJy;

    invoke-virtual {v1, v2, v0}, LX/AJy;->a(LX/0Px;LX/0Px;)V

    goto/16 :goto_0
.end method
