.class public LX/8lE;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1397641
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1397642
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/view/LayoutInflater;)Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;
    .locals 11

    .prologue
    .line 1397643
    new-instance v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v2

    check-cast v2, LX/0Sg;

    invoke-static {p0}, LX/8jL;->b(LX/0QB;)LX/8jL;

    move-result-object v3

    check-cast v3, LX/8jL;

    invoke-static {p0}, LX/8kd;->b(LX/0QB;)LX/8kd;

    move-result-object v4

    check-cast v4, LX/8kd;

    invoke-static {p0}, LX/8j6;->a(LX/0QB;)LX/8j6;

    move-result-object v5

    check-cast v5, LX/8j6;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v7, LX/8l4;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/8l4;

    invoke-static {p0}, LX/2V6;->b(LX/0QB;)LX/2V6;

    move-result-object v10

    check-cast v10, LX/2V6;

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v10}, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;-><init>(Landroid/content/res/Resources;LX/0Sg;LX/8jL;LX/8kd;LX/8j6;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/8l4;Landroid/content/Context;Landroid/view/LayoutInflater;LX/2V6;)V

    .line 1397644
    return-object v0
.end method
