.class public LX/8yJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/0Zb;

.field public c:LX/0kv;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Zb;LX/0kv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425215
    iput-object p1, p0, LX/8yJ;->a:Landroid/content/Context;

    .line 1425216
    iput-object p2, p0, LX/8yJ;->b:LX/0Zb;

    .line 1425217
    iput-object p3, p0, LX/8yJ;->c:LX/0kv;

    .line 1425218
    return-void
.end method

.method public static b(LX/0QB;)LX/8yJ;
    .locals 4

    .prologue
    .line 1425219
    new-instance v3, LX/8yJ;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v2

    check-cast v2, LX/0kv;

    invoke-direct {v3, v0, v1, v2}, LX/8yJ;-><init>(Landroid/content/Context;LX/0Zb;LX/0kv;)V

    .line 1425220
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1425221
    iget-object v0, p0, LX/8yJ;->b:LX/0Zb;

    const-string v1, "social_search_full_map_marker_click"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1425222
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1425223
    const-string v0, "social_search"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1425224
    iget-object v0, p0, LX/8yJ;->c:LX/0kv;

    iget-object v2, p0, LX/8yJ;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1425225
    invoke-virtual {v1, p1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 1425226
    const-string v0, "story_graphql_id"

    invoke-virtual {v1, v0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1425227
    const-string v0, "place_id"

    invoke-virtual {v1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1425228
    const-string v2, "entrypoint"

    if-eqz p3, :cond_1

    const-string v0, "full_map_hscroll"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1425229
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1425230
    :cond_0
    return-void

    .line 1425231
    :cond_1
    const-string v0, "full_map_annotation"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1425232
    iget-object v0, p0, LX/8yJ;->b:LX/0Zb;

    const-string v1, "social_search_full_map_toggle_click"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1425233
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1425234
    const-string v0, "social_search"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1425235
    iget-object v0, p0, LX/8yJ;->c:LX/0kv;

    iget-object v2, p0, LX/8yJ;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1425236
    invoke-virtual {v1, p1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 1425237
    const-string v0, "story_graphql_id"

    invoke-virtual {v1, v0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1425238
    const-string v2, "entrypoint"

    if-eqz p2, :cond_1

    const-string v0, "full_map_listview"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1425239
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1425240
    :cond_0
    return-void

    .line 1425241
    :cond_1
    const-string v0, "full_map_mapview"

    goto :goto_0
.end method
