.class public LX/98p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1446321
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 13

    .prologue
    .line 1446322
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1446323
    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1446324
    const-string v2, "route_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1446325
    const-string v3, "module_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1446326
    const-string v4, "init_props"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 1446327
    const-string v5, "title_res"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 1446328
    const-string v6, "show_search"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 1446329
    const-string v7, "button_icon_res"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 1446330
    const-string v8, "button_res"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 1446331
    const-string v9, "button_text"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1446332
    const-string v10, "button_event"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1446333
    const-string v11, "requested_orientation"

    const/4 v12, -0x1

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1446334
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v11

    .line 1446335
    iput v5, v11, LX/98r;->d:I

    .line 1446336
    move-object v5, v11

    .line 1446337
    iput-boolean v6, v5, LX/98r;->e:Z

    .line 1446338
    move-object v5, v5

    .line 1446339
    iput v0, v5, LX/98r;->h:I

    .line 1446340
    move-object v0, v5

    .line 1446341
    iput-object v4, v0, LX/98r;->f:Landroid/os/Bundle;

    .line 1446342
    move-object v0, v0

    .line 1446343
    iput-object v10, v0, LX/98r;->i:Ljava/lang/String;

    .line 1446344
    move-object v0, v0

    .line 1446345
    iput v7, v0, LX/98r;->j:I

    .line 1446346
    move-object v0, v0

    .line 1446347
    iput v8, v0, LX/98r;->k:I

    .line 1446348
    move-object v0, v0

    .line 1446349
    iput-object v9, v0, LX/98r;->l:Ljava/lang/String;

    .line 1446350
    move-object v0, v0

    .line 1446351
    if-eqz v1, :cond_0

    .line 1446352
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 1446353
    move-object v1, v0

    .line 1446354
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 1446355
    :goto_0
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1446356
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    return-object v0

    .line 1446357
    :cond_0
    iput-object v3, v0, LX/98r;->c:Ljava/lang/String;

    .line 1446358
    goto :goto_0
.end method
