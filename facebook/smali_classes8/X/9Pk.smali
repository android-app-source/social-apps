.class public final LX/9Pk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1485674
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1485675
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1485676
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1485677
    invoke-static {p0, p1}, LX/9Pk;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1485678
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1485679
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1485668
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1485669
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1485670
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/9Pk;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1485671
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1485672
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1485673
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1485631
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_b

    .line 1485632
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485633
    :goto_0
    return v1

    .line 1485634
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485635
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1485636
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1485637
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485638
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1485639
    const-string v11, "cover_photo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1485640
    invoke-static {p0, p1}, LX/9Pf;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1485641
    :cond_2
    const-string v11, "group_friend_members"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1485642
    invoke-static {p0, p1}, LX/9Ph;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1485643
    :cond_3
    const-string v11, "group_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1485644
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1485645
    :cond_4
    const-string v11, "group_members"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1485646
    invoke-static {p0, p1}, LX/9Pi;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1485647
    :cond_5
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1485648
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1485649
    :cond_6
    const-string v11, "parent_group"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1485650
    invoke-static {p0, p1}, LX/9Pj;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1485651
    :cond_7
    const-string v11, "url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1485652
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1485653
    :cond_8
    const-string v11, "viewer_join_state"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1485654
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto/16 :goto_1

    .line 1485655
    :cond_9
    const-string v11, "visibility"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1485656
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto/16 :goto_1

    .line 1485657
    :cond_a
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1485658
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1485659
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1485660
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1485661
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1485662
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1485663
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1485664
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1485665
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1485666
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1485667
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x7

    .line 1485571
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485572
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485573
    if-eqz v0, :cond_3

    .line 1485574
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485575
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485576
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1485577
    if-eqz v1, :cond_2

    .line 1485578
    const-string v4, "photo"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485579
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485580
    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1485581
    if-eqz v4, :cond_1

    .line 1485582
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485583
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485584
    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1485585
    if-eqz v0, :cond_0

    .line 1485586
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485587
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485588
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485589
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485590
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485591
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485592
    if-eqz v0, :cond_4

    .line 1485593
    const-string v1, "group_friend_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485594
    invoke-static {p0, v0, p2, p3}, LX/9Ph;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1485595
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1485596
    if-eqz v0, :cond_5

    .line 1485597
    const-string v1, "group_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485598
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485599
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485600
    if-eqz v0, :cond_7

    .line 1485601
    const-string v1, "group_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485602
    const/4 v1, 0x0

    .line 1485603
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485604
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1485605
    if-eqz v1, :cond_6

    .line 1485606
    const-string v4, "count"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485607
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1485608
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485609
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1485610
    if-eqz v0, :cond_8

    .line 1485611
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485612
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485613
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485614
    if-eqz v0, :cond_9

    .line 1485615
    const-string v1, "parent_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485616
    invoke-static {p0, v0, p2}, LX/9Pj;->a(LX/15i;ILX/0nX;)V

    .line 1485617
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1485618
    if-eqz v0, :cond_a

    .line 1485619
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485620
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485621
    :cond_a
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1485622
    if-eqz v0, :cond_b

    .line 1485623
    const-string v0, "viewer_join_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485624
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485625
    :cond_b
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1485626
    if-eqz v0, :cond_c

    .line 1485627
    const-string v0, "visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485628
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485629
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485630
    return-void
.end method
