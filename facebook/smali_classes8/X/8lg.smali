.class public final enum LX/8lg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8lg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8lg;

.field public static final enum ERROR:LX/8lg;

.field public static final enum SEARCH_FINISHED_NO_RESULTS:LX/8lg;

.field public static final enum SEARCH_FINISHED_WITH_RESULTS:LX/8lg;

.field public static final enum TAG_RESULTS_SHOWN:LX/8lg;

.field public static final enum TAG_SELECTION:LX/8lg;

.field public static final enum TYPE_STARTED:LX/8lg;

.field public static final enum UNINITIALIZED:LX/8lg;

.field public static final enum WAIT_FOR_SEARCH_RESULTS:LX/8lg;

.field public static final enum WAIT_FOR_TAGGED_STICKERS:LX/8lg;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1397913
    new-instance v0, LX/8lg;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v3}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->UNINITIALIZED:LX/8lg;

    .line 1397914
    new-instance v0, LX/8lg;

    const-string v1, "TAG_SELECTION"

    invoke-direct {v0, v1, v4}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->TAG_SELECTION:LX/8lg;

    .line 1397915
    new-instance v0, LX/8lg;

    const-string v1, "WAIT_FOR_TAGGED_STICKERS"

    invoke-direct {v0, v1, v5}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->WAIT_FOR_TAGGED_STICKERS:LX/8lg;

    .line 1397916
    new-instance v0, LX/8lg;

    const-string v1, "TAG_RESULTS_SHOWN"

    invoke-direct {v0, v1, v6}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->TAG_RESULTS_SHOWN:LX/8lg;

    .line 1397917
    new-instance v0, LX/8lg;

    const-string v1, "TYPE_STARTED"

    invoke-direct {v0, v1, v7}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->TYPE_STARTED:LX/8lg;

    .line 1397918
    new-instance v0, LX/8lg;

    const-string v1, "WAIT_FOR_SEARCH_RESULTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->WAIT_FOR_SEARCH_RESULTS:LX/8lg;

    .line 1397919
    new-instance v0, LX/8lg;

    const-string v1, "SEARCH_FINISHED_WITH_RESULTS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->SEARCH_FINISHED_WITH_RESULTS:LX/8lg;

    .line 1397920
    new-instance v0, LX/8lg;

    const-string v1, "SEARCH_FINISHED_NO_RESULTS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->SEARCH_FINISHED_NO_RESULTS:LX/8lg;

    .line 1397921
    new-instance v0, LX/8lg;

    const-string v1, "ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8lg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lg;->ERROR:LX/8lg;

    .line 1397922
    const/16 v0, 0x9

    new-array v0, v0, [LX/8lg;

    sget-object v1, LX/8lg;->UNINITIALIZED:LX/8lg;

    aput-object v1, v0, v3

    sget-object v1, LX/8lg;->TAG_SELECTION:LX/8lg;

    aput-object v1, v0, v4

    sget-object v1, LX/8lg;->WAIT_FOR_TAGGED_STICKERS:LX/8lg;

    aput-object v1, v0, v5

    sget-object v1, LX/8lg;->TAG_RESULTS_SHOWN:LX/8lg;

    aput-object v1, v0, v6

    sget-object v1, LX/8lg;->TYPE_STARTED:LX/8lg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8lg;->WAIT_FOR_SEARCH_RESULTS:LX/8lg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8lg;->SEARCH_FINISHED_WITH_RESULTS:LX/8lg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8lg;->SEARCH_FINISHED_NO_RESULTS:LX/8lg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8lg;->ERROR:LX/8lg;

    aput-object v2, v0, v1

    sput-object v0, LX/8lg;->$VALUES:[LX/8lg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1397910
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8lg;
    .locals 1

    .prologue
    .line 1397911
    const-class v0, LX/8lg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8lg;

    return-object v0
.end method

.method public static values()[LX/8lg;
    .locals 1

    .prologue
    .line 1397912
    sget-object v0, LX/8lg;->$VALUES:[LX/8lg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8lg;

    return-object v0
.end method
