.class public LX/A77;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/A77;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/A76;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1625509
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "transliteration_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 1625510
    return-void
.end method

.method public static a(LX/0QB;)LX/A77;
    .locals 6

    .prologue
    .line 1625511
    sget-object v0, LX/A77;->a:LX/A77;

    if-nez v0, :cond_1

    .line 1625512
    const-class v1, LX/A77;

    monitor-enter v1

    .line 1625513
    :try_start_0
    sget-object v0, LX/A77;->a:LX/A77;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1625514
    if-eqz v2, :cond_0

    .line 1625515
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1625516
    new-instance p0, LX/A77;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/A76;->a(LX/0QB;)LX/A76;

    move-result-object v5

    check-cast v5, LX/A76;

    invoke-direct {p0, v3, v4, v5}, LX/A77;-><init>(Landroid/content/Context;LX/0Tt;LX/A76;)V

    .line 1625517
    move-object v0, p0

    .line 1625518
    sput-object v0, LX/A77;->a:LX/A77;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1625519
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1625520
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1625521
    :cond_1
    sget-object v0, LX/A77;->a:LX/A77;

    return-object v0

    .line 1625522
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1625523
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1625492
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 1625493
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1625494
    const-string v1, "dictionary_table"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, LX/A74;->c:LX/0U1;

    .line 1625495
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1625496
    aput-object v3, v2, v7

    sget-object v3, LX/A74;->d:LX/0U1;

    .line 1625497
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1625498
    aput-object v3, v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/A74;->b:LX/0U1;

    .line 1625499
    iget-object p0, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p0

    .line 1625500
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1625501
    :try_start_0
    sget-object v0, LX/A74;->c:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 1625502
    sget-object v2, LX/A74;->d:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 1625503
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1625504
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1625505
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1625506
    invoke-interface {v8, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1625507
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1625508
    return-object v8
.end method
