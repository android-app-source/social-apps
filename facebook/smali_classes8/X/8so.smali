.class public final LX/8so;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:LX/8sq;


# direct methods
.method public constructor <init>(LX/8sq;)V
    .locals 0

    .prologue
    .line 1411828
    iput-object p1, p0, LX/8so;->a:LX/8sq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1411829
    iget-object v0, p0, LX/8so;->a:LX/8sq;

    iget-object v0, v0, LX/8sq;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1411830
    if-eqz v1, :cond_0

    .line 1411831
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1411832
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1411833
    iget-object v0, p0, LX/8so;->a:LX/8sq;

    iget-object v0, v0, LX/8sq;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1411834
    :cond_0
    return-void
.end method
