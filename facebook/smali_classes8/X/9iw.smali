.class public LX/9iw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8Jc;

.field private b:Landroid/content/Context;

.field public c:LX/BIz;

.field public d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/74x;

.field public g:Lcom/facebook/photos/base/tagging/TagTarget;

.field private h:F

.field private i:Landroid/graphics/Matrix;

.field private j:Landroid/graphics/PointF;

.field public k:LX/BJ0;

.field public l:Z

.field public m:LX/9ic;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/75Q;

.field public o:LX/75F;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BIz;Lcom/facebook/photos/tagging/shared/TagTypeahead;IJLcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;LX/8nB;LX/8nX;LX/8nd;LX/8nF;LX/8Jc;LX/75Q;LX/75F;LX/0ad;)V
    .locals 13
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BIz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/tagging/shared/TagTypeahead;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1528876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1528877
    iput-object p1, p0, LX/9iw;->b:Landroid/content/Context;

    .line 1528878
    iput-object p2, p0, LX/9iw;->c:LX/BIz;

    .line 1528879
    move-object/from16 v0, p3

    iput-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    .line 1528880
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/9iw;->l:Z

    .line 1528881
    move-object/from16 v0, p12

    iput-object v0, p0, LX/9iw;->a:LX/8Jc;

    .line 1528882
    move-object/from16 v0, p13

    iput-object v0, p0, LX/9iw;->n:LX/75Q;

    .line 1528883
    move-object/from16 v0, p14

    iput-object v0, p0, LX/9iw;->o:LX/75F;

    move-object v2, p0

    move/from16 v3, p4

    move-wide/from16 v4, p5

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p7

    move-object/from16 v10, p11

    move-object/from16 v11, p15

    .line 1528884
    invoke-direct/range {v2 .. v11}, LX/9iw;->a(IJLX/8nB;LX/8nX;LX/8nd;Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;LX/8nF;LX/0ad;)V

    .line 1528885
    iget-object v2, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    new-instance v3, LX/9ir;

    invoke-direct {v3, p0}, LX/9ir;-><init>(LX/9iw;)V

    invoke-virtual {v2, v3}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8JT;)V

    .line 1528886
    iget-object v2, p0, LX/9iw;->b:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v2}, LX/8Hi;->a(Landroid/view/Window;)V

    .line 1528887
    return-void
.end method

.method private a(IJLX/8nB;LX/8nX;LX/8nd;Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;LX/8nF;LX/0ad;)V
    .locals 2

    .prologue
    .line 1528865
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    const v0, 0x41e065f

    if-ne p1, v0, :cond_0

    sget-short v0, LX/8mx;->d:S

    const/4 v1, 0x0

    invoke-interface {p9, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1528866
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p5, v1}, LX/8nX;->a(Ljava/lang/Long;)LX/8nW;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8nB;)V

    .line 1528867
    :goto_0
    return-void

    .line 1528868
    :cond_0
    const v0, 0x25d6af

    if-ne p1, v0, :cond_1

    .line 1528869
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1528870
    new-instance p3, LX/8nc;

    invoke-static {p6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    invoke-static {p6}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p2

    check-cast p2, Landroid/content/res/Resources;

    invoke-direct {p3, v1, p0, p1, p2}, LX/8nc;-><init>(Ljava/lang/Long;LX/0tX;Ljava/util/concurrent/ExecutorService;Landroid/content/res/Resources;)V

    .line 1528871
    move-object v1, p3

    .line 1528872
    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8nB;)V

    goto :goto_0

    .line 1528873
    :cond_1
    if-eqz p7, :cond_2

    .line 1528874
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-interface {p8, p7}, LX/8nF;->a(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)LX/8nB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8nB;)V

    goto :goto_0

    .line 1528875
    :cond_2
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0, p4}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8nB;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/9iw;Lcom/facebook/photos/base/tagging/TagTarget;ZZ)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 1528806
    iget-object v0, p0, LX/9iw;->k:LX/BJ0;

    .line 1528807
    iget-object v2, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    .line 1528808
    sget-object v3, LX/74F;->TYPE_AHEAD_OPEN:LX/74F;

    invoke-static {v3}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-static {v2, v3}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1528809
    iget-object v2, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->r$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1528810
    iget-object v2, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v2

    invoke-virtual {v2}, LX/9ip;->o()V

    .line 1528811
    iget-object v2, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v2

    const/4 v3, 0x1

    .line 1528812
    iput-boolean v3, v2, LX/9ip;->j:Z

    .line 1528813
    iget-object v2, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v2

    invoke-virtual {v2}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->d()V

    .line 1528814
    iget-object v2, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v2}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b()V

    .line 1528815
    iget-object v2, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1528816
    iput-object p1, p0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    .line 1528817
    iget-object v0, p0, LX/9iw;->c:LX/BIz;

    invoke-virtual {v0}, LX/BIz;->a()LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v8

    .line 1528818
    iget-object v0, p0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v0}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v8}, LX/7UQ;->getPhotoDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-static {v8, v0, v2}, LX/8Jc;->a(LX/7UZ;Landroid/graphics/RectF;Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v2

    .line 1528819
    new-instance v9, Landroid/graphics/PointF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    invoke-direct {v9, v0, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1528820
    new-instance v3, Landroid/graphics/PointF;

    invoke-interface {p1}, Lcom/facebook/photos/base/tagging/TagTarget;->f()Landroid/graphics/PointF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {v8}, LX/7UQ;->getPhotoWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    invoke-interface {p1}, Lcom/facebook/photos/base/tagging/TagTarget;->f()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8}, LX/7UQ;->getPhotoHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-direct {v3, v0, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1528821
    if-eqz p3, :cond_0

    .line 1528822
    instance-of v0, p1, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_3

    .line 1528823
    invoke-virtual {v8}, LX/7UQ;->getPhotoHeight()I

    move-result v0

    invoke-virtual {v8}, LX/7UQ;->getPhotoWidth()I

    move-result v4

    invoke-interface {p1}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v5

    invoke-static {v0, v4, v5}, LX/8Jc;->a(IILandroid/graphics/RectF;)F

    move-result v0

    .line 1528824
    :goto_0
    iget-object v4, p0, LX/9iw;->c:LX/BIz;

    invoke-virtual {v4}, LX/BIz;->a()LX/9ip;

    move-result-object v4

    .line 1528825
    iget-object v5, v4, LX/9ip;->e:LX/8Jd;

    invoke-virtual {v5, v3}, LX/8Jd;->setPosition(Landroid/graphics/PointF;)V

    .line 1528826
    iget-object v5, v4, LX/9ip;->e:LX/8Jd;

    invoke-virtual {v5, v0}, LX/8Jd;->setRadius(F)V

    .line 1528827
    iget-object v5, v4, LX/9ip;->g:LX/8Hs;

    invoke-virtual {v5}, LX/8Hs;->c()V

    .line 1528828
    :cond_0
    instance-of v0, p1, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_4

    .line 1528829
    iget-object v0, p0, LX/9iw;->a:LX/8Jc;

    invoke-interface {p1}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v4

    const/4 p3, 0x0

    .line 1528830
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-interface {v8}, LX/7UZ;->getPhotoWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    .line 1528831
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v6

    invoke-interface {v8}, LX/7UZ;->getPhotoHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    .line 1528832
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7, p3, p3, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1528833
    invoke-interface {v8}, LX/7UZ;->getPhotoDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1528834
    invoke-interface {v8}, LX/7UZ;->getScale()F

    move-result v5

    invoke-interface {v8}, LX/7UZ;->getMaxZoom()F

    move-result v6

    invoke-static {v0, v7, v5, v6}, LX/8Jc;->a(LX/8Jc;Landroid/graphics/RectF;FF)LX/8Jb;

    move-result-object v5

    move-object v0, v5

    .line 1528835
    move-object v7, v0

    .line 1528836
    :goto_1
    invoke-interface {v8}, LX/7UZ;->getPhotoWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-interface {v8}, LX/7UZ;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-interface {v8}, LX/7UZ;->b()Z

    move-result v5

    invoke-static {v3, v7, v0, v4, v5}, LX/8Jc;->a(Landroid/graphics/PointF;LX/8Jb;FFZ)Landroid/graphics/PointF;

    move-result-object v0

    move-object v0, v0

    .line 1528837
    iput-object v0, p0, LX/9iw;->j:Landroid/graphics/PointF;

    .line 1528838
    new-instance v4, Landroid/graphics/PointF;

    iget v0, v9, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1528839
    new-instance v5, Landroid/graphics/PointF;

    iget-object v0, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, v7, LX/8Jb;->b:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    add-float/2addr v2, v3

    invoke-direct {v5, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1528840
    if-eqz p2, :cond_5

    const/4 v0, 0x0

    .line 1528841
    :goto_2
    iget-object v2, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v2, v5, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Landroid/graphics/PointF;F)V

    .line 1528842
    if-nez p2, :cond_1

    .line 1528843
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->b()V

    .line 1528844
    :cond_1
    if-eqz p2, :cond_6

    .line 1528845
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-static {p0}, LX/9iw;->c(LX/9iw;)LX/8JW;

    move-result-object v2

    iget-object v3, p0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v3}, Lcom/facebook/photos/base/tagging/TagTarget;->n()Ljava/util/List;

    move-result-object v3

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(ZLX/8JW;Ljava/util/List;Landroid/graphics/PointF;Landroid/graphics/PointF;Z)V

    .line 1528846
    :goto_3
    iget-object v0, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget v1, v9, Landroid/graphics/PointF;->x:F

    sub-float v4, v0, v1

    .line 1528847
    iget-object v0, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v1, v9, Landroid/graphics/PointF;->y:F

    sub-float v5, v0, v1

    .line 1528848
    iget v1, v7, LX/8Jb;->a:F

    iget v2, v9, Landroid/graphics/PointF;->x:F

    iget v3, v9, Landroid/graphics/PointF;->y:F

    const-wide/16 v6, 0x12c

    move-object v0, v8

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(FFFFFJ)V

    .line 1528849
    instance-of v0, p1, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_2

    .line 1528850
    iget-object v0, p0, LX/9iw;->k:LX/BJ0;

    check-cast p1, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528851
    iget-object v1, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    .line 1528852
    sget-object v2, LX/74F;->TAG_ON_FACE_BOX:LX/74F;

    invoke-static {v2}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v1, v2}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1528853
    iget-object v1, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1528854
    iget-object v2, p1, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1528855
    iput-object v2, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->N:Ljava/lang/String;

    .line 1528856
    :cond_2
    return-void

    .line 1528857
    :cond_3
    iget-object v0, p0, LX/9iw;->a:LX/8Jc;

    invoke-virtual {v8}, LX/7UQ;->getPhotoDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/8Jc;->a(Landroid/graphics/Matrix;)F

    move-result v0

    goto/16 :goto_0

    .line 1528858
    :cond_4
    iget-object v0, p0, LX/9iw;->a:LX/8Jc;

    .line 1528859
    invoke-interface {v8}, LX/7UZ;->getScale()F

    move-result v4

    invoke-static {v0, v4}, LX/8Jc;->a(LX/8Jc;F)LX/8Jb;

    move-result-object v4

    move-object v0, v4

    .line 1528860
    move-object v7, v0

    goto/16 :goto_1

    .line 1528861
    :cond_5
    const/high16 v0, 0x43960000    # 300.0f

    goto :goto_2

    .line 1528862
    :cond_6
    new-instance v0, LX/9it;

    invoke-direct {v0, p0, v4, v5}, LX/9it;-><init>(LX/9iw;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 1528863
    iput-object v0, v8, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->N:LX/7Ub;

    .line 1528864
    goto :goto_3
.end method

.method public static c(LX/9iw;)LX/8JW;
    .locals 1

    .prologue
    .line 1528805
    new-instance v0, LX/9iu;

    invoke-direct {v0, p0}, LX/9iu;-><init>(LX/9iw;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/74x;Lcom/facebook/photos/base/tagging/TagTarget;ZLX/9ic;)V
    .locals 5
    .param p4    # LX/9ic;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1528888
    iget-object v0, p0, LX/9iw;->c:LX/BIz;

    invoke-virtual {v0}, LX/BIz;->a()LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    .line 1528889
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1528890
    :cond_0
    :goto_0
    return-void

    .line 1528891
    :cond_1
    iput-object p1, p0, LX/9iw;->f:LX/74x;

    .line 1528892
    iput-object p4, p0, LX/9iw;->m:LX/9ic;

    .line 1528893
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1528894
    iget-object v1, p0, LX/9iw;->o:LX/75F;

    invoke-virtual {v1, p1}, LX/75F;->b(LX/74x;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1528895
    iget-object v1, p0, LX/9iw;->o:LX/75F;

    invoke-virtual {v1, p1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528896
    iget-boolean v4, v1, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v4, v4

    .line 1528897
    if-nez v4, :cond_2

    .line 1528898
    if-eqz p4, :cond_3

    .line 1528899
    invoke-virtual {p4, v1}, LX/9ic;->a(Lcom/facebook/photos/base/tagging/FaceBox;)Lcom/facebook/photos/base/tagging/FaceBox;

    move-result-object v1

    .line 1528900
    :cond_3
    if-eqz v1, :cond_2

    .line 1528901
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1528902
    :cond_4
    new-instance v1, LX/9iv;

    invoke-direct {v1, p0}, LX/9iv;-><init>(LX/9iw;)V

    invoke-static {v2, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1528903
    :cond_5
    move-object v1, v2

    .line 1528904
    iput-object v1, p0, LX/9iw;->e:Ljava/util/List;

    .line 1528905
    invoke-virtual {v0}, LX/7UQ;->getScale()F

    move-result v1

    iput v1, p0, LX/9iw;->h:F

    .line 1528906
    new-instance v1, Landroid/graphics/Matrix;

    invoke-virtual {v0}, LX/7UQ;->getPhotoDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v1, p0, LX/9iw;->i:Landroid/graphics/Matrix;

    .line 1528907
    const/4 v0, 0x1

    invoke-static {p0, p2, v0, p3}, LX/9iw;->a$redex0(LX/9iw;Lcom/facebook/photos/base/tagging/TagTarget;ZZ)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 10

    .prologue
    .line 1528782
    iget-object v0, p0, LX/9iw;->c:LX/BIz;

    invoke-virtual {v0}, LX/BIz;->a()LX/9ip;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9iw;->c:LX/BIz;

    invoke-virtual {v0}, LX/BIz;->a()LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1528783
    :cond_0
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1528784
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(ZLandroid/graphics/PointF;)V

    .line 1528785
    :cond_1
    :goto_0
    return-void

    .line 1528786
    :cond_2
    iget-object v0, p0, LX/9iw;->c:LX/BIz;

    invoke-virtual {v0}, LX/BIz;->a()LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    .line 1528787
    iget-object v1, p0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v1}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, LX/9iw;->i:Landroid/graphics/Matrix;

    invoke-static {v0, v1, v2}, LX/8Jc;->a(LX/7UZ;Landroid/graphics/RectF;Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v8

    .line 1528788
    new-instance v9, Landroid/graphics/PointF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-direct {v9, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1528789
    iget v1, v9, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v4, v1, v2

    .line 1528790
    iget v1, v9, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float v5, v1, v2

    .line 1528791
    new-instance v1, LX/9is;

    invoke-direct {v1, p0}, LX/9is;-><init>(LX/9iw;)V

    .line 1528792
    iput-object v1, v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->N:LX/7Ub;

    .line 1528793
    iget v1, p0, LX/9iw;->h:F

    iget-object v2, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, LX/9iw;->j:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    if-eqz p1, :cond_3

    const-wide/16 v6, 0x12c

    :goto_1
    invoke-virtual/range {v0 .. v7}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(FFFFFJ)V

    .line 1528794
    new-instance v0, Landroid/graphics/PointF;

    iget v1, v9, Landroid/graphics/PointF;->x:F

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1528795
    iget-object v1, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(ZLandroid/graphics/PointF;)V

    .line 1528796
    iget-object v0, p0, LX/9iw;->c:LX/BIz;

    invoke-virtual {v0}, LX/BIz;->a()LX/9ip;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9ip;->b(Z)V

    .line 1528797
    iget-object v0, p0, LX/9iw;->k:LX/BJ0;

    .line 1528798
    iget-object v1, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    const/4 v2, 0x0

    .line 1528799
    iput-object v2, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->N:Ljava/lang/String;

    .line 1528800
    iget-object v1, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v1

    invoke-virtual {v1}, LX/9ip;->n()V

    .line 1528801
    iget-object v1, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->s$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1528802
    iget-object v1, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1528803
    goto :goto_0

    .line 1528804
    :cond_3
    const-wide/16 v6, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1528779
    iget-object v0, p0, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    .line 1528780
    iget-boolean p0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    move v0, p0

    .line 1528781
    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1528777
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/9iw;->a(Z)V

    .line 1528778
    return-void
.end method
