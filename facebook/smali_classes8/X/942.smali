.class public final LX/942;
.super LX/93s;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0it;",
        ":",
        "LX/0iq;",
        "DerivedData::",
        "LX/5Qu;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/93s",
        "<TModelData;TDerivedData;TServices;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Ot;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0il;)V
    .locals 9
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/93w;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/93q;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Ck;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;",
            "LX/93w;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434449
    move-object v0, p0

    move-object v1, p1

    move-object v2, p6

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, LX/93s;-><init>(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0il;LX/03V;LX/1Ck;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Ot;)V

    .line 1434450
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/privacy/model/PrivacyOptionsResult;)Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 2

    .prologue
    .line 1434451
    new-instance v0, LX/8QV;

    iget-object v1, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1434452
    iput-object p2, v0, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1434453
    move-object v0, v0

    .line 1434454
    iget-object v1, v0, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v1}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    iput-object v1, v0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1434455
    move-object v0, v0

    .line 1434456
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    return-object v0
.end method
