.class public LX/APx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670631
    iput-object p1, p0, LX/APx;->a:LX/0So;

    .line 1670632
    return-void
.end method

.method private static a(LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1670629
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p6, :cond_0

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;Z",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSpec;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1670590
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1670591
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1670592
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1670593
    :cond_0
    :goto_0
    move v0, v0

    .line 1670594
    if-nez v0, :cond_1

    if-eqz p1, :cond_2

    .line 1670595
    :cond_1
    :goto_1
    return v1

    .line 1670596
    :cond_2
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1670597
    invoke-static {p3, p9}, LX/94f;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p3, p8}, LX/94f;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p3, p4}, LX/94f;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_3
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1670598
    if-nez v0, :cond_1

    const/4 p1, 0x1

    const/4 v5, 0x0

    .line 1670599
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object p2

    .line 1670600
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    if-eq v0, v3, :cond_b

    move v5, p1

    .line 1670601
    :cond_4
    :goto_3
    move v0, v5

    .line 1670602
    if-nez v0, :cond_1

    .line 1670603
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    if-eq v0, p5, :cond_d

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 1670604
    if-nez v0, :cond_1

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1670605
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v4

    .line 1670606
    if-nez v4, :cond_e

    if-nez p6, :cond_e

    .line 1670607
    :cond_5
    :goto_5
    move v0, v0

    .line 1670608
    if-nez v0, :cond_1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1670609
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    .line 1670610
    if-nez v0, :cond_11

    move p0, v3

    :goto_6
    if-nez p7, :cond_12

    move v5, v3

    :goto_7
    xor-int/2addr v5, p0

    if-eqz v5, :cond_13

    move v0, v3

    .line 1670611
    :goto_8
    move v0, v0

    .line 1670612
    if-nez v0, :cond_1

    move v1, v2

    goto :goto_1

    .line 1670613
    :cond_6
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 1670614
    :goto_9
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual {p4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual {p8}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez p9, :cond_1

    if-nez p5, :cond_1

    if-nez p6, :cond_1

    if-nez p7, :cond_1

    if-nez v0, :cond_1

    move v1, v2

    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 1670615
    goto :goto_9

    :cond_8
    move v0, v3

    .line 1670616
    goto/16 :goto_0

    .line 1670617
    :cond_9
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {v4}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 1670618
    invoke-static {p2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 1670619
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_b
    move v4, v5

    .line 1670620
    :goto_a
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 1670621
    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {p0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0, v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-nez v0, :cond_c

    move v5, p1

    .line 1670622
    goto/16 :goto_3

    .line 1670623
    :cond_c
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_a

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 1670624
    :cond_e
    if-eqz v4, :cond_f

    if-nez p6, :cond_10

    :cond_f
    move v0, v3

    .line 1670625
    goto/16 :goto_5

    .line 1670626
    :cond_10
    invoke-virtual {p6}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    move v0, v3

    goto/16 :goto_5

    :cond_11
    move p0, v4

    .line 1670627
    goto/16 :goto_6

    :cond_12
    move v5, v4

    goto/16 :goto_7

    .line 1670628
    :cond_13
    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    check-cast p7, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v0, p7}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v0

    if-nez v0, :cond_14

    move v0, v3

    goto/16 :goto_8

    :cond_14
    move v0, v4

    goto/16 :goto_8
.end method

.method private b(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;Z",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSpec;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "LX/ARN;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1670584
    move-object v0, p1

    move-object v1, p3

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    invoke-static/range {v0 .. v6}, LX/APx;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1670585
    const/4 v0, 0x0

    .line 1670586
    :goto_0
    return v0

    .line 1670587
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1670588
    invoke-virtual/range {p0 .. p11}, LX/APx;->a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;)Z

    move-result v0

    goto :goto_0

    .line 1670589
    :cond_1
    invoke-virtual/range {p0 .. p11}, LX/APx;->a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p6, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;LX/ARN;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfoSpec;",
            "Z",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "Lcom/facebook/composer/privacy/model/ComposerPrivacyData;",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSpec;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "LX/ARN;",
            "LX/ARN;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1670574
    move-object v1, p0

    move-object v2, p1

    move/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    invoke-virtual/range {v1 .. v13}, LX/APx;->a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;LX/ARN;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/APx;->a:LX/0So;

    move-object/from16 v0, p2

    invoke-static {v1, v0}, LX/ASb;->a(LX/0So;Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p7

    iget-boolean v1, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->e:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;Z",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSpec;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "LX/ARN;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1670578
    invoke-static/range {p1 .. p10}, LX/APx;->a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1670579
    const/4 v0, 0x1

    .line 1670580
    :goto_0
    return v0

    .line 1670581
    :cond_0
    if-eqz p11, :cond_1

    .line 1670582
    invoke-interface {p11}, LX/ARN;->a()Z

    move-result v0

    goto :goto_0

    .line 1670583
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;LX/ARN;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;Z",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSpec;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "LX/ARN;",
            "LX/ARN;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1670575
    if-eqz p11, :cond_0

    .line 1670576
    invoke-interface/range {p11 .. p11}, LX/ARN;->a()Z

    move-result v0

    .line 1670577
    :goto_0
    return v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p12

    invoke-direct/range {v0 .. v11}, LX/APx;->b(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;)Z

    move-result v0

    goto :goto_0
.end method
