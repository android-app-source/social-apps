.class public final LX/8up;
.super LX/3uQ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1415792
    iput-object p1, p0, LX/8up;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-direct {p0, p2}, LX/3uQ;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1415793
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 1415794
    const/4 v1, 0x0

    iget-object v2, p0, LX/8up;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getAccessoryButtonHeight(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1415795
    invoke-super {p0, p1}, LX/3uQ;->draw(Landroid/graphics/Canvas;)V

    .line 1415796
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1415797
    return-void
.end method
