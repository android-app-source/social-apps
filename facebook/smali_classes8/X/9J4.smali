.class public LX/9J4;
.super LX/3Ag;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public c:LX/0hI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9J2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/fig/button/FigButton;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1464221
    const v0, 0x103000c

    invoke-direct {p0, p1, v0}, LX/9J4;-><init>(Landroid/content/Context;I)V

    .line 1464222
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1464237
    invoke-direct {p0, p1, p2}, LX/3Ag;-><init>(Landroid/content/Context;I)V

    .line 1464238
    const-class v0, LX/9J4;

    invoke-static {v0, p0}, LX/9J4;->a(Ljava/lang/Class;Landroid/app/Dialog;)V

    .line 1464239
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/9J4;->requestWindowFeature(I)Z

    .line 1464240
    invoke-virtual {p0}, LX/9J4;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1464241
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1464242
    const/4 p1, 0x2

    invoke-virtual {v0, p1}, Landroid/view/Window;->clearFlags(I)V

    .line 1464243
    const v0, 0x7f0307b9

    invoke-virtual {p0, v0}, LX/9J4;->setContentView(I)V

    .line 1464244
    const v0, 0x7f0d1489

    invoke-virtual {p0, v0}, LX/9J4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/9J4;->e:Lcom/facebook/fig/button/FigButton;

    .line 1464245
    iget-object v0, p0, LX/9J4;->e:Lcom/facebook/fig/button/FigButton;

    new-instance p1, LX/9J3;

    invoke-direct {p1, p0}, LX/9J3;-><init>(LX/9J4;)V

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1464246
    const v0, 0x7f0d1488

    invoke-virtual {p0, v0}, LX/9J4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9J4;->f:Landroid/view/View;

    .line 1464247
    const v0, 0x7f0d1487

    invoke-virtual {p0, v0}, LX/9J4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9J4;->g:Landroid/view/View;

    .line 1464248
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/app/Dialog;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Dialog;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/9J4;

    invoke-static {p0}, LX/0hI;->a(LX/0QB;)LX/0hI;

    move-result-object v1

    check-cast v1, LX/0hI;

    invoke-static {p0}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object p0

    check-cast p0, LX/9J2;

    iput-object v1, p1, LX/9J4;->c:LX/0hI;

    iput-object p0, p1, LX/9J4;->d:LX/9J2;

    return-void
.end method


# virtual methods
.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 1464223
    iget-object v0, p0, LX/9J4;->f:Landroid/view/View;

    .line 1464224
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1464225
    invoke-virtual {v0, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1464226
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1464227
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1464228
    const/4 v3, 0x0

    aget v3, v2, v3

    const/4 v4, 0x1

    aget v2, v2, v4

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 1464229
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    move v0, v1

    .line 1464230
    if-eqz v0, :cond_1

    .line 1464231
    invoke-virtual {p0}, LX/9J4;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1464232
    if-eqz v0, :cond_0

    .line 1464233
    invoke-virtual {v0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1464234
    iget-object v0, p0, LX/9J4;->f:Landroid/view/View;

    new-instance v1, Lcom/facebook/goodfriends/ui/ComposerOverlayDialog$2;

    invoke-direct {v1, p0}, Lcom/facebook/goodfriends/ui/ComposerOverlayDialog$2;-><init>(LX/9J4;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1464235
    :cond_0
    const/4 v0, 0x0

    .line 1464236
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
