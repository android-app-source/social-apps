.class public abstract LX/9Bh;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field private b:LX/9Bf;

.field public c:LX/9Bg;

.field private d:LX/0wd;

.field private e:LX/4mU;

.field private f:LX/4mV;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Bh;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Bh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1452403
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/9Bh;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/0wW;LX/4mV;)V
    .locals 4

    .prologue
    .line 1452410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1452411
    sget-object v0, LX/9Bg;->HIDDEN:LX/9Bg;

    iput-object v0, p0, LX/9Bh;->c:LX/9Bg;

    .line 1452412
    iput-object p2, p0, LX/9Bh;->f:LX/4mV;

    .line 1452413
    new-instance v0, LX/9Bf;

    invoke-direct {v0, p0}, LX/9Bf;-><init>(LX/9Bh;)V

    iput-object v0, p0, LX/9Bh;->b:LX/9Bf;

    .line 1452414
    invoke-virtual {p1}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    iget-object v1, p0, LX/9Bh;->b:LX/9Bf;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    sget-object v1, LX/9Bh;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/9Bh;->d:LX/0wd;

    .line 1452415
    return-void
.end method

.method private c(LX/9Bh;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1452404
    iget-object v0, p0, LX/9Bh;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9Bh;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452405
    :goto_0
    return-void

    .line 1452406
    :cond_0
    iget-object v0, p0, LX/9Bh;->g:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1452407
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Bh;->g:Ljava/util/List;

    .line 1452408
    :cond_1
    iget-object v0, p0, LX/9Bh;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1452409
    invoke-virtual {p1, p0}, LX/9Bh;->a(LX/9Bh;)V

    goto :goto_0
.end method

.method public static h(LX/9Bh;)V
    .locals 2

    .prologue
    .line 1452399
    sget-object v0, LX/9Bg;->HIDDEN:LX/9Bg;

    iput-object v0, p0, LX/9Bh;->c:LX/9Bg;

    .line 1452400
    invoke-virtual {p0}, LX/9Bh;->f()LX/0zw;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/9Bh;->f()LX/0zw;

    move-result-object v0

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452401
    invoke-virtual {p0}, LX/9Bh;->f()LX/0zw;

    move-result-object v0

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1452402
    :cond_0
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 10

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 1452393
    float-to-double v0, p1

    const-wide/16 v2, 0x0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    move-wide v8, v4

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1452394
    iget-object v1, p0, LX/9Bh;->e:LX/4mU;

    invoke-virtual {v1, v0}, LX/4mU;->a(F)V

    .line 1452395
    iget-object v1, p0, LX/9Bh;->e:LX/4mU;

    invoke-virtual {v1, v0}, LX/4mU;->c(F)V

    .line 1452396
    invoke-virtual {p0}, LX/9Bh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/9Bh;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1452397
    :cond_0
    iget-object v0, p0, LX/9Bh;->e:LX/4mU;

    invoke-virtual {v0, p1}, LX/4mU;->e(F)V

    .line 1452398
    :cond_1
    return-void
.end method

.method public final a(LX/9Bh;)V
    .locals 1

    .prologue
    .line 1452416
    iget-object v0, p0, LX/9Bh;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9Bh;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452417
    :goto_0
    return-void

    .line 1452418
    :cond_0
    iget-object v0, p0, LX/9Bh;->h:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1452419
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Bh;->h:Ljava/util/List;

    .line 1452420
    :cond_1
    iget-object v0, p0, LX/9Bh;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1452421
    invoke-direct {p1, p0}, LX/9Bh;->c(LX/9Bh;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1452378
    invoke-virtual {p0}, LX/9Bh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/9Bh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1452379
    :goto_0
    return v0

    .line 1452380
    :cond_1
    iget-object v0, p0, LX/9Bh;->h:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 1452381
    iget-object v0, p0, LX/9Bh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bh;

    .line 1452382
    invoke-virtual {v0}, LX/9Bh;->c()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, LX/9Bh;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_3
    move v0, v1

    .line 1452383
    goto :goto_0

    .line 1452384
    :cond_4
    iget-object v0, p0, LX/9Bh;->g:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 1452385
    iget-object v0, p0, LX/9Bh;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bh;

    .line 1452386
    invoke-virtual {v0}, LX/9Bh;->c()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, LX/9Bh;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1452387
    :cond_6
    invoke-virtual {v0}, LX/9Bh;->b()V

    goto :goto_1

    .line 1452388
    :cond_7
    iget-object v0, p0, LX/9Bh;->e:LX/4mU;

    if-nez v0, :cond_8

    .line 1452389
    iget-object v0, p0, LX/9Bh;->f:LX/4mV;

    invoke-virtual {p0}, LX/9Bh;->f()LX/0zw;

    move-result-object v1

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, LX/9Bh;->e:LX/4mU;

    .line 1452390
    :cond_8
    sget-object v0, LX/9Bg;->REVEALING:LX/9Bg;

    iput-object v0, p0, LX/9Bh;->c:LX/9Bg;

    .line 1452391
    iget-object v0, p0, LX/9Bh;->d:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1452392
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 1452370
    invoke-virtual {p0}, LX/9Bh;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1452371
    sget-object v0, LX/9Bg;->HIDDEN:LX/9Bg;

    iget-object v1, p0, LX/9Bh;->c:LX/9Bg;

    invoke-virtual {v0, v1}, LX/9Bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 1452372
    if-eqz v0, :cond_1

    .line 1452373
    :cond_0
    :goto_0
    return-void

    .line 1452374
    :cond_1
    sget-object v0, LX/9Bg;->HIDING:LX/9Bg;

    iput-object v0, p0, LX/9Bh;->c:LX/9Bg;

    .line 1452375
    iget-object v0, p0, LX/9Bh;->d:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1452376
    iget-object v0, p0, LX/9Bh;->d:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452377
    invoke-static {p0}, LX/9Bh;->h(LX/9Bh;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1452369
    sget-object v0, LX/9Bg;->REVEALING:LX/9Bg;

    iget-object v1, p0, LX/9Bh;->c:LX/9Bg;

    invoke-virtual {v0, v1}, LX/9Bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1452367
    sget-object v0, LX/9Bg;->SHOWN:LX/9Bg;

    iget-object v1, p0, LX/9Bh;->c:LX/9Bg;

    invoke-virtual {v0, v1}, LX/9Bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1452368
    sget-object v0, LX/9Bg;->HIDING:LX/9Bg;

    iget-object v1, p0, LX/9Bh;->c:LX/9Bg;

    invoke-virtual {v0, v1}, LX/9Bg;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract f()LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method
