.class public final enum LX/9WB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9WB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9WB;

.field public static final enum ACTION:LX/9WB;

.field public static final enum FLOW_STEP:LX/9WB;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1500662
    new-instance v0, LX/9WB;

    const-string v1, "FLOW_STEP"

    invoke-direct {v0, v1, v2}, LX/9WB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WB;->FLOW_STEP:LX/9WB;

    .line 1500663
    new-instance v0, LX/9WB;

    const-string v1, "ACTION"

    invoke-direct {v0, v1, v3}, LX/9WB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WB;->ACTION:LX/9WB;

    .line 1500664
    const/4 v0, 0x2

    new-array v0, v0, [LX/9WB;

    sget-object v1, LX/9WB;->FLOW_STEP:LX/9WB;

    aput-object v1, v0, v2

    sget-object v1, LX/9WB;->ACTION:LX/9WB;

    aput-object v1, v0, v3

    sput-object v0, LX/9WB;->$VALUES:[LX/9WB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1500665
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9WB;
    .locals 1

    .prologue
    .line 1500666
    const-class v0, LX/9WB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9WB;

    return-object v0
.end method

.method public static values()[LX/9WB;
    .locals 1

    .prologue
    .line 1500667
    sget-object v0, LX/9WB;->$VALUES:[LX/9WB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9WB;

    return-object v0
.end method
