.class public LX/8i7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final g:Ljava/lang/CharSequence;

.field private static final h:Ljava/lang/CharSequence;

.field private static volatile i:LX/8i7;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/3Cq;

.field private final c:Landroid/graphics/drawable/Drawable;

.field private final d:Landroid/graphics/drawable/Drawable;

.field public final e:Landroid/graphics/drawable/Drawable;

.field private final f:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1391162
    const-string v0, "[badge]"

    sput-object v0, LX/8i7;->g:Ljava/lang/CharSequence;

    .line 1391163
    const-string v0, "[phonetic]"

    sput-object v0, LX/8i7;->h:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0wM;LX/3Cq;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1391200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1391201
    iput-object p1, p0, LX/8i7;->a:Landroid/content/Context;

    .line 1391202
    iput-object p3, p0, LX/8i7;->b:LX/3Cq;

    .line 1391203
    iget-object v0, p0, LX/8i7;->a:Landroid/content/Context;

    const v1, 0x7f021a22

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/8i7;->c:Landroid/graphics/drawable/Drawable;

    .line 1391204
    iget-object v0, p0, LX/8i7;->a:Landroid/content/Context;

    const v1, 0x7f021a23

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/8i7;->d:Landroid/graphics/drawable/Drawable;

    .line 1391205
    iget-object v0, p0, LX/8i7;->a:Landroid/content/Context;

    const v1, 0x7f020ec3

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/8i7;->e:Landroid/graphics/drawable/Drawable;

    .line 1391206
    const v0, 0x7f021901

    const v1, -0xca6e01

    invoke-virtual {p2, v0, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/8i7;->f:Landroid/graphics/drawable/Drawable;

    .line 1391207
    iget-object v0, p0, LX/8i7;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b176f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1391208
    iget-object v1, p0, LX/8i7;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, LX/8i7;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 1391209
    iget-object v1, p0, LX/8i7;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, LX/8i7;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 1391210
    iget-object v1, p0, LX/8i7;->e:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/8i7;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b17df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1391211
    const/4 v3, 0x0

    add-int p1, v2, v0

    invoke-virtual {v1, v0, v3, p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1391212
    iget-object v1, p0, LX/8i7;->f:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, LX/8i7;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 1391213
    return-void
.end method

.method public static a(LX/0QB;)LX/8i7;
    .locals 6

    .prologue
    .line 1391187
    sget-object v0, LX/8i7;->i:LX/8i7;

    if-nez v0, :cond_1

    .line 1391188
    const-class v1, LX/8i7;

    monitor-enter v1

    .line 1391189
    :try_start_0
    sget-object v0, LX/8i7;->i:LX/8i7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1391190
    if-eqz v2, :cond_0

    .line 1391191
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1391192
    new-instance p0, LX/8i7;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v0}, LX/3Cq;->a(LX/0QB;)LX/3Cq;

    move-result-object v5

    check-cast v5, LX/3Cq;

    invoke-direct {p0, v3, v4, v5}, LX/8i7;-><init>(Landroid/content/Context;LX/0wM;LX/3Cq;)V

    .line 1391193
    move-object v0, p0

    .line 1391194
    sput-object v0, LX/8i7;->i:LX/8i7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1391195
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1391196
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1391197
    :cond_1
    sget-object v0, LX/8i7;->i:LX/8i7;

    return-object v0

    .line 1391198
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1391199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8i7;Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1391182
    sget-object v0, LX/8i6;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1391183
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No drawable for verification status"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1391184
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1391185
    :pswitch_0
    iget-object v0, p0, LX/8i7;->d:Landroid/graphics/drawable/Drawable;

    .line 1391186
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/8i7;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/graphics/drawable/Drawable;I)V
    .locals 3

    .prologue
    .line 1391214
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1391215
    return-void
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 1391179
    const-string v0, "\u2060"

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    sget-object v1, LX/8i7;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1391180
    new-instance v0, Landroid/text/style/ImageSpan;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    sget-object v2, LX/8i7;->g:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x11

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1391181
    return-void
.end method


# virtual methods
.method public final b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 1391177
    iget-object v0, p0, LX/8i7;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0}, LX/8i7;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)V

    .line 1391178
    return-object p1
.end method

.method public final c(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 13

    .prologue
    const/4 v6, 0x1

    .line 1391164
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, LX/8i7;->a:Landroid/content/Context;

    const v2, 0x7f082337

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1391165
    new-instance v1, LX/8hx;

    invoke-direct {v1}, LX/8hx;-><init>()V

    iget-object v2, p0, LX/8i7;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a014b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1391166
    iput v2, v1, LX/8hx;->b:I

    .line 1391167
    move-object v1, v1

    .line 1391168
    iget-object v2, p0, LX/8i7;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1391169
    iput v2, v1, LX/8hx;->c:I

    .line 1391170
    move-object v1, v1

    .line 1391171
    const/high16 v2, 0x40800000    # 4.0f

    .line 1391172
    iput v2, v1, LX/8hx;->d:F

    .line 1391173
    move-object v1, v1

    .line 1391174
    new-instance v7, LX/8hy;

    iget v8, v1, LX/8hx;->a:F

    iget v9, v1, LX/8hx;->b:I

    iget v10, v1, LX/8hx;->c:I

    iget v11, v1, LX/8hx;->d:F

    iget v12, v1, LX/8hx;->e:F

    invoke-direct/range {v7 .. v12}, LX/8hy;-><init>(FIIFF)V

    move-object v1, v7

    .line 1391175
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v5, 0x18

    invoke-direct {v4, v5}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    aput-object v4, v2, v3

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v3, v2, v6

    const/4 v3, 0x2

    aput-object v1, v2, v3

    invoke-static {v0, v2}, LX/47t;->a(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;)V

    .line 1391176
    const-string v1, " "

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method
