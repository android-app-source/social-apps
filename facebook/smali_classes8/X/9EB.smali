.class public LX/9EB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9DS;


# instance fields
.field public final a:LX/9Eb;

.field public final b:LX/9Cu;

.field public final c:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/9CC;

.field public final e:LX/9D1;

.field public final f:LX/9Do;

.field public final g:LX/9Du;

.field public final h:LX/9EL;

.field public final i:LX/20j;

.field public j:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public k:LX/9DS;

.field public l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "LX/9Ct;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;LX/9Eb;LX/9Cu;LX/20j;)V
    .locals 1
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9CC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9D1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/9Do;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/9Du;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/9EL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;",
            "LX/9CC;",
            "LX/9D1;",
            "LX/9Do;",
            "LX/9Du;",
            "LX/9EL;",
            "LX/9Eb;",
            "LX/9Cu;",
            "LX/20j;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456557
    iput-object p1, p0, LX/9EB;->c:LX/0QK;

    .line 1456558
    iput-object p2, p0, LX/9EB;->d:LX/9CC;

    .line 1456559
    iput-object p3, p0, LX/9EB;->e:LX/9D1;

    .line 1456560
    iput-object p7, p0, LX/9EB;->a:LX/9Eb;

    .line 1456561
    iput-object p8, p0, LX/9EB;->b:LX/9Cu;

    .line 1456562
    iput-object p9, p0, LX/9EB;->i:LX/20j;

    .line 1456563
    iput-object p4, p0, LX/9EB;->f:LX/9Do;

    .line 1456564
    iput-object p5, p0, LX/9EB;->g:LX/9Du;

    .line 1456565
    iput-object p6, p0, LX/9EB;->h:LX/9EL;

    .line 1456566
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9EB;->l:Ljava/util/Map;

    .line 1456567
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1456568
    iget-object v0, p0, LX/9EB;->k:LX/9DS;

    if-eqz v0, :cond_0

    .line 1456569
    iget-object v0, p0, LX/9EB;->k:LX/9DS;

    invoke-interface {v0}, LX/9DS;->a()V

    .line 1456570
    :cond_0
    iget-object v0, p0, LX/9EB;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ct;

    .line 1456571
    invoke-static {v0}, LX/9Ct;->c(LX/9Ct;)V

    .line 1456572
    goto :goto_0

    .line 1456573
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/9EB;->k:LX/9DS;

    .line 1456574
    iget-object v0, p0, LX/9EB;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1456575
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1456576
    invoke-direct {p0}, LX/9EB;->b()V

    .line 1456577
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 1456578
    iget-object v0, p0, LX/9EB;->i:LX/20j;

    iget-object v1, p0, LX/9EB;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, p1}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1456579
    iget-object v1, p0, LX/9EB;->c:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1456580
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 1456581
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1456582
    if-nez p1, :cond_1

    .line 1456583
    const/4 v0, 0x0

    iput-object v0, p0, LX/9EB;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1456584
    invoke-direct {p0}, LX/9EB;->b()V

    .line 1456585
    :cond_0
    return-void

    .line 1456586
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456587
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, LX/9EB;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1456588
    iget-object v0, p0, LX/9EB;->k:LX/9DS;

    if-eqz v0, :cond_3

    .line 1456589
    iget-object v0, p0, LX/9EB;->k:LX/9DS;

    invoke-interface {v0, p1}, LX/21l;->a(Ljava/lang/Object;)V

    .line 1456590
    :goto_0
    iget-object v0, p0, LX/9EB;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v1

    .line 1456591
    iget-object v0, p0, LX/9EB;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1456592
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1456593
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456594
    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1456595
    iget-object v3, p0, LX/9EB;->l:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ct;

    .line 1456596
    invoke-static {v0}, LX/9Ct;->c(LX/9Ct;)V

    .line 1456597
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 1456598
    :cond_3
    iget-object v0, p0, LX/9EB;->c:LX/0QK;

    .line 1456599
    iget-object v5, p0, LX/9EB;->a:LX/9Eb;

    iget-object v7, p0, LX/9EB;->d:LX/9CC;

    iget-object v8, p0, LX/9EB;->e:LX/9D1;

    iget-object v9, p0, LX/9EB;->f:LX/9Do;

    iget-object v10, p0, LX/9EB;->g:LX/9Du;

    iget-object v11, p0, LX/9EB;->h:LX/9EL;

    move-object v6, v0

    invoke-virtual/range {v5 .. v11}, LX/9Eb;->a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9Ea;

    move-result-object v5

    iput-object v5, p0, LX/9EB;->k:LX/9DS;

    .line 1456600
    iget-object v5, p0, LX/9EB;->k:LX/9DS;

    invoke-interface {v5, p1}, LX/21l;->a(Ljava/lang/Object;)V

    .line 1456601
    goto :goto_0

    .line 1456602
    :cond_4
    iget-object v0, p0, LX/9EB;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456603
    iget-object v1, p0, LX/9EB;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1456604
    iget-object v1, p0, LX/9EB;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9Ct;

    invoke-virtual {v1, v0}, LX/9Ct;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1456605
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1456606
    :cond_5
    new-instance v1, LX/9EA;

    invoke-direct {v1, p0}, LX/9EA;-><init>(LX/9EB;)V

    .line 1456607
    iget-object v5, p0, LX/9EB;->b:LX/9Cu;

    iget-object v6, p0, LX/9EB;->e:LX/9D1;

    iget-object v7, p0, LX/9EB;->f:LX/9Do;

    iget-object v8, p0, LX/9EB;->d:LX/9CC;

    invoke-virtual {v5, v1, v6, v7, v8}, LX/9Cu;->a(LX/0QK;LX/9D1;LX/9Do;LX/9CC;)LX/9Ct;

    move-result-object v5

    .line 1456608
    iget-object v6, p0, LX/9EB;->l:Ljava/util/Map;

    invoke-interface {v6, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1456609
    invoke-virtual {v5, v0}, LX/9Ct;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1456610
    goto :goto_3
.end method
