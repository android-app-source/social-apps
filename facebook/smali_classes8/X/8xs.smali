.class public final LX/8xs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/8xt;


# direct methods
.method public constructor <init>(LX/8xt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1424812
    iput-object p1, p0, LX/8xs;->d:LX/8xt;

    iput-object p2, p0, LX/8xs;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8xs;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p4, p0, LX/8xs;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1424813
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424814
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1424815
    iget-object v0, p0, LX/8xs;->a:Ljava/lang/String;

    invoke-static {p1, v0}, LX/8xt;->b(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1424816
    if-nez v0, :cond_1

    .line 1424817
    :cond_0
    :goto_0
    return-void

    .line 1424818
    :cond_1
    iget-object v1, p0, LX/8xs;->d:LX/8xt;

    iget-object v1, v1, LX/8xt;->d:LX/20j;

    iget-object v2, p0, LX/8xs;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p0, LX/8xs;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)LX/6PS;

    move-result-object v0

    .line 1424819
    if-eqz v0, :cond_0

    .line 1424820
    iget-object v1, p0, LX/8xs;->d:LX/8xt;

    iget-object v1, v1, LX/8xt;->f:LX/1K9;

    new-instance v2, LX/8q4;

    iget-object v3, v0, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v0, v0, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_0
.end method
