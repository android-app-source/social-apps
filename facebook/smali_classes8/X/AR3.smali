.class public LX/AR3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0jI;",
        ":",
        "LX/0io;",
        ":",
        "LX/0iw;",
        ":",
        "LX/0ix;",
        ":",
        "LX/0iy;",
        ":",
        "LX/0iz;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j1;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0ir;",
        ":",
        "LX/0jA;",
        ":",
        "LX/0jB;",
        ":",
        "LX/0jC;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0is;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jG;",
        ":",
        "LX/0jE;",
        ":",
        "LX/0jH;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "LX/2rg;",
        ":",
        "LX/5RE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:LX/AR0;

.field private final e:LX/0SG;

.field private final f:LX/8LV;

.field private final g:LX/03V;

.field private final h:LX/75Q;

.field private final i:LX/75F;

.field private final j:LX/1EZ;

.field private final k:LX/0kL;

.field private final l:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final m:LX/7Dh;

.field private n:J

.field private o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;


# direct methods
.method public constructor <init>(LX/0il;Landroid/content/Context;LX/AR0;LX/0SG;LX/0kL;LX/8LV;LX/03V;LX/75Q;LX/75F;LX/1EZ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7Dh;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/content/Context;",
            "LX/AR0;",
            "LX/0SG;",
            "LX/0kL;",
            "LX/8LV;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/75Q;",
            "LX/75F;",
            "LX/1EZ;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/7Dh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1672229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672230
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/AR3;->n:J

    .line 1672231
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    .line 1672232
    iput-object p2, p0, LX/AR3;->c:Landroid/content/Context;

    .line 1672233
    iput-object p3, p0, LX/AR3;->d:LX/AR0;

    .line 1672234
    iput-object p4, p0, LX/AR3;->e:LX/0SG;

    .line 1672235
    iput-object p5, p0, LX/AR3;->k:LX/0kL;

    .line 1672236
    iput-object p6, p0, LX/AR3;->f:LX/8LV;

    .line 1672237
    iput-object p7, p0, LX/AR3;->g:LX/03V;

    .line 1672238
    iput-object p8, p0, LX/AR3;->h:LX/75Q;

    .line 1672239
    iput-object p9, p0, LX/AR3;->i:LX/75F;

    .line 1672240
    iput-object p10, p0, LX/AR3;->j:LX/1EZ;

    .line 1672241
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AR3;->o:LX/0Px;

    .line 1672242
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0j2;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1672243
    iput-object p11, p0, LX/AR3;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1672244
    iput-object p12, p0, LX/AR3;->m:LX/7Dh;

    .line 1672245
    return-void
.end method

.method private f()Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 21

    .prologue
    .line 1672246
    move-object/from16 v0, p0

    iget-object v1, v0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, LX/0il;

    .line 1672247
    move-object/from16 v0, p0

    iget-object v1, v0, LX/AR3;->f:LX/8LV;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v3}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v4}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v4

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0jI;

    check-cast v5, LX/0j1;

    invoke-interface {v5}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0jI;

    check-cast v7, LX/0jD;

    invoke-interface {v7}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v7

    invoke-static {v7}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/AR3;->n:J

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0jI;

    check-cast v10, LX/0j8;

    invoke-interface {v10}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v10

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/0jI;

    check-cast v11, LX/0j8;

    invoke-interface {v11}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v12}, LX/7kq;->w(LX/0Px;)Z

    move-result v12

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, LX/AR3;->m:LX/7Dh;

    invoke-virtual {v12}, LX/7Dh;->b()Z

    move-result v12

    if-eqz v12, :cond_0

    const/4 v12, 0x1

    :goto_0
    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/0jI;

    check-cast v13, LX/0j0;

    invoke-interface {v13}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v13

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/0jI;

    invoke-interface {v14}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v14

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/0jI;

    check-cast v15, LX/0ix;

    invoke-interface {v15}, LX/0ix;->isUserSelectedTags()Z

    move-result v15

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, LX/0jI;

    check-cast v16, LX/0j8;

    invoke-interface/range {v16 .. v16}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v16

    if-eqz v16, :cond_1

    const/16 v16, 0x1

    :goto_1
    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, LX/0jI;

    check-cast v17, LX/0j3;

    invoke-interface/range {v17 .. v17}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, LX/AR3;->o:LX/0Px;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, LX/7kq;->j(LX/0Px;)Z

    move-result v18

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, LX/0jI;

    check-cast v19, LX/0iw;

    invoke-interface/range {v19 .. v19}, LX/0iw;->isFeedOnlyPost()Z

    move-result v19

    invoke-interface/range {v20 .. v20}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, LX/0jI;

    check-cast v20, LX/0ir;

    invoke-interface/range {v20 .. v20}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v20

    invoke-virtual/range {v1 .. v20}, LX/8LV;->a(LX/0Px;LX/0Px;Ljava/lang/String;JLX/0Px;JLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v12, 0x0

    goto :goto_0

    :cond_1
    const/16 v16, 0x0

    goto :goto_1
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 1672248
    iget-object v0, p0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672249
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->q(LX/0Px;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 7

    .prologue
    .line 1672250
    iget-object v0, p0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672251
    invoke-direct {p0}, LX/AR3;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0j1;

    invoke-interface {v0}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1672252
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1672253
    sget-object v1, LX/8LS;->VIDEO:LX/8LS;

    if-ne v0, v1, :cond_0

    .line 1672254
    iget-object v0, p0, LX/AR3;->d:LX/AR0;

    .line 1672255
    iget-object v1, v0, LX/AR0;->f:LX/1EZ;

    invoke-virtual {v1, p1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1672256
    :goto_0
    return-void

    .line 1672257
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672258
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1672259
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v3, v0

    .line 1672260
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1672261
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v5

    sget-object v6, LX/4gF;->PHOTO:LX/4gF;

    if-ne v5, v6, :cond_1

    .line 1672262
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1672263
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1672264
    :cond_2
    iget-object v0, p0, LX/AR3;->h:LX/75Q;

    invoke-static {v0, v2}, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a(LX/75Q;Ljava/util/List;)Lcom/facebook/photos/tagging/store/TagStoreCopy;

    move-result-object v0

    iget-object v1, p0, LX/AR3;->i:LX/75F;

    invoke-static {v1, v2}, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->a(LX/75F;Ljava/util/List;)Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/tagging/store/TagStoreCopy;Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;)V

    .line 1672265
    iget-object v0, p0, LX/AR3;->j:LX/1EZ;

    invoke-virtual {v0, p1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1672266
    iget-object v0, p0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672267
    invoke-direct {p0}, LX/AR3;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 1672268
    :goto_0
    return v0

    .line 1672269
    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->l(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_1

    move v0, v3

    .line 1672270
    goto :goto_0

    .line 1672271
    :cond_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->l(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1672272
    iget-object v1, p0, LX/AR3;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1672273
    sget-object v4, LX/0pP;->v:LX/0Tn;

    iget-object v5, p0, LX/AR3;->e:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v1, v4, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1672274
    invoke-interface {v1}, LX/0hN;->commit()V

    :cond_2
    move-object v1, v0

    .line 1672275
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rg;

    invoke-interface {v1}, LX/2rg;->n()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1672276
    :cond_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v4

    iput-wide v4, p0, LX/AR3;->n:J

    .line 1672277
    :cond_4
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0j1;

    invoke-interface {v0}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1672278
    iget-object v0, p0, LX/AR3;->o:LX/0Px;

    iget-object v1, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1672279
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v4

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1672280
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1672281
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1672282
    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    move v6, v4

    .line 1672283
    :goto_2
    if-eqz v6, :cond_5

    .line 1672284
    const-string v6, "\n"

    invoke-static {v6}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {v1, v6}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1672285
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v1, v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    .line 1672286
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v3}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v3

    .line 1672287
    iput-object v6, v3, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1672288
    move-object v6, v3

    .line 1672289
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 1672290
    iput-object v3, v6, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1672291
    move-object v3, v6

    .line 1672292
    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1672293
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1672294
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v0, v4, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    :goto_3
    if-ge v5, v6, :cond_8

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1672295
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1672296
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    move v3, v5

    .line 1672297
    goto :goto_1

    :cond_7
    move v6, v5

    .line 1672298
    goto :goto_2

    .line 1672299
    :cond_8
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 1672300
    iput-object v0, p0, LX/AR3;->o:LX/0Px;

    .line 1672301
    sget-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    :cond_9
    :goto_4
    move v0, v2

    .line 1672302
    goto/16 :goto_0

    .line 1672303
    :cond_a
    iget-object v0, p0, LX/AR3;->o:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_9

    iget-object v0, p0, LX/AR3;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1672304
    iget-object v0, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, LX/AR3;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v2

    .line 1672305
    :goto_5
    if-eqz v0, :cond_b

    .line 1672306
    iget-object v0, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const-string v1, "\n"

    invoke-static {v1}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v0, v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1672307
    :cond_b
    iget-object v1, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v0, p0, LX/AR3;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v1, v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1672308
    iget-object v0, p0, LX/AR3;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v0}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v0

    sget-object v1, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1672309
    iput-object v1, v0, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1672310
    move-object v0, v0

    .line 1672311
    invoke-virtual {v0}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AR3;->o:LX/0Px;

    goto/16 :goto_4

    :cond_c
    move v0, v3

    .line 1672312
    goto :goto_5
.end method

.method public final c()Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 44
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1672313
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 1672314
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v42, v3

    check-cast v42, LX/0jI;

    .line 1672315
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jI;

    check-cast v3, LX/0iq;

    invoke-interface {v3}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v3, :cond_0

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jI;

    check-cast v3, LX/0iq;

    invoke-interface {v3}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-nez v3, :cond_0

    .line 1672316
    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->f:LX/8LV;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/AR3;->e:LX/0SG;

    invoke-static {v2, v4}, LX/AR5;->a(LX/0io;LX/0SG;)Lcom/facebook/audience/model/UploadShot;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/8LV;->a(Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 1672317
    :goto_0
    return-object v2

    .line 1672318
    :cond_0
    invoke-direct/range {p0 .. p0}, LX/AR3;->g()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v2, v42

    .line 1672319
    check-cast v2, LX/0j1;

    invoke-interface {v2}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v2, v42

    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-static {v2}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1672320
    invoke-direct/range {p0 .. p0}, LX/AR3;->f()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto :goto_0

    .line 1672321
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->d:LX/AR0;

    move-object/from16 v0, v42

    invoke-virtual {v2, v0}, LX/AR0;->a(LX/0jI;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto :goto_0

    .line 1672322
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 1672323
    invoke-virtual {v3}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v3, v42

    .line 1672324
    check-cast v3, LX/0jD;

    invoke-interface {v3}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v19

    move-object/from16 v3, v42

    .line 1672325
    check-cast v3, LX/0j3;

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v3

    invoke-virtual {v3}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v3, v42

    .line 1672326
    check-cast v3, LX/0jB;

    invoke-interface {v3}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v3

    if-nez v3, :cond_3

    const/16 v17, 0x0

    :goto_1
    move-object/from16 v3, v42

    .line 1672327
    check-cast v3, LX/0j1;

    invoke-interface {v3}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v3

    if-eqz v3, :cond_5

    move-object/from16 v2, v42

    .line 1672328
    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-static {v2}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1672329
    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->f:LX/8LV;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v5

    move-object/from16 v2, v42

    check-cast v2, LX/0j1;

    invoke-interface {v2}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v2, v42

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v2, v42

    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v10

    check-cast v42, LX/0ir;

    invoke-interface/range {v42 .. v42}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v12

    invoke-virtual/range {v3 .. v12}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_3
    move-object/from16 v3, v42

    .line 1672330
    check-cast v3, LX/0jB;

    invoke-interface {v3}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v17

    goto :goto_1

    .line 1672331
    :cond_4
    invoke-direct/range {p0 .. p0}, LX/AR3;->f()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_5
    move-object/from16 v3, v42

    .line 1672332
    check-cast v3, LX/0j6;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-wide v6, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    cmp-long v3, v6, v4

    if-nez v3, :cond_9

    .line 1672333
    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v3}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v13

    .line 1672334
    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v3}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v14

    .line 1672335
    move-object/from16 v0, p0

    iget-object v12, v0, LX/AR3;->f:LX/8LV;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v3}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v3, v42

    check-cast v3, LX/0ip;

    invoke-interface {v3}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v16

    new-instance v18, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object/from16 v3, v42

    check-cast v3, LX/0iq;

    invoke-interface {v3}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/AR3;->n:J

    move-wide/from16 v20, v0

    move-object/from16 v3, v42

    check-cast v3, LX/0j8;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v3, v42

    check-cast v3, LX/0j8;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v23

    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v3}, LX/7kq;->w(LX/0Px;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->m:LX/7Dh;

    invoke-virtual {v3}, LX/7Dh;->b()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v24, 0x1

    :goto_2
    move-object/from16 v3, v42

    check-cast v3, LX/0j0;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v3, v42

    check-cast v3, LX/0jE;

    invoke-interface {v3}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v26

    move-object/from16 v3, v42

    check-cast v3, LX/0iy;

    invoke-interface {v3}, LX/0iy;->getMarketplaceId()J

    move-result-wide v27

    invoke-interface/range {v42 .. v42}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v29

    move-object/from16 v3, v42

    check-cast v3, LX/0ix;

    invoke-interface {v3}, LX/0ix;->isUserSelectedTags()Z

    move-result v30

    move-object/from16 v3, v42

    check-cast v3, LX/0j8;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    if-eqz v3, :cond_7

    const/16 v31, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v3}, LX/7kq;->m(LX/0Px;)Z

    move-result v33

    move-object/from16 v3, v42

    check-cast v3, LX/0iw;

    invoke-interface {v3}, LX/0iw;->isFeedOnlyPost()Z

    move-result v34

    move-object/from16 v3, v42

    check-cast v3, LX/0ir;

    invoke-interface {v3}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v35

    move-object/from16 v3, v42

    check-cast v3, LX/0jH;

    invoke-interface {v3}, LX/0jH;->getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v36

    move-object/from16 v3, v42

    check-cast v3, LX/0is;

    invoke-static {v3}, LX/87S;->a(LX/0is;)LX/0Px;

    move-result-object v37

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->e:LX/0SG;

    invoke-static {v2, v3}, LX/AR5;->a(LX/0io;LX/0SG;)Lcom/facebook/audience/model/UploadShot;

    move-result-object v38

    move-object/from16 v2, v42

    check-cast v2, LX/0jG;

    invoke-interface {v2}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    if-eqz v2, :cond_8

    check-cast v42, LX/0jG;

    invoke-interface/range {v42 .. v42}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a()Ljava/lang/String;

    move-result-object v39

    :goto_4
    move-object/from16 v32, v11

    invoke-virtual/range {v12 .. v39}, LX/8LV;->a(LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/0Px;JLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    const/16 v24, 0x0

    goto/16 :goto_2

    :cond_7
    const/16 v31, 0x0

    goto :goto_3

    :cond_8
    const/16 v39, 0x0

    goto :goto_4

    :cond_9
    move-object/from16 v3, v42

    .line 1672336
    check-cast v3, LX/0j6;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-wide v6, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, LX/AR3;->o:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_14

    move-object/from16 v3, v42

    .line 1672337
    check-cast v3, LX/0j4;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 1672338
    check-cast v2, LX/0ik;

    invoke-interface {v2}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2rg;

    check-cast v2, LX/5RE;

    invoke-interface {v2}, LX/5RE;->I()LX/5RF;

    move-result-object v2

    sget-object v3, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v2, v3, :cond_c

    .line 1672339
    move-object/from16 v0, p0

    iget-object v0, v0, LX/AR3;->f:LX/8LV;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v21

    move-object/from16 v2, v42

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-wide v0, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v2, v42

    check-cast v2, LX/0ip;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v25

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/AR3;->n:J

    move-wide/from16 v27, v0

    move-object/from16 v2, v42

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v2, v42

    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v30

    move-object/from16 v2, v42

    check-cast v2, LX/0jF;

    invoke-interface {v2}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v31

    move-object/from16 v2, v42

    check-cast v2, LX/0iz;

    invoke-interface {v2}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_a

    move-object/from16 v2, v42

    check-cast v2, LX/0iz;

    invoke-interface {v2}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v32

    :goto_5
    const/16 v34, 0x0

    move-object/from16 v2, v42

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_b

    const/16 v36, 0x1

    :goto_6
    move-object/from16 v2, v42

    check-cast v2, LX/0ir;

    invoke-interface {v2}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v37

    check-cast v42, LX/0jA;

    invoke-interface/range {v42 .. v42}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v38

    move-object/from16 v26, v17

    move-object/from16 v35, v11

    invoke-virtual/range {v19 .. v38}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JLjava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Ljava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;LX/5Ra;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_a
    const-wide/16 v32, 0x0

    goto :goto_5

    :cond_b
    const/16 v36, 0x0

    goto :goto_6

    .line 1672340
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, LX/AR3;->f:LX/8LV;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v21

    move-object/from16 v2, v42

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-wide v0, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v2, v42

    check-cast v2, LX/0ip;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v25

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/AR3;->n:J

    move-wide/from16 v27, v0

    move-object/from16 v2, v42

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-object/from16 v2, v42

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v29

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->w(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->m:LX/7Dh;

    invoke-virtual {v2}, LX/7Dh;->b()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v30, 0x1

    :goto_7
    move-object/from16 v2, v42

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v2, v42

    check-cast v2, LX/0j4;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v32

    move-object/from16 v2, v42

    check-cast v2, LX/0jF;

    invoke-interface {v2}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v33

    move-object/from16 v2, v42

    check-cast v2, LX/0iz;

    invoke-interface {v2}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_e

    move-object/from16 v2, v42

    check-cast v2, LX/0iz;

    invoke-interface {v2}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v34

    :goto_8
    const/16 v36, 0x0

    move-object/from16 v2, v42

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_f

    const/16 v38, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->m(LX/0Px;)Z

    move-result v39

    move-object/from16 v2, v42

    check-cast v2, LX/0ir;

    invoke-interface {v2}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v40

    move-object/from16 v2, v42

    check-cast v2, LX/0jG;

    invoke-interface {v2}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    if-eqz v2, :cond_10

    check-cast v42, LX/0jG;

    invoke-interface/range {v42 .. v42}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a()Ljava/lang/String;

    move-result-object v41

    :goto_a
    move-object/from16 v26, v17

    move-object/from16 v37, v11

    invoke-virtual/range {v19 .. v41}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JZZLjava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Ljava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_d
    const/16 v30, 0x0

    goto :goto_7

    :cond_e
    const-wide/16 v34, 0x0

    goto :goto_8

    :cond_f
    const/16 v38, 0x0

    goto :goto_9

    :cond_10
    const/16 v41, 0x0

    goto :goto_a

    .line 1672341
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, LX/AR3;->f:LX/8LV;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v22

    move-object/from16 v2, v42

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-wide v0, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-wide/from16 v23, v0

    move-object/from16 v2, v42

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v2, v42

    check-cast v2, LX/0ip;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v27

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/AR3;->n:J

    move-wide/from16 v30, v0

    move-object/from16 v2, v42

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_12

    const/16 v32, 0x1

    :goto_b
    move-object/from16 v2, v42

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v2, v42

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v34

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->w(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->m:LX/7Dh;

    invoke-virtual {v2}, LX/7Dh;->b()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v35, 0x1

    :goto_c
    move-object/from16 v2, v42

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v2, v42

    check-cast v2, LX/0jE;

    invoke-interface {v2}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v37

    move-object/from16 v2, v42

    check-cast v2, LX/0iy;

    invoke-interface {v2}, LX/0iy;->getMarketplaceId()J

    move-result-wide v38

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->o:LX/0Px;

    invoke-static {v2}, LX/7kq;->m(LX/0Px;)Z

    move-result v41

    check-cast v42, LX/0ir;

    invoke-interface/range {v42 .. v42}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isGroupMemberBioPost()Z

    move-result v43

    move-object/from16 v28, v17

    move-object/from16 v29, v19

    move-object/from16 v40, v11

    invoke-virtual/range {v20 .. v43}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;LX/0Px;JZLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Z)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_12
    const/16 v32, 0x0

    goto :goto_b

    :cond_13
    const/16 v35, 0x0

    goto :goto_c

    .line 1672342
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AR3;->g:LX/03V;

    const-string v3, "photo upload error"

    const-string v6, "Unsupported upload type target=%d, user=%d, attach=%s"

    check-cast v42, LX/0j6;

    invoke-interface/range {v42 .. v42}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    iget-wide v8, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/AR3;->o:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v6, v7, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672343
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1672344
    iget-object v0, p0, LX/AR3;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672345
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->p(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1672346
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->d(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1672347
    const v0, 0x7f08142f

    .line 1672348
    :goto_0
    iget-object v1, p0, LX/AR3;->k:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1672349
    :cond_0
    return-void

    .line 1672350
    :cond_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 1672351
    const v0, 0x7f081416

    goto :goto_0

    .line 1672352
    :cond_2
    const v0, 0x7f081415

    goto :goto_0

    .line 1672353
    :cond_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->r(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1672354
    const v0, 0x7f081418

    goto :goto_0

    .line 1672355
    :cond_4
    invoke-direct {p0}, LX/AR3;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1672356
    const v0, 0x7f081417

    goto :goto_0

    .line 1672357
    :cond_5
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->m(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1672358
    const v0, 0x7f081419

    goto :goto_0
.end method
