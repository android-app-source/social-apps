.class public final LX/93g;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final synthetic b:LX/93i;


# direct methods
.method public constructor <init>(LX/93i;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 0

    .prologue
    .line 1434023
    iput-object p1, p0, LX/93g;->b:LX/93i;

    iput-object p2, p0, LX/93g;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1434024
    new-instance v0, LX/7lP;

    iget-object v1, p0, LX/93g;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v0, v1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    const/4 v1, 0x0

    .line 1434025
    iput-boolean v1, v0, LX/7lP;->b:Z

    .line 1434026
    move-object v0, v0

    .line 1434027
    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434028
    iget-object v1, p0, LX/93g;->b:LX/93i;

    invoke-virtual {v1, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434029
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_0

    .line 1434030
    iget-object v0, p0, LX/93g;->b:LX/93i;

    .line 1434031
    iget-object v1, v0, LX/93Q;->b:LX/03V;

    move-object v0, v1

    .line 1434032
    const-string v1, "composer_group_fetch_error"

    const-string v2, "Failed to fetch group data for composer"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1434033
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1434034
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 1434035
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1434036
    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;

    .line 1434037
    if-nez v0, :cond_0

    .line 1434038
    new-instance v0, LX/7lP;

    iget-object v1, p0, LX/93g;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v0, v1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434039
    iput-boolean v4, v0, LX/7lP;->b:Z

    .line 1434040
    move-object v0, v0

    .line 1434041
    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434042
    iget-object v1, p0, LX/93g;->b:LX/93i;

    invoke-virtual {v1, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434043
    :goto_0
    return-void

    .line 1434044
    :cond_0
    const/4 v1, 0x0

    .line 1434045
    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->d()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1434046
    new-instance v1, LX/4Wm;

    invoke-direct {v1}, LX/4Wm;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->d()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1434047
    iput-object v2, v1, LX/4Wm;->R:Ljava/lang/String;

    .line 1434048
    move-object v1, v1

    .line 1434049
    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->d()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$ParentGroupModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1434050
    iput-object v2, v1, LX/4Wm;->ac:Ljava/lang/String;

    .line 1434051
    move-object v1, v1

    .line 1434052
    invoke-virtual {v1}, LX/4Wm;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    .line 1434053
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->c()Z

    move-result v5

    .line 1434054
    iget-object v2, p0, LX/93g;->b:LX/93i;

    iget-object v2, v2, LX/93i;->d:LX/0ad;

    sget-short v3, LX/1EB;->ad:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v6

    .line 1434055
    if-eqz v5, :cond_3

    iget-object v2, p0, LX/93g;->b:LX/93i;

    .line 1434056
    invoke-static {v2, v0}, LX/93i;->a(LX/93i;Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1434057
    :goto_1
    new-instance v7, LX/7lN;

    iget-object v3, p0, LX/93g;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v3, v3, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-direct {v7, v3}, LX/7lN;-><init>(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)V

    if-eqz v6, :cond_5

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->e()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->c()LX/1Fd;

    move-result-object v3

    invoke-interface {v3}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    .line 1434058
    :goto_2
    iput-object v3, v7, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434059
    move-object v3, v7

    .line 1434060
    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->e()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1434061
    :cond_2
    iput-object v2, v3, LX/7lN;->b:Ljava/lang/String;

    .line 1434062
    move-object v2, v3

    .line 1434063
    if-eqz v6, :cond_7

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->e()Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel$PostedItemPrivacyScopeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1434064
    :goto_3
    iput-object v0, v2, LX/7lN;->c:Ljava/lang/String;

    .line 1434065
    move-object v0, v2

    .line 1434066
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434067
    new-instance v1, LX/7lP;

    iget-object v2, p0, LX/93g;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v1, v2}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434068
    iput-boolean v4, v1, LX/7lP;->b:Z

    .line 1434069
    move-object v1, v1

    .line 1434070
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434071
    iget-object v1, p0, LX/93g;->b:LX/93i;

    invoke-virtual {v1, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto/16 :goto_0

    .line 1434072
    :cond_3
    iget-object v2, p0, LX/93g;->b:LX/93i;

    .line 1434073
    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1434074
    sget-object v3, LX/93h;->a:[I

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v7

    aget v3, v3, v7

    packed-switch v3, :pswitch_data_0

    .line 1434075
    :cond_4
    const/4 v3, 0x0

    :goto_4
    move-object v3, v3

    .line 1434076
    move-object v2, v3

    .line 1434077
    goto :goto_1

    .line 1434078
    :cond_5
    iget-object v8, p0, LX/93g;->b:LX/93i;

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v9

    if-eqz v1, :cond_6

    const/4 v3, 0x1

    :goto_5
    invoke-static {v8, v9, v3, v5}, LX/93i;->a$redex0(LX/93i;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZZ)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    goto :goto_2

    :cond_6
    move v3, v4

    goto :goto_5

    :cond_7
    iget-object v3, p0, LX/93g;->b:LX/93i;

    .line 1434079
    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 1434080
    sget-object v6, LX/93h;->a:[I

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->gn_()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 1434081
    :cond_8
    const/4 v6, 0x0

    :goto_6
    move-object v0, v6

    .line 1434082
    goto :goto_3

    .line 1434083
    :pswitch_0
    iget-object v3, v2, LX/93i;->c:Landroid/content/res/Resources;

    const v7, 0x7f08132b

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 1434084
    :pswitch_1
    iget-object v3, v2, LX/93i;->c:Landroid/content/res/Resources;

    const v7, 0x7f08132a

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 1434085
    :pswitch_2
    if-eqz v1, :cond_a

    .line 1434086
    iget-object v7, v2, LX/93i;->c:Landroid/content/res/Resources;

    const v8, 0x7f08132e

    const/4 v3, 0x1

    new-array v9, v3, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-boolean v3, v2, LX/93i;->e:Z

    if-eqz v3, :cond_9

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->b()Ljava/lang/String;

    move-result-object v3

    :goto_7
    aput-object v3, v9, p1

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 1434087
    :cond_a
    iget-object v3, v2, LX/93i;->c:Landroid/content/res/Resources;

    const v7, 0x7f08131d

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 1434088
    :pswitch_3
    if-eqz v5, :cond_b

    .line 1434089
    iget-object v6, v3, LX/93i;->c:Landroid/content/res/Resources;

    const v7, 0x7f08133c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 1434090
    :cond_b
    iget-object v6, v3, LX/93i;->c:Landroid/content/res/Resources;

    const v7, 0x7f081320

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 1434091
    :pswitch_4
    if-eqz v1, :cond_d

    .line 1434092
    iget-object v7, v3, LX/93i;->c:Landroid/content/res/Resources;

    const v8, 0x7f081321

    const/4 v6, 0x1

    new-array v9, v6, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-boolean v6, v3, LX/93i;->e:Z

    if-eqz v6, :cond_c

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchGroupDetailsModels$GroupDetailsQueryModel;->b()Ljava/lang/String;

    move-result-object v6

    :goto_8
    aput-object v6, v9, p1

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    :cond_c
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v6

    goto :goto_8

    .line 1434093
    :cond_d
    iget-object v6, v3, LX/93i;->c:Landroid/content/res/Resources;

    const v7, 0x7f081322

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
