.class public final LX/9Wy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Iwv;

.field public final synthetic b:LX/9Wz;


# direct methods
.method public constructor <init>(LX/9Wz;LX/Iwv;)V
    .locals 0

    .prologue
    .line 1502129
    iput-object p1, p0, LX/9Wy;->b:LX/9Wz;

    iput-object p2, p0, LX/9Wy;->a:LX/Iwv;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 0

    .prologue
    .line 1502169
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1502168
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1502130
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1502131
    if-eqz p1, :cond_0

    .line 1502132
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1502133
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 1502134
    :goto_2
    return-void

    .line 1502135
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1502136
    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1502137
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1502138
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1502139
    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1502140
    const/4 v4, 0x2

    const-class v5, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v3, v0, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    .line 1502141
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1502142
    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3b86340b

    invoke-static {v2, v0, v1, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1502143
    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1502144
    :goto_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1502145
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1502146
    :try_start_0
    invoke-static {v3, v0}, LX/9Wz;->a(LX/15i;I)Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v0

    .line 1502147
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 1502148
    :catch_0
    move-exception v0

    .line 1502149
    iget-object v3, p0, LX/9Wy;->b:LX/9Wz;

    iget-object v3, v3, LX/9Wz;->c:LX/2Sc;

    invoke-virtual {v3, v0}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_4

    .line 1502150
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 1502151
    :cond_7
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1502152
    iget-object v1, p0, LX/9Wy;->a:LX/Iwv;

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1502153
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1502154
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1502155
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1502156
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1502157
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1502158
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    .line 1502159
    iput-object v0, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->f:LX/0Px;

    .line 1502160
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->k:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    .line 1502161
    if-eqz v0, :cond_8

    .line 1502162
    iput-object v0, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->b:LX/0Px;

    .line 1502163
    :cond_8
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->k:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 1502164
    :goto_5
    goto/16 :goto_2

    .line 1502165
    :cond_9
    iget-object v2, v1, LX/Iwv;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1502166
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_5

    .line 1502167
    :cond_a
    iget-object v2, v1, LX/Iwv;->b:LX/Iwy;

    iget-object v2, v2, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_5
.end method
