.class public final LX/ACZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V
    .locals 1

    .prologue
    .line 1642842
    iput-object p1, p0, LX/ACZ;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1642843
    const/4 v0, 0x0

    iput-object v0, p0, LX/ACZ;->b:Ljava/lang/String;

    .line 1642844
    const/4 v0, 0x0

    iput v0, p0, LX/ACZ;->c:I

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 1642845
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/ACZ;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1642846
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/ACZ;->b:Ljava/lang/String;

    .line 1642847
    iget-object v0, p0, LX/ACZ;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v1, p0, LX/ACZ;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1642848
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1642849
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1642850
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->h:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1642851
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a$redex0(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Z)V

    .line 1642852
    :goto_0
    iget-object v0, p0, LX/ACZ;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    array-length v0, v0

    iget v1, p0, LX/ACZ;->c:I

    if-eq v0, v1, :cond_0

    .line 1642853
    iget-object v0, p0, LX/ACZ;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    array-length v0, v0

    iput v0, p0, LX/ACZ;->c:I

    .line 1642854
    iget-object v0, p0, LX/ACZ;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-static {v0}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->n(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    .line 1642855
    :cond_0
    return-void

    .line 1642856
    :cond_1
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->h:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1642857
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c:LX/1Ck;

    sget-object v3, LX/ACc;->FETCH_TYPEAHEAD_RESULTS:LX/ACc;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p1, LX/ACb;

    invoke-direct {p1, v0}, LX/ACb;-><init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V

    invoke-virtual {v2, v3, v4, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1642858
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a:LX/8tB;

    invoke-virtual {v2}, LX/3Tf;->a()LX/333;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->i:LX/3NY;

    invoke-interface {v2, v1, v3}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1642859
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1642860
    if-ge p4, p3, :cond_0

    .line 1642861
    iget-object v0, p0, LX/ACZ;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->f:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1642862
    :cond_0
    return-void
.end method
