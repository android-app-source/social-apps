.class public LX/9hz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
        "LX/5kO;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)V
    .locals 1

    .prologue
    .line 1527113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527114
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/9hz;->a:Ljava/lang/String;

    .line 1527115
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    iput-object v0, p0, LX/9hz;->b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    .line 1527116
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527111
    iget-object v0, p0, LX/9hz;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1527117
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    check-cast p2, LX/5kO;

    .line 1527118
    iget-object v0, p0, LX/9hz;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aN()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1527119
    :cond_0
    :goto_0
    return-void

    .line 1527120
    :cond_1
    iget-object v0, p0, LX/9hz;->b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    invoke-static {p1, v0}, LX/9hj;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5kO;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527112
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1527110
    const-string v0, "RemoveTagMetadataMutatingVisitor"

    return-object v0
.end method
