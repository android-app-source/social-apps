.class public LX/9na;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements LX/5qf;
.implements LX/5qg;
.implements LX/5r7;
.implements LX/5rB;


# static fields
.field private static final a:Landroid/view/ViewGroup$LayoutParams;

.field private static final b:Landroid/graphics/Rect;


# instance fields
.field private c:Z

.field private d:[Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field private f:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/5r3;

.field private i:LX/9nZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:LX/9nY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/5qd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1537687
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    sput-object v0, LX/9na;->a:Landroid/view/ViewGroup$LayoutParams;

    .line 1537688
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/9na;->b:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1537689
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 1537690
    iput-boolean v1, p0, LX/9na;->c:Z

    .line 1537691
    const/4 v0, 0x0

    iput-object v0, p0, LX/9na;->d:[Landroid/view/View;

    .line 1537692
    sget-object v0, LX/5r3;->AUTO:LX/5r3;

    iput-object v0, p0, LX/9na;->h:LX/5r3;

    .line 1537693
    iput-boolean v1, p0, LX/9na;->l:Z

    .line 1537694
    return-void
.end method

.method private a(Landroid/graphics/Rect;II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1537695
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    aget-object v0, v0, p2

    .line 1537696
    sget-object v3, LX/9na;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 1537697
    sget-object v3, LX/9na;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sget-object v4, LX/9na;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sget-object v5, LX/9na;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sget-object v6, LX/9na;->b:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v4

    .line 1537698
    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v3

    .line 1537699
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    .line 1537700
    :goto_0
    if-nez v4, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_3

    if-nez v3, :cond_3

    .line 1537701
    sub-int v2, p2, p3

    invoke-super {p0, v2, v1}, Landroid/view/ViewGroup;->removeViewsInLayout(II)V

    .line 1537702
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 1537703
    instance-of v1, v0, LX/5r7;

    if-eqz v1, :cond_1

    .line 1537704
    check-cast v0, LX/5r7;

    .line 1537705
    invoke-interface {v0}, LX/5r7;->getRemoveClippedSubviews()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1537706
    invoke-interface {v0}, LX/5r7;->a()V

    .line 1537707
    :cond_1
    return-void

    :cond_2
    move v3, v2

    .line 1537708
    goto :goto_0

    .line 1537709
    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_4

    .line 1537710
    sub-int v2, p2, p3

    sget-object v3, LX/9na;->a:Landroid/view/ViewGroup$LayoutParams;

    invoke-super {p0, v0, v2, v3, v1}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 1537711
    invoke-virtual {p0}, LX/9na;->invalidate()V

    goto :goto_1

    .line 1537712
    :cond_4
    if-nez v4, :cond_0

    move v1, v2

    goto :goto_1
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1537713
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    .line 1537714
    iget v1, p0, LX/9na;->e:I

    .line 1537715
    add-int/lit8 v2, v1, -0x1

    if-ne p1, v2, :cond_0

    .line 1537716
    iget v1, p0, LX/9na;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/9na;->e:I

    aput-object v3, v0, v1

    .line 1537717
    :goto_0
    return-void

    .line 1537718
    :cond_0
    if-ltz p1, :cond_1

    if-ge p1, v1, :cond_1

    .line 1537719
    add-int/lit8 v2, p1, 0x1

    sub-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v2, v0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1537720
    iget v1, p0, LX/9na;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/9na;->e:I

    aput-object v3, v0, v1

    goto :goto_0

    .line 1537721
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public static b(LX/9na;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1537722
    iget-boolean v1, p0, LX/9na;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/9na;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1537723
    :cond_0
    :goto_0
    return-void

    .line 1537724
    :cond_1
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537725
    iget-object v1, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537726
    sget-object v1, LX/9na;->b:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1537727
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    sget-object v2, LX/9na;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sget-object v3, LX/9na;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sget-object v4, LX/9na;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sget-object v5, LX/9na;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v2

    .line 1537728
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 1537729
    :goto_1
    if-eq v2, v1, :cond_0

    move v1, v0

    .line 1537730
    :goto_2
    iget v2, p0, LX/9na;->e:I

    if-ge v0, v2, :cond_0

    .line 1537731
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_3

    .line 1537732
    iget-object v2, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v0, v1}, LX/9na;->a(Landroid/graphics/Rect;II)V

    goto :goto_0

    :cond_2
    move v1, v0

    .line 1537733
    goto :goto_1

    .line 1537734
    :cond_3
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_4

    .line 1537735
    add-int/lit8 v1, v1, 0x1

    .line 1537736
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public static b(LX/9na;Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1537737
    iget-boolean v1, p0, LX/9na;->c:Z

    invoke-static {v1}, LX/0nE;->a(Z)V

    .line 1537738
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537739
    iget-object v1, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537740
    invoke-direct {p0, p1, p2}, LX/9na;->c(Landroid/view/View;I)V

    move v1, v0

    .line 1537741
    :goto_0
    if-ge v1, p2, :cond_1

    .line 1537742
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1537743
    add-int/lit8 v0, v0, 0x1

    .line 1537744
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1537745
    :cond_1
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v1, p2, v0}, LX/9na;->a(Landroid/graphics/Rect;II)V

    .line 1537746
    iget-object v0, p0, LX/9na;->i:LX/9nZ;

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1537747
    return-void
.end method

.method private b(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1537748
    iget-object v1, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .line 1537749
    :goto_0
    iget v2, p0, LX/9na;->e:I

    if-ge v0, v2, :cond_1

    .line 1537750
    invoke-direct {p0, p1, v0, v1}, LX/9na;->a(Landroid/graphics/Rect;II)V

    .line 1537751
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1537752
    add-int/lit8 v1, v1, 0x1

    .line 1537753
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1537754
    :cond_1
    return-void
.end method

.method private c(Landroid/view/View;)I
    .locals 4

    .prologue
    .line 1537755
    iget v2, p0, LX/9na;->e:I

    .line 1537756
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    .line 1537757
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1537758
    aget-object v3, v0, v1

    if-ne v3, p1, :cond_0

    move v0, v1

    .line 1537759
    :goto_1
    return v0

    .line 1537760
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1537761
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private c(Landroid/view/View;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1537762
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    .line 1537763
    iget v1, p0, LX/9na;->e:I

    .line 1537764
    array-length v2, v0

    .line 1537765
    if-ne p2, v1, :cond_1

    .line 1537766
    if-ne v2, v1, :cond_0

    .line 1537767
    add-int/lit8 v1, v2, 0xc

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, LX/9na;->d:[Landroid/view/View;

    .line 1537768
    iget-object v1, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1537769
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    .line 1537770
    :cond_0
    iget v1, p0, LX/9na;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/9na;->e:I

    aput-object p1, v0, v1

    .line 1537771
    :goto_0
    return-void

    .line 1537772
    :cond_1
    if-ge p2, v1, :cond_3

    .line 1537773
    if-ne v2, v1, :cond_2

    .line 1537774
    add-int/lit8 v2, v2, 0xc

    new-array v2, v2, [Landroid/view/View;

    iput-object v2, p0, LX/9na;->d:[Landroid/view/View;

    .line 1537775
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1537776
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    add-int/lit8 v3, p2, 0x1

    sub-int/2addr v1, p2

    invoke-static {v0, p2, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1537777
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    .line 1537778
    :goto_1
    aput-object p1, v0, p2

    .line 1537779
    iget v0, p0, LX/9na;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/9na;->e:I

    goto :goto_0

    .line 1537780
    :cond_2
    add-int/lit8 v2, p2, 0x1

    sub-int/2addr v1, p2

    invoke-static {v0, p2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 1537781
    :cond_3
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "index="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getOrCreateReactViewBackground()LX/9nY;
    .locals 5

    .prologue
    .line 1537819
    iget-object v0, p0, LX/9na;->j:LX/9nY;

    if-nez v0, :cond_0

    .line 1537820
    new-instance v0, LX/9nY;

    invoke-direct {v0}, LX/9nY;-><init>()V

    iput-object v0, p0, LX/9na;->j:LX/9nY;

    .line 1537821
    invoke-virtual {p0}, LX/9na;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1537822
    const/4 v1, 0x0

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1537823
    if-nez v0, :cond_1

    .line 1537824
    iget-object v0, p0, LX/9na;->j:LX/9nY;

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1537825
    :cond_0
    :goto_0
    iget-object v0, p0, LX/9na;->j:LX/9nY;

    return-object v0

    .line 1537826
    :cond_1
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    iget-object v4, p0, LX/9na;->j:LX/9nY;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1537827
    invoke-super {p0, v1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1537782
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1537783
    iget-boolean v0, p0, LX/9na;->c:Z

    if-nez v0, :cond_0

    .line 1537784
    :goto_0
    return-void

    .line 1537785
    :cond_0
    iget-object v0, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537786
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537787
    iget-object v0, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-static {p0, v0}, LX/5r8;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1537788
    iget-object v0, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v0}, LX/9na;->b(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final a(FI)V
    .locals 1

    .prologue
    .line 1537789
    invoke-direct {p0}, LX/9na;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/9nY;->a(FI)V

    .line 1537790
    return-void
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 1537791
    invoke-direct {p0}, LX/9na;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/9nY;->a(IF)V

    .line 1537792
    return-void
.end method

.method public final a(IFF)V
    .locals 1

    .prologue
    .line 1537793
    invoke-direct {p0}, LX/9na;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LX/9nY;->a(IFF)V

    .line 1537794
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1537795
    iget-object v0, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1537796
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1537797
    iget-boolean v1, p0, LX/9na;->c:Z

    invoke-static {v1}, LX/0nE;->a(Z)V

    .line 1537798
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537799
    iget-object v1, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537800
    iget-object v1, p0, LX/9na;->i:LX/9nZ;

    invoke-virtual {p1, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1537801
    invoke-direct {p0, p1}, LX/9na;->c(Landroid/view/View;)I

    move-result v2

    .line 1537802
    iget-object v1, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v0

    .line 1537803
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1537804
    iget-object v3, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_0

    .line 1537805
    add-int/lit8 v0, v0, 0x1

    .line 1537806
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1537807
    :cond_1
    sub-int v0, v2, v0

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Landroid/view/ViewGroup;->removeViewsInLayout(II)V

    .line 1537808
    :cond_2
    invoke-direct {p0, v2}, LX/9na;->b(I)V

    .line 1537809
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1537810
    iget-boolean v0, p0, LX/9na;->c:Z

    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1537811
    iget-object v0, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 1537812
    :goto_0
    iget v2, p0, LX/9na;->e:I

    if-ge v0, v2, :cond_0

    .line 1537813
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v2, v2, v0

    iget-object v3, p0, LX/9na;->i:LX/9nZ;

    invoke-virtual {v2, v3}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1537814
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1537815
    :cond_0
    invoke-virtual {p0}, LX/9na;->removeAllViewsInLayout()V

    .line 1537816
    iput v1, p0, LX/9na;->e:I

    .line 1537817
    return-void
.end method

.method public final dispatchSetPressed(Z)V
    .locals 0

    .prologue
    .line 1537818
    return-void
.end method

.method public getAllChildrenCount()I
    .locals 1

    .prologue
    .line 1537682
    iget v0, p0, LX/9na;->e:I

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1
    .annotation build Lcom/facebook/react/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1537683
    invoke-virtual {p0}, LX/9na;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1537684
    invoke-virtual {p0}, LX/9na;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/9nY;

    .line 1537685
    iget p0, v0, LX/9nY;->n:I

    move v0, p0

    .line 1537686
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHitSlopRect()Landroid/graphics/Rect;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1537610
    iget-object v0, p0, LX/9na;->g:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPointerEvents()LX/5r3;
    .locals 1

    .prologue
    .line 1537629
    iget-object v0, p0, LX/9na;->h:LX/5r3;

    return-object v0
.end method

.method public getRemoveClippedSubviews()Z
    .locals 1

    .prologue
    .line 1537628
    iget-boolean v0, p0, LX/9na;->c:Z

    return v0
.end method

.method public final hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 1537627
    iget-boolean v0, p0, LX/9na;->l:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x41d9b2ed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1537623
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 1537624
    iget-boolean v1, p0, LX/9na;->c:Z

    if-eqz v1, :cond_0

    .line 1537625
    invoke-virtual {p0}, LX/9na;->a()V

    .line 1537626
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x6a5dd2d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1537619
    iget-object v1, p0, LX/9na;->k:LX/5qd;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9na;->k:LX/5qd;

    invoke-interface {v1, p0, p1}, LX/5qd;->a(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1537620
    :cond_0
    :goto_0
    return v0

    .line 1537621
    :cond_1
    iget-object v1, p0, LX/9na;->h:LX/5r3;

    sget-object v2, LX/5r3;->NONE:LX/5r3;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/9na;->h:LX/5r3;

    sget-object v2, LX/5r3;->BOX_ONLY:LX/5r3;

    if-eq v1, v2, :cond_0

    .line 1537622
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1537618
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1537615
    invoke-static {p1, p2}, LX/5qs;->a(II)V

    .line 1537616
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/9na;->setMeasuredDimension(II)V

    .line 1537617
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xdf7752d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1537611
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1537612
    iget-boolean v1, p0, LX/9na;->c:Z

    if-eqz v1, :cond_0

    .line 1537613
    invoke-virtual {p0}, LX/9na;->a()V

    .line 1537614
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x6fc00f30

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x2

    const v1, 0x4d4f1883    # 2.17155632E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1537607
    iget-object v2, p0, LX/9na;->h:LX/5r3;

    sget-object v3, LX/5r3;->NONE:LX/5r3;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/9na;->h:LX/5r3;

    sget-object v3, LX/5r3;->BOX_NONE:LX/5r3;

    if-ne v2, v3, :cond_1

    .line 1537608
    :cond_0
    const/4 v0, 0x0

    const v2, -0x657dcf72

    invoke-static {v4, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1537609
    :goto_0
    return v0

    :cond_1
    const v2, 0x17e3b892

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final requestLayout()V
    .locals 0

    .prologue
    .line 1537630
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1537631
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported for ReactViewGroup instances"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 1537632
    if-nez p1, :cond_0

    iget-object v0, p0, LX/9na;->j:LX/9nY;

    if-eqz v0, :cond_1

    .line 1537633
    :cond_0
    invoke-direct {p0}, LX/9na;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(I)V

    .line 1537634
    :cond_1
    return-void
.end method

.method public setBorderRadius(F)V
    .locals 1

    .prologue
    .line 1537635
    invoke-direct {p0}, LX/9na;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(F)V

    .line 1537636
    return-void
.end method

.method public setBorderStyle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1537637
    invoke-direct {p0}, LX/9na;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(Ljava/lang/String;)V

    .line 1537638
    return-void
.end method

.method public setHitSlopRect(Landroid/graphics/Rect;)V
    .locals 0
    .param p1    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1537639
    iput-object p1, p0, LX/9na;->g:Landroid/graphics/Rect;

    .line 1537640
    return-void
.end method

.method public setNeedsOffscreenAlphaCompositing(Z)V
    .locals 0

    .prologue
    .line 1537641
    iput-boolean p1, p0, LX/9na;->l:Z

    .line 1537642
    return-void
.end method

.method public setOnInterceptTouchEventListener(LX/5qd;)V
    .locals 0

    .prologue
    .line 1537680
    iput-object p1, p0, LX/9na;->k:LX/5qd;

    .line 1537681
    return-void
.end method

.method public setPointerEvents(LX/5r3;)V
    .locals 0

    .prologue
    .line 1537643
    iput-object p1, p0, LX/9na;->h:LX/5r3;

    .line 1537644
    return-void
.end method

.method public setRemoveClippedSubviews(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1537645
    iget-boolean v1, p0, LX/9na;->c:Z

    if-ne p1, v1, :cond_0

    .line 1537646
    :goto_0
    return-void

    .line 1537647
    :cond_0
    iput-boolean p1, p0, LX/9na;->c:Z

    .line 1537648
    if-eqz p1, :cond_2

    .line 1537649
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    .line 1537650
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-static {p0, v1}, LX/5r8;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1537651
    invoke-virtual {p0}, LX/9na;->getChildCount()I

    move-result v1

    iput v1, p0, LX/9na;->e:I

    .line 1537652
    const/16 v1, 0xc

    iget v2, p0, LX/9na;->e:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1537653
    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, LX/9na;->d:[Landroid/view/View;

    .line 1537654
    new-instance v1, LX/9nZ;

    invoke-direct {v1, p0}, LX/9nZ;-><init>(LX/9na;)V

    iput-object v1, p0, LX/9na;->i:LX/9nZ;

    .line 1537655
    :goto_1
    iget v1, p0, LX/9na;->e:I

    if-ge v0, v1, :cond_1

    .line 1537656
    invoke-virtual {p0, v0}, LX/9na;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1537657
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    aput-object v1, v2, v0

    .line 1537658
    iget-object v2, p0, LX/9na;->i:LX/9nZ;

    invoke-virtual {v1, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1537659
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1537660
    :cond_1
    invoke-virtual {p0}, LX/9na;->a()V

    goto :goto_0

    .line 1537661
    :cond_2
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537662
    iget-object v1, p0, LX/9na;->d:[Landroid/view/View;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537663
    iget-object v1, p0, LX/9na;->i:LX/9nZ;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .line 1537664
    :goto_2
    iget v2, p0, LX/9na;->e:I

    if-ge v1, v2, :cond_3

    .line 1537665
    iget-object v2, p0, LX/9na;->d:[Landroid/view/View;

    aget-object v2, v2, v1

    iget-object v3, p0, LX/9na;->i:LX/9nZ;

    invoke-virtual {v2, v3}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1537666
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1537667
    :cond_3
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, LX/9na;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1537668
    iget-object v1, p0, LX/9na;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, LX/9na;->b(Landroid/graphics/Rect;)V

    .line 1537669
    iput-object v4, p0, LX/9na;->d:[Landroid/view/View;

    .line 1537670
    iput-object v4, p0, LX/9na;->f:Landroid/graphics/Rect;

    .line 1537671
    iput v0, p0, LX/9na;->e:I

    .line 1537672
    iput-object v4, p0, LX/9na;->i:LX/9nZ;

    goto :goto_0
.end method

.method public setTranslucentBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1537673
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1537674
    iget-object v0, p0, LX/9na;->j:LX/9nY;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1537675
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    iget-object v3, p0, LX/9na;->j:LX/9nY;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1537676
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1537677
    :cond_0
    :goto_0
    return-void

    .line 1537678
    :cond_1
    if-eqz p1, :cond_0

    .line 1537679
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
