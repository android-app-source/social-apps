.class public final LX/A7u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/A7s;


# instance fields
.field public final synthetic a:Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;)V
    .locals 0

    .prologue
    .line 1626803
    iput-object p1, p0, LX/A7u;->a:Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1626804
    iget-object v0, p0, LX/A7u;->a:Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;

    const-string v1, "Universal Feedback Completed!"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1626805
    iget-object v0, p0, LX/A7u;->a:Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;

    invoke-virtual {v0}, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->finish()V

    .line 1626806
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1626807
    iget-object v0, p0, LX/A7u;->a:Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;

    const-string v1, "Universal Feedback CANCELLED!"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1626808
    iget-object v0, p0, LX/A7u;->a:Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;

    invoke-virtual {v0}, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;->finish()V

    .line 1626809
    return-void
.end method
