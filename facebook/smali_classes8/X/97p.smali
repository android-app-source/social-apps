.class public final LX/97p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1442836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)LX/97p;
    .locals 1

    .prologue
    .line 1442837
    iput-wide p1, p0, LX/97p;->a:J

    .line 1442838
    return-object p0
.end method

.method public final a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v12, 0x0

    .line 1442839
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 1442840
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1442841
    iget-wide v2, p0, LX/97p;->a:J

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1442842
    iget-wide v8, p0, LX/97p;->b:J

    move-object v6, v0

    move-wide v10, v4

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IJJ)V

    .line 1442843
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 1442844
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 1442845
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1442846
    invoke-virtual {v4, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1442847
    new-instance v3, LX/15i;

    move-object v5, v12

    move-object v6, v12

    move-object v8, v12

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1442848
    new-instance v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;

    invoke-direct {v0, v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;-><init>(LX/15i;)V

    .line 1442849
    return-object v0
.end method

.method public final b(J)LX/97p;
    .locals 1

    .prologue
    .line 1442850
    iput-wide p1, p0, LX/97p;->b:J

    .line 1442851
    return-object p0
.end method
