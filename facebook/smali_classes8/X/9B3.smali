.class public LX/9B3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/9Al;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/3H7;

.field private final c:LX/1zf;

.field public final d:LX/0tX;

.field public final e:LX/1My;

.field public final f:LX/0ad;

.field public final g:LX/3HA;

.field public final h:LX/1Ck;

.field public final i:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field

.field public k:[LX/55h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/55h",
            "<",
            "LX/9Al;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/1zt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:[Z

.field public o:LX/0v6;

.field public p:[I

.field public q:LX/9BK;

.field public r:LX/9BJ;

.field public s:Z

.field public t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1451162
    new-instance v0, LX/9Az;

    invoke-direct {v0}, LX/9Az;-><init>()V

    sput-object v0, LX/9B3;->a:LX/0QK;

    return-void
.end method

.method public constructor <init>(LX/3H7;LX/1zf;LX/0tX;LX/1My;LX/0ad;LX/3HA;LX/1Ck;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1451147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1451148
    iput-object v1, p0, LX/9B3;->q:LX/9BK;

    .line 1451149
    iput-object v1, p0, LX/9B3;->r:LX/9BJ;

    .line 1451150
    iput-boolean v0, p0, LX/9B3;->s:Z

    .line 1451151
    iput-boolean v0, p0, LX/9B3;->t:Z

    .line 1451152
    iput-object p1, p0, LX/9B3;->b:LX/3H7;

    .line 1451153
    iput-object p2, p0, LX/9B3;->c:LX/1zf;

    .line 1451154
    iput-object p3, p0, LX/9B3;->d:LX/0tX;

    .line 1451155
    iput-object p4, p0, LX/9B3;->e:LX/1My;

    .line 1451156
    iput-object p5, p0, LX/9B3;->f:LX/0ad;

    .line 1451157
    iput-object p6, p0, LX/9B3;->g:LX/3HA;

    .line 1451158
    iput-object p7, p0, LX/9B3;->h:LX/1Ck;

    .line 1451159
    iput-object p8, p0, LX/9B3;->i:Ljava/util/concurrent/Executor;

    .line 1451160
    iget-object v0, p0, LX/9B3;->c:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->c()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/9B3;->j:LX/0Px;

    .line 1451161
    return-void
.end method

.method public static a(Z)LX/0rS;
    .locals 1

    .prologue
    .line 1451146
    if-eqz p0, :cond_0

    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .locals 1

    .prologue
    .line 1451143
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/16z;->q(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/16z;->q(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1451144
    :cond_0
    const/4 v0, 0x0

    .line 1451145
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, LX/16z;->q(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/9B3;I)Z
    .locals 1

    .prologue
    .line 1451142
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/9B3;->k:[LX/55h;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/9B3;LX/1zt;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1451125
    iget-object v0, p0, LX/9B3;->q:LX/9BK;

    if-eqz v0, :cond_1

    .line 1451126
    iget-object v0, p0, LX/9B3;->q:LX/9BK;

    .line 1451127
    iget-object v1, v0, LX/9BK;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    const/4 v0, 0x1

    .line 1451128
    invoke-static {p2}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v2

    .line 1451129
    iget-object p0, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->e:LX/1CW;

    invoke-virtual {p0, v2, v0, v0}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v2

    .line 1451130
    iget-object p0, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    if-nez p0, :cond_2

    .line 1451131
    :cond_0
    :goto_0
    iget-object p0, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    iget-object v2, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->y:Landroid/util/SparseArray;

    .line 1451132
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 1451133
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 p1, 0x3

    .line 1451134
    iget-object v0, p0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820009

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1451135
    iget-object v0, p0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820008

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1451136
    iget-object v0, p0, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820006

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1451137
    :cond_1
    return-void

    .line 1451138
    :cond_2
    iget-object p0, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object p0

    .line 1451139
    if-eqz p0, :cond_0

    .line 1451140
    const v0, 0x7f0d124a

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1451141
    new-instance v0, LX/9BF;

    invoke-direct {v0, v1, p1}, LX/9BF;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;LX/1zt;)V

    invoke-virtual {p0, v2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/9B3;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;)V
    .locals 5

    .prologue
    .line 1451110
    invoke-static {p1}, LX/9B3;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    .line 1451111
    if-nez v0, :cond_1

    .line 1451112
    const/4 v1, 0x0

    .line 1451113
    :goto_0
    move-object v2, v1

    .line 1451114
    if-nez v2, :cond_0

    .line 1451115
    :goto_1
    return-void

    .line 1451116
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v1

    .line 1451117
    iget-object v3, p0, LX/9B3;->p:[I

    invoke-static {p0, p2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v4

    aput v1, v3, v4

    .line 1451118
    iget-object v1, v2, LX/44w;->b:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v1}, LX/55i;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v4, v1, LX/1vs;->b:I

    .line 1451119
    iget-object v1, p0, LX/9B3;->k:[LX/55h;

    invoke-static {p0, p2}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result p1

    aget-object p1, v1, p1

    iget-object v1, v2, LX/44w;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Iterable;

    invoke-virtual {p1, v1, v3, v4}, LX/55h;->a(Ljava/lang/Iterable;LX/15i;I)V

    goto :goto_1

    :cond_1
    new-instance v1, LX/44w;

    invoke-static {p0, v0}, LX/9B3;->b(LX/9B3;Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)Ljava/util/List;

    move-result-object v2

    .line 1451120
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    .line 1451121
    if-nez v3, :cond_2

    .line 1451122
    new-instance v3, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {v3}, Lcom/facebook/graphql/model/GraphQLPageInfo;-><init>()V

    .line 1451123
    :cond_2
    move-object v3, v3

    .line 1451124
    invoke-direct {v1, v2, v3}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/9B3;
    .locals 9

    .prologue
    .line 1451108
    new-instance v0, LX/9B3;

    invoke-static {p0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v1

    check-cast v1, LX/3H7;

    invoke-static {p0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v2

    check-cast v2, LX/1zf;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v4

    check-cast v4, LX/1My;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, LX/3HA;->b(LX/0QB;)LX/3HA;

    move-result-object v6

    check-cast v6, LX/3HA;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v8}, LX/9B3;-><init>(LX/3H7;LX/1zf;LX/0tX;LX/1My;LX/0ad;LX/3HA;LX/1Ck;Ljava/util/concurrent/Executor;)V

    .line 1451109
    return-object v0
.end method

.method public static b(LX/9B3;Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/9Al;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1451098
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1451099
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    .line 1451100
    iget-object v5, p0, LX/9B3;->c:LX/1zf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result v6

    invoke-virtual {v5, v6}, LX/1zf;->a(I)LX/1zt;

    move-result-object v5

    .line 1451101
    if-eqz v5, :cond_2

    .line 1451102
    iget-object v6, p0, LX/9B3;->j:LX/0Px;

    move-object v6, v6

    .line 1451103
    invoke-virtual {v6, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    :goto_1
    move v5, v6

    .line 1451104
    if-eqz v5, :cond_0

    .line 1451105
    new-instance v5, LX/9Al;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v5, v6, v0}, LX/9Al;-><init>(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/Integer;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1451106
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1451107
    :cond_1
    return-object v2

    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static final e(LX/1zt;)Z
    .locals 1

    .prologue
    .line 1451096
    sget-object v0, LX/1zt;->c:LX/1zt;

    move-object v0, v0

    .line 1451097
    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/9B3;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1451062
    iget-object v2, p0, LX/9B3;->k:[LX/55h;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1451063
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/55h;->c()I

    move-result v4

    if-eqz v4, :cond_0

    .line 1451064
    :goto_1
    return v0

    .line 1451065
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1451066
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static j(LX/9B3;LX/1zt;)LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1zt;",
            ")",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1451095
    new-instance v0, LX/9B1;

    invoke-direct {v0, p0, p1}, LX/9B1;-><init>(LX/9B3;LX/1zt;)V

    return-object v0
.end method

.method public static k(LX/9B3;LX/1zt;)I
    .locals 1

    .prologue
    .line 1451093
    iget-object v0, p0, LX/9B3;->j:LX/0Px;

    move-object v0, v0

    .line 1451094
    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1zt;IZ)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1451073
    invoke-static {p0, p1}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v2

    .line 1451074
    iget-object v0, p0, LX/9B3;->n:[Z

    aget-boolean v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/9B3;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1451075
    :cond_0
    :goto_0
    return-void

    .line 1451076
    :cond_1
    if-lez p2, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1451077
    iget-object v0, p0, LX/9B3;->n:[Z

    aput-boolean v1, v0, v2

    .line 1451078
    invoke-static {p0}, LX/9B3;->f(LX/9B3;)Z

    .line 1451079
    invoke-static {p3}, LX/9B3;->a(Z)LX/0rS;

    move-result-object v5

    .line 1451080
    iget-object v0, p0, LX/9B3;->k:[LX/55h;

    aget-object v0, v0, v2

    .line 1451081
    iget-object v1, v0, LX/55h;->e:Ljava/lang/String;

    move-object v3, v1

    .line 1451082
    iget-boolean v0, p0, LX/9B3;->s:Z

    if-eqz v0, :cond_3

    .line 1451083
    iget-object v0, p0, LX/9B3;->b:LX/3H7;

    invoke-virtual {p0}, LX/9B3;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/9B3;->r:LX/9BJ;

    invoke-virtual {v2}, LX/9BJ;->b()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-static {p0, p1}, LX/9B3;->j(LX/9B3;LX/1zt;)LX/0TF;

    move-result-object v7

    move-object v2, p1

    move v4, p2

    invoke-virtual/range {v0 .. v7}, LX/3H7;->a(Ljava/lang/String;LX/1zt;Ljava/lang/String;ILX/0rS;Lcom/facebook/common/callercontext/CallerContext;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1451084
    :goto_2
    iget-object v1, p0, LX/9B3;->h:LX/1Ck;

    .line 1451085
    invoke-static {p1}, LX/9B3;->e(LX/1zt;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1451086
    :goto_3
    move-object v2, v3

    .line 1451087
    new-instance v3, LX/9B0;

    invoke-direct {v3, p0, p1, p3, p2}, LX/9B0;-><init>(LX/9B3;LX/1zt;ZI)V

    move-object v3, v3

    .line 1451088
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 1451089
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1451090
    :cond_3
    iget-object v0, p0, LX/9B3;->b:LX/3H7;

    invoke-virtual {p0}, LX/9B3;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/9B3;->r:LX/9BJ;

    invoke-virtual {v2}, LX/9BJ;->b()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    iget-boolean v7, p0, LX/9B3;->t:Z

    move-object v2, p1

    move v4, p2

    invoke-virtual/range {v0 .. v7}, LX/3H7;->a(Ljava/lang/String;LX/1zt;Ljava/lang/String;ILX/0rS;Lcom/facebook/common/callercontext/CallerContext;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1451091
    iget v4, p1, LX/1zt;->e:I

    move v4, v4

    .line 1451092
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_3
.end method

.method public final b(LX/1zt;)I
    .locals 2

    .prologue
    .line 1451069
    invoke-static {p0, p1}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v0

    .line 1451070
    invoke-static {p0, v0}, LX/9B3;->a(LX/9B3;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1451071
    const/4 v0, 0x0

    .line 1451072
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/9B3;->p:[I

    aget v0, v1, v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1451068
    iget-object v0, p0, LX/9B3;->r:LX/9BJ;

    invoke-virtual {v0}, LX/9BJ;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/1zt;)Z
    .locals 2

    .prologue
    .line 1451067
    iget-object v0, p0, LX/9B3;->n:[Z

    invoke-static {p0, p1}, LX/9B3;->k(LX/9B3;LX/1zt;)I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method
