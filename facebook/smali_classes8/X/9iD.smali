.class public abstract LX/9iD;
.super LX/0gG;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0gc;

.field private c:LX/0hH;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment$SavedState;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/support/v4/app/Fragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1527541
    const-class v0, LX/9iD;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9iD;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0gc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1527489
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1527490
    iput-object v1, p0, LX/9iD;->c:LX/0hH;

    .line 1527491
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    .line 1527492
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9iD;->e:Ljava/util/ArrayList;

    .line 1527493
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    .line 1527494
    iput-object v1, p0, LX/9iD;->g:Landroid/support/v4/app/Fragment;

    .line 1527495
    iput-object p1, p0, LX/9iD;->b:LX/0gc;

    .line 1527496
    return-void
.end method


# virtual methods
.method public abstract a(I)Landroid/support/v4/app/Fragment;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1527497
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 1527498
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1527499
    if-eqz v0, :cond_0

    .line 1527500
    :goto_0
    return-object v0

    .line 1527501
    :cond_0
    iget-object v0, p0, LX/9iD;->c:LX/0hH;

    if-nez v0, :cond_1

    .line 1527502
    iget-object v0, p0, LX/9iD;->b:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iput-object v0, p0, LX/9iD;->c:LX/0hH;

    .line 1527503
    :cond_1
    invoke-virtual {p0, p2}, LX/9iD;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 1527504
    iget-object v0, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_2

    .line 1527505
    iget-object v0, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    .line 1527506
    if-eqz v0, :cond_2

    .line 1527507
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 1527508
    :cond_2
    :goto_1
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_3

    .line 1527509
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1527510
    :cond_3
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 1527511
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 1527512
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1527513
    iget-object v0, p0, LX/9iD;->c:LX/0hH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-object v0, v1

    .line 1527514
    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1527515
    if-eqz p1, :cond_5

    .line 1527516
    check-cast p1, Landroid/os/Bundle;

    .line 1527517
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1527518
    const-string v0, "states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 1527519
    iget-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1527520
    iget-object v0, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1527521
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1527522
    if-eqz v3, :cond_0

    move v1, v2

    .line 1527523
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 1527524
    iget-object v4, p0, LX/9iD;->e:Ljava/util/ArrayList;

    aget-object v0, v3, v1

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1527525
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1527526
    :cond_0
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 1527527
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1527528
    const-string v3, "f"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1527529
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1527530
    iget-object v4, p0, LX/9iD;->b:LX/0gc;

    invoke-virtual {v4, p1, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 1527531
    if-eqz v4, :cond_3

    .line 1527532
    :goto_2
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v3, :cond_2

    .line 1527533
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1527534
    :cond_2
    invoke-virtual {v4, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 1527535
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1527536
    :cond_3
    sget-object v3, LX/9iD;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad fragment at key "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1527537
    :cond_4
    const-string v0, "idxmap"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 1527538
    if-eqz v0, :cond_5

    .line 1527539
    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    .line 1527540
    :cond_5
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1527474
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1527475
    move-object v0, p3

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1527476
    iget-object v1, p0, LX/9iD;->c:LX/0hH;

    if-nez v1, :cond_0

    .line 1527477
    iget-object v1, p0, LX/9iD;->b:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iput-object v1, p0, LX/9iD;->c:LX/0hH;

    .line 1527478
    :cond_0
    :goto_0
    iget-object v1, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v1, p2, :cond_1

    .line 1527479
    iget-object v1, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1527480
    :cond_1
    invoke-virtual {p0, p3, p2}, LX/9iD;->a(Ljava/lang/Object;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1527481
    :try_start_0
    iget-object v1, p0, LX/9iD;->b:LX/0gc;

    invoke-virtual {v1, v0}, LX/0gc;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1527482
    :goto_1
    iget-object v3, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1527483
    iget-object v1, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_2

    .line 1527484
    iget-object v1, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1527485
    :cond_2
    iget-object v1, p0, LX/9iD;->c:LX/0hH;

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1527486
    return-void

    .line 1527487
    :catch_0
    move-exception v1

    .line 1527488
    sget-object v3, LX/9iD;->a:Ljava/lang/String;

    const-string v4, "destroyItem fragment state not valid at position=%d and size=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v1, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1527471
    check-cast p2, Landroid/support/v4/app/Fragment;

    .line 1527472
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1527473
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(Ljava/lang/Object;I)Z
.end method

.method public abstract b(I)Ljava/lang/String;
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1527466
    iget-object v0, p0, LX/9iD;->c:LX/0hH;

    if-eqz v0, :cond_0

    .line 1527467
    iget-object v0, p0, LX/9iD;->c:LX/0hH;

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1527468
    const/4 v0, 0x0

    iput-object v0, p0, LX/9iD;->c:LX/0hH;

    .line 1527469
    iget-object v0, p0, LX/9iD;->b:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1527470
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1527404
    check-cast p3, Landroid/support/v4/app/Fragment;

    .line 1527405
    iget-object v0, p0, LX/9iD;->g:Landroid/support/v4/app/Fragment;

    if-eq p3, v0, :cond_2

    .line 1527406
    iget-object v0, p0, LX/9iD;->g:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 1527407
    iget-object v0, p0, LX/9iD;->g:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 1527408
    iget-object v0, p0, LX/9iD;->g:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 1527409
    :cond_0
    if-eqz p3, :cond_1

    .line 1527410
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 1527411
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 1527412
    :cond_1
    iput-object p3, p0, LX/9iD;->g:Landroid/support/v4/app/Fragment;

    .line 1527413
    :cond_2
    return-void
.end method

.method public final kV_()V
    .locals 9

    .prologue
    .line 1527435
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1527436
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v5

    .line 1527437
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 1527438
    iget-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eq v5, v0, :cond_2

    move v0, v1

    :goto_0
    move v4, v2

    .line 1527439
    :goto_1
    if-ge v4, v5, :cond_4

    .line 1527440
    invoke-virtual {p0, v4}, LX/9iD;->b(I)Ljava/lang/String;

    move-result-object v7

    .line 1527441
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527442
    if-nez v0, :cond_1

    .line 1527443
    iget-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1527444
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v4, :cond_3

    :cond_0
    move v0, v1

    .line 1527445
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1527446
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1527447
    goto :goto_2

    .line 1527448
    :cond_4
    if-nez v0, :cond_5

    .line 1527449
    :goto_3
    invoke-super {p0}, LX/0gG;->kV_()V

    .line 1527450
    return-void

    .line 1527451
    :cond_5
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1527452
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1527453
    :goto_4
    if-ge v2, v5, :cond_9

    .line 1527454
    iget-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    invoke-virtual {p0, v2}, LX/9iD;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1527455
    if-nez v0, :cond_6

    .line 1527456
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1527457
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1527458
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1527459
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v8, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_7

    iget-object v1, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment$SavedState;

    :goto_6
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1527460
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v8, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_8

    iget-object v1, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    :goto_7
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_7
    move-object v1, v3

    .line 1527461
    goto :goto_6

    :cond_8
    move-object v0, v3

    .line 1527462
    goto :goto_7

    .line 1527463
    :cond_9
    iput-object v6, p0, LX/9iD;->d:Ljava/util/Map;

    .line 1527464
    iput-object v4, p0, LX/9iD;->e:Ljava/util/ArrayList;

    .line 1527465
    iput-object v7, p0, LX/9iD;->f:Ljava/util/ArrayList;

    goto :goto_3
.end method

.method public final lf_()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 1527414
    const/4 v0, 0x0

    .line 1527415
    iget-object v1, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1527416
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1527417
    iget-object v1, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/support/v4/app/Fragment$SavedState;

    .line 1527418
    iget-object v2, p0, LX/9iD;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1527419
    const-string v2, "states"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 1527420
    :cond_0
    const/4 v1, 0x0

    move-object v2, v0

    :goto_0
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1527421
    iget-object v0, p0, LX/9iD;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1527422
    if-eqz v0, :cond_2

    .line 1527423
    if-nez v2, :cond_1

    .line 1527424
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1527425
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "f"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1527426
    :try_start_0
    iget-object v4, p0, LX/9iD;->b:LX/0gc;

    invoke-virtual {v4, v2, v3, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1527427
    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1527428
    :catch_0
    move-exception v0

    .line 1527429
    sget-object v3, LX/9iD;->a:Ljava/lang/String;

    const-string v4, "Attempt to save state for inactive fragment"

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1527430
    :cond_3
    iget-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1527431
    if-nez v2, :cond_4

    .line 1527432
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1527433
    :cond_4
    const-string v1, "idxmap"

    iget-object v0, p0, LX/9iD;->d:Ljava/util/Map;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1527434
    :cond_5
    return-object v2
.end method
