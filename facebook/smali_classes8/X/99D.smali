.class public final LX/99D;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:LX/1HI;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/fbui/drawable/NetworkDrawable;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:LX/1bf;

.field public j:Lcom/facebook/common/callercontext/CallerContext;

.field public k:I

.field public l:Landroid/graphics/Bitmap;

.field public m:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILX/1HI;Ljava/util/concurrent/Executor;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1HI;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1447063
    invoke-direct {p0}, LX/1ci;-><init>()V

    .line 1447064
    iput p1, p0, LX/99D;->a:I

    .line 1447065
    iput-object p2, p0, LX/99D;->b:LX/1HI;

    .line 1447066
    iput-object p3, p0, LX/99D;->c:Ljava/util/concurrent/Executor;

    .line 1447067
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/99D;->e:Ljava/util/List;

    .line 1447068
    iput-object p4, p0, LX/99D;->d:LX/0Ot;

    .line 1447069
    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1447036
    iget-object v0, p0, LX/99D;->n:LX/1ca;

    if-ne p1, v0, :cond_6

    .line 1447037
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    iput-object v0, p0, LX/99D;->m:LX/1FJ;

    .line 1447038
    iget-object v0, p0, LX/99D;->m:LX/1FJ;

    if-eqz v0, :cond_0

    .line 1447039
    iget-object v0, p0, LX/99D;->m:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ll;

    .line 1447040
    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, LX/99D;->l:Landroid/graphics/Bitmap;

    .line 1447041
    :cond_0
    iput-object v1, p0, LX/99D;->n:LX/1ca;

    .line 1447042
    iget-object v2, p0, LX/99D;->e:Ljava/util/List;

    .line 1447043
    iput-object v1, p0, LX/99D;->e:Ljava/util/List;

    .line 1447044
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    .line 1447045
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/drawable/NetworkDrawable;

    .line 1447046
    if-eqz v0, :cond_4

    .line 1447047
    invoke-virtual {v0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->invalidateSelf()V

    .line 1447048
    iget-object v4, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->e:LX/99A;

    if-eqz v4, :cond_1

    .line 1447049
    sget-object v4, LX/99C;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1447050
    iget-object v4, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->e:LX/99A;

    iget-object v5, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    iget-object v5, v5, LX/999;->b:LX/99D;

    iget v5, v5, LX/99D;->a:I

    invoke-interface {v4, v5}, LX/99A;->a(I)V

    .line 1447051
    const/4 v4, 0x0

    iput-object v4, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->e:LX/99A;

    .line 1447052
    invoke-static {v0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->e(Lcom/facebook/fbui/drawable/NetworkDrawable;)V

    .line 1447053
    :cond_1
    const/4 v4, 0x0

    iget-object v5, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v5, v4

    :goto_1
    if-ge v5, v6, :cond_3

    .line 1447054
    iget-object v4, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->f:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/36B;

    .line 1447055
    if-eqz v4, :cond_2

    .line 1447056
    iget-object p1, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->b:LX/999;

    invoke-virtual {p1}, LX/999;->a()Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-interface {v4, p1}, LX/36B;->a(Landroid/graphics/Bitmap;)V

    .line 1447057
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 1447058
    :cond_3
    iget-object v4, v0, Lcom/facebook/fbui/drawable/NetworkDrawable;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1447059
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1447060
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1447061
    iput-object v2, p0, LX/99D;->e:Ljava/util/List;

    .line 1447062
    :cond_6
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1447033
    iget-object v0, p0, LX/99D;->n:LX/1ca;

    if-ne p1, v0, :cond_0

    .line 1447034
    const/4 v0, 0x0

    iput-object v0, p0, LX/99D;->n:LX/1ca;

    .line 1447035
    :cond_0
    return-void
.end method
