.class public final LX/9yr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1598254
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1598255
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598256
    :goto_0
    return v1

    .line 1598257
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598258
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1598259
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1598260
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598261
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1598262
    const-string v4, "node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1598263
    invoke-static {p0, p1}, LX/9yq;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1598264
    :cond_2
    const-string v4, "result_decoration"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1598265
    const/4 v3, 0x0

    .line 1598266
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_d

    .line 1598267
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598268
    :goto_2
    move v0, v3

    .line 1598269
    goto :goto_1

    .line 1598270
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1598271
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1598272
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1598273
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1598274
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598275
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_c

    .line 1598276
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1598277
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598278
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_6

    if-eqz v7, :cond_6

    .line 1598279
    const-string v8, "lineage_snippets"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1598280
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1598281
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_7

    .line 1598282
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_7

    .line 1598283
    const/4 v8, 0x0

    .line 1598284
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_11

    .line 1598285
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598286
    :goto_5
    move v7, v8

    .line 1598287
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1598288
    :cond_7
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1598289
    goto :goto_3

    .line 1598290
    :cond_8
    const-string v8, "ordered_snippets"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1598291
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1598292
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_9

    .line 1598293
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_9

    .line 1598294
    const/4 v8, 0x0

    .line 1598295
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_19

    .line 1598296
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598297
    :goto_7
    move v7, v8

    .line 1598298
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1598299
    :cond_9
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1598300
    goto/16 :goto_3

    .line 1598301
    :cond_a
    const-string v8, "social_snippet"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1598302
    const/4 v7, 0x0

    .line 1598303
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_21

    .line 1598304
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598305
    :goto_8
    move v4, v7

    .line 1598306
    goto/16 :goto_3

    .line 1598307
    :cond_b
    const-string v8, "summary_snippet"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1598308
    const/4 v7, 0x0

    .line 1598309
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v8, :cond_29

    .line 1598310
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598311
    :goto_9
    move v0, v7

    .line 1598312
    goto/16 :goto_3

    .line 1598313
    :cond_c
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1598314
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1598315
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v5}, LX/186;->b(II)V

    .line 1598316
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 1598317
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1598318
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_d
    move v0, v3

    move v4, v3

    move v5, v3

    move v6, v3

    goto/16 :goto_3

    .line 1598319
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598320
    :cond_f
    :goto_a
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_10

    .line 1598321
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1598322
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598323
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_f

    if-eqz v9, :cond_f

    .line 1598324
    const-string v10, "sentence"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1598325
    const/4 v9, 0x0

    .line 1598326
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_15

    .line 1598327
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598328
    :goto_b
    move v7, v9

    .line 1598329
    goto :goto_a

    .line 1598330
    :cond_10
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1598331
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1598332
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_11
    move v7, v8

    goto :goto_a

    .line 1598333
    :cond_12
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598334
    :cond_13
    :goto_c
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_14

    .line 1598335
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1598336
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598337
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_13

    if-eqz v10, :cond_13

    .line 1598338
    const-string v11, "text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 1598339
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_c

    .line 1598340
    :cond_14
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1598341
    invoke-virtual {p1, v9, v7}, LX/186;->b(II)V

    .line 1598342
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_b

    :cond_15
    move v7, v9

    goto :goto_c

    .line 1598343
    :cond_16
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598344
    :cond_17
    :goto_d
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_18

    .line 1598345
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1598346
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598347
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_17

    if-eqz v9, :cond_17

    .line 1598348
    const-string v10, "sentence"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 1598349
    const/4 v9, 0x0

    .line 1598350
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_1d

    .line 1598351
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598352
    :goto_e
    move v7, v9

    .line 1598353
    goto :goto_d

    .line 1598354
    :cond_18
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1598355
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1598356
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_7

    :cond_19
    move v7, v8

    goto :goto_d

    .line 1598357
    :cond_1a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598358
    :cond_1b
    :goto_f
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1c

    .line 1598359
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1598360
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598361
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1b

    if-eqz v10, :cond_1b

    .line 1598362
    const-string v11, "text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 1598363
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_f

    .line 1598364
    :cond_1c
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1598365
    invoke-virtual {p1, v9, v7}, LX/186;->b(II)V

    .line 1598366
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_e

    :cond_1d
    move v7, v9

    goto :goto_f

    .line 1598367
    :cond_1e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598368
    :cond_1f
    :goto_10
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_20

    .line 1598369
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1598370
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598371
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1f

    if-eqz v8, :cond_1f

    .line 1598372
    const-string v9, "sentence"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1e

    .line 1598373
    const/4 v8, 0x0

    .line 1598374
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v9, :cond_25

    .line 1598375
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598376
    :goto_11
    move v4, v8

    .line 1598377
    goto :goto_10

    .line 1598378
    :cond_20
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1598379
    invoke-virtual {p1, v7, v4}, LX/186;->b(II)V

    .line 1598380
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_8

    :cond_21
    move v4, v7

    goto :goto_10

    .line 1598381
    :cond_22
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598382
    :cond_23
    :goto_12
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_24

    .line 1598383
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1598384
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598385
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_23

    if-eqz v9, :cond_23

    .line 1598386
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_22

    .line 1598387
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_12

    .line 1598388
    :cond_24
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1598389
    invoke-virtual {p1, v8, v4}, LX/186;->b(II)V

    .line 1598390
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_11

    :cond_25
    move v4, v8

    goto :goto_12

    .line 1598391
    :cond_26
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598392
    :cond_27
    :goto_13
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_28

    .line 1598393
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1598394
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598395
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_27

    if-eqz v8, :cond_27

    .line 1598396
    const-string v9, "sentence"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_26

    .line 1598397
    const/4 v8, 0x0

    .line 1598398
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v9, :cond_2d

    .line 1598399
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598400
    :goto_14
    move v0, v8

    .line 1598401
    goto :goto_13

    .line 1598402
    :cond_28
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1598403
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1598404
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_9

    :cond_29
    move v0, v7

    goto :goto_13

    .line 1598405
    :cond_2a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1598406
    :cond_2b
    :goto_15
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_2c

    .line 1598407
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1598408
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1598409
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_2b

    if-eqz v9, :cond_2b

    .line 1598410
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2a

    .line 1598411
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_15

    .line 1598412
    :cond_2c
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1598413
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1598414
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_14

    :cond_2d
    move v0, v8

    goto :goto_15
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1598415
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598416
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1598417
    if-eqz v0, :cond_0

    .line 1598418
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598419
    invoke-static {p0, v0, p2, p3}, LX/9yq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1598420
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1598421
    if-eqz v0, :cond_f

    .line 1598422
    const-string v1, "result_decoration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598423
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598424
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1598425
    if-eqz v1, :cond_4

    .line 1598426
    const-string v2, "lineage_snippets"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598427
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1598428
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1598429
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1598430
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598431
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1598432
    if-eqz v4, :cond_2

    .line 1598433
    const-string p1, "sentence"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598434
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598435
    const/4 p1, 0x0

    invoke-virtual {p0, v4, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 1598436
    if-eqz p1, :cond_1

    .line 1598437
    const-string v3, "text"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598438
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1598439
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598440
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598441
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1598442
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1598443
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1598444
    if-eqz v1, :cond_8

    .line 1598445
    const-string v2, "ordered_snippets"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598446
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1598447
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 1598448
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1598449
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598450
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1598451
    if-eqz v4, :cond_6

    .line 1598452
    const-string p1, "sentence"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598453
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598454
    const/4 p1, 0x0

    invoke-virtual {p0, v4, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 1598455
    if-eqz p1, :cond_5

    .line 1598456
    const-string v3, "text"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598457
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1598458
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598459
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598460
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1598461
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1598462
    :cond_8
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1598463
    if-eqz v1, :cond_b

    .line 1598464
    const-string v2, "social_snippet"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598465
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598466
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1598467
    if-eqz v2, :cond_a

    .line 1598468
    const-string v3, "sentence"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598469
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598470
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1598471
    if-eqz v3, :cond_9

    .line 1598472
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598473
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1598474
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598475
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598476
    :cond_b
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1598477
    if-eqz v1, :cond_e

    .line 1598478
    const-string v2, "summary_snippet"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598479
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598480
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1598481
    if-eqz v2, :cond_d

    .line 1598482
    const-string v3, "sentence"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598483
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1598484
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1598485
    if-eqz v3, :cond_c

    .line 1598486
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1598487
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1598488
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598489
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598490
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598491
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1598492
    return-void
.end method
