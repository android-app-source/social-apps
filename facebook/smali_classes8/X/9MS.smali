.class public final LX/9MS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1473861
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1473862
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1473863
    :goto_0
    return v1

    .line 1473864
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1473865
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1473866
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1473867
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1473868
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1473869
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1473870
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1473871
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1473872
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1473873
    invoke-static {p0, p1}, LX/9MR;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1473874
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1473875
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1473876
    goto :goto_1

    .line 1473877
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1473878
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1473879
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_b

    .line 1473880
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1473881
    :goto_3
    move v0, v3

    .line 1473882
    goto :goto_1

    .line 1473883
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1473884
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1473885
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1473886
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1473887
    :cond_6
    const-string v8, "has_next_page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1473888
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    .line 1473889
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 1473890
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1473891
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1473892
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_7

    if-eqz v7, :cond_7

    .line 1473893
    const-string v8, "end_cursor"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1473894
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 1473895
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1473896
    :cond_9
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1473897
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1473898
    if-eqz v0, :cond_a

    .line 1473899
    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 1473900
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_b
    move v0, v3

    move v5, v3

    move v6, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1473901
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1473902
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1473903
    if-eqz v0, :cond_1

    .line 1473904
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473905
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1473906
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1473907
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/9MR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1473908
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1473909
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1473910
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1473911
    if-eqz v0, :cond_4

    .line 1473912
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473913
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1473914
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1473915
    if-eqz v1, :cond_2

    .line 1473916
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473917
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1473918
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1473919
    if-eqz v1, :cond_3

    .line 1473920
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1473921
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1473922
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1473923
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1473924
    return-void
.end method
