.class public final LX/9ZY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1510543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1510544
    if-nez p1, :cond_0

    .line 1510545
    :goto_0
    return v0

    .line 1510546
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    const/4 v2, 0x0

    .line 1510547
    if-nez v1, :cond_1

    .line 1510548
    :goto_1
    move v1, v2

    .line 1510549
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1510550
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1510551
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1510552
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1510553
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1510554
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1510555
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1510556
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 1510557
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1510558
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1510559
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1510560
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1510561
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1510562
    const/4 v10, 0x7

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1510563
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1510564
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1510565
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1510566
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1510567
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1510568
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 1510569
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 1510570
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1510571
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToAction;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 1510572
    if-nez p1, :cond_0

    .line 1510573
    :goto_0
    return v0

    .line 1510574
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1510575
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1510576
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m()Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    move-result-object v3

    const/4 v4, 0x0

    .line 1510577
    if-nez v3, :cond_1

    .line 1510578
    :goto_1
    move v3, v4

    .line 1510579
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1510580
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1510581
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1510582
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v7

    const/4 v13, 0x1

    const/4 v10, 0x0

    .line 1510583
    if-nez v7, :cond_2

    .line 1510584
    :goto_2
    move v7, v10

    .line 1510585
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->w()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v8

    const/4 v9, 0x0

    .line 1510586
    if-nez v8, :cond_5

    .line 1510587
    :goto_3
    move v8, v9

    .line 1510588
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1510589
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1510590
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u()Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    move-result-object v11

    invoke-static {p0, v11}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoneNumber;)I

    move-result v11

    .line 1510591
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1510592
    const/16 v13, 0xe

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1510593
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1510594
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1510595
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->l()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1510596
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1510597
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1510598
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1510599
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1510600
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1510601
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1510602
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1510603
    const/16 v0, 0xa

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1510604
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 1510605
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 1510606
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v12}, LX/186;->b(II)V

    .line 1510607
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1510608
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 1510609
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1510610
    const/4 v6, 0x3

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1510611
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;->a()Z

    move-result v6

    invoke-virtual {p0, v4, v6}, LX/186;->a(IZ)V

    .line 1510612
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 1510613
    const/4 v4, 0x2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;->k()Z

    move-result v5

    invoke-virtual {p0, v4, v5}, LX/186;->a(IZ)V

    .line 1510614
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1510615
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1510616
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;->a()LX/0Px;

    move-result-object v11

    .line 1510617
    if-eqz v11, :cond_4

    .line 1510618
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v8

    new-array v12, v8, [I

    move v9, v10

    .line 1510619
    :goto_4
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_3

    .line 1510620
    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    invoke-static {p0, v8}, LX/9ZY;->c(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I

    move-result v8

    aput v8, v12, v9

    .line 1510621
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_4

    .line 1510622
    :cond_3
    invoke-virtual {p0, v12, v13}, LX/186;->a([IZ)I

    move-result v8

    .line 1510623
    :goto_5
    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1510624
    invoke-virtual {p0, v10, v8}, LX/186;->b(II)V

    .line 1510625
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 1510626
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto/16 :goto_2

    :cond_4
    move v8, v10

    goto :goto_5

    .line 1510627
    :cond_5
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    const/4 v11, 0x0

    .line 1510628
    if-nez v10, :cond_6

    .line 1510629
    :goto_6
    move v10, v11

    .line 1510630
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1510631
    invoke-virtual {p0, v9, v10}, LX/186;->b(II)V

    .line 1510632
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1510633
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 1510634
    :cond_6
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1510635
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1510636
    invoke-virtual {p0, v11, v12}, LX/186;->b(II)V

    .line 1510637
    invoke-virtual {p0}, LX/186;->d()I

    move-result v11

    .line 1510638
    invoke-virtual {p0, v11}, LX/186;->d(I)V

    goto :goto_6
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1510639
    if-nez p1, :cond_0

    .line 1510640
    :goto_0
    return v2

    .line 1510641
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1510642
    if-eqz v3, :cond_2

    .line 1510643
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1510644
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1510645
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    const/4 v6, 0x0

    .line 1510646
    if-nez v0, :cond_3

    .line 1510647
    :goto_2
    move v0, v6

    .line 1510648
    aput v0, v4, v1

    .line 1510649
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1510650
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1510651
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1510652
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1510653
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1510654
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1510655
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1510656
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1510657
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1510658
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1510659
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 1510660
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1510661
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1510534
    if-nez p1, :cond_0

    .line 1510535
    :goto_0
    return v0

    .line 1510536
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1510537
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1510538
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1510539
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1510540
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1510541
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1510542
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoneNumber;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1510412
    if-nez p1, :cond_0

    .line 1510413
    :goto_0
    return v0

    .line 1510414
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1510415
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1510416
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1510417
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1510418
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1510419
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1510420
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1510421
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1510422
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1510423
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1510424
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1510519
    if-nez p1, :cond_0

    .line 1510520
    :goto_0
    return v2

    .line 1510521
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 1510522
    if-eqz v3, :cond_2

    .line 1510523
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1510524
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1510525
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {p0, v0}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v0

    aput v0, v4, v1

    .line 1510526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1510527
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1510528
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1510529
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1510530
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1510531
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 1510532
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1510533
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)LX/9Ys;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1510498
    if-nez p0, :cond_1

    .line 1510499
    :cond_0
    :goto_0
    return-object v2

    .line 1510500
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1510501
    const/4 v1, 0x0

    .line 1510502
    if-nez p0, :cond_4

    .line 1510503
    :cond_2
    :goto_1
    move v1, v1

    .line 1510504
    if-eqz v1, :cond_0

    .line 1510505
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1510506
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1510507
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1510508
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1510509
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_3

    .line 1510510
    const-string v1, "PageCallToActionConversionHelper.getPageCallToActionData"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1510511
    :cond_3
    new-instance v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageCallToActionDataModel;

    invoke-direct {v2, v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageCallToActionDataModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1510512
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 1510513
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x25d6af

    if-ne v3, v4, :cond_2

    .line 1510514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v3

    invoke-static {v0, v3}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToAction;)I

    move-result v3

    .line 1510515
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1510516
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1510517
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1510518
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1510468
    if-nez p1, :cond_0

    .line 1510469
    :goto_0
    return v2

    .line 1510470
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p0, v0}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v3

    .line 1510471
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1510472
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1510473
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1510474
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1510475
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->o()LX/0Px;

    move-result-object v8

    .line 1510476
    if-eqz v8, :cond_2

    .line 1510477
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    new-array v9, v0, [I

    move v1, v2

    .line 1510478
    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1510479
    invoke-virtual {v8, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;

    invoke-static {p0, v0}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;)I

    move-result v0

    aput v0, v9, v1

    .line 1510480
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1510481
    :cond_1
    invoke-virtual {p0, v9, v11}, LX/186;->a([IZ)I

    move-result v0

    .line 1510482
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v1

    invoke-static {p0, v1}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I

    move-result v1

    .line 1510483
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1510484
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1510485
    const/16 v10, 0xa

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1510486
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1510487
    invoke-virtual {p0, v11, v4}, LX/186;->b(II)V

    .line 1510488
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1510489
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1510490
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1510491
    const/4 v2, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1510492
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1510493
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1510494
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1510495
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1510496
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1510497
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1510455
    if-nez p1, :cond_0

    .line 1510456
    :goto_0
    return v2

    .line 1510457
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1510458
    if-eqz v3, :cond_2

    .line 1510459
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1510460
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1510461
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;

    invoke-static {p0, v0}, LX/9ZY;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I

    move-result v0

    aput v0, v4, v1

    .line 1510462
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1510463
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1510464
    :goto_2
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1510465
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1510466
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1510467
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public static c(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;)I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1510425
    if-nez p1, :cond_0

    .line 1510426
    :goto_0
    return v2

    .line 1510427
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p0, v0}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v3

    .line 1510428
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1510429
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->k()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1510430
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1510431
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1510432
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->o()LX/0Px;

    move-result-object v8

    .line 1510433
    if-eqz v8, :cond_2

    .line 1510434
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    new-array v9, v0, [I

    move v1, v2

    .line 1510435
    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1510436
    invoke-virtual {v8, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;

    invoke-static {p0, v0}, LX/9ZY;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionSelectFieldOption;)I

    move-result v0

    aput v0, v9, v1

    .line 1510437
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1510438
    :cond_1
    invoke-virtual {p0, v9, v11}, LX/186;->a([IZ)I

    move-result v0

    .line 1510439
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->p()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v1

    invoke-static {p0, v1}, LX/9ZY;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;)I

    move-result v1

    .line 1510440
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1510441
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1510442
    const/16 v10, 0xa

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1510443
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1510444
    invoke-virtual {p0, v11, v4}, LX/186;->b(II)V

    .line 1510445
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1510446
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1510447
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1510448
    const/4 v2, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigField;->n()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1510449
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1510450
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1510451
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1510452
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1510453
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1510454
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method
