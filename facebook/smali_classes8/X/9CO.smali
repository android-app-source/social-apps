.class public final enum LX/9CO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9CO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9CO;

.field public static final enum COMMENT_COMPOSER:LX/9CO;

.field public static final enum REPLY_COMPOSER:LX/9CO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1453809
    new-instance v0, LX/9CO;

    const-string v1, "COMMENT_COMPOSER"

    invoke-direct {v0, v1, v2}, LX/9CO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9CO;->COMMENT_COMPOSER:LX/9CO;

    new-instance v0, LX/9CO;

    const-string v1, "REPLY_COMPOSER"

    invoke-direct {v0, v1, v3}, LX/9CO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9CO;->REPLY_COMPOSER:LX/9CO;

    .line 1453810
    const/4 v0, 0x2

    new-array v0, v0, [LX/9CO;

    sget-object v1, LX/9CO;->COMMENT_COMPOSER:LX/9CO;

    aput-object v1, v0, v2

    sget-object v1, LX/9CO;->REPLY_COMPOSER:LX/9CO;

    aput-object v1, v0, v3

    sput-object v0, LX/9CO;->$VALUES:[LX/9CO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1453808
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9CO;
    .locals 1

    .prologue
    .line 1453806
    const-class v0, LX/9CO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9CO;

    return-object v0
.end method

.method public static values()[LX/9CO;
    .locals 1

    .prologue
    .line 1453807
    sget-object v0, LX/9CO;->$VALUES:[LX/9CO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9CO;

    return-object v0
.end method
