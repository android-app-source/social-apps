.class public final LX/ARR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# static fields
.field public static final a:LX/89L;


# instance fields
.field private final b:LX/ART;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1672928
    const-string v0, "user_draft"

    invoke-static {v0}, LX/89L;->a(Ljava/lang/String;)LX/89L;

    move-result-object v0

    sput-object v0, LX/ARR;->a:LX/89L;

    return-void
.end method

.method public constructor <init>(LX/ART;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672930
    iput-object p1, p0, LX/ARR;->b:LX/ART;

    .line 1672931
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 8
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1672932
    iget-object v0, p0, LX/ARR;->b:LX/ART;

    .line 1672933
    new-instance v1, LX/ARS;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const-class v2, LX/AR2;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/AR2;

    const-class v2, LX/AR7;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/AR7;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/BJ7;->a(LX/0QB;)LX/BJ7;

    move-result-object v7

    check-cast v7, LX/BJ7;

    move-object v2, p2

    invoke-direct/range {v1 .. v7}, LX/ARS;-><init>(LX/B5j;Landroid/content/Context;LX/AR2;LX/AR7;LX/0Uh;LX/BJ7;)V

    .line 1672934
    move-object v0, v1

    .line 1672935
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1672936
    sget-object v0, LX/ARR;->a:LX/89L;

    invoke-virtual {v0}, LX/89L;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
