.class public final LX/9g1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TGRAPHQ",
        "L_RESULT_TYPE;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9g3;

.field private final b:I


# direct methods
.method public constructor <init>(LX/9g3;I)V
    .locals 0

    .prologue
    .line 1522902
    iput-object p1, p0, LX/9g1;->a:LX/9g3;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 1522903
    iput p2, p0, LX/9g1;->b:I

    .line 1522904
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1522905
    iget-object v0, p0, LX/9g1;->a:LX/9g3;

    invoke-static {v0}, LX/9g3;->h(LX/9g3;)V

    .line 1522906
    sget-object v0, LX/9g3;->a:Ljava/lang/String;

    const-string v1, "Fetch failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1522907
    iget-object v0, p0, LX/9g1;->a:LX/9g3;

    sget-object v1, LX/9g6;->ERROR:LX/9g6;

    .line 1522908
    invoke-virtual {v0, v1}, LX/9g2;->a(LX/9g6;)V

    .line 1522909
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1522910
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1522911
    iget-object v0, p0, LX/9g1;->a:LX/9g3;

    invoke-static {v0}, LX/9g3;->h(LX/9g3;)V

    .line 1522912
    iget-object v0, p0, LX/9g1;->a:LX/9g3;

    sget-object v1, LX/9g6;->DONE:LX/9g6;

    .line 1522913
    invoke-virtual {v0, v1}, LX/9g2;->a(LX/9g6;)V

    .line 1522914
    iget-object v0, p0, LX/9g1;->a:LX/9g3;

    iget v1, p0, LX/9g1;->b:I

    invoke-static {v0, v1, p1}, LX/9g3;->a$redex0(LX/9g3;ILcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1522915
    return-void
.end method
