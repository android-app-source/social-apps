.class public final LX/AL4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/AL7;


# direct methods
.method public constructor <init>(LX/AL7;)V
    .locals 0

    .prologue
    .line 1664561
    iput-object p1, p0, LX/AL4;->a:LX/AL7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    .prologue
    .line 1664562
    iget-object v0, p0, LX/AL4;->a:LX/AL7;

    iget-object v0, v0, LX/AL7;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1664563
    iget-object v0, p0, LX/AL4;->a:LX/AL7;

    iget-object v0, v0, LX/AL7;->c:Landroid/view/View;

    iget-object v1, p0, LX/AL4;->a:LX/AL7;

    iget-object v1, v1, LX/AL7;->i:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1664564
    iget-object v0, p0, LX/AL4;->a:LX/AL7;

    iget-object v1, p0, LX/AL4;->a:LX/AL7;

    iget-object v1, v1, LX/AL7;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, LX/AL4;->a:LX/AL7;

    iget-object v2, v2, LX/AL7;->i:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    const/4 p0, 0x0

    .line 1664565
    add-int v3, v1, v2

    .line 1664566
    iget v4, v0, LX/AL7;->d:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 1664567
    iput v3, v0, LX/AL7;->d:I

    .line 1664568
    iput v3, v0, LX/AL7;->e:I

    .line 1664569
    iput v1, v0, LX/AL7;->f:I

    .line 1664570
    iget-object v3, v0, LX/AL7;->g:LX/AL6;

    if-eqz v3, :cond_0

    .line 1664571
    invoke-static {v0, p0}, LX/AL7;->a(LX/AL7;I)V

    .line 1664572
    :cond_0
    :goto_0
    return-void

    .line 1664573
    :cond_1
    iget v4, v0, LX/AL7;->e:I

    if-eq v4, v3, :cond_2

    iget v4, v0, LX/AL7;->f:I

    if-eq v4, v1, :cond_2

    .line 1664574
    iget v4, v0, LX/AL7;->d:I

    sub-int/2addr v4, v3

    invoke-static {v4, p0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1664575
    iget-object v5, v0, LX/AL7;->g:LX/AL6;

    if-eqz v5, :cond_2

    .line 1664576
    invoke-static {v0, v4}, LX/AL7;->a(LX/AL7;I)V

    .line 1664577
    :cond_2
    iput v3, v0, LX/AL7;->e:I

    .line 1664578
    iput v1, v0, LX/AL7;->f:I

    goto :goto_0
.end method
