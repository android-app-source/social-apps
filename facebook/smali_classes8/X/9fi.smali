.class public LX/9fi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/PointF;

.field public final b:I

.field public c:F


# direct methods
.method public constructor <init>(LX/9fh;LX/9fh;Landroid/graphics/PointF;I)V
    .locals 3

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 1522560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522561
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522562
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522563
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522564
    iput-object p3, p0, LX/9fi;->a:Landroid/graphics/PointF;

    .line 1522565
    invoke-virtual {p1}, LX/9fh;->a()F

    move-result v0

    invoke-virtual {p2}, LX/9fh;->a()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/9fi;->c:F

    .line 1522566
    iget v0, p0, LX/9fi;->c:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 1522567
    iput v2, p0, LX/9fi;->c:F

    .line 1522568
    :cond_0
    iget v0, p0, LX/9fi;->c:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-int/lit8 v1, p4, 0x2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/16 v0, 0x1f4

    :goto_0
    iput v0, p0, LX/9fi;->b:I

    .line 1522569
    return-void

    .line 1522570
    :cond_1
    const/16 v0, 0xfa

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/animation/Animation$AnimationListener;)V
    .locals 5

    .prologue
    .line 1522571
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1522572
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1522573
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v4, p0, LX/9fi;->c:F

    add-float/2addr v3, v4

    iget-object v4, p0, LX/9fi;->a:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1522574
    iget v1, p0, LX/9fi;->b:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1522575
    if-eqz p2, :cond_0

    .line 1522576
    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1522577
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 1522578
    invoke-virtual {p1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1522579
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 1522580
    return-void
.end method
