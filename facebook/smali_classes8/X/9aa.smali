.class public final LX/9aa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9ab;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final synthetic h:LX/9ac;


# direct methods
.method public constructor <init>(LX/9ac;LX/9ab;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 0

    .prologue
    .line 1513872
    iput-object p1, p0, LX/9aa;->h:LX/9ac;

    iput-object p2, p0, LX/9aa;->a:LX/9ab;

    iput-object p3, p0, LX/9aa;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9aa;->c:Ljava/lang/String;

    iput-object p5, p0, LX/9aa;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object p6, p0, LX/9aa;->e:Ljava/lang/String;

    iput-object p7, p0, LX/9aa;->f:Ljava/lang/String;

    iput-object p8, p0, LX/9aa;->g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1513860
    const/4 v6, 0x0

    .line 1513861
    iget-object v0, p0, LX/9aa;->a:LX/9ab;

    if-eqz v0, :cond_1

    .line 1513862
    iget-object v0, p0, LX/9aa;->h:LX/9ac;

    iget-object v1, v0, LX/9ac;->c:LX/9fy;

    iget-object v0, p0, LX/9aa;->a:LX/9ab;

    iget-wide v2, v0, LX/9ab;->a:J

    iget-object v4, p0, LX/9aa;->b:Ljava/lang/String;

    iget-object v5, p0, LX/9aa;->c:Ljava/lang/String;

    iget-object v0, p0, LX/9aa;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v0, :cond_0

    :goto_0
    iget-object v0, p0, LX/9aa;->a:LX/9ab;

    iget-object v7, v0, LX/9ab;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual/range {v1 .. v7}, LX/9fy;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1513863
    :goto_1
    return-object v0

    .line 1513864
    :cond_0
    iget-object v0, p0, LX/9aa;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1513865
    :cond_1
    iget-object v0, p0, LX/9aa;->h:LX/9ac;

    iget-object v0, v0, LX/9ac;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1513866
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1513867
    if-eqz v0, :cond_3

    .line 1513868
    iget-object v0, p0, LX/9aa;->h:LX/9ac;

    iget-object v1, v0, LX/9ac;->c:LX/9fy;

    iget-object v0, p0, LX/9aa;->h:LX/9ac;

    iget-object v0, v0, LX/9ac;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1513869
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1513870
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/9aa;->b:Ljava/lang/String;

    iget-object v5, p0, LX/9aa;->c:Ljava/lang/String;

    iget-object v0, p0, LX/9aa;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v0, :cond_2

    :goto_2
    iget-object v0, p0, LX/9aa;->h:LX/9ac;

    iget-object v7, v0, LX/9ac;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual/range {v1 .. v7}, LX/9fy;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, LX/9aa;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 1513871
    :cond_3
    iget-object v0, p0, LX/9aa;->h:LX/9ac;

    iget-object v0, v0, LX/9ac;->c:LX/9fy;

    iget-object v1, p0, LX/9aa;->b:Ljava/lang/String;

    iget-object v2, p0, LX/9aa;->e:Ljava/lang/String;

    iget-object v3, p0, LX/9aa;->c:Ljava/lang/String;

    iget-object v4, p0, LX/9aa;->f:Ljava/lang/String;

    iget-object v5, p0, LX/9aa;->g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v5, :cond_4

    iget-object v5, p0, LX/9aa;->g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v5, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v6, LX/2rw;->GROUP:LX/2rw;

    if-ne v5, v6, :cond_4

    iget-object v5, p0, LX/9aa;->g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v6, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-virtual/range {v0 .. v5}, LX/9fy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v5, ""

    goto :goto_3
.end method
