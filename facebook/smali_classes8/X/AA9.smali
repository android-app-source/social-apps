.class public final LX/AA9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1637114
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1637115
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1637116
    :goto_0
    return v1

    .line 1637117
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_3

    .line 1637118
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1637119
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1637120
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1637121
    const-string v7, "lower_bound"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1637122
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 1637123
    :cond_1
    const-string v7, "upper_bound"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1637124
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1637125
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1637126
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1637127
    if-eqz v3, :cond_4

    .line 1637128
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1637129
    :cond_4
    if-eqz v0, :cond_5

    .line 1637130
    invoke-virtual {p1, v2, v4, v1}, LX/186;->a(III)V

    .line 1637131
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1637132
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1637133
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637134
    if-eqz v0, :cond_0

    .line 1637135
    const-string v1, "lower_bound"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637136
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637137
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637138
    if-eqz v0, :cond_1

    .line 1637139
    const-string v1, "upper_bound"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637140
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637141
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1637142
    return-void
.end method
