.class public final LX/A9r;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1636065
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 1636066
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1636067
    :goto_0
    return v1

    .line 1636068
    :cond_0
    const-string v12, "impressions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1636069
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v2

    .line 1636070
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 1636071
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1636072
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1636073
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1636074
    const-string v12, "cta_clicks"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1636075
    invoke-static {p0, p1}, LX/A9q;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1636076
    :cond_2
    const-string v12, "page_likes"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1636077
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 1636078
    :cond_3
    const-string v12, "reach"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1636079
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 1636080
    :cond_4
    const-string v12, "website_clicks"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1636081
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 1636082
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1636083
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1636084
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 1636085
    if-eqz v5, :cond_7

    .line 1636086
    invoke-virtual {p1, v2, v9, v1}, LX/186;->a(III)V

    .line 1636087
    :cond_7
    if-eqz v4, :cond_8

    .line 1636088
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 1636089
    :cond_8
    if-eqz v3, :cond_9

    .line 1636090
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 1636091
    :cond_9
    if-eqz v0, :cond_a

    .line 1636092
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 1636093
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1636094
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636095
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1636096
    if-eqz v0, :cond_3

    .line 1636097
    const-string v1, "cta_clicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636098
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1636099
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1636100
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x1

    const/4 v4, 0x0

    .line 1636101
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636102
    invoke-virtual {p0, v3, v4, v4}, LX/15i;->a(III)I

    move-result v4

    .line 1636103
    if-eqz v4, :cond_0

    .line 1636104
    const-string v5, "clicks"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636105
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1636106
    :cond_0
    invoke-virtual {p0, v3, p3}, LX/15i;->g(II)I

    move-result v4

    .line 1636107
    if-eqz v4, :cond_1

    .line 1636108
    const-string v4, "cta_type"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636109
    invoke-virtual {p0, v3, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636110
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636111
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1636112
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1636113
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1636114
    if-eqz v0, :cond_4

    .line 1636115
    const-string v1, "impressions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636116
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1636117
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1636118
    if-eqz v0, :cond_5

    .line 1636119
    const-string v1, "page_likes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636120
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1636121
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1636122
    if-eqz v0, :cond_6

    .line 1636123
    const-string v1, "reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636124
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1636125
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1636126
    if-eqz v0, :cond_7

    .line 1636127
    const-string v1, "website_clicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636128
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1636129
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636130
    return-void
.end method
