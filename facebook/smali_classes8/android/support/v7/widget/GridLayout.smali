.class public Landroid/support/v7/widget/GridLayout;
.super Landroid/view/ViewGroup;
.source ""


# static fields
.field private static final A:I

.field private static final B:LX/8vz;

.field private static final C:LX/8vz;

.field public static final a:Landroid/util/Printer;

.field public static final b:Landroid/util/Printer;

.field public static final k:LX/8vz;

.field public static final l:LX/8vz;

.field public static final m:LX/8vz;

.field public static final n:LX/8vz;

.field public static final o:LX/8vz;

.field public static final p:LX/8vz;

.field public static final q:LX/8vz;

.field public static final r:LX/8vz;

.field public static final s:LX/8vz;

.field public static final t:LX/8vz;

.field private static final u:I

.field private static final v:I

.field private static final w:I

.field private static final x:I

.field private static final y:I

.field private static final z:I


# instance fields
.field public final c:LX/8wC;

.field public final d:LX/8wC;

.field public e:I

.field public f:Z

.field public g:I

.field public h:I

.field public i:I

.field public j:Landroid/util/Printer;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1422470
    new-instance v0, Landroid/util/LogPrinter;

    const/4 v1, 0x3

    const-class v2, Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/LogPrinter;-><init>(ILjava/lang/String;)V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->a:Landroid/util/Printer;

    .line 1422471
    new-instance v0, LX/8vy;

    invoke-direct {v0}, LX/8vy;-><init>()V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->b:Landroid/util/Printer;

    .line 1422472
    const/16 v0, 0x0

    sput v0, Landroid/support/v7/widget/GridLayout;->u:I

    .line 1422473
    const/16 v0, 0x1

    sput v0, Landroid/support/v7/widget/GridLayout;->v:I

    .line 1422474
    const/16 v0, 0x2

    sput v0, Landroid/support/v7/widget/GridLayout;->w:I

    .line 1422475
    const/16 v0, 0x3

    sput v0, Landroid/support/v7/widget/GridLayout;->x:I

    .line 1422476
    const/16 v0, 0x4

    sput v0, Landroid/support/v7/widget/GridLayout;->y:I

    .line 1422477
    const/16 v0, 0x5

    sput v0, Landroid/support/v7/widget/GridLayout;->z:I

    .line 1422478
    const/16 v0, 0x6

    sput v0, Landroid/support/v7/widget/GridLayout;->A:I

    .line 1422479
    new-instance v0, LX/8w0;

    invoke-direct {v0}, LX/8w0;-><init>()V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->k:LX/8vz;

    .line 1422480
    new-instance v0, LX/8w1;

    invoke-direct {v0}, LX/8w1;-><init>()V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->B:LX/8vz;

    .line 1422481
    new-instance v0, LX/8w2;

    invoke-direct {v0}, LX/8w2;-><init>()V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->C:LX/8vz;

    .line 1422482
    sget-object v0, Landroid/support/v7/widget/GridLayout;->B:LX/8vz;

    sput-object v0, Landroid/support/v7/widget/GridLayout;->l:LX/8vz;

    .line 1422483
    sget-object v0, Landroid/support/v7/widget/GridLayout;->C:LX/8vz;

    sput-object v0, Landroid/support/v7/widget/GridLayout;->m:LX/8vz;

    .line 1422484
    sget-object v0, Landroid/support/v7/widget/GridLayout;->B:LX/8vz;

    sput-object v0, Landroid/support/v7/widget/GridLayout;->n:LX/8vz;

    .line 1422485
    sget-object v0, Landroid/support/v7/widget/GridLayout;->C:LX/8vz;

    sput-object v0, Landroid/support/v7/widget/GridLayout;->o:LX/8vz;

    .line 1422486
    sget-object v0, Landroid/support/v7/widget/GridLayout;->n:LX/8vz;

    sget-object v1, Landroid/support/v7/widget/GridLayout;->o:LX/8vz;

    invoke-static {v0, v1}, Landroid/support/v7/widget/GridLayout;->a(LX/8vz;LX/8vz;)LX/8vz;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/GridLayout;->p:LX/8vz;

    .line 1422487
    sget-object v0, Landroid/support/v7/widget/GridLayout;->o:LX/8vz;

    sget-object v1, Landroid/support/v7/widget/GridLayout;->n:LX/8vz;

    invoke-static {v0, v1}, Landroid/support/v7/widget/GridLayout;->a(LX/8vz;LX/8vz;)LX/8vz;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/GridLayout;->q:LX/8vz;

    .line 1422488
    new-instance v0, LX/8w4;

    invoke-direct {v0}, LX/8w4;-><init>()V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->r:LX/8vz;

    .line 1422489
    new-instance v0, LX/8w7;

    invoke-direct {v0}, LX/8w7;-><init>()V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->s:LX/8vz;

    .line 1422490
    new-instance v0, LX/8w8;

    invoke-direct {v0}, LX/8w8;-><init>()V

    sput-object v0, Landroid/support/v7/widget/GridLayout;->t:LX/8vz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1422353
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1422354
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1422348
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1422349
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1422355
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1422356
    new-instance v0, LX/8wC;

    invoke-direct {v0, p0, v2}, LX/8wC;-><init>(Landroid/support/v7/widget/GridLayout;Z)V

    iput-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    .line 1422357
    new-instance v0, LX/8wC;

    invoke-direct {v0, p0, v1}, LX/8wC;-><init>(Landroid/support/v7/widget/GridLayout;Z)V

    iput-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    .line 1422358
    iput v1, p0, Landroid/support/v7/widget/GridLayout;->e:I

    .line 1422359
    iput-boolean v1, p0, Landroid/support/v7/widget/GridLayout;->f:Z

    .line 1422360
    iput v2, p0, Landroid/support/v7/widget/GridLayout;->g:I

    .line 1422361
    iput v1, p0, Landroid/support/v7/widget/GridLayout;->i:I

    .line 1422362
    sget-object v0, Landroid/support/v7/widget/GridLayout;->a:Landroid/util/Printer;

    iput-object v0, p0, Landroid/support/v7/widget/GridLayout;->j:Landroid/util/Printer;

    .line 1422363
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b11e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/GridLayout;->h:I

    .line 1422364
    sget-object v0, LX/03r;->GridLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1422365
    :try_start_0
    sget v0, Landroid/support/v7/widget/GridLayout;->v:I

    const/high16 v2, -0x80000000

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/GridLayout;->setRowCount(I)V

    .line 1422366
    sget v0, Landroid/support/v7/widget/GridLayout;->w:I

    const/high16 v2, -0x80000000

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/GridLayout;->setColumnCount(I)V

    .line 1422367
    sget v0, Landroid/support/v7/widget/GridLayout;->u:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/GridLayout;->setOrientation(I)V

    .line 1422368
    sget v0, Landroid/support/v7/widget/GridLayout;->x:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/GridLayout;->setUseDefaultMargins(Z)V

    .line 1422369
    sget v0, Landroid/support/v7/widget/GridLayout;->y:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/GridLayout;->setAlignmentMode(I)V

    .line 1422370
    sget v0, Landroid/support/v7/widget/GridLayout;->z:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/GridLayout;->setRowOrderPreserved(Z)V

    .line 1422371
    sget v0, Landroid/support/v7/widget/GridLayout;->A:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/GridLayout;->setColumnOrderPreserved(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1422372
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1422373
    return-void

    .line 1422374
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 1422375
    add-int v0, p0, p1

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0
.end method

.method private static a(LX/8wD;ZI)I
    .locals 2

    .prologue
    .line 1422376
    invoke-virtual {p0}, LX/8wD;->a()I

    move-result v1

    .line 1422377
    if-nez p2, :cond_0

    move v0, v1

    .line 1422378
    :goto_0
    return v0

    .line 1422379
    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, LX/8wD;->a:I

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1422380
    :goto_1
    sub-int v0, p2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 1422381
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;Z)I
    .locals 1

    .prologue
    .line 1422382
    invoke-static {p1, p2}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;Z)I

    move-result v0

    return v0
.end method

.method private a(Landroid/view/View;LX/8wE;ZZ)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1422383
    iget-boolean v0, p0, Landroid/support/v7/widget/GridLayout;->f:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 1422384
    :goto_0
    return v0

    .line 1422385
    :cond_0
    if-eqz p3, :cond_3

    iget-object v0, p2, LX/8wE;->b:LX/8wH;

    move-object v2, v0

    .line 1422386
    :goto_1
    if-eqz p3, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    .line 1422387
    :goto_2
    iget-object v2, v2, LX/8wH;->c:LX/8wD;

    .line 1422388
    if-eqz p3, :cond_5

    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez p4, :cond_1

    const/4 v1, 0x1

    .line 1422389
    :cond_1
    :goto_3
    if-eqz v1, :cond_6

    iget v0, v2, LX/8wD;->a:I

    if-eqz v0, :cond_2

    .line 1422390
    :cond_2
    :goto_4
    invoke-direct {p0, p1, p3, p4}, Landroid/support/v7/widget/GridLayout;->b(Landroid/view/View;ZZ)I

    move-result v0

    goto :goto_0

    .line 1422391
    :cond_3
    iget-object v0, p2, LX/8wE;->a:LX/8wH;

    move-object v2, v0

    goto :goto_1

    .line 1422392
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    goto :goto_2

    :cond_5
    move v1, p4

    .line 1422393
    goto :goto_3

    .line 1422394
    :cond_6
    invoke-virtual {v0}, LX/8wC;->a()I

    goto :goto_4
.end method

.method public static a([II)I
    .locals 3

    .prologue
    .line 1422395
    const/4 v0, 0x0

    array-length v1, p0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1422396
    aget v2, p0, v0

    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 1422397
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1422398
    :cond_0
    return p1
.end method

.method public static a(IZ)LX/8vz;
    .locals 2

    .prologue
    .line 1422399
    if-eqz p1, :cond_0

    const/4 v0, 0x7

    move v1, v0

    .line 1422400
    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 1422401
    :goto_1
    and-int/2addr v1, p0

    shr-int v0, v1, v0

    .line 1422402
    sparse-switch v0, :sswitch_data_0

    .line 1422403
    sget-object v0, Landroid/support/v7/widget/GridLayout;->k:LX/8vz;

    :goto_2
    return-object v0

    .line 1422404
    :cond_0
    const/16 v0, 0x70

    move v1, v0

    goto :goto_0

    .line 1422405
    :cond_1
    const/4 v0, 0x4

    goto :goto_1

    .line 1422406
    :sswitch_0
    if-eqz p1, :cond_2

    sget-object v0, Landroid/support/v7/widget/GridLayout;->p:LX/8vz;

    goto :goto_2

    :cond_2
    sget-object v0, Landroid/support/v7/widget/GridLayout;->l:LX/8vz;

    goto :goto_2

    .line 1422407
    :sswitch_1
    if-eqz p1, :cond_3

    sget-object v0, Landroid/support/v7/widget/GridLayout;->q:LX/8vz;

    goto :goto_2

    :cond_3
    sget-object v0, Landroid/support/v7/widget/GridLayout;->m:LX/8vz;

    goto :goto_2

    .line 1422408
    :sswitch_2
    sget-object v0, Landroid/support/v7/widget/GridLayout;->t:LX/8vz;

    goto :goto_2

    .line 1422409
    :sswitch_3
    sget-object v0, Landroid/support/v7/widget/GridLayout;->r:LX/8vz;

    goto :goto_2

    .line 1422410
    :sswitch_4
    sget-object v0, Landroid/support/v7/widget/GridLayout;->n:LX/8vz;

    goto :goto_2

    .line 1422411
    :sswitch_5
    sget-object v0, Landroid/support/v7/widget/GridLayout;->o:LX/8vz;

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0x7 -> :sswitch_2
        0x800003 -> :sswitch_4
        0x800005 -> :sswitch_5
    .end sparse-switch
.end method

.method private static a(LX/8vz;LX/8vz;)LX/8vz;
    .locals 1

    .prologue
    .line 1422412
    new-instance v0, LX/8w3;

    invoke-direct {v0, p0, p1}, LX/8w3;-><init>(LX/8vz;LX/8vz;)V

    return-object v0
.end method

.method public static a(LX/8vz;Z)LX/8vz;
    .locals 1

    .prologue
    .line 1422413
    sget-object v0, Landroid/support/v7/widget/GridLayout;->k:LX/8vz;

    if-eq p0, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    if-eqz p1, :cond_1

    sget-object p0, Landroid/support/v7/widget/GridLayout;->n:LX/8vz;

    goto :goto_0

    :cond_1
    sget-object p0, Landroid/support/v7/widget/GridLayout;->s:LX/8vz;

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;)LX/8wE;
    .locals 2

    .prologue
    .line 1422414
    new-instance v0, LX/8wE;

    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/8wE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public static a(Landroid/view/View;)LX/8wE;
    .locals 1

    .prologue
    .line 1422415
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8wE;

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)LX/8wE;
    .locals 1

    .prologue
    .line 1422416
    new-instance v0, LX/8wE;

    invoke-direct {v0, p0}, LX/8wE;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public static a(I)LX/8wH;
    .locals 1

    .prologue
    .line 1422417
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/support/v7/widget/GridLayout;->b(II)LX/8wH;

    move-result-object v0

    return-object v0
.end method

.method private static a(IILX/8vz;)LX/8wH;
    .locals 1

    .prologue
    .line 1422469
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Landroid/support/v7/widget/GridLayout;->a(IILX/8vz;F)LX/8wH;

    move-result-object v0

    return-object v0
.end method

.method public static a(IILX/8vz;F)LX/8wH;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1422418
    new-instance v0, LX/8wH;

    const/high16 v1, -0x80000000

    if-eq p0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v2, p0

    move v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, LX/8wH;-><init>(ZIILX/8vz;FB)V

    return-object v0

    :cond_0
    move v1, v6

    goto :goto_0
.end method

.method private a(IIZ)V
    .locals 13

    .prologue
    .line 1422491
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v12

    move v11, v0

    :goto_0
    if-ge v11, v12, :cond_6

    .line 1422492
    invoke-virtual {p0, v11}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1422493
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    .line 1422494
    invoke-static {v1}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v3

    .line 1422495
    if-eqz p3, :cond_1

    .line 1422496
    iget v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    move-object v0, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;IIII)V

    .line 1422497
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-static {v0, v11}, LX/8wC;->d(LX/8wC;I)V

    .line 1422498
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-static {v0, v11}, LX/8wC;->d(LX/8wC;I)V

    .line 1422499
    :cond_0
    :goto_1
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_0

    .line 1422500
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->e:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 1422501
    :goto_2
    if-eqz v2, :cond_3

    iget-object v0, v3, LX/8wE;->b:LX/8wH;

    .line 1422502
    :goto_3
    iget-object v4, v0, LX/8wH;->d:LX/8vz;

    sget-object v5, Landroid/support/v7/widget/GridLayout;->t:LX/8vz;

    if-ne v4, v5, :cond_0

    .line 1422503
    iget-object v4, v0, LX/8wH;->c:LX/8wD;

    .line 1422504
    if-eqz v2, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    .line 1422505
    :goto_4
    invoke-virtual {v0}, LX/8wC;->e()[I

    move-result-object v0

    .line 1422506
    iget v5, v4, LX/8wD;->b:I

    aget v5, v0, v5

    iget v4, v4, LX/8wD;->a:I

    aget v0, v0, v4

    sub-int v0, v5, v0

    .line 1422507
    invoke-direct {p0, v1, v2}, Landroid/support/v7/widget/GridLayout;->b(Landroid/view/View;Z)I

    move-result v4

    sub-int v4, v0, v4

    .line 1422508
    if-eqz v2, :cond_5

    .line 1422509
    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    move-object v0, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;IIII)V

    goto :goto_1

    .line 1422510
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto :goto_2

    .line 1422511
    :cond_3
    iget-object v0, v3, LX/8wE;->a:LX/8wH;

    goto :goto_3

    .line 1422512
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    goto :goto_4

    .line 1422513
    :cond_5
    iget v9, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    move-object v5, p0

    move-object v6, v1

    move v7, p1

    move v8, p2

    move v10, v4

    invoke-direct/range {v5 .. v10}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;IIII)V

    goto :goto_1

    .line 1422514
    :cond_6
    return-void
.end method

.method private static a(LX/8wE;IIII)V
    .locals 2

    .prologue
    .line 1422540
    new-instance v0, LX/8wD;

    add-int v1, p1, p2

    invoke-direct {v0, p1, v1}, LX/8wD;-><init>(II)V

    invoke-virtual {p0, v0}, LX/8wE;->a(LX/8wD;)V

    .line 1422541
    new-instance v0, LX/8wD;

    add-int v1, p3, p4

    invoke-direct {v0, p3, v1}, LX/8wD;-><init>(II)V

    invoke-virtual {p0, v0}, LX/8wE;->b(LX/8wD;)V

    .line 1422542
    return-void
.end method

.method private a(LX/8wE;Z)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 1422515
    if-eqz p2, :cond_3

    const-string v0, "column"

    move-object v1, v0

    .line 1422516
    :goto_0
    if-eqz p2, :cond_4

    iget-object v0, p1, LX/8wE;->b:LX/8wH;

    .line 1422517
    :goto_1
    iget-object v2, v0, LX/8wH;->c:LX/8wD;

    .line 1422518
    iget v0, v2, LX/8wD;->a:I

    if-eq v0, v4, :cond_0

    iget v0, v2, LX/8wD;->a:I

    if-gez v0, :cond_0

    .line 1422519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " indices must be positive"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->b(Ljava/lang/String;)V

    .line 1422520
    :cond_0
    if-eqz p2, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    .line 1422521
    :goto_2
    iget v0, v0, LX/8wC;->b:I

    .line 1422522
    if-eq v0, v4, :cond_2

    .line 1422523
    iget v3, v2, LX/8wD;->b:I

    if-le v3, v0, :cond_1

    .line 1422524
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " indices (start + span) mustn\'t exceed the "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " count"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/GridLayout;->b(Ljava/lang/String;)V

    .line 1422525
    :cond_1
    invoke-virtual {v2}, LX/8wD;->a()I

    move-result v2

    if-le v2, v0, :cond_2

    .line 1422526
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " span mustn\'t exceed the "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " count"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->b(Ljava/lang/String;)V

    .line 1422527
    :cond_2
    return-void

    .line 1422528
    :cond_3
    const-string v0, "row"

    move-object v1, v0

    goto :goto_0

    .line 1422529
    :cond_4
    iget-object v0, p1, LX/8wE;->a:LX/8wH;

    goto :goto_1

    .line 1422530
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    goto :goto_2
.end method

.method private a(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 1422536
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/GridLayout;->b(Landroid/view/View;Z)I

    move-result v0

    invoke-static {p2, v0, p4}, Landroid/support/v7/widget/GridLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 1422537
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Landroid/support/v7/widget/GridLayout;->b(Landroid/view/View;Z)I

    move-result v1

    invoke-static {p3, v1, p5}, Landroid/support/v7/widget/GridLayout;->getChildMeasureSpec(III)I

    move-result v1

    .line 1422538
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1422539
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1422468
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([IIII)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1422531
    array-length v1, p0

    if-le p3, v1, :cond_1

    .line 1422532
    :goto_0
    return v0

    .line 1422533
    :cond_0
    add-int/lit8 p2, p2, 0x1

    :cond_1
    if-ge p2, p3, :cond_2

    .line 1422534
    aget v1, p0, p2

    if-le v1, p1, :cond_0

    goto :goto_0

    .line 1422535
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;[TT;)[TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1422464
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    array-length v1, p0

    array-length v2, p1

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 1422465
    array-length v1, p0

    invoke-static {p0, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1422466
    array-length v1, p0

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1422467
    return-object v0
.end method

.method private b(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 1422461
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, LX/8wI;

    if-ne v0, v1, :cond_0

    .line 1422462
    const/4 v0, 0x0

    .line 1422463
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->h:I

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private b(Landroid/view/View;Z)I
    .locals 2

    .prologue
    .line 1422460
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;ZZ)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;ZZ)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private b(Landroid/view/View;ZZ)I
    .locals 1

    .prologue
    .line 1422459
    invoke-direct {p0, p1}, Landroid/support/v7/widget/GridLayout;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static b(II)LX/8wH;
    .locals 1

    .prologue
    .line 1422458
    sget-object v0, Landroid/support/v7/widget/GridLayout;->k:LX/8vz;

    invoke-static {p0, p1, v0}, Landroid/support/v7/widget/GridLayout;->a(IILX/8vz;)LX/8wH;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 15

    .prologue
    const/4 v2, 0x0

    .line 1422420
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->e:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v7, v0

    .line 1422421
    :goto_0
    if-eqz v7, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    .line 1422422
    :goto_1
    iget v1, v0, LX/8wC;->b:I

    const/high16 v3, -0x80000000

    if-eq v1, v3, :cond_4

    iget v0, v0, LX/8wC;->b:I

    move v1, v0

    .line 1422423
    :goto_2
    new-array v8, v1, [I

    .line 1422424
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v9

    move v6, v2

    move v5, v2

    move v4, v2

    :goto_3
    if-ge v6, v9, :cond_c

    .line 1422425
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8wE;

    .line 1422426
    if-eqz v7, :cond_5

    iget-object v3, v0, LX/8wE;->a:LX/8wH;

    .line 1422427
    :goto_4
    iget-object v10, v3, LX/8wH;->c:LX/8wD;

    .line 1422428
    iget-boolean v11, v3, LX/8wH;->b:Z

    .line 1422429
    invoke-virtual {v10}, LX/8wD;->a()I

    move-result v12

    .line 1422430
    if-eqz v11, :cond_0

    .line 1422431
    iget v4, v10, LX/8wD;->a:I

    .line 1422432
    :cond_0
    if-eqz v7, :cond_6

    iget-object v3, v0, LX/8wE;->b:LX/8wH;

    .line 1422433
    :goto_5
    iget-object v10, v3, LX/8wH;->c:LX/8wD;

    .line 1422434
    iget-boolean v13, v3, LX/8wH;->b:Z

    .line 1422435
    invoke-static {v10, v13, v1}, Landroid/support/v7/widget/GridLayout;->a(LX/8wD;ZI)I

    move-result v14

    .line 1422436
    if-eqz v13, :cond_d

    .line 1422437
    iget v3, v10, LX/8wD;->a:I

    .line 1422438
    :goto_6
    if-eqz v1, :cond_a

    .line 1422439
    if-eqz v11, :cond_1

    if-nez v13, :cond_9

    .line 1422440
    :cond_1
    :goto_7
    add-int v5, v3, v14

    invoke-static {v8, v4, v3, v5}, Landroid/support/v7/widget/GridLayout;->a([IIII)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1422441
    if-eqz v13, :cond_7

    .line 1422442
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_2
    move v7, v2

    .line 1422443
    goto :goto_0

    .line 1422444
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    goto :goto_1

    :cond_4
    move v1, v2

    .line 1422445
    goto :goto_2

    .line 1422446
    :cond_5
    iget-object v3, v0, LX/8wE;->b:LX/8wH;

    goto :goto_4

    .line 1422447
    :cond_6
    iget-object v3, v0, LX/8wE;->a:LX/8wH;

    goto :goto_5

    .line 1422448
    :cond_7
    add-int v5, v3, v14

    if-gt v5, v1, :cond_8

    .line 1422449
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1422450
    :cond_8
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_7

    .line 1422451
    :cond_9
    add-int v5, v3, v14

    add-int v10, v4, v12

    invoke-static {v8, v3, v5, v10}, Landroid/support/v7/widget/GridLayout;->b([IIII)V

    .line 1422452
    :cond_a
    if-eqz v7, :cond_b

    .line 1422453
    invoke-static {v0, v4, v12, v3, v14}, Landroid/support/v7/widget/GridLayout;->a(LX/8wE;IIII)V

    .line 1422454
    :goto_8
    add-int v5, v3, v14

    .line 1422455
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_3

    .line 1422456
    :cond_b
    invoke-static {v0, v3, v14, v4, v12}, Landroid/support/v7/widget/GridLayout;->a(LX/8wE;IIII)V

    goto :goto_8

    .line 1422457
    :cond_c
    return-void

    :cond_d
    move v3, v5

    goto :goto_6
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1422419
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b([IIII)V
    .locals 2

    .prologue
    .line 1422350
    array-length v0, p0

    .line 1422351
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p0, v1, v0, p3}, Ljava/util/Arrays;->fill([IIII)V

    .line 1422352
    return-void
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 1422241
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/view/View;Z)I
    .locals 1

    .prologue
    .line 1422239
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method private c(Landroid/view/View;ZZ)I
    .locals 2

    .prologue
    .line 1422226
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1422227
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;ZZ)I

    move-result v0

    .line 1422228
    :goto_0
    return v0

    .line 1422229
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    .line 1422230
    :goto_1
    if-eqz p3, :cond_2

    invoke-virtual {v0}, LX/8wC;->c()[I

    move-result-object v0

    .line 1422231
    :goto_2
    invoke-static {p1}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v1

    .line 1422232
    if-eqz p2, :cond_3

    iget-object v1, v1, LX/8wE;->b:LX/8wH;

    .line 1422233
    :goto_3
    if-eqz p3, :cond_4

    iget-object v1, v1, LX/8wH;->c:LX/8wD;

    iget v1, v1, LX/8wD;->a:I

    .line 1422234
    :goto_4
    aget v0, v0, v1

    goto :goto_0

    .line 1422235
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    goto :goto_1

    .line 1422236
    :cond_2
    invoke-virtual {v0}, LX/8wC;->d()[I

    move-result-object v0

    goto :goto_2

    .line 1422237
    :cond_3
    iget-object v1, v1, LX/8wE;->a:LX/8wH;

    goto :goto_3

    .line 1422238
    :cond_4
    iget-object v1, v1, LX/8wH;->c:LX/8wD;

    iget v1, v1, LX/8wD;->b:I

    goto :goto_4
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1422221
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/GridLayout;->i:I

    .line 1422222
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v0}, LX/8wC;->f()V

    .line 1422223
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v0}, LX/8wC;->f()V

    .line 1422224
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->d()V

    .line 1422225
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1422217
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    if-eqz v0, :cond_0

    .line 1422218
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v0}, LX/8wC;->g()V

    .line 1422219
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v0}, LX/8wC;->g()V

    .line 1422220
    :cond_0
    return-void
.end method

.method private static e()LX/8wE;
    .locals 1

    .prologue
    .line 1422216
    new-instance v0, LX/8wE;

    invoke-direct {v0}, LX/8wE;-><init>()V

    return-object v0
.end method

.method private f()I
    .locals 6

    .prologue
    .line 1422208
    const/4 v1, 0x1

    .line 1422209
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1422210
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1422211
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    .line 1422212
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8wE;

    .line 1422213
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {v0}, LX/8wE;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    .line 1422214
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1422215
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1422200
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->i:I

    if-nez v0, :cond_1

    .line 1422201
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->b()V

    .line 1422202
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->f()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/GridLayout;->i:I

    .line 1422203
    :cond_0
    :goto_0
    return-void

    .line 1422204
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->i:I

    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->f()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1422205
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->j:Landroid/util/Printer;

    const-string v1, "The fields of some layout parameters were modified in between layout operations. Check the javadoc for GridLayout.LayoutParams#rowSpec."

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 1422206
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->c()V

    .line 1422207
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->g()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Z)I
    .locals 2

    .prologue
    .line 1422197
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1422198
    const/4 v0, 0x0

    .line 1422199
    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;Z)I

    move-result v0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/GridLayout;->b(Landroid/view/View;Z)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ZZ)I
    .locals 3

    .prologue
    .line 1422193
    invoke-static {p1}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v1

    .line 1422194
    if-eqz p2, :cond_2

    if-eqz p3, :cond_1

    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1422195
    :goto_0
    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_0

    invoke-direct {p0, p1, v1, p2, p3}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;LX/8wE;ZZ)I

    move-result v0

    :cond_0
    return v0

    .line 1422196
    :cond_1
    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_0

    :cond_3
    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_0
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1422187
    instance-of v2, p1, LX/8wE;

    if-nez v2, :cond_0

    .line 1422188
    :goto_0
    return v0

    .line 1422189
    :cond_0
    check-cast p1, LX/8wE;

    .line 1422190
    invoke-direct {p0, p1, v1}, Landroid/support/v7/widget/GridLayout;->a(LX/8wE;Z)V

    .line 1422191
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/GridLayout;->a(LX/8wE;Z)V

    move v0, v1

    .line 1422192
    goto :goto_0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1422186
    invoke-static {}, Landroid/support/v7/widget/GridLayout;->e()LX/8wE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1422185
    invoke-direct {p0, p1}, Landroid/support/v7/widget/GridLayout;->a(Landroid/util/AttributeSet;)LX/8wE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1422240
    invoke-static {p1}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/8wE;

    move-result-object v0

    return-object v0
.end method

.method public getAlignmentMode()I
    .locals 1

    .prologue
    .line 1422184
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->g:I

    return v0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 1422183
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v0}, LX/8wC;->a()I

    move-result v0

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 1422242
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->e:I

    return v0
.end method

.method public getPrinter()Landroid/util/Printer;
    .locals 1

    .prologue
    .line 1422243
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->j:Landroid/util/Printer;

    return-object v0
.end method

.method public getRowCount()I
    .locals 1

    .prologue
    .line 1422244
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v0}, LX/8wC;->a()I

    move-result v0

    return v0
.end method

.method public getUseDefaultMargins()Z
    .locals 1

    .prologue
    .line 1422245
    iget-boolean v0, p0, Landroid/support/v7/widget/GridLayout;->f:Z

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 32

    .prologue
    .line 1422246
    invoke-direct/range {p0 .. p0}, Landroid/support/v7/widget/GridLayout;->g()V

    .line 1422247
    sub-int v12, p4, p2

    .line 1422248
    sub-int v1, p5, p3

    .line 1422249
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/GridLayout;->getPaddingLeft()I

    move-result v13

    .line 1422250
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/GridLayout;->getPaddingTop()I

    move-result v14

    .line 1422251
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/GridLayout;->getPaddingRight()I

    move-result v15

    .line 1422252
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/GridLayout;->getPaddingBottom()I

    move-result v2

    .line 1422253
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    sub-int v4, v12, v13

    sub-int/2addr v4, v15

    invoke-virtual {v3, v4}, LX/8wC;->c(I)V

    .line 1422254
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    sub-int/2addr v1, v14

    sub-int/2addr v1, v2

    invoke-virtual {v3, v1}, LX/8wC;->c(I)V

    .line 1422255
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v1}, LX/8wC;->e()[I

    move-result-object v16

    .line 1422256
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v1}, LX/8wC;->e()[I

    move-result-object v17

    .line 1422257
    const/4 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v18

    move v11, v1

    :goto_0
    move/from16 v0, v18

    if-ge v11, v0, :cond_4

    .line 1422258
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1422259
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    .line 1422260
    invoke-static {v3}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v1

    .line 1422261
    iget-object v2, v1, LX/8wE;->b:LX/8wH;

    .line 1422262
    iget-object v1, v1, LX/8wE;->a:LX/8wH;

    .line 1422263
    iget-object v4, v2, LX/8wH;->c:LX/8wD;

    .line 1422264
    iget-object v5, v1, LX/8wH;->c:LX/8wD;

    .line 1422265
    iget v6, v4, LX/8wD;->a:I

    aget v19, v16, v6

    .line 1422266
    iget v6, v5, LX/8wD;->a:I

    aget v20, v17, v6

    .line 1422267
    iget v4, v4, LX/8wD;->b:I

    aget v4, v16, v4

    .line 1422268
    iget v5, v5, LX/8wD;->b:I

    aget v5, v17, v5

    .line 1422269
    sub-int v21, v4, v19

    .line 1422270
    sub-int v22, v5, v20

    .line 1422271
    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;Z)I

    move-result v23

    .line 1422272
    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;Z)I

    move-result v24

    .line 1422273
    iget-object v2, v2, LX/8wH;->d:LX/8vz;

    const/4 v4, 0x1

    invoke-static {v2, v4}, Landroid/support/v7/widget/GridLayout;->a(LX/8vz;Z)LX/8vz;

    move-result-object v4

    .line 1422274
    iget-object v1, v1, LX/8wH;->d:LX/8vz;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/support/v7/widget/GridLayout;->a(LX/8vz;Z)LX/8vz;

    move-result-object v8

    .line 1422275
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v1}, LX/8wC;->b()LX/8wG;

    move-result-object v1

    invoke-virtual {v1, v11}, LX/8wG;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8w5;

    .line 1422276
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v2}, LX/8wC;->b()LX/8wG;

    move-result-object v2

    invoke-virtual {v2, v11}, LX/8wG;->a(I)Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, LX/8w5;

    .line 1422277
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/8w5;->a(Z)I

    move-result v2

    sub-int v2, v21, v2

    invoke-virtual {v4, v3, v2}, LX/8vz;->a(Landroid/view/View;I)I

    move-result v25

    .line 1422278
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, LX/8w5;->a(Z)I

    move-result v2

    sub-int v2, v22, v2

    invoke-virtual {v8, v3, v2}, LX/8vz;->a(Landroid/view/View;I)I

    move-result v26

    .line 1422279
    const/4 v2, 0x1

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v5}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;ZZ)I

    move-result v27

    .line 1422280
    const/4 v2, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v5}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;ZZ)I

    move-result v28

    .line 1422281
    const/4 v2, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v5}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;ZZ)I

    move-result v29

    .line 1422282
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v5}, Landroid/support/v7/widget/GridLayout;->c(Landroid/view/View;ZZ)I

    move-result v2

    .line 1422283
    add-int v30, v27, v29

    .line 1422284
    add-int v31, v28, v2

    .line 1422285
    add-int v5, v23, v30

    const/4 v6, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, LX/8w5;->a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;LX/8vz;IZ)I

    move-result v1

    .line 1422286
    add-int v9, v24, v31

    const/4 v10, 0x0

    move-object v5, v7

    move-object/from16 v6, p0

    move-object v7, v3

    invoke-virtual/range {v5 .. v10}, LX/8w5;->a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;LX/8vz;IZ)I

    move-result v2

    .line 1422287
    sub-int v5, v21, v30

    move/from16 v0, v23

    invoke-virtual {v4, v0, v5}, LX/8vz;->a(II)I

    move-result v4

    .line 1422288
    sub-int v5, v22, v31

    move/from16 v0, v24

    invoke-virtual {v8, v0, v5}, LX/8vz;->a(II)I

    move-result v5

    .line 1422289
    add-int v6, v19, v25

    add-int/2addr v1, v6

    .line 1422290
    invoke-direct/range {p0 .. p0}, Landroid/support/v7/widget/GridLayout;->a()Z

    move-result v6

    if-nez v6, :cond_3

    add-int v6, v13, v27

    add-int/2addr v1, v6

    .line 1422291
    :goto_1
    add-int v6, v14, v20

    add-int v6, v6, v26

    add-int/2addr v2, v6

    add-int v2, v2, v28

    .line 1422292
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    if-ne v4, v6, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    if-eq v5, v6, :cond_1

    .line 1422293
    :cond_0
    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v3, v6, v7}, Landroid/view/View;->measure(II)V

    .line 1422294
    :cond_1
    add-int/2addr v4, v1

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 1422295
    :cond_2
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto/16 :goto_0

    .line 1422296
    :cond_3
    sub-int v6, v12, v4

    sub-int/2addr v6, v15

    sub-int v6, v6, v29

    sub-int v1, v6, v1

    goto :goto_1

    .line 1422297
    :cond_4
    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1422298
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->g()V

    .line 1422299
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->d()V

    .line 1422300
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getPaddingRight()I

    move-result v1

    add-int v2, v0, v1

    .line 1422301
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getPaddingBottom()I

    move-result v1

    add-int v3, v0, v1

    .line 1422302
    neg-int v0, v2

    invoke-static {p1, v0}, Landroid/support/v7/widget/GridLayout;->a(II)I

    move-result v4

    .line 1422303
    neg-int v0, v3

    invoke-static {p2, v0}, Landroid/support/v7/widget/GridLayout;->a(II)I

    move-result v5

    .line 1422304
    const/4 v0, 0x1

    invoke-direct {p0, v4, v5, v0}, Landroid/support/v7/widget/GridLayout;->a(IIZ)V

    .line 1422305
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->e:I

    if-nez v0, :cond_0

    .line 1422306
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v0, v4}, LX/8wC;->b(I)I

    move-result v1

    .line 1422307
    invoke-direct {p0, v4, v5, v6}, Landroid/support/v7/widget/GridLayout;->a(IIZ)V

    .line 1422308
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v0, v5}, LX/8wC;->b(I)I

    move-result v0

    .line 1422309
    :goto_0
    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1422310
    add-int/2addr v0, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1422311
    invoke-static {v1, p1, v6}, LX/0vv;->a(III)I

    move-result v1

    invoke-static {v0, p2, v6}, LX/0vv;->a(III)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/GridLayout;->setMeasuredDimension(II)V

    .line 1422312
    return-void

    .line 1422313
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v0, v5}, LX/8wC;->b(I)I

    move-result v0

    .line 1422314
    invoke-direct {p0, v4, v5, v6}, Landroid/support/v7/widget/GridLayout;->a(IIZ)V

    .line 1422315
    iget-object v1, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v1, v4}, LX/8wC;->b(I)I

    move-result v1

    goto :goto_0
.end method

.method public final requestLayout()V
    .locals 0

    .prologue
    .line 1422316
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1422317
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->c()V

    .line 1422318
    return-void
.end method

.method public setAlignmentMode(I)V
    .locals 0

    .prologue
    .line 1422319
    iput p1, p0, Landroid/support/v7/widget/GridLayout;->g:I

    .line 1422320
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->requestLayout()V

    .line 1422321
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1

    .prologue
    .line 1422322
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v0, p1}, LX/8wC;->a(I)V

    .line 1422323
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->c()V

    .line 1422324
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->requestLayout()V

    .line 1422325
    return-void
.end method

.method public setColumnOrderPreserved(Z)V
    .locals 1

    .prologue
    .line 1422326
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->c:LX/8wC;

    invoke-virtual {v0, p1}, LX/8wC;->a(Z)V

    .line 1422327
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->c()V

    .line 1422328
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->requestLayout()V

    .line 1422329
    return-void
.end method

.method public setOrientation(I)V
    .locals 1

    .prologue
    .line 1422330
    iget v0, p0, Landroid/support/v7/widget/GridLayout;->e:I

    if-eq v0, p1, :cond_0

    .line 1422331
    iput p1, p0, Landroid/support/v7/widget/GridLayout;->e:I

    .line 1422332
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->c()V

    .line 1422333
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->requestLayout()V

    .line 1422334
    :cond_0
    return-void
.end method

.method public setPrinter(Landroid/util/Printer;)V
    .locals 0

    .prologue
    .line 1422335
    if-nez p1, :cond_0

    sget-object p1, Landroid/support/v7/widget/GridLayout;->b:Landroid/util/Printer;

    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/GridLayout;->j:Landroid/util/Printer;

    .line 1422336
    return-void
.end method

.method public setRowCount(I)V
    .locals 1

    .prologue
    .line 1422337
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v0, p1}, LX/8wC;->a(I)V

    .line 1422338
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->c()V

    .line 1422339
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->requestLayout()V

    .line 1422340
    return-void
.end method

.method public setRowOrderPreserved(Z)V
    .locals 1

    .prologue
    .line 1422341
    iget-object v0, p0, Landroid/support/v7/widget/GridLayout;->d:LX/8wC;

    invoke-virtual {v0, p1}, LX/8wC;->a(Z)V

    .line 1422342
    invoke-direct {p0}, Landroid/support/v7/widget/GridLayout;->c()V

    .line 1422343
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->requestLayout()V

    .line 1422344
    return-void
.end method

.method public setUseDefaultMargins(Z)V
    .locals 0

    .prologue
    .line 1422345
    iput-boolean p1, p0, Landroid/support/v7/widget/GridLayout;->f:Z

    .line 1422346
    invoke-virtual {p0}, Landroid/support/v7/widget/GridLayout;->requestLayout()V

    .line 1422347
    return-void
.end method
