.class public LX/8W0;
.super LX/1a1;
.source ""

# interfaces
.implements LX/8Vr;


# instance fields
.field private final l:Lcom/facebook/widget/tiles/ThreadTileView;

.field private final m:Lcom/facebook/widget/text/BetterTextView;

.field private final n:Lcom/facebook/widget/text/BetterTextView;

.field private final o:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1353658
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1353659
    const v0, 0x7f0d1402

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/8W0;->l:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1353660
    const v0, 0x7f0d13f6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8W0;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 1353661
    const v0, 0x7f0d13f7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8W0;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 1353662
    const v0, 0x7f0d1403

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8W0;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 1353663
    return-void
.end method


# virtual methods
.method public final a(IILX/8Vb;ZLX/8Vp;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1353664
    iget-object v0, p0, LX/8W0;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p3, LX/8Vb;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1353665
    iget-object v0, p3, LX/8Vb;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1353666
    iget-object v0, p0, LX/8W0;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1353667
    :goto_0
    iget-object v0, p3, LX/8Vb;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1353668
    iget-object v0, p0, LX/8W0;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1353669
    :goto_1
    iget-object v0, p0, LX/8W0;->l:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v1, p3, LX/8Vb;->i:LX/8Vd;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1353670
    return-void

    .line 1353671
    :cond_0
    iget-object v0, p0, LX/8W0;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p3, LX/8Vb;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1353672
    :cond_1
    iget-object v0, p0, LX/8W0;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p3, LX/8Vb;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
