.class public final LX/6yG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1159636
    iput-object p1, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1159637
    iget-object v0, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    if-eqz v0, :cond_0

    .line 1159638
    iget-object v0, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    invoke-virtual {v0}, LX/6y3;->a()V

    .line 1159639
    :cond_0
    iget-object v0, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c:LX/6zA;

    iget-object v1, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-static {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6z8;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6zA;->a(LX/6z8;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1159640
    iget-object v0, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->k:LX/73a;

    iget-object v1, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v1}, LX/73a;->a(Landroid/view/View;)V

    .line 1159641
    :goto_0
    iget-object v0, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    iget-object v1, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/6y3;->a(Z)V

    .line 1159642
    return-void

    .line 1159643
    :cond_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    .line 1159644
    iget-object v0, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_0

    .line 1159645
    :cond_2
    iget-object v0, p0, LX/6yG;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159646
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159647
    return-void
.end method
