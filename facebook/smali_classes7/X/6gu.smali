.class public final LX/6gu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 63

    .prologue
    .line 1126552
    const/16 v56, 0x0

    .line 1126553
    const-wide/16 v54, 0x0

    .line 1126554
    const/16 v53, 0x0

    .line 1126555
    const/16 v52, 0x0

    .line 1126556
    const/16 v51, 0x0

    .line 1126557
    const/16 v50, 0x0

    .line 1126558
    const/16 v47, 0x0

    .line 1126559
    const-wide/16 v48, 0x0

    .line 1126560
    const/16 v46, 0x0

    .line 1126561
    const/16 v43, 0x0

    .line 1126562
    const-wide/16 v44, 0x0

    .line 1126563
    const/16 v42, 0x0

    .line 1126564
    const/16 v41, 0x0

    .line 1126565
    const/16 v40, 0x0

    .line 1126566
    const/16 v37, 0x0

    .line 1126567
    const-wide/16 v38, 0x0

    .line 1126568
    const/16 v36, 0x0

    .line 1126569
    const-wide/16 v34, 0x0

    .line 1126570
    const/16 v31, 0x0

    .line 1126571
    const-wide/16 v32, 0x0

    .line 1126572
    const/16 v30, 0x0

    .line 1126573
    const/16 v25, 0x0

    .line 1126574
    const-wide/16 v28, 0x0

    .line 1126575
    const-wide/16 v26, 0x0

    .line 1126576
    const/16 v24, 0x0

    .line 1126577
    const-wide/16 v22, 0x0

    .line 1126578
    const-wide/16 v20, 0x0

    .line 1126579
    const-wide/16 v18, 0x0

    .line 1126580
    const/16 v17, 0x0

    .line 1126581
    const/16 v16, 0x0

    .line 1126582
    const/4 v15, 0x0

    .line 1126583
    const/4 v14, 0x0

    .line 1126584
    const/4 v13, 0x0

    .line 1126585
    const/4 v12, 0x0

    .line 1126586
    const/4 v11, 0x0

    .line 1126587
    const/4 v10, 0x0

    .line 1126588
    const/4 v9, 0x0

    .line 1126589
    const/4 v8, 0x0

    .line 1126590
    const/4 v7, 0x0

    .line 1126591
    const/4 v6, 0x0

    .line 1126592
    const/4 v5, 0x0

    .line 1126593
    const/4 v4, 0x0

    .line 1126594
    const/4 v3, 0x0

    .line 1126595
    const/4 v2, 0x0

    .line 1126596
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v57

    sget-object v58, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v57

    move-object/from16 v1, v58

    if-eq v0, v1, :cond_2e

    .line 1126597
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1126598
    const/4 v2, 0x0

    .line 1126599
    :goto_0
    return v2

    .line 1126600
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_1e

    .line 1126601
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1126602
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1126603
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1126604
    const-string v6, "animation_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1126605
    invoke-static/range {p0 .. p1}, LX/6gf;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v59, v2

    goto :goto_1

    .line 1126606
    :cond_1
    const-string v6, "attraction_force_strength"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1126607
    const/4 v2, 0x1

    .line 1126608
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1126609
    :cond_2
    const-string v6, "emitter_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1126610
    invoke-static/range {p0 .. p1}, LX/6gj;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v58, v2

    goto :goto_1

    .line 1126611
    :cond_3
    const-string v6, "extra_config"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1126612
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v57, v2

    goto :goto_1

    .line 1126613
    :cond_4
    const-string v6, "gravity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1126614
    invoke-static/range {p0 .. p1}, LX/6gk;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v56, v2

    goto :goto_1

    .line 1126615
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1126616
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v55, v2

    goto :goto_1

    .line 1126617
    :cond_6
    const-string v6, "init_max_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1126618
    invoke-static/range {p0 .. p1}, LX/6gl;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v54, v2

    goto/16 :goto_1

    .line 1126619
    :cond_7
    const-string v6, "init_max_rotation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1126620
    const/4 v2, 0x1

    .line 1126621
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v21, v2

    move-wide/from16 v52, v6

    goto/16 :goto_1

    .line 1126622
    :cond_8
    const-string v6, "init_max_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1126623
    invoke-static/range {p0 .. p1}, LX/6gm;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v51, v2

    goto/16 :goto_1

    .line 1126624
    :cond_9
    const-string v6, "init_min_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1126625
    invoke-static/range {p0 .. p1}, LX/6gn;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v50, v2

    goto/16 :goto_1

    .line 1126626
    :cond_a
    const-string v6, "init_min_rotation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1126627
    const/4 v2, 0x1

    .line 1126628
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v20, v2

    move-wide/from16 v48, v6

    goto/16 :goto_1

    .line 1126629
    :cond_b
    const-string v6, "init_min_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1126630
    invoke-static/range {p0 .. p1}, LX/6go;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto/16 :goto_1

    .line 1126631
    :cond_c
    const-string v6, "init_particles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1126632
    const/4 v2, 0x1

    .line 1126633
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v19, v2

    move/from16 v46, v6

    goto/16 :goto_1

    .line 1126634
    :cond_d
    const-string v6, "max_color_hsva"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1126635
    invoke-static/range {p0 .. p1}, LX/6gp;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v45, v2

    goto/16 :goto_1

    .line 1126636
    :cond_e
    const-string v6, "max_lifetime_millis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1126637
    const/4 v2, 0x1

    .line 1126638
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v18, v2

    move/from16 v44, v6

    goto/16 :goto_1

    .line 1126639
    :cond_f
    const-string v6, "max_linear_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1126640
    const/4 v2, 0x1

    .line 1126641
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v17, v2

    move-wide/from16 v42, v6

    goto/16 :goto_1

    .line 1126642
    :cond_10
    const-string v6, "max_particles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1126643
    const/4 v2, 0x1

    .line 1126644
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v16, v2

    move/from16 v39, v6

    goto/16 :goto_1

    .line 1126645
    :cond_11
    const-string v6, "max_pixel_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1126646
    const/4 v2, 0x1

    .line 1126647
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v15, v2

    move-wide/from16 v40, v6

    goto/16 :goto_1

    .line 1126648
    :cond_12
    const-string v6, "max_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1126649
    invoke-static/range {p0 .. p1}, LX/6gq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 1126650
    :cond_13
    const-string v6, "max_rotation_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 1126651
    const/4 v2, 0x1

    .line 1126652
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v14, v2

    move-wide/from16 v36, v6

    goto/16 :goto_1

    .line 1126653
    :cond_14
    const-string v6, "min_color_hsva"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 1126654
    invoke-static/range {p0 .. p1}, LX/6gr;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 1126655
    :cond_15
    const-string v6, "min_lifetime_millis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 1126656
    const/4 v2, 0x1

    .line 1126657
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v13, v2

    move/from16 v34, v6

    goto/16 :goto_1

    .line 1126658
    :cond_16
    const-string v6, "min_linear_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 1126659
    const/4 v2, 0x1

    .line 1126660
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v12, v2

    move-wide/from16 v32, v6

    goto/16 :goto_1

    .line 1126661
    :cond_17
    const-string v6, "min_pixel_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 1126662
    const/4 v2, 0x1

    .line 1126663
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v30, v6

    goto/16 :goto_1

    .line 1126664
    :cond_18
    const-string v6, "min_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 1126665
    invoke-static/range {p0 .. p1}, LX/6gs;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 1126666
    :cond_19
    const-string v6, "min_rotation_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 1126667
    const/4 v2, 0x1

    .line 1126668
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v28, v6

    goto/16 :goto_1

    .line 1126669
    :cond_1a
    const-string v6, "rotational_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 1126670
    const/4 v2, 0x1

    .line 1126671
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v26, v6

    goto/16 :goto_1

    .line 1126672
    :cond_1b
    const-string v6, "spawn_rate"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 1126673
    const/4 v2, 0x1

    .line 1126674
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide/from16 v24, v6

    goto/16 :goto_1

    .line 1126675
    :cond_1c
    const-string v6, "velocity_scalar"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1126676
    invoke-static/range {p0 .. p1}, LX/6gt;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 1126677
    :cond_1d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1126678
    :cond_1e
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1126679
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126680
    if-eqz v3, :cond_1f

    .line 1126681
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126682
    :cond_1f
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126683
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126684
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126685
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126686
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126687
    if-eqz v21, :cond_20

    .line 1126688
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v52

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126689
    :cond_20
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126690
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126691
    if-eqz v20, :cond_21

    .line 1126692
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v48

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126693
    :cond_21
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126694
    if-eqz v19, :cond_22

    .line 1126695
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1126696
    :cond_22
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126697
    if-eqz v18, :cond_23

    .line 1126698
    const/16 v2, 0xe

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1126699
    :cond_23
    if-eqz v17, :cond_24

    .line 1126700
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v42

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126701
    :cond_24
    if-eqz v16, :cond_25

    .line 1126702
    const/16 v2, 0x10

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1126703
    :cond_25
    if-eqz v15, :cond_26

    .line 1126704
    const/16 v3, 0x11

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v40

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126705
    :cond_26
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126706
    if-eqz v14, :cond_27

    .line 1126707
    const/16 v3, 0x13

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v36

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126708
    :cond_27
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126709
    if-eqz v13, :cond_28

    .line 1126710
    const/16 v2, 0x15

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1126711
    :cond_28
    if-eqz v12, :cond_29

    .line 1126712
    const/16 v3, 0x16

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v32

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126713
    :cond_29
    if-eqz v11, :cond_2a

    .line 1126714
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v30

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126715
    :cond_2a
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126716
    if-eqz v10, :cond_2b

    .line 1126717
    const/16 v3, 0x19

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v28

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126718
    :cond_2b
    if-eqz v9, :cond_2c

    .line 1126719
    const/16 v3, 0x1a

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126720
    :cond_2c
    if-eqz v8, :cond_2d

    .line 1126721
    const/16 v3, 0x1b

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126722
    :cond_2d
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1126723
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2e
    move/from16 v57, v52

    move/from16 v58, v53

    move/from16 v59, v56

    move-wide/from16 v52, v48

    move/from16 v56, v51

    move-wide/from16 v48, v44

    move/from16 v51, v46

    move/from16 v44, v37

    move/from16 v45, v40

    move/from16 v46, v41

    move-wide/from16 v40, v34

    move/from16 v34, v25

    move/from16 v35, v30

    move/from16 v60, v15

    move v15, v9

    move v9, v3

    move/from16 v3, v16

    move/from16 v16, v10

    move v10, v4

    move-wide/from16 v61, v22

    move/from16 v23, v24

    move/from16 v22, v17

    move-wide/from16 v24, v18

    move/from16 v17, v11

    move/from16 v19, v13

    move/from16 v18, v12

    move v11, v5

    move v13, v7

    move v12, v6

    move-wide/from16 v4, v54

    move/from16 v54, v47

    move/from16 v55, v50

    move/from16 v47, v42

    move/from16 v50, v43

    move-wide/from16 v42, v38

    move/from16 v38, v31

    move/from16 v39, v36

    move-wide/from16 v36, v32

    move-wide/from16 v30, v26

    move-wide/from16 v32, v28

    move-wide/from16 v26, v20

    move/from16 v20, v14

    move-wide/from16 v28, v61

    move/from16 v21, v60

    move v14, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 1126724
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1126725
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1126726
    if-eqz v0, :cond_0

    .line 1126727
    const-string v1, "animation_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126728
    invoke-static {p0, v0, p2, p3}, LX/6gf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1126729
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126730
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1126731
    const-string v2, "attraction_force_strength"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126732
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126733
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126734
    if-eqz v0, :cond_2

    .line 1126735
    const-string v1, "emitter_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126736
    invoke-static {p0, v0, p2, p3}, LX/6gj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1126737
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1126738
    if-eqz v0, :cond_3

    .line 1126739
    const-string v1, "extra_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126740
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1126741
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126742
    if-eqz v0, :cond_4

    .line 1126743
    const-string v1, "gravity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126744
    invoke-static {p0, v0, p2}, LX/6gk;->a(LX/15i;ILX/0nX;)V

    .line 1126745
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1126746
    if-eqz v0, :cond_5

    .line 1126747
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126748
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1126749
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126750
    if-eqz v0, :cond_6

    .line 1126751
    const-string v1, "init_max_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126752
    invoke-static {p0, v0, p2}, LX/6gl;->a(LX/15i;ILX/0nX;)V

    .line 1126753
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126754
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_7

    .line 1126755
    const-string v2, "init_max_rotation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126756
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126757
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126758
    if-eqz v0, :cond_8

    .line 1126759
    const-string v1, "init_max_velocity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126760
    invoke-static {p0, v0, p2}, LX/6gm;->a(LX/15i;ILX/0nX;)V

    .line 1126761
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126762
    if-eqz v0, :cond_9

    .line 1126763
    const-string v1, "init_min_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126764
    invoke-static {p0, v0, p2}, LX/6gn;->a(LX/15i;ILX/0nX;)V

    .line 1126765
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126766
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_a

    .line 1126767
    const-string v2, "init_min_rotation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126768
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126769
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126770
    if-eqz v0, :cond_b

    .line 1126771
    const-string v1, "init_min_velocity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126772
    invoke-static {p0, v0, p2}, LX/6go;->a(LX/15i;ILX/0nX;)V

    .line 1126773
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1126774
    if-eqz v0, :cond_c

    .line 1126775
    const-string v1, "init_particles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126776
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1126777
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126778
    if-eqz v0, :cond_d

    .line 1126779
    const-string v1, "max_color_hsva"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126780
    invoke-static {p0, v0, p2}, LX/6gp;->a(LX/15i;ILX/0nX;)V

    .line 1126781
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1126782
    if-eqz v0, :cond_e

    .line 1126783
    const-string v1, "max_lifetime_millis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126784
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1126785
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126786
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_f

    .line 1126787
    const-string v2, "max_linear_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126788
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126789
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1126790
    if-eqz v0, :cond_10

    .line 1126791
    const-string v1, "max_particles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126792
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1126793
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126794
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_11

    .line 1126795
    const-string v2, "max_pixel_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126796
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126797
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126798
    if-eqz v0, :cond_12

    .line 1126799
    const-string v1, "max_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126800
    invoke-static {p0, v0, p2}, LX/6gq;->a(LX/15i;ILX/0nX;)V

    .line 1126801
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126802
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_13

    .line 1126803
    const-string v2, "max_rotation_velocity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126804
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126805
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126806
    if-eqz v0, :cond_14

    .line 1126807
    const-string v1, "min_color_hsva"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126808
    invoke-static {p0, v0, p2}, LX/6gr;->a(LX/15i;ILX/0nX;)V

    .line 1126809
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1126810
    if-eqz v0, :cond_15

    .line 1126811
    const-string v1, "min_lifetime_millis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126812
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1126813
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126814
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_16

    .line 1126815
    const-string v2, "min_linear_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126816
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126817
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126818
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_17

    .line 1126819
    const-string v2, "min_pixel_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126820
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126821
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126822
    if-eqz v0, :cond_18

    .line 1126823
    const-string v1, "min_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126824
    invoke-static {p0, v0, p2}, LX/6gs;->a(LX/15i;ILX/0nX;)V

    .line 1126825
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126826
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_19

    .line 1126827
    const-string v2, "min_rotation_velocity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126828
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126829
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126830
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1a

    .line 1126831
    const-string v2, "rotational_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126832
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126833
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126834
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1b

    .line 1126835
    const-string v2, "spawn_rate"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126836
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126837
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126838
    if-eqz v0, :cond_1c

    .line 1126839
    const-string v1, "velocity_scalar"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126840
    invoke-static {p0, v0, p2}, LX/6gt;->a(LX/15i;ILX/0nX;)V

    .line 1126841
    :cond_1c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1126842
    return-void
.end method
