.class public LX/8NY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:J

.field public final k:LX/8Oa;

.field public final l:Z


# direct methods
.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLX/0am;LX/0am;LX/8Oa;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/8Oa;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1337458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337459
    iput-wide p3, p0, LX/8NY;->a:J

    .line 1337460
    iput-wide p1, p0, LX/8NY;->b:J

    .line 1337461
    iput-object p5, p0, LX/8NY;->c:Ljava/lang/String;

    .line 1337462
    iput-object p6, p0, LX/8NY;->d:Ljava/lang/String;

    .line 1337463
    iput-object p7, p0, LX/8NY;->e:Ljava/lang/String;

    .line 1337464
    iput-object p8, p0, LX/8NY;->f:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1337465
    iput-boolean p9, p0, LX/8NY;->g:Z

    .line 1337466
    iput-boolean p10, p0, LX/8NY;->h:Z

    .line 1337467
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, LX/8NY;->i:Z

    .line 1337468
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, LX/8NY;->j:J

    .line 1337469
    move-object/from16 v0, p13

    iput-object v0, p0, LX/8NY;->k:LX/8Oa;

    .line 1337470
    move/from16 v0, p14

    iput-boolean v0, p0, LX/8NY;->l:Z

    .line 1337471
    return-void
.end method
