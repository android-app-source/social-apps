.class public LX/7Rn;
.super LX/7Rm;
.source ""


# instance fields
.field private c:Landroid/view/Surface;

.field private d:Z


# direct methods
.method public constructor <init>(LX/7Rl;Landroid/view/Surface;Z)V
    .locals 0

    .prologue
    .line 1207852
    invoke-direct {p0, p1}, LX/7Rm;-><init>(LX/7Rl;)V

    .line 1207853
    invoke-virtual {p0, p2}, LX/7Rm;->a(Ljava/lang/Object;)V

    .line 1207854
    iput-object p2, p0, LX/7Rn;->c:Landroid/view/Surface;

    .line 1207855
    iput-boolean p3, p0, LX/7Rn;->d:Z

    .line 1207856
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 3

    .prologue
    .line 1207857
    iget-object v0, p0, LX/7Rm;->b:LX/7Rl;

    iget-object v1, p0, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    .line 1207858
    iget-object v2, v0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1207859
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    .line 1207860
    const/4 v0, -0x1

    iput v0, p0, LX/7Rm;->e:I

    iput v0, p0, LX/7Rm;->d:I

    .line 1207861
    iget-object v0, p0, LX/7Rn;->c:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 1207862
    iget-boolean v0, p0, LX/7Rn;->d:Z

    if-eqz v0, :cond_0

    .line 1207863
    iget-object v0, p0, LX/7Rn;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1207864
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Rn;->c:Landroid/view/Surface;

    .line 1207865
    :cond_1
    return-void
.end method
