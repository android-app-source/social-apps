.class public LX/7kj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/1Kt;

.field public final b:LX/7ki;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7kj",
            "<TT;>.",
            "ListDataSetObserver;"
        }
    .end annotation
.end field

.field public c:LX/0g8;

.field public d:Landroid/widget/BaseAdapter;

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/1Kt;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1232403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232404
    iput-object p1, p0, LX/7kj;->a:LX/1Kt;

    .line 1232405
    new-instance v0, LX/7ki;

    invoke-direct {v0, p0}, LX/7ki;-><init>(LX/7kj;)V

    iput-object v0, p0, LX/7kj;->b:LX/7ki;

    .line 1232406
    iput-object p2, p0, LX/7kj;->e:LX/0ad;

    .line 1232407
    return-void
.end method

.method public static a(LX/0QB;)LX/7kj;
    .locals 3

    .prologue
    .line 1232408
    new-instance v2, LX/7kj;

    invoke-static {p0}, LX/23N;->b(LX/0QB;)LX/23N;

    move-result-object v0

    check-cast v0, LX/1Kt;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/7kj;-><init>(LX/1Kt;LX/0ad;)V

    .line 1232409
    move-object v0, v2

    .line 1232410
    return-object v0
.end method
