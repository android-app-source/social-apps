.class public LX/8Tk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/8Ve;

.field private final c:LX/0tX;

.field private final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/8TD;

.field public final f:LX/8TY;

.field public final g:LX/8TS;

.field private final h:LX/8V7;

.field private final i:LX/8TS;

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Tn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Ve;LX/0tX;LX/1Ck;LX/8TD;LX/8TY;LX/8TS;LX/8V7;LX/8TS;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1348985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348986
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1348987
    iput-object v0, p0, LX/8Tk;->j:LX/0Ot;

    .line 1348988
    iput-object p1, p0, LX/8Tk;->a:Landroid/content/Context;

    .line 1348989
    iput-object p2, p0, LX/8Tk;->b:LX/8Ve;

    .line 1348990
    iput-object p3, p0, LX/8Tk;->c:LX/0tX;

    .line 1348991
    iput-object p4, p0, LX/8Tk;->d:LX/1Ck;

    .line 1348992
    iput-object p5, p0, LX/8Tk;->e:LX/8TD;

    .line 1348993
    iput-object p6, p0, LX/8Tk;->f:LX/8TY;

    .line 1348994
    iput-object p7, p0, LX/8Tk;->g:LX/8TS;

    .line 1348995
    iput-object p8, p0, LX/8Tk;->h:LX/8V7;

    .line 1348996
    iput-object p9, p0, LX/8Tk;->i:LX/8TS;

    .line 1348997
    return-void
.end method

.method public static b(LX/0QB;)LX/8Tk;
    .locals 10

    .prologue
    .line 1348998
    new-instance v0, LX/8Tk;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/8Ve;->b(LX/0QB;)LX/8Ve;

    move-result-object v2

    check-cast v2, LX/8Ve;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v5

    check-cast v5, LX/8TD;

    invoke-static {p0}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v6

    check-cast v6, LX/8TY;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v7

    check-cast v7, LX/8TS;

    invoke-static {p0}, LX/8V7;->b(LX/0QB;)LX/8V7;

    move-result-object v8

    check-cast v8, LX/8V7;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v9

    check-cast v9, LX/8TS;

    invoke-direct/range {v0 .. v9}, LX/8Tk;-><init>(Landroid/content/Context;LX/8Ve;LX/0tX;LX/1Ck;LX/8TD;LX/8TY;LX/8TS;LX/8V7;LX/8TS;)V

    .line 1348999
    const/16 v1, 0x307a

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 1349000
    iput-object v1, v0, LX/8Tk;->j:LX/0Ot;

    .line 1349001
    return-object v0
.end method


# virtual methods
.method public final a(ILX/8Tj;)V
    .locals 4

    .prologue
    .line 1349002
    iget-object v0, p0, LX/8Tk;->i:LX/8TS;

    .line 1349003
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1349004
    iget-object v1, v0, LX/8TO;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1349005
    new-instance v1, LX/8UC;

    invoke-direct {v1}, LX/8UC;-><init>()V

    move-object v1, v1

    .line 1349006
    const-string v2, "game_id"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1349007
    const-string v0, "friends_count"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1349008
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1349009
    iget-object v1, p0, LX/8Tk;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1349010
    iget-object v1, p0, LX/8Tk;->h:LX/8V7;

    invoke-virtual {v1}, LX/8V7;->a()LX/0zO;

    move-result-object v1

    .line 1349011
    iget-object v2, p0, LX/8Tk;->c:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1349012
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {v2}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1349013
    new-instance v1, LX/8Ti;

    invoke-direct {v1, p0, p2}, LX/8Ti;-><init>(LX/8Tk;LX/8Tj;)V

    .line 1349014
    iget-object v2, p0, LX/8Tk;->d:LX/1Ck;

    const-string v3, "fb_friends_leaderboard_query"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1349015
    return-void
.end method
