.class public LX/7hs;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/view/inputmethod/InputMethodManager;

.field public final c:Ljava/lang/String;

.field public final d:Landroid/content/pm/PackageManager;

.field public final e:Landroid/accounts/AccountManager;

.field public final f:Landroid/telephony/TelephonyManager;

.field private final g:Z

.field public h:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup",
            "<",
            "LX/7hm;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/7hm;

.field public j:Landroid/widget/TextView;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/view/View;

.field public m:Landroid/widget/Button;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/7hr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1226619
    const-class v0, LX/7hs;

    sput-object v0, LX/7hs;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/accounts/AccountManager;Landroid/telephony/TelephonyManager;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/auth/annotations/ShouldUseWorkLogin;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1226620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226621
    iput-object p1, p0, LX/7hs;->b:Landroid/view/inputmethod/InputMethodManager;

    .line 1226622
    iput-object p2, p0, LX/7hs;->c:Ljava/lang/String;

    .line 1226623
    iput-object p3, p0, LX/7hs;->d:Landroid/content/pm/PackageManager;

    .line 1226624
    iput-object p4, p0, LX/7hs;->e:Landroid/accounts/AccountManager;

    .line 1226625
    iput-object p5, p0, LX/7hs;->f:Landroid/telephony/TelephonyManager;

    .line 1226626
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/7hs;->g:Z

    .line 1226627
    return-void
.end method

.method public static c(LX/7hs;)V
    .locals 2

    .prologue
    .line 1226628
    iget-object v1, p0, LX/7hs;->l:Landroid/view/View;

    iget-object v0, p0, LX/7hs;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/7hs;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 1226629
    return-void

    .line 1226630
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/7hs;)V
    .locals 5

    .prologue
    .line 1226612
    iget-object v0, p0, LX/7hs;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1226613
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1226614
    :cond_0
    :goto_0
    return-void

    .line 1226615
    :cond_1
    iget-object v0, p0, LX/7hs;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1226616
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1226617
    iget-object v0, p0, LX/7hs;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v3, p0, LX/7hs;->h:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-virtual {v3}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1226618
    new-instance v3, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-boolean v0, p0, LX/7hs;->g:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/28K;->WORK_ACCOUNT_PASSWORD:LX/28K;

    :goto_1
    invoke-direct {v3, v1, v2, v0}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    new-instance v0, LX/4At;

    iget-object v1, p0, LX/7hs;->h:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-virtual {v1}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080155

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    goto :goto_0

    :cond_2
    sget-object v0, LX/28K;->UNSET:LX/28K;

    goto :goto_1
.end method
