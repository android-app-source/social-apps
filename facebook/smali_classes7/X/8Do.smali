.class public LX/8Do;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8Do;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1313215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313216
    iput-object p1, p0, LX/8Do;->a:LX/0ad;

    .line 1313217
    return-void
.end method

.method public static a(LX/0QB;)LX/8Do;
    .locals 4

    .prologue
    .line 1313202
    sget-object v0, LX/8Do;->b:LX/8Do;

    if-nez v0, :cond_1

    .line 1313203
    const-class v1, LX/8Do;

    monitor-enter v1

    .line 1313204
    :try_start_0
    sget-object v0, LX/8Do;->b:LX/8Do;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1313205
    if-eqz v2, :cond_0

    .line 1313206
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1313207
    new-instance p0, LX/8Do;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/8Do;-><init>(LX/0ad;)V

    .line 1313208
    move-object v0, p0

    .line 1313209
    sput-object v0, LX/8Do;->b:LX/8Do;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1313210
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1313211
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1313212
    :cond_1
    sget-object v0, LX/8Do;->b:LX/8Do;

    return-object v0

    .line 1313213
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1313214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 1313201
    iget-object v0, p0, LX/8Do;->a:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/8Dn;->p:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 1313198
    iget-object v0, p0, LX/8Do;->a:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/8Dn;->h:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 4

    .prologue
    .line 1313200
    iget-object v0, p0, LX/8Do;->a:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget v2, LX/8Dn;->l:I

    const/4 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;II)I

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 4

    .prologue
    .line 1313199
    iget-object v0, p0, LX/8Do;->a:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/8Dn;->q:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    return v0
.end method
