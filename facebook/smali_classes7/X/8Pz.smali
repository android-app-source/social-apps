.class public final LX/8Pz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Q1;


# direct methods
.method public constructor <init>(LX/8Q1;)V
    .locals 0

    .prologue
    .line 1342356
    iput-object p1, p0, LX/8Pz;->a:LX/8Q1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1342357
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1342358
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1342359
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoModel;

    .line 1342360
    if-nez v0, :cond_0

    .line 1342361
    :goto_0
    return-void

    .line 1342362
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoModel;->a()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;

    move-result-object v0

    .line 1342363
    iget-object v1, p0, LX/8Pz;->a:LX/8Q1;

    iget-object v1, v1, LX/8Q1;->d:LX/339;

    invoke-virtual {v1, v0}, LX/339;->b(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;)V

    goto :goto_0
.end method
