.class public LX/6iK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public b:LX/6ek;

.field public c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/messaging/model/folders/FolderCounts;

.field public h:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public i:Z

.field public j:J

.field public k:J

.field public l:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1129290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129291
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->b:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-object v0, v0

    .line 1129292
    iput-object v0, p0, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1129293
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1129294
    iput-object v0, p0, LX/6iK;->d:Ljava/util/List;

    .line 1129295
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1129296
    iput-object v0, p0, LX/6iK;->e:Ljava/util/List;

    .line 1129297
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1129298
    iput-object v0, p0, LX/6iK;->f:Ljava/util/List;

    .line 1129299
    sget-object v0, Lcom/facebook/messaging/model/folders/FolderCounts;->a:Lcom/facebook/messaging/model/folders/FolderCounts;

    iput-object v0, p0, LX/6iK;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 1129300
    iput-wide v2, p0, LX/6iK;->j:J

    .line 1129301
    iput-wide v2, p0, LX/6iK;->k:J

    .line 1129302
    iput-wide v2, p0, LX/6iK;->l:J

    .line 1129303
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)LX/6iK;
    .locals 2

    .prologue
    .line 1129277
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iput-object v0, p0, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129278
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    iput-object v0, p0, LX/6iK;->b:LX/6ek;

    .line 1129279
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iput-object v0, p0, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1129280
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    iput-object v0, p0, LX/6iK;->d:Ljava/util/List;

    .line 1129281
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->e:LX/0Px;

    iput-object v0, p0, LX/6iK;->e:Ljava/util/List;

    .line 1129282
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->f:LX/0Px;

    iput-object v0, p0, LX/6iK;->f:Ljava/util/List;

    .line 1129283
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    iput-object v0, p0, LX/6iK;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 1129284
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    iput-object v0, p0, LX/6iK;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1129285
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->i:Z

    iput-boolean v0, p0, LX/6iK;->i:Z

    .line 1129286
    iget-wide v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    iput-wide v0, p0, LX/6iK;->j:J

    .line 1129287
    iget-wide v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->j:J

    iput-wide v0, p0, LX/6iK;->k:J

    .line 1129288
    iget-wide v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    iput-wide v0, p0, LX/6iK;->l:J

    .line 1129289
    return-object p0
.end method

.method public final m()Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 1

    .prologue
    .line 1129276
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/service/model/FetchThreadListResult;-><init>(LX/6iK;)V

    return-object v0
.end method
