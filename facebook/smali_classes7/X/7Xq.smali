.class public final LX/7Xq;
.super LX/639;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/7Xr;


# direct methods
.method public constructor <init>(LX/7Xr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1218750
    iput-object p1, p0, LX/7Xq;->b:LX/7Xr;

    iput-object p2, p0, LX/7Xq;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/639;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 5

    .prologue
    .line 1218751
    iget-object v0, p0, LX/7Xq;->b:LX/7Xr;

    iget-object v0, v0, LX/7Xr;->a:LX/11x;

    if-eqz v0, :cond_0

    .line 1218752
    iget-object v0, p0, LX/7Xq;->b:LX/7Xr;

    iget-object v0, v0, LX/7Xr;->a:LX/11x;

    iget-object v1, p0, LX/7Xq;->a:Ljava/lang/String;

    .line 1218753
    iget-object v2, v0, LX/11x;->a:LX/11v;

    .line 1218754
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1218755
    iget-object v4, v2, LX/11v;->k:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "click"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v0, v2, LX/11v;->m:Ljava/lang/String;

    .line 1218756
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1218757
    move-object p0, p0

    .line 1218758
    const-string v0, "zero_indicator"

    .line 1218759
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1218760
    move-object p0, p0

    .line 1218761
    const-string v0, "zero_indicator_goto"

    .line 1218762
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1218763
    move-object p0, p0

    .line 1218764
    invoke-interface {v4, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1218765
    :try_start_0
    iget-object v4, v2, LX/11v;->g:Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v2, LX/11v;->b:Landroid/content/Context;

    invoke-interface {v4, v3, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1218766
    :cond_0
    :goto_0
    return-void

    .line 1218767
    :catch_0
    sget-object v3, LX/11v;->a:Ljava/lang/Class;

    const-string v4, "Activity not found for opening url: [%s]"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v1, p0, v0

    invoke-static {v3, v4, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
