.class public final LX/7Xe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1218573
    iput-object p1, p0, LX/7Xe;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    iput-object p2, p0, LX/7Xe;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 1218574
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1218575
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;

    .line 1218576
    iget-object v1, p0, LX/7Xe;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    iget-object v1, v1, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->t:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->f()I

    move-result v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1218577
    const-string v1, "enabled"

    invoke-virtual {v0}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1218578
    iget-object v1, p0, LX/7Xe;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    invoke-virtual {v0}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->f()I

    move-result v2

    const/4 v3, 0x1

    iget-object v4, p0, LX/7Xe;->a:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a$redex0(Lcom/facebook/zero/service/ZeroHeaderRequestManager;IILjava/lang/String;)V

    .line 1218579
    iget-object v1, p0, LX/7Xe;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    invoke-static {v1, v0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->b(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1218580
    :goto_0
    return-object v0

    .line 1218581
    :cond_0
    iget-object v1, p0, LX/7Xe;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    invoke-virtual {v0}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->f()I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, LX/7Xe;->a:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a$redex0(Lcom/facebook/zero/service/ZeroHeaderRequestManager;IILjava/lang/String;)V

    .line 1218582
    sget-object v1, LX/1nY;->OTHER:LX/1nY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Zero header request not sent because status is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
