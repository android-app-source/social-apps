.class public final enum LX/7zf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7zf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7zf;

.field public static final enum NOT_VISIBLE:LX/7zf;

.field public static final enum OFFSCREEN:LX/7zf;

.field public static final enum VISIBLE:LX/7zf;

.field public static final enum VISIBLE_FOR_AUTOPLAY:LX/7zf;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1281290
    new-instance v0, LX/7zf;

    const-string v1, "VISIBLE_FOR_AUTOPLAY"

    invoke-direct {v0, v1, v2}, LX/7zf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zf;->VISIBLE_FOR_AUTOPLAY:LX/7zf;

    .line 1281291
    new-instance v0, LX/7zf;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v3}, LX/7zf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zf;->VISIBLE:LX/7zf;

    .line 1281292
    new-instance v0, LX/7zf;

    const-string v1, "NOT_VISIBLE"

    invoke-direct {v0, v1, v4}, LX/7zf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zf;->NOT_VISIBLE:LX/7zf;

    .line 1281293
    new-instance v0, LX/7zf;

    const-string v1, "OFFSCREEN"

    invoke-direct {v0, v1, v5}, LX/7zf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zf;->OFFSCREEN:LX/7zf;

    .line 1281294
    const/4 v0, 0x4

    new-array v0, v0, [LX/7zf;

    sget-object v1, LX/7zf;->VISIBLE_FOR_AUTOPLAY:LX/7zf;

    aput-object v1, v0, v2

    sget-object v1, LX/7zf;->VISIBLE:LX/7zf;

    aput-object v1, v0, v3

    sget-object v1, LX/7zf;->NOT_VISIBLE:LX/7zf;

    aput-object v1, v0, v4

    sget-object v1, LX/7zf;->OFFSCREEN:LX/7zf;

    aput-object v1, v0, v5

    sput-object v0, LX/7zf;->$VALUES:[LX/7zf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1281295
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7zf;
    .locals 1

    .prologue
    .line 1281296
    const-class v0, LX/7zf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7zf;

    return-object v0
.end method

.method public static values()[LX/7zf;
    .locals 1

    .prologue
    .line 1281297
    sget-object v0, LX/7zf;->$VALUES:[LX/7zf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7zf;

    return-object v0
.end method
