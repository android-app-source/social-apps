.class public final enum LX/6ws;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ws;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ws;

.field public static final enum MODAL_BOTTOM:LX/6ws;

.field public static final enum SLIDE_RIGHT:LX/6ws;


# instance fields
.field private final mTitleBarNavIconStyle:LX/73h;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1158097
    new-instance v0, LX/6ws;

    const-string v1, "MODAL_BOTTOM"

    sget-object v2, LX/73h;->CROSS:LX/73h;

    invoke-direct {v0, v1, v3, v2}, LX/6ws;-><init>(Ljava/lang/String;ILX/73h;)V

    sput-object v0, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1158098
    new-instance v0, LX/6ws;

    const-string v1, "SLIDE_RIGHT"

    sget-object v2, LX/73h;->BACK_ARROW:LX/73h;

    invoke-direct {v0, v1, v4, v2}, LX/6ws;-><init>(Ljava/lang/String;ILX/73h;)V

    sput-object v0, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    .line 1158099
    const/4 v0, 0x2

    new-array v0, v0, [LX/6ws;

    sget-object v1, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    aput-object v1, v0, v3

    sget-object v1, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    aput-object v1, v0, v4

    sput-object v0, LX/6ws;->$VALUES:[LX/6ws;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/73h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/73h;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1158094
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1158095
    iput-object p3, p0, LX/6ws;->mTitleBarNavIconStyle:LX/73h;

    .line 1158096
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ws;
    .locals 1

    .prologue
    .line 1158091
    const-class v0, LX/6ws;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ws;

    return-object v0
.end method

.method public static values()[LX/6ws;
    .locals 1

    .prologue
    .line 1158093
    sget-object v0, LX/6ws;->$VALUES:[LX/6ws;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ws;

    return-object v0
.end method


# virtual methods
.method public final getTitleBarNavIconStyle()LX/73h;
    .locals 1

    .prologue
    .line 1158092
    iget-object v0, p0, LX/6ws;->mTitleBarNavIconStyle:LX/73h;

    return-object v0
.end method
