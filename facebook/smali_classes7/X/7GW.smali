.class public LX/7GW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7GW;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BI)LX/1su;
    .locals 4

    .prologue
    .line 1189246
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 1189247
    new-instance v1, LX/1sr;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    array-length v3, p0

    sub-int/2addr v3, p1

    invoke-direct {v2, p0, p1, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v1, v2}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/7GW;
    .locals 3

    .prologue
    .line 1189248
    sget-object v0, LX/7GW;->a:LX/7GW;

    if-nez v0, :cond_1

    .line 1189249
    const-class v1, LX/7GW;

    monitor-enter v1

    .line 1189250
    :try_start_0
    sget-object v0, LX/7GW;->a:LX/7GW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1189251
    if-eqz v2, :cond_0

    .line 1189252
    :try_start_1
    new-instance v0, LX/7GW;

    invoke-direct {v0}, LX/7GW;-><init>()V

    .line 1189253
    move-object v0, v0

    .line 1189254
    sput-object v0, LX/7GW;->a:LX/7GW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189255
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1189256
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189257
    :cond_1
    sget-object v0, LX/7GW;->a:LX/7GW;

    return-object v0

    .line 1189258
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1189259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
