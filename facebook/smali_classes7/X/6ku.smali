.class public LX/6ku;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final deltas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6kU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1141767
    new-instance v0, LX/1sv;

    const-string v1, "Payload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6ku;->b:LX/1sv;

    .line 1141768
    new-instance v0, LX/1sw;

    const-string v1, "deltas"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ku;->c:LX/1sw;

    .line 1141769
    sput-boolean v3, LX/6ku;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6kU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1141770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1141771
    iput-object p1, p0, LX/6ku;->deltas:Ljava/util/List;

    .line 1141772
    return-void
.end method

.method public static a(LX/6ku;)V
    .locals 4

    .prologue
    .line 1141773
    iget-object v0, p0, LX/6ku;->deltas:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1141774
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'deltas\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6ku;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1141775
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1141776
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1141777
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1141778
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1141779
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Payload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1141780
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141781
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141782
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141783
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141784
    const-string v4, "deltas"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141785
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141786
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141787
    iget-object v0, p0, LX/6ku;->deltas:Ljava/util/List;

    if-nez v0, :cond_3

    .line 1141788
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141789
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141790
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141791
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1141792
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1141793
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1141794
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 1141795
    :cond_3
    iget-object v0, p0, LX/6ku;->deltas:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1141796
    invoke-static {p0}, LX/6ku;->a(LX/6ku;)V

    .line 1141797
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1141798
    iget-object v0, p0, LX/6ku;->deltas:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1141799
    sget-object v0, LX/6ku;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141800
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1141801
    iget-object v0, p0, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kU;

    .line 1141802
    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    goto :goto_0

    .line 1141803
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1141804
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1141805
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1141806
    if-nez p1, :cond_1

    .line 1141807
    :cond_0
    :goto_0
    return v0

    .line 1141808
    :cond_1
    instance-of v1, p1, LX/6ku;

    if-eqz v1, :cond_0

    .line 1141809
    check-cast p1, LX/6ku;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1141810
    if-nez p1, :cond_3

    .line 1141811
    :cond_2
    :goto_1
    move v0, v2

    .line 1141812
    goto :goto_0

    .line 1141813
    :cond_3
    iget-object v0, p0, LX/6ku;->deltas:Ljava/util/List;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1141814
    :goto_2
    iget-object v3, p1, LX/6ku;->deltas:Ljava/util/List;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1141815
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1141816
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141817
    iget-object v0, p0, LX/6ku;->deltas:Ljava/util/List;

    iget-object v3, p1, LX/6ku;->deltas:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1141818
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1141819
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1141820
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1141821
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1141822
    sget-boolean v0, LX/6ku;->a:Z

    .line 1141823
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6ku;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1141824
    return-object v0
.end method
