.class public final synthetic LX/77Q;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1171815
    invoke-static {}, LX/77S;->values()[LX/77S;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/77Q;->b:[I

    :try_start_0
    sget-object v0, LX/77Q;->b:[I

    sget-object v1, LX/77S;->SECONDS_SINCE_GREATER_THAN:LX/77S;

    invoke-virtual {v1}, LX/77S;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, LX/77Q;->b:[I

    sget-object v1, LX/77S;->SECONDS_SINCE_LESS_THAN:LX/77S;

    invoke-virtual {v1}, LX/77S;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, LX/77Q;->b:[I

    sget-object v1, LX/77S;->COUNT_AT_LEAST:LX/77S;

    invoke-virtual {v1}, LX/77S;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, LX/77Q;->b:[I

    sget-object v1, LX/77S;->COUNT_AT_MOST:LX/77S;

    invoke-virtual {v1}, LX/77S;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    .line 1171816
    :goto_3
    invoke-static {}, LX/77R;->values()[LX/77R;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/77Q;->a:[I

    :try_start_4
    sget-object v0, LX/77Q;->a:[I

    sget-object v1, LX/77R;->IMPRESSION:LX/77R;

    invoke-virtual {v1}, LX/77R;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, LX/77Q;->a:[I

    sget-object v1, LX/77R;->PRIMARY_ACTION:LX/77R;

    invoke-virtual {v1}, LX/77R;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v0, LX/77Q;->a:[I

    sget-object v1, LX/77R;->SECONDARY_ACTION:LX/77R;

    invoke-virtual {v1}, LX/77R;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, LX/77Q;->a:[I

    sget-object v1, LX/77R;->DISMISS_ACTION:LX/77R;

    invoke-virtual {v1}, LX/77R;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    goto :goto_7

    :catch_1
    goto :goto_6

    :catch_2
    goto :goto_5

    :catch_3
    goto :goto_4

    :catch_4
    goto :goto_3

    :catch_5
    goto :goto_2

    :catch_6
    goto :goto_1

    :catch_7
    goto :goto_0
.end method
