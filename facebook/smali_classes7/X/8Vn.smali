.class public final LX/8Vn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yp;


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:LX/8Ut;

.field public final synthetic c:LX/8Vo;


# direct methods
.method public constructor <init>(LX/8Vo;Ljava/util/Map;LX/8Ut;)V
    .locals 0

    .prologue
    .line 1353358
    iput-object p1, p0, LX/8Vn;->c:LX/8Vo;

    iput-object p2, p0, LX/8Vn;->a:Ljava/util/Map;

    iput-object p3, p0, LX/8Vn;->b:LX/8Ut;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1353359
    return-void
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1353360
    return-void
.end method

.method public final a(LX/7zB;)V
    .locals 4

    .prologue
    .line 1353361
    iget-object v0, p0, LX/8Vn;->c:LX/8Vo;

    iget-object v0, v0, LX/8Vo;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->SCREENSHOT_UPLOAD:LX/8TE;

    const/4 v2, 0x0

    iget-object v3, p0, LX/8Vn;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, p1, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1353362
    iget-object v0, p0, LX/8Vn;->b:LX/8Ut;

    invoke-virtual {v0}, LX/8Ut;->a()V

    .line 1353363
    return-void
.end method

.method public final a(LX/7zL;)V
    .locals 8

    .prologue
    .line 1353364
    iget-object v0, p0, LX/8Vn;->c:LX/8Vo;

    iget-object v0, v0, LX/8Vo;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->SCREENSHOT_UPLOAD:LX/8TE;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, LX/8Vn;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1353365
    iget-object v0, p0, LX/8Vn;->b:LX/8Ut;

    iget-object v1, p0, LX/8Vn;->c:LX/8Vo;

    .line 1353366
    iget-object v2, p1, LX/7zL;->a:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1353367
    iget-object v2, p1, LX/7zL;->a:Ljava/lang/String;

    .line 1353368
    :goto_0
    move-object v1, v2

    .line 1353369
    iget-object v2, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v2, v2, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->g:LX/8Uw;

    iget-object v3, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v3, v3, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->b:Ljava/lang/String;

    iget-object v4, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget v4, v4, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->c:I

    iget-object v5, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v6, v5, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->d:Ljava/util/List;

    iget-object v5, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v7, v5, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->e:LX/8Uv;

    iget-object v5, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object p0, v5, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->f:Ljava/util/Map;

    move-object v5, v1

    .line 1353370
    sget-object p1, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_FBID:LX/8TE;

    iget-object p1, p1, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {p0, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353371
    new-instance p1, LX/4GY;

    invoke-direct {p1}, LX/4GY;-><init>()V

    .line 1353372
    const-string v0, "thread_id"

    invoke-virtual {p1, v0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1353373
    move-object p1, p1

    .line 1353374
    const-string v0, "game_id"

    invoke-virtual {p1, v0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353375
    move-object p1, p1

    .line 1353376
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1353377
    const-string v1, "score"

    invoke-virtual {p1, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1353378
    move-object p1, p1

    .line 1353379
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1353380
    const-string v0, "screenshot"

    invoke-virtual {p1, v0, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353381
    :cond_0
    new-instance v0, LX/8Ue;

    invoke-direct {v0}, LX/8Ue;-><init>()V

    move-object v0, v0

    .line 1353382
    const-string v1, "input"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1353383
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p1

    .line 1353384
    iget-object v0, v2, LX/8Uw;->a:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    .line 1353385
    new-instance v0, LX/8Uu;

    invoke-direct {v0, v2, p0, v7}, LX/8Uu;-><init>(LX/8Uw;Ljava/util/Map;LX/8Uv;)V

    iget-object v1, v2, LX/8Uw;->d:Ljava/util/concurrent/Executor;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1353386
    return-void

    .line 1353387
    :cond_1
    iget-object v2, p1, LX/7zL;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1353388
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p1, LX/7zL;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1353389
    const-string v3, "media_id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1353390
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 1353391
    iget-object v2, v1, LX/8Vo;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8TD;

    sget-object v4, LX/8TE;->SCREENSHOT_UPLOAD:LX/8TE;

    const-string p0, "Unable to parse to response to JSON object"

    invoke-virtual {v2, v4, p0, v3}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1353392
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1353393
    iget-object v0, p0, LX/8Vn;->a:Ljava/util/Map;

    sget-object v1, LX/8TE;->EXTRAS_FAIL_REASON:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    const-string v2, "Upload cancellation"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353394
    iget-object v0, p0, LX/8Vn;->c:LX/8Vo;

    iget-object v0, v0, LX/8Vo;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->SCREENSHOT_UPLOAD:LX/8TE;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, LX/8Vn;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1353395
    iget-object v0, p0, LX/8Vn;->b:LX/8Ut;

    .line 1353396
    iget-object v1, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v1, v1, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->f:Ljava/util/Map;

    sget-object v2, LX/8TE;->EXTRAS_FAIL_REASON:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    const-string v3, "screenshot upload cancelled"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353397
    iget-object v1, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v1, v1, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->g:LX/8Uw;

    iget-object v1, v1, LX/8Uw;->c:LX/8TD;

    sget-object v2, LX/8TE;->MESSENGER_GAME_SCORE_SHARE:LX/8TE;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object p0, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object p0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->f:Ljava/util/Map;

    invoke-virtual {v1, v2, v3, v4, p0}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1353398
    iget-object v1, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v1, v1, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->e:LX/8Uv;

    if-eqz v1, :cond_0

    .line 1353399
    iget-object v1, v0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v1, v1, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->e:LX/8Uv;

    invoke-interface {v1}, LX/8Uv;->b()V

    .line 1353400
    :cond_0
    return-void
.end method
