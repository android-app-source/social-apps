.class public final LX/8aW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 1367731
    const/16 v27, 0x0

    .line 1367732
    const/16 v26, 0x0

    .line 1367733
    const/16 v25, 0x0

    .line 1367734
    const/16 v24, 0x0

    .line 1367735
    const/16 v23, 0x0

    .line 1367736
    const/16 v22, 0x0

    .line 1367737
    const/16 v21, 0x0

    .line 1367738
    const/16 v20, 0x0

    .line 1367739
    const/16 v19, 0x0

    .line 1367740
    const/16 v18, 0x0

    .line 1367741
    const/16 v17, 0x0

    .line 1367742
    const/16 v16, 0x0

    .line 1367743
    const/4 v15, 0x0

    .line 1367744
    const/4 v14, 0x0

    .line 1367745
    const/4 v13, 0x0

    .line 1367746
    const/4 v12, 0x0

    .line 1367747
    const/4 v11, 0x0

    .line 1367748
    const/4 v10, 0x0

    .line 1367749
    const/4 v9, 0x0

    .line 1367750
    const/4 v8, 0x0

    .line 1367751
    const/4 v7, 0x0

    .line 1367752
    const/4 v6, 0x0

    .line 1367753
    const/4 v5, 0x0

    .line 1367754
    const/4 v4, 0x0

    .line 1367755
    const/4 v3, 0x0

    .line 1367756
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    .line 1367757
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1367758
    const/4 v3, 0x0

    .line 1367759
    :goto_0
    return v3

    .line 1367760
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1367761
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1a

    .line 1367762
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v28

    .line 1367763
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1367764
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    if-eqz v28, :cond_1

    .line 1367765
    const-string v29, "block_quote_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_2

    .line 1367766
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v27

    goto :goto_1

    .line 1367767
    :cond_2
    const-string v29, "body_text_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_3

    .line 1367768
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 1367769
    :cond_3
    const-string v29, "byline"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_4

    .line 1367770
    invoke-static/range {p0 .. p1}, LX/8Zq;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 1367771
    :cond_4
    const-string v29, "byline_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    .line 1367772
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 1367773
    :cond_5
    const-string v29, "caption_credit_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 1367774
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 1367775
    :cond_6
    const-string v29, "caption_description_extra_large_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    .line 1367776
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 1367777
    :cond_7
    const-string v29, "caption_description_large_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_8

    .line 1367778
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 1367779
    :cond_8
    const-string v29, "caption_description_medium_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    .line 1367780
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1367781
    :cond_9
    const-string v29, "caption_description_small_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_a

    .line 1367782
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1367783
    :cond_a
    const-string v29, "caption_title_extra_large_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    .line 1367784
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1367785
    :cond_b
    const-string v29, "caption_title_large_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    .line 1367786
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1367787
    :cond_c
    const-string v29, "caption_title_medium_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_d

    .line 1367788
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1367789
    :cond_d
    const-string v29, "caption_title_small_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_e

    .line 1367790
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1367791
    :cond_e
    const-string v29, "custom_fonts"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_f

    .line 1367792
    invoke-static/range {p0 .. p1}, LX/8a1;->b(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1367793
    :cond_f
    const-string v29, "end_credits_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    .line 1367794
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1367795
    :cond_10
    const-string v29, "header_one_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_11

    .line 1367796
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1367797
    :cond_11
    const-string v29, "header_two_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_12

    .line 1367798
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1367799
    :cond_12
    const-string v29, "kicker_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_13

    .line 1367800
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1367801
    :cond_13
    const-string v29, "link_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_14

    .line 1367802
    invoke-static/range {p0 .. p1}, LX/8a7;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1367803
    :cond_14
    const-string v29, "logo"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_15

    .line 1367804
    invoke-static/range {p0 .. p1}, LX/8aC;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1367805
    :cond_15
    const-string v29, "pull_quote_attribution_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_16

    .line 1367806
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1367807
    :cond_16
    const-string v29, "pull_quote_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_17

    .line 1367808
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1367809
    :cond_17
    const-string v29, "related_articles_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_18

    .line 1367810
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1367811
    :cond_18
    const-string v29, "subtitle_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_19

    .line 1367812
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1367813
    :cond_19
    const-string v29, "title_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    .line 1367814
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1367815
    :cond_1a
    const/16 v28, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1367816
    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367817
    const/16 v27, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367818
    const/16 v26, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367819
    const/16 v25, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367820
    const/16 v24, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367821
    const/16 v23, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367822
    const/16 v22, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367823
    const/16 v21, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367824
    const/16 v20, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367825
    const/16 v19, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367826
    const/16 v18, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367827
    const/16 v17, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1367828
    const/16 v16, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1367829
    const/16 v15, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1367830
    const/16 v14, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1367831
    const/16 v13, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1367832
    const/16 v12, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1367833
    const/16 v11, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1367834
    const/16 v10, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1367835
    const/16 v9, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1367836
    const/16 v8, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1367837
    const/16 v7, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1367838
    const/16 v6, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1367839
    const/16 v5, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1367840
    const/16 v4, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1367841
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1367842
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1367843
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367844
    if-eqz v0, :cond_0

    .line 1367845
    const-string v1, "block_quote_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367846
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367847
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367848
    if-eqz v0, :cond_1

    .line 1367849
    const-string v1, "body_text_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367850
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367851
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367852
    if-eqz v0, :cond_2

    .line 1367853
    const-string v1, "byline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367854
    invoke-static {p0, v0, p2}, LX/8Zq;->a(LX/15i;ILX/0nX;)V

    .line 1367855
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367856
    if-eqz v0, :cond_3

    .line 1367857
    const-string v1, "byline_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367858
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367859
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367860
    if-eqz v0, :cond_4

    .line 1367861
    const-string v1, "caption_credit_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367862
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367863
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367864
    if-eqz v0, :cond_5

    .line 1367865
    const-string v1, "caption_description_extra_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367866
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367867
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367868
    if-eqz v0, :cond_6

    .line 1367869
    const-string v1, "caption_description_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367870
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367871
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367872
    if-eqz v0, :cond_7

    .line 1367873
    const-string v1, "caption_description_medium_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367874
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367875
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367876
    if-eqz v0, :cond_8

    .line 1367877
    const-string v1, "caption_description_small_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367878
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367879
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367880
    if-eqz v0, :cond_9

    .line 1367881
    const-string v1, "caption_title_extra_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367882
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367883
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367884
    if-eqz v0, :cond_a

    .line 1367885
    const-string v1, "caption_title_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367886
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367887
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367888
    if-eqz v0, :cond_b

    .line 1367889
    const-string v1, "caption_title_medium_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367890
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367891
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367892
    if-eqz v0, :cond_c

    .line 1367893
    const-string v1, "caption_title_small_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367894
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367895
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367896
    if-eqz v0, :cond_d

    .line 1367897
    const-string v1, "custom_fonts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367898
    invoke-static {p0, v0, p2, p3}, LX/8a1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367899
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367900
    if-eqz v0, :cond_e

    .line 1367901
    const-string v1, "end_credits_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367902
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367903
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367904
    if-eqz v0, :cond_f

    .line 1367905
    const-string v1, "header_one_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367906
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367907
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367908
    if-eqz v0, :cond_10

    .line 1367909
    const-string v1, "header_two_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367910
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367911
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367912
    if-eqz v0, :cond_11

    .line 1367913
    const-string v1, "kicker_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367914
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367915
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367916
    if-eqz v0, :cond_12

    .line 1367917
    const-string v1, "link_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367918
    invoke-static {p0, v0, p2}, LX/8a7;->a(LX/15i;ILX/0nX;)V

    .line 1367919
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367920
    if-eqz v0, :cond_13

    .line 1367921
    const-string v1, "logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367922
    invoke-static {p0, v0, p2}, LX/8aC;->a(LX/15i;ILX/0nX;)V

    .line 1367923
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367924
    if-eqz v0, :cond_14

    .line 1367925
    const-string v1, "pull_quote_attribution_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367926
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367927
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367928
    if-eqz v0, :cond_15

    .line 1367929
    const-string v1, "pull_quote_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367930
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367931
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367932
    if-eqz v0, :cond_16

    .line 1367933
    const-string v1, "related_articles_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367934
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367935
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367936
    if-eqz v0, :cond_17

    .line 1367937
    const-string v1, "subtitle_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367938
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367939
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1367940
    if-eqz v0, :cond_18

    .line 1367941
    const-string v1, "title_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1367942
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1367943
    :cond_18
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1367944
    return-void
.end method
