.class public abstract enum LX/6pJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6pJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6pJ;

.field public static final enum ENTER_PASSWORD:LX/6pJ;

.field public static final enum ENTER_PIN:LX/6pJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1149238
    new-instance v0, LX/6pK;

    const-string v1, "ENTER_PIN"

    invoke-direct {v0, v1, v2}, LX/6pK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149239
    new-instance v0, LX/6pL;

    const-string v1, "ENTER_PASSWORD"

    invoke-direct {v0, v1, v3}, LX/6pL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pJ;->ENTER_PASSWORD:LX/6pJ;

    .line 1149240
    const/4 v0, 0x2

    new-array v0, v0, [LX/6pJ;

    sget-object v1, LX/6pJ;->ENTER_PIN:LX/6pJ;

    aput-object v1, v0, v2

    sget-object v1, LX/6pJ;->ENTER_PASSWORD:LX/6pJ;

    aput-object v1, v0, v3

    sput-object v0, LX/6pJ;->$VALUES:[LX/6pJ;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1149241
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getHeaderText(Ljava/lang/String;Landroid/content/res/Resources;LX/6pM;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1149242
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p2}, LX/6pM;->getHeaderTextResId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6pJ;
    .locals 1

    .prologue
    .line 1149243
    const-class v0, LX/6pJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6pJ;

    return-object v0
.end method

.method public static values()[LX/6pJ;
    .locals 1

    .prologue
    .line 1149244
    sget-object v0, LX/6pJ;->$VALUES:[LX/6pJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6pJ;

    return-object v0
.end method


# virtual methods
.method public abstract getFragment(LX/6pM;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;Landroid/content/res/Resources;I)Landroid/support/v4/app/Fragment;
.end method
