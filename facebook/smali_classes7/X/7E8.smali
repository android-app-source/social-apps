.class public final LX/7E8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/video/GlVideoRenderThread;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/video/GlVideoRenderThread;)V
    .locals 0

    .prologue
    .line 1184307
    iput-object p1, p0, LX/7E8;->a:Lcom/facebook/spherical/video/GlVideoRenderThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 1184308
    iget-object v0, p0, LX/7E8;->a:Lcom/facebook/spherical/video/GlVideoRenderThread;

    iget-boolean v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7E8;->a:Lcom/facebook/spherical/video/GlVideoRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 1184309
    :cond_0
    iget-object v0, p0, LX/7E8;->a:Lcom/facebook/spherical/video/GlVideoRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1184310
    :goto_0
    return-void

    .line 1184311
    :cond_1
    iget-object v0, p0, LX/7E8;->a:Lcom/facebook/spherical/video/GlVideoRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
