.class public LX/6uX;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;",
        "LX/6uH;",
        ">;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;)V
    .locals 0

    .prologue
    .line 1155961
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1155962
    return-void
.end method

.method private a(LX/6uH;)V
    .locals 2

    .prologue
    .line 1155957
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;

    .line 1155958
    iget-object v1, p0, LX/6uX;->l:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1155959
    invoke-virtual {v0, p1}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a(LX/6uH;)V

    .line 1155960
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/6E2;)V
    .locals 0

    .prologue
    .line 1155954
    check-cast p1, LX/6uH;

    invoke-direct {p0, p1}, LX/6uX;->a(LX/6uH;)V

    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1155955
    iput-object p1, p0, LX/6uX;->l:LX/6qh;

    .line 1155956
    return-void
.end method
