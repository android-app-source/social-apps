.class public LX/71T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# instance fields
.field private final a:LX/712;


# direct methods
.method public constructor <init>(LX/712;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163365
    iput-object p1, p0, LX/71T;->a:LX/712;

    .line 1163366
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1163367
    sget-object v0, LX/71S;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1163368
    iget-object v0, p0, LX/71T;->a:LX/712;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/712;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1163369
    :pswitch_0
    check-cast p2, LX/71Q;

    .line 1163370
    if-nez p3, :cond_0

    new-instance p3, LX/71R;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/71R;-><init>(Landroid/content/Context;)V

    .line 1163371
    :goto_1
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1163372
    iput-object p2, p3, LX/71R;->d:LX/71Q;

    .line 1163373
    iget-object p1, p3, LX/71R;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v0, p3, LX/71R;->d:LX/71Q;

    iget-boolean v0, v0, LX/71Q;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1163374
    iget-object v0, p3, LX/71R;->b:Landroid/widget/TextView;

    iget-object p1, p3, LX/71R;->d:LX/71Q;

    iget-object p1, p1, LX/71Q;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163375
    move-object v0, p3

    .line 1163376
    goto :goto_0

    .line 1163377
    :cond_0
    check-cast p3, LX/71R;

    goto :goto_1

    .line 1163378
    :cond_1
    const/16 v0, 0x8

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
