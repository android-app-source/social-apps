.class public final LX/842;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;",
        ">;",
        "LX/2lp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2dk;


# direct methods
.method public constructor <init>(LX/2dk;)V
    .locals 0

    .prologue
    .line 1290753
    iput-object p1, p0, LX/842;->a:LX/2dk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290754
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1290755
    if-eqz p1, :cond_0

    .line 1290756
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290757
    if-nez v0, :cond_1

    .line 1290758
    :cond_0
    const/4 v0, 0x0

    .line 1290759
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/2lp;

    .line 1290760
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290761
    check-cast v0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->a()Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2dk;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 1290762
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290763
    check-cast v0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->a()Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/2dk;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/2lp;-><init>(LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    move-object v0, v1

    goto :goto_0
.end method
