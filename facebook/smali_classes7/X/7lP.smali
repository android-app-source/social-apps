.class public final LX/7lP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

.field public e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

.field public f:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1234872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234873
    iput-boolean v1, p0, LX/7lP;->a:Z

    .line 1234874
    iput-boolean v1, p0, LX/7lP;->b:Z

    .line 1234875
    iput-boolean v1, p0, LX/7lP;->c:Z

    .line 1234876
    sget-object v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->a:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    iput-object v0, p0, LX/7lP;->d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1234877
    iput-object v2, p0, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1234878
    iput-object v2, p0, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1234879
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1234880
    iput-object v0, p0, LX/7lP;->g:LX/0Px;

    .line 1234881
    iput-boolean v1, p0, LX/7lP;->h:Z

    .line 1234882
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1234854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234855
    iput-boolean v1, p0, LX/7lP;->a:Z

    .line 1234856
    iput-boolean v1, p0, LX/7lP;->b:Z

    .line 1234857
    iput-boolean v1, p0, LX/7lP;->c:Z

    .line 1234858
    sget-object v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->a:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    iput-object v0, p0, LX/7lP;->d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1234859
    iput-object v2, p0, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1234860
    iput-object v2, p0, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1234861
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1234862
    iput-object v0, p0, LX/7lP;->g:LX/0Px;

    .line 1234863
    iput-boolean v1, p0, LX/7lP;->h:Z

    .line 1234864
    iget-boolean v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    iput-boolean v0, p0, LX/7lP;->a:Z

    .line 1234865
    iget-boolean v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    iput-boolean v0, p0, LX/7lP;->b:Z

    .line 1234866
    iget-boolean v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->e:Z

    iput-boolean v0, p0, LX/7lP;->c:Z

    .line 1234867
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iput-object v0, p0, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1234868
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1234869
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->g:LX/0Px;

    iput-object v0, p0, LX/7lP;->g:LX/0Px;

    .line 1234870
    iget-boolean v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->h:Z

    iput-boolean v0, p0, LX/7lP;->h:Z

    .line 1234871
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;
    .locals 2

    .prologue
    .line 1234850
    iget-object v0, p0, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "If we are setting fixed privacy data, we cannot have selectable privacy data."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1234851
    iput-object p1, p0, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1234852
    return-object p0

    .line 1234853
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;
    .locals 3

    .prologue
    .line 1234832
    iget-object v0, p0, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "If we are setting selectable privacy data, we cannot have fixed privacy data."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1234833
    iput-object p1, p0, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1234834
    iget-object v0, p0, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1234835
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1234836
    if-eqz v0, :cond_0

    .line 1234837
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v0

    .line 1234838
    if-nez v1, :cond_2

    .line 1234839
    const/4 v0, 0x0

    .line 1234840
    :goto_1
    move-object v0, v0

    .line 1234841
    iput-object v0, p0, LX/7lP;->d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1234842
    :cond_0
    return-object p0

    .line 1234843
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1234844
    :cond_2
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    if-eqz v0, :cond_3

    .line 1234845
    sget-object v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->a:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    goto :goto_1

    .line 1234846
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1234847
    new-instance v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1234848
    :cond_4
    sget-object v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->a:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    goto :goto_1
.end method

.method public final a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;
    .locals 2

    .prologue
    .line 1234849
    new-instance v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v0, p0}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;-><init>(LX/7lP;)V

    return-object v0
.end method
