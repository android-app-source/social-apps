.class public LX/7U1;
.super LX/7Ty;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/1FJ;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/7U0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1211288
    const-class v0, LX/7U1;

    sput-object v0, LX/7U1;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1FJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1211248
    invoke-static {p1, p2}, LX/7U1;->a(Landroid/content/res/Resources;LX/1FJ;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, LX/7Ty;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 1211249
    invoke-virtual {p2}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/7U1;->a:LX/1FJ;

    .line 1211250
    invoke-virtual {p2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 1211251
    instance-of p1, v0, LX/1q7;

    if-eqz p1, :cond_0

    .line 1211252
    sget-object p1, LX/7U0;->ANIMATED:LX/7U0;

    iput-object p1, p0, LX/7U1;->d:LX/7U0;

    .line 1211253
    :goto_0
    return-void

    .line 1211254
    :cond_0
    sget-object p1, LX/7U0;->STATIC:LX/7U0;

    iput-object p1, p0, LX/7U1;->d:LX/7U0;

    goto :goto_0
.end method

.method private static a(Landroid/content/res/Resources;LX/1FJ;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 1211268
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211269
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1211270
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 1211271
    instance-of v1, v0, LX/1ll;

    if-eqz v1, :cond_0

    .line 1211272
    check-cast v0, LX/1ll;

    .line 1211273
    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211274
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    .line 1211275
    :goto_0
    return-object v0

    .line 1211276
    :cond_0
    instance-of v1, v0, LX/1q7;

    if-eqz v1, :cond_1

    .line 1211277
    check-cast v0, LX/1q7;

    .line 1211278
    iget-object v1, v0, LX/1q7;->b:Ljava/util/List;

    move-object v1, v1

    .line 1211279
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211280
    iget-object v1, v0, LX/1q7;->c:Ljava/util/List;

    move-object v1, v1

    .line 1211281
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211282
    new-instance v1, LX/4nD;

    .line 1211283
    iget-object v2, v0, LX/1q7;->b:Ljava/util/List;

    move-object v2, v2

    .line 1211284
    iget-object v3, v0, LX/1q7;->c:Ljava/util/List;

    move-object v3, v3

    .line 1211285
    invoke-direct {v1, p0, v2, v3}, LX/4nD;-><init>(Landroid/content/res/Resources;Ljava/util/List;Ljava/util/List;)V

    move-object v0, v1

    .line 1211286
    goto :goto_0

    .line 1211287
    :cond_1
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized image class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 1211265
    invoke-virtual {p0}, LX/7Ty;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1211266
    :goto_0
    return-void

    .line 1211267
    :cond_0
    iget-object v0, p0, LX/7U1;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1211261
    invoke-virtual {p0}, LX/7Ty;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211262
    sget-object v0, LX/7U1;->c:Ljava/lang/Class;

    const-string v1, "draw: Drawable %x already closed. Underlying closeable ref = %x"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/7U1;->a:LX/1FJ;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1211263
    :goto_0
    return-void

    .line 1211264
    :cond_0
    invoke-super {p0, p1}, LX/7Ty;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final finalize()V
    .locals 5

    .prologue
    .line 1211255
    invoke-virtual {p0}, LX/7Ty;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211256
    :goto_0
    return-void

    .line 1211257
    :cond_0
    sget-object v0, LX/7U1;->c:Ljava/lang/Class;

    const-string v1, "finalize: Drawable %x still open. Underlying closeable ref = %x, bitmap = %x"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/7U1;->a:LX/1FJ;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/7U1;->a:LX/1FJ;

    invoke-virtual {v4}, LX/1FJ;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1211258
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, LX/7U1;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1211259
    invoke-virtual {p0}, LX/7U1;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1211260
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
