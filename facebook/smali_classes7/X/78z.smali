.class public final LX/78z;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/790;


# direct methods
.method public constructor <init>(LX/790;)V
    .locals 0

    .prologue
    .line 1173736
    iput-object p1, p0, LX/78z;->a:LX/790;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/790;B)V
    .locals 0

    .prologue
    .line 1173737
    invoke-direct {p0, p1}, LX/78z;-><init>(LX/790;)V

    return-void
.end method

.method private a(I)LX/78n;
    .locals 1

    .prologue
    .line 1173738
    iget-object v0, p0, LX/78z;->a:LX/790;

    iget-object v0, v0, LX/790;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/78n;

    return-object v0
.end method

.method private static a(Landroid/widget/TextView;LX/78n;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1173731
    iget-object v0, p1, LX/78n;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1173732
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173733
    iget v0, p1, LX/78n;->b:I

    move v0, v0

    .line 1173734
    invoke-virtual {p0, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1173735
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 1173730
    iget-object v0, p0, LX/78z;->a:LX/790;

    iget-object v0, v0, LX/790;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1173722
    invoke-direct {p0, p1}, LX/78z;->a(I)LX/78n;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1173727
    iget-object v0, p0, LX/78z;->a:LX/790;

    iget-object v0, v0, LX/790;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/78n;

    .line 1173728
    iget v1, v0, LX/78n;->b:I

    move v0, v1

    .line 1173729
    int-to-long v0, v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1173723
    if-nez p2, :cond_0

    .line 1173724
    iget-object v0, p0, LX/78z;->a:LX/790;

    invoke-virtual {v0}, LX/790;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0311c9

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 1173725
    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, p1}, LX/78z;->a(I)LX/78n;

    move-result-object v2

    invoke-static {v0, v2}, LX/78z;->a(Landroid/widget/TextView;LX/78n;)V

    .line 1173726
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method
