.class public final LX/7yq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yp;


# instance fields
.field public final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "LX/7yp;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/7yp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1280365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280366
    iput-object p1, p0, LX/7yq;->a:Ljava/lang/Iterable;

    .line 1280367
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7yq;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1280368
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1280380
    iget-object v0, p0, LX/7yq;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1280381
    iget-object v0, p0, LX/7yq;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yp;

    .line 1280382
    invoke-interface {v0}, LX/7yp;->a()V

    goto :goto_0

    .line 1280383
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 1280377
    iget-object v0, p0, LX/7yq;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yp;

    .line 1280378
    invoke-interface {v0, p1}, LX/7yp;->a(F)V

    goto :goto_0

    .line 1280379
    :cond_0
    return-void
.end method

.method public final a(LX/7zB;)V
    .locals 2

    .prologue
    .line 1280384
    iget-object v0, p0, LX/7yq;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1280385
    :cond_0
    return-void

    .line 1280386
    :cond_1
    iget-object v0, p0, LX/7yq;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yp;

    .line 1280387
    invoke-interface {v0, p1}, LX/7yp;->a(LX/7zB;)V

    goto :goto_0
.end method

.method public final a(LX/7zL;)V
    .locals 2

    .prologue
    .line 1280373
    iget-object v0, p0, LX/7yq;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1280374
    :cond_0
    return-void

    .line 1280375
    :cond_1
    iget-object v0, p0, LX/7yq;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yp;

    .line 1280376
    invoke-interface {v0, p1}, LX/7yp;->a(LX/7zL;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1280369
    iget-object v0, p0, LX/7yq;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1280370
    :cond_0
    return-void

    .line 1280371
    :cond_1
    iget-object v0, p0, LX/7yq;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yp;

    .line 1280372
    invoke-interface {v0}, LX/7yp;->b()V

    goto :goto_0
.end method
