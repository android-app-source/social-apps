.class public final LX/7rz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1263917
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 1263918
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1263919
    :goto_0
    return v1

    .line 1263920
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1263921
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1263922
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1263923
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1263924
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1263925
    const-string v9, "claim_action_link"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1263926
    const/4 v8, 0x0

    .line 1263927
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_e

    .line 1263928
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1263929
    :goto_2
    move v7, v8

    .line 1263930
    goto :goto_1

    .line 1263931
    :cond_2
    const-string v9, "event"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1263932
    invoke-static {p0, p1}, LX/7st;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1263933
    :cond_3
    const-string v9, "event_tickets"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1263934
    const/4 v8, 0x0

    .line 1263935
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_13

    .line 1263936
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1263937
    :goto_3
    move v5, v8

    .line 1263938
    goto :goto_1

    .line 1263939
    :cond_4
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1263940
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1263941
    :cond_5
    const-string v9, "message"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1263942
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1263943
    :cond_6
    const-string v9, "order_status"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1263944
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto/16 :goto_1

    .line 1263945
    :cond_7
    const-string v9, "receipt_url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1263946
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1263947
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1263948
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1263949
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1263950
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1263951
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1263952
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1263953
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1263954
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1263955
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1

    .line 1263956
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1263957
    :cond_b
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_d

    .line 1263958
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1263959
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1263960
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_b

    if-eqz v10, :cond_b

    .line 1263961
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1263962
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 1263963
    :cond_c
    const-string v11, "url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1263964
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 1263965
    :cond_d
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1263966
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 1263967
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1263968
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_e
    move v7, v8

    move v9, v8

    goto :goto_4

    .line 1263969
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1263970
    :cond_10
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_12

    .line 1263971
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1263972
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1263973
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_10

    if-eqz v9, :cond_10

    .line 1263974
    const-string v10, "nodes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1263975
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1263976
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_11

    .line 1263977
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_11

    .line 1263978
    invoke-static {p0, p1}, LX/7ry;->b(LX/15w;LX/186;)I

    move-result v9

    .line 1263979
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1263980
    :cond_11
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1263981
    goto :goto_5

    .line 1263982
    :cond_12
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1263983
    invoke-virtual {p1, v8, v5}, LX/186;->b(II)V

    .line 1263984
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_13
    move v5, v8

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x5

    .line 1263985
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1263986
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1263987
    if-eqz v0, :cond_2

    .line 1263988
    const-string v1, "claim_action_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263989
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1263990
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1263991
    if-eqz v1, :cond_0

    .line 1263992
    const-string v3, "title"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263993
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1263994
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1263995
    if-eqz v1, :cond_1

    .line 1263996
    const-string v3, "url"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263997
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1263998
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1263999
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1264000
    if-eqz v0, :cond_3

    .line 1264001
    const-string v1, "event"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264002
    invoke-static {p0, v0, p2, p3}, LX/7st;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1264003
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1264004
    if-eqz v0, :cond_6

    .line 1264005
    const-string v1, "event_tickets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264006
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1264007
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1264008
    if-eqz v1, :cond_5

    .line 1264009
    const-string v3, "nodes"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264010
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1264011
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 1264012
    invoke-virtual {p0, v1, v3}, LX/15i;->q(II)I

    move-result v0

    invoke-static {p0, v0, p2, p3}, LX/7ry;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1264013
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1264014
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1264015
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1264016
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1264017
    if-eqz v0, :cond_7

    .line 1264018
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264019
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1264020
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1264021
    if-eqz v0, :cond_8

    .line 1264022
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264023
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1264024
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1264025
    if-eqz v0, :cond_9

    .line 1264026
    const-string v0, "order_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264027
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1264028
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1264029
    if-eqz v0, :cond_a

    .line 1264030
    const-string v1, "receipt_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264031
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1264032
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1264033
    return-void
.end method
