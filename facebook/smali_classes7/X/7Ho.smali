.class public LX/7Ho;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0rf;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/7HY;",
            "LX/7Hn",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/7Hl;

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/7Hl;LX/0rb;LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1191185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191186
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7Ho;->a:Ljava/util/HashMap;

    .line 1191187
    iput-object p1, p0, LX/7Ho;->b:LX/7Hl;

    .line 1191188
    iput-object p3, p0, LX/7Ho;->c:LX/0Uh;

    .line 1191189
    invoke-interface {p2, p0}, LX/0rb;->a(LX/0rf;)V

    .line 1191190
    return-void
.end method

.method public static a(LX/7Ho;LX/7B6;LX/7HY;)LX/7Hi;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            "LX/7HY;",
            ")",
            "LX/7Hi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1191163
    iget-object v0, p0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Hn;

    .line 1191164
    if-nez v0, :cond_0

    .line 1191165
    const/4 v0, 0x0

    .line 1191166
    :goto_0
    return-object v0

    .line 1191167
    :cond_0
    iget-object v1, p1, LX/7B6;->c:Ljava/lang/String;

    .line 1191168
    iget-object v2, p1, LX/7B6;->b:Ljava/lang/String;

    .line 1191169
    invoke-virtual {v0, v1, v2}, LX/7Hn;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1191170
    invoke-virtual {v0, v1, v2}, LX/7Hn;->a(Ljava/lang/String;Ljava/lang/String;)LX/7Hi;

    move-result-object v0

    goto :goto_0

    .line 1191171
    :cond_1
    if-nez v1, :cond_2

    const-string v1, ""

    .line 1191172
    :cond_2
    const/4 v5, 0x0

    .line 1191173
    iget-object v3, v0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 1191174
    if-nez v3, :cond_3

    .line 1191175
    iget-object v3, v0, LX/7Hn;->b:LX/7Hi;

    move-object v3, v3

    .line 1191176
    :goto_1
    move-object v0, v3

    .line 1191177
    goto :goto_0

    .line 1191178
    :cond_3
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1191179
    if-eqz v5, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result p2

    if-le p1, p2, :cond_7

    :cond_4
    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    :goto_3
    move-object v5, v4

    .line 1191180
    goto :goto_2

    .line 1191181
    :cond_5
    if-eqz v5, :cond_6

    .line 1191182
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7Hi;

    goto :goto_1

    .line 1191183
    :cond_6
    iget-object v3, v0, LX/7Hn;->b:LX/7Hi;

    move-object v3, v3

    .line 1191184
    goto :goto_1

    :cond_7
    move-object v4, v5

    goto :goto_3
.end method

.method public static a(LX/7Ho;LX/7B6;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            "Ljava/util/List",
            "<",
            "LX/7Hi",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1191191
    iget-object v0, p0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HY;

    .line 1191192
    invoke-virtual {v0}, LX/7HY;->isRemote()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1191193
    invoke-static {p0, p1, v0}, LX/7Ho;->a(LX/7Ho;LX/7B6;LX/7HY;)LX/7Hi;

    move-result-object v0

    .line 1191194
    if-eqz v0, :cond_0

    .line 1191195
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1191196
    :cond_1
    return-void
.end method

.method public static b(LX/0QB;)LX/7Ho;
    .locals 4

    .prologue
    .line 1191161
    new-instance v3, LX/7Ho;

    invoke-static {p0}, LX/7Hl;->b(LX/0QB;)LX/7Hl;

    move-result-object v0

    check-cast v0, LX/7Hl;

    invoke-static {p0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v1

    check-cast v1, LX/0rb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, LX/7Ho;-><init>(LX/7Hl;LX/0rb;LX/0Uh;)V

    .line 1191162
    return-object v3
.end method


# virtual methods
.method public final a(LX/32G;)V
    .locals 2

    .prologue
    .line 1191155
    iget-object v0, p0, LX/7Ho;->b:LX/7Hl;

    invoke-virtual {v0, p1}, LX/7Hl;->a(LX/32G;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1191156
    iget-object v0, p0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HY;

    .line 1191157
    iget-object p1, p0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Hn;

    .line 1191158
    if-eqz v0, :cond_0

    .line 1191159
    invoke-virtual {v0}, LX/7Hn;->a()V

    goto :goto_0

    .line 1191160
    :cond_1
    return-void
.end method

.method public final a(LX/7HY;LX/7Hn;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7HY;",
            "LX/7Hn",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1191151
    iget-object v0, p0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1191152
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Key already exists."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1191153
    :cond_0
    iget-object v0, p0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191154
    return-void
.end method
