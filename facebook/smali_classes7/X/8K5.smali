.class public final enum LX/8K5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8K5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8K5;

.field public static final enum COMPOSER_CANCEL_DIALOG:LX/8K5;

.field public static final enum COMPOSER_POPOVER_MENU:LX/8K5;

.field public static final enum EDIT_PENDING_POST:LX/8K5;

.field public static final enum PLATFORM_COMPOSER_CANCEL_DIALOG:LX/8K5;

.field public static final enum PLATFORM_COMPOSER_POPOVER_MENU:LX/8K5;

.field public static final enum UPLOAD_LATER_PENDING_POST:LX/8K5;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1329838
    new-instance v0, LX/8K5;

    const-string v1, "COMPOSER_CANCEL_DIALOG"

    const-string v2, "composer_cancel_dialog"

    invoke-direct {v0, v1, v4, v2}, LX/8K5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K5;->COMPOSER_CANCEL_DIALOG:LX/8K5;

    .line 1329839
    new-instance v0, LX/8K5;

    const-string v1, "PLATFORM_COMPOSER_CANCEL_DIALOG"

    const-string v2, "platform_composer_cancel_dialog"

    invoke-direct {v0, v1, v5, v2}, LX/8K5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K5;->PLATFORM_COMPOSER_CANCEL_DIALOG:LX/8K5;

    .line 1329840
    new-instance v0, LX/8K5;

    const-string v1, "COMPOSER_POPOVER_MENU"

    const-string v2, "composer_popover_menu"

    invoke-direct {v0, v1, v6, v2}, LX/8K5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K5;->COMPOSER_POPOVER_MENU:LX/8K5;

    .line 1329841
    new-instance v0, LX/8K5;

    const-string v1, "PLATFORM_COMPOSER_POPOVER_MENU"

    const-string v2, "platform_composer_popover_menu"

    invoke-direct {v0, v1, v7, v2}, LX/8K5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K5;->PLATFORM_COMPOSER_POPOVER_MENU:LX/8K5;

    .line 1329842
    new-instance v0, LX/8K5;

    const-string v1, "EDIT_PENDING_POST"

    const-string v2, "edit_uploading_post"

    invoke-direct {v0, v1, v8, v2}, LX/8K5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K5;->EDIT_PENDING_POST:LX/8K5;

    .line 1329843
    new-instance v0, LX/8K5;

    const-string v1, "UPLOAD_LATER_PENDING_POST"

    const/4 v2, 0x5

    const-string v3, "upload_later_pending_post"

    invoke-direct {v0, v1, v2, v3}, LX/8K5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K5;->UPLOAD_LATER_PENDING_POST:LX/8K5;

    .line 1329844
    const/4 v0, 0x6

    new-array v0, v0, [LX/8K5;

    sget-object v1, LX/8K5;->COMPOSER_CANCEL_DIALOG:LX/8K5;

    aput-object v1, v0, v4

    sget-object v1, LX/8K5;->PLATFORM_COMPOSER_CANCEL_DIALOG:LX/8K5;

    aput-object v1, v0, v5

    sget-object v1, LX/8K5;->COMPOSER_POPOVER_MENU:LX/8K5;

    aput-object v1, v0, v6

    sget-object v1, LX/8K5;->PLATFORM_COMPOSER_POPOVER_MENU:LX/8K5;

    aput-object v1, v0, v7

    sget-object v1, LX/8K5;->EDIT_PENDING_POST:LX/8K5;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8K5;->UPLOAD_LATER_PENDING_POST:LX/8K5;

    aput-object v2, v0, v1

    sput-object v0, LX/8K5;->$VALUES:[LX/8K5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1329845
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1329846
    iput-object p3, p0, LX/8K5;->analyticsName:Ljava/lang/String;

    .line 1329847
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8K5;
    .locals 1

    .prologue
    .line 1329848
    const-class v0, LX/8K5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8K5;

    return-object v0
.end method

.method public static values()[LX/8K5;
    .locals 1

    .prologue
    .line 1329849
    sget-object v0, LX/8K5;->$VALUES:[LX/8K5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8K5;

    return-object v0
.end method
