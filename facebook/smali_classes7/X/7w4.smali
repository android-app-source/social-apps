.class public LX/7w4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uH;


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/payments/confirmation/SimpleConfirmationData;


# direct methods
.method public constructor <init>(ZLcom/facebook/payments/confirmation/SimpleConfirmationData;)V
    .locals 0

    .prologue
    .line 1275389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275390
    iput-boolean p1, p0, LX/7w4;->a:Z

    .line 1275391
    iput-object p2, p0, LX/7w4;->b:Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1275392
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1275388
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1275385
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1275387
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This should not be called"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()LX/6uT;
    .locals 1

    .prologue
    .line 1275386
    sget-object v0, LX/6uT;->PRODUCT_USER_ENGAGE_OPTION:LX/6uT;

    return-object v0
.end method
