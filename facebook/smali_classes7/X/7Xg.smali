.class public final LX/7Xg;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "Lcom/facebook/zero/service/ZeroHeaderRequestManager;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7Xg;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/ZeroHeaderRequestManager;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1218588
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1218589
    return-void
.end method

.method public static a(LX/0QB;)LX/7Xg;
    .locals 4

    .prologue
    .line 1218590
    sget-object v0, LX/7Xg;->a:LX/7Xg;

    if-nez v0, :cond_1

    .line 1218591
    const-class v1, LX/7Xg;

    monitor-enter v1

    .line 1218592
    :try_start_0
    sget-object v0, LX/7Xg;->a:LX/7Xg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1218593
    if-eqz v2, :cond_0

    .line 1218594
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1218595
    new-instance v3, LX/7Xg;

    const/16 p0, 0x13fa

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/7Xg;-><init>(LX/0Ot;)V

    .line 1218596
    move-object v0, v3

    .line 1218597
    sput-object v0, LX/7Xg;->a:LX/7Xg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1218598
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1218599
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1218600
    :cond_1
    sget-object v0, LX/7Xg;->a:LX/7Xg;

    return-object v0

    .line 1218601
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1218602
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1218603
    check-cast p3, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    .line 1218604
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1218605
    const-string v1, "com.facebook.zero.ACTION_FORCE_ZERO_HEADER_REFRESH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1218606
    const/4 v0, 0x1

    const-string v1, "forced"

    invoke-virtual {p3, v0, v1}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1218607
    :cond_0
    return-void
.end method
