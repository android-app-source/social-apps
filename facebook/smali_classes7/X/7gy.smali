.class public final enum LX/7gy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gy;

.field public static final enum INCOMING_REPLY:LX/7gy;

.field public static final enum INCOMING_STORY:LX/7gy;

.field public static final enum OUTGOING_STORY:LX/7gy;

.field public static final enum OUTOING_REPLY:LX/7gy;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1225166
    new-instance v0, LX/7gy;

    const-string v1, "INCOMING_REPLY"

    invoke-direct {v0, v1, v2}, LX/7gy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gy;->INCOMING_REPLY:LX/7gy;

    .line 1225167
    new-instance v0, LX/7gy;

    const-string v1, "INCOMING_STORY"

    invoke-direct {v0, v1, v3}, LX/7gy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gy;->INCOMING_STORY:LX/7gy;

    .line 1225168
    new-instance v0, LX/7gy;

    const-string v1, "OUTOING_REPLY"

    invoke-direct {v0, v1, v4}, LX/7gy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gy;->OUTOING_REPLY:LX/7gy;

    .line 1225169
    new-instance v0, LX/7gy;

    const-string v1, "OUTGOING_STORY"

    invoke-direct {v0, v1, v5}, LX/7gy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gy;->OUTGOING_STORY:LX/7gy;

    .line 1225170
    const/4 v0, 0x4

    new-array v0, v0, [LX/7gy;

    sget-object v1, LX/7gy;->INCOMING_REPLY:LX/7gy;

    aput-object v1, v0, v2

    sget-object v1, LX/7gy;->INCOMING_STORY:LX/7gy;

    aput-object v1, v0, v3

    sget-object v1, LX/7gy;->OUTOING_REPLY:LX/7gy;

    aput-object v1, v0, v4

    sget-object v1, LX/7gy;->OUTGOING_STORY:LX/7gy;

    aput-object v1, v0, v5

    sput-object v0, LX/7gy;->$VALUES:[LX/7gy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1225171
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gy;
    .locals 1

    .prologue
    .line 1225172
    const-class v0, LX/7gy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gy;

    return-object v0
.end method

.method public static values()[LX/7gy;
    .locals 1

    .prologue
    .line 1225173
    sget-object v0, LX/7gy;->$VALUES:[LX/7gy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gy;

    return-object v0
.end method
