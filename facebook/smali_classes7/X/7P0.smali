.class public LX/7P0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<[B>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/io/OutputStream;

.field public final b:Ljava/io/InputStream;

.field private final d:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<[B>;"
        }
    .end annotation
.end field

.field public e:[B

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field public final j:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1202417
    new-instance v0, LX/7Ow;

    invoke-direct {v0}, LX/7Ow;-><init>()V

    sput-object v0, LX/7P0;->c:LX/1FN;

    return-void
.end method

.method public constructor <init>([BLX/1FN;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "LX/1FN",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1202418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202419
    iput v1, p0, LX/7P0;->f:I

    .line 1202420
    iput v1, p0, LX/7P0;->g:I

    .line 1202421
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/7P0;->j:Ljava/lang/Object;

    .line 1202422
    new-instance v0, LX/7Ox;

    invoke-direct {v0, p0}, LX/7Ox;-><init>(LX/7P0;)V

    iput-object v0, p0, LX/7P0;->a:Ljava/io/OutputStream;

    .line 1202423
    new-instance v0, LX/7Oy;

    invoke-direct {v0, p0}, LX/7Oy;-><init>(LX/7P0;)V

    iput-object v0, p0, LX/7P0;->b:Ljava/io/InputStream;

    .line 1202424
    iput-object p1, p0, LX/7P0;->e:[B

    .line 1202425
    iput-object p2, p0, LX/7P0;->d:LX/1FN;

    .line 1202426
    iput v1, p0, LX/7P0;->f:I

    .line 1202427
    iput v1, p0, LX/7P0;->g:I

    .line 1202428
    iput-boolean v1, p0, LX/7P0;->h:Z

    .line 1202429
    iput-boolean v1, p0, LX/7P0;->i:Z

    .line 1202430
    return-void
.end method

.method public static a(LX/7P0;)V
    .locals 2

    .prologue
    .line 1202431
    iget-boolean v0, p0, LX/7P0;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/7P0;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7P0;->e:[B

    if-eqz v0, :cond_0

    .line 1202432
    iget-object v0, p0, LX/7P0;->d:LX/1FN;

    iget-object v1, p0, LX/7P0;->e:[B

    invoke-interface {v0, v1}, LX/1FN;->a(Ljava/lang/Object;)V

    .line 1202433
    const/4 v0, 0x0

    iput-object v0, p0, LX/7P0;->e:[B

    .line 1202434
    :cond_0
    return-void
.end method
