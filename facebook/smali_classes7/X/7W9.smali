.class public final LX/7W9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yJ;


# instance fields
.field public final synthetic a:LX/7WA;


# direct methods
.method public constructor <init>(LX/7WA;)V
    .locals 0

    .prologue
    .line 1215882
    iput-object p1, p0, LX/7W9;->a:LX/7WA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V
    .locals 3

    .prologue
    .line 1215883
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->x:LX/0yP;

    invoke-virtual {v0, p0}, LX/0yP;->b(LX/0yJ;)V

    .line 1215884
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->y:LX/0yI;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    .line 1215885
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1215886
    new-instance v0, LX/7W8;

    invoke-direct {v0, p0}, LX/7W8;-><init>(LX/7W9;)V

    .line 1215887
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1215888
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1215889
    :goto_0
    return-void

    .line 1215890
    :cond_0
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v1, p0, LX/7W9;->a:LX/7WA;

    iget-object v1, v1, LX/7WA;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7W9;->a:LX/7WA;

    iget-object v2, v2, LX/7WA;->c:Landroid/os/Bundle;

    .line 1215891
    invoke-static {v0, v1, v2}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a$redex0(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1215892
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;LX/0yi;)V
    .locals 3

    .prologue
    .line 1215893
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->x:LX/0yP;

    invoke-virtual {v0, p0}, LX/0yP;->b(LX/0yJ;)V

    .line 1215894
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->y:LX/0yI;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/Throwable;LX/0yi;)V

    .line 1215895
    iget-object v0, p0, LX/7W9;->a:LX/7WA;

    iget-object v0, v0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v1, p0, LX/7W9;->a:LX/7WA;

    iget-object v1, v1, LX/7WA;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7W9;->a:LX/7WA;

    iget-object v2, v2, LX/7WA;->c:Landroid/os/Bundle;

    .line 1215896
    invoke-static {v0, v1, v2}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a$redex0(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1215897
    return-void
.end method
