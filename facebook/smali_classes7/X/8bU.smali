.class public LX/8bU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field public final b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1372078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1372079
    iput-object p1, p0, LX/8bU;->a:Ljava/util/concurrent/ExecutorService;

    .line 1372080
    iput-object p2, p0, LX/8bU;->b:Ljava/util/concurrent/ExecutorService;

    .line 1372081
    return-void
.end method

.method public static a(LX/0QB;)LX/8bU;
    .locals 5

    .prologue
    .line 1372082
    const-class v1, LX/8bU;

    monitor-enter v1

    .line 1372083
    :try_start_0
    sget-object v0, LX/8bU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1372084
    sput-object v2, LX/8bU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1372085
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1372086
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1372087
    new-instance p0, LX/8bU;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4}, LX/8bU;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V

    .line 1372088
    move-object v0, p0

    .line 1372089
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1372090
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8bU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1372091
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1372092
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;ILX/8bT;)V
    .locals 3

    .prologue
    .line 1372093
    iget-object v0, p0, LX/8bU;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/richdocument/utils/OffUiThreadImageDecoder$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/richdocument/utils/OffUiThreadImageDecoder$1;-><init>(LX/8bU;Landroid/content/Context;ILX/8bT;)V

    const v2, -0x4fd9463c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1372094
    return-void
.end method
