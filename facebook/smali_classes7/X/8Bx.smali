.class public final enum LX/8Bx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Bx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Bx;

.field public static final enum BUSINESS_ACTIVITY:LX/8Bx;

.field public static final enum GAMES_SELECTION_ACTIVITY:LX/8Bx;

.field public static final enum MESSENGER_HOME_FRAGMENT:LX/8Bx;

.field public static final enum QUICKSILVER_ACTIVITY:LX/8Bx;

.field public static final enum THREAD_VIEW:LX/8Bx;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1309978
    new-instance v0, LX/8Bx;

    const-string v1, "THREAD_VIEW"

    invoke-direct {v0, v1, v2}, LX/8Bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Bx;->THREAD_VIEW:LX/8Bx;

    .line 1309979
    new-instance v0, LX/8Bx;

    const-string v1, "MESSENGER_HOME_FRAGMENT"

    invoke-direct {v0, v1, v3}, LX/8Bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Bx;->MESSENGER_HOME_FRAGMENT:LX/8Bx;

    .line 1309980
    new-instance v0, LX/8Bx;

    const-string v1, "BUSINESS_ACTIVITY"

    invoke-direct {v0, v1, v4}, LX/8Bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Bx;->BUSINESS_ACTIVITY:LX/8Bx;

    .line 1309981
    new-instance v0, LX/8Bx;

    const-string v1, "GAMES_SELECTION_ACTIVITY"

    invoke-direct {v0, v1, v5}, LX/8Bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Bx;->GAMES_SELECTION_ACTIVITY:LX/8Bx;

    .line 1309982
    new-instance v0, LX/8Bx;

    const-string v1, "QUICKSILVER_ACTIVITY"

    invoke-direct {v0, v1, v6}, LX/8Bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Bx;->QUICKSILVER_ACTIVITY:LX/8Bx;

    .line 1309983
    const/4 v0, 0x5

    new-array v0, v0, [LX/8Bx;

    sget-object v1, LX/8Bx;->THREAD_VIEW:LX/8Bx;

    aput-object v1, v0, v2

    sget-object v1, LX/8Bx;->MESSENGER_HOME_FRAGMENT:LX/8Bx;

    aput-object v1, v0, v3

    sget-object v1, LX/8Bx;->BUSINESS_ACTIVITY:LX/8Bx;

    aput-object v1, v0, v4

    sget-object v1, LX/8Bx;->GAMES_SELECTION_ACTIVITY:LX/8Bx;

    aput-object v1, v0, v5

    sget-object v1, LX/8Bx;->QUICKSILVER_ACTIVITY:LX/8Bx;

    aput-object v1, v0, v6

    sput-object v0, LX/8Bx;->$VALUES:[LX/8Bx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1309984
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Bx;
    .locals 1

    .prologue
    .line 1309985
    const-class v0, LX/8Bx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Bx;

    return-object v0
.end method

.method public static values()[LX/8Bx;
    .locals 1

    .prologue
    .line 1309986
    sget-object v0, LX/8Bx;->$VALUES:[LX/8Bx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Bx;

    return-object v0
.end method
