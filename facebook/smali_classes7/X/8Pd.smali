.class public final LX/8Pd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;",
        ">;",
        "LX/8QJ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/PrivacyOperationsClient;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V
    .locals 0

    .prologue
    .line 1341925
    iput-object p1, p0, LX/8Pd;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1341926
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 1341927
    if-eqz p1, :cond_0

    .line 1341928
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1341929
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1341930
    :goto_0
    return-object v0

    .line 1341931
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1341932
    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;

    .line 1341933
    if-nez v0, :cond_4

    .line 1341934
    const/4 v2, 0x0

    .line 1341935
    :goto_1
    move-object v2, v2

    .line 1341936
    if-eqz v2, :cond_2

    invoke-static {v2}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v2}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    move-object v0, v1

    .line 1341937
    goto :goto_0

    .line 1341938
    :cond_3
    iget-object v0, p0, LX/8Pd;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v0, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R3;

    invoke-static {v2}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1341939
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->s()Z

    move-result v3

    .line 1341940
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, LX/3R3;->a(LX/0Px;Z)LX/8QJ;

    move-result-object v1

    move-object v2, v1

    .line 1341941
    move-object v0, v2

    .line 1341942
    goto :goto_0

    .line 1341943
    :cond_4
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    .line 1341944
    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v3

    .line 1341945
    if-nez v3, :cond_5

    .line 1341946
    const/4 v4, 0x0

    .line 1341947
    :goto_2
    move-object v3, v4

    .line 1341948
    iput-object v3, v2, LX/23u;->al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1341949
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    goto :goto_1

    .line 1341950
    :cond_5
    new-instance v4, LX/4YL;

    invoke-direct {v4}, LX/4YL;-><init>()V

    .line 1341951
    invoke-virtual {v3}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v5

    .line 1341952
    if-nez v5, :cond_6

    .line 1341953
    const/4 p1, 0x0

    .line 1341954
    :goto_3
    move-object v5, p1

    .line 1341955
    iput-object v5, v4, LX/4YL;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 1341956
    invoke-virtual {v3}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;->b()Z

    move-result v5

    .line 1341957
    iput-boolean v5, v4, LX/4YL;->l:Z

    .line 1341958
    invoke-virtual {v4}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    goto :goto_2

    .line 1341959
    :cond_6
    new-instance p1, LX/4YI;

    invoke-direct {p1}, LX/4YI;-><init>()V

    .line 1341960
    invoke-virtual {v5}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v0

    .line 1341961
    iput-object v0, p1, LX/4YI;->b:LX/0Px;

    .line 1341962
    invoke-virtual {p1}, LX/4YI;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object p1

    goto :goto_3
.end method
