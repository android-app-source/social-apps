.class public LX/7Oj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Oi;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:J

.field private final c:J

.field private final d:LX/3DW;

.field public e:LX/7Oh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1201997
    const-class v0, LX/7Oj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Oj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JJLX/3DW;)V
    .locals 1

    .prologue
    .line 1201998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1201999
    iput-wide p1, p0, LX/7Oj;->b:J

    .line 1202000
    iput-wide p3, p0, LX/7Oj;->c:J

    .line 1202001
    iput-object p5, p0, LX/7Oj;->d:LX/3DW;

    .line 1202002
    return-void
.end method

.method public static b(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1202003
    :try_start_0
    invoke-static {p0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1202004
    :catch_0
    move-exception v0

    .line 1202005
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    const-class v2, Ljava/io/IOException;

    invoke-static {v1, v2}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1202006
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unknown error getting result"

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(LX/2WF;Ljava/io/OutputStream;)J
    .locals 11

    .prologue
    .line 1202007
    new-instance v0, LX/1iJ;

    invoke-direct {v0, p2}, LX/1iJ;-><init>(Ljava/io/OutputStream;)V

    .line 1202008
    iget-object v7, p0, LX/7Oj;->e:LX/7Oh;

    if-eqz v7, :cond_1

    iget-wide v7, p0, LX/7Oj;->b:J

    iget-wide v9, p1, LX/2WF;->a:J

    cmp-long v7, v7, v9

    if-nez v7, :cond_1

    const/4 v7, 0x1

    :goto_0
    move v1, v7

    .line 1202009
    if-eqz v1, :cond_0

    .line 1202010
    iget-object v1, p0, LX/7Oj;->e:LX/7Oh;

    .line 1202011
    const/4 v2, 0x0

    iput-object v2, p0, LX/7Oj;->e:LX/7Oh;

    .line 1202012
    invoke-virtual {v1, v0}, LX/7Oh;->a(Ljava/io/OutputStream;)V

    .line 1202013
    invoke-virtual {v1}, LX/7Oh;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1202014
    iget-wide v7, v0, LX/1iJ;->a:J

    move-wide v0, v7

    .line 1202015
    :goto_1
    return-wide v0

    .line 1202016
    :cond_0
    new-instance v6, LX/7Oh;

    invoke-direct {v6}, LX/7Oh;-><init>()V

    .line 1202017
    invoke-virtual {v6, v0}, LX/7Oh;->a(Ljava/io/OutputStream;)V

    .line 1202018
    iget-object v1, p0, LX/7Oj;->d:LX/3DW;

    iget-wide v2, p1, LX/2WF;->a:J

    iget-wide v4, p1, LX/2WF;->b:J

    invoke-interface/range {v1 .. v6}, LX/3DW;->a(JJLX/3Di;)V

    .line 1202019
    invoke-virtual {v6}, LX/7Oh;->b()Z

    .line 1202020
    iget-wide v7, v0, LX/1iJ;->a:J

    move-wide v0, v7

    .line 1202021
    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final a()LX/3Dd;
    .locals 7

    .prologue
    .line 1202022
    iget-object v0, p0, LX/7Oj;->e:LX/7Oh;

    if-nez v0, :cond_0

    .line 1202023
    new-instance v0, LX/7Oh;

    invoke-direct {v0}, LX/7Oh;-><init>()V

    iput-object v0, p0, LX/7Oj;->e:LX/7Oh;

    .line 1202024
    iget-object v1, p0, LX/7Oj;->d:LX/3DW;

    iget-wide v2, p0, LX/7Oj;->b:J

    iget-wide v4, p0, LX/7Oj;->c:J

    iget-object v6, p0, LX/7Oj;->e:LX/7Oh;

    invoke-interface/range {v1 .. v6}, LX/3DW;->a(JJLX/3Di;)V

    .line 1202025
    :cond_0
    iget-object v0, p0, LX/7Oj;->e:LX/7Oh;

    .line 1202026
    iget-object v1, v0, LX/7Oh;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v1}, LX/7Oj;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Dd;

    move-object v0, v1

    .line 1202027
    return-object v0
.end method
