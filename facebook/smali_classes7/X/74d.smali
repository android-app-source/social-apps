.class public LX/74d;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1168193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(III)F
    .locals 2

    .prologue
    .line 1168194
    const/16 v0, 0x5a

    if-eq p2, v0, :cond_0

    const/16 v0, 0x10e

    if-ne p2, v0, :cond_1

    :cond_0
    int-to-float v0, p1

    int-to-float v1, p0

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_1
    int-to-float v0, p0

    int-to-float v1, p1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;Lcom/facebook/photos/base/media/PhotoItem;)F
    .locals 3
    .param p1    # Lcom/facebook/photos/base/media/PhotoItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 1168181
    if-nez p1, :cond_0

    move v0, v2

    .line 1168182
    :goto_0
    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    const/high16 v2, 0x7fc00000    # NaNf

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    .line 1168183
    :goto_1
    return v0

    .line 1168184
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v0

    goto :goto_0

    .line 1168185
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v2

    .line 1168186
    if-eqz v2, :cond_2

    iget v0, v2, LX/434;->a:I

    if-nez v0, :cond_3

    iget v0, v2, LX/434;->b:I

    if-nez v0, :cond_3

    .line 1168187
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1

    .line 1168188
    :cond_3
    if-nez p1, :cond_5

    move v0, v1

    .line 1168189
    :goto_2
    if-ne v0, v1, :cond_4

    .line 1168190
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v0

    .line 1168191
    :cond_4
    iget v1, v2, LX/434;->b:I

    iget v2, v2, LX/434;->a:I

    invoke-static {v1, v2, v0}, LX/74d;->a(III)F

    move-result v0

    goto :goto_1

    .line 1168192
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v0

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168178
    if-nez p0, :cond_0

    .line 1168179
    const/4 v0, 0x0

    .line 1168180
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0a4;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method
