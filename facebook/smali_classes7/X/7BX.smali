.class public LX/7BX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7BW;


# static fields
.field private static final a:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchJsonResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/7Bb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0lB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/7B0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1179004
    new-instance v0, LX/7BU;

    invoke-direct {v0}, LX/7BU;-><init>()V

    sput-object v0, LX/7BX;->a:LX/266;

    .line 1179005
    new-instance v0, LX/7BV;

    invoke-direct {v0}, LX/7BV;-><init>()V

    sput-object v0, LX/7BX;->b:LX/266;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1179007
    iget-object v1, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    sget-object v2, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    if-eq v1, v2, :cond_1

    .line 1179008
    :cond_0
    :goto_0
    return v0

    .line 1179009
    :cond_1
    iget-object v1, p0, LX/7BX;->e:LX/0ad;

    sget-short v2, LX/100;->bc:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1179010
    iget-object v1, p0, LX/7BX;->e:LX/0ad;

    sget v2, LX/100;->bd:I

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    .line 1179011
    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    iget-object v2, v2, LX/7B6;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    iget-object v2, v2, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1179012
    check-cast p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1179013
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1179014
    invoke-direct {p0, p1}, LX/7BX;->b(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1179015
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "format"

    const-string v5, "json"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179016
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "platform"

    const-string v5, "android"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179017
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "global"

    const-string v5, "true"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179018
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "cdn_cfg"

    iget-object v5, p0, LX/7BX;->e:LX/0ad;

    sget-char v6, LX/100;->be:C

    const-string v7, "default"

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179019
    const-string v0, "graphsearch_typeahead_global_keywords"

    .line 1179020
    :goto_0
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "query"

    invoke-static {p1}, LX/7Bb;->a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179021
    invoke-static {p1, v0}, LX/7Bb;->a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1179022
    iget-object v0, p0, LX/7BX;->g:LX/0Uh;

    sget v5, LX/2SU;->I:I

    invoke-virtual {v0, v5}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v5, LX/03R;->YES:LX/03R;

    if-ne v0, v5, :cond_4

    move v0, v1

    .line 1179023
    :goto_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v5

    .line 1179024
    iput-object v4, v5, LX/14O;->b:Ljava/lang/String;

    .line 1179025
    move-object v4, v5

    .line 1179026
    const-string v5, "GET"

    .line 1179027
    iput-object v5, v4, LX/14O;->c:Ljava/lang/String;

    .line 1179028
    move-object v4, v4

    .line 1179029
    const-string v5, "method/graphsearchtypeahead.get"

    .line 1179030
    iput-object v5, v4, LX/14O;->d:Ljava/lang/String;

    .line 1179031
    move-object v4, v4

    .line 1179032
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v5}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v4

    .line 1179033
    iput-object v3, v4, LX/14O;->g:Ljava/util/List;

    .line 1179034
    move-object v3, v4

    .line 1179035
    sget-object v4, LX/14S;->JSONPARSER:LX/14S;

    .line 1179036
    iput-object v4, v3, LX/14O;->k:LX/14S;

    .line 1179037
    move-object v4, v3

    .line 1179038
    if-eqz v0, :cond_5

    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-FB-ForkingType"

    const-string v6, "edge-sgp-search"

    invoke-direct {v3, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    :goto_2
    invoke-virtual {v4, v3}, LX/14O;->a(LX/0Px;)LX/14O;

    move-result-object v3

    .line 1179039
    iget-object v4, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    sget-object v5, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    if-ne v4, v5, :cond_0

    iget-object v4, p0, LX/7BX;->e:LX/0ad;

    sget-short v5, LX/100;->bf:S

    invoke-interface {v4, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-direct {p0, p1}, LX/7BX;->b(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, LX/7BX;->f:LX/7B0;

    invoke-virtual {v0}, LX/7B0;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1179040
    :cond_1
    iput-boolean v1, v3, LX/14O;->B:Z

    .line 1179041
    :cond_2
    iget-object v1, p0, LX/7BX;->f:LX/7B0;

    iget-object v0, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1179042
    iget-object v2, v0, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1179043
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, LX/7Az;->CACHEWARMER:LX/7Az;

    :goto_3
    invoke-virtual {v1, v0, v3}, LX/7B0;->a(LX/7Az;LX/14O;)V

    .line 1179044
    invoke-virtual {v3}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1179045
    :cond_3
    iget-object v0, p0, LX/7BX;->c:LX/7Bb;

    invoke-virtual {v0, p1, v3}, LX/7Bb;->a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/util/List;)V

    .line 1179046
    iget-object v0, p0, LX/7BX;->c:LX/7Bb;

    invoke-virtual {v0, p1, v3}, LX/7Bb;->b(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/util/List;)V

    .line 1179047
    const-string v0, "graphsearch_typeahead"

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 1179048
    goto :goto_1

    .line 1179049
    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    .line 1179050
    :cond_6
    sget-object v0, LX/7Az;->TYPEAHEAD:LX/7Az;

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1179051
    new-instance v1, LX/0lp;

    invoke-direct {v1}, LX/0lp;-><init>()V

    .line 1179052
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    sget-object v2, LX/7BX;->a:LX/266;

    invoke-virtual {v0, v2}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/model/GraphSearchJsonResponse;

    .line 1179053
    iget-object v2, v0, Lcom/facebook/search/api/model/GraphSearchJsonResponse;->response:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 1179054
    iget-object v2, p0, LX/7BX;->d:LX/0lB;

    invoke-virtual {v1, v2}, LX/15w;->a(LX/0lD;)V

    .line 1179055
    sget-object v2, LX/7BX;->b:LX/266;

    invoke-virtual {v1, v2}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1179056
    new-instance v2, LX/7Hc;

    invoke-static {v1}, LX/7BD;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchJsonResponse;->numTopResultsToShow:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v0, v0, Lcom/facebook/search/api/model/GraphSearchJsonResponse;->numTopResultsToShow:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    invoke-direct {v2, v1, v0}, LX/7Hc;-><init>(LX/0Px;I)V

    .line 1179057
    invoke-static {p2}, LX/7Hc;->a(LX/1pN;)I

    move-result v0

    .line 1179058
    iput v0, v2, LX/7Hc;->d:I

    .line 1179059
    return-object v2

    .line 1179060
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
