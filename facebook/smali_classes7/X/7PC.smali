.class public final LX/7PC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/server/NetworkRangeWriter;

.field private b:Ljava/io/OutputStream;

.field public c:Ljava/io/IOException;

.field public d:LX/15D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/15D",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/server/NetworkRangeWriter;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 1202540
    iput-object p1, p0, LX/7PC;->a:Lcom/facebook/video/server/NetworkRangeWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202541
    iput-object p2, p0, LX/7PC;->b:Ljava/io/OutputStream;

    .line 1202542
    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1202528
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 1202529
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/16 v1, 0xce

    if-eq v0, v1, :cond_0

    .line 1202530
    new-instance v1, LX/7P4;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/7P4;-><init>(ILjava/lang/String;)V

    throw v1

    .line 1202531
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 1202532
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 1202533
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    .line 1202534
    :try_start_0
    new-instance v0, LX/7Ol;

    invoke-direct {v0, v1}, LX/7Ol;-><init>(Ljava/io/InputStream;)V

    iget-object v1, p0, LX/7PC;->b:Ljava/io/OutputStream;

    invoke-static {v0, v1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1202535
    :goto_0
    return-object v0

    .line 1202536
    :catch_0
    move-exception v0

    .line 1202537
    iput-object v0, p0, LX/7PC;->c:Ljava/io/IOException;

    .line 1202538
    iget-object v0, p0, LX/7PC;->a:Lcom/facebook/video/server/NetworkRangeWriter;

    iget-object v0, v0, Lcom/facebook/video/server/NetworkRangeWriter;->g:LX/1Lw;

    iget-object v1, p0, LX/7PC;->d:LX/15D;

    invoke-virtual {v0, v1}, LX/1Lw;->c(LX/15D;)Z

    .line 1202539
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method
