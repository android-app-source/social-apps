.class public LX/6fD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/6fD;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2Mk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119592
    const-class v0, LX/6fD;

    sput-object v0, LX/6fD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2Mk;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2Mk;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1119588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119589
    iput-object p1, p0, LX/6fD;->b:LX/0Or;

    .line 1119590
    iput-object p2, p0, LX/6fD;->c:LX/2Mk;

    .line 1119591
    return-void
.end method

.method public static a(LX/0QB;)LX/6fD;
    .locals 5

    .prologue
    .line 1119452
    sget-object v0, LX/6fD;->d:LX/6fD;

    if-nez v0, :cond_1

    .line 1119453
    const-class v1, LX/6fD;

    monitor-enter v1

    .line 1119454
    :try_start_0
    sget-object v0, LX/6fD;->d:LX/6fD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1119455
    if-eqz v2, :cond_0

    .line 1119456
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1119457
    new-instance v4, LX/6fD;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v3

    check-cast v3, LX/2Mk;

    invoke-direct {v4, p0, v3}, LX/6fD;-><init>(LX/0Or;LX/2Mk;)V

    .line 1119458
    move-object v0, v4

    .line 1119459
    sput-object v0, LX/6fD;->d:LX/6fD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1119460
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1119461
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1119462
    :cond_1
    sget-object v0, LX/6fD;->d:LX/6fD;

    return-object v0

    .line 1119463
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1119464
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;Z)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 18

    .prologue
    .line 1119504
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    .line 1119505
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1119506
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Message Collections with different thread ids"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1119507
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1119508
    :cond_1
    :goto_0
    return-object p1

    .line 1119509
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 p1, p2

    .line 1119510
    goto :goto_0

    .line 1119511
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1119512
    invoke-static/range {p1 .. p2}, LX/6fD;->d(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 p1, p2

    .line 1119513
    goto :goto_0

    .line 1119514
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->d()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 1119515
    new-instance v6, LX/6fC;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/6fD;->c:LX/2Mk;

    invoke-direct {v6, v2}, LX/6fC;-><init>(LX/2Mk;)V

    .line 1119516
    new-instance v7, LX/6fC;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/6fD;->c:LX/2Mk;

    invoke-direct {v7, v2}, LX/6fC;-><init>(LX/2Mk;)V

    .line 1119517
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v8, :cond_5

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 1119518
    invoke-virtual {v6, v2}, LX/6fC;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 1119519
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1119520
    :cond_5
    const/4 v3, 0x0

    .line 1119521
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v9, :cond_17

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 1119522
    invoke-virtual {v7, v2}, LX/6fC;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 1119523
    invoke-static {v2, v5}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1119524
    const/4 v2, 0x1

    .line 1119525
    :goto_3
    if-nez v2, :cond_8

    .line 1119526
    if-nez p3, :cond_1

    .line 1119527
    :goto_4
    const/4 v2, 0x0

    .line 1119528
    const/4 v3, 0x0

    .line 1119529
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v5

    .line 1119530
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v8

    .line 1119531
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1119532
    new-instance v9, LX/6fC;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/6fD;->c:LX/2Mk;

    invoke-direct {v9, v4}, LX/6fC;-><init>(LX/2Mk;)V

    move v4, v2

    .line 1119533
    :cond_6
    :goto_5
    if-ge v4, v5, :cond_12

    if-ge v3, v8, :cond_12

    .line 1119534
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v11

    .line 1119535
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 1119536
    invoke-virtual {v9, v11}, LX/6fC;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1119537
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 1119538
    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 1119539
    :cond_8
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v5, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    goto :goto_4

    .line 1119540
    :cond_9
    invoke-virtual {v9, v2}, LX/6fC;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1119541
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1119542
    :cond_a
    invoke-static {v11, v2}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 1119543
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v11, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v14, v12, v13

    .line 1119544
    invoke-static {v2, v11}, LX/6fD;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1119545
    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1119546
    :goto_6
    add-int/lit8 v2, v4, 0x1

    .line 1119547
    add-int/lit8 v3, v3, 0x1

    move v4, v2

    goto :goto_5

    .line 1119548
    :cond_b
    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_6

    .line 1119549
    :cond_c
    invoke-virtual {v7, v11}, LX/6fC;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 1119550
    invoke-virtual {v7, v11}, LX/6fC;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v12

    .line 1119551
    iget-wide v14, v11, Lcom/facebook/messaging/model/messages/Message;->c:J

    iget-wide v0, v12, Lcom/facebook/messaging/model/messages/Message;->c:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-gtz v13, :cond_d

    iget-boolean v13, v11, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v13, :cond_e

    iget-boolean v13, v12, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v13, :cond_e

    .line 1119552
    :cond_d
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v11, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v14, v2, v13

    .line 1119553
    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1119554
    invoke-virtual {v9, v11}, LX/6fC;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 1119555
    invoke-virtual {v7, v12}, LX/6fC;->d(Lcom/facebook/messaging/model/messages/Message;)V

    .line 1119556
    add-int/lit8 v2, v4, 0x1

    :goto_7
    move v4, v2

    .line 1119557
    goto/16 :goto_5

    .line 1119558
    :cond_e
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v11, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v14, v12, v13

    .line 1119559
    :goto_8
    invoke-static {v11, v2}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_6

    if-ge v3, v8, :cond_6

    .line 1119560
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 1119561
    invoke-virtual {v6, v2}, LX/6fC;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-static {v2, v11}, LX/6fD;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-nez v12, :cond_f

    .line 1119562
    invoke-virtual {v7, v2}, LX/6fC;->d(Lcom/facebook/messaging/model/messages/Message;)V

    .line 1119563
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v14, v12, v13

    .line 1119564
    :goto_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 1119565
    :cond_f
    invoke-virtual {v9, v2}, LX/6fC;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 1119566
    invoke-virtual {v7, v2}, LX/6fC;->d(Lcom/facebook/messaging/model/messages/Message;)V

    .line 1119567
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v14, v12, v13

    goto :goto_9

    .line 1119568
    :cond_10
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v14, v12, v13

    .line 1119569
    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1119570
    invoke-virtual {v9, v2}, LX/6fC;->a(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_9

    .line 1119571
    :cond_11
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v11, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v13, v2, v12

    .line 1119572
    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1119573
    invoke-virtual {v9, v11}, LX/6fC;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 1119574
    add-int/lit8 v2, v4, 0x1

    goto :goto_7

    .line 1119575
    :cond_12
    :goto_a
    if-ge v3, v8, :cond_14

    .line 1119576
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 1119577
    invoke-virtual {v9, v2}, LX/6fC;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1119578
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v2, v4, v5

    .line 1119579
    :goto_b
    add-int/lit8 v3, v3, 0x1

    .line 1119580
    goto :goto_a

    .line 1119581
    :cond_13
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 1119582
    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1119583
    invoke-virtual {v9, v2}, LX/6fC;->a(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_b

    .line 1119584
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6fD;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/user/model/User;

    .line 1119585
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->e()Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->e()Z

    move-result v2

    if-eqz v2, :cond_16

    :cond_15
    const/4 v5, 0x1

    .line 1119586
    :goto_c
    new-instance v2, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;ZLcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;ZLcom/facebook/user/model/User;)V

    move-object/from16 p1, v2

    goto/16 :goto_0

    .line 1119587
    :cond_16
    const/4 v5, 0x0

    goto :goto_c

    :cond_17
    move v2, v3

    goto/16 :goto_3
.end method

.method private static a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1119500
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1119501
    :cond_0
    :goto_0
    return v0

    .line 1119502
    :cond_1
    invoke-static {p0}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1119503
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 1119499
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 1119480
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v0, v0

    .line 1119481
    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v3

    .line 1119482
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v0, v0

    .line 1119483
    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v4

    .line 1119484
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1119485
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 1119486
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 1119487
    iget-boolean v5, v0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v5, :cond_1

    iget-boolean v5, v1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v5, :cond_1

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v6, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    move v0, v2

    .line 1119488
    :goto_0
    return v0

    .line 1119489
    :cond_2
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    iget-object v6, v1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    if-eq v5, v6, :cond_3

    move v0, v2

    .line 1119490
    goto :goto_0

    .line 1119491
    :cond_3
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-wide v6, v0, Lcom/facebook/messaging/model/messages/Message;->g:J

    iget-wide v8, v1, Lcom/facebook/messaging/model/messages/Message;->g:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_4

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    iget-object v6, v1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    move v0, v2

    .line 1119492
    goto :goto_0

    .line 1119493
    :cond_5
    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-nez v5, :cond_6

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-eqz v5, :cond_9

    :cond_6
    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 1119494
    if-eqz v5, :cond_7

    move v0, v2

    .line 1119495
    goto :goto_0

    .line 1119496
    :cond_7
    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 1119497
    goto :goto_0

    .line 1119498
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto :goto_0

    :cond_9
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 1

    .prologue
    .line 1119479
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;Z)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 1

    .prologue
    .line 1119478
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;Z)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1119465
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1119466
    :goto_0
    return v0

    .line 1119467
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1119468
    goto :goto_0

    .line 1119469
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1119470
    goto :goto_0

    .line 1119471
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->d()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 1119472
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v5, v0

    .line 1119473
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 1119474
    invoke-static {v0, v4}, LX/6fD;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1119475
    goto :goto_0

    .line 1119476
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1119477
    goto :goto_0
.end method
