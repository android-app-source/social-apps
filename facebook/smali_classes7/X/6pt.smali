.class public final LX/6pt;
.super LX/6nn;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nn",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6pu;


# direct methods
.method public constructor <init>(LX/6pu;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1149712
    iput-object p1, p0, LX/6pt;->d:LX/6pu;

    iput-object p2, p0, LX/6pt;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object p3, p0, LX/6pt;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iput-object p4, p0, LX/6pt;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/6nn;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1149713
    iget-object v0, p0, LX/6pt;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pt;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/auth/pin/EnterPinFragment;Z)V

    .line 1149714
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1149715
    iget-object v0, p0, LX/6pt;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pt;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iget-object v2, p0, LX/6pt;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    .line 1149716
    return-void
.end method
