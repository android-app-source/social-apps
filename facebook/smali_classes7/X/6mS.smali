.class public LX/6mS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final realtimeViewerFbid:Ljava/lang/Long;

.field public final recentUnread:Ljava/lang/Integer;

.field public final seenTimestamp:Ljava/lang/Long;

.field public final unread:Ljava/lang/Integer;

.field public final unseen:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x1

    const/16 v3, 0x8

    .line 1144495
    new-instance v0, LX/1sv;

    const-string v1, "InboxNotification"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mS;->b:LX/1sv;

    .line 1144496
    new-instance v0, LX/1sw;

    const-string v1, "unread"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mS;->c:LX/1sw;

    .line 1144497
    new-instance v0, LX/1sw;

    const-string v1, "unseen"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mS;->d:LX/1sw;

    .line 1144498
    new-instance v0, LX/1sw;

    const-string v1, "seenTimestamp"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mS;->e:LX/1sw;

    .line 1144499
    new-instance v0, LX/1sw;

    const-string v1, "recentUnread"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mS;->f:LX/1sw;

    .line 1144500
    new-instance v0, LX/1sw;

    const-string v1, "realtimeViewerFbid"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mS;->g:LX/1sw;

    .line 1144501
    sput-boolean v4, LX/6mS;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1144488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144489
    iput-object p1, p0, LX/6mS;->unread:Ljava/lang/Integer;

    .line 1144490
    iput-object p2, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    .line 1144491
    iput-object p3, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    .line 1144492
    iput-object p4, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    .line 1144493
    iput-object p5, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    .line 1144494
    return-void
.end method

.method public static b(LX/1su;)LX/6mS;
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/16 v8, 0x8

    const/4 v5, 0x0

    .line 1144465
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v4, v5

    move-object v3, v5

    move-object v2, v5

    move-object v1, v5

    .line 1144466
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1144467
    iget-byte v6, v0, LX/1sw;->b:B

    if-eqz v6, :cond_5

    .line 1144468
    iget-short v6, v0, LX/1sw;->c:S

    packed-switch v6, :pswitch_data_0

    .line 1144469
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1144470
    :pswitch_0
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v8, :cond_0

    .line 1144471
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 1144472
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1144473
    :pswitch_1
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v8, :cond_1

    .line 1144474
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 1144475
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1144476
    :pswitch_2
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v9, :cond_2

    .line 1144477
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1144478
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1144479
    :pswitch_3
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v8, :cond_3

    .line 1144480
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 1144481
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1144482
    :pswitch_4
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v9, :cond_4

    .line 1144483
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 1144484
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1144485
    :cond_5
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1144486
    new-instance v0, LX/6mS;

    invoke-direct/range {v0 .. v5}, LX/6mS;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 1144487
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1144407
    if-eqz p2, :cond_8

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1144408
    :goto_0
    if-eqz p2, :cond_9

    const-string v0, "\n"

    move-object v3, v0

    .line 1144409
    :goto_1
    if-eqz p2, :cond_a

    const-string v0, " "

    .line 1144410
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "InboxNotification"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144411
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144412
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144413
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144414
    const/4 v1, 0x1

    .line 1144415
    iget-object v6, p0, LX/6mS;->unread:Ljava/lang/Integer;

    if-eqz v6, :cond_0

    .line 1144416
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144417
    const-string v1, "unread"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144418
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144419
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144420
    iget-object v1, p0, LX/6mS;->unread:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 1144421
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1144422
    :cond_0
    iget-object v6, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    if-eqz v6, :cond_2

    .line 1144423
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144424
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144425
    const-string v1, "unseen"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144426
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144427
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144428
    iget-object v1, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    if-nez v1, :cond_c

    .line 1144429
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1144430
    :cond_2
    iget-object v6, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    if-eqz v6, :cond_4

    .line 1144431
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144432
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144433
    const-string v1, "seenTimestamp"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144434
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144435
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144436
    iget-object v1, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    if-nez v1, :cond_d

    .line 1144437
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1144438
    :cond_4
    iget-object v6, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    if-eqz v6, :cond_10

    .line 1144439
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144440
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144441
    const-string v1, "recentUnread"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144442
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144443
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144444
    iget-object v1, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    if-nez v1, :cond_e

    .line 1144445
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144446
    :goto_6
    iget-object v1, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 1144447
    if-nez v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144448
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144449
    const-string v1, "realtimeViewerFbid"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144450
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144451
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144452
    iget-object v0, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    if-nez v0, :cond_f

    .line 1144453
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144454
    :cond_7
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144455
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144456
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1144457
    :cond_8
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1144458
    :cond_9
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1144459
    :cond_a
    const-string v0, ""

    goto/16 :goto_2

    .line 1144460
    :cond_b
    iget-object v1, p0, LX/6mS;->unread:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1144461
    :cond_c
    iget-object v1, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1144462
    :cond_d
    iget-object v1, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1144463
    :cond_e
    iget-object v1, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1144464
    :cond_f
    iget-object v0, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_10
    move v2, v1

    goto/16 :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1144383
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1144384
    iget-object v0, p0, LX/6mS;->unread:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1144385
    iget-object v0, p0, LX/6mS;->unread:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1144386
    sget-object v0, LX/6mS;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144387
    iget-object v0, p0, LX/6mS;->unread:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1144388
    :cond_0
    iget-object v0, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1144389
    iget-object v0, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1144390
    sget-object v0, LX/6mS;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144391
    iget-object v0, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1144392
    :cond_1
    iget-object v0, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1144393
    iget-object v0, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1144394
    sget-object v0, LX/6mS;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144395
    iget-object v0, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144396
    :cond_2
    iget-object v0, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1144397
    iget-object v0, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1144398
    sget-object v0, LX/6mS;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144399
    iget-object v0, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1144400
    :cond_3
    iget-object v0, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1144401
    iget-object v0, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1144402
    sget-object v0, LX/6mS;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144403
    iget-object v0, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144404
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1144405
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1144406
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1144340
    if-nez p1, :cond_1

    .line 1144341
    :cond_0
    :goto_0
    return v0

    .line 1144342
    :cond_1
    instance-of v1, p1, LX/6mS;

    if-eqz v1, :cond_0

    .line 1144343
    check-cast p1, LX/6mS;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1144344
    if-nez p1, :cond_3

    .line 1144345
    :cond_2
    :goto_1
    move v0, v2

    .line 1144346
    goto :goto_0

    .line 1144347
    :cond_3
    iget-object v0, p0, LX/6mS;->unread:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1144348
    :goto_2
    iget-object v3, p1, LX/6mS;->unread:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1144349
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1144350
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144351
    iget-object v0, p0, LX/6mS;->unread:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mS;->unread:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144352
    :cond_5
    iget-object v0, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1144353
    :goto_4
    iget-object v3, p1, LX/6mS;->unseen:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1144354
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1144355
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144356
    iget-object v0, p0, LX/6mS;->unseen:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mS;->unseen:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144357
    :cond_7
    iget-object v0, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1144358
    :goto_6
    iget-object v3, p1, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1144359
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1144360
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144361
    iget-object v0, p0, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6mS;->seenTimestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144362
    :cond_9
    iget-object v0, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1144363
    :goto_8
    iget-object v3, p1, LX/6mS;->recentUnread:Ljava/lang/Integer;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1144364
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1144365
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144366
    iget-object v0, p0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mS;->recentUnread:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144367
    :cond_b
    iget-object v0, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1144368
    :goto_a
    iget-object v3, p1, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1144369
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1144370
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144371
    iget-object v0, p0, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    iget-object v3, p1, LX/6mS;->realtimeViewerFbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_d
    move v2, v1

    .line 1144372
    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 1144373
    goto/16 :goto_2

    :cond_f
    move v3, v2

    .line 1144374
    goto/16 :goto_3

    :cond_10
    move v0, v2

    .line 1144375
    goto :goto_4

    :cond_11
    move v3, v2

    .line 1144376
    goto :goto_5

    :cond_12
    move v0, v2

    .line 1144377
    goto :goto_6

    :cond_13
    move v3, v2

    .line 1144378
    goto :goto_7

    :cond_14
    move v0, v2

    .line 1144379
    goto :goto_8

    :cond_15
    move v3, v2

    .line 1144380
    goto :goto_9

    :cond_16
    move v0, v2

    .line 1144381
    goto :goto_a

    :cond_17
    move v3, v2

    .line 1144382
    goto :goto_b
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1144336
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1144337
    sget-boolean v0, LX/6mS;->a:Z

    .line 1144338
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mS;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1144339
    return-object v0
.end method
