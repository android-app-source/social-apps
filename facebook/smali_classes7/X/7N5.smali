.class public LX/7N5;
.super LX/7MM;
.source ""


# instance fields
.field public c:LX/2pM;

.field public d:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

.field public e:LX/3Ig;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199861
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7N5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199862
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199837
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7N5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199838
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1199853
    invoke-direct {p0, p1, p2, p3}, LX/7MM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199854
    new-instance v0, LX/2pM;

    invoke-virtual {p0}, LX/7N5;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2pM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/7N5;->c:LX/2pM;

    .line 1199855
    iget-object v0, p0, LX/7N5;->c:LX/2pM;

    invoke-virtual {p0, v0}, LX/7N5;->addView(Landroid/view/View;)V

    .line 1199856
    new-instance v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, LX/7N5;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/7N5;->d:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    .line 1199857
    iget-object v0, p0, LX/7N5;->d:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0, v0}, LX/7N5;->addView(Landroid/view/View;)V

    .line 1199858
    new-instance v0, LX/3Ig;

    invoke-virtual {p0}, LX/7N5;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3Ig;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/7N5;->e:LX/3Ig;

    .line 1199859
    iget-object v0, p0, LX/7N5;->e:LX/3Ig;

    invoke-virtual {p0, v0}, LX/7N5;->addView(Landroid/view/View;)V

    .line 1199860
    return-void
.end method


# virtual methods
.method public final a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 0

    .prologue
    .line 1199850
    invoke-virtual {p0}, LX/2oy;->im_()V

    .line 1199851
    invoke-virtual {p0, p1, p2, p3}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1199852
    return-void
.end method

.method public final b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 2

    .prologue
    .line 1199839
    iget-object v0, p3, LX/2pa;->b:LX/0P1;

    const-string v1, "ShowGifPlayIconKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, LX/2pa;->b:LX/0P1;

    const-string v1, "ShowGifPlayIconKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1199840
    iget-object v0, p0, LX/7N5;->e:LX/3Ig;

    invoke-virtual {v0, p1, p2, p3}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1199841
    iget-object v0, p0, LX/7N5;->c:LX/2pM;

    invoke-virtual {v0}, LX/2pM;->g()V

    .line 1199842
    iget-object v0, p0, LX/7N5;->c:LX/2pM;

    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 1199843
    iget-object v0, p0, LX/7N5;->d:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 1199844
    :goto_0
    return-void

    .line 1199845
    :cond_0
    iget-object v0, p0, LX/7N5;->c:LX/2pM;

    invoke-virtual {v0, p1, p2, p3}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1199846
    iget-object v0, p0, LX/7N5;->d:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {v0, p1, p2, p3}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1199847
    iget-object v0, p0, LX/7N5;->e:LX/3Ig;

    invoke-virtual {v0}, LX/3Ig;->e()V

    .line 1199848
    iget-object v0, p0, LX/7N5;->e:LX/3Ig;

    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 1199849
    goto :goto_0
.end method
