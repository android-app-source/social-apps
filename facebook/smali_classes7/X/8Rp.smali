.class public LX/8Rp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2c9;

.field private final b:LX/8Sa;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2c9;LX/8Sa;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 2
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsFriendsExceptEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsSpecificFriendsEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsFullCustomPrivacyEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2c9;",
            "LX/8Sa;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1345687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1345688
    iput-object p2, p0, LX/8Rp;->a:LX/2c9;

    .line 1345689
    iput-object p3, p0, LX/8Rp;->b:LX/8Sa;

    .line 1345690
    iput-object p4, p0, LX/8Rp;->c:LX/0Or;

    .line 1345691
    iput-object p5, p0, LX/8Rp;->d:LX/0Or;

    .line 1345692
    iput-object p6, p0, LX/8Rp;->e:LX/0Or;

    .line 1345693
    const v0, 0x7f010288

    const v1, 0x7f0a04c7

    invoke-static {p1, v0, v1}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, LX/8Rp;->f:I

    .line 1345694
    return-void
.end method

.method private static a(LX/8Rp;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I
    .locals 2

    .prologue
    .line 1345695
    sget-object v1, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-static {p1, v1}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    return v0
.end method

.method public static a(Z)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
    .locals 1

    .prologue
    .line 1345696
    if-eqz p0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;
    .locals 3

    .prologue
    .line 1345697
    if-eqz p0, :cond_0

    .line 1345698
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1345699
    if-nez v0, :cond_1

    .line 1345700
    :cond_0
    const/4 v0, 0x0

    .line 1345701
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/4YG;

    invoke-direct {v0}, LX/4YG;-><init>()V

    .line 1345702
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1345703
    iput-object v1, v0, LX/4YG;->b:Ljava/lang/String;

    .line 1345704
    move-object v0, v0

    .line 1345705
    invoke-virtual {p0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    .line 1345706
    iput-object v1, v0, LX/4YG;->c:Ljava/lang/String;

    .line 1345707
    move-object v0, v0

    .line 1345708
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0xe198c7c

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1345709
    iput-object v1, v0, LX/4YG;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1345710
    move-object v0, v0

    .line 1345711
    invoke-virtual {v0}, LX/4YG;->a()Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;
    .locals 3

    .prologue
    .line 1345712
    if-nez p0, :cond_0

    .line 1345713
    const/4 v0, 0x0

    .line 1345714
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4YG;

    invoke-direct {v0}, LX/4YG;-><init>()V

    .line 1345715
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 1345716
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    .line 1345717
    iput-object v1, v0, LX/4YG;->b:Ljava/lang/String;

    .line 1345718
    move-object v0, v0

    .line 1345719
    invoke-virtual {p0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    .line 1345720
    iput-object v1, v0, LX/4YG;->c:Ljava/lang/String;

    .line 1345721
    move-object v0, v0

    .line 1345722
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x285feb

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1345723
    iput-object v1, v0, LX/4YG;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1345724
    move-object v0, v0

    .line 1345725
    invoke-virtual {v0}, LX/4YG;->a()Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;Z)",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;"
        }
    .end annotation

    .prologue
    .line 1345726
    new-instance v0, LX/8QP;

    invoke-direct {v0}, LX/8QP;-><init>()V

    const-string v1, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-virtual {v0, v1}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    .line 1345727
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1345728
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1345729
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345730
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345731
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345732
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->b(Ljava/lang/String;)LX/8QP;

    goto :goto_0

    .line 1345733
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    const-string v4, "friends_except_acquaintances"

    .line 1345734
    iput-object v4, v0, LX/2dc;->f:Ljava/lang/String;

    .line 1345735
    move-object v0, v0

    .line 1345736
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1345737
    new-instance v4, LX/4YK;

    invoke-direct {v4}, LX/4YK;-><init>()V

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1345738
    iput-object v5, v4, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1345739
    move-object v4, v4

    .line 1345740
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1345741
    iput-object v3, v4, LX/4YK;->d:LX/0Px;

    .line 1345742
    move-object v3, v4

    .line 1345743
    invoke-static {p2}, LX/8Rp;->a(Z)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v4

    .line 1345744
    iput-object v4, v3, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1345745
    move-object v3, v3

    .line 1345746
    invoke-virtual {v3}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    .line 1345747
    invoke-static {p0, p1}, LX/8Rp;->a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;

    move-result-object v0

    .line 1345748
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1345749
    invoke-virtual {v0, v1}, LX/8QP;->a(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->b(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->c(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1345750
    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1345751
    const v0, 0x7f0812dd

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1345752
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1345753
    :cond_0
    const v0, 0x7f0812d5

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1345754
    :goto_0
    return-object v0

    .line 1345755
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1345756
    const v1, 0x7f0812ea

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1345757
    :pswitch_0
    const v1, 0x7f0812e7

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1345758
    :pswitch_1
    const v1, 0x7f0812e8

    new-array v2, v5, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1345759
    :pswitch_2
    const v1, 0x7f0812e9

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(LX/1oU;Z)Z
    .locals 2

    .prologue
    .line 1345760
    if-eqz p0, :cond_1

    invoke-static {p0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eq v0, v1, :cond_1

    invoke-static {p0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eq v0, v1, :cond_1

    invoke-static {p0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/res/Resources;)LX/8QN;
    .locals 5

    .prologue
    .line 1345761
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v2, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    .line 1345762
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-static {p0, v1}, LX/8Rp;->a(LX/8Rp;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I

    move-result v1

    .line 1345763
    new-instance v2, LX/8QN;

    const v3, 0x7f0812dd

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/8Rp;->f:I

    invoke-direct {v2, v3, v0, v1, v4}, LX/8QN;-><init>(Ljava/lang/String;III)V

    return-object v2
.end method

.method public static b(LX/0QB;)LX/8Rp;
    .locals 7

    .prologue
    .line 1345764
    new-instance v0, LX/8Rp;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/2c9;->b(LX/0QB;)LX/2c9;

    move-result-object v2

    check-cast v2, LX/2c9;

    invoke-static {p0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v3

    check-cast v3, LX/8Sa;

    const/16 v4, 0x34d

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x353

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x34e

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/8Rp;-><init>(Landroid/content/Context;LX/2c9;LX/8Sa;LX/0Or;LX/0Or;LX/0Or;)V

    .line 1345765
    return-object v0
.end method

.method public static b(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;Z)",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;"
        }
    .end annotation

    .prologue
    .line 1345662
    new-instance v0, LX/8QP;

    invoke-direct {v0}, LX/8QP;-><init>()V

    const-string v1, "{\"value\":\"SELF\"}"

    invoke-virtual {v0, v1}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    .line 1345663
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1345664
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1345665
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345666
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345667
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345668
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->a(Ljava/lang/String;)LX/8QP;

    goto :goto_0

    .line 1345669
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    const-string v4, "custom"

    .line 1345670
    iput-object v4, v0, LX/2dc;->f:Ljava/lang/String;

    .line 1345671
    move-object v0, v0

    .line 1345672
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1345673
    new-instance v4, LX/4YK;

    invoke-direct {v4}, LX/4YK;-><init>()V

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1345674
    iput-object v5, v4, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1345675
    move-object v4, v4

    .line 1345676
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1345677
    iput-object v3, v4, LX/4YK;->b:LX/0Px;

    .line 1345678
    move-object v3, v4

    .line 1345679
    invoke-static {p2}, LX/8Rp;->a(Z)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v4

    .line 1345680
    iput-object v4, v3, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1345681
    move-object v3, v3

    .line 1345682
    invoke-virtual {v3}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    .line 1345683
    invoke-static {p0, p1}, LX/8Rp;->b(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->a(LX/0Px;)LX/8QP;

    move-result-object v0

    .line 1345684
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1345685
    invoke-virtual {v0, v1}, LX/8QP;->b(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->c(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1345686
    return-object v0
.end method

.method public static b(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1345529
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1345530
    :cond_0
    const v0, 0x7f0812d5

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1345531
    :goto_0
    return-object v0

    .line 1345532
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1345533
    const v1, 0x7f0812f7

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1345534
    :pswitch_0
    const v1, 0x7f0812f4

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1345535
    :pswitch_1
    const v1, 0x7f0812f5

    new-array v2, v5, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1345536
    :pswitch_2
    const v1, 0x7f0812f6

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private c(Landroid/content/res/Resources;Ljava/util/List;)LX/8QO;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;)",
            "LX/8QO;"
        }
    .end annotation

    .prologue
    .line 1345524
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT_ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v2, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v1

    .line 1345525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT_ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-static {p0, v0}, LX/8Rp;->a(LX/8Rp;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I

    move-result v2

    .line 1345526
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1345527
    new-instance v0, LX/8QO;

    const v3, 0x7f0812e4

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/8Rp;->f:I

    invoke-direct {v0, v3, v1, v2, v4}, LX/8QO;-><init>(Ljava/lang/String;III)V

    .line 1345528
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/8QO;

    invoke-static {p1, p2}, LX/8Rp;->a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/8Rp;->f:I

    invoke-direct {v0, v3, v1, v2, v4}, LX/8QO;-><init>(Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private d(Landroid/content/res/Resources;Ljava/util/List;)LX/8QX;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;)",
            "LX/8QX;"
        }
    .end annotation

    .prologue
    .line 1345537
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v2, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v1

    .line 1345538
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-static {p0, v0}, LX/8Rp;->a(LX/8Rp;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I

    move-result v2

    .line 1345539
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1345540
    new-instance v0, LX/8QX;

    const v3, 0x7f0812ef

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/8Rp;->f:I

    invoke-direct {v0, v3, v1, v2, v4}, LX/8QX;-><init>(Ljava/lang/String;III)V

    .line 1345541
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/8QX;

    invoke-static {p1, p2}, LX/8Rp;->b(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/8Rp;->f:I

    invoke-direct {v0, v3, v1, v2, v4}, LX/8QX;-><init>(Ljava/lang/String;III)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;Landroid/content/res/Resources;ZZ)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/privacy/model/SelectablePrivacyData;",
            "Landroid/content/res/Resources;",
            "ZZ)",
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1345542
    if-nez p1, :cond_0

    .line 1345543
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345544
    :goto_0
    return-object v0

    .line 1345545
    :cond_0
    iget-object v1, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v1

    .line 1345546
    invoke-virtual {p1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->c()I

    move-result v2

    .line 1345547
    if-nez v1, :cond_1

    .line 1345548
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345549
    goto :goto_0

    .line 1345550
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1345551
    invoke-virtual {p0, v1}, LX/8Rp;->c(LX/1oS;)Z

    move-result v4

    .line 1345552
    invoke-virtual {p0, v1}, LX/8Rp;->d(LX/1oS;)Z

    move-result v5

    .line 1345553
    if-eqz v4, :cond_2

    .line 1345554
    invoke-interface {v1}, LX/1oS;->x_()LX/0Px;

    move-result-object v6

    invoke-direct {p0, p2, v6}, LX/8Rp;->c(Landroid/content/res/Resources;Ljava/util/List;)LX/8QO;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1345555
    :cond_2
    if-eqz v5, :cond_3

    .line 1345556
    invoke-interface {v1}, LX/1oS;->j()LX/0Px;

    move-result-object v6

    invoke-direct {p0, p2, v6}, LX/8Rp;->d(Landroid/content/res/Resources;Ljava/util/List;)LX/8QX;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1345557
    :cond_3
    if-nez v4, :cond_4

    if-nez v5, :cond_4

    .line 1345558
    invoke-virtual {p0, v1, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1345559
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->f()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1345560
    iget-boolean v2, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v2, v2

    .line 1345561
    if-nez v2, :cond_5

    if-nez p3, :cond_6

    .line 1345562
    :cond_5
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345563
    :cond_6
    invoke-interface {v1}, LX/1oS;->l()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, LX/1oS;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_7

    .line 1345564
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v1

    invoke-virtual {p0, v1, p2, p4, v0}, LX/8Rp;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;Landroid/content/res/Resources;ZZ)LX/8QY;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1345565
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345566
    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0Or;Landroid/content/res/Resources;Ljava/util/List;Ljava/util/List;)LX/622;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;)",
            "LX/622",
            "<",
            "LX/8QM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345567
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1345568
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v9

    .line 1345569
    const/4 v5, 0x0

    .line 1345570
    const/4 v4, 0x0

    .line 1345571
    const/4 v3, 0x0

    .line 1345572
    iget-object v10, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    .line 1345573
    const/4 v2, 0x0

    move v6, v2

    :goto_0
    iget-object v2, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v6, v2, :cond_7

    .line 1345574
    iget-object v2, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345575
    if-eqz v10, :cond_0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v10, v7}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_16

    .line 1345576
    :cond_0
    invoke-virtual {p1, v2}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v7

    invoke-virtual {p0, v2, v7}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v7

    .line 1345577
    move-object/from16 v0, p2

    invoke-static {v2, v0}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v11

    if-eqz v11, :cond_1

    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, LX/8Rp;->c(LX/1oS;)Z

    move-result v11

    if-nez v11, :cond_16

    .line 1345578
    :cond_1
    move-object/from16 v0, p2

    invoke-static {v2, v0}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v11

    if-eqz v11, :cond_2

    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, LX/8Rp;->d(LX/1oS;)Z

    move-result v11

    if-nez v11, :cond_16

    .line 1345579
    :cond_2
    move-object/from16 v0, p2

    invoke-static {v2, v0}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v11

    if-eqz v11, :cond_3

    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v11

    if-nez v11, :cond_16

    .line 1345580
    :cond_3
    iget-object v11, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v11}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v11

    if-eqz v11, :cond_5

    move-object v4, v7

    .line 1345581
    :cond_4
    :goto_1
    invoke-static {v2}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v11

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v11, v12, :cond_6

    move-object v5, v7

    .line 1345582
    :goto_2
    invoke-static {v2}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v2, v7, :cond_16

    iget-object v2, p0, LX/8Rp;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-nez v2, :cond_16

    iget-object v2, p0, LX/8Rp;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1345583
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {p0, v0, v1}, LX/8Rp;->c(Landroid/content/res/Resources;Ljava/util/List;)LX/8QO;

    move-result-object v2

    .line 1345584
    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345585
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_16

    .line 1345586
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v5

    move-object v13, v2

    move-object v2, v3

    move-object v3, v13

    .line 1345587
    :goto_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    goto/16 :goto_0

    .line 1345588
    :cond_5
    iget-object v11, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v11}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v11

    if-eqz v11, :cond_4

    move-object v3, v7

    .line 1345589
    goto :goto_1

    .line 1345590
    :cond_6
    invoke-virtual {v8, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345591
    invoke-static {v2}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v11

    invoke-interface {v9, v11, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1345592
    :cond_7
    iget-object v2, p0, LX/8Rp;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1345593
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, LX/8Rp;->b(Landroid/content/res/Resources;)LX/8QN;

    move-result-object v2

    .line 1345594
    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345595
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    .line 1345596
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v2

    .line 1345597
    :cond_8
    iget-object v10, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v2, 0x0

    move v7, v2

    move-object v6, v4

    :goto_4
    if-ge v7, v11, :cond_a

    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345598
    invoke-virtual {p1, v2}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v4

    invoke-virtual {p0, v2, v4}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v4

    .line 1345599
    invoke-virtual {v8, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345600
    iget-object v12, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v12}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v12

    if-eqz v12, :cond_9

    move-object v2, v3

    .line 1345601
    :goto_5
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move-object v6, v4

    move-object v3, v2

    goto :goto_4

    .line 1345602
    :cond_9
    iget-object v12, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v12}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v2

    if-eqz v2, :cond_15

    move-object v2, v4

    move-object v4, v6

    .line 1345603
    goto :goto_5

    .line 1345604
    :cond_a
    iget-object v2, p0, LX/8Rp;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, p0, LX/8Rp;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1345605
    move-object/from16 v0, p4

    move-object/from16 v1, p6

    invoke-direct {p0, v0, v1}, LX/8Rp;->d(Landroid/content/res/Resources;Ljava/util/List;)LX/8QX;

    move-result-object v2

    .line 1345606
    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345607
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    .line 1345608
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->SPECIFIC_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v2

    .line 1345609
    :cond_b
    if-eqz v5, :cond_c

    .line 1345610
    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345611
    :cond_c
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1345612
    const/4 v2, 0x0

    .line 1345613
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1345614
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345615
    :cond_d
    :goto_6
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1345616
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345617
    :cond_e
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    .line 1345618
    if-eqz v4, :cond_f

    .line 1345619
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345620
    :cond_f
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    .line 1345621
    if-eqz v7, :cond_10

    .line 1345622
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v5, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345623
    :cond_10
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->SPECIFIC_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    .line 1345624
    if-eqz v6, :cond_11

    if-nez v4, :cond_11

    if-nez v7, :cond_11

    if-nez v10, :cond_11

    iget-object v4, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v4, v2}, LX/8Rp;->a(LX/1oU;Z)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1345625
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345626
    :cond_11
    if-eqz v3, :cond_12

    iget-object v4, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v4, v2}, LX/8Rp;->a(LX/1oU;Z)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1345627
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345628
    :cond_12
    if-eqz v10, :cond_13

    .line 1345629
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->SPECIFIC_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345630
    :cond_13
    new-instance v2, LX/8Ro;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iget-object v7, p0, LX/8Rp;->a:LX/2c9;

    move-object v3, p0

    move-object/from16 v6, p3

    invoke-direct/range {v2 .. v7}, LX/8Ro;-><init>(LX/8Rp;Ljava/util/List;Ljava/util/List;LX/0Or;LX/2c9;)V

    return-object v2

    .line 1345631
    :cond_14
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1345632
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-interface {v9, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345633
    const/4 v2, 0x1

    goto/16 :goto_6

    :cond_15
    move-object v2, v3

    move-object v4, v6

    goto/16 :goto_5

    :cond_16
    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_3
.end method

.method public final a(LX/1oT;I)LX/8QM;
    .locals 7

    .prologue
    .line 1345634
    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    sget-object v2, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v2

    .line 1345635
    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    invoke-static {p0, v0}, LX/8Rp;->a(LX/8Rp;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I

    move-result v3

    .line 1345636
    invoke-interface {p1}, LX/1oT;->y_()Ljava/lang/String;

    move-result-object v6

    .line 1345637
    new-instance v0, LX/8QM;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v4, p0, LX/8Rp;->f:I

    invoke-interface {p1}, LX/1oT;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, LX/8QM;-><init>(Ljava/lang/Integer;IIILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;Landroid/content/res/Resources;ZZ)LX/8QY;
    .locals 6

    .prologue
    .line 1345638
    new-instance v0, LX/8QY;

    iget v5, p0, LX/8Rp;->f:I

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/8QY;-><init>(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;Landroid/content/res/Resources;ZZI)V

    return-object v0
.end method

.method public final a(LX/1oS;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;
    .locals 6

    .prologue
    .line 1345639
    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    sget-object v2, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v2

    .line 1345640
    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    invoke-static {p0, v0}, LX/8Rp;->a(LX/8Rp;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I

    move-result v3

    .line 1345641
    invoke-interface {p1}, LX/1oS;->j()LX/0Px;

    move-result-object v0

    .line 1345642
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_0

    .line 1345643
    const/4 v0, 0x0

    .line 1345644
    :goto_0
    return-object v0

    .line 1345645
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/2cp;

    .line 1345646
    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    invoke-interface {v1}, LX/2cp;->c()Ljava/lang/String;

    move-result-object v1

    iget v4, p0, LX/8Rp;->f:I

    invoke-interface {p1}, LX/1oS;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    goto :goto_0
.end method

.method public final b(LX/1oS;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1345647
    if-nez p1, :cond_0

    move v0, v1

    .line 1345648
    :goto_0
    return v0

    .line 1345649
    :cond_0
    invoke-static {p1}, LX/2cA;->a(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8Rp;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1345650
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1345651
    goto :goto_0
.end method

.method public final c(LX/1oS;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1345652
    if-nez p1, :cond_0

    move v0, v1

    .line 1345653
    :goto_0
    return v0

    .line 1345654
    :cond_0
    invoke-static {p1}, LX/2cA;->c(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8Rp;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8Rp;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1345655
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1345656
    goto :goto_0
.end method

.method public final d(LX/1oS;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1345657
    if-nez p1, :cond_0

    move v0, v1

    .line 1345658
    :goto_0
    return v0

    .line 1345659
    :cond_0
    invoke-static {p1}, LX/2cA;->e(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8Rp;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8Rp;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1345660
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1345661
    goto :goto_0
.end method
