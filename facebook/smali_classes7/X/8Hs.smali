.class public LX/8Hs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Z

.field private c:F

.field private d:F

.field public e:LX/4mU;

.field private f:LX/8Hr;

.field private g:LX/8Hq;

.field private h:Landroid/os/Handler;

.field private i:Lcom/facebook/photos/galleryutil/visibility/VisibilityAnimator$HideRunnable;

.field public j:LX/EoE;


# direct methods
.method public constructor <init>(Landroid/view/View;JLX/4mV;)V
    .locals 6

    .prologue
    .line 1321603
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    .line 1321604
    return-void
.end method

.method public constructor <init>(Landroid/view/View;JZLX/4mV;)V
    .locals 8

    .prologue
    .line 1321601
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;FF)V

    .line 1321602
    return-void
.end method

.method public constructor <init>(Landroid/view/View;JZLX/4mV;FF)V
    .locals 2

    .prologue
    .line 1321590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321591
    iput-object p1, p0, LX/8Hs;->a:Landroid/view/View;

    .line 1321592
    iput-boolean p4, p0, LX/8Hs;->b:Z

    .line 1321593
    iput p6, p0, LX/8Hs;->c:F

    .line 1321594
    iput p7, p0, LX/8Hs;->d:F

    .line 1321595
    invoke-virtual {p5, p1}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, LX/8Hs;->e:LX/4mU;

    .line 1321596
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    invoke-virtual {v0, p2, p3}, LX/4mU;->a(J)V

    .line 1321597
    new-instance v0, LX/8Hr;

    invoke-direct {v0, p0}, LX/8Hr;-><init>(LX/8Hs;)V

    iput-object v0, p0, LX/8Hs;->f:LX/8Hr;

    .line 1321598
    new-instance v0, LX/8Hq;

    invoke-direct {v0, p0}, LX/8Hq;-><init>(LX/8Hs;)V

    iput-object v0, p0, LX/8Hs;->g:LX/8Hq;

    .line 1321599
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/8Hs;->h:Landroid/os/Handler;

    .line 1321600
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1321586
    iget-object v0, p0, LX/8Hs;->i:Lcom/facebook/photos/galleryutil/visibility/VisibilityAnimator$HideRunnable;

    if-eqz v0, :cond_0

    .line 1321587
    iget-object v0, p0, LX/8Hs;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/8Hs;->i:Lcom/facebook/photos/galleryutil/visibility/VisibilityAnimator$HideRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1321588
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Hs;->i:Lcom/facebook/photos/galleryutil/visibility/VisibilityAnimator$HideRunnable;

    .line 1321589
    :cond_0
    return-void
.end method

.method public static f(LX/8Hs;)V
    .locals 2

    .prologue
    .line 1321581
    iget v0, p0, LX/8Hs;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1321582
    iget-boolean v0, p0, LX/8Hs;->b:Z

    if-eqz v0, :cond_1

    .line 1321583
    iget-object v0, p0, LX/8Hs;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1321584
    :cond_0
    :goto_0
    return-void

    .line 1321585
    :cond_1
    iget-object v0, p0, LX/8Hs;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1321605
    invoke-direct {p0}, LX/8Hs;->e()V

    .line 1321606
    if-eqz p1, :cond_0

    .line 1321607
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    iget-object v1, p0, LX/8Hs;->f:LX/8Hr;

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1321608
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    iget v1, p0, LX/8Hs;->c:F

    invoke-virtual {v0, v1}, LX/4mU;->f(F)V

    .line 1321609
    :goto_0
    iget-object v0, p0, LX/8Hs;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1321610
    return-void

    .line 1321611
    :cond_0
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1321612
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    invoke-virtual {v0}, LX/4mU;->a()V

    .line 1321613
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    iget v1, p0, LX/8Hs;->c:F

    invoke-virtual {v0, v1}, LX/4mU;->e(F)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1321580
    iget-object v0, p0, LX/8Hs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1321575
    const/4 v0, 0x1

    .line 1321576
    invoke-virtual {p0}, LX/8Hs;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1321577
    invoke-virtual {p0, v0}, LX/8Hs;->b(Z)V

    .line 1321578
    :goto_0
    return-void

    .line 1321579
    :cond_0
    invoke-virtual {p0, v0}, LX/8Hs;->a(Z)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1321568
    invoke-direct {p0}, LX/8Hs;->e()V

    .line 1321569
    if-eqz p1, :cond_0

    .line 1321570
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    iget-object v1, p0, LX/8Hs;->g:LX/8Hq;

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1321571
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    iget v1, p0, LX/8Hs;->d:F

    invoke-virtual {v0, v1}, LX/4mU;->f(F)V

    .line 1321572
    :goto_0
    return-void

    .line 1321573
    :cond_0
    iget-object v0, p0, LX/8Hs;->e:LX/4mU;

    iget v1, p0, LX/8Hs;->d:F

    invoke-virtual {v0, v1}, LX/4mU;->e(F)V

    .line 1321574
    invoke-static {p0}, LX/8Hs;->f(LX/8Hs;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1321566
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/8Hs;->a(Z)V

    .line 1321567
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1321564
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/8Hs;->b(Z)V

    .line 1321565
    return-void
.end method
