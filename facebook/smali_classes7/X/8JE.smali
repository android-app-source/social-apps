.class public final LX/8JE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:F

.field public final synthetic c:Lcom/facebook/photos/tagging/shared/BubbleLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/BubbleLayout;FF)V
    .locals 0

    .prologue
    .line 1327939
    iput-object p1, p0, LX/8JE;->c:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    iput p2, p0, LX/8JE;->a:F

    iput p3, p0, LX/8JE;->b:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 1327940
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1327941
    iget-object v1, p0, LX/8JE;->c:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    iget v2, p0, LX/8JE;->a:F

    iget v3, p0, LX/8JE;->b:F

    iget v4, p0, LX/8JE;->a:F

    sub-float/2addr v3, v4

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    .line 1327942
    invoke-static {v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a$redex0(Lcom/facebook/photos/tagging/shared/BubbleLayout;F)V

    .line 1327943
    return-void
.end method
