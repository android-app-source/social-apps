.class public final LX/7Qa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/7Qb;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/157;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:LX/0gM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0gM",
            "<",
            "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateSubscriptionModel;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;


# direct methods
.method public constructor <init>(LX/7Qb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1204568
    iput-object p1, p0, LX/7Qa;->a:LX/7Qb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204569
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7Qa;->d:Ljava/util/ArrayList;

    .line 1204570
    iput-object p2, p0, LX/7Qa;->b:Ljava/lang/String;

    .line 1204571
    iput-object p3, p0, LX/7Qa;->c:Ljava/lang/String;

    .line 1204572
    return-void
.end method

.method private static c(LX/7Qa;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/157;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1204573
    iget-object v0, p0, LX/7Qa;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1204574
    iget-object v0, p0, LX/7Qa;->e:LX/0gM;

    if-eqz v0, :cond_0

    .line 1204575
    iget-object v0, p0, LX/7Qa;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nv;

    iget-object v1, p0, LX/7Qa;->e:LX/0gM;

    .line 1204576
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1204577
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1204578
    iget-object v3, v0, LX/2nv;->b:LX/0gX;

    invoke-virtual {v3, v2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1204579
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Qa;->e:LX/0gM;

    .line 1204580
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(LX/157;)V
    .locals 4

    .prologue
    .line 1204581
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/7Qa;->c(LX/7Qa;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1204582
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1204583
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1204584
    :goto_1
    monitor-exit p0

    return-void

    .line 1204585
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1204586
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/7Qa;->d:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1204587
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 5
    .param p1    # Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1204588
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/7Qa;->c(LX/7Qa;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1204589
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1204590
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/157;

    .line 1204591
    if-eqz v0, :cond_0

    .line 1204592
    if-nez p1, :cond_1

    .line 1204593
    iget-object v4, p0, LX/7Qa;->b:Ljava/lang/String;

    invoke-interface {v0, v4}, LX/157;->a(Ljava/lang/String;)V

    .line 1204594
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1204595
    :cond_1
    iget-object v4, p0, LX/7Qa;->b:Ljava/lang/String;

    invoke-interface {v0, v4, p1, p2}, LX/157;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1204596
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1204597
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 1204598
    iget-object v0, p0, LX/7Qa;->e:LX/0gM;

    if-nez v0, :cond_1

    .line 1204599
    iget-object v0, p0, LX/7Qa;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nv;

    iget-object v1, p0, LX/7Qa;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7Qa;->a:LX/7Qb;

    iget-object v3, p0, LX/7Qa;->b:Ljava/lang/String;

    .line 1204600
    new-instance v4, LX/7QX;

    invoke-direct {v4, v2, v3}, LX/7QX;-><init>(LX/7Qb;Ljava/lang/String;)V

    move-object v2, v4

    .line 1204601
    new-instance v3, LX/4Gk;

    invoke-direct {v3}, LX/4Gk;-><init>()V

    invoke-virtual {v3, v1}, LX/4Gk;->a(Ljava/lang/String;)LX/4Gk;

    move-result-object v3

    .line 1204602
    new-instance v4, LX/7RE;

    invoke-direct {v4}, LX/7RE;-><init>()V

    move-object v4, v4

    .line 1204603
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1204604
    iget-object v3, v0, LX/2nv;->c:LX/19j;

    iget-boolean v3, v3, LX/19j;->S:Z

    if-eqz v3, :cond_0

    .line 1204605
    const-string v3, "scrubbing"

    const-string v5, "MPEG_DASH"

    invoke-virtual {v4, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1204606
    const-string v3, "enable_read_only_viewer_count"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1204607
    :cond_0
    move-object v4, v4

    .line 1204608
    const/4 v3, 0x0

    .line 1204609
    :try_start_0
    iget-object v5, v0, LX/2nv;->b:LX/0gX;

    invoke-virtual {v5, v4, v2}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1204610
    :goto_0
    move-object v0, v3

    .line 1204611
    iput-object v0, p0, LX/7Qa;->e:LX/0gM;

    .line 1204612
    :cond_1
    return-void

    .line 1204613
    :catch_0
    move-exception v4

    .line 1204614
    sget-object v5, LX/2nv;->a:Ljava/lang/String;

    const-string v6, "Live video broadcast status update subscription failed. %s"

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final declared-synchronized b(LX/157;)V
    .locals 2

    .prologue
    .line 1204615
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7Qa;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1204616
    iget-object v0, p0, LX/7Qa;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1204617
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1204618
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1204619
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1204620
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1204621
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1204622
    :cond_1
    monitor-exit p0

    return-void
.end method
