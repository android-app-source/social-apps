.class public LX/82P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1dp;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/1X9;",
            "LX/1dq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const v1, 0x106000d

    .line 1288179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1288180
    new-instance v0, LX/1dq;

    invoke-direct {v0, v1, v1, v1, v1}, LX/1dq;-><init>(IIII)V

    .line 1288181
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, LX/1X9;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    .line 1288182
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->TOP:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288183
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->DIVIDER_TOP:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288184
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288185
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288186
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->MIDDLE:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288187
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->BOX:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288188
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->BOTTOM:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288189
    iget-object v1, p0, LX/82P;->a:Ljava/util/EnumMap;

    sget-object v2, LX/1X9;->FOLLOW_UP:LX/1X9;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288190
    return-void
.end method

.method public static a(LX/0QB;)LX/82P;
    .locals 3

    .prologue
    .line 1288168
    const-class v1, LX/82P;

    monitor-enter v1

    .line 1288169
    :try_start_0
    sget-object v0, LX/82P;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1288170
    sput-object v2, LX/82P;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1288171
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1288172
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1288173
    new-instance v0, LX/82P;

    invoke-direct {v0}, LX/82P;-><init>()V

    .line 1288174
    move-object v0, v0

    .line 1288175
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1288176
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/82P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1288177
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1288178
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;LX/1X9;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1288167
    iget-object v0, p0, LX/82P;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dq;

    invoke-virtual {v0, p1, p3, p4}, LX/1dq;->a(Landroid/content/res/Resources;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
