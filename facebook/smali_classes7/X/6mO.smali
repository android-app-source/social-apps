.class public LX/6mO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final caller_context:Ljava/lang/String;

.field public final queryId:Ljava/lang/Long;

.field public final queryParams:Ljava/lang/String;

.field public final requestId:Ljava/lang/Integer;

.field public final strip_default:Ljava/lang/Boolean;

.field public final strip_nulls:Ljava/lang/Boolean;

.field public final viewer_context_user_id:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0xb

    const/16 v5, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 1144057
    new-instance v0, LX/1sv;

    const-string v1, "GraphQLRequest"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mO;->b:LX/1sv;

    .line 1144058
    new-instance v0, LX/1sw;

    const-string v1, "requestId"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mO;->c:LX/1sw;

    .line 1144059
    new-instance v0, LX/1sw;

    const-string v1, "queryId"

    invoke-direct {v0, v1, v5, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mO;->d:LX/1sw;

    .line 1144060
    new-instance v0, LX/1sw;

    const-string v1, "queryParams"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mO;->e:LX/1sw;

    .line 1144061
    new-instance v0, LX/1sw;

    const-string v1, "strip_nulls"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mO;->f:LX/1sw;

    .line 1144062
    new-instance v0, LX/1sw;

    const-string v1, "strip_default"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mO;->g:LX/1sw;

    .line 1144063
    new-instance v0, LX/1sw;

    const-string v1, "viewer_context_user_id"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mO;->h:LX/1sw;

    .line 1144064
    new-instance v0, LX/1sw;

    const-string v1, "caller_context"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mO;->i:LX/1sw;

    .line 1144065
    sput-boolean v4, LX/6mO;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1144066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144067
    iput-object p1, p0, LX/6mO;->requestId:Ljava/lang/Integer;

    .line 1144068
    iput-object p2, p0, LX/6mO;->queryId:Ljava/lang/Long;

    .line 1144069
    iput-object p3, p0, LX/6mO;->queryParams:Ljava/lang/String;

    .line 1144070
    iput-object p4, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    .line 1144071
    iput-object p5, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    .line 1144072
    iput-object p6, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    .line 1144073
    iput-object p7, p0, LX/6mO;->caller_context:Ljava/lang/String;

    .line 1144074
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1144075
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1144076
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v1, v0

    .line 1144077
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 1144078
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GraphQLRequest"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144079
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144080
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144081
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144082
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144083
    const-string v4, "requestId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144084
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144085
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144086
    iget-object v4, p0, LX/6mO;->requestId:Ljava/lang/Integer;

    if-nez v4, :cond_7

    .line 1144087
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144088
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144089
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144090
    const-string v4, "queryId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144091
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144092
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144093
    iget-object v4, p0, LX/6mO;->queryId:Ljava/lang/Long;

    if-nez v4, :cond_8

    .line 1144094
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144095
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144096
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144097
    const-string v4, "queryParams"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144098
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144099
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144100
    iget-object v4, p0, LX/6mO;->queryParams:Ljava/lang/String;

    if-nez v4, :cond_9

    .line 1144101
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144102
    :goto_5
    iget-object v4, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    .line 1144103
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144104
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144105
    const-string v4, "strip_nulls"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144106
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144107
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144108
    iget-object v4, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    if-nez v4, :cond_a

    .line 1144109
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144110
    :cond_0
    :goto_6
    iget-object v4, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    if-eqz v4, :cond_1

    .line 1144111
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144112
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144113
    const-string v4, "strip_default"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144114
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144115
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144116
    iget-object v4, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    if-nez v4, :cond_b

    .line 1144117
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144118
    :cond_1
    :goto_7
    iget-object v4, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    if-eqz v4, :cond_2

    .line 1144119
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144120
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144121
    const-string v4, "viewer_context_user_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144122
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144123
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144124
    iget-object v4, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    if-nez v4, :cond_c

    .line 1144125
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144126
    :cond_2
    :goto_8
    iget-object v4, p0, LX/6mO;->caller_context:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 1144127
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144128
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144129
    const-string v4, "caller_context"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144130
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144131
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144132
    iget-object v0, p0, LX/6mO;->caller_context:Ljava/lang/String;

    if-nez v0, :cond_d

    .line 1144133
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144134
    :cond_3
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144135
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144136
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1144137
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1144138
    :cond_5
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1144139
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 1144140
    :cond_7
    iget-object v4, p0, LX/6mO;->requestId:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1144141
    :cond_8
    iget-object v4, p0, LX/6mO;->queryId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1144142
    :cond_9
    iget-object v4, p0, LX/6mO;->queryParams:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1144143
    :cond_a
    iget-object v4, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1144144
    :cond_b
    iget-object v4, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1144145
    :cond_c
    iget-object v4, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1144146
    :cond_d
    iget-object v0, p0, LX/6mO;->caller_context:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1144147
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1144148
    iget-object v0, p0, LX/6mO;->requestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1144149
    sget-object v0, LX/6mO;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144150
    iget-object v0, p0, LX/6mO;->requestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1144151
    :cond_0
    iget-object v0, p0, LX/6mO;->queryId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1144152
    sget-object v0, LX/6mO;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144153
    iget-object v0, p0, LX/6mO;->queryId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144154
    :cond_1
    iget-object v0, p0, LX/6mO;->queryParams:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1144155
    sget-object v0, LX/6mO;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144156
    iget-object v0, p0, LX/6mO;->queryParams:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1144157
    :cond_2
    iget-object v0, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1144158
    iget-object v0, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1144159
    sget-object v0, LX/6mO;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144160
    iget-object v0, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1144161
    :cond_3
    iget-object v0, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1144162
    iget-object v0, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1144163
    sget-object v0, LX/6mO;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144164
    iget-object v0, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1144165
    :cond_4
    iget-object v0, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1144166
    iget-object v0, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1144167
    sget-object v0, LX/6mO;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144168
    iget-object v0, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144169
    :cond_5
    iget-object v0, p0, LX/6mO;->caller_context:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1144170
    iget-object v0, p0, LX/6mO;->caller_context:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1144171
    sget-object v0, LX/6mO;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144172
    iget-object v0, p0, LX/6mO;->caller_context:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1144173
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1144174
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1144175
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1144176
    if-nez p1, :cond_1

    .line 1144177
    :cond_0
    :goto_0
    return v0

    .line 1144178
    :cond_1
    instance-of v1, p1, LX/6mO;

    if-eqz v1, :cond_0

    .line 1144179
    check-cast p1, LX/6mO;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1144180
    if-nez p1, :cond_3

    .line 1144181
    :cond_2
    :goto_1
    move v0, v2

    .line 1144182
    goto :goto_0

    .line 1144183
    :cond_3
    iget-object v0, p0, LX/6mO;->requestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1144184
    :goto_2
    iget-object v3, p1, LX/6mO;->requestId:Ljava/lang/Integer;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1144185
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1144186
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144187
    iget-object v0, p0, LX/6mO;->requestId:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mO;->requestId:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144188
    :cond_5
    iget-object v0, p0, LX/6mO;->queryId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1144189
    :goto_4
    iget-object v3, p1, LX/6mO;->queryId:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1144190
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1144191
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144192
    iget-object v0, p0, LX/6mO;->queryId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mO;->queryId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144193
    :cond_7
    iget-object v0, p0, LX/6mO;->queryParams:Ljava/lang/String;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1144194
    :goto_6
    iget-object v3, p1, LX/6mO;->queryParams:Ljava/lang/String;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1144195
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1144196
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144197
    iget-object v0, p0, LX/6mO;->queryParams:Ljava/lang/String;

    iget-object v3, p1, LX/6mO;->queryParams:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144198
    :cond_9
    iget-object v0, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1144199
    :goto_8
    iget-object v3, p1, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1144200
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1144201
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144202
    iget-object v0, p0, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mO;->strip_nulls:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144203
    :cond_b
    iget-object v0, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1144204
    :goto_a
    iget-object v3, p1, LX/6mO;->strip_default:Ljava/lang/Boolean;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1144205
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1144206
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144207
    iget-object v0, p0, LX/6mO;->strip_default:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mO;->strip_default:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144208
    :cond_d
    iget-object v0, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1144209
    :goto_c
    iget-object v3, p1, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1144210
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1144211
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144212
    iget-object v0, p0, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    iget-object v3, p1, LX/6mO;->viewer_context_user_id:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144213
    :cond_f
    iget-object v0, p0, LX/6mO;->caller_context:Ljava/lang/String;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1144214
    :goto_e
    iget-object v3, p1, LX/6mO;->caller_context:Ljava/lang/String;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1144215
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1144216
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144217
    iget-object v0, p0, LX/6mO;->caller_context:Ljava/lang/String;

    iget-object v3, p1, LX/6mO;->caller_context:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 1144218
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1144219
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 1144220
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1144221
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 1144222
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1144223
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 1144224
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1144225
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 1144226
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 1144227
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 1144228
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 1144229
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 1144230
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 1144231
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 1144232
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1144233
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1144234
    sget-boolean v0, LX/6mO;->a:Z

    .line 1144235
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mO;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1144236
    return-object v0
.end method
