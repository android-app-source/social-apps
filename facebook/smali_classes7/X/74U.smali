.class public final enum LX/74U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74U;

.field public static final enum FACEBOX:LX/74U;

.field public static final enum FULLSCREEN_TYPEAHEAD:LX/74U;

.field public static final enum PRESS_AND_HOLD:LX/74U;

.field public static final enum TAP_ANYWHERE:LX/74U;

.field public static final enum UNSET:LX/74U;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1168091
    new-instance v0, LX/74U;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v2}, LX/74U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74U;->UNSET:LX/74U;

    .line 1168092
    new-instance v0, LX/74U;

    const-string v1, "FULLSCREEN_TYPEAHEAD"

    invoke-direct {v0, v1, v3}, LX/74U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74U;->FULLSCREEN_TYPEAHEAD:LX/74U;

    .line 1168093
    new-instance v0, LX/74U;

    const-string v1, "TAP_ANYWHERE"

    invoke-direct {v0, v1, v4}, LX/74U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74U;->TAP_ANYWHERE:LX/74U;

    .line 1168094
    new-instance v0, LX/74U;

    const-string v1, "FACEBOX"

    invoke-direct {v0, v1, v5}, LX/74U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74U;->FACEBOX:LX/74U;

    .line 1168095
    new-instance v0, LX/74U;

    const-string v1, "PRESS_AND_HOLD"

    invoke-direct {v0, v1, v6}, LX/74U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74U;->PRESS_AND_HOLD:LX/74U;

    .line 1168096
    const/4 v0, 0x5

    new-array v0, v0, [LX/74U;

    sget-object v1, LX/74U;->UNSET:LX/74U;

    aput-object v1, v0, v2

    sget-object v1, LX/74U;->FULLSCREEN_TYPEAHEAD:LX/74U;

    aput-object v1, v0, v3

    sget-object v1, LX/74U;->TAP_ANYWHERE:LX/74U;

    aput-object v1, v0, v4

    sget-object v1, LX/74U;->FACEBOX:LX/74U;

    aput-object v1, v0, v5

    sget-object v1, LX/74U;->PRESS_AND_HOLD:LX/74U;

    aput-object v1, v0, v6

    sput-object v0, LX/74U;->$VALUES:[LX/74U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1168097
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74U;
    .locals 1

    .prologue
    .line 1168098
    const-class v0, LX/74U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74U;

    return-object v0
.end method

.method public static values()[LX/74U;
    .locals 1

    .prologue
    .line 1168099
    sget-object v0, LX/74U;->$VALUES:[LX/74U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74U;

    return-object v0
.end method
