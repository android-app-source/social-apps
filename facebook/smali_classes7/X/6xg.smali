.class public final enum LX/6xg;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xg;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xg;

.field public static final enum INVOICE:LX/6xg;

.field public static final enum MOR_DONATIONS:LX/6xg;

.field public static final enum MOR_DUMMY_THIRD_PARTY:LX/6xg;

.field public static final enum MOR_EVENT_TICKETING:LX/6xg;

.field public static final enum MOR_MESSENGER_COMMERCE:LX/6xg;

.field public static final enum MOR_NONE:LX/6xg;

.field public static final enum MOR_P2P_TRANSFER:LX/6xg;

.field public static final enum NMOR_BUSINESS_PLATFORM_COMMERCE:LX/6xg;

.field public static final enum NMOR_INSTANT_EXPERIENCES:LX/6xg;

.field public static final enum NMOR_MESSENGER_OMNIM:LX/6xg;

.field public static final enum NMOR_MESSENGER_PLATFORM:LX/6xg;

.field public static final enum NMOR_PAGES_COMMERCE:LX/6xg;

.field public static final enum NMOR_SYNCHRONOUS_COMPONENT_FLOW:LX/6xg;

.field public static final enum NMOR_TIP_JAR:LX/6xg;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1159080
    new-instance v0, LX/6xg;

    const-string v1, "INVOICE"

    const-string v2, "ads_invoice"

    invoke-direct {v0, v1, v4, v2}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->INVOICE:LX/6xg;

    .line 1159081
    new-instance v0, LX/6xg;

    const-string v1, "MOR_DUMMY_THIRD_PARTY"

    const-string v2, "mor_dummy_third_party"

    invoke-direct {v0, v1, v5, v2}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->MOR_DUMMY_THIRD_PARTY:LX/6xg;

    .line 1159082
    new-instance v0, LX/6xg;

    const-string v1, "MOR_NONE"

    const-string v2, "mor_none"

    invoke-direct {v0, v1, v6, v2}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->MOR_NONE:LX/6xg;

    .line 1159083
    new-instance v0, LX/6xg;

    const-string v1, "MOR_EVENT_TICKETING"

    const-string v2, "mor_event_ticketing"

    invoke-direct {v0, v1, v7, v2}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->MOR_EVENT_TICKETING:LX/6xg;

    .line 1159084
    new-instance v0, LX/6xg;

    const-string v1, "MOR_MESSENGER_COMMERCE"

    const-string v2, "mor_messenger_commerce"

    invoke-direct {v0, v1, v8, v2}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->MOR_MESSENGER_COMMERCE:LX/6xg;

    .line 1159085
    new-instance v0, LX/6xg;

    const-string v1, "MOR_P2P_TRANSFER"

    const/4 v2, 0x5

    const-string v3, "mor_p2p_transfer"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->MOR_P2P_TRANSFER:LX/6xg;

    .line 1159086
    new-instance v0, LX/6xg;

    const-string v1, "NMOR_BUSINESS_PLATFORM_COMMERCE"

    const/4 v2, 0x6

    const-string v3, "nmor_business_platform_commerce"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->NMOR_BUSINESS_PLATFORM_COMMERCE:LX/6xg;

    .line 1159087
    new-instance v0, LX/6xg;

    const-string v1, "NMOR_MESSENGER_PLATFORM"

    const/4 v2, 0x7

    const-string v3, "nmor_messenger_platform"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->NMOR_MESSENGER_PLATFORM:LX/6xg;

    .line 1159088
    new-instance v0, LX/6xg;

    const-string v1, "NMOR_MESSENGER_OMNIM"

    const/16 v2, 0x8

    const-string v3, "nmor_messenger_omnim"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->NMOR_MESSENGER_OMNIM:LX/6xg;

    .line 1159089
    new-instance v0, LX/6xg;

    const-string v1, "NMOR_PAGES_COMMERCE"

    const/16 v2, 0x9

    const-string v3, "nmor_pages_commerce"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->NMOR_PAGES_COMMERCE:LX/6xg;

    .line 1159090
    new-instance v0, LX/6xg;

    const-string v1, "NMOR_SYNCHRONOUS_COMPONENT_FLOW"

    const/16 v2, 0xa

    const-string v3, "nmor_synchronous_component_flow"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->NMOR_SYNCHRONOUS_COMPONENT_FLOW:LX/6xg;

    .line 1159091
    new-instance v0, LX/6xg;

    const-string v1, "NMOR_TIP_JAR"

    const/16 v2, 0xb

    const-string v3, "nmor_tip_jar"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->NMOR_TIP_JAR:LX/6xg;

    .line 1159092
    new-instance v0, LX/6xg;

    const-string v1, "MOR_DONATIONS"

    const/16 v2, 0xc

    const-string v3, "mor_donations"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->MOR_DONATIONS:LX/6xg;

    .line 1159093
    new-instance v0, LX/6xg;

    const-string v1, "NMOR_INSTANT_EXPERIENCES"

    const/16 v2, 0xd

    const-string v3, "nmor_instant_experiences"

    invoke-direct {v0, v1, v2, v3}, LX/6xg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xg;->NMOR_INSTANT_EXPERIENCES:LX/6xg;

    .line 1159094
    const/16 v0, 0xe

    new-array v0, v0, [LX/6xg;

    sget-object v1, LX/6xg;->INVOICE:LX/6xg;

    aput-object v1, v0, v4

    sget-object v1, LX/6xg;->MOR_DUMMY_THIRD_PARTY:LX/6xg;

    aput-object v1, v0, v5

    sget-object v1, LX/6xg;->MOR_NONE:LX/6xg;

    aput-object v1, v0, v6

    sget-object v1, LX/6xg;->MOR_EVENT_TICKETING:LX/6xg;

    aput-object v1, v0, v7

    sget-object v1, LX/6xg;->MOR_MESSENGER_COMMERCE:LX/6xg;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6xg;->MOR_P2P_TRANSFER:LX/6xg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6xg;->NMOR_BUSINESS_PLATFORM_COMMERCE:LX/6xg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6xg;->NMOR_MESSENGER_PLATFORM:LX/6xg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6xg;->NMOR_MESSENGER_OMNIM:LX/6xg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6xg;->NMOR_PAGES_COMMERCE:LX/6xg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6xg;->NMOR_SYNCHRONOUS_COMPONENT_FLOW:LX/6xg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6xg;->NMOR_TIP_JAR:LX/6xg;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6xg;->MOR_DONATIONS:LX/6xg;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6xg;->NMOR_INSTANT_EXPERIENCES:LX/6xg;

    aput-object v2, v0, v1

    sput-object v0, LX/6xg;->$VALUES:[LX/6xg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1159095
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1159096
    iput-object p3, p0, LX/6xg;->mValue:Ljava/lang/String;

    .line 1159097
    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6xg;
    .locals 1

    .prologue
    .line 1159098
    invoke-static {}, LX/6xg;->values()[LX/6xg;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xg;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xg;
    .locals 1

    .prologue
    .line 1159099
    const-class v0, LX/6xg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    return-object v0
.end method

.method public static values()[LX/6xg;
    .locals 1

    .prologue
    .line 1159100
    sget-object v0, LX/6xg;->$VALUES:[LX/6xg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xg;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1159101
    invoke-virtual {p0}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1159102
    iget-object v0, p0, LX/6xg;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public final toPaymentModulesClient()LX/6xh;
    .locals 3

    .prologue
    .line 1159103
    sget-object v0, LX/6xf;->a:[I

    invoke-virtual {p0}, LX/6xg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1159104
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not defined for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1159105
    :pswitch_0
    sget-object v0, LX/6xh;->MOCK:LX/6xh;

    .line 1159106
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/6xh;->PAGES_COMMERCE:LX/6xh;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
