.class public LX/6vM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6v3;


# instance fields
.field private final a:LX/6vS;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/6vS;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156921
    iput-object p1, p0, LX/6vM;->a:LX/6vS;

    .line 1156922
    iput-object p2, p0, LX/6vM;->b:Landroid/content/res/Resources;

    .line 1156923
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156911
    invoke-virtual {p0}, LX/6vM;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156912
    invoke-virtual {p0}, LX/6vM;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1156913
    iget-object v0, p0, LX/6vM;->b:Landroid/content/res/Resources;

    const v1, 0x7f081e2a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156914
    const-string v0, ""

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156915
    iget-object v0, p0, LX/6vM;->a:LX/6vS;

    invoke-virtual {v0}, LX/6vS;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156916
    iget-object v0, p0, LX/6vM;->a:LX/6vS;

    invoke-virtual {v0}, LX/6vS;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156917
    iget-object v0, p0, LX/6vM;->a:LX/6vS;

    invoke-virtual {v0}, LX/6vS;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156918
    iget-object v0, p0, LX/6vM;->a:LX/6vS;

    invoke-virtual {v0}, LX/6vS;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156919
    iget-object v0, p0, LX/6vM;->a:LX/6vS;

    invoke-virtual {v0}, LX/6vS;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
