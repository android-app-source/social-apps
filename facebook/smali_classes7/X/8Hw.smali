.class public final enum LX/8Hw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Hw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Hw;

.field public static final enum APPLYING:LX/8Hw;

.field public static final enum CLOSING:LX/8Hw;

.field public static final enum NONE:LX/8Hw;

.field public static final enum PREPROCESSING:LX/8Hw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1321776
    new-instance v0, LX/8Hw;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/8Hw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Hw;->NONE:LX/8Hw;

    .line 1321777
    new-instance v0, LX/8Hw;

    const-string v1, "PREPROCESSING"

    invoke-direct {v0, v1, v3}, LX/8Hw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Hw;->PREPROCESSING:LX/8Hw;

    .line 1321778
    new-instance v0, LX/8Hw;

    const-string v1, "APPLYING"

    invoke-direct {v0, v1, v4}, LX/8Hw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Hw;->APPLYING:LX/8Hw;

    .line 1321779
    new-instance v0, LX/8Hw;

    const-string v1, "CLOSING"

    invoke-direct {v0, v1, v5}, LX/8Hw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Hw;->CLOSING:LX/8Hw;

    .line 1321780
    const/4 v0, 0x4

    new-array v0, v0, [LX/8Hw;

    sget-object v1, LX/8Hw;->NONE:LX/8Hw;

    aput-object v1, v0, v2

    sget-object v1, LX/8Hw;->PREPROCESSING:LX/8Hw;

    aput-object v1, v0, v3

    sget-object v1, LX/8Hw;->APPLYING:LX/8Hw;

    aput-object v1, v0, v4

    sget-object v1, LX/8Hw;->CLOSING:LX/8Hw;

    aput-object v1, v0, v5

    sput-object v0, LX/8Hw;->$VALUES:[LX/8Hw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1321781
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Hw;
    .locals 1

    .prologue
    .line 1321782
    const-class v0, LX/8Hw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Hw;

    return-object v0
.end method

.method public static values()[LX/8Hw;
    .locals 1

    .prologue
    .line 1321783
    sget-object v0, LX/8Hw;->$VALUES:[LX/8Hw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Hw;

    return-object v0
.end method
