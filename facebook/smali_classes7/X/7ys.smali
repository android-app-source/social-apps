.class public final LX/7ys;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public b:Z

.field public final c:I

.field public d:LX/7yr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1280408
    sget-object v0, LX/7yr;->SHA256:LX/7yr;

    invoke-virtual {v0}, LX/7yr;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v1, v0}, LX/7ys;-><init>(ZILjava/lang/String;)V

    .line 1280409
    return-void
.end method

.method public constructor <init>(ZILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1280400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280401
    iput-boolean p1, p0, LX/7ys;->b:Z

    .line 1280402
    iput p2, p0, LX/7ys;->c:I

    .line 1280403
    :try_start_0
    invoke-static {p3}, LX/7yr;->valueOf(Ljava/lang/String;)LX/7yr;

    move-result-object v0

    iput-object v0, p0, LX/7ys;->d:LX/7yr;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1280404
    :goto_0
    const/4 v0, 0x1

    iput v0, p0, LX/7ys;->a:I

    .line 1280405
    return-void

    .line 1280406
    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/7ys;->d:LX/7yr;

    .line 1280407
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7ys;->b:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 1280398
    iput-boolean p1, p0, LX/7ys;->b:Z

    .line 1280399
    return-void
.end method
