.class public final enum LX/6qt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6qt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6qt;

.field public static final enum CHECKOUT_LOADER:LX/6qt;

.field public static final enum PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/6qt;

.field public static final enum PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/6qt;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1151151
    new-instance v0, LX/6qt;

    const-string v1, "CHECKOUT_LOADER"

    invoke-direct {v0, v1, v2}, LX/6qt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qt;->CHECKOUT_LOADER:LX/6qt;

    .line 1151152
    new-instance v0, LX/6qt;

    const-string v1, "PAYMENTS_COMPONENT_WITH_UI_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/6qt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qt;->PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/6qt;

    .line 1151153
    new-instance v0, LX/6qt;

    const-string v1, "PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS"

    invoke-direct {v0, v1, v4}, LX/6qt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qt;->PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/6qt;

    .line 1151154
    const/4 v0, 0x3

    new-array v0, v0, [LX/6qt;

    sget-object v1, LX/6qt;->CHECKOUT_LOADER:LX/6qt;

    aput-object v1, v0, v2

    sget-object v1, LX/6qt;->PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/6qt;

    aput-object v1, v0, v3

    sget-object v1, LX/6qt;->PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/6qt;

    aput-object v1, v0, v4

    sput-object v0, LX/6qt;->$VALUES:[LX/6qt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1151150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6qt;
    .locals 1

    .prologue
    .line 1151148
    const-class v0, LX/6qt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6qt;

    return-object v0
.end method

.method public static values()[LX/6qt;
    .locals 1

    .prologue
    .line 1151149
    sget-object v0, LX/6qt;->$VALUES:[LX/6qt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6qt;

    return-object v0
.end method
