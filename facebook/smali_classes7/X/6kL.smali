.class public LX/6kL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;"
        }
    .end annotation
.end field

.field public final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final messageMetadata:LX/6kn;

.field public final ttl:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1136350
    new-instance v0, LX/1sv;

    const-string v1, "DeltaSentMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kL;->b:LX/1sv;

    .line 1136351
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kL;->c:LX/1sw;

    .line 1136352
    new-instance v0, LX/1sw;

    const-string v1, "attachments"

    const/16 v2, 0xf

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kL;->d:LX/1sw;

    .line 1136353
    new-instance v0, LX/1sw;

    const-string v1, "ttl"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kL;->e:LX/1sw;

    .line 1136354
    new-instance v0, LX/1sw;

    const-string v1, "data"

    const/16 v2, 0xd

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kL;->f:LX/1sw;

    .line 1136355
    sput-boolean v4, LX/6kL;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/util/List;Ljava/lang/Integer;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kn;",
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1136344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136345
    iput-object p1, p0, LX/6kL;->messageMetadata:LX/6kn;

    .line 1136346
    iput-object p2, p0, LX/6kL;->attachments:Ljava/util/List;

    .line 1136347
    iput-object p3, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    .line 1136348
    iput-object p4, p0, LX/6kL;->data:Ljava/util/Map;

    .line 1136349
    return-void
.end method

.method public static a(LX/6kL;)V
    .locals 4

    .prologue
    .line 1136339
    iget-object v0, p0, LX/6kL;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1136340
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kL;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136341
    :cond_0
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6kk;->a:LX/1sn;

    iget-object v1, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1136342
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'ttl\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1136343
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136286
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1136287
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1136288
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    move-object v1, v0

    .line 1136289
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaSentMessage"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136290
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136291
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136292
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136293
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136294
    const-string v0, "messageMetadata"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136295
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136296
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136297
    iget-object v0, p0, LX/6kL;->messageMetadata:LX/6kn;

    if-nez v0, :cond_6

    .line 1136298
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136299
    :goto_3
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1136300
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136301
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136302
    const-string v0, "attachments"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136303
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136304
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136305
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    if-nez v0, :cond_7

    .line 1136306
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136307
    :cond_0
    :goto_4
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1136308
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136309
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136310
    const-string v0, "ttl"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136311
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136312
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136313
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    if-nez v0, :cond_8

    .line 1136314
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136315
    :cond_1
    :goto_5
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 1136316
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136317
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136318
    const-string v0, "data"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136319
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136320
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136321
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    if-nez v0, :cond_a

    .line 1136322
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136323
    :cond_2
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136324
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136325
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136326
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1136327
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1136328
    :cond_5
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1136329
    :cond_6
    iget-object v0, p0, LX/6kL;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1136330
    :cond_7
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1136331
    :cond_8
    sget-object v0, LX/6kk;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1136332
    if-eqz v0, :cond_9

    .line 1136333
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136334
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136335
    :cond_9
    iget-object v5, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1136336
    if-eqz v0, :cond_1

    .line 1136337
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1136338
    :cond_a
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    const/16 v3, 0xb

    .line 1136261
    invoke-static {p0}, LX/6kL;->a(LX/6kL;)V

    .line 1136262
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136263
    iget-object v0, p0, LX/6kL;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1136264
    sget-object v0, LX/6kL;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136265
    iget-object v0, p0, LX/6kL;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1136266
    :cond_0
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1136267
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1136268
    sget-object v0, LX/6kL;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136269
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6kL;->attachments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1136270
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jZ;

    .line 1136271
    invoke-virtual {v0, p1}, LX/6jZ;->a(LX/1su;)V

    goto :goto_0

    .line 1136272
    :cond_1
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1136273
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1136274
    sget-object v0, LX/6kL;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136275
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1136276
    :cond_2
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 1136277
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 1136278
    sget-object v0, LX/6kL;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136279
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6kL;->data:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v3, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1136280
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1136281
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1136282
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1136283
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136284
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136285
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136225
    if-nez p1, :cond_1

    .line 1136226
    :cond_0
    :goto_0
    return v0

    .line 1136227
    :cond_1
    instance-of v1, p1, LX/6kL;

    if-eqz v1, :cond_0

    .line 1136228
    check-cast p1, LX/6kL;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136229
    if-nez p1, :cond_3

    .line 1136230
    :cond_2
    :goto_1
    move v0, v2

    .line 1136231
    goto :goto_0

    .line 1136232
    :cond_3
    iget-object v0, p0, LX/6kL;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1136233
    :goto_2
    iget-object v3, p1, LX/6kL;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1136234
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136235
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136236
    iget-object v0, p0, LX/6kL;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kL;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136237
    :cond_5
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1136238
    :goto_4
    iget-object v3, p1, LX/6kL;->attachments:Ljava/util/List;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1136239
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136240
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136241
    iget-object v0, p0, LX/6kL;->attachments:Ljava/util/List;

    iget-object v3, p1, LX/6kL;->attachments:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136242
    :cond_7
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1136243
    :goto_6
    iget-object v3, p1, LX/6kL;->ttl:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1136244
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1136245
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136246
    iget-object v0, p0, LX/6kL;->ttl:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kL;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136247
    :cond_9
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1136248
    :goto_8
    iget-object v3, p1, LX/6kL;->data:Ljava/util/Map;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1136249
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1136250
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136251
    iget-object v0, p0, LX/6kL;->data:Ljava/util/Map;

    iget-object v3, p1, LX/6kL;->data:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1136252
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1136253
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1136254
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1136255
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1136256
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1136257
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1136258
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1136259
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1136260
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136224
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136221
    sget-boolean v0, LX/6kL;->a:Z

    .line 1136222
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kL;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136223
    return-object v0
.end method
