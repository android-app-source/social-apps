.class public LX/6wa;
.super LX/6tx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6tx",
        "<",
        "Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157817
    const-class v0, Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;

    invoke-direct {p0, p1, v0}, LX/6tx;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1157818
    return-void
.end method

.method public static b(LX/0QB;)LX/6wa;
    .locals 2

    .prologue
    .line 1157819
    new-instance v1, LX/6wa;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/6wa;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1157820
    return-object v1
.end method


# virtual methods
.method public final a(LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1157821
    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1157822
    const-string v1, "viewer"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1157823
    const-string v2, "pay_account"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1157824
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1157825
    const-string p0, "emails"

    invoke-static {v1, p0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1157826
    invoke-static {}, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->newBuilder()LX/6vd;

    move-result-object p1

    const-string v0, "id"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1157827
    iput-object v0, p1, LX/6vd;->a:Ljava/lang/String;

    .line 1157828
    move-object p1, p1

    .line 1157829
    const-string v0, "is_default"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    .line 1157830
    iput-boolean v0, p1, LX/6vd;->c:Z

    .line 1157831
    move-object p1, p1

    .line 1157832
    const-string v0, "normalized_email_address"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1157833
    iput-object v0, p1, LX/6vd;->b:Ljava/lang/String;

    .line 1157834
    move-object p1, p1

    .line 1157835
    invoke-virtual {p1}, LX/6vd;->d()Lcom/facebook/payments/contactinfo/model/EmailContactInfo;

    move-result-object p1

    move-object v1, p1

    .line 1157836
    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1157837
    :cond_0
    new-instance v1, Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;-><init>(LX/0Px;)V

    move-object v0, v1

    .line 1157838
    return-object v0
.end method

.method public final b()LX/14N;
    .locals 4

    .prologue
    .line 1157839
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1157840
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() {pay_account {emails {id, is_default, normalized_email_address}}}"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157841
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "get_email_contact_info"

    .line 1157842
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1157843
    move-object v1, v1

    .line 1157844
    const-string v2, "GET"

    .line 1157845
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1157846
    move-object v1, v1

    .line 1157847
    const-string v2, "graphql"

    .line 1157848
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1157849
    move-object v1, v1

    .line 1157850
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1157851
    move-object v0, v1

    .line 1157852
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1157853
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1157854
    move-object v0, v0

    .line 1157855
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157856
    const-string v0, "get_email_contact_info"

    return-object v0
.end method
