.class public LX/7Tz;
.super LX/7Ty;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1211225
    const-class v0, LX/7Tz;

    sput-object v0, LX/7Tz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/4dF;LX/1FJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4dF;",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1211226
    invoke-direct {p0, p1}, LX/7Ty;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 1211227
    invoke-virtual {p2}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/7Tz;->c:LX/1FJ;

    .line 1211228
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 1211229
    invoke-virtual {p0}, LX/7Ty;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1211230
    :goto_0
    return-void

    .line 1211231
    :cond_0
    iget-object v0, p0, LX/7Tz;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1211232
    invoke-virtual {p0}, LX/7Ty;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211233
    sget-object v0, LX/7Tz;->a:Ljava/lang/Class;

    const-string v1, "draw: Drawable %x already closed. Underlying closeable ref = %x"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/7Tz;->c:LX/1FJ;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1211234
    :goto_0
    return-void

    .line 1211235
    :cond_0
    invoke-super {p0, p1}, LX/7Ty;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final finalize()V
    .locals 5

    .prologue
    .line 1211236
    invoke-virtual {p0}, LX/7Ty;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211237
    :goto_0
    return-void

    .line 1211238
    :cond_0
    sget-object v0, LX/7Tz;->a:Ljava/lang/Class;

    const-string v1, "finalize: Drawable %x still open. Underlying closeable ref = %x, bitmap = %x"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/7Tz;->c:LX/1FJ;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/7Tz;->c:LX/1FJ;

    invoke-virtual {v4}, LX/1FJ;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1211239
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, LX/7Tz;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1211240
    invoke-virtual {p0}, LX/7Tz;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1211241
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
