.class public final LX/6xH;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/payments/form/model/FormFieldAttributes;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/payments/form/model/FormFieldAttributes;

.field public d:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1158613
    new-instance v0, LX/6xI;

    invoke-direct {v0}, LX/6xI;-><init>()V

    .line 1158614
    sget-object v0, LX/6xN;->PRICE:LX/6xN;

    const-string v1, ""

    sget-object v2, LX/6xe;->REQUIRED:LX/6xe;

    sget-object v3, LX/6xO;->PRICE_NO_DECIMALS:LX/6xO;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v0

    invoke-virtual {v0}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    move-object v0, v0

    .line 1158615
    sput-object v0, LX/6xH;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1158616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158617
    const-string v0, ""

    iput-object v0, p0, LX/6xH;->b:Ljava/lang/String;

    .line 1158618
    sget-object v0, LX/6xH;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    iput-object v0, p0, LX/6xH;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1158619
    return-void
.end method
