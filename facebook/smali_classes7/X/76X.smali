.class public LX/76X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/76X;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/quickpromotion/customrender/CustomRenderType;",
            "LX/76W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/76W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171077
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/76X;->a:Ljava/util/Map;

    .line 1171078
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/76W;

    .line 1171079
    iget-object v2, p0, LX/76X;->a:Ljava/util/Map;

    invoke-interface {v0}, LX/76W;->a()Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1171080
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/76X;
    .locals 6

    .prologue
    .line 1171081
    sget-object v0, LX/76X;->b:LX/76X;

    if-nez v0, :cond_1

    .line 1171082
    const-class v1, LX/76X;

    monitor-enter v1

    .line 1171083
    :try_start_0
    sget-object v0, LX/76X;->b:LX/76X;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171084
    if-eqz v2, :cond_0

    .line 1171085
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171086
    new-instance v3, LX/76X;

    .line 1171087
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/76Y;

    invoke-direct {p0, v0}, LX/76Y;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1171088
    invoke-direct {v3, v4}, LX/76X;-><init>(Ljava/util/Set;)V

    .line 1171089
    move-object v0, v3

    .line 1171090
    sput-object v0, LX/76X;->b:LX/76X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171091
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171092
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171093
    :cond_1
    sget-object v0, LX/76X;->b:LX/76X;

    return-object v0

    .line 1171094
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
