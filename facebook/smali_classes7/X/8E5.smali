.class public final enum LX/8E5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8E5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8E5;

.field public static final enum CAN_WAIT:LX/8E5;

.field public static final enum INTERACTIVE:LX/8E5;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1313426
    new-instance v0, LX/8E5;

    const-string v1, "INTERACTIVE"

    invoke-direct {v0, v1, v2}, LX/8E5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E5;->INTERACTIVE:LX/8E5;

    .line 1313427
    new-instance v0, LX/8E5;

    const-string v1, "CAN_WAIT"

    invoke-direct {v0, v1, v3}, LX/8E5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E5;->CAN_WAIT:LX/8E5;

    .line 1313428
    const/4 v0, 0x2

    new-array v0, v0, [LX/8E5;

    sget-object v1, LX/8E5;->INTERACTIVE:LX/8E5;

    aput-object v1, v0, v2

    sget-object v1, LX/8E5;->CAN_WAIT:LX/8E5;

    aput-object v1, v0, v3

    sput-object v0, LX/8E5;->$VALUES:[LX/8E5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1313429
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8E5;
    .locals 1

    .prologue
    .line 1313430
    const-class v0, LX/8E5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8E5;

    return-object v0
.end method

.method public static values()[LX/8E5;
    .locals 1

    .prologue
    .line 1313431
    sget-object v0, LX/8E5;->$VALUES:[LX/8E5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8E5;

    return-object v0
.end method
