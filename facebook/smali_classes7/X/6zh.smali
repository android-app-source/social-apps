.class public final LX/6zh;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6zj;

.field public final synthetic b:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

.field public final synthetic c:LX/6zi;


# direct methods
.method public constructor <init>(LX/6zi;LX/6zj;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1161342
    iput-object p1, p0, LX/6zh;->c:LX/6zi;

    iput-object p2, p0, LX/6zh;->a:LX/6zj;

    iput-object p3, p0, LX/6zh;->b:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1161343
    iget-object v0, p0, LX/6zh;->c:LX/6zi;

    iget-object v0, v0, LX/6zi;->f:LX/70k;

    new-instance v1, LX/6zg;

    invoke-direct {v1, p0}, LX/6zg;-><init>(LX/6zh;)V

    invoke-virtual {v0, v1}, LX/70k;->a(LX/1DI;)V

    .line 1161344
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 1161345
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2Oo;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 1161346
    :goto_0
    iget-object v1, p0, LX/6zh;->c:LX/6zi;

    iget-object v1, v1, LX/6zi;->b:LX/03V;

    sget-object v2, LX/6zi;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Get Payment Methods Info for the logged-in user failed. "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1161347
    return-void

    .line 1161348
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1161349
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161350
    iget-object v0, p0, LX/6zh;->c:LX/6zi;

    iget-object v0, v0, LX/6zi;->f:LX/70k;

    invoke-virtual {v0}, LX/70k;->b()V

    .line 1161351
    iget-object v0, p0, LX/6zh;->a:LX/6zj;

    .line 1161352
    new-instance v1, LX/704;

    invoke-direct {v1}, LX/704;-><init>()V

    move-object v1, v1

    .line 1161353
    iput-object p1, v1, LX/704;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161354
    move-object v1, v1

    .line 1161355
    iget-object v2, p0, LX/6zh;->b:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    .line 1161356
    iget-object p0, v2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object p0, p0

    .line 1161357
    if-eqz p0, :cond_0

    .line 1161358
    iget-object p0, v2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object p0, p0

    .line 1161359
    check-cast p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    .line 1161360
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->b:Lcom/facebook/payments/picker/model/ProductCoreClientData;

    move-object p0, v2

    .line 1161361
    :goto_0
    move-object v2, p0

    .line 1161362
    iput-object v2, v1, LX/704;->b:Lcom/facebook/payments/picker/model/ProductCoreClientData;

    .line 1161363
    move-object v1, v1

    .line 1161364
    new-instance v2, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    invoke-direct {v2, v1}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;-><init>(LX/704;)V

    move-object v1, v2

    .line 1161365
    invoke-interface {v0, v1}, LX/6zj;->a(Lcom/facebook/payments/picker/model/CoreClientData;)V

    .line 1161366
    return-void

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
