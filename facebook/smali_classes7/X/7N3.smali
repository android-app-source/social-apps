.class public abstract LX/7N3;
.super LX/7MM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/7MM",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public c:Z

.field public d:LX/7MP;

.field public e:LX/2pI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/7I7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final o:LX/3ID;

.field private final p:Landroid/view/GestureDetector;

.field public final q:LX/7O7;

.field public r:Z

.field public s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199823
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199824
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199821
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199822
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1199805
    invoke-direct {p0, p1, p2, p3}, LX/7MM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199806
    new-instance v0, LX/3ID;

    invoke-direct {v0}, LX/3ID;-><init>()V

    iput-object v0, p0, LX/7N3;->o:LX/3ID;

    .line 1199807
    new-instance v0, LX/7O7;

    invoke-direct {v0, p0}, LX/7O7;-><init>(LX/7N3;)V

    iput-object v0, p0, LX/7N3;->q:LX/7O7;

    .line 1199808
    sget-object v0, LX/7MP;->AUTO:LX/7MP;

    iput-object v0, p0, LX/7N3;->d:LX/7MP;

    .line 1199809
    iput-boolean v2, p0, LX/7N3;->r:Z

    .line 1199810
    const/16 v0, 0xbb8

    iput v0, p0, LX/7N3;->s:I

    .line 1199811
    const-class v0, LX/7N3;

    invoke-static {v0, p0}, LX/7N3;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1199812
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7OB;

    invoke-direct {v1, p0}, LX/7OB;-><init>(LX/7N3;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199813
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7O8;

    invoke-direct {v1, p0}, LX/7O8;-><init>(LX/7N3;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199814
    invoke-virtual {p0}, LX/7N3;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1199815
    iget-object v0, p0, LX/7N3;->o:LX/3ID;

    invoke-virtual {p0}, LX/7N3;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 1199816
    iput v1, v0, LX/3ID;->a:I

    .line 1199817
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/7O9;

    invoke-direct {v1, p0}, LX/7O9;-><init>(LX/7N3;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/7N3;->p:Landroid/view/GestureDetector;

    .line 1199818
    new-instance v0, LX/7O6;

    invoke-direct {v0, p0}, LX/7O6;-><init>(LX/7N3;)V

    move-object v0, v0

    .line 1199819
    invoke-virtual {p0, v0}, LX/7N3;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 1199820
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/7N3;

    invoke-static {v2}, LX/2pI;->a(LX/0QB;)LX/2pI;

    move-result-object v1

    check-cast v1, LX/2pI;

    new-instance v0, LX/7I7;

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    invoke-direct {v0, p0}, LX/7I7;-><init>(LX/0Uh;)V

    move-object v2, v0

    check-cast v2, LX/7I7;

    iput-object v1, p1, LX/7N3;->e:LX/2pI;

    iput-object v2, p1, LX/7N3;->f:LX/7I7;

    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 1199802
    iget-object v0, p0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->AUTO:LX/7MP;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7MP;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1199803
    return-void

    .line 1199804
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1199799
    if-eqz p2, :cond_0

    .line 1199800
    sget-object v0, LX/7OA;->AUTO_WITH_INITIALLY_HIDDEN:LX/7OA;

    invoke-virtual {p0, v0}, LX/7N3;->a(LX/7OA;)V

    .line 1199801
    :cond_0
    return-void
.end method

.method public final a(LX/7OA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1199792
    iput-boolean v1, p0, LX/7N3;->r:Z

    .line 1199793
    invoke-virtual {p1}, LX/7OA;->isInitiallyVisible()Z

    move-result v0

    iput-boolean v0, p0, LX/7N3;->c:Z

    .line 1199794
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-eqz v0, :cond_0

    .line 1199795
    invoke-virtual {p0, v1}, LX/7N3;->d(I)V

    .line 1199796
    :goto_0
    invoke-virtual {p1}, LX/7OA;->getBehavior()LX/7MP;

    move-result-object v0

    iput-object v0, p0, LX/7N3;->d:LX/7MP;

    .line 1199797
    return-void

    .line 1199798
    :cond_0
    invoke-virtual {p0, v1}, LX/7N3;->c(I)V

    goto :goto_0
.end method

.method public c(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1199786
    iput-boolean v1, p0, LX/7N3;->c:Z

    .line 1199787
    iget-object v0, p0, LX/7N3;->e:LX/2pI;

    invoke-virtual {v0, p0, p1, v1}, LX/2pI;->a(Landroid/view/View;II)V

    .line 1199788
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1199789
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2pX;

    sget-object v2, LX/2pR;->DEFAULT:LX/2pR;

    invoke-direct {v1, v2}, LX/2pX;-><init>(LX/2pR;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1199790
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2ov;

    iget-boolean v2, p0, LX/7N3;->c:Z

    invoke-direct {v1, v2}, LX/2ov;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1199791
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1199784
    iget-object v0, p0, LX/7N3;->q:LX/7O7;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/7O7;->removeMessages(I)V

    .line 1199785
    return-void
.end method

.method public d(I)V
    .locals 7

    .prologue
    .line 1199777
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7N3;->c:Z

    .line 1199778
    iget-object v0, p0, LX/7N3;->e:LX/2pI;

    .line 1199779
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-long v5, p1

    invoke-virtual {v3, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, v0, LX/2pI;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, LX/7LS;

    invoke-direct {v4, v0, p0}, LX/7LS;-><init>(LX/2pI;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1199780
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1199781
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2pX;

    sget-object v2, LX/2pR;->HIDE:LX/2pR;

    invoke-direct {v1, v2}, LX/2pX;-><init>(LX/2pR;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1199782
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2ov;

    iget-boolean v2, p0, LX/7N3;->c:Z

    invoke-direct {v1, v2}, LX/2ov;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1199783
    :cond_0
    return-void
.end method

.method public dH_()V
    .locals 2

    .prologue
    .line 1199825
    invoke-super {p0}, LX/7MM;->dH_()V

    .line 1199826
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1199827
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 1199828
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1199829
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1199830
    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->AUTO:LX/7MP;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/7N3;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1199831
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/7N3;->d(I)V

    .line 1199832
    invoke-virtual {p0}, LX/7N3;->k()V

    .line 1199833
    :cond_0
    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1199774
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-nez v0, :cond_0

    .line 1199775
    const/4 v0, 0x1

    .line 1199776
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/7MM;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1199768
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 1199769
    :cond_0
    :goto_0
    invoke-super {p0, p1}, LX/7MM;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 1199770
    :pswitch_0
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-eqz v0, :cond_0

    .line 1199771
    invoke-virtual {p0}, LX/7N3;->k()V

    goto :goto_0

    .line 1199772
    :pswitch_1
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-eqz v0, :cond_0

    .line 1199773
    invoke-virtual {p0}, LX/7N3;->j()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public g()V
    .locals 2

    .prologue
    .line 1199757
    invoke-direct {p0}, LX/7N3;->w()V

    .line 1199758
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 1199759
    :cond_0
    :goto_0
    return-void

    .line 1199760
    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1199761
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1199762
    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-eq v0, v1, :cond_0

    .line 1199763
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-eqz v0, :cond_2

    .line 1199764
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, LX/7N3;->c(I)V

    goto :goto_0

    .line 1199765
    :cond_2
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7MP;

    if-eq v0, v1, :cond_3

    .line 1199766
    invoke-virtual {p0}, LX/7N3;->h()V

    goto :goto_0

    .line 1199767
    :cond_3
    invoke-virtual {p0}, LX/7N3;->i()V

    goto :goto_0
.end method

.method public abstract getContentView()I
.end method

.method public h()V
    .locals 1

    .prologue
    .line 1199753
    invoke-direct {p0}, LX/7N3;->w()V

    .line 1199754
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, LX/7N3;->d(I)V

    .line 1199755
    invoke-virtual {p0}, LX/7N3;->j()V

    .line 1199756
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1199749
    invoke-direct {p0}, LX/7N3;->w()V

    .line 1199750
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, LX/7N3;->d(I)V

    .line 1199751
    iget-object v0, p0, LX/7N3;->q:LX/7O7;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/7O7;->removeMessages(I)V

    .line 1199752
    return-void
.end method

.method public final j()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1199745
    invoke-direct {p0}, LX/7N3;->w()V

    .line 1199746
    iget-object v0, p0, LX/7N3;->q:LX/7O7;

    invoke-virtual {v0, v4}, LX/7O7;->removeMessages(I)V

    .line 1199747
    iget-object v0, p0, LX/7N3;->q:LX/7O7;

    iget v1, p0, LX/7N3;->s:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, LX/7O7;->sendEmptyMessageDelayed(IJ)Z

    .line 1199748
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 1199743
    iget-object v0, p0, LX/7N3;->q:LX/7O7;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/7O7;->removeMessages(I)V

    .line 1199744
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1199740
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-nez v0, :cond_0

    .line 1199741
    const/4 v0, 0x1

    .line 1199742
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/7MM;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, 0x147aa7

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1199737
    iget-object v1, p0, LX/7N3;->o:LX/3ID;

    invoke-virtual {v1, p1}, LX/3ID;->a(Landroid/view/MotionEvent;)Z

    .line 1199738
    iget-object v1, p0, LX/7N3;->p:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1199739
    const v1, 0x32e780ac

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1199736
    const/4 v0, 0x1

    return v0
.end method
