.class public final enum LX/7gX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gX;

.field public static final enum ATTEMPT_TO_SEND_REPLY:LX/7gX;

.field public static final enum OPEN_MEDIA:LX/7gX;

.field public static final enum OPEN_SEEN_SUMMARY:LX/7gX;

.field public static final enum OPEN_STORY:LX/7gX;

.field public static final enum STORY_PROFILE_IMPRESSION:LX/7gX;

.field public static final enum STORY_TRAY_IMPRESSION:LX/7gX;

.field public static final enum STORY_TRAY_REFRESH:LX/7gX;


# instance fields
.field private final mModuleName:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1224468
    new-instance v0, LX/7gX;

    const-string v1, "STORY_TRAY_IMPRESSION"

    const-string v2, "story_tray_impression"

    const-string v3, "snacks_actions"

    invoke-direct {v0, v1, v5, v2, v3}, LX/7gX;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gX;->STORY_TRAY_IMPRESSION:LX/7gX;

    .line 1224469
    new-instance v0, LX/7gX;

    const-string v1, "STORY_TRAY_REFRESH"

    const-string v2, "refresh_story_tray"

    const-string v3, "snacks_actions"

    invoke-direct {v0, v1, v6, v2, v3}, LX/7gX;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gX;->STORY_TRAY_REFRESH:LX/7gX;

    .line 1224470
    new-instance v0, LX/7gX;

    const-string v1, "STORY_PROFILE_IMPRESSION"

    const-string v2, "story_profile_impression"

    const-string v3, "snacks_actions"

    invoke-direct {v0, v1, v7, v2, v3}, LX/7gX;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gX;->STORY_PROFILE_IMPRESSION:LX/7gX;

    .line 1224471
    new-instance v0, LX/7gX;

    const-string v1, "OPEN_STORY"

    const-string v2, "open_story"

    const-string v3, "snacks_actions"

    invoke-direct {v0, v1, v8, v2, v3}, LX/7gX;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gX;->OPEN_STORY:LX/7gX;

    .line 1224472
    new-instance v0, LX/7gX;

    const-string v1, "OPEN_MEDIA"

    const-string v2, "open_media"

    const-string v3, "snacks_actions"

    invoke-direct {v0, v1, v9, v2, v3}, LX/7gX;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gX;->OPEN_MEDIA:LX/7gX;

    .line 1224473
    new-instance v0, LX/7gX;

    const-string v1, "ATTEMPT_TO_SEND_REPLY"

    const/4 v2, 0x5

    const-string v3, "attempt_to_send_reply"

    const-string v4, "snacks_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gX;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gX;->ATTEMPT_TO_SEND_REPLY:LX/7gX;

    .line 1224474
    new-instance v0, LX/7gX;

    const-string v1, "OPEN_SEEN_SUMMARY"

    const/4 v2, 0x6

    const-string v3, "open_seen_summary"

    const-string v4, "snacks_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gX;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gX;->OPEN_SEEN_SUMMARY:LX/7gX;

    .line 1224475
    const/4 v0, 0x7

    new-array v0, v0, [LX/7gX;

    sget-object v1, LX/7gX;->STORY_TRAY_IMPRESSION:LX/7gX;

    aput-object v1, v0, v5

    sget-object v1, LX/7gX;->STORY_TRAY_REFRESH:LX/7gX;

    aput-object v1, v0, v6

    sget-object v1, LX/7gX;->STORY_PROFILE_IMPRESSION:LX/7gX;

    aput-object v1, v0, v7

    sget-object v1, LX/7gX;->OPEN_STORY:LX/7gX;

    aput-object v1, v0, v8

    sget-object v1, LX/7gX;->OPEN_MEDIA:LX/7gX;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/7gX;->ATTEMPT_TO_SEND_REPLY:LX/7gX;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7gX;->OPEN_SEEN_SUMMARY:LX/7gX;

    aput-object v2, v0, v1

    sput-object v0, LX/7gX;->$VALUES:[LX/7gX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224464
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224465
    iput-object p3, p0, LX/7gX;->mName:Ljava/lang/String;

    .line 1224466
    iput-object p4, p0, LX/7gX;->mModuleName:Ljava/lang/String;

    .line 1224467
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gX;
    .locals 1

    .prologue
    .line 1224460
    const-class v0, LX/7gX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gX;

    return-object v0
.end method

.method public static values()[LX/7gX;
    .locals 1

    .prologue
    .line 1224463
    sget-object v0, LX/7gX;->$VALUES:[LX/7gX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gX;

    return-object v0
.end method


# virtual methods
.method public final getModuleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224462
    iget-object v0, p0, LX/7gX;->mModuleName:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224461
    iget-object v0, p0, LX/7gX;->mName:Ljava/lang/String;

    return-object v0
.end method
