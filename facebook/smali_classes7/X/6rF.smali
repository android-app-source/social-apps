.class public final LX/6rF;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

.field public final synthetic b:LX/6rI;


# direct methods
.method public constructor <init>(LX/6rI;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 0

    .prologue
    .line 1151927
    iput-object p1, p0, LX/6rF;->b:LX/6rI;

    iput-object p2, p0, LX/6rF;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1151913
    iget-object v0, p0, LX/6rF;->b:LX/6rI;

    iget-object v1, p0, LX/6rF;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1151914
    iget-object v2, v0, LX/6rI;->d:LX/6Ex;

    invoke-interface {v2, p1}, LX/6Ex;->a(Ljava/lang/Throwable;)V

    .line 1151915
    iget-boolean v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->u:Z

    if-eqz v2, :cond_0

    .line 1151916
    :goto_0
    return-void

    .line 1151917
    :cond_0
    iget-object v2, v0, LX/6rI;->a:Landroid/content/Context;

    iget-object v3, v0, LX/6rI;->a:Landroid/content/Context;

    .line 1151918
    const-class v4, LX/2Oo;

    invoke-static {p1, v4}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v4

    check-cast v4, LX/2Oo;

    .line 1151919
    if-nez v4, :cond_1

    .line 1151920
    const v4, 0x7f08003e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1151921
    :goto_1
    move-object v3, v4

    .line 1151922
    iget-boolean v4, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    iget-object p0, v0, LX/6rI;->f:LX/6qh;

    invoke-static {v2, v3, v4, p0}, LX/6rI;->a(Landroid/content/Context;Ljava/lang/String;ZLX/6qh;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1151923
    check-cast p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;

    .line 1151924
    new-instance v0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;

    iget-object v1, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->b:LX/0lF;

    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;-><init>(Ljava/lang/String;LX/0lF;)V

    .line 1151925
    iget-object v1, p0, LX/6rF;->b:LX/6rI;

    iget-object v1, v1, LX/6rI;->d:LX/6Ex;

    invoke-interface {v1, v0}, LX/6Ex;->a(Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V

    .line 1151926
    return-void
.end method
