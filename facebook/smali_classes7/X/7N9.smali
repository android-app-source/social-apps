.class public LX/7N9;
.super LX/7Mr;
.source ""


# instance fields
.field private final a:Lcom/facebook/resources/ui/FbTextView;

.field private final b:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Landroid/widget/SeekBar;

.field private p:LX/Cag;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199977
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7N9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199978
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199975
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7N9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199976
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1199967
    invoke-direct {p0, p1, p2, p3}, LX/7Mr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199968
    const v0, 0x7f0d1329

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7N9;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1199969
    const v0, 0x7f0d132b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7N9;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1199970
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, LX/7N9;->o:Landroid/widget/SeekBar;

    .line 1199971
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/7N9;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1199972
    invoke-virtual {p0, v0}, LX/7N9;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1199973
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1199974
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2oi;)V
    .locals 3

    .prologue
    .line 1199957
    invoke-super {p0, p1}, LX/7Mr;->a(LX/2oi;)V

    .line 1199958
    iget-object v0, p0, LX/7N9;->p:LX/Cag;

    if-eqz v0, :cond_0

    .line 1199959
    iget-object v0, p0, LX/7N9;->p:LX/Cag;

    .line 1199960
    iget-object v1, v0, LX/Cag;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1199961
    iget-object v2, v1, LX/CcX;->a:LX/CaP;

    move-object v1, v2

    .line 1199962
    if-eqz v1, :cond_0

    .line 1199963
    iget-object v1, v0, LX/Cag;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1199964
    iget-object v2, v1, LX/CcX;->a:LX/CaP;

    move-object v1, v2

    .line 1199965
    iget-object v2, v0, LX/Cag;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-boolean v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->l:Z

    invoke-virtual {v1, v2, p1}, LX/CaP;->a(ZLX/2oi;)V

    .line 1199966
    :cond_0
    return-void
.end method

.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1199953
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "InvisibleSeekBarListenerKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1199954
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "InvisibleSeekBarListenerKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cag;

    iput-object v0, p0, LX/7N9;->p:LX/Cag;

    .line 1199955
    invoke-virtual {p0}, LX/7N9;->i()V

    .line 1199956
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1199951
    const/4 v0, 0x0

    iput-object v0, p0, LX/7N9;->p:LX/Cag;

    .line 1199952
    return-void
.end method

.method public getActiveThumbResource()I
    .locals 1

    .prologue
    .line 1199950
    const/4 v0, 0x0

    return v0
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1199935
    const v0, 0x7f0312e7

    return v0
.end method

.method public final i()V
    .locals 10

    .prologue
    .line 1199936
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 1199937
    :cond_0
    :goto_0
    return-void

    .line 1199938
    :cond_1
    invoke-super {p0}, LX/7Mr;->i()V

    .line 1199939
    iget-object v0, p0, LX/7N9;->p:LX/Cag;

    if-eqz v0, :cond_0

    .line 1199940
    iget-object v0, p0, LX/7N9;->p:LX/Cag;

    iget-object v1, p0, LX/7N9;->o:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iget-object v2, p0, LX/7N9;->o:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    iget-object v3, p0, LX/7N9;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, LX/7N9;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 1199941
    iget-object v5, v0, LX/Cag;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v5, v5, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1199942
    iget-object v6, v5, LX/CcX;->a:LX/CaP;

    move-object v5, v6

    .line 1199943
    if-eqz v5, :cond_2

    .line 1199944
    iget-object v5, v0, LX/Cag;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v5, v5, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1199945
    iget-object v6, v5, LX/CcX;->a:LX/CaP;

    move-object v5, v6

    .line 1199946
    iget-object v6, v0, LX/Cag;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v6, v6, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    move v7, v1

    move v8, v2

    move-object v9, v3

    move-object p0, v4

    .line 1199947
    iget-object v0, v5, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0, v6}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1199948
    iget-object v0, v5, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0, v7, v8, v9, p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(IILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1199949
    :cond_2
    goto :goto_0
.end method
