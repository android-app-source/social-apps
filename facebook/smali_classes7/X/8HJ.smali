.class public LX/8HJ;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;",
        "Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;",
        ">;"
    }
.end annotation


# instance fields
.field private b:LX/0se;


# direct methods
.method public constructor <init>(LX/0sO;LX/0se;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320851
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 1320852
    iput-object p2, p0, LX/8HJ;->b:LX/0se;

    .line 1320853
    return-void
.end method

.method public static a(LX/0QB;)LX/8HJ;
    .locals 3

    .prologue
    .line 1320848
    new-instance v2, LX/8HJ;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v1

    check-cast v1, LX/0se;

    invoke-direct {v2, v0, v1}, LX/8HJ;-><init>(LX/0sO;LX/0se;)V

    .line 1320849
    move-object v0, v2

    .line 1320850
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1320847
    new-instance v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;

    iget-object v1, p0, LX/0ro;->a:LX/0sO;

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1320846
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 1320830
    check-cast p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    .line 1320831
    new-instance v0, LX/5jz;

    invoke-direct {v0}, LX/5jz;-><init>()V

    move-object v1, v0

    .line 1320832
    const/4 v0, 0x1

    .line 1320833
    iput-boolean v0, v1, LX/0gW;->l:Z

    .line 1320834
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1320835
    :goto_0
    return-object v0

    .line 1320836
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    move-object v0, v0

    .line 1320837
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1320838
    iget-object v0, p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    move-object v0, v0

    .line 1320839
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1320840
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1320841
    :cond_1
    iget-object v0, p0, LX/8HJ;->b:LX/0se;

    invoke-virtual {v0, v1}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 1320842
    const-string v0, "nodes"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-object v0, v1

    .line 1320843
    goto :goto_0
.end method

.method public final i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 1320844
    check-cast p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    .line 1320845
    invoke-virtual {p1}, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b()LX/3WA;

    move-result-object v0

    invoke-virtual {v0}, LX/3WA;->getMetadataRecommendedRequestPriority()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    return-object v0
.end method
