.class public final enum LX/6lm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6lm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6lm;

.field public static final enum MATCH_LARGEST:LX/6lm;

.field public static final enum MATCH_LARGEST_NONTEXT:LX/6lm;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1142925
    new-instance v0, LX/6lm;

    const-string v1, "MATCH_LARGEST_NONTEXT"

    invoke-direct {v0, v1, v2}, LX/6lm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6lm;->MATCH_LARGEST_NONTEXT:LX/6lm;

    .line 1142926
    new-instance v0, LX/6lm;

    const-string v1, "MATCH_LARGEST"

    invoke-direct {v0, v1, v3}, LX/6lm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6lm;->MATCH_LARGEST:LX/6lm;

    .line 1142927
    const/4 v0, 0x2

    new-array v0, v0, [LX/6lm;

    sget-object v1, LX/6lm;->MATCH_LARGEST_NONTEXT:LX/6lm;

    aput-object v1, v0, v2

    sget-object v1, LX/6lm;->MATCH_LARGEST:LX/6lm;

    aput-object v1, v0, v3

    sput-object v0, LX/6lm;->$VALUES:[LX/6lm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1142928
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6lm;
    .locals 1

    .prologue
    .line 1142929
    const-class v0, LX/6lm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6lm;

    return-object v0
.end method

.method public static values()[LX/6lm;
    .locals 1

    .prologue
    .line 1142930
    sget-object v0, LX/6lm;->$VALUES:[LX/6lm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6lm;

    return-object v0
.end method
