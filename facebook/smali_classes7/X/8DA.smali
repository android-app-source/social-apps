.class public LX/8DA;
.super LX/3kT;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8DA;


# direct methods
.method public constructor <init>(LX/0SG;LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312349
    invoke-direct {p0, p1, p2, p3, p4}, LX/3kT;-><init>(LX/0SG;LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1312350
    return-void
.end method

.method public static a(LX/0QB;)LX/8DA;
    .locals 7

    .prologue
    .line 1312351
    sget-object v0, LX/8DA;->a:LX/8DA;

    if-nez v0, :cond_1

    .line 1312352
    const-class v1, LX/8DA;

    monitor-enter v1

    .line 1312353
    :try_start_0
    sget-object v0, LX/8DA;->a:LX/8DA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1312354
    if-eqz v2, :cond_0

    .line 1312355
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1312356
    new-instance p0, LX/8DA;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5, v6}, LX/8DA;-><init>(LX/0SG;LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1312357
    move-object v0, p0

    .line 1312358
    sput-object v0, LX/8DA;->a:LX/8DA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1312359
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1312360
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1312361
    :cond_1
    sget-object v0, LX/8DA;->a:LX/8DA;

    return-object v0

    .line 1312362
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1312363
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1312364
    const-string v0, "2438"

    return-object v0
.end method
