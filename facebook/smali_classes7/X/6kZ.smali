.class public LX/6kZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6kd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1139695
    new-instance v0, LX/1sv;

    const-string v1, "GenericMap"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kZ;->b:LX/1sv;

    .line 1139696
    new-instance v0, LX/1sw;

    const-string v1, "data"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kZ;->c:LX/1sw;

    .line 1139697
    sput-boolean v3, LX/6kZ;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6kd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1139769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1139770
    iput-object p1, p0, LX/6kZ;->data:Ljava/util/Map;

    .line 1139771
    return-void
.end method

.method public static b(LX/1su;)LX/6kZ;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1139749
    const/4 v0, 0x0

    .line 1139750
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 1139751
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 1139752
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_4

    .line 1139753
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 1139754
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1139755
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xd

    if-ne v3, v4, :cond_3

    .line 1139756
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v3

    .line 1139757
    new-instance v2, Ljava/util/HashMap;

    iget v0, v3, LX/7H3;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    move v0, v1

    .line 1139758
    :goto_1
    iget v4, v3, LX/7H3;->c:I

    if-gez v4, :cond_1

    invoke-static {}, LX/1su;->s()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1139759
    :cond_0
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v4

    .line 1139760
    invoke-static {p0}, LX/6kd;->b(LX/1su;)LX/6kd;

    move-result-object v5

    .line 1139761
    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1139762
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1139763
    :cond_1
    iget v4, v3, LX/7H3;->c:I

    if-lt v0, v4, :cond_0

    :cond_2
    move-object v0, v2

    .line 1139764
    goto :goto_0

    .line 1139765
    :cond_3
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1139766
    :cond_4
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1139767
    new-instance v1, LX/6kZ;

    invoke-direct {v1, v0}, LX/6kZ;-><init>(Ljava/util/Map;)V

    .line 1139768
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1139728
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1139729
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1139730
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1139731
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GenericMap"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139732
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139733
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139734
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139735
    iget-object v4, p0, LX/6kZ;->data:Ljava/util/Map;

    if-eqz v4, :cond_0

    .line 1139736
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139737
    const-string v4, "data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139738
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139739
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139740
    iget-object v0, p0, LX/6kZ;->data:Ljava/util/Map;

    if-nez v0, :cond_4

    .line 1139741
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139742
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139743
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139744
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1139745
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1139746
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1139747
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 1139748
    :cond_4
    iget-object v0, p0, LX/6kZ;->data:Ljava/util/Map;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 1139717
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1139718
    iget-object v0, p0, LX/6kZ;->data:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 1139719
    iget-object v0, p0, LX/6kZ;->data:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 1139720
    sget-object v0, LX/6kZ;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139721
    new-instance v0, LX/7H3;

    const/16 v1, 0xb

    const/16 v2, 0xc

    iget-object v3, p0, LX/6kZ;->data:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1139722
    iget-object v0, p0, LX/6kZ;->data:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1139723
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1139724
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kd;

    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    goto :goto_0

    .line 1139725
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1139726
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1139727
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1139702
    if-nez p1, :cond_1

    .line 1139703
    :cond_0
    :goto_0
    return v0

    .line 1139704
    :cond_1
    instance-of v1, p1, LX/6kZ;

    if-eqz v1, :cond_0

    .line 1139705
    check-cast p1, LX/6kZ;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1139706
    if-nez p1, :cond_3

    .line 1139707
    :cond_2
    :goto_1
    move v0, v2

    .line 1139708
    goto :goto_0

    .line 1139709
    :cond_3
    iget-object v0, p0, LX/6kZ;->data:Ljava/util/Map;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1139710
    :goto_2
    iget-object v3, p1, LX/6kZ;->data:Ljava/util/Map;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1139711
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1139712
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1139713
    iget-object v0, p0, LX/6kZ;->data:Ljava/util/Map;

    iget-object v3, p1, LX/6kZ;->data:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1139714
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1139715
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1139716
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1139701
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1139698
    sget-boolean v0, LX/6kZ;->a:Z

    .line 1139699
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kZ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1139700
    return-object v0
.end method
