.class public abstract LX/7Cy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/view/TextureView$SurfaceTextureListener;

.field public c:Lcom/facebook/spherical/GlMediaRenderThread;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/graphics/SurfaceTexture;

.field public e:Ljava/lang/Runnable;

.field public f:Ljava/lang/Runnable;

.field public g:Z

.field public h:Z

.field public i:I

.field public j:I

.field public final synthetic k:LX/2qW;


# direct methods
.method public constructor <init>(LX/2qW;Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 1

    .prologue
    .line 1182104
    iput-object p1, p0, LX/7Cy;->k:LX/2qW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182105
    const-class v0, LX/7Cy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7Cy;->a:Ljava/lang/String;

    .line 1182106
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView$SurfaceTextureListener;

    iput-object v0, p0, LX/7Cy;->b:Landroid/view/TextureView$SurfaceTextureListener;

    .line 1182107
    return-void
.end method

.method public static a$redex0(LX/7Cy;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;II)V
    .locals 1

    .prologue
    .line 1182095
    iput p4, p0, LX/7Cy;->i:I

    .line 1182096
    iput p5, p0, LX/7Cy;->j:I

    .line 1182097
    iput-object p2, p0, LX/7Cy;->e:Ljava/lang/Runnable;

    .line 1182098
    iput-object p3, p0, LX/7Cy;->f:Ljava/lang/Runnable;

    .line 1182099
    iput-object p1, p0, LX/7Cy;->d:Landroid/graphics/SurfaceTexture;

    .line 1182100
    iget-boolean v0, p0, LX/7Cy;->g:Z

    if-eqz v0, :cond_0

    .line 1182101
    invoke-virtual {p0}, LX/7Cy;->a()V

    .line 1182102
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7Cy;->g:Z

    .line 1182103
    :cond_0
    return-void
.end method

.method public static c(LX/7Cy;)V
    .locals 3

    .prologue
    .line 1182088
    iget-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_1

    .line 1182089
    iget-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1182090
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 1182091
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1182092
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->t:Z

    .line 1182093
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1182094
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1182057
    iget-object v2, p0, LX/7Cy;->d:Landroid/graphics/SurfaceTexture;

    if-nez v2, :cond_0

    .line 1182058
    iget-object v2, p0, LX/7Cy;->a:Ljava/lang/String;

    const-string v3, "id:%d beginRendering surface texture is null"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1182059
    iput-boolean v0, p0, LX/7Cy;->g:Z

    .line 1182060
    :goto_0
    return-void

    .line 1182061
    :cond_0
    iget-object v2, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v2, :cond_2

    .line 1182062
    iget-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1182063
    iget-boolean v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->s:Z

    if-eqz v1, :cond_1

    .line 1182064
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->I:Landroid/view/Choreographer;

    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->G:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v1, v2}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1182065
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->s:Z

    .line 1182066
    invoke-static {v0}, Lcom/facebook/spherical/GlMediaRenderThread;->r(Lcom/facebook/spherical/GlMediaRenderThread;)V

    .line 1182067
    :cond_1
    goto :goto_0

    .line 1182068
    :cond_2
    iget-object v2, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v2, :cond_3

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1182069
    invoke-virtual {p0}, LX/7Cy;->b()Lcom/facebook/spherical/GlMediaRenderThread;

    move-result-object v0

    iput-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1182070
    iget-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1182071
    iget-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v0}, Lcom/facebook/spherical/GlMediaRenderThread;->start()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1182072
    goto :goto_1
.end method

.method public abstract b()Lcom/facebook/spherical/GlMediaRenderThread;
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1182085
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Cy;->h:Z

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v4, p2

    move v5, p3

    .line 1182086
    invoke-static/range {v0 .. v5}, LX/7Cy;->a$redex0(LX/7Cy;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;II)V

    .line 1182087
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1182081
    iget-object v0, p0, LX/7Cy;->k:LX/2qW;

    invoke-virtual {v0, v1}, LX/2qW;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1182082
    invoke-static {p0}, LX/7Cy;->c(LX/7Cy;)V

    .line 1182083
    iput-object v1, p0, LX/7Cy;->d:Landroid/graphics/SurfaceTexture;

    .line 1182084
    iget-boolean v0, p0, LX/7Cy;->h:Z

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 1182075
    iput p2, p0, LX/7Cy;->i:I

    .line 1182076
    iput p3, p0, LX/7Cy;->j:I

    .line 1182077
    iget-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 1182078
    iget-object v0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1182079
    const/4 p0, 0x0

    invoke-virtual {v0, p2, p3, p0}, Lcom/facebook/spherical/GlMediaRenderThread;->a(IIZ)V

    .line 1182080
    :cond_0
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 1182073
    iget-object v0, p0, LX/7Cy;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V

    .line 1182074
    return-void
.end method
