.class public LX/8Oh;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/7TG;

.field private final d:LX/8KY;

.field private final e:LX/7Ss;

.field public final f:LX/7Su;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/2MV;

.field public final i:LX/2Mj;

.field private final j:LX/0ad;

.field private final k:LX/0cX;

.field private final l:LX/8Of;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1340122
    const-class v0, LX/8Oh;

    sput-object v0, LX/8Oh;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/7TG;LX/8KY;LX/7Ss;LX/7Su;LX/0Or;LX/2MV;LX/8Oc;LX/0ad;LX/0cX;LX/8Of;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/VideoUploadResizingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/7TG;",
            "LX/8KY;",
            "LX/7Ss;",
            "LX/7Su;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2MV;",
            "LX/8Oc;",
            "LX/0ad;",
            "LX/0cX;",
            "LX/8Of;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1340123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1340124
    iput-object p1, p0, LX/8Oh;->b:LX/0SG;

    .line 1340125
    iput-object p2, p0, LX/8Oh;->c:LX/7TG;

    .line 1340126
    iput-object p3, p0, LX/8Oh;->d:LX/8KY;

    .line 1340127
    iput-object p4, p0, LX/8Oh;->e:LX/7Ss;

    .line 1340128
    iput-object p5, p0, LX/8Oh;->f:LX/7Su;

    .line 1340129
    iput-object p6, p0, LX/8Oh;->g:LX/0Or;

    .line 1340130
    iput-object p7, p0, LX/8Oh;->h:LX/2MV;

    .line 1340131
    iput-object p8, p0, LX/8Oh;->i:LX/2Mj;

    .line 1340132
    iput-object p9, p0, LX/8Oh;->j:LX/0ad;

    .line 1340133
    iput-object p10, p0, LX/8Oh;->k:LX/0cX;

    .line 1340134
    iput-object p11, p0, LX/8Oh;->l:LX/8Of;

    .line 1340135
    return-void
.end method

.method private static a(LX/8Oh;LX/8Ob;Ljava/lang/String;LX/8Oa;LX/8Ok;Ljava/lang/String;LX/8KX;LX/2Md;)LX/7TH;
    .locals 3

    .prologue
    .line 1340136
    invoke-static {}, LX/7TH;->newBuilder()LX/7TI;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1340137
    iput-object v1, v0, LX/7TI;->a:Ljava/io/File;

    .line 1340138
    move-object v0, v0

    .line 1340139
    iget-object v1, p6, LX/8KX;->b:Ljava/io/File;

    move-object v1, v1

    .line 1340140
    iput-object v1, v0, LX/7TI;->b:Ljava/io/File;

    .line 1340141
    move-object v0, v0

    .line 1340142
    iput-object p7, v0, LX/7TI;->c:LX/2Md;

    .line 1340143
    move-object v0, v0

    .line 1340144
    iput-object p4, v0, LX/7TI;->h:LX/60y;

    .line 1340145
    move-object v0, v0

    .line 1340146
    const-string v1, "Baseline"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1340147
    const/4 v1, 0x1

    .line 1340148
    :goto_0
    move v1, v1

    .line 1340149
    if-nez v1, :cond_0

    .line 1340150
    const/16 p2, 0x100

    .line 1340151
    const-string v1, "High"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1340152
    new-instance v1, LX/7Sy;

    const/16 v2, 0x8

    invoke-direct {v1, v2, p2}, LX/7Sy;-><init>(II)V

    .line 1340153
    :goto_1
    move-object v1, v1

    .line 1340154
    iput-object v1, v0, LX/7TI;->l:LX/7Sy;

    .line 1340155
    :cond_0
    iget-boolean v1, p1, LX/8Ob;->L:Z

    if-eqz v1, :cond_1

    .line 1340156
    iget v1, p1, LX/8Ob;->M:I

    .line 1340157
    iput v1, v0, LX/7TI;->f:I

    .line 1340158
    move-object v1, v0

    .line 1340159
    iget v2, p1, LX/8Ob;->N:I

    .line 1340160
    iput v2, v1, LX/7TI;->g:I

    .line 1340161
    :cond_1
    iget v1, p1, LX/8Ob;->J:I

    if-eqz v1, :cond_2

    .line 1340162
    iget v1, p1, LX/8Ob;->J:I

    invoke-virtual {v0, v1}, LX/7TI;->c(I)LX/7TI;

    .line 1340163
    :cond_2
    iget-boolean v1, p1, LX/8Ob;->K:Z

    if-eqz v1, :cond_3

    .line 1340164
    const/4 v1, 0x1

    .line 1340165
    iput-boolean v1, v0, LX/7TI;->i:Z

    .line 1340166
    :cond_3
    iget-object v1, p1, LX/8Ob;->O:Landroid/graphics/RectF;

    if-eqz v1, :cond_4

    .line 1340167
    iget-object v1, p1, LX/8Ob;->O:Landroid/graphics/RectF;

    .line 1340168
    iput-object v1, v0, LX/7TI;->d:Landroid/graphics/RectF;

    .line 1340169
    :cond_4
    iget-object v1, p1, LX/8Ob;->P:LX/0Px;

    if-eqz v1, :cond_5

    .line 1340170
    iget-object v1, p0, LX/8Oh;->l:LX/8Of;

    iget-object v2, p1, LX/8Ob;->P:LX/0Px;

    invoke-virtual {v1, v2}, LX/8Of;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    .line 1340171
    iput-object v1, v0, LX/7TI;->n:LX/0Px;

    .line 1340172
    :cond_5
    if-eqz p3, :cond_6

    .line 1340173
    iget-object v1, p3, LX/8Oa;->g:LX/7T7;

    invoke-virtual {v1, v0}, LX/7T7;->a(LX/7TI;)LX/7TI;

    move-result-object v0

    .line 1340174
    :cond_6
    invoke-virtual {v0}, LX/7TI;->o()LX/7TH;

    move-result-object v0

    return-object v0

    :cond_7
    const/4 v1, 0x0

    goto :goto_0

    .line 1340175
    :cond_8
    new-instance v1, LX/7Sy;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p2}, LX/7Sy;-><init>(II)V

    goto :goto_1
.end method

.method private a(LX/8Ob;Ljava/lang/String;LX/73w;LX/74b;LX/8Oa;Ljava/lang/String;Ljava/io/File;LX/8Ok;LX/8OL;)LX/8KX;
    .locals 48

    .prologue
    .line 1340176
    const-string v2, "Baseline"

    .line 1340177
    move-object/from16 v0, p1

    iget-boolean v3, v0, LX/8Ob;->I:Z

    if-nez v3, :cond_e

    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v3, v3, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    iget-boolean v3, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    if-nez v3, :cond_e

    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v3, v3, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    iget-wide v4, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-gez v3, :cond_e

    .line 1340178
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Oh;->j:LX/0ad;

    const v4, -0x6796

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1340179
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Oh;->j:LX/0ad;

    const/16 v3, 0x186c

    const-string v4, "main"

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v16, v2

    .line 1340180
    :goto_0
    const/16 v17, 0x0

    .line 1340181
    const/4 v3, 0x0

    .line 1340182
    if-eqz p7, :cond_d

    :try_start_0
    invoke-virtual/range {p7 .. p7}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p7 .. p7}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1340183
    :cond_0
    invoke-virtual/range {p7 .. p7}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x400

    cmp-long v2, v4, v6

    if-gez v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1340184
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Oh;->d:LX/8KY;

    const/high16 v4, 0xa00000

    const-string v5, "mp4"

    move-object/from16 v0, p7

    invoke-virtual {v2, v0, v4, v5}, LX/8KY;->a(Ljava/io/File;ILjava/lang/String;)LX/8KX;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v3

    move-object v9, v3

    .line 1340185
    :goto_2
    if-nez v9, :cond_c

    .line 1340186
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Oh;->d:LX/8KY;

    const/high16 v5, 0xa00000

    const/high16 v6, 0x200000

    const-string v7, "mp4"

    const-string v8, "mp4"

    move-object/from16 v3, p6

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v8}, LX/8KY;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)LX/8KX;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v46

    .line 1340187
    :goto_3
    :try_start_2
    const-string v2, "video-processing"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340188
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Oh;->e:LX/7Ss;

    .line 1340189
    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->H:LX/7Su;

    if-eqz v3, :cond_b

    .line 1340190
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->H:LX/7Su;

    move-object/from16 v18, v2

    .line 1340191
    :goto_4
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->P:LX/0Px;

    if-eqz v2, :cond_1

    .line 1340192
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, LX/2Md;->a(Z)V

    .line 1340193
    :cond_1
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->H:LX/7Su;

    if-eqz v2, :cond_6

    const/4 v5, 0x1

    :goto_5
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->V:LX/8OY;

    iget v6, v2, LX/8OY;->a:I

    move-object/from16 v0, p1

    iget-boolean v8, v0, LX/8Ob;->L:Z

    move-object/from16 v0, p1

    iget v9, v0, LX/8Ob;->M:I

    move-object/from16 v0, p1

    iget v10, v0, LX/8Ob;->N:I

    move-object/from16 v0, p1

    iget-boolean v11, v0, LX/8Ob;->K:Z

    move-object/from16 v0, p1

    iget v12, v0, LX/8Ob;->J:I

    move-object/from16 v0, p1

    iget-object v13, v0, LX/8Ob;->O:Landroid/graphics/RectF;

    move-object/from16 v0, p1

    iget-wide v14, v0, LX/8Ob;->T:J

    move-object/from16 v3, p3

    move-object/from16 v7, v16

    invoke-virtual/range {v3 .. v15}, LX/73w;->a(LX/74b;ZILjava/lang/String;ZIIZILandroid/graphics/RectF;J)V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p8

    move-object/from16 v7, v16

    move-object/from16 v8, v46

    move-object/from16 v9, v18

    .line 1340194
    invoke-static/range {v2 .. v9}, LX/8Oh;->a(LX/8Oh;LX/8Ob;Ljava/lang/String;LX/8Oa;LX/8Ok;Ljava/lang/String;LX/8KX;LX/2Md;)LX/7TH;

    move-result-object v2

    .line 1340195
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Oh;->c:LX/7TG;

    invoke-virtual {v3, v2}, LX/7TG;->a(LX/7TH;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1340196
    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, LX/8OL;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1340197
    const v3, 0x28f60231

    :try_start_3
    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7TD;
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1340198
    :try_start_4
    invoke-virtual/range {p9 .. p9}, LX/8OL;->b()V

    .line 1340199
    invoke-virtual/range {v46 .. v46}, LX/8KX;->c()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 1340200
    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-gez v3, :cond_7

    .line 1340201
    new-instance v2, LX/8Ol;

    const-string v3, "empty resized file"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, LX/8Ol;-><init>(Ljava/lang/String;Z)V

    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 1340202
    :catch_0
    move-exception v2

    move-object/from16 v37, v2

    move-object/from16 v3, v46

    move-object/from16 v2, v17

    .line 1340203
    :goto_6
    invoke-virtual/range {p9 .. p9}, LX/8OL;->e()Z

    move-result v4

    .line 1340204
    if-eqz v3, :cond_2

    .line 1340205
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, LX/8KX;->a(Z)V

    .line 1340206
    invoke-virtual {v3}, LX/8KX;->a()V

    .line 1340207
    :cond_2
    if-eqz v4, :cond_3

    .line 1340208
    invoke-virtual/range {p3 .. p4}, LX/73w;->c(LX/74b;)V

    .line 1340209
    const-string v3, "processing"

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340210
    :cond_3
    const/4 v3, 0x1

    move-object/from16 v0, v37

    invoke-static {v0, v3}, LX/0cX;->a(Ljava/lang/Exception;Z)LX/73z;

    move-result-object v5

    .line 1340211
    if-nez v2, :cond_4

    .line 1340212
    new-instance v2, LX/7TE;

    invoke-direct {v2}, LX/7TE;-><init>()V

    .line 1340213
    :cond_4
    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v3, v3, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    iget-boolean v6, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    iget-boolean v7, v2, LX/7TE;->a:Z

    iget-boolean v8, v2, LX/7TE;->b:Z

    iget-boolean v9, v2, LX/7TE;->c:Z

    iget-boolean v10, v2, LX/7TE;->d:Z

    iget-boolean v11, v2, LX/7TE;->e:Z

    iget-boolean v12, v2, LX/7TE;->f:Z

    iget-boolean v13, v2, LX/7TE;->g:Z

    iget-wide v14, v2, LX/7TE;->h:J

    iget-wide v0, v2, LX/7TE;->i:J

    move-wide/from16 v16, v0

    iget-wide v0, v2, LX/7TE;->j:J

    move-wide/from16 v18, v0

    iget-wide v0, v2, LX/7TE;->k:J

    move-wide/from16 v20, v0

    iget-wide v0, v2, LX/7TE;->l:J

    move-wide/from16 v22, v0

    iget-wide v0, v2, LX/7TE;->m:J

    move-wide/from16 v24, v0

    iget-wide v0, v2, LX/7TE;->n:J

    move-wide/from16 v26, v0

    iget-wide v0, v2, LX/7TE;->o:J

    move-wide/from16 v28, v0

    iget-wide v0, v2, LX/7TE;->p:J

    move-wide/from16 v30, v0

    iget-wide v0, v2, LX/7TE;->q:J

    move-wide/from16 v32, v0

    iget-object v0, v2, LX/7TE;->r:Ljava/lang/String;

    move-object/from16 v34, v0

    iget-object v0, v2, LX/7TE;->s:Ljava/lang/String;

    move-object/from16 v35, v0

    iget-object v0, v2, LX/7TE;->t:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-virtual/range {v3 .. v36}, LX/73w;->a(LX/74b;LX/73y;ZZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1340214
    new-instance v2, LX/74H;

    move-object/from16 v0, v37

    invoke-direct {v2, v0}, LX/74H;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 1340215
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1340216
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 1340217
    :catch_1
    move-exception v2

    .line 1340218
    :try_start_5
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    .line 1340219
    instance-of v2, v3, LX/7T9;

    if-eqz v2, :cond_a

    .line 1340220
    move-object v0, v3

    check-cast v0, LX/7T9;

    move-object v2, v0

    .line 1340221
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v4, v4, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    invoke-virtual {v2}, LX/7T9;->a()Z

    move-result v5

    iput-boolean v5, v4, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    .line 1340222
    invoke-virtual {v2}, LX/7T9;->b()LX/7TE;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v4

    .line 1340223
    :goto_7
    :try_start_6
    new-instance v2, LX/740;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Resizing video failed. Reason: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v2, v5, v3, v6}, LX/740;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1340224
    :catchall_0
    move-exception v2

    move-object v3, v4

    :goto_8
    :try_start_7
    invoke-virtual/range {p9 .. p9}, LX/8OL;->b()V

    throw v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 1340225
    :catch_2
    move-exception v2

    move-object/from16 v37, v2

    move-object v2, v3

    move-object/from16 v3, v46

    goto/16 :goto_6

    .line 1340226
    :cond_7
    :try_start_8
    invoke-virtual/range {v46 .. v46}, LX/8KX;->c()Ljava/io/File;

    move-result-object v3

    invoke-virtual/range {v46 .. v46}, LX/8KX;->b()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1340227
    new-instance v2, LX/8Ol;

    const-string v3, "can\'t rename scratch file"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, LX/8Ol;-><init>(Ljava/lang/String;Z)V

    throw v2

    .line 1340228
    :cond_8
    invoke-virtual/range {v46 .. v46}, LX/8KX;->b()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v3, v6, v4

    if-eqz v3, :cond_9

    .line 1340229
    new-instance v2, LX/8Ol;

    const-string v3, "move failed"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, LX/8Ol;-><init>(Ljava/lang/String;Z)V

    throw v2

    .line 1340230
    :cond_9
    iget-object v0, v2, LX/7TD;->m:LX/7TE;

    move-object/from16 v47, v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    .line 1340231
    :try_start_9
    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->A:LX/74b;

    const-string v4, "mp4"

    iget v5, v2, LX/7TD;->d:I

    iget v6, v2, LX/7TD;->e:I

    iget v7, v2, LX/7TD;->f:I

    iget v8, v2, LX/7TD;->g:I

    iget v9, v2, LX/7TD;->h:I

    iget v10, v2, LX/7TD;->i:I

    iget v11, v2, LX/7TD;->j:I

    iget v12, v2, LX/7TD;->k:I

    iget-wide v13, v2, LX/7TD;->l:J

    iget-wide v15, v2, LX/7TD;->b:J

    iget-wide v0, v2, LX/7TD;->c:J

    move-wide/from16 v17, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, LX/7TE;->a:Z

    move/from16 v19, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, LX/7TE;->b:Z

    move/from16 v20, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, LX/7TE;->c:Z

    move/from16 v21, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, LX/7TE;->d:Z

    move/from16 v22, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, LX/7TE;->e:Z

    move/from16 v23, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, LX/7TE;->f:Z

    move/from16 v24, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, LX/7TE;->g:Z

    move/from16 v25, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->h:J

    move-wide/from16 v26, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->i:J

    move-wide/from16 v28, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->j:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->k:J

    move-wide/from16 v32, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->l:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->m:J

    move-wide/from16 v36, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->n:J

    move-wide/from16 v38, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->o:J

    move-wide/from16 v40, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->p:J

    move-wide/from16 v42, v0

    move-object/from16 v0, v47

    iget-wide v0, v0, LX/7TE;->q:J

    move-wide/from16 v44, v0

    move-object/from16 v2, p3

    invoke-virtual/range {v2 .. v45}, LX/73w;->a(LX/74b;Ljava/lang/String;IIIIIIIIJJJZZZZZZZJJJJJJJJJJ)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    .line 1340232
    return-object v46

    .line 1340233
    :catch_3
    move-exception v2

    move-object/from16 v37, v2

    move-object/from16 v2, v17

    goto/16 :goto_6

    :catch_4
    move-exception v2

    move-object/from16 v37, v2

    move-object v3, v9

    move-object/from16 v2, v17

    goto/16 :goto_6

    :catch_5
    move-exception v2

    move-object/from16 v37, v2

    move-object/from16 v3, v46

    move-object/from16 v2, v47

    goto/16 :goto_6

    .line 1340234
    :catchall_1
    move-exception v2

    move-object/from16 v3, v17

    goto/16 :goto_8

    :cond_a
    move-object/from16 v4, v17

    goto/16 :goto_7

    :cond_b
    move-object/from16 v18, v2

    goto/16 :goto_4

    :cond_c
    move-object/from16 v46, v9

    goto/16 :goto_3

    :cond_d
    move-object v9, v3

    goto/16 :goto_2

    :cond_e
    move-object/from16 v16, v2

    goto/16 :goto_0
.end method

.method private static a(LX/8Ob;LX/73w;Ljava/io/File;)LX/8OX;
    .locals 6

    .prologue
    .line 1340268
    const/4 v0, 0x0

    .line 1340269
    if-eqz p2, :cond_0

    .line 1340270
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 1340271
    const-wide/16 v4, 0x400

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v4, p0, LX/8Ob;->m:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1340272
    new-instance v0, LX/8OX;

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2, v3}, LX/8OX;-><init>(Ljava/lang/String;J)V

    .line 1340273
    iget-object v1, p0, LX/8Ob;->A:LX/74b;

    .line 1340274
    invoke-virtual {v1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 1340275
    const-string v5, "video_resized_file_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340276
    sget-object v5, LX/74R;->MEDIA_UPLOAD_PROCESS_FOUND_EXISTING_FILE:LX/74R;

    const/4 p0, 0x0

    invoke-static {p1, v5, v4, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340277
    :cond_0
    return-object v0
.end method

.method private static a(LX/8Oh;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Oa;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;LX/8Ok;LX/8OL;)LX/8OX;
    .locals 11

    .prologue
    .line 1340235
    const/4 v10, 0x0

    .line 1340236
    :try_start_0
    iget-object v4, p1, LX/8Ob;->A:LX/74b;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p5

    move-object v3, p3

    move-object v5, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, LX/8Oh;->a(LX/8Ob;Ljava/lang/String;LX/73w;LX/74b;LX/8Oa;Ljava/lang/String;Ljava/io/File;LX/8Ok;LX/8OL;)LX/8KX;

    move-result-object v0

    .line 1340237
    if-eqz v0, :cond_1

    .line 1340238
    invoke-virtual {v0}, LX/8KX;->b()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 1340239
    iget-wide v4, p1, LX/8Ob;->m:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 1340240
    invoke-virtual {v0}, LX/8KX;->b()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 1340241
    new-instance v0, LX/8OX;

    invoke-direct {v0, v1, v2, v3}, LX/8OX;-><init>(Ljava/lang/String;J)V
    :try_end_0
    .catch LX/74H; {:try_start_0 .. :try_end_0} :catch_0

    .line 1340242
    :goto_0
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {p2, v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c(Z)V
    :try_end_1
    .catch LX/74H; {:try_start_1 .. :try_end_1} :catch_1

    .line 1340243
    :goto_1
    return-object v0

    .line 1340244
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v10

    .line 1340245
    :goto_2
    invoke-virtual/range {p9 .. p9}, LX/8OL;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1340246
    throw v1

    .line 1340247
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c(Z)V

    goto :goto_1

    .line 1340248
    :catch_1
    move-exception v1

    goto :goto_2

    :cond_1
    move-object v0, v10

    goto :goto_0
.end method

.method private static a(LX/8Oh;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ok;LX/8OL;LX/8Oa;)LX/8OX;
    .locals 11

    .prologue
    .line 1340249
    invoke-virtual {p3}, LX/73w;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, LX/8Oa;->a(Ljava/lang/String;)V

    .line 1340250
    invoke-virtual/range {p6 .. p6}, LX/8Oa;->a()Ljava/lang/String;

    move-result-object v7

    .line 1340251
    iget-object v1, p1, LX/8Ob;->q:Ljava/lang/String;

    invoke-static {p0, v7, v1}, LX/8Oh;->a(LX/8Oh;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    .line 1340252
    invoke-static {p1, p3, v8}, LX/8Oh;->a(LX/8Ob;LX/73w;Ljava/io/File;)LX/8OX;

    move-result-object v1

    .line 1340253
    if-nez v1, :cond_0

    .line 1340254
    iget-object v6, p1, LX/8Ob;->q:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p6

    move-object v9, p4

    move-object/from16 v10, p5

    invoke-static/range {v1 .. v10}, LX/8Oh;->a(LX/8Oh;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Oa;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;LX/8Ok;LX/8OL;)LX/8OX;

    move-result-object v1

    .line 1340255
    :cond_0
    return-object v1
.end method

.method public static a(LX/0QB;)LX/8Oh;
    .locals 13

    .prologue
    .line 1340256
    new-instance v1, LX/8Oh;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/7TG;->a(LX/0QB;)LX/7TG;

    move-result-object v3

    check-cast v3, LX/7TG;

    invoke-static {p0}, LX/8KY;->a(LX/0QB;)LX/8KY;

    move-result-object v4

    check-cast v4, LX/8KY;

    invoke-static {p0}, LX/7Ss;->a(LX/0QB;)LX/7Ss;

    move-result-object v5

    check-cast v5, LX/7Ss;

    .line 1340257
    new-instance v6, LX/7Su;

    invoke-direct {v6}, LX/7Su;-><init>()V

    .line 1340258
    move-object v6, v6

    .line 1340259
    move-object v6, v6

    .line 1340260
    check-cast v6, LX/7Su;

    const/16 v7, 0x347

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/2MU;->b(LX/0QB;)LX/2MU;

    move-result-object v8

    check-cast v8, LX/2MV;

    invoke-static {p0}, LX/8Oc;->a(LX/0QB;)LX/8Oc;

    move-result-object v9

    check-cast v9, LX/8Oc;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v11

    check-cast v11, LX/0cX;

    invoke-static {p0}, LX/8Of;->b(LX/0QB;)LX/8Of;

    move-result-object v12

    check-cast v12, LX/8Of;

    invoke-direct/range {v1 .. v12}, LX/8Oh;-><init>(LX/0SG;LX/7TG;LX/8KY;LX/7Ss;LX/7Su;LX/0Or;LX/2MV;LX/8Oc;LX/0ad;LX/0cX;LX/8Of;)V

    .line 1340261
    move-object v0, v1

    .line 1340262
    return-object v0
.end method

.method public static a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/base/media/VideoItem;
    .locals 3

    .prologue
    .line 1340263
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 1340264
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1340265
    instance-of v2, v0, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v2, :cond_0

    .line 1340266
    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    return-object v0

    .line 1340267
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "UploadOperation doesn\'t contain VideoItem"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/8Oh;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1340078
    iget-object v0, p0, LX/8Oh;->d:LX/8KY;

    const-string v1, "mp4"

    invoke-virtual {v0, p1, p2, v1}, LX/8KY;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1340073
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1340074
    :cond_0
    :goto_0
    return-object v0

    .line 1340075
    :cond_1
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 1340076
    if-ltz v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 1340077
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1340079
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v4, v0

    .line 1340080
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1340081
    instance-of v0, v0, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v0, :cond_3

    move v0, v3

    .line 1340082
    :goto_1
    if-eqz v0, :cond_4

    .line 1340083
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    move-object v0, v0

    .line 1340084
    if-eqz v0, :cond_2

    .line 1340085
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_0

    move v2, v3

    :cond_0
    const-string v3, "mediaItems metadata needs to of same length as mediaItems"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1340086
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1340087
    const/4 v3, 0x1

    .line 1340088
    const-class v1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1340089
    const-string v1, "video_creative_editing_metadata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1340090
    if-nez v1, :cond_6

    .line 1340091
    :cond_1
    :goto_2
    const-string v1, "video_upload_quality"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1340092
    if-nez v2, :cond_d

    .line 1340093
    :cond_2
    :goto_3
    return-void

    .line 1340094
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1340095
    goto :goto_0

    .line 1340096
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "UploadOperation doesn\'t contain VideoItem"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_1

    .line 1340097
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v2

    .line 1340098
    if-eqz v2, :cond_7

    .line 1340099
    iget-boolean v4, v2, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    iput-boolean v4, p1, LX/8Ob;->L:Z

    .line 1340100
    iget-boolean v4, p1, LX/8Ob;->L:Z

    if-eqz v4, :cond_7

    .line 1340101
    iput-boolean v3, p1, LX/8Ob;->I:Z

    .line 1340102
    iget v4, v2, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    iput v4, p1, LX/8Ob;->M:I

    .line 1340103
    iget v2, v2, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    iput v2, p1, LX/8Ob;->N:I

    .line 1340104
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result v4

    .line 1340105
    if-eqz v4, :cond_8

    const/16 v2, 0x5a

    if-eq v4, v2, :cond_8

    const/16 v2, 0xb4

    if-eq v4, v2, :cond_8

    const/16 v2, 0x10e

    if-ne v4, v2, :cond_c

    :cond_8
    move v2, v3

    :goto_4
    const-string v5, "videoCreateiveEditingData.getRotationAngle() must be one of 0, 90, 180, 270"

    invoke-static {v2, v5}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1340106
    if-eqz v4, :cond_9

    .line 1340107
    iput v4, p1, LX/8Ob;->J:I

    .line 1340108
    iput-boolean v3, p1, LX/8Ob;->I:Z

    .line 1340109
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v2

    iput-boolean v2, p1, LX/8Ob;->K:Z

    .line 1340110
    iget-boolean v2, p1, LX/8Ob;->K:Z

    if-eqz v2, :cond_a

    .line 1340111
    iput-boolean v3, p1, LX/8Ob;->I:Z

    .line 1340112
    :cond_a
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    invoke-static {v2}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v2

    iput-object v2, p1, LX/8Ob;->O:Landroid/graphics/RectF;

    .line 1340113
    iget-object v2, p1, LX/8Ob;->O:Landroid/graphics/RectF;

    if-eqz v2, :cond_b

    .line 1340114
    iput-boolean v3, p1, LX/8Ob;->I:Z

    .line 1340115
    :cond_b
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getGLRendererConfigs()LX/0Px;

    move-result-object v1

    iput-object v1, p1, LX/8Ob;->P:LX/0Px;

    .line 1340116
    iget-object v1, p1, LX/8Ob;->P:LX/0Px;

    if-eqz v1, :cond_1

    .line 1340117
    iput-boolean v3, p1, LX/8Ob;->I:Z

    goto :goto_2

    .line 1340118
    :cond_c
    const/4 v2, 0x0

    goto :goto_4

    .line 1340119
    :cond_d
    const-string v1, "high"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "low"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "medium"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "standard"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "raw"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    :cond_e
    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1340120
    iput-object v2, p1, LX/8Ob;->n:Ljava/lang/String;

    goto/16 :goto_3

    .line 1340121
    :cond_f
    const/4 v1, 0x0

    goto :goto_5
.end method

.method public static a(IJIF)Z
    .locals 7

    .prologue
    .line 1340068
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    int-to-double v2, p0

    long-to-double v4, p1

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    .line 1340069
    int-to-long v2, p0

    sub-long v2, p1, v2

    .line 1340070
    cmpl-float v0, v0, p4

    if-lez v0, :cond_0

    int-to-long v0, p3

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 1340071
    :goto_0
    return v0

    .line 1340072
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/8Ob;)Z
    .locals 15

    .prologue
    .line 1340054
    iget-object v0, p0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v10, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    .line 1340055
    invoke-static {p0}, LX/8Oh;->c(LX/8Ob;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1340056
    const/4 v11, 0x1

    .line 1340057
    iget-object v0, p0, LX/8Ob;->y:LX/73w;

    iget-object v1, p0, LX/8Ob;->A:LX/74b;

    iget-wide v2, v10, Lcom/facebook/photos/upload/operation/TranscodeInfo;->flowStartCount:J

    iget-wide v4, v10, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    iget-wide v6, v10, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    iget-wide v8, v10, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    iget-boolean v10, v10, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isSegmentedTranscode:Z

    .line 1340058
    invoke-virtual {v1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v12

    .line 1340059
    const-string v13, "video_transcode_flow_count"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340060
    const-string v13, "video_transcode_start_count"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340061
    const-string v13, "video_transcode_success_count"

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340062
    const-string v13, "video_transcode_fail_count"

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340063
    const-string v13, "video_transcode_is_segmented"

    invoke-static {v10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340064
    sget-object v13, LX/74R;->MEDIA_UPLOAD_PROCESS_OMITTED_TOO_MANY_FAILURES:LX/74R;

    const/4 v14, 0x0

    invoke-static {v0, v13, v12, v14}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340065
    move v0, v11

    .line 1340066
    :goto_0
    return v0

    .line 1340067
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/8Oh;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;LX/73w;)Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1340016
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1340017
    sget-object v1, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    if-ne v0, v1, :cond_0

    .line 1340018
    :goto_0
    return v10

    .line 1340019
    :cond_0
    :try_start_0
    iget-object v0, p2, LX/8Ob;->A:LX/74b;

    iget-object v1, p2, LX/8Ob;->q:Ljava/lang/String;

    invoke-static {v1}, LX/8Oh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p2, LX/8Ob;->m:J

    .line 1340020
    invoke-virtual {v0}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 1340021
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1340022
    const-string v5, "extension"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340023
    :cond_1
    const-string v5, "original_file_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340024
    sget-object v5, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_START:LX/74R;

    const/4 v6, 0x0

    invoke-static {p3, v5, v4, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340025
    const/4 v0, 0x0

    iput-object v0, p2, LX/8Ob;->H:LX/7Su;

    .line 1340026
    iget-object v1, p2, LX/8Ob;->V:LX/8OY;

    .line 1340027
    if-eqz v1, :cond_4

    move v0, v11

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1340028
    invoke-virtual {v1}, LX/8OY;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v1}, LX/8OY;->e(LX/8OY;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1340029
    if-nez v0, :cond_2

    .line 1340030
    iget-object v0, p0, LX/8Oh;->f:LX/7Su;

    iget-object v1, p2, LX/8Ob;->V:LX/8OY;

    iget v1, v1, LX/8OY;->a:I

    iget-object v2, p2, LX/8Ob;->V:LX/8OY;

    iget v2, v2, LX/8OY;->b:I

    invoke-virtual {v0, v1, v2}, LX/7Su;->a(II)V

    .line 1340031
    iget-object v0, p0, LX/8Oh;->f:LX/7Su;

    iput-object v0, p2, LX/8Ob;->H:LX/7Su;

    .line 1340032
    :cond_2
    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 1340033
    invoke-static {p1}, LX/8Oh;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    .line 1340034
    invoke-virtual {p0, v0}, LX/8Oh;->a(Lcom/facebook/photos/base/media/VideoItem;)LX/60x;

    move-result-object v0

    .line 1340035
    iget-object v1, p2, LX/8Ob;->H:LX/7Su;

    if-eqz v1, :cond_7

    .line 1340036
    iget-object v1, p0, LX/8Oh;->i:LX/2Mj;

    iget-object v2, p2, LX/8Ob;->H:LX/7Su;

    invoke-virtual {v1, v0, v2, v4, v3}, LX/2Mj;->a(LX/60x;LX/2Md;II)LX/7Sw;

    move-result-object v0

    .line 1340037
    :goto_3
    move-object v0, v0

    .line 1340038
    iget v4, v0, LX/7Sw;->c:I

    .line 1340039
    iget-boolean v1, p2, LX/8Ob;->I:Z

    if-nez v1, :cond_3

    iget-wide v2, p2, LX/8Ob;->m:J

    const/high16 v1, 0xa00000

    const v5, 0x3e4ccccd    # 0.2f

    invoke-static {v4, v2, v3, v1, v5}, LX/8Oh;->a(IJIF)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    move v5, v11

    .line 1340040
    :goto_4
    iget-object v1, p2, LX/8Ob;->A:LX/74b;

    iget-wide v2, p2, LX/8Ob;->m:J

    iget-boolean v6, p2, LX/8Ob;->G:Z

    iget-object v7, p2, LX/8Ob;->V:LX/8OY;

    iget v7, v7, LX/8OY;->a:I

    iget v8, v0, LX/7Sw;->e:I

    iget v9, v0, LX/7Sw;->f:I

    move-object v0, p3

    invoke-virtual/range {v0 .. v9}, LX/73w;->a(LX/74b;JIZZIII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_5
    move v10, v5

    .line 1340041
    goto/16 :goto_0

    :cond_4
    move v0, v10

    .line 1340042
    goto :goto_1

    :cond_5
    move v5, v10

    .line 1340043
    goto :goto_4

    .line 1340044
    :catch_0
    move-exception v0

    .line 1340045
    sget-object v1, LX/8Oh;->a:Ljava/lang/Class;

    const-string v2, "Could not read video metadata for original file."

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1340046
    invoke-static {v0, v11}, LX/0cX;->a(Ljava/lang/Exception;Z)LX/73z;

    move-result-object v5

    .line 1340047
    iget-object v1, p2, LX/8Ob;->A:LX/74b;

    iget-wide v2, p2, LX/8Ob;->m:J

    move-object v0, p3

    move v4, v10

    invoke-virtual/range {v0 .. v5}, LX/73w;->a(LX/74b;JZLX/73y;)V

    move v5, v10

    .line 1340048
    goto :goto_5

    .line 1340049
    :catch_1
    move-exception v0

    .line 1340050
    sget-object v1, LX/8Oh;->a:Ljava/lang/Class;

    const-string v2, "Error estimating transcoded file size"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1340051
    invoke-static {v0, v11}, LX/0cX;->a(Ljava/lang/Exception;Z)LX/73z;

    move-result-object v5

    .line 1340052
    iget-object v1, p2, LX/8Ob;->A:LX/74b;

    iget-wide v2, p2, LX/8Ob;->m:J

    move-object v0, p3

    move v4, v10

    invoke-virtual/range {v0 .. v5}, LX/73w;->a(LX/74b;JZLX/73y;)V

    move v5, v10

    goto :goto_5

    :cond_6
    :try_start_1
    const/4 v0, 0x0

    goto/16 :goto_2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1340053
    :cond_7
    iget-object v1, p0, LX/8Oh;->i:LX/2Mj;

    invoke-virtual {v1, v0, v4, v3}, LX/2Mj;->a(LX/60x;II)LX/7Sw;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(ZLX/8Ob;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1340008
    if-eqz p0, :cond_1

    .line 1340009
    :cond_0
    :goto_0
    return v0

    .line 1340010
    :cond_1
    iget-object v3, p1, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v3, v3, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    .line 1340011
    iget-boolean v4, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    if-eqz v4, :cond_3

    iget-wide v3, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    const-wide/16 v5, 0x2

    cmp-long v3, v3, v5

    if-gez v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    move v2, v3

    .line 1340012
    if-eqz v2, :cond_2

    move v0, v1

    .line 1340013
    goto :goto_0

    .line 1340014
    :cond_2
    iget-boolean v2, p1, LX/8Ob;->Q:Z

    if-eqz v2, :cond_0

    invoke-static {p1}, LX/8Oh;->c(LX/8Ob;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1340015
    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static c(LX/8Ob;)Z
    .locals 8

    .prologue
    .line 1340002
    iget-object v0, p0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v0, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    .line 1340003
    iget-wide v2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    .line 1340004
    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    iget-wide v6, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    iget-wide v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    add-long/2addr v0, v6

    sub-long v0, v4, v0

    .line 1340005
    iget-wide v4, p0, LX/8Ob;->R:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-wide v2, p0, LX/8Ob;->S:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 1340006
    :cond_0
    const/4 v0, 0x1

    .line 1340007
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/8Oh;LX/8Ob;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x5

    .line 1339997
    iget-object v0, p0, LX/8Oh;->j:LX/0ad;

    const v1, -0x67a4

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1339998
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/8Ob;->Q:Z

    .line 1339999
    iget-object v0, p0, LX/8Oh;->j:LX/0ad;

    sget-wide v2, LX/0bz;->BG:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p1, LX/8Ob;->R:J

    .line 1340000
    iget-object v0, p0, LX/8Oh;->j:LX/0ad;

    sget-wide v2, LX/0bz;->BF:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p1, LX/8Ob;->S:J

    .line 1340001
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/VideoItem;)LX/60x;
    .locals 2

    .prologue
    .line 1339994
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339995
    iget-object v0, p0, LX/8Oh;->h:LX/2MV;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v0

    .line 1339996
    return-object v0
.end method

.method public final a(LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ok;LX/8OL;)LX/8OV;
    .locals 19

    .prologue
    .line 1339971
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Oh;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/facebook/photos/upload/operation/UploadOperation;->b(J)V

    .line 1339972
    move-object/from16 v0, p1

    iget-wide v4, v0, LX/8Ob;->r:J

    move-object/from16 v0, p1

    iget v6, v0, LX/8Ob;->t:I

    invoke-static {v4, v5, v6}, LX/8OV;->a(JI)LX/8OV;

    move-result-object v17

    .line 1339973
    move-object/from16 v0, v17

    iget v0, v0, LX/8OV;->a:I

    move/from16 v18, v0

    .line 1339974
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/8Ok;->a(Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    .line 1339975
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8Ob;->A:LX/74b;

    invoke-virtual/range {v17 .. v17}, LX/8OV;->b()I

    move-result v5

    move-object/from16 v0, p1

    iget-wide v6, v0, LX/8Ob;->r:J

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5, v6, v7}, LX/73w;->a(LX/74b;IJ)V

    .line 1339976
    const-wide/16 v14, 0x0

    .line 1339977
    const/4 v4, 0x0

    move/from16 v16, v4

    :goto_0
    invoke-virtual/range {v17 .. v17}, LX/8OV;->b()I

    move-result v4

    move/from16 v0, v16

    if-ge v0, v4, :cond_2

    .line 1339978
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/8OV;->a(I)LX/8Oa;

    move-result-object v10

    .line 1339979
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 1339980
    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/8Ok;->a(I)V

    :cond_0
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    .line 1339981
    invoke-static/range {v4 .. v10}, LX/8Oh;->a(LX/8Oh;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ok;LX/8OL;LX/8Oa;)LX/8OX;

    move-result-object v4

    .line 1339982
    if-eqz v4, :cond_1

    .line 1339983
    iget-object v11, v4, LX/8OX;->a:Ljava/lang/String;

    iget-wide v12, v4, LX/8OX;->b:J

    invoke-virtual/range {v10 .. v15}, LX/8Oa;->a(Ljava/lang/String;JJ)V

    .line 1339984
    iget-wide v14, v10, LX/8Oa;->f:J

    .line 1339985
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    goto :goto_0

    .line 1339986
    :cond_1
    invoke-virtual/range {p4 .. p4}, LX/8Ok;->b()V

    .line 1339987
    move-object/from16 v0, p1

    iget-object v6, v0, LX/8Ob;->A:LX/74b;

    invoke-virtual/range {v17 .. v17}, LX/8OV;->b()I

    move-result v7

    move-object/from16 v0, p1

    iget-wide v8, v0, LX/8Ob;->r:J

    iget-object v4, v10, LX/8Oa;->c:LX/8OZ;

    invoke-virtual {v4}, LX/8OZ;->getValue()I

    move-result v11

    iget-object v4, v10, LX/8Oa;->g:LX/7T7;

    iget v12, v4, LX/7T7;->d:I

    iget-object v4, v10, LX/8Oa;->g:LX/7T7;

    iget v13, v4, LX/7T7;->e:I

    move-object/from16 v5, p3

    move/from16 v10, v16

    invoke-virtual/range {v5 .. v13}, LX/73w;->a(LX/74b;IJIIII)V

    .line 1339988
    const/4 v4, 0x0

    .line 1339989
    :goto_1
    return-object v4

    .line 1339990
    :cond_2
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->d(Z)V

    .line 1339991
    invoke-virtual/range {p4 .. p4}, LX/8Ok;->b()V

    .line 1339992
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8Ob;->A:LX/74b;

    invoke-virtual/range {v17 .. v17}, LX/8OV;->b()I

    move-result v5

    move-object/from16 v0, p1

    iget-wide v6, v0, LX/8Ob;->r:J

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5, v6, v7}, LX/73w;->b(LX/74b;IJ)V

    move-object/from16 v4, v17

    .line 1339993
    goto :goto_1
.end method

.method public final a(LX/8Ob;LX/73w;Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1339939
    iget-object v2, p1, LX/8Ob;->V:LX/8OY;

    .line 1339940
    iget-boolean v3, v2, LX/8OY;->c:Z

    move v2, v3

    .line 1339941
    if-eqz v2, :cond_1

    iget-boolean v2, p1, LX/8Ob;->I:Z

    if-nez v2, :cond_1

    move v0, v1

    .line 1339942
    :cond_0
    :goto_0
    return v0

    .line 1339943
    :cond_1
    iget-object v2, p2, LX/73w;->j:Ljava/lang/String;

    move-object v2, v2

    .line 1339944
    iget-object v3, p1, LX/8Ob;->q:Ljava/lang/String;

    invoke-static {p0, v2, v3}, LX/8Oh;->a(LX/8Oh;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1339945
    invoke-static {p1, p2, v2}, LX/8Oh;->a(LX/8Ob;LX/73w;Ljava/io/File;)LX/8OX;

    move-result-object v2

    .line 1339946
    if-eqz v2, :cond_8

    .line 1339947
    iget-object v2, v2, LX/8OX;->a:Ljava/lang/String;

    iput-object v2, p1, LX/8Ob;->j:Ljava/lang/String;

    .line 1339948
    iput-boolean v0, p3, Lcom/facebook/photos/upload/operation/UploadOperation;->R:Z

    .line 1339949
    move v3, v0

    .line 1339950
    :goto_1
    if-nez v3, :cond_7

    .line 1339951
    iget-object v2, p1, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v2, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    iget-wide v4, v2, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_2

    .line 1339952
    iget-object v2, p1, LX/8Ob;->A:LX/74b;

    iget-object v4, p1, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v4, v4, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    iget-wide v4, v4, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    .line 1339953
    invoke-virtual {v2}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v6

    .line 1339954
    const-string v7, "video_transcode_success_count"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339955
    sget-object v7, LX/74R;->MEDIA_UPLOAD_PROCESS_MISSING_EXISTING_FILE:LX/74R;

    const/4 v8, 0x0

    invoke-static {p2, v7, v6, v8}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1339956
    :cond_2
    iget-boolean v2, p1, LX/8Ob;->I:Z

    if-nez v2, :cond_7

    .line 1339957
    invoke-static {p1}, LX/8Oh;->a(LX/8Ob;)Z

    move-result v2

    .line 1339958
    :goto_2
    if-nez v3, :cond_3

    if-eqz v2, :cond_4

    .line 1339959
    :cond_3
    :goto_3
    if-nez v0, :cond_6

    .line 1339960
    iget-object v0, p0, LX/8Oh;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1339961
    invoke-static {p0, p3, p1, p2}, LX/8Oh;->a(LX/8Oh;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;LX/73w;)Z

    move-result v1

    move v0, v1

    .line 1339962
    :goto_4
    if-eqz v0, :cond_0

    .line 1339963
    invoke-static {p0, p1}, LX/8Oh;->d(LX/8Oh;LX/8Ob;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1339964
    goto :goto_3

    .line 1339965
    :cond_5
    iget-object v0, p1, LX/8Ob;->A:LX/74b;

    iget-wide v2, p1, LX/8Ob;->m:J

    .line 1339966
    invoke-virtual {v0}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 1339967
    const-string v5, "original_file_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339968
    const-string v5, "attempt_video_resize"

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339969
    sget-object v5, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SKIPPED:LX/74R;

    const/4 v6, 0x0

    invoke-static {p2, v5, v4, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1339970
    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    move v2, v1

    goto :goto_2

    :cond_8
    move v3, v1

    goto :goto_1
.end method

.method public final b(LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ok;LX/8OL;)LX/8OX;
    .locals 10

    .prologue
    .line 1339929
    iget-object v0, p0, LX/8Oh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->b(J)V

    .line 1339930
    iget-object v5, p1, LX/8Ob;->q:Ljava/lang/String;

    .line 1339931
    const/4 v0, 0x1

    invoke-virtual {p4, p2, v0}, LX/8Ok;->a(Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    .line 1339932
    iget-object v0, p3, LX/73w;->j:Ljava/lang/String;

    move-object v6, v0

    .line 1339933
    iget-object v0, p1, LX/8Ob;->q:Ljava/lang/String;

    invoke-static {p0, v6, v0}, LX/8Oh;->a(LX/8Oh;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 1339934
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v8, p4

    move-object v9, p5

    invoke-static/range {v0 .. v9}, LX/8Oh;->a(LX/8Oh;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Oa;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;LX/8Ok;LX/8OL;)LX/8OX;

    move-result-object v0

    .line 1339935
    if-eqz v0, :cond_0

    .line 1339936
    const/4 v1, 0x1

    .line 1339937
    iput-boolean v1, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->R:Z

    .line 1339938
    :cond_0
    return-object v0
.end method
