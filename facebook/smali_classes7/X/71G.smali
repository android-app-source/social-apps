.class public LX/71G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6vm;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/71F;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/71F;)V
    .locals 1

    .prologue
    .line 1163193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163194
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1163195
    iput-object p1, p0, LX/71G;->a:Ljava/lang/String;

    .line 1163196
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/71F;

    iput-object v0, p0, LX/71G;->b:LX/71F;

    .line 1163197
    return-void

    .line 1163198
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/71I;
    .locals 1

    .prologue
    .line 1163199
    sget-object v0, LX/71I;->SECURITY_FOOTER:LX/71I;

    return-object v0
.end method
