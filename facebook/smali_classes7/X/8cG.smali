.class public LX/8cG;
.super LX/2TZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "LX/8cM;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 1374196
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 1374197
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1374198
    new-instance v0, LX/8cL;

    invoke-direct {v0}, LX/8cL;-><init>()V

    move-object v0, v0

    .line 1374199
    sget-object v1, LX/7Bv;->b:LX/0U1;

    .line 1374200
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1374201
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1374202
    iput-object v1, v0, LX/8cL;->a:Ljava/lang/String;

    .line 1374203
    move-object v0, v0

    .line 1374204
    sget-object v1, LX/7Bv;->c:LX/0U1;

    .line 1374205
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1374206
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1374207
    iput-object v1, v0, LX/8cL;->e:Ljava/lang/String;

    .line 1374208
    move-object v0, v0

    .line 1374209
    sget-object v1, LX/7Bv;->d:LX/0U1;

    .line 1374210
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1374211
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1374212
    iput-object v1, v0, LX/8cL;->b:Ljava/lang/String;

    .line 1374213
    move-object v0, v0

    .line 1374214
    sget-object v1, LX/7Bv;->e:LX/0U1;

    .line 1374215
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1374216
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1374217
    iput-object v1, v0, LX/8cL;->c:Ljava/lang/String;

    .line 1374218
    move-object v0, v0

    .line 1374219
    sget-object v1, LX/7Bv;->f:LX/0U1;

    .line 1374220
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1374221
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1374222
    iput-object v1, v0, LX/8cL;->d:Ljava/lang/String;

    .line 1374223
    move-object v0, v0

    .line 1374224
    sget-object v1, LX/7Bv;->g:LX/0U1;

    .line 1374225
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1374226
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    .line 1374227
    iput-wide v2, v0, LX/8cL;->f:D

    .line 1374228
    move-object v0, v0

    .line 1374229
    sget-object v1, LX/7Bv;->h:LX/0U1;

    .line 1374230
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1374231
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1374232
    iput-object v1, v0, LX/8cL;->g:Ljava/lang/String;

    .line 1374233
    move-object v0, v0

    .line 1374234
    new-instance v1, LX/8cM;

    invoke-direct {v1, v0}, LX/8cM;-><init>(LX/8cL;)V

    move-object v0, v1

    .line 1374235
    return-object v0
.end method
