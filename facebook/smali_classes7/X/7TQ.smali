.class public final enum LX/7TQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7TQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7TQ;

.field public static final enum CallEndAcceptAfterHangUp:LX/7TQ;

.field public static final enum CallEndCallerNotVisible:LX/7TQ;

.field public static final enum CallEndCarrierBlocked:LX/7TQ;

.field public static final enum CallEndClientEncryptionError:LX/7TQ;

.field public static final enum CallEndClientError:LX/7TQ;

.field public static final enum CallEndClientInterrupted:LX/7TQ;

.field public static final enum CallEndConnectionDropped:LX/7TQ;

.field public static final enum CallEndHangupCall:LX/7TQ;

.field public static final enum CallEndIgnoreCall:LX/7TQ;

.field public static final enum CallEndInAnotherCall:LX/7TQ;

.field public static final enum CallEndIncomingTimeout:LX/7TQ;

.field public static final enum CallEndNoAnswerTimeout:LX/7TQ;

.field public static final enum CallEndNoPermission:LX/7TQ;

.field public static final enum CallEndNoUIError:LX/7TQ;

.field public static final enum CallEndOtherCarrierBlocked:LX/7TQ;

.field public static final enum CallEndOtherInstanceHandled:LX/7TQ;

.field public static final enum CallEndOtherNotCapable:LX/7TQ;

.field public static final enum CallEndSignalingMessageFailed:LX/7TQ;

.field public static final enum CallEndUnsupportedVersion:LX/7TQ;

.field public static final enum CallEndWebRTCError:LX/7TQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1210610
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndIgnoreCall"

    invoke-direct {v0, v1, v3}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    .line 1210611
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndHangupCall"

    invoke-direct {v0, v1, v4}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    .line 1210612
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndInAnotherCall"

    invoke-direct {v0, v1, v5}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndInAnotherCall:LX/7TQ;

    .line 1210613
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndAcceptAfterHangUp"

    invoke-direct {v0, v1, v6}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndAcceptAfterHangUp:LX/7TQ;

    .line 1210614
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndNoAnswerTimeout"

    invoke-direct {v0, v1, v7}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndNoAnswerTimeout:LX/7TQ;

    .line 1210615
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndIncomingTimeout"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndIncomingTimeout:LX/7TQ;

    .line 1210616
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndOtherInstanceHandled"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndOtherInstanceHandled:LX/7TQ;

    .line 1210617
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndSignalingMessageFailed"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndSignalingMessageFailed:LX/7TQ;

    .line 1210618
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndConnectionDropped"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndConnectionDropped:LX/7TQ;

    .line 1210619
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndClientInterrupted"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndClientInterrupted:LX/7TQ;

    .line 1210620
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndWebRTCError"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndWebRTCError:LX/7TQ;

    .line 1210621
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndClientError"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndClientError:LX/7TQ;

    .line 1210622
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndNoPermission"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    .line 1210623
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndOtherNotCapable"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndOtherNotCapable:LX/7TQ;

    .line 1210624
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndNoUIError"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndNoUIError:LX/7TQ;

    .line 1210625
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndUnsupportedVersion"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndUnsupportedVersion:LX/7TQ;

    .line 1210626
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndCallerNotVisible"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndCallerNotVisible:LX/7TQ;

    .line 1210627
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndCarrierBlocked"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndCarrierBlocked:LX/7TQ;

    .line 1210628
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndOtherCarrierBlocked"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndOtherCarrierBlocked:LX/7TQ;

    .line 1210629
    new-instance v0, LX/7TQ;

    const-string v1, "CallEndClientEncryptionError"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/7TQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TQ;->CallEndClientEncryptionError:LX/7TQ;

    .line 1210630
    const/16 v0, 0x14

    new-array v0, v0, [LX/7TQ;

    sget-object v1, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    aput-object v1, v0, v3

    sget-object v1, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    aput-object v1, v0, v4

    sget-object v1, LX/7TQ;->CallEndInAnotherCall:LX/7TQ;

    aput-object v1, v0, v5

    sget-object v1, LX/7TQ;->CallEndAcceptAfterHangUp:LX/7TQ;

    aput-object v1, v0, v6

    sget-object v1, LX/7TQ;->CallEndNoAnswerTimeout:LX/7TQ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7TQ;->CallEndIncomingTimeout:LX/7TQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7TQ;->CallEndOtherInstanceHandled:LX/7TQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7TQ;->CallEndSignalingMessageFailed:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7TQ;->CallEndConnectionDropped:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7TQ;->CallEndClientInterrupted:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7TQ;->CallEndWebRTCError:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7TQ;->CallEndClientError:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7TQ;->CallEndOtherNotCapable:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7TQ;->CallEndNoUIError:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7TQ;->CallEndUnsupportedVersion:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7TQ;->CallEndCallerNotVisible:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7TQ;->CallEndCarrierBlocked:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7TQ;->CallEndOtherCarrierBlocked:LX/7TQ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/7TQ;->CallEndClientEncryptionError:LX/7TQ;

    aput-object v2, v0, v1

    sput-object v0, LX/7TQ;->$VALUES:[LX/7TQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1210631
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7TQ;
    .locals 1

    .prologue
    .line 1210608
    const-class v0, LX/7TQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7TQ;

    return-object v0
.end method

.method public static values()[LX/7TQ;
    .locals 1

    .prologue
    .line 1210609
    sget-object v0, LX/7TQ;->$VALUES:[LX/7TQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7TQ;

    return-object v0
.end method
