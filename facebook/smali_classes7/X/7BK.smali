.class public final enum LX/7BK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7BK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7BK;

.field public static final enum APP:LX/7BK;

.field public static final enum EVENT:LX/7BK;

.field public static final enum GROUP:LX/7BK;

.field public static final enum HASHTAG_EXACT:LX/7BK;

.field public static final enum KEYWORD_SUGGESTION:LX/7BK;

.field public static final enum PAGE:LX/7BK;

.field public static final enum PAGE_ADMIN:LX/7BK;

.field public static final enum PAGE_FAN:LX/7BK;

.field public static final enum SHORTCUT:LX/7BK;

.field public static final enum USER:LX/7BK;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1178250
    new-instance v0, LX/7BK;

    const-string v1, "USER"

    invoke-direct {v0, v1, v3}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->USER:LX/7BK;

    .line 1178251
    new-instance v0, LX/7BK;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v4}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->PAGE:LX/7BK;

    .line 1178252
    new-instance v0, LX/7BK;

    const-string v1, "APP"

    invoke-direct {v0, v1, v5}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->APP:LX/7BK;

    .line 1178253
    new-instance v0, LX/7BK;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v6}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->GROUP:LX/7BK;

    .line 1178254
    new-instance v0, LX/7BK;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v7}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->EVENT:LX/7BK;

    .line 1178255
    new-instance v0, LX/7BK;

    const-string v1, "SHORTCUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->SHORTCUT:LX/7BK;

    .line 1178256
    new-instance v0, LX/7BK;

    const-string v1, "PAGE_FAN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->PAGE_FAN:LX/7BK;

    .line 1178257
    new-instance v0, LX/7BK;

    const-string v1, "PAGE_ADMIN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->PAGE_ADMIN:LX/7BK;

    .line 1178258
    new-instance v0, LX/7BK;

    const-string v1, "HASHTAG_EXACT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->HASHTAG_EXACT:LX/7BK;

    .line 1178259
    new-instance v0, LX/7BK;

    const-string v1, "KEYWORD_SUGGESTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7BK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7BK;->KEYWORD_SUGGESTION:LX/7BK;

    .line 1178260
    const/16 v0, 0xa

    new-array v0, v0, [LX/7BK;

    sget-object v1, LX/7BK;->USER:LX/7BK;

    aput-object v1, v0, v3

    sget-object v1, LX/7BK;->PAGE:LX/7BK;

    aput-object v1, v0, v4

    sget-object v1, LX/7BK;->APP:LX/7BK;

    aput-object v1, v0, v5

    sget-object v1, LX/7BK;->GROUP:LX/7BK;

    aput-object v1, v0, v6

    sget-object v1, LX/7BK;->EVENT:LX/7BK;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7BK;->SHORTCUT:LX/7BK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7BK;->PAGE_FAN:LX/7BK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7BK;->PAGE_ADMIN:LX/7BK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7BK;->HASHTAG_EXACT:LX/7BK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7BK;->KEYWORD_SUGGESTION:LX/7BK;

    aput-object v2, v0, v1

    sput-object v0, LX/7BK;->$VALUES:[LX/7BK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1178261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7BK;
    .locals 1

    .prologue
    .line 1178262
    const-class v0, LX/7BK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7BK;

    return-object v0
.end method

.method public static values()[LX/7BK;
    .locals 1

    .prologue
    .line 1178263
    sget-object v0, LX/7BK;->$VALUES:[LX/7BK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7BK;

    return-object v0
.end method
