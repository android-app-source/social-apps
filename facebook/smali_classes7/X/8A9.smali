.class public final enum LX/8A9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8A9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8A9;

.field public static final enum LAUNCH_AD_IMAGE_CROPPER:LX/8A9;

.field public static final enum LAUNCH_COMPOSER:LX/8A9;

.field public static final enum LAUNCH_COMPOSER_ALBUM_CREATOR:LX/8A9;

.field public static final enum LAUNCH_COVER_PIC_CROPPER:LX/8A9;

.field public static final enum LAUNCH_FUNDRAISER_COVER_PHOTO_CROPPER:LX/8A9;

.field public static final enum LAUNCH_GENERIC_CROPPER:LX/8A9;

.field public static final enum LAUNCH_PLACE_PICKER:LX/8A9;

.field public static final enum LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

.field public static final enum LAUNCH_PROFILE_PIC_EDIT_GALLERY:LX/8A9;

.field public static final enum LAUNCH_SLIDESHOW_EDIT_ACTIVITY:LX/8A9;

.field public static final enum NONE:LX/8A9;

.field public static final enum RETURN_TO_STAGING_GROUND:LX/8A9;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1305769
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_FUNDRAISER_COVER_PHOTO_CROPPER"

    invoke-direct {v0, v1, v3}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_FUNDRAISER_COVER_PHOTO_CROPPER:LX/8A9;

    .line 1305770
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_PROFILE_PIC_CROPPER"

    invoke-direct {v0, v1, v4}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    .line 1305771
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_PROFILE_PIC_EDIT_GALLERY"

    invoke-direct {v0, v1, v5}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_PROFILE_PIC_EDIT_GALLERY:LX/8A9;

    .line 1305772
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_AD_IMAGE_CROPPER"

    invoke-direct {v0, v1, v6}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_AD_IMAGE_CROPPER:LX/8A9;

    .line 1305773
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_GENERIC_CROPPER"

    invoke-direct {v0, v1, v7}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_GENERIC_CROPPER:LX/8A9;

    .line 1305774
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_COMPOSER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_COMPOSER:LX/8A9;

    .line 1305775
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_COMPOSER_ALBUM_CREATOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_COMPOSER_ALBUM_CREATOR:LX/8A9;

    .line 1305776
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_PLACE_PICKER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_PLACE_PICKER:LX/8A9;

    .line 1305777
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_COVER_PIC_CROPPER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_COVER_PIC_CROPPER:LX/8A9;

    .line 1305778
    new-instance v0, LX/8A9;

    const-string v1, "LAUNCH_SLIDESHOW_EDIT_ACTIVITY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->LAUNCH_SLIDESHOW_EDIT_ACTIVITY:LX/8A9;

    .line 1305779
    new-instance v0, LX/8A9;

    const-string v1, "RETURN_TO_STAGING_GROUND"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->RETURN_TO_STAGING_GROUND:LX/8A9;

    .line 1305780
    new-instance v0, LX/8A9;

    const-string v1, "NONE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8A9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A9;->NONE:LX/8A9;

    .line 1305781
    const/16 v0, 0xc

    new-array v0, v0, [LX/8A9;

    sget-object v1, LX/8A9;->LAUNCH_FUNDRAISER_COVER_PHOTO_CROPPER:LX/8A9;

    aput-object v1, v0, v3

    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    aput-object v1, v0, v4

    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_EDIT_GALLERY:LX/8A9;

    aput-object v1, v0, v5

    sget-object v1, LX/8A9;->LAUNCH_AD_IMAGE_CROPPER:LX/8A9;

    aput-object v1, v0, v6

    sget-object v1, LX/8A9;->LAUNCH_GENERIC_CROPPER:LX/8A9;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8A9;->LAUNCH_COMPOSER:LX/8A9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8A9;->LAUNCH_COMPOSER_ALBUM_CREATOR:LX/8A9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8A9;->LAUNCH_PLACE_PICKER:LX/8A9;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8A9;->LAUNCH_COVER_PIC_CROPPER:LX/8A9;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8A9;->LAUNCH_SLIDESHOW_EDIT_ACTIVITY:LX/8A9;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8A9;->RETURN_TO_STAGING_GROUND:LX/8A9;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    aput-object v2, v0, v1

    sput-object v0, LX/8A9;->$VALUES:[LX/8A9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1305782
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8A9;
    .locals 1

    .prologue
    .line 1305783
    const-class v0, LX/8A9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8A9;

    return-object v0
.end method

.method public static values()[LX/8A9;
    .locals 1

    .prologue
    .line 1305784
    sget-object v0, LX/8A9;->$VALUES:[LX/8A9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8A9;

    return-object v0
.end method
