.class public final LX/7ug;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1272331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 1272332
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1272333
    iget-object v1, p0, LX/7ug;->a:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1272334
    iget-object v3, p0, LX/7ug;->b:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1272335
    iget-object v5, p0, LX/7ug;->c:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1272336
    iget-object v6, p0, LX/7ug;->d:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1272337
    iget-object v7, p0, LX/7ug;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1272338
    iget-object v8, p0, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1272339
    iget-object v9, p0, LX/7ug;->h:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1272340
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1272341
    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 1272342
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1272343
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1272344
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1272345
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1272346
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1272347
    const/4 v1, 0x6

    iget-boolean v3, p0, LX/7ug;->g:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1272348
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1272349
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1272350
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1272351
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1272352
    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1272353
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1272354
    new-instance v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    invoke-direct {v1, v0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;-><init>(LX/15i;)V

    .line 1272355
    return-object v1
.end method
