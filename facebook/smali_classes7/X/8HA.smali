.class public LX/8HA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/EditPhotoCaptionParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320569
    const-class v0, LX/8HA;

    sput-object v0, LX/8HA;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320571
    return-void
.end method

.method public static a(LX/0QB;)LX/8HA;
    .locals 1

    .prologue
    .line 1320572
    new-instance v0, LX/8HA;

    invoke-direct {v0}, LX/8HA;-><init>()V

    .line 1320573
    move-object v0, v0

    .line 1320574
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320575
    check-cast p1, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;

    .line 1320576
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1320577
    iget-object v0, p1, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->b:LX/175;

    move-object v0, v0

    .line 1320578
    if-nez v0, :cond_0

    const-string v0, " "

    .line 1320579
    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "name"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320580
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/8HA;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1320581
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1320582
    move-object v0, v0

    .line 1320583
    const-string v2, "POST"

    .line 1320584
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1320585
    move-object v0, v0

    .line 1320586
    iget-object v2, p1, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1320587
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1320588
    move-object v0, v0

    .line 1320589
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1320590
    move-object v0, v0

    .line 1320591
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320592
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320593
    move-object v0, v0

    .line 1320594
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1320595
    :cond_0
    invoke-static {v0}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1320596
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1320597
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320598
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
