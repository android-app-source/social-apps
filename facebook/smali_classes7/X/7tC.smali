.class public final LX/7tC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1267149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1267150
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1267151
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1267152
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1267153
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1267154
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267155
    :goto_1
    move v1, v2

    .line 1267156
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1267157
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1267158
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1267159
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1267160
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1267161
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1267162
    const-string v9, "duration_min"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1267163
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_2

    .line 1267164
    :cond_2
    const-string v9, "product"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1267165
    const/4 v8, 0x0

    .line 1267166
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_d

    .line 1267167
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267168
    :goto_3
    move v6, v8

    .line 1267169
    goto :goto_2

    .line 1267170
    :cond_3
    const-string v9, "time_start"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1267171
    invoke-static {p0, p1}, LX/7sy;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_2

    .line 1267172
    :cond_4
    const-string v9, "timeslot_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1267173
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 1267174
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1267175
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1267176
    if-eqz v1, :cond_7

    .line 1267177
    invoke-virtual {p1, v2, v7, v2}, LX/186;->a(III)V

    .line 1267178
    :cond_7
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1267179
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1267180
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1267181
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2

    .line 1267182
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267183
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_c

    .line 1267184
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1267185
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1267186
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_a

    if-eqz v10, :cond_a

    .line 1267187
    const-string v11, "price"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1267188
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 1267189
    :cond_b
    const-string v11, "provider"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1267190
    const/4 v10, 0x0

    .line 1267191
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v11, :cond_11

    .line 1267192
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267193
    :goto_5
    move v6, v10

    .line 1267194
    goto :goto_4

    .line 1267195
    :cond_c
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1267196
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 1267197
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v6}, LX/186;->b(II)V

    .line 1267198
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_d
    move v6, v8

    move v9, v8

    goto :goto_4

    .line 1267199
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267200
    :cond_f
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_10

    .line 1267201
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1267202
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1267203
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_f

    if-eqz v11, :cond_f

    .line 1267204
    const-string v12, "plain_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 1267205
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_6

    .line 1267206
    :cond_10
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1267207
    invoke-virtual {p1, v10, v6}, LX/186;->b(II)V

    .line 1267208
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_5

    :cond_11
    move v6, v10

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1267209
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1267210
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 1267211
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 v2, 0x0

    .line 1267212
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267213
    invoke-virtual {p0, v1, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 1267214
    if-eqz v2, :cond_0

    .line 1267215
    const-string v3, "duration_min"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267216
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1267217
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1267218
    if-eqz v2, :cond_4

    .line 1267219
    const-string v3, "product"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267220
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267221
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1267222
    if-eqz v3, :cond_1

    .line 1267223
    const-string v4, "price"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267224
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267225
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1267226
    if-eqz v3, :cond_3

    .line 1267227
    const-string v4, "provider"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267228
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267229
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1267230
    if-eqz v4, :cond_2

    .line 1267231
    const-string v2, "plain_text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267232
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267233
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267234
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267235
    :cond_4
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1267236
    if-eqz v2, :cond_6

    .line 1267237
    const-string v3, "time_start"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267238
    const-wide/16 v6, 0x0

    .line 1267239
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267240
    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v4

    .line 1267241
    cmp-long v6, v4, v6

    if-eqz v6, :cond_5

    .line 1267242
    const-string v6, "time"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267243
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(J)V

    .line 1267244
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267245
    :cond_6
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1267246
    if-eqz v2, :cond_7

    .line 1267247
    const-string v3, "timeslot_id"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267248
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267249
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267250
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 1267251
    :cond_8
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1267252
    return-void
.end method
