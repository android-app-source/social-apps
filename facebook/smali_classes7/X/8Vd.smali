.class public LX/8Vd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Vc;


# instance fields
.field private final a:LX/3Rb;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Rb;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Rb;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1353194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1353195
    iput-object p1, p0, LX/8Vd;->a:LX/3Rb;

    .line 1353196
    iput-object p2, p0, LX/8Vd;->b:LX/0Px;

    .line 1353197
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1353193
    invoke-virtual {p0}, LX/8Vd;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(III)LX/1bf;
    .locals 2

    .prologue
    .line 1353192
    iget-object v1, p0, LX/8Vd;->a:LX/3Rb;

    iget-object v0, p0, LX/8Vd;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0, p2, p3}, LX/3Rb;->a(LX/8t9;II)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public final b(III)LX/1bf;
    .locals 1

    .prologue
    .line 1353191
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()LX/8ue;
    .locals 1

    .prologue
    .line 1353198
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1353190
    const/4 v0, 0x0

    return v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1353189
    iget-object v0, p0, LX/8Vd;->b:LX/0Px;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1353187
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 1353188
    const/4 v0, 0x0

    return v0
.end method
