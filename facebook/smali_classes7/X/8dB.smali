.class public interface abstract LX/8dB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8cx;
.implements LX/8cy;
.implements LX/8cz;


# virtual methods
.method public abstract D()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract E()LX/8d5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract F()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract G()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract I()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract J()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract K()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dW_()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()Z
.end method
