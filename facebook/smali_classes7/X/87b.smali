.class public final enum LX/87b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/87b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/87b;

.field public static final enum DASHBOARD:LX/87b;

.field public static final enum INTERN_SETTINGS:LX/87b;

.field public static final enum NEWSFEED:LX/87b;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1300451
    new-instance v0, LX/87b;

    const-string v1, "DASHBOARD"

    const-string v2, "dashboard"

    invoke-direct {v0, v1, v3, v2}, LX/87b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/87b;->DASHBOARD:LX/87b;

    .line 1300452
    new-instance v0, LX/87b;

    const-string v1, "INTERN_SETTINGS"

    const-string v2, "intern_settings"

    invoke-direct {v0, v1, v4, v2}, LX/87b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/87b;->INTERN_SETTINGS:LX/87b;

    .line 1300453
    new-instance v0, LX/87b;

    const-string v1, "NEWSFEED"

    const-string v2, "newsfeed"

    invoke-direct {v0, v1, v5, v2}, LX/87b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/87b;->NEWSFEED:LX/87b;

    .line 1300454
    const/4 v0, 0x3

    new-array v0, v0, [LX/87b;

    sget-object v1, LX/87b;->DASHBOARD:LX/87b;

    aput-object v1, v0, v3

    sget-object v1, LX/87b;->INTERN_SETTINGS:LX/87b;

    aput-object v1, v0, v4

    sget-object v1, LX/87b;->NEWSFEED:LX/87b;

    aput-object v1, v0, v5

    sput-object v0, LX/87b;->$VALUES:[LX/87b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1300455
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1300456
    iput-object p3, p0, LX/87b;->value:Ljava/lang/String;

    .line 1300457
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/87b;
    .locals 1

    .prologue
    .line 1300458
    const-class v0, LX/87b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/87b;

    return-object v0
.end method

.method public static values()[LX/87b;
    .locals 1

    .prologue
    .line 1300459
    sget-object v0, LX/87b;->$VALUES:[LX/87b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/87b;

    return-object v0
.end method
