.class public LX/7ZO;
.super LX/2wH;
.source ""

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wH",
        "<",
        "LX/7ZR;",
        ">;",
        "Landroid/os/IBinder$DeathRecipient;"
    }
.end annotation


# static fields
.field private static final d:LX/7Z9;


# instance fields
.field private e:LX/7Yt;

.field private f:Lcom/google/android/gms/cast/CastDevice;

.field private g:Landroid/os/Bundle;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, LX/7Z9;

    const-string v1, "CastRemoteDisplayClientImpl"

    invoke-direct {v0, v1}, LX/7Z9;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/7ZO;->d:LX/7Z9;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Lcom/google/android/gms/cast/CastDevice;Landroid/os/Bundle;LX/7Yt;LX/1qf;LX/1qg;)V
    .locals 7

    const/16 v3, 0x53

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    sget-object v0, LX/7ZO;->d:LX/7Z9;

    const-string v1, "instance created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object p6, p0, LX/7ZO;->e:LX/7Yt;

    iput-object p4, p0, LX/7ZO;->f:Lcom/google/android/gms/cast/CastDevice;

    iput-object p5, p0, LX/7ZO;->g:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.cast.remote_display.ICastRemoteDisplayService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/7ZR;

    if-eqz p0, :cond_1

    check-cast v0, LX/7ZR;

    goto :goto_0

    :cond_1
    new-instance v0, LX/7ZS;

    invoke-direct {v0, p1}, LX/7ZS;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.remote_display.service.START"

    return-object v0
.end method

.method public final binderDied()V
    .locals 0

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.remote_display.ICastRemoteDisplayService"

    return-object v0
.end method

.method public final f()V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, LX/7ZO;->d:LX/7Z9;

    const-string v1, "disconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v3, p0, LX/7ZO;->e:LX/7Yt;

    iput-object v3, p0, LX/7ZO;->f:Lcom/google/android/gms/cast/CastDevice;

    :try_start_0
    invoke-virtual {p0}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/7ZR;

    invoke-interface {v0}, LX/7ZR;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, LX/2wH;->f()V

    :goto_0
    return-void

    :catch_0
    :goto_1
    invoke-super {p0}, LX/2wH;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, LX/2wH;->f()V

    throw v0

    :catch_1
    goto :goto_1
.end method
