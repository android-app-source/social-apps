.class public LX/6hN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static volatile l:LX/6hN;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Landroid/view/WindowManager;

.field private final d:LX/0Uh;

.field private final e:LX/6lH;

.field private final f:LX/6hL;

.field private volatile g:I

.field private volatile h:I

.field private volatile i:I

.field private volatile j:Landroid/graphics/Point;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128126
    const-class v0, LX/6hN;

    sput-object v0, LX/6hN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;LX/0Uh;LX/6hL;LX/6lH;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/photos/size/IsAttachmentSizeControlEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/WindowManager;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/6hL;",
            "LX/6lH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1128150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128151
    iput-object p1, p0, LX/6hN;->b:Landroid/content/Context;

    .line 1128152
    iput-object p2, p0, LX/6hN;->c:Landroid/view/WindowManager;

    .line 1128153
    iput-object p3, p0, LX/6hN;->d:LX/0Uh;

    .line 1128154
    iput-object p4, p0, LX/6hN;->f:LX/6hL;

    .line 1128155
    iput-object p5, p0, LX/6hN;->e:LX/6lH;

    .line 1128156
    iput-object p6, p0, LX/6hN;->k:LX/0Or;

    .line 1128157
    return-void
.end method

.method private static a(Landroid/graphics/Point;)I
    .locals 3

    .prologue
    const/16 v0, 0x3c0

    .line 1128148
    iget v1, p0, Landroid/graphics/Point;->x:I

    iget v2, p0, Landroid/graphics/Point;->y:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1128149
    if-le v1, v0, :cond_0

    const/16 v0, 0x800

    :cond_0
    return v0
.end method

.method public static a(LX/0QB;)LX/6hN;
    .locals 10

    .prologue
    .line 1128127
    sget-object v0, LX/6hN;->l:LX/6hN;

    if-nez v0, :cond_1

    .line 1128128
    const-class v1, LX/6hN;

    monitor-enter v1

    .line 1128129
    :try_start_0
    sget-object v0, LX/6hN;->l:LX/6hN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1128130
    if-eqz v2, :cond_0

    .line 1128131
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1128132
    new-instance v3, LX/6hN;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    .line 1128133
    new-instance v8, LX/6hL;

    invoke-direct {v8}, LX/6hL;-><init>()V

    .line 1128134
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    .line 1128135
    iput-object v7, v8, LX/6hL;->a:LX/0ad;

    .line 1128136
    move-object v7, v8

    .line 1128137
    check-cast v7, LX/6hL;

    .line 1128138
    new-instance v9, LX/6lH;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-direct {v9, v8}, LX/6lH;-><init>(LX/0W3;)V

    .line 1128139
    move-object v8, v9

    .line 1128140
    check-cast v8, LX/6lH;

    const/16 v9, 0x152a

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/6hN;-><init>(Landroid/content/Context;Landroid/view/WindowManager;LX/0Uh;LX/6hL;LX/6lH;LX/0Or;)V

    .line 1128141
    move-object v0, v3

    .line 1128142
    sput-object v0, LX/6hN;->l:LX/6hN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1128143
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1128144
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1128145
    :cond_1
    sget-object v0, LX/6hN;->l:LX/6hN;

    return-object v0

    .line 1128146
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1128147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1128081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized i()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 1128111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    if-nez v0, :cond_1

    .line 1128112
    iget-object v0, p0, LX/6hN;->c:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1128113
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 1128114
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_2

    .line 1128115
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 1128116
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    if-le v2, v3, :cond_0

    .line 1128117
    iget v2, v1, Landroid/graphics/Point;->y:I

    iget v3, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 1128118
    :cond_0
    :goto_0
    move-object v0, v1

    .line 1128119
    iput-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    .line 1128120
    :cond_1
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1128121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1128122
    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1128123
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    if-le v2, v3, :cond_3

    .line 1128124
    iget v2, v1, Landroid/graphics/Point;->y:I

    iget v3, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 1128125
    :cond_3
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1128108
    iget v0, p0, LX/6hN;->g:I

    if-nez v0, :cond_0

    .line 1128109
    iget-object v0, p0, LX/6hN;->b:Landroid/content/Context;

    sget-object v1, LX/6hM;->TWO_IMAGE_WIDTH_HEIGHT:LX/6hM;

    iget v1, v1, LX/6hM;->dp:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/6hN;->g:I

    .line 1128110
    :cond_0
    iget v0, p0, LX/6hN;->g:I

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1128158
    iget v0, p0, LX/6hN;->h:I

    if-nez v0, :cond_0

    .line 1128159
    iget-object v0, p0, LX/6hN;->b:Landroid/content/Context;

    sget-object v1, LX/6hM;->THREE_IMAGE_WIDTH_HEIGHT:LX/6hM;

    iget v1, v1, LX/6hM;->dp:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/6hN;->h:I

    .line 1128160
    :cond_0
    iget v0, p0, LX/6hN;->h:I

    return v0
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 1128101
    iget v0, p0, LX/6hN;->i:I

    if-nez v0, :cond_0

    .line 1128102
    iget-object v0, p0, LX/6hN;->b:Landroid/content/Context;

    sget-object v1, LX/6hM;->SINGLE_IMAGE_NO_SIZE_WIDTH_HEIGHT:LX/6hM;

    iget v1, v1, LX/6hM;->dp:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/6hN;->i:I

    .line 1128103
    iget v0, p0, LX/6hN;->i:I

    .line 1128104
    iget-object v1, p0, LX/6hN;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, LX/6hN;->b:Landroid/content/Context;

    const/high16 v3, 0x42f00000    # 120.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1128105
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v0, v1

    .line 1128106
    iput v0, p0, LX/6hN;->i:I

    .line 1128107
    :cond_0
    iget v0, p0, LX/6hN;->i:I

    return v0
.end method

.method public final d()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1128093
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 1128094
    invoke-direct {p0}, LX/6hN;->i()Landroid/graphics/Point;

    .line 1128095
    :cond_0
    iget-object v0, p0, LX/6hN;->d:LX/0Uh;

    const/16 v1, 0x1e2

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1128096
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, LX/6hN;->j:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1128097
    :goto_0
    return v0

    .line 1128098
    :cond_1
    iget-object v0, p0, LX/6hN;->d:LX/0Uh;

    const/16 v1, 0x1e3

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1128099
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    invoke-static {v0}, LX/6hN;->a(Landroid/graphics/Point;)I

    move-result v0

    goto :goto_0

    .line 1128100
    :cond_2
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    goto :goto_0
.end method

.method public final e()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1128085
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 1128086
    invoke-direct {p0}, LX/6hN;->i()Landroid/graphics/Point;

    .line 1128087
    :cond_0
    iget-object v0, p0, LX/6hN;->d:LX/0Uh;

    const/16 v1, 0x1e2

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1128088
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, LX/6hN;->j:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1128089
    :goto_0
    return v0

    .line 1128090
    :cond_1
    iget-object v0, p0, LX/6hN;->d:LX/0Uh;

    const/16 v1, 0x1e3

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1128091
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    invoke-static {v0}, LX/6hN;->a(Landroid/graphics/Point;)I

    move-result v0

    goto :goto_0

    .line 1128092
    :cond_2
    iget-object v0, p0, LX/6hN;->j:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public final declared-synchronized f()I
    .locals 1

    .prologue
    .line 1128084
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/6hN;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()I
    .locals 1

    .prologue
    .line 1128083
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/6hN;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()I
    .locals 1

    .prologue
    .line 1128082
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/6hN;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
