.class public final LX/76m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public final synthetic b:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V
    .locals 0

    .prologue
    .line 1171257
    iput-object p1, p0, LX/76m;->b:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iput-object p2, p0, LX/76m;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1171258
    new-instance v1, LX/0ju;

    iget-object v0, p0, LX/76m;->b:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    invoke-direct {v1, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1171259
    :try_start_0
    iget-object v0, p0, LX/76m;->b:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iget-object v0, v0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->g:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->h()LX/4ps;

    move-result-object v0

    iget-object v2, p0, LX/76m;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0, v2}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1171260
    :goto_0
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1171261
    return-void

    .line 1171262
    :catch_0
    move-exception v0

    .line 1171263
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 1171264
    new-instance v3, Ljava/io/PrintWriter;

    invoke-direct {v3, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v3}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1171265
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    goto :goto_0
.end method
