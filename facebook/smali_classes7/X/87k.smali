.class public final LX/87k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/87o;


# direct methods
.method public constructor <init>(LX/87o;LX/0TF;)V
    .locals 0

    .prologue
    .line 1300573
    iput-object p1, p0, LX/87k;->b:LX/87o;

    iput-object p2, p0, LX/87k;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1300571
    iget-object v0, p0, LX/87k;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1300572
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1300567
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1300568
    if-nez p1, :cond_0

    .line 1300569
    :goto_0
    return-void

    .line 1300570
    :cond_0
    iget-object v0, p0, LX/87k;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
