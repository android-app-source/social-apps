.class public LX/8Fz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8Fz;


# instance fields
.field public final a:LX/11R;

.field public final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/11R;LX/0SG;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1318396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318397
    iput-object p1, p0, LX/8Fz;->a:LX/11R;

    .line 1318398
    iput-object p2, p0, LX/8Fz;->b:LX/0SG;

    .line 1318399
    return-void
.end method

.method public static a(LX/0QB;)LX/8Fz;
    .locals 5

    .prologue
    .line 1318401
    sget-object v0, LX/8Fz;->c:LX/8Fz;

    if-nez v0, :cond_1

    .line 1318402
    const-class v1, LX/8Fz;

    monitor-enter v1

    .line 1318403
    :try_start_0
    sget-object v0, LX/8Fz;->c:LX/8Fz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1318404
    if-eqz v2, :cond_0

    .line 1318405
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1318406
    new-instance p0, LX/8Fz;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v3

    check-cast v3, LX/11R;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/8Fz;-><init>(LX/11R;LX/0SG;)V

    .line 1318407
    move-object v0, p0

    .line 1318408
    sput-object v0, LX/8Fz;->c:LX/8Fz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1318409
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1318410
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1318411
    :cond_1
    sget-object v0, LX/8Fz;->c:LX/8Fz;

    return-object v0

    .line 1318412
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1318413
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;)Z
    .locals 1

    .prologue
    .line 1318400
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
