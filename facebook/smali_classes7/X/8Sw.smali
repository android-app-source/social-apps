.class public LX/8Sw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TD;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/8W4;


# direct methods
.method public constructor <init>(LX/8W4;LX/0Ot;)V
    .locals 0
    .param p1    # LX/8W4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicksilver/QuicksilverJavascriptInterface$QuicksilverJSDelegate;",
            "LX/0Ot",
            "<",
            "LX/8TD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1347902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1347903
    iput-object p1, p0, LX/8Sw;->b:LX/8W4;

    .line 1347904
    iput-object p2, p0, LX/8Sw;->a:LX/0Ot;

    .line 1347905
    return-void
.end method


# virtual methods
.method public postMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 1347906
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1347907
    const-string v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1347908
    const-string v2, "onendgame"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1347909
    iget-object v0, p0, LX/8Sw;->b:LX/8W4;

    .line 1347910
    iget-object v1, v0, LX/8W4;->a:LX/8Sv;

    invoke-virtual {v1}, LX/8Sv;->a()V

    .line 1347911
    :cond_0
    :goto_0
    return-void

    .line 1347912
    :cond_1
    const-string v2, "onscore"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1347913
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347914
    :try_start_1
    iget-object v2, v1, LX/8W4;->a:LX/8Sv;

    invoke-static {v0}, LX/8W7;->g(Lorg/json/JSONObject;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/8Sv;->a(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1347915
    :goto_1
    goto :goto_0

    .line 1347916
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1347917
    iget-object v0, p0, LX/8Sw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid JSON received via postMessage: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1347918
    :cond_2
    :try_start_2
    const-string v2, "onbeginload"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1347919
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1347920
    :try_start_3
    iget-object v5, v1, LX/8W4;->a:LX/8Sv;

    invoke-static {v0}, LX/8W7;->g(Lorg/json/JSONObject;)I

    move-result v6

    invoke-virtual {v5, v6}, LX/8Sv;->b(I)V

    .line 1347921
    iget-object v5, v1, LX/8W4;->b:LX/8W7;

    iget-object v6, v1, LX/8W4;->b:LX/8W7;

    iget-object v6, v6, LX/8W7;->d:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v7

    .line 1347922
    iput-wide v7, v5, LX/8W7;->a:J
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1347923
    :goto_2
    :try_start_4
    goto :goto_0

    .line 1347924
    :cond_3
    const-string v2, "onprogressload"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1347925
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 1347926
    :try_start_5
    iget-object v2, v1, LX/8W4;->a:LX/8Sv;

    invoke-static {v0}, LX/8W7;->g(Lorg/json/JSONObject;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/8Sv;->c(I)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1347927
    :goto_3
    :try_start_6
    goto :goto_0

    .line 1347928
    :cond_4
    const-string v2, "ongameready"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1347929
    iget-object v0, p0, LX/8Sw;->b:LX/8W4;

    .line 1347930
    iget-object v1, v0, LX/8W4;->a:LX/8Sv;

    invoke-virtual {v1}, LX/8Sv;->b()V

    .line 1347931
    goto :goto_0

    .line 1347932
    :cond_5
    const-string v2, "onscreenshot"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1347933
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0

    .line 1347934
    :try_start_7
    iget-object v2, v1, LX/8W4;->a:LX/8Sv;

    invoke-static {v0}, LX/8W7;->e(Lorg/json/JSONObject;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8Sv;->a(Landroid/graphics/Bitmap;)V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_0

    .line 1347935
    :goto_4
    :try_start_8
    goto/16 :goto_0

    .line 1347936
    :cond_6
    const-string v2, "onpicture"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1347937
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0

    .line 1347938
    :try_start_9
    iget-object v2, v1, LX/8W4;->a:LX/8Sv;

    invoke-static {v0}, LX/8W7;->e(Lorg/json/JSONObject;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8Sv;->a(Landroid/graphics/Bitmap;)V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_0

    .line 1347939
    :goto_5
    :try_start_a
    goto/16 :goto_0

    .line 1347940
    :cond_7
    const-string v2, "averageframetime"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1347941
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;

    invoke-virtual {v1, v0}, LX/8W4;->d(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 1347942
    :cond_8
    const-string v2, "initializeasync"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1347943
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_0

    .line 1347944
    :try_start_b
    iget-object v2, v1, LX/8W4;->a:LX/8Sv;

    invoke-static {v0}, LX/8W7;->f(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8Sv;->a(Ljava/lang/String;)V
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_6
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_0

    .line 1347945
    :goto_6
    :try_start_c
    goto/16 :goto_0

    .line 1347946
    :cond_9
    const-string v2, "getplayerdataasync"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1347947
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_0

    .line 1347948
    :try_start_d
    invoke-static {v0}, LX/8W7;->f(Lorg/json/JSONObject;)Ljava/lang/String;
    :try_end_d
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_8
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_0

    :try_start_e
    move-result-object v3
    :try_end_e
    .catch Lorg/json/JSONException; {:try_start_e .. :try_end_e} :catch_0

    .line 1347949
    :try_start_f
    sget-object v2, LX/8W6;->KEYS:LX/8W6;

    invoke-virtual {v2}, LX/8W6;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1347950
    const-string v4, "content"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    move-object v4, v4

    .line 1347951
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1347952
    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_a

    .line 1347953
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1347954
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1347955
    :cond_a
    iget-object v2, v1, LX/8W4;->a:LX/8Sv;

    invoke-virtual {v2, v3, v5}, LX/8Sv;->a(Ljava/lang/String;Ljava/util/List;)V
    :try_end_f
    .catch Lorg/json/JSONException; {:try_start_f .. :try_end_f} :catch_7
    .catch Lorg/json/JSONException; {:try_start_f .. :try_end_f} :catch_0

    .line 1347956
    :goto_8
    :try_start_10
    goto/16 :goto_0

    .line 1347957
    :cond_b
    const-string v2, "setplayerdataasync"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1347958
    iget-object v1, p0, LX/8Sw;->b:LX/8W4;
    :try_end_10
    .catch Lorg/json/JSONException; {:try_start_10 .. :try_end_10} :catch_0

    .line 1347959
    :try_start_11
    invoke-static {v0}, LX/8W7;->f(Lorg/json/JSONObject;)Ljava/lang/String;
    :try_end_11
    .catch Lorg/json/JSONException; {:try_start_11 .. :try_end_11} :catch_a
    .catch Lorg/json/JSONException; {:try_start_11 .. :try_end_11} :catch_0

    :try_start_12
    move-result-object v2
    :try_end_12
    .catch Lorg/json/JSONException; {:try_start_12 .. :try_end_12} :catch_0

    .line 1347960
    :try_start_13
    sget-object v3, LX/8W6;->DATA:LX/8W6;

    invoke-virtual {v3}, LX/8W6;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1347961
    const-string v4, "content"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    move-object v3, v4

    .line 1347962
    iget-object v4, v1, LX/8W4;->a:LX/8Sv;

    invoke-virtual {v4, v2, v3}, LX/8Sv;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_13
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_13} :catch_9
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_13} :catch_0

    .line 1347963
    :goto_9
    goto/16 :goto_0

    .line 1347964
    :catch_1
    :try_start_14
    move-exception v2

    .line 1347965
    iget-object v3, v1, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->c:LX/8TD;

    sget-object v4, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p2, "Invalid JSON content received by onScore: "

    invoke-direct {v5, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
    :try_end_14
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_14} :catch_0

    .line 1347966
    :catch_2
    :try_start_15
    move-exception v5

    .line 1347967
    iget-object v6, v1, LX/8W4;->b:LX/8W7;

    iget-object v6, v6, LX/8W7;->c:LX/8TD;

    sget-object v7, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Invalid JSON content received by onBeginLoad: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v5}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2
    :try_end_15
    .catch Lorg/json/JSONException; {:try_start_15 .. :try_end_15} :catch_0

    .line 1347968
    :catch_3
    :try_start_16
    move-exception v2

    .line 1347969
    iget-object v3, v1, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->c:LX/8TD;

    sget-object v4, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid JSON content received by onProgressLoad: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3
    :try_end_16
    .catch Lorg/json/JSONException; {:try_start_16 .. :try_end_16} :catch_0

    .line 1347970
    :catch_4
    :try_start_17
    move-exception v2

    .line 1347971
    iget-object v3, v1, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->c:LX/8TD;

    sget-object v4, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid JSON content received by onScreenshot: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4
    :try_end_17
    .catch Lorg/json/JSONException; {:try_start_17 .. :try_end_17} :catch_0

    .line 1347972
    :catch_5
    :try_start_18
    move-exception v2

    .line 1347973
    iget-object v3, v1, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->c:LX/8TD;

    sget-object v4, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid JSON content received by onPicture: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5
    :try_end_18
    .catch Lorg/json/JSONException; {:try_start_18 .. :try_end_18} :catch_0

    .line 1347974
    :catch_6
    :try_start_19
    move-exception v2

    .line 1347975
    iget-object v3, v1, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->c:LX/8TD;

    sget-object v4, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid JSON content received by onInitializeAsync: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6
    :try_end_19
    .catch Lorg/json/JSONException; {:try_start_19 .. :try_end_19} :catch_0

    .line 1347976
    :catch_7
    :try_start_1a
    iget-object v2, v1, LX/8W4;->a:LX/8Sv;

    const-string v4, "Missing or malformed value: keys"

    invoke-virtual {v2, v3, v4}, LX/8Sv;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1a
    .catch Lorg/json/JSONException; {:try_start_1a .. :try_end_1a} :catch_8
    .catch Lorg/json/JSONException; {:try_start_1a .. :try_end_1a} :catch_0

    :try_start_1b
    goto/16 :goto_8

    .line 1347977
    :catch_8
    move-exception v2

    .line 1347978
    iget-object v3, v1, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->c:LX/8TD;

    sget-object v4, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid JSON content received by onGetPlayerDataAsync: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8
    :try_end_1b
    .catch Lorg/json/JSONException; {:try_start_1b .. :try_end_1b} :catch_0

    .line 1347979
    :catch_9
    :try_start_1c
    iget-object v3, v1, LX/8W4;->a:LX/8Sv;

    const-string v4, "Missing or malformed object: data"

    invoke-virtual {v3, v2, v4}, LX/8Sv;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1c
    .catch Lorg/json/JSONException; {:try_start_1c .. :try_end_1c} :catch_a
    .catch Lorg/json/JSONException; {:try_start_1c .. :try_end_1c} :catch_0

    goto/16 :goto_9

    .line 1347980
    :catch_a
    move-exception v2

    .line 1347981
    iget-object v3, v1, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->c:LX/8TD;

    sget-object v4, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid JSON content received by onSetPlayerDataAsync: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9
.end method
