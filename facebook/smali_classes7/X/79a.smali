.class public LX/79a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/79Z;

.field public static final b:Ljava/lang/String;

.field private static volatile h:LX/79a;


# instance fields
.field private final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            "LX/79Z;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/03V;

.field private final e:LX/0Uh;

.field private final f:LX/0ad;

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/79Y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1174138
    const-class v0, LX/79a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/79a;->b:Ljava/lang/String;

    .line 1174139
    new-instance v0, LX/79Z;

    const v1, 0x7f021797

    const v2, 0x7f081a90

    const v3, 0x7f081a9d

    invoke-direct {v0, v1, v2, v3}, LX/79Z;-><init>(III)V

    sput-object v0, LX/79a;->a:LX/79Z;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Uh;LX/0ad;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1174113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174114
    iput-object p1, p0, LX/79a;->d:LX/03V;

    .line 1174115
    iput-object p2, p0, LX/79a;->e:LX/0Uh;

    .line 1174116
    iput-object p3, p0, LX/79a;->f:LX/0ad;

    .line 1174117
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021793

    const v4, 0x7f081a90

    const v5, 0x7f081a9d

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021794

    const v4, 0x7f081a9c

    const v5, 0x7f081aa9

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021795

    const v4, 0x7f081a97

    const v5, 0x7f081aa4

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021796

    const v4, 0x7f081a9a

    const v5, 0x7f081aa7

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021797

    const v4, 0x7f081a91

    const v5, 0x7f081a9e

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021798

    const v4, 0x7f081a95

    const v5, 0x7f081aa2

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021798

    const v4, 0x7f081a98

    const v5, 0x7f081aa5

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f021799

    const v4, 0x7f081a96

    const v5, 0x7f081aa3

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PLACES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f02179b

    const v4, 0x7f081a94

    const v5, 0x7f081aa1

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f02179c

    const v4, 0x7f081a99

    const v5, 0x7f081aa6

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f02179d

    const v4, 0x7f081a9b

    const v5, 0x7f081aa8

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f02179a

    const v4, 0x7f081a93

    const v5, 0x7f081aa0

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PRODUCTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    new-instance v2, LX/79Z;

    const v3, 0x7f020989

    const v4, 0x7f081a92

    const v5, 0x7f081a9f

    invoke-direct {v2, v3, v4, v5}, LX/79Z;-><init>(III)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    .line 1174118
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/79a;->c:LX/0P1;

    .line 1174119
    return-void
.end method

.method public static a(LX/0QB;)LX/79a;
    .locals 6

    .prologue
    .line 1174140
    sget-object v0, LX/79a;->h:LX/79a;

    if-nez v0, :cond_1

    .line 1174141
    const-class v1, LX/79a;

    monitor-enter v1

    .line 1174142
    :try_start_0
    sget-object v0, LX/79a;->h:LX/79a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1174143
    if-eqz v2, :cond_0

    .line 1174144
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1174145
    new-instance p0, LX/79a;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/79a;-><init>(LX/03V;LX/0Uh;LX/0ad;)V

    .line 1174146
    move-object v0, p0

    .line 1174147
    sput-object v0, LX/79a;->h:LX/79a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1174148
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1174149
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1174150
    :cond_1
    sget-object v0, LX/79a;->h:LX/79a;

    return-object v0

    .line 1174151
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1174152
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/79Y;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1174125
    iget-object v0, p0, LX/79a;->g:LX/0Px;

    if-nez v0, :cond_2

    .line 1174126
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081aaa

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081aab

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081ab5

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 1174127
    iget-object v1, p0, LX/79a;->e:LX/0Uh;

    const/16 v2, 0x613

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1174128
    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PRODUCTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081aac

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1174129
    :cond_0
    iget-object v1, p0, LX/79a;->e:LX/0Uh;

    const/16 v2, 0x612

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1174130
    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081aad

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1174131
    :cond_1
    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PLACES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081aae

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1174132
    iget-object v1, p0, LX/79a;->f:LX/0ad;

    sget-short v2, LX/0wh;->E:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1174133
    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081aaf

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1174134
    :goto_0
    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081ab4

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/79Y;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v4, 0x7f081ab6

    invoke-direct {v2, v3, v4}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1174135
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/79a;->g:LX/0Px;

    .line 1174136
    :cond_2
    iget-object v0, p0, LX/79a;->g:LX/0Px;

    return-object v0

    .line 1174137
    :cond_3
    new-instance v1, LX/79Y;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v3, 0x7f081ab0

    invoke-direct {v1, v2, v3}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/79Y;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v4, 0x7f081ab1

    invoke-direct {v2, v3, v4}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/79Y;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v4, 0x7f081ab2

    invoke-direct {v2, v3, v4}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/79Y;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const v4, 0x7f081ab3

    invoke-direct {v2, v3, v4}, LX/79Y;-><init>(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;I)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ")",
            "LX/0am",
            "<",
            "LX/79Z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1174120
    iget-object v0, p0, LX/79a;->c:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Z;

    .line 1174121
    if-nez v0, :cond_0

    .line 1174122
    iget-object v0, p0, LX/79a;->d:LX/03V;

    sget-object v1, LX/79a;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174123
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1174124
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method
