.class public final LX/75j;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Landroid/os/Message;

.field public final synthetic b:Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 1169857
    iput-object p1, p0, LX/75j;->b:Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;

    iput-object p2, p0, LX/75j;->a:Landroid/os/Message;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 1169882
    iget-object v0, p0, LX/75j;->a:Landroid/os/Message;

    const/4 v1, 0x0

    invoke-static {v1, p1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1169883
    iget-object v0, p0, LX/75j;->a:Landroid/os/Message;

    invoke-static {v0}, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->b(Landroid/os/Message;)V

    .line 1169884
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1169858
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 1169859
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    .line 1169860
    iget-boolean v2, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 1169861
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const-string v2, "result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1169862
    :goto_0
    if-eqz v0, :cond_1

    .line 1169863
    check-cast v0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;

    .line 1169864
    iget-object v2, p0, LX/75j;->a:Landroid/os/Message;

    .line 1169865
    iget-object v3, v0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1169866
    iget-wide v6, v0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;->b:J

    move-wide v4, v6

    .line 1169867
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/platform/common/action/PlatformAppCall;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v7, v0

    .line 1169868
    :goto_1
    if-eqz v7, :cond_3

    const-string v0, "seamless_login_token"

    move-object v6, v0

    .line 1169869
    :goto_2
    if-eqz v7, :cond_4

    const-string v0, "expires_seconds_since_epoch"

    .line 1169870
    :goto_3
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1169871
    invoke-virtual {v7, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169872
    invoke-virtual {v7, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1169873
    move-object v0, v7

    .line 1169874
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1169875
    :goto_4
    iget-object v0, p0, LX/75j;->a:Landroid/os/Message;

    invoke-static {v0}, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->b(Landroid/os/Message;)V

    .line 1169876
    return-void

    :cond_0
    move-object v0, v1

    .line 1169877
    goto :goto_0

    .line 1169878
    :cond_1
    iget-object v0, p0, LX/75j;->a:Landroid/os/Message;

    new-instance v2, Lcom/facebook/fbservice/service/ServiceException;

    invoke-direct {v2, p1}, Lcom/facebook/fbservice/service/ServiceException;-><init>(Lcom/facebook/fbservice/service/OperationResult;)V

    invoke-static {v1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    goto :goto_4

    .line 1169879
    :cond_2
    const/4 v0, 0x0

    move v7, v0

    goto :goto_1

    .line 1169880
    :cond_3
    const-string v0, "com.facebook.platform.extra.SEAMLESS_LOGIN_TOKEN"

    move-object v6, v0

    goto :goto_2

    .line 1169881
    :cond_4
    const-string v0, "com.facebook.platform.extra.EXPIRES_SECONDS_SINCE_EPOCH"

    goto :goto_3
.end method
