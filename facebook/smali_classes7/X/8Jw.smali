.class public LX/8Jw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Tn;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0SG;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:Landroid/content/Context;

.field private final g:LX/0u7;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Jy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1329644
    const-class v0, LX/8Jw;

    sput-object v0, LX/8Jw;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;LX/0u7;LX/0Or;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/ImageSampling/NeedPhotoUploadImageSample;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            "LX/0Or",
            "<",
            "LX/8Jy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1329645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329646
    sget-object v0, LX/1Ip;->a:LX/0Tn;

    const-string v1, "last_image_sample_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/8Jw;->a:LX/0Tn;

    .line 1329647
    iput-object p1, p0, LX/8Jw;->c:LX/0Or;

    .line 1329648
    iput-object p2, p0, LX/8Jw;->d:LX/0SG;

    .line 1329649
    iput-object p3, p0, LX/8Jw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1329650
    iput-object p4, p0, LX/8Jw;->f:Landroid/content/Context;

    .line 1329651
    iput-object p5, p0, LX/8Jw;->g:LX/0u7;

    .line 1329652
    iput-object p6, p0, LX/8Jw;->h:LX/0Or;

    .line 1329653
    return-void
.end method

.method public static a(LX/0QB;)LX/8Jw;
    .locals 8

    .prologue
    .line 1329654
    new-instance v1, LX/8Jw;

    const/16 v2, 0x342

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v5, Landroid/content/Context;

    const-class v6, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v5, v6}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v6

    check-cast v6, LX/0u7;

    const/16 v7, 0x2ed2

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, LX/8Jw;-><init>(LX/0Or;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;LX/0u7;LX/0Or;)V

    .line 1329655
    move-object v0, v1

    .line 1329656
    return-object v0
.end method

.method public static d(LX/8Jw;)Ljava/io/File;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1329657
    iget-object v1, p0, LX/8Jw;->f:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1329658
    if-eqz v1, :cond_0

    .line 1329659
    new-instance v0, Ljava/io/File;

    const-string v2, "last_image_sample_timestamp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1329660
    :cond_0
    return-object v0
.end method

.method public static e(LX/8Jw;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1329661
    iget-object v2, p0, LX/8Jw;->g:LX/0u7;

    invoke-virtual {v2}, LX/0u7;->a()F

    move-result v2

    .line 1329662
    const v3, 0x3f333333    # 0.7f

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_1

    .line 1329663
    :cond_0
    :goto_0
    return v0

    .line 1329664
    :cond_1
    const v3, 0x3e99999a    # 0.3f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    move v0, v1

    .line 1329665
    goto :goto_0

    .line 1329666
    :cond_2
    iget-object v2, p0, LX/8Jw;->g:LX/0u7;

    invoke-virtual {v2}, LX/0u7;->b()LX/0y1;

    move-result-object v2

    .line 1329667
    sget-object v3, LX/0y1;->CHARGING_AC:LX/0y1;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/0y1;->CHARGING_USB:LX/0y1;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/0y1;->FULL:LX/0y1;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1329668
    goto :goto_0
.end method


# virtual methods
.method public final c()LX/8Jv;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 1329669
    const/4 v4, 0x0

    .line 1329670
    iget-object v0, p0, LX/8Jw;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, LX/8Jw;->e(LX/8Jw;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1329671
    iget-object v0, p0, LX/8Jw;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v8

    .line 1329672
    iget-object v0, p0, LX/8Jw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/8Jw;->a:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 1329673
    invoke-static {p0}, LX/8Jw;->d(LX/8Jw;)Ljava/io/File;

    move-result-object v0

    .line 1329674
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1329675
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    .line 1329676
    :goto_0
    cmp-long v5, v6, v8

    if-ltz v5, :cond_0

    move-wide v6, v2

    .line 1329677
    :cond_0
    cmp-long v5, v0, v8

    if-ltz v5, :cond_1

    move-wide v0, v2

    .line 1329678
    :cond_1
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1329679
    const-wide/32 v2, 0x240c8400

    add-long/2addr v0, v2

    cmp-long v0, v0, v8

    if-gez v0, :cond_3

    .line 1329680
    iget-object v0, p0, LX/8Jw;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Jv;

    .line 1329681
    :goto_1
    if-nez v0, :cond_2

    .line 1329682
    new-instance v0, LX/8Jx;

    invoke-direct {v0}, LX/8Jx;-><init>()V

    .line 1329683
    :cond_2
    invoke-interface {v0, p0}, LX/8Jv;->a(LX/8Jw;)V

    .line 1329684
    return-object v0

    :cond_3
    move-object v0, v4

    goto :goto_1

    :cond_4
    move-wide v0, v2

    goto :goto_0
.end method
