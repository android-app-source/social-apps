.class public abstract LX/7Mr;
.super LX/3Gb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/video/engine/VideoPlayerParams;

.field public d:I

.field public e:Landroid/widget/SeekBar;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public f:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:I

.field private final p:LX/7NU;

.field private q:I

.field private r:I

.field public s:I

.field private t:I

.field public u:Lcom/facebook/resources/ui/FbTextView;

.field public v:Lcom/facebook/resources/ui/FbTextView;

.field public w:Landroid/graphics/drawable/Drawable;

.field public x:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1199075
    const-class v0, LX/7Mr;

    sput-object v0, LX/7Mr;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199076
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Mr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199077
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199078
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Mr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199079
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1199080
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199081
    iput v0, p0, LX/7Mr;->q:I

    .line 1199082
    iput v0, p0, LX/7Mr;->r:I

    .line 1199083
    iput v0, p0, LX/7Mr;->s:I

    .line 1199084
    iput v0, p0, LX/7Mr;->t:I

    .line 1199085
    const-class v0, LX/7Mr;

    invoke-static {v0, p0}, LX/7Mr;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1199086
    new-instance v0, LX/7NU;

    invoke-direct {v0, p0}, LX/7NU;-><init>(LX/7Mr;)V

    iput-object v0, p0, LX/7Mr;->p:LX/7NU;

    .line 1199087
    invoke-virtual {p0}, LX/7Mr;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1199088
    invoke-virtual {p0}, LX/7Mr;->f()V

    .line 1199089
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x10

    if-lt v0, p1, :cond_0

    invoke-virtual {p0}, LX/7Mr;->getActiveThumbResource()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1199090
    iget-object v0, p0, LX/7Mr;->e:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/7Mr;->w:Landroid/graphics/drawable/Drawable;

    .line 1199091
    invoke-virtual {p0}, LX/7Mr;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, LX/7Mr;->getActiveThumbResource()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/7Mr;->x:Landroid/graphics/drawable/Drawable;

    .line 1199092
    :cond_0
    invoke-virtual {p0}, LX/7Mr;->g()V

    .line 1199093
    invoke-virtual {p0}, LX/7Mr;->h()V

    .line 1199094
    return-void
.end method

.method public static a(LX/7Mr;IIZ)V
    .locals 8

    .prologue
    .line 1199095
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1199096
    iget v0, p0, LX/7Mr;->o:I

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 1199097
    iget p1, p0, LX/7Mr;->o:I

    .line 1199098
    :goto_0
    iget v0, p0, LX/7Mr;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 1199099
    iget v0, p0, LX/7Mr;->s:I

    sub-int/2addr p1, v0

    .line 1199100
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/7Mr;->b(II)V

    .line 1199101
    if-eqz p3, :cond_1

    .line 1199102
    iget-object v0, p0, LX/7Mr;->e:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    .line 1199103
    if-nez p2, :cond_3

    .line 1199104
    :goto_1
    move v1, v2

    .line 1199105
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1199106
    invoke-virtual {p0}, LX/7Mr;->w()V

    .line 1199107
    :cond_1
    return-void

    .line 1199108
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, LX/7Mr;->o:I

    goto :goto_0

    .line 1199109
    :cond_3
    iget-object v3, p0, LX/7Mr;->e:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    .line 1199110
    int-to-long v4, p1

    int-to-long v6, v3

    mul-long/2addr v4, v6

    int-to-long v6, p2

    div-long/2addr v4, v6

    long-to-int v4, v4

    .line 1199111
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7Mr;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object p0

    check-cast p0, LX/1C2;

    iput-object v1, p1, LX/7Mr;->a:LX/03V;

    iput-object p0, p1, LX/7Mr;->f:LX/1C2;

    return-void
.end method

.method public static a$redex0(LX/7Mr;Z)V
    .locals 4

    .prologue
    .line 1199112
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_0

    .line 1199113
    iget-object v0, p0, LX/7Mr;->a:LX/03V;

    const-string v1, "seekBarBasePlugin"

    const-string v2, "Attempt to update progress bar while the playback controller is null"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1199114
    :goto_0
    return-void

    .line 1199115
    :cond_0
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v1

    .line 1199116
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->o()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 1199117
    :goto_1
    iget v2, p0, LX/7Mr;->d:I

    invoke-static {p0, v1, v2, v0}, LX/7Mr;->a(LX/7Mr;IIZ)V

    goto :goto_0

    .line 1199118
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(LX/2oi;)V
    .locals 0

    .prologue
    .line 1199051
    return-void
.end method

.method public a(LX/2pa;Z)V
    .locals 7

    .prologue
    .line 1199123
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1199124
    if-eqz p2, :cond_0

    .line 1199125
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "SeekPositionMsKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1199126
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "SeekPositionMsKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1199127
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1199128
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/7Mr;->o:I

    .line 1199129
    :cond_0
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "TrimStartPosition"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1199130
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "TrimStartPosition"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/7Mr;->s:I

    .line 1199131
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "TrimEndPosition"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/7Mr;->t:I

    .line 1199132
    :cond_1
    iget v0, p0, LX/7Mr;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    iget v0, p0, LX/7Mr;->t:I

    iget v1, p0, LX/7Mr;->s:I

    sub-int/2addr v0, v1

    .line 1199133
    :goto_0
    if-lez v0, :cond_3

    .line 1199134
    iput v0, p0, LX/7Mr;->d:I

    .line 1199135
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/7Mr;->d:I

    int-to-long v4, v3

    invoke-static {v4, v5}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1199136
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1199137
    iget-object v4, p0, LX/7Mr;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v2, v5, v6, v3}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1199138
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    .line 1199139
    iget-object v3, p0, LX/7Mr;->u:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v3, :cond_2

    .line 1199140
    iget-object v3, p0, LX/7Mr;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setMinWidth(I)V

    .line 1199141
    :cond_2
    iget-object v3, p0, LX/7Mr;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setMinWidth(I)V

    .line 1199142
    :cond_3
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/7Mr;->a$redex0(LX/7Mr;Z)V

    .line 1199143
    return-void

    .line 1199144
    :cond_4
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    goto :goto_0
.end method

.method public b(II)V
    .locals 6

    .prologue
    .line 1199145
    if-lez p2, :cond_0

    if-gez p1, :cond_1

    .line 1199146
    :cond_0
    :goto_0
    return-void

    .line 1199147
    :cond_1
    div-int/lit16 v0, p2, 0x3e8

    .line 1199148
    div-int/lit16 v1, p1, 0x3e8

    .line 1199149
    sub-int/2addr v0, v1

    .line 1199150
    iget v2, p0, LX/7Mr;->q:I

    if-ne v1, v2, :cond_2

    iget v2, p0, LX/7Mr;->r:I

    if-eq v0, v2, :cond_0

    .line 1199151
    :cond_2
    iput v1, p0, LX/7Mr;->q:I

    .line 1199152
    iput v0, p0, LX/7Mr;->r:I

    .line 1199153
    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-static {v2, v3}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v1

    .line 1199154
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    invoke-static {v4, v5}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1199155
    iget-object v2, p0, LX/7Mr;->u:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v2, :cond_3

    .line 1199156
    iget-object v2, p0, LX/7Mr;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1199157
    :cond_3
    iget-object v1, p0, LX/7Mr;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 1199074
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1199119
    iget-object v0, p0, LX/7Mr;->p:LX/7NU;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/7NU;->removeMessages(I)V

    .line 1199120
    iput v2, p0, LX/7Mr;->d:I

    .line 1199121
    iput v2, p0, LX/7Mr;->o:I

    .line 1199122
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 1199045
    const v0, 0x7f0d1329

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    .line 1199046
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1199047
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7Mr;->u:Lcom/facebook/resources/ui/FbTextView;

    .line 1199048
    :cond_0
    const v0, 0x7f0d132b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7Mr;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 1199049
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, LX/7Mr;->e:Landroid/widget/SeekBar;

    .line 1199050
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 1199052
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NT;

    invoke-direct {v1, p0}, LX/7NT;-><init>(LX/7Mr;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199053
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NS;

    invoke-direct {v1, p0}, LX/7NS;-><init>(LX/7Mr;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199054
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NW;

    invoke-direct {v1, p0}, LX/7NW;-><init>(LX/7Mr;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199055
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NR;

    invoke-direct {v1, p0}, LX/7NR;-><init>(LX/7Mr;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199056
    return-void
.end method

.method public abstract getActiveThumbResource()I
.end method

.method public abstract getContentView()I
.end method

.method public h()V
    .locals 2

    .prologue
    .line 1199057
    iget-object v0, p0, LX/7Mr;->e:Landroid/widget/SeekBar;

    new-instance v1, LX/7NV;

    invoke-direct {v1, p0}, LX/7NV;-><init>(LX/7Mr;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1199058
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 1199059
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/7Mr;->a$redex0(LX/7Mr;Z)V

    .line 1199060
    return-void
.end method

.method public setSeekBarVisibility(I)V
    .locals 1

    .prologue
    .line 1199061
    iget-object v0, p0, LX/7Mr;->u:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 1199062
    iget-object v0, p0, LX/7Mr;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1199063
    :cond_0
    iget-object v0, p0, LX/7Mr;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1199064
    iget-object v0, p0, LX/7Mr;->e:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1199065
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 1199066
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 1199067
    return-void
.end method

.method public final w()V
    .locals 4

    .prologue
    .line 1199068
    iget-object v0, p0, LX/7Mr;->p:LX/7NU;

    const/4 v1, 0x2

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, LX/7NU;->sendEmptyMessageDelayed(IJ)Z

    .line 1199069
    return-void
.end method

.method public final x()V
    .locals 2

    .prologue
    .line 1199070
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 1199071
    invoke-virtual {p0}, LX/7Mr;->i()V

    .line 1199072
    :cond_0
    iget-object v0, p0, LX/7Mr;->p:LX/7NU;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/7NU;->removeMessages(I)V

    .line 1199073
    return-void
.end method
