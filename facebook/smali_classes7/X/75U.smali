.class public LX/75U;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1169620
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1169621
    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/photos/upload/quality/PhotosHighDefUploadSettingValue;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1169622
    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/1Ip;->k:LX/0Tn;

    invoke-interface {p0, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1169623
    return-void
.end method
