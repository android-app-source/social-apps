.class public final LX/7Hx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/3fx;

.field public final b:Ljava/lang/String;

.field private final c:LX/4hT;


# direct methods
.method public constructor <init>(LX/3fx;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1191438
    iput-object p1, p0, LX/7Hx;->a:LX/3fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191439
    iput-object p2, p0, LX/7Hx;->b:Ljava/lang/String;

    .line 1191440
    const/4 v1, 0x0

    .line 1191441
    iget-object v0, p0, LX/7Hx;->a:LX/3fx;

    iget-object v0, v0, LX/3fx;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1191442
    if-eqz p2, :cond_0

    .line 1191443
    :try_start_0
    iget-object p1, p0, LX/7Hx;->a:LX/3fx;

    iget-object p1, p1, LX/3fx;->a:LX/3Lz;

    invoke-virtual {p1, p2, v0}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1191444
    :goto_0
    move-object v0, v0

    .line 1191445
    iput-object v0, p0, LX/7Hx;->c:LX/4hT;

    .line 1191446
    return-void

    :catch_0
    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1191437
    iget-object v0, p0, LX/7Hx;->c:LX/4hT;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1191447
    iget-object v0, p0, LX/7Hx;->c:LX/4hT;

    if-nez v0, :cond_0

    .line 1191448
    iget-object v0, p0, LX/7Hx;->b:Ljava/lang/String;

    .line 1191449
    :goto_0
    return-object v0

    .line 1191450
    :cond_0
    iget-object v0, p0, LX/7Hx;->a:LX/3fx;

    iget-object v0, v0, LX/3fx;->a:LX/3Lz;

    iget-object v1, p0, LX/7Hx;->c:LX/4hT;

    invoke-virtual {v0, v1}, LX/3Lz;->isValidNumber(LX/4hT;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1191451
    iget-object v0, p0, LX/7Hx;->b:Ljava/lang/String;

    goto :goto_0

    .line 1191452
    :cond_1
    iget-object v0, p0, LX/7Hx;->a:LX/3fx;

    iget-object v0, v0, LX/3fx;->a:LX/3Lz;

    iget-object v1, p0, LX/7Hx;->c:LX/4hT;

    sget-object v2, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v0, v1, v2}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1191427
    iget-object v0, p0, LX/7Hx;->c:LX/4hT;

    if-nez v0, :cond_0

    .line 1191428
    iget-object v0, p0, LX/7Hx;->b:Ljava/lang/String;

    .line 1191429
    :goto_0
    return-object v0

    .line 1191430
    :cond_0
    iget-object v0, p0, LX/7Hx;->a:LX/3fx;

    iget-object v1, v0, LX/3fx;->a:LX/3Lz;

    iget-object v0, p0, LX/7Hx;->a:LX/3fx;

    iget-object v0, v0, LX/3fx;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v0

    .line 1191431
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/7Hx;->c:LX/4hT;

    .line 1191432
    iget v2, v1, LX/4hT;->countryCode_:I

    move v1, v2

    .line 1191433
    if-eq v0, v1, :cond_2

    .line 1191434
    :cond_1
    sget-object v0, LX/4hG;->INTERNATIONAL:LX/4hG;

    .line 1191435
    :goto_1
    iget-object v1, p0, LX/7Hx;->a:LX/3fx;

    iget-object v1, v1, LX/3fx;->a:LX/3Lz;

    iget-object v2, p0, LX/7Hx;->c:LX/4hT;

    invoke-virtual {v1, v2, v0}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x20

    const/16 v2, 0xa0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2d

    const/16 v2, 0x2011

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1191436
    :cond_2
    sget-object v0, LX/4hG;->NATIONAL:LX/4hG;

    goto :goto_1
.end method
