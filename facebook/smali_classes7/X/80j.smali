.class public final LX/80j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1kt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1kt",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/api/story/FetchSingleStoryParams;

.field public b:LX/1ks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ks",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:LX/3HP;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/3HM;


# direct methods
.method public constructor <init>(LX/3HM;Lcom/facebook/api/story/FetchSingleStoryParams;LX/1ks;Ljava/lang/String;LX/3HP;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/3HP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/story/FetchSingleStoryParams;",
            "LX/1ks",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/api/ufiservices/common/FeedbackCacheProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1284058
    iput-object p1, p0, LX/80j;->f:LX/3HM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1284059
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/80j;->e:Ljava/util/List;

    .line 1284060
    iput-object p2, p0, LX/80j;->a:Lcom/facebook/api/story/FetchSingleStoryParams;

    .line 1284061
    iput-object p3, p0, LX/80j;->b:LX/1ks;

    .line 1284062
    iput-object p4, p0, LX/80j;->c:Ljava/lang/String;

    .line 1284063
    iput-object p5, p0, LX/80j;->d:LX/3HP;

    .line 1284064
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1284065
    iget-object v1, p0, LX/80j;->b:LX/1ks;

    invoke-virtual {v1}, LX/1ks;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 1284066
    sget-object v2, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_0

    .line 1284067
    :goto_0
    return-object v1

    .line 1284068
    :cond_0
    iget-object v1, p0, LX/80j;->a:Lcom/facebook/api/story/FetchSingleStoryParams;

    iget-object v1, v1, Lcom/facebook/api/story/FetchSingleStoryParams;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1284069
    iget-object v1, p0, LX/80j;->f:LX/3HM;

    iget-object v1, v1, LX/3HM;->j:LX/0jU;

    iget-object v2, p0, LX/80j;->a:Lcom/facebook/api/story/FetchSingleStoryParams;

    iget-object v2, v2, Lcom/facebook/api/story/FetchSingleStoryParams;->a:Ljava/lang/String;

    .line 1284070
    invoke-static {v1, v2}, LX/0jU;->i(LX/0jU;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-static {v3}, LX/0jU;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/api/story/FetchSingleStoryResult;

    move-result-object v3

    move-object v1, v3

    .line 1284071
    :goto_1
    if-nez v1, :cond_2

    iget-object v2, p0, LX/80j;->a:Lcom/facebook/api/story/FetchSingleStoryParams;

    iget-object v2, v2, Lcom/facebook/api/story/FetchSingleStoryParams;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1284072
    iget-object v1, p0, LX/80j;->f:LX/3HM;

    iget-object v1, v1, LX/3HM;->j:LX/0jU;

    iget-object v2, p0, LX/80j;->a:Lcom/facebook/api/story/FetchSingleStoryParams;

    iget-object v2, v2, Lcom/facebook/api/story/FetchSingleStoryParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0jU;->b(Ljava/lang/String;)Lcom/facebook/api/story/FetchSingleStoryResult;

    move-result-object v1

    move-object v4, v1

    .line 1284073
    :goto_2
    if-eqz v4, :cond_1

    iget-object v1, v4, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_1

    .line 1284074
    iget-object v0, p0, LX/80j;->f:LX/3HM;

    iget-object v0, v0, LX/3HM;->j:LX/0jU;

    iget-object v1, v4, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1}, LX/0jU;->e(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;

    move-result-object v6

    .line 1284075
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v2, v4, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    .line 1284076
    iget-wide v7, v4, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v7

    .line 1284077
    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 1284078
    goto :goto_0

    :cond_2
    move-object v4, v1

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1284079
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1284080
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_0

    .line 1284081
    iget-object v0, p0, LX/80j;->f:LX/3HM;

    iget-object v1, v0, LX/3HM;->o:LX/3HE;

    .line 1284082
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284083
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3HE;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1284084
    :cond_0
    iget-object v0, p0, LX/80j;->b:LX/1ks;

    invoke-virtual {v0, p1}, LX/1ks;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    .line 1284085
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1284086
    invoke-virtual {p0}, LX/80j;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 1284087
    sget-object v1, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    .line 1284088
    :goto_0
    return-object v0

    .line 1284089
    :cond_0
    iget-object v0, p0, LX/80j;->a:Lcom/facebook/api/story/FetchSingleStoryParams;

    iget-object v0, v0, Lcom/facebook/api/story/FetchSingleStoryParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1284090
    sget-object v0, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    goto :goto_0

    .line 1284091
    :cond_1
    iget-object v0, p0, LX/80j;->b:LX/1ks;

    invoke-virtual {v0}, LX/1ks;->b()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ")",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1284092
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284093
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_2

    .line 1284094
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    .line 1284095
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284096
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->B()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1284097
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 1284098
    move-object v0, v1

    .line 1284099
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    .line 1284100
    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284101
    if-nez v0, :cond_3

    .line 1284102
    :cond_1
    :goto_1
    return-object p1

    .line 1284103
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284104
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    .line 1284105
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    .line 1284106
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284107
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1284108
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 1284109
    move-object v0, v1

    .line 1284110
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    goto :goto_0

    .line 1284111
    :cond_3
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1284112
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_1

    .line 1284113
    iget-object v0, p0, LX/80j;->c:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1284114
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284115
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1284116
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, p0, LX/80j;->c:Ljava/lang/String;

    .line 1284117
    iput-object v1, v0, LX/23u;->m:Ljava/lang/String;

    .line 1284118
    move-object v0, v0

    .line 1284119
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1284120
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    .line 1284121
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 1284122
    move-object v0, v1

    .line 1284123
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    move-object v1, v0

    .line 1284124
    :goto_2
    iget-object v0, p0, LX/80j;->f:LX/3HM;

    iget-object v2, v0, LX/3HM;->k:LX/3HO;

    new-instance v3, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1284125
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284126
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1284127
    iget-object v4, v1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v4, v4

    .line 1284128
    iget-wide v8, v1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v8

    .line 1284129
    invoke-direct {v3, v0, v4, v6, v7}, Lcom/facebook/api/story/FetchSingleStoryResult;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/0ta;J)V

    invoke-virtual {v2, v3}, LX/3HO;->a(Lcom/facebook/api/story/FetchSingleStoryResult;)Lcom/facebook/api/story/FetchSingleStoryResult;

    move-result-object v0

    .line 1284130
    if-eqz v0, :cond_4

    iget-object v2, v0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1284131
    iget-object v3, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1284132
    if-eq v2, v3, :cond_4

    .line 1284133
    invoke-static {v1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    iget-object v0, v0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1284134
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 1284135
    move-object v0, v1

    .line 1284136
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 1284137
    :cond_4
    iget-object v0, p0, LX/80j;->f:LX/3HM;

    iget-object v0, v0, LX/3HM;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 1284138
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v2, v3}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 1284140
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1284141
    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v4, :cond_8

    iget-object v0, p0, LX/80j;->d:LX/3HP;

    if-eqz v0, :cond_8

    .line 1284142
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284143
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1284144
    :goto_3
    if-eqz v0, :cond_6

    .line 1284145
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 1284146
    if-eqz v4, :cond_5

    .line 1284147
    invoke-virtual {v4, v2, v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(J)V

    .line 1284148
    iget-object v5, p0, LX/80j;->e:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1284149
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_3

    .line 1284150
    :cond_6
    const/4 v0, 0x1

    .line 1284151
    iget-object v4, p0, LX/80j;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1284152
    iget-object v4, p0, LX/80j;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1284153
    iget-object v6, p0, LX/80j;->d:LX/3HP;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3HP;->a(Ljava/lang/String;)LX/1kt;

    move-result-object v6

    .line 1284154
    new-instance v7, LX/1lO;

    invoke-direct {v7}, LX/1lO;-><init>()V

    .line 1284155
    iput-object v4, v7, LX/1lO;->k:Ljava/lang/Object;

    .line 1284156
    move-object v4, v7

    .line 1284157
    invoke-virtual {v4}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v4

    .line 1284158
    if-eqz v0, :cond_7

    .line 1284159
    invoke-interface {v6, v4}, LX/1kt;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    goto :goto_4

    .line 1284160
    :cond_8
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v4

    .line 1284161
    new-instance v5, LX/80i;

    invoke-direct {v5, p0, v4, v2, v3}, LX/80i;-><init>(LX/80j;Ljava/util/Set;J)V

    .line 1284162
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1284163
    check-cast v0, LX/0jT;

    invoke-virtual {v5, v0}, LX/1jx;->b(LX/0jT;)LX/0jT;

    .line 1284164
    invoke-static {v1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/1lO;->a(Ljava/util/Collection;)LX/1lO;

    move-result-object v0

    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    goto/16 :goto_1

    :cond_9
    move-object v1, p1

    goto/16 :goto_2
.end method
