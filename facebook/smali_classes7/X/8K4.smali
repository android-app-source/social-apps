.class public final enum LX/8K4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8K4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8K4;

.field public static final enum ALBUM:LX/8K4;

.field public static final enum APP_ATTRIBUTION:LX/8K4;

.field public static final enum CHECK_IN:LX/8K4;

.field public static final enum CREATIVE_PHOTO_EDITING:LX/8K4;

.field public static final enum CREATIVE_VIDEO_EDITING:LX/8K4;

.field public static final enum DRAFT_OTHERS:LX/8K4;

.field public static final enum FEED_ONLY:LX/8K4;

.field public static final enum LIVE:LX/8K4;

.field public static final enum MARKET_PLACE:LX/8K4;

.field public static final enum MULTI_MEDIA:LX/8K4;

.field public static final enum NON_STATUS:LX/8K4;

.field public static final enum PLUGIN_DISABLED:LX/8K4;

.field public static final enum PRODUCT_ATTACHMENT:LX/8K4;

.field public static final enum PUBLISHED_POST:LX/8K4;

.field public static final enum RESHARE:LX/8K4;

.field public static final enum SERVER_DRAFT:LX/8K4;

.field public static final enum STICKER_POST:LX/8K4;

.field public static final enum THROWBACK:LX/8K4;

.field public static final enum VIDEO_POST:LX/8K4;

.field public static final enum X_Y_TAGS:LX/8K4;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1329815
    new-instance v0, LX/8K4;

    const-string v1, "PUBLISHED_POST"

    const-string v2, "published_post"

    invoke-direct {v0, v1, v4, v2}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->PUBLISHED_POST:LX/8K4;

    .line 1329816
    new-instance v0, LX/8K4;

    const-string v1, "MARKET_PLACE"

    const-string v2, "market_place"

    invoke-direct {v0, v1, v5, v2}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->MARKET_PLACE:LX/8K4;

    .line 1329817
    new-instance v0, LX/8K4;

    const-string v1, "PRODUCT_ATTACHMENT"

    const-string v2, "product_attachment"

    invoke-direct {v0, v1, v6, v2}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->PRODUCT_ATTACHMENT:LX/8K4;

    .line 1329818
    new-instance v0, LX/8K4;

    const-string v1, "THROWBACK"

    const-string v2, "throwback"

    invoke-direct {v0, v1, v7, v2}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->THROWBACK:LX/8K4;

    .line 1329819
    new-instance v0, LX/8K4;

    const-string v1, "RESHARE"

    const-string v2, "reshare"

    invoke-direct {v0, v1, v8, v2}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->RESHARE:LX/8K4;

    .line 1329820
    new-instance v0, LX/8K4;

    const-string v1, "LIVE"

    const/4 v2, 0x5

    const-string v3, "live"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->LIVE:LX/8K4;

    .line 1329821
    new-instance v0, LX/8K4;

    const-string v1, "ALBUM"

    const/4 v2, 0x6

    const-string v3, "album"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->ALBUM:LX/8K4;

    .line 1329822
    new-instance v0, LX/8K4;

    const-string v1, "MULTI_MEDIA"

    const/4 v2, 0x7

    const-string v3, "multi_media"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->MULTI_MEDIA:LX/8K4;

    .line 1329823
    new-instance v0, LX/8K4;

    const-string v1, "CREATIVE_PHOTO_EDITING"

    const/16 v2, 0x8

    const-string v3, "creative_photo_editing"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->CREATIVE_PHOTO_EDITING:LX/8K4;

    .line 1329824
    new-instance v0, LX/8K4;

    const-string v1, "CREATIVE_VIDEO_EDITING"

    const/16 v2, 0x9

    const-string v3, "creative_video_editing"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->CREATIVE_VIDEO_EDITING:LX/8K4;

    .line 1329825
    new-instance v0, LX/8K4;

    const-string v1, "X_Y_TAGS"

    const/16 v2, 0xa

    const-string v3, "x_y_tags"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->X_Y_TAGS:LX/8K4;

    .line 1329826
    new-instance v0, LX/8K4;

    const-string v1, "NON_STATUS"

    const/16 v2, 0xb

    const-string v3, "non_status"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->NON_STATUS:LX/8K4;

    .line 1329827
    new-instance v0, LX/8K4;

    const-string v1, "FEED_ONLY"

    const/16 v2, 0xc

    const-string v3, "feed_only"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->FEED_ONLY:LX/8K4;

    .line 1329828
    new-instance v0, LX/8K4;

    const-string v1, "STICKER_POST"

    const/16 v2, 0xd

    const-string v3, "sticker_post"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->STICKER_POST:LX/8K4;

    .line 1329829
    new-instance v0, LX/8K4;

    const-string v1, "VIDEO_POST"

    const/16 v2, 0xe

    const-string v3, "video_post"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->VIDEO_POST:LX/8K4;

    .line 1329830
    new-instance v0, LX/8K4;

    const-string v1, "CHECK_IN"

    const/16 v2, 0xf

    const-string v3, "check_in"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->CHECK_IN:LX/8K4;

    .line 1329831
    new-instance v0, LX/8K4;

    const-string v1, "APP_ATTRIBUTION"

    const/16 v2, 0x10

    const-string v3, "app_attribution"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->APP_ATTRIBUTION:LX/8K4;

    .line 1329832
    new-instance v0, LX/8K4;

    const-string v1, "SERVER_DRAFT"

    const/16 v2, 0x11

    const-string v3, "server_draft"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->SERVER_DRAFT:LX/8K4;

    .line 1329833
    new-instance v0, LX/8K4;

    const-string v1, "PLUGIN_DISABLED"

    const/16 v2, 0x12

    const-string v3, "plugin_disabled"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->PLUGIN_DISABLED:LX/8K4;

    .line 1329834
    new-instance v0, LX/8K4;

    const-string v1, "DRAFT_OTHERS"

    const/16 v2, 0x13

    const-string v3, "draft_others"

    invoke-direct {v0, v1, v2, v3}, LX/8K4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8K4;->DRAFT_OTHERS:LX/8K4;

    .line 1329835
    const/16 v0, 0x14

    new-array v0, v0, [LX/8K4;

    sget-object v1, LX/8K4;->PUBLISHED_POST:LX/8K4;

    aput-object v1, v0, v4

    sget-object v1, LX/8K4;->MARKET_PLACE:LX/8K4;

    aput-object v1, v0, v5

    sget-object v1, LX/8K4;->PRODUCT_ATTACHMENT:LX/8K4;

    aput-object v1, v0, v6

    sget-object v1, LX/8K4;->THROWBACK:LX/8K4;

    aput-object v1, v0, v7

    sget-object v1, LX/8K4;->RESHARE:LX/8K4;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8K4;->LIVE:LX/8K4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8K4;->ALBUM:LX/8K4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8K4;->MULTI_MEDIA:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8K4;->CREATIVE_PHOTO_EDITING:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8K4;->CREATIVE_VIDEO_EDITING:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8K4;->X_Y_TAGS:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8K4;->NON_STATUS:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8K4;->FEED_ONLY:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8K4;->STICKER_POST:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8K4;->VIDEO_POST:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8K4;->CHECK_IN:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8K4;->APP_ATTRIBUTION:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8K4;->SERVER_DRAFT:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/8K4;->PLUGIN_DISABLED:LX/8K4;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/8K4;->DRAFT_OTHERS:LX/8K4;

    aput-object v2, v0, v1

    sput-object v0, LX/8K4;->$VALUES:[LX/8K4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1329812
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1329813
    iput-object p3, p0, LX/8K4;->analyticsName:Ljava/lang/String;

    .line 1329814
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8K4;
    .locals 1

    .prologue
    .line 1329837
    const-class v0, LX/8K4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8K4;

    return-object v0
.end method

.method public static values()[LX/8K4;
    .locals 1

    .prologue
    .line 1329836
    sget-object v0, LX/8K4;->$VALUES:[LX/8K4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8K4;

    return-object v0
.end method
