.class public LX/7yL;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1FZ;

.field private final c:LX/7yJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1279021
    const-class v0, LX/7yL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7yL;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1FZ;LX/7yJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279023
    iput-object p1, p0, LX/7yL;->b:LX/1FZ;

    .line 1279024
    iput-object p2, p0, LX/7yL;->c:LX/7yJ;

    .line 1279025
    return-void
.end method

.method public static a(LX/0QB;)LX/7yL;
    .locals 1

    .prologue
    .line 1279026
    invoke-static {p0}, LX/7yL;->b(LX/0QB;)LX/7yL;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7yL;
    .locals 3

    .prologue
    .line 1279027
    new-instance v2, LX/7yL;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v0

    check-cast v0, LX/1FZ;

    invoke-static {p0}, LX/7yJ;->a(LX/0QB;)LX/7yJ;

    move-result-object v1

    check-cast v1, LX/7yJ;

    invoke-direct {v2, v0, v1}, LX/7yL;-><init>(LX/1FZ;LX/7yJ;)V

    .line 1279028
    return-object v2
.end method


# virtual methods
.method public final a(LX/7yR;Landroid/graphics/Bitmap;LX/7yQ;)[B
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 1279029
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    const/4 v9, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 1279030
    invoke-virtual {p1}, LX/7yR;->c()F

    move-result v3

    invoke-virtual {p1}, LX/7yR;->a()F

    move-result v4

    sub-float/2addr v3, v4

    int-to-float v4, v0

    mul-float/2addr v3, v4

    .line 1279031
    invoke-virtual {p1}, LX/7yR;->d()F

    move-result v4

    invoke-virtual {p1}, LX/7yR;->b()F

    move-result v5

    sub-float/2addr v4, v5

    int-to-float v5, v2

    mul-float/2addr v4, v5

    .line 1279032
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const v4, 0x3fe66666    # 1.8f

    mul-float/2addr v3, v4

    .line 1279033
    invoke-virtual {p1}, LX/7yR;->a()F

    move-result v4

    invoke-virtual {p1}, LX/7yR;->c()F

    move-result v5

    add-float/2addr v4, v5

    div-float/2addr v4, v10

    int-to-float v5, v0

    mul-float/2addr v4, v5

    .line 1279034
    div-float v5, v3, v10

    sub-float v5, v4, v5

    invoke-static {v9, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 1279035
    int-to-float v6, v0

    div-float v7, v3, v10

    add-float/2addr v4, v7

    invoke-static {v6, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1279036
    invoke-virtual {p1}, LX/7yR;->b()F

    move-result v6

    invoke-virtual {p1}, LX/7yR;->d()F

    move-result v7

    add-float/2addr v6, v7

    div-float/2addr v6, v10

    int-to-float v7, v2

    mul-float/2addr v6, v7

    .line 1279037
    div-float v7, v3, v10

    sub-float v7, v6, v7

    invoke-static {v9, v7}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 1279038
    int-to-float v9, v2

    div-float/2addr v3, v10

    add-float/2addr v3, v6

    invoke-static {v9, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 1279039
    new-instance v6, Landroid/graphics/Rect;

    float-to-int v5, v5

    float-to-int v7, v7

    float-to-int v4, v4

    float-to-int v3, v3

    invoke-direct {v6, v5, v7, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v3, v6

    .line 1279040
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p3}, LX/7yQ;->b()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1279041
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {p3}, LX/7yQ;->b()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1279042
    :try_start_0
    iget-object v4, p0, LX/7yL;->b:LX/1FZ;

    invoke-virtual {v4, v0, v2}, LX/1FZ;->a(II)LX/1FJ;
    :try_end_0
    .catch LX/1FM; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v4

    .line 1279043
    :try_start_1
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1279044
    new-instance v2, Landroid/graphics/Canvas;

    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1279045
    const/4 v0, 0x0

    invoke-virtual {v2, p2, v3, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1279046
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1279047
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1279048
    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {p3}, LX/7yQ;->a()I

    move-result v6

    invoke-virtual {v0, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1279049
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1279050
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 1279051
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1279052
    if-eqz v4, :cond_1

    :try_start_2
    invoke-virtual {v4}, LX/1FJ;->close()V
    :try_end_2
    .catch LX/1FM; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3

    .line 1279053
    :cond_1
    :goto_0
    return-object v0

    .line 1279054
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1279055
    :catchall_0
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    :goto_1
    if-eqz v4, :cond_2

    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v4}, LX/1FJ;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch LX/1FM; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch LX/1FM; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_3

    .line 1279056
    :catch_1
    iget-object v0, p0, LX/7yL;->c:LX/7yJ;

    const-string v2, "TooManyBitmapsException"

    invoke-virtual {v0, v2}, LX/7yJ;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 1279057
    goto :goto_0

    .line 1279058
    :catch_2
    move-exception v4

    :try_start_6
    invoke-static {v2, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_6
    .catch LX/1FM; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 1279059
    :catch_3
    const-string v0, "OutOfMemory %0.3f %0.3f"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v8

    const/4 v4, 0x1

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1279060
    iget-object v2, p0, LX/7yL;->c:LX/7yJ;

    invoke-virtual {v2, v0}, LX/7yJ;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 1279061
    goto :goto_0

    .line 1279062
    :cond_3
    :try_start_7
    invoke-virtual {v4}, LX/1FJ;->close()V
    :try_end_7
    .catch LX/1FM; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_1
.end method
