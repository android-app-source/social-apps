.class public LX/6kE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final isPinProtected:Ljava/lang/Boolean;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1135507
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPaymentPinProtectionStatusData"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kE;->b:LX/1sv;

    .line 1135508
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kE;->c:LX/1sw;

    .line 1135509
    new-instance v0, LX/1sw;

    const-string v1, "isPinProtected"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kE;->d:LX/1sw;

    .line 1135510
    sput-boolean v3, LX/6kE;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1135503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135504
    iput-object p1, p0, LX/6kE;->threadKey:LX/6l9;

    .line 1135505
    iput-object p2, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    .line 1135506
    return-void
.end method

.method public static a(LX/6kE;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1135498
    iget-object v0, p0, LX/6kE;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1135499
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135500
    :cond_0
    iget-object v0, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 1135501
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'isPinProtected\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135502
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135433
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1135434
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1135435
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1135436
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaPaymentPinProtectionStatusData"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135437
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135438
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135439
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135440
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135441
    const-string v4, "threadKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135442
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135443
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135444
    iget-object v4, p0, LX/6kE;->threadKey:LX/6l9;

    if-nez v4, :cond_3

    .line 1135445
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135446
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135447
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135448
    const-string v4, "isPinProtected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135449
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135450
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135451
    iget-object v0, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    .line 1135452
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135453
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135454
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135455
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135456
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1135457
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1135458
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1135459
    :cond_3
    iget-object v4, p0, LX/6kE;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1135460
    :cond_4
    iget-object v0, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1135487
    invoke-static {p0}, LX/6kE;->a(LX/6kE;)V

    .line 1135488
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135489
    iget-object v0, p0, LX/6kE;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1135490
    sget-object v0, LX/6kE;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135491
    iget-object v0, p0, LX/6kE;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1135492
    :cond_0
    iget-object v0, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1135493
    sget-object v0, LX/6kE;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135494
    iget-object v0, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1135495
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135496
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135497
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135465
    if-nez p1, :cond_1

    .line 1135466
    :cond_0
    :goto_0
    return v0

    .line 1135467
    :cond_1
    instance-of v1, p1, LX/6kE;

    if-eqz v1, :cond_0

    .line 1135468
    check-cast p1, LX/6kE;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135469
    if-nez p1, :cond_3

    .line 1135470
    :cond_2
    :goto_1
    move v0, v2

    .line 1135471
    goto :goto_0

    .line 1135472
    :cond_3
    iget-object v0, p0, LX/6kE;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1135473
    :goto_2
    iget-object v3, p1, LX/6kE;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1135474
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135475
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135476
    iget-object v0, p0, LX/6kE;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6kE;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135477
    :cond_5
    iget-object v0, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1135478
    :goto_4
    iget-object v3, p1, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1135479
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1135480
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135481
    iget-object v0, p0, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kE;->isPinProtected:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1135482
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1135483
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1135484
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1135485
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1135486
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135464
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135461
    sget-boolean v0, LX/6kE;->a:Z

    .line 1135462
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kE;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135463
    return-object v0
.end method
