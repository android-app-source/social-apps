.class public final LX/7lm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7ll;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;)V
    .locals 0

    .prologue
    .line 1235671
    iput-object p1, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 3

    .prologue
    .line 1235681
    iget-object v0, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    .line 1235682
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v2, v2, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getRetrySource()LX/5Ro;

    move-result-object v2

    :goto_0
    move-object v2, v2

    .line 1235683
    invoke-virtual {v0, v1, v2}, LX/0gd;->a(Ljava/lang/String;LX/5Ro;)V

    .line 1235684
    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 3

    .prologue
    .line 1235685
    iget-object v0, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_PUBLISH_START:LX/0ge;

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1235686
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 6

    .prologue
    .line 1235677
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v2

    .line 1235678
    iget-object v0, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v0, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1235679
    iget-object v0, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    iget-object v1, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    iget-object v2, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->i()I

    move-result v5

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LX/0gd;->a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1235680
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1235672
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v2

    .line 1235673
    iget-object v0, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v0, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1235674
    iget-object v0, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f:LX/1CW;

    invoke-virtual {v0, p3, v1, v1}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v5

    .line 1235675
    iget-object v0, p0, LX/7lm;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    iget-object v1, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    iget-object v2, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->i()I

    move-result v7

    move-object v4, p1

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, LX/0gd;->a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/fbservice/service/ServiceException;I)V

    .line 1235676
    return-void
.end method
