.class public LX/6iI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:LX/0rS;

.field public b:LX/6ek;

.field public c:LX/6em;

.field public d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6el;",
            ">;"
        }
    .end annotation
.end field

.field public e:J

.field public f:I

.field public g:Lcom/facebook/http/interfaces/RequestPriority;

.field public h:LX/6iH;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1129191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129192
    sget-object v0, LX/6em;->ALL:LX/6em;

    iput-object v0, p0, LX/6iI;->c:LX/6em;

    .line 1129193
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1129194
    iput-object v0, p0, LX/6iI;->d:LX/0Rf;

    .line 1129195
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/6iI;->e:J

    .line 1129196
    const/16 v0, 0x14

    iput v0, p0, LX/6iI;->f:I

    .line 1129197
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    iput-object v0, p0, LX/6iI;->g:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1129198
    sget-object v0, LX/6iH;->NONE:LX/6iH;

    iput-object v0, p0, LX/6iI;->h:LX/6iH;

    .line 1129199
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)LX/6iI;
    .locals 4

    .prologue
    .line 1129200
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v0, v0

    .line 1129201
    iput-object v0, p0, LX/6iI;->a:LX/0rS;

    .line 1129202
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v0, v0

    .line 1129203
    iput-object v0, p0, LX/6iI;->b:LX/6ek;

    .line 1129204
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    move-object v0, v0

    .line 1129205
    iput-object v0, p0, LX/6iI;->c:LX/6em;

    .line 1129206
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    move-object v0, v0

    .line 1129207
    iput-object v0, p0, LX/6iI;->d:LX/0Rf;

    .line 1129208
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    move-object v0, v0

    .line 1129209
    iput-object v0, p0, LX/6iI;->h:LX/6iH;

    .line 1129210
    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    move-wide v0, v2

    .line 1129211
    iput-wide v0, p0, LX/6iI;->e:J

    .line 1129212
    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v0

    iput v0, p0, LX/6iI;->f:I

    .line 1129213
    return-object p0
.end method

.method public final i()Lcom/facebook/messaging/service/model/FetchThreadListParams;
    .locals 1

    .prologue
    .line 1129214
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/service/model/FetchThreadListParams;-><init>(LX/6iI;)V

    return-object v0
.end method
