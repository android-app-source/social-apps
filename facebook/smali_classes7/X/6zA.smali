.class public LX/6zA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wl;


# instance fields
.field private final a:LX/0SG;

.field public final b:Ljava/util/Calendar;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160829
    iput-object p1, p0, LX/6zA;->a:LX/0SG;

    .line 1160830
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/6zA;->b:Ljava/util/Calendar;

    .line 1160831
    iput-object p2, p0, LX/6zA;->c:Landroid/content/res/Resources;

    .line 1160832
    return-void
.end method

.method private a(Ljava/lang/String;J)Z
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1160833
    const-string v0, "/"

    invoke-static {v0}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    .line 1160834
    const/4 v0, 0x0

    invoke-static {v1, v0}, LX/0Ph;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1160835
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/0Ph;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1160836
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1160837
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1160838
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1160839
    add-int/lit8 v4, v0, -0x1

    .line 1160840
    if-ltz v4, :cond_0

    const/16 v5, 0xb

    if-le v4, v5, :cond_1

    .line 1160841
    :cond_0
    :goto_0
    move v0, v2

    .line 1160842
    return v0

    .line 1160843
    :cond_1
    iget-object v5, p0, LX/6zA;->b:Ljava/util/Calendar;

    invoke-virtual {v5, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1160844
    iget-object v5, p0, LX/6zA;->b:Ljava/util/Calendar;

    invoke-virtual {v5, v3}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit16 v5, v5, -0x7d0

    .line 1160845
    iget-object v6, p0, LX/6zA;->b:Ljava/util/Calendar;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 1160846
    if-gt v1, v5, :cond_2

    if-ne v1, v5, :cond_0

    if-lt v4, v6, :cond_0

    :cond_2
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6z8;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1160847
    invoke-interface {p1}, LX/6z8;->a()Ljava/lang/String;

    move-result-object v1

    .line 1160848
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1160849
    :cond_0
    :goto_0
    return v0

    .line 1160850
    :cond_1
    const-string v2, "\\d{2}\\/\\d{2}"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1160851
    iget-object v0, p0, LX/6zA;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, LX/6zA;->a(Ljava/lang/String;J)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/6z8;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1160852
    iget-object v0, p0, LX/6zA;->c:Landroid/content/res/Resources;

    const v1, 0x7f081e70

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
