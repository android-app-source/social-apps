.class public final enum LX/6iD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6iD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6iD;

.field public static final enum CACHE:LX/6iD;

.field public static final enum DATABASE:LX/6iD;

.field public static final enum SERVER:LX/6iD;


# instance fields
.field public final parcelValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1129049
    new-instance v0, LX/6iD;

    const-string v1, "CACHE"

    invoke-direct {v0, v1, v2, v2}, LX/6iD;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6iD;->CACHE:LX/6iD;

    .line 1129050
    new-instance v0, LX/6iD;

    const-string v1, "DATABASE"

    invoke-direct {v0, v1, v3, v3}, LX/6iD;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6iD;->DATABASE:LX/6iD;

    .line 1129051
    new-instance v0, LX/6iD;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v4, v4}, LX/6iD;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6iD;->SERVER:LX/6iD;

    .line 1129052
    const/4 v0, 0x3

    new-array v0, v0, [LX/6iD;

    sget-object v1, LX/6iD;->CACHE:LX/6iD;

    aput-object v1, v0, v2

    sget-object v1, LX/6iD;->DATABASE:LX/6iD;

    aput-object v1, v0, v3

    sget-object v1, LX/6iD;->SERVER:LX/6iD;

    aput-object v1, v0, v4

    sput-object v0, LX/6iD;->$VALUES:[LX/6iD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1129053
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1129054
    iput p3, p0, LX/6iD;->parcelValue:I

    .line 1129055
    return-void
.end method

.method public static fromParcelValue(I)LX/6iD;
    .locals 1

    .prologue
    .line 1129056
    packed-switch p0, :pswitch_data_0

    .line 1129057
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1129058
    :pswitch_0
    sget-object v0, LX/6iD;->CACHE:LX/6iD;

    .line 1129059
    :goto_0
    return-object v0

    .line 1129060
    :pswitch_1
    sget-object v0, LX/6iD;->DATABASE:LX/6iD;

    goto :goto_0

    .line 1129061
    :pswitch_2
    sget-object v0, LX/6iD;->SERVER:LX/6iD;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/6iD;
    .locals 1

    .prologue
    .line 1129062
    const-class v0, LX/6iD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6iD;

    return-object v0
.end method

.method public static values()[LX/6iD;
    .locals 1

    .prologue
    .line 1129063
    sget-object v0, LX/6iD;->$VALUES:[LX/6iD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6iD;

    return-object v0
.end method
