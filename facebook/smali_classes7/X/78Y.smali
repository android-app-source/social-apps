.class public final LX/78Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;)V
    .locals 0

    .prologue
    .line 1172978
    iput-object p1, p0, LX/78Y;->a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1172979
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1172980
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1172981
    iget-object v1, p0, LX/78Y;->a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 1172982
    :goto_0
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    if-nez p0, :cond_1

    .line 1172983
    :goto_1
    return-void

    .line 1172984
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1172985
    :cond_1
    if-eqz v0, :cond_2

    .line 1172986
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a00d2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1172987
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    iget-object p1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1172988
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setClickable(Z)V

    goto :goto_1

    .line 1172989
    :cond_2
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a010e

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1172990
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setClickable(Z)V

    goto :goto_1
.end method
