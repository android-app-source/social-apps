.class public LX/6k6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final payload:[B

.field public final timestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1134741
    new-instance v0, LX/1sv;

    const-string v1, "DeltaNonPersistedPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6k6;->b:LX/1sv;

    .line 1134742
    new-instance v0, LX/1sw;

    const-string v1, "payload"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k6;->c:LX/1sw;

    .line 1134743
    new-instance v0, LX/1sw;

    const-string v1, "timestamp"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k6;->d:LX/1sw;

    .line 1134744
    sput-boolean v4, LX/6k6;->a:Z

    return-void
.end method

.method public constructor <init>([BLjava/lang/Long;)V
    .locals 0

    .prologue
    .line 1134809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134810
    iput-object p1, p0, LX/6k6;->payload:[B

    .line 1134811
    iput-object p2, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    .line 1134812
    return-void
.end method

.method public static a(LX/6k6;)V
    .locals 4

    .prologue
    .line 1134806
    iget-object v0, p0, LX/6k6;->payload:[B

    if-nez v0, :cond_0

    .line 1134807
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'payload\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6k6;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134808
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x80

    .line 1134771
    if-eqz p2, :cond_2

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1134772
    :goto_0
    if-eqz p2, :cond_3

    const-string v0, "\n"

    move-object v3, v0

    .line 1134773
    :goto_1
    if-eqz p2, :cond_4

    const-string v0, " "

    .line 1134774
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaNonPersistedPayload"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1134775
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134776
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134777
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134778
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134779
    const-string v1, "payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134780
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134781
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134782
    iget-object v1, p0, LX/6k6;->payload:[B

    if-nez v1, :cond_5

    .line 1134783
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134784
    :cond_0
    :goto_3
    iget-object v1, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 1134785
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134786
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134787
    const-string v1, "timestamp"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134788
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134789
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134790
    iget-object v0, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    if-nez v0, :cond_9

    .line 1134791
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134792
    :cond_1
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134793
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134794
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1134795
    :cond_2
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1134796
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1134797
    :cond_4
    const-string v0, ""

    goto/16 :goto_2

    .line 1134798
    :cond_5
    iget-object v1, p0, LX/6k6;->payload:[B

    array-length v1, v1

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 1134799
    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v6, :cond_8

    .line 1134800
    if-eqz v2, :cond_6

    const-string v1, " "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134801
    :cond_6
    iget-object v1, p0, LX/6k6;->payload:[B

    aget-byte v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v7, 0x1

    if-le v1, v7, :cond_7

    iget-object v1, p0, LX/6k6;->payload:[B

    aget-byte v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, LX/6k6;->payload:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    :goto_6
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134802
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 1134803
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "0"

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, LX/6k6;->payload:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    .line 1134804
    :cond_8
    iget-object v1, p0, LX/6k6;->payload:[B

    array-length v1, v1

    if-le v1, v8, :cond_0

    const-string v1, " ..."

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1134805
    :cond_9
    iget-object v0, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1134813
    invoke-static {p0}, LX/6k6;->a(LX/6k6;)V

    .line 1134814
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1134815
    iget-object v0, p0, LX/6k6;->payload:[B

    if-eqz v0, :cond_0

    .line 1134816
    sget-object v0, LX/6k6;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134817
    iget-object v0, p0, LX/6k6;->payload:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 1134818
    :cond_0
    iget-object v0, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1134819
    iget-object v0, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1134820
    sget-object v0, LX/6k6;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134821
    iget-object v0, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1134822
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1134823
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1134824
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1134749
    if-nez p1, :cond_1

    .line 1134750
    :cond_0
    :goto_0
    return v0

    .line 1134751
    :cond_1
    instance-of v1, p1, LX/6k6;

    if-eqz v1, :cond_0

    .line 1134752
    check-cast p1, LX/6k6;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1134753
    if-nez p1, :cond_3

    .line 1134754
    :cond_2
    :goto_1
    move v0, v2

    .line 1134755
    goto :goto_0

    .line 1134756
    :cond_3
    iget-object v0, p0, LX/6k6;->payload:[B

    if-eqz v0, :cond_8

    move v0, v1

    .line 1134757
    :goto_2
    iget-object v3, p1, LX/6k6;->payload:[B

    if-eqz v3, :cond_9

    move v3, v1

    .line 1134758
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1134759
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134760
    iget-object v0, p0, LX/6k6;->payload:[B

    iget-object v3, p1, LX/6k6;->payload:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1134761
    :cond_5
    iget-object v0, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1134762
    :goto_4
    iget-object v3, p1, LX/6k6;->timestamp:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1134763
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1134764
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134765
    iget-object v0, p0, LX/6k6;->timestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6k6;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1134766
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1134767
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1134768
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1134769
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1134770
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1134748
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1134745
    sget-boolean v0, LX/6k6;->a:Z

    .line 1134746
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6k6;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1134747
    return-object v0
.end method
