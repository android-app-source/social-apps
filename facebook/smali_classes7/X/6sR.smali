.class public LX/6sR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/payments/checkout/CheckoutParams;

.field public b:Z

.field public c:Lcom/facebook/payments/model/PaymentsPin;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

.field public n:Landroid/os/Parcelable;

.field public o:Lcom/facebook/flatbuffers/Flattenable;

.field public p:LX/6tr;

.field public q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

.field public s:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;>;"
        }
    .end annotation
.end field

.field public t:Ljava/lang/String;

.field public u:I

.field public v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

.field public w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

.field public x:Ljava/lang/Integer;

.field public y:Lcom/facebook/payments/currency/CurrencyAmount;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1153372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;
    .locals 2

    .prologue
    .line 1153296
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    .line 1153297
    iput-object v0, p0, LX/6sR;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1153298
    move-object v0, p0

    .line 1153299
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->d()Z

    move-result v1

    .line 1153300
    iput-boolean v1, v0, LX/6sR;->b:Z

    .line 1153301
    move-object v0, v0

    .line 1153302
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->e()Lcom/facebook/payments/model/PaymentsPin;

    move-result-object v1

    .line 1153303
    iput-object v1, v0, LX/6sR;->c:Lcom/facebook/payments/model/PaymentsPin;

    .line 1153304
    move-object v0, v0

    .line 1153305
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->f()Ljava/lang/String;

    move-result-object v1

    .line 1153306
    iput-object v1, v0, LX/6sR;->d:Ljava/lang/String;

    .line 1153307
    move-object v0, v0

    .line 1153308
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->g()Ljava/lang/String;

    move-result-object v1

    .line 1153309
    iput-object v1, v0, LX/6sR;->e:Ljava/lang/String;

    .line 1153310
    move-object v0, v0

    .line 1153311
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v1

    .line 1153312
    iput-object v1, v0, LX/6sR;->f:LX/0am;

    .line 1153313
    move-object v0, v0

    .line 1153314
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->i()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6sR;->a(Ljava/util/List;)LX/6sR;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->j()LX/0am;

    move-result-object v1

    .line 1153315
    iput-object v1, v0, LX/6sR;->h:LX/0am;

    .line 1153316
    move-object v0, v0

    .line 1153317
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->k()LX/0Px;

    move-result-object v1

    .line 1153318
    iput-object v1, v0, LX/6sR;->i:LX/0Px;

    .line 1153319
    move-object v0, v0

    .line 1153320
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->l()LX/0am;

    move-result-object v1

    .line 1153321
    iput-object v1, v0, LX/6sR;->j:LX/0am;

    .line 1153322
    move-object v0, v0

    .line 1153323
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->m()LX/0am;

    move-result-object v1

    .line 1153324
    iput-object v1, v0, LX/6sR;->k:LX/0am;

    .line 1153325
    move-object v0, v0

    .line 1153326
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->n()LX/0Px;

    move-result-object v1

    .line 1153327
    iput-object v1, v0, LX/6sR;->l:LX/0Px;

    .line 1153328
    move-object v0, v0

    .line 1153329
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v1

    .line 1153330
    iput-object v1, v0, LX/6sR;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1153331
    move-object v0, v0

    .line 1153332
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v1

    .line 1153333
    iput-object v1, v0, LX/6sR;->n:Landroid/os/Parcelable;

    .line 1153334
    move-object v0, v0

    .line 1153335
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->q()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    .line 1153336
    iput-object v1, v0, LX/6sR;->o:Lcom/facebook/flatbuffers/Flattenable;

    .line 1153337
    move-object v0, v0

    .line 1153338
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v1

    .line 1153339
    iput-object v1, v0, LX/6sR;->p:LX/6tr;

    .line 1153340
    move-object v0, v0

    .line 1153341
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v1

    .line 1153342
    iput-object v1, v0, LX/6sR;->q:LX/0am;

    .line 1153343
    move-object v0, v0

    .line 1153344
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object v1

    .line 1153345
    iput-object v1, v0, LX/6sR;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1153346
    move-object v0, v0

    .line 1153347
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v1

    .line 1153348
    iput-object v1, v0, LX/6sR;->s:LX/0P1;

    .line 1153349
    move-object v0, v0

    .line 1153350
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->v()Ljava/lang/String;

    move-result-object v1

    .line 1153351
    iput-object v1, v0, LX/6sR;->t:Ljava/lang/String;

    .line 1153352
    move-object v0, v0

    .line 1153353
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->w()I

    move-result v1

    .line 1153354
    iput v1, v0, LX/6sR;->u:I

    .line 1153355
    move-object v0, v0

    .line 1153356
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->x()Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    move-result-object v1

    .line 1153357
    iput-object v1, v0, LX/6sR;->v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    .line 1153358
    move-object v0, v0

    .line 1153359
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->y()Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    move-result-object v1

    .line 1153360
    iput-object v1, v0, LX/6sR;->w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    .line 1153361
    move-object v0, v0

    .line 1153362
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->z()Ljava/lang/Integer;

    move-result-object v1

    .line 1153363
    iput-object v1, v0, LX/6sR;->x:Ljava/lang/Integer;

    .line 1153364
    move-object v0, v0

    .line 1153365
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->A()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 1153366
    iput-object v1, v0, LX/6sR;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153367
    move-object v0, v0

    .line 1153368
    return-object v0
.end method

.method public final a(Ljava/util/List;)LX/6sR;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;)",
            "LX/6sR;"
        }
    .end annotation

    .prologue
    .line 1153370
    check-cast p1, LX/0Px;

    iput-object p1, p0, LX/6sR;->g:LX/0Px;

    .line 1153371
    return-object p0
.end method

.method public final z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;
    .locals 1

    .prologue
    .line 1153369
    new-instance v0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-direct {v0, p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;-><init>(LX/6sR;)V

    return-object v0
.end method
