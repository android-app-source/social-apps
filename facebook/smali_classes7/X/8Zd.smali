.class public final LX/8Zd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1365031
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1365032
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1365033
    :goto_0
    return v1

    .line 1365034
    :cond_0
    const-string v8, "is_sponsored"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1365035
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    .line 1365036
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1365037
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1365038
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1365039
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1365040
    const-string v8, "global_share"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1365041
    invoke-static {p0, p1}, LX/8Zl;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1365042
    :cond_2
    const-string v8, "social_context_profiles"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1365043
    invoke-static {p0, p1}, LX/8aR;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1365044
    :cond_3
    const-string v8, "social_context_text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1365045
    invoke-static {p0, p1}, LX/8aS;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1365046
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1365047
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1365048
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1365049
    if-eqz v0, :cond_6

    .line 1365050
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 1365051
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1365052
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1365053
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1365054
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1365055
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365056
    if-eqz v0, :cond_0

    .line 1365057
    const-string v1, "global_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365058
    invoke-static {p0, v0, p2, p3}, LX/8Zl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1365059
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1365060
    if-eqz v0, :cond_1

    .line 1365061
    const-string v1, "is_sponsored"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365062
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1365063
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365064
    if-eqz v0, :cond_2

    .line 1365065
    const-string v1, "social_context_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365066
    invoke-static {p0, v0, p2, p3}, LX/8aR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1365067
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365068
    if-eqz v0, :cond_3

    .line 1365069
    const-string v1, "social_context_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365070
    invoke-static {p0, v0, p2}, LX/8aS;->a(LX/15i;ILX/0nX;)V

    .line 1365071
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1365072
    return-void
.end method
