.class public final LX/7NV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:LX/7Mr;

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(LX/7Mr;)V
    .locals 0

    .prologue
    .line 1200326
    iput-object p1, p0, LX/7NV;->a:LX/7Mr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6

    .prologue
    .line 1200327
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    invoke-virtual {v0, p2}, LX/7Mr;->c(I)V

    .line 1200328
    if-nez p3, :cond_0

    .line 1200329
    :goto_0
    return-void

    .line 1200330
    :cond_0
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->e:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    .line 1200331
    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget v1, v1, LX/7Mr;->d:I

    int-to-long v2, v1

    int-to-long v4, p2

    mul-long/2addr v2, v4

    int-to-long v0, v0

    div-long v0, v2, v0

    long-to-int v0, v0

    .line 1200332
    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget v1, v1, LX/7Mr;->s:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1200333
    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget v1, v1, LX/7Mr;->s:I

    add-int/2addr v0, v1

    .line 1200334
    :cond_1
    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    if-eqz v1, :cond_2

    .line 1200335
    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2qc;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-direct {v2, v0, v3}, LX/2qc;-><init>(ILX/04g;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 1200336
    :cond_2
    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget-object v2, p0, LX/7NV;->a:LX/7Mr;

    iget v2, v2, LX/7Mr;->d:I

    invoke-virtual {v1, v0, v2}, LX/7Mr;->b(II)V

    goto :goto_0
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3

    .prologue
    .line 1200337
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 1200338
    :cond_0
    :goto_0
    return-void

    .line 1200339
    :cond_1
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->x:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 1200340
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->e:Landroid/widget/SeekBar;

    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget-object v1, v1, LX/7Mr;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1200341
    :cond_2
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    invoke-virtual {v0}, LX/7Mr;->u()V

    .line 1200342
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->l()Z

    move-result v0

    iput-boolean v0, p0, LX/7NV;->b:Z

    .line 1200343
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    iput v0, p0, LX/7NV;->c:I

    .line 1200344
    iget-boolean v0, p0, LX/7NV;->b:Z

    if-eqz v0, :cond_0

    .line 1200345
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qb;

    sget-object v2, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    invoke-direct {v1, v2}, LX/2qb;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 9

    .prologue
    .line 1200346
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 1200347
    :cond_0
    :goto_0
    return-void

    .line 1200348
    :cond_1
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->w:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 1200349
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->e:Landroid/widget/SeekBar;

    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget-object v1, v1, LX/7Mr;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1200350
    :cond_2
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1200351
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    invoke-virtual {v0}, LX/7Mr;->v()V

    .line 1200352
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v5

    .line 1200353
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1200354
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/7Mr;->f:LX/1C2;

    iget-object v1, p0, LX/7NV;->a:LX/7Mr;

    iget-object v1, v1, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/7NV;->a:LX/7Mr;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    .line 1200355
    iget-object v3, v2, LX/2pb;->D:LX/04G;

    move-object v2, v3

    .line 1200356
    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget v4, p0, LX/7NV;->c:I

    iget-object v6, p0, LX/7NV;->a:LX/7Mr;

    iget-object v6, v6, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v7, p0, LX/7NV;->a:LX/7Mr;

    iget-object v7, v7, LX/2oy;->j:LX/2pb;

    invoke-virtual {v7}, LX/2pb;->s()LX/04D;

    move-result-object v7

    iget-object v8, p0, LX/7NV;->a:LX/7Mr;

    iget-object v8, v8, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v8, v8, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v0 .. v8}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;Z)LX/1C2;

    .line 1200357
    :cond_3
    iget-boolean v0, p0, LX/7NV;->b:Z

    if-eqz v0, :cond_0

    .line 1200358
    iget-object v0, p0, LX/7NV;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qa;

    sget-object v2, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    invoke-direct {v1, v2}, LX/2qa;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method
