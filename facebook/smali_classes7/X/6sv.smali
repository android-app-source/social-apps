.class public final enum LX/6sv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6sv;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6sv;

.field public static final enum INIT:LX/6sv;

.field public static final enum PAYMENT_COMPLETED:LX/6sv;

.field public static final enum PROCESSING_PAYMENT:LX/6sv;

.field public static final enum READY_FOR_PAYMENT:LX/6sv;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1153857
    new-instance v0, LX/6sv;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, LX/6sv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6sv;->INIT:LX/6sv;

    .line 1153858
    new-instance v0, LX/6sv;

    const-string v1, "READY_FOR_PAYMENT"

    invoke-direct {v0, v1, v3}, LX/6sv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6sv;->READY_FOR_PAYMENT:LX/6sv;

    .line 1153859
    new-instance v0, LX/6sv;

    const-string v1, "PROCESSING_PAYMENT"

    invoke-direct {v0, v1, v4}, LX/6sv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6sv;->PROCESSING_PAYMENT:LX/6sv;

    .line 1153860
    new-instance v0, LX/6sv;

    const-string v1, "PAYMENT_COMPLETED"

    invoke-direct {v0, v1, v5}, LX/6sv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6sv;->PAYMENT_COMPLETED:LX/6sv;

    .line 1153861
    const/4 v0, 0x4

    new-array v0, v0, [LX/6sv;

    sget-object v1, LX/6sv;->INIT:LX/6sv;

    aput-object v1, v0, v2

    sget-object v1, LX/6sv;->READY_FOR_PAYMENT:LX/6sv;

    aput-object v1, v0, v3

    sget-object v1, LX/6sv;->PROCESSING_PAYMENT:LX/6sv;

    aput-object v1, v0, v4

    sget-object v1, LX/6sv;->PAYMENT_COMPLETED:LX/6sv;

    aput-object v1, v0, v5

    sput-object v0, LX/6sv;->$VALUES:[LX/6sv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1153862
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6sv;
    .locals 1

    .prologue
    .line 1153863
    const-class v0, LX/6sv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6sv;

    return-object v0
.end method

.method public static values()[LX/6sv;
    .locals 1

    .prologue
    .line 1153864
    sget-object v0, LX/6sv;->$VALUES:[LX/6sv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6sv;

    return-object v0
.end method
