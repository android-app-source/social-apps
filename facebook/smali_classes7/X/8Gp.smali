.class public LX/8Gp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/AddPhotoTagParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320037
    const-class v0, LX/8Gp;

    sput-object v0, LX/8Gp;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320036
    return-void
.end method

.method public static a(LX/0QB;)LX/8Gp;
    .locals 1

    .prologue
    .line 1320007
    new-instance v0, LX/8Gp;

    invoke-direct {v0}, LX/8Gp;-><init>()V

    .line 1320008
    move-object v0, v0

    .line 1320009
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320013
    check-cast p1, Lcom/facebook/photos/data/method/AddPhotoTagParams;

    .line 1320014
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320015
    iget-object v0, p1, Lcom/facebook/photos/data/method/AddPhotoTagParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1320016
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320017
    invoke-virtual {p1}, Lcom/facebook/photos/data/method/AddPhotoTagParams;->d()Ljava/lang/String;

    .line 1320018
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v1, LX/8Gp;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1320019
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1320020
    move-object v0, v0

    .line 1320021
    const-string v1, "POST"

    .line 1320022
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1320023
    move-object v0, v0

    .line 1320024
    iget-object v1, p1, Lcom/facebook/photos/data/method/AddPhotoTagParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1320025
    const-string v2, "%s/tags"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v1, v3, p0

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1320026
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1320027
    move-object v0, v0

    .line 1320028
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tags"

    invoke-virtual {p1}, Lcom/facebook/photos/data/method/AddPhotoTagParams;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1320029
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1320030
    move-object v0, v0

    .line 1320031
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320032
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320033
    move-object v0, v0

    .line 1320034
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1320010
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1320011
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320012
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
