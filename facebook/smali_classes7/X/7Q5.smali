.class public final LX/7Q5;
.super Ljava/io/FilterOutputStream;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public b:I


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 1203578
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1203579
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7Q5;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1203580
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1203567
    iget-object v0, p0, LX/7Q5;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1203568
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "Prefetch uri cancelled"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1203569
    :cond_0
    return-void
.end method


# virtual methods
.method public final write(I)V
    .locals 1

    .prologue
    .line 1203581
    invoke-direct {p0}, LX/7Q5;->d()V

    .line 1203582
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 1203583
    iget v0, p0, LX/7Q5;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7Q5;->b:I

    .line 1203584
    return-void
.end method

.method public final write([B)V
    .locals 2

    .prologue
    .line 1203574
    invoke-direct {p0}, LX/7Q5;->d()V

    .line 1203575
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 1203576
    iget v0, p0, LX/7Q5;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, LX/7Q5;->b:I

    .line 1203577
    return-void
.end method

.method public final write([BII)V
    .locals 1

    .prologue
    .line 1203570
    invoke-direct {p0}, LX/7Q5;->d()V

    .line 1203571
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 1203572
    iget v0, p0, LX/7Q5;->b:I

    add-int/2addr v0, p3

    iput v0, p0, LX/7Q5;->b:I

    .line 1203573
    return-void
.end method
