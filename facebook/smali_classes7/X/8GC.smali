.class public final enum LX/8GC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8GC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8GC;

.field public static final enum COLLECTION_NAME:LX/8GC;

.field public static final enum DELTA_TYPE:LX/8GC;

.field public static final enum FRAME_ASSET_URI:LX/8GC;

.field public static final enum FRAME_ID:LX/8GC;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1318726
    new-instance v0, LX/8GC;

    const-string v1, "DELTA_TYPE"

    const-string v2, "delta_type"

    invoke-direct {v0, v1, v3, v2}, LX/8GC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GC;->DELTA_TYPE:LX/8GC;

    .line 1318727
    new-instance v0, LX/8GC;

    const-string v1, "COLLECTION_NAME"

    const-string v2, "collection_name"

    invoke-direct {v0, v1, v4, v2}, LX/8GC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GC;->COLLECTION_NAME:LX/8GC;

    .line 1318728
    new-instance v0, LX/8GC;

    const-string v1, "FRAME_ID"

    const-string v2, "frame_id"

    invoke-direct {v0, v1, v5, v2}, LX/8GC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GC;->FRAME_ID:LX/8GC;

    .line 1318729
    new-instance v0, LX/8GC;

    const-string v1, "FRAME_ASSET_URI"

    const-string v2, "asset_uri"

    invoke-direct {v0, v1, v6, v2}, LX/8GC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GC;->FRAME_ASSET_URI:LX/8GC;

    .line 1318730
    const/4 v0, 0x4

    new-array v0, v0, [LX/8GC;

    sget-object v1, LX/8GC;->DELTA_TYPE:LX/8GC;

    aput-object v1, v0, v3

    sget-object v1, LX/8GC;->COLLECTION_NAME:LX/8GC;

    aput-object v1, v0, v4

    sget-object v1, LX/8GC;->FRAME_ID:LX/8GC;

    aput-object v1, v0, v5

    sget-object v1, LX/8GC;->FRAME_ASSET_URI:LX/8GC;

    aput-object v1, v0, v6

    sput-object v0, LX/8GC;->$VALUES:[LX/8GC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1318731
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1318732
    iput-object p3, p0, LX/8GC;->mName:Ljava/lang/String;

    .line 1318733
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8GC;
    .locals 1

    .prologue
    .line 1318734
    const-class v0, LX/8GC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8GC;

    return-object v0
.end method

.method public static values()[LX/8GC;
    .locals 1

    .prologue
    .line 1318735
    sget-object v0, LX/8GC;->$VALUES:[LX/8GC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8GC;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1318736
    iget-object v0, p0, LX/8GC;->mName:Ljava/lang/String;

    return-object v0
.end method
