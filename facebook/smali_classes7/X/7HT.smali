.class public abstract LX/7HT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7HS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/7HS",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/7Hn;

.field private d:LX/2Sq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Sq",
            "<TT;>;"
        }
    .end annotation
.end field

.field private e:LX/2Sp;

.field public f:LX/7HZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1190807
    const-class v0, LX/7HT;

    sput-object v0, LX/7HT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/7Hq;)V
    .locals 2

    .prologue
    .line 1190795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190796
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    iput-object v0, p0, LX/7HT;->f:LX/7HZ;

    .line 1190797
    iput-object p1, p0, LX/7HT;->b:LX/1Ck;

    .line 1190798
    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    move-result-object v0

    .line 1190799
    sget-object v1, LX/7Hp;->a:[I

    invoke-virtual {v0}, LX/7HY;->ordinal()I

    move-result p1

    aget v1, v1, p1

    packed-switch v1, :pswitch_data_0

    .line 1190800
    const/4 v1, 0x0

    :goto_0
    move-object v0, v1

    .line 1190801
    iput-object v0, p0, LX/7HT;->c:LX/7Hn;

    .line 1190802
    return-void

    .line 1190803
    :pswitch_0
    iget-object v1, p2, LX/7Hq;->a:LX/7Hn;

    goto :goto_0

    .line 1190804
    :pswitch_1
    iget-object v1, p2, LX/7Hq;->b:LX/7Hn;

    goto :goto_0

    .line 1190805
    :pswitch_2
    iget-object v1, p2, LX/7Hq;->c:LX/7Hn;

    goto :goto_0

    .line 1190806
    :pswitch_3
    iget-object v1, p2, LX/7Hq;->d:LX/7Hn;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(LX/7HT;LX/7Hi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190792
    iget-object v0, p0, LX/7HT;->d:LX/2Sq;

    if-eqz v0, :cond_0

    .line 1190793
    iget-object v0, p0, LX/7HT;->d:LX/2Sq;

    invoke-interface {v0, p1}, LX/2Sq;->a(LX/7Hi;)V

    .line 1190794
    :cond_0
    return-void
.end method

.method public static e(LX/7HT;LX/7Hi;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190708
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 1190709
    iget-object v1, v0, LX/7B6;->b:Ljava/lang/String;

    .line 1190710
    iget-object v0, p0, LX/7HT;->c:LX/7Hn;

    if-nez v0, :cond_0

    .line 1190711
    :goto_0
    return-void

    .line 1190712
    :cond_0
    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    .line 1190713
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 1190714
    iget-object v2, v0, LX/7Hc;->b:LX/0Px;

    move-object v2, v2

    .line 1190715
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1190716
    :cond_1
    iget-object v0, p0, LX/7HT;->c:LX/7Hn;

    .line 1190717
    iget-object v2, p1, LX/7Hi;->a:LX/7B6;

    move-object v2, v2

    .line 1190718
    iget-object v2, v2, LX/7B6;->c:Ljava/lang/String;

    .line 1190719
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190720
    if-nez v2, :cond_2

    const-string v2, ""

    .line 1190721
    :cond_2
    invoke-virtual {v0, v2, v1}, LX/7Hn;->a(Ljava/lang/String;Ljava/lang/String;)LX/7Hi;

    move-result-object v3

    .line 1190722
    if-nez v3, :cond_3

    .line 1190723
    :goto_2
    move-object v3, p1

    .line 1190724
    invoke-virtual {v0, v2, v1, v3}, LX/7Hn;->a(Ljava/lang/String;Ljava/lang/String;LX/7Hi;)V

    .line 1190725
    goto :goto_0

    .line 1190726
    :cond_3
    iget-object v4, p1, LX/7Hi;->a:LX/7B6;

    .line 1190727
    iget-object v5, v3, LX/7Hi;->a:LX/7B6;

    move-object v5, v5

    .line 1190728
    invoke-virtual {v4, v5}, LX/7B6;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1190729
    iget-object v4, p1, LX/7Hi;->c:LX/7HY;

    move-object v4, v4

    .line 1190730
    iget-object v5, v3, LX/7Hi;->c:LX/7HY;

    move-object v5, v5

    .line 1190731
    invoke-virtual {v4, v5}, LX/7HY;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1190732
    iget-object v4, p1, LX/7Hi;->d:LX/7Ha;

    move-object v4, v4

    .line 1190733
    iget-object v5, v3, LX/7Hi;->d:LX/7Ha;

    move-object v5, v5

    .line 1190734
    invoke-virtual {v4, v5}, LX/7Ha;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1190735
    iget-object v4, v3, LX/7Hi;->b:LX/7Hc;

    move-object v4, v4

    .line 1190736
    iget-object v5, p1, LX/7Hi;->b:LX/7Hc;

    move-object v5, v5

    .line 1190737
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190738
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190739
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v6

    .line 1190740
    iget-object v7, v4, LX/7Hc;->b:LX/0Px;

    move-object v7, v7

    .line 1190741
    invoke-virtual {v7}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0cA;->b(Ljava/util/Iterator;)LX/0cA;

    .line 1190742
    iget-object v7, v5, LX/7Hc;->b:LX/0Px;

    move-object v7, v7

    .line 1190743
    invoke-virtual {v7}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0cA;->b(Ljava/util/Iterator;)LX/0cA;

    .line 1190744
    new-instance v7, LX/7Hc;

    invoke-virtual {v6}, LX/0cA;->b()LX/0Rf;

    move-result-object v6

    invoke-virtual {v6}, LX/0Py;->asList()LX/0Px;

    move-result-object v6

    .line 1190745
    iget v8, v4, LX/7Hc;->c:I

    move v8, v8

    .line 1190746
    iget p0, v4, LX/7Hc;->d:I

    move p0, p0

    .line 1190747
    iget v3, v5, LX/7Hc;->d:I

    move v3, v3

    .line 1190748
    const/4 v4, -0x1

    .line 1190749
    if-ne p0, v4, :cond_5

    if-ne v3, v4, :cond_5

    move p0, v4

    .line 1190750
    :cond_4
    :goto_3
    move p0, p0

    .line 1190751
    invoke-direct {v7, v6, v8, p0}, LX/7Hc;-><init>(LX/0Px;II)V

    move-object v5, v7

    .line 1190752
    new-instance v4, LX/7Hi;

    iget-object v6, p1, LX/7Hi;->a:LX/7B6;

    iget-object v7, p1, LX/7Hi;->c:LX/7HY;

    iget-object v8, p1, LX/7Hi;->d:LX/7Ha;

    invoke-direct {v4, v6, v5, v7, v8}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    move-object p1, v4

    goto :goto_2

    .line 1190753
    :cond_5
    if-eqz p0, :cond_6

    if-nez v3, :cond_4

    .line 1190754
    :cond_6
    const/4 p0, 0x0

    goto :goto_3
.end method

.method public static f(LX/7HT;)V
    .locals 1

    .prologue
    .line 1190789
    invoke-virtual {p0}, LX/7HT;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1190790
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    invoke-virtual {p0, v0}, LX/7HT;->a(LX/7HZ;)V

    .line 1190791
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<TT;>;>;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public a(LX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1190786
    iget-object v0, p0, LX/7HT;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1190787
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    invoke-virtual {p0, v0}, LX/7HT;->a(LX/7HZ;)V

    .line 1190788
    return-void
.end method

.method public final a(LX/2Sp;)V
    .locals 0

    .prologue
    .line 1190784
    iput-object p1, p0, LX/7HT;->e:LX/2Sp;

    .line 1190785
    return-void
.end method

.method public final a(LX/2Sq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Sq",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190808
    iput-object p1, p0, LX/7HT;->d:LX/2Sq;

    .line 1190809
    return-void
.end method

.method public abstract a(LX/7B6;Ljava/lang/Throwable;)V
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public a(LX/7B6;Ljava/util/concurrent/CancellationException;)V
    .locals 0
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1190783
    return-void
.end method

.method public final a(LX/7HZ;)V
    .locals 1

    .prologue
    .line 1190778
    iget-object v0, p0, LX/7HT;->f:LX/7HZ;

    if-ne v0, p1, :cond_1

    .line 1190779
    :cond_0
    :goto_0
    return-void

    .line 1190780
    :cond_1
    iput-object p1, p0, LX/7HT;->f:LX/7HZ;

    .line 1190781
    iget-object v0, p0, LX/7HT;->e:LX/2Sp;

    if-eqz v0, :cond_0

    .line 1190782
    iget-object v0, p0, LX/7HT;->e:LX/2Sp;

    invoke-interface {v0, p1}, LX/2Sp;->a(LX/7HZ;)V

    goto :goto_0
.end method

.method public a(LX/7Hi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190763
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 1190764
    iget-object v1, v0, LX/7B6;->b:Ljava/lang/String;

    .line 1190765
    iget-object v0, p0, LX/7HT;->c:LX/7Hn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7HT;->c:LX/7Hn;

    .line 1190766
    iget-object v2, p1, LX/7Hi;->a:LX/7B6;

    move-object v2, v2

    .line 1190767
    iget-object v2, v2, LX/7B6;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/7Hn;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1190768
    :cond_0
    :goto_0
    invoke-static {p0, p1}, LX/7HT;->c(LX/7HT;LX/7Hi;)V

    .line 1190769
    invoke-static {p0}, LX/7HT;->f(LX/7HT;)V

    .line 1190770
    return-void

    .line 1190771
    :cond_1
    invoke-virtual {p0}, LX/7HT;->b()LX/7HY;

    .line 1190772
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 1190773
    iget-object v2, v0, LX/7Hc;->b:LX/0Px;

    move-object v2, v2

    .line 1190774
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1190775
    :cond_2
    iget-object v0, p0, LX/7HT;->c:LX/7Hn;

    .line 1190776
    iget-object v2, p1, LX/7Hi;->a:LX/7B6;

    move-object v2, v2

    .line 1190777
    iget-object v2, v2, LX/7B6;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, p1}, LX/7Hn;->a(Ljava/lang/String;Ljava/lang/String;LX/7Hi;)V

    goto :goto_0
.end method

.method public abstract b()LX/7HY;
.end method

.method public final b(LX/7B6;)V
    .locals 4

    .prologue
    .line 1190757
    invoke-virtual {p0}, LX/7HT;->a()Ljava/lang/String;

    move-result-object v0

    .line 1190758
    invoke-virtual {p0, p1}, LX/7HT;->a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1190759
    new-instance v2, LX/7HR;

    invoke-direct {v2, p0, p1}, LX/7HR;-><init>(LX/7HT;LX/7B6;)V

    .line 1190760
    iget-object v3, p0, LX/7HT;->b:LX/1Ck;

    invoke-virtual {v3, v0, v1, v2}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1190761
    sget-object v0, LX/7HZ;->ACTIVE:LX/7HZ;

    invoke-virtual {p0, v0}, LX/7HT;->a(LX/7HZ;)V

    .line 1190762
    return-void
.end method

.method public final c()LX/7Hn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/7Hn",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1190756
    iget-object v0, p0, LX/7HT;->c:LX/7Hn;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1190755
    iget-object v0, p0, LX/7HT;->b:LX/1Ck;

    invoke-virtual {p0}, LX/7HT;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
