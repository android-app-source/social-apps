.class public final LX/6oz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Z

.field public final synthetic d:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;JLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1148734
    iput-object p1, p0, LX/6oz;->d:LX/6p6;

    iput-wide p2, p0, LX/6oz;->a:J

    iput-object p4, p0, LX/6oz;->b:Ljava/lang/String;

    iput-boolean p5, p0, LX/6oz;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1148735
    iget-object v0, p0, LX/6oz;->d:LX/6p6;

    iget-object v0, v0, LX/6p6;->i:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iget-wide v2, p0, LX/6oz;->a:J

    iget-object v1, p0, LX/6oz;->b:Ljava/lang/String;

    iget-boolean v4, p0, LX/6oz;->c:Z

    .line 1148736
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1148737
    sget-object v6, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->a:Ljava/lang/String;

    new-instance p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;-><init>(JLjava/lang/String;Z)V

    invoke-virtual {v5, v6, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1148738
    const-string v6, "delete_payment_pin"

    invoke-static {v0, v5, v6}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 1148739
    return-object v0
.end method
