.class public LX/7MR;
.super LX/2oy;
.source ""


# instance fields
.field public final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1198769
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7MR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198770
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198767
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7MR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198768
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1198762
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198763
    invoke-virtual {p0}, LX/7MR;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1198764
    const v0, 0x7f0d094e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/7MR;->a:Landroid/view/View;

    .line 1198765
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7MU;

    invoke-direct {v1, p0}, LX/7MU;-><init>(LX/7MR;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1198766
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1198760
    iget-object v0, p0, LX/7MR;->a:Landroid/view/View;

    new-instance v1, LX/7MS;

    invoke-direct {v1, p0}, LX/7MS;-><init>(LX/7MR;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198761
    return-void
.end method


# virtual methods
.method public a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1198771
    if-eqz p2, :cond_0

    .line 1198772
    invoke-direct {p0}, LX/7MR;->h()V

    .line 1198773
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1198756
    if-eqz p1, :cond_0

    .line 1198757
    iget-object v0, p0, LX/7MR;->a:Landroid/view/View;

    new-instance v1, LX/7MT;

    invoke-direct {v1, p0}, LX/7MT;-><init>(LX/7MR;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198758
    :goto_0
    return-void

    .line 1198759
    :cond_0
    invoke-direct {p0}, LX/7MR;->h()V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1198754
    iget-object v0, p0, LX/7MR;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198755
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 1198749
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qa;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-direct {v1, v2}, LX/2qa;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1198750
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 1198752
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qb;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-direct {v1, v2}, LX/2qb;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1198753
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1198751
    const v0, 0x7f03029a

    return v0
.end method
