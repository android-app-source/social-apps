.class public LX/6fz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:J

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1120843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)LX/6fz;
    .locals 1

    .prologue
    .line 1120858
    iput-wide p1, p0, LX/6fz;->b:J

    .line 1120859
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6fz;
    .locals 0

    .prologue
    .line 1120856
    iput-object p1, p0, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1120857
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadParticipant;)LX/6fz;
    .locals 2

    .prologue
    .line 1120849
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1120850
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    iput-wide v0, p0, LX/6fz;->b:J

    .line 1120851
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->c:J

    iput-wide v0, p0, LX/6fz;->c:J

    .line 1120852
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->d:Ljava/lang/String;

    iput-object v0, p0, LX/6fz;->d:Ljava/lang/String;

    .line 1120853
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e:J

    iput-wide v0, p0, LX/6fz;->e:J

    .line 1120854
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f:Z

    iput-boolean v0, p0, LX/6fz;->f:Z

    .line 1120855
    return-object p0
.end method

.method public final a(Z)LX/6fz;
    .locals 0

    .prologue
    .line 1120860
    iput-boolean p1, p0, LX/6fz;->f:Z

    .line 1120861
    return-object p0
.end method

.method public final b(J)LX/6fz;
    .locals 1

    .prologue
    .line 1120847
    iput-wide p1, p0, LX/6fz;->c:J

    .line 1120848
    return-object p0
.end method

.method public final c(J)LX/6fz;
    .locals 1

    .prologue
    .line 1120845
    iput-wide p1, p0, LX/6fz;->e:J

    .line 1120846
    return-object p0
.end method

.method public final g()Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 1

    .prologue
    .line 1120844
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;-><init>(LX/6fz;)V

    return-object v0
.end method
