.class public final LX/7c7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/7cH;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;)LX/7c6;
    .locals 3

    :try_start_0
    new-instance v0, LX/7c6;

    sget-object v1, LX/7c7;->a:LX/7cH;

    const-string v2, "IBitmapDescriptorFactory is not initialized"

    invoke-static {v1, v2}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7cH;

    move-object v1, v1

    invoke-interface {v1, p0}, LX/7cH;->a(Landroid/graphics/Bitmap;)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7c6;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
