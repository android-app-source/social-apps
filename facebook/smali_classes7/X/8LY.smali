.class public final LX/8LY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/photos/upload/operation/UploadRecord;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1333348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1333349
    const/4 v0, 0x0

    .line 1333350
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/upload/operation/UploadRecord;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1333351
    :goto_0
    return-object v0

    .line 1333352
    :catch_0
    move-exception v1

    .line 1333353
    sget-object v2, Lcom/facebook/photos/upload/operation/UploadRecord;->a:Ljava/lang/Class;

    const-string v3, "Unable to deserialize class from parcel"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1333354
    new-array v0, p1, [Lcom/facebook/photos/upload/operation/UploadRecord;

    return-object v0
.end method
