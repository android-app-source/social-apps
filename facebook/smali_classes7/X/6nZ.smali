.class public LX/6nZ;
.super LX/6nY;
.source ""


# static fields
.field public static final a:Landroid/content/ComponentName;


# instance fields
.field private final b:LX/6nP;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1147376
    new-instance v0, Landroid/content/ComponentName;

    sget-object v1, LX/1ZP;->a:Ljava/lang/String;

    const-string v2, "com.facebook.oxygen.appmanager.nekodirect.NekoDirectService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6nZ;->a:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/6nP;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1147377
    const-string v0, "nekodirect"

    sget-object v1, LX/6nZ;->a:Landroid/content/ComponentName;

    invoke-direct {p0, p1, v0, v1, p2}, LX/6nY;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Ljava/util/concurrent/ExecutorService;)V

    .line 1147378
    iput-object p3, p0, LX/6nZ;->b:LX/6nP;

    .line 1147379
    return-void
.end method

.method public static a(LX/0QB;)LX/6nZ;
    .locals 4

    .prologue
    .line 1147380
    new-instance v3, LX/6nZ;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    .line 1147381
    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v2}, LX/6nS;->a(LX/03V;)LX/6nP;

    move-result-object v2

    move-object v2, v2

    .line 1147382
    check-cast v2, LX/6nP;

    invoke-direct {v3, v0, v1, v2}, LX/6nZ;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/6nP;)V

    .line 1147383
    move-object v0, v3

    .line 1147384
    return-object v0
.end method

.method public static a$redex0(LX/6nZ;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1147385
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147386
    const-string v0, "exception"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1147387
    if-nez v0, :cond_1

    .line 1147388
    :cond_0
    return-void

    .line 1147389
    :cond_1
    iget-object v1, p0, LX/6nZ;->b:LX/6nP;

    const/4 v7, 0x1

    .line 1147390
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147391
    const-string v2, "error_code"

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 1147392
    const/4 v4, 0x0

    .line 1147393
    const-string v2, "exception"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 1147394
    if-eqz v2, :cond_3

    .line 1147395
    :try_start_0
    const-string v5, "exception"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1147396
    if-eqz v2, :cond_2

    .line 1147397
    :try_start_1
    sget-object v4, LX/6nM;->Deserialized:LX/6nM;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-object v8, v4

    move-object v4, v2

    move-object v2, v8

    .line 1147398
    :goto_0
    sget-object v5, LX/6nM;->NotIncluded:LX/6nM;

    if-ne v2, v5, :cond_4

    .line 1147399
    const-string v5, "serialization_result"

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 1147400
    if-nez v5, :cond_4

    .line 1147401
    sget-object v2, LX/6nM;->SerializationFailed:LX/6nM;

    move-object v5, v2

    .line 1147402
    :goto_1
    const-string v2, "stringified_exception"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1147403
    const-string v2, "exception_hierarchies"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1147404
    invoke-static {v2}, LX/6nP;->a(Ljava/util/List;)LX/0Px;

    move-result-object v7

    .line 1147405
    new-instance v2, LX/6nN;

    invoke-direct/range {v2 .. v7}, LX/6nN;-><init>(ILjava/lang/Throwable;LX/6nM;Ljava/lang/String;LX/0Px;)V

    move-object v0, v2

    .line 1147406
    if-eqz v0, :cond_0

    .line 1147407
    const/4 v1, 0x0

    .line 1147408
    new-instance v3, LX/6nT;

    invoke-direct {v3, v1, v0}, LX/6nT;-><init>(Ljava/lang/String;LX/6nN;)V

    .line 1147409
    iget-object v2, v0, LX/6nN;->b:Ljava/lang/Throwable;

    if-eqz v2, :cond_5

    .line 1147410
    iget-object v2, v0, LX/6nN;->b:Ljava/lang/Throwable;

    invoke-static {v2, v3}, LX/6nN;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 1147411
    iget-object v2, v0, LX/6nN;->b:Ljava/lang/Throwable;

    .line 1147412
    :goto_2
    move-object v1, v2

    .line 1147413
    move-object v0, v1

    .line 1147414
    if-eqz v0, :cond_0

    .line 1147415
    throw v0

    .line 1147416
    :cond_2
    :try_start_2
    sget-object v4, LX/6nM;->NotIncluded:LX/6nM;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-object v8, v4

    move-object v4, v2

    move-object v2, v8

    .line 1147417
    goto :goto_0

    .line 1147418
    :catch_0
    move-exception v2

    .line 1147419
    :goto_3
    iget-object v5, v1, LX/6nP;->a:LX/6nR;

    sget-object v6, LX/6nO;->c:Ljava/lang/String;

    .line 1147420
    iget-object v8, v5, LX/6nR;->a:LX/03V;

    invoke-virtual {v8, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1147421
    sget-object v2, LX/6nM;->DeserializationFailed:LX/6nM;

    goto :goto_0

    .line 1147422
    :cond_3
    sget-object v2, LX/6nM;->NotIncluded:LX/6nM;

    goto :goto_0

    .line 1147423
    :catch_1
    move-exception v4

    move-object v8, v4

    move-object v4, v2

    move-object v2, v8

    goto :goto_3

    :cond_4
    move-object v5, v2

    goto :goto_1

    .line 1147424
    :cond_5
    iget-object v2, v0, LX/6nN;->d:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 1147425
    new-instance v2, LX/6nU;

    iget-object v4, v0, LX/6nN;->d:Ljava/lang/String;

    invoke-direct {v2, v4, v0}, LX/6nU;-><init>(Ljava/lang/String;LX/6nN;)V

    .line 1147426
    invoke-static {v2, v3}, LX/6nN;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1147427
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Remote error code "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, LX/6nN;->a:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1147428
    new-instance v2, LX/6nU;

    invoke-direct {v2, v4, v0}, LX/6nU;-><init>(Ljava/lang/String;LX/6nN;)V

    .line 1147429
    invoke-static {v2, v3}, LX/6nN;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static b(Landroid/os/IBinder;)LX/6nK;
    .locals 3

    .prologue
    .line 1147430
    if-nez p0, :cond_1

    .line 1147431
    const/4 v0, 0x0

    .line 1147432
    :goto_0
    move-object v0, v0

    .line 1147433
    if-nez v0, :cond_0

    .line 1147434
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot convert binder to interface: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1147435
    :cond_0
    return-object v0

    .line 1147436
    :cond_1
    const-string v0, "com.facebook.oxygen.appmanager.nekodirect.INekoDirect"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 1147437
    if-eqz v0, :cond_2

    instance-of v1, v0, LX/6nK;

    if-eqz v1, :cond_2

    .line 1147438
    check-cast v0, LX/6nK;

    goto :goto_0

    .line 1147439
    :cond_2
    new-instance v0, LX/6nL;

    invoke-direct {v0, p0}, LX/6nL;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1147440
    invoke-super {p0}, LX/6nY;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1147441
    const-string v1, "target_api_version"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1147442
    return-object v0
.end method

.method public final a(Ljava/lang/String;IILjava/util/Set;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1147443
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1147444
    const-string v0, "allowed_networks"

    invoke-virtual {v5, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1147445
    const-string v0, "trace_release_query"

    invoke-virtual {v5, v0, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1147446
    const-string v0, "shown_permissions"

    invoke-static {p4}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1147447
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 1147448
    new-instance v0, LX/6nW;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, LX/6nW;-><init>(LX/6nZ;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 1147449
    invoke-virtual {p0, v2, v0}, LX/6nY;->a(Lcom/google/common/util/concurrent/SettableFuture;Landroid/content/ServiceConnection;)V

    .line 1147450
    new-instance v0, LX/6nb;

    invoke-direct {v0, v2}, LX/6nb;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    return-object v0
.end method
