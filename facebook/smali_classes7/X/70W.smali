.class public LX/70W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/70U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/70U",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162377
    return-void
.end method


# virtual methods
.method public final a()LX/6zQ;
    .locals 1

    .prologue
    .line 1162378
    sget-object v0, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    return-object v0
.end method

.method public final a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
    .locals 3

    .prologue
    .line 1162379
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1162380
    invoke-static {v0}, LX/6zQ;->forValue(Ljava/lang/String;)LX/6zQ;

    move-result-object v0

    sget-object v1, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1162381
    new-instance v0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;

    const-string v1, "title"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 1162382
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
