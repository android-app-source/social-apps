.class public LX/6sX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1153504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/ArrayList;Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1153505
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, LX/6xk;->PAYMENT_TYPE:LX/6xk;

    invoke-virtual {v1}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->c:LX/6xg;

    invoke-virtual {v2}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153506
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, LX/6xk;->ORDER_ID:LX/6xk;

    invoke-virtual {v1}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153507
    iget-object v0, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->i:LX/0m9;

    if-eqz v0, :cond_0

    .line 1153508
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, LX/6xk;->EXTRA_DATA:LX/6xk;

    invoke-virtual {v1}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->i:LX/0m9;

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153509
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, LX/6xk;->MAILING_ADDRESS_ID:LX/6xk;

    invoke-virtual {v1}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->o:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153510
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v1, LX/6xk;->SHIPPING_OPTION_ID:LX/6xk;

    invoke-virtual {v1}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153511
    return-void
.end method
