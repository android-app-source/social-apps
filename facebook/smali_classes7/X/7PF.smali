.class public final LX/7PF;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final synthetic a:LX/3Dg;


# direct methods
.method public constructor <init>(LX/3Dg;)V
    .locals 0

    .prologue
    .line 1202654
    iput-object p1, p0, LX/7PF;->a:LX/3Dg;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 1202663
    iget-object v0, p0, LX/7PF;->a:LX/3Dg;

    invoke-static {v0}, LX/3Dg;->f(LX/3Dg;)V

    .line 1202664
    return-void
.end method

.method public final write(I)V
    .locals 1

    .prologue
    .line 1202662
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final write([BII)V
    .locals 2

    .prologue
    .line 1202655
    :goto_0
    if-lez p3, :cond_0

    .line 1202656
    const v0, 0x8000

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1202657
    iget-object v1, p0, LX/7PF;->a:LX/3Dg;

    invoke-static {v1, p1, p2, v0}, LX/3Dg;->a(LX/3Dg;[BII)V

    .line 1202658
    add-int/2addr p2, v0

    .line 1202659
    sub-int/2addr p3, v0

    .line 1202660
    goto :goto_0

    .line 1202661
    :cond_0
    return-void
.end method
