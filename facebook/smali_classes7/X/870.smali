.class public final enum LX/870;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/870;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/870;

.field public static final enum DOODLE_DRAWING:LX/870;

.field public static final enum DOODLE_EMPTY:LX/870;

.field public static final enum DOODLE_HAS_DRAWING:LX/870;

.field public static final enum NO_FORMAT_IN_PROCESS:LX/870;

.field public static final enum TEXT_DRAGGING:LX/870;

.field public static final enum TEXT_EDITING:LX/870;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1298275
    new-instance v0, LX/870;

    const-string v1, "NO_FORMAT_IN_PROCESS"

    invoke-direct {v0, v1, v3}, LX/870;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    .line 1298276
    new-instance v0, LX/870;

    const-string v1, "DOODLE_EMPTY"

    invoke-direct {v0, v1, v4}, LX/870;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/870;->DOODLE_EMPTY:LX/870;

    .line 1298277
    new-instance v0, LX/870;

    const-string v1, "DOODLE_HAS_DRAWING"

    invoke-direct {v0, v1, v5}, LX/870;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/870;->DOODLE_HAS_DRAWING:LX/870;

    .line 1298278
    new-instance v0, LX/870;

    const-string v1, "DOODLE_DRAWING"

    invoke-direct {v0, v1, v6}, LX/870;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/870;->DOODLE_DRAWING:LX/870;

    .line 1298279
    new-instance v0, LX/870;

    const-string v1, "TEXT_EDITING"

    invoke-direct {v0, v1, v7}, LX/870;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/870;->TEXT_EDITING:LX/870;

    .line 1298280
    new-instance v0, LX/870;

    const-string v1, "TEXT_DRAGGING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/870;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/870;->TEXT_DRAGGING:LX/870;

    .line 1298281
    const/4 v0, 0x6

    new-array v0, v0, [LX/870;

    sget-object v1, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    aput-object v1, v0, v3

    sget-object v1, LX/870;->DOODLE_EMPTY:LX/870;

    aput-object v1, v0, v4

    sget-object v1, LX/870;->DOODLE_HAS_DRAWING:LX/870;

    aput-object v1, v0, v5

    sget-object v1, LX/870;->DOODLE_DRAWING:LX/870;

    aput-object v1, v0, v6

    sget-object v1, LX/870;->TEXT_EDITING:LX/870;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/870;->TEXT_DRAGGING:LX/870;

    aput-object v2, v0, v1

    sput-object v0, LX/870;->$VALUES:[LX/870;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1298274
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/870;
    .locals 1

    .prologue
    .line 1298273
    const-class v0, LX/870;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/870;

    return-object v0
.end method

.method public static values()[LX/870;
    .locals 1

    .prologue
    .line 1298272
    sget-object v0, LX/870;->$VALUES:[LX/870;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/870;

    return-object v0
.end method
