.class public final LX/7VF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/zero/protocol/graphql/ZeroSetOptinStateMutationModels$ZeroSetOptinStateMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/7WC;

.field public final synthetic c:LX/7VG;


# direct methods
.method public constructor <init>(LX/7VG;Ljava/lang/String;LX/7WC;)V
    .locals 0

    .prologue
    .line 1214153
    iput-object p1, p0, LX/7VF;->c:LX/7VG;

    iput-object p2, p0, LX/7VF;->a:Ljava/lang/String;

    iput-object p3, p0, LX/7VF;->b:LX/7WC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1214154
    iget-object v0, p0, LX/7VF;->b:LX/7WC;

    invoke-virtual {v0}, LX/7WC;->a()V

    .line 1214155
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1214156
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1214157
    iget-object v0, p0, LX/7VF;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1214158
    iget-object v0, p0, LX/7VF;->b:LX/7WC;

    invoke-static {p1}, LX/7VG;->b(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7WC;->a(Ljava/lang/String;)V

    .line 1214159
    :goto_0
    return-void

    .line 1214160
    :cond_0
    new-instance v0, LX/7VE;

    invoke-direct {v0, p0, p1}, LX/7VE;-><init>(LX/7VF;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1214161
    iget-object v1, p0, LX/7VF;->c:LX/7VG;

    iget-object v1, v1, LX/7VG;->d:LX/0yP;

    invoke-virtual {v1, v0}, LX/0yP;->a(LX/0yJ;)V

    .line 1214162
    iget-object v0, p0, LX/7VF;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0yi;->valueOf(Ljava/lang/String;)LX/0yi;

    move-result-object v0

    .line 1214163
    iget-object v1, p0, LX/7VF;->c:LX/7VG;

    iget-object v1, v1, LX/7VG;->d:LX/0yP;

    sget-object v2, LX/32P;->OPTIN:LX/32P;

    invoke-virtual {v1, v0, v2}, LX/0yP;->a(LX/0yi;LX/32P;)V

    goto :goto_0
.end method
