.class public LX/7jZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1229643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;I)I
    .locals 6
    .param p0    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "extractFeaturedProductCount"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const v5, 0x51fd3f55

    const/4 v2, 0x0

    .line 1229644
    if-eqz p1, :cond_3

    .line 1229645
    invoke-static {p0, p1, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1229646
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 1229647
    invoke-static {p0, p1, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 1229648
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    :goto_3
    if-eqz v1, :cond_0

    .line 1229649
    invoke-static {p0, p1, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v2

    .line 1229650
    :cond_0
    return v2

    .line 1229651
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 1229652
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 1229653
    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_3

    .line 1229654
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4
.end method

.method public static a(Ljava/lang/String;)LX/4Ia;
    .locals 2

    .prologue
    .line 1229655
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1229656
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1229657
    new-instance v1, LX/4Ia;

    invoke-direct {v1}, LX/4Ia;-><init>()V

    .line 1229658
    const-string p0, "ids"

    invoke-virtual {v1, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1229659
    move-object v0, v1

    .line 1229660
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;Ljava/lang/String;)LX/7ja;
    .locals 10

    .prologue
    .line 1229661
    new-instance v0, LX/7jc;

    invoke-direct {v0}, LX/7jc;-><init>()V

    .line 1229662
    iput-object p0, v0, LX/7jc;->d:Ljava/lang/String;

    .line 1229663
    move-object v0, v0

    .line 1229664
    iput-object p1, v0, LX/7jc;->e:Ljava/lang/String;

    .line 1229665
    move-object v0, v0

    .line 1229666
    iput-object p2, v0, LX/7jc;->c:Ljava/lang/String;

    .line 1229667
    move-object v0, v0

    .line 1229668
    iput-boolean p3, v0, LX/7jc;->a:Z

    .line 1229669
    move-object v0, v0

    .line 1229670
    new-instance v1, LX/7ig;

    invoke-direct {v1}, LX/7ig;-><init>()V

    invoke-virtual {p4}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1229671
    iput-object v2, v1, LX/7ig;->b:Ljava/lang/String;

    .line 1229672
    move-object v1, v1

    .line 1229673
    invoke-virtual {p4}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v2

    .line 1229674
    iput v2, v1, LX/7ig;->a:I

    .line 1229675
    move-object v1, v1

    .line 1229676
    invoke-virtual {v1}, LX/7ig;->a()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    .line 1229677
    iput-object v1, v0, LX/7jc;->h:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    .line 1229678
    move-object v0, v0

    .line 1229679
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    new-instance v2, LX/7jd;

    invoke-direct {v2}, LX/7jd;-><init>()V

    new-instance v3, LX/7je;

    invoke-direct {v3}, LX/7je;-><init>()V

    .line 1229680
    iput-object p5, v3, LX/7je;->a:Ljava/lang/String;

    .line 1229681
    move-object v3, v3

    .line 1229682
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1229683
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1229684
    iget-object v5, v3, LX/7je;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1229685
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1229686
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1229687
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1229688
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1229689
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1229690
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1229691
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1229692
    new-instance v5, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel$ImageModel;

    invoke-direct {v5, v4}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel$ImageModel;-><init>(LX/15i;)V

    .line 1229693
    move-object v3, v5

    .line 1229694
    iput-object v3, v2, LX/7jd;->b:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel$ImageModel;

    .line 1229695
    move-object v2, v2

    .line 1229696
    invoke-virtual {v2}, LX/7jd;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1229697
    iput-object v1, v0, LX/7jc;->f:LX/0Px;

    .line 1229698
    move-object v0, v0

    .line 1229699
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    new-instance v2, LX/7jf;

    invoke-direct {v2}, LX/7jf;-><init>()V

    new-instance v3, LX/7jg;

    invoke-direct {v3}, LX/7jg;-><init>()V

    .line 1229700
    iput-object p5, v3, LX/7jg;->a:Ljava/lang/String;

    .line 1229701
    move-object v3, v3

    .line 1229702
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1229703
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1229704
    iget-object v5, v3, LX/7jg;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1229705
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1229706
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1229707
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1229708
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1229709
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1229710
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1229711
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1229712
    new-instance v5, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;

    invoke-direct {v5, v4}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;-><init>(LX/15i;)V

    .line 1229713
    move-object v3, v5

    .line 1229714
    iput-object v3, v2, LX/7jf;->b:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel$ImageModel;

    .line 1229715
    move-object v2, v2

    .line 1229716
    invoke-virtual {v2}, LX/7jf;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$ProductImagesLargeModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1229717
    iput-object v1, v0, LX/7jc;->g:LX/0Px;

    .line 1229718
    move-object v0, v0

    .line 1229719
    invoke-virtual {v0}, LX/7jc;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1229720
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v3, v0

    .line 1229721
    if-nez v3, :cond_1

    .line 1229722
    :cond_0
    return-void

    .line 1229723
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1229724
    const-string v5, "pending_media_item_upload"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1229725
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1229726
    goto :goto_1
.end method
