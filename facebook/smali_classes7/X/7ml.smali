.class public LX/7ml;
.super LX/7mi;
.source ""


# instance fields
.field public a:Lcom/facebook/composer/publish/common/PendingStory;

.field public final b:LX/7mm;

.field public final c:LX/7mk;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V
    .locals 1

    .prologue
    .line 1237432
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-direct {p0, v0}, LX/7mi;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1237433
    iput-object p1, p0, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    .line 1237434
    iput-object p2, p0, LX/7ml;->b:LX/7mm;

    .line 1237435
    iget-object v0, p0, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    .line 1237436
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    .line 1237437
    if-nez p1, :cond_0

    .line 1237438
    sget-object p1, LX/7mk;->OTHER:LX/7mk;

    .line 1237439
    :goto_0
    move-object v0, p1

    .line 1237440
    iput-object v0, p0, LX/7ml;->c:LX/7mk;

    .line 1237441
    return-void

    .line 1237442
    :cond_0
    invoke-static {p1}, LX/17E;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 1237443
    sget-object p1, LX/7mk;->MMP:LX/7mk;

    goto :goto_0

    .line 1237444
    :cond_1
    invoke-static {p1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 1237445
    sget-object p1, LX/7mk;->VIDEO:LX/7mk;

    goto :goto_0

    .line 1237446
    :cond_2
    invoke-static {p1}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1237447
    sget-object p1, LX/7mk;->PHOTO:LX/7mk;

    goto :goto_0

    .line 1237448
    :cond_3
    sget-object p1, LX/7mk;->TEXT:LX/7mk;

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 1237449
    iget-object v0, p0, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    .line 1237450
    iget-object v1, v0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    move-object v0, v1

    .line 1237451
    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1237452
    iget-object v0, p0, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    .line 1237453
    iget-object p0, v0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    move-object v0, p0

    .line 1237454
    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
