.class public final enum LX/7m7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7m7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7m7;

.field public static final enum CANCELLED:LX/7m7;

.field public static final enum EXCEPTION:LX/7m7;

.field public static final enum SUCCESS:LX/7m7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1236257
    new-instance v0, LX/7m7;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/7m7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7m7;->SUCCESS:LX/7m7;

    .line 1236258
    new-instance v0, LX/7m7;

    const-string v1, "EXCEPTION"

    invoke-direct {v0, v1, v3}, LX/7m7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7m7;->EXCEPTION:LX/7m7;

    .line 1236259
    new-instance v0, LX/7m7;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4}, LX/7m7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7m7;->CANCELLED:LX/7m7;

    .line 1236260
    const/4 v0, 0x3

    new-array v0, v0, [LX/7m7;

    sget-object v1, LX/7m7;->SUCCESS:LX/7m7;

    aput-object v1, v0, v2

    sget-object v1, LX/7m7;->EXCEPTION:LX/7m7;

    aput-object v1, v0, v3

    sget-object v1, LX/7m7;->CANCELLED:LX/7m7;

    aput-object v1, v0, v4

    sput-object v0, LX/7m7;->$VALUES:[LX/7m7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1236261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7m7;
    .locals 1

    .prologue
    .line 1236262
    const-class v0, LX/7m7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7m7;

    return-object v0
.end method

.method public static values()[LX/7m7;
    .locals 1

    .prologue
    .line 1236263
    sget-object v0, LX/7m7;->$VALUES:[LX/7m7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7m7;

    return-object v0
.end method
