.class public final LX/6r8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6qd",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6xb;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/6qc;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1151712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1151713
    return-void
.end method

.method private static a(LX/0Px;Ljava/lang/Class;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1151687
    invoke-static {p0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    invoke-static {p1}, LX/0Rj;->instanceOf(Ljava/lang/Class;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wv;->a(LX/0Rl;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1151688
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->r()LX/6tr;

    move-result-object v0

    sget-object v3, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 1151689
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v7, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    .line 1151690
    sget-object v3, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-virtual {v7, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h()LX/0am;

    move-result-object v3

    invoke-static {v3}, LX/47j;->a(LX/0am;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v1

    .line 1151691
    :goto_1
    sget-object v4, LX/6rp;->SHIPPING_OPTION:LX/6rp;

    invoke-virtual {v7, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j()LX/0am;

    move-result-object v4

    invoke-static {v4}, LX/47j;->a(LX/0am;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v1

    .line 1151692
    :goto_2
    sget-object v5, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v7, v5}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->s()LX/0am;

    move-result-object v5

    invoke-static {v5}, LX/47j;->a(LX/0am;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v1

    .line 1151693
    :goto_3
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1151694
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v6

    invoke-virtual {v6}, LX/0P1;->values()LX/0Py;

    move-result-object v6

    invoke-virtual {v6}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Px;

    .line 1151695
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    move v6, v8

    .line 1151696
    :goto_4
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v10

    iget-object v10, v10, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v11, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {v10, v11}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    if-nez v6, :cond_8

    :goto_5
    move v8, v9

    .line 1151697
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v6

    iget-object v9, v6, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1151698
    sget-object v6, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v7, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v6, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v9, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->l()LX/0am;

    move-result-object v6

    invoke-static {v6}, LX/47j;->a(LX/0am;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v1

    .line 1151699
    :goto_6
    sget-object v10, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v7, v10}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    sget-object v7, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-virtual {v9, v7}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->m()LX/0am;

    move-result-object v7

    invoke-static {v7}, LX/47j;->a(LX/0am;)Z

    move-result v7

    if-eqz v7, :cond_6

    move v7, v1

    .line 1151700
    :goto_7
    if-eqz v0, :cond_7

    if-nez v3, :cond_7

    if-nez v4, :cond_7

    if-nez v5, :cond_7

    if-nez v8, :cond_7

    if-nez v6, :cond_7

    if-nez v7, :cond_7

    :goto_8
    return v1

    :cond_1
    move v0, v2

    .line 1151701
    goto/16 :goto_0

    :cond_2
    move v3, v2

    .line 1151702
    goto/16 :goto_1

    :cond_3
    move v4, v2

    .line 1151703
    goto/16 :goto_2

    :cond_4
    move v5, v2

    .line 1151704
    goto/16 :goto_3

    :cond_5
    move v6, v2

    .line 1151705
    goto :goto_6

    :cond_6
    move v7, v2

    .line 1151706
    goto :goto_7

    :cond_7
    move v1, v2

    .line 1151707
    goto :goto_8

    :cond_8
    move v9, v8

    goto :goto_5

    :cond_9
    move v6, v9

    goto :goto_4
.end method

.method public static b(LX/0QB;)LX/6r8;
    .locals 2

    .prologue
    .line 1151708
    new-instance v0, LX/6r8;

    invoke-direct {v0}, LX/6r8;-><init>()V

    .line 1151709
    const/16 v1, 0x2d26

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 1151710
    iput-object v1, v0, LX/6r8;->a:LX/0Or;

    .line 1151711
    return-object v0
.end method


# virtual methods
.method public final a(LX/6qc;)V
    .locals 0

    .prologue
    .line 1151669
    iput-object p1, p0, LX/6r8;->b:LX/6qc;

    .line 1151670
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1151714
    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;I)V
    .locals 0

    .prologue
    .line 1151715
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;I)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .locals 0

    .prologue
    .line 1151716
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Rf;)V
    .locals 0

    .prologue
    .line 1151717
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Rf;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6tr;)V
    .locals 0

    .prologue
    .line 1151718
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/6tr;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/73T;)V
    .locals 0

    .prologue
    .line 1151719
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/73T;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1151720
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Landroid/os/Parcelable;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 0

    .prologue
    .line 1151721
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V
    .locals 0

    .prologue
    .line 1151723
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .locals 0

    .prologue
    .line 1151722
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V
    .locals 0

    .prologue
    .line 1151738
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 0

    .prologue
    .line 1151737
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .locals 0

    .prologue
    .line 1151736
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/MailingAddress;)V
    .locals 0

    .prologue
    .line 1151735
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/shipping/model/MailingAddress;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/ShippingOption;)V
    .locals 0

    .prologue
    .line 1151734
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/shipping/model/ShippingOption;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 0

    .prologue
    .line 1151733
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2, p3}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1151732
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V
    .locals 0

    .prologue
    .line 1151731
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2, p3}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;LX/0Px;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1151730
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/util/List;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;Z)V
    .locals 0

    .prologue
    .line 1151729
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Z)V

    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;I)V
    .locals 2

    .prologue
    .line 1151724
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151725
    iput p2, v0, LX/6sR;->u:I

    .line 1151726
    move-object v0, v0

    .line 1151727
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151728
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151671
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151672
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151673
    iput-object p2, v0, LX/6sR;->i:LX/0Px;

    .line 1151674
    move-object v0, v0

    .line 1151675
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1151676
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1151677
    iput-object v1, v0, LX/6sR;->h:LX/0am;

    .line 1151678
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j()LX/0am;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1151679
    const/4 v1, 0x0

    invoke-static {p2, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1151680
    iput-object v1, v0, LX/6sR;->h:LX/0am;

    .line 1151681
    :cond_1
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151682
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Rf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151683
    invoke-virtual {p2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6rp;

    .line 1151684
    invoke-virtual {p1, v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a(LX/6rp;)Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object p1

    goto :goto_0

    .line 1151685
    :cond_0
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-interface {v0, p1}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151686
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/6tr;)V
    .locals 2

    .prologue
    .line 1151578
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151579
    iput-object p2, v0, LX/6sR;->p:LX/6tr;

    .line 1151580
    move-object v0, v0

    .line 1151581
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151582
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/73T;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1151567
    const-string v0, "extra_mutation"

    invoke-virtual {p2, v0, v5}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1151568
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1151569
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-interface {v0, p1}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151570
    :goto_1
    return-void

    .line 1151571
    :sswitch_0
    const-string v4, "mutation_selected_price"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "mutation_pay_button"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    .line 1151572
    :pswitch_0
    const-string v0, "selected_price_index"

    .line 1151573
    iget-object v2, p2, LX/73T;->b:Landroid/os/Bundle;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    move v0, v2

    .line 1151574
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v5}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V

    goto :goto_1

    .line 1151575
    :pswitch_1
    iget-object v0, p0, LX/6r8;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xb;

    .line 1151576
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->c:LX/6xZ;

    invoke-virtual {v0, v1, v3, v4, v5}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V

    .line 1151577
    invoke-virtual {p0, p1, v2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Z)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1ed37afb -> :sswitch_0
        0x39ac95ff -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1151560
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151561
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151562
    iput-object p2, v0, LX/6sR;->n:Landroid/os/Parcelable;

    .line 1151563
    move-object v0, v0

    .line 1151564
    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    .line 1151565
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151566
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 2

    .prologue
    .line 1151555
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/facebook/payments/checkout/CheckoutParams;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    .line 1151556
    iput-object v1, v0, LX/6sR;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1151557
    move-object v0, v0

    .line 1151558
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151559
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V
    .locals 2

    .prologue
    .line 1151550
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151551
    iput-object p2, v0, LX/6sR;->v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    .line 1151552
    move-object v0, v0

    .line 1151553
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151554
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .locals 2

    .prologue
    .line 1151544
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151545
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151546
    iput-object p2, v0, LX/6sR;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1151547
    move-object v0, v0

    .line 1151548
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151549
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V
    .locals 2

    .prologue
    .line 1151539
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151540
    iput-object p2, v0, LX/6sR;->c:Lcom/facebook/payments/model/PaymentsPin;

    .line 1151541
    move-object v0, v0

    .line 1151542
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151543
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 2

    .prologue
    .line 1151533
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151534
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1151535
    iput-object v1, v0, LX/6sR;->q:LX/0am;

    .line 1151536
    move-object v0, v0

    .line 1151537
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151538
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .locals 2

    .prologue
    .line 1151519
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151520
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151521
    iput-object p2, v0, LX/6sR;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1151522
    move-object v0, v0

    .line 1151523
    iget-object v1, p2, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v1, v1

    .line 1151524
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1151525
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1151526
    iput-object v1, v0, LX/6sR;->q:LX/0am;

    .line 1151527
    :cond_0
    :goto_0
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151528
    return-void

    .line 1151529
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->s()LX/0am;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1151530
    invoke-virtual {p2}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->g()Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    move-result-object v1

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1151531
    iput-object v1, v0, LX/6sR;->q:LX/0am;

    .line 1151532
    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/shipping/model/MailingAddress;)V
    .locals 5

    .prologue
    .line 1151504
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151505
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->i()LX/0Px;

    move-result-object v0

    .line 1151506
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1151507
    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1151508
    :goto_1
    move-object v0, v0

    .line 1151509
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6sR;->a(Ljava/util/List;)LX/6sR;

    move-result-object v0

    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1151510
    iput-object v1, v0, LX/6sR;->f:LX/0am;

    .line 1151511
    move-object v0, v0

    .line 1151512
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151513
    return-void

    .line 1151514
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1151515
    :cond_1
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1151516
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1151517
    invoke-virtual {v1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151518
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/shipping/model/ShippingOption;)V
    .locals 2

    .prologue
    .line 1151498
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151499
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1151500
    iput-object v1, v0, LX/6sR;->h:LX/0am;

    .line 1151501
    move-object v0, v0

    .line 1151502
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151503
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 2

    .prologue
    .line 1151491
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151492
    iput-object p2, v0, LX/6sR;->x:Ljava/lang/Integer;

    .line 1151493
    move-object v0, v0

    .line 1151494
    iput-object p3, v0, LX/6sR;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1151495
    move-object v0, v0

    .line 1151496
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151497
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1151486
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151487
    iput-object p2, v0, LX/6sR;->e:Ljava/lang/String;

    .line 1151488
    move-object v0, v0

    .line 1151489
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151490
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151479
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->u()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1151480
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151481
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v1

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 1151482
    iput-object v0, v1, LX/6sR;->s:LX/0P1;

    .line 1151483
    move-object v0, v1

    .line 1151484
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151485
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151583
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151584
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->n()LX/0Px;

    move-result-object v0

    .line 1151585
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1151586
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1151587
    new-instance v1, LX/6r6;

    invoke-direct {v1}, LX/6r6;-><init>()V

    .line 1151588
    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    move-object v3, v1

    .line 1151589
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1151590
    invoke-interface {v1}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1151591
    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1151592
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1151593
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v1

    .line 1151594
    iput-object v0, v1, LX/6sR;->l:LX/0Px;

    .line 1151595
    move-object v1, v1

    .line 1151596
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1151597
    sget-object v3, LX/6r7;->a:[I

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v4

    invoke-virtual {v4}, LX/6vb;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_1

    .line 1151598
    :pswitch_0
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1151599
    iput-object v0, v1, LX/6sR;->j:LX/0am;

    .line 1151600
    goto :goto_1

    .line 1151601
    :pswitch_1
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1151602
    iput-object v0, v1, LX/6sR;->k:LX/0am;

    .line 1151603
    goto :goto_1

    .line 1151604
    :cond_2
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v1}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v1

    invoke-interface {v0, v1}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151605
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Z)V
    .locals 2

    .prologue
    .line 1151606
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151607
    iput-boolean p2, v0, LX/6sR;->b:Z

    .line 1151608
    move-object v0, v0

    .line 1151609
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151610
    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1151611
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-static {p1}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1151612
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1}, LX/6r8;->b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;)V

    return-void
.end method

.method public final bridge synthetic b(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .locals 0

    .prologue
    .line 1151613
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V

    return-void
.end method

.method public final bridge synthetic b(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1151614
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;)V
    .locals 2

    .prologue
    .line 1151615
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151616
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 1151617
    iput-object v1, v0, LX/6sR;->s:LX/0P1;

    .line 1151618
    move-object v0, v0

    .line 1151619
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151620
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1151621
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151622
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/6sR;->a(Ljava/util/List;)LX/6sR;

    move-result-object v3

    .line 1151623
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151624
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1151625
    iput-object v0, v3, LX/6sR;->f:LX/0am;

    .line 1151626
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h()LX/0am;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1151627
    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1151628
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1151629
    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->j()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1151630
    :goto_1
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1151631
    iput-object v0, v3, LX/6sR;->f:LX/0am;

    .line 1151632
    :cond_1
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v3}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v1

    invoke-interface {v0, v1}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151633
    return-void

    .line 1151634
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1151635
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151636
    iput-object p2, v0, LX/6sR;->d:Ljava/lang/String;

    .line 1151637
    move-object v0, v0

    .line 1151638
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151639
    return-void
.end method

.method public final bridge synthetic c(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .locals 0

    .prologue
    .line 1151640
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->c(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V

    return-void
.end method

.method public final bridge synthetic c(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1151641
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p0, p1, p2}, LX/6r8;->c(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V

    return-void
.end method

.method public final c(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1151642
    iget-object v0, p0, LX/6r8;->b:LX/6qc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151643
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151644
    iput-object p2, v0, LX/6sR;->l:LX/0Px;

    .line 1151645
    move-object v0, v0

    .line 1151646
    const-class v1, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;

    invoke-static {p2, v1}, LX/6r8;->a(LX/0Px;Ljava/lang/Class;)LX/0Px;

    move-result-object v1

    .line 1151647
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1151648
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1151649
    iput-object v1, v0, LX/6sR;->j:LX/0am;

    .line 1151650
    :cond_0
    :goto_0
    const-class v1, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;

    invoke-static {p2, v1}, LX/6r8;->a(LX/0Px;Ljava/lang/Class;)LX/0Px;

    move-result-object v1

    .line 1151651
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1151652
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1151653
    iput-object v1, v0, LX/6sR;->k:LX/0am;

    .line 1151654
    :cond_1
    :goto_1
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151655
    return-void

    .line 1151656
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->l()LX/0am;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1151657
    invoke-static {v1, v3}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1151658
    iput-object v1, v0, LX/6sR;->j:LX/0am;

    .line 1151659
    goto :goto_0

    .line 1151660
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->m()LX/0am;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1151661
    invoke-static {v1, v3}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1151662
    iput-object v1, v0, LX/6sR;->k:LX/0am;

    .line 1151663
    goto :goto_1
.end method

.method public final c(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1151664
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1151665
    iput-object p2, v0, LX/6sR;->t:Ljava/lang/String;

    .line 1151666
    move-object v0, v0

    .line 1151667
    iget-object v1, p0, LX/6r8;->b:LX/6qc;

    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    invoke-interface {v1, v0}, LX/6qc;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151668
    return-void
.end method
