.class public LX/7kd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/7kb;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/7kb;)V
    .locals 0

    .prologue
    .line 1232309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232310
    iput-object p1, p0, LX/7kd;->a:Landroid/content/res/Resources;

    .line 1232311
    iput-object p2, p0, LX/7kd;->b:LX/7kb;

    .line 1232312
    return-void
.end method

.method private a(D)F
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1232313
    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    .line 1232314
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    float-to-double v4, v0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_0

    .line 1232315
    const/high16 v1, 0x3e800000    # 0.25f

    add-float/2addr v0, v1

    .line 1232316
    :cond_0
    iget-object v1, p0, LX/7kd;->b:LX/7kb;

    .line 1232317
    iget v2, v1, LX/7kb;->c:F

    move v1, v2

    .line 1232318
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(IIZ)LX/7kc;
    .locals 22

    .prologue
    .line 1232319
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v5}, LX/7kb;->e()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v7

    .line 1232320
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v5}, LX/7kb;->g()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v8

    .line 1232321
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v5}, LX/7kb;->f()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v11

    .line 1232322
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v5}, LX/7kb;->h()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v12

    .line 1232323
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v5}, LX/7kb;->d()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v15

    .line 1232324
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v5}, LX/7kb;->i()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v16

    .line 1232325
    sub-int v9, p2, v16

    .line 1232326
    div-int v5, p1, v15

    .line 1232327
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v4}, LX/7kb;->a()I

    move-result v4

    if-le v5, v4, :cond_0

    .line 1232328
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v4}, LX/7kb;->a()I

    move-result v4

    add-int/lit8 v5, v5, -0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1232329
    :cond_0
    div-int v4, v9, v15

    .line 1232330
    if-lez v4, :cond_2

    .line 1232331
    :goto_0
    int-to-float v4, v4

    .line 1232332
    if-nez p3, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v6}, LX/7kb;->b()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v6, v4, v6

    if-lez v6, :cond_1

    .line 1232333
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7kd;->b:LX/7kb;

    invoke-virtual {v4}, LX/7kb;->b()I

    move-result v4

    int-to-float v4, v4

    .line 1232334
    :cond_1
    float-to-double v0, v4

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, LX/7kd;->a(D)F

    move-result v6

    .line 1232335
    div-int v13, p1, v5

    .line 1232336
    if-le v13, v15, :cond_4

    .line 1232337
    sub-int v4, v13, v15

    .line 1232338
    div-int/lit8 v10, v4, 0x2

    rem-int/lit8 v14, v4, 0x2

    add-int/2addr v10, v14

    add-int/2addr v7, v10

    .line 1232339
    div-int/lit8 v4, v4, 0x2

    add-int/2addr v8, v4

    .line 1232340
    :goto_1
    int-to-float v4, v9

    div-float/2addr v4, v6

    float-to-int v14, v4

    .line 1232341
    if-le v14, v15, :cond_3

    .line 1232342
    sub-int v4, v14, v15

    .line 1232343
    div-int/lit8 v9, v4, 0x2

    rem-int/lit8 v10, v4, 0x2

    add-int/2addr v9, v10

    add-int/2addr v9, v11

    .line 1232344
    div-int/lit8 v4, v4, 0x2

    add-int v10, v12, v4

    .line 1232345
    :goto_2
    int-to-float v4, v13

    mul-float/2addr v4, v6

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    add-float v4, v4, v16

    float-to-int v0, v4

    move/from16 v16, v0

    .line 1232346
    new-instance v4, LX/7kc;

    invoke-direct/range {v4 .. v16}, LX/7kc;-><init>(IFIIIIIIIIII)V

    return-object v4

    .line 1232347
    :cond_2
    int-to-double v0, v9

    move-wide/from16 v18, v0

    int-to-double v0, v15

    move-wide/from16 v20, v0

    div-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v4, v0

    goto :goto_0

    :cond_3
    move v10, v12

    move v9, v11

    move v14, v15

    goto :goto_2

    :cond_4
    move v13, v15

    goto :goto_1
.end method
