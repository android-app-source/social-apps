.class public final enum LX/7yI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7yI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7yI;

.field public static final enum FRONTAL_MODEL:LX/7yI;

.field public static final enum FRONTAL_TILT_LEFT:LX/7yI;

.field public static final enum FRONTAL_TILT_RIGHT:LX/7yI;

.field public static final enum LEFT_FULL_PROFILE:LX/7yI;

.field public static final enum LEFT_HALF_PROFILE:LX/7yI;

.field public static final enum RIGHT_FULL_PROFILE:LX/7yI;

.field public static final enum RIGHT_HALF_PROFILE:LX/7yI;


# instance fields
.field private mClassId:I

.field private mRefClassId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x7

    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 1278843
    new-instance v0, LX/7yI;

    const-string v1, "LEFT_FULL_PROFILE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v8, v4, v2}, LX/7yI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7yI;->LEFT_FULL_PROFILE:LX/7yI;

    .line 1278844
    new-instance v0, LX/7yI;

    const-string v1, "LEFT_HALF_PROFILE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v4, v5, v2}, LX/7yI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7yI;->LEFT_HALF_PROFILE:LX/7yI;

    .line 1278845
    new-instance v0, LX/7yI;

    const-string v1, "FRONTAL_TILT_LEFT"

    const/4 v2, 0x2

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v6, v3}, LX/7yI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7yI;->FRONTAL_TILT_LEFT:LX/7yI;

    .line 1278846
    new-instance v0, LX/7yI;

    const-string v1, "FRONTAL_MODEL"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7, v7}, LX/7yI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7yI;->FRONTAL_MODEL:LX/7yI;

    .line 1278847
    new-instance v0, LX/7yI;

    const-string v1, "FRONTAL_TILT_RIGHT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v5, v2, v6}, LX/7yI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7yI;->FRONTAL_TILT_RIGHT:LX/7yI;

    .line 1278848
    new-instance v0, LX/7yI;

    const-string v1, "RIGHT_HALF_PROFILE"

    const/4 v2, 0x5

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3, v5}, LX/7yI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7yI;->RIGHT_HALF_PROFILE:LX/7yI;

    .line 1278849
    new-instance v0, LX/7yI;

    const-string v1, "RIGHT_FULL_PROFILE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v6, v2, v4}, LX/7yI;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7yI;->RIGHT_FULL_PROFILE:LX/7yI;

    .line 1278850
    new-array v0, v7, [LX/7yI;

    sget-object v1, LX/7yI;->LEFT_FULL_PROFILE:LX/7yI;

    aput-object v1, v0, v8

    sget-object v1, LX/7yI;->LEFT_HALF_PROFILE:LX/7yI;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    sget-object v2, LX/7yI;->FRONTAL_TILT_LEFT:LX/7yI;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/7yI;->FRONTAL_MODEL:LX/7yI;

    aput-object v2, v0, v1

    sget-object v1, LX/7yI;->FRONTAL_TILT_RIGHT:LX/7yI;

    aput-object v1, v0, v5

    const/4 v1, 0x5

    sget-object v2, LX/7yI;->RIGHT_HALF_PROFILE:LX/7yI;

    aput-object v2, v0, v1

    sget-object v1, LX/7yI;->RIGHT_FULL_PROFILE:LX/7yI;

    aput-object v1, v0, v6

    sput-object v0, LX/7yI;->$VALUES:[LX/7yI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1278851
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1278852
    iput p3, p0, LX/7yI;->mClassId:I

    .line 1278853
    iput p4, p0, LX/7yI;->mRefClassId:I

    .line 1278854
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7yI;
    .locals 1

    .prologue
    .line 1278855
    const-class v0, LX/7yI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7yI;

    return-object v0
.end method

.method public static values()[LX/7yI;
    .locals 1

    .prologue
    .line 1278856
    sget-object v0, LX/7yI;->$VALUES:[LX/7yI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7yI;

    return-object v0
.end method


# virtual methods
.method public final id()I
    .locals 1

    .prologue
    .line 1278857
    iget v0, p0, LX/7yI;->mClassId:I

    return v0
.end method

.method public final reflectedId()I
    .locals 1

    .prologue
    .line 1278858
    iget v0, p0, LX/7yI;->mRefClassId:I

    return v0
.end method
