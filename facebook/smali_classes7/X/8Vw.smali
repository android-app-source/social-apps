.class public final LX/8Vw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Td;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;)V
    .locals 0

    .prologue
    .line 1353551
    iput-object p1, p0, LX/8Vw;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1353552
    iget-object v0, p0, LX/8Vw;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    .line 1353553
    if-nez p1, :cond_2

    const/4 v1, 0x0

    :goto_0
    iput-object v1, v0, LX/8Vt;->c:LX/0Px;

    .line 1353554
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1353555
    iget-object v0, p0, LX/8Vw;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    .line 1353556
    if-nez p2, :cond_3

    const/4 v1, 0x0

    :goto_1
    iput-object v1, v0, LX/8Vt;->d:LX/0Px;

    .line 1353557
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1353558
    iget-object v0, p0, LX/8Vw;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    iget-boolean v0, v0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->f:Z

    if-eqz v0, :cond_0

    .line 1353559
    iget-object v0, p0, LX/8Vw;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    invoke-virtual {v0}, LX/8Vt;->d()V

    .line 1353560
    :cond_0
    iget-object v0, p0, LX/8Vw;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->e:LX/8VT;

    if-eqz v0, :cond_1

    .line 1353561
    :cond_1
    return-void

    .line 1353562
    :cond_2
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 1353563
    :cond_3
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_1
.end method
