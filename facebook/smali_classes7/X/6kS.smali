.class public LX/6kS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final graphQLResult:Ljava/lang/String;

.field public final threadKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1136822
    new-instance v0, LX/1sv;

    const-string v1, "DeltaThreadSnapshot"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kS;->b:LX/1sv;

    .line 1136823
    new-instance v0, LX/1sw;

    const-string v1, "threadKeys"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kS;->c:LX/1sw;

    .line 1136824
    new-instance v0, LX/1sw;

    const-string v1, "graphQLResult"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kS;->d:LX/1sw;

    .line 1136825
    sput-boolean v4, LX/6kS;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1136826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136827
    iput-object p1, p0, LX/6kS;->threadKeys:Ljava/util/List;

    .line 1136828
    iput-object p2, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    .line 1136829
    return-void
.end method

.method public static a(LX/6kS;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1136830
    iget-object v0, p0, LX/6kS;->threadKeys:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1136831
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKeys\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kS;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136832
    :cond_0
    iget-object v0, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1136833
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'graphQLResult\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kS;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136834
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136835
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1136836
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1136837
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1136838
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaThreadSnapshot"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136839
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136840
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136841
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136842
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136843
    const-string v4, "threadKeys"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136844
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136845
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136846
    iget-object v4, p0, LX/6kS;->threadKeys:Ljava/util/List;

    if-nez v4, :cond_3

    .line 1136847
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136848
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136849
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136850
    const-string v4, "graphQLResult"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136851
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136852
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136853
    iget-object v0, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1136854
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136855
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136856
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136857
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136858
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1136859
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1136860
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1136861
    :cond_3
    iget-object v4, p0, LX/6kS;->threadKeys:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1136862
    :cond_4
    iget-object v0, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1136863
    invoke-static {p0}, LX/6kS;->a(LX/6kS;)V

    .line 1136864
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136865
    iget-object v0, p0, LX/6kS;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1136866
    sget-object v0, LX/6kS;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136867
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6kS;->threadKeys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1136868
    iget-object v0, p0, LX/6kS;->threadKeys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6l9;

    .line 1136869
    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    goto :goto_0

    .line 1136870
    :cond_0
    iget-object v0, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1136871
    sget-object v0, LX/6kS;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136872
    iget-object v0, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1136873
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136874
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136875
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136876
    if-nez p1, :cond_1

    .line 1136877
    :cond_0
    :goto_0
    return v0

    .line 1136878
    :cond_1
    instance-of v1, p1, LX/6kS;

    if-eqz v1, :cond_0

    .line 1136879
    check-cast p1, LX/6kS;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136880
    if-nez p1, :cond_3

    .line 1136881
    :cond_2
    :goto_1
    move v0, v2

    .line 1136882
    goto :goto_0

    .line 1136883
    :cond_3
    iget-object v0, p0, LX/6kS;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1136884
    :goto_2
    iget-object v3, p1, LX/6kS;->threadKeys:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1136885
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136886
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136887
    iget-object v0, p0, LX/6kS;->threadKeys:Ljava/util/List;

    iget-object v3, p1, LX/6kS;->threadKeys:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136888
    :cond_5
    iget-object v0, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1136889
    :goto_4
    iget-object v3, p1, LX/6kS;->graphQLResult:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1136890
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136891
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136892
    iget-object v0, p0, LX/6kS;->graphQLResult:Ljava/lang/String;

    iget-object v3, p1, LX/6kS;->graphQLResult:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1136893
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1136894
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1136895
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1136896
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1136897
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136898
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136899
    sget-boolean v0, LX/6kS;->a:Z

    .line 1136900
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kS;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136901
    return-object v0
.end method
