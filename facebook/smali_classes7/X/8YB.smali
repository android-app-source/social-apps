.class public final enum LX/8YB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8YB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8YB;

.field public static final enum DEFAULT:LX/8YB;

.field public static final enum GROUPS:LX/8YB;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1355938
    new-instance v0, LX/8YB;

    const-string v1, "GROUPS"

    invoke-direct {v0, v1, v2}, LX/8YB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8YB;->GROUPS:LX/8YB;

    .line 1355939
    new-instance v0, LX/8YB;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, LX/8YB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8YB;->DEFAULT:LX/8YB;

    .line 1355940
    const/4 v0, 0x2

    new-array v0, v0, [LX/8YB;

    sget-object v1, LX/8YB;->GROUPS:LX/8YB;

    aput-object v1, v0, v2

    sget-object v1, LX/8YB;->DEFAULT:LX/8YB;

    aput-object v1, v0, v3

    sput-object v0, LX/8YB;->$VALUES:[LX/8YB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1355943
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8YB;
    .locals 1

    .prologue
    .line 1355942
    const-class v0, LX/8YB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8YB;

    return-object v0
.end method

.method public static values()[LX/8YB;
    .locals 1

    .prologue
    .line 1355941
    sget-object v0, LX/8YB;->$VALUES:[LX/8YB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8YB;

    return-object v0
.end method
