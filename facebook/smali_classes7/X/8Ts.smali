.class public final LX/8Ts;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Sh;

.field public final synthetic b:LX/8Tt;


# direct methods
.method public constructor <init>(LX/8Tt;LX/8Sh;)V
    .locals 0

    .prologue
    .line 1349228
    iput-object p1, p0, LX/8Ts;->b:LX/8Tt;

    iput-object p2, p0, LX/8Ts;->a:LX/8Sh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1349226
    iget-object v0, p0, LX/8Ts;->a:LX/8Sh;

    invoke-virtual {v0, p1}, LX/8Sh;->a(Ljava/lang/Throwable;)V

    .line 1349227
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1349221
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1349222
    if-nez p1, :cond_0

    .line 1349223
    iget-object v0, p0, LX/8Ts;->a:LX/8Sh;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Player state mutation was unable to complete"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/8Sh;->a(Ljava/lang/Throwable;)V

    .line 1349224
    :goto_0
    return-void

    .line 1349225
    :cond_0
    iget-object v0, p0, LX/8Ts;->a:LX/8Sh;

    invoke-virtual {v0}, LX/8Sh;->a()V

    goto :goto_0
.end method
