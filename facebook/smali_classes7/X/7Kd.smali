.class public interface abstract LX/7Kd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Kc;


# virtual methods
.method public abstract a()Z
.end method

.method public abstract b()V
.end method

.method public abstract c()Landroid/view/View;
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract getMetadata()LX/7IE;
.end method

.method public abstract getSeekPosition()I
.end method

.method public abstract getVideoViewCurrentPosition()I
.end method

.method public abstract getVideoViewDurationInMillis()I
.end method

.method public abstract setDelayedCompletionListener(LX/3FJ;)V
.end method

.method public abstract setVideoViewClickable(Z)V
.end method

.method public abstract setVideoViewMediaController(Landroid/widget/MediaController;)V
.end method

.method public abstract setVideoViewOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
.end method

.method public abstract setVideoViewOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
.end method

.method public abstract setVideoViewOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
.end method

.method public abstract setVideoViewPath$48ad1708(Landroid/net/Uri;)V
.end method

.method public abstract setVideoViewRotation(F)V
.end method
