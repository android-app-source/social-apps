.class public final LX/7DV;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7D7;

.field public final synthetic b:LX/7Da;


# direct methods
.method public constructor <init>(LX/7Da;LX/7D7;)V
    .locals 0

    .prologue
    .line 1183009
    iput-object p1, p0, LX/7DV;->b:LX/7Da;

    iput-object p2, p0, LX/7DV;->a:LX/7D7;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1183010
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1183011
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1183012
    :cond_0
    :goto_0
    return-void

    .line 1183013
    :cond_1
    if-eqz v0, :cond_0

    .line 1183014
    :try_start_0
    iget-object v1, p0, LX/7DV;->b:LX/7Da;

    iget-object v1, v1, LX/7Da;->a:Ljava/util/Map;

    iget-object v2, p0, LX/7DV;->a:LX/7D7;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7DZ;

    sget-object v2, LX/7DY;->URI_READY:LX/7DY;

    .line 1183015
    iput-object v2, v1, LX/7DZ;->a:LX/7DY;

    .line 1183016
    new-instance v1, LX/7Dk;

    iget-object v2, p0, LX/7DV;->a:LX/7D7;

    invoke-direct {v1, v2, v0}, LX/7Dk;-><init>(LX/7D7;LX/1FJ;)V

    .line 1183017
    iget-object v2, p0, LX/7DV;->b:LX/7Da;

    iget-object v2, v2, LX/7Da;->h:LX/7DQ;

    invoke-virtual {v2, v1}, LX/7DQ;->a(LX/7Dk;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1183018
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1183019
    iget-object v0, p0, LX/7DV;->b:LX/7Da;

    iget-object v0, v0, LX/7Da;->a:Ljava/util/Map;

    iget-object v1, p0, LX/7DV;->a:LX/7D7;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7DZ;

    sget-object v1, LX/7DY;->URI_READY:LX/7DY;

    .line 1183020
    iput-object v1, v0, LX/7DZ;->a:LX/7DY;

    .line 1183021
    return-void
.end method
