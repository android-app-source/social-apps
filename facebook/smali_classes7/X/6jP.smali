.class public LX/6jP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1130660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(D)D
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1130661
    neg-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    div-double v0, v2, v0

    return-wide v0
.end method

.method public static a(DJJ)D
    .locals 8

    .prologue
    .line 1130662
    const-wide/32 v6, 0x48190800

    move-wide v0, p0

    move-wide v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v7}, LX/6jP;->a(DJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(DJJJ)D
    .locals 6

    .prologue
    .line 1130663
    sub-long v0, p4, p2

    .line 1130664
    const-wide/16 v2, 0x0

    cmpl-double v2, p0, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    :cond_0
    :goto_0
    return-wide p0

    :cond_1
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    long-to-double v0, v0

    long-to-double v4, p6

    div-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    mul-double/2addr p0, v0

    goto :goto_0
.end method
