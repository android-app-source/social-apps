.class public final LX/7FF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Runnable;

.field public final synthetic b:LX/7FG;


# direct methods
.method public constructor <init>(LX/7FG;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1186008
    iput-object p1, p0, LX/7FF;->b:LX/7FG;

    iput-object p2, p0, LX/7FF;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1186009
    iget-object v0, p0, LX/7FF;->b:LX/7FG;

    iget-object v0, v0, LX/7FG;->b:LX/03V;

    sget-object v1, LX/7FG;->a:Ljava/lang/String;

    const-string v2, "NaRF:Fetching Notification From Server Failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1186010
    iget-object v0, p0, LX/7FF;->b:LX/7FG;

    invoke-static {v0}, LX/7FG;->e(LX/7FG;)V

    .line 1186011
    iget-object v0, p0, LX/7FF;->b:LX/7FG;

    iget-object v0, v0, LX/7FG;->f:Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j()V

    .line 1186012
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1186013
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1186014
    if-eqz p1, :cond_0

    .line 1186015
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1186016
    if-nez v0, :cond_1

    .line 1186017
    :cond_0
    :goto_0
    return-void

    .line 1186018
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1186019
    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;

    .line 1186020
    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;->a()LX/0Px;

    move-result-object v2

    .line 1186021
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1186022
    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-nez v4, :cond_3

    :cond_2
    :goto_1
    if-nez v1, :cond_0

    .line 1186023
    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel$ActorsModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    .line 1186024
    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel$ActorsModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1186025
    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1186026
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;->l()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1186027
    iget-object v5, p0, LX/7FF;->b:LX/7FG;

    invoke-static {v5, v4, v1}, LX/7FG;->a$redex0(LX/7FG;LX/15i;I)Landroid/text/Spannable;

    move-result-object v1

    .line 1186028
    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 1186029
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1186030
    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;->k()J

    move-result-wide v4

    .line 1186031
    iget-object v6, p0, LX/7FF;->b:LX/7FG;

    new-instance v0, LX/7FH;

    invoke-direct/range {v0 .. v5}, LX/7FH;-><init>(Landroid/text/Spannable;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1186032
    iput-object v0, v6, LX/7FG;->g:LX/7FH;

    .line 1186033
    :try_start_0
    iget-object v0, p0, LX/7FF;->b:LX/7FG;

    iget-object v0, v0, LX/7FG;->f:Lcom/facebook/structuredsurvey/StructuredSurveyController;

    iget-object v1, p0, LX/7FF;->a:Ljava/lang/Runnable;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Ljava/lang/Runnable;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1186034
    :catch_0
    move-exception v0

    .line 1186035
    iget-object v1, p0, LX/7FF;->b:LX/7FG;

    iget-object v1, v1, LX/7FG;->b:LX/03V;

    sget-object v2, LX/7FG;->a:Ljava/lang/String;

    const-string v3, "NaRF:IntegrationPoint Model Init Failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1186036
    iget-object v0, p0, LX/7FF;->b:LX/7FG;

    invoke-static {v0}, LX/7FG;->e(LX/7FG;)V

    .line 1186037
    iget-object v0, p0, LX/7FF;->b:LX/7FG;

    iget-object v0, v0, LX/7FG;->f:Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j()V

    goto/16 :goto_0

    .line 1186038
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$FetchNotificationForSurveyModel;->l()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 1186039
    if-eqz v4, :cond_2

    move v1, v3

    goto :goto_1

    .line 1186040
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method
