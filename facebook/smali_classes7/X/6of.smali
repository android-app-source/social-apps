.class public LX/6of;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6of;


# instance fields
.field private final a:LX/6oi;


# direct methods
.method public constructor <init>(LX/6oi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1148339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148340
    iput-object p1, p0, LX/6of;->a:LX/6oi;

    .line 1148341
    return-void
.end method

.method public static a(LX/0QB;)LX/6of;
    .locals 4

    .prologue
    .line 1148342
    sget-object v0, LX/6of;->b:LX/6of;

    if-nez v0, :cond_1

    .line 1148343
    const-class v1, LX/6of;

    monitor-enter v1

    .line 1148344
    :try_start_0
    sget-object v0, LX/6of;->b:LX/6of;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1148345
    if-eqz v2, :cond_0

    .line 1148346
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1148347
    new-instance p0, LX/6of;

    invoke-static {v0}, LX/6oi;->a(LX/0QB;)LX/6oi;

    move-result-object v3

    check-cast v3, LX/6oi;

    invoke-direct {p0, v3}, LX/6of;-><init>(LX/6oi;)V

    .line 1148348
    move-object v0, p0

    .line 1148349
    sput-object v0, LX/6of;->b:LX/6of;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148350
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1148351
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1148352
    :cond_1
    sget-object v0, LX/6of;->b:LX/6of;

    return-object v0

    .line 1148353
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1148354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/auth/pin/model/PaymentPin;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1148355
    const-string v0, "getPaymentPin"

    const v1, -0x23a9eaf0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1148356
    :try_start_0
    iget-object v0, p0, LX/6of;->a:LX/6oi;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1148357
    const-string v1, "payment_pin_id"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1148358
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 1148359
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1148360
    const v0, -0x5b091424

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    :goto_0
    return-object v0

    .line 1148361
    :cond_0
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1148362
    sget-object v0, LX/6oj;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v2

    .line 1148363
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    sget-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1148364
    :goto_1
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1148365
    const v1, 0x168559b1

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1148366
    :cond_1
    :try_start_5
    new-instance v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-direct {v0, v2, v3}, Lcom/facebook/payments/auth/pin/model/PaymentPin;-><init>(J)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1148367
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1148368
    :catchall_1
    move-exception v0

    const v1, -0x9898cf8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
