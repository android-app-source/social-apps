.class public LX/7xn;
.super LX/62U;
.source ""


# instance fields
.field public m:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/widget/text/BetterButton;

.field public o:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public p:Landroid/content/Context;

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1277871
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1277872
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/7xn;->p:Landroid/content/Context;

    .line 1277873
    const-class v0, LX/7xn;

    iget-object v1, p0, LX/7xn;->p:Landroid/content/Context;

    invoke-static {v0, p0, v1}, LX/7xn;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1277874
    const v0, 0x7f0d0e76

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, LX/7xn;->n:Lcom/facebook/widget/text/BetterButton;

    .line 1277875
    const v0, 0x7f0d0e88

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/7xn;->o:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1277876
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7xn;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object p0

    check-cast p0, LX/1nG;

    iput-object v0, p1, LX/7xn;->q:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, LX/7xn;->m:LX/1nG;

    return-void
.end method
