.class public LX/6kd;
.super LX/6kT;
.source ""


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/4 v3, 0x1

    .line 1140086
    sput-boolean v3, LX/6kd;->a:Z

    .line 1140087
    new-instance v0, LX/1sv;

    const-string v1, "GenericMapValue"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kd;->b:LX/1sv;

    .line 1140088
    new-instance v0, LX/1sw;

    const-string v1, "asMap"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kd;->c:LX/1sw;

    .line 1140089
    new-instance v0, LX/1sw;

    const-string v1, "asString"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kd;->d:LX/1sw;

    .line 1140090
    new-instance v0, LX/1sw;

    const-string v1, "asLong"

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kd;->e:LX/1sw;

    .line 1140091
    new-instance v0, LX/1sw;

    const-string v1, "asBinary"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kd;->f:LX/1sw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1140230
    invoke-direct {p0}, LX/6kT;-><init>()V

    .line 1140231
    return-void
.end method

.method public static b(LX/1su;)LX/6kd;
    .locals 3

    .prologue
    .line 1140217
    new-instance v0, LX/6kd;

    invoke-direct {v0}, LX/6kd;-><init>()V

    .line 1140218
    new-instance v0, LX/6kd;

    invoke-direct {v0}, LX/6kd;-><init>()V

    .line 1140219
    const/4 v1, 0x0

    iput v1, v0, LX/6kd;->setField_:I

    .line 1140220
    const/4 v1, 0x0

    iput-object v1, v0, LX/6kd;->value_:Ljava/lang/Object;

    .line 1140221
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 1140222
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v1

    .line 1140223
    invoke-virtual {v0, p0, v1}, LX/6kd;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v0, LX/6kd;->value_:Ljava/lang/Object;

    .line 1140224
    iget-object v2, v0, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 1140225
    iget-short v1, v1, LX/1sw;->c:S

    iput v1, v0, LX/6kd;->setField_:I

    .line 1140226
    :cond_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    .line 1140227
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1140228
    move-object v0, v0

    .line 1140229
    return-object v0
.end method

.method private c()LX/6kZ;
    .locals 3

    .prologue
    .line 1140210
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1140211
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1140212
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140213
    check-cast v0, LX/6kZ;

    return-object v0

    .line 1140214
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'asMap\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140215
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1140216
    invoke-virtual {p0, v2}, LX/6kd;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1140203
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1140204
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1140205
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140206
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 1140207
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'asString\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140208
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1140209
    invoke-virtual {p0, v2}, LX/6kd;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private e()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 1140196
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1140197
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1140198
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140199
    check-cast v0, Ljava/lang/Long;

    return-object v0

    .line 1140200
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'asLong\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140201
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1140202
    invoke-virtual {p0, v2}, LX/6kd;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f()[B
    .locals 3

    .prologue
    .line 1140189
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1140190
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1140191
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140192
    check-cast v0, [B

    check-cast v0, [B

    return-object v0

    .line 1140193
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'asBinary\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140194
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1140195
    invoke-virtual {p0, v2}, LX/6kd;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1su;LX/1sw;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1140232
    iget-short v1, p2, LX/1sw;->c:S

    packed-switch v1, :pswitch_data_0

    .line 1140233
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    .line 1140234
    :goto_0
    return-object v0

    .line 1140235
    :pswitch_0
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/6kd;->c:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_0

    .line 1140236
    invoke-static {p1}, LX/6kZ;->b(LX/1su;)LX/6kZ;

    move-result-object v0

    goto :goto_0

    .line 1140237
    :cond_0
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140238
    :pswitch_1
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/6kd;->d:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_1

    .line 1140239
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1140240
    :cond_1
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140241
    :pswitch_2
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/6kd;->e:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_2

    .line 1140242
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 1140243
    :cond_2
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140244
    :pswitch_3
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/6kd;->f:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_3

    .line 1140245
    invoke-virtual {p1}, LX/1su;->q()[B

    move-result-object v0

    goto :goto_0

    .line 1140246
    :cond_3
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(IZ)Ljava/lang/String;
    .locals 10

    .prologue
    const/16 v9, 0x80

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1140131
    if-eqz p2, :cond_6

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 1140132
    :goto_0
    if-eqz p2, :cond_7

    const-string v0, "\n"

    move-object v4, v0

    .line 1140133
    :goto_1
    if-eqz p2, :cond_8

    const-string v0, " "

    .line 1140134
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v1, "GenericMapValue"

    invoke-direct {v6, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140135
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140136
    const-string v1, "("

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140137
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140138
    iget v1, p0, LX/6kT;->setField_:I

    move v1, v1

    .line 1140139
    if-ne v1, v3, :cond_10

    .line 1140140
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140141
    const-string v1, "asMap"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140142
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140143
    const-string v1, ":"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140144
    invoke-direct {p0}, LX/6kd;->c()LX/6kZ;

    move-result-object v1

    if-nez v1, :cond_9

    .line 1140145
    const-string v1, "null"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1140146
    :goto_4
    iget v7, p0, LX/6kT;->setField_:I

    move v7, v7

    .line 1140147
    const/4 v8, 0x2

    if-ne v7, v8, :cond_1

    .line 1140148
    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140149
    :cond_0
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140150
    const-string v1, "asString"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140151
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140152
    const-string v1, ":"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140153
    invoke-direct {p0}, LX/6kd;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    .line 1140154
    const-string v1, "null"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1140155
    :cond_1
    iget v7, p0, LX/6kT;->setField_:I

    move v7, v7

    .line 1140156
    const/4 v8, 0x3

    if-ne v7, v8, :cond_3

    .line 1140157
    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140158
    :cond_2
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140159
    const-string v1, "asLong"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140160
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140161
    const-string v1, ":"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140162
    invoke-direct {p0}, LX/6kd;->e()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_b

    .line 1140163
    const-string v1, "null"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 1140164
    :cond_3
    iget v7, p0, LX/6kT;->setField_:I

    move v7, v7

    .line 1140165
    const/4 v8, 0x4

    if-ne v7, v8, :cond_5

    .line 1140166
    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140167
    :cond_4
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140168
    const-string v1, "asBinary"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140169
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140170
    const-string v1, ":"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140171
    invoke-direct {p0}, LX/6kd;->f()[B

    move-result-object v0

    if-nez v0, :cond_c

    .line 1140172
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140173
    :cond_5
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140174
    const-string v0, ")"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140175
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1140176
    :cond_6
    const-string v0, ""

    move-object v5, v0

    goto/16 :goto_0

    .line 1140177
    :cond_7
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_1

    .line 1140178
    :cond_8
    const-string v0, ""

    goto/16 :goto_2

    .line 1140179
    :cond_9
    invoke-direct {p0}, LX/6kd;->c()LX/6kZ;

    move-result-object v1

    add-int/lit8 v7, p1, 0x1

    invoke-static {v1, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1140180
    :cond_a
    invoke-direct {p0}, LX/6kd;->d()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v7, p1, 0x1

    invoke-static {v1, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1140181
    :cond_b
    invoke-direct {p0}, LX/6kd;->e()Ljava/lang/Long;

    move-result-object v1

    add-int/lit8 v7, p1, 0x1

    invoke-static {v1, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1140182
    :cond_c
    invoke-direct {p0}, LX/6kd;->f()[B

    move-result-object v0

    array-length v0, v0

    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1140183
    :goto_8
    if-ge v2, v1, :cond_f

    .line 1140184
    if-eqz v2, :cond_d

    const-string v0, " "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140185
    :cond_d
    invoke-direct {p0}, LX/6kd;->f()[B

    move-result-object v0

    aget-byte v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_e

    invoke-direct {p0}, LX/6kd;->f()[B

    move-result-object v0

    aget-byte v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, LX/6kd;->f()[B

    move-result-object v7

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :goto_9
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140186
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1140187
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "0"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, LX/6kd;->f()[B

    move-result-object v7

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 1140188
    :cond_f
    invoke-direct {p0}, LX/6kd;->f()[B

    move-result-object v0

    array-length v0, v0

    if-le v0, v9, :cond_5

    const-string v0, " ..."

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_10
    move v1, v3

    goto/16 :goto_4
.end method

.method public final a(LX/1su;S)V
    .locals 3

    .prologue
    .line 1140116
    packed-switch p2, :pswitch_data_0

    .line 1140117
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot write union with unknown field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1140118
    :pswitch_0
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140119
    check-cast v0, LX/6kZ;

    .line 1140120
    invoke-virtual {v0, p1}, LX/6kZ;->a(LX/1su;)V

    .line 1140121
    :goto_0
    return-void

    .line 1140122
    :pswitch_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140123
    check-cast v0, Ljava/lang/String;

    .line 1140124
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1140125
    :pswitch_2
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140126
    check-cast v0, Ljava/lang/Long;

    .line 1140127
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    goto :goto_0

    .line 1140128
    :pswitch_3
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140129
    check-cast v0, [B

    check-cast v0, [B

    .line 1140130
    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(I)LX/1sw;
    .locals 3

    .prologue
    .line 1140109
    packed-switch p1, :pswitch_data_0

    .line 1140110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown field id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1140111
    :pswitch_0
    sget-object v0, LX/6kd;->c:LX/1sw;

    .line 1140112
    :goto_0
    return-object v0

    .line 1140113
    :pswitch_1
    sget-object v0, LX/6kd;->d:LX/1sw;

    goto :goto_0

    .line 1140114
    :pswitch_2
    sget-object v0, LX/6kd;->e:LX/1sw;

    goto :goto_0

    .line 1140115
    :pswitch_3
    sget-object v0, LX/6kd;->f:LX/1sw;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1140096
    instance-of v0, p1, LX/6kd;

    if-eqz v0, :cond_1

    .line 1140097
    check-cast p1, LX/6kd;

    .line 1140098
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1140099
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 1140100
    if-ne v0, v1, :cond_3

    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_2

    .line 1140101
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140102
    check-cast v0, [B

    check-cast v0, [B

    .line 1140103
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 1140104
    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1140105
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1140106
    :cond_2
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140107
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 1140108
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1140095
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1140092
    sget-boolean v0, LX/6kd;->a:Z

    .line 1140093
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kd;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1140094
    return-object v0
.end method
