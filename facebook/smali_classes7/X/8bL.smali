.class public LX/8bL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;

.field public final b:LX/0ad;

.field private final c:LX/0W3;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1371932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1371933
    iput-object p1, p0, LX/8bL;->a:LX/0Uh;

    .line 1371934
    iput-object p2, p0, LX/8bL;->b:LX/0ad;

    .line 1371935
    iput-object p3, p0, LX/8bL;->c:LX/0W3;

    .line 1371936
    return-void
.end method

.method public static b(LX/0QB;)LX/8bL;
    .locals 4

    .prologue
    .line 1371944
    new-instance v3, LX/8bL;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v2

    check-cast v2, LX/0W3;

    invoke-direct {v3, v0, v1, v2}, LX/8bL;-><init>(LX/0Uh;LX/0ad;LX/0W3;)V

    .line 1371945
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1371940
    iget-object v0, p0, LX/8bL;->a:LX/0Uh;

    const/16 v1, 0xb6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1371941
    if-eqz v0, :cond_0

    .line 1371942
    iget-object v0, p0, LX/8bL;->c:LX/0W3;

    sget-wide v2, LX/0X5;->fr:J

    invoke-interface {v0, v2, v3, p1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1371943
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/8bL;->b:LX/0ad;

    sget-char v1, LX/2yD;->F:C

    invoke-interface {v0, v1, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1371946
    iget-object v0, p0, LX/8bL;->b:LX/0ad;

    sget-short v1, LX/2yD;->r:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1371947
    const/4 v0, 0x1

    .line 1371948
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/8bL;->a:LX/0Uh;

    const/16 v1, 0xb8

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1371939
    iget-object v0, p0, LX/8bL;->b:LX/0ad;

    sget-char v1, LX/2yD;->t:C

    const-string v2, "top"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1371938
    iget-object v0, p0, LX/8bL;->b:LX/0ad;

    sget-char v1, LX/2yD;->o:C

    const-string v2, "CONTROL"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1371937
    iget-object v0, p0, LX/8bL;->b:LX/0ad;

    sget-short v1, LX/2yD;->A:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/8bL;->a:LX/0Uh;

    const/16 v1, 0xb0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0
.end method
