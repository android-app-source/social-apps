.class public final LX/8Xs;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

.field public b:[LX/8VU;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;LX/0gc;)V
    .locals 1

    .prologue
    .line 1355610
    iput-object p1, p0, LX/8Xs;->a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    .line 1355611
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 1355612
    invoke-virtual {p0}, LX/8Xs;->b()I

    move-result v0

    new-array v0, v0, [LX/8VU;

    iput-object v0, p0, LX/8Xs;->b:[LX/8VU;

    .line 1355613
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1355598
    iget-object v0, p0, LX/8Xs;->a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    iget-object v1, p0, LX/8Xs;->a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    iget-object v1, v1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->o:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 1355600
    iget-object v0, p0, LX/8Xs;->b:[LX/8VU;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 1355601
    iget-object v0, p0, LX/8Xs;->a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->o:[I

    aget v0, v0, p1

    sget v1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->a:I

    if-ne v0, v1, :cond_1

    .line 1355602
    iget-object v0, p0, LX/8Xs;->b:[LX/8VU;

    new-instance v1, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;

    invoke-direct {v1}, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;-><init>()V

    aput-object v1, v0, p1

    .line 1355603
    :goto_0
    iget-object v0, p0, LX/8Xs;->b:[LX/8VU;

    aget-object v0, v0, p1

    new-instance v1, LX/8Xr;

    invoke-direct {v1, p0}, LX/8Xr;-><init>(LX/8Xs;)V

    invoke-interface {v0, v1}, LX/8VU;->a(LX/8VT;)V

    .line 1355604
    :cond_0
    iget-object v0, p0, LX/8Xs;->b:[LX/8VU;

    aget-object v0, v0, p1

    check-cast v0, Landroid/support/v4/app/Fragment;

    :goto_1
    return-object v0

    .line 1355605
    :cond_1
    iget-object v0, p0, LX/8Xs;->a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->o:[I

    aget v0, v0, p1

    sget v1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->b:I

    if-ne v0, v1, :cond_2

    .line 1355606
    iget-object v0, p0, LX/8Xs;->b:[LX/8VU;

    new-instance v1, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    invoke-direct {v1}, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;-><init>()V

    aput-object v1, v0, p1

    goto :goto_0

    .line 1355607
    :cond_2
    iget-object v0, p0, LX/8Xs;->a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->o:[I

    aget v0, v0, p1

    sget v1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->c:I

    if-ne v0, v1, :cond_3

    .line 1355608
    iget-object v0, p0, LX/8Xs;->b:[LX/8VU;

    new-instance v1, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    invoke-direct {v1}, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;-><init>()V

    aput-object v1, v0, p1

    goto :goto_0

    .line 1355609
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1355599
    iget-object v0, p0, LX/8Xs;->a:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->o:[I

    array-length v0, v0

    return v0
.end method
