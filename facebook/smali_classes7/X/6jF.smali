.class public final enum LX/6jF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6jF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6jF;

.field public static final enum INTERSTITIAL_BUBBLES:LX/6jF;

.field public static final enum INTERSTITIAL_COOL_ANIMATIONS:LX/6jF;

.field public static final enum INTERSTITIAL_GORILLA:LX/6jF;

.field public static final enum INTERSTITIAL_SIMPLE_ANIMATIONS:LX/6jF;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1130433
    new-instance v0, LX/6jF;

    const-string v1, "INTERSTITIAL_BUBBLES"

    invoke-direct {v0, v1, v2}, LX/6jF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6jF;->INTERSTITIAL_BUBBLES:LX/6jF;

    .line 1130434
    new-instance v0, LX/6jF;

    const-string v1, "INTERSTITIAL_GORILLA"

    invoke-direct {v0, v1, v3}, LX/6jF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6jF;->INTERSTITIAL_GORILLA:LX/6jF;

    .line 1130435
    new-instance v0, LX/6jF;

    const-string v1, "INTERSTITIAL_SIMPLE_ANIMATIONS"

    invoke-direct {v0, v1, v4}, LX/6jF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6jF;->INTERSTITIAL_SIMPLE_ANIMATIONS:LX/6jF;

    .line 1130436
    new-instance v0, LX/6jF;

    const-string v1, "INTERSTITIAL_COOL_ANIMATIONS"

    invoke-direct {v0, v1, v5}, LX/6jF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6jF;->INTERSTITIAL_COOL_ANIMATIONS:LX/6jF;

    .line 1130437
    const/4 v0, 0x4

    new-array v0, v0, [LX/6jF;

    sget-object v1, LX/6jF;->INTERSTITIAL_BUBBLES:LX/6jF;

    aput-object v1, v0, v2

    sget-object v1, LX/6jF;->INTERSTITIAL_GORILLA:LX/6jF;

    aput-object v1, v0, v3

    sget-object v1, LX/6jF;->INTERSTITIAL_SIMPLE_ANIMATIONS:LX/6jF;

    aput-object v1, v0, v4

    sget-object v1, LX/6jF;->INTERSTITIAL_COOL_ANIMATIONS:LX/6jF;

    aput-object v1, v0, v5

    sput-object v0, LX/6jF;->$VALUES:[LX/6jF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1130438
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1130439
    return-void
.end method

.method public static fromOrdinal(I)LX/6jF;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1130440
    if-gez p0, :cond_0

    .line 1130441
    const/4 v0, 0x0

    .line 1130442
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/6jF;->values()[LX/6jF;

    move-result-object v0

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6jF;
    .locals 1

    .prologue
    .line 1130443
    const-class v0, LX/6jF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6jF;

    return-object v0
.end method

.method public static values()[LX/6jF;
    .locals 1

    .prologue
    .line 1130444
    sget-object v0, LX/6jF;->$VALUES:[LX/6jF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6jF;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1130445
    sget-object v0, LX/6jE;->a:[I

    invoke-virtual {p0}, LX/6jF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1130446
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1130447
    :pswitch_0
    const-string v0, "full_interstitial_bubble"

    .line 1130448
    :goto_0
    return-object v0

    .line 1130449
    :pswitch_1
    const-string v0, "full_interstitial_gorilla"

    goto :goto_0

    .line 1130450
    :pswitch_2
    const-string v0, "full_interstitial_simple_animations"

    goto :goto_0

    .line 1130451
    :pswitch_3
    const-string v0, "full_interstitial_cool_animations"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
