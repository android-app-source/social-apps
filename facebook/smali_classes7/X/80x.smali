.class public LX/80x;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0w9;

.field private final e:LX/0rq;

.field private final f:LX/0sU;


# direct methods
.method public constructor <init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0w9;LX/0rq;LX/0So;LX/0sU;LX/0ad;LX/0sZ;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1284955
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p7

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 1284956
    iput-object p5, p0, LX/80x;->d:LX/0w9;

    .line 1284957
    iput-object p6, p0, LX/80x;->e:LX/0rq;

    .line 1284958
    move-object/from16 v0, p8

    iput-object v0, p0, LX/80x;->f:LX/0sU;

    .line 1284959
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1284953
    const/4 v0, 0x2

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1284954
    const-string v0, "fetch_page_feed"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1284952
    const-string v0, "PageFeedNetworkTime"

    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 1284951
    const v0, 0xa0093

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 1284939
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1284940
    new-instance v0, LX/80v;

    invoke-direct {v0}, LX/80v;-><init>()V

    move-object v0, v0

    .line 1284941
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1284942
    const-string v1, "before_home_story_param"

    const-string v2, "after_home_story_param"

    invoke-static {v0, p1, v1, v2}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1284943
    iget-object v1, p0, LX/80x;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1284944
    iget-object v1, p0, LX/80x;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 1284945
    invoke-static {v0}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 1284946
    const-string v1, "first_home_story_param"

    .line 1284947
    iget v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v2, v2

    .line 1284948
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    iget-object v3, p0, LX/80x;->e:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->c()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1284949
    iget-object v1, p0, LX/80x;->f:LX/0sU;

    const-string v2, "PAGE"

    invoke-virtual {v1, v0, v2}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 1284950
    return-object v0
.end method
