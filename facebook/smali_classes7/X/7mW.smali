.class public LX/7mW;
.super LX/7mV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7mV",
        "<",
        "LX/7mj;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final e:Ljava/lang/String;

.field private static volatile m:LX/7mW;


# instance fields
.field public final f:LX/7mO;

.field public final g:LX/0TD;

.field public final h:LX/1RW;

.field private final i:LX/0ad;

.field public j:LX/7mU;

.field public k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field public l:LX/1RX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1237146
    const-class v0, LX/7mW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7mW;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/7mO;LX/0TD;LX/1RW;LX/0ad;LX/1RX;)V
    .locals 1
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1237160
    invoke-direct {p0, p1}, LX/7mV;-><init>(LX/0SG;)V

    .line 1237161
    iput-object p2, p0, LX/7mW;->f:LX/7mO;

    .line 1237162
    iput-object p3, p0, LX/7mW;->g:LX/0TD;

    .line 1237163
    iput-object p4, p0, LX/7mW;->h:LX/1RW;

    .line 1237164
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/7mW;->k:LX/0am;

    .line 1237165
    sget-object v0, LX/7mU;->INIT:LX/7mU;

    iput-object v0, p0, LX/7mW;->j:LX/7mU;

    .line 1237166
    iput-object p5, p0, LX/7mW;->i:LX/0ad;

    .line 1237167
    iput-object p6, p0, LX/7mW;->l:LX/1RX;

    .line 1237168
    return-void
.end method

.method public static a(LX/0QB;)LX/7mW;
    .locals 10

    .prologue
    .line 1237147
    sget-object v0, LX/7mW;->m:LX/7mW;

    if-nez v0, :cond_1

    .line 1237148
    const-class v1, LX/7mW;

    monitor-enter v1

    .line 1237149
    :try_start_0
    sget-object v0, LX/7mW;->m:LX/7mW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1237150
    if-eqz v2, :cond_0

    .line 1237151
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1237152
    new-instance v3, LX/7mW;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/7mO;->a(LX/0QB;)LX/7mO;

    move-result-object v5

    check-cast v5, LX/7mO;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v7

    check-cast v7, LX/1RW;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/1RX;->a(LX/0QB;)LX/1RX;

    move-result-object v9

    check-cast v9, LX/1RX;

    invoke-direct/range {v3 .. v9}, LX/7mW;-><init>(LX/0SG;LX/7mO;LX/0TD;LX/1RW;LX/0ad;LX/1RX;)V

    .line 1237153
    move-object v0, v3

    .line 1237154
    sput-object v0, LX/7mW;->m:LX/7mW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237155
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1237156
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1237157
    :cond_1
    sget-object v0, LX/7mW;->m:LX/7mW;

    return-object v0

    .line 1237158
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1237159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/7mW;)J
    .locals 4

    .prologue
    .line 1237144
    iget-object v0, p0, LX/7mV;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0}, LX/7mV;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static synthetic g(LX/7mW;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1237145
    invoke-super {p0}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/7mj;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1237131
    const/4 v0, 0x0

    .line 1237132
    iget-object v1, p0, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1237133
    :try_start_0
    iget-object v2, p0, LX/7mW;->j:LX/7mU;

    sget-object v3, LX/7mU;->INIT:LX/7mU;

    if-ne v2, v3, :cond_0

    .line 1237134
    sget-object v2, LX/7mU;->FETCHING:LX/7mU;

    iput-object v2, p0, LX/7mW;->j:LX/7mU;

    .line 1237135
    iget-object v2, p0, LX/7mW;->g:LX/0TD;

    new-instance v3, LX/7mP;

    invoke-direct {v3, p0}, LX/7mP;-><init>(LX/7mW;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, p0, LX/7mW;->k:LX/0am;

    .line 1237136
    iget-object v2, p0, LX/7mW;->k:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/7mQ;

    invoke-direct {v3, p0}, LX/7mQ;-><init>(LX/7mW;)V

    iget-object v4, p0, LX/7mW;->g:LX/0TD;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1237137
    :cond_0
    iget-object v2, p0, LX/7mW;->k:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1237138
    iget-object v0, p0, LX/7mW;->k:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/7mT;

    invoke-direct {v2, p0}, LX/7mT;-><init>(LX/7mW;)V

    invoke-static {v0, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1237139
    :cond_1
    monitor-exit v1

    .line 1237140
    if-eqz v0, :cond_2

    .line 1237141
    :goto_0
    return-object v0

    .line 1237142
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1237143
    :cond_2
    invoke-super {p0}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(LX/7mi;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1237130
    check-cast p1, LX/7mj;

    invoke-virtual {p0, p1}, LX/7mW;->a(LX/7mj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/7mj;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7mj;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1237128
    invoke-super {p0, p1}, LX/7mV;->a(LX/7mi;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1237129
    iget-object v0, p0, LX/7mW;->g:LX/0TD;

    new-instance v1, LX/7mR;

    invoke-direct {v1, p0, p1}, LX/7mR;-><init>(LX/7mW;LX/7mj;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1237126
    invoke-super {p0, p1}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1237127
    iget-object v0, p0, LX/7mW;->g:LX/0TD;

    new-instance v1, LX/7mS;

    invoke-direct {v1, p0, p1}, LX/7mS;-><init>(LX/7mW;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 1237125
    iget-object v0, p0, LX/7mW;->i:LX/0ad;

    sget-wide v2, LX/1aO;->m:J

    const-wide/16 v4, 0x3

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    const-wide/32 v2, 0x15180

    mul-long/2addr v0, v2

    return-wide v0
.end method
