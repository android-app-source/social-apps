.class public LX/6qx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ev;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ev",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1151341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1151342
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1151343
    return-void
.end method

.method public final bridge synthetic a(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1151344
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 2

    .prologue
    .line 1151345
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1151346
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    sget-object v1, LX/6re;->FIXED_AMOUNT:LX/6re;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1151347
    return-void

    .line 1151348
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic b(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1151349
    return-void
.end method
