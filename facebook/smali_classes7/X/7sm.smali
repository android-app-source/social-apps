.class public final LX/7sm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 1265701
    const/16 v20, 0x0

    .line 1265702
    const/16 v19, 0x0

    .line 1265703
    const/16 v18, 0x0

    .line 1265704
    const-wide/16 v16, 0x0

    .line 1265705
    const/4 v15, 0x0

    .line 1265706
    const/4 v14, 0x0

    .line 1265707
    const/4 v13, 0x0

    .line 1265708
    const/4 v12, 0x0

    .line 1265709
    const/4 v9, 0x0

    .line 1265710
    const-wide/16 v10, 0x0

    .line 1265711
    const/4 v8, 0x0

    .line 1265712
    const/4 v7, 0x0

    .line 1265713
    const/4 v6, 0x0

    .line 1265714
    const/4 v5, 0x0

    .line 1265715
    const/4 v4, 0x0

    .line 1265716
    const/4 v3, 0x0

    .line 1265717
    const/4 v2, 0x0

    .line 1265718
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_13

    .line 1265719
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1265720
    const/4 v2, 0x0

    .line 1265721
    :goto_0
    return v2

    .line 1265722
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_e

    .line 1265723
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1265724
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1265725
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1265726
    const-string v7, "additional_charges"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1265727
    invoke-static/range {p0 .. p1}, LX/7sL;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 1265728
    :cond_1
    const-string v7, "available_inventory"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1265729
    const/4 v2, 0x1

    .line 1265730
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v21, v6

    move v6, v2

    goto :goto_1

    .line 1265731
    :cond_2
    const-string v7, "description"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1265732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 1265733
    :cond_3
    const-string v7, "end_sales_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1265734
    const/4 v2, 0x1

    .line 1265735
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1265736
    :cond_4
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1265737
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1265738
    :cond_5
    const-string v7, "minimum_quantity"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1265739
    const/4 v2, 0x1

    .line 1265740
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v9, v2

    move/from16 v18, v7

    goto/16 :goto_1

    .line 1265741
    :cond_6
    const-string v7, "name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1265742
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1265743
    :cond_7
    const-string v7, "sales_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1265744
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1265745
    :cond_8
    const-string v7, "seat_map_thumbnail"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1265746
    invoke-static/range {p0 .. p1}, LX/7sk;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1265747
    :cond_9
    const-string v7, "start_sales_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1265748
    const/4 v2, 0x1

    .line 1265749
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v14

    move v8, v2

    goto/16 :goto_1

    .line 1265750
    :cond_a
    const-string v7, "tier_price"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1265751
    invoke-static/range {p0 .. p1}, LX/7sl;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1265752
    :cond_b
    const-string v7, "tier_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1265753
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventTicketTierStatusEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventTicketTierStatusEnum;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1265754
    :cond_c
    const-string v7, "tier_type"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1265755
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 1265756
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1265757
    :cond_e
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1265758
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265759
    if-eqz v6, :cond_f

    .line 1265760
    const/4 v2, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 1265761
    :cond_f
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265762
    if-eqz v3, :cond_10

    .line 1265763
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1265764
    :cond_10
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265765
    if-eqz v9, :cond_11

    .line 1265766
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1265767
    :cond_11
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265768
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265769
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1265770
    if-eqz v8, :cond_12

    .line 1265771
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1265772
    :cond_12
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1265773
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1265774
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1265775
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_13
    move/from16 v21, v19

    move/from16 v22, v20

    move/from16 v19, v15

    move/from16 v20, v18

    move/from16 v18, v14

    move-wide v14, v10

    move v11, v7

    move v10, v6

    move v6, v5

    move/from16 v24, v12

    move v12, v8

    move v8, v2

    move/from16 v25, v4

    move-wide/from16 v4, v16

    move/from16 v16, v24

    move/from16 v17, v13

    move v13, v9

    move v9, v3

    move/from16 v3, v25

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0xb

    const/4 v6, 0x7

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 1265776
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1265777
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1265778
    if-eqz v0, :cond_0

    .line 1265779
    const-string v1, "additional_charges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265780
    invoke-static {p0, v0, p2, p3}, LX/7sL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1265781
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1265782
    if-eqz v0, :cond_1

    .line 1265783
    const-string v1, "available_inventory"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265784
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1265785
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1265786
    if-eqz v0, :cond_2

    .line 1265787
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265788
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265789
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1265790
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 1265791
    const-string v2, "end_sales_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265792
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1265793
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1265794
    if-eqz v0, :cond_4

    .line 1265795
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265796
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265797
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1265798
    if-eqz v0, :cond_5

    .line 1265799
    const-string v1, "minimum_quantity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265800
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1265801
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1265802
    if-eqz v0, :cond_6

    .line 1265803
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265804
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265805
    :cond_6
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1265806
    if-eqz v0, :cond_7

    .line 1265807
    const-string v0, "sales_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265808
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265809
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1265810
    if-eqz v0, :cond_8

    .line 1265811
    const-string v1, "seat_map_thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265812
    invoke-static {p0, v0, p2}, LX/7sk;->a(LX/15i;ILX/0nX;)V

    .line 1265813
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1265814
    cmp-long v2, v0, v4

    if-eqz v2, :cond_9

    .line 1265815
    const-string v2, "start_sales_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265816
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1265817
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1265818
    if-eqz v0, :cond_e

    .line 1265819
    const-string v1, "tier_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265820
    const/4 v3, 0x0

    .line 1265821
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1265822
    invoke-virtual {p0, v0, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1265823
    if-eqz v1, :cond_a

    .line 1265824
    const-string v2, "currency"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265825
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265826
    :cond_a
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1265827
    if-eqz v1, :cond_b

    .line 1265828
    const-string v2, "formatted"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265829
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265830
    :cond_b
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 1265831
    if-eqz v1, :cond_c

    .line 1265832
    const-string v2, "offset"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265833
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1265834
    :cond_c
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1265835
    if-eqz v1, :cond_d

    .line 1265836
    const-string v2, "offset_amount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265837
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265838
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1265839
    :cond_e
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 1265840
    if-eqz v0, :cond_f

    .line 1265841
    const-string v0, "tier_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265842
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265843
    :cond_f
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1265844
    if-eqz v0, :cond_10

    .line 1265845
    const-string v1, "tier_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265846
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265847
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1265848
    return-void
.end method
