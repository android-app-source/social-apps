.class public LX/6mu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/AnalyticsLogger;


# instance fields
.field private final a:LX/05h;

.field public final b:LX/059;

.field private final c:LX/06z;

.field public d:LX/0HW;


# direct methods
.method public constructor <init>(LX/05h;LX/06z;LX/059;)V
    .locals 0

    .prologue
    .line 1146799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146800
    iput-object p1, p0, LX/6mu;->a:LX/05h;

    .line 1146801
    iput-object p2, p0, LX/6mu;->c:LX/06z;

    .line 1146802
    iput-object p3, p0, LX/6mu;->b:LX/059;

    .line 1146803
    return-void
.end method


# virtual methods
.method public final reportEvent(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1146804
    iget-object v0, p0, LX/6mu;->c:LX/06z;

    if-eqz v0, :cond_2

    .line 1146805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1146806
    iget-object v1, p0, LX/6mu;->c:LX/06z;

    .line 1146807
    iget-boolean p3, v1, LX/06z;->q:Z

    move v1, p3

    .line 1146808
    if-eqz v1, :cond_0

    const-string v1, "zero|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146809
    :cond_0
    iget-object v1, p0, LX/6mu;->c:LX/06z;

    .line 1146810
    iget-boolean p3, v1, LX/06z;->r:Z

    move v1, p3

    .line 1146811
    if-eqz v1, :cond_1

    const-string v1, "noZeroRtt|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146812
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 1146813
    const-string v1, "settings"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1146814
    :cond_2
    iget-object v0, p0, LX/6mu;->d:LX/0HW;

    if-eqz v0, :cond_3

    .line 1146815
    const-string v0, "network_type"

    iget-object v1, p0, LX/6mu;->d:LX/0HW;

    invoke-virtual {v1}, LX/0HW;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1146816
    :cond_3
    iget-object v0, p0, LX/6mu;->a:LX/05h;

    invoke-virtual {v0, p2, p1}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1146817
    return-void
.end method
