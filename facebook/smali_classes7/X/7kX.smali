.class public final LX/7kX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceProductEditMutationModels$CommerceProductEditMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

.field public final synthetic b:LX/7kY;


# direct methods
.method public constructor <init>(LX/7kY;Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V
    .locals 0

    .prologue
    .line 1232085
    iput-object p1, p0, LX/7kX;->b:LX/7kY;

    iput-object p2, p0, LX/7kX;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1232086
    iget-object v0, p0, LX/7kX;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->c:LX/7ka;

    iget-object v1, p0, LX/7kX;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232087
    iget-object v2, v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1232088
    invoke-virtual {v0, v1}, LX/7ka;->a(Ljava/lang/String;)V

    .line 1232089
    iget-object v0, p0, LX/7kX;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->b:LX/7jB;

    sget-object v1, LX/7jD;->EDIT:LX/7jD;

    iget-object v2, p0, LX/7kX;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232090
    iget-object p0, v2, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v2, p0

    .line 1232091
    invoke-static {v1, v2}, LX/7jK;->a(LX/7jD;Ljava/lang/String;)LX/7jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1232092
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1232093
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1232094
    iget-object v0, p0, LX/7kX;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->c:LX/7ka;

    iget-object v1, p0, LX/7kX;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232095
    iget-object v2, v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1232096
    invoke-virtual {v0, v1}, LX/7ka;->a(Ljava/lang/String;)V

    .line 1232097
    iget-object v0, p0, LX/7kX;->b:LX/7kY;

    invoke-static {v0}, LX/7kY;->a$redex0(LX/7kY;)V

    .line 1232098
    if-eqz p1, :cond_0

    .line 1232099
    iget-object v0, p0, LX/7kX;->b:LX/7kY;

    iget-object v1, v0, LX/7kY;->b:LX/7jB;

    sget-object v2, LX/7jD;->EDIT:LX/7jD;

    .line 1232100
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1232101
    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductEditMutationModels$CommerceProductEditMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/CommerceProductEditMutationModels$CommerceProductEditMutationFieldsModel;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v0

    iget-object v3, p0, LX/7kX;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232102
    iget-object p0, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v3, p0

    .line 1232103
    invoke-static {v2, v0, v3}, LX/7jK;->a(LX/7jD;Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;Ljava/lang/String;)LX/7jK;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1232104
    :goto_0
    return-void

    .line 1232105
    :cond_0
    iget-object v0, p0, LX/7kX;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->b:LX/7jB;

    sget-object v1, LX/7jD;->EDIT:LX/7jD;

    iget-object v2, p0, LX/7kX;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232106
    iget-object v3, v2, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v2, v3

    .line 1232107
    invoke-static {v1, v2}, LX/7jK;->a(LX/7jD;Ljava/lang/String;)LX/7jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
