.class public LX/6mN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final folder:Ljava/lang/String;

.field public final viewed:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1144053
    new-instance v0, LX/1sv;

    const-string v1, "FolderViewed"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mN;->b:LX/1sv;

    .line 1144054
    new-instance v0, LX/1sw;

    const-string v1, "folder"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mN;->c:LX/1sw;

    .line 1144055
    new-instance v0, LX/1sw;

    const-string v1, "viewed"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mN;->d:LX/1sw;

    .line 1144056
    sput-boolean v3, LX/6mN;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1144049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144050
    iput-object p1, p0, LX/6mN;->folder:Ljava/lang/String;

    .line 1144051
    iput-object p2, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    .line 1144052
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1144017
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1144018
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1144019
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1144020
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "FolderViewed"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144021
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144022
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144023
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144024
    const/4 v1, 0x1

    .line 1144025
    iget-object v5, p0, LX/6mN;->folder:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 1144026
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144027
    const-string v1, "folder"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144028
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144029
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144030
    iget-object v1, p0, LX/6mN;->folder:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 1144031
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144032
    :goto_3
    const/4 v1, 0x0

    .line 1144033
    :cond_0
    iget-object v5, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    if-eqz v5, :cond_2

    .line 1144034
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144035
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144036
    const-string v1, "viewed"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144037
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144038
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144039
    iget-object v0, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    .line 1144040
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144041
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144042
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144043
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1144044
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1144045
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1144046
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1144047
    :cond_6
    iget-object v1, p0, LX/6mN;->folder:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1144048
    :cond_7
    iget-object v0, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1143979
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1143980
    iget-object v0, p0, LX/6mN;->folder:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1143981
    iget-object v0, p0, LX/6mN;->folder:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1143982
    sget-object v0, LX/6mN;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143983
    iget-object v0, p0, LX/6mN;->folder:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143984
    :cond_0
    iget-object v0, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1143985
    iget-object v0, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1143986
    sget-object v0, LX/6mN;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143987
    iget-object v0, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1143988
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1143989
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1143990
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1143995
    if-nez p1, :cond_1

    .line 1143996
    :cond_0
    :goto_0
    return v0

    .line 1143997
    :cond_1
    instance-of v1, p1, LX/6mN;

    if-eqz v1, :cond_0

    .line 1143998
    check-cast p1, LX/6mN;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1143999
    if-nez p1, :cond_3

    .line 1144000
    :cond_2
    :goto_1
    move v0, v2

    .line 1144001
    goto :goto_0

    .line 1144002
    :cond_3
    iget-object v0, p0, LX/6mN;->folder:Ljava/lang/String;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1144003
    :goto_2
    iget-object v3, p1, LX/6mN;->folder:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1144004
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1144005
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144006
    iget-object v0, p0, LX/6mN;->folder:Ljava/lang/String;

    iget-object v3, p1, LX/6mN;->folder:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144007
    :cond_5
    iget-object v0, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1144008
    :goto_4
    iget-object v3, p1, LX/6mN;->viewed:Ljava/lang/Boolean;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1144009
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1144010
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144011
    iget-object v0, p0, LX/6mN;->viewed:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mN;->viewed:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1144012
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1144013
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1144014
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1144015
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1144016
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1143994
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1143991
    sget-boolean v0, LX/6mN;->a:Z

    .line 1143992
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mN;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1143993
    return-object v0
.end method
