.class public final LX/7lo;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1235702
    iput-object p1, p0, LX/7lo;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p2, p0, LX/7lo;->a:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    iput-object p3, p0, LX/7lo;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1235703
    iget-object v0, p0, LX/7lo;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    iget-object v1, p0, LX/7lo;->a:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    iget-object v1, v1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    sget-object v2, LX/2rt;->LIFE_EVENT:LX/2rt;

    iget-object v3, p0, LX/7lo;->b:Ljava/lang/String;

    const-string v4, "publish_life_event"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/0gd;->a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1235704
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1235705
    iget-object v0, p0, LX/7lo;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f:LX/1CW;

    invoke-virtual {v0, p1, v1, v1}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v5

    .line 1235706
    iget-object v0, p0, LX/7lo;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->j:LX/4nT;

    invoke-virtual {v0, v5}, LX/4nT;->a(Ljava/lang/String;)V

    .line 1235707
    iget-object v0, p0, LX/7lo;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    iget-object v1, p0, LX/7lo;->a:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    iget-object v1, v1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    sget-object v2, LX/2rt;->LIFE_EVENT:LX/2rt;

    iget-object v3, p0, LX/7lo;->b:Ljava/lang/String;

    const-string v4, "publish_life_event"

    const/4 v7, 0x0

    move-object v6, p1

    invoke-virtual/range {v0 .. v7}, LX/0gd;->a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/fbservice/service/ServiceException;I)V

    .line 1235708
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1235709
    invoke-direct {p0}, LX/7lo;->a()V

    return-void
.end method
