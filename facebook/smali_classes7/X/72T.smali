.class public final LX/72T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6qh;

.field public b:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/content/Context;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:LX/739;

.field public final g:LX/6xb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/739;LX/6xb;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1164617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164618
    iput-object p1, p0, LX/72T;->d:Landroid/content/Context;

    .line 1164619
    iput-object p2, p0, LX/72T;->e:Ljava/util/concurrent/Executor;

    .line 1164620
    iput-object p3, p0, LX/72T;->f:LX/739;

    .line 1164621
    iput-object p4, p0, LX/72T;->g:LX/6xb;

    .line 1164622
    return-void
.end method

.method public static a$redex0(LX/72T;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1164591
    iget-object v0, p0, LX/72T;->g:LX/6xb;

    sget-object v1, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    invoke-virtual {v0, p1, v1, p2}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/Throwable;)V

    .line 1164592
    new-instance v0, LX/6dy;

    iget-object v1, p0, LX/72T;->d:Landroid/content/Context;

    const v2, 0x7f080016

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p3, v1}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, LX/72T;->d:Landroid/content/Context;

    const v2, 0x7f081e1c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1164593
    iput-object v1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 1164594
    move-object v0, v0

    .line 1164595
    const/4 v1, 0x1

    .line 1164596
    iput-boolean v1, v0, LX/6dy;->f:Z

    .line 1164597
    move-object v0, v0

    .line 1164598
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    .line 1164599
    iget-object v1, p0, LX/72T;->a:LX/6qh;

    invoke-static {v0}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->b(Lcom/facebook/messaging/dialog/ConfirmActionParams;)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6qh;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    .line 1164600
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/73T;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 1164610
    const-string v0, "extra_mutation"

    invoke-virtual {p2, v0, v8}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1164611
    const-string v1, "shipping_address_id"

    invoke-virtual {p2, v1, v8}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1164612
    const-string v1, "make_default_mutation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    .line 1164613
    invoke-virtual/range {v0 .. v5}, LX/72T;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;Ljava/lang/String;ZZ)V

    .line 1164614
    :cond_0
    :goto_0
    return-void

    .line 1164615
    :cond_1
    const-string v1, "delete_mutation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v6, p0

    move-object v7, p1

    move-object v9, v3

    move v10, v5

    move v11, v4

    .line 1164616
    invoke-virtual/range {v6 .. v11}, LX/72T;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;Ljava/lang/String;ZZ)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;Ljava/lang/String;ZZ)V
    .locals 8

    .prologue
    .line 1164601
    iget-object v0, p0, LX/72T;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164602
    :goto_0
    return-void

    .line 1164603
    :cond_0
    iget-object v0, p0, LX/72T;->a:LX/6qh;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1164604
    new-instance v0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;-><init>(Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;Ljava/lang/String;ZZ)V

    .line 1164605
    iget-object v1, p0, LX/72T;->f:LX/739;

    .line 1164606
    iget-object v2, v1, LX/739;->c:LX/732;

    invoke-virtual {v2, v0}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1164607
    iput-object v0, p0, LX/72T;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1164608
    iget-object v0, p0, LX/72T;->a:LX/6qh;

    iget-object v1, p0, LX/72T;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/6qh;->a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    .line 1164609
    iget-object v7, p0, LX/72T;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v0, LX/72S;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LX/72S;-><init>(LX/72T;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;ZZ)V

    iget-object v1, p0, LX/72T;->e:Ljava/util/concurrent/Executor;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
