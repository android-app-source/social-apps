.class public final enum LX/73x;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/73x;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/73x;

.field public static final enum APP_INTERRUPTED_TOO_MANY_TIMES:LX/73x;

.field public static final enum CANT_UPLOAD_ALBUM_PHOTO:LX/73x;

.field public static final enum CANT_UPLOAD_TO_ALBUM:LX/73x;

.field public static final enum MISSING_MEDIA_FILE:LX/73x;

.field public static final enum NETWORK_ERROR:LX/73x;

.field public static final enum NOT_ENOUGH_DISK_SPACE:LX/73x;

.field public static final enum PERMANENT_API_ERROR_WITH_VALID_MESSAGE:LX/73x;

.field public static final enum PERMANENT_CONTENT_REJECTED_ERROR:LX/73x;

.field public static final enum PERMANENT_ERROR:LX/73x;

.field public static final enum PERMANENT_PROCESSING_ERROR:LX/73x;

.field public static final enum PERMANENT_VIDEO_PROCESSING_ERROR:LX/73x;

.field public static final enum TRANSIENT_ERROR:LX/73x;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1167163
    new-instance v0, LX/73x;

    const-string v1, "CANT_UPLOAD_TO_ALBUM"

    invoke-direct {v0, v1, v3}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->CANT_UPLOAD_TO_ALBUM:LX/73x;

    .line 1167164
    new-instance v0, LX/73x;

    const-string v1, "CANT_UPLOAD_ALBUM_PHOTO"

    invoke-direct {v0, v1, v4}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->CANT_UPLOAD_ALBUM_PHOTO:LX/73x;

    .line 1167165
    new-instance v0, LX/73x;

    const-string v1, "PERMANENT_API_ERROR_WITH_VALID_MESSAGE"

    invoke-direct {v0, v1, v5}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->PERMANENT_API_ERROR_WITH_VALID_MESSAGE:LX/73x;

    .line 1167166
    new-instance v0, LX/73x;

    const-string v1, "TRANSIENT_ERROR"

    invoke-direct {v0, v1, v6}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->TRANSIENT_ERROR:LX/73x;

    .line 1167167
    new-instance v0, LX/73x;

    const-string v1, "PERMANENT_ERROR"

    invoke-direct {v0, v1, v7}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->PERMANENT_ERROR:LX/73x;

    .line 1167168
    new-instance v0, LX/73x;

    const-string v1, "PERMANENT_CONTENT_REJECTED_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->PERMANENT_CONTENT_REJECTED_ERROR:LX/73x;

    .line 1167169
    new-instance v0, LX/73x;

    const-string v1, "PERMANENT_PROCESSING_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->PERMANENT_PROCESSING_ERROR:LX/73x;

    .line 1167170
    new-instance v0, LX/73x;

    const-string v1, "PERMANENT_VIDEO_PROCESSING_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->PERMANENT_VIDEO_PROCESSING_ERROR:LX/73x;

    .line 1167171
    new-instance v0, LX/73x;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->NETWORK_ERROR:LX/73x;

    .line 1167172
    new-instance v0, LX/73x;

    const-string v1, "NOT_ENOUGH_DISK_SPACE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->NOT_ENOUGH_DISK_SPACE:LX/73x;

    .line 1167173
    new-instance v0, LX/73x;

    const-string v1, "MISSING_MEDIA_FILE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->MISSING_MEDIA_FILE:LX/73x;

    .line 1167174
    new-instance v0, LX/73x;

    const-string v1, "APP_INTERRUPTED_TOO_MANY_TIMES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/73x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73x;->APP_INTERRUPTED_TOO_MANY_TIMES:LX/73x;

    .line 1167175
    const/16 v0, 0xc

    new-array v0, v0, [LX/73x;

    sget-object v1, LX/73x;->CANT_UPLOAD_TO_ALBUM:LX/73x;

    aput-object v1, v0, v3

    sget-object v1, LX/73x;->CANT_UPLOAD_ALBUM_PHOTO:LX/73x;

    aput-object v1, v0, v4

    sget-object v1, LX/73x;->PERMANENT_API_ERROR_WITH_VALID_MESSAGE:LX/73x;

    aput-object v1, v0, v5

    sget-object v1, LX/73x;->TRANSIENT_ERROR:LX/73x;

    aput-object v1, v0, v6

    sget-object v1, LX/73x;->PERMANENT_ERROR:LX/73x;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/73x;->PERMANENT_CONTENT_REJECTED_ERROR:LX/73x;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/73x;->PERMANENT_PROCESSING_ERROR:LX/73x;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/73x;->PERMANENT_VIDEO_PROCESSING_ERROR:LX/73x;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/73x;->NETWORK_ERROR:LX/73x;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/73x;->NOT_ENOUGH_DISK_SPACE:LX/73x;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/73x;->MISSING_MEDIA_FILE:LX/73x;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/73x;->APP_INTERRUPTED_TOO_MANY_TIMES:LX/73x;

    aput-object v2, v0, v1

    sput-object v0, LX/73x;->$VALUES:[LX/73x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1167176
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/73x;
    .locals 1

    .prologue
    .line 1167177
    const-class v0, LX/73x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/73x;

    return-object v0
.end method

.method public static values()[LX/73x;
    .locals 1

    .prologue
    .line 1167178
    sget-object v0, LX/73x;->$VALUES:[LX/73x;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/73x;

    return-object v0
.end method
