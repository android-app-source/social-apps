.class public abstract LX/7YX;
.super LX/7YU;
.source ""


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1220098
    invoke-direct {p0, p1}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1220099
    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V
    .locals 1

    .prologue
    .line 1220100
    invoke-direct {p0, p1, p2}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    .line 1220101
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YX;->b:Ljava/lang/String;

    .line 1220102
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YX;->c:Ljava/lang/String;

    .line 1220103
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1220104
    const-string v0, "subtitle_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YX;->b:Ljava/lang/String;

    .line 1220105
    const-string v0, "image_url_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YX;->c:Ljava/lang/String;

    .line 1220106
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1220107
    iget-object v0, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1220108
    invoke-super {p0, v1}, LX/7YU;->a(LX/0hN;)V

    .line 1220109
    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "subtitle_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YX;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "image_url_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YX;->c:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1220110
    return-void
.end method
