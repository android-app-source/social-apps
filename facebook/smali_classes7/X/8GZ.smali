.class public LX/8GZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/RectF;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/362;",
            "LX/362;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/graphics/RectF;

.field private final d:[F

.field private final e:Landroid/graphics/Matrix;

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1319790
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/8GZ;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319785
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/8GZ;->b:Ljava/util/Map;

    .line 1319786
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/8GZ;->d:[F

    .line 1319787
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    .line 1319788
    sget-object v0, LX/8GZ;->a:Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1319789
    return-void
.end method

.method public static a(LX/0QB;)LX/8GZ;
    .locals 1

    .prologue
    .line 1319781
    new-instance v0, LX/8GZ;

    invoke-direct {v0}, LX/8GZ;-><init>()V

    .line 1319782
    move-object v0, v0

    .line 1319783
    return-object v0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 1319780
    iget-object v0, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    sget-object v1, LX/8GZ;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    sget-object v1, LX/8GZ;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    sget-object v1, LX/8GZ;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sget-object v1, LX/8GZ;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/362;)LX/362;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1319778
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319779
    iget-object v0, p0, LX/8GZ;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/362;

    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/362;",
            ">;)",
            "Ljava/util/List",
            "<+",
            "LX/362;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1319761
    iget-object v0, p0, LX/8GZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1319762
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1319763
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319764
    :cond_0
    const/4 v0, 0x0

    .line 1319765
    :goto_0
    return-object v0

    .line 1319766
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/362;

    .line 1319767
    iget-object v1, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    invoke-interface {v0}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/graphics/RectF;->intersects(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1319768
    iget-object v1, p0, LX/8GZ;->d:[F

    invoke-interface {v0}, LX/362;->b()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->x:F

    aput v4, v1, v7

    .line 1319769
    iget-object v1, p0, LX/8GZ;->d:[F

    invoke-interface {v0}, LX/362;->b()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    aput v4, v1, v8

    .line 1319770
    iget-object v1, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    iget-object v4, p0, LX/8GZ;->d:[F

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1319771
    invoke-direct {p0}, LX/8GZ;->a()Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, LX/8GZ;->f:I

    if-lez v1, :cond_5

    .line 1319772
    :cond_3
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1319773
    iget-object v4, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    invoke-interface {v0}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1319774
    new-instance v4, Landroid/graphics/PointF;

    iget-object v5, p0, LX/8GZ;->d:[F

    aget v5, v5, v7

    iget-object v6, p0, LX/8GZ;->d:[F

    aget v6, v6, v8

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0}, LX/362;->c()F

    move-result v5

    iget v6, p0, LX/8GZ;->f:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    const/high16 v6, 0x43b40000    # 360.0f

    rem-float/2addr v5, v6

    invoke-interface {v0, v1, v4, v5}, LX/362;->a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;

    move-result-object v1

    .line 1319775
    :goto_2
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1319776
    iget-object v4, p0, LX/8GZ;->b:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 1319777
    goto :goto_0

    :cond_5
    move-object v1, v0

    goto :goto_2
.end method

.method public final a(Landroid/graphics/RectF;I)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 1319754
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319755
    sget-object v0, LX/8GZ;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1319756
    iput-object p1, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    .line 1319757
    iput p2, p0, LX/8GZ;->f:I

    .line 1319758
    iget-object v0, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    sget-object v2, LX/8GZ;->a:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1319759
    iget-object v0, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    int-to-float v1, p2

    invoke-virtual {v0, v1, v4, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1319760
    return-void
.end method

.method public final b(LX/362;)LX/362;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1319745
    instance-of v0, p1, LX/5i8;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/5i8;

    invoke-interface {v0}, LX/5i8;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319746
    :cond_0
    :goto_0
    return-object p1

    .line 1319747
    :cond_1
    invoke-direct {p0}, LX/8GZ;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, LX/8GZ;->f:I

    if-lez v0, :cond_0

    .line 1319748
    :cond_2
    iget-object v0, p0, LX/8GZ;->d:[F

    invoke-interface {p1}, LX/362;->b()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aput v1, v0, v3

    .line 1319749
    iget-object v0, p0, LX/8GZ;->d:[F

    invoke-interface {p1}, LX/362;->b()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aput v1, v0, v4

    .line 1319750
    iget-object v0, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8GZ;->d:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1319751
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1319752
    iget-object v1, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    invoke-interface {p1}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1319753
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, LX/8GZ;->d:[F

    aget v2, v2, v3

    iget-object v3, p0, LX/8GZ;->d:[F

    aget v3, v3, v4

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {p1}, LX/362;->c()F

    move-result v2

    iget v3, p0, LX/8GZ;->f:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v2, v3

    invoke-interface {p1, v0, v1, v2}, LX/362;->a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;

    move-result-object p1

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/362;",
            ">;)",
            "Ljava/util/List",
            "<+",
            "LX/362;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1319728
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319729
    :cond_0
    const/4 v0, 0x0

    .line 1319730
    :goto_0
    return-object v0

    .line 1319731
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1319732
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1319733
    iget-object v0, p0, LX/8GZ;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1319734
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/362;

    .line 1319735
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 1319736
    invoke-interface {v0}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v4, v6, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1319737
    iget-object v1, p0, LX/8GZ;->d:[F

    invoke-interface {v0}, LX/362;->b()Landroid/graphics/PointF;

    move-result-object v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    aput v7, v1, v2

    .line 1319738
    iget-object v1, p0, LX/8GZ;->d:[F

    invoke-interface {v0}, LX/362;->b()Landroid/graphics/PointF;

    move-result-object v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    aput v7, v1, v10

    .line 1319739
    iget-object v1, p0, LX/8GZ;->d:[F

    invoke-virtual {v4, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1319740
    iget v1, p0, LX/8GZ;->f:I

    if-lez v1, :cond_3

    .line 1319741
    iget v1, p0, LX/8GZ;->f:I

    rsub-int v1, v1, 0x168

    .line 1319742
    :goto_2
    new-instance v7, Landroid/graphics/PointF;

    iget-object v8, p0, LX/8GZ;->d:[F

    aget v8, v8, v2

    iget-object v9, p0, LX/8GZ;->d:[F

    aget v9, v9, v10

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0}, LX/362;->c()F

    move-result v8

    int-to-float v1, v1

    add-float/2addr v1, v8

    const/high16 v8, 0x43b40000    # 360.0f

    rem-float/2addr v1, v8

    invoke-interface {v0, v6, v7, v1}, LX/362;->a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;

    move-result-object v0

    .line 1319743
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 1319744
    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final c(LX/362;)LX/362;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1319722
    iget-object v0, p0, LX/8GZ;->c:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    .line 1319723
    :goto_0
    return-object p1

    .line 1319724
    :cond_0
    iget-object v0, p0, LX/8GZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1319725
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    .line 1319726
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/362;

    move-object p1, v0

    goto :goto_0

    .line 1319727
    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method
