.class public final LX/7f1;
.super LX/7f0;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final f:I


# instance fields
.field public g:Landroid/opengl/EGLContext;

.field public h:Landroid/opengl/EGLConfig;

.field public i:Landroid/opengl/EGLDisplay;

.field public j:Landroid/opengl/EGLSurface;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1220277
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, LX/7f1;->f:I

    return-void
.end method

.method public static d(LX/7f1;)V
    .locals 2

    .prologue
    .line 1220268
    iget-object v0, p0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/7f1;->g:Landroid/opengl/EGLContext;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/7f1;->h:Landroid/opengl/EGLConfig;

    if-nez v0, :cond_1

    .line 1220269
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This object has been released"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220270
    :cond_1
    return-void
.end method

.method public static e(LX/7f1;)V
    .locals 4

    .prologue
    .line 1220278
    iget-object v0, p0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1220279
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220280
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 1220271
    invoke-static {p0}, LX/7f1;->d(LX/7f1;)V

    .line 1220272
    iget-object v0, p0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-ne v0, v1, :cond_0

    .line 1220273
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No EGLSurface - can\'t swap buffers"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220274
    :cond_0
    iget-object v0, p0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1, p1, p2}, Landroid/opengl/EGLExt;->eglPresentationTimeANDROID(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;J)Z

    .line 1220275
    iget-object v0, p0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1220276
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1220262
    invoke-static {p0}, LX/7f1;->d(LX/7f1;)V

    .line 1220263
    iget-object v0, p0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-ne v0, v1, :cond_0

    .line 1220264
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No EGLSurface - can\'t make current"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220265
    :cond_0
    iget-object v0, p0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    iget-object v2, p0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    iget-object v3, p0, LX/7f1;->g:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1220266
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220267
    :cond_1
    return-void
.end method
