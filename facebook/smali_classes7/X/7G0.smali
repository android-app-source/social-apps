.class public LX/7G0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7G1;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7G1;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7G1;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1188659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188660
    iput-object p1, p0, LX/7G0;->a:LX/7G1;

    .line 1188661
    iput-object p2, p0, LX/7G0;->b:LX/0Or;

    .line 1188662
    return-void
.end method

.method public static a(LX/0QB;)LX/7G0;
    .locals 1

    .prologue
    .line 1188658
    invoke-static {p0}, LX/7G0;->b(LX/0QB;)LX/7G0;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7G0;
    .locals 3

    .prologue
    .line 1188656
    new-instance v1, LX/7G0;

    invoke-static {p0}, LX/7G1;->a(LX/0QB;)LX/7G1;

    move-result-object v0

    check-cast v0, LX/7G1;

    const/16 v2, 0x2fd

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/7G0;-><init>(LX/7G1;LX/0Or;)V

    .line 1188657
    return-object v1
.end method


# virtual methods
.method public final a(LX/6kT;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1188631
    instance-of v0, p1, LX/6kW;

    if-eqz v0, :cond_1

    .line 1188632
    check-cast p1, LX/6kW;

    .line 1188633
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sync_delta_passed_over"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1188634
    const-string v1, "delta_type"

    .line 1188635
    iget v2, p1, LX/6kT;->setField_:I

    move v2, v2

    .line 1188636
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188637
    const-string v1, "reason"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188638
    iget-object v1, p0, LX/7G0;->a:LX/7G1;

    sget-object v2, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v1, v0, v2}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 1188639
    :cond_0
    :goto_0
    return-void

    .line 1188640
    :cond_1
    instance-of v0, p1, LX/6kU;

    if-eqz v0, :cond_0

    .line 1188641
    check-cast p1, LX/6kU;

    .line 1188642
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sync_delta_passed_over"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1188643
    const-string v1, "client_only_delta_type"

    .line 1188644
    iget v2, p1, LX/6kT;->setField_:I

    move v2, v2

    .line 1188645
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188646
    const-string v1, "reason"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188647
    iget-object v1, p0, LX/7G0;->a:LX/7G1;

    sget-object v2, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v1, v0, v2}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 1188648
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 3

    .prologue
    .line 1188649
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sync_bad_new_message_delta"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1188650
    const-string v1, "message_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188651
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188652
    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188653
    const-string v1, "offlineThreadingId"

    invoke-virtual {v0, v1, p5, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1188654
    iget-object v1, p0, LX/7G0;->a:LX/7G1;

    sget-object v2, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v1, v0, v2}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 1188655
    return-void
.end method
