.class public LX/6q3;
.super LX/6q2;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/6of;

.field private final c:LX/6og;


# direct methods
.method public constructor <init>(LX/0Uh;LX/6of;LX/6og;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149862
    const-string v0, "PaymentDbServiceHandler"

    invoke-direct {p0, v0}, LX/6q2;-><init>(Ljava/lang/String;)V

    .line 1149863
    iput-object p1, p0, LX/6q3;->a:LX/0Uh;

    .line 1149864
    iput-object p2, p0, LX/6q3;->b:LX/6of;

    .line 1149865
    iput-object p3, p0, LX/6q3;->c:LX/6og;

    .line 1149866
    return-void
.end method

.method public static b(LX/0QB;)LX/6q3;
    .locals 4

    .prologue
    .line 1149867
    new-instance v3, LX/6q3;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/6of;->a(LX/0QB;)LX/6of;

    move-result-object v1

    check-cast v1, LX/6of;

    invoke-static {p0}, LX/6og;->a(LX/0QB;)LX/6og;

    move-result-object v2

    check-cast v2, LX/6og;

    invoke-direct {v3, v0, v1, v2}, LX/6q3;-><init>(LX/0Uh;LX/6of;LX/6og;)V

    .line 1149868
    return-object v3
.end method


# virtual methods
.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1149869
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 1149870
    iget-object v0, p0, LX/6q3;->a:LX/0Uh;

    const/16 v2, 0x5a7

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149871
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149872
    iget-object v2, p0, LX/6q3;->c:LX/6og;

    invoke-virtual {v2, v0}, LX/6og;->a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    .line 1149873
    :cond_0
    return-object v1
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149874
    iget-object v0, p0, LX/6q3;->a:LX/0Uh;

    const/16 v1, 0x5a7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1149875
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1149876
    :goto_0
    return-object v0

    .line 1149877
    :cond_0
    iget-object v0, p0, LX/6q3;->b:LX/6of;

    invoke-virtual {v0}, LX/6of;->a()Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-result-object v0

    .line 1149878
    if-eqz v0, :cond_1

    .line 1149879
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1149880
    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 1149881
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149882
    iget-object v2, p0, LX/6q3;->c:LX/6og;

    invoke-virtual {v2, v0}, LX/6og;->a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    move-object v0, v1

    .line 1149883
    goto :goto_0
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149884
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    .line 1149885
    iget-object v0, p0, LX/6q3;->a:LX/0Uh;

    const/16 v1, 0x5a7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149886
    iget-object v0, p0, LX/6q3;->c:LX/6og;

    .line 1149887
    sget-object v1, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0, v1}, LX/6og;->a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    .line 1149888
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1149889
    return-object v0
.end method
