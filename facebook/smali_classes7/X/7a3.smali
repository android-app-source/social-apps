.class public final LX/7a3;
.super LX/2xL;
.source ""


# instance fields
.field public final synthetic d:Lcom/google/android/gms/location/LocationRequest;

.field public final synthetic e:Landroid/app/PendingIntent;

.field public final synthetic f:LX/2vt;


# direct methods
.method public constructor <init>(LX/2vt;LX/2wX;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
    .locals 0

    iput-object p1, p0, LX/7a3;->f:LX/2vt;

    iput-object p3, p0, LX/7a3;->d:Lcom/google/android/gms/location/LocationRequest;

    iput-object p4, p0, LX/7a3;->e:Landroid/app/PendingIntent;

    invoke-direct {p0, p2}, LX/2xL;-><init>(LX/2wX;)V

    return-void
.end method


# virtual methods
.method public final b(LX/2wK;)V
    .locals 14

    check-cast p1, LX/2wF;

    new-instance v0, LX/2xQ;

    invoke-direct {v0, p0}, LX/2xQ;-><init>(LX/2wh;)V

    iget-object v1, p0, LX/7a3;->d:Lcom/google/android/gms/location/LocationRequest;

    iget-object v2, p0, LX/7a3;->e:Landroid/app/PendingIntent;

    iget-object v3, p1, LX/2wF;->e:LX/2wV;

    iget-object v4, v3, LX/2wV;->a:LX/2wU;

    invoke-interface {v4}, LX/2wU;->a()V

    iget-object v4, v3, LX/2wV;->a:LX/2wU;

    invoke-interface {v4}, LX/2wU;->b()Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, LX/2xF;

    invoke-static {v1}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/internal/LocationRequestInternal;

    move-result-object v5

    const/4 v7, 0x1

    const/4 v10, 0x0

    new-instance v6, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;

    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2xS;->asBinder()Landroid/os/IBinder;

    move-result-object v13

    :goto_0
    move v8, v7

    move-object v9, v5

    move-object v11, v2

    move-object v12, v10

    invoke-direct/range {v6 .. v13}, Lcom/google/android/gms/location/internal/LocationRequestUpdateData;-><init>(IILcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/os/IBinder;Landroid/app/PendingIntent;Landroid/os/IBinder;Landroid/os/IBinder;)V

    move-object v5, v6

    invoke-interface {v4, v5}, LX/2xF;->a(Lcom/google/android/gms/location/internal/LocationRequestUpdateData;)V

    return-void

    :cond_0
    move-object v13, v10

    goto :goto_0
.end method
