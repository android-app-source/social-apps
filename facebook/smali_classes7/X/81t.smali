.class public final LX/81t;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1286680
    const-class v1, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;

    const v0, 0xbcd7965

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "QuestionRemoveVoteMutation"

    const-string v6, "d3355ba6a81d138d785047e55aa3715c"

    const-string v7, "question_remove_vote"

    const-string v8, "0"

    const-string v9, "10155069968776729"

    const/4 v10, 0x0

    .line 1286681
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1286682
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1286683
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1286684
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1286685
    sparse-switch v0, :sswitch_data_0

    .line 1286686
    :goto_0
    return-object p1

    .line 1286687
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1286688
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1286689
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1286690
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3619b3d2 -> :sswitch_1
        -0x736e985 -> :sswitch_3
        0x5fb57ca -> :sswitch_0
        0x410878b1 -> :sswitch_2
    .end sparse-switch
.end method
