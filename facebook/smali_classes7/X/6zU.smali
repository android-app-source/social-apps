.class public final enum LX/6zU;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6zP;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6zU;",
        ">;",
        "LX/6zP;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6zU;

.field public static final enum CREDIT_CARD:LX/6zU;

.field public static final enum MANUAL_TRANSFER:LX/6zU;

.field public static final enum NET_BANKING:LX/6zU;

.field public static final enum PAYPAL_BILLING_AGREEMENT:LX/6zU;

.field public static final enum PAY_OVER_COUNTER:LX/6zU;

.field public static final enum UNKNOWN:LX/6zU;


# instance fields
.field private final mNewPaymentOptionType:LX/6zQ;

.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1161187
    new-instance v0, LX/6zU;

    const-string v1, "CREDIT_CARD"

    const-string v2, "cc"

    sget-object v3, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    invoke-direct {v0, v1, v5, v2, v3}, LX/6zU;-><init>(Ljava/lang/String;ILjava/lang/String;LX/6zQ;)V

    sput-object v0, LX/6zU;->CREDIT_CARD:LX/6zU;

    .line 1161188
    new-instance v0, LX/6zU;

    const-string v1, "PAYPAL_BILLING_AGREEMENT"

    const-string v2, "paypal_ba"

    sget-object v3, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    invoke-direct {v0, v1, v6, v2, v3}, LX/6zU;-><init>(Ljava/lang/String;ILjava/lang/String;LX/6zQ;)V

    sput-object v0, LX/6zU;->PAYPAL_BILLING_AGREEMENT:LX/6zU;

    .line 1161189
    new-instance v0, LX/6zU;

    const-string v1, "MANUAL_TRANSFER"

    const-string v2, "manual_transfer"

    sget-object v3, LX/6zQ;->NEW_MANUAL_TRANSFER:LX/6zQ;

    invoke-direct {v0, v1, v7, v2, v3}, LX/6zU;-><init>(Ljava/lang/String;ILjava/lang/String;LX/6zQ;)V

    sput-object v0, LX/6zU;->MANUAL_TRANSFER:LX/6zU;

    .line 1161190
    new-instance v0, LX/6zU;

    const-string v1, "NET_BANKING"

    const-string v2, "net_banking"

    sget-object v3, LX/6zQ;->NEW_NET_BANKING:LX/6zQ;

    invoke-direct {v0, v1, v8, v2, v3}, LX/6zU;-><init>(Ljava/lang/String;ILjava/lang/String;LX/6zQ;)V

    sput-object v0, LX/6zU;->NET_BANKING:LX/6zU;

    .line 1161191
    new-instance v0, LX/6zU;

    const-string v1, "PAY_OVER_COUNTER"

    const-string v2, "pay_over_counter"

    sget-object v3, LX/6zQ;->NEW_PAY_OVER_COUNTER:LX/6zQ;

    invoke-direct {v0, v1, v9, v2, v3}, LX/6zU;-><init>(Ljava/lang/String;ILjava/lang/String;LX/6zQ;)V

    sput-object v0, LX/6zU;->PAY_OVER_COUNTER:LX/6zU;

    .line 1161192
    new-instance v0, LX/6zU;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "unknown"

    sget-object v4, LX/6zQ;->UNKNOWN:LX/6zQ;

    invoke-direct {v0, v1, v2, v3, v4}, LX/6zU;-><init>(Ljava/lang/String;ILjava/lang/String;LX/6zQ;)V

    sput-object v0, LX/6zU;->UNKNOWN:LX/6zU;

    .line 1161193
    const/4 v0, 0x6

    new-array v0, v0, [LX/6zU;

    sget-object v1, LX/6zU;->CREDIT_CARD:LX/6zU;

    aput-object v1, v0, v5

    sget-object v1, LX/6zU;->PAYPAL_BILLING_AGREEMENT:LX/6zU;

    aput-object v1, v0, v6

    sget-object v1, LX/6zU;->MANUAL_TRANSFER:LX/6zU;

    aput-object v1, v0, v7

    sget-object v1, LX/6zU;->NET_BANKING:LX/6zU;

    aput-object v1, v0, v8

    sget-object v1, LX/6zU;->PAY_OVER_COUNTER:LX/6zU;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/6zU;->UNKNOWN:LX/6zU;

    aput-object v2, v0, v1

    sput-object v0, LX/6zU;->$VALUES:[LX/6zU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;LX/6zQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/6zQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1161194
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1161195
    iput-object p3, p0, LX/6zU;->mValue:Ljava/lang/String;

    .line 1161196
    iput-object p4, p0, LX/6zU;->mNewPaymentOptionType:LX/6zQ;

    .line 1161197
    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6zU;
    .locals 2

    .prologue
    .line 1161198
    invoke-static {}, LX/6zU;->values()[LX/6zU;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/6zU;->UNKNOWN:LX/6zU;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6zU;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6zU;
    .locals 1

    .prologue
    .line 1161199
    const-class v0, LX/6zU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6zU;

    return-object v0
.end method

.method public static values()[LX/6zU;
    .locals 1

    .prologue
    .line 1161200
    sget-object v0, LX/6zU;->$VALUES:[LX/6zU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6zU;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .prologue
    .line 1161201
    invoke-virtual {p0}, LX/6zU;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .prologue
    .line 1161202
    iget-object v0, p0, LX/6zU;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public final toNewPaymentOptionType()LX/6zQ;
    .locals 1

    .prologue
    .line 1161203
    iget-object v0, p0, LX/6zU;->mNewPaymentOptionType:LX/6zQ;

    return-object v0
.end method
