.class public LX/8BV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8BW;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1309318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1309319
    check-cast p1, LX/8BW;

    const/4 v6, 0x1

    .line 1309320
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1309321
    iget-object v1, p1, LX/8BW;->b:Lcom/facebook/media/upload/MediaUploadParameters;

    move-object v1, v1

    .line 1309322
    iget-object v2, v1, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1309323
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1309324
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "composer_session_id"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309325
    :cond_0
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_session_id"

    .line 1309326
    iget-object v5, p1, LX/8BW;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1309327
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309328
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_phase"

    const-string v5, "finish"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309329
    iget-object v3, v1, Lcom/facebook/media/upload/MediaUploadParameters;->f:Ljava/util/List;

    if-nez v3, :cond_3

    .line 1309330
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1309331
    :goto_0
    move-object v3, v3

    .line 1309332
    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1309333
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "v2.3/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1309334
    iget-object v4, v1, Lcom/facebook/media/upload/MediaUploadParameters;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1309335
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/videos"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1309336
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v4

    const-string v5, "media-upload-video-chunk-post"

    .line 1309337
    iput-object v5, v4, LX/14O;->b:Ljava/lang/String;

    .line 1309338
    move-object v4, v4

    .line 1309339
    const-string v5, "POST"

    .line 1309340
    iput-object v5, v4, LX/14O;->c:Ljava/lang/String;

    .line 1309341
    move-object v4, v4

    .line 1309342
    iput-object v3, v4, LX/14O;->d:Ljava/lang/String;

    .line 1309343
    move-object v3, v4

    .line 1309344
    sget-object v4, LX/14S;->JSON:LX/14S;

    .line 1309345
    iput-object v4, v3, LX/14O;->k:LX/14S;

    .line 1309346
    move-object v3, v3

    .line 1309347
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1309348
    iput-object v0, v3, LX/14O;->g:Ljava/util/List;

    .line 1309349
    move-object v0, v3

    .line 1309350
    iget-object v3, v1, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    if-nez v3, :cond_4

    .line 1309351
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1309352
    :goto_1
    move-object v1, v3

    .line 1309353
    const/4 v3, 0x0

    .line 1309354
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1309355
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1309356
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/media/upload/MediaAttachementBody;

    .line 1309357
    invoke-virtual {v3}, Lcom/facebook/media/upload/MediaAttachementBody;->a()LX/4cQ;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move-object v3, v4

    .line 1309358
    :cond_2
    move-object v1, v3

    .line 1309359
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1309360
    move-object v0, v0

    .line 1309361
    iput-boolean v6, v0, LX/14O;->n:Z

    .line 1309362
    move-object v0, v0

    .line 1309363
    iput-boolean v6, v0, LX/14O;->p:Z

    .line 1309364
    move-object v0, v0

    .line 1309365
    iput-object v2, v0, LX/14O;->A:Ljava/lang/String;

    .line 1309366
    move-object v0, v0

    .line 1309367
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v3, v1, Lcom/facebook/media/upload/MediaUploadParameters;->f:Ljava/util/List;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    goto/16 :goto_0

    :cond_4
    iget-object v3, v1, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1309368
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
