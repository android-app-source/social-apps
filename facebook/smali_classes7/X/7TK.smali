.class public LX/7TK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7TM;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7TN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/7TM;",
            ">;",
            "LX/0Or",
            "<",
            "LX/7TN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1210323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210324
    iput-object p1, p0, LX/7TK;->a:LX/0Or;

    .line 1210325
    iput-object p2, p0, LX/7TK;->b:LX/0Or;

    .line 1210326
    return-void
.end method


# virtual methods
.method public final a()LX/7TL;
    .locals 2

    .prologue
    .line 1210327
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 1210328
    iget-object v0, p0, LX/7TK;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7TL;

    .line 1210329
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7TK;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7TL;

    goto :goto_0
.end method
