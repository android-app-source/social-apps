.class public final enum LX/8bN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8bN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8bN;

.field public static final enum CAROUSEL:LX/8bN;

.field public static final enum IMAGE:LX/8bN;

.field public static final enum VIDEO:LX/8bN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1371994
    new-instance v0, LX/8bN;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v2}, LX/8bN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8bN;->IMAGE:LX/8bN;

    new-instance v0, LX/8bN;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/8bN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8bN;->VIDEO:LX/8bN;

    new-instance v0, LX/8bN;

    const-string v1, "CAROUSEL"

    invoke-direct {v0, v1, v4}, LX/8bN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8bN;->CAROUSEL:LX/8bN;

    .line 1371995
    const/4 v0, 0x3

    new-array v0, v0, [LX/8bN;

    sget-object v1, LX/8bN;->IMAGE:LX/8bN;

    aput-object v1, v0, v2

    sget-object v1, LX/8bN;->VIDEO:LX/8bN;

    aput-object v1, v0, v3

    sget-object v1, LX/8bN;->CAROUSEL:LX/8bN;

    aput-object v1, v0, v4

    sput-object v0, LX/8bN;->$VALUES:[LX/8bN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1371996
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8bN;
    .locals 1

    .prologue
    .line 1371997
    const-class v0, LX/8bN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8bN;

    return-object v0
.end method

.method public static values()[LX/8bN;
    .locals 1

    .prologue
    .line 1371998
    sget-object v0, LX/8bN;->$VALUES:[LX/8bN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8bN;

    return-object v0
.end method
