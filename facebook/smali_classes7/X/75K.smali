.class public final LX/75K;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6Z0;

.field private final b:LX/0So;

.field private c:Landroid/content/Context;

.field private d:Lcom/facebook/photos/base/media/PhotoItem;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6Z0;LX/0So;Lcom/facebook/photos/base/media/PhotoItem;J)V
    .locals 1

    .prologue
    .line 1169219
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1169220
    iput-object p1, p0, LX/75K;->c:Landroid/content/Context;

    .line 1169221
    iput-object p2, p0, LX/75K;->a:LX/6Z0;

    .line 1169222
    iput-object p3, p0, LX/75K;->b:LX/0So;

    .line 1169223
    iput-object p4, p0, LX/75K;->d:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1169224
    iput-wide p5, p0, LX/75K;->e:J

    .line 1169225
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1169226
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1169227
    iget-object v1, p0, LX/75K;->d:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-static {v1}, LX/75E;->a(Lcom/facebook/ipc/media/MediaItem;)Ljava/lang/String;

    move-result-object v1

    .line 1169228
    sget-object v2, LX/6Yz;->b:LX/0U1;

    .line 1169229
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1169230
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169231
    sget-object v1, LX/6Yz;->a:LX/0U1;

    .line 1169232
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1169233
    iget-wide v2, p0, LX/75K;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1169234
    sget-object v1, LX/6Yz;->c:LX/0U1;

    .line 1169235
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1169236
    iget-object v2, p0, LX/75K;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1169237
    iget-object v1, p0, LX/75K;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, LX/75K;->a:LX/6Z0;

    iget-object v2, v2, LX/6Z0;->f:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1169238
    const/4 v0, 0x0

    return-object v0
.end method
