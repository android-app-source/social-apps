.class public final enum LX/6g2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6g2;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6g2;

.field public static final enum AUDIO_GROUP_CALL:LX/6g2;

.field public static final enum NO_ONGOING_CALL:LX/6g2;

.field public static final enum UNKNOWN:LX/6g2;

.field public static final enum VIDEO_GROUP_CALL:LX/6g2;


# instance fields
.field private final state:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1120878
    new-instance v0, LX/6g2;

    const-string v1, "NO_ONGOING_CALL"

    const-string v2, "NO_ONGOING_CALL"

    invoke-direct {v0, v1, v3, v2}, LX/6g2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6g2;->NO_ONGOING_CALL:LX/6g2;

    .line 1120879
    new-instance v0, LX/6g2;

    const-string v1, "AUDIO_GROUP_CALL"

    const-string v2, "AUDIO_GROUP_CALL"

    invoke-direct {v0, v1, v4, v2}, LX/6g2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6g2;->AUDIO_GROUP_CALL:LX/6g2;

    .line 1120880
    new-instance v0, LX/6g2;

    const-string v1, "VIDEO_GROUP_CALL"

    const-string v2, "VIDEO_GROUP_CALL"

    invoke-direct {v0, v1, v5, v2}, LX/6g2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6g2;->VIDEO_GROUP_CALL:LX/6g2;

    .line 1120881
    new-instance v0, LX/6g2;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v6, v2}, LX/6g2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6g2;->UNKNOWN:LX/6g2;

    .line 1120882
    const/4 v0, 0x4

    new-array v0, v0, [LX/6g2;

    sget-object v1, LX/6g2;->NO_ONGOING_CALL:LX/6g2;

    aput-object v1, v0, v3

    sget-object v1, LX/6g2;->AUDIO_GROUP_CALL:LX/6g2;

    aput-object v1, v0, v4

    sget-object v1, LX/6g2;->VIDEO_GROUP_CALL:LX/6g2;

    aput-object v1, v0, v5

    sget-object v1, LX/6g2;->UNKNOWN:LX/6g2;

    aput-object v1, v0, v6

    sput-object v0, LX/6g2;->$VALUES:[LX/6g2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1120883
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1120884
    iput-object p3, p0, LX/6g2;->state:Ljava/lang/String;

    .line 1120885
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6g2;
    .locals 1

    .prologue
    .line 1120886
    const-class v0, LX/6g2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6g2;

    return-object v0
.end method

.method public static values()[LX/6g2;
    .locals 1

    .prologue
    .line 1120887
    sget-object v0, LX/6g2;->$VALUES:[LX/6g2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6g2;

    return-object v0
.end method


# virtual methods
.method public final equalsName(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1120888
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/6g2;->state:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1120889
    iget-object v0, p0, LX/6g2;->state:Ljava/lang/String;

    return-object v0
.end method
