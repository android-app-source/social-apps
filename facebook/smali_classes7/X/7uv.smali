.class public final LX/7uv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1273282
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1273283
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1273284
    :goto_0
    return v1

    .line 1273285
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1273286
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 1273287
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1273288
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1273289
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1273290
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1273291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1273292
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 1273293
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1273294
    const/4 v3, 0x0

    .line 1273295
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 1273296
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1273297
    :goto_3
    move v2, v3

    .line 1273298
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1273299
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1273300
    goto :goto_1

    .line 1273301
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1273302
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1273303
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1273304
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1273305
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1273306
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1273307
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1273308
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1273309
    const-string v5, "fbqrcode"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1273310
    invoke-static {p0, p1}, LX/7uu;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_4

    .line 1273311
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1273312
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1273313
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1273314
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1273315
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1273316
    if-eqz v0, :cond_2

    .line 1273317
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1273318
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1273319
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1273320
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 1273321
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1273322
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1273323
    if-eqz v3, :cond_0

    .line 1273324
    const-string p1, "fbqrcode"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1273325
    invoke-static {p0, v3, p2}, LX/7uu;->a(LX/15i;ILX/0nX;)V

    .line 1273326
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1273327
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1273328
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1273329
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1273330
    return-void
.end method
