.class public LX/8Gj;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1319913
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1319914
    return-void
.end method

.method public static a(LX/8Ha;Lcom/facebook/photos/data/service/PhotosServiceHandler;)LX/1qM;
    .locals 1
    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/photos/data/service/PhotosServiceQueue;
    .end annotation

    .prologue
    .line 1319915
    new-instance v0, LX/2m1;

    invoke-direct {v0, p0, p1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static a()Ljava/lang/Integer;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/photos/data/cache/PhotoSetCacheSize;
    .end annotation

    .prologue
    .line 1319916
    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/lang/Long;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/photos/data/cache/PhotoSetCacheMaxAge;
    .end annotation

    .prologue
    .line 1319917
    const-wide/32 v0, 0x36ee80

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1319918
    return-void
.end method
