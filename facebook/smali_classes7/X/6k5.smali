.class public LX/6k5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final numNoOps:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1134687
    new-instance v0, LX/1sv;

    const-string v1, "DeltaNoOp"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6k5;->b:LX/1sv;

    .line 1134688
    new-instance v0, LX/1sw;

    const-string v1, "numNoOps"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k5;->c:LX/1sw;

    .line 1134689
    sput-boolean v3, LX/6k5;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1134690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134691
    iput-object p1, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    .line 1134692
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1134693
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1134694
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1134695
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1134696
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaNoOp"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1134697
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134698
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134699
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134700
    iget-object v4, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    if-eqz v4, :cond_0

    .line 1134701
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134702
    const-string v4, "numNoOps"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134703
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134704
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134705
    iget-object v0, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 1134706
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134707
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134708
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134709
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1134710
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1134711
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1134712
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 1134713
    :cond_4
    iget-object v0, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1134714
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1134715
    iget-object v0, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1134716
    iget-object v0, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1134717
    sget-object v0, LX/6k5;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134718
    iget-object v0, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1134719
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1134720
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1134721
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1134722
    if-nez p1, :cond_1

    .line 1134723
    :cond_0
    :goto_0
    return v0

    .line 1134724
    :cond_1
    instance-of v1, p1, LX/6k5;

    if-eqz v1, :cond_0

    .line 1134725
    check-cast p1, LX/6k5;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1134726
    if-nez p1, :cond_3

    .line 1134727
    :cond_2
    :goto_1
    move v0, v2

    .line 1134728
    goto :goto_0

    .line 1134729
    :cond_3
    iget-object v0, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1134730
    :goto_2
    iget-object v3, p1, LX/6k5;->numNoOps:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1134731
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1134732
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134733
    iget-object v0, p0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    iget-object v3, p1, LX/6k5;->numNoOps:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1134734
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1134735
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1134736
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1134737
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1134738
    sget-boolean v0, LX/6k5;->a:Z

    .line 1134739
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6k5;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1134740
    return-object v0
.end method
