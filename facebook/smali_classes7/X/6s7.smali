.class public LX/6s7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152918
    iput-object p1, p0, LX/6s7;->a:LX/6rs;

    .line 1152919
    return-void
.end method

.method public static b(LX/0QB;)LX/6s7;
    .locals 2

    .prologue
    .line 1152927
    new-instance v1, LX/6s7;

    invoke-static {p0}, LX/6rs;->b(LX/0QB;)LX/6rs;

    move-result-object v0

    check-cast v0, LX/6rs;

    invoke-direct {v1, v0}, LX/6s7;-><init>(LX/6rs;)V

    .line 1152928
    return-object v1
.end method


# virtual methods
.method public final a(LX/6rc;Ljava/lang/String;LX/0lF;)Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;
    .locals 3

    .prologue
    .line 1152929
    sget-object v0, LX/6s6;->a:[I

    invoke-virtual {p1}, LX/6rc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1152930
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot handle identifier "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1152931
    :pswitch_0
    new-instance v0, Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;

    invoke-direct {v0, p1}, Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;-><init>(LX/6rc;)V

    .line 1152932
    :goto_0
    return-object v0

    .line 1152933
    :pswitch_1
    iget-object v0, p0, LX/6s7;->a:LX/6rs;

    .line 1152934
    iget-object v1, v0, LX/6rs;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6rr;

    move-object v0, v1

    .line 1152935
    invoke-interface {v0, p2, p3}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;

    goto :goto_0

    .line 1152936
    :pswitch_2
    iget-object v0, p0, LX/6s7;->a:LX/6rs;

    .line 1152937
    const/4 v1, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :cond_0
    :goto_1
    packed-switch v1, :pswitch_data_2

    .line 1152938
    iget-object v1, v0, LX/6rs;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6rr;

    :goto_2
    move-object v0, v1

    .line 1152939
    invoke-interface {v0, p2, p3}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;

    goto :goto_0

    .line 1152940
    :pswitch_3
    const-string v2, "1.1"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    .line 1152941
    :pswitch_4
    iget-object v1, v0, LX/6rs;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6rr;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xbdb4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1152920
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1152921
    invoke-virtual {p2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1152922
    const-string v3, "identifier"

    invoke-virtual {v0, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1152923
    const-string v3, "identifier"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/6rc;->forValue(Ljava/lang/String;)LX/6rc;

    move-result-object v3

    .line 1152924
    invoke-virtual {p0, v3, p1, v0}, LX/6s7;->a(LX/6rc;Ljava/lang/String;LX/0lF;)Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;

    move-result-object v0

    .line 1152925
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1152926
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
