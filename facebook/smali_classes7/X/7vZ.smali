.class public LX/7vZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field public final b:LX/0ie;


# direct methods
.method public constructor <init>(LX/0tX;LX/0ie;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1274810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274811
    iput-object p1, p0, LX/7vZ;->a:LX/0tX;

    .line 1274812
    iput-object p2, p0, LX/7vZ;->b:LX/0ie;

    .line 1274813
    return-void
.end method

.method public static a(LX/0QB;)LX/7vZ;
    .locals 1

    .prologue
    .line 1274809
    invoke-static {p0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1274758
    new-instance v0, LX/7ug;

    invoke-direct {v0}, LX/7ug;-><init>()V

    invoke-interface {p0}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v2

    .line 1274759
    iput-object v2, v0, LX/7ug;->e:Ljava/lang/String;

    .line 1274760
    move-object v0, v0

    .line 1274761
    iput-boolean v1, v0, LX/7ug;->g:Z

    .line 1274762
    move-object v0, v0

    .line 1274763
    iput-object p1, v0, LX/7ug;->h:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274764
    move-object v0, v0

    .line 1274765
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 1274766
    iput-object v2, v0, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274767
    move-object v3, v0

    .line 1274768
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v0, :cond_2

    .line 1274769
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1274770
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1274771
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v1

    .line 1274772
    :goto_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v4, :cond_5

    .line 1274773
    new-instance v4, LX/7uj;

    invoke-direct {v4}, LX/7uj;-><init>()V

    add-int/lit8 v5, v0, 0x1

    .line 1274774
    iput v5, v4, LX/7uj;->a:I

    .line 1274775
    move-object v4, v4

    .line 1274776
    invoke-virtual {v4}, LX/7uj;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v4

    .line 1274777
    iput-object v4, v3, LX/7ug;->c:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1274778
    :cond_0
    :goto_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v4, :cond_6

    .line 1274779
    new-instance v2, LX/7uj;

    invoke-direct {v2}, LX/7uj;-><init>()V

    add-int/lit8 v0, v0, -0x1

    .line 1274780
    iput v0, v2, LX/7uj;->a:I

    .line 1274781
    move-object v0, v2

    .line 1274782
    invoke-virtual {v0}, LX/7uj;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v0

    .line 1274783
    iput-object v0, v3, LX/7ug;->c:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1274784
    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1274785
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    if-nez v0, :cond_7

    .line 1274786
    :goto_4
    new-instance v0, LX/7uh;

    invoke-direct {v0}, LX/7uh;-><init>()V

    add-int/lit8 v1, v1, -0x1

    .line 1274787
    iput v1, v0, LX/7uh;->a:I

    .line 1274788
    move-object v0, v0

    .line 1274789
    invoke-virtual {v0}, LX/7uh;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v0

    .line 1274790
    iput-object v0, v3, LX/7ug;->a:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    .line 1274791
    :cond_2
    invoke-virtual {v3}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v0

    return-object v0

    .line 1274792
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a()I

    move-result v0

    goto :goto_0

    .line 1274793
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;->a()I

    move-result v2

    goto :goto_1

    .line 1274794
    :cond_5
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v4, :cond_0

    .line 1274795
    new-instance v4, LX/7uk;

    invoke-direct {v4}, LX/7uk;-><init>()V

    add-int/lit8 v5, v2, 0x1

    .line 1274796
    iput v5, v4, LX/7uk;->a:I

    .line 1274797
    move-object v4, v4

    .line 1274798
    invoke-virtual {v4}, LX/7uk;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v4

    .line 1274799
    iput-object v4, v3, LX/7ug;->d:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    .line 1274800
    goto :goto_2

    .line 1274801
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v0, :cond_1

    .line 1274802
    new-instance v0, LX/7uk;

    invoke-direct {v0}, LX/7uk;-><init>()V

    add-int/lit8 v2, v2, -0x1

    .line 1274803
    iput v2, v0, LX/7uk;->a:I

    .line 1274804
    move-object v0, v0

    .line 1274805
    invoke-virtual {v0}, LX/7uk;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v0

    .line 1274806
    iput-object v0, v3, LX/7ug;->d:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    .line 1274807
    goto :goto_3

    .line 1274808
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a()I

    move-result v1

    goto :goto_4
.end method

.method public static a(LX/7vZ;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            "Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274693
    const/4 v0, 0x0

    .line 1274694
    if-nez p2, :cond_4

    .line 1274695
    :goto_0
    move-object v0, v0

    .line 1274696
    new-instance v1, LX/4Ed;

    invoke-direct {v1}, LX/4Ed;-><init>()V

    .line 1274697
    new-instance v2, LX/4EG;

    invoke-direct {v2}, LX/4EG;-><init>()V

    .line 1274698
    iget-object v3, p4, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274699
    iget-object v3, p4, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 1274700
    iget-object v3, p4, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1274701
    :cond_0
    iget-object v3, p4, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1274702
    iget-object v3, p4, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1274703
    :cond_1
    new-instance v3, LX/4EG;

    invoke-direct {v3}, LX/4EG;-><init>()V

    .line 1274704
    iget-object v4, p4, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274705
    invoke-virtual {v3, p5}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1274706
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1274707
    invoke-virtual {v3, p6}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1274708
    :cond_2
    new-instance v4, LX/4EL;

    invoke-direct {v4}, LX/4EL;-><init>()V

    .line 1274709
    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1274710
    move-object v2, v4

    .line 1274711
    invoke-virtual {v1, v2}, LX/4Ed;->a(LX/4EL;)LX/4Ed;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/4Ed;->b(Ljava/lang/String;)LX/4Ed;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/4Ed;->c(Ljava/lang/String;)LX/4Ed;

    move-result-object v0

    .line 1274712
    iget-object v1, p4, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1274713
    iget-object v1, p4, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Ed;->a(Ljava/util/List;)LX/4Ed;

    .line 1274714
    :cond_3
    invoke-static {}, LX/7uR;->d()LX/7uQ;

    move-result-object v1

    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uQ;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 1274715
    iget-object v1, p0, LX/7vZ;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1274716
    new-instance v1, LX/7vX;

    invoke-direct {v1, p0, p2}, LX/7vX;-><init>(LX/7vZ;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 1274717
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1274718
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1274719
    return-object v0

    .line 1274720
    :cond_4
    sget-object v1, LX/7vY;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 1274721
    :pswitch_0
    const-string v0, "WATCHED"

    goto/16 :goto_0

    .line 1274722
    :pswitch_1
    const-string v0, "GOING"

    goto/16 :goto_0

    .line 1274723
    :pswitch_2
    const-string v0, "UNWATCHED"

    goto/16 :goto_0

    .line 1274724
    :pswitch_3
    const-string v0, "DECLINED"

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274745
    new-instance v0, LX/7ug;

    invoke-direct {v0}, LX/7ug;-><init>()V

    .line 1274746
    iput-object p1, v0, LX/7ug;->e:Ljava/lang/String;

    .line 1274747
    move-object v0, v0

    .line 1274748
    const/4 v1, 0x0

    .line 1274749
    iput-boolean v1, v0, LX/7ug;->g:Z

    .line 1274750
    move-object v0, v0

    .line 1274751
    iput-object p2, v0, LX/7ug;->h:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274752
    move-object v0, v0

    .line 1274753
    invoke-static {p2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    .line 1274754
    iput-object v1, v0, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274755
    move-object v0, v0

    .line 1274756
    invoke-virtual {v0}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v3

    .line 1274757
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v6}, LX/7vZ;->a(LX/7vZ;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7vZ;
    .locals 3

    .prologue
    .line 1274743
    new-instance v2, LX/7vZ;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0ie;->a(LX/0QB;)LX/0ie;

    move-result-object v1

    check-cast v1, LX/0ie;

    invoke-direct {v2, v0, v1}, LX/7vZ;-><init>(LX/0tX;LX/0ie;)V

    .line 1274744
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Lcom/facebook/events/common/ActionMechanism;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274742
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/events/common/ActionMechanism;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274741
    const-string v4, "unknown"

    if-eqz p5, :cond_0

    invoke-virtual {p5}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v6, "unknown"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/events/common/ActionMechanism;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274727
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    const-string v3, "unknown"

    move-object v2, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1274728
    new-instance v1, LX/7ug;

    invoke-direct {v1}, LX/7ug;-><init>()V

    .line 1274729
    iput-object p1, v1, LX/7ug;->e:Ljava/lang/String;

    .line 1274730
    move-object v1, v1

    .line 1274731
    const/4 v2, 0x0

    .line 1274732
    iput-boolean v2, v1, LX/7ug;->g:Z

    .line 1274733
    move-object v1, v1

    .line 1274734
    iput-object p2, v1, LX/7ug;->h:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274735
    move-object v1, v1

    .line 1274736
    invoke-static {p2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 1274737
    iput-object v2, v1, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274738
    move-object v1, v1

    .line 1274739
    invoke-virtual {v1}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v4

    .line 1274740
    invoke-virtual {p5}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v0

    move-object v7, p7

    invoke-static/range {v1 .. v7}, LX/7vZ;->a(LX/7vZ;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274725
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    const/4 v5, 0x0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1274726
    invoke-direct {p0, p1, p2, v0, p6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
