.class public final LX/7Xk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1rM;


# direct methods
.method public constructor <init>(LX/1rM;)V
    .locals 0

    .prologue
    .line 1218627
    iput-object p1, p0, LX/7Xk;->a:LX/1rM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1218628
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 1218629
    :goto_0
    return-void

    .line 1218630
    :cond_0
    iget-object v0, p0, LX/7Xk;->a:LX/1rM;

    invoke-static {v0, p1}, LX/1rM;->a$redex0(LX/1rM;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1218631
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1218632
    iget-object v0, p0, LX/7Xk;->a:LX/1rM;

    .line 1218633
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1218634
    check-cast v1, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel;

    invoke-virtual {v1}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel;->a()Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;

    move-result-object v2

    .line 1218635
    if-nez v2, :cond_0

    .line 1218636
    iget-object v1, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/7YV;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1218637
    iget-object v1, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1218638
    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0df;->y:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1218639
    iget-object v1, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1218640
    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0df;->z:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1218641
    iget-object v1, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1218642
    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0df;->A:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1218643
    iget-object v1, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1218644
    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0df;->B:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1218645
    iget-object v1, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1218646
    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0df;->C:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1218647
    :goto_0
    return-void

    .line 1218648
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_new"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1218649
    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x522bfd46

    if-ne v3, v4, :cond_1

    .line 1218650
    new-instance v3, LX/7Yb;

    iget-object v4, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v4, v2}, LX/7Yb;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    invoke-virtual {v3}, LX/7Yb;->a()V

    .line 1218651
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->t()I

    move-result v2

    int-to-long v3, v2

    invoke-static {v0, v1, v3, v4}, LX/1rM;->a(LX/1rM;Ljava/lang/String;J)V

    goto :goto_0

    .line 1218652
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x6b01bef3

    if-ne v3, v4, :cond_2

    .line 1218653
    new-instance v3, LX/7Ya;

    iget-object v4, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v4, v2}, LX/7Ya;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    invoke-virtual {v3}, LX/7YX;->c()V

    goto :goto_1

    .line 1218654
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x633499c7

    if-ne v3, v4, :cond_3

    .line 1218655
    new-instance v3, LX/7YY;

    iget-object v4, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v4, v2}, LX/7YY;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    invoke-virtual {v3}, LX/7YX;->c()V

    goto :goto_1

    .line 1218656
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x57f963aa

    if-ne v3, v4, :cond_4

    .line 1218657
    new-instance v3, LX/7YZ;

    iget-object v4, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v4, v2}, LX/7YZ;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    invoke-virtual {v3}, LX/7YZ;->a()V

    goto :goto_1

    .line 1218658
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x11141292

    if-ne v3, v4, :cond_5

    .line 1218659
    new-instance v3, LX/7YW;

    iget-object v4, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v5, v0, LX/1rM;->n:LX/0yV;

    invoke-direct {v3, v4, v5, v2}, LX/7YW;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    invoke-virtual {v3}, LX/7YW;->a()V

    goto :goto_1

    .line 1218660
    :cond_5
    new-instance v1, LX/7YV;

    iget-object v3, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v4, v0, LX/1rM;->n:LX/0yV;

    invoke-direct {v1, v3, v4, v2}, LX/7YV;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    invoke-virtual {v1}, LX/7YV;->a()V

    .line 1218661
    invoke-virtual {v2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->y()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
