.class public LX/7WY;
.super LX/4oi;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216694
    invoke-direct {p0, p1}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1216695
    sget-object v0, LX/0df;->D:LX/0Tn;

    invoke-virtual {p0, v0}, LX/4oi;->a(LX/0Tn;)V

    .line 1216696
    const v0, 0x7f080e91

    invoke-virtual {p0, v0}, LX/7WY;->setTitle(I)V

    .line 1216697
    const v0, 0x7f080e92

    invoke-virtual {p0, v0}, LX/7WY;->setSummaryOn(I)V

    .line 1216698
    const v0, 0x7f080e93

    invoke-virtual {p0, v0}, LX/7WY;->setSummaryOff(I)V

    .line 1216699
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/4oi;->getPersistedBoolean(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7WY;->setDefaultValue(Ljava/lang/Object;)V

    .line 1216700
    return-void
.end method

.method public static b(LX/0QB;)LX/7WY;
    .locals 2

    .prologue
    .line 1216701
    new-instance v1, LX/7WY;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/7WY;-><init>(Landroid/content/Context;)V

    .line 1216702
    return-object v1
.end method
