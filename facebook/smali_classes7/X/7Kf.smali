.class public interface abstract LX/7Kf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3FS;


# virtual methods
.method public abstract a(LX/04g;)V
.end method

.method public abstract a(Lcom/facebook/video/engine/VideoPlayerParams;DLX/0P1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "D",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(ZLX/04g;)V
.end method

.method public abstract a()Z
.end method

.method public abstract callOnClick()Z
.end method

.method public abstract getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.end method

.method public abstract getCurrentPosition()I
.end method

.method public abstract getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
.end method

.method public abstract setAlwaysPlayVideoUnmuted(Z)V
.end method

.method public abstract setBackgroundResource(I)V
.end method

.method public abstract setIsVideoCompleted(Z)V
.end method

.method public abstract setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public abstract setOnClickPlayerListener(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setPauseMediaPlayerOnVideoPause(Z)V
.end method

.method public abstract setPlayerOrigin(LX/04D;)V
.end method

.method public abstract setVideoListener(LX/2pf;)V
.end method
