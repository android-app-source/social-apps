.class public final LX/6o3;
.super Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
.source ""


# instance fields
.field public final synthetic a:LX/6o4;

.field private final b:LX/6oG;


# direct methods
.method public constructor <init>(LX/6o4;LX/6oG;)V
    .locals 0

    .prologue
    .line 1147821
    iput-object p1, p0, LX/6o3;->a:LX/6o4;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;-><init>()V

    .line 1147822
    iput-object p2, p0, LX/6o3;->b:LX/6oG;

    .line 1147823
    return-void
.end method


# virtual methods
.method public final onAuthenticationError(ILjava/lang/CharSequence;)V
    .locals 11

    .prologue
    .line 1147824
    iget-object v0, p0, LX/6o3;->a:LX/6o4;

    iget-boolean v0, v0, LX/6o4;->c:Z

    if-nez v0, :cond_0

    .line 1147825
    const-string v0, "FingerprintAuthenticationManager"

    const-string v1, "onAuthenticationError: errorCode=%s errString=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1147826
    iget-object v0, p0, LX/6o3;->b:LX/6oG;

    .line 1147827
    iget-object v5, v0, LX/6oG;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    .line 1147828
    invoke-static {v5, p2}, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->c(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;Ljava/lang/CharSequence;)V

    .line 1147829
    iget-object v6, v5, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->r:Landroid/os/Handler;

    new-instance v7, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment$4;

    invoke-direct {v7, v5}, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment$4;-><init>(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;)V

    const-wide/16 v8, 0x640

    const v10, -0x212a553

    invoke-static {v6, v7, v8, v9, v10}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1147830
    :cond_0
    iget-object v0, p0, LX/6o3;->a:LX/6o4;

    invoke-virtual {v0}, LX/6o4;->a()V

    .line 1147831
    return-void
.end method

.method public final onAuthenticationFailed()V
    .locals 2

    .prologue
    .line 1147832
    iget-object v0, p0, LX/6o3;->b:LX/6oG;

    .line 1147833
    iget-object v1, v0, LX/6oG;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    .line 1147834
    const p0, 0x7f081dfe

    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->c(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;Ljava/lang/CharSequence;)V

    .line 1147835
    iget-object p0, v1, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->u:Lcom/facebook/resources/ui/FbTextView;

    if-nez p0, :cond_0

    .line 1147836
    :goto_0
    return-void

    .line 1147837
    :cond_0
    iget-object p0, v1, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object p0

    const v0, 0x7f040063

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object p0

    .line 1147838
    iget-object v0, v1, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbTextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1147839
    iget-object v0, p0, LX/6o3;->b:LX/6oG;

    invoke-virtual {v0, p1, p2}, LX/6oG;->a(ILjava/lang/CharSequence;)V

    .line 1147840
    return-void
.end method

.method public final onAuthenticationSucceeded(Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .locals 6

    .prologue
    .line 1147841
    iget-object v0, p0, LX/6o3;->b:LX/6oG;

    const/4 v3, 0x2

    .line 1147842
    invoke-virtual {p1}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;->getCryptoObject()Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    .line 1147843
    :try_start_0
    iget-object v2, v0, LX/6oG;->a:Ljava/lang/String;

    .line 1147844
    invoke-static {v2}, LX/673;->b(Ljava/lang/String;)LX/673;

    move-result-object v4

    .line 1147845
    new-instance p1, Ljava/lang/String;

    invoke-virtual {v4}, LX/673;->f()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    invoke-direct {p1, v4}, Ljava/lang/String;-><init>([B)V

    move-object v1, p1

    .line 1147846
    iget-object v2, v0, LX/6oG;->d:LX/6oH;

    invoke-virtual {v2}, LX/6oH;->a()V

    .line 1147847
    iget-object v2, v0, LX/6oG;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    .line 1147848
    iget-object v4, v2, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v4, v4

    .line 1147849
    if-eqz v4, :cond_0

    .line 1147850
    invoke-virtual {v4}, Landroid/app/Dialog;->hide()V

    .line 1147851
    :cond_0
    iget-object v4, v2, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v4}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1147852
    iget-object v4, v2, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1147853
    :cond_1
    iget-object v4, v2, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->n:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    .line 1147854
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1147855
    const-string p0, "verifyFingerprintNonceParams"

    new-instance p1, Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;

    invoke-direct {p1, v1}, Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1147856
    const-string p0, "verify_fingerprint_nonce"

    invoke-static {v4, v5, p0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 1147857
    iput-object v4, v2, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1147858
    iget-object v4, v2, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v5, LX/6o2;

    invoke-direct {v5, v2, v1}, LX/6o2;-><init>(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;Ljava/lang/String;)V

    iget-object p0, v2, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->q:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1147859
    :goto_0
    return-void

    .line 1147860
    :catch_0
    :goto_1
    iget v1, v0, LX/6oG;->c:I

    if-gt v1, v3, :cond_2

    .line 1147861
    iget-object v1, v0, LX/6oG;->d:LX/6oH;

    iget-object v1, v1, LX/6oH;->b:Landroid/content/Context;

    const v2, 0x7f081dfe

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/6oG;->a(ILjava/lang/CharSequence;)V

    .line 1147862
    iget-object v1, v0, LX/6oG;->d:LX/6oH;

    iget-object v2, v0, LX/6oG;->a:Ljava/lang/String;

    iget-object v3, v0, LX/6oG;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    iget v4, v0, LX/6oG;->c:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v1, v2, v3, v4}, LX/6oH;->a$redex0(LX/6oH;Ljava/lang/String;Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;I)V

    goto :goto_0

    .line 1147863
    :cond_2
    iget-object v1, v0, LX/6oG;->d:LX/6oH;

    invoke-virtual {v1}, LX/6oH;->a()V

    .line 1147864
    iget-object v1, v0, LX/6oG;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    .line 1147865
    invoke-static {v1}, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->n(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;)V

    .line 1147866
    goto :goto_0

    .line 1147867
    :catch_1
    goto :goto_1
.end method
