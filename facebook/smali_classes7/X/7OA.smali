.class public final enum LX/7OA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7OA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7OA;

.field public static final enum ALWAYS_HIDDEN:LX/7OA;

.field public static final enum ALWAYS_VISIBLE:LX/7OA;

.field public static final enum ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7OA;

.field public static final enum AUTO_WITH_INITIALLY_HIDDEN:LX/7OA;

.field public static final enum AUTO_WITH_INITIALLY_VISIBLE:LX/7OA;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1201203
    new-instance v0, LX/7OA;

    const-string v1, "AUTO_WITH_INITIALLY_VISIBLE"

    invoke-direct {v0, v1, v2}, LX/7OA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7OA;->AUTO_WITH_INITIALLY_VISIBLE:LX/7OA;

    .line 1201204
    new-instance v0, LX/7OA;

    const-string v1, "AUTO_WITH_INITIALLY_HIDDEN"

    invoke-direct {v0, v1, v3}, LX/7OA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7OA;->AUTO_WITH_INITIALLY_HIDDEN:LX/7OA;

    .line 1201205
    new-instance v0, LX/7OA;

    const-string v1, "ALWAYS_VISIBLE"

    invoke-direct {v0, v1, v4}, LX/7OA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7OA;->ALWAYS_VISIBLE:LX/7OA;

    .line 1201206
    new-instance v0, LX/7OA;

    const-string v1, "ALWAYS_HIDDEN"

    invoke-direct {v0, v1, v5}, LX/7OA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7OA;->ALWAYS_HIDDEN:LX/7OA;

    .line 1201207
    new-instance v0, LX/7OA;

    const-string v1, "ALWAYS_VISIBLE_UNTIL_CLICKED"

    invoke-direct {v0, v1, v6}, LX/7OA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7OA;->ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7OA;

    .line 1201208
    const/4 v0, 0x5

    new-array v0, v0, [LX/7OA;

    sget-object v1, LX/7OA;->AUTO_WITH_INITIALLY_VISIBLE:LX/7OA;

    aput-object v1, v0, v2

    sget-object v1, LX/7OA;->AUTO_WITH_INITIALLY_HIDDEN:LX/7OA;

    aput-object v1, v0, v3

    sget-object v1, LX/7OA;->ALWAYS_VISIBLE:LX/7OA;

    aput-object v1, v0, v4

    sget-object v1, LX/7OA;->ALWAYS_HIDDEN:LX/7OA;

    aput-object v1, v0, v5

    sget-object v1, LX/7OA;->ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7OA;

    aput-object v1, v0, v6

    sput-object v0, LX/7OA;->$VALUES:[LX/7OA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1201193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7OA;
    .locals 1

    .prologue
    .line 1201202
    const-class v0, LX/7OA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7OA;

    return-object v0
.end method

.method public static values()[LX/7OA;
    .locals 1

    .prologue
    .line 1201209
    sget-object v0, LX/7OA;->$VALUES:[LX/7OA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7OA;

    return-object v0
.end method


# virtual methods
.method public final getBehavior()LX/7MP;
    .locals 1

    .prologue
    .line 1201194
    sget-object v0, LX/7OA;->ALWAYS_VISIBLE:LX/7OA;

    if-ne p0, v0, :cond_0

    .line 1201195
    sget-object v0, LX/7MP;->ALWAYS_VISIBLE:LX/7MP;

    .line 1201196
    :goto_0
    return-object v0

    .line 1201197
    :cond_0
    sget-object v0, LX/7OA;->ALWAYS_HIDDEN:LX/7OA;

    if-ne p0, v0, :cond_1

    .line 1201198
    sget-object v0, LX/7MP;->ALWAYS_HIDDEN:LX/7MP;

    goto :goto_0

    .line 1201199
    :cond_1
    sget-object v0, LX/7OA;->ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7OA;

    if-ne p0, v0, :cond_2

    .line 1201200
    sget-object v0, LX/7MP;->ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7MP;

    goto :goto_0

    .line 1201201
    :cond_2
    sget-object v0, LX/7MP;->AUTO:LX/7MP;

    goto :goto_0
.end method

.method public final isInitiallyVisible()Z
    .locals 1

    .prologue
    .line 1201192
    sget-object v0, LX/7OA;->ALWAYS_VISIBLE:LX/7OA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/7OA;->AUTO_WITH_INITIALLY_VISIBLE:LX/7OA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/7OA;->ALWAYS_VISIBLE_UNTIL_CLICKED:LX/7OA;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
