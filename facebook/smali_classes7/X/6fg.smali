.class public final LX/6fg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:J

.field public d:Lcom/facebook/messaging/model/attachment/Attachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/model/threads/MontageThreadPreview;)V
    .locals 2

    .prologue
    .line 1120193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120194
    const-string v0, "montageThreadPreview cannot be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1120195
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->f:Ljava/lang/String;

    iput-object v0, p0, LX/6fg;->a:Ljava/lang/String;

    .line 1120196
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    iput-wide v0, p0, LX/6fg;->b:J

    .line 1120197
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    iput-wide v0, p0, LX/6fg;->c:J

    .line 1120198
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    iput-object v0, p0, LX/6fg;->d:Lcom/facebook/messaging/model/attachment/Attachment;

    .line 1120199
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    iput-object v0, p0, LX/6fg;->e:Ljava/lang/String;

    .line 1120200
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    iput-object v0, p0, LX/6fg;->f:Ljava/lang/String;

    .line 1120201
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1120202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120203
    const-string v0, "mMessageId cannot be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6fg;->a:Ljava/lang/String;

    .line 1120204
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/threads/MontageThreadPreview;
    .locals 2

    .prologue
    .line 1120192
    new-instance v0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/MontageThreadPreview;-><init>(LX/6fg;)V

    return-object v0
.end method
