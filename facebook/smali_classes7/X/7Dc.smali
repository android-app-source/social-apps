.class public final LX/7Dc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7D7;",
            "LX/7Dd;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7D7;",
            "LX/7Dd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/16 v1, 0x7dd

    .line 1183172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183173
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LX/7Dc;->a:Ljava/util/Map;

    .line 1183174
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LX/7Dc;->b:Ljava/util/Map;

    .line 1183175
    if-ge p1, v1, :cond_0

    .line 1183176
    const/4 v0, 0x0

    iput v0, p0, LX/7Dc;->c:I

    .line 1183177
    :goto_0
    return-void

    .line 1183178
    :cond_0
    if-ne p1, v1, :cond_1

    .line 1183179
    const/16 v0, 0xc

    iput v0, p0, LX/7Dc;->c:I

    goto :goto_0

    .line 1183180
    :cond_1
    const/16 v0, 0x28

    iput v0, p0, LX/7Dc;->c:I

    goto :goto_0
.end method

.method public static a(LX/7Dc;I)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map",
            "<",
            "LX/7D7;",
            "LX/7Dd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1183162
    if-nez p1, :cond_0

    .line 1183163
    iget-object v0, p0, LX/7Dc;->a:Ljava/util/Map;

    .line 1183164
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7Dc;->b:Ljava/util/Map;

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/7D7;",
            "LX/7Dd;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1183165
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 1183166
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dd;

    .line 1183167
    add-int/lit8 v3, v1, 0x1

    iget v0, v0, LX/7Dd;->b:I

    aput v0, v4, v1

    move v1, v3

    .line 1183168
    goto :goto_0

    .line 1183169
    :cond_0
    array-length v0, v4

    invoke-static {v0, v4, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1183170
    invoke-interface {p0}, Ljava/util/Map;->clear()V

    .line 1183171
    return-void
.end method

.method public static a(LX/7Dc;)Z
    .locals 2

    .prologue
    .line 1183161
    iget-object v0, p0, LX/7Dc;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/7Dc;LX/7D7;)Z
    .locals 1

    .prologue
    .line 1183156
    iget v0, p1, LX/7D7;->a:I

    move v0, v0

    .line 1183157
    invoke-static {p0, v0}, LX/7Dc;->a(LX/7Dc;I)Ljava/util/Map;

    move-result-object v0

    .line 1183158
    if-eqz v0, :cond_0

    .line 1183159
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 1183160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/7Dc;LX/7D7;I)V
    .locals 9

    .prologue
    .line 1183138
    sget-object v0, LX/7Df;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 1183139
    iget-object v0, p0, LX/7Dc;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dd;

    .line 1183140
    if-eqz v0, :cond_1

    .line 1183141
    iput-wide v4, v0, LX/7Dd;->c:J

    .line 1183142
    :cond_0
    :goto_0
    return-void

    .line 1183143
    :cond_1
    new-instance v1, LX/7Dd;

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v1 .. v5}, LX/7Dd;-><init>(LX/7D7;IJ)V

    .line 1183144
    iget-object v0, p0, LX/7Dc;->b:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1183145
    iget-object v0, p0, LX/7Dc;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget v2, p0, LX/7Dc;->c:I

    if-le v0, v2, :cond_0

    .line 1183146
    iget-object v0, p0, LX/7Dc;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1183147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Dd;

    iget-wide v4, v1, LX/7Dd;->c:J

    iget-wide v6, v2, LX/7Dd;->c:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_3

    .line 1183148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7D7;

    .line 1183149
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dd;

    move-object p1, v1

    move-object v1, v0

    :goto_2
    move-object v2, v1

    .line 1183150
    goto :goto_1

    .line 1183151
    :cond_2
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1183152
    new-array v0, v4, [I

    .line 1183153
    iget v1, v2, LX/7Dd;->b:I

    aput v1, v0, v3

    .line 1183154
    invoke-static {v4, v0, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1183155
    iget-object v0, p0, LX/7Dc;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method
