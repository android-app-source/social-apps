.class public LX/7BO;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1178546
    new-instance v0, LX/7BN;

    invoke-direct {v0}, LX/7BN;-><init>()V

    sput-object v0, LX/7BO;->a:LX/266;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1178454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178455
    return-void
.end method

.method private static a(Ljava/util/List;)LX/0Px;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1178547
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178548
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1178549
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;

    .line 1178550
    iget-object v3, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->category:Ljava/lang/String;

    iget-object v4, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    iget-boolean v5, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->isVerified:Z

    iget-object v6, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    iget-object v7, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->path:Ljava/lang/String;

    iget-object v8, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    iget-object v9, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->photo:Ljava/lang/String;

    iget-object v10, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    iget-object v11, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->text:Ljava/lang/String;

    iget-object v12, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->type:Ljava/lang/String;

    iget-object v13, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->uid:Ljava/lang/String;

    iget-object v14, v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->matchedTokens:LX/0Px;

    invoke-static/range {v3 .. v14}, LX/7BO;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;)Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v3

    move-object v0, v3

    .line 1178551
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1178552
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/7BO;
    .locals 1

    .prologue
    .line 1178537
    new-instance v0, LX/7BO;

    invoke-direct {v0}, LX/7BO;-><init>()V

    .line 1178538
    move-object v0, v0

    .line 1178539
    return-object v0
.end method

.method private static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1178540
    invoke-static {p0}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1178541
    const/4 v0, 0x0

    .line 1178542
    :goto_0
    return-object v0

    .line 1178543
    :cond_0
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1178544
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://www.facebook.com"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1178545
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;)Lcom/facebook/search/api/SearchTypeaheadResult;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/search/api/SearchTypeaheadResult;"
        }
    .end annotation

    .prologue
    .line 1178523
    if-nez p1, :cond_0

    .line 1178524
    const/4 v2, 0x0

    .line 1178525
    :goto_0
    invoke-static {p3}, LX/7BO;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1178526
    invoke-static {p4}, LX/7BO;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1178527
    invoke-static/range {p5 .. p5}, LX/7BO;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1178528
    invoke-static/range {p6 .. p6}, LX/7BO;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1178529
    if-nez p9, :cond_1

    .line 1178530
    const/4 v3, 0x0

    .line 1178531
    :goto_1
    invoke-static/range {p10 .. p10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1178532
    invoke-static {}, Lcom/facebook/search/api/SearchTypeaheadResult;->newBuilder()LX/7BL;

    move-result-object v10

    invoke-virtual {v10, p0}, LX/7BL;->a(Ljava/lang/String;)LX/7BL;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/7BL;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/7BL;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/7BL;->b(Z)LX/7BL;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/7BL;->b(Landroid/net/Uri;)LX/7BL;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/7BL;->c(Landroid/net/Uri;)LX/7BL;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/7BL;->a(Landroid/net/Uri;)LX/7BL;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/7BL;->d(Landroid/net/Uri;)LX/7BL;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, LX/7BL;->b(Ljava/lang/String;)LX/7BL;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, LX/7BL;->f(Ljava/lang/String;)LX/7BL;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/7BL;->a(LX/7BK;)LX/7BL;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, LX/7BL;->a(J)LX/7BL;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/7BL;->a(Ljava/util/List;)LX/7BL;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/7BL;->a(LX/0Px;)LX/7BL;

    move-result-object v2

    invoke-virtual {v2}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v2

    return-object v2

    .line 1178533
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1178534
    :catch_0
    new-instance v2, LX/7BM;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FriendStatus: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " invalid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/7BM;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1178535
    :cond_1
    :try_start_1
    invoke-virtual/range {p9 .. p9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/7BK;->valueOf(Ljava/lang/String;)LX/7BK;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_1

    .line 1178536
    :catch_1
    new-instance v2, LX/7BM;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p9

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is invalid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/7BM;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1178470
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178471
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178472
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178473
    invoke-static {p0}, LX/7BO;->b(Ljava/util/List;)LX/0P1;

    move-result-object v2

    .line 1178474
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1178475
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1178476
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 1178477
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1178478
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 1178479
    iget-wide v6, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1178480
    iget-wide v6, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 1178481
    if-eqz v1, :cond_0

    .line 1178482
    invoke-static {}, Lcom/facebook/search/api/SearchTypeaheadResult;->newBuilder()LX/7BL;

    move-result-object v9

    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, v1, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    .line 1178483
    :goto_1
    iput-object v8, v9, LX/7BL;->a:Ljava/lang/String;

    .line 1178484
    move-object v8, v9

    .line 1178485
    iget-object v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1178486
    iput-object v9, v8, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1178487
    move-object v8, v8

    .line 1178488
    iget-boolean v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    .line 1178489
    iput-boolean v9, v8, LX/7BL;->o:Z

    .line 1178490
    move-object v8, v8

    .line 1178491
    iget-object v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    .line 1178492
    iput-object v9, v8, LX/7BL;->d:Landroid/net/Uri;

    .line 1178493
    move-object v8, v8

    .line 1178494
    iget-object v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    .line 1178495
    iput-object v9, v8, LX/7BL;->e:Landroid/net/Uri;

    .line 1178496
    move-object v8, v8

    .line 1178497
    iget-object v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    .line 1178498
    iput-object v9, v8, LX/7BL;->f:Landroid/net/Uri;

    .line 1178499
    move-object v8, v8

    .line 1178500
    iget-object v9, v1, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    .line 1178501
    iput-object v9, v8, LX/7BL;->g:Ljava/lang/String;

    .line 1178502
    move-object v8, v8

    .line 1178503
    iget-object v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 1178504
    iput-object v9, v8, LX/7BL;->l:Ljava/lang/String;

    .line 1178505
    move-object v8, v8

    .line 1178506
    iget-object v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    .line 1178507
    iput-object v9, v8, LX/7BL;->m:LX/7BK;

    .line 1178508
    move-object v8, v8

    .line 1178509
    iget-wide v10, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    .line 1178510
    iput-wide v10, v8, LX/7BL;->n:J

    .line 1178511
    move-object v8, v8

    .line 1178512
    iget-object v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    .line 1178513
    iput-object v9, v8, LX/7BL;->r:Ljava/util/List;

    .line 1178514
    move-object v8, v8

    .line 1178515
    invoke-virtual {v8}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v8

    .line 1178516
    move-object v0, v8

    .line 1178517
    :cond_0
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1178518
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 1178519
    iget-wide v6, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1178520
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1178521
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1178522
    :cond_4
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method private static b(Ljava/util/List;)LX/0P1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;)",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1178466
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 1178467
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 1178468
    iget-wide v4, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1178469
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/15w;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1178456
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178457
    :try_start_0
    sget-object v0, LX/7BO;->a:LX/266;

    invoke-virtual {p1, v0}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 1178458
    if-nez v0, :cond_0

    .line 1178459
    new-instance v0, LX/7BM;

    const-string v1, "Unable to parse uberbar search results list"

    invoke-direct {v0, v1}, LX/7BM;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1178460
    :catch_0
    move-exception v0

    .line 1178461
    new-instance v1, LX/7BM;

    const-string v2, "Unable to parse uberbar search results list"

    invoke-direct {v1, v2, v0}, LX/7BM;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1178462
    :cond_0
    invoke-static {v0}, LX/7BO;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    .line 1178463
    if-nez v0, :cond_1

    .line 1178464
    new-instance v0, LX/7BM;

    const-string v1, "Unable to parse uberbar search results list"

    invoke-direct {v0, v1}, LX/7BM;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1178465
    :cond_1
    return-object v0
.end method
