.class public final LX/8LQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Z

.field public B:LX/5Rn;

.field private C:J

.field private D:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

.field private E:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field private F:J

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public H:Z

.field public I:Ljava/lang/String;

.field private J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:J

.field private L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Z

.field private O:I

.field public P:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:I

.field private S:I

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/composer/publish/common/EditPostParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/audience/model/UploadShot;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:I

.field private X:I

.field public Y:I

.field private Z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Ljava/lang/String;

.field public aa:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:Z

.field private ad:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field public ae:Ljava/lang/String;

.field public af:Z

.field private ag:LX/5Ra;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:Z

.field public ai:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aj:Z

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field private f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;

.field public j:J

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field public m:Z

.field private n:Z

.field public o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field public p:LX/8LR;

.field public q:LX/8LS;

.field public r:Ljava/lang/String;

.field public s:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private t:J

.field public u:Z

.field public v:Lcom/facebook/composer/protocol/PostReviewParams;

.field public w:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

.field public x:J

.field private y:Lcom/facebook/share/model/ComposerAppAttribution;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, -0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1331996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331997
    iput-object v1, p0, LX/8LQ;->a:Ljava/lang/String;

    .line 1331998
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1331999
    iput-object v0, p0, LX/8LQ;->b:LX/0Px;

    .line 1332000
    iput-object v1, p0, LX/8LQ;->c:LX/0Px;

    .line 1332001
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1332002
    iput-object v0, p0, LX/8LQ;->d:LX/0Px;

    .line 1332003
    iput-object v1, p0, LX/8LQ;->e:Ljava/lang/String;

    .line 1332004
    sget-object v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, LX/8LQ;->f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1332005
    iput-object v1, p0, LX/8LQ;->g:Ljava/lang/String;

    .line 1332006
    iput-wide v4, p0, LX/8LQ;->h:J

    .line 1332007
    iput-object v1, p0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332008
    iput-wide v6, p0, LX/8LQ;->j:J

    .line 1332009
    iput-object v1, p0, LX/8LQ;->k:Ljava/lang/String;

    .line 1332010
    iput-object v1, p0, LX/8LQ;->l:Ljava/lang/String;

    .line 1332011
    iput-boolean v2, p0, LX/8LQ;->m:Z

    .line 1332012
    iput-boolean v2, p0, LX/8LQ;->n:Z

    .line 1332013
    iput-object v1, p0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332014
    iput-object v1, p0, LX/8LQ;->p:LX/8LR;

    .line 1332015
    iput-object v1, p0, LX/8LQ;->q:LX/8LS;

    .line 1332016
    iput-object v1, p0, LX/8LQ;->r:Ljava/lang/String;

    .line 1332017
    iput-object v1, p0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1332018
    iput-wide v6, p0, LX/8LQ;->t:J

    .line 1332019
    iput-boolean v2, p0, LX/8LQ;->u:Z

    .line 1332020
    iput-object v1, p0, LX/8LQ;->v:Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1332021
    iput-object v1, p0, LX/8LQ;->w:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1332022
    iput-wide v4, p0, LX/8LQ;->x:J

    .line 1332023
    iput-object v1, p0, LX/8LQ;->y:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1332024
    iput-boolean v2, p0, LX/8LQ;->z:Z

    .line 1332025
    iput-boolean v2, p0, LX/8LQ;->A:Z

    .line 1332026
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    iput-object v0, p0, LX/8LQ;->B:LX/5Rn;

    .line 1332027
    iput-wide v4, p0, LX/8LQ;->C:J

    .line 1332028
    iput-object v1, p0, LX/8LQ;->D:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 1332029
    iput-object v1, p0, LX/8LQ;->E:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1332030
    iput-wide v4, p0, LX/8LQ;->F:J

    .line 1332031
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8LQ;->G:Ljava/util/List;

    .line 1332032
    iput-boolean v2, p0, LX/8LQ;->H:Z

    .line 1332033
    iput-wide v4, p0, LX/8LQ;->K:J

    .line 1332034
    iput-boolean v2, p0, LX/8LQ;->N:Z

    .line 1332035
    iput v2, p0, LX/8LQ;->O:I

    .line 1332036
    iput v3, p0, LX/8LQ;->W:I

    .line 1332037
    iput v3, p0, LX/8LQ;->X:I

    .line 1332038
    iput v3, p0, LX/8LQ;->Y:I

    .line 1332039
    iput-boolean v2, p0, LX/8LQ;->ah:Z

    .line 1332040
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, -0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1331855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331856
    iput-object v1, p0, LX/8LQ;->a:Ljava/lang/String;

    .line 1331857
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1331858
    iput-object v0, p0, LX/8LQ;->b:LX/0Px;

    .line 1331859
    iput-object v1, p0, LX/8LQ;->c:LX/0Px;

    .line 1331860
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1331861
    iput-object v0, p0, LX/8LQ;->d:LX/0Px;

    .line 1331862
    iput-object v1, p0, LX/8LQ;->e:Ljava/lang/String;

    .line 1331863
    sget-object v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, LX/8LQ;->f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1331864
    iput-object v1, p0, LX/8LQ;->g:Ljava/lang/String;

    .line 1331865
    iput-wide v4, p0, LX/8LQ;->h:J

    .line 1331866
    iput-object v1, p0, LX/8LQ;->i:Ljava/lang/String;

    .line 1331867
    iput-wide v6, p0, LX/8LQ;->j:J

    .line 1331868
    iput-object v1, p0, LX/8LQ;->k:Ljava/lang/String;

    .line 1331869
    iput-object v1, p0, LX/8LQ;->l:Ljava/lang/String;

    .line 1331870
    iput-boolean v2, p0, LX/8LQ;->m:Z

    .line 1331871
    iput-boolean v2, p0, LX/8LQ;->n:Z

    .line 1331872
    iput-object v1, p0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1331873
    iput-object v1, p0, LX/8LQ;->p:LX/8LR;

    .line 1331874
    iput-object v1, p0, LX/8LQ;->q:LX/8LS;

    .line 1331875
    iput-object v1, p0, LX/8LQ;->r:Ljava/lang/String;

    .line 1331876
    iput-object v1, p0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1331877
    iput-wide v6, p0, LX/8LQ;->t:J

    .line 1331878
    iput-boolean v2, p0, LX/8LQ;->u:Z

    .line 1331879
    iput-object v1, p0, LX/8LQ;->v:Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1331880
    iput-object v1, p0, LX/8LQ;->w:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1331881
    iput-wide v4, p0, LX/8LQ;->x:J

    .line 1331882
    iput-object v1, p0, LX/8LQ;->y:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1331883
    iput-boolean v2, p0, LX/8LQ;->z:Z

    .line 1331884
    iput-boolean v2, p0, LX/8LQ;->A:Z

    .line 1331885
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    iput-object v0, p0, LX/8LQ;->B:LX/5Rn;

    .line 1331886
    iput-wide v4, p0, LX/8LQ;->C:J

    .line 1331887
    iput-object v1, p0, LX/8LQ;->D:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 1331888
    iput-object v1, p0, LX/8LQ;->E:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1331889
    iput-wide v4, p0, LX/8LQ;->F:J

    .line 1331890
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8LQ;->G:Ljava/util/List;

    .line 1331891
    iput-boolean v2, p0, LX/8LQ;->H:Z

    .line 1331892
    iput-wide v4, p0, LX/8LQ;->K:J

    .line 1331893
    iput-boolean v2, p0, LX/8LQ;->N:Z

    .line 1331894
    iput v2, p0, LX/8LQ;->O:I

    .line 1331895
    iput v3, p0, LX/8LQ;->W:I

    .line 1331896
    iput v3, p0, LX/8LQ;->X:I

    .line 1331897
    iput v3, p0, LX/8LQ;->Y:I

    .line 1331898
    iput-boolean v2, p0, LX/8LQ;->ah:Z

    .line 1331899
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->a:Ljava/lang/String;

    .line 1331900
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    iput-object v0, p0, LX/8LQ;->b:LX/0Px;

    .line 1331901
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    iput-object v0, p0, LX/8LQ;->c:LX/0Px;

    .line 1331902
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    iput-object v0, p0, LX/8LQ;->d:LX/0Px;

    .line 1331903
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->e:Ljava/lang/String;

    .line 1331904
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->e:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, LX/8LQ;->f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1331905
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->f:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->g:Ljava/lang/String;

    .line 1331906
    iget-wide v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    iput-wide v0, p0, LX/8LQ;->h:J

    .line 1331907
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->i:Ljava/lang/String;

    .line 1331908
    iget-wide v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    iput-wide v0, p0, LX/8LQ;->j:J

    .line 1331909
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->k:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->k:Ljava/lang/String;

    .line 1331910
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->l:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->l:Ljava/lang/String;

    .line 1331911
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->m:Z

    iput-boolean v0, p0, LX/8LQ;->m:Z

    .line 1331912
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->n:Z

    iput-boolean v0, p0, LX/8LQ;->n:Z

    .line 1331913
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iput-object v0, p0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1331914
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    iput-object v0, p0, LX/8LQ;->p:LX/8LR;

    .line 1331915
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    iput-object v0, p0, LX/8LQ;->q:LX/8LS;

    .line 1331916
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->s:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->r:Ljava/lang/String;

    .line 1331917
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1331918
    iget-wide v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->u:J

    iput-wide v0, p0, LX/8LQ;->t:J

    .line 1331919
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->v:Z

    iput-boolean v0, p0, LX/8LQ;->u:Z

    .line 1331920
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->w:Lcom/facebook/composer/protocol/PostReviewParams;

    iput-object v0, p0, LX/8LQ;->v:Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1331921
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    iput-object v0, p0, LX/8LQ;->w:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1331922
    iget-wide v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->y:J

    iput-wide v0, p0, LX/8LQ;->x:J

    .line 1331923
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, LX/8LQ;->y:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1331924
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->C:Z

    iput-boolean v0, p0, LX/8LQ;->z:Z

    .line 1331925
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->D:Z

    iput-boolean v0, p0, LX/8LQ;->A:Z

    .line 1331926
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->z:LX/5Rn;

    iput-object v0, p0, LX/8LQ;->B:LX/5Rn;

    .line 1331927
    iget-wide v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->A:J

    iput-wide v0, p0, LX/8LQ;->C:J

    .line 1331928
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->J:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    iput-object v0, p0, LX/8LQ;->D:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 1331929
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, LX/8LQ;->E:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1331930
    iget-wide v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->L:J

    iput-wide v0, p0, LX/8LQ;->F:J

    .line 1331931
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->M:Ljava/util/List;

    iput-object v0, p0, LX/8LQ;->G:Ljava/util/List;

    .line 1331932
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    iput-boolean v0, p0, LX/8LQ;->H:Z

    .line 1331933
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->I:Ljava/lang/String;

    .line 1331934
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ae:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->J:Ljava/lang/String;

    .line 1331935
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ag:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->L:Ljava/lang/String;

    .line 1331936
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ah:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->M:Ljava/lang/String;

    .line 1331937
    iget v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->X:I

    iput v0, p0, LX/8LQ;->O:I

    .line 1331938
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->aj:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    iput-object v0, p0, LX/8LQ;->P:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1331939
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->Q:Ljava/lang/String;

    .line 1331940
    iget v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->O:I

    iput v0, p0, LX/8LQ;->R:I

    .line 1331941
    iget v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->P:I

    iput v0, p0, LX/8LQ;->S:I

    .line 1331942
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ak:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->T:Ljava/lang/String;

    .line 1331943
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->al:Lcom/facebook/composer/publish/common/EditPostParams;

    iput-object v0, p0, LX/8LQ;->U:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1331944
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    iput-object v0, p0, LX/8LQ;->V:Lcom/facebook/audience/model/UploadShot;

    .line 1331945
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ac:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->Z:Ljava/lang/String;

    .line 1331946
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->an:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, LX/8LQ;->aa:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1331947
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ao:LX/0Px;

    iput-object v0, p0, LX/8LQ;->ab:LX/0Px;

    .line 1331948
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ap:Z

    iput-boolean v0, p0, LX/8LQ;->ac:Z

    .line 1331949
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->aq:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, LX/8LQ;->ad:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1331950
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ar:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->ae:Ljava/lang/String;

    .line 1331951
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->as:Z

    iput-boolean v0, p0, LX/8LQ;->af:Z

    .line 1331952
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->at:LX/5Ra;

    iput-object v0, p0, LX/8LQ;->ag:LX/5Ra;

    .line 1331953
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    iput-boolean v0, p0, LX/8LQ;->ah:Z

    .line 1331954
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->av:Ljava/lang/String;

    iput-object v0, p0, LX/8LQ;->ai:Ljava/lang/String;

    .line 1331955
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->aw:Z

    iput-boolean v0, p0, LX/8LQ;->aj:Z

    .line 1331956
    return-void
.end method


# virtual methods
.method public final a(I)LX/8LQ;
    .locals 0

    .prologue
    .line 1331957
    iput p1, p0, LX/8LQ;->R:I

    .line 1331958
    return-object p0
.end method

.method public final a(J)LX/8LQ;
    .locals 1

    .prologue
    .line 1331959
    iput-wide p1, p0, LX/8LQ;->h:J

    .line 1331960
    return-object p0
.end method

.method public final a(LX/0Px;)LX/8LQ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "LX/8LQ;"
        }
    .end annotation

    .prologue
    .line 1331961
    iput-object p1, p0, LX/8LQ;->b:LX/0Px;

    .line 1331962
    return-object p0
.end method

.method public final a(LX/5Ra;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331963
    iput-object p1, p0, LX/8LQ;->ag:LX/5Ra;

    .line 1331964
    return-object p0
.end method

.method public final a(LX/5Rn;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331965
    iput-object p1, p0, LX/8LQ;->B:LX/5Rn;

    .line 1331966
    return-object p0
.end method

.method public final a(LX/8LR;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331967
    iput-object p1, p0, LX/8LQ;->p:LX/8LR;

    .line 1331968
    return-object p0
.end method

.method public final a(LX/8LS;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331969
    iput-object p1, p0, LX/8LQ;->q:LX/8LS;

    .line 1331970
    return-object p0
.end method

.method public final a(Lcom/facebook/audience/model/UploadShot;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331971
    iput-object p1, p0, LX/8LQ;->V:Lcom/facebook/audience/model/UploadShot;

    .line 1331972
    return-object p0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331973
    iput-object p1, p0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1331974
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331975
    iput-object p1, p0, LX/8LQ;->ad:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1331976
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331977
    iput-object p1, p0, LX/8LQ;->D:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 1331978
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;
    .locals 1

    .prologue
    .line 1331979
    iget-object v0, p0, LX/8LQ;->f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    if-nez v0, :cond_0

    .line 1331980
    sget-object v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, LX/8LQ;->f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1331981
    :goto_0
    return-object p0

    .line 1331982
    :cond_0
    iput-object p1, p0, LX/8LQ;->f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331983
    iput-object p1, p0, LX/8LQ;->E:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1331984
    return-object p0
.end method

.method public final a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331985
    iput-object p1, p0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1331986
    return-object p0
.end method

.method public final a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/8LQ;
    .locals 0
    .param p1    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1331987
    iput-object p1, p0, LX/8LQ;->aa:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1331988
    return-object p0
.end method

.method public final a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331989
    iput-object p1, p0, LX/8LQ;->y:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1331990
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331991
    iput-object p1, p0, LX/8LQ;->a:Ljava/lang/String;

    .line 1331992
    return-object p0
.end method

.method public final a(Z)LX/8LQ;
    .locals 0

    .prologue
    .line 1331993
    iput-boolean p1, p0, LX/8LQ;->m:Z

    .line 1331994
    return-object p0
.end method

.method public final a()Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 74

    .prologue
    .line 1331995
    new-instance v3, Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8LQ;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8LQ;->b:LX/0Px;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8LQ;->c:LX/0Px;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/8LQ;->d:LX/0Px;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/8LQ;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/8LQ;->f:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/8LQ;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v11, v0, LX/8LQ;->h:J

    move-object/from16 v0, p0

    iget-object v13, v0, LX/8LQ;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/8LQ;->j:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->k:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->l:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->m:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->n:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->p:LX/8LR;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->q:LX/8LS;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->r:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8LQ;->t:J

    move-wide/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->u:Z

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->v:Lcom/facebook/composer/protocol/PostReviewParams;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->w:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8LQ;->x:J

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->y:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->z:Z

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->A:Z

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->B:LX/5Rn;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8LQ;->C:J

    move-wide/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->D:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->E:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8LQ;->F:J

    move-wide/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->G:Ljava/util/List;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->H:Z

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->I:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->J:Ljava/lang/String;

    move-object/from16 v45, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8LQ;->K:J

    move-wide/from16 v46, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->L:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->M:Ljava/lang/String;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->N:Z

    move/from16 v50, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8LQ;->O:I

    move/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->P:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->Q:Ljava/lang/String;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8LQ;->R:I

    move/from16 v54, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8LQ;->S:I

    move/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->T:Ljava/lang/String;

    move-object/from16 v56, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->U:Lcom/facebook/composer/publish/common/EditPostParams;

    move-object/from16 v57, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->V:Lcom/facebook/audience/model/UploadShot;

    move-object/from16 v58, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8LQ;->W:I

    move/from16 v59, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8LQ;->X:I

    move/from16 v60, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8LQ;->Y:I

    move/from16 v61, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->Z:Ljava/lang/String;

    move-object/from16 v62, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->aa:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object/from16 v63, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->ab:LX/0Px;

    move-object/from16 v64, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->ac:Z

    move/from16 v65, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->ad:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object/from16 v66, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->ae:Ljava/lang/String;

    move-object/from16 v67, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->af:Z

    move/from16 v68, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->ag:LX/5Ra;

    move-object/from16 v69, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->ah:Z

    move/from16 v70, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8LQ;->ai:Ljava/lang/String;

    move-object/from16 v71, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8LQ;->aj:Z

    move/from16 v72, v0

    const/16 v73, 0x0

    invoke-direct/range {v3 .. v73}, Lcom/facebook/photos/upload/operation/UploadOperation;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZLcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/8LR;LX/8LS;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;JZLcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/util/List;ZLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZILcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;Lcom/facebook/audience/model/UploadShot;IIILjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;ZLX/5Ra;ZLjava/lang/String;ZLX/8LP;)V

    return-object v3
.end method

.method public final b(I)LX/8LQ;
    .locals 0

    .prologue
    .line 1332041
    iput p1, p0, LX/8LQ;->S:I

    .line 1332042
    return-object p0
.end method

.method public final b(J)LX/8LQ;
    .locals 1

    .prologue
    .line 1332043
    iput-wide p1, p0, LX/8LQ;->j:J

    .line 1332044
    return-object p0
.end method

.method public final b(LX/0Px;)LX/8LQ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;)",
            "LX/8LQ;"
        }
    .end annotation

    .prologue
    .line 1332045
    iput-object p1, p0, LX/8LQ;->c:LX/0Px;

    .line 1332046
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331851
    iput-object p1, p0, LX/8LQ;->Q:Ljava/lang/String;

    .line 1331852
    return-object p0
.end method

.method public final b(Z)LX/8LQ;
    .locals 0

    .prologue
    .line 1331853
    iput-boolean p1, p0, LX/8LQ;->n:Z

    .line 1331854
    return-object p0
.end method

.method public final c(I)LX/8LQ;
    .locals 0

    .prologue
    .line 1331825
    iput p1, p0, LX/8LQ;->W:I

    .line 1331826
    return-object p0
.end method

.method public final c(J)LX/8LQ;
    .locals 1

    .prologue
    .line 1331821
    iput-wide p1, p0, LX/8LQ;->t:J

    .line 1331822
    return-object p0
.end method

.method public final c(LX/0Px;)LX/8LQ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/8LQ;"
        }
    .end annotation

    .prologue
    .line 1331819
    iput-object p1, p0, LX/8LQ;->d:LX/0Px;

    .line 1331820
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331817
    iput-object p1, p0, LX/8LQ;->e:Ljava/lang/String;

    .line 1331818
    return-object p0
.end method

.method public final d(I)LX/8LQ;
    .locals 0

    .prologue
    .line 1331823
    iput p1, p0, LX/8LQ;->Y:I

    .line 1331824
    return-object p0
.end method

.method public final d(J)LX/8LQ;
    .locals 1

    .prologue
    .line 1331815
    iput-wide p1, p0, LX/8LQ;->x:J

    .line 1331816
    return-object p0
.end method

.method public final d(LX/0Px;)LX/8LQ;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;)",
            "LX/8LQ;"
        }
    .end annotation

    .prologue
    .line 1331813
    iput-object p1, p0, LX/8LQ;->ab:LX/0Px;

    .line 1331814
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331811
    iput-object p1, p0, LX/8LQ;->g:Ljava/lang/String;

    .line 1331812
    return-object p0
.end method

.method public final d(Z)LX/8LQ;
    .locals 0

    .prologue
    .line 1331809
    iput-boolean p1, p0, LX/8LQ;->z:Z

    .line 1331810
    return-object p0
.end method

.method public final e(I)LX/8LQ;
    .locals 1

    .prologue
    .line 1331806
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-gt p1, v0, :cond_0

    .line 1331807
    iput p1, p0, LX/8LQ;->O:I

    .line 1331808
    :cond_0
    return-object p0
.end method

.method public final e(J)LX/8LQ;
    .locals 1

    .prologue
    .line 1331804
    iput-wide p1, p0, LX/8LQ;->C:J

    .line 1331805
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331802
    iput-object p1, p0, LX/8LQ;->i:Ljava/lang/String;

    .line 1331803
    return-object p0
.end method

.method public final e(Z)LX/8LQ;
    .locals 0

    .prologue
    .line 1331800
    iput-boolean p1, p0, LX/8LQ;->A:Z

    .line 1331801
    return-object p0
.end method

.method public final f(J)LX/8LQ;
    .locals 1

    .prologue
    .line 1331827
    iput-wide p1, p0, LX/8LQ;->F:J

    .line 1331828
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331829
    iput-object p1, p0, LX/8LQ;->k:Ljava/lang/String;

    .line 1331830
    return-object p0
.end method

.method public final f(Z)LX/8LQ;
    .locals 0

    .prologue
    .line 1331831
    iput-boolean p1, p0, LX/8LQ;->N:Z

    .line 1331832
    return-object p0
.end method

.method public final g(J)LX/8LQ;
    .locals 1

    .prologue
    .line 1331833
    iput-wide p1, p0, LX/8LQ;->K:J

    .line 1331834
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331835
    iput-object p1, p0, LX/8LQ;->l:Ljava/lang/String;

    .line 1331836
    return-object p0
.end method

.method public final g(Z)LX/8LQ;
    .locals 0

    .prologue
    .line 1331837
    iput-boolean p1, p0, LX/8LQ;->ac:Z

    .line 1331838
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331839
    iput-object p1, p0, LX/8LQ;->r:Ljava/lang/String;

    .line 1331840
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331841
    iput-object p1, p0, LX/8LQ;->I:Ljava/lang/String;

    .line 1331842
    return-object p0
.end method

.method public final i(Z)LX/8LQ;
    .locals 0

    .prologue
    .line 1331843
    iput-boolean p1, p0, LX/8LQ;->aj:Z

    .line 1331844
    return-object p0
.end method

.method public final j(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331845
    iput-object p1, p0, LX/8LQ;->J:Ljava/lang/String;

    .line 1331846
    return-object p0
.end method

.method public final k(Ljava/lang/String;)LX/8LQ;
    .locals 0

    .prologue
    .line 1331847
    iput-object p1, p0, LX/8LQ;->L:Ljava/lang/String;

    .line 1331848
    return-object p0
.end method

.method public final l(Ljava/lang/String;)LX/8LQ;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1331849
    iput-object p1, p0, LX/8LQ;->M:Ljava/lang/String;

    .line 1331850
    return-object p0
.end method
