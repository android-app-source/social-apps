.class public LX/75B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169033
    return-void
.end method

.method public static a(FFFFLandroid/graphics/RectF;)D
    .locals 14

    .prologue
    .line 1169034
    sub-float v4, p2, p0

    .line 1169035
    sub-float v5, p3, p1

    .line 1169036
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/RectF;->right:F

    cmpg-float v2, p0, v2

    if-gez v2, :cond_0

    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v2, p0, v2

    if-lez v2, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1169037
    :goto_0
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, p1

    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1169038
    :goto_1
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/RectF;->height()F

    move-result v6

    .line 1169039
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 1169040
    float-to-double v8, v2

    div-float v2, v4, v7

    float-to-double v10, v2

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    mul-double/2addr v8, v10

    float-to-double v2, v3

    div-float v4, v5, v6

    float-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    return-wide v2

    .line 1169041
    :cond_0
    const/high16 v2, 0x40c00000    # 6.0f

    goto :goto_0

    .line 1169042
    :cond_1
    const/high16 v3, 0x41100000    # 9.0f

    goto :goto_1
.end method

.method public static final a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/RectF;)D
    .locals 4

    .prologue
    .line 1169043
    iget v0, p0, Landroid/graphics/PointF;->x:F

    iget v1, p0, Landroid/graphics/PointF;->y:F

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-static {v0, v1, v2, v3, p2}, LX/75B;->a(FFFFLandroid/graphics/RectF;)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/75B;
    .locals 1

    .prologue
    .line 1169044
    new-instance v0, LX/75B;

    invoke-direct {v0}, LX/75B;-><init>()V

    .line 1169045
    move-object v0, v0

    .line 1169046
    return-object v0
.end method
