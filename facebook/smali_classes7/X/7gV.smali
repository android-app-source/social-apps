.class public final enum LX/7gV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gV;

.field public static final enum DIRECT:LX/7gV;

.field public static final enum SNACKS:LX/7gV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1224444
    new-instance v0, LX/7gV;

    const-string v1, "DIRECT"

    invoke-direct {v0, v1, v2}, LX/7gV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gV;->DIRECT:LX/7gV;

    .line 1224445
    new-instance v0, LX/7gV;

    const-string v1, "SNACKS"

    invoke-direct {v0, v1, v3}, LX/7gV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gV;->SNACKS:LX/7gV;

    .line 1224446
    const/4 v0, 0x2

    new-array v0, v0, [LX/7gV;

    sget-object v1, LX/7gV;->DIRECT:LX/7gV;

    aput-object v1, v0, v2

    sget-object v1, LX/7gV;->SNACKS:LX/7gV;

    aput-object v1, v0, v3

    sput-object v0, LX/7gV;->$VALUES:[LX/7gV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1224447
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gV;
    .locals 1

    .prologue
    .line 1224448
    const-class v0, LX/7gV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gV;

    return-object v0
.end method

.method public static values()[LX/7gV;
    .locals 1

    .prologue
    .line 1224449
    sget-object v0, LX/7gV;->$VALUES:[LX/7gV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gV;

    return-object v0
.end method
