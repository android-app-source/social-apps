.class public final LX/6zq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# instance fields
.field private final a:LX/712;


# direct methods
.method public constructor <init>(LX/712;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1161484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161485
    iput-object p1, p0, LX/6zq;->a:LX/712;

    .line 1161486
    return-void
.end method

.method public static b(LX/0QB;)LX/6zq;
    .locals 2

    .prologue
    .line 1161487
    new-instance v1, LX/6zq;

    invoke-static {p0}, LX/712;->a(LX/0QB;)LX/712;

    move-result-object v0

    check-cast v0, LX/712;

    invoke-direct {v1, v0}, LX/6zq;-><init>(LX/712;)V

    .line 1161488
    return-object v1
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1161489
    sget-object v0, LX/6zp;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1161490
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal row type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1161491
    :pswitch_0
    check-cast p2, LX/700;

    .line 1161492
    if-nez p3, :cond_0

    new-instance p3, LX/6zw;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/6zw;-><init>(Landroid/content/Context;)V

    .line 1161493
    :goto_0
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1161494
    iput-object p2, p3, LX/6zw;->d:LX/700;

    .line 1161495
    iget-object v0, p3, LX/6zw;->d:LX/700;

    iget-boolean v0, v0, LX/700;->b:Z

    if-eqz v0, :cond_1

    .line 1161496
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/6zw;->setOrientation(I)V

    .line 1161497
    :goto_1
    iget-object v0, p3, LX/6zw;->b:Landroid/widget/TextView;

    iget-object p4, p3, LX/6zw;->d:LX/700;

    iget-object p4, p4, LX/700;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {p4}, Lcom/facebook/common/locale/Country;->a()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1161498
    new-instance v0, LX/6zo;

    invoke-direct {v0, p0, p2, p1}, LX/6zo;-><init>(LX/6zq;LX/700;LX/6qh;)V

    .line 1161499
    iget-object p0, p3, LX/6zw;->c:LX/7Tj;

    new-instance p1, LX/6zu;

    invoke-direct {p1, p3, v0}, LX/6zu;-><init>(LX/6zw;LX/6zn;)V

    .line 1161500
    iput-object p1, p0, LX/7Tj;->u:LX/6u6;

    .line 1161501
    new-instance p0, LX/6zv;

    invoke-direct {p0, p3}, LX/6zv;-><init>(LX/6zw;)V

    invoke-virtual {p3, p0}, LX/6zw;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1161502
    move-object v0, p3

    .line 1161503
    :goto_2
    return-object v0

    .line 1161504
    :pswitch_1
    check-cast p2, LX/701;

    .line 1161505
    if-nez p3, :cond_2

    new-instance p3, LX/6zc;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/6zc;-><init>(Landroid/content/Context;)V

    .line 1161506
    :goto_3
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1161507
    iput-object p2, p3, LX/6zc;->b:LX/701;

    .line 1161508
    iget-object v0, p3, LX/6zc;->c:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    iget-object p1, p3, LX/6zc;->b:LX/701;

    iget-object p1, p1, LX/701;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setPaymentMethod(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 1161509
    iget-object p1, p3, LX/6zc;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v0, p3, LX/6zc;->b:LX/701;

    iget-boolean v0, v0, LX/701;->b:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1161510
    move-object v0, p3

    .line 1161511
    goto :goto_2

    .line 1161512
    :pswitch_2
    check-cast p2, LX/6zy;

    .line 1161513
    if-nez p3, :cond_4

    new-instance p3, LX/6zY;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/6zY;-><init>(Landroid/content/Context;)V

    .line 1161514
    :goto_5
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 1161515
    iget-object v0, p3, LX/6zY;->a:Landroid/widget/TextView;

    new-instance p1, LX/6zX;

    invoke-direct {p1, p3, p2}, LX/6zX;-><init>(LX/6zY;LX/6zy;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1161516
    move-object v0, p3

    .line 1161517
    goto :goto_2

    .line 1161518
    :pswitch_3
    check-cast p2, LX/6zz;

    .line 1161519
    if-nez p3, :cond_5

    new-instance p3, LX/6za;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/6za;-><init>(Landroid/content/Context;)V

    .line 1161520
    :goto_6
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 1161521
    iget-object v0, p3, LX/6za;->a:Landroid/widget/TextView;

    new-instance p1, LX/6zZ;

    invoke-direct {p1, p3, p2}, LX/6zZ;-><init>(LX/6za;LX/6zz;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1161522
    move-object v0, p3

    .line 1161523
    goto :goto_2

    .line 1161524
    :pswitch_4
    iget-object v0, p0, LX/6zq;->a:LX/712;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/712;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 1161525
    :cond_0
    check-cast p3, LX/6zw;

    goto/16 :goto_0

    .line 1161526
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, LX/6zw;->setOrientation(I)V

    .line 1161527
    iget-object v0, p3, LX/6zw;->b:Landroid/widget/TextView;

    const p4, 0x800005

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setGravity(I)V

    goto/16 :goto_1

    .line 1161528
    :cond_2
    check-cast p3, LX/6zc;

    goto :goto_3

    .line 1161529
    :cond_3
    const/16 v0, 0x8

    goto :goto_4

    .line 1161530
    :cond_4
    check-cast p3, LX/6zY;

    goto :goto_5

    .line 1161531
    :cond_5
    check-cast p3, LX/6za;

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
