.class public LX/7Hc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/7Hc;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1190883
    new-instance v0, LX/7Hc;

    .line 1190884
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1190885
    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    sput-object v0, LX/7Hc;->a:LX/7Hc;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190881
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, LX/7Hc;-><init>(LX/0Px;II)V

    .line 1190882
    return-void
.end method

.method public constructor <init>(LX/0Px;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 1190879
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, LX/7Hc;-><init>(LX/0Px;II)V

    .line 1190880
    return-void
.end method

.method public constructor <init>(LX/0Px;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    .line 1190874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190875
    iput-object p1, p0, LX/7Hc;->b:LX/0Px;

    .line 1190876
    iput p2, p0, LX/7Hc;->c:I

    .line 1190877
    iput p3, p0, LX/7Hc;->d:I

    .line 1190878
    return-void
.end method

.method public static a(LX/1pN;)I
    .locals 6

    .prologue
    .line 1190886
    iget-object v0, p0, LX/1pN;->c:LX/0Px;

    move-object v2, v0

    .line 1190887
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 1190888
    const-string v4, "X-FB-ForkingPoolID"

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1190889
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1190890
    :goto_1
    return v0

    .line 1190891
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1190892
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a(LX/7Hc;LX/0Px;)LX/7Hc;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/7Hc",
            "<TT;>;",
            "LX/0Px",
            "<TT;>;)",
            "LX/7Hc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1190873
    new-instance v0, LX/7Hc;

    iget v1, p0, LX/7Hc;->c:I

    iget v2, p0, LX/7Hc;->d:I

    invoke-direct {v0, p1, v1, v2}, LX/7Hc;-><init>(LX/0Px;II)V

    return-object v0
.end method


# virtual methods
.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1190872
    iget-object v0, p0, LX/7Hc;->b:LX/0Px;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1190871
    iget v0, p0, LX/7Hc;->c:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1190864
    instance-of v0, p1, LX/7Hc;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/7Hc;

    .line 1190865
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 1190866
    iget-object v1, p0, LX/7Hc;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/7Hc;

    .line 1190867
    iget v1, v0, LX/7Hc;->c:I

    move v0, v1

    .line 1190868
    iget v1, p0, LX/7Hc;->c:I

    if-ne v0, v1, :cond_0

    check-cast p1, LX/7Hc;

    .line 1190869
    iget v0, p1, LX/7Hc;->d:I

    move v0, v0

    .line 1190870
    iget v1, p0, LX/7Hc;->d:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1190857
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1190858
    iget-object v2, p0, LX/7Hc;->b:LX/0Px;

    move-object v2, v2

    .line 1190859
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 1190860
    iget v2, p0, LX/7Hc;->c:I

    move v2, v2

    .line 1190861
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 1190862
    iget v2, p0, LX/7Hc;->d:I

    move v2, v2

    .line 1190863
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
