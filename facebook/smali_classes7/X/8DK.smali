.class public final enum LX/8DK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8DK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8DK;

.field public static final enum COMPLETE:LX/8DK;

.field public static final enum SKIPPED:LX/8DK;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1312489
    new-instance v0, LX/8DK;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v2}, LX/8DK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8DK;->COMPLETE:LX/8DK;

    .line 1312490
    new-instance v0, LX/8DK;

    const-string v1, "SKIPPED"

    invoke-direct {v0, v1, v3}, LX/8DK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8DK;->SKIPPED:LX/8DK;

    .line 1312491
    const/4 v0, 0x2

    new-array v0, v0, [LX/8DK;

    sget-object v1, LX/8DK;->COMPLETE:LX/8DK;

    aput-object v1, v0, v2

    sget-object v1, LX/8DK;->SKIPPED:LX/8DK;

    aput-object v1, v0, v3

    sput-object v0, LX/8DK;->$VALUES:[LX/8DK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1312486
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8DK;
    .locals 1

    .prologue
    .line 1312488
    const-class v0, LX/8DK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8DK;

    return-object v0
.end method

.method public static values()[LX/8DK;
    .locals 1

    .prologue
    .line 1312487
    sget-object v0, LX/8DK;->$VALUES:[LX/8DK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8DK;

    return-object v0
.end method
