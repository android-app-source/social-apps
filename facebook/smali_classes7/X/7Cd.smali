.class public LX/7Cd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1181177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(F)LX/5Pg;
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/high16 v13, 0x40000000    # 2.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f000000    # 0.5f

    const v10, 0x3eaaaaab

    .line 1181178
    const/16 v0, 0x30

    new-array v2, v0, [F

    .line 1181179
    cmpl-float v0, p0, v1

    if-lez v0, :cond_0

    .line 1181180
    div-float v0, p0, v13

    .line 1181181
    float-to-double v4, v0

    const/high16 v3, 0x42000000    # 32.0f

    div-float/2addr v0, v3

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x4040000000000000L    # 32.0

    mul-double/2addr v6, v8

    div-double/2addr v4, v6

    double-to-float v0, v4

    .line 1181182
    sub-float/2addr v0, v12

    div-float/2addr v0, v13

    .line 1181183
    :goto_0
    const/4 v3, 0x0

    add-float v4, v1, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181184
    const/4 v3, 0x1

    sub-float v4, v12, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181185
    const/4 v3, 0x2

    add-float v4, v1, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181186
    const/4 v3, 0x3

    add-float v4, v1, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181187
    const/4 v3, 0x4

    sub-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181188
    const/4 v3, 0x5

    add-float v4, v1, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181189
    const/4 v3, 0x6

    sub-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181190
    const/4 v3, 0x7

    sub-float v4, v12, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181191
    const/16 v3, 0x8

    add-float v4, v1, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181192
    const/16 v3, 0x9

    const/high16 v4, 0x40400000    # 3.0f

    sub-float/2addr v4, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181193
    const/16 v3, 0xa

    add-float v4, v1, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181194
    const/16 v3, 0xb

    add-float v4, v13, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181195
    const/16 v3, 0xc

    sub-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181196
    const/16 v3, 0xd

    add-float v4, v13, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181197
    const/16 v3, 0xe

    sub-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181198
    const/16 v3, 0xf

    const/high16 v4, 0x40400000    # 3.0f

    sub-float/2addr v4, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181199
    const/16 v3, 0x10

    sub-float v4, v13, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181200
    const/16 v3, 0x11

    const/high16 v4, 0x40400000    # 3.0f

    sub-float/2addr v4, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181201
    const/16 v3, 0x12

    add-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181202
    const/16 v3, 0x13

    const/high16 v4, 0x40400000    # 3.0f

    sub-float/2addr v4, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181203
    const/16 v3, 0x14

    add-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181204
    const/16 v3, 0x15

    add-float v4, v13, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181205
    const/16 v3, 0x16

    sub-float v4, v13, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181206
    const/16 v3, 0x17

    add-float v4, v13, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181207
    const/16 v3, 0x18

    sub-float v4, v13, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181208
    const/16 v3, 0x19

    sub-float v4, v12, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181209
    const/16 v3, 0x1a

    add-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181210
    const/16 v3, 0x1b

    sub-float v4, v12, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181211
    const/16 v3, 0x1c

    add-float v4, v12, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181212
    const/16 v3, 0x1d

    add-float v4, v1, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181213
    const/16 v3, 0x1e

    sub-float v4, v13, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181214
    const/16 v3, 0x1f

    add-float v4, v1, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181215
    const/16 v3, 0x20

    add-float v4, v1, v0

    mul-float/2addr v4, v11

    aput v4, v2, v3

    .line 1181216
    const/16 v3, 0x21

    sub-float v4, v13, v0

    mul-float/2addr v4, v10

    aput v4, v2, v3

    .line 1181217
    const/16 v3, 0x22

    add-float/2addr v1, v0

    mul-float/2addr v1, v11

    aput v1, v2, v3

    .line 1181218
    const/16 v1, 0x23

    add-float v3, v12, v0

    mul-float/2addr v3, v10

    aput v3, v2, v1

    .line 1181219
    const/16 v1, 0x24

    sub-float v3, v12, v0

    mul-float/2addr v3, v11

    aput v3, v2, v1

    .line 1181220
    const/16 v1, 0x25

    add-float v3, v12, v0

    mul-float/2addr v3, v10

    aput v3, v2, v1

    .line 1181221
    const/16 v1, 0x26

    sub-float v3, v12, v0

    mul-float/2addr v3, v11

    aput v3, v2, v1

    .line 1181222
    const/16 v1, 0x27

    sub-float v3, v13, v0

    mul-float/2addr v3, v10

    aput v3, v2, v1

    .line 1181223
    const/16 v1, 0x28

    add-float v3, v12, v0

    mul-float/2addr v3, v11

    aput v3, v2, v1

    .line 1181224
    const/16 v1, 0x29

    add-float v3, v12, v0

    mul-float/2addr v3, v10

    aput v3, v2, v1

    .line 1181225
    const/16 v1, 0x2a

    sub-float v3, v13, v0

    mul-float/2addr v3, v11

    aput v3, v2, v1

    .line 1181226
    const/16 v1, 0x2b

    add-float v3, v12, v0

    mul-float/2addr v3, v10

    aput v3, v2, v1

    .line 1181227
    const/16 v1, 0x2c

    sub-float v3, v13, v0

    mul-float/2addr v3, v11

    aput v3, v2, v1

    .line 1181228
    const/16 v1, 0x2d

    sub-float v3, v13, v0

    mul-float/2addr v3, v10

    aput v3, v2, v1

    .line 1181229
    const/16 v1, 0x2e

    add-float v3, v12, v0

    mul-float/2addr v3, v11

    aput v3, v2, v1

    .line 1181230
    const/16 v1, 0x2f

    sub-float v0, v13, v0

    mul-float/2addr v0, v10

    aput v0, v2, v1

    .line 1181231
    new-instance v0, LX/5Pg;

    const/4 v1, 0x2

    invoke-direct {v0, v2, v1}, LX/5Pg;-><init>([FI)V

    return-object v0

    :cond_0
    move v0, v1

    .line 1181232
    goto/16 :goto_0
.end method
