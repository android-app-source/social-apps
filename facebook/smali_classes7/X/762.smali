.class public LX/762;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private final mBuffer:LX/161;

.field private mClosed:Z

.field private mNetworkException:LX/5oc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/161;)V
    .locals 0

    .prologue
    .line 1170502
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 1170503
    iput-object p1, p0, LX/762;->mBuffer:LX/161;

    .line 1170504
    return-void
.end method

.method private checkNotClosed()V
    .locals 2

    .prologue
    .line 1170499
    iget-boolean v0, p0, LX/762;->mClosed:Z

    if-eqz v0, :cond_0

    .line 1170500
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Buffer is Closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1170501
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized available()I
    .locals 1

    .prologue
    .line 1170498
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/762;->mBuffer:LX/161;

    invoke-interface {v0}, LX/161;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 1170463
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/762;->mClosed:Z

    if-nez v0, :cond_0

    .line 1170464
    iget-object v0, p0, LX/762;->mBuffer:LX/161;

    invoke-interface {v0}, LX/161;->close()V

    .line 1170465
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/762;->mClosed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170466
    :cond_0
    monitor-exit p0

    return-void

    .line 1170467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public mark(I)V
    .locals 1

    .prologue
    .line 1170497
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 1170496
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized onBody()V
    .locals 1

    .prologue
    .line 1170493
    monitor-enter p0

    const v0, 0x4bb1fafd    # 2.332825E7f

    :try_start_0
    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170494
    monitor-exit p0

    return-void

    .line 1170495
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onEOM()V
    .locals 1

    .prologue
    .line 1170505
    monitor-enter p0

    const v0, 0x12a6fd76

    :try_start_0
    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170506
    monitor-exit p0

    return-void

    .line 1170507
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read()I
    .locals 4

    .prologue
    .line 1170486
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [B

    .line 1170487
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, LX/762;->read([BII)I

    move-result v1

    .line 1170488
    packed-switch v1, :pswitch_data_0

    .line 1170489
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "n="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1170491
    :pswitch_1
    const/4 v1, 0x0

    :try_start_1
    aget-byte v0, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1170492
    :goto_0
    monitor-exit p0

    return v0

    :pswitch_2
    const/4 v0, -0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized read([B)I
    .locals 2

    .prologue
    .line 1170485
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/762;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([BII)I
    .locals 3

    .prologue
    .line 1170474
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/762;->checkNotClosed()V

    .line 1170475
    iget-object v0, p0, LX/762;->mNetworkException:LX/5oc;

    if-eqz v0, :cond_0

    .line 1170476
    iget-object v0, p0, LX/762;->mNetworkException:LX/5oc;

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170477
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1170478
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/762;->mBuffer:LX/161;

    invoke-interface {v0, p1, p2, p3}, LX/161;->read([BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 1170479
    :goto_0
    if-nez v0, :cond_2

    .line 1170480
    const-wide/16 v0, 0x3e8

    const v2, -0xbce9c4

    :try_start_2
    invoke-static {p0, v0, v1, v2}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1170481
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/762;->mNetworkException:LX/5oc;

    if-eqz v0, :cond_1

    .line 1170482
    iget-object v0, p0, LX/762;->mNetworkException:LX/5oc;

    throw v0

    .line 1170483
    :cond_1
    iget-object v0, p0, LX/762;->mBuffer:LX/161;

    invoke-interface {v0, p1, p2, p3}, LX/161;->read([BII)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    goto :goto_0

    :catch_0
    goto :goto_1

    .line 1170484
    :cond_2
    monitor-exit p0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 1170473
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public declared-synchronized setError(LX/5oc;)V
    .locals 1

    .prologue
    .line 1170469
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/762;->mNetworkException:LX/5oc;

    .line 1170470
    const v0, 0x6febd50

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170471
    monitor-exit p0

    return-void

    .line 1170472
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public skip(J)J
    .locals 1

    .prologue
    .line 1170468
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
