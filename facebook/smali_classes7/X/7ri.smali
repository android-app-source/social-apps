.class public final LX/7ri;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1263277
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1263278
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1263279
    :goto_0
    return v1

    .line 1263280
    :cond_0
    const-string v7, "viewer_non_friend_count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1263281
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 1263282
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1263283
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1263284
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1263285
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1263286
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1263287
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1263288
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 1263289
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 1263290
    invoke-static {p0, p1}, LX/7rh;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1263291
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1263292
    :cond_2
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1263293
    goto :goto_1

    .line 1263294
    :cond_3
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1263295
    invoke-static {p0, p1}, LX/4aB;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1263296
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1263297
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1263298
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1263299
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1263300
    if-eqz v0, :cond_6

    .line 1263301
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 1263302
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1263303
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1263304
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1263305
    if-eqz v0, :cond_1

    .line 1263306
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263307
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1263308
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1263309
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/7rh;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1263310
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1263311
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1263312
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1263313
    if-eqz v0, :cond_2

    .line 1263314
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263315
    invoke-static {p0, v0, p2}, LX/4aB;->a(LX/15i;ILX/0nX;)V

    .line 1263316
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1263317
    if-eqz v0, :cond_3

    .line 1263318
    const-string v1, "viewer_non_friend_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263319
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1263320
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1263321
    return-void
.end method
