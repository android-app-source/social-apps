.class public LX/8TO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1348553
    const-class v0, LX/8TO;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/8TO;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/8TN;)V
    .locals 1

    .prologue
    .line 1348554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348555
    iget-object v0, p1, LX/8TN;->a:Ljava/lang/String;

    iput-object v0, p0, LX/8TO;->b:Ljava/lang/String;

    .line 1348556
    iget-object v0, p1, LX/8TN;->c:Ljava/lang/String;

    iput-object v0, p0, LX/8TO;->c:Ljava/lang/String;

    .line 1348557
    iget-object v0, p1, LX/8TN;->d:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    iput-object v0, p0, LX/8TO;->d:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    .line 1348558
    iget-object v0, p1, LX/8TN;->b:Ljava/lang/String;

    iput-object v0, p0, LX/8TO;->e:Ljava/lang/String;

    .line 1348559
    iget-object v0, p1, LX/8TN;->e:Ljava/lang/String;

    iput-object v0, p0, LX/8TO;->f:Ljava/lang/String;

    .line 1348560
    iget-object v0, p1, LX/8TN;->f:Ljava/lang/String;

    iput-object v0, p0, LX/8TO;->g:Ljava/lang/String;

    .line 1348561
    iget-object v0, p1, LX/8TN;->g:Ljava/lang/String;

    iput-object v0, p0, LX/8TO;->i:Ljava/lang/String;

    .line 1348562
    iget-object v0, p1, LX/8TN;->h:Landroid/net/Uri;

    iput-object v0, p0, LX/8TO;->j:Landroid/net/Uri;

    .line 1348563
    iget-object v0, p1, LX/8TN;->i:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    iput-object v0, p0, LX/8TO;->h:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    .line 1348564
    return-void
.end method
