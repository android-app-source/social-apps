.class public final LX/7uT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1270665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 1270666
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1270667
    iget-object v1, p0, LX/7uT;->a:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1270668
    iget-object v3, p0, LX/7uT;->b:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1270669
    iget-object v5, p0, LX/7uT;->c:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1270670
    iget-object v6, p0, LX/7uT;->d:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1270671
    iget-object v7, p0, LX/7uT;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1270672
    const/4 v8, 0x5

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1270673
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1270674
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1270675
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1270676
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1270677
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1270678
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1270679
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1270680
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1270681
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1270682
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1270683
    new-instance v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;-><init>(LX/15i;)V

    .line 1270684
    return-object v1
.end method
