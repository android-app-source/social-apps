.class public final LX/7z2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/7zC;

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7z1;",
            "LX/7z0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/7zC;)V
    .locals 1

    .prologue
    .line 1280560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280561
    iput-object p1, p0, LX/7z2;->a:LX/7zC;

    .line 1280562
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7z2;->b:Ljava/util/Map;

    .line 1280563
    return-void
.end method

.method public constructor <init>(LX/7zR;)V
    .locals 1

    .prologue
    .line 1280564
    new-instance v0, LX/7zC;

    invoke-direct {v0, p1}, LX/7zC;-><init>(LX/7zR;)V

    invoke-direct {p0, v0}, LX/7z2;-><init>(LX/7zC;)V

    .line 1280565
    return-void
.end method

.method public static a$redex0(LX/7z2;LX/7z1;LX/7zB;)V
    .locals 3

    .prologue
    .line 1280566
    iget-object v0, p0, LX/7z2;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7z0;

    .line 1280567
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 1280568
    check-cast v1, LX/7z0;

    .line 1280569
    const/4 v2, 0x0

    .line 1280570
    iput-object v2, v1, LX/7z0;->c:LX/7zL;

    .line 1280571
    iput-object p2, v1, LX/7z0;->d:LX/7zB;

    .line 1280572
    const/4 v2, 0x1

    .line 1280573
    iput-boolean v2, v1, LX/7z0;->b:Z

    .line 1280574
    monitor-enter v0

    .line 1280575
    const v1, -0x7300e8d6

    :try_start_0
    invoke-static {v0, v1}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 1280576
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1280577
    iget-object v0, p0, LX/7z2;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280578
    :cond_0
    return-void

    .line 1280579
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static a$redex0(LX/7z2;LX/7z1;LX/7zL;)V
    .locals 3

    .prologue
    .line 1280580
    iget-object v0, p0, LX/7z2;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7z0;

    .line 1280581
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 1280582
    check-cast v1, LX/7z0;

    .line 1280583
    iput-object p2, v1, LX/7z0;->c:LX/7zL;

    .line 1280584
    const/4 v2, 0x0

    .line 1280585
    iput-object v2, v1, LX/7z0;->d:LX/7zB;

    .line 1280586
    const/4 v2, 0x1

    .line 1280587
    iput-boolean v2, v1, LX/7z0;->b:Z

    .line 1280588
    monitor-enter v0

    .line 1280589
    const v1, -0x6e7f1e

    :try_start_0
    invoke-static {v0, v1}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 1280590
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1280591
    iget-object v0, p0, LX/7z2;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280592
    :cond_0
    return-void

    .line 1280593
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public final a(LX/7yy;LX/7yw;LX/7yp;)LX/7z0;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1280594
    iget-object v0, p1, LX/7yy;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1280595
    if-eqz v0, :cond_0

    .line 1280596
    iget-object v0, p1, LX/7yy;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1280597
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1280598
    :cond_0
    new-instance v0, LX/7zB;

    const-string v1, "Empty file key"

    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/7zB;-><init>(Ljava/lang/String;JZLjava/lang/Exception;)V

    throw v0

    .line 1280599
    :cond_1
    new-instance v1, LX/7z1;

    invoke-direct {v1, p0}, LX/7z1;-><init>(LX/7z2;)V

    .line 1280600
    if-nez p3, :cond_2

    move-object v0, v1

    .line 1280601
    :goto_0
    iget-object v2, p0, LX/7z2;->a:LX/7zC;

    .line 1280602
    iget-object v3, p2, LX/7yw;->g:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 1280603
    const/4 v3, 0x0

    .line 1280604
    :goto_1
    move v3, v3

    .line 1280605
    if-eqz v3, :cond_3

    .line 1280606
    new-instance v3, LX/7z9;

    iget-object v4, v2, LX/7zC;->a:LX/7zR;

    invoke-direct {v3, p1, p2, v0, v4}, LX/7z9;-><init>(LX/7yy;LX/7yw;LX/7yp;LX/7zR;)V

    .line 1280607
    :goto_2
    move-object v0, v3

    .line 1280608
    new-instance v2, LX/7z0;

    invoke-direct {v2, v0}, LX/7z0;-><init>(LX/7z8;)V

    .line 1280609
    iget-object v0, p0, LX/7z2;->b:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280610
    iget-object v0, v2, LX/7z0;->a:LX/7z8;

    invoke-virtual {v0}, LX/7z8;->a()V

    .line 1280611
    return-object v2

    .line 1280612
    :cond_2
    new-instance v0, LX/7yq;

    const/4 v2, 0x2

    new-array v2, v2, [LX/7yp;

    aput-object v1, v2, v4

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, LX/7yq;-><init>(Ljava/lang/Iterable;)V

    goto :goto_0

    :cond_3
    new-instance v3, LX/7z8;

    iget-object v4, v2, LX/7zC;->a:LX/7zR;

    invoke-direct {v3, p1, p2, v0, v4}, LX/7z8;-><init>(LX/7yy;LX/7yw;LX/7yp;LX/7zR;)V

    goto :goto_2

    :cond_4
    iget-object v3, p2, LX/7yw;->g:Ljava/lang/String;

    const-string v4, "2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_1
.end method

.method public final a(LX/7z0;)V
    .locals 7

    .prologue
    .line 1280613
    move-object v0, p1

    check-cast v0, LX/7z0;

    .line 1280614
    iget-object v1, v0, LX/7z0;->a:LX/7z8;

    .line 1280615
    iget-object v2, v1, LX/7z8;->b:LX/7yp;

    move-object v2, v2

    .line 1280616
    const/4 v3, 0x0

    .line 1280617
    instance-of v1, v2, LX/7yq;

    if-eqz v1, :cond_4

    move-object v1, v2

    .line 1280618
    check-cast v1, LX/7yq;

    iget-object v1, v1, LX/7yq;->a:Ljava/lang/Iterable;

    .line 1280619
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v6, v3

    move-object v3, v2

    move-object v2, v6

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7yp;

    .line 1280620
    instance-of v5, v1, LX/7z1;

    if-eqz v5, :cond_0

    move-object v3, v1

    .line 1280621
    goto :goto_0

    :cond_0
    move-object v2, v1

    .line 1280622
    goto :goto_0

    :cond_1
    move-object v1, v3

    .line 1280623
    :goto_1
    instance-of v3, v1, LX/7z1;

    if-eqz v3, :cond_2

    .line 1280624
    iget-object v2, p0, LX/7z2;->b:Ljava/util/Map;

    check-cast v1, LX/7z1;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280625
    iget-object v0, v0, LX/7z0;->a:LX/7z8;

    invoke-virtual {v0}, LX/7z8;->a()V

    .line 1280626
    :goto_2
    return-void

    .line 1280627
    :cond_2
    if-eqz v2, :cond_3

    .line 1280628
    const/4 v5, 0x0

    .line 1280629
    check-cast p1, LX/7z0;

    .line 1280630
    new-instance v0, LX/7z1;

    invoke-direct {v0, p0}, LX/7z1;-><init>(LX/7z2;)V

    .line 1280631
    iget-object v1, p0, LX/7z2;->b:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280632
    iget-object v1, p1, LX/7z0;->a:LX/7z8;

    new-instance v3, LX/7yq;

    const/4 v4, 0x2

    new-array v4, v4, [LX/7yp;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, LX/7yq;-><init>(Ljava/lang/Iterable;)V

    .line 1280633
    iput-object v3, v1, LX/7z8;->b:LX/7yp;

    .line 1280634
    iget-object v0, p1, LX/7z0;->a:LX/7z8;

    invoke-virtual {v0}, LX/7z8;->a()V

    .line 1280635
    goto :goto_2

    .line 1280636
    :cond_3
    new-instance v1, LX/7z1;

    invoke-direct {v1, p0}, LX/7z1;-><init>(LX/7z2;)V

    .line 1280637
    iget-object v2, p0, LX/7z2;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280638
    iget-object v2, v0, LX/7z0;->a:LX/7z8;

    .line 1280639
    iput-object v1, v2, LX/7z8;->b:LX/7yp;

    .line 1280640
    iget-object v0, v0, LX/7z0;->a:LX/7z8;

    invoke-virtual {v0}, LX/7z8;->a()V

    goto :goto_2

    :cond_4
    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method public final b(LX/7z0;)V
    .locals 2

    .prologue
    .line 1280641
    check-cast p1, LX/7z0;

    iget-object v0, p1, LX/7z0;->a:LX/7z8;

    .line 1280642
    iget-object v1, v0, LX/7z8;->h:LX/7z3;

    if-nez v1, :cond_0

    .line 1280643
    invoke-static {v0}, LX/7z8;->g(LX/7z8;)V

    .line 1280644
    :goto_0
    return-void

    .line 1280645
    :cond_0
    iget-object v1, v0, LX/7z8;->e:LX/7zR;

    iget-object p0, v0, LX/7z8;->h:LX/7z3;

    .line 1280646
    iget-object p1, v1, LX/7zR;->c:Ljava/util/Map;

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1280647
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1280648
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1280649
    :cond_1
    goto :goto_0
.end method

.method public final c(LX/7z0;)LX/7zL;
    .locals 1

    .prologue
    .line 1280650
    :goto_0
    move-object v0, p1

    check-cast v0, LX/7z0;

    iget-boolean v0, v0, LX/7z0;->b:Z

    if-nez v0, :cond_0

    .line 1280651
    monitor-enter p1

    .line 1280652
    const v0, -0x67bf249a

    :try_start_0
    invoke-static {p1, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1280653
    :goto_1
    :try_start_1
    monitor-exit p1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1280654
    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_0
    move-object v0, p1

    .line 1280655
    check-cast v0, LX/7z0;

    iget-object v0, v0, LX/7z0;->d:LX/7zB;

    .line 1280656
    if-eqz v0, :cond_1

    .line 1280657
    throw v0

    .line 1280658
    :cond_1
    check-cast p1, LX/7z0;

    iget-object v0, p1, LX/7z0;->c:LX/7zL;

    return-object v0
.end method
