.class public LX/8Nc;
.super LX/8NQ;
.source ""


# instance fields
.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final e:I

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;JZLjava/lang/String;JLX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/0am;LX/0am;Lcom/facebook/ipc/composer/model/ProductItemAttachment;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/util/List;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 44
    .param p12    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p21    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p37    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p38    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p42    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;",
            "JZ",
            "Ljava/lang/String;",
            "J",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1337726
    const/16 v35, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-wide/from16 v10, p8

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-wide/from16 v16, p14

    move-object/from16 v18, p16

    move/from16 v19, p17

    move/from16 v20, p18

    move-object/from16 v21, p19

    move-object/from16 v22, p20

    move-object/from16 v23, p21

    move-object/from16 v24, p22

    move-object/from16 v25, p23

    move/from16 v26, p24

    move-object/from16 v27, p25

    move-object/from16 v28, p26

    move-wide/from16 v29, p27

    move-object/from16 v31, p29

    move-object/from16 v32, p30

    move-object/from16 v33, p31

    move-object/from16 v34, p32

    move/from16 v36, p36

    move-object/from16 v37, p37

    move-object/from16 v38, p38

    move-object/from16 v39, p39

    move-object/from16 v40, p40

    move-object/from16 v41, p41

    invoke-direct/range {v2 .. v43}, LX/8NQ;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;JZLjava/lang/String;JLX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/0am;LX/0am;Lcom/facebook/ipc/composer/model/ProductItemAttachment;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1337727
    invoke-static/range {p33 .. p33}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/8Nc;->c:LX/0Px;

    .line 1337728
    move/from16 v0, p34

    move-object/from16 v1, p0

    iput v0, v1, LX/8Nc;->d:I

    .line 1337729
    move/from16 v0, p35

    move-object/from16 v1, p0

    iput v0, v1, LX/8Nc;->e:I

    .line 1337730
    move-object/from16 v0, p42

    move-object/from16 v1, p0

    iput-object v0, v1, LX/8Nc;->f:Ljava/lang/String;

    .line 1337731
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;II)LX/8Nc;
    .locals 45
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;II)",
            "LX/8Nc;"
        }
    .end annotation

    .prologue
    .line 1337732
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aP()LX/5Ra;

    move-result-object v2

    if-nez v2, :cond_0

    const/16 v44, 0x0

    .line 1337733
    :goto_0
    new-instance v2, LX/8Nc;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->B()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->D()Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->E()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->H()Z

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->F()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ah()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ai()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aj()J

    move-result-wide v16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->X()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Y()Z

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Z()Z

    move-result v20

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->an()LX/5Rn;

    move-result-object v3

    sget-object v21, LX/5Rn;->NORMAL:LX/5Rn;

    move-object/from16 v0, v21

    if-ne v3, v0, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ao()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v22

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->w()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aA()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Q()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->R()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    sget-object v3, LX/8NQ;->a:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aB()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aC()J

    move-result-wide v29

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->A()Landroid/os/Bundle;

    move-result-object v31

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aJ()Ljava/lang/String;

    move-result-object v32

    sget-object v3, LX/8NQ;->b:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aK()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v34

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aM()Z

    move-result v38

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aD()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aE()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aO()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aN()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v42

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->G()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v3, p0

    move-object/from16 v35, p2

    move/from16 v36, p3

    move/from16 v37, p4

    invoke-direct/range {v2 .. v44}, LX/8Nc;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;JZLjava/lang/String;JLX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/0am;LX/0am;Lcom/facebook/ipc/composer/model/ProductItemAttachment;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/util/List;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 1337734
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aP()LX/5Ra;

    move-result-object v2

    invoke-interface {v2}, LX/5Ra;->getMoodId()Ljava/lang/String;

    move-result-object v44

    goto/16 :goto_0

    .line 1337735
    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_1
.end method
