.class public final LX/7XE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 43

    .prologue
    .line 1217327
    const/16 v39, 0x0

    .line 1217328
    const/16 v38, 0x0

    .line 1217329
    const/16 v37, 0x0

    .line 1217330
    const/16 v36, 0x0

    .line 1217331
    const/16 v35, 0x0

    .line 1217332
    const/16 v34, 0x0

    .line 1217333
    const/16 v33, 0x0

    .line 1217334
    const/16 v32, 0x0

    .line 1217335
    const/16 v31, 0x0

    .line 1217336
    const/16 v30, 0x0

    .line 1217337
    const/16 v29, 0x0

    .line 1217338
    const/16 v28, 0x0

    .line 1217339
    const/16 v27, 0x0

    .line 1217340
    const/16 v26, 0x0

    .line 1217341
    const/16 v25, 0x0

    .line 1217342
    const/16 v24, 0x0

    .line 1217343
    const/16 v23, 0x0

    .line 1217344
    const/16 v22, 0x0

    .line 1217345
    const/16 v21, 0x0

    .line 1217346
    const/16 v20, 0x0

    .line 1217347
    const/16 v19, 0x0

    .line 1217348
    const/16 v18, 0x0

    .line 1217349
    const/16 v17, 0x0

    .line 1217350
    const/16 v16, 0x0

    .line 1217351
    const/4 v15, 0x0

    .line 1217352
    const/4 v14, 0x0

    .line 1217353
    const/4 v13, 0x0

    .line 1217354
    const/4 v12, 0x0

    .line 1217355
    const/4 v11, 0x0

    .line 1217356
    const/4 v10, 0x0

    .line 1217357
    const/4 v9, 0x0

    .line 1217358
    const/4 v8, 0x0

    .line 1217359
    const/4 v7, 0x0

    .line 1217360
    const/4 v6, 0x0

    .line 1217361
    const/4 v5, 0x0

    .line 1217362
    const/4 v4, 0x0

    .line 1217363
    const/4 v3, 0x0

    .line 1217364
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_1

    .line 1217365
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1217366
    const/4 v3, 0x0

    .line 1217367
    :goto_0
    return v3

    .line 1217368
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1217369
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_22

    .line 1217370
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v40

    .line 1217371
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1217372
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    if-eqz v40, :cond_1

    .line 1217373
    const-string v41, "__type__"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-nez v41, :cond_2

    const-string v41, "__typename"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_3

    .line 1217374
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v39

    goto :goto_1

    .line 1217375
    :cond_3
    const-string v41, "back_button_behavior"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_4

    .line 1217376
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    goto :goto_1

    .line 1217377
    :cond_4
    const-string v41, "campaign_id"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_5

    .line 1217378
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    goto :goto_1

    .line 1217379
    :cond_5
    const-string v41, "campaign_token_to_refresh_type"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_6

    .line 1217380
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    goto :goto_1

    .line 1217381
    :cond_6
    const-string v41, "clickable_link_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_7

    .line 1217382
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    goto/16 :goto_1

    .line 1217383
    :cond_7
    const-string v41, "clickable_link_url"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_8

    .line 1217384
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    goto/16 :goto_1

    .line 1217385
    :cond_8
    const-string v41, "confirmation_back_button_behavior"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_9

    .line 1217386
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto/16 :goto_1

    .line 1217387
    :cond_9
    const-string v41, "confirmation_description"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_a

    .line 1217388
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 1217389
    :cond_a
    const-string v41, "confirmation_primary_button_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_b

    .line 1217390
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto/16 :goto_1

    .line 1217391
    :cond_b
    const-string v41, "confirmation_secondary_button_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_c

    .line 1217392
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 1217393
    :cond_c
    const-string v41, "confirmation_title"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_d

    .line 1217394
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 1217395
    :cond_d
    const-string v41, "delay_interval"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_e

    .line 1217396
    const/4 v7, 0x1

    .line 1217397
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v28

    goto/16 :goto_1

    .line 1217398
    :cond_e
    const-string v41, "description_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_f

    .line 1217399
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 1217400
    :cond_f
    const-string v41, "facepile_profile_picture_urls"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_10

    .line 1217401
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1217402
    :cond_10
    const-string v41, "facepile_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_11

    .line 1217403
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 1217404
    :cond_11
    const-string v41, "image_url"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_12

    .line 1217405
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 1217406
    :cond_12
    const-string v41, "optin_type"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_13

    .line 1217407
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 1217408
    :cond_13
    const-string v41, "primary_button_action"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_14

    .line 1217409
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 1217410
    :cond_14
    const-string v41, "primary_button_intent"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_15

    .line 1217411
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 1217412
    :cond_15
    const-string v41, "primary_button_step"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_16

    .line 1217413
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1217414
    :cond_16
    const-string v41, "primary_button_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_17

    .line 1217415
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 1217416
    :cond_17
    const-string v41, "secondary_button_action"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_18

    .line 1217417
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 1217418
    :cond_18
    const-string v41, "secondary_button_intent"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_19

    .line 1217419
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 1217420
    :cond_19
    const-string v41, "secondary_button_override_back_only"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1a

    .line 1217421
    const/4 v6, 0x1

    .line 1217422
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 1217423
    :cond_1a
    const-string v41, "secondary_button_step"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1b

    .line 1217424
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1217425
    :cond_1b
    const-string v41, "secondary_button_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1c

    .line 1217426
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1217427
    :cond_1c
    const-string v41, "should_show_confirmation"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1d

    .line 1217428
    const/4 v5, 0x1

    .line 1217429
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 1217430
    :cond_1d
    const-string v41, "should_use_default_image"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1e

    .line 1217431
    const/4 v4, 0x1

    .line 1217432
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 1217433
    :cond_1e
    const-string v41, "subtitle"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1f

    .line 1217434
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1217435
    :cond_1f
    const-string v41, "terms_and_conditions_text"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_20

    .line 1217436
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 1217437
    :cond_20
    const-string v41, "title"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_21

    .line 1217438
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1217439
    :cond_21
    const-string v41, "ttl"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_0

    .line 1217440
    const/4 v3, 0x1

    .line 1217441
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto/16 :goto_1

    .line 1217442
    :cond_22
    const/16 v40, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1217443
    const/16 v40, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217444
    const/16 v39, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v39

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217445
    const/16 v38, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v38

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217446
    const/16 v37, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217447
    const/16 v36, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217448
    const/16 v35, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217449
    const/16 v34, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217450
    const/16 v33, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217451
    const/16 v32, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217452
    const/16 v31, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217453
    const/16 v30, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1217454
    if-eqz v7, :cond_23

    .line 1217455
    const/16 v7, 0xb

    const/16 v29, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v7, v1, v2}, LX/186;->a(III)V

    .line 1217456
    :cond_23
    const/16 v7, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217457
    const/16 v7, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217458
    const/16 v7, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217459
    const/16 v7, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217460
    const/16 v7, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217461
    const/16 v7, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217462
    const/16 v7, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217463
    const/16 v7, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217464
    const/16 v7, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217465
    const/16 v7, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217466
    const/16 v7, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1217467
    if-eqz v6, :cond_24

    .line 1217468
    const/16 v6, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1217469
    :cond_24
    const/16 v6, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 1217470
    const/16 v6, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 1217471
    if-eqz v5, :cond_25

    .line 1217472
    const/16 v5, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->a(IZ)V

    .line 1217473
    :cond_25
    if-eqz v4, :cond_26

    .line 1217474
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 1217475
    :cond_26
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1217476
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1217477
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1217478
    if-eqz v3, :cond_27

    .line 1217479
    const/16 v3, 0x1f

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8, v4}, LX/186;->a(III)V

    .line 1217480
    :cond_27
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/16 v3, 0xd

    const/4 v2, 0x0

    .line 1217481
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1217482
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1217483
    if-eqz v0, :cond_0

    .line 1217484
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217485
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1217486
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217487
    if-eqz v0, :cond_1

    .line 1217488
    const-string v1, "back_button_behavior"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217489
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217490
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217491
    if-eqz v0, :cond_2

    .line 1217492
    const-string v1, "campaign_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217493
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217494
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217495
    if-eqz v0, :cond_3

    .line 1217496
    const-string v1, "campaign_token_to_refresh_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217497
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217498
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217499
    if-eqz v0, :cond_4

    .line 1217500
    const-string v1, "clickable_link_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217501
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217502
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217503
    if-eqz v0, :cond_5

    .line 1217504
    const-string v1, "clickable_link_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217505
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217506
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217507
    if-eqz v0, :cond_6

    .line 1217508
    const-string v1, "confirmation_back_button_behavior"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217509
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217510
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217511
    if-eqz v0, :cond_7

    .line 1217512
    const-string v1, "confirmation_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217513
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217514
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217515
    if-eqz v0, :cond_8

    .line 1217516
    const-string v1, "confirmation_primary_button_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217517
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217518
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217519
    if-eqz v0, :cond_9

    .line 1217520
    const-string v1, "confirmation_secondary_button_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217521
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217522
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217523
    if-eqz v0, :cond_a

    .line 1217524
    const-string v1, "confirmation_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217525
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217526
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1217527
    if-eqz v0, :cond_b

    .line 1217528
    const-string v1, "delay_interval"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217529
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1217530
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217531
    if-eqz v0, :cond_c

    .line 1217532
    const-string v1, "description_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217533
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217534
    :cond_c
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1217535
    if-eqz v0, :cond_d

    .line 1217536
    const-string v0, "facepile_profile_picture_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217537
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1217538
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217539
    if-eqz v0, :cond_e

    .line 1217540
    const-string v1, "facepile_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217541
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217542
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217543
    if-eqz v0, :cond_f

    .line 1217544
    const-string v1, "image_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217545
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217546
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217547
    if-eqz v0, :cond_10

    .line 1217548
    const-string v1, "optin_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217549
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217550
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217551
    if-eqz v0, :cond_11

    .line 1217552
    const-string v1, "primary_button_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217553
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217554
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217555
    if-eqz v0, :cond_12

    .line 1217556
    const-string v1, "primary_button_intent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217557
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217558
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217559
    if-eqz v0, :cond_13

    .line 1217560
    const-string v1, "primary_button_step"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217561
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217562
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217563
    if-eqz v0, :cond_14

    .line 1217564
    const-string v1, "primary_button_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217565
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217566
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217567
    if-eqz v0, :cond_15

    .line 1217568
    const-string v1, "secondary_button_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217569
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217570
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217571
    if-eqz v0, :cond_16

    .line 1217572
    const-string v1, "secondary_button_intent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217573
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217574
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1217575
    if-eqz v0, :cond_17

    .line 1217576
    const-string v1, "secondary_button_override_back_only"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217577
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1217578
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217579
    if-eqz v0, :cond_18

    .line 1217580
    const-string v1, "secondary_button_step"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217581
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217582
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217583
    if-eqz v0, :cond_19

    .line 1217584
    const-string v1, "secondary_button_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217585
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217586
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1217587
    if-eqz v0, :cond_1a

    .line 1217588
    const-string v1, "should_show_confirmation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217589
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1217590
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1217591
    if-eqz v0, :cond_1b

    .line 1217592
    const-string v1, "should_use_default_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217593
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1217594
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217595
    if-eqz v0, :cond_1c

    .line 1217596
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217597
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217598
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217599
    if-eqz v0, :cond_1d

    .line 1217600
    const-string v1, "terms_and_conditions_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217601
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217602
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1217603
    if-eqz v0, :cond_1e

    .line 1217604
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217605
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1217606
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1217607
    if-eqz v0, :cond_1f

    .line 1217608
    const-string v1, "ttl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1217609
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1217610
    :cond_1f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1217611
    return-void
.end method
