.class public final LX/6tt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ex;


# instance fields
.field public final synthetic a:LX/6tv;


# direct methods
.method public constructor <init>(LX/6tv;)V
    .locals 0

    .prologue
    .line 1155260
    iput-object p1, p0, LX/6tt;->a:LX/6tv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1155261
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V
    .locals 4

    .prologue
    .line 1155262
    iget-object v0, p0, LX/6tt;->a:LX/6tv;

    .line 1155263
    iget-object v1, v0, LX/6tv;->c:LX/6xb;

    iget-object v2, v0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, v0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->c:LX/6xZ;

    const-string p0, "payflows_success"

    invoke-virtual {v1, v2, v3, p0}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1155264
    iget-object v1, v0, LX/6tv;->e:LX/6qd;

    iget-object v2, v0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1, v2, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V

    .line 1155265
    invoke-virtual {v0}, LX/6tv;->c()V

    .line 1155266
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1155267
    iget-object v0, p0, LX/6tt;->a:LX/6tv;

    iget-object v0, v0, LX/6tv;->f:LX/6qp;

    invoke-interface {v0, p1}, LX/6qp;->a(Ljava/lang/String;)V

    .line 1155268
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1155269
    iget-object v0, p0, LX/6tt;->a:LX/6tv;

    .line 1155270
    iget-object v1, v0, LX/6tv;->c:LX/6xb;

    iget-object v2, v0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object p0, v0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object p0

    iget-object p0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->c:LX/6xZ;

    invoke-virtual {v1, v2, p0, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/Throwable;)V

    .line 1155271
    invoke-virtual {v0}, LX/6tv;->b()V

    .line 1155272
    return-void
.end method
