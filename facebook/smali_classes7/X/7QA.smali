.class public final LX/7QA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/video/settings/graphql/AutoplaySettingsGraphQLModels$ViewerQueryModel$DeviceAutoplaySettingModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final synthetic b:LX/1mD;


# direct methods
.method public constructor <init>(LX/1mD;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1203715
    iput-object p1, p0, LX/7QA;->b:LX/1mD;

    iput-object p2, p0, LX/7QA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1203716
    iget-object v0, p0, LX/7QA;->b:LX/1mD;

    const-string v1, "Failed to read the client autoplay setting from the server."

    invoke-virtual {v0, v1, p1}, LX/1mD;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203717
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1203697
    check-cast p1, Lcom/facebook/video/settings/graphql/AutoplaySettingsGraphQLModels$ViewerQueryModel$DeviceAutoplaySettingModel;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1203698
    invoke-virtual {p1}, Lcom/facebook/video/settings/graphql/AutoplaySettingsGraphQLModels$ViewerQueryModel$DeviceAutoplaySettingModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    if-ne v0, v7, :cond_3

    .line 1203699
    invoke-virtual {p1}, Lcom/facebook/video/settings/graphql/AutoplaySettingsGraphQLModels$ViewerQueryModel$DeviceAutoplaySettingModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1203700
    iget-object v3, p0, LX/7QA;->b:LX/1mD;

    iget-object v4, p0, LX/7QA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v0, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    invoke-virtual {v1, v2, v6, v0, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    invoke-virtual {v1, v2, v7}, LX/15i;->h(II)Z

    move-result v1

    const/4 v2, 0x0

    .line 1203701
    invoke-static {v4}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/03R;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/03R;->asBoolean(Z)Z

    move-result v5

    .line 1203702
    invoke-static {v0}, LX/1mD;->a(Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;)LX/1mC;

    move-result-object v6

    .line 1203703
    if-nez v5, :cond_4

    .line 1203704
    iget-object v5, v3, LX/1mD;->e:LX/1mC;

    if-eq v5, v6, :cond_0

    if-eqz v1, :cond_0

    .line 1203705
    iget-object v5, v3, LX/1mD;->e:LX/1mC;

    invoke-static {v4, v5}, LX/1mS;->b(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V

    .line 1203706
    :cond_0
    iput-object v6, v3, LX/1mD;->e:LX/1mC;

    .line 1203707
    iget-object v5, v3, LX/1mD;->e:LX/1mC;

    invoke-static {v4, v5}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V

    .line 1203708
    if-nez v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-static {v4, v2}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;Z)V

    .line 1203709
    :cond_2
    :goto_0
    return-void

    .line 1203710
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1203711
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/video/settings/graphql/AutoplaySettingsGraphQLModels$ViewerQueryModel$DeviceAutoplaySettingModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1203712
    iget-object v0, p0, LX/7QA;->b:LX/1mD;

    iget-object v1, p0, LX/7QA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/7QA;->b:LX/1mD;

    iget-object v2, v2, LX/1mD;->e:LX/1mC;

    const-string v3, "MIGRATION"

    invoke-virtual {v0, v1, v2, v3}, LX/1mD;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;Ljava/lang/String;)V

    goto :goto_0

    .line 1203713
    :cond_4
    if-eqz v5, :cond_2

    iget-object v2, v3, LX/1mD;->e:LX/1mC;

    if-eq v2, v6, :cond_2

    .line 1203714
    iget-object v2, v3, LX/1mD;->e:LX/1mC;

    const-string v5, "SETTING_CHANGE"

    invoke-virtual {v3, v4, v2, v5}, LX/1mD;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;Ljava/lang/String;)V

    goto :goto_0
.end method
