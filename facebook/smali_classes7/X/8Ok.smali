.class public final LX/8Ok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/60y;


# instance fields
.field private final a:J

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/8Om;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0SG;

.field private final d:LX/8OL;

.field private e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation
.end field

.field private f:J

.field private g:I

.field private h:Z

.field private i:I

.field private j:Z


# direct methods
.method public constructor <init>(LX/8Om;LX/0SG;LX/8OL;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1340354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1340355
    const-wide/16 v0, 0x64

    iput-wide v0, p0, LX/8Ok;->a:J

    .line 1340356
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/8Ok;->f:J

    .line 1340357
    iput v2, p0, LX/8Ok;->g:I

    .line 1340358
    iput-boolean v2, p0, LX/8Ok;->h:Z

    .line 1340359
    const/4 v0, 0x1

    iput v0, p0, LX/8Ok;->i:I

    .line 1340360
    iput-boolean v2, p0, LX/8Ok;->j:Z

    .line 1340361
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/8Ok;->b:Ljava/lang/ref/WeakReference;

    .line 1340362
    iput-object p2, p0, LX/8Ok;->c:LX/0SG;

    .line 1340363
    iput-object p3, p0, LX/8Ok;->d:LX/8OL;

    .line 1340364
    return-void
.end method

.method private b(D)D
    .locals 5

    .prologue
    .line 1340350
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    .line 1340351
    iget v2, p0, LX/8Ok;->g:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    .line 1340352
    iget v2, p0, LX/8Ok;->i:I

    int-to-double v2, v2

    .line 1340353
    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1340340
    iget-boolean v0, p0, LX/8Ok;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8Ok;->j:Z

    if-eqz v0, :cond_1

    .line 1340341
    :cond_0
    iget-object v0, p0, LX/8Ok;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Om;

    .line 1340342
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/8Ok;->d:LX/8OL;

    .line 1340343
    iget-boolean v2, v1, LX/8OL;->d:Z

    move v1, v2

    .line 1340344
    if-nez v1, :cond_1

    .line 1340345
    iget-object v1, p0, LX/8Ok;->e:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 1340346
    :goto_0
    if-nez v1, :cond_3

    .line 1340347
    :cond_1
    :goto_1
    return-void

    .line 1340348
    :cond_2
    iget-object v1, p0, LX/8Ok;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/upload/operation/UploadOperation;

    goto :goto_0

    .line 1340349
    :cond_3
    iget-object v2, v0, LX/8Om;->g:LX/0b3;

    new-instance v3, LX/8Kk;

    sget-object v4, LX/8KZ;->PROCESSING:LX/8KZ;

    const/high16 p0, 0x42c80000    # 100.0f

    invoke-direct {v3, v1, v4, p0}, LX/8Kk;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method

.method public final a(D)V
    .locals 9

    .prologue
    .line 1340365
    iget-object v0, p0, LX/8Ok;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Om;

    .line 1340366
    iget-object v1, p0, LX/8Ok;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 1340367
    if-eqz v0, :cond_1

    iget-wide v4, p0, LX/8Ok;->f:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x64

    cmp-long v1, v4, v6

    if-ltz v1, :cond_1

    .line 1340368
    iput-wide v2, p0, LX/8Ok;->f:J

    .line 1340369
    iget-boolean v1, p0, LX/8Ok;->h:Z

    if-eqz v1, :cond_0

    .line 1340370
    invoke-direct {p0, p1, p2}, LX/8Ok;->b(D)D

    move-result-wide p1

    .line 1340371
    :cond_0
    iget-object v1, p0, LX/8Ok;->e:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, p1, p2, v1}, LX/8Om;->a(DLcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340372
    :cond_1
    return-void

    .line 1340373
    :cond_2
    iget-object v1, p0, LX/8Ok;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/upload/operation/UploadOperation;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1340322
    iget v0, p0, LX/8Ok;->i:I

    if-gt p1, v0, :cond_0

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1340323
    iput p1, p0, LX/8Ok;->g:I

    .line 1340324
    return-void

    .line 1340325
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1340329
    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1340330
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/8Ok;->e:Ljava/lang/ref/WeakReference;

    .line 1340331
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/8Ok;->f:J

    .line 1340332
    iput p2, p0, LX/8Ok;->i:I

    .line 1340333
    iput v2, p0, LX/8Ok;->g:I

    .line 1340334
    iput-boolean v2, p0, LX/8Ok;->j:Z

    .line 1340335
    iget v0, p0, LX/8Ok;->i:I

    if-le v0, v1, :cond_1

    .line 1340336
    iput-boolean v1, p0, LX/8Ok;->h:Z

    .line 1340337
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1340338
    goto :goto_0

    .line 1340339
    :cond_1
    iput-boolean v2, p0, LX/8Ok;->h:Z

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1340326
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Ok;->j:Z

    .line 1340327
    invoke-virtual {p0}, LX/8Ok;->a()V

    .line 1340328
    return-void
.end method
