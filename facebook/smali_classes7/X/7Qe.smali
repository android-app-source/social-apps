.class public LX/7Qe;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1Ck;

.field private final c:LX/19j;

.field private final d:LX/0tX;

.field public final e:LX/0Zc;

.field private final f:Ljava/lang/String;

.field public g:LX/7QY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1204704
    const-class v0, LX/7Qe;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Qe;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/19j;LX/0tX;LX/0Zc;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1204705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204706
    iput-object p1, p0, LX/7Qe;->b:LX/1Ck;

    .line 1204707
    iput-object p2, p0, LX/7Qe;->c:LX/19j;

    .line 1204708
    iput-object p3, p0, LX/7Qe;->d:LX/0tX;

    .line 1204709
    iput-object p4, p0, LX/7Qe;->e:LX/0Zc;

    .line 1204710
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/7Qe;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7Qe;->f:Ljava/lang/String;

    .line 1204711
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/7QY;)V
    .locals 1

    .prologue
    .line 1204712
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/7Qe;->g:LX/7QY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204713
    monitor-exit p0

    return-void

    .line 1204714
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1204715
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1204716
    new-instance v1, LX/7RD;

    invoke-direct {v1}, LX/7RD;-><init>()V

    move-object v1, v1

    .line 1204717
    const-string v2, "video_ids"

    invoke-virtual {v1, v2, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1204718
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1204719
    iget-object v2, p0, LX/7Qe;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    invoke-static {v1}, LX/0tX;->d(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1204720
    iget-object v2, p0, LX/7Qe;->e:LX/0Zc;

    const-string v3, ""

    sget-object v4, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_START:LX/7IM;

    const-string v5, "Live subscription query started"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1204721
    iget-object v2, p0, LX/7Qe;->b:LX/1Ck;

    iget-object v3, p0, LX/7Qe;->f:Ljava/lang/String;

    new-instance v4, LX/7Qc;

    invoke-direct {v4, p0, v0}, LX/7Qc;-><init>(LX/7Qe;Ljava/util/Collection;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204722
    monitor-exit p0

    return-void

    .line 1204723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1204724
    monitor-enter p0

    .line 1204725
    :try_start_0
    new-instance v0, LX/7RC;

    invoke-direct {v0}, LX/7RC;-><init>()V

    move-object v0, v0

    .line 1204726
    const-string v1, "video_story_ids"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1204727
    iget-object v1, p0, LX/7Qe;->c:LX/19j;

    iget-boolean v1, v1, LX/19j;->S:Z

    if-eqz v1, :cond_0

    .line 1204728
    const-string v1, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1204729
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1204730
    iget-object v1, p0, LX/7Qe;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->d(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1204731
    iget-object v1, p0, LX/7Qe;->e:LX/0Zc;

    const-string v2, ""

    sget-object v3, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_START:LX/7IM;

    const-string v4, "Live subscription query started"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1204732
    iget-object v1, p0, LX/7Qe;->b:LX/1Ck;

    iget-object v2, p0, LX/7Qe;->f:Ljava/lang/String;

    new-instance v3, LX/7Qd;

    invoke-direct {v3, p0}, LX/7Qd;-><init>(LX/7Qe;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204733
    monitor-exit p0

    return-void

    .line 1204734
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
