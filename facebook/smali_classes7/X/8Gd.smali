.class public final LX/8Gd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0xi;


# instance fields
.field public final synthetic a:LX/8Ge;


# direct methods
.method public constructor <init>(LX/8Ge;)V
    .locals 0

    .prologue
    .line 1319839
    iput-object p1, p0, LX/8Gd;->a:LX/8Ge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 3

    .prologue
    .line 1319840
    iget-object v0, p0, LX/8Gd;->a:LX/8Ge;

    iget-object v0, v0, LX/8Ge;->i:LX/9dH;

    if-nez v0, :cond_0

    .line 1319841
    :goto_0
    return-void

    .line 1319842
    :cond_0
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1319843
    iget-object v1, p0, LX/8Gd;->a:LX/8Ge;

    iget-boolean v1, v1, LX/8Ge;->j:Z

    if-eqz v1, :cond_1

    .line 1319844
    const v1, 0x3f99999a    # 1.2f

    mul-float/2addr v0, v1

    .line 1319845
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    .line 1319846
    invoke-virtual {p1}, LX/0wd;->j()LX/0wd;

    goto :goto_0

    .line 1319847
    :cond_2
    iget-object v1, p0, LX/8Gd;->a:LX/8Ge;

    iget v1, v1, LX/8Ge;->f:F

    iget-object v2, p0, LX/8Gd;->a:LX/8Ge;

    iget v2, v2, LX/8Ge;->d:F

    invoke-static {v1, v2, v0}, LX/0yq;->a(FFF)F

    move-result v0

    .line 1319848
    iget-object v1, p0, LX/8Gd;->a:LX/8Ge;

    iget-object v1, v1, LX/8Ge;->i:LX/9dH;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/9dH;->a(FZ)V

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 3

    .prologue
    .line 1319849
    iget-object v0, p0, LX/8Gd;->a:LX/8Ge;

    iget-object v0, v0, LX/8Ge;->i:LX/9dH;

    if-nez v0, :cond_0

    .line 1319850
    :goto_0
    return-void

    .line 1319851
    :cond_0
    iget-object v0, p0, LX/8Gd;->a:LX/8Ge;

    iget-object v0, v0, LX/8Ge;->i:LX/9dH;

    iget-object v1, p0, LX/8Gd;->a:LX/8Ge;

    iget v1, v1, LX/8Ge;->d:F

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/9dH;->a(FZ)V

    .line 1319852
    iget-object v0, p0, LX/8Gd;->a:LX/8Ge;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1319853
    iput-object v1, v0, LX/8Ge;->c:LX/0am;

    .line 1319854
    invoke-virtual {p1}, LX/0wd;->a()V

    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 0

    .prologue
    .line 1319855
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 0

    .prologue
    .line 1319856
    return-void
.end method
