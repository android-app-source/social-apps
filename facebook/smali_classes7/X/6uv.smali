.class public final LX/6uv;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;)V
    .locals 0

    .prologue
    .line 1156301
    iput-object p1, p0, LX/6uv;->a:Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 3

    .prologue
    .line 1156302
    iget-object v0, p0, LX/6uv;->a:Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;

    .line 1156303
    sget-object v1, LX/6ux;->a:[I

    .line 1156304
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 1156305
    invoke-virtual {v2}, LX/73S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1156306
    :cond_0
    :goto_0
    return-void

    .line 1156307
    :pswitch_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1156308
    if-eqz v1, :cond_0

    .line 1156309
    const-string v2, "extra_activity_result_data"

    invoke-virtual {p1, v2}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 1156310
    const/4 p0, -0x1

    invoke-virtual {v1, p0, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1156311
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1156312
    iget-object v0, p0, LX/6uv;->a:Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;

    iget-object v0, v0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6uv;->a:Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1156313
    return-void
.end method
