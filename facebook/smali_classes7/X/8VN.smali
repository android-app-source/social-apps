.class public final LX/8VN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1352943
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1352944
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1352945
    :goto_0
    return v1

    .line 1352946
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1352947
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1352948
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1352949
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1352950
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1352951
    const-string v5, "application"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1352952
    invoke-static {p0, p1}, LX/8VL;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1352953
    :cond_2
    const-string v5, "tags"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1352954
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1352955
    :cond_3
    const-string v5, "text_lines"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1352956
    invoke-static {p0, p1}, LX/8VM;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1352957
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1352958
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1352959
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1352960
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1352961
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1352962
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1352963
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1352964
    if-eqz v0, :cond_0

    .line 1352965
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1352966
    invoke-static {p0, v0, p2, p3}, LX/8VL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1352967
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1352968
    if-eqz v0, :cond_1

    .line 1352969
    const-string v0, "tags"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1352970
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1352971
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1352972
    if-eqz v0, :cond_2

    .line 1352973
    const-string v1, "text_lines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1352974
    invoke-static {p0, v0, p2, p3}, LX/8VM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1352975
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1352976
    return-void
.end method
