.class public final LX/80i;
.super LX/1jx;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:J

.field public final synthetic c:LX/80j;


# direct methods
.method public constructor <init>(LX/80j;Ljava/util/Set;J)V
    .locals 1

    .prologue
    .line 1284057
    iput-object p1, p0, LX/80i;->c:LX/80j;

    iput-object p2, p0, LX/80i;->a:Ljava/util/Set;

    iput-wide p3, p0, LX/80i;->b:J

    invoke-direct {p0}, LX/1jx;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1284051
    instance-of v0, p1, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1284052
    iget-object v1, p0, LX/80i;->a:Ljava/util/Set;

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1284053
    :cond_0
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1284054
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-wide v2, p0, LX/80i;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(J)V

    .line 1284055
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p1, v4}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V

    .line 1284056
    :cond_1
    return v4
.end method
