.class public final LX/84P;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5P5;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic e:LX/2h7;

.field public final synthetic f:LX/2iT;


# direct methods
.method public constructor <init>(LX/2iT;LX/5P5;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/2h7;)V
    .locals 1

    .prologue
    .line 1290930
    iput-object p1, p0, LX/84P;->f:LX/2iT;

    iput-object p2, p0, LX/84P;->a:LX/5P5;

    iput-wide p3, p0, LX/84P;->b:J

    iput-object p5, p0, LX/84P;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p6, p0, LX/84P;->d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p7, p0, LX/84P;->e:LX/2h7;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1290922
    iget-object v0, p0, LX/84P;->a:LX/5P5;

    if-eqz v0, :cond_0

    .line 1290923
    iget-object v0, p0, LX/84P;->a:LX/5P5;

    invoke-interface {v0}, LX/5P5;->b()V

    .line 1290924
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v1, p0, LX/84P;->d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1290925
    iget-object v0, p0, LX/84P;->f:LX/2iT;

    invoke-virtual {v0, p1}, LX/2hY;->a(Ljava/lang/Throwable;)V

    .line 1290926
    :goto_0
    return-void

    .line 1290927
    :cond_1
    iget-object v0, p0, LX/84P;->f:LX/2iT;

    iget-object v0, v0, LX/2hY;->b:LX/2hZ;

    iget-object v1, p0, LX/84P;->f:LX/2iT;

    iget-wide v2, p0, LX/84P;->b:J

    iget-object v4, p0, LX/84P;->e:LX/2h7;

    .line 1290928
    new-instance p0, LX/84S;

    invoke-direct {p0, v1, v2, v3, v4}, LX/84S;-><init>(LX/2iT;JLX/2h7;)V

    move-object v1, p0

    .line 1290929
    invoke-virtual {v0, p1, v1}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1290917
    check-cast p1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1290918
    iget-object v0, p0, LX/84P;->a:LX/5P5;

    if-eqz v0, :cond_0

    .line 1290919
    iget-object v0, p0, LX/84P;->a:LX/5P5;

    invoke-interface {v0}, LX/5P5;->a()V

    .line 1290920
    :cond_0
    iget-object v0, p0, LX/84P;->f:LX/2iT;

    iget-wide v2, p0, LX/84P;->b:J

    if-nez p1, :cond_1

    iget-object p1, p0, LX/84P;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, p1, v1}, LX/2hY;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290921
    return-void
.end method
