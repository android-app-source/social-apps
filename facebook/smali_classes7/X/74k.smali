.class public final LX/74k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J

.field public c:Z

.field public d:Z

.field public e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/ipc/media/data/LocalMediaData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field private i:Ljava/lang/String;

.field private j:LX/4gP;

.field public k:LX/4gN;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1168376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168377
    iput-wide v2, p0, LX/74k;->h:J

    .line 1168378
    iput-wide v2, p0, LX/74k;->a:J

    .line 1168379
    iput-wide v2, p0, LX/74k;->b:J

    .line 1168380
    iput-boolean v1, p0, LX/74k;->c:Z

    .line 1168381
    iput-boolean v1, p0, LX/74k;->d:Z

    .line 1168382
    iput-object v0, p0, LX/74k;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1168383
    iput-object v0, p0, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168384
    iput-object v0, p0, LX/74k;->g:Ljava/lang/String;

    .line 1168385
    const-string v0, ""

    iput-object v0, p0, LX/74k;->i:Ljava/lang/String;

    .line 1168386
    new-instance v0, LX/4gP;

    invoke-direct {v0}, LX/4gP;-><init>()V

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    sget-object v1, LX/4gQ;->Photo:LX/4gQ;

    invoke-virtual {v0, v1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    iput-object v0, p0, LX/74k;->j:LX/4gP;

    .line 1168387
    new-instance v0, LX/4gN;

    invoke-direct {v0}, LX/4gN;-><init>()V

    iput-object v0, p0, LX/74k;->k:LX/4gN;

    .line 1168388
    return-void
.end method


# virtual methods
.method public final a(I)LX/74k;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1168389
    iget-object v0, p0, LX/74k;->j:LX/4gP;

    .line 1168390
    iput p1, v0, LX/4gP;->e:I

    .line 1168391
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/74k;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1168373
    iget-object v0, p0, LX/74k;->j:LX/4gP;

    invoke-static {p1}, LX/74n;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    .line 1168374
    iput-object p1, p0, LX/74k;->i:Ljava/lang/String;

    .line 1168375
    return-object p0
.end method

.method public final a()Lcom/facebook/photos/base/media/PhotoItem;
    .locals 6

    .prologue
    .line 1168368
    iget-object v0, p0, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    if-nez v0, :cond_0

    .line 1168369
    iget-object v0, p0, LX/74k;->j:LX/4gP;

    new-instance v1, Lcom/facebook/ipc/media/MediaIdKey;

    iget-object v2, p0, LX/74k;->i:Ljava/lang/String;

    iget-wide v4, p0, LX/74k;->h:J

    invoke-direct {v1, v2, v4, v5}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    .line 1168370
    iget-object v0, p0, LX/74k;->j:LX/4gP;

    invoke-virtual {v0}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1168371
    iget-object v1, p0, LX/74k;->k:LX/4gN;

    invoke-virtual {v1, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    iput-object v0, p0, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168372
    :cond_0
    new-instance v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-direct {v0, p0}, Lcom/facebook/photos/base/media/PhotoItem;-><init>(LX/74k;)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;)LX/74k;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1168366
    iget-object v0, p0, LX/74k;->j:LX/4gP;

    invoke-static {p1}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    .line 1168367
    return-object p0
.end method
