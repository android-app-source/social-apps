.class public LX/73s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field private b:Landroid/content/res/Resources;

.field public c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1166347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1166348
    iput-object p1, p0, LX/73s;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1166349
    iput-object p2, p0, LX/73s;->b:Landroid/content/res/Resources;

    .line 1166350
    iput-object p3, p0, LX/73s;->c:Landroid/content/Context;

    .line 1166351
    return-void
.end method

.method public static b(LX/0QB;)LX/73s;
    .locals 4

    .prologue
    .line 1166352
    new-instance v3, LX/73s;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v3, v0, v1, v2}, LX/73s;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;Landroid/content/Context;)V

    .line 1166353
    return-object v3
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1166354
    iget-object v0, p0, LX/73s;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a0675

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1166355
    new-instance v1, LX/73r;

    invoke-direct {v1, p0, p5, v0}, LX/73r;-><init>(LX/73s;Ljava/lang/String;I)V

    .line 1166356
    new-instance v0, LX/47x;

    iget-object v2, p0, LX/73s;->b:Landroid/content/res/Resources;

    invoke-direct {v0, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1166357
    invoke-virtual {v0, p1}, LX/47x;->a(I)LX/47x;

    .line 1166358
    const/16 v2, 0x21

    invoke-virtual {v0, p2, p3, v1, v2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1166359
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1166360
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1166361
    return-void
.end method
