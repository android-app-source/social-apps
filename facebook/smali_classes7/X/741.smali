.class public final enum LX/741;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/741;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/741;

.field public static final enum LOCAL:LX/741;

.field public static final enum UNKNOWN:LX/741;

.field public static final enum VAULT:LX/741;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1167377
    new-instance v0, LX/741;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v2}, LX/741;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/741;->LOCAL:LX/741;

    .line 1167378
    new-instance v0, LX/741;

    const-string v1, "VAULT"

    invoke-direct {v0, v1, v3}, LX/741;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/741;->VAULT:LX/741;

    .line 1167379
    new-instance v0, LX/741;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/741;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/741;->UNKNOWN:LX/741;

    .line 1167380
    const/4 v0, 0x3

    new-array v0, v0, [LX/741;

    sget-object v1, LX/741;->LOCAL:LX/741;

    aput-object v1, v0, v2

    sget-object v1, LX/741;->VAULT:LX/741;

    aput-object v1, v0, v3

    sget-object v1, LX/741;->UNKNOWN:LX/741;

    aput-object v1, v0, v4

    sput-object v0, LX/741;->$VALUES:[LX/741;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1167381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/741;
    .locals 1

    .prologue
    .line 1167382
    const-class v0, LX/741;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/741;

    return-object v0
.end method

.method public static values()[LX/741;
    .locals 1

    .prologue
    .line 1167383
    sget-object v0, LX/741;->$VALUES:[LX/741;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/741;

    return-object v0
.end method
