.class public LX/8Np;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/8Nj;

.field private final d:LX/1Ck;

.field public final e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0Px;LX/8Nj;)V
    .locals 1
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/8Nj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/8Nj;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337930
    iput-object p1, p0, LX/8Np;->a:LX/0tX;

    .line 1337931
    iput-object p2, p0, LX/8Np;->d:LX/1Ck;

    .line 1337932
    iput-object p3, p0, LX/8Np;->b:LX/0Px;

    .line 1337933
    iput-object p4, p0, LX/8Np;->c:LX/8Nj;

    .line 1337934
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/8Np;->e:Landroid/os/Handler;

    .line 1337935
    return-void
.end method

.method public static c(LX/8Np;)V
    .locals 5

    .prologue
    .line 1337936
    iget-object v0, p0, LX/8Np;->d:LX/1Ck;

    const-string v1, "videos_status_fetch"

    .line 1337937
    new-instance v2, LX/8Nq;

    invoke-direct {v2}, LX/8Nq;-><init>()V

    .line 1337938
    const-string v3, "targetIDs"

    iget-object v4, p0, LX/8Np;->b:LX/0Px;

    invoke-virtual {v2, v3, v4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1337939
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 1337940
    iget-object v3, p0, LX/8Np;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    invoke-static {v2}, LX/0tX;->c(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1337941
    move-object v2, v2

    .line 1337942
    new-instance v3, LX/8Nn;

    invoke-direct {v3, p0}, LX/8Nn;-><init>(LX/8Np;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1337943
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1337944
    iget-object v0, p0, LX/8Np;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/photos/upload/serverprocessing/VideoStatusChecker$1;

    invoke-direct {v1, p0}, Lcom/facebook/photos/upload/serverprocessing/VideoStatusChecker$1;-><init>(LX/8Np;)V

    const-wide/16 v2, 0xbb8

    const v4, 0x1d70e84e

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1337945
    return-void
.end method
