.class public LX/8Lj;
.super LX/1a1;
.source ""


# instance fields
.field public final l:LX/0kb;

.field public final m:LX/0ad;

.field public final n:Landroid/widget/LinearLayout;

.field public final o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field public final p:Lcom/facebook/resources/ui/FbTextView;

.field public final q:LX/1RW;

.field public r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/0kb;LX/1RW;LX/0YR;LX/0ad;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1333713
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1333714
    iput-object p2, p0, LX/8Lj;->l:LX/0kb;

    .line 1333715
    iput-object p5, p0, LX/8Lj;->m:LX/0ad;

    .line 1333716
    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, LX/8Lj;->n:Landroid/widget/LinearLayout;

    .line 1333717
    iget-object v0, p0, LX/8Lj;->n:Landroid/widget/LinearLayout;

    const v1, 0x7f0d2fe8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, LX/8Lj;->o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1333718
    iget-object v0, p0, LX/8Lj;->n:Landroid/widget/LinearLayout;

    const v1, 0x7f0d2fe9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 1333719
    iput-object p3, p0, LX/8Lj;->q:LX/1RW;

    .line 1333720
    invoke-interface {p4}, LX/0YR;->a()LX/1hM;

    move-result-object v0

    .line 1333721
    if-eqz v0, :cond_0

    .line 1333722
    iget-object v1, v0, LX/1hM;->Q:Ljava/lang/String;

    move-object v0, v1

    .line 1333723
    iput-object v0, p0, LX/8Lj;->r:Ljava/lang/String;

    .line 1333724
    :goto_0
    return-void

    .line 1333725
    :cond_0
    const-string v0, "unknown"

    iput-object v0, p0, LX/8Lj;->r:Ljava/lang/String;

    goto :goto_0
.end method

.method public static D(LX/8Lj;)V
    .locals 5

    .prologue
    .line 1333726
    iget-object v0, p0, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1333727
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v3, v4, :cond_3

    .line 1333728
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_2

    .line 1333729
    :cond_0
    :goto_0
    move v0, v1

    .line 1333730
    if-eqz v0, :cond_1

    .line 1333731
    iget-object v0, p0, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 1333732
    iget-object v1, p0, LX/8Lj;->m:LX/0ad;

    sget-char v2, LX/1aO;->ac:C

    iget-object v3, p0, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f082016

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1333733
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1333734
    iget-object v0, p0, LX/8Lj;->q:LX/1RW;

    const-string v1, "airplane_mode_is_on"

    invoke-virtual {v0, v1}, LX/1RW;->k(Ljava/lang/String;)V

    .line 1333735
    :goto_1
    return-void

    .line 1333736
    :cond_1
    iget-object v0, p0, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f08200a

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1333737
    iget-object v0, p0, LX/8Lj;->q:LX/1RW;

    const-string v1, "no_network"

    invoke-virtual {v0, v1}, LX/1RW;->k(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 1333738
    goto :goto_0

    .line 1333739
    :cond_3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public static c(LX/8Lj;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1333740
    iget-object v0, p0, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1333741
    iget-object v1, p0, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b149c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1333742
    invoke-virtual {v0, v4, v4, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1333743
    iget-object v1, p0, LX/8Lj;->o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1333744
    return-void
.end method
