.class public LX/7X4;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Yb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Xl;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216919
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216920
    iput-object p2, p0, LX/7X4;->a:LX/0Or;

    .line 1216921
    invoke-interface {p3}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.zero.ZERO_RATING_STATE_CHANGED"

    new-instance v2, LX/7X2;

    invoke-direct {v2, p0}, LX/7X2;-><init>(LX/7X4;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/7X4;->b:LX/0Yb;

    .line 1216922
    new-instance v0, LX/7X3;

    invoke-direct {v0, p0}, LX/7X3;-><init>(LX/7X4;)V

    invoke-virtual {p0, v0}, LX/7X4;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216923
    const v0, 0x7f080ea2

    invoke-virtual {p0, v0}, LX/7X4;->setTitle(I)V

    .line 1216924
    invoke-static {p0}, LX/7X4;->e(LX/7X4;)V

    .line 1216925
    return-void
.end method

.method public static b(LX/0QB;)LX/7X4;
    .locals 4

    .prologue
    .line 1216930
    new-instance v2, LX/7X4;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v1, 0x14d1

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-direct {v2, v0, v3, v1}, LX/7X4;-><init>(Landroid/content/Context;LX/0Or;LX/0Xl;)V

    .line 1216931
    return-object v2
.end method

.method public static e(LX/7X4;)V
    .locals 2

    .prologue
    .line 1216926
    iget-object v0, p0, LX/7X4;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1216927
    invoke-virtual {p0}, LX/7X4;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080e8e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7X4;->setSummary(Ljava/lang/CharSequence;)V

    .line 1216928
    :goto_0
    return-void

    .line 1216929
    :cond_0
    invoke-virtual {p0}, LX/7X4;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080e8f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7X4;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
