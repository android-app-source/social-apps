.class public final LX/6gN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1124872
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1124873
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1124874
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1124875
    invoke-static {p0, p1}, LX/6gN;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1124876
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1124877
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1124878
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1124879
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1124880
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/6gN;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1124881
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1124882
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1124883
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 1124884
    const/16 v16, 0x0

    .line 1124885
    const/4 v15, 0x0

    .line 1124886
    const/4 v14, 0x0

    .line 1124887
    const/4 v13, 0x0

    .line 1124888
    const/4 v12, 0x0

    .line 1124889
    const-wide/16 v10, 0x0

    .line 1124890
    const/4 v9, 0x0

    .line 1124891
    const/4 v8, 0x0

    .line 1124892
    const/4 v5, 0x0

    .line 1124893
    const-wide/16 v6, 0x0

    .line 1124894
    const/4 v4, 0x0

    .line 1124895
    const/4 v3, 0x0

    .line 1124896
    const/4 v2, 0x0

    .line 1124897
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_f

    .line 1124898
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1124899
    const/4 v2, 0x0

    .line 1124900
    :goto_0
    return v2

    .line 1124901
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_c

    .line 1124902
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1124903
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1124904
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1124905
    const-string v6, "image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1124906
    invoke-static/range {p0 .. p1}, LX/6gM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1124907
    :cond_1
    const-string v6, "image_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1124908
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1124909
    :cond_2
    const-string v6, "landscape_anchoring"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1124910
    invoke-static/range {p0 .. p1}, LX/6gI;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1124911
    :cond_3
    const-string v6, "landscape_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1124912
    invoke-static/range {p0 .. p1}, LX/6gJ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1124913
    :cond_4
    const-string v6, "landscape_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1124914
    invoke-static/range {p0 .. p1}, LX/6gK;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1124915
    :cond_5
    const-string v6, "opacity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1124916
    const/4 v2, 0x1

    .line 1124917
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1124918
    :cond_6
    const-string v6, "portrait_anchoring"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1124919
    invoke-static/range {p0 .. p1}, LX/6gI;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1124920
    :cond_7
    const-string v6, "portrait_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1124921
    invoke-static/range {p0 .. p1}, LX/6gJ;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1124922
    :cond_8
    const-string v6, "portrait_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1124923
    invoke-static/range {p0 .. p1}, LX/6gK;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1124924
    :cond_9
    const-string v6, "rotation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1124925
    const/4 v2, 0x1

    .line 1124926
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto/16 :goto_1

    .line 1124927
    :cond_a
    const-string v6, "sticker"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1124928
    invoke-static/range {p0 .. p1}, LX/6gS;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1124929
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1124930
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1124931
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124932
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124933
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124934
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124935
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1124936
    if-eqz v3, :cond_d

    .line 1124937
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124938
    :cond_d
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1124939
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1124940
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1124941
    if-eqz v8, :cond_e

    .line 1124942
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124943
    :cond_e
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1124944
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v17, v14

    move/from16 v18, v15

    move/from16 v19, v16

    move v14, v9

    move v15, v12

    move/from16 v16, v13

    move v13, v8

    move v12, v5

    move v9, v4

    move v8, v2

    move-wide v4, v10

    move-wide v10, v6

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    .line 1124945
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1124946
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124947
    if-eqz v0, :cond_1

    .line 1124948
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124949
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1124950
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1124951
    if-eqz v1, :cond_0

    .line 1124952
    const-string v3, "uri"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124953
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1124954
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1124955
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1124956
    if-eqz v0, :cond_2

    .line 1124957
    const-string v0, "image_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124958
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1124959
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124960
    if-eqz v0, :cond_3

    .line 1124961
    const-string v1, "landscape_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124962
    invoke-static {p0, v0, p2}, LX/6gI;->a(LX/15i;ILX/0nX;)V

    .line 1124963
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124964
    if-eqz v0, :cond_4

    .line 1124965
    const-string v1, "landscape_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124966
    invoke-static {p0, v0, p2}, LX/6gJ;->a(LX/15i;ILX/0nX;)V

    .line 1124967
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124968
    if-eqz v0, :cond_5

    .line 1124969
    const-string v1, "landscape_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124970
    invoke-static {p0, v0, p2}, LX/6gK;->a(LX/15i;ILX/0nX;)V

    .line 1124971
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1124972
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 1124973
    const-string v2, "opacity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124974
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1124975
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124976
    if-eqz v0, :cond_7

    .line 1124977
    const-string v1, "portrait_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124978
    invoke-static {p0, v0, p2}, LX/6gI;->a(LX/15i;ILX/0nX;)V

    .line 1124979
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124980
    if-eqz v0, :cond_8

    .line 1124981
    const-string v1, "portrait_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124982
    invoke-static {p0, v0, p2}, LX/6gJ;->a(LX/15i;ILX/0nX;)V

    .line 1124983
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124984
    if-eqz v0, :cond_9

    .line 1124985
    const-string v1, "portrait_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124986
    invoke-static {p0, v0, p2}, LX/6gK;->a(LX/15i;ILX/0nX;)V

    .line 1124987
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1124988
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_a

    .line 1124989
    const-string v2, "rotation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124990
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1124991
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1124992
    if-eqz v0, :cond_b

    .line 1124993
    const-string v1, "sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124994
    invoke-static {p0, v0, p2}, LX/6gS;->a(LX/15i;ILX/0nX;)V

    .line 1124995
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1124996
    return-void
.end method
