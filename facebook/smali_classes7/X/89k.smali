.class public final LX/89k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/89g;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:LX/21y;

.field public j:Ljava/lang/Boolean;

.field public k:LX/21C;

.field public l:Lcom/facebook/graphql/model/GraphQLComment;

.field public m:Lcom/facebook/graphql/model/GraphQLComment;

.field public n:Ljava/lang/Boolean;

.field public o:I

.field public p:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1305166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;
    .locals 1

    .prologue
    .line 1305162
    invoke-static {p1}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1305163
    if-eqz v0, :cond_0

    .line 1305164
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/89k;->d:Ljava/lang/String;

    .line 1305165
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/89k;
    .locals 0

    .prologue
    .line 1305129
    iput-object p1, p0, LX/89k;->b:Ljava/lang/String;

    .line 1305130
    return-object p0
.end method

.method public final a(Z)LX/89k;
    .locals 1

    .prologue
    .line 1305160
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/89k;->j:Ljava/lang/Boolean;

    .line 1305161
    return-object p0
.end method

.method public final a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;
    .locals 2

    .prologue
    .line 1305158
    new-instance v0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;-><init>(LX/89k;)V

    .line 1305159
    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;
    .locals 1

    .prologue
    .line 1305152
    invoke-static {p1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1305153
    if-nez v0, :cond_0

    .line 1305154
    const/4 v0, 0x0

    .line 1305155
    :goto_0
    move-object v0, v0

    .line 1305156
    iput-object v0, p0, LX/89k;->l:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1305157
    return-object p0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/89k;
    .locals 0

    .prologue
    .line 1305150
    iput-object p1, p0, LX/89k;->c:Ljava/lang/String;

    .line 1305151
    return-object p0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;
    .locals 1

    .prologue
    .line 1305148
    invoke-static {p1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    iput-object v0, p0, LX/89k;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1305149
    return-object p0
.end method

.method public final d(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;
    .locals 1

    .prologue
    .line 1305142
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1305143
    :cond_0
    const/4 v0, 0x0

    .line 1305144
    :goto_0
    move-object v0, v0

    .line 1305145
    if-eqz v0, :cond_1

    .line 1305146
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/89k;->e:Ljava/lang/String;

    .line 1305147
    :cond_1
    return-object p0

    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1305133
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1305134
    if-eqz v0, :cond_1

    .line 1305135
    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    .line 1305136
    iput-object v0, p0, LX/89k;->i:LX/21y;

    .line 1305137
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1305138
    iput-object v0, p0, LX/89k;->g:Ljava/lang/String;

    .line 1305139
    :cond_0
    :goto_0
    return-object p0

    .line 1305140
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1305141
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/89k;->e(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    goto :goto_0
.end method

.method public final f(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1305131
    invoke-static {p1}, LX/16z;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/89k;->a(Z)LX/89k;

    .line 1305132
    return-object p0
.end method
