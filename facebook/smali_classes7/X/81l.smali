.class public final LX/81l;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1286578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1286567
    if-nez p0, :cond_0

    .line 1286568
    const/4 v0, 0x0

    .line 1286569
    :goto_0
    return-object v0

    .line 1286570
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1286571
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1286572
    iput v1, v0, LX/2dc;->c:I

    .line 1286573
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1286574
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1286575
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1286576
    iput v1, v0, LX/2dc;->i:I

    .line 1286577
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel;)Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;
    .locals 14

    .prologue
    .line 1286451
    if-nez p0, :cond_0

    .line 1286452
    const/4 v0, 0x0

    .line 1286453
    :goto_0
    return-object v0

    .line 1286454
    :cond_0
    new-instance v2, LX/4Xk;

    invoke-direct {v2}, LX/4Xk;-><init>()V

    .line 1286455
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1286456
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1286457
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1286458
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;

    .line 1286459
    if-nez v0, :cond_3

    .line 1286460
    const/4 v4, 0x0

    .line 1286461
    :goto_2
    move-object v0, v4

    .line 1286462
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1286463
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1286464
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1286465
    iput-object v0, v2, LX/4Xk;->b:LX/0Px;

    .line 1286466
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel;->b()LX/0us;

    move-result-object v0

    .line 1286467
    if-nez v0, :cond_d

    .line 1286468
    const/4 v1, 0x0

    .line 1286469
    :goto_3
    move-object v0, v1

    .line 1286470
    iput-object v0, v2, LX/4Xk;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1286471
    invoke-virtual {v2}, LX/4Xk;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v0

    goto :goto_0

    .line 1286472
    :cond_3
    new-instance v4, LX/4Xl;

    invoke-direct {v4}, LX/4Xl;-><init>()V

    .line 1286473
    invoke-virtual {v0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->a()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v5

    .line 1286474
    if-nez v5, :cond_4

    .line 1286475
    const/4 v6, 0x0

    .line 1286476
    :goto_4
    move-object v5, v6

    .line 1286477
    iput-object v5, v4, LX/4Xl;->b:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1286478
    invoke-virtual {v0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1286479
    iput-object v5, v4, LX/4Xl;->c:Ljava/lang/String;

    .line 1286480
    new-instance v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    invoke-direct {v5, v4}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;-><init>(LX/4Xl;)V

    .line 1286481
    move-object v4, v5

    .line 1286482
    goto :goto_2

    .line 1286483
    :cond_4
    new-instance v6, LX/4Wm;

    invoke-direct {v6}, LX/4Wm;-><init>()V

    .line 1286484
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    .line 1286485
    iput-object v7, v6, LX/4Wm;->t:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1286486
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->c()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    move-result-object v7

    .line 1286487
    if-nez v7, :cond_5

    .line 1286488
    const/4 v8, 0x0

    .line 1286489
    :goto_5
    move-object v7, v8

    .line 1286490
    iput-object v7, v6, LX/4Wm;->v:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 1286491
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->d()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v7

    .line 1286492
    if-nez v7, :cond_7

    .line 1286493
    const/4 v8, 0x0

    .line 1286494
    :goto_6
    move-object v7, v8

    .line 1286495
    iput-object v7, v6, LX/4Wm;->J:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 1286496
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e()I

    move-result v7

    .line 1286497
    iput v7, v6, LX/4Wm;->K:I

    .line 1286498
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->ey_()Ljava/lang/String;

    move-result-object v7

    .line 1286499
    iput-object v7, v6, LX/4Wm;->R:Ljava/lang/String;

    .line 1286500
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->ez_()Ljava/lang/String;

    move-result-object v7

    .line 1286501
    iput-object v7, v6, LX/4Wm;->ac:Ljava/lang/String;

    .line 1286502
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v7

    .line 1286503
    if-nez v7, :cond_b

    .line 1286504
    const/4 v8, 0x0

    .line 1286505
    :goto_7
    move-object v7, v8

    .line 1286506
    iput-object v7, v6, LX/4Wm;->af:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1286507
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/81l;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1286508
    iput-object v7, v6, LX/4Wm;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1286509
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v7

    .line 1286510
    iput-object v7, v6, LX/4Wm;->bk:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1286511
    invoke-virtual {v5}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m()LX/174;

    move-result-object v7

    .line 1286512
    if-nez v7, :cond_c

    .line 1286513
    const/4 v8, 0x0

    .line 1286514
    :goto_8
    move-object v7, v8

    .line 1286515
    iput-object v7, v6, LX/4Wm;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1286516
    invoke-virtual {v6}, LX/4Wm;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v6

    goto :goto_4

    .line 1286517
    :cond_5
    new-instance v8, LX/4WQ;

    invoke-direct {v8}, LX/4WQ;-><init>()V

    .line 1286518
    invoke-virtual {v7}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->a()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    move-result-object v9

    .line 1286519
    if-nez v9, :cond_6

    .line 1286520
    const/4 v10, 0x0

    .line 1286521
    :goto_9
    move-object v9, v10

    .line 1286522
    iput-object v9, v8, LX/4WQ;->c:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1286523
    invoke-virtual {v8}, LX/4WQ;->a()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v8

    goto :goto_5

    .line 1286524
    :cond_6
    new-instance v10, LX/4Xy;

    invoke-direct {v10}, LX/4Xy;-><init>()V

    .line 1286525
    invoke-virtual {v9}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/81l;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1286526
    iput-object v7, v10, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1286527
    invoke-virtual {v10}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v10

    goto :goto_9

    .line 1286528
    :cond_7
    new-instance v10, LX/4Wo;

    invoke-direct {v10}, LX/4Wo;-><init>()V

    .line 1286529
    invoke-virtual {v7}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->a()I

    move-result v8

    .line 1286530
    iput v8, v10, LX/4Wo;->b:I

    .line 1286531
    invoke-virtual {v7}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->b()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 1286532
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 1286533
    const/4 v8, 0x0

    move v9, v8

    :goto_a
    invoke-virtual {v7}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_8

    .line 1286534
    invoke-virtual {v7}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;

    .line 1286535
    if-nez v8, :cond_a

    .line 1286536
    const/4 v12, 0x0

    .line 1286537
    :goto_b
    move-object v8, v12

    .line 1286538
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1286539
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_a

    .line 1286540
    :cond_8
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1286541
    iput-object v8, v10, LX/4Wo;->d:LX/0Px;

    .line 1286542
    :cond_9
    invoke-virtual {v10}, LX/4Wo;->a()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v8

    goto/16 :goto_6

    .line 1286543
    :cond_a
    new-instance v12, LX/33O;

    invoke-direct {v12}, LX/33O;-><init>()V

    .line 1286544
    invoke-virtual {v8}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->a()LX/1Fb;

    move-result-object v13

    invoke-static {v13}, LX/81l;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    .line 1286545
    iput-object v13, v12, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1286546
    invoke-virtual {v12}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v12

    goto :goto_b

    .line 1286547
    :cond_b
    new-instance v8, LX/4Wm;

    invoke-direct {v8}, LX/4Wm;-><init>()V

    .line 1286548
    invoke-virtual {v7}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v9

    .line 1286549
    iput-object v9, v8, LX/4Wm;->t:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1286550
    invoke-virtual {v7}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;->c()Ljava/lang/String;

    move-result-object v9

    .line 1286551
    iput-object v9, v8, LX/4Wm;->R:Ljava/lang/String;

    .line 1286552
    invoke-virtual {v8}, LX/4Wm;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v8

    goto/16 :goto_7

    .line 1286553
    :cond_c
    new-instance v8, LX/173;

    invoke-direct {v8}, LX/173;-><init>()V

    .line 1286554
    invoke-interface {v7}, LX/174;->a()Ljava/lang/String;

    move-result-object v9

    .line 1286555
    iput-object v9, v8, LX/173;->f:Ljava/lang/String;

    .line 1286556
    invoke-virtual {v8}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    goto/16 :goto_8

    .line 1286557
    :cond_d
    new-instance v1, LX/17L;

    invoke-direct {v1}, LX/17L;-><init>()V

    .line 1286558
    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    .line 1286559
    iput-object v3, v1, LX/17L;->c:Ljava/lang/String;

    .line 1286560
    invoke-interface {v0}, LX/0us;->b()Z

    move-result v3

    .line 1286561
    iput-boolean v3, v1, LX/17L;->d:Z

    .line 1286562
    invoke-interface {v0}, LX/0us;->c()Z

    move-result v3

    .line 1286563
    iput-boolean v3, v1, LX/17L;->e:Z

    .line 1286564
    invoke-interface {v0}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v3

    .line 1286565
    iput-object v3, v1, LX/17L;->f:Ljava/lang/String;

    .line 1286566
    invoke-virtual {v1}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    goto/16 :goto_3
.end method
