.class public final enum LX/7kr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7kr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7kr;

.field public static final enum NONEXISTANT_PHOTO:LX/7kr;

.field public static final enum TOO_MANY_PHOTOS:LX/7kr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1232624
    new-instance v0, LX/7kr;

    const-string v1, "NONEXISTANT_PHOTO"

    invoke-direct {v0, v1, v2}, LX/7kr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7kr;->NONEXISTANT_PHOTO:LX/7kr;

    .line 1232625
    new-instance v0, LX/7kr;

    const-string v1, "TOO_MANY_PHOTOS"

    invoke-direct {v0, v1, v3}, LX/7kr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7kr;->TOO_MANY_PHOTOS:LX/7kr;

    .line 1232626
    const/4 v0, 0x2

    new-array v0, v0, [LX/7kr;

    sget-object v1, LX/7kr;->NONEXISTANT_PHOTO:LX/7kr;

    aput-object v1, v0, v2

    sget-object v1, LX/7kr;->TOO_MANY_PHOTOS:LX/7kr;

    aput-object v1, v0, v3

    sput-object v0, LX/7kr;->$VALUES:[LX/7kr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1232629
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7kr;
    .locals 1

    .prologue
    .line 1232628
    const-class v0, LX/7kr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7kr;

    return-object v0
.end method

.method public static values()[LX/7kr;
    .locals 1

    .prologue
    .line 1232627
    sget-object v0, LX/7kr;->$VALUES:[LX/7kr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7kr;

    return-object v0
.end method
