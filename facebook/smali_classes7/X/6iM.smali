.class public LX/6iM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

.field public b:LX/0rS;

.field public c:LX/0rS;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public e:J

.field public f:Z

.field public g:I

.field public h:J

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1129382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129383
    iput-wide v2, p0, LX/6iM;->e:J

    .line 1129384
    const/16 v0, 0x32

    iput v0, p0, LX/6iM;->g:I

    .line 1129385
    iput-wide v2, p0, LX/6iM;->h:J

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/FetchThreadParams;)LX/6iM;
    .locals 4

    .prologue
    .line 1129363
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v0, v0

    .line 1129364
    iput-object v0, p0, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 1129365
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v0, v0

    .line 1129366
    iput-object v0, p0, LX/6iM;->b:LX/0rS;

    .line 1129367
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->c:LX/0rS;

    move-object v0, v0

    .line 1129368
    iput-object v0, p0, LX/6iM;->c:LX/0rS;

    .line 1129369
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->d:LX/0Px;

    move-object v0, v0

    .line 1129370
    iput-object v0, p0, LX/6iM;->d:LX/0Px;

    .line 1129371
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->e:Z

    move v0, v0

    .line 1129372
    iput-boolean v0, p0, LX/6iM;->f:Z

    .line 1129373
    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->f:J

    move-wide v0, v2

    .line 1129374
    iput-wide v0, p0, LX/6iM;->e:J

    .line 1129375
    iget v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v0, v0

    .line 1129376
    iput v0, p0, LX/6iM;->g:I

    .line 1129377
    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->h:J

    move-wide v0, v2

    .line 1129378
    iput-wide v0, p0, LX/6iM;->h:J

    .line 1129379
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->i:Z

    move v0, v0

    .line 1129380
    iput-boolean v0, p0, LX/6iM;->i:Z

    .line 1129381
    return-object p0
.end method

.method public final j()Lcom/facebook/messaging/service/model/FetchThreadParams;
    .locals 1

    .prologue
    .line 1129362
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/service/model/FetchThreadParams;-><init>(LX/6iM;)V

    return-object v0
.end method
