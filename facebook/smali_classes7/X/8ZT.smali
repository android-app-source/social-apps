.class public final LX/8ZT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1364469
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1364470
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1364471
    :goto_0
    return v1

    .line 1364472
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1364473
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_6

    .line 1364474
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1364475
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1364476
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1364477
    const-string v5, "aggregated_ranges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1364478
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1364479
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    .line 1364480
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_2

    .line 1364481
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1364482
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_10

    .line 1364483
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1364484
    :goto_3
    move v4, v5

    .line 1364485
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1364486
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1364487
    goto :goto_1

    .line 1364488
    :cond_3
    const-string v5, "ranges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1364489
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1364490
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 1364491
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1364492
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1364493
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_18

    .line 1364494
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1364495
    :goto_5
    move v4, v5

    .line 1364496
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1364497
    :cond_4
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1364498
    goto/16 :goto_1

    .line 1364499
    :cond_5
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1364500
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1364501
    :cond_6
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1364502
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1364503
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1364504
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1364505
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    goto/16 :goto_1

    .line 1364506
    :cond_8
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 1364507
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1364508
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1364509
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_8

    if-eqz v11, :cond_8

    .line 1364510
    const-string v12, "length"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1364511
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_6

    .line 1364512
    :cond_9
    const-string v12, "offset"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1364513
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v6

    goto :goto_6

    .line 1364514
    :cond_a
    const-string v12, "sample_entities"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1364515
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1364516
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_b

    .line 1364517
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_b

    .line 1364518
    invoke-static {p0, p1}, LX/8ZK;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1364519
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1364520
    :cond_b
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 1364521
    goto :goto_6

    .line 1364522
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1364523
    :cond_d
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1364524
    if-eqz v7, :cond_e

    .line 1364525
    invoke-virtual {p1, v5, v10, v5}, LX/186;->a(III)V

    .line 1364526
    :cond_e
    if-eqz v4, :cond_f

    .line 1364527
    invoke-virtual {p1, v6, v9, v5}, LX/186;->a(III)V

    .line 1364528
    :cond_f
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v8}, LX/186;->b(II)V

    .line 1364529
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_10
    move v4, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto/16 :goto_6

    .line 1364530
    :cond_11
    const-string v12, "length"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 1364531
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v9, v7

    move v7, v6

    .line 1364532
    :cond_12
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_15

    .line 1364533
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1364534
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1364535
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_12

    if-eqz v11, :cond_12

    .line 1364536
    const-string v12, "entity"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 1364537
    invoke-static {p0, p1}, LX/8ZS;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_8

    .line 1364538
    :cond_13
    const-string v12, "offset"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 1364539
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v6

    goto :goto_8

    .line 1364540
    :cond_14
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_8

    .line 1364541
    :cond_15
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1364542
    invoke-virtual {p1, v5, v10}, LX/186;->b(II)V

    .line 1364543
    if-eqz v7, :cond_16

    .line 1364544
    invoke-virtual {p1, v6, v9, v5}, LX/186;->a(III)V

    .line 1364545
    :cond_16
    if-eqz v4, :cond_17

    .line 1364546
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v8, v5}, LX/186;->a(III)V

    .line 1364547
    :cond_17
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_5

    :cond_18
    move v4, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto :goto_8
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1364548
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1364549
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1364550
    if-eqz v0, :cond_5

    .line 1364551
    const-string v1, "aggregated_ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364552
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1364553
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 1364554
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 v5, 0x0

    .line 1364555
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1364556
    invoke-virtual {p0, v2, v5, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1364557
    if-eqz v3, :cond_0

    .line 1364558
    const-string v4, "length"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364559
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1364560
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1364561
    if-eqz v3, :cond_1

    .line 1364562
    const-string v4, "offset"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364563
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1364564
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1364565
    if-eqz v3, :cond_3

    .line 1364566
    const-string v4, "sample_entities"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364567
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1364568
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p0, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 1364569
    invoke-virtual {p0, v3, v4}, LX/15i;->q(II)I

    move-result v5

    invoke-static {p0, v5, p2}, LX/8ZK;->a(LX/15i;ILX/0nX;)V

    .line 1364570
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1364571
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1364572
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1364573
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1364574
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1364575
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1364576
    if-eqz v0, :cond_a

    .line 1364577
    const-string v1, "ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364578
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1364579
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 1364580
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 v5, 0x0

    .line 1364581
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1364582
    invoke-virtual {p0, v2, v5}, LX/15i;->g(II)I

    move-result v3

    .line 1364583
    if-eqz v3, :cond_6

    .line 1364584
    const-string v4, "entity"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364585
    invoke-static {p0, v3, p2}, LX/8ZS;->a(LX/15i;ILX/0nX;)V

    .line 1364586
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1364587
    if-eqz v3, :cond_7

    .line 1364588
    const-string v4, "length"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364589
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1364590
    :cond_7
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1364591
    if-eqz v3, :cond_8

    .line 1364592
    const-string v4, "offset"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364593
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1364594
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1364595
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1364596
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1364597
    :cond_a
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364598
    if-eqz v0, :cond_b

    .line 1364599
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364600
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364601
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1364602
    return-void
.end method
