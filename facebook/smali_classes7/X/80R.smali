.class public LX/80R;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0w9;

.field private final e:LX/0sX;


# direct methods
.method public constructor <init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0w9;LX/0So;LX/0ad;LX/0sZ;LX/0sX;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1283029
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 1283030
    iput-object p5, p0, LX/80R;->d:LX/0w9;

    .line 1283031
    move-object/from16 v0, p9

    iput-object v0, p0, LX/80R;->e:LX/0sX;

    .line 1283032
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1283033
    const/4 v0, 0x2

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1283034
    const-string v0, "fetch_friend_list_feed"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1283035
    const-string v0, "FriendListFeedNetworkTime"

    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 1283036
    const v0, 0xa0080

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 1283037
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1283038
    new-instance v0, LX/80P;

    invoke-direct {v0}, LX/80P;-><init>()V

    move-object v0, v0

    .line 1283039
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1283040
    const-string v1, "before_home_story_param"

    const-string v2, "after_home_story_param"

    invoke-static {v0, p1, v1, v2}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1283041
    iget-object v1, p0, LX/80R;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1283042
    iget-object v1, p0, LX/80R;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 1283043
    invoke-static {v0}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 1283044
    const-string v1, "friend_list_id"

    .line 1283045
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1283046
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_home_story_param"

    .line 1283047
    iget v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v3, v3

    .line 1283048
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/80R;->e:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1283049
    return-object v0
.end method
