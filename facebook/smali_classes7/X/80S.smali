.class public LX/80S;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0w9;


# direct methods
.method public constructor <init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0w9;LX/0So;LX/0ad;LX/0sZ;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1283064
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 1283065
    iput-object p5, p0, LX/80S;->d:LX/0w9;

    .line 1283066
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1283067
    const/4 v0, 0x5

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1283050
    const-string v0, "fetch_good_friends_feed"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1283063
    const-string v0, "GoodFriendsFeedNetworkTime"

    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 1283062
    const v0, 0xa009e

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 1283051
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1283052
    new-instance v0, LX/80T;

    invoke-direct {v0}, LX/80T;-><init>()V

    move-object v0, v0

    .line 1283053
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1283054
    const-string v1, "before_home_story_param"

    const-string v2, "after_home_story_param"

    invoke-static {v0, p1, v1, v2}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1283055
    iget-object v1, p0, LX/80S;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1283056
    iget-object v1, p0, LX/80S;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 1283057
    invoke-static {v0}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 1283058
    const-string v1, "first_home_story_param"

    .line 1283059
    iget v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v2, v2

    .line 1283060
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1283061
    return-object v0
.end method
