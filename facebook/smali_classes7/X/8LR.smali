.class public final enum LX/8LR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8LR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8LR;

.field public static final enum COVER_PHOTO:LX/8LR;

.field public static final enum EDIT_MULTIMEDIA:LX/8LR;

.field public static final enum EDIT_POST:LX/8LR;

.field public static final enum LIFE_EVENT:LX/8LR;

.field public static final enum MENU_PHOTO:LX/8LR;

.field public static final enum MULTIMEDIA:LX/8LR;

.field public static final enum PHOTO_REVIEW:LX/8LR;

.field public static final enum PLACE_PHOTO:LX/8LR;

.field public static final enum PRODUCT_IMAGE:LX/8LR;

.field public static final enum PROFILE_INTRO_CARD:LX/8LR;

.field public static final enum PROFILE_PIC:LX/8LR;

.field public static final enum PROFILE_VIDEO:LX/8LR;

.field public static final enum SINGLE_PHOTO:LX/8LR;

.field public static final enum SLIDESHOW:LX/8LR;

.field public static final enum STATUS:LX/8LR;

.field public static final enum TARGET:LX/8LR;

.field public static final enum VIDEO_STATUS:LX/8LR;

.field public static final enum VIDEO_TARGET:LX/8LR;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1332049
    new-instance v0, LX/8LR;

    const-string v1, "SINGLE_PHOTO"

    invoke-direct {v0, v1, v3}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->SINGLE_PHOTO:LX/8LR;

    .line 1332050
    new-instance v0, LX/8LR;

    const-string v1, "STATUS"

    invoke-direct {v0, v1, v4}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->STATUS:LX/8LR;

    .line 1332051
    new-instance v0, LX/8LR;

    const-string v1, "TARGET"

    invoke-direct {v0, v1, v5}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->TARGET:LX/8LR;

    .line 1332052
    new-instance v0, LX/8LR;

    const-string v1, "PHOTO_REVIEW"

    invoke-direct {v0, v1, v6}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->PHOTO_REVIEW:LX/8LR;

    .line 1332053
    new-instance v0, LX/8LR;

    const-string v1, "LIFE_EVENT"

    invoke-direct {v0, v1, v7}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->LIFE_EVENT:LX/8LR;

    .line 1332054
    new-instance v0, LX/8LR;

    const-string v1, "PROFILE_PIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->PROFILE_PIC:LX/8LR;

    .line 1332055
    new-instance v0, LX/8LR;

    const-string v1, "PROFILE_VIDEO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->PROFILE_VIDEO:LX/8LR;

    .line 1332056
    new-instance v0, LX/8LR;

    const-string v1, "PROFILE_INTRO_CARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->PROFILE_INTRO_CARD:LX/8LR;

    .line 1332057
    new-instance v0, LX/8LR;

    const-string v1, "COVER_PHOTO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->COVER_PHOTO:LX/8LR;

    .line 1332058
    new-instance v0, LX/8LR;

    const-string v1, "VIDEO_STATUS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->VIDEO_STATUS:LX/8LR;

    .line 1332059
    new-instance v0, LX/8LR;

    const-string v1, "VIDEO_TARGET"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->VIDEO_TARGET:LX/8LR;

    .line 1332060
    new-instance v0, LX/8LR;

    const-string v1, "PLACE_PHOTO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->PLACE_PHOTO:LX/8LR;

    .line 1332061
    new-instance v0, LX/8LR;

    const-string v1, "MENU_PHOTO"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->MENU_PHOTO:LX/8LR;

    .line 1332062
    new-instance v0, LX/8LR;

    const-string v1, "PRODUCT_IMAGE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->PRODUCT_IMAGE:LX/8LR;

    .line 1332063
    new-instance v0, LX/8LR;

    const-string v1, "MULTIMEDIA"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->MULTIMEDIA:LX/8LR;

    .line 1332064
    new-instance v0, LX/8LR;

    const-string v1, "EDIT_POST"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->EDIT_POST:LX/8LR;

    .line 1332065
    new-instance v0, LX/8LR;

    const-string v1, "EDIT_MULTIMEDIA"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->EDIT_MULTIMEDIA:LX/8LR;

    .line 1332066
    new-instance v0, LX/8LR;

    const-string v1, "SLIDESHOW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/8LR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LR;->SLIDESHOW:LX/8LR;

    .line 1332067
    const/16 v0, 0x12

    new-array v0, v0, [LX/8LR;

    sget-object v1, LX/8LR;->SINGLE_PHOTO:LX/8LR;

    aput-object v1, v0, v3

    sget-object v1, LX/8LR;->STATUS:LX/8LR;

    aput-object v1, v0, v4

    sget-object v1, LX/8LR;->TARGET:LX/8LR;

    aput-object v1, v0, v5

    sget-object v1, LX/8LR;->PHOTO_REVIEW:LX/8LR;

    aput-object v1, v0, v6

    sget-object v1, LX/8LR;->LIFE_EVENT:LX/8LR;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8LR;->PROFILE_PIC:LX/8LR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8LR;->PROFILE_VIDEO:LX/8LR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8LR;->PROFILE_INTRO_CARD:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8LR;->COVER_PHOTO:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8LR;->VIDEO_STATUS:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8LR;->VIDEO_TARGET:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8LR;->PLACE_PHOTO:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8LR;->MENU_PHOTO:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8LR;->PRODUCT_IMAGE:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8LR;->MULTIMEDIA:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8LR;->EDIT_POST:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8LR;->EDIT_MULTIMEDIA:LX/8LR;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8LR;->SLIDESHOW:LX/8LR;

    aput-object v2, v0, v1

    sput-object v0, LX/8LR;->$VALUES:[LX/8LR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1332068
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8LR;
    .locals 1

    .prologue
    .line 1332048
    const-class v0, LX/8LR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8LR;

    return-object v0
.end method

.method public static values()[LX/8LR;
    .locals 1

    .prologue
    .line 1332047
    sget-object v0, LX/8LR;->$VALUES:[LX/8LR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8LR;

    return-object v0
.end method
