.class public LX/7cw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;
    .locals 18

    invoke-static/range {p0 .. p0}, LX/2xb;->b(Landroid/os/Parcel;)I

    move-result v16

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v16

    if-ge v2, v0, :cond_0

    invoke-static/range {p0 .. p0}, LX/2xb;->a(Landroid/os/Parcel;)I

    move-result v2

    invoke-static {v2}, LX/2xb;->a(I)I

    move-result v17

    sparse-switch v17, :sswitch_data_0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->a(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    sget-object v4, Lcom/google/android/gms/nearby/bootstrap/Device;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v4}, LX/2xb;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/nearby/bootstrap/Device;

    move-object v4, v2

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->o(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v13

    goto :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->o(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v14

    goto :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->o(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v15

    goto :goto_0

    :sswitch_6
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->c(Landroid/os/Parcel;I)B

    move-result v7

    goto :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v3

    goto :goto_0

    :sswitch_8
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->g(Landroid/os/Parcel;I)J

    move-result-wide v8

    goto :goto_0

    :sswitch_9
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    :sswitch_a
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->c(Landroid/os/Parcel;I)B

    move-result v11

    goto :goto_0

    :sswitch_b
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2xb;->c(Landroid/os/Parcel;I)B

    move-result v12

    goto :goto_0

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v16

    if-eq v2, v0, :cond_1

    new-instance v2, LX/4sr;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Overread allowed size end="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0}, LX/4sr;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v2

    :cond_1
    new-instance v2, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;

    invoke-direct/range {v2 .. v15}, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;-><init>(ILcom/google/android/gms/nearby/bootstrap/Device;Ljava/lang/String;Ljava/lang/String;BJLjava/lang/String;BBLandroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;)V

    return-object v2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0x3e8 -> :sswitch_7
    .end sparse-switch
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, LX/7cw;->a(Landroid/os/Parcel;)Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;

    return-object v0
.end method
