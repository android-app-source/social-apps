.class public final enum LX/7gW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gW;

.field public static final enum EXPIRE_STATE:LX/7gW;

.field public static final enum SEEN_STATE:LX/7gW;

.field public static final enum SENT_STATE:LX/7gW;


# instance fields
.field private final reason:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1224450
    new-instance v0, LX/7gW;

    const-string v1, "SEEN_STATE"

    const-string v2, "seen_state"

    invoke-direct {v0, v1, v3, v2}, LX/7gW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gW;->SEEN_STATE:LX/7gW;

    .line 1224451
    new-instance v0, LX/7gW;

    const-string v1, "SENT_STATE"

    const-string v2, "sent_state"

    invoke-direct {v0, v1, v4, v2}, LX/7gW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gW;->SENT_STATE:LX/7gW;

    .line 1224452
    new-instance v0, LX/7gW;

    const-string v1, "EXPIRE_STATE"

    const-string v2, "expire_state"

    invoke-direct {v0, v1, v5, v2}, LX/7gW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gW;->EXPIRE_STATE:LX/7gW;

    .line 1224453
    const/4 v0, 0x3

    new-array v0, v0, [LX/7gW;

    sget-object v1, LX/7gW;->SEEN_STATE:LX/7gW;

    aput-object v1, v0, v3

    sget-object v1, LX/7gW;->SENT_STATE:LX/7gW;

    aput-object v1, v0, v4

    sget-object v1, LX/7gW;->EXPIRE_STATE:LX/7gW;

    aput-object v1, v0, v5

    sput-object v0, LX/7gW;->$VALUES:[LX/7gW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224454
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224455
    iput-object p3, p0, LX/7gW;->reason:Ljava/lang/String;

    .line 1224456
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gW;
    .locals 1

    .prologue
    .line 1224457
    const-class v0, LX/7gW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gW;

    return-object v0
.end method

.method public static values()[LX/7gW;
    .locals 1

    .prologue
    .line 1224458
    sget-object v0, LX/7gW;->$VALUES:[LX/7gW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gW;

    return-object v0
.end method


# virtual methods
.method public final getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224459
    iget-object v0, p0, LX/7gW;->reason:Ljava/lang/String;

    return-object v0
.end method
