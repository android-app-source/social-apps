.class public final LX/8fi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1385761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 1385762
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1385763
    iget-object v1, p0, LX/8fi;->a:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1385764
    iget-object v3, p0, LX/8fi;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1385765
    iget-object v5, p0, LX/8fi;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1385766
    iget-object v6, p0, LX/8fi;->d:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1385767
    iget-object v7, p0, LX/8fi;->e:LX/0Px;

    invoke-virtual {v0, v7}, LX/186;->c(Ljava/util/List;)I

    move-result v7

    .line 1385768
    iget-object v8, p0, LX/8fi;->f:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1385769
    const/4 v9, 0x6

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1385770
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1385771
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1385772
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1385773
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1385774
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1385775
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1385776
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1385777
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1385778
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1385779
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1385780
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1385781
    new-instance v1, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;-><init>(LX/15i;)V

    .line 1385782
    return-object v1
.end method
