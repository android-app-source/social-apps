.class public LX/6k0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final newMessage:LX/6k4;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1134214
    new-instance v0, LX/1sv;

    const-string v1, "DeltaMontageMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6k0;->b:LX/1sv;

    .line 1134215
    new-instance v0, LX/1sw;

    const-string v1, "newMessage"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k0;->c:LX/1sw;

    .line 1134216
    sput-boolean v3, LX/6k0;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6k4;)V
    .locals 0

    .prologue
    .line 1134211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134212
    iput-object p1, p0, LX/6k0;->newMessage:LX/6k4;

    .line 1134213
    return-void
.end method

.method public static a(LX/6k0;)V
    .locals 4

    .prologue
    .line 1134208
    iget-object v0, p0, LX/6k0;->newMessage:LX/6k4;

    if-nez v0, :cond_0

    .line 1134209
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'newMessage\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6k0;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134210
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1134188
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1134189
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1134190
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1134191
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaMontageMessage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1134192
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134193
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134194
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134195
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134196
    const-string v4, "newMessage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134197
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134198
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134199
    iget-object v0, p0, LX/6k0;->newMessage:LX/6k4;

    if-nez v0, :cond_3

    .line 1134200
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134201
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134202
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134203
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1134204
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1134205
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1134206
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 1134207
    :cond_3
    iget-object v0, p0, LX/6k0;->newMessage:LX/6k4;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1134180
    invoke-static {p0}, LX/6k0;->a(LX/6k0;)V

    .line 1134181
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1134182
    iget-object v0, p0, LX/6k0;->newMessage:LX/6k4;

    if-eqz v0, :cond_0

    .line 1134183
    sget-object v0, LX/6k0;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134184
    iget-object v0, p0, LX/6k0;->newMessage:LX/6k4;

    invoke-virtual {v0, p1}, LX/6k4;->a(LX/1su;)V

    .line 1134185
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1134186
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1134187
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1134165
    if-nez p1, :cond_1

    .line 1134166
    :cond_0
    :goto_0
    return v0

    .line 1134167
    :cond_1
    instance-of v1, p1, LX/6k0;

    if-eqz v1, :cond_0

    .line 1134168
    check-cast p1, LX/6k0;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1134169
    if-nez p1, :cond_3

    .line 1134170
    :cond_2
    :goto_1
    move v0, v2

    .line 1134171
    goto :goto_0

    .line 1134172
    :cond_3
    iget-object v0, p0, LX/6k0;->newMessage:LX/6k4;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1134173
    :goto_2
    iget-object v3, p1, LX/6k0;->newMessage:LX/6k4;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1134174
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1134175
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134176
    iget-object v0, p0, LX/6k0;->newMessage:LX/6k4;

    iget-object v3, p1, LX/6k0;->newMessage:LX/6k4;

    invoke-virtual {v0, v3}, LX/6k4;->a(LX/6k4;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1134177
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1134178
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1134179
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1134164
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1134161
    sget-boolean v0, LX/6k0;->a:Z

    .line 1134162
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6k0;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1134163
    return-object v0
.end method
