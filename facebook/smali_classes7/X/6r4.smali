.class public final LX/6r4;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/payments/contactinfo/model/NameContactInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public final synthetic b:LX/6r5;


# direct methods
.method public constructor <init>(LX/6r5;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1151407
    iput-object p1, p0, LX/6r4;->b:LX/6r5;

    iput-object p2, p0, LX/6r4;->a:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 1151412
    iget-object v0, p0, LX/6r4;->b:LX/6r5;

    iget-object v0, v0, LX/6r5;->g:LX/6qb;

    invoke-interface {v0}, LX/6qb;->a()V

    .line 1151413
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1151408
    check-cast p1, Lcom/facebook/payments/contactinfo/model/NameContactInfo;

    .line 1151409
    iget-object v0, p0, LX/6r4;->a:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1151410
    iget-object v0, p0, LX/6r4;->b:LX/6r5;

    iget-object v0, v0, LX/6r5;->g:LX/6qb;

    invoke-interface {v0, p1}, LX/6qb;->a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V

    .line 1151411
    :cond_0
    return-void
.end method
