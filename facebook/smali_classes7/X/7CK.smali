.class public final enum LX/7CK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7CK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7CK;

.field public static final enum BACK_BUTTON:LX/7CK;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1180448
    new-instance v0, LX/7CK;

    const-string v1, "BACK_BUTTON"

    const-string v2, "back_button"

    invoke-direct {v0, v1, v3, v2}, LX/7CK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CK;->BACK_BUTTON:LX/7CK;

    .line 1180449
    const/4 v0, 0x1

    new-array v0, v0, [LX/7CK;

    sget-object v1, LX/7CK;->BACK_BUTTON:LX/7CK;

    aput-object v1, v0, v3

    sput-object v0, LX/7CK;->$VALUES:[LX/7CK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1180450
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1180451
    iput-object p3, p0, LX/7CK;->name:Ljava/lang/String;

    .line 1180452
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7CK;
    .locals 1

    .prologue
    .line 1180447
    const-class v0, LX/7CK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7CK;

    return-object v0
.end method

.method public static values()[LX/7CK;
    .locals 1

    .prologue
    .line 1180446
    sget-object v0, LX/7CK;->$VALUES:[LX/7CK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7CK;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1180445
    iget-object v0, p0, LX/7CK;->name:Ljava/lang/String;

    return-object v0
.end method
