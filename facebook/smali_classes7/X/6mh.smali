.class public LX/6mh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final time:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1146386
    new-instance v0, LX/1sv;

    const-string v1, "TimeSyncRequest"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mh;->b:LX/1sv;

    .line 1146387
    new-instance v0, LX/1sw;

    const-string v1, "time"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mh;->c:LX/1sw;

    .line 1146388
    sput-boolean v3, LX/6mh;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1146383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146384
    iput-object p1, p0, LX/6mh;->time:Ljava/lang/Long;

    .line 1146385
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1146337
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1146338
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1146339
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1146340
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TimeSyncRequest"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146341
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146342
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146343
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146344
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146345
    const-string v4, "time"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146346
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146347
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146348
    iget-object v0, p0, LX/6mh;->time:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 1146349
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146350
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146351
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146352
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1146353
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1146354
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1146355
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 1146356
    :cond_3
    iget-object v0, p0, LX/6mh;->time:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1146376
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146377
    iget-object v0, p0, LX/6mh;->time:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1146378
    sget-object v0, LX/6mh;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146379
    iget-object v0, p0, LX/6mh;->time:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146380
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146381
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146382
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1146361
    if-nez p1, :cond_1

    .line 1146362
    :cond_0
    :goto_0
    return v0

    .line 1146363
    :cond_1
    instance-of v1, p1, LX/6mh;

    if-eqz v1, :cond_0

    .line 1146364
    check-cast p1, LX/6mh;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146365
    if-nez p1, :cond_3

    .line 1146366
    :cond_2
    :goto_1
    move v0, v2

    .line 1146367
    goto :goto_0

    .line 1146368
    :cond_3
    iget-object v0, p0, LX/6mh;->time:Ljava/lang/Long;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1146369
    :goto_2
    iget-object v3, p1, LX/6mh;->time:Ljava/lang/Long;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1146370
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1146371
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146372
    iget-object v0, p0, LX/6mh;->time:Ljava/lang/Long;

    iget-object v3, p1, LX/6mh;->time:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1146373
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1146374
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1146375
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1146360
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146357
    sget-boolean v0, LX/6mh;->a:Z

    .line 1146358
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mh;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1146359
    return-object v0
.end method
