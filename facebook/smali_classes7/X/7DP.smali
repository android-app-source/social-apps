.class public LX/7DP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7D0;


# instance fields
.field private final a:LX/5Pc;

.field private b:LX/5Pb;

.field public c:LX/5PR;

.field public d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1182906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182907
    iput v0, p0, LX/7DP;->d:I

    .line 1182908
    iput v0, p0, LX/7DP;->e:I

    .line 1182909
    iput v0, p0, LX/7DP;->f:I

    .line 1182910
    new-instance v0, LX/5Pd;

    invoke-direct {v0, p1}, LX/5Pd;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/7DP;->a:LX/5Pc;

    .line 1182911
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1182905
    iget v0, p0, LX/7DP;->d:I

    return v0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1182904
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1182853
    return-void
.end method

.method public final a(LX/19o;)V
    .locals 0

    .prologue
    .line 1182903
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1182881
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1182882
    :cond_0
    :goto_0
    return-void

    .line 1182883
    :cond_1
    iget v0, p0, LX/7DP;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 1182884
    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x46180400    # 9729.0f

    const/4 v1, 0x0

    const/16 v3, 0xde1

    .line 1182885
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v1, v1, v1, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1182886
    new-array v0, v6, [I

    .line 1182887
    invoke-static {v6, v0, v5}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1182888
    aget v0, v0, v5

    iput v0, p0, LX/7DP;->d:I

    .line 1182889
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 1182890
    iget v0, p0, LX/7DP;->d:I

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1182891
    const-string v0, "glBindTexture mTextureID"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1182892
    const/16 v0, 0x2801

    invoke-static {v3, v0, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1182893
    const/16 v0, 0x2800

    invoke-static {v3, v0, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1182894
    const/16 v0, 0x2802

    const v1, 0x812f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1182895
    const/16 v0, 0x2803

    const v1, 0x812f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1182896
    const-string v0, "glTexParameter"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1182897
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1182898
    const/16 v0, 0x404

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 1182899
    const-string v0, "glCullFace"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1182900
    :cond_2
    const/16 v0, 0xde1

    invoke-static {v0, v2, p1, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 1182901
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "glTexImage2D textureId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/7DP;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1182902
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V
    .locals 2

    .prologue
    .line 1182875
    iget-object v0, p1, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mProjectionType:Ljava/lang/String;

    move-object v0, v0

    .line 1182876
    invoke-static {v0}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v0

    .line 1182877
    sget-object v1, LX/7DO;->a:[I

    invoke-virtual {v0}, LX/19o;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1182878
    :goto_0
    return-void

    .line 1182879
    :pswitch_0
    invoke-static {p1}, LX/7D5;->a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7DP;->c:LX/5PR;

    goto :goto_0

    .line 1182880
    :pswitch_1
    invoke-static {p1}, LX/7D4;->a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7DP;->c:LX/5PR;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V
    .locals 0

    .prologue
    .line 1182874
    return-void
.end method

.method public final a([F[F[FII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1182864
    iget-object v0, p0, LX/7DP;->c:LX/5PR;

    if-nez v0, :cond_0

    .line 1182865
    :goto_0
    return-void

    .line 1182866
    :cond_0
    iget v0, p0, LX/7DP;->e:I

    if-ne p4, v0, :cond_1

    iget v0, p0, LX/7DP;->f:I

    if-eq p5, v0, :cond_2

    .line 1182867
    :cond_1
    invoke-static {v1, v1, p4, p5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1182868
    iput p4, p0, LX/7DP;->e:I

    .line 1182869
    iput p5, p0, LX/7DP;->f:I

    .line 1182870
    :cond_2
    const/16 v0, 0x10

    new-array v0, v0, [F

    move-object v2, p2

    move v3, v1

    move-object v4, p1

    move v5, v1

    .line 1182871
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1182872
    const/16 v2, 0x4000

    invoke-static {v2}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1182873
    iget-object v2, p0, LX/7DP;->b:LX/5Pb;

    invoke-virtual {v2}, LX/5Pb;->a()LX/5Pa;

    move-result-object v2

    const-string v3, "sTexture"

    const/16 v4, 0xde1

    iget v5, p0, LX/7DP;->d:I

    invoke-virtual {v2, v3, v1, v4, v5}, LX/5Pa;->a(Ljava/lang/String;III)LX/5Pa;

    move-result-object v1

    const-string v2, "uMVPMatrix"

    invoke-virtual {v1, v2, v0}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uSTMatrix"

    invoke-virtual {v0, v1, p3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, LX/7DP;->c:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1182862
    iget-object v0, p0, LX/7DP;->a:LX/5Pc;

    const v1, 0x7f07004f

    const v2, 0x7f07004e

    invoke-interface {v0, v1, v2}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7DP;->b:LX/5Pb;

    .line 1182863
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1182856
    iget v0, p0, LX/7DP;->d:I

    if-eq v0, v2, :cond_0

    .line 1182857
    new-array v0, v4, [I

    .line 1182858
    iget v1, p0, LX/7DP;->d:I

    aput v1, v0, v3

    .line 1182859
    invoke-static {v4, v0, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1182860
    iput v2, p0, LX/7DP;->d:I

    .line 1182861
    :cond_0
    return-void
.end method

.method public final d()LX/7DC;
    .locals 1

    .prologue
    .line 1182855
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/7DD;
    .locals 1

    .prologue
    .line 1182854
    const/4 v0, 0x0

    return-object v0
.end method
