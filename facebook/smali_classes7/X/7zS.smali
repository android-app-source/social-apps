.class public LX/7zS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7zS;


# instance fields
.field public final a:LX/7z2;


# direct methods
.method public constructor <init>(LX/0TD;LX/11H;)V
    .locals 2
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1280982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280983
    new-instance v0, LX/7z2;

    new-instance v1, LX/7zR;

    invoke-direct {v1, p1, p2}, LX/7zR;-><init>(LX/0TD;LX/11H;)V

    invoke-direct {v0, v1}, LX/7z2;-><init>(LX/7zR;)V

    iput-object v0, p0, LX/7zS;->a:LX/7z2;

    .line 1280984
    return-void
.end method

.method public static a(LX/0QB;)LX/7zS;
    .locals 5

    .prologue
    .line 1280985
    sget-object v0, LX/7zS;->b:LX/7zS;

    if-nez v0, :cond_1

    .line 1280986
    const-class v1, LX/7zS;

    monitor-enter v1

    .line 1280987
    :try_start_0
    sget-object v0, LX/7zS;->b:LX/7zS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1280988
    if-eqz v2, :cond_0

    .line 1280989
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1280990
    new-instance p0, LX/7zS;

    invoke-static {v0}, LX/44f;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    invoke-direct {p0, v3, v4}, LX/7zS;-><init>(LX/0TD;LX/11H;)V

    .line 1280991
    move-object v0, p0

    .line 1280992
    sput-object v0, LX/7zS;->b:LX/7zS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1280993
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1280994
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1280995
    :cond_1
    sget-object v0, LX/7zS;->b:LX/7zS;

    return-object v0

    .line 1280996
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1280997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/7z2;
    .locals 1

    .prologue
    .line 1280998
    iget-object v0, p0, LX/7zS;->a:LX/7z2;

    return-object v0
.end method
