.class public LX/79G;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:LX/0ih;

.field private static volatile e:LX/79G;


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1173959
    const-class v0, LX/79G;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/79G;->a:Ljava/lang/String;

    .line 1173960
    sget-object v0, LX/0ig;->bf:LX/0ih;

    sput-object v0, LX/79G;->b:LX/0ih;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1173961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1173962
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1173963
    iput-object v0, p0, LX/79G;->c:LX/0Ot;

    .line 1173964
    return-void
.end method

.method public static a(LX/0QB;)LX/79G;
    .locals 4

    .prologue
    .line 1173965
    sget-object v0, LX/79G;->e:LX/79G;

    if-nez v0, :cond_1

    .line 1173966
    const-class v1, LX/79G;

    monitor-enter v1

    .line 1173967
    :try_start_0
    sget-object v0, LX/79G;->e:LX/79G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1173968
    if-eqz v2, :cond_0

    .line 1173969
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1173970
    new-instance v3, LX/79G;

    invoke-direct {v3}, LX/79G;-><init>()V

    .line 1173971
    const/16 p0, 0xaa5

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1173972
    iput-object p0, v3, LX/79G;->c:LX/0Ot;

    .line 1173973
    move-object v0, v3

    .line 1173974
    sput-object v0, LX/79G;->e:LX/79G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1173975
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1173976
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1173977
    :cond_1
    sget-object v0, LX/79G;->e:LX/79G;

    return-object v0

    .line 1173978
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1173979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/79G;Ljava/lang/String;LX/1rQ;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1173980
    iget-object v0, p0, LX/79G;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v1, LX/79G;->b:LX/0ih;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1173981
    return-void
.end method

.method public static b(ZLjava/lang/String;)LX/1rQ;
    .locals 2

    .prologue
    .line 1173982
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "video_call"

    invoke-virtual {v0, v1, p0}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v0

    const-string v1, "trigger"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/79G;)V
    .locals 2

    .prologue
    .line 1173983
    iget-object v0, p0, LX/79G;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v1, LX/79G;->b:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1173984
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/79G;->d:Z

    .line 1173985
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1173986
    const-string v0, "RTC_ENGINE_END_CALL"

    .line 1173987
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "end_call_reason_string"

    invoke-virtual {v1, v2, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "call_id"

    invoke-virtual {v1, v2, p2, p3}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v1

    move-object v1, v1

    .line 1173988
    invoke-static {p0, v0, v1}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 1173989
    iget-object v0, p0, LX/79G;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v1, LX/79G;->b:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1173990
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1173991
    const-string v0, "RTC_SHARE_VIDEO_RESPONSE"

    .line 1173992
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "escalation_succeeded"

    invoke-virtual {v1, v2, p1}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v1

    move-object v1, v1

    .line 1173993
    invoke-static {p0, v0, v1}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 1173994
    return-void
.end method

.method public final a(ZZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 1173995
    invoke-static {p0}, LX/79G;->f(LX/79G;)V

    .line 1173996
    if-eqz p2, :cond_0

    const-string v0, "UI_JOIN"

    :goto_0
    invoke-static {p1, p3}, LX/79G;->b(ZLjava/lang/String;)LX/1rQ;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 1173997
    return-void

    .line 1173998
    :cond_0
    const-string v0, "UI_JOIN_DECLINE"

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1173999
    const-string v0, "RTC_ENGINE_JOIN"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 1174000
    return-void
.end method
