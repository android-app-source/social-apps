.class public final LX/8WO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;)V
    .locals 0

    .prologue
    .line 1354033
    iput-object p1, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1354016
    iget-object v0, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->l:LX/8WP;

    if-eqz v0, :cond_0

    .line 1354017
    iget-object v0, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->l:LX/8WP;

    const/4 v1, 0x0

    sget-object v2, LX/8TK;->PLAY_ALL_FACEBOOK_FRIENDS:LX/8TK;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/8WP;->a(LX/8Vb;LX/8TK;I)V

    .line 1354018
    :cond_0
    return-void
.end method

.method public final a(LX/8Vb;I)V
    .locals 4

    .prologue
    .line 1354019
    iget-object v0, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->l:LX/8WP;

    if-eqz v0, :cond_0

    sget-object v0, LX/8WQ;->DISABLED:LX/8WQ;

    iget-object v1, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v1, v1, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    invoke-virtual {v0, v1}, LX/8WQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-virtual {v0, p1}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->a(LX/8Vb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1354020
    :cond_0
    :goto_0
    return-void

    .line 1354021
    :cond_1
    sget-object v0, LX/8WQ;->DIRECT_CHALLENGE:LX/8WQ;

    iget-object v1, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v1, v1, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    invoke-virtual {v0, v1}, LX/8WQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1354022
    iget-object v0, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->l:LX/8WP;

    sget-object v1, LX/8TK;->LEADERBOARD_ROW:LX/8TK;

    invoke-interface {v0, p1, v1, p2}, LX/8WP;->a(LX/8Vb;LX/8TK;I)V

    goto :goto_0

    .line 1354023
    :cond_2
    sget-object v0, LX/8WQ;->SHOW_CHALLENGE_POPOVER:LX/8WQ;

    iget-object v1, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v1, v1, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    invoke-virtual {v0, v1}, LX/8WQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TY;

    .line 1354024
    iget-object v1, v0, LX/8TY;->a:LX/0Uh;

    const/16 v2, 0x8f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1354025
    if-eqz v0, :cond_0

    .line 1354026
    iget-object v0, p0, LX/8WO;->a:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    .line 1354027
    new-instance v1, LX/8WN;

    invoke-direct {v1, p0, p2}, LX/8WN;-><init>(LX/8WO;I)V

    .line 1354028
    new-instance v2, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;

    invoke-direct {v2}, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;-><init>()V

    .line 1354029
    iput-object p1, v2, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->r:LX/8Vb;

    .line 1354030
    iput-object v1, v2, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->s:LX/8WN;

    .line 1354031
    move-object v1, v2

    .line 1354032
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "game_challenge_popover"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0
.end method
