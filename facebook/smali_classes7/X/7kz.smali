.class public LX/7kz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1233155
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-static {p0}, LX/7kz;->c(LX/0Px;)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1233156
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    invoke-static {p0}, LX/7kz;->c(LX/0Px;)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/0Px;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1233150
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1233151
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v4

    sget-object v5, LX/4gF;->VIDEO:LX/4gF;

    if-ne v4, v5, :cond_1

    sget-object v4, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1233152
    add-int/lit8 v0, v1, 0x1

    .line 1233153
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1233154
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static d(LX/0Px;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1233145
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1233146
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v4, LX/4gF;->PHOTO:LX/4gF;

    if-ne v0, v4, :cond_1

    .line 1233147
    add-int/lit8 v0, v1, 0x1

    .line 1233148
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1233149
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static f(LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1233143
    if-nez p0, :cond_0

    move v0, v1

    .line 1233144
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v3, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v3, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
