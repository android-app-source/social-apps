.class public LX/8FU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8FU;


# instance fields
.field private final a:LX/0iA;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/0iA;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1317921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1317922
    iput-object p1, p0, LX/8FU;->a:LX/0iA;

    .line 1317923
    iput-object p2, p0, LX/8FU;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1317924
    return-void
.end method

.method public static a(LX/0QB;)LX/8FU;
    .locals 5

    .prologue
    .line 1317925
    sget-object v0, LX/8FU;->c:LX/8FU;

    if-nez v0, :cond_1

    .line 1317926
    const-class v1, LX/8FU;

    monitor-enter v1

    .line 1317927
    :try_start_0
    sget-object v0, LX/8FU;->c:LX/8FU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1317928
    if-eqz v2, :cond_0

    .line 1317929
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1317930
    new-instance p0, LX/8FU;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4}, LX/8FU;-><init>(LX/0iA;Lcom/facebook/content/SecureContextHelper;)V

    .line 1317931
    move-object v0, p0

    .line 1317932
    sput-object v0, LX/8FU;->c:LX/8FU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1317933
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1317934
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1317935
    :cond_1
    sget-object v0, LX/8FU;->c:LX/8FU;

    return-object v0

    .line 1317936
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1317937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1317938
    iget-object v0, p0, LX/8FU;->a:LX/0iA;

    const-class v2, LX/13D;

    invoke-virtual {v0, p1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13D;

    .line 1317939
    if-nez v0, :cond_0

    move v0, v1

    .line 1317940
    :goto_0
    return v0

    .line 1317941
    :cond_0
    invoke-virtual {v0}, LX/13D;->b()Ljava/lang/String;

    move-result-object v2

    .line 1317942
    sget-object p1, LX/1wS;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1317943
    invoke-virtual {v0, p2}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 1317944
    :goto_1
    move-object v0, v2

    .line 1317945
    if-eqz v0, :cond_1

    .line 1317946
    iget-object v1, p0, LX/8FU;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1317947
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1317948
    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
