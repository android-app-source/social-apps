.class public LX/7Gd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RETURN_TYPE:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:J

.field private final c:LX/7G5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7G5",
            "<TRETURN_TYPE;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Random;


# direct methods
.method public constructor <init>(JJLX/7G5;Ljava/util/Random;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "LX/7G5",
            "<TRETURN_TYPE;>;",
            "Ljava/util/Random;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1189636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189637
    iput-wide p1, p0, LX/7Gd;->a:J

    .line 1189638
    iput-wide p3, p0, LX/7Gd;->b:J

    .line 1189639
    iput-object p5, p0, LX/7Gd;->c:LX/7G5;

    .line 1189640
    iput-object p6, p0, LX/7Gd;->d:Ljava/util/Random;

    .line 1189641
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRETURN_TYPE;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 1189642
    const/4 v1, 0x0

    .line 1189643
    const/4 v0, 0x0

    move v2, v0

    move-wide v4, v6

    move-object v0, v1

    :goto_0
    const/16 v1, 0x14

    if-ge v2, v1, :cond_1

    .line 1189644
    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    .line 1189645
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iget-object v3, p0, LX/7Gd;->d:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v8, v3

    add-double/2addr v0, v8

    long-to-double v8, v4

    mul-double/2addr v0, v8

    double-to-int v0, v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 1189646
    :cond_0
    iget-object v0, p0, LX/7Gd;->c:LX/7G5;

    invoke-interface {v0}, LX/7G5;->a()LX/7Gc;

    move-result-object v0

    .line 1189647
    iget-boolean v1, v0, LX/7Gc;->a:Z

    if-nez v1, :cond_2

    .line 1189648
    iget-object v0, v0, LX/7Gc;->b:Ljava/lang/Object;

    .line 1189649
    :cond_1
    return-object v0

    .line 1189650
    :cond_2
    iget-object v3, v0, LX/7Gc;->b:Ljava/lang/Object;

    .line 1189651
    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 1189652
    iget-wide v0, p0, LX/7Gd;->b:J

    .line 1189653
    :goto_1
    iget-object v4, p0, LX/7Gd;->c:LX/7G5;

    invoke-interface {v4, v0, v1}, LX/7G5;->a(J)V

    .line 1189654
    add-int/lit8 v2, v2, 0x1

    move-wide v4, v0

    move-object v0, v3

    goto :goto_0

    .line 1189655
    :cond_3
    const-wide/16 v0, 0x2

    mul-long/2addr v0, v4

    iget-wide v4, p0, LX/7Gd;->a:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_1
.end method
