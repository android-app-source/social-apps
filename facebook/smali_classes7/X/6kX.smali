.class public LX/6kX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final numFreeMessagesRemaining:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1139544
    new-instance v0, LX/1sv;

    const-string v1, "DeltaZeroRating"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kX;->b:LX/1sv;

    .line 1139545
    new-instance v0, LX/1sw;

    const-string v1, "numFreeMessagesRemaining"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kX;->c:LX/1sw;

    .line 1139546
    sput-boolean v3, LX/6kX;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1139504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1139505
    iput-object p1, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    .line 1139506
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1139523
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1139524
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1139525
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1139526
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaZeroRating"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139527
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139528
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139529
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139530
    iget-object v4, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    if-eqz v4, :cond_0

    .line 1139531
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139532
    const-string v4, "numFreeMessagesRemaining"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139533
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139534
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139535
    iget-object v0, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 1139536
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139537
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139538
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139539
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1139540
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1139541
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1139542
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 1139543
    :cond_4
    iget-object v0, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1139547
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1139548
    iget-object v0, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1139549
    iget-object v0, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1139550
    sget-object v0, LX/6kX;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139551
    iget-object v0, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1139552
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1139553
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1139554
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1139508
    if-nez p1, :cond_1

    .line 1139509
    :cond_0
    :goto_0
    return v0

    .line 1139510
    :cond_1
    instance-of v1, p1, LX/6kX;

    if-eqz v1, :cond_0

    .line 1139511
    check-cast p1, LX/6kX;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1139512
    if-nez p1, :cond_3

    .line 1139513
    :cond_2
    :goto_1
    move v0, v2

    .line 1139514
    goto :goto_0

    .line 1139515
    :cond_3
    iget-object v0, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1139516
    :goto_2
    iget-object v3, p1, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1139517
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1139518
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1139519
    iget-object v0, p0, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kX;->numFreeMessagesRemaining:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1139520
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1139521
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1139522
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1139507
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1139501
    sget-boolean v0, LX/6kX;->a:Z

    .line 1139502
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kX;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1139503
    return-object v0
.end method
