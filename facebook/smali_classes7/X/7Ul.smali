.class public final LX/7Ul;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1213389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;
    .locals 2

    .prologue
    .line 1213390
    new-instance v0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;

    invoke-direct {v0, p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1213391
    invoke-static {p1}, LX/7Ul;->a(Landroid/os/Parcel;)Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1213392
    new-array v0, p1, [Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;

    move-object v0, v0

    .line 1213393
    return-object v0
.end method
