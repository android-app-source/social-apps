.class public LX/8WX;
.super LX/1OM;
.source ""

# interfaces
.implements LX/8VY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/8VY;"
    }
.end annotation


# instance fields
.field private final a:LX/8WT;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field public d:LX/8WO;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field public h:Ljava/lang/String;

.field public i:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1354189
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1354190
    new-instance v0, LX/8WT;

    invoke-direct {v0, p0}, LX/8WT;-><init>(LX/8WX;)V

    iput-object v0, p0, LX/8WX;->a:LX/8WT;

    .line 1354191
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1354192
    iput-object v0, p0, LX/8WX;->b:LX/0Px;

    .line 1354193
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/8WX;->e:Ljava/lang/Integer;

    .line 1354194
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8WX;->g:Z

    .line 1354195
    return-void
.end method

.method public static d(LX/8WX;)Z
    .locals 2

    .prologue
    .line 1354128
    const/4 v0, 0x2

    iget-object v1, p0, LX/8WX;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, LX/3CW;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1354181
    packed-switch p2, :pswitch_data_0

    .line 1354182
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1354183
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077c

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1354184
    new-instance v0, LX/8WU;

    invoke-direct {v0, p0, v1}, LX/8WU;-><init>(LX/8WX;Landroid/view/View;)V

    goto :goto_0

    .line 1354185
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030777

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1354186
    new-instance v0, LX/8WS;

    invoke-direct {v0, v1}, LX/8WS;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1354187
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030779

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1354188
    new-instance v0, LX/8WW;

    invoke-direct {v0, p0, v1}, LX/8WW;-><init>(LX/8WX;Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1354136
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    .line 1354137
    instance-of v0, p1, LX/8WS;

    if-eqz v0, :cond_0

    .line 1354138
    invoke-static {p0}, LX/8WX;->d(LX/8WX;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    .line 1354139
    :goto_0
    iget-object v3, p0, LX/8WX;->b:LX/0Px;

    sub-int v0, p2, v0

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    move-object v4, v0

    .line 1354140
    invoke-static {p0}, LX/8WX;->d(LX/8WX;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    sub-int v0, p2, v0

    .line 1354141
    check-cast p1, LX/8WS;

    .line 1354142
    iget-object v3, p0, LX/8WX;->a:LX/8WT;

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 1354143
    iget-object v5, p1, LX/8WS;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object v6, v4, LX/8Vb;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354144
    iget-object v5, p1, LX/8WS;->m:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v6, v4, LX/8Vb;->i:LX/8Vd;

    invoke-virtual {v5, v6}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1354145
    iget-object v5, v4, LX/8Vb;->n:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_9

    iget-boolean v5, v4, LX/8Vb;->j:Z

    if-nez v5, :cond_9

    .line 1354146
    iget-object v5, p1, LX/8WS;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v6, v4, LX/8Vb;->n:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354147
    iget-object v5, p1, LX/8WS;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1354148
    :goto_2
    iget-object v5, v4, LX/8Vb;->f:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_a

    iget-boolean v5, v4, LX/8Vb;->j:Z

    if-nez v5, :cond_a

    .line 1354149
    iget-object v5, p1, LX/8WS;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v6, v4, LX/8Vb;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354150
    iget-object v5, p1, LX/8WS;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1354151
    :goto_3
    iget-object v5, p1, LX/8WS;->l:Landroid/view/View;

    new-instance v6, LX/8WR;

    invoke-direct {v6, p1, v3, v4, v0}, LX/8WR;-><init>(LX/8WS;LX/8WT;LX/8Vb;I)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354152
    if-nez p2, :cond_2

    move v0, v1

    .line 1354153
    :goto_4
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p2, v3, :cond_3

    move v3, v1

    .line 1354154
    :goto_5
    iget-object v5, v4, LX/8Vb;->a:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v4, v4, LX/8Vb;->a:Ljava/lang/String;

    iget-object v5, p0, LX/8WX;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1354155
    invoke-virtual {p1, v1, v0, v3}, LX/8WS;->a(ZZZ)V

    .line 1354156
    :cond_0
    :goto_6
    return-void

    :cond_1
    move v0, v2

    .line 1354157
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1354158
    goto :goto_4

    :cond_3
    move v3, v2

    .line 1354159
    goto :goto_5

    .line 1354160
    :cond_4
    invoke-virtual {p1, v2, v0, v3}, LX/8WS;->a(ZZZ)V

    goto :goto_6

    .line 1354161
    :cond_5
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1354162
    instance-of v0, p1, LX/8WU;

    if-eqz v0, :cond_0

    .line 1354163
    check-cast p1, LX/8WU;

    iget-object v0, p0, LX/8WX;->e:Ljava/lang/Integer;

    iget-object v1, p0, LX/8WX;->f:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, -0x1

    .line 1354164
    const/4 v2, 0x0

    .line 1354165
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move-object v1, v2

    move v3, v5

    move v2, v4

    .line 1354166
    :cond_6
    :goto_7
    if-eq v3, v5, :cond_7

    if-eq v2, v5, :cond_7

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1354167
    :cond_7
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1354168
    :goto_8
    goto :goto_6

    .line 1354169
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1354170
    :cond_9
    iget-object v5, p1, LX/8WS;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v8}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1354171
    :cond_a
    iget-object v5, p1, LX/8WS;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v8}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_3

    .line 1354172
    :pswitch_0
    const v3, 0x7f020737

    .line 1354173
    const v2, 0x7f0a00d1

    .line 1354174
    iget-object v6, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08236c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    .line 1354175
    :pswitch_1
    const v3, 0x7f020740

    .line 1354176
    const v2, 0x7f0a0705

    .line 1354177
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08236c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    .line 1354178
    :cond_b
    iget-object v5, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1354179
    iget-object v4, p1, LX/8WU;->m:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v5, p1, LX/8WU;->l:LX/8WX;

    iget-object v5, v5, LX/8WX;->i:LX/0wM;

    iget-object v6, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v5, v3, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1354180
    iget-object v2, p1, LX/8WU;->n:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Z)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setData"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1354196
    iput-object p2, p0, LX/8WX;->c:Ljava/lang/String;

    .line 1354197
    iput-object p3, p0, LX/8WX;->e:Ljava/lang/Integer;

    .line 1354198
    iput-object p4, p0, LX/8WX;->f:Ljava/lang/String;

    .line 1354199
    iput-boolean p5, p0, LX/8WX;->g:Z

    .line 1354200
    if-nez p1, :cond_0

    .line 1354201
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1354202
    :goto_0
    iput-object v0, p0, LX/8WX;->b:LX/0Px;

    .line 1354203
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1354204
    return-void

    .line 1354205
    :cond_0
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1354135
    iget-object v0, p0, LX/8WX;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1354129
    invoke-static {p0}, LX/8WX;->d(LX/8WX;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1354130
    const/4 v0, 0x1

    .line 1354131
    :goto_0
    return v0

    .line 1354132
    :cond_0
    iget-boolean v0, p0, LX/8WX;->g:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1354133
    const/4 v0, 0x3

    goto :goto_0

    .line 1354134
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final ij_()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1354122
    invoke-static {p0}, LX/8WX;->d(LX/8WX;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1354123
    :goto_0
    iget-boolean v3, p0, LX/8WX;->g:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1354124
    iget-object v1, p0, LX/8WX;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    return v2

    :cond_0
    move v0, v2

    .line 1354125
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1354126
    goto :goto_1

    .line 1354127
    :cond_2
    iget-object v1, p0, LX/8WX;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int v2, v1, v0

    goto :goto_2
.end method
