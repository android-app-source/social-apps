.class public final LX/8cV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapSnippetsModel;",
        ">;",
        "LX/7By;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Qd;


# direct methods
.method public constructor <init>(LX/3Qd;)V
    .locals 0

    .prologue
    .line 1374545
    iput-object p1, p0, LX/8cV;->a:LX/3Qd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1374546
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 1374547
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1374548
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374549
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapSnippetsModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapSnippetsModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesSnippetFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1374550
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374551
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapSnippetsModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapSnippetsModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesSnippetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesSnippetFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    move v2, v4

    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesSnippetFragmentModel$EdgesModel;

    .line 1374552
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesSnippetFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1374553
    :try_start_0
    iget-object v1, p0, LX/8cV;->a:LX/3Qd;

    iget-object v1, v1, LX/3Qd;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8cA;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesSnippetFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8cA;->a(LX/8bp;)LX/7C0;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 1374554
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1374555
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1374556
    iget-object v0, p0, LX/8cV;->a:LX/3Qd;

    iget-object v0, v0, LX/3Qd;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    invoke-virtual {v0, v1}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_1

    .line 1374557
    :cond_1
    new-instance v1, LX/7By;

    const-wide/16 v2, 0x0

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1374558
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 1374559
    invoke-direct/range {v1 .. v6}, LX/7By;-><init>(JZLX/0Px;LX/0Px;)V

    return-object v1
.end method
