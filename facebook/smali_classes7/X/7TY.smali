.class public LX/7TY;
.super LX/34c;
.source ""


# instance fields
.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1210765
    invoke-direct {p0, p1}, LX/34c;-><init>(Landroid/content/Context;)V

    .line 1210766
    iput-boolean v0, p0, LX/7TY;->c:Z

    .line 1210767
    iput-boolean v0, p0, LX/7TY;->d:Z

    .line 1210768
    iput-boolean v0, p0, LX/7TY;->e:Z

    .line 1210769
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7TY;->g:Z

    .line 1210770
    return-void
.end method

.method private a(LX/7TV;LX/3Ai;)V
    .locals 2

    .prologue
    .line 1210754
    iget-boolean v0, p0, LX/7TY;->g:Z

    if-nez v0, :cond_2

    .line 1210755
    iget-object v0, p1, LX/7TV;->l:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1210756
    :cond_0
    :goto_0
    invoke-virtual {p2}, LX/3Ai;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1210757
    iget-object v0, p1, LX/7TV;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p2}, LX/3Ai;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210758
    :cond_1
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/7TU;

    invoke-direct {v1, p0, p2}, LX/7TU;-><init>(LX/7TY;LX/3Ai;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1210759
    return-void

    .line 1210760
    :cond_2
    iget-object v0, p1, LX/7TV;->l:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1210761
    iget-boolean v0, p0, LX/7TY;->e:Z

    if-eqz v0, :cond_3

    .line 1210762
    iget-object v0, p1, LX/7TV;->l:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1210763
    :cond_3
    invoke-virtual {p2}, LX/3Ai;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1210764
    iget-object v0, p1, LX/7TV;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p2}, LX/3Ai;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1210741
    iget-object v0, p0, LX/34c;->c:Landroid/content/Context;

    move-object v0, v0

    .line 1210742
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1210743
    if-nez p2, :cond_0

    .line 1210744
    const v1, 0x7f0301d2

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1210745
    new-instance v0, LX/7TW;

    invoke-direct {v0, v1}, LX/7TW;-><init>(Landroid/view/View;)V

    .line 1210746
    :goto_0
    return-object v0

    .line 1210747
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 1210748
    const v1, 0x7f0301cf

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1210749
    new-instance v0, LX/7TV;

    invoke-direct {v0, v1}, LX/7TV;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1210750
    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    .line 1210751
    const v1, 0x7f0301d3

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1210752
    new-instance v0, LX/7TX;

    invoke-direct {v0, v1}, LX/7TX;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1210753
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for creating view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LX/1a1;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1210724
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    .line 1210725
    if-nez v2, :cond_1

    .line 1210726
    iget-boolean v2, p0, LX/7TY;->d:Z

    if-eqz v2, :cond_0

    :goto_0
    sub-int v0, p2, v0

    invoke-virtual {p0, v0}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 1210727
    check-cast p1, LX/7TW;

    .line 1210728
    invoke-virtual {p0, p1, v0}, LX/7TY;->a(LX/7TW;LX/3Ai;)V

    .line 1210729
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 1210730
    goto :goto_0

    .line 1210731
    :cond_1
    if-ne v2, v0, :cond_3

    .line 1210732
    iget-boolean v2, p0, LX/7TY;->d:Z

    if-eqz v2, :cond_2

    :goto_2
    sub-int v0, p2, v0

    invoke-virtual {p0, v0}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 1210733
    check-cast p1, LX/7TV;

    .line 1210734
    invoke-direct {p0, p1, v0}, LX/7TY;->a(LX/7TV;LX/3Ai;)V

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1210735
    goto :goto_2

    .line 1210736
    :cond_3
    const/4 v0, 0x2

    if-ne v2, v0, :cond_4

    .line 1210737
    check-cast p1, LX/7TX;

    .line 1210738
    iget-object v0, p1, LX/7TX;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/7TY;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210739
    goto :goto_1

    .line 1210740
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for binding view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LX/7TW;LX/3Ai;)V
    .locals 2

    .prologue
    .line 1210715
    invoke-direct {p0, p1, p2}, LX/7TY;->a(LX/7TV;LX/3Ai;)V

    .line 1210716
    iget-object v0, p2, LX/3Ai;->d:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 1210717
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1210718
    iget-object v0, p1, LX/7TW;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1210719
    iget-object v0, p1, LX/7TW;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1210720
    iget-object v1, p2, LX/3Ai;->d:Ljava/lang/CharSequence;

    move-object v1, v1

    .line 1210721
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210722
    :goto_0
    return-void

    .line 1210723
    :cond_0
    iget-object v0, p1, LX/7TW;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 1210707
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7TY;->d:Z

    .line 1210708
    iget-object v0, p0, LX/34c;->c:Landroid/content/Context;

    move-object v0, v0

    .line 1210709
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7TY;->f:Ljava/lang/String;

    .line 1210710
    return-void
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 1210712
    iget-boolean v0, p0, LX/7TY;->d:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1210713
    const/4 v0, 0x2

    .line 1210714
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, LX/7TY;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ij_()I
    .locals 2

    .prologue
    .line 1210711
    invoke-virtual {p0}, LX/34c;->e()I

    move-result v1

    iget-boolean v0, p0, LX/7TY;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
