.class public LX/6li;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SR::",
        "Lcom/facebook/messaging/xma/StyleRenderer;",
        "SC::",
        "LX/6lX;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSR;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSC;>;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            "LX/0Ot",
            "<TSR;>;",
            "LX/0Ot",
            "<TSC;>;)V"
        }
    .end annotation

    .prologue
    .line 1142881
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/6li;-><init>(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;LX/0Ot;LX/0Ot;Z)V

    .line 1142882
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;LX/0Ot;LX/0Ot;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            "LX/0Ot",
            "<TSR;>;",
            "LX/0Ot",
            "<TSC;>;Z)V"
        }
    .end annotation

    .prologue
    .line 1142883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142884
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    iput-object v0, p0, LX/6li;->a:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1142885
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    iput-object v0, p0, LX/6li;->b:LX/0Ot;

    .line 1142886
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    iput-object v0, p0, LX/6li;->c:LX/0Ot;

    .line 1142887
    iput-boolean p4, p0, LX/6li;->d:Z

    .line 1142888
    return-void
.end method
