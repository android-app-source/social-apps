.class public LX/8Og;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/11H;

.field private final b:LX/8NR;

.field private final c:LX/0So;

.field private final d:LX/0b3;

.field private e:LX/8On;


# direct methods
.method public constructor <init>(LX/11H;LX/8NR;LX/0So;LX/0b3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1339923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339924
    iput-object p1, p0, LX/8Og;->a:LX/11H;

    .line 1339925
    iput-object p2, p0, LX/8Og;->b:LX/8NR;

    .line 1339926
    iput-object p3, p0, LX/8Og;->c:LX/0So;

    .line 1339927
    iput-object p4, p0, LX/8Og;->d:LX/0b3;

    .line 1339928
    return-void
.end method

.method private static a(LX/8Ob;LX/8OV;)LX/8Oa;
    .locals 8

    .prologue
    const-wide/32 v0, 0x500000

    .line 1339917
    iget-wide v2, p0, LX/8Ob;->u:J

    invoke-virtual {p1, v2, v3}, LX/8OV;->a(J)LX/8Oa;

    move-result-object v4

    .line 1339918
    if-eqz v4, :cond_0

    .line 1339919
    iget-wide v2, v4, LX/8Oa;->f:J

    iget-wide v6, v4, LX/8Oa;->e:J

    sub-long/2addr v2, v6

    .line 1339920
    cmp-long v5, v2, v0

    if-lez v5, :cond_1

    .line 1339921
    :goto_0
    iput-wide v0, p0, LX/8Ob;->v:J

    .line 1339922
    :cond_0
    return-object v4

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8Og;
    .locals 5

    .prologue
    .line 1339849
    new-instance v4, LX/8Og;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v0

    check-cast v0, LX/11H;

    invoke-static {p0}, LX/8NR;->a(LX/0QB;)LX/8NR;

    move-result-object v1

    check-cast v1, LX/8NR;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v3

    check-cast v3, LX/0b3;

    invoke-direct {v4, v0, v1, v2, v3}, LX/8Og;-><init>(LX/11H;LX/8NR;LX/0So;LX/0b3;)V

    .line 1339850
    move-object v0, v4

    .line 1339851
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;Ljava/lang/String;LX/73w;LX/8Ne;LX/8OV;LX/74b;LX/8Oj;LX/8OL;LX/0cW;LX/8On;)V
    .locals 42

    .prologue
    .line 1339852
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, LX/8Og;->e:LX/8On;

    .line 1339853
    move-object/from16 v0, p2

    iget-object v2, v0, LX/8Ob;->h:LX/8Oo;

    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->u:J

    invoke-virtual {v2, v4, v5}, LX/8Oo;->b(J)V

    .line 1339854
    if-eqz p6, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1339855
    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->u:J

    .line 1339856
    const-wide/16 v4, 0x0

    .line 1339857
    move-object/from16 v0, p2

    iget-wide v2, v0, LX/8Ob;->u:J

    move-object/from16 v0, p6

    invoke-virtual {v0, v2, v3}, LX/8OV;->a(J)LX/8Oa;

    move-result-object v2

    move-wide/from16 v38, v4

    move-wide/from16 v40, v6

    .line 1339858
    :goto_1
    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->l:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_7

    if-eqz v2, :cond_7

    .line 1339859
    const/4 v3, 0x0

    .line 1339860
    const-wide/16 v4, 0x0

    .line 1339861
    const/16 v16, 0x0

    .line 1339862
    const-string v15, ""

    move-object/from16 v35, v2

    .line 1339863
    :goto_2
    :try_start_0
    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, LX/8Ob;->a(J)Ljava/lang/String;

    move-result-object v15

    .line 1339864
    move-object/from16 v0, p2

    iget-object v6, v0, LX/8Ob;->h:LX/8Oo;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->d:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v10, v0, LX/8Ob;->v:J

    move-object/from16 v0, p2

    iget-wide v12, v0, LX/8Ob;->l:J

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->d()I

    move-result v14

    invoke-virtual/range {v6 .. v15}, LX/8Oo;->a(Ljava/lang/String;JJJILjava/lang/String;)V

    .line 1339865
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Og;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v8

    .line 1339866
    move-object/from16 v0, v35

    iget-object v0, v0, LX/8Oa;->a:Ljava/lang/String;

    move-object/from16 v27, v0

    .line 1339867
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Og;->a:LX/11H;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Og;->b:LX/8NR;

    new-instance v17, LX/8NS;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p2

    iget-wide v0, v0, LX/8Ob;->u:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, LX/8Ob;->v:J

    move-wide/from16 v22, v0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v24

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p8

    iget v0, v0, LX/8Oj;->a:F

    move/from16 v29, v0

    const/16 v30, 0x0

    const-wide/16 v31, 0x0

    const-wide/16 v33, 0x0

    const/16 v36, 0x0

    move-object/from16 v28, p3

    invoke-direct/range {v17 .. v36}, LX/8NS;-><init>(JJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;FZJJLX/8Oa;Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->c:LX/14U;

    move-object/from16 v0, v17

    invoke-virtual {v2, v6, v0, v7}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8NT;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1339868
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Og;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v10

    .line 1339869
    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->v:J

    move-object/from16 v3, p8

    invoke-virtual/range {v3 .. v11}, LX/8Oj;->a(JJJJ)V

    .line 1339870
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Og;->d:LX/0b3;

    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->l:J

    move-object/from16 v0, p8

    iget v8, v0, LX/8Oj;->a:F

    move-object/from16 v0, p1

    invoke-static {v0, v3, v6, v7, v8}, LX/8Op;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/0b3;JF)V

    .line 1339871
    const-string v3, "after chunk video"

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, LX/8OL;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1339872
    const/4 v3, 0x1

    .line 1339873
    :try_start_2
    move-object/from16 v0, p2

    iget-object v6, v0, LX/8Ob;->h:LX/8Oo;

    invoke-virtual {v6, v15}, LX/8Oo;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v35

    .line 1339874
    :goto_3
    if-nez v16, :cond_0

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-interface/range {p5 .. p5}, LX/8Ne;->b()I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v4, v6

    if-lez v6, :cond_9

    .line 1339875
    :cond_0
    const-string v4, "after chunk video retries"

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1339876
    if-nez v16, :cond_3

    move-object/from16 v0, p2

    iget-object v4, v0, LX/8Ob;->w:Ljava/lang/Exception;

    if-eqz v4, :cond_3

    .line 1339877
    move-object/from16 v0, p2

    iget-object v2, v0, LX/8Ob;->w:Ljava/lang/Exception;

    throw v2

    .line 1339878
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1339879
    :catch_0
    move-exception v8

    move/from16 v3, v16

    move-object v6, v2

    .line 1339880
    :goto_4
    move-object/from16 v0, p8

    invoke-virtual {v0, v4, v5}, LX/8Oj;->a(J)V

    .line 1339881
    move-object/from16 v0, p8

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v15}, LX/8Oj;->a(LX/8Ob;Ljava/lang/String;)LX/8O2;

    move-result-object v12

    .line 1339882
    move-object/from16 v0, p2

    iput-object v8, v0, LX/8Ob;->w:Ljava/lang/Exception;

    .line 1339883
    move-object/from16 v0, p0

    iget-object v7, v0, LX/8Og;->e:LX/8On;

    move-object/from16 v0, p2

    iget-object v9, v0, LX/8Ob;->x:LX/8Oi;

    move-wide v10, v4

    invoke-virtual/range {v7 .. v12}, LX/8On;->a(Ljava/lang/Exception;LX/8Oi;JLX/8O2;)Landroid/util/Pair;

    move-result-object v7

    .line 1339884
    if-eqz v7, :cond_a

    .line 1339885
    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v8, v2

    move-object/from16 v0, p2

    iput-wide v8, v0, LX/8Ob;->u:J

    .line 1339886
    iget-object v2, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v8, v2

    move-object/from16 v0, p2

    iput-wide v8, v0, LX/8Ob;->v:J

    .line 1339887
    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v10, v0, LX/8Ob;->l:J

    cmp-long v2, v8, v10

    if-nez v2, :cond_2

    .line 1339888
    new-instance v6, LX/8NT;

    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v8, v2

    iget-object v2, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    invoke-direct {v6, v8, v9, v2, v3}, LX/8NT;-><init>(JJ)V

    .line 1339889
    const/4 v3, 0x1

    .line 1339890
    move-object/from16 v0, p2

    iget-object v2, v0, LX/8Ob;->h:LX/8Oo;

    invoke-virtual {v2, v15}, LX/8Oo;->a(Ljava/lang/String;)V

    move/from16 v16, v3

    move-object/from16 v2, v35

    move-object v3, v6

    goto/16 :goto_3

    .line 1339891
    :cond_2
    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->v:J

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_a

    .line 1339892
    move-object/from16 v0, p2

    move-object/from16 v1, p6

    invoke-static {v0, v1}, LX/8Og;->a(LX/8Ob;LX/8OV;)LX/8Oa;

    move-result-object v35

    move/from16 v16, v3

    move-object/from16 v2, v35

    move-object v3, v6

    goto/16 :goto_3

    .line 1339893
    :cond_3
    if-nez v3, :cond_4

    .line 1339894
    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 1339895
    :cond_4
    invoke-interface/range {p5 .. p5}, LX/8Ne;->a()V

    .line 1339896
    move-object/from16 v0, p8

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v15}, LX/8Oj;->a(LX/8Ob;Ljava/lang/String;)LX/8O2;

    move-result-object v13

    .line 1339897
    invoke-virtual {v3}, LX/8NT;->a()J

    move-result-wide v4

    move-object/from16 v0, p2

    iput-wide v4, v0, LX/8Ob;->u:J

    .line 1339898
    invoke-virtual {v3}, LX/8NT;->b()J

    move-result-wide v4

    .line 1339899
    invoke-virtual {v3}, LX/8NT;->a()J

    move-result-wide v6

    sub-long/2addr v4, v6

    move-object/from16 v0, p2

    iput-wide v4, v0, LX/8Ob;->v:J

    .line 1339900
    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->v:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_5

    .line 1339901
    move-object/from16 v0, p2

    move-object/from16 v1, p6

    invoke-static {v0, v1}, LX/8Og;->a(LX/8Ob;LX/8OV;)LX/8Oa;

    move-result-object v2

    .line 1339902
    :cond_5
    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->u:J

    iput-wide v4, v13, LX/8O2;->f:J

    .line 1339903
    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->v:J

    iput-wide v4, v13, LX/8O2;->g:J

    .line 1339904
    move-object/from16 v0, p2

    iget-object v5, v0, LX/8Ob;->d:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->v:J

    move-object/from16 v0, p2

    iget-wide v10, v0, LX/8Ob;->v:J

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->d()I

    move-result v12

    move-object/from16 v3, p4

    move-object/from16 v4, p7

    invoke-virtual/range {v3 .. v13}, LX/73w;->a(LX/74b;Ljava/lang/String;JJJILX/8O2;)V

    .line 1339905
    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->u:J

    cmp-long v3, v4, v40

    if-nez v3, :cond_6

    .line 1339906
    const-wide/16 v4, 0x1

    add-long v4, v4, v38

    .line 1339907
    const-wide/16 v6, 0x2

    cmp-long v3, v4, v6

    if-lez v3, :cond_8

    .line 1339908
    new-instance v2, LX/740;

    const-string v3, "Transfer chunk failure"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, LX/740;-><init>(Ljava/lang/String;Z)V

    throw v2

    .line 1339909
    :cond_6
    move-object/from16 v0, p2

    iget-wide v0, v0, LX/8Ob;->u:J

    move-wide/from16 v40, v0

    .line 1339910
    const-wide/16 v10, 0x0

    .line 1339911
    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecord;

    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->v:J

    add-long/2addr v6, v8

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZ)V

    .line 1339912
    move-object/from16 v0, p2

    iget-object v4, v0, LX/8Ob;->g:Ljava/util/Map;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/8Ob;->d:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339913
    move-object/from16 v0, p2

    iget-object v4, v0, LX/8Ob;->d:Ljava/lang/String;

    move-object/from16 v0, p10

    invoke-virtual {v0, v4, v3}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-wide v4, v10

    move-wide/from16 v6, v40

    :goto_5
    move-wide/from16 v38, v4

    move-wide/from16 v40, v6

    .line 1339914
    goto/16 :goto_1

    .line 1339915
    :cond_7
    return-void

    .line 1339916
    :catch_1
    move-exception v8

    move-object v6, v2

    goto/16 :goto_4

    :catch_2
    move-exception v8

    move-object v6, v3

    move/from16 v3, v16

    goto/16 :goto_4

    :cond_8
    move-wide/from16 v6, v40

    goto :goto_5

    :cond_9
    move-object/from16 v35, v2

    goto/16 :goto_2

    :cond_a
    move/from16 v16, v3

    move-object/from16 v2, v35

    move-object v3, v6

    goto/16 :goto_3
.end method
