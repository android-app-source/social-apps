.class public LX/6sj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6sf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6sf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153676
    iput-object p1, p0, LX/6sj;->a:Ljava/util/Set;

    .line 1153677
    return-void
.end method

.method public static a(LX/0QB;)LX/6sj;
    .locals 6

    .prologue
    .line 1153682
    const-class v1, LX/6sj;

    monitor-enter v1

    .line 1153683
    :try_start_0
    sget-object v0, LX/6sj;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1153684
    sput-object v2, LX/6sj;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1153685
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1153686
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1153687
    new-instance v3, LX/6sj;

    .line 1153688
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/6sk;

    invoke-direct {p0, v0}, LX/6sk;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1153689
    invoke-direct {v3, v4}, LX/6sj;-><init>(Ljava/util/Set;)V

    .line 1153690
    move-object v0, v3

    .line 1153691
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1153692
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6sj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1153693
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1153694
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6zU;)LX/6sf;
    .locals 3

    .prologue
    .line 1153678
    iget-object v0, p0, LX/6sj;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6sf;

    .line 1153679
    invoke-interface {v0}, LX/6sf;->a()LX/6zU;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1153680
    return-object v0

    .line 1153681
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported paymentMethodType seen: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
