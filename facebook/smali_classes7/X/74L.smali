.class public LX/74L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/74L;


# instance fields
.field public a:LX/0SG;

.field public b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/74K;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1167746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167747
    iput-object p1, p0, LX/74L;->a:LX/0SG;

    .line 1167748
    return-void
.end method

.method public static a(LX/0QB;)LX/74L;
    .locals 4

    .prologue
    .line 1167749
    sget-object v0, LX/74L;->c:LX/74L;

    if-nez v0, :cond_1

    .line 1167750
    const-class v1, LX/74L;

    monitor-enter v1

    .line 1167751
    :try_start_0
    sget-object v0, LX/74L;->c:LX/74L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1167752
    if-eqz v2, :cond_0

    .line 1167753
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1167754
    new-instance p0, LX/74L;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/74L;-><init>(LX/0SG;)V

    .line 1167755
    move-object v0, p0

    .line 1167756
    sput-object v0, LX/74L;->c:LX/74L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1167757
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1167758
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1167759
    :cond_1
    sget-object v0, LX/74L;->c:LX/74L;

    return-object v0

    .line 1167760
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1167761
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/0P1;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1167762
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    .line 1167763
    iget-object v0, p0, LX/74L;->b:Ljava/util/LinkedList;

    if-eqz v0, :cond_2

    .line 1167764
    iget-object v0, p0, LX/74L;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v3

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74K;

    .line 1167765
    add-int/lit8 v5, v1, 0x1

    .line 1167766
    iget-object v1, v0, LX/74K;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v3

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/74J;

    .line 1167767
    add-int/lit8 v4, v2, 0x1

    .line 1167768
    new-instance v9, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v9, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 1167769
    const-string v2, "time"

    iget-wide v10, v1, LX/74J;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v2, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1167770
    const-string v2, "qn"

    iget-object v10, v0, LX/74K;->a:Ljava/lang/String;

    invoke-virtual {v9, v2, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1167771
    const-string v2, "event"

    iget-object v10, v1, LX/74J;->b:LX/74R;

    invoke-virtual {v10}, LX/74R;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v2, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1167772
    iget-object v1, v1, LX/74J;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1167773
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v9, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1167774
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1167775
    :cond_0
    :try_start_1
    invoke-virtual {v9}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1167776
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, "Upload_"

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, "_Event_"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move v2, v4

    .line 1167777
    goto :goto_1

    :cond_1
    move v1, v5

    .line 1167778
    goto/16 :goto_0

    .line 1167779
    :cond_2
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(LX/74R;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74R;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x5

    .line 1167780
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/74L;->b:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 1167781
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/74L;->b:Ljava/util/LinkedList;

    .line 1167782
    :cond_0
    iget-object v0, p0, LX/74L;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, LX/74L;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74K;

    iget-object v0, v0, LX/74K;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1167783
    :cond_1
    new-instance v0, LX/74K;

    invoke-direct {v0, p0, p2}, LX/74K;-><init>(LX/74L;Ljava/lang/String;)V

    .line 1167784
    iget-object v1, p0, LX/74L;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1167785
    iget-object v1, p0, LX/74L;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v3, :cond_2

    .line 1167786
    iget-object v1, p0, LX/74L;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 1167787
    :cond_2
    :goto_0
    iget-object v1, v0, LX/74K;->b:Ljava/util/LinkedList;

    new-instance v2, LX/74J;

    invoke-direct {v2, p0, p1, p3}, LX/74J;-><init>(LX/74L;LX/74R;Ljava/util/Map;)V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1167788
    iget-object v1, v0, LX/74K;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v3, :cond_3

    .line 1167789
    iget-object v0, v0, LX/74K;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1167790
    :cond_3
    monitor-exit p0

    return-void

    .line 1167791
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/74L;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1167792
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
