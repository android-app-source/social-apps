.class public LX/6ju;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final folders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final timestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1133567
    new-instance v0, LX/1sv;

    const-string v1, "DeltaMarkFolderSeen"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6ju;->b:LX/1sv;

    .line 1133568
    new-instance v0, LX/1sw;

    const-string v1, "folders"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ju;->c:LX/1sw;

    .line 1133569
    new-instance v0, LX/1sw;

    const-string v1, "timestamp"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ju;->d:LX/1sw;

    .line 1133570
    sput-boolean v4, LX/6ju;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1133563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133564
    iput-object p1, p0, LX/6ju;->folders:Ljava/util/List;

    .line 1133565
    iput-object p2, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    .line 1133566
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1133491
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1133492
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1133493
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1133494
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaMarkFolderSeen"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133495
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133496
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133497
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133498
    const/4 v1, 0x1

    .line 1133499
    iget-object v5, p0, LX/6ju;->folders:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 1133500
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133501
    const-string v1, "folders"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133502
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133503
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133504
    iget-object v1, p0, LX/6ju;->folders:Ljava/util/List;

    if-nez v1, :cond_6

    .line 1133505
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133506
    :goto_3
    const/4 v1, 0x0

    .line 1133507
    :cond_0
    iget-object v5, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 1133508
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133509
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133510
    const-string v1, "timestamp"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133511
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133512
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133513
    iget-object v0, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 1133514
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133515
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133516
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133517
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133518
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1133519
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1133520
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1133521
    :cond_6
    iget-object v1, p0, LX/6ju;->folders:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1133522
    :cond_7
    iget-object v0, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1133549
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133550
    iget-object v0, p0, LX/6ju;->folders:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1133551
    iget-object v0, p0, LX/6ju;->folders:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1133552
    sget-object v0, LX/6ju;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133553
    new-instance v0, LX/1u3;

    const/16 v1, 0x8

    iget-object v2, p0, LX/6ju;->folders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1133554
    iget-object v0, p0, LX/6ju;->folders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1133555
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_0

    .line 1133556
    :cond_0
    iget-object v0, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1133557
    iget-object v0, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1133558
    sget-object v0, LX/6ju;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133559
    iget-object v0, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1133560
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133561
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133562
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133527
    if-nez p1, :cond_1

    .line 1133528
    :cond_0
    :goto_0
    return v0

    .line 1133529
    :cond_1
    instance-of v1, p1, LX/6ju;

    if-eqz v1, :cond_0

    .line 1133530
    check-cast p1, LX/6ju;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133531
    if-nez p1, :cond_3

    .line 1133532
    :cond_2
    :goto_1
    move v0, v2

    .line 1133533
    goto :goto_0

    .line 1133534
    :cond_3
    iget-object v0, p0, LX/6ju;->folders:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1133535
    :goto_2
    iget-object v3, p1, LX/6ju;->folders:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1133536
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133537
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133538
    iget-object v0, p0, LX/6ju;->folders:Ljava/util/List;

    iget-object v3, p1, LX/6ju;->folders:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133539
    :cond_5
    iget-object v0, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133540
    :goto_4
    iget-object v3, p1, LX/6ju;->timestamp:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133541
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133542
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133543
    iget-object v0, p0, LX/6ju;->timestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6ju;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1133544
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1133545
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1133546
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1133547
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1133548
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133526
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133523
    sget-boolean v0, LX/6ju;->a:Z

    .line 1133524
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6ju;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133525
    return-object v0
.end method
