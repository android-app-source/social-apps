.class public LX/7N8;
.super LX/7MM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/7MM;"
    }
.end annotation


# static fields
.field private static final o:Ljava/lang/String;

.field public static final p:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public c:LX/3Gg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/7OG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final q:Landroid/view/View;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public final t:Lcom/facebook/resources/ui/FbTextView;

.field private u:Lcom/facebook/video/engine/VideoPlayerParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1199922
    const-class v0, LX/7N8;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7N8;->o:Ljava/lang/String;

    .line 1199923
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_QUALITY_LABEL_INLINE_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/7N8;->p:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199920
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7N8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199921
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199918
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7N8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199919
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1199868
    invoke-direct {p0, p1, p2, p3}, LX/7MM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199869
    const-class v0, LX/7N8;

    invoke-static {v0, p0}, LX/7N8;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1199870
    const v0, 0x7f03092e

    move v0, v0

    .line 1199871
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1199872
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/7N8;->q:Landroid/view/View;

    .line 1199873
    const v0, 0x7f0d27c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 1199874
    invoke-virtual {p0}, LX/7N8;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080d6b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7N8;->r:Ljava/lang/String;

    .line 1199875
    iget-object v0, p0, LX/7N8;->c:LX/3Gg;

    invoke-virtual {v0}, LX/3Gg;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7N8;->s:Ljava/lang/String;

    .line 1199876
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7N7;

    invoke-direct {v1, p0}, LX/7N7;-><init>(LX/7N8;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199877
    iget-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1199878
    iget-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setClickable(Z)V

    .line 1199879
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7N8;

    invoke-static {p0}, LX/3Gg;->b(LX/0QB;)LX/3Gg;

    move-result-object v1

    check-cast v1, LX/3Gg;

    invoke-static {p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v2

    check-cast v2, LX/1C2;

    invoke-static {p0}, LX/7OG;->a(LX/0QB;)LX/7OG;

    move-result-object v3

    check-cast v3, LX/7OG;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object p0

    check-cast p0, LX/0iA;

    iput-object v1, p1, LX/7N8;->c:LX/3Gg;

    iput-object v2, p1, LX/7N8;->d:LX/1C2;

    iput-object v3, p1, LX/7N8;->e:LX/7OG;

    iput-object p0, p1, LX/7N8;->f:LX/0iA;

    return-void
.end method

.method public static h(LX/7N8;)V
    .locals 6

    .prologue
    .line 1199924
    iget-object v0, p0, LX/7N8;->c:LX/3Gg;

    invoke-virtual {v0}, LX/3Gg;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/7N8;->e:LX/7OG;

    invoke-virtual {v0}, LX/7OG;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1199925
    iget-object v0, p0, LX/7N8;->e:LX/7OG;

    invoke-virtual {v0}, LX/7OG;->a()Ljava/lang/String;

    move-result-object v0

    .line 1199926
    :goto_0
    move-object v0, v0

    .line 1199927
    if-eqz v0, :cond_1

    .line 1199928
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1199929
    :cond_0
    iget-object v1, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/7N8;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1199930
    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, LX/7N8;->s:Ljava/lang/String;

    goto :goto_0

    .line 1199931
    :cond_3
    iget-object v1, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1199932
    iget-object v1, p0, LX/7N8;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1199933
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2qd;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    sget-object v4, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    const-string v5, "Auto"

    invoke-direct {v2, v3, v4, v5}, LX/2qd;-><init>(LX/04g;LX/2oi;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_1

    .line 1199934
    :cond_4
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2qd;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    sget-object v4, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    invoke-direct {v2, v3, v4, v0}, LX/2qd;-><init>(LX/04g;LX/2oi;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1199907
    invoke-super {p0, p1, p2}, LX/7MM;->a(LX/2pa;Z)V

    .line 1199908
    if-eqz p2, :cond_0

    .line 1199909
    iget-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/7N8;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1199910
    invoke-static {p0}, LX/7N8;->h(LX/7N8;)V

    .line 1199911
    :cond_0
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1199912
    iget-object v0, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->j()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    if-nez v0, :cond_1

    .line 1199913
    iget-object v0, p0, LX/7N8;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1199914
    iget-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1199915
    :goto_0
    return-void

    .line 1199916
    :cond_1
    iget-object v0, p0, LX/7N8;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1199917
    iget-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1199905
    invoke-super {p0}, LX/7MM;->d()V

    .line 1199906
    return-void
.end method

.method public final dI_()V
    .locals 6

    .prologue
    .line 1199887
    iget-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1199888
    :cond_0
    :goto_0
    return-void

    .line 1199889
    :cond_1
    iget-object v0, p0, LX/7N8;->f:LX/0iA;

    sget-object v1, LX/7N8;->p:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/7OF;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 1199890
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/7OF;

    if-eqz v1, :cond_0

    .line 1199891
    check-cast v0, LX/7OF;

    iget-object v1, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    const/4 p0, -0x1

    .line 1199892
    iget-boolean v2, v0, LX/7OF;->d:Z

    if-eqz v2, :cond_2

    .line 1199893
    :goto_1
    goto :goto_0

    .line 1199894
    :cond_2
    iget-object v2, v0, LX/7OF;->c:LX/0wM;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0208ed

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3, p0}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1199895
    new-instance v3, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1199896
    sput-object v3, LX/7OF;->e:LX/0hs;

    .line 1199897
    iput p0, v3, LX/0hs;->t:I

    .line 1199898
    sget-object v3, LX/7OF;->e:LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080d6d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1199899
    sget-object v3, LX/7OF;->e:LX/0hs;

    invoke-virtual {v3, v2}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1199900
    sget-object v2, LX/7OF;->e:LX/0hs;

    const v3, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v3}, LX/0ht;->b(F)V

    .line 1199901
    sget-object v2, LX/7OF;->e:LX/0hs;

    invoke-virtual {v2, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1199902
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/7OF;->d:Z

    .line 1199903
    iget-object v2, v0, LX/7OF;->b:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    const-string v3, "4566"

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1199904
    goto :goto_1
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    .line 1199880
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7N8;->q:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 1199881
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, LX/7N8;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getLeft()I

    move-result v1

    iget-object v2, p0, LX/7N8;->q:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, LX/7N8;->q:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, LX/7N8;->q:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1199882
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1199883
    iget-object v0, p0, LX/7N8;->d:LX/1C2;

    iget-object v1, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->s()LX/04D;

    move-result-object v3

    iget-object v4, p0, LX/2oy;->j:LX/2pb;

    .line 1199884
    iget-object v5, v4, LX/2pb;->D:LX/04G;

    move-object v4, v5

    .line 1199885
    iget-object v5, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->h()I

    iget-object v5, p0, LX/7N8;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    const-string v6, "inline"

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;ZLjava/lang/String;)LX/1C2;

    .line 1199886
    :cond_0
    invoke-super {p0, p1}, LX/7MM;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
