.class public final LX/7FL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/structuredsurvey/util/clientconstraints/SurveyClientConstraintsResolver;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/structuredsurvey/util/clientconstraints/SurveyClientConstraintsResolver;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1186119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1186120
    iput-object p1, p0, LX/7FL;->a:LX/0QB;

    .line 1186121
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1186129
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/7FL;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1186123
    packed-switch p2, :pswitch_data_0

    .line 1186124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1186125
    :pswitch_0
    new-instance v0, LX/7FN;

    invoke-direct {v0}, LX/7FN;-><init>()V

    .line 1186126
    move-object v0, v0

    .line 1186127
    move-object v0, v0

    .line 1186128
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1186122
    const/4 v0, 0x1

    return v0
.end method
