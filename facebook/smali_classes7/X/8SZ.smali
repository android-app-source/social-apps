.class public final enum LX/8SZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8SZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8SZ;

.field public static final enum GLYPH:LX/8SZ;

.field public static final enum PILL:LX/8SZ;

.field public static final enum TOKEN:LX/8SZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1346764
    new-instance v0, LX/8SZ;

    const-string v1, "PILL"

    invoke-direct {v0, v1, v2}, LX/8SZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8SZ;->PILL:LX/8SZ;

    .line 1346765
    new-instance v0, LX/8SZ;

    const-string v1, "TOKEN"

    invoke-direct {v0, v1, v3}, LX/8SZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8SZ;->TOKEN:LX/8SZ;

    .line 1346766
    new-instance v0, LX/8SZ;

    const-string v1, "GLYPH"

    invoke-direct {v0, v1, v4}, LX/8SZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8SZ;->GLYPH:LX/8SZ;

    .line 1346767
    const/4 v0, 0x3

    new-array v0, v0, [LX/8SZ;

    sget-object v1, LX/8SZ;->PILL:LX/8SZ;

    aput-object v1, v0, v2

    sget-object v1, LX/8SZ;->TOKEN:LX/8SZ;

    aput-object v1, v0, v3

    sget-object v1, LX/8SZ;->GLYPH:LX/8SZ;

    aput-object v1, v0, v4

    sput-object v0, LX/8SZ;->$VALUES:[LX/8SZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1346768
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8SZ;
    .locals 1

    .prologue
    .line 1346769
    const-class v0, LX/8SZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8SZ;

    return-object v0
.end method

.method public static values()[LX/8SZ;
    .locals 1

    .prologue
    .line 1346770
    sget-object v0, LX/8SZ;->$VALUES:[LX/8SZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8SZ;

    return-object v0
.end method
