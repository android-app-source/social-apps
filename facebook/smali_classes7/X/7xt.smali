.class public LX/7xt;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketOrder$EventTickets$Nodes;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketOrder$EventTickets$Nodes$Fbqrcode;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field public d:LX/19B;

.field private e:LX/1Ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/19B;LX/1Ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1278134
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1278135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7xt;->b:Ljava/util/List;

    .line 1278136
    iput-object p3, p0, LX/7xt;->e:LX/1Ad;

    .line 1278137
    iput-object p1, p0, LX/7xt;->c:Landroid/content/Context;

    .line 1278138
    iput-object p2, p0, LX/7xt;->d:LX/19B;

    .line 1278139
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1278132
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03052c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1278133
    new-instance v1, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;

    invoke-direct {v1, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 1278119
    check-cast p1, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;

    .line 1278120
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 1278121
    iget-object v1, p0, LX/7xt;->d:LX/19B;

    .line 1278122
    invoke-virtual {v1}, LX/19B;->a()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    move v1, v2

    .line 1278123
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v2, v1, :cond_0

    .line 1278124
    new-instance v2, LX/1a3;

    const/4 v3, -0x2

    invoke-direct {v2, v1, v3}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1278125
    :cond_0
    iget-object v0, p0, LX/7xt;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;

    .line 1278126
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1278127
    :goto_0
    iget-object v0, p0, LX/7xt;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->b()LX/1Fb;

    move-result-object v0

    .line 1278128
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/7xt;->e:LX/1Ad;

    invoke-virtual {p1, v1, v0, v2}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;->a(Ljava/lang/String;Ljava/lang/String;LX/1Ad;)V

    .line 1278129
    return-void

    .line 1278130
    :cond_1
    iget-object v0, p0, LX/7xt;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081fc0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1278131
    iget-object v0, p0, LX/7xt;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7xt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
