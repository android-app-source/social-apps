.class public final LX/7Cx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:LX/3II;

.field private b:F

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>(LX/3II;FF)V
    .locals 1

    .prologue
    .line 1182040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182041
    const/4 v0, 0x0

    iput v0, p0, LX/7Cx;->d:F

    .line 1182042
    iput-object p1, p0, LX/7Cx;->a:LX/3II;

    .line 1182043
    iput p2, p0, LX/7Cx;->b:F

    .line 1182044
    iput p3, p0, LX/7Cx;->c:F

    .line 1182045
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 1182046
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    .line 1182047
    iget v1, p0, LX/7Cx;->d:F

    sub-float v1, v0, v1

    .line 1182048
    iget-object v2, p0, LX/7Cx;->a:LX/3II;

    invoke-interface {v2}, LX/3II;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1182049
    iget-object v2, p0, LX/7Cx;->a:LX/3II;

    invoke-interface {v2}, LX/3II;->get360TextureView()LX/2qW;

    move-result-object v2

    iget v3, p0, LX/7Cx;->b:F

    mul-float/2addr v3, v1

    iget v4, p0, LX/7Cx;->c:F

    mul-float/2addr v1, v4

    .line 1182050
    iget-object v4, v2, LX/2qW;->c:LX/7Cy;

    if-eqz v4, :cond_0

    iget-object v4, v2, LX/2qW;->c:LX/7Cy;

    iget-object v4, v4, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v4, :cond_0

    .line 1182051
    iget-object v4, v2, LX/2qW;->c:LX/7Cy;

    iget-object v4, v4, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1182052
    iget-object v2, v4, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v2, v2, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1182053
    iget-object v2, v4, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    invoke-virtual {v2, v3, v1}, LX/7Ce;->d(FF)V

    .line 1182054
    iget-object v2, v4, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v2, v2, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1182055
    :cond_0
    iput v0, p0, LX/7Cx;->d:F

    .line 1182056
    return-void
.end method
