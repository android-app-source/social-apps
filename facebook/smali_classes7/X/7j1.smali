.class public LX/7j1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ZZZZLjava/lang/String;Ljava/lang/String;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1229003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229004
    iput-boolean p1, p0, LX/7j1;->a:Z

    .line 1229005
    iput-boolean p2, p0, LX/7j1;->b:Z

    .line 1229006
    iput-boolean p3, p0, LX/7j1;->c:Z

    .line 1229007
    iput-boolean p4, p0, LX/7j1;->d:Z

    .line 1229008
    iput-object p5, p0, LX/7j1;->e:Ljava/lang/String;

    .line 1229009
    iput-object p6, p0, LX/7j1;->f:Ljava/lang/String;

    .line 1229010
    iput-object p7, p0, LX/7j1;->g:LX/0am;

    .line 1229011
    return-void
.end method

.method public synthetic constructor <init>(ZZZZLjava/lang/String;Ljava/lang/String;LX/0am;B)V
    .locals 0

    .prologue
    .line 1229012
    invoke-direct/range {p0 .. p7}, LX/7j1;-><init>(ZZZZLjava/lang/String;Ljava/lang/String;LX/0am;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1229013
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/7j1;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1229014
    :cond_1
    :goto_0
    return v0

    .line 1229015
    :cond_2
    if-eq p0, p1, :cond_1

    .line 1229016
    check-cast p1, LX/7j1;

    .line 1229017
    iget-boolean v2, p0, LX/7j1;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1229018
    iget-boolean v3, p1, LX/7j1;->a:Z

    move v3, v3

    .line 1229019
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/7j1;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1229020
    iget-boolean v3, p1, LX/7j1;->b:Z

    move v3, v3

    .line 1229021
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/7j1;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1229022
    iget-boolean v3, p1, LX/7j1;->c:Z

    move v3, v3

    .line 1229023
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/7j1;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1229024
    iget-boolean v3, p1, LX/7j1;->d:Z

    move v3, v3

    .line 1229025
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7j1;->e:Ljava/lang/String;

    .line 1229026
    iget-object v3, p1, LX/7j1;->e:Ljava/lang/String;

    move-object v3, v3

    .line 1229027
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7j1;->f:Ljava/lang/String;

    .line 1229028
    iget-object v3, p1, LX/7j1;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1229029
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7j1;->g:LX/0am;

    .line 1229030
    iget-object v3, p1, LX/7j1;->g:LX/0am;

    move-object v3, v3

    .line 1229031
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1229032
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/7j1;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/7j1;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/7j1;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, LX/7j1;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/7j1;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/7j1;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/7j1;->g:LX/0am;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
