.class public final LX/7xu;
.super Landroid/widget/LinearLayout$LayoutParams;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1278197
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1278198
    iput-boolean v0, p0, LX/7xu;->a:Z

    .line 1278199
    iput-boolean v0, p0, LX/7xu;->b:Z

    .line 1278200
    iput v0, p0, LX/7xu;->c:I

    .line 1278201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1278185
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1278186
    iput-boolean v0, p0, LX/7xu;->a:Z

    .line 1278187
    iput-boolean v0, p0, LX/7xu;->b:Z

    .line 1278188
    iput v0, p0, LX/7xu;->c:I

    .line 1278189
    sget-object v0, LX/03r;->EventBuyTicketViewBlock_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1278190
    const/16 v1, 0x0

    iget-boolean v2, p0, LX/7xu;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/7xu;->a:Z

    .line 1278191
    const/16 v1, 0x1

    iget-boolean v2, p0, LX/7xu;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/7xu;->b:Z

    .line 1278192
    const/16 v1, 0x2

    iget v2, p0, LX/7xu;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/7xu;->c:I

    .line 1278193
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1278194
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1278202
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1278203
    iput-boolean v0, p0, LX/7xu;->a:Z

    .line 1278204
    iput-boolean v0, p0, LX/7xu;->b:Z

    .line 1278205
    iput v0, p0, LX/7xu;->c:I

    .line 1278206
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 1278196
    iget-boolean v0, p0, LX/7xu;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/7xu;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(LX/7xu;)Z
    .locals 1

    .prologue
    .line 1278195
    invoke-direct {p0}, LX/7xu;->a()Z

    move-result v0

    return v0
.end method
