.class public final enum LX/7JJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7JJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7JJ;

.field public static final enum CastApplicationManager_StopApplication:LX/7JJ;

.field public static final enum CastDeviceConnector_RequestStatus:LX/7JJ;

.field public static final enum CastDevicesManager_TryInitialize:LX/7JJ;

.field public static final enum CastPlayer_RemoveMessageReceivedCallbacks:LX/7JJ;

.field public static final enum CastPlayer_SetMessageReceivedCallbacks:LX/7JJ;

.field public static final enum CastPlayer_VolumeDown:LX/7JJ;

.field public static final enum CastPlayer_VolumeUp:LX/7JJ;

.field public static final enum FbAppPlayer_ConfirmLaunch:LX/7JJ;

.field public static final enum FbAppPlayer_LaunchExperience:LX/7JJ;

.field public static final enum FbAppPlayer_OnMessageReceived:LX/7JJ;

.field public static final enum FbAppPlayer_Pause:LX/7JJ;

.field public static final enum FbAppPlayer_Play:LX/7JJ;

.field public static final enum FbAppPlayer_Seek:LX/7JJ;

.field public static final enum FbAppPlayer_SendMessage:LX/7JJ;

.field public static final enum FbAppPlayer_Skip:LX/7JJ;

.field public static final enum FbAppPlayer_Start:LX/7JJ;

.field public static final enum FbAppPlayer_StartPlaying:LX/7JJ;

.field public static final enum VideoCastControllerFragment_OnStopTrackingTouch:LX/7JJ;

.field public static final enum VideoCastManager_ChangeVideo:LX/7JJ;

.field public static final enum VideoCastManager_GetCurrentStreamPosition:LX/7JJ;

.field public static final enum VideoCastManager_Pause:LX/7JJ;

.field public static final enum VideoCastManager_Play:LX/7JJ;

.field public static final enum VideoCastManager_Seek:LX/7JJ;

.field public static final enum VideoCastManager_Skip:LX/7JJ;

.field public static final enum VideoCastManager_Stop:LX/7JJ;

.field public static final enum VideoCastManager_TogglePlayback:LX/7JJ;

.field public static final enum VideoCastManager_VolumeDown:LX/7JJ;

.field public static final enum VideoCastManager_VolumeUp:LX/7JJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1193377
    new-instance v0, LX/7JJ;

    const-string v1, "CastApplicationManager_StopApplication"

    invoke-direct {v0, v1, v3}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->CastApplicationManager_StopApplication:LX/7JJ;

    .line 1193378
    new-instance v0, LX/7JJ;

    const-string v1, "CastDeviceConnector_RequestStatus"

    invoke-direct {v0, v1, v4}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->CastDeviceConnector_RequestStatus:LX/7JJ;

    .line 1193379
    new-instance v0, LX/7JJ;

    const-string v1, "CastDevicesManager_TryInitialize"

    invoke-direct {v0, v1, v5}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->CastDevicesManager_TryInitialize:LX/7JJ;

    .line 1193380
    new-instance v0, LX/7JJ;

    const-string v1, "CastPlayer_RemoveMessageReceivedCallbacks"

    invoke-direct {v0, v1, v6}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->CastPlayer_RemoveMessageReceivedCallbacks:LX/7JJ;

    .line 1193381
    new-instance v0, LX/7JJ;

    const-string v1, "CastPlayer_SetMessageReceivedCallbacks"

    invoke-direct {v0, v1, v7}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->CastPlayer_SetMessageReceivedCallbacks:LX/7JJ;

    .line 1193382
    new-instance v0, LX/7JJ;

    const-string v1, "CastPlayer_VolumeDown"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->CastPlayer_VolumeDown:LX/7JJ;

    .line 1193383
    new-instance v0, LX/7JJ;

    const-string v1, "CastPlayer_VolumeUp"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->CastPlayer_VolumeUp:LX/7JJ;

    .line 1193384
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_ConfirmLaunch"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_ConfirmLaunch:LX/7JJ;

    .line 1193385
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_LaunchExperience"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_LaunchExperience:LX/7JJ;

    .line 1193386
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_OnMessageReceived"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_OnMessageReceived:LX/7JJ;

    .line 1193387
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_Pause"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_Pause:LX/7JJ;

    .line 1193388
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_Play"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_Play:LX/7JJ;

    .line 1193389
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_Seek"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_Seek:LX/7JJ;

    .line 1193390
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_SendMessage"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_SendMessage:LX/7JJ;

    .line 1193391
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_Skip"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_Skip:LX/7JJ;

    .line 1193392
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_Start"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_Start:LX/7JJ;

    .line 1193393
    new-instance v0, LX/7JJ;

    const-string v1, "FbAppPlayer_StartPlaying"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->FbAppPlayer_StartPlaying:LX/7JJ;

    .line 1193394
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastControllerFragment_OnStopTrackingTouch"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastControllerFragment_OnStopTrackingTouch:LX/7JJ;

    .line 1193395
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_ChangeVideo"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_ChangeVideo:LX/7JJ;

    .line 1193396
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_GetCurrentStreamPosition"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_GetCurrentStreamPosition:LX/7JJ;

    .line 1193397
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_Pause"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_Pause:LX/7JJ;

    .line 1193398
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_Play"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_Play:LX/7JJ;

    .line 1193399
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_Seek"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_Seek:LX/7JJ;

    .line 1193400
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_Skip"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_Skip:LX/7JJ;

    .line 1193401
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_Stop"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_Stop:LX/7JJ;

    .line 1193402
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_TogglePlayback"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_TogglePlayback:LX/7JJ;

    .line 1193403
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_VolumeDown"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_VolumeDown:LX/7JJ;

    .line 1193404
    new-instance v0, LX/7JJ;

    const-string v1, "VideoCastManager_VolumeUp"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/7JJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7JJ;->VideoCastManager_VolumeUp:LX/7JJ;

    .line 1193405
    const/16 v0, 0x1c

    new-array v0, v0, [LX/7JJ;

    sget-object v1, LX/7JJ;->CastApplicationManager_StopApplication:LX/7JJ;

    aput-object v1, v0, v3

    sget-object v1, LX/7JJ;->CastDeviceConnector_RequestStatus:LX/7JJ;

    aput-object v1, v0, v4

    sget-object v1, LX/7JJ;->CastDevicesManager_TryInitialize:LX/7JJ;

    aput-object v1, v0, v5

    sget-object v1, LX/7JJ;->CastPlayer_RemoveMessageReceivedCallbacks:LX/7JJ;

    aput-object v1, v0, v6

    sget-object v1, LX/7JJ;->CastPlayer_SetMessageReceivedCallbacks:LX/7JJ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7JJ;->CastPlayer_VolumeDown:LX/7JJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7JJ;->CastPlayer_VolumeUp:LX/7JJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7JJ;->FbAppPlayer_ConfirmLaunch:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7JJ;->FbAppPlayer_LaunchExperience:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7JJ;->FbAppPlayer_OnMessageReceived:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7JJ;->FbAppPlayer_Pause:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7JJ;->FbAppPlayer_Play:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7JJ;->FbAppPlayer_Seek:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7JJ;->FbAppPlayer_SendMessage:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7JJ;->FbAppPlayer_Skip:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7JJ;->FbAppPlayer_Start:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7JJ;->FbAppPlayer_StartPlaying:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7JJ;->VideoCastControllerFragment_OnStopTrackingTouch:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7JJ;->VideoCastManager_ChangeVideo:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/7JJ;->VideoCastManager_GetCurrentStreamPosition:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/7JJ;->VideoCastManager_Pause:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/7JJ;->VideoCastManager_Play:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/7JJ;->VideoCastManager_Seek:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/7JJ;->VideoCastManager_Skip:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/7JJ;->VideoCastManager_Stop:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/7JJ;->VideoCastManager_TogglePlayback:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/7JJ;->VideoCastManager_VolumeDown:LX/7JJ;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/7JJ;->VideoCastManager_VolumeUp:LX/7JJ;

    aput-object v2, v0, v1

    sput-object v0, LX/7JJ;->$VALUES:[LX/7JJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1193406
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7JJ;
    .locals 1

    .prologue
    .line 1193407
    const-class v0, LX/7JJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7JJ;

    return-object v0
.end method

.method public static values()[LX/7JJ;
    .locals 1

    .prologue
    .line 1193408
    sget-object v0, LX/7JJ;->$VALUES:[LX/7JJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7JJ;

    return-object v0
.end method
