.class public LX/8HI;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;",
        "Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320808
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 1320809
    return-void
.end method

.method public static a(LX/0QB;)LX/8HI;
    .locals 2

    .prologue
    .line 1320810
    new-instance v1, LX/8HI;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v1, v0}, LX/8HI;-><init>(LX/0sO;)V

    .line 1320811
    move-object v0, v1

    .line 1320812
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1320813
    new-instance v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;

    iget-object v1, p0, LX/0ro;->a:LX/0sO;

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1320814
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 1320815
    check-cast p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    .line 1320816
    new-instance v0, LX/5jy;

    invoke-direct {v0}, LX/5jy;-><init>()V

    move-object v1, v0

    .line 1320817
    const/4 v0, 0x1

    .line 1320818
    iput-boolean v0, v1, LX/0gW;->l:Z

    .line 1320819
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1320820
    :goto_0
    return-object v0

    .line 1320821
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    move-object v0, v0

    .line 1320822
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1320823
    iget-object v0, p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    move-object v0, v0

    .line 1320824
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1320825
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1320826
    :cond_1
    const-string v0, "nodes"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-object v0, v1

    .line 1320827
    goto :goto_0
.end method

.method public final i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 1320828
    check-cast p1, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    .line 1320829
    invoke-virtual {p1}, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b()LX/3WA;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b()LX/3WA;

    move-result-object v0

    invoke-virtual {v0}, LX/3WA;->getMetadataRecommendedRequestPriority()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
