.class public final LX/7Bv;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1179663
    new-instance v0, LX/0U1;

    const-string v1, "internal_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->a:LX/0U1;

    .line 1179664
    new-instance v0, LX/0U1;

    const-string v1, "name"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->b:LX/0U1;

    .line 1179665
    new-instance v0, LX/0U1;

    const-string v1, "type"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->c:LX/0U1;

    .line 1179666
    new-instance v0, LX/0U1;

    const-string v1, "semantic"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->d:LX/0U1;

    .line 1179667
    new-instance v0, LX/0U1;

    const-string v1, "subtext"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->e:LX/0U1;

    .line 1179668
    new-instance v0, LX/0U1;

    const-string v1, "bolded_subtext"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->f:LX/0U1;

    .line 1179669
    new-instance v0, LX/0U1;

    const-string v1, "cost"

    const-string v2, "DOUBLE NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->g:LX/0U1;

    .line 1179670
    new-instance v0, LX/0U1;

    const-string v1, "source"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Bv;->h:LX/0U1;

    return-void
.end method
