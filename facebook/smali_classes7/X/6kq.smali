.class public LX/6kq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final days:Ljava/lang/Byte;

.field public final durationMinutes:Ljava/lang/Short;

.field public final startMinutes:Ljava/lang/Short;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 1141579
    new-instance v0, LX/1sv;

    const-string v1, "NotificationDoNotDisturbRange"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kq;->b:LX/1sv;

    .line 1141580
    new-instance v0, LX/1sw;

    const-string v1, "days"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kq;->c:LX/1sw;

    .line 1141581
    new-instance v0, LX/1sw;

    const-string v1, "startMinutes"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kq;->d:LX/1sw;

    .line 1141582
    new-instance v0, LX/1sw;

    const-string v1, "durationMinutes"

    invoke-direct {v0, v1, v5, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kq;->e:LX/1sw;

    .line 1141583
    sput-boolean v3, LX/6kq;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Byte;Ljava/lang/Short;Ljava/lang/Short;)V
    .locals 0

    .prologue
    .line 1141574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1141575
    iput-object p1, p0, LX/6kq;->days:Ljava/lang/Byte;

    .line 1141576
    iput-object p2, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    .line 1141577
    iput-object p3, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    .line 1141578
    return-void
.end method

.method public static a(LX/6kq;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1141567
    iget-object v0, p0, LX/6kq;->days:Ljava/lang/Byte;

    if-nez v0, :cond_0

    .line 1141568
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'days\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1141569
    :cond_0
    iget-object v0, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    if-nez v0, :cond_1

    .line 1141570
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'startMinutes\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1141571
    :cond_1
    iget-object v0, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    if-nez v0, :cond_2

    .line 1141572
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'durationMinutes\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1141573
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1141531
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1141532
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1141533
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1141534
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NotificationDoNotDisturbRange"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1141535
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141536
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141537
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141538
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141539
    const-string v4, "days"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141540
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141541
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141542
    iget-object v4, p0, LX/6kq;->days:Ljava/lang/Byte;

    if-nez v4, :cond_3

    .line 1141543
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141544
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141545
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141546
    const-string v4, "startMinutes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141547
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141548
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141549
    iget-object v4, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    if-nez v4, :cond_4

    .line 1141550
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141551
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141552
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141553
    const-string v4, "durationMinutes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141554
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141555
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141556
    iget-object v0, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    if-nez v0, :cond_5

    .line 1141557
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141558
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141559
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141560
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1141561
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1141562
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1141563
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1141564
    :cond_3
    iget-object v4, p0, LX/6kq;->days:Ljava/lang/Byte;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1141565
    :cond_4
    iget-object v4, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1141566
    :cond_5
    iget-object v0, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1141484
    invoke-static {p0}, LX/6kq;->a(LX/6kq;)V

    .line 1141485
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1141486
    iget-object v0, p0, LX/6kq;->days:Ljava/lang/Byte;

    if-eqz v0, :cond_0

    .line 1141487
    sget-object v0, LX/6kq;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141488
    iget-object v0, p0, LX/6kq;->days:Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(B)V

    .line 1141489
    :cond_0
    iget-object v0, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    if-eqz v0, :cond_1

    .line 1141490
    sget-object v0, LX/6kq;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141491
    iget-object v0, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(S)V

    .line 1141492
    :cond_1
    iget-object v0, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    if-eqz v0, :cond_2

    .line 1141493
    sget-object v0, LX/6kq;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141494
    iget-object v0, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(S)V

    .line 1141495
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1141496
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1141497
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1141502
    if-nez p1, :cond_1

    .line 1141503
    :cond_0
    :goto_0
    return v0

    .line 1141504
    :cond_1
    instance-of v1, p1, LX/6kq;

    if-eqz v1, :cond_0

    .line 1141505
    check-cast p1, LX/6kq;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1141506
    if-nez p1, :cond_3

    .line 1141507
    :cond_2
    :goto_1
    move v0, v2

    .line 1141508
    goto :goto_0

    .line 1141509
    :cond_3
    iget-object v0, p0, LX/6kq;->days:Ljava/lang/Byte;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1141510
    :goto_2
    iget-object v3, p1, LX/6kq;->days:Ljava/lang/Byte;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1141511
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1141512
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141513
    iget-object v0, p0, LX/6kq;->days:Ljava/lang/Byte;

    iget-object v3, p1, LX/6kq;->days:Ljava/lang/Byte;

    invoke-virtual {v0, v3}, Ljava/lang/Byte;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141514
    :cond_5
    iget-object v0, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1141515
    :goto_4
    iget-object v3, p1, LX/6kq;->startMinutes:Ljava/lang/Short;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1141516
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1141517
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141518
    iget-object v0, p0, LX/6kq;->startMinutes:Ljava/lang/Short;

    iget-object v3, p1, LX/6kq;->startMinutes:Ljava/lang/Short;

    invoke-virtual {v0, v3}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141519
    :cond_7
    iget-object v0, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1141520
    :goto_6
    iget-object v3, p1, LX/6kq;->durationMinutes:Ljava/lang/Short;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1141521
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1141522
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141523
    iget-object v0, p0, LX/6kq;->durationMinutes:Ljava/lang/Short;

    iget-object v3, p1, LX/6kq;->durationMinutes:Ljava/lang/Short;

    invoke-virtual {v0, v3}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1141524
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1141525
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1141526
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1141527
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1141528
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1141529
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1141530
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1141501
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1141498
    sget-boolean v0, LX/6kq;->a:Z

    .line 1141499
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kq;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1141500
    return-object v0
.end method
