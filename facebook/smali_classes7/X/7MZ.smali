.class public LX/7MZ;
.super LX/2oy;
.source ""


# instance fields
.field private final a:Lcom/facebook/resources/ui/FbTextView;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1198859
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7MZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198860
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198861
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7MZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198862
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1198863
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198864
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7MZ;->b:Ljava/util/Map;

    .line 1198865
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7MZ;->c:Ljava/util/Map;

    .line 1198866
    const v0, 0x7f0303ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1198867
    const v0, 0x7f0d0c28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7MZ;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1198868
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7MY;

    invoke-direct {v1, p0}, LX/7MY;-><init>(LX/7MZ;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1198869
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1198870
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1198871
    const-string v2, "%s=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1198872
    :cond_0
    return-void
.end method

.method public static h(LX/7MZ;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1198873
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1198874
    iget-object v0, p0, LX/7MZ;->b:Ljava/util/Map;

    invoke-static {v0, v2}, LX/7MZ;->a(Ljava/util/Map;Ljava/util/List;)V

    .line 1198875
    iget-object v0, p0, LX/7MZ;->c:Ljava/util/Map;

    invoke-static {v0, v2}, LX/7MZ;->a(Ljava/util/Map;Ljava/util/List;)V

    .line 1198876
    iget-object v3, p0, LX/7MZ;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1198877
    iget-object v0, p0, LX/7MZ;->a:Lcom/facebook/resources/ui/FbTextView;

    const-string v3, "\n"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v1

    invoke-static {v3, v4}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1198878
    return-void

    :cond_0
    move v0, v1

    .line 1198879
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 1198880
    if-eqz p2, :cond_0

    .line 1198881
    iget-object v0, p0, LX/7MZ;->c:Ljava/util/Map;

    const-string v1, "RichVideoPlayer Hash"

    iget-object v2, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198882
    :try_start_0
    iget-object v0, p0, LX/7MZ;->c:Ljava/util/Map;

    const-string v1, "Video Id"

    iget-object v2, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1198883
    :goto_0
    invoke-static {p0}, LX/7MZ;->h(LX/7MZ;)V

    .line 1198884
    :cond_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1198885
    iget-object v0, p0, LX/7MZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1198886
    iget-object v0, p0, LX/7MZ;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1198887
    return-void
.end method
