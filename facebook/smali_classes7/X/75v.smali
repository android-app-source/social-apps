.class public final enum LX/75v;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/75v;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/75v;

.field public static final enum NONE:LX/75v;

.field public static final enum ZLIB:LX/75v;

.field public static final enum ZLIB_OPTIONAL:LX/75v;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1170204
    new-instance v0, LX/75v;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, LX/75v;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/75v;->NONE:LX/75v;

    new-instance v0, LX/75v;

    const-string v1, "ZLIB"

    invoke-direct {v0, v1, v3, v3}, LX/75v;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/75v;->ZLIB:LX/75v;

    new-instance v0, LX/75v;

    const-string v1, "ZLIB_OPTIONAL"

    invoke-direct {v0, v1, v4, v4}, LX/75v;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/75v;->ZLIB_OPTIONAL:LX/75v;

    .line 1170205
    const/4 v0, 0x3

    new-array v0, v0, [LX/75v;

    sget-object v1, LX/75v;->NONE:LX/75v;

    aput-object v1, v0, v2

    sget-object v1, LX/75v;->ZLIB:LX/75v;

    aput-object v1, v0, v3

    sget-object v1, LX/75v;->ZLIB_OPTIONAL:LX/75v;

    aput-object v1, v0, v4

    sput-object v0, LX/75v;->$VALUES:[LX/75v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1170201
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1170202
    iput p3, p0, LX/75v;->value:I

    .line 1170203
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/75v;
    .locals 1

    .prologue
    .line 1170200
    const-class v0, LX/75v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/75v;

    return-object v0
.end method

.method public static values()[LX/75v;
    .locals 1

    .prologue
    .line 1170198
    sget-object v0, LX/75v;->$VALUES:[LX/75v;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/75v;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 1170199
    iget v0, p0, LX/75v;->value:I

    return v0
.end method
