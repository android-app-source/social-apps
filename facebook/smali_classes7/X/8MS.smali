.class public final LX/8MS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7ml;

.field public final synthetic b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V
    .locals 0

    .prologue
    .line 1334413
    iput-object p1, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iput-object p2, p0, LX/8MS;->a:LX/7ml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x4d216e5d    # 1.69272784E8f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1334414
    new-instance v1, LX/5OM;

    iget-object v2, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v2, v2, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1334415
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1334416
    iget-object v3, p0, LX/8MS;->a:LX/7ml;

    .line 1334417
    iget-object v4, v3, LX/7ml;->c:LX/7mk;

    move-object v3, v4

    .line 1334418
    sget-object v4, LX/7mk;->PHOTO:LX/7mk;

    if-ne v3, v4, :cond_0

    .line 1334419
    iget-object v3, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v4, p0, LX/8MS;->a:LX/7ml;

    .line 1334420
    const v5, 0x7f082060

    invoke-virtual {v2, v5}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v5

    .line 1334421
    new-instance v8, LX/8MU;

    invoke-direct {v8, v3, v4}, LX/8MU;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334422
    :cond_0
    iget-object v3, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v4, p0, LX/8MS;->a:LX/7ml;

    invoke-static {v3, v4}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)Z

    move-result v3

    .line 1334423
    if-eqz v3, :cond_1

    iget-object v4, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    .line 1334424
    iget-object v5, v4, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-short v8, LX/1aO;->av:S

    const/4 v9, 0x0

    invoke-interface {v5, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v5

    move v4, v5

    .line 1334425
    if-eqz v4, :cond_1

    .line 1334426
    iget-object v4, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v5, p0, LX/8MS;->a:LX/7ml;

    .line 1334427
    iget-object v8, v4, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v9, LX/1aO;->aw:C

    iget-object v10, v4, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f082062

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v9, v10}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object v8, v8

    .line 1334428
    invoke-virtual {v2, v8}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v8

    .line 1334429
    new-instance v9, LX/8MW;

    invoke-direct {v9, v4, v5}, LX/8MW;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334430
    :cond_1
    if-eqz v3, :cond_2

    iget-object v4, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-static {v4}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->B(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1334431
    iget-object v4, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v5, p0, LX/8MS;->a:LX/7ml;

    invoke-static {v4, v5, v2}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;LX/5OG;)V

    .line 1334432
    :cond_2
    if-eqz v3, :cond_3

    iget-object v3, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    .line 1334433
    iget-object v4, v3, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-short v5, LX/1aO;->at:S

    const/4 v8, 0x0

    invoke-interface {v4, v5, v8}, LX/0ad;->a(SZ)Z

    move-result v4

    move v3, v4

    .line 1334434
    if-eqz v3, :cond_4

    .line 1334435
    :cond_3
    iget-object v3, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v4, p0, LX/8MS;->a:LX/7ml;

    invoke-static {v3, v4, v2, v7}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;LX/5OG;Z)V

    .line 1334436
    :cond_4
    invoke-virtual {v1, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1334437
    iget-object v2, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-static {v2, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/5OM;)V

    .line 1334438
    iput-boolean v7, v1, LX/0ht;->e:Z

    .line 1334439
    invoke-virtual {v1}, LX/0ht;->d()V

    .line 1334440
    iget-object v1, p0, LX/8MS;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v1, v1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    sget-object v2, LX/8K6;->PENDING_SECTION:LX/8K6;

    iget-object v2, v2, LX/8K6;->analyticsName:Ljava/lang/String;

    iget-object v3, p0, LX/8MS;->a:LX/7ml;

    invoke-virtual {v3}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1RW;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334441
    const v1, -0x4e80dc22

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
