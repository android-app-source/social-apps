.class public final enum LX/6em;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6em;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6em;

.field public static final enum ALL:LX/6em;

.field public static final enum NON_SMS:LX/6em;

.field public static final enum SMS:LX/6em;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1118167
    new-instance v0, LX/6em;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v2}, LX/6em;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6em;->ALL:LX/6em;

    .line 1118168
    new-instance v0, LX/6em;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v3}, LX/6em;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6em;->SMS:LX/6em;

    .line 1118169
    new-instance v0, LX/6em;

    const-string v1, "NON_SMS"

    invoke-direct {v0, v1, v4}, LX/6em;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6em;->NON_SMS:LX/6em;

    .line 1118170
    const/4 v0, 0x3

    new-array v0, v0, [LX/6em;

    sget-object v1, LX/6em;->ALL:LX/6em;

    aput-object v1, v0, v2

    sget-object v1, LX/6em;->SMS:LX/6em;

    aput-object v1, v0, v3

    sget-object v1, LX/6em;->NON_SMS:LX/6em;

    aput-object v1, v0, v4

    sput-object v0, LX/6em;->$VALUES:[LX/6em;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1118166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6em;
    .locals 1

    .prologue
    .line 1118172
    const-class v0, LX/6em;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6em;

    return-object v0
.end method

.method public static values()[LX/6em;
    .locals 1

    .prologue
    .line 1118171
    sget-object v0, LX/6em;->$VALUES:[LX/6em;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6em;

    return-object v0
.end method
