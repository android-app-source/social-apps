.class public final LX/6qi;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/CheckoutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 0

    .prologue
    .line 1151029
    iput-object p1, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 3

    .prologue
    .line 1151030
    iget-object v0, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    const/4 p0, -0x1

    .line 1151031
    sget-object v1, LX/6qg;->a:[I

    .line 1151032
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 1151033
    invoke-virtual {v2}, LX/73S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1151034
    :cond_0
    :goto_0
    return-void

    .line 1151035
    :pswitch_0
    iget-object v1, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v2, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1, v2, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/73T;)V

    goto :goto_0

    .line 1151036
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    .line 1151037
    if-eqz v2, :cond_0

    .line 1151038
    const-string v1, "extra_activity_result_data"

    invoke-virtual {p1, v1}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 1151039
    if-eqz v1, :cond_1

    .line 1151040
    invoke-virtual {v2, p0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1151041
    :goto_1
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1151042
    :cond_1
    invoke-virtual {v2, p0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1

    .line 1151043
    :pswitch_2
    invoke-static {v0}, Lcom/facebook/payments/checkout/CheckoutFragment;->o(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1151044
    iget-object v0, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1151045
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1151046
    iget-object v0, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1151047
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 2

    .prologue
    .line 1151048
    iget-object v1, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    if-eqz p2, :cond_0

    sget-object v0, LX/6qt;->PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/6qt;

    :goto_0
    invoke-static {v1, v0, p1}, Lcom/facebook/payments/checkout/CheckoutFragment;->a$redex0(Lcom/facebook/payments/checkout/CheckoutFragment;LX/6qt;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1151049
    return-void

    .line 1151050
    :cond_0
    sget-object v0, LX/6qt;->PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/6qt;

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1151051
    iget-object v0, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6qi;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1151052
    return-void
.end method
