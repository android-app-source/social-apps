.class public final enum LX/8GB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8GB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8GB;

.field public static final enum FRAME_ASSET_DOWNLOAD_REQUEST_FAILURE:LX/8GB;

.field public static final enum FRAME_ASSET_DOWNLOAD_REQUEST_SUBMITTED:LX/8GB;

.field public static final enum FRAME_ASSET_DOWNLOAD_REQUEST_SUCCESS:LX/8GB;

.field public static final enum FRAME_DELTA_RECEIVED:LX/8GB;

.field public static final enum FRAME_IGNORED_ASSET_MISSING:LX/8GB;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1318714
    new-instance v0, LX/8GB;

    const-string v1, "FRAME_DELTA_RECEIVED"

    const-string v2, "frame_delta_received"

    invoke-direct {v0, v1, v3, v2}, LX/8GB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GB;->FRAME_DELTA_RECEIVED:LX/8GB;

    .line 1318715
    new-instance v0, LX/8GB;

    const-string v1, "FRAME_ASSET_DOWNLOAD_REQUEST_SUBMITTED"

    const-string v2, "frame_asset_download_request_submitted"

    invoke-direct {v0, v1, v4, v2}, LX/8GB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GB;->FRAME_ASSET_DOWNLOAD_REQUEST_SUBMITTED:LX/8GB;

    .line 1318716
    new-instance v0, LX/8GB;

    const-string v1, "FRAME_ASSET_DOWNLOAD_REQUEST_SUCCESS"

    const-string v2, "frame_asset_download_request_success"

    invoke-direct {v0, v1, v5, v2}, LX/8GB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GB;->FRAME_ASSET_DOWNLOAD_REQUEST_SUCCESS:LX/8GB;

    .line 1318717
    new-instance v0, LX/8GB;

    const-string v1, "FRAME_ASSET_DOWNLOAD_REQUEST_FAILURE"

    const-string v2, "frame_asset_download_request_failure"

    invoke-direct {v0, v1, v6, v2}, LX/8GB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GB;->FRAME_ASSET_DOWNLOAD_REQUEST_FAILURE:LX/8GB;

    .line 1318718
    new-instance v0, LX/8GB;

    const-string v1, "FRAME_IGNORED_ASSET_MISSING"

    const-string v2, "frame_ignored_asset_missing"

    invoke-direct {v0, v1, v7, v2}, LX/8GB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8GB;->FRAME_IGNORED_ASSET_MISSING:LX/8GB;

    .line 1318719
    const/4 v0, 0x5

    new-array v0, v0, [LX/8GB;

    sget-object v1, LX/8GB;->FRAME_DELTA_RECEIVED:LX/8GB;

    aput-object v1, v0, v3

    sget-object v1, LX/8GB;->FRAME_ASSET_DOWNLOAD_REQUEST_SUBMITTED:LX/8GB;

    aput-object v1, v0, v4

    sget-object v1, LX/8GB;->FRAME_ASSET_DOWNLOAD_REQUEST_SUCCESS:LX/8GB;

    aput-object v1, v0, v5

    sget-object v1, LX/8GB;->FRAME_ASSET_DOWNLOAD_REQUEST_FAILURE:LX/8GB;

    aput-object v1, v0, v6

    sget-object v1, LX/8GB;->FRAME_IGNORED_ASSET_MISSING:LX/8GB;

    aput-object v1, v0, v7

    sput-object v0, LX/8GB;->$VALUES:[LX/8GB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1318720
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1318721
    iput-object p3, p0, LX/8GB;->mName:Ljava/lang/String;

    .line 1318722
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8GB;
    .locals 1

    .prologue
    .line 1318723
    const-class v0, LX/8GB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8GB;

    return-object v0
.end method

.method public static values()[LX/8GB;
    .locals 1

    .prologue
    .line 1318724
    sget-object v0, LX/8GB;->$VALUES:[LX/8GB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8GB;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1318725
    iget-object v0, p0, LX/8GB;->mName:Ljava/lang/String;

    return-object v0
.end method
