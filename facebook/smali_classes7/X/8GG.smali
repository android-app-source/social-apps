.class public LX/8GG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8G6;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/8G6;

.field public final c:[Z

.field public d:I


# direct methods
.method public constructor <init>(LX/0Px;LX/8G6;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;",
            "LX/8G6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1318747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318748
    const/4 v0, 0x0

    iput v0, p0, LX/8GG;->d:I

    .line 1318749
    iput-object p1, p0, LX/8GG;->a:LX/0Px;

    .line 1318750
    iput-object p2, p0, LX/8GG;->b:LX/8G6;

    .line 1318751
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, LX/8GG;->c:[Z

    .line 1318752
    return-void
.end method

.method private b(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)I
    .locals 3

    .prologue
    .line 1318753
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/8GG;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1318754
    iget-object v0, p0, LX/8GG;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318755
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318756
    return v1

    .line 1318757
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1318758
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected frame: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V
    .locals 3

    .prologue
    .line 1318759
    invoke-direct {p0, p1}, LX/8GG;->b(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)I

    move-result v0

    .line 1318760
    iget-object v1, p0, LX/8GG;->c:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 1318761
    :goto_0
    iget v0, p0, LX/8GG;->d:I

    iget-object v1, p0, LX/8GG;->c:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/8GG;->c:[Z

    iget v1, p0, LX/8GG;->d:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_0

    .line 1318762
    iget-object v1, p0, LX/8GG;->b:LX/8G6;

    iget-object v0, p0, LX/8GG;->a:LX/0Px;

    iget v2, p0, LX/8GG;->d:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-interface {v1, v0}, LX/8G6;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V

    .line 1318763
    iget v0, p0, LX/8GG;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/8GG;->d:I

    goto :goto_0

    .line 1318764
    :cond_0
    return-void
.end method
