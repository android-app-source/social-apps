.class public LX/8N8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1335982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335983
    iput-object p1, p0, LX/8N8;->a:LX/0SG;

    .line 1335984
    return-void
.end method

.method public static a(LX/0QB;)LX/8N8;
    .locals 2

    .prologue
    .line 1335985
    new-instance v1, LX/8N8;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-direct {v1, v0}, LX/8N8;-><init>(LX/0SG;)V

    .line 1335986
    move-object v0, v1

    .line 1335987
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 14

    .prologue
    .line 1335988
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    const-wide/16 v10, 0x0

    const/high16 v1, 0x3f000000    # 0.5f

    .line 1335989
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1335990
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1335991
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1335992
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "qn"

    invoke-direct {v2, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335993
    :cond_0
    iget-wide v12, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    move-wide v4, v12

    .line 1335994
    cmp-long v0, v4, v10

    if-eqz v0, :cond_1

    .line 1335995
    iget-object v0, p0, LX/8N8;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    .line 1335996
    sub-long v4, v6, v4

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 1335997
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "time_since_original_post"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335998
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "photo"

    .line 1335999
    iget-wide v12, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v4, v12

    .line 1336000
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336001
    iget v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->G:F

    move v0, v0

    .line 1336002
    iget v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->H:F

    move v2, v2

    .line 1336003
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v1

    .line 1336004
    :cond_2
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1336005
    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "focus_x"

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336006
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "focus_y"

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336007
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/cover"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1336008
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "publish-photo"

    .line 1336009
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1336010
    move-object v1, v1

    .line 1336011
    const-string v2, "POST"

    .line 1336012
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1336013
    move-object v1, v1

    .line 1336014
    iput-object v0, v1, LX/14O;->d:Ljava/lang/String;

    .line 1336015
    move-object v0, v1

    .line 1336016
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1336017
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1336018
    move-object v0, v0

    .line 1336019
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1336020
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1336021
    move-object v0, v0

    .line 1336022
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1336023
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1336024
    iget-wide v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v0, v2

    .line 1336025
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
