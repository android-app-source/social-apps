.class public final LX/7Ox;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final synthetic a:LX/7P0;


# direct methods
.method public constructor <init>(LX/7P0;)V
    .locals 0

    .prologue
    .line 1202341
    iput-object p1, p0, LX/7Ox;->a:LX/7P0;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    return-void
.end method

.method private a([BII)I
    .locals 5

    .prologue
    .line 1202342
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-object v1, v0, LX/7P0;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 1202343
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-boolean v0, v0, LX/7P0;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget v0, v0, LX/7P0;->g:I

    iget-object v2, p0, LX/7Ox;->a:LX/7P0;

    iget-object v2, v2, LX/7P0;->e:[B

    array-length v2, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_0

    .line 1202344
    :try_start_1
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-object v0, v0, LX/7P0;->j:Ljava/lang/Object;

    const v2, -0x1253fedb

    invoke-static {v0, v2}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1202345
    :catch_0
    move-exception v0

    .line 1202346
    :try_start_2
    new-instance v2, Ljava/io/InterruptedIOException;

    invoke-direct {v2}, Ljava/io/InterruptedIOException;-><init>()V

    invoke-virtual {v2, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/InterruptedIOException;

    throw v0

    .line 1202347
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1202348
    :cond_0
    :try_start_3
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-boolean v0, v0, LX/7P0;->h:Z

    if-eqz v0, :cond_1

    .line 1202349
    new-instance v0, LX/7Oz;

    invoke-direct {v0}, LX/7Oz;-><init>()V

    throw v0

    .line 1202350
    :cond_1
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget v0, v0, LX/7P0;->f:I

    .line 1202351
    iget-object v2, p0, LX/7Ox;->a:LX/7P0;

    iget v2, v2, LX/7P0;->g:I

    .line 1202352
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1202353
    iget-object v1, p0, LX/7Ox;->a:LX/7P0;

    iget-object v1, v1, LX/7P0;->e:[B

    array-length v1, v1

    sub-int/2addr v1, v2

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1202354
    add-int/2addr v0, v2

    iget-object v2, p0, LX/7Ox;->a:LX/7P0;

    iget-object v2, v2, LX/7P0;->e:[B

    array-length v2, v2

    rem-int/2addr v0, v2

    .line 1202355
    iget-object v2, p0, LX/7Ox;->a:LX/7P0;

    iget-object v2, v2, LX/7P0;->e:[B

    array-length v2, v2

    sub-int/2addr v2, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1202356
    iget-object v3, p0, LX/7Ox;->a:LX/7P0;

    iget-object v3, v3, LX/7P0;->e:[B

    invoke-static {p1, p2, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1202357
    if-ge v2, v1, :cond_2

    .line 1202358
    add-int v0, p2, v2

    .line 1202359
    sub-int v2, v1, v2

    .line 1202360
    iget-object v3, p0, LX/7Ox;->a:LX/7P0;

    iget-object v3, v3, LX/7P0;->e:[B

    const/4 v4, 0x0

    invoke-static {p1, v0, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1202361
    :cond_2
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-object v2, v0, LX/7P0;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 1202362
    :try_start_4
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    .line 1202363
    iget v3, v0, LX/7P0;->g:I

    add-int/2addr v3, v1

    iput v3, v0, LX/7P0;->g:I

    .line 1202364
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-object v0, v0, LX/7P0;->j:Ljava/lang/Object;

    const v3, -0x2b261de2

    invoke-static {v0, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1202365
    monitor-exit v2

    .line 1202366
    return v1

    .line 1202367
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 1202368
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-object v1, v0, LX/7P0;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 1202369
    :try_start_0
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    const/4 v2, 0x1

    .line 1202370
    iput-boolean v2, v0, LX/7P0;->i:Z

    .line 1202371
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    iget-object v0, v0, LX/7P0;->j:Ljava/lang/Object;

    const v2, 0x75dd3499

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1202372
    iget-object v0, p0, LX/7Ox;->a:LX/7P0;

    invoke-static {v0}, LX/7P0;->a(LX/7P0;)V

    .line 1202373
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write(I)V
    .locals 1

    .prologue
    .line 1202374
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final write([BII)V
    .locals 1

    .prologue
    .line 1202375
    :goto_0
    if-lez p3, :cond_0

    .line 1202376
    invoke-direct {p0, p1, p2, p3}, LX/7Ox;->a([BII)I

    move-result v0

    .line 1202377
    sub-int/2addr p3, v0

    .line 1202378
    add-int/2addr p2, v0

    .line 1202379
    goto :goto_0

    .line 1202380
    :cond_0
    return-void
.end method
