.class public LX/77Z;
.super LX/2g7;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/77Z;


# instance fields
.field private final a:LX/13O;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/13O;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171908
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171909
    iput-object p1, p0, LX/77Z;->a:LX/13O;

    .line 1171910
    iput-object p2, p0, LX/77Z;->b:LX/0SG;

    .line 1171911
    return-void
.end method

.method public static a(LX/0QB;)LX/77Z;
    .locals 5

    .prologue
    .line 1171912
    sget-object v0, LX/77Z;->c:LX/77Z;

    if-nez v0, :cond_1

    .line 1171913
    const-class v1, LX/77Z;

    monitor-enter v1

    .line 1171914
    :try_start_0
    sget-object v0, LX/77Z;->c:LX/77Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171915
    if-eqz v2, :cond_0

    .line 1171916
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171917
    new-instance p0, LX/77Z;

    invoke-static {v0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v3

    check-cast v3, LX/13O;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/77Z;-><init>(LX/13O;LX/0SG;)V

    .line 1171918
    move-object v0, p0

    .line 1171919
    sput-object v0, LX/77Z;->c:LX/77Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171920
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171921
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171922
    :cond_1
    sget-object v0, LX/77Z;->c:LX/77Z;

    return-object v0

    .line 1171923
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 8

    .prologue
    .line 1171925
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171926
    iget-object v0, p0, LX/77Z;->a:LX/13O;

    sget-object v1, LX/77X;->MESSAGE_RECEIVED:LX/77X;

    invoke-virtual {v1}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/13O;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 1171927
    iget-object v2, p0, LX/77Z;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 1171928
    iget-object v4, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 1171929
    add-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
