.class public LX/8DE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# instance fields
.field private a:LX/82T;


# direct methods
.method public constructor <init>(LX/82T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312382
    iput-object p1, p0, LX/8DE;->a:LX/82T;

    .line 1312383
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1312384
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1312385
    iget-object v0, p0, LX/8DE;->a:LX/82T;

    invoke-virtual {v0}, LX/82T;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1312386
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1312387
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1312388
    const-string v0, "3573"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1312389
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ACTIONBAR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
