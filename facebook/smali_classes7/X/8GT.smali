.class public LX/8GT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/8GT;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0en;

.field public final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final d:LX/0Tf;

.field private final e:LX/8GS;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0en;LX/0Tf;)V
    .locals 2
    .param p3    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1319513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319514
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/8GT;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1319515
    new-instance v0, LX/8GS;

    invoke-direct {v0, p0}, LX/8GS;-><init>(LX/8GT;)V

    iput-object v0, p0, LX/8GT;->e:LX/8GS;

    .line 1319516
    iput-object p1, p0, LX/8GT;->a:Landroid/content/Context;

    .line 1319517
    iput-object p2, p0, LX/8GT;->b:LX/0en;

    .line 1319518
    iput-object p3, p0, LX/8GT;->d:LX/0Tf;

    .line 1319519
    return-void
.end method

.method public static a(LX/0QB;)LX/8GT;
    .locals 6

    .prologue
    .line 1319520
    sget-object v0, LX/8GT;->f:LX/8GT;

    if-nez v0, :cond_1

    .line 1319521
    const-class v1, LX/8GT;

    monitor-enter v1

    .line 1319522
    :try_start_0
    sget-object v0, LX/8GT;->f:LX/8GT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1319523
    if-eqz v2, :cond_0

    .line 1319524
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1319525
    new-instance p0, LX/8GT;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v4

    check-cast v4, LX/0en;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, LX/0Tf;

    invoke-direct {p0, v3, v4, v5}, LX/8GT;-><init>(Landroid/content/Context;LX/0en;LX/0Tf;)V

    .line 1319526
    move-object v0, p0

    .line 1319527
    sput-object v0, LX/8GT;->f:LX/8GT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1319528
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1319529
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1319530
    :cond_1
    sget-object v0, LX/8GT;->f:LX/8GT;

    return-object v0

    .line 1319531
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1319532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8GT;Ljava/lang/String;Z)Ljava/io/File;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1319533
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1319534
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, LX/8GT;->a:Landroid/content/Context;

    const-string v3, "ce"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1319535
    if-nez p2, :cond_1

    .line 1319536
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 1319537
    goto :goto_0

    .line 1319538
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1319539
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Could not create directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1319540
    :cond_2
    iget-object v1, p0, LX/8GT;->e:LX/8GS;

    .line 1319541
    iget-object v4, v1, LX/8GS;->a:LX/8GT;

    iget-object v4, v4, LX/8GT;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1319542
    iget-object v4, v1, LX/8GS;->a:LX/8GT;

    iget-object v4, v4, LX/8GT;->d:LX/0Tf;

    iget-object v5, v1, LX/8GS;->b:Ljava/lang/Runnable;

    const-wide/16 v6, 0xa

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v4

    .line 1319543
    iget-object v5, v1, LX/8GS;->c:Ljava/lang/Runnable;

    iget-object v6, v1, LX/8GS;->a:LX/8GT;

    iget-object v6, v6, LX/8GT;->d:LX/0Tf;

    invoke-interface {v4, v5, v6}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1319544
    :cond_3
    goto :goto_1
.end method

.method public static a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1319545
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1319546
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1319547
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1319548
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 1319549
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1319550
    invoke-static {p0, p1, v1}, LX/8GT;->a(LX/8GT;Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v4

    .line 1319551
    if-nez v4, :cond_1

    .line 1319552
    const/4 v0, 0x0

    .line 1319553
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 1319554
    goto :goto_0

    .line 1319555
    :cond_1
    new-instance v0, Ljava/io/File;

    const-string v5, "%s.%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v2

    aput-object p2, v6, v1

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_1
.end method
