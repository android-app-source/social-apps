.class public LX/88n;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/88r;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1302947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1302948
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1302949
    iput-object v0, p0, LX/88n;->a:LX/0Ot;

    .line 1302950
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1302951
    iput-object v0, p0, LX/88n;->b:LX/0Ot;

    .line 1302952
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1302953
    iput-object v0, p0, LX/88n;->c:LX/0Ot;

    .line 1302954
    return-void
.end method

.method public static a(LX/0QB;)LX/88n;
    .locals 1

    .prologue
    .line 1302939
    invoke-static {p0}, LX/88n;->b(LX/0QB;)LX/88n;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/88n;
    .locals 4

    .prologue
    .line 1302955
    new-instance v0, LX/88n;

    invoke-direct {v0}, LX/88n;-><init>()V

    .line 1302956
    const/16 v1, 0x1032

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x12b1

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x23fc

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 1302957
    iput-object v1, v0, LX/88n;->a:LX/0Ot;

    iput-object v2, v0, LX/88n;->b:LX/0Ot;

    iput-object v3, v0, LX/88n;->c:LX/0Ot;

    .line 1302958
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1302940
    iget-object v0, p0, LX/88n;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1302941
    iget-object v0, p0, LX/88n;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/88r;

    .line 1302942
    sget-object p0, LX/88q;->a:Ljava/util/EnumSet;

    invoke-virtual {p0}, Ljava/util/EnumSet;->clone()Ljava/util/EnumSet;

    move-result-object p0

    iput-object p0, v0, LX/88r;->a:Ljava/util/EnumSet;

    .line 1302943
    iget-object p0, v0, LX/88r;->c:LX/1Ck;

    invoke-virtual {p0}, LX/1Ck;->c()V

    .line 1302944
    iget-object p0, v0, LX/88r;->b:LX/88t;

    .line 1302945
    iget-object v0, p0, LX/88t;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V

    .line 1302946
    return-void
.end method

.method public final a(LX/88p;Ljava/util/concurrent/Callable;LX/0Ve;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/88p;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;>;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1302932
    iget-object v0, p0, LX/88n;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/88j;->f:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1302933
    iget-object v0, p0, LX/88n;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/88r;

    new-instance v1, LX/88q;

    invoke-direct {v1, p2, p3, p1}, LX/88q;-><init>(Ljava/util/concurrent/Callable;LX/0Ve;LX/88p;)V

    .line 1302934
    iget-object v2, v0, LX/88r;->b:LX/88t;

    .line 1302935
    iget-object p0, v2, LX/88t;->a:Ljava/util/PriorityQueue;

    invoke-virtual {p0, v1}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    .line 1302936
    invoke-static {v0}, LX/88r;->b(LX/88r;)V

    .line 1302937
    :goto_0
    return-void

    .line 1302938
    :cond_0
    iget-object v0, p0, LX/88n;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0, p1, p2, p3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
