.class public LX/8JL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:Ljava/lang/Object;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/2Rd;

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0rq;

.field private g:LX/0sa;

.field public h:LX/03V;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1328456
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, LX/8JL;->a:Ljava/util/List;

    .line 1328457
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/8JL;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/2Rd;LX/0Or;LX/0rq;LX/0sa;LX/03V;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/photoset/gatekeeper/IsInFamilyTaggingAndroid;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/2Rd;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0rq;",
            "LX/0sa;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1328359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1328360
    iput-object p1, p0, LX/8JL;->b:LX/0Or;

    .line 1328361
    iput-object p2, p0, LX/8JL;->c:LX/0Ot;

    .line 1328362
    iput-object p3, p0, LX/8JL;->d:LX/2Rd;

    .line 1328363
    iput-object p4, p0, LX/8JL;->e:LX/0Or;

    .line 1328364
    iput-object p5, p0, LX/8JL;->f:LX/0rq;

    .line 1328365
    iput-object p6, p0, LX/8JL;->g:LX/0sa;

    .line 1328366
    iput-object p7, p0, LX/8JL;->h:LX/03V;

    .line 1328367
    sget-object v0, LX/8JL;->a:Ljava/util/List;

    iput-object v0, p0, LX/8JL;->i:Ljava/util/List;

    .line 1328368
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8JL;->j:Z

    .line 1328369
    return-void
.end method

.method public static a(LX/0QB;)LX/8JL;
    .locals 15

    .prologue
    .line 1328370
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1328371
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1328372
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1328373
    if-nez v1, :cond_0

    .line 1328374
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1328375
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1328376
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1328377
    sget-object v1, LX/8JL;->k:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1328378
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1328379
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1328380
    :cond_1
    if-nez v1, :cond_4

    .line 1328381
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1328382
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1328383
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1328384
    new-instance v7, LX/8JL;

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0xafd

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v10

    check-cast v10, LX/2Rd;

    const/16 v11, 0x154d

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v12

    check-cast v12, LX/0rq;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v13

    check-cast v13, LX/0sa;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-direct/range {v7 .. v14}, LX/8JL;-><init>(LX/0Or;LX/0Ot;LX/2Rd;LX/0Or;LX/0rq;LX/0sa;LX/03V;)V

    .line 1328385
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1328386
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1328387
    if-nez v1, :cond_2

    .line 1328388
    sget-object v0, LX/8JL;->k:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8JL;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1328389
    :goto_1
    if-eqz v0, :cond_3

    .line 1328390
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1328391
    :goto_3
    check-cast v0, LX/8JL;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1328392
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1328393
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1328394
    :catchall_1
    move-exception v0

    .line 1328395
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1328396
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1328397
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1328398
    :cond_2
    :try_start_8
    sget-object v0, LX/8JL;->k:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8JL;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a$redex0(LX/8JL;Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    const v13, 0x5f9b6617

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1328399
    if-eqz p1, :cond_0

    .line 1328400
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1328401
    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    .line 1328402
    :goto_2
    return-object v4

    .line 1328403
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1328404
    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1328405
    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_0

    .line 1328406
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1328407
    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v3, v13}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1328408
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_5

    move v0, v2

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_1

    .line 1328409
    :cond_6
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1328410
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1328411
    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v3, v13}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v7

    move v6, v3

    .line 1328412
    :goto_5
    if-ge v6, v7, :cond_b

    .line 1328413
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1328414
    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v3, v13}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_6
    invoke-virtual {v0, v6}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1328415
    const-class v8, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v0, v3, v8}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    .line 1328416
    new-instance v8, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v4, v1, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1328417
    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 1328418
    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 1328419
    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v1

    iget-object v12, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v12, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    move v1, v2

    :goto_7
    if-eqz v1, :cond_c

    .line 1328420
    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1328421
    :goto_8
    new-instance v1, LX/7Gq;

    invoke-direct {v1}, LX/7Gq;-><init>()V

    .line 1328422
    iput-object v8, v1, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 1328423
    move-object v1, v1

    .line 1328424
    iget-object v8, p0, LX/8JL;->d:LX/2Rd;

    invoke-virtual {v8, v9}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1328425
    iput-object v8, v1, LX/7Gq;->g:Ljava/lang/String;

    .line 1328426
    move-object v1, v1

    .line 1328427
    iput-object v9, v1, LX/7Gq;->f:Ljava/lang/String;

    .line 1328428
    move-object v1, v1

    .line 1328429
    invoke-virtual {v1, v10, v11}, LX/7Gq;->a(J)LX/7Gq;

    move-result-object v1

    sget-object v8, LX/7Gr;->FAMILY_NONUSER_MEMBER:LX/7Gr;

    .line 1328430
    iput-object v8, v1, LX/7Gq;->e:LX/7Gr;

    .line 1328431
    move-object v1, v1

    .line 1328432
    iput-object v0, v1, LX/7Gq;->c:Ljava/lang/String;

    .line 1328433
    move-object v0, v1

    .line 1328434
    invoke-virtual {v0}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    .line 1328435
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1328436
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_5

    .line 1328437
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_4

    .line 1328438
    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_6

    :cond_9
    move v1, v3

    .line 1328439
    goto :goto_7

    :cond_a
    move v1, v3

    goto :goto_7

    :cond_b
    move-object v4, v5

    .line 1328440
    goto/16 :goto_2

    :cond_c
    move-object v0, v4

    goto :goto_8
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1328441
    invoke-virtual {p0}, LX/8JL;->b()V

    .line 1328442
    iget-object v0, p0, LX/8JL;->i:Ljava/util/List;

    return-object v0
.end method

.method public final declared-synchronized b()V
    .locals 7

    .prologue
    .line 1328443
    monitor-enter p0

    .line 1328444
    :try_start_0
    iget-object v0, p0, LX/8JL;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 1328445
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8JL;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1328446
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1328447
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/8JL;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1328448
    new-instance v3, LX/8Jp;

    invoke-direct {v3}, LX/8Jp;-><init>()V

    move-object v3, v3

    .line 1328449
    const-string v4, "targetId"

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_media_type"

    iget-object v6, p0, LX/8JL;->f:LX/0rq;

    invoke-virtual {v6}, LX/0rq;->b()LX/0wF;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1328450
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x3c

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 1328451
    iget-object v3, p0, LX/8JL;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1328452
    move-object v0, v3

    .line 1328453
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8JL;->j:Z

    .line 1328454
    new-instance v1, LX/8JK;

    invoke-direct {v1, p0}, LX/8JK;-><init>(LX/8JL;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1328455
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
