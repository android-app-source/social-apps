.class public final LX/7OB;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7N3;


# direct methods
.method public constructor <init>(LX/7N3;)V
    .locals 0

    .prologue
    .line 1201210
    iput-object p1, p0, LX/7OB;->a:LX/7N3;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1201211
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1201212
    check-cast p1, LX/2ou;

    const/4 v2, 0x1

    .line 1201213
    iget-object v0, p0, LX/7OB;->a:LX/7N3;

    iget-boolean v0, v0, LX/7N3;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1201214
    iget-object v0, p0, LX/7OB;->a:LX/7N3;

    .line 1201215
    iput-boolean v2, v0, LX/7N3;->r:Z

    .line 1201216
    :cond_0
    iget-object v0, p0, LX/7OB;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->AUTO:LX/7MP;

    if-ne v0, v1, :cond_2

    .line 1201217
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-eq v0, v1, :cond_1

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->SEEKING:LX/2qV;

    if-ne v0, v1, :cond_3

    .line 1201218
    :cond_1
    iget-object v0, p0, LX/7OB;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->q:LX/7O7;

    invoke-virtual {v0, v2}, LX/7O7;->removeMessages(I)V

    .line 1201219
    :cond_2
    :goto_0
    return-void

    .line 1201220
    :cond_3
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_4

    .line 1201221
    iget-object v0, p0, LX/7OB;->a:LX/7N3;

    invoke-virtual {v0}, LX/7N3;->i()V

    goto :goto_0

    .line 1201222
    :cond_4
    iget-object v0, p0, LX/7OB;->a:LX/7N3;

    iget-boolean v0, v0, LX/7N3;->r:Z

    if-eqz v0, :cond_2

    .line 1201223
    iget-object v0, p0, LX/7OB;->a:LX/7N3;

    invoke-virtual {v0}, LX/7N3;->j()V

    goto :goto_0
.end method
