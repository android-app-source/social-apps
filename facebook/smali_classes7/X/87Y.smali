.class public LX/87Y;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0tO;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1300349
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    sput-object v0, LX/87Y;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0tO;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300351
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1300352
    iput-object v0, p0, LX/87Y;->d:LX/0Px;

    .line 1300353
    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, LX/87Y;->e:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1300354
    iput-object p1, p0, LX/87Y;->b:Landroid/content/Context;

    .line 1300355
    iput-object p2, p0, LX/87Y;->c:LX/0tO;

    .line 1300356
    iget-object v0, p0, LX/87Y;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1300357
    :cond_0
    :goto_0
    return-void

    .line 1300358
    :cond_1
    iget-object v0, p0, LX/87Y;->c:LX/0tO;

    const/4 p2, 0x0

    const/4 v1, 0x0

    .line 1300359
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget-short p1, LX/0wk;->y:S

    invoke-interface {v2, p1, p2}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1300360
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget-char p1, LX/0wk;->G:C

    invoke-interface {v2, p1, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1300361
    :cond_2
    :goto_1
    move-object v0, v1

    .line 1300362
    new-instance v1, LX/87a;

    iget-object v2, p0, LX/87Y;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/87a;-><init>(Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 1300363
    invoke-virtual {v1}, LX/87a;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1300364
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    sget-object v2, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v1}, LX/87a;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/87Y;->d:LX/0Px;

    .line 1300365
    const/4 v0, 0x0

    iput v0, p0, LX/87Y;->f:I

    .line 1300366
    iget-object v0, p0, LX/87Y;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    :goto_2
    iput-object v0, p0, LX/87Y;->e:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/87Y;->d:LX/0Px;

    iget v1, p0, LX/87Y;->f:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_2

    .line 1300367
    :cond_4
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget-short p1, LX/0wk;->q:S

    invoke-interface {v2, p1, p2}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1300368
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget-char p1, LX/0wk;->u:C

    invoke-interface {v2, p1, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            "LX/1c9",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;)I"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1300337
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/1c9;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1300338
    invoke-virtual {p1, v1}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1300339
    invoke-static {p0, v0}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300340
    :goto_1
    return v1

    .line 1300341
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1300342
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/16 v3, 0x23

    const/4 v1, 0x0

    .line 1300343
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 1300344
    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    if-ne v2, v3, :cond_1

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    .line 1300345
    :goto_1
    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1300346
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 1300347
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1300348
    goto :goto_2
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1300332
    if-ne p0, p1, :cond_1

    .line 1300333
    :cond_0
    :goto_0
    return v0

    .line 1300334
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 1300335
    goto :goto_0

    .line 1300336
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/87Y;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/87Y;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/87Y;
    .locals 3

    .prologue
    .line 1300330
    new-instance v2, LX/87Y;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v1

    check-cast v1, LX/0tO;

    invoke-direct {v2, v0, v1}, LX/87Y;-><init>(Landroid/content/Context;LX/0tO;)V

    .line 1300331
    return-object v2
.end method


# virtual methods
.method public final a(I)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .locals 1

    .prologue
    .line 1300329
    iget-object v0, p0, LX/87Y;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1300328
    iget-object v0, p0, LX/87Y;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
