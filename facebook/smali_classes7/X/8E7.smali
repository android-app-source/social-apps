.class public final enum LX/8E7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8E7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8E7;

.field public static final enum PAGES_DEFAULT_TAB_FRAGMENT_LOADED:LX/8E7;

.field public static final enum PAGES_FIRST_CARD_DATA_LOADED:LX/8E7;

.field public static final enum PAGES_FIRST_CARD_LOADING_TIMER:LX/8E7;

.field public static final enum PAGES_FIRST_CARD_RENDERED:LX/8E7;

.field public static final enum PAGES_HEADER_PERF_LOGGING_STOPPED:LX/8E7;

.field public static final enum PAGE_ADMIN_MEGAHPHONE_DATA_FETCHED:LX/8E7;

.field public static final enum PAGE_HEADER_DATA_FETCHED:LX/8E7;

.field public static final enum PMA_ABOUT_FRAGMENT_ALL_CARDS_LOADED:LX/8E7;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1313453
    new-instance v0, LX/8E7;

    const-string v1, "PAGES_HEADER_PERF_LOGGING_STOPPED"

    invoke-direct {v0, v1, v3}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PAGES_HEADER_PERF_LOGGING_STOPPED:LX/8E7;

    .line 1313454
    new-instance v0, LX/8E7;

    const-string v1, "PAGES_FIRST_CARD_LOADING_TIMER"

    invoke-direct {v0, v1, v4}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PAGES_FIRST_CARD_LOADING_TIMER:LX/8E7;

    .line 1313455
    new-instance v0, LX/8E7;

    const-string v1, "PAGES_DEFAULT_TAB_FRAGMENT_LOADED"

    invoke-direct {v0, v1, v5}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PAGES_DEFAULT_TAB_FRAGMENT_LOADED:LX/8E7;

    .line 1313456
    new-instance v0, LX/8E7;

    const-string v1, "PAGES_FIRST_CARD_DATA_LOADED"

    invoke-direct {v0, v1, v6}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PAGES_FIRST_CARD_DATA_LOADED:LX/8E7;

    .line 1313457
    new-instance v0, LX/8E7;

    const-string v1, "PAGES_FIRST_CARD_RENDERED"

    invoke-direct {v0, v1, v7}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PAGES_FIRST_CARD_RENDERED:LX/8E7;

    .line 1313458
    new-instance v0, LX/8E7;

    const-string v1, "PMA_ABOUT_FRAGMENT_ALL_CARDS_LOADED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PMA_ABOUT_FRAGMENT_ALL_CARDS_LOADED:LX/8E7;

    .line 1313459
    new-instance v0, LX/8E7;

    const-string v1, "PAGE_HEADER_DATA_FETCHED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PAGE_HEADER_DATA_FETCHED:LX/8E7;

    .line 1313460
    new-instance v0, LX/8E7;

    const-string v1, "PAGE_ADMIN_MEGAHPHONE_DATA_FETCHED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8E7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8E7;->PAGE_ADMIN_MEGAHPHONE_DATA_FETCHED:LX/8E7;

    .line 1313461
    const/16 v0, 0x8

    new-array v0, v0, [LX/8E7;

    sget-object v1, LX/8E7;->PAGES_HEADER_PERF_LOGGING_STOPPED:LX/8E7;

    aput-object v1, v0, v3

    sget-object v1, LX/8E7;->PAGES_FIRST_CARD_LOADING_TIMER:LX/8E7;

    aput-object v1, v0, v4

    sget-object v1, LX/8E7;->PAGES_DEFAULT_TAB_FRAGMENT_LOADED:LX/8E7;

    aput-object v1, v0, v5

    sget-object v1, LX/8E7;->PAGES_FIRST_CARD_DATA_LOADED:LX/8E7;

    aput-object v1, v0, v6

    sget-object v1, LX/8E7;->PAGES_FIRST_CARD_RENDERED:LX/8E7;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8E7;->PMA_ABOUT_FRAGMENT_ALL_CARDS_LOADED:LX/8E7;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8E7;->PAGE_HEADER_DATA_FETCHED:LX/8E7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8E7;->PAGE_ADMIN_MEGAHPHONE_DATA_FETCHED:LX/8E7;

    aput-object v2, v0, v1

    sput-object v0, LX/8E7;->$VALUES:[LX/8E7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1313462
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8E7;
    .locals 1

    .prologue
    .line 1313463
    const-class v0, LX/8E7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8E7;

    return-object v0
.end method

.method public static values()[LX/8E7;
    .locals 1

    .prologue
    .line 1313464
    sget-object v0, LX/8E7;->$VALUES:[LX/8E7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8E7;

    return-object v0
.end method
