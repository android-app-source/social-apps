.class public LX/8Qu;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/2c9;

.field public c:LX/8Sa;

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/privacy/model/AudiencePickerModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/8Qz;

.field public f:LX/8R4;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Qp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2c9;LX/8Sa;LX/0Or;LX/8Qz;LX/8R4;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/8Qz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/8R4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2c9;",
            "LX/8Sa;",
            "LX/0Or",
            "<",
            "Lcom/facebook/privacy/model/AudiencePickerModel;",
            ">;",
            "LX/8Qz;",
            "LX/8R4;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1343739
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1343740
    iput-object p1, p0, LX/8Qu;->a:Landroid/content/Context;

    .line 1343741
    iput-object p2, p0, LX/8Qu;->b:LX/2c9;

    .line 1343742
    iput-object p3, p0, LX/8Qu;->c:LX/8Sa;

    .line 1343743
    iput-object p4, p0, LX/8Qu;->d:LX/0Or;

    .line 1343744
    iput-object p5, p0, LX/8Qu;->e:LX/8Qz;

    .line 1343745
    iput-object p6, p0, LX/8Qu;->f:LX/8R4;

    .line 1343746
    invoke-direct {p0}, LX/8Qu;->b()V

    .line 1343747
    return-void
.end method

.method public static a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1343821
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1343822
    const v0, 0x7f0812e5

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1343823
    :goto_0
    return-object v0

    .line 1343824
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1343825
    const v1, 0x7f0812ed

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1343826
    :pswitch_0
    const v1, 0x7f0812eb

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1343827
    :pswitch_1
    const v1, 0x7f0812ec

    new-array v2, v5, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/8Qu;Lcom/facebook/graphql/model/GraphQLPrivacyOption;ZLjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "Z",
            "Ljava/util/List",
            "<",
            "LX/8Qp;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1343835
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v3

    .line 1343836
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1343837
    :cond_0
    return-void

    .line 1343838
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 1343839
    sget-object v5, LX/8Qo;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    .line 1343840
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1343841
    :pswitch_1
    new-instance v0, LX/8Qr;

    iget-object v5, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0812f9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0209f0

    invoke-direct {v0, v5, v6, v7, v2}, LX/8Qr;-><init>(Ljava/lang/String;IZZ)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1343842
    :pswitch_2
    new-instance v0, LX/8Qr;

    iget-object v5, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0812f8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0209f0

    invoke-direct {v0, v5, v6, p2, v7}, LX/8Qr;-><init>(Ljava/lang/String;IZZ)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<+",
            "LX/2cp;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1343828
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1343829
    const v0, 0x7f0812f0

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1343830
    :goto_0
    return-object v0

    .line 1343831
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1343832
    const v1, 0x7f0812f3

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1343833
    :pswitch_0
    const v1, 0x7f0812f1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1343834
    :pswitch_1
    const v1, 0x7f0812f2

    new-array v2, v5, [Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-interface {v0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b()V
    .locals 15

    .prologue
    .line 1343769
    iget-object v0, p0, LX/8Qu;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343770
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1343771
    if-nez v0, :cond_0

    move-object v1, v9

    .line 1343772
    :goto_0
    move-object v0, v1

    .line 1343773
    iput-object v0, p0, LX/8Qu;->g:Ljava/util/List;

    .line 1343774
    return-void

    .line 1343775
    :cond_0
    iget-object v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v10, v1

    .line 1343776
    iget-object v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->c:LX/0Px;

    move-object v11, v1

    .line 1343777
    iget-object v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    move-object v12, v1

    .line 1343778
    iget-boolean v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->k:Z

    move v13, v1

    .line 1343779
    const/4 v7, 0x0

    :goto_1
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v1

    if-ge v7, v1, :cond_8

    .line 1343780
    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343781
    iget v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    move v1, v1

    .line 1343782
    if-ne v7, v1, :cond_4

    const/4 v5, 0x1

    .line 1343783
    :goto_2
    iget v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->f:I

    move v1, v1

    .line 1343784
    if-ne v7, v1, :cond_5

    const/4 v1, 0x1

    .line 1343785
    :goto_3
    if-eqz v13, :cond_1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v5, :cond_1

    if-eqz v1, :cond_3

    .line 1343786
    :cond_1
    const/16 v6, 0x2711

    .line 1343787
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    .line 1343788
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v3

    .line 1343789
    iget-object v1, p0, LX/8Qu;->c:LX/8Sa;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    sget-object v14, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-virtual {v1, v4, v14}, LX/8Sa;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/8SZ;)I

    move-result v4

    .line 1343790
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1343791
    invoke-static {v8}, LX/2cA;->d(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1343792
    const/16 v6, 0x2713

    .line 1343793
    iget-object v1, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0812ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1343794
    iget-object v1, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1343795
    iget-object v3, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    move-object v3, v3

    .line 1343796
    invoke-static {v1, v3}, LX/8Qu;->b(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 1343797
    const v4, 0x7f02089a

    .line 1343798
    :cond_2
    :goto_4
    new-instance v1, LX/8Qq;

    invoke-direct/range {v1 .. v7}, LX/8Qq;-><init>(Ljava/lang/String;Ljava/lang/String;IZII)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1343799
    iget-boolean v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->g:Z

    move v1, v1

    .line 1343800
    if-eqz v1, :cond_3

    if-eqz v5, :cond_3

    .line 1343801
    iget-boolean v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    move v1, v1

    .line 1343802
    if-nez v1, :cond_7

    const/4 v1, 0x1

    :goto_5
    invoke-static {p0, v8, v1, v9}, LX/8Qu;->a(LX/8Qu;Lcom/facebook/graphql/model/GraphQLPrivacyOption;ZLjava/util/List;)V

    .line 1343803
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 1343804
    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    .line 1343805
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 1343806
    :cond_6
    invoke-static {v8}, LX/2cA;->b(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1343807
    const/16 v6, 0x2712

    .line 1343808
    iget-object v1, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0812e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1343809
    iget-object v1, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1343810
    iget-object v3, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    move-object v3, v3

    .line 1343811
    invoke-static {v1, v3}, LX/8Qu;->a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 1343812
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT_ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v14, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-static {v4, v14}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v4

    goto :goto_4

    .line 1343813
    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    .line 1343814
    :cond_8
    if-eqz v13, :cond_a

    .line 1343815
    new-instance v1, LX/8Qt;

    iget-object v2, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0812d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/8Qs;->MORE_OPTIONS:LX/8Qs;

    invoke-direct {v1, v2, v3}, LX/8Qt;-><init>(Ljava/lang/String;LX/8Qs;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    :goto_6
    move-object v1, v9

    .line 1343816
    goto/16 :goto_0

    .line 1343817
    :cond_a
    iget-object v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    move-object v1, v1

    .line 1343818
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1343819
    new-instance v1, LX/8Qt;

    iget-object v2, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0812da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/8Qs;->SEE_ALL_LISTS:LX/8Qs;

    invoke-direct {v1, v2, v3}, LX/8Qt;-><init>(Ljava/lang/String;LX/8Qs;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 1343820
    iget-object v0, p0, LX/8Qu;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1343768
    iget-object v0, p0, LX/8Qu;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1343767
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1343766
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1343751
    if-nez p2, :cond_2

    .line 1343752
    new-instance v0, LX/8SS;

    iget-object v1, p0, LX/8Qu;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/8SS;-><init>(Landroid/content/Context;)V

    .line 1343753
    :goto_0
    iget-object v1, p0, LX/8Qu;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Qp;

    .line 1343754
    instance-of v3, v1, LX/8Qq;

    if-eqz v3, :cond_4

    move-object v7, v1

    .line 1343755
    check-cast v7, LX/8Qq;

    .line 1343756
    iget-object v1, v7, LX/8Qq;->a:Ljava/lang/String;

    iget-object v2, v7, LX/8Qq;->b:Ljava/lang/String;

    iget v3, v7, LX/8Qq;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-boolean v4, v7, LX/8Qq;->d:Z

    if-eqz v4, :cond_3

    sget-object v4, LX/03R;->YES:LX/03R;

    :goto_1
    iget v7, v7, LX/8Qq;->e:I

    const/16 v8, 0x2711

    if-eq v7, v8, :cond_0

    move v6, v5

    :cond_0
    invoke-virtual/range {v0 .. v6}, LX/8SS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/03R;ZZ)V

    .line 1343757
    :cond_1
    :goto_2
    return-object v0

    .line 1343758
    :cond_2
    check-cast p2, LX/8SS;

    move-object v0, p2

    goto :goto_0

    .line 1343759
    :cond_3
    sget-object v4, LX/03R;->NO:LX/03R;

    goto :goto_1

    .line 1343760
    :cond_4
    instance-of v3, v1, LX/8Qr;

    if-eqz v3, :cond_6

    move-object v5, v1

    .line 1343761
    check-cast v5, LX/8Qr;

    .line 1343762
    iget-object v1, v5, LX/8Qr;->a:Ljava/lang/String;

    iget v3, v5, LX/8Qr;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-boolean v4, v5, LX/8Qr;->c:Z

    if-eqz v4, :cond_5

    sget-object v4, LX/03R;->YES:LX/03R;

    :goto_3
    iget-boolean v5, v5, LX/8Qr;->d:Z

    invoke-virtual/range {v0 .. v6}, LX/8SS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/03R;ZZ)V

    goto :goto_2

    :cond_5
    sget-object v4, LX/03R;->NO:LX/03R;

    goto :goto_3

    .line 1343763
    :cond_6
    instance-of v3, v1, LX/8Qt;

    if-eqz v3, :cond_1

    .line 1343764
    check-cast v1, LX/8Qt;

    .line 1343765
    iget-object v1, v1, LX/8Qt;->a:Ljava/lang/String;

    sget-object v4, LX/03R;->UNSET:LX/03R;

    move-object v3, v2

    invoke-virtual/range {v0 .. v6}, LX/8SS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/03R;ZZ)V

    goto :goto_2
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 1343748
    invoke-direct {p0}, LX/8Qu;->b()V

    .line 1343749
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 1343750
    return-void
.end method
