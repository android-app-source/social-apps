.class public final enum LX/7mU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7mU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7mU;

.field public static final enum FETCHED:LX/7mU;

.field public static final enum FETCHING:LX/7mU;

.field public static final enum INIT:LX/7mU;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1237061
    new-instance v0, LX/7mU;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, LX/7mU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mU;->INIT:LX/7mU;

    new-instance v0, LX/7mU;

    const-string v1, "FETCHING"

    invoke-direct {v0, v1, v3}, LX/7mU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mU;->FETCHING:LX/7mU;

    new-instance v0, LX/7mU;

    const-string v1, "FETCHED"

    invoke-direct {v0, v1, v4}, LX/7mU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mU;->FETCHED:LX/7mU;

    .line 1237062
    const/4 v0, 0x3

    new-array v0, v0, [LX/7mU;

    sget-object v1, LX/7mU;->INIT:LX/7mU;

    aput-object v1, v0, v2

    sget-object v1, LX/7mU;->FETCHING:LX/7mU;

    aput-object v1, v0, v3

    sget-object v1, LX/7mU;->FETCHED:LX/7mU;

    aput-object v1, v0, v4

    sput-object v0, LX/7mU;->$VALUES:[LX/7mU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1237063
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7mU;
    .locals 1

    .prologue
    .line 1237064
    const-class v0, LX/7mU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7mU;

    return-object v0
.end method

.method public static values()[LX/7mU;
    .locals 1

    .prologue
    .line 1237065
    sget-object v0, LX/7mU;->$VALUES:[LX/7mU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7mU;

    return-object v0
.end method
