.class public LX/6yv;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;",
        "Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/common/PaymentNetworkOperationHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160569
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1160570
    iput-object p2, p0, LX/6yv;->c:LX/0Or;

    .line 1160571
    return-void
.end method

.method public static b(LX/0QB;)LX/6yv;
    .locals 3

    .prologue
    .line 1160572
    new-instance v1, LX/6yv;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    const/16 v2, 0x12cb

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/6yv;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/0Or;)V

    .line 1160573
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1160574
    check-cast p1, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;

    .line 1160575
    iget-object v0, p0, LX/6yv;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1160576
    const-string v1, "/%s/creditcards"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p0, LX/6yv;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1160577
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1160578
    aput-object v0, v2, v3

    invoke-static {v1, v2}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v0

    const-string v1, "add_credit_card"

    .line 1160579
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1160580
    move-object v0, v0

    .line 1160581
    const-string v1, "POST"

    .line 1160582
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1160583
    move-object v0, v0

    .line 1160584
    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->a()Ljava/util/List;

    move-result-object v1

    .line 1160585
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1160586
    move-object v0, v0

    .line 1160587
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1160588
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1160589
    move-object v0, v0

    .line 1160590
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160591
    const-string v0, "add_credit_card"

    return-object v0
.end method
