.class public LX/8A2;
.super LX/151;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8A2;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1305567
    invoke-direct {p0, p1, p2, p3}, LX/151;-><init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V

    .line 1305568
    return-void
.end method

.method public static a(LX/0QB;)LX/8A2;
    .locals 6

    .prologue
    .line 1305569
    sget-object v0, LX/8A2;->b:LX/8A2;

    if-nez v0, :cond_1

    .line 1305570
    const-class v1, LX/8A2;

    monitor-enter v1

    .line 1305571
    :try_start_0
    sget-object v0, LX/8A2;->b:LX/8A2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1305572
    if-eqz v2, :cond_0

    .line 1305573
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1305574
    new-instance p0, LX/8A2;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/8A2;-><init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V

    .line 1305575
    move-object v0, p0

    .line 1305576
    sput-object v0, LX/8A2;->b:LX/8A2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1305577
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1305578
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1305579
    :cond_1
    sget-object v0, LX/8A2;->b:LX/8A2;

    return-object v0

    .line 1305580
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1305581
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1305582
    const-string v0, "content://com.facebook.pages.app.provider.PagesManagerLoggedInUserProvider/logged_in_user"

    return-object v0
.end method
