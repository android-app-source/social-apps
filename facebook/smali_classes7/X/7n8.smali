.class public final LX/7n8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1238585
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1238586
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1238587
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1238588
    invoke-static {p0, p1}, LX/7n8;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1238589
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1238590
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1238591
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1238592
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1238593
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/7n8;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1238594
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1238595
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1238596
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1238597
    const-wide/16 v12, 0x0

    .line 1238598
    const/4 v10, 0x0

    .line 1238599
    const-wide/16 v8, 0x0

    .line 1238600
    const/4 v7, 0x0

    .line 1238601
    const/4 v6, 0x0

    .line 1238602
    const/4 v5, 0x0

    .line 1238603
    const/4 v4, 0x0

    .line 1238604
    const/4 v3, 0x0

    .line 1238605
    const/4 v2, 0x0

    .line 1238606
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v14, :cond_b

    .line 1238607
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1238608
    const/4 v2, 0x0

    .line 1238609
    :goto_0
    return v2

    .line 1238610
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 1238611
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1238612
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1238613
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1238614
    const-string v6, "end_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1238615
    const/4 v2, 0x1

    .line 1238616
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1238617
    :cond_1
    const-string v6, "item_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1238618
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 1238619
    :cond_2
    const-string v6, "start_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1238620
    const/4 v2, 0x1

    .line 1238621
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v14, v6

    goto :goto_1

    .line 1238622
    :cond_3
    const-string v6, "sub_message_profiles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1238623
    invoke-static/range {p0 .. p1}, LX/7n7;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 1238624
    :cond_4
    const-string v6, "target_ent"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1238625
    invoke-static/range {p0 .. p1}, LX/7r7;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto :goto_1

    .line 1238626
    :cond_5
    const-string v6, "target_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1238627
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto :goto_1

    .line 1238628
    :cond_6
    const-string v6, "timezone"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1238629
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1238630
    :cond_7
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1238631
    :cond_8
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1238632
    if-eqz v3, :cond_9

    .line 1238633
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1238634
    :cond_9
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1238635
    if-eqz v8, :cond_a

    .line 1238636
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1238637
    :cond_a
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1238638
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1238639
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1238640
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1238641
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v11, v6

    move-wide v14, v8

    move v8, v2

    move v9, v4

    move/from16 v16, v5

    move-wide v4, v12

    move v12, v7

    move v13, v10

    move/from16 v10, v16

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1238642
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1238643
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1238644
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1238645
    const-string v2, "end_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1238646
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1238647
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1238648
    if-eqz v0, :cond_1

    .line 1238649
    const-string v1, "item_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1238650
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1238651
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1238652
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 1238653
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1238654
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1238655
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1238656
    if-eqz v0, :cond_4

    .line 1238657
    const-string v1, "sub_message_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1238658
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1238659
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1238660
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/7n7;->a(LX/15i;ILX/0nX;)V

    .line 1238661
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1238662
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1238663
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1238664
    if-eqz v0, :cond_5

    .line 1238665
    const-string v1, "target_ent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1238666
    invoke-static {p0, v0, p2, p3}, LX/7r7;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1238667
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1238668
    if-eqz v0, :cond_6

    .line 1238669
    const-string v1, "target_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1238670
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1238671
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1238672
    if-eqz v0, :cond_7

    .line 1238673
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1238674
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1238675
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1238676
    return-void
.end method
