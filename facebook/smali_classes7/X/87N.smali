.class public LX/87N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1300025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0il;)LX/86o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "Services::",
            "LX/0il",
            "<TModelData;>;>(TServices;)",
            "LX/86o;"
        }
    .end annotation

    .prologue
    .line 1300024
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;LX/86o;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 2

    .prologue
    .line 1300002
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    .line 1300003
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 1300004
    :goto_0
    return-object p0

    .line 1300005
    :cond_0
    invoke-virtual {p1}, LX/86o;->shouldCloseOnBackPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1300006
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getInspirationBackStack()LX/0Px;

    move-result-object v0

    sget-object v1, LX/86t;->TRAY:LX/86t;

    invoke-static {v0, v1}, LX/87R;->b(LX/0Px;LX/86t;)LX/0Px;

    move-result-object v0

    .line 1300007
    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setInspirationBackStack(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    .line 1300008
    :cond_1
    sget-object v0, LX/86o;->NONE:LX/86o;

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setOpenBottomTray(LX/86o;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsBottomTrayTransitioning(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/0il;LX/0jK;LX/86o;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;Services::",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "LX/0jK;",
            "LX/86o;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1300016
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    .line 1300017
    invoke-virtual {p2}, LX/86o;->shouldCloseOnBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300018
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getInspirationBackStack()LX/0Px;

    move-result-object v0

    sget-object v2, LX/86t;->TRAY:LX/86t;

    invoke-static {v0, v2}, LX/87R;->a(LX/0Px;LX/86t;)LX/0Px;

    move-result-object v0

    .line 1300019
    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setInspirationBackStack(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    .line 1300020
    :cond_0
    invoke-virtual {v1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setOpenBottomTray(LX/86o;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    .line 1300021
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsBottomTrayTransitioning(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    .line 1300022
    check-cast p0, LX/0im;

    invoke-interface {p0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1300023
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Z
    .locals 2

    .prologue
    .line 1300015
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z
    .locals 1

    .prologue
    .line 1300014
    invoke-static {p0, p1}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0il;LX/0jK;LX/86o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;Services::",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "LX/0jK;",
            "LX/86o;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1300010
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    .line 1300011
    invoke-static {v0, p2}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;LX/86o;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    .line 1300012
    check-cast p0, LX/0im;

    invoke-interface {p0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1300013
    return-void
.end method

.method public static b(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z
    .locals 1

    .prologue
    .line 1300009
    invoke-static {p0, p1}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
