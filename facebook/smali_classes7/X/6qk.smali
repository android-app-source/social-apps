.class public final LX/6qk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/CheckoutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 0

    .prologue
    .line 1151074
    iput-object p1, p0, LX/6qk;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 1151067
    iget-object v0, p0, LX/6qk;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->w()I

    move-result v0

    iget-object v1, p0, LX/6qk;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    .line 1151068
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 1151069
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1151070
    iget-object v0, p0, LX/6qk;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qk;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    iget-object v2, p0, LX/6qk;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    .line 1151071
    iget-object p0, v2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, p0

    .line 1151072
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;I)V

    .line 1151073
    :cond_0
    return-void
.end method
