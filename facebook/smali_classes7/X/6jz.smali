.class public LX/6jz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final action:Ljava/lang/Integer;

.field public final messageId:Ljava/lang/String;

.field public final offlineThreadingId:Ljava/lang/String;

.field public final reaction:Ljava/lang/String;

.field public final senderId:Ljava/lang/Long;

.field public final threadKey:LX/6l9;

.field public final userId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x1

    const/16 v4, 0xb

    .line 1134076
    new-instance v0, LX/1sv;

    const-string v1, "DeltaMessageReaction"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jz;->b:LX/1sv;

    .line 1134077
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jz;->c:LX/1sw;

    .line 1134078
    new-instance v0, LX/1sw;

    const-string v1, "messageId"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jz;->d:LX/1sw;

    .line 1134079
    new-instance v0, LX/1sw;

    const-string v1, "action"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jz;->e:LX/1sw;

    .line 1134080
    new-instance v0, LX/1sw;

    const-string v1, "userId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jz;->f:LX/1sw;

    .line 1134081
    new-instance v0, LX/1sw;

    const-string v1, "reaction"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jz;->g:LX/1sw;

    .line 1134082
    new-instance v0, LX/1sw;

    const-string v1, "senderId"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jz;->h:LX/1sw;

    .line 1134083
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jz;->i:LX/1sw;

    .line 1134084
    sput-boolean v5, LX/6jz;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6l9;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1134067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134068
    iput-object p1, p0, LX/6jz;->threadKey:LX/6l9;

    .line 1134069
    iput-object p2, p0, LX/6jz;->messageId:Ljava/lang/String;

    .line 1134070
    iput-object p3, p0, LX/6jz;->action:Ljava/lang/Integer;

    .line 1134071
    iput-object p4, p0, LX/6jz;->userId:Ljava/lang/Long;

    .line 1134072
    iput-object p5, p0, LX/6jz;->reaction:Ljava/lang/String;

    .line 1134073
    iput-object p6, p0, LX/6jz;->senderId:Ljava/lang/Long;

    .line 1134074
    iput-object p7, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    .line 1134075
    return-void
.end method

.method private static a(LX/6jz;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1134054
    iget-object v0, p0, LX/6jz;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1134055
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134056
    :cond_0
    iget-object v0, p0, LX/6jz;->messageId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1134057
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134058
    :cond_1
    iget-object v0, p0, LX/6jz;->action:Ljava/lang/Integer;

    if-nez v0, :cond_2

    .line 1134059
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'action\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134060
    :cond_2
    iget-object v0, p0, LX/6jz;->userId:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 1134061
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'userId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134062
    :cond_3
    iget-object v0, p0, LX/6jz;->senderId:Ljava/lang/Long;

    if-nez v0, :cond_4

    .line 1134063
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'senderId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134064
    :cond_4
    iget-object v0, p0, LX/6jz;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    sget-object v0, LX/6kp;->a:LX/1sn;

    iget-object v1, p0, LX/6jz;->action:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1134065
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'action\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6jz;->action:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1134066
    :cond_5
    return-void
.end method

.method public static b(LX/1su;)LX/6jz;
    .locals 12

    .prologue
    const/16 v11, 0xa

    const/16 v10, 0xb

    const/4 v7, 0x0

    .line 1134024
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 1134025
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1134026
    iget-byte v8, v0, LX/1sw;->b:B

    if-eqz v8, :cond_7

    .line 1134027
    iget-short v8, v0, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 1134028
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134029
    :pswitch_0
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xc

    if-ne v8, v9, :cond_0

    .line 1134030
    invoke-static {p0}, LX/6l9;->b(LX/1su;)LX/6l9;

    move-result-object v1

    goto :goto_0

    .line 1134031
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134032
    :pswitch_1
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_1

    .line 1134033
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1134034
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134035
    :pswitch_2
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0x8

    if-ne v8, v9, :cond_2

    .line 1134036
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 1134037
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134038
    :pswitch_3
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_3

    .line 1134039
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1134040
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134041
    :pswitch_4
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_4

    .line 1134042
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 1134043
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134044
    :pswitch_5
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_5

    .line 1134045
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto :goto_0

    .line 1134046
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134047
    :pswitch_6
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_6

    .line 1134048
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1134049
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1134050
    :cond_7
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1134051
    new-instance v0, LX/6jz;

    invoke-direct/range {v0 .. v7}, LX/6jz;-><init>(LX/6l9;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    .line 1134052
    invoke-static {v0}, LX/6jz;->a(LX/6jz;)V

    .line 1134053
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1134085
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1134086
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1134087
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    move-object v1, v0

    .line 1134088
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaMessageReaction"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1134089
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134090
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134091
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134092
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134093
    const-string v0, "threadKey"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134094
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134095
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134096
    iget-object v0, p0, LX/6jz;->threadKey:LX/6l9;

    if-nez v0, :cond_6

    .line 1134097
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134098
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134099
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134100
    const-string v0, "messageId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134101
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134102
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134103
    iget-object v0, p0, LX/6jz;->messageId:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1134104
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134105
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134106
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134107
    const-string v0, "action"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134108
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134109
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134110
    iget-object v0, p0, LX/6jz;->action:Ljava/lang/Integer;

    if-nez v0, :cond_8

    .line 1134111
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134112
    :cond_0
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134113
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134114
    const-string v0, "userId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134115
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134116
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134117
    iget-object v0, p0, LX/6jz;->userId:Ljava/lang/Long;

    if-nez v0, :cond_a

    .line 1134118
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134119
    :goto_6
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1134120
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134121
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134122
    const-string v0, "reaction"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134123
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134124
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134125
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 1134126
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134127
    :cond_1
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134128
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134129
    const-string v0, "senderId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134130
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134131
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134132
    iget-object v0, p0, LX/6jz;->senderId:Ljava/lang/Long;

    if-nez v0, :cond_c

    .line 1134133
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134134
    :goto_8
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1134135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134136
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134137
    const-string v0, "offlineThreadingId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134138
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134139
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134140
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    if-nez v0, :cond_d

    .line 1134141
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134142
    :cond_2
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134143
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134144
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1134145
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1134146
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1134147
    :cond_5
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1134148
    :cond_6
    iget-object v0, p0, LX/6jz;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1134149
    :cond_7
    iget-object v0, p0, LX/6jz;->messageId:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1134150
    :cond_8
    sget-object v0, LX/6kp;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6jz;->action:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1134151
    if-eqz v0, :cond_9

    .line 1134152
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134153
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134154
    :cond_9
    iget-object v5, p0, LX/6jz;->action:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1134155
    if-eqz v0, :cond_0

    .line 1134156
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1134157
    :cond_a
    iget-object v0, p0, LX/6jz;->userId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1134158
    :cond_b
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1134159
    :cond_c
    iget-object v0, p0, LX/6jz;->senderId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1134160
    :cond_d
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1133996
    invoke-static {p0}, LX/6jz;->a(LX/6jz;)V

    .line 1133997
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133998
    iget-object v0, p0, LX/6jz;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1133999
    sget-object v0, LX/6jz;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134000
    iget-object v0, p0, LX/6jz;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1134001
    :cond_0
    iget-object v0, p0, LX/6jz;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1134002
    sget-object v0, LX/6jz;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134003
    iget-object v0, p0, LX/6jz;->messageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1134004
    :cond_1
    iget-object v0, p0, LX/6jz;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1134005
    sget-object v0, LX/6jz;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134006
    iget-object v0, p0, LX/6jz;->action:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1134007
    :cond_2
    iget-object v0, p0, LX/6jz;->userId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1134008
    sget-object v0, LX/6jz;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134009
    iget-object v0, p0, LX/6jz;->userId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1134010
    :cond_3
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1134011
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1134012
    sget-object v0, LX/6jz;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134013
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1134014
    :cond_4
    iget-object v0, p0, LX/6jz;->senderId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1134015
    sget-object v0, LX/6jz;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134016
    iget-object v0, p0, LX/6jz;->senderId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1134017
    :cond_5
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1134018
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1134019
    sget-object v0, LX/6jz;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134020
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1134021
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1134022
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1134023
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133939
    if-nez p1, :cond_1

    .line 1133940
    :cond_0
    :goto_0
    return v0

    .line 1133941
    :cond_1
    instance-of v1, p1, LX/6jz;

    if-eqz v1, :cond_0

    .line 1133942
    check-cast p1, LX/6jz;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133943
    if-nez p1, :cond_3

    .line 1133944
    :cond_2
    :goto_1
    move v0, v2

    .line 1133945
    goto :goto_0

    .line 1133946
    :cond_3
    iget-object v0, p0, LX/6jz;->threadKey:LX/6l9;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1133947
    :goto_2
    iget-object v3, p1, LX/6jz;->threadKey:LX/6l9;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1133948
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133949
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133950
    iget-object v0, p0, LX/6jz;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6jz;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133951
    :cond_5
    iget-object v0, p0, LX/6jz;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1133952
    :goto_4
    iget-object v3, p1, LX/6jz;->messageId:Ljava/lang/String;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1133953
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133954
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133955
    iget-object v0, p0, LX/6jz;->messageId:Ljava/lang/String;

    iget-object v3, p1, LX/6jz;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133956
    :cond_7
    iget-object v0, p0, LX/6jz;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1133957
    :goto_6
    iget-object v3, p1, LX/6jz;->action:Ljava/lang/Integer;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1133958
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1133959
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133960
    iget-object v0, p0, LX/6jz;->action:Ljava/lang/Integer;

    iget-object v3, p1, LX/6jz;->action:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133961
    :cond_9
    iget-object v0, p0, LX/6jz;->userId:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1133962
    :goto_8
    iget-object v3, p1, LX/6jz;->userId:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1133963
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1133964
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133965
    iget-object v0, p0, LX/6jz;->userId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jz;->userId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133966
    :cond_b
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1133967
    :goto_a
    iget-object v3, p1, LX/6jz;->reaction:Ljava/lang/String;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1133968
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1133969
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133970
    iget-object v0, p0, LX/6jz;->reaction:Ljava/lang/String;

    iget-object v3, p1, LX/6jz;->reaction:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133971
    :cond_d
    iget-object v0, p0, LX/6jz;->senderId:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1133972
    :goto_c
    iget-object v3, p1, LX/6jz;->senderId:Ljava/lang/Long;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1133973
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1133974
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133975
    iget-object v0, p0, LX/6jz;->senderId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jz;->senderId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133976
    :cond_f
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1133977
    :goto_e
    iget-object v3, p1, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1133978
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1133979
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133980
    iget-object v0, p0, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    iget-object v3, p1, LX/6jz;->offlineThreadingId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 1133981
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1133982
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 1133983
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1133984
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 1133985
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1133986
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 1133987
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1133988
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 1133989
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 1133990
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 1133991
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 1133992
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 1133993
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 1133994
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 1133995
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133938
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133935
    sget-boolean v0, LX/6jz;->a:Z

    .line 1133936
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jz;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133937
    return-object v0
.end method
