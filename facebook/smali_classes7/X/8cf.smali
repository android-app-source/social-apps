.class public final enum LX/8cf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8cf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8cf;

.field public static final enum HEAD:LX/8cf;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1374730
    new-instance v0, LX/8cf;

    const-string v1, "HEAD"

    const-string v2, "head"

    invoke-direct {v0, v1, v3, v2}, LX/8cf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8cf;->HEAD:LX/8cf;

    .line 1374731
    const/4 v0, 0x1

    new-array v0, v0, [LX/8cf;

    sget-object v1, LX/8cf;->HEAD:LX/8cf;

    aput-object v1, v0, v3

    sput-object v0, LX/8cf;->$VALUES:[LX/8cf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1374732
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1374733
    iput-object p3, p0, LX/8cf;->name:Ljava/lang/String;

    .line 1374734
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8cf;
    .locals 1

    .prologue
    .line 1374735
    const-class v0, LX/8cf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8cf;

    return-object v0
.end method

.method public static values()[LX/8cf;
    .locals 1

    .prologue
    .line 1374736
    sget-object v0, LX/8cf;->$VALUES:[LX/8cf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8cf;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1374737
    iget-object v0, p0, LX/8cf;->name:Ljava/lang/String;

    return-object v0
.end method
