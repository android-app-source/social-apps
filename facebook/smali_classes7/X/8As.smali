.class public final LX/8As;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1308050
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1308051
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1308052
    if-eqz v0, :cond_3

    .line 1308053
    const-string v1, "available_menu_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308054
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1308055
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1308056
    if-eqz v1, :cond_0

    .line 1308057
    const-string p3, "has_available_link_menus"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308058
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1308059
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1308060
    if-eqz v1, :cond_1

    .line 1308061
    const-string p3, "has_available_photo_menus"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308062
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1308063
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1308064
    if-eqz v1, :cond_2

    .line 1308065
    const-string p3, "has_available_structured_menu"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308066
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1308067
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1308068
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1308069
    if-eqz v0, :cond_4

    .line 1308070
    const-string v1, "has_link_menus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308071
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1308072
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1308073
    if-eqz v0, :cond_5

    .line 1308074
    const-string v1, "has_photo_menus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308075
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1308076
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1308077
    if-eqz v0, :cond_6

    .line 1308078
    const-string v1, "has_structured_menu"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308079
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1308080
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1308081
    return-void
.end method
