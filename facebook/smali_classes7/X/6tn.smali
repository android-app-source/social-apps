.class public LX/6tn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/6xb;

.field private final c:LX/5fv;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/6xb;LX/5fv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155191
    iput-object p1, p0, LX/6tn;->a:Landroid/content/res/Resources;

    .line 1155192
    iput-object p2, p0, LX/6tn;->b:LX/6xb;

    .line 1155193
    iput-object p3, p0, LX/6tn;->c:LX/5fv;

    .line 1155194
    return-void
.end method

.method public static a(LX/6tn;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/0Px;Z)LX/6t8;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/logging/PaymentsLoggingSessionData;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;Z)",
            "LX/6t8;"
        }
    .end annotation

    .prologue
    .line 1155195
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1155196
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    .line 1155197
    invoke-direct {p0, v0}, LX/6tn;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;)LX/73b;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155198
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1155199
    :cond_0
    if-nez p3, :cond_1

    .line 1155200
    invoke-static {p2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1155201
    if-eqz v0, :cond_1

    .line 1155202
    new-instance v1, LX/73b;

    iget-object v3, p0, LX/6tn;->a:Landroid/content/res/Resources;

    const v4, 0x7f081d45

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/6tn;->c:LX/5fv;

    invoke-virtual {v4, v0}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v1, v3, v4, v5}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155203
    iget-object v1, p0, LX/6tn;->b:LX/6xb;

    .line 1155204
    iget-object v3, v0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v3, v3

    .line 1155205
    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1155206
    iget-object v4, v0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v0, v4

    .line 1155207
    const-string v4, "raw_amount"

    invoke-virtual {v1, p1, v4, v3}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1155208
    const-string v4, "currency"

    invoke-virtual {v1, p1, v4, v0}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1155209
    :cond_1
    new-instance v0, LX/6t8;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6t8;-><init>(LX/0Px;)V

    return-object v0
.end method

.method private a(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;)LX/73b;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1155210
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1155211
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 1155212
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1155213
    new-instance v0, LX/73b;

    iget-object v2, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    iget-object v3, p0, LX/6tn;->c:LX/5fv;

    invoke-virtual {v3, v1}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1, v4}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1155214
    :goto_0
    return-object v0

    .line 1155215
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1155216
    new-instance v0, LX/73b;

    iget-object v1, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v4}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1155217
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to generate rowData for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
