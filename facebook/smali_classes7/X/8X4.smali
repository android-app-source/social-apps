.class public LX/8X4;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/8VV;


# instance fields
.field public a:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/8TD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/8Sn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1354550
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8X4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1354551
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354552
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/8X4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354553
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354554
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354555
    const-class p1, LX/8X4;

    invoke-static {p1, p0}, LX/8X4;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1354556
    invoke-virtual {p0}, LX/8X4;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0310de

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1354557
    invoke-static {p0}, LX/8X4;->getCardViewFragment(LX/8X4;)Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    move-result-object p1

    new-instance p2, LX/8X3;

    invoke-direct {p2, p0}, LX/8X3;-><init>(LX/8X4;)V

    .line 1354558
    iput-object p2, p1, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->c:LX/8X2;

    .line 1354559
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8X4;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v1

    check-cast v1, LX/8TS;

    invoke-static {p0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object p0

    check-cast p0, LX/8TD;

    iput-object v1, p1, LX/8X4;->a:LX/8TS;

    iput-object p0, p1, LX/8X4;->b:LX/8TD;

    return-void
.end method

.method public static getCardViewFragment(LX/8X4;)Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;
    .locals 2

    .prologue
    .line 1354560
    invoke-virtual {p0}, LX/8X4;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    .line 1354561
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2809

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1354562
    invoke-static {p0}, LX/8X4;->getCardViewFragment(LX/8X4;)Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    move-result-object v0

    iget-object v1, p0, LX/8X4;->a:LX/8TS;

    .line 1354563
    iget v2, v1, LX/8TS;->n:I

    move v1, v2

    .line 1354564
    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->a(I)V

    .line 1354565
    invoke-static {p0}, LX/8X4;->getCardViewFragment(LX/8X4;)Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->b()V

    .line 1354566
    invoke-static {p0}, LX/8X4;->getCardViewFragment(LX/8X4;)Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->d()V

    .line 1354567
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;LX/8Vb;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "LX/8Vb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1354568
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1354569
    invoke-static {p0}, LX/8X4;->getCardViewFragment(LX/8X4;)Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->c()Z

    move-result v0

    return v0
.end method

.method public setCallbackDelegate(LX/8Sn;)V
    .locals 0

    .prologue
    .line 1354570
    iput-object p1, p0, LX/8X4;->c:LX/8Sn;

    .line 1354571
    return-void
.end method

.method public setScreenshot(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1354572
    invoke-static {p0}, LX/8X4;->getCardViewFragment(LX/8X4;)Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->a(Landroid/graphics/Bitmap;)V

    .line 1354573
    return-void
.end method
