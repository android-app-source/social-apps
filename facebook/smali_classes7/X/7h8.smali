.class public LX/7h8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1225533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225534
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1225525
    if-nez p0, :cond_0

    .line 1225526
    const/4 v0, 0x0

    .line 1225527
    :goto_0
    return-object v0

    .line 1225528
    :cond_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1225529
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1225530
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1225531
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1225532
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/audience/model/UploadShot;)LX/7h5;
    .locals 6

    .prologue
    .line 1225515
    new-instance v0, LX/7h5;

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getTimezoneOffset()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, LX/7h5;-><init>(JJ)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1225522
    const-string v0, "file://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1225523
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1225524
    :cond_0
    return-object p0
.end method

.method public static b(Lcom/facebook/audience/model/UploadShot;)LX/7h7;
    .locals 1

    .prologue
    .line 1225516
    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1225517
    sget-object v0, LX/7h7;->REPLY_FROM_THREAD:LX/7h7;

    .line 1225518
    :goto_0
    return-object v0

    .line 1225519
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getReactionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1225520
    sget-object v0, LX/7h7;->REPLY_FROM_STACK:LX/7h7;

    goto :goto_0

    .line 1225521
    :cond_1
    sget-object v0, LX/7h7;->STACK:LX/7h7;

    goto :goto_0
.end method
