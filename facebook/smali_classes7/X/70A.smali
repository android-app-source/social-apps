.class public LX/70A;
.super LX/6sU;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sU",
        "<",
        "Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final c:LX/70Y;

.field private final d:LX/70P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161883
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/70A;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/70Y;LX/70P;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1161884
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-direct {p0, p1, v0}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1161885
    iput-object p2, p0, LX/70A;->c:LX/70Y;

    .line 1161886
    iput-object p3, p0, LX/70A;->d:LX/70P;

    .line 1161887
    return-void
.end method

.method public static a(LX/0QB;)LX/70A;
    .locals 10

    .prologue
    .line 1161888
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1161889
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1161890
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1161891
    if-nez v1, :cond_0

    .line 1161892
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1161893
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1161894
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1161895
    sget-object v1, LX/70A;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1161896
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1161897
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1161898
    :cond_1
    if-nez v1, :cond_4

    .line 1161899
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1161900
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1161901
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1161902
    new-instance v9, LX/70A;

    invoke-static {v0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    .line 1161903
    new-instance v8, LX/70Y;

    invoke-static {v0}, LX/70Z;->a(LX/0QB;)LX/70Z;

    move-result-object v7

    check-cast v7, LX/70Z;

    invoke-direct {v8, v7}, LX/70Y;-><init>(LX/70Z;)V

    .line 1161904
    move-object v7, v8

    .line 1161905
    check-cast v7, LX/70Y;

    .line 1161906
    new-instance p0, LX/70P;

    invoke-static {v0}, LX/70Z;->a(LX/0QB;)LX/70Z;

    move-result-object v8

    check-cast v8, LX/70Z;

    invoke-direct {p0, v8}, LX/70P;-><init>(LX/70Z;)V

    .line 1161907
    move-object v8, p0

    .line 1161908
    check-cast v8, LX/70P;

    invoke-direct {v9, v1, v7, v8}, LX/70A;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/70Y;LX/70P;)V

    .line 1161909
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1161910
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1161911
    if-nez v1, :cond_2

    .line 1161912
    sget-object v0, LX/70A;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/70A;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1161913
    :goto_1
    if-eqz v0, :cond_3

    .line 1161914
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1161915
    :goto_3
    check-cast v0, LX/70A;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1161916
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1161917
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1161918
    :catchall_1
    move-exception v0

    .line 1161919
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1161920
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1161921
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1161922
    :cond_2
    :try_start_8
    sget-object v0, LX/70A;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/70A;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1161923
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    .line 1161924
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    move-object v0, v0

    .line 1161925
    invoke-static {v0}, LX/73n;->a(LX/6xg;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1161926
    invoke-virtual {p0}, LX/70A;->d()Ljava/lang/String;

    move-result-object v0

    .line 1161927
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    move-object v1, v1

    .line 1161928
    invoke-static {v1}, LX/73n;->a(LX/6xg;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1161929
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    move-object v1, v1

    .line 1161930
    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1161931
    invoke-static {v1, v2}, LX/73n;->a(LX/6xg;Ljava/lang/String;)Ljava/lang/String;

    .line 1161932
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "payment_options"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1161933
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1161934
    invoke-static {p1, v1, v2}, LX/70f;->a(Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 1161935
    const-string v3, "fields"

    invoke-static {v1, v2}, LX/70f;->a(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    .line 1161936
    const-string v2, "GET"

    invoke-static {v0, v2}, LX/70f;->a(Ljava/lang/String;Ljava/lang/String;)LX/14O;

    move-result-object v2

    const-string v3, "/act_%s"

    .line 1161937
    iget-object p0, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    move-object p0, p0

    .line 1161938
    invoke-static {v3, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1161939
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 1161940
    move-object v2, v2

    .line 1161941
    invoke-virtual {v2, v1}, LX/14O;->a(Ljava/util/Map;)LX/14O;

    move-result-object v1

    invoke-virtual {v1}, LX/14O;->C()LX/14N;

    move-result-object v1

    move-object v0, v1

    .line 1161942
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/70A;->d()Ljava/lang/String;

    move-result-object v0

    .line 1161943
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1161944
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1161945
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "payment_modules_options"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1161946
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1161947
    const-string v4, ".payment_type(%s)"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1161948
    iget-object v4, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    move-object v4, v4

    .line 1161949
    invoke-virtual {v4}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1161950
    invoke-static {p1, v2, v3}, LX/70f;->a(Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 1161951
    iget-object v4, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    move-object v4, v4

    .line 1161952
    if-eqz v4, :cond_1

    .line 1161953
    const-string v4, ".extra_data(%s)"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1161954
    iget-object v4, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    move-object v4, v4

    .line 1161955
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1161956
    :cond_1
    iget-object v4, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1161957
    if-eqz v4, :cond_2

    .line 1161958
    const-string v4, ".receiver_id(%s)"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1161959
    iget-object v4, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1161960
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1161961
    :cond_2
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string p0, "fields"

    invoke-static {v2, v3}, LX/70f;->a(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p0, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1161962
    const-string v2, "GET"

    invoke-static {v0, v2}, LX/70f;->a(Ljava/lang/String;Ljava/lang/String;)LX/14O;

    move-result-object v2

    const-string v3, "me"

    .line 1161963
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 1161964
    move-object v2, v2

    .line 1161965
    iput-object v1, v2, LX/14O;->g:Ljava/util/List;

    .line 1161966
    move-object v1, v2

    .line 1161967
    invoke-virtual {v1}, LX/14O;->C()LX/14N;

    move-result-object v1

    move-object v0, v1

    .line 1161968
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1161969
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    .line 1161970
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1161971
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    move-object v1, v1

    .line 1161972
    invoke-static {v1}, LX/73n;->a(LX/6xg;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "payment_options"

    :goto_0
    move-object v1, v1

    .line 1161973
    invoke-static {v0, v1}, LX/16N;->e(LX/0lF;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    .line 1161974
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    move-object v1, v1

    .line 1161975
    invoke-static {v1}, LX/73n;->a(LX/6xg;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/70A;->d:LX/70P;

    .line 1161976
    invoke-static {v0}, LX/70R;->a(LX/0lF;)LX/70Q;

    move-result-object v7

    .line 1161977
    invoke-static {v1, v0}, LX/70P;->b(LX/70P;LX/0lF;)LX/0Px;

    move-result-object v9

    .line 1161978
    invoke-static {v1, v0}, LX/70P;->c(LX/70P;LX/0lF;)LX/0Px;

    move-result-object v8

    .line 1161979
    new-instance v4, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    iget-object v5, v7, LX/70Q;->a:Lcom/facebook/common/locale/Country;

    iget-object v6, v7, LX/70Q;->b:Ljava/lang/String;

    iget-object v7, v7, LX/70Q;->c:Ljava/lang/String;

    invoke-direct/range {v4 .. v9}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;-><init>(Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)V

    move-object v0, v4

    .line 1161980
    move-object v1, v0

    .line 1161981
    :goto_1
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    move-object v0, v0

    .line 1161982
    invoke-static {v0}, LX/73n;->a(LX/6xg;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1161983
    iget-object v0, v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v0, v0

    .line 1161984
    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    move-object v2, v2

    .line 1161985
    invoke-static {v0, v2}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    .line 1161986
    iget-object v2, v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1161987
    iget-object v3, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1161988
    new-instance v4, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161989
    iget-object v5, v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v8, v5

    .line 1161990
    iget-object v5, v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v9, v5

    .line 1161991
    move-object v5, v0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;-><init>(Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)V

    move-object v1, v4

    .line 1161992
    :cond_0
    return-object v1

    .line 1161993
    :cond_1
    iget-object v1, p0, LX/70A;->c:LX/70Y;

    .line 1161994
    invoke-static {v0}, LX/70R;->a(LX/0lF;)LX/70Q;

    move-result-object v7

    .line 1161995
    invoke-static {v1, v0}, LX/70Y;->b(LX/70Y;LX/0lF;)LX/0Px;

    move-result-object v9

    .line 1161996
    invoke-static {v1, v0}, LX/70Y;->c(LX/70Y;LX/0lF;)LX/0Px;

    move-result-object v8

    .line 1161997
    new-instance v4, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    iget-object v5, v7, LX/70Q;->a:Lcom/facebook/common/locale/Country;

    iget-object v6, v7, LX/70Q;->b:Ljava/lang/String;

    iget-object v7, v7, LX/70Q;->c:Ljava/lang/String;

    invoke-direct/range {v4 .. v9}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;-><init>(Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)V

    move-object v0, v4

    .line 1161998
    move-object v1, v0

    goto :goto_1

    :cond_2
    const-string v1, "payment_modules_options"

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161999
    const-string v0, "get_payment_methods_Info"

    return-object v0
.end method
