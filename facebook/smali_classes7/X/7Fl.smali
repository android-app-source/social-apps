.class public final LX/7Fl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1188113
    const/4 v12, 0x0

    .line 1188114
    const/4 v11, 0x0

    .line 1188115
    const/4 v10, 0x0

    .line 1188116
    const/4 v9, 0x0

    .line 1188117
    const/4 v8, 0x0

    .line 1188118
    const/4 v7, 0x0

    .line 1188119
    const/4 v6, 0x0

    .line 1188120
    const/4 v5, 0x0

    .line 1188121
    const/4 v4, 0x0

    .line 1188122
    const/4 v3, 0x0

    .line 1188123
    const/4 v2, 0x0

    .line 1188124
    const/4 v1, 0x0

    .line 1188125
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1188126
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1188127
    const/4 v1, 0x0

    .line 1188128
    :goto_0
    return v1

    .line 1188129
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1188130
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_9

    .line 1188131
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1188132
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1188133
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1188134
    const-string v14, "branch_default_page_index"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1188135
    const/4 v4, 0x1

    .line 1188136
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 1188137
    :cond_2
    const-string v14, "branch_question_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1188138
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1188139
    :cond_3
    const-string v14, "branch_response_maps"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1188140
    invoke-static/range {p0 .. p1}, LX/7Ff;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1188141
    :cond_4
    const-string v14, "branch_subquestion_index_int"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1188142
    const/4 v3, 0x1

    .line 1188143
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 1188144
    :cond_5
    const-string v14, "direct_next_page_index_int"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1188145
    const/4 v2, 0x1

    .line 1188146
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 1188147
    :cond_6
    const-string v14, "node_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1188148
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1188149
    :cond_7
    const-string v14, "qe_next_page_index"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1188150
    const/4 v1, 0x1

    .line 1188151
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_1

    .line 1188152
    :cond_8
    const-string v14, "random_next_page_indices"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1188153
    invoke-static/range {p0 .. p1}, LX/2gu;->b(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1188154
    :cond_9
    const/16 v13, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1188155
    if-eqz v4, :cond_a

    .line 1188156
    const/4 v4, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v13}, LX/186;->a(III)V

    .line 1188157
    :cond_a
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1188158
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1188159
    if-eqz v3, :cond_b

    .line 1188160
    const/4 v3, 0x3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1188161
    :cond_b
    if-eqz v2, :cond_c

    .line 1188162
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v3}, LX/186;->a(III)V

    .line 1188163
    :cond_c
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1188164
    if-eqz v1, :cond_d

    .line 1188165
    const/4 v1, 0x6

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v2}, LX/186;->a(III)V

    .line 1188166
    :cond_d
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1188167
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1188203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1188204
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1188205
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1188206
    invoke-static {p0, p1}, LX/7Fl;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1188207
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1188208
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 1188168
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1188169
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1188170
    if-eqz v0, :cond_0

    .line 1188171
    const-string v1, "branch_default_page_index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188172
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1188173
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1188174
    if-eqz v0, :cond_1

    .line 1188175
    const-string v1, "branch_question_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188176
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1188177
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1188178
    if-eqz v0, :cond_2

    .line 1188179
    const-string v1, "branch_response_maps"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188180
    invoke-static {p0, v0, p2, p3}, LX/7Ff;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1188181
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1188182
    if-eqz v0, :cond_3

    .line 1188183
    const-string v1, "branch_subquestion_index_int"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188184
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1188185
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1188186
    if-eqz v0, :cond_4

    .line 1188187
    const-string v1, "direct_next_page_index_int"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188188
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1188189
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1188190
    if-eqz v0, :cond_5

    .line 1188191
    const-string v1, "node_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188192
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1188193
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1188194
    if-eqz v0, :cond_6

    .line 1188195
    const-string v1, "qe_next_page_index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188196
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1188197
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1188198
    if-eqz v0, :cond_7

    .line 1188199
    const-string v0, "random_next_page_indices"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188200
    invoke-virtual {p0, p1, v3}, LX/15i;->e(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->c(Ljava/util/Iterator;LX/0nX;)V

    .line 1188201
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1188202
    return-void
.end method
