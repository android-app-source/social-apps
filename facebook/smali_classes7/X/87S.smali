.class public LX/87S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300113
    return-void
.end method

.method public static a(LX/0is;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ">(TModelData;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1300114
    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1300115
    :cond_0
    const/4 v0, 0x0

    .line 1300116
    :goto_0
    return-object v0

    .line 1300117
    :cond_1
    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1300118
    const/4 v0, 0x0

    .line 1300119
    :goto_1
    move-object v0, v0

    .line 1300120
    invoke-static {p0}, LX/87Q;->a(LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v1

    .line 1300121
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1300122
    invoke-static {v0}, LX/87V;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1300123
    invoke-static {v0}, LX/87S;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300124
    :cond_2
    invoke-static {v1}, LX/87S;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300125
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/87Q;->a(LX/0is;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;
    .locals 2

    .prologue
    .line 1300126
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->setPrompt_id(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getTrackingString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->setPrompt_tracking_string(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getPromptType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->setPrompt_type(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    move-result-object v0

    return-object v0
.end method
