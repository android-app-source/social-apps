.class public final LX/7m6;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1236250
    iput-object p1, p0, LX/7m6;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iput-object p2, p0, LX/7m6;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1236251
    iget-object v0, p0, LX/7m6;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v0, v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    iget-object v1, p0, LX/7m6;->a:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1236252
    iget-object v0, p0, LX/7m6;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v0, v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->f:LX/03V;

    const-string v1, "pending_story_delete_failed"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failed to delete pending story, sessionId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/7m6;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1236253
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 3

    .prologue
    .line 1236254
    iget-object v0, p0, LX/7m6;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v0, v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    iget-object v1, p0, LX/7m6;->a:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1236255
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1236256
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/7m6;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
