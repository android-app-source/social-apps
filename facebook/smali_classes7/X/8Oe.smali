.class public final LX/8Oe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/photos/base/media/VideoItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/base/media/VideoItem;

.field public final synthetic b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public final synthetic c:Ljava/io/File;

.field public final synthetic d:LX/8Of;


# direct methods
.method public constructor <init>(LX/8Of;Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1339759
    iput-object p1, p0, LX/8Oe;->d:LX/8Of;

    iput-object p2, p0, LX/8Oe;->a:Lcom/facebook/photos/base/media/VideoItem;

    iput-object p3, p0, LX/8Oe;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object p4, p0, LX/8Oe;->c:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1339760
    const/4 v4, 0x0

    .line 1339761
    iget-object v0, p0, LX/8Oe;->a:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1339762
    iget-object v0, p0, LX/8Oe;->a:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, LX/8Oe;->a:Lcom/facebook/photos/base/media/VideoItem;

    iget-object v2, p0, LX/8Oe;->a:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v2

    const/4 v10, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 1339763
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 1339764
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v8

    .line 1339765
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v3

    rem-int/lit16 v3, v3, 0xb4

    if-nez v3, :cond_3

    const/4 v3, 0x1

    move v6, v3

    .line 1339766
    :goto_0
    if-eqz v6, :cond_4

    .line 1339767
    iget v3, v8, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v3, v3

    .line 1339768
    move v5, v3

    .line 1339769
    :goto_1
    if-eqz v6, :cond_5

    .line 1339770
    iget v3, v8, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v3, v3

    .line 1339771
    :goto_2
    int-to-float v5, v5

    .line 1339772
    int-to-float v3, v3

    div-float/2addr v3, v2

    .line 1339773
    sub-float v3, v5, v3

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v3, v8

    div-float/2addr v3, v5

    .line 1339774
    if-eqz v4, :cond_0

    if-eqz v6, :cond_6

    .line 1339775
    :cond_0
    iput v10, v7, Landroid/graphics/RectF;->left:F

    .line 1339776
    iput v9, v7, Landroid/graphics/RectF;->right:F

    .line 1339777
    iput v3, v7, Landroid/graphics/RectF;->top:F

    .line 1339778
    sub-float v3, v9, v3

    iput v3, v7, Landroid/graphics/RectF;->bottom:F

    .line 1339779
    :goto_3
    move-object v0, v7

    .line 1339780
    :goto_4
    iget-object v2, p0, LX/8Oe;->d:LX/8Of;

    iget-object v2, v2, LX/8Of;->e:LX/7St;

    .line 1339781
    iget v3, v1, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v3, v3

    .line 1339782
    iget v5, v1, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v1, v5

    .line 1339783
    invoke-virtual {v2, v3, v1}, LX/7St;->a(II)V

    .line 1339784
    invoke-static {}, LX/7TH;->newBuilder()LX/7TI;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/8Oe;->a:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1339785
    iput-object v2, v1, LX/7TI;->a:Ljava/io/File;

    .line 1339786
    move-object v1, v1

    .line 1339787
    iput-object v0, v1, LX/7TI;->d:Landroid/graphics/RectF;

    .line 1339788
    move-object v0, v1

    .line 1339789
    iget-object v1, p0, LX/8Oe;->d:LX/8Of;

    iget-object v1, v1, LX/8Of;->e:LX/7St;

    .line 1339790
    iput-object v1, v0, LX/7TI;->c:LX/2Md;

    .line 1339791
    move-object v0, v0

    .line 1339792
    iput v4, v0, LX/7TI;->f:I

    .line 1339793
    move-object v0, v0

    .line 1339794
    iget-object v1, p0, LX/8Oe;->a:Lcom/facebook/photos/base/media/VideoItem;

    .line 1339795
    iget-wide v5, v1, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v2, v5

    .line 1339796
    long-to-int v1, v2

    .line 1339797
    iput v1, v0, LX/7TI;->g:I

    .line 1339798
    move-object v0, v0

    .line 1339799
    iget-object v1, p0, LX/8Oe;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v1

    .line 1339800
    iput-boolean v1, v0, LX/7TI;->i:Z

    .line 1339801
    move-object v1, v0

    .line 1339802
    iget-object v0, p0, LX/8Oe;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/7Sv;->MIRROR_HORIZONTALLY:LX/7Sv;

    .line 1339803
    :goto_5
    iput-object v0, v1, LX/7TI;->e:LX/7Sv;

    .line 1339804
    move-object v0, v1

    .line 1339805
    iget-object v1, p0, LX/8Oe;->c:Ljava/io/File;

    .line 1339806
    iput-object v1, v0, LX/7TI;->b:Ljava/io/File;

    .line 1339807
    move-object v0, v0

    .line 1339808
    iget-object v1, p0, LX/8Oe;->d:LX/8Of;

    iget-object v2, p0, LX/8Oe;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getGLRendererConfigs()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8Of;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    .line 1339809
    iput-object v1, v0, LX/7TI;->n:LX/0Px;

    .line 1339810
    move-object v0, v0

    .line 1339811
    invoke-virtual {v0}, LX/7TI;->o()LX/7TH;

    move-result-object v0

    .line 1339812
    iget-object v1, p0, LX/8Oe;->d:LX/8Of;

    iget-object v1, v1, LX/8Of;->c:LX/7TG;

    invoke-virtual {v1, v0}, LX/7TG;->a(LX/7TH;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1339813
    new-instance v1, LX/8Od;

    invoke-direct {v1, p0}, LX/8Od;-><init>(LX/8Oe;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1339814
    const v1, -0x2f232f27

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    return-object v0

    .line 1339815
    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 1339816
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v0, v0

    .line 1339817
    goto/16 :goto_4

    .line 1339818
    :cond_2
    sget-object v0, LX/7Sv;->NONE:LX/7Sv;

    goto :goto_5

    .line 1339819
    :cond_3
    const/4 v3, 0x0

    move v6, v3

    goto/16 :goto_0

    .line 1339820
    :cond_4
    iget v3, v8, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v3, v3

    .line 1339821
    move v5, v3

    goto/16 :goto_1

    .line 1339822
    :cond_5
    iget v3, v8, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v3, v3

    .line 1339823
    goto/16 :goto_2

    .line 1339824
    :cond_6
    iput v3, v7, Landroid/graphics/RectF;->left:F

    .line 1339825
    sub-float v3, v9, v3

    iput v3, v7, Landroid/graphics/RectF;->right:F

    .line 1339826
    iput v10, v7, Landroid/graphics/RectF;->top:F

    .line 1339827
    iput v9, v7, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3
.end method
