.class public final LX/7CR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/7CS;

.field public final b:Ljava/lang/StringBuffer;

.field public final c:Ljava/lang/StringBuffer;

.field public d:I


# direct methods
.method public constructor <init>(LX/7CS;I)V
    .locals 2

    .prologue
    .line 1180569
    iput-object p1, p0, LX/7CR;->a:LX/7CS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1180570
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v1, p0, LX/7CR;->a:LX/7CS;

    .line 1180571
    iget p1, v1, LX/7CS;->e:I

    move v1, p1

    .line 1180572
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, LX/7CR;->b:Ljava/lang/StringBuffer;

    .line 1180573
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v1, p0, LX/7CR;->a:LX/7CS;

    .line 1180574
    iget p1, v1, LX/7CS;->e:I

    move v1, p1

    .line 1180575
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, LX/7CR;->c:Ljava/lang/StringBuffer;

    .line 1180576
    iput p2, p0, LX/7CR;->d:I

    .line 1180577
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1180561
    iget v0, p0, LX/7CR;->d:I

    iget-object v1, p0, LX/7CR;->b:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1180562
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 1180563
    iget-object v0, p0, LX/7CR;->b:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1180564
    :goto_0
    return-void

    .line 1180565
    :cond_0
    iget-object v1, p0, LX/7CR;->b:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1180556
    iget v0, p0, LX/7CR;->d:I

    iget-object v1, p0, LX/7CR;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1180557
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 1180558
    iget-object v0, p0, LX/7CR;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1180559
    :goto_0
    return-void

    .line 1180560
    :cond_0
    iget-object v1, p0, LX/7CR;->c:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method


# virtual methods
.method public final a(C)V
    .locals 0

    .prologue
    .line 1180553
    invoke-virtual {p0, p1}, LX/7CR;->b(C)V

    .line 1180554
    invoke-virtual {p0, p1}, LX/7CR;->c(C)V

    .line 1180555
    return-void
.end method

.method public final a(CC)V
    .locals 0

    .prologue
    .line 1180566
    invoke-virtual {p0, p1}, LX/7CR;->b(C)V

    .line 1180567
    invoke-virtual {p0, p2}, LX/7CR;->c(C)V

    .line 1180568
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1180550
    invoke-direct {p0, p1}, LX/7CR;->b(Ljava/lang/String;)V

    .line 1180551
    invoke-direct {p0, p1}, LX/7CR;->c(Ljava/lang/String;)V

    .line 1180552
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1180547
    invoke-direct {p0, p1}, LX/7CR;->b(Ljava/lang/String;)V

    .line 1180548
    invoke-direct {p0, p2}, LX/7CR;->c(Ljava/lang/String;)V

    .line 1180549
    return-void
.end method

.method public final b(C)V
    .locals 2

    .prologue
    .line 1180544
    iget-object v0, p0, LX/7CR;->b:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    iget v1, p0, LX/7CR;->d:I

    if-ge v0, v1, :cond_0

    .line 1180545
    iget-object v0, p0, LX/7CR;->b:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1180546
    :cond_0
    return-void
.end method

.method public final c(C)V
    .locals 2

    .prologue
    .line 1180541
    iget-object v0, p0, LX/7CR;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    iget v1, p0, LX/7CR;->d:I

    if-ge v0, v1, :cond_0

    .line 1180542
    iget-object v0, p0, LX/7CR;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1180543
    :cond_0
    return-void
.end method
