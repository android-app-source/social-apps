.class public LX/6mZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final hostName:Ljava/lang/String;

.field public final ipAddr:Ljava/lang/String;

.field public final vipAddr:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0xb

    .line 1145175
    new-instance v0, LX/1sv;

    const-string v1, "ProxygenInfo"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mZ;->b:LX/1sv;

    .line 1145176
    new-instance v0, LX/1sw;

    const-string v1, "ipAddr"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mZ;->c:LX/1sw;

    .line 1145177
    new-instance v0, LX/1sw;

    const-string v1, "hostName"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mZ;->d:LX/1sw;

    .line 1145178
    new-instance v0, LX/1sw;

    const-string v1, "vipAddr"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mZ;->e:LX/1sw;

    .line 1145179
    sput-boolean v4, LX/6mZ;->a:Z

    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1145139
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1145140
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1145141
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1145142
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ProxygenInfo"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1145143
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145144
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145145
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145146
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145147
    const-string v4, "ipAddr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145148
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145149
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145150
    iget-object v4, p0, LX/6mZ;->ipAddr:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 1145151
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145152
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145153
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145154
    const-string v4, "hostName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145155
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145156
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145157
    iget-object v4, p0, LX/6mZ;->hostName:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 1145158
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145159
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145160
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145161
    const-string v4, "vipAddr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145162
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145163
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145164
    iget-object v0, p0, LX/6mZ;->vipAddr:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1145165
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145166
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145167
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145168
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1145169
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1145170
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1145171
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1145172
    :cond_3
    iget-object v4, p0, LX/6mZ;->ipAddr:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1145173
    :cond_4
    iget-object v4, p0, LX/6mZ;->hostName:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1145174
    :cond_5
    iget-object v0, p0, LX/6mZ;->vipAddr:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1145180
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1145181
    iget-object v0, p0, LX/6mZ;->ipAddr:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1145182
    sget-object v0, LX/6mZ;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145183
    iget-object v0, p0, LX/6mZ;->ipAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145184
    :cond_0
    iget-object v0, p0, LX/6mZ;->hostName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1145185
    sget-object v0, LX/6mZ;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145186
    iget-object v0, p0, LX/6mZ;->hostName:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145187
    :cond_1
    iget-object v0, p0, LX/6mZ;->vipAddr:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1145188
    sget-object v0, LX/6mZ;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145189
    iget-object v0, p0, LX/6mZ;->vipAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145190
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1145191
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1145192
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1145110
    if-nez p1, :cond_1

    .line 1145111
    :cond_0
    :goto_0
    return v0

    .line 1145112
    :cond_1
    instance-of v1, p1, LX/6mZ;

    if-eqz v1, :cond_0

    .line 1145113
    check-cast p1, LX/6mZ;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1145114
    if-nez p1, :cond_3

    .line 1145115
    :cond_2
    :goto_1
    move v0, v2

    .line 1145116
    goto :goto_0

    .line 1145117
    :cond_3
    iget-object v0, p0, LX/6mZ;->ipAddr:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1145118
    :goto_2
    iget-object v3, p1, LX/6mZ;->ipAddr:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1145119
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1145120
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145121
    iget-object v0, p0, LX/6mZ;->ipAddr:Ljava/lang/String;

    iget-object v3, p1, LX/6mZ;->ipAddr:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145122
    :cond_5
    iget-object v0, p0, LX/6mZ;->hostName:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1145123
    :goto_4
    iget-object v3, p1, LX/6mZ;->hostName:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1145124
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1145125
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145126
    iget-object v0, p0, LX/6mZ;->hostName:Ljava/lang/String;

    iget-object v3, p1, LX/6mZ;->hostName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145127
    :cond_7
    iget-object v0, p0, LX/6mZ;->vipAddr:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1145128
    :goto_6
    iget-object v3, p1, LX/6mZ;->vipAddr:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1145129
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1145130
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145131
    iget-object v0, p0, LX/6mZ;->vipAddr:Ljava/lang/String;

    iget-object v3, p1, LX/6mZ;->vipAddr:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1145132
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1145133
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1145134
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1145135
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1145136
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1145137
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1145138
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1145109
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1145106
    sget-boolean v0, LX/6mZ;->a:Z

    .line 1145107
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mZ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1145108
    return-object v0
.end method
