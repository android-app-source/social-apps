.class public LX/6jn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final count:Ljava/lang/Integer;

.field public final counts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/6l4;",
            ">;"
        }
    .end annotation
.end field

.field public final hasMore:Ljava/lang/Boolean;

.field public final threadFolder:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 1132924
    new-instance v0, LX/1sv;

    const-string v1, "DeltaFolderCount"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jn;->b:LX/1sv;

    .line 1132925
    new-instance v0, LX/1sw;

    const-string v1, "threadFolder"

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jn;->c:LX/1sw;

    .line 1132926
    new-instance v0, LX/1sw;

    const-string v1, "count"

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jn;->d:LX/1sw;

    .line 1132927
    new-instance v0, LX/1sw;

    const-string v1, "hasMore"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jn;->e:LX/1sw;

    .line 1132928
    new-instance v0, LX/1sw;

    const-string v1, "counts"

    const/16 v2, 0xd

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jn;->f:LX/1sw;

    .line 1132929
    sput-boolean v4, LX/6jn;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/6l4;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1132918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132919
    iput-object p1, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    .line 1132920
    iput-object p2, p0, LX/6jn;->count:Ljava/lang/Integer;

    .line 1132921
    iput-object p3, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    .line 1132922
    iput-object p4, p0, LX/6jn;->counts:Ljava/util/Map;

    .line 1132923
    return-void
.end method

.method public static a(LX/6jn;)V
    .locals 4

    .prologue
    .line 1132797
    iget-object v0, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 1132798
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'threadFolder\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jn;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132799
    :cond_0
    iget-object v0, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6l8;->a:LX/1sn;

    iget-object v1, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1132800
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'threadFolder\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1132801
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1132865
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1132866
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v2, v0

    .line 1132867
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    move-object v1, v0

    .line 1132868
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaFolderCount"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132869
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132870
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132871
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132872
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132873
    const-string v0, "threadFolder"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132874
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132875
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132876
    iget-object v0, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1132877
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132878
    :cond_0
    :goto_3
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1132879
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132880
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132881
    const-string v0, "count"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132882
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132883
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132884
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    if-nez v0, :cond_9

    .line 1132885
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132886
    :cond_1
    :goto_4
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1132887
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132888
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132889
    const-string v0, "hasMore"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132890
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132891
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132892
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    .line 1132893
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132894
    :cond_2
    :goto_5
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 1132895
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132896
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132897
    const-string v0, "counts"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132898
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132899
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132900
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    if-nez v0, :cond_b

    .line 1132901
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132902
    :cond_3
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132903
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132904
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132905
    :cond_4
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1132906
    :cond_5
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1132907
    :cond_6
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1132908
    :cond_7
    sget-object v0, LX/6l8;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1132909
    if-eqz v0, :cond_8

    .line 1132910
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132911
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132912
    :cond_8
    iget-object v5, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1132913
    if-eqz v0, :cond_0

    .line 1132914
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1132915
    :cond_9
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1132916
    :cond_a
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1132917
    :cond_b
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 1132842
    invoke-static {p0}, LX/6jn;->a(LX/6jn;)V

    .line 1132843
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132844
    iget-object v0, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1132845
    sget-object v0, LX/6jn;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132846
    iget-object v0, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1132847
    :cond_0
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1132848
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1132849
    sget-object v0, LX/6jn;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132850
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1132851
    :cond_1
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1132852
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1132853
    sget-object v0, LX/6jn;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132854
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1132855
    :cond_2
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 1132856
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 1132857
    sget-object v0, LX/6jn;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132858
    new-instance v0, LX/7H3;

    const/16 v1, 0x8

    const/16 v2, 0xc

    iget-object v3, p0, LX/6jn;->counts:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1132859
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1132860
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, LX/1su;->a(I)V

    .line 1132861
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6l4;

    invoke-virtual {v0, p1}, LX/6l4;->a(LX/1su;)V

    goto :goto_0

    .line 1132862
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132863
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132864
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132806
    if-nez p1, :cond_1

    .line 1132807
    :cond_0
    :goto_0
    return v0

    .line 1132808
    :cond_1
    instance-of v1, p1, LX/6jn;

    if-eqz v1, :cond_0

    .line 1132809
    check-cast p1, LX/6jn;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1132810
    if-nez p1, :cond_3

    .line 1132811
    :cond_2
    :goto_1
    move v0, v2

    .line 1132812
    goto :goto_0

    .line 1132813
    :cond_3
    iget-object v0, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1132814
    :goto_2
    iget-object v3, p1, LX/6jn;->threadFolder:Ljava/lang/Integer;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1132815
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1132816
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132817
    iget-object v0, p0, LX/6jn;->threadFolder:Ljava/lang/Integer;

    iget-object v3, p1, LX/6jn;->threadFolder:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132818
    :cond_5
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1132819
    :goto_4
    iget-object v3, p1, LX/6jn;->count:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1132820
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1132821
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132822
    iget-object v0, p0, LX/6jn;->count:Ljava/lang/Integer;

    iget-object v3, p1, LX/6jn;->count:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132823
    :cond_7
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1132824
    :goto_6
    iget-object v3, p1, LX/6jn;->hasMore:Ljava/lang/Boolean;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1132825
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1132826
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132827
    iget-object v0, p0, LX/6jn;->hasMore:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jn;->hasMore:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132828
    :cond_9
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1132829
    :goto_8
    iget-object v3, p1, LX/6jn;->counts:Ljava/util/Map;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1132830
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1132831
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132832
    iget-object v0, p0, LX/6jn;->counts:Ljava/util/Map;

    iget-object v3, p1, LX/6jn;->counts:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1132833
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1132834
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1132835
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1132836
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1132837
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1132838
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1132839
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1132840
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1132841
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132805
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132802
    sget-boolean v0, LX/6jn;->a:Z

    .line 1132803
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jn;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132804
    return-object v0
.end method
