.class public LX/6q8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/auth/pin/params/CreateFingerprintNonceParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dC;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150032
    iput-object p1, p0, LX/6q8;->a:LX/0dC;

    .line 1150033
    iput-object p2, p0, LX/6q8;->b:LX/0Or;

    .line 1150034
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1150035
    check-cast p1, Lcom/facebook/payments/auth/pin/params/CreateFingerprintNonceParams;

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1150036
    new-array v0, v4, [Lorg/apache/http/NameValuePair;

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pin"

    .line 1150037
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/params/CreateFingerprintNonceParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1150038
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/6q8;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1150039
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "create_fingerprint_nonce_method"

    .line 1150040
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150041
    move-object v1, v1

    .line 1150042
    const-string v2, "POST"

    .line 1150043
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150044
    move-object v1, v1

    .line 1150045
    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, LX/6q8;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    const-string v4, "p2p_gen_touch_id_nonces"

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1150046
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150047
    move-object v1, v1

    .line 1150048
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150049
    move-object v0, v1

    .line 1150050
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1150051
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150052
    move-object v0, v0

    .line 1150053
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1150054
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150055
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "nonce"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "Expected response in the form of {\"nonce\": \"token\"} but was %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1150056
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
