.class public final enum LX/7Je;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Je;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Je;

.field public static final enum AVAILABLE_DISK_SIZE:LX/7Je;

.field public static final enum CONNECTION_SUB_TYPE_PARAM:LX/7Je;

.field public static final enum CONNECTION_TYPE_PARAM:LX/7Je;

.field public static final enum DELETE_REASON:LX/7Je;

.field public static final enum DISK_QUOTA:LX/7Je;

.field public static final enum DOWNLOADED_SIZE:LX/7Je;

.field public static final enum DOWNLOAD_ATTEMPTS:LX/7Je;

.field public static final enum DOWNLOAD_DURATION:LX/7Je;

.field public static final enum DOWNLOAD_OPTION_STATE:LX/7Je;

.field public static final enum DOWNLOAD_ORIGIN:LX/7Je;

.field public static final enum DOWNLOAD_SESSION_ID:LX/7Je;

.field public static final enum DOWNLOAD_TYPE:LX/7Je;

.field public static final enum EXCEPTION:LX/7Je;

.field public static final enum EXCEPTION_CODE:LX/7Je;

.field public static final enum OFFLINE_VIDEO_COUNT:LX/7Je;

.field public static final enum PLAYER_ORIGIN:LX/7Je;

.field public static final enum SAVED_OFFLINE:LX/7Je;

.field public static final enum SCHEDULING_POLICY:LX/7Je;

.field public static final enum TIME_SINCE_LAST_CHECK:LX/7Je;

.field public static final enum TOTAL_OFFLINE_VIDEO_SIZE:LX/7Je;

.field public static final enum VIDEO_FILE_NAME:LX/7Je;

.field public static final enum VIDEO_ID:LX/7Je;

.field public static final enum VIDEO_SIZE:LX/7Je;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1193980
    new-instance v0, LX/7Je;

    const-string v1, "VIDEO_ID"

    const-string v2, "video_id"

    invoke-direct {v0, v1, v4, v2}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->VIDEO_ID:LX/7Je;

    .line 1193981
    new-instance v0, LX/7Je;

    const-string v1, "VIDEO_SIZE"

    const-string v2, "video_size"

    invoke-direct {v0, v1, v5, v2}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->VIDEO_SIZE:LX/7Je;

    .line 1193982
    new-instance v0, LX/7Je;

    const-string v1, "DOWNLOADED_SIZE"

    const-string v2, "downloaded_size"

    invoke-direct {v0, v1, v6, v2}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DOWNLOADED_SIZE:LX/7Je;

    .line 1193983
    new-instance v0, LX/7Je;

    const-string v1, "DOWNLOAD_SESSION_ID"

    const-string v2, "download_session_id"

    invoke-direct {v0, v1, v7, v2}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DOWNLOAD_SESSION_ID:LX/7Je;

    .line 1193984
    new-instance v0, LX/7Je;

    const-string v1, "DOWNLOAD_ORIGIN"

    const-string v2, "download_origin"

    invoke-direct {v0, v1, v8, v2}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DOWNLOAD_ORIGIN:LX/7Je;

    .line 1193985
    new-instance v0, LX/7Je;

    const-string v1, "DOWNLOAD_DURATION"

    const/4 v2, 0x5

    const-string v3, "download_duration"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DOWNLOAD_DURATION:LX/7Je;

    .line 1193986
    new-instance v0, LX/7Je;

    const-string v1, "DOWNLOAD_ATTEMPTS"

    const/4 v2, 0x6

    const-string v3, "download_attempts"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DOWNLOAD_ATTEMPTS:LX/7Je;

    .line 1193987
    new-instance v0, LX/7Je;

    const-string v1, "CONNECTION_TYPE_PARAM"

    const/4 v2, 0x7

    const-string v3, "connection_type"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->CONNECTION_TYPE_PARAM:LX/7Je;

    .line 1193988
    new-instance v0, LX/7Je;

    const-string v1, "CONNECTION_SUB_TYPE_PARAM"

    const/16 v2, 0x8

    const-string v3, "connection_sub_type"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->CONNECTION_SUB_TYPE_PARAM:LX/7Je;

    .line 1193989
    new-instance v0, LX/7Je;

    const-string v1, "TOTAL_OFFLINE_VIDEO_SIZE"

    const/16 v2, 0x9

    const-string v3, "offline_video_size"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->TOTAL_OFFLINE_VIDEO_SIZE:LX/7Je;

    .line 1193990
    new-instance v0, LX/7Je;

    const-string v1, "AVAILABLE_DISK_SIZE"

    const/16 v2, 0xa

    const-string v3, "available_disk_size"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->AVAILABLE_DISK_SIZE:LX/7Je;

    .line 1193991
    new-instance v0, LX/7Je;

    const-string v1, "SAVED_OFFLINE"

    const/16 v2, 0xb

    const-string v3, "saved_offline"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->SAVED_OFFLINE:LX/7Je;

    .line 1193992
    new-instance v0, LX/7Je;

    const-string v1, "DISK_QUOTA"

    const/16 v2, 0xc

    const-string v3, "disk_quota"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DISK_QUOTA:LX/7Je;

    .line 1193993
    new-instance v0, LX/7Je;

    const-string v1, "OFFLINE_VIDEO_COUNT"

    const/16 v2, 0xd

    const-string v3, "offline_video_count"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->OFFLINE_VIDEO_COUNT:LX/7Je;

    .line 1193994
    new-instance v0, LX/7Je;

    const-string v1, "VIDEO_FILE_NAME"

    const/16 v2, 0xe

    const-string v3, "video_file_name"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->VIDEO_FILE_NAME:LX/7Je;

    .line 1193995
    new-instance v0, LX/7Je;

    const-string v1, "DELETE_REASON"

    const/16 v2, 0xf

    const-string v3, "delete_reason"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DELETE_REASON:LX/7Je;

    .line 1193996
    new-instance v0, LX/7Je;

    const-string v1, "EXCEPTION"

    const/16 v2, 0x10

    const-string v3, "exception"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->EXCEPTION:LX/7Je;

    .line 1193997
    new-instance v0, LX/7Je;

    const-string v1, "EXCEPTION_CODE"

    const/16 v2, 0x11

    const-string v3, "exception_code"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->EXCEPTION_CODE:LX/7Je;

    .line 1193998
    new-instance v0, LX/7Je;

    const-string v1, "TIME_SINCE_LAST_CHECK"

    const/16 v2, 0x12

    const-string v3, "time_since_last_check"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->TIME_SINCE_LAST_CHECK:LX/7Je;

    .line 1193999
    new-instance v0, LX/7Je;

    const-string v1, "DOWNLOAD_OPTION_STATE"

    const/16 v2, 0x13

    const-string v3, "download_option_state"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DOWNLOAD_OPTION_STATE:LX/7Je;

    .line 1194000
    new-instance v0, LX/7Je;

    const-string v1, "SCHEDULING_POLICY"

    const/16 v2, 0x14

    const-string v3, "scheduling_policy"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->SCHEDULING_POLICY:LX/7Je;

    .line 1194001
    new-instance v0, LX/7Je;

    const-string v1, "DOWNLOAD_TYPE"

    const/16 v2, 0x15

    const-string v3, "download_type"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->DOWNLOAD_TYPE:LX/7Je;

    .line 1194002
    new-instance v0, LX/7Je;

    const-string v1, "PLAYER_ORIGIN"

    const/16 v2, 0x16

    const-string v3, "player_origin"

    invoke-direct {v0, v1, v2, v3}, LX/7Je;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Je;->PLAYER_ORIGIN:LX/7Je;

    .line 1194003
    const/16 v0, 0x17

    new-array v0, v0, [LX/7Je;

    sget-object v1, LX/7Je;->VIDEO_ID:LX/7Je;

    aput-object v1, v0, v4

    sget-object v1, LX/7Je;->VIDEO_SIZE:LX/7Je;

    aput-object v1, v0, v5

    sget-object v1, LX/7Je;->DOWNLOADED_SIZE:LX/7Je;

    aput-object v1, v0, v6

    sget-object v1, LX/7Je;->DOWNLOAD_SESSION_ID:LX/7Je;

    aput-object v1, v0, v7

    sget-object v1, LX/7Je;->DOWNLOAD_ORIGIN:LX/7Je;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Je;->DOWNLOAD_DURATION:LX/7Je;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Je;->DOWNLOAD_ATTEMPTS:LX/7Je;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Je;->CONNECTION_TYPE_PARAM:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Je;->CONNECTION_SUB_TYPE_PARAM:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7Je;->TOTAL_OFFLINE_VIDEO_SIZE:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7Je;->AVAILABLE_DISK_SIZE:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7Je;->SAVED_OFFLINE:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7Je;->DISK_QUOTA:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7Je;->OFFLINE_VIDEO_COUNT:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7Je;->VIDEO_FILE_NAME:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7Je;->DELETE_REASON:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7Je;->EXCEPTION:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7Je;->EXCEPTION_CODE:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7Je;->TIME_SINCE_LAST_CHECK:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/7Je;->DOWNLOAD_OPTION_STATE:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/7Je;->SCHEDULING_POLICY:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/7Je;->DOWNLOAD_TYPE:LX/7Je;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/7Je;->PLAYER_ORIGIN:LX/7Je;

    aput-object v2, v0, v1

    sput-object v0, LX/7Je;->$VALUES:[LX/7Je;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1194004
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1194005
    iput-object p3, p0, LX/7Je;->value:Ljava/lang/String;

    .line 1194006
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Je;
    .locals 1

    .prologue
    .line 1194007
    const-class v0, LX/7Je;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Je;

    return-object v0
.end method

.method public static values()[LX/7Je;
    .locals 1

    .prologue
    .line 1194008
    sget-object v0, LX/7Je;->$VALUES:[LX/7Je;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Je;

    return-object v0
.end method
