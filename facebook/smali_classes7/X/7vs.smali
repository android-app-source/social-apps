.class public LX/7vs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Du;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7vi;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/6r9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/1nQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/6Ex;

.field private f:LX/7vr;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1275211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275212
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1275213
    iput-object v0, p0, LX/7vs;->a:LX/0Ot;

    .line 1275214
    return-void
.end method

.method public static a$redex0(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1275219
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275220
    const/4 v3, 0x0

    .line 1275221
    invoke-virtual {v0}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v1

    sget-object v2, LX/7wp;->CHECKOUT:LX/7wp;

    invoke-interface {v1, v2}, LX/7wo;->a(LX/7wp;)LX/7wo;

    move-result-object v1

    invoke-interface {v1, v3}, LX/7wo;->a(Ljava/lang/String;)LX/7wo;

    move-result-object v1

    invoke-interface {v1, v3}, LX/7wo;->b(Ljava/lang/String;)LX/7wo;

    move-result-object v1

    invoke-interface {v1}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v1

    invoke-static {p0, p1, v1}, LX/7vs;->b(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1275222
    iget-object v0, p0, LX/7vs;->e:LX/6Ex;

    invoke-interface {v0, p3}, LX/6Ex;->a(Ljava/lang/Throwable;)V

    .line 1275223
    iget-object v0, p0, LX/7vs;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7vi;

    invoke-virtual {v0}, LX/7vi;->a()V

    .line 1275224
    iget-object v0, p0, LX/7vs;->c:Landroid/content/Context;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    const/4 v2, 0x0

    invoke-static {v0, p2, v1, v2}, LX/6rI;->a(Landroid/content/Context;Ljava/lang/String;ZLX/6qh;)V

    .line 1275225
    return-void
.end method

.method public static b(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 2

    .prologue
    .line 1275192
    iget-object v0, p0, LX/7vs;->b:LX/6r9;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Landroid/os/Parcelable;)V

    .line 1275193
    return-void
.end method

.method private e(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/7vr;
    .locals 2

    .prologue
    .line 1275215
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275216
    iget-object v1, p0, LX/7vs;->f:LX/7vr;

    if-nez v1, :cond_0

    .line 1275217
    new-instance v1, LX/7vr;

    invoke-direct {v1, p0, v0, p1}, LX/7vr;-><init>(LX/7vs;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/payments/checkout/model/CheckoutData;)V

    iput-object v1, p0, LX/7vs;->f:LX/7vr;

    .line 1275218
    :cond_0
    iget-object v0, p0, LX/7vs;->f:LX/7vr;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1275199
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275200
    invoke-virtual {v1}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v0

    sget-object v2, LX/7wp;->BUYING:LX/7wp;

    invoke-interface {v0, v2}, LX/7wo;->a(LX/7wp;)LX/7wo;

    move-result-object v0

    invoke-interface {v0}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/7vs;->b(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1275201
    iget-object v0, p0, LX/7vs;->d:LX/1nQ;

    iget-object v2, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget v4, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    .line 1275202
    iget-object v5, v0, LX/1nQ;->i:LX/0Zb;

    const-string v6, "event_buy_tickets_purchase_button_tapped"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 1275203
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1275204
    const-string v6, "event_ticketing"

    invoke-virtual {v5, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    iget-object v6, v0, LX/1nQ;->j:LX/0kv;

    iget-object v7, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v6, v7}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "Event"

    invoke-virtual {v5, v6}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    iget-object v6, v2, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "event_id"

    iget-object v7, v2, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "session_id"

    iget-object v7, v2, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "purchased_tickets_count"

    invoke-virtual {v5, v6, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v5

    invoke-virtual {v5}, LX/0oG;->d()V

    .line 1275205
    :cond_0
    iget-object v0, p0, LX/7vs;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7vi;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->l()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v5

    check-cast v5, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;

    iget-object v5, v5, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {p0, p1}, LX/7vs;->e(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/7vr;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/7vi;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;LX/7vr;)V

    .line 1275206
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v2, v3

    .line 1275207
    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1275208
    iget-object v0, p0, LX/7vs;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7vi;

    invoke-virtual {v0}, LX/7vi;->a()V

    .line 1275209
    const/4 v0, 0x0

    iput-object v0, p0, LX/7vs;->e:LX/6Ex;

    .line 1275210
    return-void
.end method

.method public final a(LX/6Ex;)V
    .locals 0

    .prologue
    .line 1275197
    iput-object p1, p0, LX/7vs;->e:LX/6Ex;

    .line 1275198
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1275196
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 2

    .prologue
    .line 1275194
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275195
    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->BUYING:LX/7wp;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1275190
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275191
    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 5

    .prologue
    .line 1275178
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275179
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1275180
    :goto_0
    return-void

    .line 1275181
    :cond_0
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1275182
    iget-object v1, p0, LX/7vs;->e:LX/6Ex;

    iget-object v2, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/6Ex;->a(Ljava/lang/String;)V

    .line 1275183
    :cond_1
    iget-object v1, p0, LX/7vs;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7vi;

    iget-object v2, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    invoke-direct {p0, p1}, LX/7vs;->e(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/7vr;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/7vi;->a(Ljava/lang/String;LX/7vr;)V

    .line 1275184
    iget-object v1, p0, LX/7vs;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7vi;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    invoke-direct {p0, p1}, LX/7vs;->e(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/7vr;

    move-result-object v2

    .line 1275185
    new-instance v3, LX/7oA;

    invoke-direct {v3}, LX/7oA;-><init>()V

    move-object v3, v3

    .line 1275186
    const-string v4, "order_id"

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1275187
    const-string v4, "should_fetch_ticket_tiers"

    const/4 p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1275188
    iget-object v4, v1, LX/7vi;->d:LX/1Ck;

    const-string p0, "event_ticket_poll_order_status"

    iget-object p1, v1, LX/7vi;->b:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance p1, LX/7vg;

    invoke-direct {p1, v1, v2}, LX/7vg;-><init>(LX/7vi;LX/7vr;)V

    invoke-virtual {v4, p0, v3, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1275189
    goto :goto_0
.end method
