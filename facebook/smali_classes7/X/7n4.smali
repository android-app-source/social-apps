.class public final LX/7n4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1237699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;
    .locals 11

    .prologue
    .line 1237700
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1237701
    iget-object v1, p0, LX/7n4;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1237702
    iget-object v1, p0, LX/7n4;->d:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1237703
    iget-object v1, p0, LX/7n4;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1237704
    iget-object v1, p0, LX/7n4;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1237705
    iget-object v1, p0, LX/7n4;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1237706
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1237707
    const/4 v1, 0x0

    iget-wide v2, p0, LX/7n4;->a:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1237708
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1237709
    const/4 v1, 0x2

    iget-wide v2, p0, LX/7n4;->c:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1237710
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1237711
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1237712
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1237713
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1237714
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1237715
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1237716
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1237717
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1237718
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1237719
    new-instance v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-direct {v1, v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;-><init>(LX/15i;)V

    .line 1237720
    return-object v1
.end method
