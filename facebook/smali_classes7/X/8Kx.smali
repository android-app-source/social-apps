.class public final enum LX/8Kx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Kx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Kx;

.field public static final enum AutoRetry:LX/8Kx;

.field public static final enum InitialPost:LX/8Kx;

.field public static final enum Restore:LX/8Kx;

.field public static final enum Resume:LX/8Kx;

.field public static final enum UserRestart:LX/8Kx;

.field public static final enum UserRetry:LX/8Kx;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1331008
    new-instance v0, LX/8Kx;

    const-string v1, "InitialPost"

    invoke-direct {v0, v1, v3}, LX/8Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Kx;->InitialPost:LX/8Kx;

    .line 1331009
    new-instance v0, LX/8Kx;

    const-string v1, "UserRetry"

    invoke-direct {v0, v1, v4}, LX/8Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Kx;->UserRetry:LX/8Kx;

    .line 1331010
    new-instance v0, LX/8Kx;

    const-string v1, "AutoRetry"

    invoke-direct {v0, v1, v5}, LX/8Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Kx;->AutoRetry:LX/8Kx;

    .line 1331011
    new-instance v0, LX/8Kx;

    const-string v1, "Resume"

    invoke-direct {v0, v1, v6}, LX/8Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Kx;->Resume:LX/8Kx;

    .line 1331012
    new-instance v0, LX/8Kx;

    const-string v1, "Restore"

    invoke-direct {v0, v1, v7}, LX/8Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Kx;->Restore:LX/8Kx;

    .line 1331013
    new-instance v0, LX/8Kx;

    const-string v1, "UserRestart"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Kx;->UserRestart:LX/8Kx;

    .line 1331014
    const/4 v0, 0x6

    new-array v0, v0, [LX/8Kx;

    sget-object v1, LX/8Kx;->InitialPost:LX/8Kx;

    aput-object v1, v0, v3

    sget-object v1, LX/8Kx;->UserRetry:LX/8Kx;

    aput-object v1, v0, v4

    sget-object v1, LX/8Kx;->AutoRetry:LX/8Kx;

    aput-object v1, v0, v5

    sget-object v1, LX/8Kx;->Resume:LX/8Kx;

    aput-object v1, v0, v6

    sget-object v1, LX/8Kx;->Restore:LX/8Kx;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8Kx;->UserRestart:LX/8Kx;

    aput-object v2, v0, v1

    sput-object v0, LX/8Kx;->$VALUES:[LX/8Kx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1331015
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Kx;
    .locals 1

    .prologue
    .line 1331016
    const-class v0, LX/8Kx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Kx;

    return-object v0
.end method

.method public static values()[LX/8Kx;
    .locals 1

    .prologue
    .line 1331017
    sget-object v0, LX/8Kx;->$VALUES:[LX/8Kx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Kx;

    return-object v0
.end method
