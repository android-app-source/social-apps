.class public final LX/7G6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7G5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7G5",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:J

.field public final synthetic c:I

.field public final synthetic d:LX/7G8;

.field private e:I


# direct methods
.method public constructor <init>(LX/7G8;Ljava/lang/String;JI)V
    .locals 1

    .prologue
    .line 1188760
    iput-object p1, p0, LX/7G6;->d:LX/7G8;

    iput-object p2, p0, LX/7G6;->a:Ljava/lang/String;

    iput-wide p3, p0, LX/7G6;->b:J

    iput p5, p0, LX/7G6;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188761
    const/4 v0, 0x0

    iput v0, p0, LX/7G6;->e:I

    return-void
.end method


# virtual methods
.method public final a()LX/7Gc;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/7Gc",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1188762
    iget-object v1, p0, LX/7G6;->d:LX/7G8;

    iget-object v1, v1, LX/7G8;->c:LX/2Sx;

    invoke-virtual {v1}, LX/2Sx;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1188763
    new-instance v1, LX/7Gc;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    move-object v0, v1

    .line 1188764
    :goto_0
    return-object v0

    .line 1188765
    :cond_0
    iget-object v1, p0, LX/7G6;->d:LX/7G8;

    iget-object v1, v1, LX/7G8;->d:LX/7G1;

    iget-object v2, p0, LX/7G6;->d:LX/7G8;

    invoke-virtual {v2}, LX/7G8;->a()LX/7GT;

    move-result-object v2

    iget v3, p0, LX/7G6;->e:I

    .line 1188766
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "sync_resume_queue_connection_attempt"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "attempt"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1188767
    invoke-virtual {v1, v4, v2}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 1188768
    iget-object v1, p0, LX/7G6;->d:LX/7G8;

    iget-object v1, v1, LX/7G8;->e:LX/6Po;

    sget-object v2, LX/7GY;->c:LX/6Pr;

    const-string v3, "get_diffs. queueType = %s, syncToken = %s, seqId = %d, attempt %d."

    iget-object v4, p0, LX/7G6;->d:LX/7G8;

    invoke-virtual {v4}, LX/7G8;->a()LX/7GT;

    move-result-object v4

    iget-object v4, v4, LX/7GT;->apiString:Ljava/lang/String;

    iget-object v5, p0, LX/7G6;->a:Ljava/lang/String;

    iget-wide v6, p0, LX/7G6;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget v7, p0, LX/7G6;->e:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v3, v4, v5, v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 1188769
    iget-object v1, p0, LX/7G6;->d:LX/7G8;

    iget-object v1, v1, LX/7G8;->b:LX/7GD;

    iget v2, p0, LX/7G6;->c:I

    iget-object v3, p0, LX/7G6;->a:Ljava/lang/String;

    iget-wide v4, p0, LX/7G6;->b:J

    iget-object v6, p0, LX/7G6;->d:LX/7G8;

    iget-object v6, v6, LX/7G8;->i:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, LX/7GD;->a(ILjava/lang/String;JLjava/lang/String;)Z

    move-result v2

    .line 1188770
    new-instance v1, LX/7Gc;

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(J)V
    .locals 8

    .prologue
    .line 1188771
    iget-object v0, p0, LX/7G6;->d:LX/7G8;

    iget-object v0, v0, LX/7G8;->e:LX/6Po;

    sget-object v1, LX/7GY;->c:LX/6Pr;

    const-string v2, "get_diffs failed. queueType = %s, syncToken = %s, seqId = %d, attempt %d."

    iget-object v3, p0, LX/7G6;->d:LX/7G8;

    invoke-virtual {v3}, LX/7G8;->a()LX/7GT;

    move-result-object v3

    iget-object v3, v3, LX/7GT;->apiString:Ljava/lang/String;

    iget-object v4, p0, LX/7G6;->a:Ljava/lang/String;

    iget-wide v6, p0, LX/7G6;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget v6, p0, LX/7G6;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 1188772
    iget v0, p0, LX/7G6;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7G6;->e:I

    .line 1188773
    return-void
.end method
