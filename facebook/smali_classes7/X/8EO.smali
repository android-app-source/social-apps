.class public final LX/8EO;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageAdminCallToActionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1313593
    const-class v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageAdminCallToActionModel;

    const v0, -0x7c14da2c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PageAdminCallToAction"

    const-string v6, "167b2a9b3e0cd89086de29be13053831"

    const-string v7, "page"

    const-string v8, "10155207368586729"

    const-string v9, "10155259089536729"

    .line 1313594
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1313595
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1313596
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1313601
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1313602
    sparse-switch v0, :sswitch_data_0

    .line 1313603
    :goto_0
    return-object p1

    .line 1313604
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1313605
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1313606
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1313607
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x76efa4c9 -> :sswitch_3
        -0x71e86c6d -> :sswitch_0
        -0x2fe52f35 -> :sswitch_2
        0x78326898 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1313597
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1313598
    :goto_1
    return v0

    .line 1313599
    :pswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1313600
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
