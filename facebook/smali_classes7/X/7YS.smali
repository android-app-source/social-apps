.class public LX/7YS;
.super LX/6Ya;
.source ""


# instance fields
.field public final c:LX/2o9;

.field private final d:Landroid/os/Handler;

.field private e:LX/6YP;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/2o9;Landroid/os/Handler;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219918
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1219919
    new-instance v0, Lcom/facebook/zero/upsell/ui/screencontroller/VpnCallToHandleController$1;

    invoke-direct {v0, p0}, Lcom/facebook/zero/upsell/ui/screencontroller/VpnCallToHandleController$1;-><init>(LX/7YS;)V

    iput-object v0, p0, LX/7YS;->f:Ljava/lang/Runnable;

    .line 1219920
    iput-object p1, p0, LX/7YS;->c:LX/2o9;

    .line 1219921
    iput-object p2, p0, LX/7YS;->d:Landroid/os/Handler;

    .line 1219922
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1219928
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "org.internet"

    const-string v2, "com.facebook.iorg.app.activity.IorgDialogActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "protocol_version"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "token"

    const-string v2, "there_is_no_token"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feature_key_string_v2"

    iget-object v2, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1219929
    iget-object v3, v2, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v2, v3

    .line 1219930
    iget-object v2, v2, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1219931
    new-instance v1, LX/6YP;

    invoke-direct {v1, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/7YS;->e:LX/6YP;

    .line 1219932
    iget-object v1, p0, LX/7YS;->e:LX/6YP;

    invoke-virtual {v1}, LX/6YP;->a()V

    .line 1219933
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1219934
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1219935
    if-nez v3, :cond_2

    .line 1219936
    :cond_0
    :goto_0
    move v1, v1

    .line 1219937
    if-nez v1, :cond_1

    .line 1219938
    iget-object v0, p0, LX/7YS;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/zero/upsell/ui/screencontroller/VpnCallToHandleController$2;

    invoke-direct {v1, p0}, Lcom/facebook/zero/upsell/ui/screencontroller/VpnCallToHandleController$2;-><init>(LX/7YS;)V

    const v2, 0x221abe5d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1219939
    iget-object v0, p0, LX/7YS;->e:LX/6YP;

    .line 1219940
    :goto_1
    return-object v0

    .line 1219941
    :cond_1
    iget-object v1, p0, LX/7YS;->c:LX/2o9;

    iget-object v2, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v1, v2}, LX/2o9;->a(Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;)V

    .line 1219942
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x1f2b

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1219943
    iget-object v0, p0, LX/7YS;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/7YS;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3

    .line 1219944
    new-instance v4, LX/46l;

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v4, v2, v3, p1}, LX/46l;-><init>(JLjava/util/concurrent/TimeUnit;)V

    move-object v2, v4

    .line 1219945
    invoke-virtual {v2}, LX/46l;->a()J

    move-result-wide v2

    const v4, -0xf826503

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1219946
    iget-object v0, p0, LX/7YS;->e:LX/6YP;

    goto :goto_1

    .line 1219947
    :cond_2
    const/high16 v4, 0x10000

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 1219948
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1219926
    iget-object v0, p0, LX/7YS;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/7YS;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1219927
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1219923
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    if-nez v0, :cond_0

    .line 1219924
    :goto_0
    return-void

    .line 1219925
    :cond_0
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->FETCH_UPSELL:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_0
.end method
