.class public LX/826;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1287509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1287510
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1287511
    check-cast p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    .line 1287512
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1287513
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "item_id"

    .line 1287514
    iget-object v5, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1287515
    invoke-direct {v2, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "action"

    .line 1287516
    iget-object v5, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v5, v5

    .line 1287517
    invoke-virtual {v5}, LX/5vL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "curation_surface"

    .line 1287518
    iget-object v5, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1287519
    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "curation_mechanism"

    .line 1287520
    iget-object v5, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1287521
    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v5, "json"

    invoke-direct {v2, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-virtual {v4, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1287522
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1287523
    if-eqz v0, :cond_0

    .line 1287524
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy"

    .line 1287525
    iget-object v2, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1287526
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1287527
    :cond_0
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1287528
    if-eqz v0, :cond_1

    .line 1287529
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "tracking"

    .line 1287530
    iget-object v2, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->k:Ljava/lang/String;

    move-object v2, v2

    .line 1287531
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1287532
    :cond_1
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1287533
    if-eqz v0, :cond_2

    .line 1287534
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "source_story_id"

    .line 1287535
    iget-object v2, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    move-object v2, v2

    .line 1287536
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1287537
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1287538
    iget-object v1, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1287539
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/items"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1287540
    new-instance v0, LX/14N;

    const-string v1, "updateTimelineAppCollection"

    const-string v2, "POST"

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1287541
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1287542
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1287543
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
