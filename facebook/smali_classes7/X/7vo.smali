.class public LX/7vo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dp;


# instance fields
.field private final a:LX/6tI;


# direct methods
.method public constructor <init>(LX/6tI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275121
    iput-object p1, p0, LX/7vo;->a:LX/6tI;

    .line 1275122
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;
    .locals 2

    .prologue
    .line 1275123
    sget-object v0, LX/7vn;->a:[I

    invoke-virtual {p2}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1275124
    iget-object v0, p0, LX/7vo;->a:LX/6tI;

    invoke-virtual {v0, p1, p2}, LX/6tI;->a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1275125
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031634

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;

    .line 1275126
    new-instance v1, LX/7wE;

    invoke-direct {v1, v0}, LX/7wE;-><init>(Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;)V

    move-object v0, v1

    .line 1275127
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
