.class public final enum LX/8Cz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Cz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Cz;

.field public static final enum LIST_VIEW:LX/8Cz;

.field public static final enum RECYCLER_VIEW:LX/8Cz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1312118
    new-instance v0, LX/8Cz;

    const-string v1, "RECYCLER_VIEW"

    invoke-direct {v0, v1, v2}, LX/8Cz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Cz;->RECYCLER_VIEW:LX/8Cz;

    .line 1312119
    new-instance v0, LX/8Cz;

    const-string v1, "LIST_VIEW"

    invoke-direct {v0, v1, v3}, LX/8Cz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Cz;->LIST_VIEW:LX/8Cz;

    .line 1312120
    const/4 v0, 0x2

    new-array v0, v0, [LX/8Cz;

    sget-object v1, LX/8Cz;->RECYCLER_VIEW:LX/8Cz;

    aput-object v1, v0, v2

    sget-object v1, LX/8Cz;->LIST_VIEW:LX/8Cz;

    aput-object v1, v0, v3

    sput-object v0, LX/8Cz;->$VALUES:[LX/8Cz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1312116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Cz;
    .locals 1

    .prologue
    .line 1312117
    const-class v0, LX/8Cz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Cz;

    return-object v0
.end method

.method public static values()[LX/8Cz;
    .locals 1

    .prologue
    .line 1312115
    sget-object v0, LX/8Cz;->$VALUES:[LX/8Cz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Cz;

    return-object v0
.end method


# virtual methods
.method public final test()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1312114
    const-string v0, "Test"

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1312113
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Custom: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
