.class public final LX/6ur;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

.field public final synthetic b:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

.field public final synthetic c:LX/6uu;


# direct methods
.method public constructor <init>(LX/6uu;Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Lcom/facebook/payments/ui/PrimaryCtaButtonView;)V
    .locals 0

    .prologue
    .line 1156204
    iput-object p1, p0, LX/6ur;->c:LX/6uu;

    iput-object p2, p0, LX/6ur;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    iput-object p3, p0, LX/6ur;->b:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x79d57ae8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1156205
    iget-object v1, p0, LX/6ur;->c:LX/6uu;

    iget-object v2, p0, LX/6ur;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    iget-object v3, p0, LX/6ur;->b:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    .line 1156206
    invoke-virtual {v3}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->a()V

    .line 1156207
    new-instance v5, LX/4Dp;

    invoke-direct {v5}, LX/4Dp;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a()Ljava/lang/String;

    move-result-object v6

    .line 1156208
    const-string v7, "billing_agreement_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156209
    move-object v5, v5

    .line 1156210
    iget-object v6, v2, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->c:LX/6zT;

    invoke-virtual {v6}, LX/6zT;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1156211
    const-string v7, "billing_agreement_type"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156212
    move-object v5, v5

    .line 1156213
    new-instance v6, LX/6us;

    invoke-direct {v6, v1, v2, v3}, LX/6us;-><init>(LX/6uu;Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Lcom/facebook/payments/ui/PrimaryCtaButtonView;)V

    .line 1156214
    iget-object v7, v1, LX/6uu;->b:LX/1Ck;

    const-string p0, "paypal_mutation_key"

    iget-object p1, v1, LX/6uu;->c:LX/70L;

    .line 1156215
    new-instance v1, LX/70G;

    invoke-direct {v1}, LX/70G;-><init>()V

    move-object v1, v1

    .line 1156216
    const-string v2, "input"

    invoke-virtual {v1, v2, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1156217
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1156218
    iget-object v2, p1, LX/70L;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1156219
    new-instance v2, LX/70K;

    invoke-direct {v2, p1}, LX/70K;-><init>(LX/70L;)V

    iget-object v3, p1, LX/70L;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v5, v1

    .line 1156220
    invoke-virtual {v7, p0, v5, v6}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1156221
    const v1, -0x568b123c

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
