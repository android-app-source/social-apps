.class public final LX/8R5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8R4;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudiencePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudiencePickerFragment;)V
    .locals 0

    .prologue
    .line 1343988
    iput-object p1, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/8RD;LX/0Px;LX/8R1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8RD;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;",
            "LX/8R1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1343961
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->i:Lcom/facebook/privacy/model/AudiencePickerInput;

    .line 1343962
    iget-object v1, v0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v1

    .line 1343963
    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1343964
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;->i:Lcom/facebook/privacy/model/AudiencePickerInput;

    .line 1343965
    iget-object v2, v1, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v2

    .line 1343966
    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v1, v1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    .line 1343967
    new-instance v3, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-direct {v3}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;-><init>()V

    .line 1343968
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1343969
    const-string v2, "Type"

    invoke-virtual {p1}, LX/8RD;->ordinal()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1343970
    const-string v5, "FriendLists"

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1343971
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1343972
    :goto_0
    invoke-static {v4, v5, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1343973
    const-string v5, "PreselectedMembers"

    invoke-static {p2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1343974
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1343975
    :goto_1
    invoke-static {v4, v5, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1343976
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1343977
    move-object v1, v3

    .line 1343978
    iput-object v1, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    .line 1343979
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    .line 1343980
    iput-object p3, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->q:LX/8R1;

    .line 1343981
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    .line 1343982
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0605

    iget-object v3, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v3, v3, Lcom/facebook/privacy/selector/AudiencePickerFragment;->h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1343983
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1343984
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->f:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1343985
    return-void

    .line 1343986
    :cond_0
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto :goto_0

    .line 1343987
    :cond_1
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1343937
    sget-object v0, LX/8RD;->FRIENDS_EXCEPT:LX/8RD;

    iget-object v1, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343938
    iget-object v2, v1, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    move-object v1, v2

    .line 1343939
    new-instance v2, LX/8R2;

    invoke-direct {v2, p0}, LX/8R2;-><init>(LX/8R5;)V

    invoke-direct {p0, v0, v1, v2}, LX/8R5;->a(LX/8RD;LX/0Px;LX/8R1;)V

    .line 1343940
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    const v1, 0x7f0812fd

    .line 1343941
    invoke-static {v0, v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a$redex0(Lcom/facebook/privacy/selector/AudiencePickerFragment;I)V

    .line 1343942
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1343955
    sget-object v0, LX/8RD;->SPECIFIC_FRIENDS:LX/8RD;

    iget-object v1, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343956
    iget-object v2, v1, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    move-object v1, v2

    .line 1343957
    new-instance v2, LX/8R3;

    invoke-direct {v2, p0}, LX/8R3;-><init>(LX/8R5;)V

    invoke-direct {p0, v0, v1, v2}, LX/8R5;->a(LX/8RD;LX/0Px;LX/8R1;)V

    .line 1343958
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    const v1, 0x7f0812fe

    .line 1343959
    invoke-static {v0, v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a$redex0(Lcom/facebook/privacy/selector/AudiencePickerFragment;I)V

    .line 1343960
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1343943
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343944
    new-instance v2, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

    invoke-direct {v2}, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;-><init>()V

    .line 1343945
    iput-object v1, v2, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343946
    move-object v1, v2

    .line 1343947
    iput-object v1, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->g:Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

    .line 1343948
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    .line 1343949
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0605

    iget-object v3, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v3, v3, Lcom/facebook/privacy/selector/AudiencePickerFragment;->g:Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1343950
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1343951
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->f:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1343952
    iget-object v0, p0, LX/8R5;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    const v1, 0x7f0812fc

    .line 1343953
    invoke-static {v0, v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a$redex0(Lcom/facebook/privacy/selector/AudiencePickerFragment;I)V

    .line 1343954
    return-void
.end method
