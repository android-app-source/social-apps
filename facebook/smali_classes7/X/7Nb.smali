.class public LX/7Nb;
.super LX/2oy;
.source ""


# instance fields
.field private a:Landroid/os/Handler;

.field public b:I

.field public c:I

.field private d:Ljava/lang/Runnable;

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1200446
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Nb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200447
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200472
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Nb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200473
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1200474
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200475
    const/4 v0, 0x2

    iput v0, p0, LX/7Nb;->e:I

    .line 1200476
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/7Nb;->a:Landroid/os/Handler;

    .line 1200477
    new-instance v0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;-><init>(LX/7Nb;)V

    iput-object v0, p0, LX/7Nb;->d:Ljava/lang/Runnable;

    .line 1200478
    return-void
.end method

.method public static g(LX/7Nb;)V
    .locals 6

    .prologue
    .line 1200462
    const/16 v0, 0x3e8

    .line 1200463
    invoke-static {p0}, LX/7Nb;->getVideoEndTime(LX/7Nb;)I

    move-result v1

    iget v2, p0, LX/7Nb;->b:I

    sub-int/2addr v1, v2

    if-lez v1, :cond_1

    invoke-static {p0}, LX/7Nb;->getVideoEndTime(LX/7Nb;)I

    move-result v1

    iget v2, p0, LX/7Nb;->b:I

    sub-int/2addr v1, v2

    :goto_0
    move v1, v1

    .line 1200464
    if-lez v1, :cond_0

    .line 1200465
    int-to-float v0, v1

    const v1, 0x3e19999a    # 0.15f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1200466
    :cond_0
    iget-object v1, p0, LX/7Nb;->a:Landroid/os/Handler;

    iget-object v2, p0, LX/7Nb;->d:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1200467
    iget-object v1, p0, LX/7Nb;->a:Landroid/os/Handler;

    iget-object v2, p0, LX/7Nb;->d:Ljava/lang/Runnable;

    int-to-long v4, v0

    const v0, -0x6e1064ec

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1200468
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getVideoEndTime(LX/7Nb;)I
    .locals 2

    .prologue
    .line 1200469
    iget v0, p0, LX/7Nb;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1200470
    iget v0, p0, LX/7Nb;->c:I

    .line 1200471
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->k()I

    move-result v0

    goto :goto_0
.end method

.method public static h(LX/7Nb;)V
    .locals 4

    .prologue
    .line 1200459
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1200460
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qc;

    iget v2, p0, LX/7Nb;->b:I

    sget-object v3, LX/04g;->BY_PLAYER:LX/04g;

    invoke-direct {v1, v2, v3}, LX/2qc;-><init>(ILX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1200461
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 1200450
    invoke-super {p0, p1, p2}, LX/2oy;->a(LX/2pa;Z)V

    .line 1200451
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "TrimStartPosition"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1200452
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "TrimStartPosition"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/7Nb;->b:I

    .line 1200453
    :goto_0
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "TrimEndPosition"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1200454
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "TrimEndPosition"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/7Nb;->c:I

    .line 1200455
    :goto_1
    iget-object v0, p0, LX/7Nb;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/7Nb;->d:Ljava/lang/Runnable;

    const v2, 0x61a8553b

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1200456
    return-void

    .line 1200457
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, LX/7Nb;->b:I

    goto :goto_0

    .line 1200458
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, LX/7Nb;->c:I

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1200448
    iget-object v0, p0, LX/7Nb;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/7Nb;->d:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1200449
    return-void
.end method
