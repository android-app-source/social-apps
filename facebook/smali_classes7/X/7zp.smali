.class public final enum LX/7zp;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/1gt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7zp;",
        ">;",
        "LX/1gt;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7zp;

.field public static final enum NETWORK_ERROR:LX/7zp;

.field public static final enum NETWORK_NEXT:LX/7zp;

.field public static final enum NETWORK_NEXT_IS_NULL:LX/7zp;

.field public static final enum NETWORK_NEXT_NOT_IN_USE:LX/7zp;

.field public static final enum NETWORK_SUCCESS:LX/7zp;


# instance fields
.field private mShouldLogClientEvent:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1281418
    new-instance v0, LX/7zp;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v3}, LX/7zp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zp;->NETWORK_ERROR:LX/7zp;

    .line 1281419
    new-instance v0, LX/7zp;

    const-string v1, "NETWORK_SUCCESS"

    invoke-direct {v0, v1, v2, v2}, LX/7zp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7zp;->NETWORK_SUCCESS:LX/7zp;

    .line 1281420
    new-instance v0, LX/7zp;

    const-string v1, "NETWORK_NEXT"

    invoke-direct {v0, v1, v4}, LX/7zp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zp;->NETWORK_NEXT:LX/7zp;

    .line 1281421
    new-instance v0, LX/7zp;

    const-string v1, "NETWORK_NEXT_NOT_IN_USE"

    invoke-direct {v0, v1, v5, v2}, LX/7zp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7zp;->NETWORK_NEXT_NOT_IN_USE:LX/7zp;

    .line 1281422
    new-instance v0, LX/7zp;

    const-string v1, "NETWORK_NEXT_IS_NULL"

    invoke-direct {v0, v1, v6, v2}, LX/7zp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7zp;->NETWORK_NEXT_IS_NULL:LX/7zp;

    .line 1281423
    const/4 v0, 0x5

    new-array v0, v0, [LX/7zp;

    sget-object v1, LX/7zp;->NETWORK_ERROR:LX/7zp;

    aput-object v1, v0, v3

    sget-object v1, LX/7zp;->NETWORK_SUCCESS:LX/7zp;

    aput-object v1, v0, v2

    sget-object v1, LX/7zp;->NETWORK_NEXT:LX/7zp;

    aput-object v1, v0, v4

    sget-object v1, LX/7zp;->NETWORK_NEXT_NOT_IN_USE:LX/7zp;

    aput-object v1, v0, v5

    sget-object v1, LX/7zp;->NETWORK_NEXT_IS_NULL:LX/7zp;

    aput-object v1, v0, v6

    sput-object v0, LX/7zp;->$VALUES:[LX/7zp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1281416
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7zp;-><init>(Ljava/lang/String;IZ)V

    .line 1281417
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 1281413
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1281414
    iput-boolean p3, p0, LX/7zp;->mShouldLogClientEvent:Z

    .line 1281415
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7zp;
    .locals 1

    .prologue
    .line 1281412
    const-class v0, LX/7zp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7zp;

    return-object v0
.end method

.method public static values()[LX/7zp;
    .locals 1

    .prologue
    .line 1281407
    sget-object v0, LX/7zp;->$VALUES:[LX/7zp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7zp;

    return-object v0
.end method


# virtual methods
.method public final getClientEventName()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281409
    iget-boolean v0, p0, LX/7zp;->mShouldLogClientEvent:Z

    if-eqz v0, :cond_0

    .line 1281410
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ar_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/7zp;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1281411
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1281408
    invoke-virtual {p0}, LX/7zp;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
