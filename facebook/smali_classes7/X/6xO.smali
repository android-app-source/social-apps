.class public final enum LX/6xO;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xO;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xO;

.field public static final enum PRICE:LX/6xO;

.field public static final enum PRICE_NO_DECIMALS:LX/6xO;

.field public static final enum TEXT:LX/6xO;

.field public static final enum UNKNOWN:LX/6xO;


# instance fields
.field private inputType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1158765
    new-instance v0, LX/6xO;

    const-string v1, "PRICE"

    const/16 v2, 0x2002

    invoke-direct {v0, v1, v5, v2}, LX/6xO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6xO;->PRICE:LX/6xO;

    .line 1158766
    new-instance v0, LX/6xO;

    const-string v1, "PRICE_NO_DECIMALS"

    invoke-direct {v0, v1, v3, v4}, LX/6xO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6xO;->PRICE_NO_DECIMALS:LX/6xO;

    .line 1158767
    new-instance v0, LX/6xO;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v4, v3}, LX/6xO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6xO;->TEXT:LX/6xO;

    .line 1158768
    new-instance v0, LX/6xO;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6, v3}, LX/6xO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6xO;->UNKNOWN:LX/6xO;

    .line 1158769
    const/4 v0, 0x4

    new-array v0, v0, [LX/6xO;

    sget-object v1, LX/6xO;->PRICE:LX/6xO;

    aput-object v1, v0, v5

    sget-object v1, LX/6xO;->PRICE_NO_DECIMALS:LX/6xO;

    aput-object v1, v0, v3

    sget-object v1, LX/6xO;->TEXT:LX/6xO;

    aput-object v1, v0, v4

    sget-object v1, LX/6xO;->UNKNOWN:LX/6xO;

    aput-object v1, v0, v6

    sput-object v0, LX/6xO;->$VALUES:[LX/6xO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1158770
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1158771
    iput p3, p0, LX/6xO;->inputType:I

    .line 1158772
    return-void
.end method

.method public static of(Ljava/lang/String;)LX/6xO;
    .locals 2

    .prologue
    .line 1158773
    const-string v0, "string"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158774
    sget-object v0, LX/6xO;->TEXT:LX/6xO;

    .line 1158775
    :goto_0
    return-object v0

    .line 1158776
    :cond_0
    const-string v0, "currency"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1158777
    sget-object v0, LX/6xO;->PRICE:LX/6xO;

    goto :goto_0

    .line 1158778
    :cond_1
    invoke-static {}, LX/6xO;->values()[LX/6xO;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/6xO;->UNKNOWN:LX/6xO;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xO;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xO;
    .locals 1

    .prologue
    .line 1158779
    const-class v0, LX/6xO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xO;

    return-object v0
.end method

.method public static values()[LX/6xO;
    .locals 1

    .prologue
    .line 1158780
    sget-object v0, LX/6xO;->$VALUES:[LX/6xO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xO;

    return-object v0
.end method


# virtual methods
.method public final getInputType()I
    .locals 1

    .prologue
    .line 1158781
    iget v0, p0, LX/6xO;->inputType:I

    return v0
.end method

.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1158782
    invoke-virtual {p0}, LX/6xO;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1158783
    invoke-virtual {p0}, LX/6xO;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
