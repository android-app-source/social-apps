.class public final LX/8Hj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/8Hb;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/8Hb;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1321503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321504
    iput-object p1, p0, LX/8Hj;->a:LX/0QB;

    .line 1321505
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/8Hb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1321506
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/8Hj;

    invoke-direct {v2, p0}, LX/8Hj;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1321507
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/8Hj;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1321508
    packed-switch p2, :pswitch_data_0

    .line 1321509
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1321510
    :pswitch_0
    new-instance p2, LX/99t;

    invoke-static {p1}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v0

    check-cast v0, LX/17S;

    invoke-static {p1}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v1

    check-cast v1, LX/17Q;

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lB;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    invoke-direct {p2, v0, v1, v2, p0}, LX/99t;-><init>(LX/17S;LX/17Q;LX/0lB;Landroid/content/res/Resources;)V

    .line 1321511
    move-object v0, p2

    .line 1321512
    :goto_0
    return-object v0

    .line 1321513
    :pswitch_1
    new-instance v2, LX/B3S;

    invoke-static {p1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    const/16 v1, 0x24de

    invoke-static {p1, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/356;->a(LX/0QB;)LX/356;

    move-result-object v1

    check-cast v1, LX/356;

    const/16 p2, 0x13a4

    invoke-static {p1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-direct {v2, v0, p0, v1, p2}, LX/B3S;-><init>(LX/0wM;LX/0Or;LX/356;LX/0Or;)V

    .line 1321514
    move-object v0, v2

    .line 1321515
    goto :goto_0

    .line 1321516
    :pswitch_2
    new-instance v2, LX/Cak;

    invoke-static {p1}, LX/Cbs;->b(LX/0QB;)LX/Cbs;

    move-result-object v3

    check-cast v3, LX/Cbs;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {p1}, LX/9hJ;->b(LX/0QB;)LX/9hJ;

    move-result-object v6

    check-cast v6, LX/9hJ;

    const/16 v7, 0x13a4

    invoke-static {p1, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/Cak;-><init>(LX/Cbs;Lcom/facebook/content/SecureContextHelper;LX/0wM;LX/9hJ;LX/0Or;)V

    .line 1321517
    move-object v0, v2

    .line 1321518
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1321519
    const/4 v0, 0x3

    return v0
.end method
