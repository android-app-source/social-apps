.class public LX/70X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/70S;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/70S",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162384
    return-void
.end method


# virtual methods
.method public final a()LX/6zU;
    .locals 1

    .prologue
    .line 1162385
    sget-object v0, LX/6zU;->PAYPAL_BILLING_AGREEMENT:LX/6zU;

    return-object v0
.end method

.method public final a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/PaymentMethod;
    .locals 3

    .prologue
    .line 1162386
    const-string v0, "paypal_ba"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1162387
    const-string v0, "paypal_ba"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1162388
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "email"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a(Ljava/lang/String;Ljava/lang/String;)LX/6zS;

    move-result-object v1

    const-string v2, "ba_type"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6zT;->forValue(Ljava/lang/String;)LX/6zT;

    move-result-object v0

    .line 1162389
    iput-object v0, v1, LX/6zS;->c:LX/6zT;

    .line 1162390
    move-object v0, v1

    .line 1162391
    const-string v1, "cib_consent_text"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1162392
    iput-object v1, v0, LX/6zS;->d:Ljava/lang/String;

    .line 1162393
    move-object v0, v0

    .line 1162394
    const-string v1, "cib_terms_url"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1162395
    iput-object v1, v0, LX/6zS;->e:Ljava/lang/String;

    .line 1162396
    move-object v0, v0

    .line 1162397
    invoke-virtual {v0}, LX/6zS;->a()Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    move-result-object v0

    return-object v0
.end method
