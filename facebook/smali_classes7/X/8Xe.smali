.class public LX/8Xe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8T7;

.field public final b:Landroid/view/View;

.field public final c:Lcom/facebook/drawingview/DrawingView;

.field private final d:Lcom/facebook/messaging/doodle/ColourPicker;

.field public final e:Lcom/facebook/messaging/doodle/ColourIndicator;

.field private final f:Lcom/facebook/widget/FbImageView;

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/facebook/drawingview/DrawingView;LX/8T7;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/drawingview/DrawingView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1355239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1355240
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Xe;->g:Z

    .line 1355241
    iput-object p1, p0, LX/8Xe;->b:Landroid/view/View;

    .line 1355242
    iput-object p2, p0, LX/8Xe;->c:Lcom/facebook/drawingview/DrawingView;

    .line 1355243
    iput-object p3, p0, LX/8Xe;->a:LX/8T7;

    .line 1355244
    iget-object v0, p0, LX/8Xe;->b:Landroid/view/View;

    const v1, 0x7f0d13f1

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourPicker;

    iput-object v0, p0, LX/8Xe;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 1355245
    iget-object v0, p0, LX/8Xe;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    new-instance v1, LX/8Xb;

    invoke-direct {v1, p0}, LX/8Xb;-><init>(LX/8Xe;)V

    .line 1355246
    iput-object v1, v0, Lcom/facebook/messaging/doodle/ColourPicker;->c:LX/8CF;

    .line 1355247
    iget-object v0, p0, LX/8Xe;->b:Landroid/view/View;

    const v1, 0x7f0d13ef

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    iput-object v0, p0, LX/8Xe;->e:Lcom/facebook/messaging/doodle/ColourIndicator;

    .line 1355248
    iget-object v0, p0, LX/8Xe;->b:Landroid/view/View;

    const v1, 0x7f0d13f0

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/8Xe;->f:Lcom/facebook/widget/FbImageView;

    .line 1355249
    iget-object v0, p0, LX/8Xe;->f:Lcom/facebook/widget/FbImageView;

    new-instance v1, LX/8Xc;

    invoke-direct {v1, p0}, LX/8Xc;-><init>(LX/8Xe;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355250
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1355251
    iget-object v0, p0, LX/8Xe;->c:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 1355252
    if-eqz p1, :cond_0

    .line 1355253
    iget-object v0, p0, LX/8Xe;->a:LX/8T7;

    iget-object v1, p0, LX/8Xe;->f:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, v1, v2}, LX/8T7;->g(Landroid/view/View;LX/8Sl;)V

    .line 1355254
    iget-object v0, p0, LX/8Xe;->a:LX/8T7;

    iget-object v1, p0, LX/8Xe;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v0, v1, v2}, LX/8T7;->g(Landroid/view/View;LX/8Sl;)V

    .line 1355255
    iget-object v0, p0, LX/8Xe;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1355256
    :goto_0
    iput-boolean p1, p0, LX/8Xe;->g:Z

    .line 1355257
    return-void

    .line 1355258
    :cond_0
    iget-object v0, p0, LX/8Xe;->a:LX/8T7;

    iget-object v1, p0, LX/8Xe;->f:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, v1, v2}, LX/8T7;->h(Landroid/view/View;LX/8Sl;)V

    .line 1355259
    iget-object v0, p0, LX/8Xe;->a:LX/8T7;

    iget-object v1, p0, LX/8Xe;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    new-instance v2, LX/8Xd;

    invoke-direct {v2, p0}, LX/8Xd;-><init>(LX/8Xe;)V

    invoke-virtual {v0, v1, v2}, LX/8T7;->h(Landroid/view/View;LX/8Sl;)V

    goto :goto_0
.end method
