.class public final LX/6pw;
.super LX/6nn;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nn",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6px;


# direct methods
.method public constructor <init>(LX/6px;Lcom/facebook/payments/auth/pin/EnterPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1149747
    iput-object p1, p0, LX/6pw;->d:LX/6px;

    iput-object p2, p0, LX/6pw;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iput-object p3, p0, LX/6pw;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object p4, p0, LX/6pw;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/6nn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1149741
    iget-object v0, p0, LX/6pw;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->d()V

    .line 1149742
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1149745
    iget-object v0, p0, LX/6pw;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pw;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/auth/pin/EnterPinFragment;Z)V

    .line 1149746
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1149743
    iget-object v0, p0, LX/6pw;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pw;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iget-object v2, p0, LX/6pw;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    .line 1149744
    return-void
.end method
