.class public LX/8PY;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1341869
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1qM;
    .locals 6

    .prologue
    .line 1341870
    const-class v1, LX/8PY;

    monitor-enter v1

    .line 1341871
    :try_start_0
    sget-object v0, LX/8PY;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1341872
    sput-object v2, LX/8PY;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1341873
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1341874
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1341875
    invoke-static {v0}, LX/3nM;->a(LX/0QB;)LX/3nM;

    move-result-object v3

    check-cast v3, LX/3nM;

    invoke-static {v0}, LX/3nP;->a(LX/0QB;)LX/3nP;

    move-result-object v4

    check-cast v4, LX/3nP;

    invoke-static {v0}, LX/2c3;->a(LX/0QB;)LX/2c3;

    move-result-object v5

    check-cast v5, LX/2c3;

    invoke-static {v0}, LX/8SI;->b(LX/0QB;)LX/8SI;

    move-result-object p0

    check-cast p0, LX/8SI;

    invoke-static {v3, v4, v5, p0}, LX/8PZ;->a(LX/3nM;LX/3nP;LX/2c3;LX/8SI;)LX/1qM;

    move-result-object v3

    move-object v0, v3

    .line 1341876
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1341877
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1341878
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1341879
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1341880
    invoke-static {p0}, LX/3nM;->a(LX/0QB;)LX/3nM;

    move-result-object v0

    check-cast v0, LX/3nM;

    invoke-static {p0}, LX/3nP;->a(LX/0QB;)LX/3nP;

    move-result-object v1

    check-cast v1, LX/3nP;

    invoke-static {p0}, LX/2c3;->a(LX/0QB;)LX/2c3;

    move-result-object v2

    check-cast v2, LX/2c3;

    invoke-static {p0}, LX/8SI;->b(LX/0QB;)LX/8SI;

    move-result-object v3

    check-cast v3, LX/8SI;

    invoke-static {v0, v1, v2, v3}, LX/8PZ;->a(LX/3nM;LX/3nP;LX/2c3;LX/8SI;)LX/1qM;

    move-result-object v0

    return-object v0
.end method
