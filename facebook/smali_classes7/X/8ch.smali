.class public final enum LX/8ch;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8ch;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8ch;

.field public static final enum CLICK:LX/8ch;

.field public static final enum COLLAPSE:LX/8ch;

.field public static final enum EXPAND:LX/8ch;

.field public static final enum LIKED:LX/8ch;

.field public static final enum OPEN_LINK:LX/8ch;

.field public static final enum OPEN_LINK_BY_IMAGE:LX/8ch;

.field public static final enum REACTED:LX/8ch;

.field public static final enum SAVE:LX/8ch;

.field public static final enum SHARE:LX/8ch;

.field public static final enum UNLIKED:LX/8ch;

.field public static final enum UNREACTED:LX/8ch;

.field public static final enum UNSAVE:LX/8ch;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1374746
    new-instance v0, LX/8ch;

    const-string v1, "CLICK"

    const-string v2, "click"

    invoke-direct {v0, v1, v4, v2}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->CLICK:LX/8ch;

    .line 1374747
    new-instance v0, LX/8ch;

    const-string v1, "EXPAND"

    const-string v2, "expand"

    invoke-direct {v0, v1, v5, v2}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->EXPAND:LX/8ch;

    .line 1374748
    new-instance v0, LX/8ch;

    const-string v1, "COLLAPSE"

    const-string v2, "collapse"

    invoke-direct {v0, v1, v6, v2}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->COLLAPSE:LX/8ch;

    .line 1374749
    new-instance v0, LX/8ch;

    const-string v1, "LIKED"

    const-string v2, "liked"

    invoke-direct {v0, v1, v7, v2}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->LIKED:LX/8ch;

    .line 1374750
    new-instance v0, LX/8ch;

    const-string v1, "UNLIKED"

    const-string v2, "unliked"

    invoke-direct {v0, v1, v8, v2}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->UNLIKED:LX/8ch;

    .line 1374751
    new-instance v0, LX/8ch;

    const-string v1, "REACTED"

    const/4 v2, 0x5

    const-string v3, "reacted"

    invoke-direct {v0, v1, v2, v3}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->REACTED:LX/8ch;

    .line 1374752
    new-instance v0, LX/8ch;

    const-string v1, "UNREACTED"

    const/4 v2, 0x6

    const-string v3, "unreacted"

    invoke-direct {v0, v1, v2, v3}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->UNREACTED:LX/8ch;

    .line 1374753
    new-instance v0, LX/8ch;

    const-string v1, "OPEN_LINK"

    const/4 v2, 0x7

    const-string v3, "open_link"

    invoke-direct {v0, v1, v2, v3}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->OPEN_LINK:LX/8ch;

    .line 1374754
    new-instance v0, LX/8ch;

    const-string v1, "OPEN_LINK_BY_IMAGE"

    const/16 v2, 0x8

    const-string v3, "open_link_by_image"

    invoke-direct {v0, v1, v2, v3}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    .line 1374755
    new-instance v0, LX/8ch;

    const-string v1, "SHARE"

    const/16 v2, 0x9

    const-string v3, "share"

    invoke-direct {v0, v1, v2, v3}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->SHARE:LX/8ch;

    .line 1374756
    new-instance v0, LX/8ch;

    const-string v1, "SAVE"

    const/16 v2, 0xa

    const-string v3, "save"

    invoke-direct {v0, v1, v2, v3}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->SAVE:LX/8ch;

    .line 1374757
    new-instance v0, LX/8ch;

    const-string v1, "UNSAVE"

    const/16 v2, 0xb

    const-string v3, "unsave"

    invoke-direct {v0, v1, v2, v3}, LX/8ch;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8ch;->UNSAVE:LX/8ch;

    .line 1374758
    const/16 v0, 0xc

    new-array v0, v0, [LX/8ch;

    sget-object v1, LX/8ch;->CLICK:LX/8ch;

    aput-object v1, v0, v4

    sget-object v1, LX/8ch;->EXPAND:LX/8ch;

    aput-object v1, v0, v5

    sget-object v1, LX/8ch;->COLLAPSE:LX/8ch;

    aput-object v1, v0, v6

    sget-object v1, LX/8ch;->LIKED:LX/8ch;

    aput-object v1, v0, v7

    sget-object v1, LX/8ch;->UNLIKED:LX/8ch;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8ch;->REACTED:LX/8ch;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8ch;->UNREACTED:LX/8ch;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8ch;->OPEN_LINK:LX/8ch;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8ch;->SHARE:LX/8ch;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8ch;->SAVE:LX/8ch;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8ch;->UNSAVE:LX/8ch;

    aput-object v2, v0, v1

    sput-object v0, LX/8ch;->$VALUES:[LX/8ch;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1374759
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1374760
    iput-object p3, p0, LX/8ch;->value:Ljava/lang/String;

    .line 1374761
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8ch;
    .locals 1

    .prologue
    .line 1374762
    const-class v0, LX/8ch;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8ch;

    return-object v0
.end method

.method public static values()[LX/8ch;
    .locals 1

    .prologue
    .line 1374763
    sget-object v0, LX/8ch;->$VALUES:[LX/8ch;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8ch;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1374764
    iget-object v0, p0, LX/8ch;->value:Ljava/lang/String;

    return-object v0
.end method
