.class public LX/7gG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/7gG;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224041
    const-class v0, LX/7gG;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7gG;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1224042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224043
    iput-object p1, p0, LX/7gG;->b:LX/0Zb;

    .line 1224044
    iput-object p2, p0, LX/7gG;->c:LX/03V;

    .line 1224045
    return-void
.end method

.method public static a(LX/0QB;)LX/7gG;
    .locals 5

    .prologue
    .line 1224046
    sget-object v0, LX/7gG;->d:LX/7gG;

    if-nez v0, :cond_1

    .line 1224047
    const-class v1, LX/7gG;

    monitor-enter v1

    .line 1224048
    :try_start_0
    sget-object v0, LX/7gG;->d:LX/7gG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1224049
    if-eqz v2, :cond_0

    .line 1224050
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1224051
    new-instance p0, LX/7gG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/7gG;-><init>(LX/0Zb;LX/03V;)V

    .line 1224052
    move-object v0, p0

    .line 1224053
    sput-object v0, LX/7gG;->d:LX/7gG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1224054
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1224055
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1224056
    :cond_1
    sget-object v0, LX/7gG;->d:LX/7gG;

    return-object v0

    .line 1224057
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1224058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
