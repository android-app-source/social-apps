.class public final LX/7Ym;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Yl;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2wX;Ljava/lang/String;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Ljava/lang/String;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/7Yk;

    invoke-direct {v0, p0, p1, p2}, LX/7Yk;-><init>(LX/7Ym;LX/2wX;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;Ljava/lang/String;Ljava/lang/String;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/7Yg;

    invoke-direct {v0, p0, p1, p2, p3}, LX/7Yg;-><init>(LX/7Ym;LX/2wX;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;Ljava/lang/String;Z)LX/2wg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Ljava/lang/String;",
            "Z)",
            "LX/2wg",
            "<",
            "LX/7Yd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, LX/7Yw;

    invoke-direct {v0}, LX/7Yw;-><init>()V

    iget-object v1, v0, LX/7Yw;->a:Lcom/google/android/gms/cast/LaunchOptions;

    iput-boolean p3, v1, Lcom/google/android/gms/cast/LaunchOptions;->b:Z

    move-object v0, v0

    iget-object v1, v0, LX/7Yw;->a:Lcom/google/android/gms/cast/LaunchOptions;

    move-object v0, v1

    new-instance v1, LX/7Yi;

    invoke-direct {v1, p0, p1, p2, v0}, LX/7Yi;-><init>(LX/7Ym;LX/2wX;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    invoke-virtual {p1, v1}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v1

    move-object v0, v1

    return-object v0
.end method

.method public final a(LX/2wX;)V
    .locals 2

    :try_start_0
    sget-object v0, LX/7Z8;->a:LX/2vn;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vo;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/7Z3;

    invoke-static {v0}, LX/7Z3;->A(LX/7Z3;)LX/7Z6;

    move-result-object v1

    invoke-interface {v1}, LX/7Z6;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/2wX;D)V
    .locals 2

    :try_start_0
    sget-object v0, LX/7Z8;->a:LX/2vn;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vo;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/7Z3;

    invoke-virtual {v0, p2, p3}, LX/7Z3;->a(D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/2wX;Ljava/lang/String;LX/7JG;)V
    .locals 2

    :try_start_0
    sget-object v0, LX/7Z8;->a:LX/2vn;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vo;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/7Z3;

    invoke-virtual {v0, p2, p3}, LX/7Z3;->a(Ljava/lang/String;LX/7JG;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(LX/2wX;)D
    .locals 4

    sget-object v0, LX/7Z8;->a:LX/2vn;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vo;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/7Z3;

    invoke-static {v0}, LX/7Z3;->C(LX/7Z3;)V

    iget-wide v2, v0, LX/7Z3;->p:D

    move-wide v0, v2

    return-wide v0
.end method

.method public final b(LX/2wX;Ljava/lang/String;Ljava/lang/String;)LX/2wg;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/2wg",
            "<",
            "LX/7Yd;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    new-instance v1, LX/7Yj;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, LX/7Yj;-><init>(LX/7Ym;LX/2wX;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/cast/JoinOptions;)V

    invoke-virtual {p1, v1}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v1

    move-object v0, v1

    return-object v0
.end method

.method public final b(LX/2wX;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    sget-object v0, LX/7Z8;->a:LX/2vn;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vo;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/7Z3;

    invoke-virtual {v0, p2}, LX/7Z3;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
