.class public final enum LX/708;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6vZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/708;",
        ">;",
        "LX/6vZ;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/708;

.field public static final enum COUNTRY_SELECTOR:LX/708;

.field public static final enum NEW_PAYMENT_OPTION:LX/708;

.field public static final enum SECURITY_FOOTER:LX/708;

.field public static final enum SELECT_PAYMENT_METHOD:LX/708;

.field public static final enum SINGLE_ROW_DIVIDER:LX/708;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1161843
    new-instance v0, LX/708;

    const-string v1, "COUNTRY_SELECTOR"

    invoke-direct {v0, v1, v2}, LX/708;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/708;->COUNTRY_SELECTOR:LX/708;

    .line 1161844
    new-instance v0, LX/708;

    const-string v1, "SELECT_PAYMENT_METHOD"

    invoke-direct {v0, v1, v3}, LX/708;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    .line 1161845
    new-instance v0, LX/708;

    const-string v1, "SINGLE_ROW_DIVIDER"

    invoke-direct {v0, v1, v4}, LX/708;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/708;->SINGLE_ROW_DIVIDER:LX/708;

    .line 1161846
    new-instance v0, LX/708;

    const-string v1, "NEW_PAYMENT_OPTION"

    invoke-direct {v0, v1, v5}, LX/708;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/708;->NEW_PAYMENT_OPTION:LX/708;

    .line 1161847
    new-instance v0, LX/708;

    const-string v1, "SECURITY_FOOTER"

    invoke-direct {v0, v1, v6}, LX/708;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/708;->SECURITY_FOOTER:LX/708;

    .line 1161848
    const/4 v0, 0x5

    new-array v0, v0, [LX/708;

    sget-object v1, LX/708;->COUNTRY_SELECTOR:LX/708;

    aput-object v1, v0, v2

    sget-object v1, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    aput-object v1, v0, v3

    sget-object v1, LX/708;->SINGLE_ROW_DIVIDER:LX/708;

    aput-object v1, v0, v4

    sget-object v1, LX/708;->NEW_PAYMENT_OPTION:LX/708;

    aput-object v1, v0, v5

    sget-object v1, LX/708;->SECURITY_FOOTER:LX/708;

    aput-object v1, v0, v6

    sput-object v0, LX/708;->$VALUES:[LX/708;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1161849
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/708;
    .locals 1

    .prologue
    .line 1161850
    const-class v0, LX/708;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/708;

    return-object v0
.end method

.method public static values()[LX/708;
    .locals 1

    .prologue
    .line 1161851
    sget-object v0, LX/708;->$VALUES:[LX/708;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/708;

    return-object v0
.end method
