.class public LX/7i0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1226801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226802
    const-string v0, ""

    iput-object v0, p0, LX/7i0;->a:Ljava/lang/String;

    .line 1226803
    const-string v0, ""

    iput-object v0, p0, LX/7i0;->b:Ljava/lang/String;

    .line 1226804
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, LX/7i0;->c:J

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/7i0;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1226805
    if-nez p0, :cond_0

    .line 1226806
    const/4 v0, 0x0

    .line 1226807
    :goto_0
    return-object v0

    .line 1226808
    :cond_0
    const-string v0, "_<<<>>>_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1226809
    array-length v0, v2

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 1226810
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to parse header!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1226811
    :cond_1
    new-instance v1, LX/7i0;

    invoke-direct {v1}, LX/7i0;-><init>()V

    .line 1226812
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, LX/7i0;->c:J

    .line 1226813
    const-string v0, "_EMPTY_PLACEHOLDER_"

    aget-object v3, v2, v6

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_1
    iput-object v0, v1, LX/7i0;->a:Ljava/lang/String;

    .line 1226814
    const-string v0, "_EMPTY_PLACEHOLDER_"

    aget-object v3, v2, v7

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, ""

    :goto_2
    iput-object v0, v1, LX/7i0;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1226815
    goto :goto_0

    .line 1226816
    :cond_2
    aget-object v0, v2, v6

    goto :goto_1

    .line 1226817
    :cond_3
    aget-object v0, v2, v7

    goto :goto_2
.end method
