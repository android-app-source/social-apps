.class public final LX/7AF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1176209
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_b

    .line 1176210
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176211
    :goto_0
    return v1

    .line 1176212
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176213
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1176214
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1176215
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1176216
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1176217
    const-string v11, "attribution_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1176218
    const/4 v10, 0x0

    .line 1176219
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v11, :cond_f

    .line 1176220
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176221
    :goto_2
    move v9, v10

    .line 1176222
    goto :goto_1

    .line 1176223
    :cond_2
    const-string v11, "global_share"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1176224
    invoke-static {p0, p1}, LX/7AB;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1176225
    :cond_3
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1176226
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1176227
    :cond_4
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1176228
    invoke-static {p0, p1}, LX/7A9;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1176229
    :cond_5
    const-string v11, "permalink_node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1176230
    invoke-static {p0, p1}, LX/7AC;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1176231
    :cond_6
    const-string v11, "source_object"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1176232
    invoke-static {p0, p1}, LX/7AE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1176233
    :cond_7
    const-string v11, "subtitle_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1176234
    const/4 v10, 0x0

    .line 1176235
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v11, :cond_13

    .line 1176236
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176237
    :goto_3
    move v3, v10

    .line 1176238
    goto/16 :goto_1

    .line 1176239
    :cond_8
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1176240
    const/4 v10, 0x0

    .line 1176241
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v11, :cond_17

    .line 1176242
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176243
    :goto_4
    move v2, v10

    .line 1176244
    goto/16 :goto_1

    .line 1176245
    :cond_9
    const-string v11, "url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1176246
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1176247
    :cond_a
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1176248
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1176249
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1176250
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1176251
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1176252
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1176253
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1176254
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1176255
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1176256
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1176257
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1

    .line 1176258
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176259
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_e

    .line 1176260
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1176261
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1176262
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_d

    if-eqz v11, :cond_d

    .line 1176263
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1176264
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_5

    .line 1176265
    :cond_e
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1176266
    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1176267
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_2

    :cond_f
    move v9, v10

    goto :goto_5

    .line 1176268
    :cond_10
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176269
    :cond_11
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_12

    .line 1176270
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1176271
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1176272
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_11

    if-eqz v11, :cond_11

    .line 1176273
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 1176274
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_6

    .line 1176275
    :cond_12
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1176276
    invoke-virtual {p1, v10, v3}, LX/186;->b(II)V

    .line 1176277
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_3

    :cond_13
    move v3, v10

    goto :goto_6

    .line 1176278
    :cond_14
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1176279
    :cond_15
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_16

    .line 1176280
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1176281
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1176282
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_15

    if-eqz v11, :cond_15

    .line 1176283
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 1176284
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_7

    .line 1176285
    :cond_16
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1176286
    invoke-virtual {p1, v10, v2}, LX/186;->b(II)V

    .line 1176287
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_4

    :cond_17
    move v2, v10

    goto :goto_7
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1176288
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1176289
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176290
    if-eqz v0, :cond_1

    .line 1176291
    const-string v1, "attribution_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176292
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1176293
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1176294
    if-eqz v1, :cond_0

    .line 1176295
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176296
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1176297
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1176298
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176299
    if-eqz v0, :cond_2

    .line 1176300
    const-string v1, "global_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176301
    invoke-static {p0, v0, p2, p3}, LX/7AB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1176302
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176303
    if-eqz v0, :cond_3

    .line 1176304
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176305
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1176306
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176307
    if-eqz v0, :cond_4

    .line 1176308
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176309
    invoke-static {p0, v0, p2, p3}, LX/7A9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1176310
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176311
    if-eqz v0, :cond_5

    .line 1176312
    const-string v1, "permalink_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176313
    invoke-static {p0, v0, p2}, LX/7AC;->a(LX/15i;ILX/0nX;)V

    .line 1176314
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176315
    if-eqz v0, :cond_6

    .line 1176316
    const-string v1, "source_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176317
    invoke-static {p0, v0, p2, p3}, LX/7AE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1176318
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176319
    if-eqz v0, :cond_8

    .line 1176320
    const-string v1, "subtitle_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176321
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1176322
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1176323
    if-eqz v1, :cond_7

    .line 1176324
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176325
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1176326
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1176327
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176328
    if-eqz v0, :cond_a

    .line 1176329
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176330
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1176331
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1176332
    if-eqz v1, :cond_9

    .line 1176333
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176334
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1176335
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1176336
    :cond_a
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1176337
    if-eqz v0, :cond_b

    .line 1176338
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176339
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1176340
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1176341
    return-void
.end method
