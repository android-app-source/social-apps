.class public final LX/72E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)V
    .locals 0

    .prologue
    .line 1164055
    iput-object p1, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1164056
    iget-boolean v0, p0, LX/72E;->b:Z

    if-eqz v0, :cond_0

    .line 1164057
    :goto_0
    return-void

    .line 1164058
    :cond_0
    iput-boolean v4, p0, LX/72E;->b:Z

    .line 1164059
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1164060
    iput-boolean v3, p0, LX/72E;->b:Z

    .line 1164061
    iget-object v0, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->s:LX/73G;

    iget-object v1, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-static {v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->n(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)LX/6z8;

    move-result-object v1

    invoke-interface {v0, v1}, LX/6wl;->a(LX/6z8;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->s:LX/73G;

    invoke-virtual {v1}, LX/73G;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1164062
    iget-object v0, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->c:LX/73a;

    iget-object v1, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v1}, LX/73a;->a(Landroid/view/View;)V

    .line 1164063
    :goto_1
    iget-object v0, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->d:LX/72M;

    iget-object v1, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/72M;->a(Z)V

    goto :goto_0

    .line 1164064
    :cond_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->s:LX/73G;

    invoke-virtual {v1}, LX/73G;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1164065
    iget-object v0, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0, v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_1

    .line 1164066
    :cond_2
    iget-object v0, p0, LX/72E;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0, v4}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1164067
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1164068
    return-void
.end method
