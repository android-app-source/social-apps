.class public final enum LX/73i;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/73i;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/73i;

.field public static final enum DEFAULT:LX/73i;

.field public static final enum PAYMENTS_WHITE:LX/73i;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1166219
    new-instance v0, LX/73i;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/73i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73i;->DEFAULT:LX/73i;

    .line 1166220
    new-instance v0, LX/73i;

    const-string v1, "PAYMENTS_WHITE"

    invoke-direct {v0, v1, v3}, LX/73i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 1166221
    const/4 v0, 0x2

    new-array v0, v0, [LX/73i;

    sget-object v1, LX/73i;->DEFAULT:LX/73i;

    aput-object v1, v0, v2

    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    aput-object v1, v0, v3

    sput-object v0, LX/73i;->$VALUES:[LX/73i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1166216
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/73i;
    .locals 1

    .prologue
    .line 1166218
    const-class v0, LX/73i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/73i;

    return-object v0
.end method

.method public static values()[LX/73i;
    .locals 1

    .prologue
    .line 1166217
    sget-object v0, LX/73i;->$VALUES:[LX/73i;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/73i;

    return-object v0
.end method
