.class public LX/71R;
.super LX/6E7;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E7;",
        "LX/6vq",
        "<",
        "LX/71Q;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/TextView;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field public d:LX/71Q;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1163345
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1163346
    const-class v0, LX/71R;

    invoke-static {v0, p0}, LX/71R;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1163347
    const v0, 0x7f030f1c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1163348
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/71R;->setOrientation(I)V

    .line 1163349
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/71R;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1163350
    invoke-virtual {p0}, LX/71R;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0545

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1163351
    invoke-virtual {p0}, LX/71R;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b13c4

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1163352
    invoke-virtual {p0, v0, v1, v0, v1}, LX/71R;->setPadding(IIII)V

    .line 1163353
    const v0, 0x7f0d0beb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/71R;->b:Landroid/widget/TextView;

    .line 1163354
    const v0, 0x7f0d0b20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/71R;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1163355
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/71R;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object p0

    check-cast p0, LX/6xb;

    iput-object p0, p1, LX/71R;->a:LX/6xb;

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 4

    .prologue
    .line 1163356
    iget-object v0, p0, LX/71R;->a:LX/6xb;

    iget-object v1, p0, LX/71R;->d:LX/71Q;

    iget-object v1, v1, LX/71Q;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, LX/71R;->d:LX/71Q;

    iget-object v2, v2, LX/71Q;->a:Ljava/lang/String;

    iget-object v3, p0, LX/71R;->d:LX/71Q;

    iget-object v3, v3, LX/71Q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1163357
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1163358
    const-string v1, "extra_user_action"

    iget-object v2, p0, LX/71R;->d:LX/71Q;

    iget-object v2, v2, LX/71Q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163359
    const-string v1, "extra_section_type"

    sget-object v2, LX/71X;->PAYMENTS_PICKER_OPTION:LX/71X;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1163360
    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->USER_ACTION:LX/73S;

    invoke-direct {v1, v2, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1163361
    invoke-virtual {p0, v1}, LX/6E7;->a(LX/73T;)V

    .line 1163362
    return-void
.end method
