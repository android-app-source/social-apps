.class public LX/7Wa;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Xl;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216707
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216708
    iput-object p2, p0, LX/7Wa;->a:LX/0Xl;

    .line 1216709
    new-instance v0, LX/7WZ;

    invoke-direct {v0, p0}, LX/7WZ;-><init>(LX/7Wa;)V

    invoke-virtual {p0, v0}, LX/7Wa;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216710
    const v0, 0x7f080e96

    invoke-virtual {p0, v0}, LX/7Wa;->setTitle(I)V

    .line 1216711
    return-void
.end method

.method public static b(LX/0QB;)LX/7Wa;
    .locals 3

    .prologue
    .line 1216712
    new-instance v2, LX/7Wa;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-direct {v2, v0, v1}, LX/7Wa;-><init>(Landroid/content/Context;LX/0Xl;)V

    .line 1216713
    return-object v2
.end method
