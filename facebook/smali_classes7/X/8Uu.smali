.class public final LX/8Uu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/MessengerGameShareMutationModels$MessengerScoreShareMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:LX/8Uv;

.field public final synthetic c:LX/8Uw;


# direct methods
.method public constructor <init>(LX/8Uw;Ljava/util/Map;LX/8Uv;)V
    .locals 0

    .prologue
    .line 1351611
    iput-object p1, p0, LX/8Uu;->c:LX/8Uw;

    iput-object p2, p0, LX/8Uu;->a:Ljava/util/Map;

    iput-object p3, p0, LX/8Uu;->b:LX/8Uv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1351603
    iget-object v0, p0, LX/8Uu;->c:LX/8Uw;

    iget-object v0, v0, LX/8Uw;->c:LX/8TD;

    sget-object v1, LX/8TE;->MESSENGER_GAME_SCORE_SHARE:LX/8TE;

    const/4 v2, 0x0

    iget-object v3, p0, LX/8Uu;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, p1, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1351604
    iget-object v0, p0, LX/8Uu;->b:LX/8Uv;

    if-eqz v0, :cond_0

    .line 1351605
    iget-object v0, p0, LX/8Uu;->b:LX/8Uv;

    invoke-interface {v0}, LX/8Uv;->b()V

    .line 1351606
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1351607
    iget-object v0, p0, LX/8Uu;->c:LX/8Uw;

    iget-object v0, v0, LX/8Uw;->c:LX/8TD;

    sget-object v1, LX/8TE;->MESSENGER_GAME_SCORE_SHARE:LX/8TE;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, LX/8Uu;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1351608
    iget-object v0, p0, LX/8Uu;->b:LX/8Uv;

    if-eqz v0, :cond_0

    .line 1351609
    iget-object v0, p0, LX/8Uu;->b:LX/8Uv;

    invoke-interface {v0}, LX/8Uv;->a()V

    .line 1351610
    :cond_0
    return-void
.end method
