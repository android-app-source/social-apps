.class public LX/7Tc;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1210845
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1210846
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LX/7Tc;->a:Landroid/view/View;

    .line 1210847
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 1210848
    iget-object v0, p0, LX/7Tc;->a:Landroid/view/View;

    new-instance v1, LX/1a3;

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1210849
    new-instance v0, LX/7Tb;

    iget-object v1, p0, LX/7Tc;->a:Landroid/view/View;

    invoke-direct {v0, v1}, LX/7Tb;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 0

    .prologue
    .line 1210850
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1210851
    const/4 v0, 0x0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1210852
    const/4 v0, 0x1

    return v0
.end method
