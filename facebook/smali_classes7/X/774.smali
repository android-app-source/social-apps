.class public LX/774;
.super LX/2g7;
.source ""


# instance fields
.field private a:LX/0V8;


# direct methods
.method public constructor <init>(LX/0V8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171597
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171598
    iput-object p1, p0, LX/774;->a:LX/0V8;

    .line 1171599
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 6

    .prologue
    .line 1171600
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171601
    iget-object v0, p0, LX/774;->a:LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    iget-object v2, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x400

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0V8;->a(LX/0VA;J)Z

    move-result v0

    return v0
.end method
