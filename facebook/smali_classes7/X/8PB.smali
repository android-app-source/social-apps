.class public final LX/8PB;
.super LX/4hh;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public n:Z

.field public o:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1341333
    invoke-direct {p0}, LX/4hh;-><init>()V

    return-void
.end method

.method private A()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341332
    new-instance v0, LX/8P7;

    invoke-direct {v0, p0}, LX/8P7;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private B()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341331
    new-instance v0, LX/8P8;

    invoke-direct {v0, p0}, LX/8P8;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private C()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341330
    new-instance v0, LX/8P9;

    invoke-direct {v0, p0}, LX/8P9;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private D()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341329
    new-instance v0, LX/8PA;

    invoke-direct {v0, p0}, LX/8PA;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private E()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1341328
    new-instance v0, LX/8Ov;

    invoke-direct {v0, p0}, LX/8Ov;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private F()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341327
    new-instance v0, LX/8Ow;

    invoke-direct {v0, p0}, LX/8Ow;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private H()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341326
    new-instance v0, LX/8Oy;

    invoke-direct {v0, p0}, LX/8Oy;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private J()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1341325
    new-instance v0, LX/8P0;

    invoke-direct {v0, p0}, LX/8P0;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private v()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341324
    new-instance v0, LX/8P2;

    invoke-direct {v0, p0}, LX/8P2;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private w()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1341323
    new-instance v0, LX/8P3;

    invoke-direct {v0, p0}, LX/8P3;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private x()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341309
    new-instance v0, LX/8P4;

    invoke-direct {v0, p0}, LX/8P4;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private y()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341322
    new-instance v0, LX/8P5;

    invoke-direct {v0, p0}, LX/8P5;-><init>(LX/8PB;)V

    return-object v0
.end method

.method private z()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341321
    new-instance v0, LX/8P6;

    invoke-direct {v0, p0}, LX/8P6;-><init>(LX/8PB;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1341320
    const-string v2, "com.facebook.platform.extra.PLACE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->v()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.facebook.platform.extra.FRIENDS"

    invoke-direct {p0}, LX/8PB;->w()LX/4hg;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v3, v1}, LX/4hh;->a(Landroid/content/Intent;Ljava/lang/String;ZLX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.LINK"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->x()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.IMAGE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->y()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.TITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->z()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.SUBTITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->A()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.DESCRIPTION"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->B()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.REF"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->C()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.DATA_FAILURES_FATAL"

    const-class v4, Ljava/lang/Boolean;

    invoke-direct {p0}, LX/8PB;->D()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.facebook.platform.extra.PHOTOS"

    invoke-direct {p0}, LX/8PB;->E()LX/4hg;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v3, v1}, LX/4hh;->a(Landroid/content/Intent;Ljava/lang/String;ZLX/4hg;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v2, "com.facebook.platform.extra.PHOTOS"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->F()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v2, "com.facebook.platform.extra.QUOTE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->H()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "com.facebook.platform.extra.MEDIA"

    const-class v4, Landroid/os/Bundle;

    invoke-direct {p0}, LX/8PB;->J()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->a(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1341313
    const-string v2, "PLACE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->v()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FRIENDS"

    invoke-direct {p0}, LX/8PB;->w()LX/4hg;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v3, v1}, LX/4hh;->a(Landroid/os/Bundle;Ljava/lang/String;ZLX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "link"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->x()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "IMAGE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->y()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "TITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->z()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "SUBTITLE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->A()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "DESCRIPTION"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->B()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "REF"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->C()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "DATA_FAILURES_FATAL"

    const-class v4, Ljava/lang/Boolean;

    invoke-direct {p0}, LX/8PB;->D()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PHOTOS"

    invoke-direct {p0}, LX/8PB;->E()LX/4hg;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v3, v1}, LX/4hh;->a(Landroid/os/Bundle;Ljava/lang/String;ZLX/4hg;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v2, "PHOTOS"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->F()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v2, "VIDEO"

    const-class v4, Ljava/lang/String;

    .line 1341314
    new-instance v0, LX/8Ox;

    invoke-direct {v0, p0}, LX/8Ox;-><init>(LX/8PB;)V

    move-object v5, v0

    .line 1341315
    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "QUOTE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PB;->H()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "HASHTAG"

    const-class v4, Ljava/lang/String;

    .line 1341316
    new-instance v0, LX/8Oz;

    invoke-direct {v0, p0}, LX/8Oz;-><init>(LX/8PB;)V

    move-object v5, v0

    .line 1341317
    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "MEDIA"

    const-class v4, Landroid/os/Bundle;

    invoke-direct {p0}, LX/8PB;->J()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->a(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "IS_NATIVE_INTENT"

    const-class v4, Ljava/lang/Boolean;

    .line 1341318
    new-instance v0, LX/8P1;

    invoke-direct {v0, p0}, LX/8P1;-><init>(LX/8PB;)V

    move-object v5, v0

    .line 1341319
    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1341312
    iget-boolean v0, p0, LX/8PB;->n:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1341311
    iget-object v0, p0, LX/8PB;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8PB;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1341310
    iget-object v0, p0, LX/8PB;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
