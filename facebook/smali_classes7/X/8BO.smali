.class public LX/8BO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/4d1;

.field private final b:LX/7z2;

.field private c:Z

.field public d:LX/7z0;


# direct methods
.method public constructor <init>(LX/7zS;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309160
    new-instance v0, LX/4d1;

    invoke-direct {v0}, LX/4d1;-><init>()V

    iput-object v0, p0, LX/8BO;->a:LX/4d1;

    .line 1309161
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8BO;->c:Z

    .line 1309162
    iget-object v0, p1, LX/7zS;->a:LX/7z2;

    move-object v0, v0

    .line 1309163
    iput-object v0, p0, LX/8BO;->b:LX/7z2;

    .line 1309164
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1309165
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309166
    invoke-virtual {p0}, LX/8BO;->b()V

    .line 1309167
    :cond_0
    iget-boolean v0, p0, LX/8BO;->c:Z

    if-eqz v0, :cond_1

    .line 1309168
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0, p1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1309169
    :cond_1
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1309170
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/8BO;->c:Z

    if-nez v0, :cond_0

    .line 1309171
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8BO;->c:Z

    .line 1309172
    iget-object v0, p0, LX/8BO;->a:LX/4d1;

    invoke-virtual {v0}, LX/4d1;->a()Z

    .line 1309173
    iget-object v0, p0, LX/8BO;->d:LX/7z0;

    if-eqz v0, :cond_0

    .line 1309174
    iget-object v0, p0, LX/8BO;->b:LX/7z2;

    iget-object v1, p0, LX/8BO;->d:LX/7z0;

    invoke-virtual {v0, v1}, LX/7z2;->b(LX/7z0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1309175
    :cond_0
    monitor-exit p0

    return-void

    .line 1309176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
