.class public final LX/8Pc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel;",
        ">;",
        "LX/8QJ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/PrivacyOperationsClient;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V
    .locals 0

    .prologue
    .line 1341912
    iput-object p1, p0, LX/8Pc;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1341913
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1341914
    if-eqz p1, :cond_0

    .line 1341915
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1341916
    if-eqz v0, :cond_0

    .line 1341917
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1341918
    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel;

    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1341919
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1341920
    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel;

    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1341921
    :cond_0
    const/4 v0, 0x0

    .line 1341922
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/8Pc;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v0, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R3;

    .line 1341923
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1341924
    check-cast v1, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$AlbumPrivacyOptionsModel$PrivacyScopeModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/3R3;->a(LX/0Px;Z)LX/8QJ;

    move-result-object v0

    goto :goto_0
.end method
