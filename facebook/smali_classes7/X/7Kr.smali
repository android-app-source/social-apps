.class public LX/7Kr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field private static final b:LX/19o;

.field private static final c:LX/03z;


# instance fields
.field public final d:LX/1C2;

.field public final e:LX/0Aq;

.field public final f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field private final g:LX/0J8;

.field public final h:LX/0J7;

.field private final i:LX/0JY;

.field public final j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

.field public final k:Lcom/facebook/video/analytics/VideoPlayerInfo;

.field private final l:LX/098;

.field public m:Z

.field public n:LX/09M;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1196582
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sput-object v0, LX/7Kr;->a:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1196583
    sput-object v1, LX/7Kr;->b:LX/19o;

    .line 1196584
    sput-object v1, LX/7Kr;->c:LX/03z;

    return-void
.end method

.method public constructor <init>(LX/1C2;LX/0Aq;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/0J8;LX/0J7;LX/0JY;Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoPlayerInfo;)V
    .locals 9

    .prologue
    .line 1196621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196622
    iput-object p1, p0, LX/7Kr;->d:LX/1C2;

    .line 1196623
    iput-object p2, p0, LX/7Kr;->e:LX/0Aq;

    .line 1196624
    iput-object p3, p0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1196625
    iput-object p4, p0, LX/7Kr;->g:LX/0J8;

    .line 1196626
    iput-object p5, p0, LX/7Kr;->h:LX/0J7;

    .line 1196627
    iput-object p6, p0, LX/7Kr;->i:LX/0JY;

    .line 1196628
    move-object/from16 v0, p7

    iput-object v0, p0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1196629
    move-object/from16 v0, p8

    iput-object v0, p0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1196630
    new-instance v1, LX/0J6;

    iget-object v2, p0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, LX/7Kr;->b:LX/19o;

    sget-object v6, LX/7Kr;->c:LX/03z;

    sget-object v7, LX/7Kr;->a:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, LX/0J6;-><init>(ZZZLX/19o;LX/03z;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Z)V

    iput-object v1, p0, LX/7Kr;->l:LX/098;

    .line 1196631
    return-void
.end method

.method private static b(LX/7Kr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1196620
    iget-boolean v0, p0, LX/7Kr;->m:Z

    if-eqz v0, :cond_0

    const-string v0, "new_api_player"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "old_api_psr"

    goto :goto_0
.end method

.method private static b(LX/7Kr;LX/04g;)Z
    .locals 4

    .prologue
    .line 1196608
    iget-object v0, p0, LX/7Kr;->h:LX/0J7;

    iget-object v1, p0, LX/7Kr;->e:LX/0Aq;

    .line 1196609
    iget v2, v1, LX/0Aq;->b:I

    move v1, v2

    .line 1196610
    iget v2, v0, LX/0J7;->b:I

    if-ne v2, v1, :cond_1

    iget v2, v0, LX/0J7;->c:I

    iget v3, v0, LX/0J7;->b:I

    if-eq v2, v3, :cond_1

    .line 1196611
    sget-object v2, LX/04g;->BY_DIALOG:LX/04g;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/04g;->BY_FLYOUT:LX/04g;

    if-ne p1, v2, :cond_5

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1196612
    if-eqz v2, :cond_4

    .line 1196613
    :cond_1
    const/4 v2, 0x1

    .line 1196614
    :goto_1
    move v0, v2

    .line 1196615
    if-nez v0, :cond_2

    iget-object v0, p0, LX/7Kr;->h:LX/0J7;

    .line 1196616
    iget-boolean v1, v0, LX/0J7;->e:Z

    move v0, v1

    .line 1196617
    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 1196618
    :goto_2
    return v0

    .line 1196619
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static c(LX/7Kr;)LX/09M;
    .locals 1

    .prologue
    .line 1196607
    iget-boolean v0, p0, LX/7Kr;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Kr;->n:LX/09M;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1196592
    iget-object v0, p0, LX/7Kr;->d:LX/1C2;

    iget-object v1, p0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1196593
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a:LX/162;

    move-object v1, v2

    .line 1196594
    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    sget-object v3, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v4, p0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1196595
    iget-object v5, v4, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1196596
    iget-object v5, p0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1196597
    iget-object v6, v5, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v5, v6

    .line 1196598
    iget-object v6, p0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1196599
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v6, v7

    .line 1196600
    iget-object v6, v6, LX/04g;->value:Ljava/lang/String;

    iget-object v7, p0, LX/7Kr;->e:LX/0Aq;

    .line 1196601
    iget v8, v7, LX/0Aq;->c:I

    move v7, v8

    .line 1196602
    iget-object v8, p0, LX/7Kr;->e:LX/0Aq;

    .line 1196603
    iget v9, v8, LX/0Aq;->b:I

    move v8, v9

    .line 1196604
    iget-object v9, p0, LX/7Kr;->l:LX/098;

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1196605
    iget-object v0, p0, LX/7Kr;->h:LX/0J7;

    invoke-virtual {v0}, LX/0J7;->a()V

    .line 1196606
    return-void
.end method

.method public final a(I)V
    .locals 16

    .prologue
    .line 1196585
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, LX/0Aq;->c(I)V

    .line 1196586
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->c()V

    .line 1196587
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v7}, LX/0Aq;->b()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v8}, LX/0Aq;->a()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v9}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v10}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v11}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v11

    iget-object v11, v11, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/7Kr;->c(LX/7Kr;)LX/09M;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7Kr;->i:LX/0JY;

    invoke-virtual {v15}, LX/0JY;->a()LX/097;

    move-result-object v15

    iget-object v15, v15, LX/097;->value:Ljava/lang/String;

    invoke-virtual/range {v1 .. v15}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;IIILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 1196588
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 1196589
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Aq;->e(I)V

    .line 1196590
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->h:LX/0J7;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0J7;->a(I)V

    .line 1196591
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 17

    .prologue
    .line 1196632
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/7Kr;->m:Z

    if-nez v1, :cond_0

    .line 1196633
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->c()V

    .line 1196634
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v7}, LX/0Aq;->b()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->i:LX/0JY;

    invoke-virtual {v8}, LX/0JY;->a()LX/097;

    move-result-object v8

    iget-object v8, v8, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v9}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v10}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7Kr;->g:LX/0J8;

    invoke-virtual {v11}, LX/0J8;->b()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v12}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v12

    iget-object v12, v12, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v14}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c()LX/04H;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7Kr;->n:LX/09M;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v16, v0

    invoke-virtual/range {v1 .. v16}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    .line 1196635
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 1196636
    :cond_0
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 17

    .prologue
    .line 1196525
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v3}, LX/09M;->c()V

    .line 1196526
    invoke-static/range {p0 .. p1}, LX/7Kr;->b(LX/7Kr;LX/04g;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1196527
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v4}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v5}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v5

    sget-object v6, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v6, v6, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->i:LX/0JY;

    invoke-virtual {v8}, LX/0JY;->a()LX/097;

    move-result-object v8

    iget-object v9, v8, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v8}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v8}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->g:LX/0J8;

    invoke-virtual {v8}, LX/0J8;->b()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v8}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v8

    iget-object v13, v8, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/7Kr;->c(LX/7Kr;)LX/09M;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v16, v0

    move/from16 v8, p2

    invoke-virtual/range {v3 .. v16}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;)LX/1C2;

    .line 1196528
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v3}, LX/09M;->a()V

    .line 1196529
    return-void

    .line 1196530
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3}, LX/7Kr;->a(LX/04g;IZ)V

    goto :goto_0
.end method

.method public final a(LX/04g;ILcom/facebook/video/engine/VideoPlayerParams;)V
    .locals 16

    .prologue
    .line 1196531
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->b()V

    .line 1196532
    invoke-static/range {p0 .. p1}, LX/7Kr;->b(LX/7Kr;LX/04g;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1196533
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    .line 1196534
    if-eqz p3, :cond_0

    .line 1196535
    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2oH;

    .line 1196536
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/2oH;->a(Z)LX/2oH;

    .line 1196537
    invoke-virtual {v1}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v14

    .line 1196538
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Kr;->i:LX/0JY;

    invoke-virtual {v5}, LX/0JY;->a()LX/097;

    move-result-object v5

    iget-object v6, v5, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v5}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v5}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Kr;->g:LX/0J8;

    invoke-virtual {v5}, LX/0J8;->b()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v5}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v5

    iget-object v10, v5, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v5}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c()LX/04H;

    move-result-object v12

    iget-object v13, v14, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    const/4 v15, 0x0

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v15}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;Ljava/lang/String;LX/098;Z)LX/1C2;

    .line 1196539
    :cond_1
    return-void
.end method

.method public final a(LX/04g;IZ)V
    .locals 17

    .prologue
    .line 1196540
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v1}, LX/0Aq;->a()I

    move-result v1

    move/from16 v0, p2

    if-ge v0, v1, :cond_0

    .line 1196541
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v1}, LX/0Aq;->a()I

    move-result p2

    .line 1196542
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    move/from16 v0, p2

    invoke-virtual {v1, v0}, LX/0Aq;->c(I)V

    .line 1196543
    if-eqz p3, :cond_1

    .line 1196544
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v8}, LX/0Aq;->b()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v9}, LX/0Aq;->a()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v10}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v11}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v12}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v12

    iget-object v12, v12, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/7Kr;->c(LX/7Kr;)LX/09M;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Kr;->i:LX/0JY;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, LX/0JY;->a()LX/097;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v1 .. v16}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 1196545
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 1196546
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v2}, LX/0Aq;->b()I

    move-result v2

    invoke-virtual {v1, v2}, LX/0Aq;->d(I)V

    .line 1196547
    return-void

    .line 1196548
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v8}, LX/0Aq;->b()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v9}, LX/0Aq;->a()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v10}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v11}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v12}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v12

    iget-object v12, v12, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/7Kr;->c(LX/7Kr;)LX/09M;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Kr;->i:LX/0JY;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, LX/0JY;->a()LX/097;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    goto :goto_0
.end method

.method public final a(LX/04g;ZZI)V
    .locals 17

    .prologue
    .line 1196549
    if-nez p2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->h:LX/0J7;

    invoke-virtual {v1}, LX/0J7;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p3, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 1196550
    :goto_0
    if-eqz v1, :cond_2

    .line 1196551
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v1}, LX/0Aq;->a()I

    move-result v1

    move/from16 v0, p4

    if-ge v0, v1, :cond_1

    .line 1196552
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v1}, LX/0Aq;->a()I

    move-result p4

    .line 1196553
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    move/from16 v0, p4

    invoke-virtual {v1, v0}, LX/0Aq;->c(I)V

    .line 1196554
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v8}, LX/0Aq;->b()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v9}, LX/0Aq;->a()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v10}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v11}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v12}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v12

    iget-object v12, v12, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/7Kr;->c(LX/7Kr;)LX/09M;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Kr;->i:LX/0JY;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, LX/0JY;->a()LX/097;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 1196555
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 1196556
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->e:LX/0Aq;

    move/from16 v0, p4

    invoke-virtual {v1, v0}, LX/0Aq;->d(I)V

    .line 1196557
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->h:LX/0J7;

    invoke-virtual {v1}, LX/0J7;->a()V

    .line 1196558
    :cond_2
    return-void

    .line 1196559
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final a(Landroid/net/Uri;II)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 1196560
    const-string v0, "FullScreen VideoView error = %d %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1196561
    iget-object v0, p0, LX/7Kr;->d:LX/1C2;

    iget-object v2, p0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1196562
    iget-object v3, v2, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    move-object v2, v3

    .line 1196563
    iget-object v3, p0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1196564
    iget-object v4, v3, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1196565
    iget-object v4, p0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1196566
    iget-object v5, v4, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v4, v5

    .line 1196567
    iget-object v5, v4, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1196568
    iget-object v6, v4, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v6

    .line 1196569
    invoke-static {p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/7Kr;->l:LX/098;

    move-object v4, p1

    move-object v10, v9

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1196570
    return-void
.end method

.method public final b(LX/04g;I)V
    .locals 17

    .prologue
    .line 1196571
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->c()V

    .line 1196572
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->h:LX/0J7;

    invoke-virtual {v1}, LX/0J7;->b()V

    .line 1196573
    invoke-static/range {p0 .. p1}, LX/7Kr;->b(LX/7Kr;LX/04g;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1196574
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->i:LX/0JY;

    invoke-virtual {v7}, LX/0JY;->a()LX/097;

    move-result-object v7

    iget-object v8, v7, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->g:LX/0J8;

    invoke-virtual {v7}, LX/0J8;->b()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v7

    iget-object v12, v7, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c()LX/04H;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/7Kr;->c(LX/7Kr;)LX/09M;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v16, v0

    move/from16 v7, p2

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    .line 1196575
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->n:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 1196576
    :cond_0
    return-void
.end method

.method public final c(LX/04g;I)V
    .locals 17

    .prologue
    .line 1196577
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/7Kr;->m:Z

    if-eqz v1, :cond_0

    .line 1196578
    :goto_0
    return-void

    .line 1196579
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->h:LX/0J7;

    invoke-virtual {v1}, LX/0J7;->b()V

    .line 1196580
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->d:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a()LX/162;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v3}, Lcom/facebook/video/analytics/VideoPlayerInfo;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->i:LX/0JY;

    invoke-virtual {v7}, LX/0JY;->a()LX/097;

    move-result-object v7

    iget-object v8, v7, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoPlayerInfo;->a()LX/04D;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->g:LX/0J8;

    invoke-virtual {v7}, LX/0J8;->b()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b()LX/04g;

    move-result-object v7

    iget-object v12, v7, LX/04g;->value:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/7Kr;->b(LX/7Kr;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v7}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c()LX/04H;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Kr;->l:LX/098;

    move-object/from16 v16, v0

    move/from16 v7, p2

    invoke-virtual/range {v1 .. v16}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    .line 1196581
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Kr;->h:LX/0J7;

    invoke-virtual {v1}, LX/0J7;->a()V

    goto :goto_0
.end method
