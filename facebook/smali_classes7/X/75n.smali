.class public LX/75n;
.super LX/0te;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/os/Messenger;

.field public c:LX/0aG;

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1169971
    const-class v0, LX/75n;

    sput-object v0, LX/75n;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1169989
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 1169990
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, LX/75m;

    invoke-direct {v1, p0}, LX/75m;-><init>(LX/75n;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, LX/75n;->a:Landroid/os/Messenger;

    return-void
.end method

.method public static a(Landroid/os/Message;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1169983
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1169984
    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1169985
    :try_start_0
    iget-object v1, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1169986
    :goto_0
    return-void

    .line 1169987
    :catch_0
    move-exception v0

    .line 1169988
    sget-object v1, LX/75n;->b:Ljava/lang/Class;

    const-string v2, "sending reply failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Message;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1169979
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1169980
    const-string v1, "error"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169981
    invoke-static {p0, v0}, LX/75n;->a(Landroid/os/Message;Landroid/os/Bundle;)V

    .line 1169982
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/75n;

    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    const/16 v2, 0x19e

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-virtual {p0, v0, v2, v1}, LX/75n;->a(LX/0aG;LX/0Or;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0aG;LX/0Or;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169975
    iput-object p1, p0, LX/75n;->c:LX/0aG;

    .line 1169976
    iput-object p2, p0, LX/75n;->d:LX/0Or;

    .line 1169977
    iput-object p3, p0, LX/75n;->e:Ljava/util/concurrent/ExecutorService;

    .line 1169978
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1169974
    iget-object v0, p0, LX/75n;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x6e3481eb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1169972
    invoke-static {p0, p0}, LX/75n;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1169973
    const/16 v1, 0x25

    const v2, -0x99f730b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
