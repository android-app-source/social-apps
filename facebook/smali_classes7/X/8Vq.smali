.class public final LX/8Vq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Vp;


# instance fields
.field public final synthetic a:LX/8Vt;


# direct methods
.method public constructor <init>(LX/8Vt;)V
    .locals 0

    .prologue
    .line 1353406
    iput-object p1, p0, LX/8Vq;->a:LX/8Vt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/8Vb;I)V
    .locals 3

    .prologue
    .line 1353407
    iget-object v0, p0, LX/8Vq;->a:LX/8Vt;

    iget-object v0, v0, LX/8Vt;->e:LX/8Vx;

    if-eqz v0, :cond_0

    .line 1353408
    iget-object v0, p0, LX/8Vq;->a:LX/8Vt;

    invoke-virtual {v0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1353409
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 1353410
    sget-object v1, LX/8TX;->SUGGESTED_MATCH:LX/8TX;

    .line 1353411
    add-int/lit8 v2, p2, -0x1

    iget-object v0, p0, LX/8Vq;->a:LX/8Vt;

    iget-object v0, v0, LX/8Vt;->c:LX/0Px;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    sub-int v0, v2, v0

    .line 1353412
    :goto_1
    iget-object v2, p0, LX/8Vq;->a:LX/8Vt;

    iget-object v2, v2, LX/8Vt;->e:LX/8Vx;

    .line 1353413
    iget-object p0, v2, LX/8Vx;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    iget-object p0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->e:LX/8VT;

    if-eqz p0, :cond_0

    .line 1353414
    iget-object p0, v2, LX/8Vx;->a:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    iget-object p0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->e:LX/8VT;

    invoke-interface {p0, p1, v1, v0}, LX/8VT;->a(LX/8Vb;LX/8TX;I)V

    .line 1353415
    :cond_0
    return-void

    .line 1353416
    :cond_1
    iget-object v0, p0, LX/8Vq;->a:LX/8Vt;

    iget-object v0, v0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1353417
    :cond_2
    sget-object v1, LX/8TX;->EXISTING_MATCH:LX/8TX;

    .line 1353418
    add-int/lit8 v0, p2, -0x1

    goto :goto_1
.end method
