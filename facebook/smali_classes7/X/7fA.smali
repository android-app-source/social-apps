.class public LX/7fA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1220860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220861
    return-void
.end method

.method public static a()I
    .locals 4

    .prologue
    .line 1220837
    const/4 v0, 0x0

    .line 1220838
    invoke-static {}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->isH264HwSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1220839
    const/4 v0, 0x1

    .line 1220840
    :cond_0
    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->d:Ljava/util/Set;

    const-string v2, "video/avc"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "video/avc"

    sget-object v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->v:[LX/7f8;

    sget-object v3, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->y:[I

    invoke-static {v1, v2, v3}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1220841
    if-eqz v1, :cond_1

    .line 1220842
    or-int/lit8 v0, v0, 0x2

    .line 1220843
    :cond_1
    invoke-static {}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->isVp8HwSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1220844
    or-int/lit8 v0, v0, 0x4

    .line 1220845
    :cond_2
    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->d:Ljava/util/Set;

    const-string v2, "video/x-vnd.on2.vp8"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "video/x-vnd.on2.vp8"

    sget-object v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->p:[LX/7f8;

    sget-object v3, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->y:[I

    invoke-static {v1, v2, v3}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v1

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1220846
    if-eqz v1, :cond_3

    .line 1220847
    or-int/lit8 v0, v0, 0x8

    .line 1220848
    :cond_3
    invoke-static {}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->isVp9HwSupported()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1220849
    or-int/lit8 v0, v0, 0x10

    .line 1220850
    :cond_4
    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->d:Ljava/util/Set;

    const-string v2, "video/x-vnd.on2.vp9"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "video/x-vnd.on2.vp9"

    sget-object v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->s:[LX/7f8;

    sget-object v3, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->y:[I

    invoke-static {v1, v2, v3}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v1

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 1220851
    if-eqz v1, :cond_5

    .line 1220852
    or-int/lit8 v0, v0, 0x20

    .line 1220853
    :cond_5
    invoke-static {}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->isH264HwSupported()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1220854
    or-int/lit8 v0, v0, 0x40

    .line 1220855
    :cond_6
    invoke-static {}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->isVp8HwSupported()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1220856
    or-int/lit16 v0, v0, 0x80

    .line 1220857
    :cond_7
    invoke-static {}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->isVp9HwSupported()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1220858
    or-int/lit16 v0, v0, 0x100

    .line 1220859
    :cond_8
    return v0

    :cond_9
    const/4 v1, 0x0

    goto :goto_0

    :cond_a
    const/4 v1, 0x0

    goto :goto_1

    :cond_b
    const/4 v1, 0x0

    goto :goto_2
.end method
