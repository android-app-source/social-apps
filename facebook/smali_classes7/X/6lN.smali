.class public LX/6lN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/11T;

.field private final c:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/11T;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1142712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142713
    iput-object p1, p0, LX/6lN;->a:Landroid/content/Context;

    .line 1142714
    iput-object p2, p0, LX/6lN;->b:LX/11T;

    .line 1142715
    iput-object p3, p0, LX/6lN;->c:LX/0SG;

    .line 1142716
    return-void
.end method

.method public static a(LX/0QB;)LX/6lN;
    .locals 6

    .prologue
    .line 1142701
    const-class v1, LX/6lN;

    monitor-enter v1

    .line 1142702
    :try_start_0
    sget-object v0, LX/6lN;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1142703
    sput-object v2, LX/6lN;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1142704
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142705
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1142706
    new-instance p0, LX/6lN;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v4

    check-cast v4, LX/11T;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/6lN;-><init>(Landroid/content/Context;LX/11T;LX/0SG;)V

    .line 1142707
    move-object v0, p0

    .line 1142708
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1142709
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6lN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1142710
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1142711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static c(J)Z
    .locals 8

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v0, 0x1

    .line 1142697
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1142698
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1142699
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1142700
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, 0x3c

    .line 1142676
    iget-object v0, p0, LX/6lN;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, p1

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    div-long/2addr v0, v4

    div-long/2addr v0, v4

    long-to-int v0, v0

    .line 1142677
    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    .line 1142678
    iget-object v0, p0, LX/6lN;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1142679
    :goto_0
    return-object v0

    .line 1142680
    :cond_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 1142681
    div-int/lit8 v0, v0, 0x18

    .line 1142682
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 1142683
    iget-object v0, p0, LX/6lN;->b:LX/11T;

    invoke-virtual {v0}, LX/11T;->d()Ljava/text/SimpleDateFormat;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1142684
    :cond_1
    const/16 v2, 0xb4

    if-ge v0, v2, :cond_2

    .line 1142685
    iget-object v0, p0, LX/6lN;->b:LX/11T;

    invoke-virtual {v0}, LX/11T;->g()Ljava/text/SimpleDateFormat;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1142686
    :cond_2
    iget-object v0, p0, LX/6lN;->b:LX/11T;

    invoke-virtual {v0}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(J)Ljava/lang/String;
    .locals 13

    .prologue
    const-wide/16 v10, 0x3c

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1142687
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 1142688
    invoke-static {p1, p2}, LX/6lN;->c(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1142689
    iget-object v1, p0, LX/6lN;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1142690
    :goto_0
    return-object v0

    .line 1142691
    :cond_0
    iget-object v1, p0, LX/6lN;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    div-long/2addr v2, v10

    div-long/2addr v2, v10

    long-to-int v1, v2

    div-int/lit8 v1, v1, 0x18

    .line 1142692
    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    .line 1142693
    iget-object v1, p0, LX/6lN;->a:Landroid/content/Context;

    const v2, 0x7f0818c2

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, LX/6lN;->b:LX/11T;

    invoke-virtual {v4}, LX/11T;->d()Ljava/text/SimpleDateFormat;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, LX/6lN;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1142694
    :cond_1
    const/16 v2, 0xb4

    if-ge v1, v2, :cond_2

    .line 1142695
    iget-object v1, p0, LX/6lN;->a:Landroid/content/Context;

    const v2, 0x7f0818c2

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, LX/6lN;->b:LX/11T;

    invoke-virtual {v4}, LX/11T;->g()Ljava/text/SimpleDateFormat;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, LX/6lN;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1142696
    :cond_2
    iget-object v1, p0, LX/6lN;->a:Landroid/content/Context;

    const v2, 0x7f0818c2

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, LX/6lN;->b:LX/11T;

    invoke-virtual {v4}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, LX/6lN;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
