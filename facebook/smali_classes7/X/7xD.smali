.class public LX/7xD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1277151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277152
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1277153
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1277154
    :cond_1
    const/4 v0, 0x0

    :goto_1
    move-object v0, v0

    .line 1277155
    if-eqz v0, :cond_2

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1277156
    :sswitch_0
    const-string v1, "email"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1277157
    :pswitch_0
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    goto :goto_1

    .line 1277158
    :pswitch_1
    sget-object v0, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x5c24b9c -> :sswitch_0
        0x65b3d6e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1277159
    iget v0, p1, LX/7ws;->a:I

    move v0, v0

    .line 1277160
    iget-object v1, p1, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v1, v1

    .line 1277161
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v1

    .line 1277162
    iget-object v2, p1, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1277163
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;

    move-result-object v0

    .line 1277164
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1277165
    :cond_0
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1277166
    goto :goto_0
.end method

.method public static d(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Z
    .locals 3

    .prologue
    .line 1277167
    iget v0, p1, LX/7ws;->a:I

    move v0, v0

    .line 1277168
    iget-object v1, p1, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v1, v1

    .line 1277169
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v1

    .line 1277170
    iget-object v2, p1, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1277171
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;

    move-result-object v0

    .line 1277172
    if-nez v0, :cond_0

    .line 1277173
    const/4 v0, 0x1

    .line 1277174
    :goto_0
    return v0

    .line 1277175
    :cond_0
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    move-object v1, v1

    .line 1277176
    sget-object v2, LX/7wx;->COMPOUND_MAP:LX/7wx;

    if-eq v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/events/tickets/modal/model/FieldItem;->g()Z

    move-result v0

    goto :goto_0

    .line 1277177
    :cond_1
    iget-object v1, p1, LX/7ws;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1277178
    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/modal/model/FieldItem;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1277179
    invoke-static {p0, p1}, LX/7xD;->d(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277180
    const-string v0, "empty"

    .line 1277181
    :goto_0
    return-object v0

    .line 1277182
    :cond_0
    iget-object v0, p1, LX/7ws;->d:Ljava/lang/String;

    move-object v1, v0

    .line 1277183
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 1277184
    const-string v0, "empty"

    goto :goto_0

    .line 1277185
    :sswitch_0
    const-string v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "phone"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    .line 1277186
    :pswitch_0
    const-string v0, "email_format"

    goto :goto_0

    .line 1277187
    :pswitch_1
    const-string v0, "phone_format"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5c24b9c -> :sswitch_0
        0x65b3d6e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static f(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Z
    .locals 3

    .prologue
    .line 1277188
    iget v0, p1, LX/7ws;->a:I

    move v0, v0

    .line 1277189
    iget-object v1, p1, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v1, v1

    .line 1277190
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v1

    .line 1277191
    iget-object v2, p1, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1277192
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;

    move-result-object v0

    .line 1277193
    if-eqz v0, :cond_0

    .line 1277194
    iget-object v1, p1, LX/7ws;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1277195
    iget-object v2, p1, LX/7ws;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1277196
    invoke-virtual {v0, v2}, Lcom/facebook/events/tickets/modal/model/FieldItem;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/7xD;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
