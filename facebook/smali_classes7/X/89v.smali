.class public final enum LX/89v;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89v;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89v;

.field public static final enum BOOKMARKS:LX/89v;

.field public static final enum CCU_INTERSTITIAL_NUX:LX/89v;

.field public static final enum CONTINUOUS_SYNC:LX/89v;

.field public static final enum EVENTS_EXTENDED_INVITE:LX/89v;

.field public static final enum FRIENDS_CENTER:LX/89v;

.field public static final enum FRIENDS_TAB:LX/89v;

.field public static final enum FRIEND_BROWSER:LX/89v;

.field public static final enum FRIEND_REQUEST_TAB:LX/89v;

.field public static final enum INTERSTITIAL:LX/89v;

.field public static final enum IORG_INCENTIVE_INVITE:LX/89v;

.field public static final enum NEWS_FEED_FIND_FRIENDS:LX/89v;

.field public static final enum NEWS_FEED_MEGAPHONE:LX/89v;

.field public static final enum NEWS_FEED_PYMI:LX/89v;

.field public static final enum NEWS_FEED_PYMK:LX/89v;

.field public static final enum NEW_ACCOUNT_NUX:LX/89v;

.field public static final enum SEARCH_BOX:LX/89v;

.field public static final enum UNKNOWN:LX/89v;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1305431
    new-instance v0, LX/89v;

    const-string v1, "NEWS_FEED_FIND_FRIENDS"

    const-string v2, "NEWS_FEED_FIND_FRIENDS"

    invoke-direct {v0, v1, v4, v2}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->NEWS_FEED_FIND_FRIENDS:LX/89v;

    .line 1305432
    new-instance v0, LX/89v;

    const-string v1, "NEWS_FEED_MEGAPHONE"

    const-string v2, "NEWS_FEED_MEGAPHONE"

    invoke-direct {v0, v1, v5, v2}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->NEWS_FEED_MEGAPHONE:LX/89v;

    .line 1305433
    new-instance v0, LX/89v;

    const-string v1, "NEWS_FEED_PYMI"

    const-string v2, "NEWS_FEED_PYMI"

    invoke-direct {v0, v1, v6, v2}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->NEWS_FEED_PYMI:LX/89v;

    .line 1305434
    new-instance v0, LX/89v;

    const-string v1, "NEWS_FEED_PYMK"

    const-string v2, "NEWS_FEED_PYMK"

    invoke-direct {v0, v1, v7, v2}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->NEWS_FEED_PYMK:LX/89v;

    .line 1305435
    new-instance v0, LX/89v;

    const-string v1, "FRIENDS_TAB"

    const-string v2, "FRIENDS_TAB"

    invoke-direct {v0, v1, v8, v2}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->FRIENDS_TAB:LX/89v;

    .line 1305436
    new-instance v0, LX/89v;

    const-string v1, "FRIEND_BROWSER"

    const/4 v2, 0x5

    const-string v3, "FRIEND_BROWSER"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->FRIEND_BROWSER:LX/89v;

    .line 1305437
    new-instance v0, LX/89v;

    const-string v1, "NEW_ACCOUNT_NUX"

    const/4 v2, 0x6

    const-string v3, "NEW_ACCOUNT_NUX"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    .line 1305438
    new-instance v0, LX/89v;

    const-string v1, "INTERSTITIAL"

    const/4 v2, 0x7

    const-string v3, "INTERSTITIAL"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->INTERSTITIAL:LX/89v;

    .line 1305439
    new-instance v0, LX/89v;

    const-string v1, "BOOKMARKS"

    const/16 v2, 0x8

    const-string v3, "BOOKMARKS"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->BOOKMARKS:LX/89v;

    .line 1305440
    new-instance v0, LX/89v;

    const-string v1, "SEARCH_BOX"

    const/16 v2, 0x9

    const-string v3, "SEARCH_BOX"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->SEARCH_BOX:LX/89v;

    .line 1305441
    new-instance v0, LX/89v;

    const-string v1, "FRIEND_REQUEST_TAB"

    const/16 v2, 0xa

    const-string v3, "FRIEND_REQUEST_TAB"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->FRIEND_REQUEST_TAB:LX/89v;

    .line 1305442
    new-instance v0, LX/89v;

    const-string v1, "FRIENDS_CENTER"

    const/16 v2, 0xb

    const-string v3, "FRIENDS_CENTER"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->FRIENDS_CENTER:LX/89v;

    .line 1305443
    new-instance v0, LX/89v;

    const-string v1, "CONTINUOUS_SYNC"

    const/16 v2, 0xc

    const-string v3, "CONTINUOUS_SYNC"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->CONTINUOUS_SYNC:LX/89v;

    .line 1305444
    new-instance v0, LX/89v;

    const-string v1, "IORG_INCENTIVE_INVITE"

    const/16 v2, 0xd

    const-string v3, "IORG_INCENTIVE_INVITE"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->IORG_INCENTIVE_INVITE:LX/89v;

    .line 1305445
    new-instance v0, LX/89v;

    const-string v1, "EVENTS_EXTENDED_INVITE"

    const/16 v2, 0xe

    const-string v3, "EVENTS_EXTENDED_INVITE"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->EVENTS_EXTENDED_INVITE:LX/89v;

    .line 1305446
    new-instance v0, LX/89v;

    const-string v1, "CCU_INTERSTITIAL_NUX"

    const/16 v2, 0xf

    const-string v3, "CCU_INTERSTITIAL_NUX"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->CCU_INTERSTITIAL_NUX:LX/89v;

    .line 1305447
    new-instance v0, LX/89v;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x10

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v3}, LX/89v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89v;->UNKNOWN:LX/89v;

    .line 1305448
    const/16 v0, 0x11

    new-array v0, v0, [LX/89v;

    sget-object v1, LX/89v;->NEWS_FEED_FIND_FRIENDS:LX/89v;

    aput-object v1, v0, v4

    sget-object v1, LX/89v;->NEWS_FEED_MEGAPHONE:LX/89v;

    aput-object v1, v0, v5

    sget-object v1, LX/89v;->NEWS_FEED_PYMI:LX/89v;

    aput-object v1, v0, v6

    sget-object v1, LX/89v;->NEWS_FEED_PYMK:LX/89v;

    aput-object v1, v0, v7

    sget-object v1, LX/89v;->FRIENDS_TAB:LX/89v;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/89v;->FRIEND_BROWSER:LX/89v;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/89v;->INTERSTITIAL:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/89v;->BOOKMARKS:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/89v;->SEARCH_BOX:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/89v;->FRIEND_REQUEST_TAB:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/89v;->FRIENDS_CENTER:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/89v;->CONTINUOUS_SYNC:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/89v;->IORG_INCENTIVE_INVITE:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/89v;->EVENTS_EXTENDED_INVITE:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/89v;->CCU_INTERSTITIAL_NUX:LX/89v;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/89v;->UNKNOWN:LX/89v;

    aput-object v2, v0, v1

    sput-object v0, LX/89v;->$VALUES:[LX/89v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1305449
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1305450
    iput-object p3, p0, LX/89v;->value:Ljava/lang/String;

    .line 1305451
    return-void
.end method

.method public static fromSerializable(Ljava/io/Serializable;)LX/89v;
    .locals 1

    .prologue
    .line 1305452
    if-nez p0, :cond_0

    .line 1305453
    sget-object p0, LX/89v;->UNKNOWN:LX/89v;

    .line 1305454
    :goto_0
    return-object p0

    .line 1305455
    :cond_0
    instance-of v0, p0, LX/89v;

    if-eqz v0, :cond_1

    .line 1305456
    check-cast p0, LX/89v;

    goto :goto_0

    .line 1305457
    :cond_1
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1305458
    check-cast p0, Ljava/lang/String;

    invoke-static {p0}, LX/89v;->fromString(Ljava/lang/String;)LX/89v;

    move-result-object p0

    goto :goto_0

    .line 1305459
    :cond_2
    sget-object p0, LX/89v;->UNKNOWN:LX/89v;

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)LX/89v;
    .locals 5

    .prologue
    .line 1305460
    invoke-static {}, LX/89v;->values()[LX/89v;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1305461
    iget-object v4, v0, LX/89v;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1305462
    :goto_1
    return-object v0

    .line 1305463
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1305464
    :cond_1
    sget-object v0, LX/89v;->UNKNOWN:LX/89v;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/89v;
    .locals 1

    .prologue
    .line 1305465
    const-class v0, LX/89v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89v;

    return-object v0
.end method

.method public static values()[LX/89v;
    .locals 1

    .prologue
    .line 1305466
    sget-object v0, LX/89v;->$VALUES:[LX/89v;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89v;

    return-object v0
.end method
