.class public LX/8Gg;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1319897
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1qM;
    .locals 4

    .prologue
    .line 1319899
    const-class v1, LX/8Gg;

    monitor-enter v1

    .line 1319900
    :try_start_0
    sget-object v0, LX/8Gg;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1319901
    sput-object v2, LX/8Gg;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1319902
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319903
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1319904
    invoke-static {v0}, LX/8Ha;->a(LX/0QB;)LX/8Ha;

    move-result-object v3

    check-cast v3, LX/8Ha;

    invoke-static {v0}, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a(LX/0QB;)Lcom/facebook/photos/data/service/PhotosServiceHandler;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;

    invoke-static {v3, p0}, LX/8Gj;->a(LX/8Ha;Lcom/facebook/photos/data/service/PhotosServiceHandler;)LX/1qM;

    move-result-object v3

    move-object v0, v3

    .line 1319905
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1319906
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1319907
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1319908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1319898
    invoke-static {p0}, LX/8Ha;->a(LX/0QB;)LX/8Ha;

    move-result-object v0

    check-cast v0, LX/8Ha;

    invoke-static {p0}, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a(LX/0QB;)Lcom/facebook/photos/data/service/PhotosServiceHandler;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/service/PhotosServiceHandler;

    invoke-static {v0, v1}, LX/8Gj;->a(LX/8Ha;Lcom/facebook/photos/data/service/PhotosServiceHandler;)LX/1qM;

    move-result-object v0

    return-object v0
.end method
