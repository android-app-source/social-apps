.class public final enum LX/74O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74O;

.field public static final enum ALBUM:LX/74O;

.field public static final enum ALBUM_PERMALINK:LX/74O;

.field public static final enum FACEWEB:LX/74O;

.field public static final enum FEED:LX/74O;

.field public static final enum FOOD_PHOTOS:LX/74O;

.field public static final enum FULL_SCREEN_GALLERY:LX/74O;

.field public static final enum FUNDRAISER_COVER_PHOTO:LX/74O;

.field public static final enum GROUPS_COVER_PHOTO:LX/74O;

.field public static final enum GROUPS_INFO_PAGE_PHOTO:LX/74O;

.field public static final enum INTENT:LX/74O;

.field public static final enum NEARBYPLACES:LX/74O;

.field public static final enum NOT_TAGGED_TAB:LX/74O;

.field public static final enum OTHER:LX/74O;

.field public static final enum PAGE_COVER_PHOTO:LX/74O;

.field public static final enum PAGE_GRID_PHOTO_CARD:LX/74O;

.field public static final enum PAGE_PHOTOS_TAB:LX/74O;

.field public static final enum PAGE_PHOTO_MENUS:LX/74O;

.field public static final enum PAGE_PROFILE_PHOTO:LX/74O;

.field public static final enum PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_ALL_PHOTOS:LX/74O;

.field public static final enum PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_SPECIAL_CARD:LX/74O;

.field public static final enum PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS:LX/74O;

.field public static final enum PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_ALL_PHOTOS:LX/74O;

.field public static final enum PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_SPECIAL_CARD:LX/74O;

.field public static final enum PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS:LX/74O;

.field public static final enum PHOTOS_BY_CATEGORY:LX/74O;

.field public static final enum PHOTOS_FEED:LX/74O;

.field public static final enum PHOTOS_OF_:LX/74O;

.field public static final enum PHOTOS_TAB:LX/74O;

.field public static final enum PHOTO_COMMENT:LX/74O;

.field public static final enum PROMOTION_CAMPAIGN_PHOTOS:LX/74O;

.field public static final enum SEARCH_EYEWITNESS_MODULE:LX/74O;

.field public static final enum SEARCH_PHOTOS_GRID_MODULE:LX/74O;

.field public static final enum SEARCH_PHOTO_RESULTS_PAGE:LX/74O;

.field public static final enum SEARCH_PROFILE_SNAPSHOT_MODULE:LX/74O;

.field public static final enum SEARCH_TOP_PHOTOS_MODULE:LX/74O;

.field public static final enum SNOWFLAKE:LX/74O;

.field public static final enum SOUVENIRS:LX/74O;

.field public static final enum SYNC:LX/74O;

.field public static final enum TIMELINE:LX/74O;

.field public static final enum TIMELINE_COVER_PHOTO:LX/74O;

.field public static final enum TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74O;

.field public static final enum TIMELINE_PHOTO_WIDGET:LX/74O;

.field public static final enum TIMELINE_PROFILE_PHOTO:LX/74O;

.field public static final enum YOUR_PHOTOS:LX/74O;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1167813
    new-instance v0, LX/74O;

    const-string v1, "OTHER"

    const-string v2, "other"

    invoke-direct {v0, v1, v4, v2}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->OTHER:LX/74O;

    .line 1167814
    new-instance v0, LX/74O;

    const-string v1, "SYNC"

    const-string v2, "shoebox"

    invoke-direct {v0, v1, v5, v2}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SYNC:LX/74O;

    .line 1167815
    new-instance v0, LX/74O;

    const-string v1, "PHOTOS_TAB"

    const-string v2, "photos_tab"

    invoke-direct {v0, v1, v6, v2}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PHOTOS_TAB:LX/74O;

    .line 1167816
    new-instance v0, LX/74O;

    const-string v1, "PHOTOS_OF_"

    const-string v2, "photos_of_"

    invoke-direct {v0, v1, v7, v2}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PHOTOS_OF_:LX/74O;

    .line 1167817
    new-instance v0, LX/74O;

    const-string v1, "PHOTOS_BY_CATEGORY"

    const-string v2, "photos_by_category"

    invoke-direct {v0, v1, v8, v2}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PHOTOS_BY_CATEGORY:LX/74O;

    .line 1167818
    new-instance v0, LX/74O;

    const-string v1, "PAGE_PROFILE_PHOTO"

    const/4 v2, 0x5

    const-string v3, "page_profile_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PAGE_PROFILE_PHOTO:LX/74O;

    .line 1167819
    new-instance v0, LX/74O;

    const-string v1, "PAGE_COVER_PHOTO"

    const/4 v2, 0x6

    const-string v3, "page_cover_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PAGE_COVER_PHOTO:LX/74O;

    .line 1167820
    new-instance v0, LX/74O;

    const-string v1, "PAGE_PHOTO_MENUS"

    const/4 v2, 0x7

    const-string v3, "page_photo_menus"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PAGE_PHOTO_MENUS:LX/74O;

    .line 1167821
    new-instance v0, LX/74O;

    const-string v1, "FOOD_PHOTOS"

    const/16 v2, 0x8

    const-string v3, "food_photos"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->FOOD_PHOTOS:LX/74O;

    .line 1167822
    new-instance v0, LX/74O;

    const-string v1, "PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_SPECIAL_CARD"

    const/16 v2, 0x9

    const-string v3, "pandora_benny_photos_of_large_thumbnail_special_card"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_SPECIAL_CARD:LX/74O;

    .line 1167823
    new-instance v0, LX/74O;

    const-string v1, "PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_SPECIAL_CARD"

    const/16 v2, 0xa

    const-string v3, "pandora_benny_photos_of_small_thumbnail_special_card"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_SPECIAL_CARD:LX/74O;

    .line 1167824
    new-instance v0, LX/74O;

    const-string v1, "PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_ALL_PHOTOS"

    const/16 v2, 0xb

    const-string v3, "pandora_benny_photos_of_large_thumbnail_all_photos"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_ALL_PHOTOS:LX/74O;

    .line 1167825
    new-instance v0, LX/74O;

    const-string v1, "PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_ALL_PHOTOS"

    const/16 v2, 0xc

    const-string v3, "pandora_benny_photos_of_small_thumbnail_all_photos"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_ALL_PHOTOS:LX/74O;

    .line 1167826
    new-instance v0, LX/74O;

    const-string v1, "PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS"

    const/16 v2, 0xd

    const-string v3, "pandora_benny_photos_of_large_thumbnail_target_and_mutual_friends"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS:LX/74O;

    .line 1167827
    new-instance v0, LX/74O;

    const-string v1, "PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS"

    const/16 v2, 0xe

    const-string v3, "pandora_benny_photos_of_small_thumbnail_target_and_mutual_friends"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS:LX/74O;

    .line 1167828
    new-instance v0, LX/74O;

    const-string v1, "NOT_TAGGED_TAB"

    const/16 v2, 0xf

    const-string v3, "not_tagged_tab"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->NOT_TAGGED_TAB:LX/74O;

    .line 1167829
    new-instance v0, LX/74O;

    const-string v1, "YOUR_PHOTOS"

    const/16 v2, 0x10

    const-string v3, "your_photos"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->YOUR_PHOTOS:LX/74O;

    .line 1167830
    new-instance v0, LX/74O;

    const-string v1, "SEARCH_EYEWITNESS_MODULE"

    const/16 v2, 0x11

    const-string v3, "search_eyewitness_module"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SEARCH_EYEWITNESS_MODULE:LX/74O;

    .line 1167831
    new-instance v0, LX/74O;

    const-string v1, "SEARCH_TOP_PHOTOS_MODULE"

    const/16 v2, 0x12

    const-string v3, "search_top_photos_module"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SEARCH_TOP_PHOTOS_MODULE:LX/74O;

    .line 1167832
    new-instance v0, LX/74O;

    const-string v1, "SEARCH_PHOTOS_GRID_MODULE"

    const/16 v2, 0x13

    const-string v3, "search_photos_grid_module"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SEARCH_PHOTOS_GRID_MODULE:LX/74O;

    .line 1167833
    new-instance v0, LX/74O;

    const-string v1, "SEARCH_PHOTO_RESULTS_PAGE"

    const/16 v2, 0x14

    const-string v3, "search_photo_results_page"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SEARCH_PHOTO_RESULTS_PAGE:LX/74O;

    .line 1167834
    new-instance v0, LX/74O;

    const-string v1, "SEARCH_PROFILE_SNAPSHOT_MODULE"

    const/16 v2, 0x15

    const-string v3, "search_profile_snapshot_module"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SEARCH_PROFILE_SNAPSHOT_MODULE:LX/74O;

    .line 1167835
    new-instance v0, LX/74O;

    const-string v1, "TIMELINE_PROFILE_PHOTO"

    const/16 v2, 0x16

    const-string v3, "timeline_profile_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->TIMELINE_PROFILE_PHOTO:LX/74O;

    .line 1167836
    new-instance v0, LX/74O;

    const-string v1, "TIMELINE_COVER_PHOTO"

    const/16 v2, 0x17

    const-string v3, "timeline_cover_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->TIMELINE_COVER_PHOTO:LX/74O;

    .line 1167837
    new-instance v0, LX/74O;

    const-string v1, "TIMELINE_PHOTO_WIDGET"

    const/16 v2, 0x18

    const-string v3, "timeline_photo_widget"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->TIMELINE_PHOTO_WIDGET:LX/74O;

    .line 1167838
    new-instance v0, LX/74O;

    const-string v1, "TIMELINE_INTRO_CARD_FAV_PHOTO"

    const/16 v2, 0x19

    const-string v3, "timeline_intro_card_fav_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74O;

    .line 1167839
    new-instance v0, LX/74O;

    const-string v1, "GROUPS_COVER_PHOTO"

    const/16 v2, 0x1a

    const-string v3, "groups_cover_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->GROUPS_COVER_PHOTO:LX/74O;

    .line 1167840
    new-instance v0, LX/74O;

    const-string v1, "GROUPS_INFO_PAGE_PHOTO"

    const/16 v2, 0x1b

    const-string v3, "groups_info_page_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->GROUPS_INFO_PAGE_PHOTO:LX/74O;

    .line 1167841
    new-instance v0, LX/74O;

    const-string v1, "FEED"

    const/16 v2, 0x1c

    const-string v3, "feed"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->FEED:LX/74O;

    .line 1167842
    new-instance v0, LX/74O;

    const-string v1, "TIMELINE"

    const/16 v2, 0x1d

    const-string v3, "timeline"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->TIMELINE:LX/74O;

    .line 1167843
    new-instance v0, LX/74O;

    const-string v1, "ALBUM"

    const/16 v2, 0x1e

    const-string v3, "albums_tab"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->ALBUM:LX/74O;

    .line 1167844
    new-instance v0, LX/74O;

    const-string v1, "ALBUM_PERMALINK"

    const/16 v2, 0x1f

    const-string v3, "album_permalink"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->ALBUM_PERMALINK:LX/74O;

    .line 1167845
    new-instance v0, LX/74O;

    const-string v1, "PHOTO_COMMENT"

    const/16 v2, 0x20

    const-string v3, "photo_comment"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PHOTO_COMMENT:LX/74O;

    .line 1167846
    new-instance v0, LX/74O;

    const-string v1, "FULL_SCREEN_GALLERY"

    const/16 v2, 0x21

    const-string v3, "full_screen_gallery"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->FULL_SCREEN_GALLERY:LX/74O;

    .line 1167847
    new-instance v0, LX/74O;

    const-string v1, "PAGE_GRID_PHOTO_CARD"

    const/16 v2, 0x22

    const-string v3, "page_grid_photo_card"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PAGE_GRID_PHOTO_CARD:LX/74O;

    .line 1167848
    new-instance v0, LX/74O;

    const-string v1, "PAGE_PHOTOS_TAB"

    const/16 v2, 0x23

    const-string v3, "page_photos_tab"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PAGE_PHOTOS_TAB:LX/74O;

    .line 1167849
    new-instance v0, LX/74O;

    const-string v1, "PHOTOS_FEED"

    const/16 v2, 0x24

    const-string v3, "photos_feed"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PHOTOS_FEED:LX/74O;

    .line 1167850
    new-instance v0, LX/74O;

    const-string v1, "SNOWFLAKE"

    const/16 v2, 0x25

    const-string v3, "snowflake"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SNOWFLAKE:LX/74O;

    .line 1167851
    new-instance v0, LX/74O;

    const-string v1, "FACEWEB"

    const/16 v2, 0x26

    const-string v3, "faceweb"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->FACEWEB:LX/74O;

    .line 1167852
    new-instance v0, LX/74O;

    const-string v1, "SOUVENIRS"

    const/16 v2, 0x27

    const-string v3, "souvenirs"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->SOUVENIRS:LX/74O;

    .line 1167853
    new-instance v0, LX/74O;

    const-string v1, "PROMOTION_CAMPAIGN_PHOTOS"

    const/16 v2, 0x28

    const-string v3, "promotion_campaign"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->PROMOTION_CAMPAIGN_PHOTOS:LX/74O;

    .line 1167854
    new-instance v0, LX/74O;

    const-string v1, "INTENT"

    const/16 v2, 0x29

    const-string v3, "intent"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->INTENT:LX/74O;

    .line 1167855
    new-instance v0, LX/74O;

    const-string v1, "NEARBYPLACES"

    const/16 v2, 0x2a

    const-string v3, "nearbyplaces"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->NEARBYPLACES:LX/74O;

    .line 1167856
    new-instance v0, LX/74O;

    const-string v1, "FUNDRAISER_COVER_PHOTO"

    const/16 v2, 0x2b

    const-string v3, "fundraiser_cover_photo"

    invoke-direct {v0, v1, v2, v3}, LX/74O;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74O;->FUNDRAISER_COVER_PHOTO:LX/74O;

    .line 1167857
    const/16 v0, 0x2c

    new-array v0, v0, [LX/74O;

    sget-object v1, LX/74O;->OTHER:LX/74O;

    aput-object v1, v0, v4

    sget-object v1, LX/74O;->SYNC:LX/74O;

    aput-object v1, v0, v5

    sget-object v1, LX/74O;->PHOTOS_TAB:LX/74O;

    aput-object v1, v0, v6

    sget-object v1, LX/74O;->PHOTOS_OF_:LX/74O;

    aput-object v1, v0, v7

    sget-object v1, LX/74O;->PHOTOS_BY_CATEGORY:LX/74O;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/74O;->PAGE_PROFILE_PHOTO:LX/74O;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/74O;->PAGE_COVER_PHOTO:LX/74O;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/74O;->PAGE_PHOTO_MENUS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/74O;->FOOD_PHOTOS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/74O;->PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_SPECIAL_CARD:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_SPECIAL_CARD:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/74O;->PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_ALL_PHOTOS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_ALL_PHOTOS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/74O;->PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/74O;->NOT_TAGGED_TAB:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/74O;->YOUR_PHOTOS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/74O;->SEARCH_EYEWITNESS_MODULE:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/74O;->SEARCH_TOP_PHOTOS_MODULE:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/74O;->SEARCH_PHOTOS_GRID_MODULE:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/74O;->SEARCH_PHOTO_RESULTS_PAGE:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/74O;->SEARCH_PROFILE_SNAPSHOT_MODULE:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/74O;->TIMELINE_PROFILE_PHOTO:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/74O;->TIMELINE_COVER_PHOTO:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/74O;->TIMELINE_PHOTO_WIDGET:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/74O;->TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/74O;->GROUPS_COVER_PHOTO:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/74O;->GROUPS_INFO_PAGE_PHOTO:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/74O;->FEED:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/74O;->TIMELINE:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/74O;->ALBUM:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/74O;->ALBUM_PERMALINK:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/74O;->PHOTO_COMMENT:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/74O;->FULL_SCREEN_GALLERY:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/74O;->PAGE_GRID_PHOTO_CARD:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/74O;->PAGE_PHOTOS_TAB:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/74O;->PHOTOS_FEED:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/74O;->SNOWFLAKE:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/74O;->FACEWEB:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/74O;->SOUVENIRS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/74O;->PROMOTION_CAMPAIGN_PHOTOS:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/74O;->INTENT:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/74O;->NEARBYPLACES:LX/74O;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/74O;->FUNDRAISER_COVER_PHOTO:LX/74O;

    aput-object v2, v0, v1

    sput-object v0, LX/74O;->$VALUES:[LX/74O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167858
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167859
    iput-object p3, p0, LX/74O;->value:Ljava/lang/String;

    .line 1167860
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74O;
    .locals 1

    .prologue
    .line 1167861
    const-class v0, LX/74O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74O;

    return-object v0
.end method

.method public static values()[LX/74O;
    .locals 1

    .prologue
    .line 1167862
    sget-object v0, LX/74O;->$VALUES:[LX/74O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74O;

    return-object v0
.end method
