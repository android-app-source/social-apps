.class public LX/7Qn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Qf;


# instance fields
.field public final a:LX/04D;

.field public final b:LX/0JP;

.field public final c:LX/0J9;

.field public final d:J


# direct methods
.method public constructor <init>(LX/04D;LX/0JP;LX/0J9;J)V
    .locals 0

    .prologue
    .line 1204790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204791
    iput-object p1, p0, LX/7Qn;->a:LX/04D;

    .line 1204792
    iput-object p2, p0, LX/7Qn;->b:LX/0JP;

    .line 1204793
    iput-object p3, p0, LX/7Qn;->c:LX/0J9;

    .line 1204794
    iput-wide p4, p0, LX/7Qn;->d:J

    .line 1204795
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 4

    .prologue
    .line 1204796
    sget-object v0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qn;->a:LX/04D;

    iget-object v1, v1, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204797
    sget-object v0, LX/0JS;->TTI_TYPE:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qn;->b:LX/0JP;

    iget-object v1, v1, LX/0JP;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204798
    sget-object v0, LX/0JS;->TTI_PERF_S:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-wide v2, p0, LX/7Qn;->d:J

    long-to-float v1, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204799
    iget-object v0, p0, LX/7Qn;->c:LX/0J9;

    if-eqz v0, :cond_0

    .line 1204800
    sget-object v0, LX/0JS;->DATA_PREFETCH_STATUS:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qn;->c:LX/0J9;

    iget-object v1, v1, LX/0J9;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204801
    :cond_0
    return-void
.end method
