.class public LX/8Ni;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8KA;


# direct methods
.method public constructor <init>(LX/8KA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337811
    iput-object p1, p0, LX/8Ni;->a:LX/8KA;

    .line 1337812
    return-void
.end method

.method private static a(IIIIII)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1337813
    mul-int v2, p3, p1

    if-lt p0, v2, :cond_0

    move v2, v0

    .line 1337814
    :goto_0
    mul-int v3, p3, p0

    if-lt p1, v3, :cond_1

    .line 1337815
    :goto_1
    if-nez v0, :cond_2

    if-nez v2, :cond_2

    .line 1337816
    :goto_2
    return p2

    :cond_0
    move v2, v1

    .line 1337817
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1337818
    goto :goto_1

    .line 1337819
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1337820
    if-le p0, p1, :cond_5

    .line 1337821
    if-le p1, p4, :cond_3

    .line 1337822
    int-to-float v0, p4

    int-to-float v1, p1

    div-float/2addr v0, v1

    .line 1337823
    :cond_3
    :goto_3
    int-to-float v1, p0

    mul-float/2addr v1, v0

    int-to-float v2, p1

    mul-float/2addr v1, v2

    mul-float/2addr v1, v0

    .line 1337824
    int-to-float v2, p5

    cmpl-float v2, v1, v2

    if-lez v2, :cond_4

    .line 1337825
    float-to-double v2, v0

    int-to-float v0, p5

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 1337826
    :cond_4
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1337827
    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int p2, v0

    .line 1337828
    goto :goto_2

    .line 1337829
    :cond_5
    if-le p0, p4, :cond_3

    .line 1337830
    int-to-float v0, p4

    int-to-float v1, p0

    div-float/2addr v0, v1

    goto :goto_3
.end method

.method public static a(LX/0QB;)LX/8Ni;
    .locals 1

    .prologue
    .line 1337831
    invoke-static {p0}, LX/8Ni;->b(LX/0QB;)LX/8Ni;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8Ni;
    .locals 2

    .prologue
    .line 1337832
    new-instance v1, LX/8Ni;

    invoke-static {p0}, LX/8KA;->a(LX/0QB;)LX/8KA;

    move-result-object v0

    check-cast v0, LX/8KA;

    invoke-direct {v1, v0}, LX/8Ni;-><init>(LX/8KA;)V

    .line 1337833
    return-object v1
.end method


# virtual methods
.method public final a(IIZZ)LX/43G;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1337834
    if-eqz p4, :cond_1

    iget-object v0, p0, LX/8Ni;->a:LX/8KA;

    .line 1337835
    iget-object v1, v0, LX/8KA;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/8KA;->b:LX/0ad;

    sget v2, LX/8Jz;->q:I

    const/16 v3, 0x800

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    :goto_0
    move v2, v1

    .line 1337836
    :goto_1
    const/4 v3, 0x2

    const/16 v4, 0x280

    const v5, 0xe1000

    move v0, p1

    move v1, p2

    invoke-static/range {v0 .. v5}, LX/8Ni;->a(IIIIII)I

    move-result v0

    .line 1337837
    if-eqz p3, :cond_0

    .line 1337838
    const/16 v0, 0x1800

    .line 1337839
    :cond_0
    new-instance v2, LX/43G;

    if-eqz p4, :cond_3

    iget-object v1, p0, LX/8Ni;->a:LX/8KA;

    invoke-virtual {v1}, LX/8KA;->b()I

    move-result v1

    :goto_2
    invoke-direct {v2, v0, v0, v6, v1}, LX/43G;-><init>(IIZI)V

    .line 1337840
    return-object v2

    .line 1337841
    :cond_1
    iget-object v1, p0, LX/8Ni;->a:LX/8KA;

    if-ge p1, p2, :cond_2

    move v0, v6

    :goto_3
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1337842
    iget-object v2, v1, LX/8KA;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v1, LX/8KA;->b:LX/0ad;

    sget v4, LX/8Jz;->p:I

    const/16 v5, 0x800

    invoke-interface {v2, v4, v5}, LX/0ad;->a(II)I

    move-result v2

    move v4, v2

    .line 1337843
    :goto_4
    if-eqz v0, :cond_6

    iget-object v2, v1, LX/8KA;->b:LX/0ad;

    sget v5, LX/8Jz;->v:F

    invoke-interface {v2, v5, v3}, LX/0ad;->a(FF)F

    move-result v2

    .line 1337844
    :goto_5
    int-to-float v3, v4

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move v2, v2

    .line 1337845
    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 1337846
    :cond_3
    iget-object v1, p0, LX/8Ni;->a:LX/8KA;

    invoke-virtual {v1}, LX/8KA;->a()I

    move-result v1

    goto :goto_2

    :cond_4
    iget-object v1, v0, LX/8KA;->b:LX/0ad;

    sget v2, LX/8Jz;->u:I

    const/16 v3, 0x3c0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    goto :goto_0

    .line 1337847
    :cond_5
    iget-object v2, v1, LX/8KA;->b:LX/0ad;

    sget v4, LX/8Jz;->t:I

    const/16 v5, 0x3c0

    invoke-interface {v2, v4, v5}, LX/0ad;->a(II)I

    move-result v2

    move v4, v2

    goto :goto_4

    :cond_6
    move v2, v3

    .line 1337848
    goto :goto_5
.end method

.method public final a(LX/43C;Ljava/lang/String;LX/434;Ljava/io/File;ZZZ)LX/43G;
    .locals 8

    .prologue
    .line 1337849
    iget v0, p3, LX/434;->b:I

    iget v1, p3, LX/434;->a:I

    invoke-virtual {p0, v0, v1, p5, p7}, LX/8Ni;->a(IIZZ)LX/43G;

    move-result-object v5

    .line 1337850
    invoke-virtual {p4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 1337851
    iget v3, p3, LX/434;->b:I

    iget v4, p3, LX/434;->a:I

    move-object v0, p1

    move-object v1, p2

    move v6, p6

    invoke-interface/range {v0 .. v6}, LX/43C;->a(Ljava/lang/String;Ljava/lang/String;IILX/43G;Z)LX/43G;

    move-result-object v0

    .line 1337852
    invoke-virtual {p4}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 1337853
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 1337854
    new-instance v0, LX/8Nh;

    invoke-direct {v0, v2}, LX/8Nh;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1337855
    :cond_0
    return-object v0
.end method
