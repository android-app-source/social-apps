.class public final LX/7EK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7FJ;

.field public final synthetic b:Lcom/facebook/structuredsurvey/StructuredSurveyController;


# direct methods
.method public constructor <init>(Lcom/facebook/structuredsurvey/StructuredSurveyController;LX/7FJ;)V
    .locals 0

    .prologue
    .line 1184624
    iput-object p1, p0, LX/7EK;->b:Lcom/facebook/structuredsurvey/StructuredSurveyController;

    iput-object p2, p0, LX/7EK;->a:LX/7FJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1184625
    iget-object v0, p0, LX/7EK;->b:Lcom/facebook/structuredsurvey/StructuredSurveyController;

    iget-object v0, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j:LX/03V;

    sget-object v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NaRF:Survey Post Impression:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/7EK;->a:LX/7FJ;

    invoke-virtual {v3}, LX/7FJ;->getImpressionEvent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184626
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1184627
    return-void
.end method
