.class public LX/8YH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8YK;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/8YK;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/8YD;

.field private final d:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z


# direct methods
.method public constructor <init>(LX/8YD;)V
    .locals 2

    .prologue
    .line 1356025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356026
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8YH;->a:Ljava/util/Map;

    .line 1356027
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/8YH;->b:Ljava/util/Set;

    .line 1356028
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/8YH;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 1356029
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8YH;->e:Z

    .line 1356030
    if-nez p1, :cond_0

    .line 1356031
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "springLooper is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356032
    :cond_0
    iput-object p1, p0, LX/8YH;->c:LX/8YD;

    .line 1356033
    iget-object v0, p0, LX/8YH;->c:LX/8YD;

    .line 1356034
    iput-object p0, v0, LX/8YD;->a:LX/8YH;

    .line 1356035
    return-void
.end method

.method private a(LX/8YK;)V
    .locals 2

    .prologue
    .line 1356015
    if-nez p1, :cond_0

    .line 1356016
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "spring is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356017
    :cond_0
    iget-object v0, p0, LX/8YH;->a:Ljava/util/Map;

    .line 1356018
    iget-object v1, p1, LX/8YK;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1356019
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1356020
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "spring is already registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356021
    :cond_1
    iget-object v0, p0, LX/8YH;->a:Ljava/util/Map;

    .line 1356022
    iget-object v1, p1, LX/8YK;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1356023
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356024
    return-void
.end method


# virtual methods
.method public final a()LX/8YK;
    .locals 1

    .prologue
    .line 1356012
    new-instance v0, LX/8YK;

    invoke-direct {v0, p0}, LX/8YK;-><init>(LX/8YH;)V

    .line 1356013
    invoke-direct {p0, v0}, LX/8YH;->a(LX/8YK;)V

    .line 1356014
    return-object v0
.end method

.method public final a(D)V
    .locals 8

    .prologue
    .line 1355998
    iget-object v0, p0, LX/8YH;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1355999
    :cond_0
    iget-object v3, p0, LX/8YH;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8YK;

    .line 1356000
    invoke-virtual {v3}, LX/8YK;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1356001
    iget-boolean v5, v3, LX/8YK;->j:Z

    move v5, v5

    .line 1356002
    if-nez v5, :cond_7

    :cond_1
    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 1356003
    if-eqz v5, :cond_2

    .line 1356004
    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double v5, p1, v5

    invoke-virtual {v3, v5, v6}, LX/8YK;->f(D)V

    goto :goto_1

    .line 1356005
    :cond_2
    iget-object v5, p0, LX/8YH;->b:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1356006
    :cond_3
    iget-object v0, p0, LX/8YH;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1356007
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8YH;->e:Z

    .line 1356008
    :cond_4
    iget-object v0, p0, LX/8YH;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_3

    .line 1356009
    :cond_5
    iget-boolean v0, p0, LX/8YH;->e:Z

    if-eqz v0, :cond_6

    .line 1356010
    iget-object v0, p0, LX/8YH;->c:LX/8YD;

    invoke-virtual {v0}, LX/8YD;->b()V

    .line 1356011
    :cond_6
    return-void

    :cond_7
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1355989
    iget-object v0, p0, LX/8YH;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YK;

    .line 1355990
    if-nez v0, :cond_0

    .line 1355991
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "springId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not reference a registered spring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1355992
    :cond_0
    iget-object v1, p0, LX/8YH;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1355993
    iget-boolean v0, p0, LX/8YH;->e:Z

    move v0, v0

    .line 1355994
    if-eqz v0, :cond_1

    .line 1355995
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8YH;->e:Z

    .line 1355996
    iget-object v0, p0, LX/8YH;->c:LX/8YD;

    invoke-virtual {v0}, LX/8YD;->a()V

    .line 1355997
    :cond_1
    return-void
.end method
