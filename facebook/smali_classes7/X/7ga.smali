.class public LX/7ga;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7ga;


# instance fields
.field private final a:LX/0Zb;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1224588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224589
    iput-object p1, p0, LX/7ga;->a:LX/0Zb;

    .line 1224590
    return-void
.end method

.method public static a(LX/0QB;)LX/7ga;
    .locals 4

    .prologue
    .line 1224575
    sget-object v0, LX/7ga;->d:LX/7ga;

    if-nez v0, :cond_1

    .line 1224576
    const-class v1, LX/7ga;

    monitor-enter v1

    .line 1224577
    :try_start_0
    sget-object v0, LX/7ga;->d:LX/7ga;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1224578
    if-eqz v2, :cond_0

    .line 1224579
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1224580
    new-instance p0, LX/7ga;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/7ga;-><init>(LX/0Zb;)V

    .line 1224581
    move-object v0, p0

    .line 1224582
    sput-object v0, LX/7ga;->d:LX/7ga;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1224583
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1224584
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1224585
    :cond_1
    sget-object v0, LX/7ga;->d:LX/7ga;

    return-object v0

    .line 1224586
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1224587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # LX/7gX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1224565
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/7gX;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1224566
    invoke-virtual {p1}, LX/7gX;->getModuleName()Ljava/lang/String;

    move-result-object v0

    .line 1224567
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1224568
    invoke-direct {p0}, LX/7ga;->b()Ljava/lang/String;

    move-result-object v0

    .line 1224569
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1224570
    if-eqz p2, :cond_0

    .line 1224571
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1224572
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1224573
    :cond_0
    iget-object v0, p0, LX/7ga;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1224574
    return-void
.end method

.method public static b(LX/7ga;Ljava/lang/String;Ljava/lang/String;ILX/7gk;LX/7gZ;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1224558
    invoke-direct {p0}, LX/7ga;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 1224559
    sget-object v1, LX/7gY;->STORY_OWNER:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224560
    sget-object v1, LX/7gY;->MEDIA_ID:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224561
    sget-object v1, LX/7gY;->MEDIA_INDEX:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1224562
    sget-object v1, LX/7gY;->MEDIA_TYPE:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, LX/7gk;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224563
    sget-object v1, LX/7gY;->STORY_OWNER_TYPE:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5}, LX/7gZ;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224564
    return-object v0
.end method

.method private b(ZII)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1224553
    invoke-direct {p0}, LX/7ga;->c()Landroid/os/Bundle;

    move-result-object v0

    .line 1224554
    sget-object v1, LX/7gY;->HAS_MY_STORY:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1224555
    sget-object v1, LX/7gY;->STORY_COUNT:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1224556
    sget-object v1, LX/7gY;->NEW_STORY_COUNT:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1224557
    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1224550
    iget-object v0, p0, LX/7ga;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "You must call start a snacks tray session before calling this API"

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1224551
    iget-object v0, p0, LX/7ga;->b:Ljava/lang/String;

    return-object v0

    .line 1224552
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1224591
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1224592
    sget-object v1, LX/7gY;->TRAY_SESSION_ID:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/7ga;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224593
    return-object v0
.end method

.method private f()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 1224542
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1224543
    sget-object v1, LX/7gY;->TRAY_SESSION_ID:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/7ga;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224544
    sget-object v1, LX/7gY;->VIEWER_SESSION_ID:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1224545
    iget-object v2, p0, LX/7ga;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "You must call start a viewer sesssion before calling this API"

    invoke-static {v2, v3}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1224546
    iget-object v2, p0, LX/7ga;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1224547
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224548
    return-object v0

    .line 1224549
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IZLX/7gZ;)V
    .locals 3

    .prologue
    .line 1224535
    invoke-direct {p0}, LX/7ga;->c()Landroid/os/Bundle;

    move-result-object v0

    .line 1224536
    sget-object v1, LX/7gY;->STORY_OWNER:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224537
    sget-object v1, LX/7gY;->STORY_INDEX:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1224538
    sget-object v1, LX/7gY;->HAS_NEW_CONTENT:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1224539
    sget-object v1, LX/7gY;->STORY_OWNER_TYPE:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, LX/7gZ;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224540
    sget-object v1, LX/7gX;->STORY_PROFILE_IMPRESSION:LX/7gX;

    invoke-static {p0, v1, v0}, LX/7ga;->a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V

    .line 1224541
    return-void
.end method

.method public final a(Ljava/lang/String;IZLX/7gZ;ILX/7gd;)V
    .locals 3

    .prologue
    .line 1224524
    sget-object v0, LX/7gd;->STORY_TRAY:LX/7gd;

    if-ne p6, v0, :cond_0

    .line 1224525
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7ga;->c:Ljava/lang/String;

    .line 1224526
    :cond_0
    invoke-direct {p0}, LX/7ga;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 1224527
    sget-object v1, LX/7gY;->STORY_OWNER:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224528
    sget-object v1, LX/7gY;->STORY_INDEX:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1224529
    sget-object v1, LX/7gY;->HAS_NEW_CONTENT:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1224530
    sget-object v1, LX/7gY;->STORY_OWNER_TYPE:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, LX/7gZ;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224531
    sget-object v1, LX/7gY;->STORY_SIZE:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1224532
    sget-object v1, LX/7gY;->SOURCE:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p6}, LX/7gd;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224533
    sget-object v1, LX/7gX;->OPEN_STORY:LX/7gX;

    invoke-static {p0, v1, v0}, LX/7ga;->a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V

    .line 1224534
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILX/7gk;LX/7gZ;Z)V
    .locals 2

    .prologue
    .line 1224520
    invoke-static/range {p0 .. p5}, LX/7ga;->b(LX/7ga;Ljava/lang/String;Ljava/lang/String;ILX/7gk;LX/7gZ;)Landroid/os/Bundle;

    move-result-object v0

    .line 1224521
    sget-object v1, LX/7gY;->FIRST_VIEW:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1224522
    sget-object v1, LX/7gX;->OPEN_MEDIA:LX/7gX;

    invoke-static {p0, v1, v0}, LX/7ga;->a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V

    .line 1224523
    return-void
.end method

.method public final a(ZII)V
    .locals 2

    .prologue
    .line 1224512
    invoke-direct {p0, p1, p2, p3}, LX/7ga;->b(ZII)Landroid/os/Bundle;

    move-result-object v0

    .line 1224513
    sget-object v1, LX/7gX;->STORY_TRAY_IMPRESSION:LX/7gX;

    invoke-static {p0, v1, v0}, LX/7ga;->a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V

    .line 1224514
    return-void
.end method

.method public final a(ZIILX/7gd;)V
    .locals 3

    .prologue
    .line 1224515
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7ga;->b:Ljava/lang/String;

    .line 1224516
    invoke-direct {p0, p1, p2, p3}, LX/7ga;->b(ZII)Landroid/os/Bundle;

    move-result-object v0

    .line 1224517
    sget-object v1, LX/7gY;->SOURCE:LX/7gY;

    invoke-virtual {v1}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, LX/7gd;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224518
    sget-object v1, LX/7gX;->STORY_TRAY_REFRESH:LX/7gX;

    invoke-static {p0, v1, v0}, LX/7ga;->a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V

    .line 1224519
    return-void
.end method
