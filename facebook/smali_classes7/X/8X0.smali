.class public LX/8X0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

.field public final b:LX/8Tm;

.field public final c:LX/8TD;

.field public final d:LX/8TS;

.field public e:LX/8XO;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;LX/8Tm;LX/8TD;LX/8TS;Landroid/support/v7/widget/RecyclerView;)V
    .locals 2
    .param p5    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1354535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1354536
    iput-object p1, p0, LX/8X0;->a:Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    .line 1354537
    iput-object p2, p0, LX/8X0;->b:LX/8Tm;

    .line 1354538
    iput-object p3, p0, LX/8X0;->c:LX/8TD;

    .line 1354539
    iput-object p4, p0, LX/8X0;->d:LX/8TS;

    .line 1354540
    iget-object v0, p0, LX/8X0;->a:Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    invoke-virtual {p5, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1354541
    iget-object v0, p0, LX/8X0;->a:Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    new-instance v1, LX/8Wy;

    invoke-direct {v1, p0}, LX/8Wy;-><init>(LX/8X0;)V

    .line 1354542
    iput-object v1, v0, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->c:LX/8Wy;

    .line 1354543
    return-void
.end method
