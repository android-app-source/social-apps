.class public final LX/7ng;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1240266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I
    .locals 18

    .prologue
    .line 1240267
    if-nez p1, :cond_0

    .line 1240268
    const/4 v2, 0x0

    .line 1240269
    :goto_0
    return v2

    .line 1240270
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1240271
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1240272
    const/4 v2, 0x0

    .line 1240273
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k()LX/0Px;

    move-result-object v4

    .line 1240274
    if-eqz v4, :cond_4

    .line 1240275
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    new-array v7, v2, [I

    .line 1240276
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 1240277
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/7ng;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v7, v3

    .line 1240278
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1240279
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v2}, LX/186;->a([IZ)I

    move-result v2

    move v3, v2

    .line 1240280
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/7ng;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v7

    .line 1240281
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1240282
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1240283
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1240284
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/7ng;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v11

    .line 1240285
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v12

    .line 1240286
    const/4 v2, 0x0

    .line 1240287
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r()LX/0Px;

    move-result-object v13

    .line 1240288
    if-eqz v13, :cond_3

    .line 1240289
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    new-array v14, v2, [I

    .line 1240290
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 1240291
    invoke-virtual {v13, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/7ng;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v14, v4

    .line 1240292
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 1240293
    :cond_2
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 1240294
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/7ng;->a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I

    move-result v4

    .line 1240295
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1240296
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->u()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LX/186;->c(Ljava/util/List;)I

    move-result v14

    .line 1240297
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/7ng;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v15

    .line 1240298
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->x()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1240299
    const/16 v17, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1240300
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1240301
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 1240302
    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v3}, LX/186;->b(II)V

    .line 1240303
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1240304
    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1240305
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1240306
    const/4 v3, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1240307
    const/4 v3, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1240308
    const/16 v3, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1240309
    const/16 v3, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1240310
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1240311
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1240312
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1240313
    const/16 v2, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1240314
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->w()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1240315
    const/16 v2, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1240316
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 1240317
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1240318
    if-nez p1, :cond_0

    .line 1240319
    :goto_0
    return v0

    .line 1240320
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1240321
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1240322
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1240323
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1240324
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1240325
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1240326
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1240327
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1240328
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1240329
    if-nez p1, :cond_0

    .line 1240330
    :goto_0
    return v0

    .line 1240331
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1240332
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1240333
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 1240334
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 1240335
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1240336
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1240337
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1240338
    if-nez p1, :cond_0

    .line 1240339
    :goto_0
    return v0

    .line 1240340
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1240341
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1240342
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1240343
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1240344
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1240345
    if-nez p1, :cond_0

    .line 1240346
    :goto_0
    return v0

    .line 1240347
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1240348
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1240349
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1240350
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1240351
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1240352
    if-nez p0, :cond_1

    .line 1240353
    :cond_0
    :goto_0
    return-object v2

    .line 1240354
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1240355
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 1240356
    if-nez p0, :cond_3

    .line 1240357
    :goto_1
    move v1, v4

    .line 1240358
    if-eqz v1, :cond_0

    .line 1240359
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1240360
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1240361
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1240362
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1240363
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1240364
    const-string v1, "EventsConverter.getEventCommonTextWithEntities"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1240365
    :cond_2
    new-instance v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    invoke-direct {v2, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1240366
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v5

    .line 1240367
    if-eqz v5, :cond_5

    .line 1240368
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 1240369
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 1240370
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    const/4 v8, 0x0

    .line 1240371
    if-nez v1, :cond_6

    .line 1240372
    :goto_3
    move v1, v8

    .line 1240373
    aput v1, v6, v3

    .line 1240374
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1240375
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 1240376
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1240377
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1240378
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1240379
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 1240380
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 1240381
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_4

    .line 1240382
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v9

    invoke-static {v0, v9}, LX/7ng;->b(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I

    move-result v9

    .line 1240383
    const/4 v10, 0x3

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1240384
    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 1240385
    const/4 v9, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v10

    invoke-virtual {v0, v9, v10, v8}, LX/186;->a(III)V

    .line 1240386
    const/4 v9, 0x2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v10

    invoke-virtual {v0, v9, v10, v8}, LX/186;->a(III)V

    .line 1240387
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 1240388
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto :goto_3
.end method

.method public static b(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I
    .locals 14

    .prologue
    .line 1240389
    if-nez p1, :cond_0

    .line 1240390
    const/4 v0, 0x0

    .line 1240391
    :goto_0
    return v0

    .line 1240392
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    .line 1240393
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 1240394
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v0

    const/4 v1, 0x0

    .line 1240395
    if-nez v0, :cond_3

    .line 1240396
    :goto_1
    move v4, v1

    .line 1240397
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    invoke-static {p0, v0}, LX/7ng;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I

    move-result v5

    .line 1240398
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1240399
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1240400
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1240401
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1240402
    if-nez v0, :cond_4

    .line 1240403
    const/4 v1, 0x0

    .line 1240404
    :goto_2
    move v9, v1

    .line 1240405
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    const/4 v1, 0x0

    .line 1240406
    if-nez v0, :cond_6

    .line 1240407
    :goto_3
    move v10, v1

    .line 1240408
    const/4 v0, 0x0

    .line 1240409
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v11

    .line 1240410
    if-eqz v11, :cond_2

    .line 1240411
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v0

    new-array v12, v0, [I

    .line 1240412
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1240413
    invoke-virtual {v11, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    invoke-static {p0, v0}, LX/7ng;->a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I

    move-result v0

    aput v0, v12, v1

    .line 1240414
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1240415
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v12, v0}, LX/186;->a([IZ)I

    move-result v0

    .line 1240416
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1240417
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1240418
    const/16 v12, 0xf

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1240419
    const/4 v12, 0x0

    invoke-virtual {p0, v12, v2}, LX/186;->b(II)V

    .line 1240420
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1240421
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1240422
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1240423
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1240424
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1240425
    const/4 v2, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->o()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1240426
    const/4 v2, 0x7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->p()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1240427
    const/16 v2, 0x8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->q()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1240428
    const/16 v2, 0x9

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 1240429
    const/16 v2, 0xa

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 1240430
    const/16 v2, 0xb

    invoke-virtual {p0, v2, v10}, LX/186;->b(II)V

    .line 1240431
    const/16 v2, 0xc

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1240432
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1240433
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 1240434
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1240435
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 1240436
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1240437
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1240438
    invoke-virtual {p0, v1, v4}, LX/186;->b(II)V

    .line 1240439
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1240440
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1240441
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1240442
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1240443
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    const/4 v11, 0x0

    .line 1240444
    if-nez v10, :cond_5

    .line 1240445
    :goto_5
    move v10, v11

    .line 1240446
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->aE()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1240447
    const/4 v12, 0x5

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1240448
    const/4 v12, 0x1

    invoke-virtual {p0, v12, v1}, LX/186;->b(II)V

    .line 1240449
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v9}, LX/186;->b(II)V

    .line 1240450
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v10}, LX/186;->b(II)V

    .line 1240451
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v11}, LX/186;->b(II)V

    .line 1240452
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1240453
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1240454
    :cond_5
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1240455
    const/4 v13, 0x1

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1240456
    invoke-virtual {p0, v11, v12}, LX/186;->b(II)V

    .line 1240457
    invoke-virtual {p0}, LX/186;->d()I

    move-result v11

    .line 1240458
    invoke-virtual {p0, v11}, LX/186;->d(I)V

    goto :goto_5

    .line 1240459
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1240460
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1240461
    invoke-virtual {p0, v1, v10}, LX/186;->b(II)V

    .line 1240462
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1240463
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1240464
    if-nez p1, :cond_0

    .line 1240465
    :goto_0
    return v2

    .line 1240466
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 1240467
    if-eqz v3, :cond_2

    .line 1240468
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1240469
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1240470
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    const/4 v6, 0x0

    .line 1240471
    if-nez v0, :cond_3

    .line 1240472
    :goto_2
    move v0, v6

    .line 1240473
    aput v0, v4, v1

    .line 1240474
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1240475
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1240476
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1240477
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1240478
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1240479
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 1240480
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1240481
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1240482
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    const/4 v8, 0x0

    .line 1240483
    if-nez v7, :cond_4

    .line 1240484
    :goto_4
    move v7, v8

    .line 1240485
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1240486
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1240487
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1240488
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 1240489
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    .line 1240490
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1240491
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    const/4 v12, 0x0

    .line 1240492
    if-nez v11, :cond_5

    .line 1240493
    :goto_5
    move v11, v12

    .line 1240494
    const/4 v12, 0x3

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1240495
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1240496
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v10}, LX/186;->b(II)V

    .line 1240497
    const/4 v8, 0x2

    invoke-virtual {p0, v8, v11}, LX/186;->b(II)V

    .line 1240498
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1240499
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4

    .line 1240500
    :cond_5
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1240501
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1240502
    invoke-virtual {p0, v12, v0}, LX/186;->b(II)V

    .line 1240503
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 1240504
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_5
.end method
