.class public final enum LX/8TI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8TI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8TI;

.field public static final enum CHALLENGE_CREATION:LX/8TI;

.field public static final enum GAME_SHARE:LX/8TI;

.field public static final enum SCORE_SHARE:LX/8TI;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1348446
    new-instance v0, LX/8TI;

    const-string v1, "GAME_SHARE"

    const-string v2, "game_share"

    invoke-direct {v0, v1, v3, v2}, LX/8TI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TI;->GAME_SHARE:LX/8TI;

    .line 1348447
    new-instance v0, LX/8TI;

    const-string v1, "SCORE_SHARE"

    const-string v2, "score_share"

    invoke-direct {v0, v1, v4, v2}, LX/8TI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TI;->SCORE_SHARE:LX/8TI;

    .line 1348448
    new-instance v0, LX/8TI;

    const-string v1, "CHALLENGE_CREATION"

    const-string v2, "challenge_creation"

    invoke-direct {v0, v1, v5, v2}, LX/8TI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TI;->CHALLENGE_CREATION:LX/8TI;

    .line 1348449
    const/4 v0, 0x3

    new-array v0, v0, [LX/8TI;

    sget-object v1, LX/8TI;->GAME_SHARE:LX/8TI;

    aput-object v1, v0, v3

    sget-object v1, LX/8TI;->SCORE_SHARE:LX/8TI;

    aput-object v1, v0, v4

    sget-object v1, LX/8TI;->CHALLENGE_CREATION:LX/8TI;

    aput-object v1, v0, v5

    sput-object v0, LX/8TI;->$VALUES:[LX/8TI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348443
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1348444
    iput-object p3, p0, LX/8TI;->value:Ljava/lang/String;

    .line 1348445
    return-void
.end method

.method public static fromValue(Ljava/lang/String;)LX/8TI;
    .locals 2

    .prologue
    .line 1348450
    sget-object v0, LX/8TI;->GAME_SHARE:LX/8TI;

    iget-object v0, v0, LX/8TI;->value:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348451
    sget-object v0, LX/8TI;->GAME_SHARE:LX/8TI;

    .line 1348452
    :goto_0
    return-object v0

    .line 1348453
    :cond_0
    sget-object v0, LX/8TI;->SCORE_SHARE:LX/8TI;

    iget-object v0, v0, LX/8TI;->value:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1348454
    sget-object v0, LX/8TI;->SCORE_SHARE:LX/8TI;

    goto :goto_0

    .line 1348455
    :cond_1
    sget-object v0, LX/8TI;->CHALLENGE_CREATION:LX/8TI;

    iget-object v0, v0, LX/8TI;->value:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1348456
    sget-object v0, LX/8TI;->CHALLENGE_CREATION:LX/8TI;

    goto :goto_0

    .line 1348457
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid value provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/8TI;
    .locals 1

    .prologue
    .line 1348442
    const-class v0, LX/8TI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8TI;

    return-object v0
.end method

.method public static values()[LX/8TI;
    .locals 1

    .prologue
    .line 1348441
    sget-object v0, LX/8TI;->$VALUES:[LX/8TI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8TI;

    return-object v0
.end method
