.class public final LX/8Od;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/7TD;",
        "Lcom/facebook/photos/base/media/VideoItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Oe;


# direct methods
.method public constructor <init>(LX/8Oe;)V
    .locals 0

    .prologue
    .line 1339755
    iput-object p1, p0, LX/8Od;->a:LX/8Oe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1339756
    check-cast p1, LX/7TD;

    .line 1339757
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "output file uri : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, LX/7TD;->a:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1339758
    iget-object v0, p0, LX/8Od;->a:LX/8Oe;

    iget-object v0, v0, LX/8Oe;->d:LX/8Of;

    iget-object v0, v0, LX/8Of;->d:LX/74n;

    iget-object v1, p1, LX/7TD;->a:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v0, v1, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    return-object v0
.end method
