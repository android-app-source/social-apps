.class public LX/7hI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1225907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225908
    return-void
.end method

.method private static a(LX/7hH;I)I
    .locals 2

    .prologue
    .line 1225904
    iget v0, p0, LX/7hH;->a:I

    iget v1, p0, LX/7hH;->b:I

    .line 1225905
    const/16 p0, 0x5a

    if-eq p1, p0, :cond_0

    const/16 p0, 0x10e

    if-ne p1, p0, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    move v0, v0

    .line 1225906
    return v0
.end method

.method private static a(Landroid/graphics/Bitmap;IIIZ)Landroid/graphics/Bitmap;
    .locals 20

    .prologue
    .line 1225854
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1225855
    move/from16 v0, p1

    int-to-float v3, v0

    move/from16 v0, p2

    int-to-float v4, v0

    div-float/2addr v3, v4

    .line 1225856
    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 1225857
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1225858
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 1225859
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v3, v5

    div-int/lit8 v3, v3, 0x2

    .line 1225860
    const/4 v4, 0x0

    move v9, v2

    move v10, v5

    .line 1225861
    :goto_0
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v10, v9, v0, v1}, LX/7hI;->a(IIIZ)Landroid/graphics/Matrix;

    move-result-object v7

    .line 1225862
    move/from16 v0, p1

    int-to-float v2, v0

    int-to-float v5, v10

    div-float/2addr v2, v5

    move/from16 v0, p2

    int-to-float v5, v0

    int-to-float v6, v9

    div-float/2addr v5, v6

    invoke-virtual {v7, v2, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1225863
    const/4 v5, 0x0

    .line 1225864
    const/4 v2, 0x0

    move v11, v2

    move-object v12, v5

    .line 1225865
    :goto_1
    if-nez v12, :cond_1

    const/4 v2, 0x5

    if-eq v11, v2, :cond_1

    .line 1225866
    int-to-double v14, v10

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    int-to-double v0, v11

    move-wide/from16 v18, v0

    :try_start_0
    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    div-double v14, v14, v16

    double-to-int v5, v14

    int-to-double v14, v9

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    int-to-double v0, v11

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    div-double v14, v14, v16

    double-to-int v6, v14

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move-object v12, v2

    .line 1225867
    goto :goto_1

    .line 1225868
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 1225869
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1225870
    const/4 v3, 0x0

    .line 1225871
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    move v9, v2

    move v10, v5

    goto :goto_0

    .line 1225872
    :catch_0
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    .line 1225873
    goto :goto_1

    .line 1225874
    :cond_1
    if-nez v12, :cond_2

    .line 1225875
    new-instance v2, Ljava/lang/OutOfMemoryError;

    invoke-direct {v2}, Ljava/lang/OutOfMemoryError;-><init>()V

    throw v2

    .line 1225876
    :cond_2
    return-object v12
.end method

.method public static a(Landroid/graphics/Bitmap;IZLX/7hH;IF)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1225883
    sub-float v1, p5, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3727c5ac    # 1.0E-5f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    move p5, v0

    .line 1225884
    :cond_0
    invoke-static {p3, p1}, LX/7hI;->a(LX/7hH;I)I

    move-result v2

    .line 1225885
    iget v1, p3, LX/7hH;->a:I

    iget v3, p3, LX/7hH;->b:I

    .line 1225886
    const/16 p3, 0x5a

    if-eq p1, p3, :cond_1

    const/16 p3, 0x10e

    if-ne p1, p3, :cond_2

    :cond_1
    move v3, v1

    :cond_2
    move v1, v3

    .line 1225887
    move v1, v1

    .line 1225888
    cmpl-float v3, p5, v0

    if-nez v3, :cond_4

    .line 1225889
    if-ge v1, v2, :cond_3

    move v2, v1

    .line 1225890
    :goto_0
    if-eqz p2, :cond_8

    sub-int v0, p1, p4

    .line 1225891
    :goto_1
    invoke-static {p0, v2, v1, v0, p2}, LX/7hI;->a(Landroid/graphics/Bitmap;IIIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_3
    move v1, v2

    .line 1225892
    goto :goto_0

    .line 1225893
    :cond_4
    if-gt v2, v1, :cond_6

    .line 1225894
    div-int v3, v1, v2

    int-to-float v3, v3

    .line 1225895
    cmpl-float v3, p5, v3

    if-lez v3, :cond_5

    .line 1225896
    int-to-float v2, v1

    div-float/2addr v0, p5

    mul-float/2addr v0, v2

    float-to-int v2, v0

    goto :goto_0

    .line 1225897
    :cond_5
    int-to-float v0, v2

    mul-float/2addr v0, p5

    float-to-int v0, v0

    move v1, v0

    .line 1225898
    goto :goto_0

    .line 1225899
    :cond_6
    div-int v3, v2, v1

    int-to-float v3, v3

    .line 1225900
    cmpl-float v3, p5, v3

    if-lez v3, :cond_7

    .line 1225901
    int-to-float v1, v2

    div-float/2addr v0, p5

    mul-float/2addr v0, v1

    float-to-int v0, v0

    move v1, v0

    goto :goto_0

    .line 1225902
    :cond_7
    int-to-float v0, v1

    mul-float/2addr v0, p5

    float-to-int v2, v0

    goto :goto_0

    .line 1225903
    :cond_8
    add-int v0, p1, p4

    goto :goto_1
.end method

.method private static a(IIIZ)Landroid/graphics/Matrix;
    .locals 4

    .prologue
    .line 1225877
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1225878
    if-eqz p2, :cond_0

    .line 1225879
    int-to-float v1, p2

    div-int/lit8 v2, p0, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, p1, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1225880
    :cond_0
    if-eqz p3, :cond_1

    .line 1225881
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1225882
    :cond_1
    return-object v0
.end method
