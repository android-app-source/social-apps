.class public LX/7YJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;",
        "Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1219679
    const-class v0, LX/7YJ;

    sput-object v0, LX/7YJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1219684
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1219685
    check-cast p1, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;

    .line 1219686
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1219687
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1219688
    const-string v1, "enc_phone"

    .line 1219689
    iget-object v2, p1, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1219690
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219691
    const-string v1, "promo_id"

    .line 1219692
    iget-object v2, p1, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1219693
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219694
    const-string v1, "location"

    .line 1219695
    iget-object v2, p1, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->c:LX/6Y4;

    invoke-virtual {v2}, LX/6Y4;->getParamName()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1219696
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219697
    const-string v1, "format"

    const-string v2, "json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219698
    move-object v0, v0

    .line 1219699
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1219700
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v1, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1219701
    :cond_0
    new-instance v0, LX/14N;

    const-string v1, "zeroBuyPromo"

    const-string v2, "GET"

    const-string v3, "method/mobile.zeroBuyPromo"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1219680
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1219681
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1219682
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->a(Lorg/json/JSONObject;)Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    move-result-object v0

    return-object v0
.end method
