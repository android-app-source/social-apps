.class public LX/7g2;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# instance fields
.field public a:I

.field public b:Landroid/app/LocalActivityManager;

.field public c:Landroid/widget/TabWidget;

.field public d:Landroid/widget/FrameLayout;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7g8;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/view/View;

.field public g:LX/7g3;

.field private h:Landroid/view/View$OnKeyListener;


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1223859
    invoke-virtual {p0}, LX/7g2;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x1020013

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabWidget;

    iput-object v0, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    .line 1223860
    iget-object v0, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    if-nez v0, :cond_0

    .line 1223861
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your TabHost must have a TabWidget whose id attribute is \'android.R.id.tabs\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1223862
    :cond_0
    new-instance v0, LX/7g5;

    invoke-direct {v0, p0}, LX/7g5;-><init>(LX/7g2;)V

    iput-object v0, p0, LX/7g2;->h:Landroid/view/View$OnKeyListener;

    .line 1223863
    :try_start_0
    const-class v3, Landroid/widget/TabWidget;

    .line 1223864
    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredClasses()[Ljava/lang/Class;

    move-result-object v4

    .line 1223865
    const/4 v1, 0x0

    .line 1223866
    array-length v5, v4

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    .line 1223867
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "OnTabSelectionChanged"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1223868
    :goto_1
    const-string v1, "setTabSelectionListener"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v0, v2, v4

    invoke-virtual {v3, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1223869
    new-instance v2, LX/7g6;

    invoke-direct {v2, p0}, LX/7g6;-><init>(LX/7g2;)V

    .line 1223870
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    .line 1223871
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1223872
    iget-object v2, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1223873
    :goto_2
    invoke-virtual {p0}, LX/7g2;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x1020011

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/7g2;->d:Landroid/widget/FrameLayout;

    .line 1223874
    iget-object v0, p0, LX/7g2;->d:Landroid/widget/FrameLayout;

    if-nez v0, :cond_2

    .line 1223875
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your TabHost must have a FrameLayout whose id attribute is \'android.R.id.tabcontent\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1223876
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1223877
    :catch_0
    move-exception v0

    .line 1223878
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 1223879
    :catch_1
    move-exception v0

    .line 1223880
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_2

    .line 1223881
    :catch_2
    move-exception v0

    .line 1223882
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2

    .line 1223883
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1223832
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    .line 1223833
    if-nez v4, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1223834
    iget-object v0, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    invoke-virtual {v0}, Landroid/widget/TabWidget;->getOrientation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1223835
    iget-object v0, p0, LX/7g2;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iget-object v2, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    invoke-virtual {v2}, Landroid/widget/TabWidget;->getTop()I

    move-result v2

    if-ge v0, v2, :cond_2

    const/4 v0, 0x3

    .line 1223836
    :goto_0
    move v0, v0

    .line 1223837
    packed-switch v0, :pswitch_data_1

    .line 1223838
    :pswitch_0
    const/16 v3, 0x13

    .line 1223839
    const/16 v2, 0x21

    .line 1223840
    const/4 v0, 0x2

    .line 1223841
    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    if-ne v5, v3, :cond_0

    iget-object v3, p0, LX/7g2;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1223842
    iget-object v2, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    iget v3, p0, LX/7g2;->a:I

    invoke-virtual {v2, v3}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 1223843
    invoke-virtual {p0, v0}, LX/7g2;->playSoundEffect(I)V

    .line 1223844
    :goto_2
    return v1

    .line 1223845
    :pswitch_1
    const/16 v2, 0x15

    .line 1223846
    const/16 v0, 0x11

    move v3, v2

    move v2, v0

    move v0, v1

    .line 1223847
    goto :goto_1

    .line 1223848
    :pswitch_2
    const/16 v3, 0x16

    .line 1223849
    const/16 v2, 0x42

    .line 1223850
    const/4 v0, 0x3

    .line 1223851
    goto :goto_1

    .line 1223852
    :pswitch_3
    const/16 v3, 0x14

    .line 1223853
    const/16 v2, 0x82

    .line 1223854
    const/4 v0, 0x4

    .line 1223855
    goto :goto_1

    :cond_0
    move v1, v4

    .line 1223856
    goto :goto_2

    .line 1223857
    :pswitch_4
    iget-object v0, p0, LX/7g2;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v0

    iget-object v2, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    invoke-virtual {v2}, Landroid/widget/TabWidget;->getLeft()I

    move-result v2

    if-ge v0, v2, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1223858
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final dispatchWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 1223829
    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1223830
    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchWindowFocusChanged(Z)V

    .line 1223831
    :cond_0
    return-void
.end method

.method public getCurrentTab()I
    .locals 1

    .prologue
    .line 1223784
    iget v0, p0, LX/7g2;->a:I

    return v0
.end method

.method public getCurrentTabTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1223825
    iget v0, p0, LX/7g2;->a:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/7g2;->a:I

    iget-object v1, p0, LX/7g2;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1223826
    iget-object v0, p0, LX/7g2;->e:Ljava/util/List;

    iget v1, p0, LX/7g2;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7g8;

    .line 1223827
    iget-object v1, v0, LX/7g8;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1223828
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentTabView()Landroid/view/View;
    .locals 2

    .prologue
    .line 1223822
    iget v0, p0, LX/7g2;->a:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/7g2;->a:I

    iget-object v1, p0, LX/7g2;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1223823
    iget-object v0, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    iget v1, p0, LX/7g2;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 1223824
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1223821
    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    return-object v0
.end method

.method public getTabContentView()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 1223820
    iget-object v0, p0, LX/7g2;->d:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getTabWidget()Landroid/widget/TabWidget;
    .locals 1

    .prologue
    .line 1223819
    iget-object v0, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4b5edf81

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1223815
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1223816
    invoke-virtual {p0}, LX/7g2;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1223817
    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1223818
    const/16 v1, 0x2d

    const v2, 0x190549ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4196ea00

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1223811
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1223812
    invoke-virtual {p0}, LX/7g2;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1223813
    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1223814
    const/16 v1, 0x2d

    const v2, 0x7734e2b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchModeChanged(Z)V
    .locals 2

    .prologue
    .line 1223807
    if-nez p1, :cond_1

    .line 1223808
    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1223809
    :cond_0
    iget-object v0, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    iget v1, p0, LX/7g2;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1223810
    :cond_1
    return-void
.end method

.method public final sendAccessibilityEvent(I)V
    .locals 0

    .prologue
    .line 1223806
    return-void
.end method

.method public setCurrentTab(I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1223790
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/7g2;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1223791
    :cond_0
    :goto_0
    return-void

    .line 1223792
    :cond_1
    iget v0, p0, LX/7g2;->a:I

    if-eq p1, v0, :cond_0

    .line 1223793
    iget v0, p0, LX/7g2;->a:I

    if-eq v0, v3, :cond_2

    .line 1223794
    iget-object v0, p0, LX/7g2;->e:Ljava/util/List;

    iget v1, p0, LX/7g2;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1223795
    :cond_2
    iput p1, p0, LX/7g2;->a:I

    .line 1223796
    iget-object v0, p0, LX/7g2;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7g8;

    .line 1223797
    iget-object v1, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    iget v2, p0, LX/7g2;->a:I

    invoke-virtual {v1, v2}, Landroid/widget/TabWidget;->focusCurrentTab(I)V

    .line 1223798
    iget-object v0, v0, LX/7g8;->b:LX/7g7;

    invoke-interface {v0}, LX/7g7;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/7g2;->f:Landroid/view/View;

    .line 1223799
    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1223800
    iget-object v0, p0, LX/7g2;->d:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/7g2;->f:Landroid/view/View;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1223801
    :cond_3
    iget-object v0, p0, LX/7g2;->c:Landroid/widget/TabWidget;

    invoke-virtual {v0}, Landroid/widget/TabWidget;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1223802
    iget-object v0, p0, LX/7g2;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1223803
    :cond_4
    iget-object v0, p0, LX/7g2;->g:LX/7g3;

    if-eqz v0, :cond_5

    .line 1223804
    iget-object v0, p0, LX/7g2;->g:LX/7g3;

    invoke-virtual {p0}, LX/7g2;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/7g3;->a(Ljava/lang/String;)V

    .line 1223805
    :cond_5
    goto :goto_0
.end method

.method public setOnTabChangedListener(LX/7g3;)V
    .locals 0

    .prologue
    .line 1223788
    iput-object p1, p0, LX/7g2;->g:LX/7g3;

    .line 1223789
    return-void
.end method

.method public setup(Landroid/app/LocalActivityManager;)V
    .locals 0

    .prologue
    .line 1223785
    invoke-virtual {p0}, LX/7g2;->a()V

    .line 1223786
    iput-object p1, p0, LX/7g2;->b:Landroid/app/LocalActivityManager;

    .line 1223787
    return-void
.end method
