.class public LX/87V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0fO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/86i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300220
    return-void
.end method

.method private a(LX/0Px;LX/0io;I)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ">(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;TModelData;I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1300221
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1300222
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1300223
    iget-object v5, p0, LX/87V;->b:LX/86i;

    invoke-static {p2}, LX/87R;->a(LX/0io;)Z

    move-result v6

    invoke-static {p2}, LX/2rf;->a(LX/0io;)Z

    move-result v7

    invoke-virtual {v5, v0, v6, v7}, LX/86i;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;ZZ)Z

    move-result v5

    move v5, v5

    .line 1300224
    if-eqz v5, :cond_1

    .line 1300225
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300226
    add-int/lit8 v0, v1, 0x1

    add-int/lit8 v5, p3, -0x1

    if-eq v1, v5, :cond_0

    .line 1300227
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1300228
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(LX/0is;I)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ":",
            "LX/0io;",
            ">(TModelData;I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300229
    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    check-cast p1, LX/0io;

    invoke-direct {p0, v0, p1, p2}, LX/87V;->a(LX/0Px;LX/0io;I)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/87V;
    .locals 5

    .prologue
    .line 1300230
    const-class v1, LX/87V;

    monitor-enter v1

    .line 1300231
    :try_start_0
    sget-object v0, LX/87V;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1300232
    sput-object v2, LX/87V;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1300233
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300234
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1300235
    new-instance p0, LX/87V;

    invoke-direct {p0}, LX/87V;-><init>()V

    .line 1300236
    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v3

    check-cast v3, LX/0fO;

    invoke-static {v0}, LX/86i;->a(LX/0QB;)LX/86i;

    move-result-object v4

    check-cast v4, LX/86i;

    .line 1300237
    iput-object v3, p0, LX/87V;->a:LX/0fO;

    iput-object v4, p0, LX/87V;->b:LX/86i;

    .line 1300238
    move-object v0, p0

    .line 1300239
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1300240
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/87V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1300241
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1300242
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/0Px;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1300243
    if-eqz p1, :cond_0

    invoke-static {p0, p1}, LX/87Q;->c(LX/0Px;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)Z
    .locals 1

    .prologue
    .line 1300244
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1300245
    iget-object v0, p0, LX/87V;->a:LX/0fO;

    .line 1300246
    invoke-static {v0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object p0

    invoke-virtual {p0}, LX/0i8;->r()I

    move-result p0

    move v0, p0

    .line 1300247
    return v0
.end method

.method public final a(LX/0is;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ":",
            "LX/0io;",
            ">(TModelData;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300248
    invoke-virtual {p0, p1}, LX/87V;->b(LX/0is;)LX/0Px;

    move-result-object v0

    .line 1300249
    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getStarId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/87V;->a(LX/0Px;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1300250
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getStarId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LX/87Q;->a(LX/0is;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0io;Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ">(TModelData;",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1300251
    invoke-virtual {p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v3

    .line 1300252
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300253
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1300254
    invoke-virtual {p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getDefaultInspirationLandingIndex()I

    move-result v0

    .line 1300255
    add-int/lit8 v4, v0, 0x1

    invoke-direct {p0, v3, p1, v4}, LX/87V;->a(LX/0Px;LX/0io;I)LX/0Px;

    move-result-object v3

    .line 1300256
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    if-lt v0, v4, :cond_2

    .line 1300257
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1300258
    :goto_1
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 1300259
    goto :goto_0

    :cond_1
    move v2, v1

    .line 1300260
    goto :goto_1

    :cond_2
    move v2, v0

    goto :goto_1
.end method

.method public final a(LX/0jL;LX/0is;LX/0is;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ":",
            "LX/0io;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSpec$SetsInspirationSwipeableModel;",
            ">(TMutation;TModelData;TModelData;)Z"
        }
    .end annotation

    .prologue
    .line 1300261
    invoke-interface {p3}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v5

    .line 1300262
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300263
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v3

    .line 1300264
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v4

    .line 1300265
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getStarId()Ljava/lang/String;

    move-result-object v2

    move-object v0, p2

    .line 1300266
    check-cast v0, LX/0io;

    move-object v1, p3

    check-cast v1, LX/0io;

    invoke-static {v0, v1}, LX/87R;->a(LX/0io;LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1300267
    invoke-static {p3}, LX/87Q;->a(LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    .line 1300268
    invoke-static {v0}, LX/87V;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)Z

    move-result v0

    move v0, v0

    .line 1300269
    if-eqz v0, :cond_1

    .line 1300270
    const-string v3, "1752514608329267"

    move-object v0, v2

    .line 1300271
    :goto_0
    invoke-interface {p3}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    move-object v2, v0

    move-object v0, v1

    .line 1300272
    :goto_1
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getStarId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1300273
    :cond_0
    check-cast p1, LX/0jL;

    invoke-static {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setSelectedId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setLastSelectedPreCaptureId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setStarId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Ljava/lang/Object;

    .line 1300274
    const/4 v0, 0x1

    .line 1300275
    :goto_2
    return v0

    .line 1300276
    :cond_1
    invoke-virtual {p0, p3}, LX/87V;->a(LX/0is;)LX/0Px;

    move-result-object v0

    invoke-static {v0, v3}, LX/87Q;->c(LX/0Px;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    move-object v0, v3

    .line 1300277
    goto :goto_0

    .line 1300278
    :cond_2
    check-cast p2, LX/0io;

    check-cast p3, LX/0io;

    invoke-static {p2, p3}, LX/87R;->b(LX/0io;LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1300279
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v3

    move-object v0, v4

    goto :goto_1

    .line 1300280
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move-object v0, v4

    goto :goto_1

    :cond_5
    move-object v0, v2

    goto :goto_0
.end method

.method public final b(LX/0is;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ":",
            "LX/0io;",
            ">(TModelData;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300281
    invoke-virtual {p0}, LX/87V;->a()I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/87V;->a(LX/0is;I)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/0is;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ":",
            "LX/0io;",
            ">(TModelData;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300282
    const v0, 0x7fffffff

    invoke-direct {p0, p1, v0}, LX/87V;->a(LX/0is;I)LX/0Px;

    move-result-object v0

    return-object v0
.end method
