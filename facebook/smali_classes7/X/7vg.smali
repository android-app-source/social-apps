.class public final LX/7vg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7vr;

.field public final synthetic b:LX/7vi;


# direct methods
.method public constructor <init>(LX/7vi;LX/7vr;)V
    .locals 0

    .prologue
    .line 1274968
    iput-object p1, p0, LX/7vg;->b:LX/7vi;

    iput-object p2, p0, LX/7vg;->a:LX/7vr;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1274969
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_0

    .line 1274970
    check-cast p1, LX/4Ua;

    iget-object v0, p0, LX/7vg;->a:LX/7vr;

    invoke-static {p1, v0}, LX/7vi;->b(LX/4Ua;LX/7vr;)V

    .line 1274971
    :goto_0
    return-void

    .line 1274972
    :cond_0
    iget-object v0, p0, LX/7vg;->a:LX/7vr;

    invoke-virtual {v0, p1}, LX/7vr;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1274973
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    .line 1274974
    iget-object v0, p0, LX/7vg;->b:LX/7vi;

    iget-object v1, p0, LX/7vg;->a:LX/7vr;

    invoke-static {v0, p1, v1}, LX/7vi;->a$redex0(LX/7vi;Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;LX/7vr;)V

    .line 1274975
    return-void
.end method
