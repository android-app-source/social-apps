.class public final LX/8W4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/8Sv;

.field public final synthetic b:LX/8W7;


# direct methods
.method public constructor <init>(LX/8W7;LX/8Sv;)V
    .locals 0

    .prologue
    .line 1353717
    iput-object p1, p0, LX/8W4;->b:LX/8W7;

    iput-object p2, p0, LX/8W4;->a:LX/8Sv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final d(Lorg/json/JSONObject;)V
    .locals 13

    .prologue
    .line 1353718
    :try_start_0
    const-string v8, "content"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    move-wide v0, v8
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1353719
    iget-object v2, p0, LX/8W4;->b:LX/8W7;

    iget-object v2, v2, LX/8W7;->c:LX/8TD;

    iget-object v3, p0, LX/8W4;->b:LX/8W7;

    iget-object v3, v3, LX/8W7;->d:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iget-object v3, p0, LX/8W4;->b:LX/8W7;

    iget-wide v6, v3, LX/8W7;->a:J

    sub-long/2addr v4, v6

    .line 1353720
    const-string v8, "quicksilver_average_frame_time"

    const/4 v9, 0x0

    invoke-static {v2, v8, v9}, LX/8TD;->b(LX/8TD;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "elapsed_time"

    long-to-float v10, v4

    const/high16 v11, 0x447a0000    # 1000.0f

    div-float/2addr v10, v11

    float-to-double v10, v10

    invoke-virtual {v8, v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "frame_time"

    invoke-virtual {v8, v9, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 1353721
    iget-object v9, v2, LX/8TD;->a:LX/0Zb;

    invoke-interface {v9, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1353722
    :goto_0
    return-void

    .line 1353723
    :catch_0
    move-exception v0

    .line 1353724
    iget-object v1, p0, LX/8W4;->b:LX/8W7;

    iget-object v1, v1, LX/8W7;->c:LX/8TD;

    sget-object v2, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid JSON content received by onAverageFrameTime: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
