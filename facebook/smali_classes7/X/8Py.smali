.class public final LX/8Py;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:LX/8Q1;


# direct methods
.method public constructor <init>(LX/8Q1;)V
    .locals 0

    .prologue
    .line 1342323
    iput-object p1, p0, LX/8Py;->a:LX/8Q1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 11

    .prologue
    .line 1342324
    sget-object v0, LX/8Q0;->FETCH_STICKY_GUARDRAIL:LX/8Q0;

    iget-object v0, v0, LX/8Q0;->desc:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1342325
    iget-object v0, p0, LX/8Py;->a:LX/8Q1;

    iget-object v0, v0, LX/8Q1;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lZ;

    invoke-virtual {v0}, LX/3lZ;->c()V

    .line 1342326
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1342327
    :cond_1
    sget-object v0, LX/8Q0;->ENABLE_NEWCOMER_AUDIENCE:LX/8Q0;

    iget-object v0, v0, LX/8Q0;->desc:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1342328
    iget-object v0, p0, LX/8Py;->a:LX/8Q1;

    .line 1342329
    iget-object v1, v0, LX/8Q1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object p0, LX/2bs;->j:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v1, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    sget-object p0, LX/2bs;->k:LX/0Tn;

    invoke-interface {v1, p0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1342330
    goto :goto_0

    .line 1342331
    :cond_2
    sget-object v0, LX/8Q0;->FORCE_AAA_TUX:LX/8Q0;

    iget-object v0, v0, LX/8Q0;->desc:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1342332
    iget-object v0, p0, LX/8Py;->a:LX/8Q1;

    .line 1342333
    iget-object v1, v0, LX/8Q1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object p0, LX/2bs;->h:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v1, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1342334
    goto :goto_0

    .line 1342335
    :cond_3
    sget-object v0, LX/8Q0;->FORCE_AAA_ONLY_ME:LX/8Q0;

    iget-object v0, v0, LX/8Q0;->desc:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1342336
    iget-object v0, p0, LX/8Py;->a:LX/8Q1;

    .line 1342337
    iget-object v1, v0, LX/8Q1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object p0, LX/2bs;->i:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v1, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1342338
    goto :goto_0

    .line 1342339
    :cond_4
    sget-object v0, LX/8Q0;->FETCH_AUDIENCE_EDUCATION:LX/8Q0;

    iget-object v0, v0, LX/8Q0;->desc:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1342340
    iget-object v0, p0, LX/8Py;->a:LX/8Q1;

    .line 1342341
    iget-object v2, v0, LX/8Q1;->c:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 1342342
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1342343
    iget-object v5, v2, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v6, "fetch_audience_info"

    sget-object v8, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v9, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v10, -0x7464effa

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    .line 1342344
    invoke-static {v2, v5}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v2, v5

    .line 1342345
    new-instance v3, LX/8Pz;

    invoke-direct {v3, v0}, LX/8Pz;-><init>(LX/8Q1;)V

    iget-object v4, v0, LX/8Q1;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1342346
    goto/16 :goto_0

    .line 1342347
    :cond_5
    sget-object v0, LX/8Q0;->REMOVE_DEFAULT_OVERRIDE:LX/8Q0;

    iget-object v0, v0, LX/8Q0;->desc:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1342348
    iget-object v0, p0, LX/8Py;->a:LX/8Q1;

    iget-object v0, v0, LX/8Q1;->d:LX/339;

    .line 1342349
    iget-object v1, v0, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2bs;->m:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1342350
    goto/16 :goto_0

    .line 1342351
    :cond_6
    sget-object v0, LX/8Q0;->LAUNCH_PROFILE_PHOTO_CHECKUP:LX/8Q0;

    iget-object v0, v0, LX/8Q0;->desc:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342352
    iget-object v0, p0, LX/8Py;->a:LX/8Q1;

    const-string v1, "PROFILE_PHOTO_CHECKUP"

    .line 1342353
    sget-object v2, LX/0ax;->dU:Ljava/lang/String;

    const-string v3, "fb4a_intern_settings"

    invoke-static {v2, v3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1342354
    iget-object v3, v0, LX/8Q1;->f:LX/17W;

    invoke-virtual {v0}, LX/8Q1;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1342355
    goto/16 :goto_0
.end method
