.class public final enum LX/7Ha;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Ha;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Ha;

.field public static final enum EXACT:LX/7Ha;

.field public static final enum PREFIX:LX/7Ha;

.field public static final enum UNSET:LX/7Ha;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1190846
    new-instance v0, LX/7Ha;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v2}, LX/7Ha;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ha;->UNSET:LX/7Ha;

    .line 1190847
    new-instance v0, LX/7Ha;

    const-string v1, "EXACT"

    invoke-direct {v0, v1, v3}, LX/7Ha;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ha;->EXACT:LX/7Ha;

    .line 1190848
    new-instance v0, LX/7Ha;

    const-string v1, "PREFIX"

    invoke-direct {v0, v1, v4}, LX/7Ha;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ha;->PREFIX:LX/7Ha;

    .line 1190849
    const/4 v0, 0x3

    new-array v0, v0, [LX/7Ha;

    sget-object v1, LX/7Ha;->UNSET:LX/7Ha;

    aput-object v1, v0, v2

    sget-object v1, LX/7Ha;->EXACT:LX/7Ha;

    aput-object v1, v0, v3

    sget-object v1, LX/7Ha;->PREFIX:LX/7Ha;

    aput-object v1, v0, v4

    sput-object v0, LX/7Ha;->$VALUES:[LX/7Ha;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1190845
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Ha;
    .locals 1

    .prologue
    .line 1190844
    const-class v0, LX/7Ha;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Ha;

    return-object v0
.end method

.method public static values()[LX/7Ha;
    .locals 1

    .prologue
    .line 1190843
    sget-object v0, LX/7Ha;->$VALUES:[LX/7Ha;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Ha;

    return-object v0
.end method
