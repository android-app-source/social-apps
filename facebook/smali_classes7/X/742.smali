.class public final enum LX/742;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/742;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/742;

.field public static final enum CHUNKED:LX/742;

.field public static final enum NON_CHUNKED:LX/742;

.field public static final enum NOT_RELEVANT:LX/742;

.field public static final enum PARALLEL_CHUNKED:LX/742;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1167389
    new-instance v0, LX/742;

    const-string v1, "NOT_RELEVANT"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/742;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/742;->NOT_RELEVANT:LX/742;

    .line 1167390
    new-instance v0, LX/742;

    const-string v1, "NON_CHUNKED"

    const-string v2, "non_chunked"

    invoke-direct {v0, v1, v4, v2}, LX/742;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/742;->NON_CHUNKED:LX/742;

    .line 1167391
    new-instance v0, LX/742;

    const-string v1, "CHUNKED"

    const-string v2, "chunked"

    invoke-direct {v0, v1, v5, v2}, LX/742;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/742;->CHUNKED:LX/742;

    .line 1167392
    new-instance v0, LX/742;

    const-string v1, "PARALLEL_CHUNKED"

    const-string v2, "parallel_chunked"

    invoke-direct {v0, v1, v6, v2}, LX/742;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/742;->PARALLEL_CHUNKED:LX/742;

    .line 1167393
    const/4 v0, 0x4

    new-array v0, v0, [LX/742;

    sget-object v1, LX/742;->NOT_RELEVANT:LX/742;

    aput-object v1, v0, v3

    sget-object v1, LX/742;->NON_CHUNKED:LX/742;

    aput-object v1, v0, v4

    sget-object v1, LX/742;->CHUNKED:LX/742;

    aput-object v1, v0, v5

    sget-object v1, LX/742;->PARALLEL_CHUNKED:LX/742;

    aput-object v1, v0, v6

    sput-object v0, LX/742;->$VALUES:[LX/742;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167384
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167385
    iput-object p3, p0, LX/742;->value:Ljava/lang/String;

    .line 1167386
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/742;
    .locals 1

    .prologue
    .line 1167388
    const-class v0, LX/742;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/742;

    return-object v0
.end method

.method public static values()[LX/742;
    .locals 1

    .prologue
    .line 1167387
    sget-object v0, LX/742;->$VALUES:[LX/742;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/742;

    return-object v0
.end method
