.class public LX/7PU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7PJ;


# instance fields
.field private final a:LX/19Z;

.field private final b:LX/7I0;

.field private final c:LX/7PS;

.field private final d:LX/0So;

.field private final e:LX/0oz;

.field private f:J


# direct methods
.method public constructor <init>(LX/0So;LX/19Z;LX/7I0;LX/0oz;)V
    .locals 2

    .prologue
    .line 1202910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202911
    iput-object p2, p0, LX/7PU;->a:LX/19Z;

    .line 1202912
    iput-object p3, p0, LX/7PU;->b:LX/7I0;

    .line 1202913
    iput-object p1, p0, LX/7PU;->d:LX/0So;

    .line 1202914
    new-instance v0, LX/7PS;

    invoke-direct {v0, p1, p3}, LX/7PS;-><init>(LX/0So;LX/7I0;)V

    iput-object v0, p0, LX/7PU;->c:LX/7PS;

    .line 1202915
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7PU;->f:J

    .line 1202916
    iput-object p4, p0, LX/7PU;->e:LX/0oz;

    .line 1202917
    return-void
.end method

.method private static a(IIJI)J
    .locals 2

    .prologue
    .line 1202918
    const-wide/16 v0, -0x1

    .line 1202919
    if-lez p0, :cond_0

    if-lez p1, :cond_0

    .line 1202920
    mul-int/lit16 v0, p4, 0x3e8

    add-int/2addr v0, p0

    int-to-float v0, v0

    int-to-float v1, p1

    div-float/2addr v0, v1

    long-to-float v1, p2

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 1202921
    :cond_0
    return-wide v0
.end method

.method private static a(LX/7Pw;LX/7IG;)J
    .locals 8

    .prologue
    .line 1202922
    const-wide/16 v0, -0x1

    .line 1202923
    if-eqz p1, :cond_0

    .line 1202924
    invoke-virtual {p1}, LX/7IG;->b()I

    move-result v2

    .line 1202925
    invoke-virtual {p0}, LX/7Pw;->b()J

    move-result-wide v4

    .line 1202926
    if-lez v2, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 1202927
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v4

    int-to-long v2, v2

    div-long/2addr v0, v2

    .line 1202928
    :cond_0
    return-wide v0
.end method


# virtual methods
.method public final a(LX/7Pw;J)J
    .locals 12

    .prologue
    .line 1202929
    iget-object v0, p0, LX/7PU;->a:LX/19Z;

    iget v1, p1, LX/7Pw;->a:I

    invoke-virtual {v0, v1}, LX/19Z;->b(I)LX/7IG;

    move-result-object v10

    .line 1202930
    iget-object v0, p0, LX/7PU;->a:LX/19Z;

    .line 1202931
    iget-boolean v1, v0, LX/19Z;->d:Z

    move v0, v1

    .line 1202932
    if-eqz v0, :cond_6

    iget-object v0, p0, LX/7PU;->b:LX/7I0;

    iget-boolean v0, v0, LX/7I0;->o:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    .line 1202933
    :goto_0
    iget-object v1, p0, LX/7PU;->b:LX/7I0;

    iget-object v1, v1, LX/7I0;->t:LX/0p3;

    sget-object v2, LX/0p3;->POOR:LX/0p3;

    invoke-virtual {v1, v2}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/7PU;->e:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    iget-object v2, p0, LX/7PU;->b:LX/7I0;

    iget-object v2, v2, LX/7I0;->t:LX/0p3;

    invoke-virtual {v1, v2}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-ltz v1, :cond_7

    :cond_0
    const/4 v1, 0x1

    .line 1202934
    :goto_1
    if-nez v10, :cond_1

    iget-object v2, p0, LX/7PU;->b:LX/7I0;

    iget-boolean v2, v2, LX/7I0;->k:Z

    if-eqz v2, :cond_4

    :cond_1
    if-eqz v10, :cond_2

    invoke-virtual {v10}, LX/7IG;->d()LX/7IJ;

    move-result-object v2

    sget-object v3, LX/7IJ;->INSEEK:LX/7IJ;

    if-eq v2, v3, :cond_4

    :cond_2
    if-eqz v10, :cond_3

    invoke-virtual {v10}, LX/7IG;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7PU;->b:LX/7I0;

    iget-boolean v2, v2, LX/7I0;->q:Z

    if-eqz v2, :cond_4

    :cond_3
    if-eqz v1, :cond_4

    if-eqz v0, :cond_8

    .line 1202935
    :cond_4
    const-wide/16 v2, -0x2

    .line 1202936
    :cond_5
    :goto_2
    return-wide v2

    .line 1202937
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 1202938
    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    .line 1202939
    :cond_8
    if-eqz v10, :cond_9

    invoke-virtual {v10}, LX/7IG;->d()LX/7IJ;

    move-result-object v0

    sget-object v1, LX/7IJ;->STOP:LX/7IJ;

    if-ne v0, v1, :cond_9

    iget-object v0, p0, LX/7PU;->b:LX/7I0;

    iget-boolean v0, v0, LX/7I0;->l:Z

    if-eqz v0, :cond_9

    .line 1202940
    const-wide/16 v2, -0x1

    goto :goto_2

    .line 1202941
    :cond_9
    if-eqz v10, :cond_a

    invoke-virtual {v10}, LX/7IG;->d()LX/7IJ;

    move-result-object v0

    sget-object v1, LX/7IJ;->PAUSED:LX/7IJ;

    if-ne v0, v1, :cond_a

    iget-object v0, p0, LX/7PU;->b:LX/7I0;

    iget-boolean v0, v0, LX/7I0;->m:Z

    if-eqz v0, :cond_a

    .line 1202942
    const-wide/16 v2, 0x0

    goto :goto_2

    .line 1202943
    :cond_a
    const-wide/16 v8, -0x1

    .line 1202944
    iget-object v0, p0, LX/7PU;->b:LX/7I0;

    iget v0, v0, LX/7I0;->n:I

    if-lez v0, :cond_b

    if-eqz v10, :cond_b

    .line 1202945
    invoke-virtual {v10}, LX/7IG;->a()I

    move-result v0

    invoke-virtual {v10}, LX/7IG;->b()I

    move-result v1

    invoke-virtual {p1}, LX/7Pw;->b()J

    move-result-wide v2

    iget-object v4, p0, LX/7PU;->b:LX/7I0;

    iget v4, v4, LX/7I0;->n:I

    invoke-static {v0, v1, v2, v3, v4}, LX/7PU;->a(IIJI)J

    move-result-wide v8

    .line 1202946
    :cond_b
    const/4 v4, -0x1

    .line 1202947
    const/4 v5, -0x1

    .line 1202948
    if-eqz v10, :cond_c

    .line 1202949
    iget v0, v10, LX/7IG;->c:I

    move v4, v0

    .line 1202950
    iget v0, v10, LX/7IG;->d:I

    move v5, v0

    .line 1202951
    :cond_c
    iget-object v0, p0, LX/7PU;->c:LX/7PS;

    invoke-static {p1, v10}, LX/7PU;->a(LX/7Pw;LX/7IG;)J

    move-result-wide v6

    move-object v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v9}, LX/7PS;->a(LX/7Pw;JIIJJ)J

    move-result-wide v2

    .line 1202952
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_d

    .line 1202953
    iget-object v0, p0, LX/7PU;->b:LX/7I0;

    iget v0, v0, LX/7I0;->f:I

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1202954
    iget-object v2, p0, LX/7PU;->b:LX/7I0;

    iget v2, v2, LX/7I0;->d:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1202955
    :cond_d
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_f

    .line 1202956
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_5

    .line 1202957
    iget-object v0, p0, LX/7PU;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v4, p0, LX/7PU;->f:J

    sub-long/2addr v0, v4

    .line 1202958
    iget-object v4, p0, LX/7PU;->b:LX/7I0;

    iget-wide v4, v4, LX/7I0;->r:J

    cmp-long v4, v0, v4

    if-lez v4, :cond_5

    .line 1202959
    iget-object v2, p0, LX/7PU;->b:LX/7I0;

    iget-wide v2, v2, LX/7I0;->s:J

    .line 1202960
    invoke-static {p1, v10}, LX/7PU;->a(LX/7Pw;LX/7IG;)J

    move-result-wide v4

    .line 1202961
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_f

    iget-object v6, p0, LX/7PU;->b:LX/7I0;

    iget-boolean v6, v6, LX/7I0;->u:Z

    if-eqz v6, :cond_f

    .line 1202962
    iget-wide v6, p0, LX/7PU;->f:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_e

    .line 1202963
    :goto_3
    mul-long/2addr v0, v4

    long-to-float v0, v0

    iget-object v1, p0, LX/7PU;->b:LX/7I0;

    iget v1, v1, LX/7I0;->j:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    float-to-long v0, v0

    .line 1202964
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1202965
    invoke-static {v0, v1}, LX/7PS;->a(J)J

    move-result-wide v0

    .line 1202966
    :goto_4
    iget-object v2, p0, LX/7PU;->d:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/7PU;->f:J

    move-wide v2, v0

    goto/16 :goto_2

    .line 1202967
    :cond_e
    iget-object v0, p0, LX/7PU;->b:LX/7I0;

    iget-wide v0, v0, LX/7I0;->r:J

    goto :goto_3

    :cond_f
    move-wide v0, v2

    goto :goto_4
.end method
