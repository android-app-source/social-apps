.class public final LX/8Qd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1343471
    iput-object p1, p0, LX/8Qd;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1343464
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1343465
    check-cast p1, LX/0Px;

    .line 1343466
    iget-object v0, p0, LX/8Qd;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    const v1, 0x7f0812ff

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1343467
    new-instance v1, LX/8vN;

    invoke-direct {v1, v0, p1}, LX/8vN;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 1343468
    iget-object v0, p0, LX/8Qd;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    sget v2, LX/8Qg;->b:I

    invoke-virtual {v0, v2, v1}, LX/8tB;->a(ILX/621;)V

    .line 1343469
    iget-object v0, p0, LX/8Qd;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->d()V

    .line 1343470
    return-void
.end method
