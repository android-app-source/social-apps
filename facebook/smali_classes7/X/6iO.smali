.class public final LX/6iO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6j5;

.field public b:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public c:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field public d:Lcom/facebook/messaging/model/messages/MessagesCollection;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:J

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1129389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129390
    sget-object v0, LX/6j5;->UNSPECIFIED:LX/6j5;

    iput-object v0, p0, LX/6iO;->a:LX/6j5;

    .line 1129391
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1129392
    iput-object v0, p0, LX/6iO;->h:LX/0Px;

    .line 1129393
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 2

    .prologue
    .line 1129401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129402
    sget-object v0, LX/6j5;->UNSPECIFIED:LX/6j5;

    iput-object v0, p0, LX/6iO;->a:LX/6j5;

    .line 1129403
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1129404
    iput-object v0, p0, LX/6iO;->h:LX/0Px;

    .line 1129405
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->b:LX/6j5;

    iput-object v0, p0, LX/6iO;->a:LX/6j5;

    .line 1129406
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iput-object v0, p0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129407
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-object v0, p0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 1129408
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    iput-object v0, p0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 1129409
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    iput-object v0, p0, LX/6iO;->e:LX/0Px;

    .line 1129410
    iget-wide v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    iput-wide v0, p0, LX/6iO;->g:J

    .line 1129411
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->h:LX/0Px;

    iput-object v0, p0, LX/6iO;->h:LX/0Px;

    .line 1129412
    return-void
.end method


# virtual methods
.method public final a(J)LX/6iO;
    .locals 1

    .prologue
    .line 1129399
    iput-wide p1, p0, LX/6iO;->g:J

    .line 1129400
    return-object p0
.end method

.method public final a(LX/0Px;)LX/6iO;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/6iO;"
        }
    .end annotation

    .prologue
    .line 1129413
    iput-object p1, p0, LX/6iO;->e:LX/0Px;

    .line 1129414
    return-object p0
.end method

.method public final a(Lcom/facebook/fbservice/results/DataFetchDisposition;)LX/6iO;
    .locals 0

    .prologue
    .line 1129397
    iput-object p1, p0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129398
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 12

    .prologue
    .line 1129396
    new-instance v1, Lcom/facebook/messaging/service/model/FetchThreadResult;

    iget-object v2, p0, LX/6iO;->a:LX/6j5;

    iget-object v3, p0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-object v4, p0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v5, p0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v6, p0, LX/6iO;->f:Ljava/util/Map;

    iget-object v7, p0, LX/6iO;->e:LX/0Px;

    iget-wide v8, p0, LX/6iO;->g:J

    iget-object v10, p0, LX/6iO;->h:LX/0Px;

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/facebook/messaging/service/model/FetchThreadResult;-><init>(LX/6j5;Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;Ljava/util/Map;LX/0Px;JLX/0Px;B)V

    return-object v1
.end method

.method public final b(LX/0Px;)LX/6iO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
            ">;)",
            "LX/6iO;"
        }
    .end annotation

    .prologue
    .line 1129394
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/6iO;->h:LX/0Px;

    .line 1129395
    return-object p0
.end method
