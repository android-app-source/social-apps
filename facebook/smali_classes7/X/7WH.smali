.class public final LX/7WH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1216221
    const-wide/16 v8, 0x0

    .line 1216222
    const/4 v7, 0x0

    .line 1216223
    const/4 v6, 0x0

    .line 1216224
    const/4 v5, 0x0

    .line 1216225
    const/4 v4, 0x0

    .line 1216226
    const/4 v3, 0x0

    .line 1216227
    const/4 v2, 0x0

    .line 1216228
    const/4 v1, 0x0

    .line 1216229
    const/4 v0, 0x0

    .line 1216230
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1216231
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1216232
    const/4 v0, 0x0

    .line 1216233
    :goto_0
    return v0

    .line 1216234
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_7

    .line 1216235
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1216236
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1216237
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1216238
    const-string v4, "expiration"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1216239
    const/4 v0, 0x1

    .line 1216240
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1216241
    :cond_1
    const-string v4, "friendly_names_to_rewrite"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1216242
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v0

    move v12, v0

    goto :goto_1

    .line 1216243
    :cond_2
    const-string v4, "paid_mqtt_rewrite_rules"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1216244
    invoke-static {p0, p1}, LX/7XU;->a(LX/15w;LX/186;)I

    move-result v0

    move v11, v0

    goto :goto_1

    .line 1216245
    :cond_3
    const-string v4, "remaining_quota"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1216246
    const/4 v0, 0x1

    .line 1216247
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v0

    move v10, v4

    goto :goto_1

    .line 1216248
    :cond_4
    const-string v4, "rewrite_rule"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1216249
    invoke-static {p0, p1}, LX/7WG;->a(LX/15w;LX/186;)I

    move-result v0

    move v9, v0

    goto :goto_1

    .line 1216250
    :cond_5
    const-string v4, "total_quota"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1216251
    const/4 v0, 0x1

    .line 1216252
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v0

    move v8, v4

    goto :goto_1

    .line 1216253
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1216254
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1216255
    if-eqz v1, :cond_8

    .line 1216256
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1216257
    :cond_8
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1216258
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1216259
    if-eqz v7, :cond_9

    .line 1216260
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v10, v1}, LX/186;->a(III)V

    .line 1216261
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1216262
    if-eqz v6, :cond_a

    .line 1216263
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v8, v1}, LX/186;->a(III)V

    .line 1216264
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v10, v5

    move v11, v6

    move v12, v7

    move v6, v0

    move v7, v1

    move v1, v2

    move v13, v3

    move-wide v2, v8

    move v9, v4

    move v8, v13

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1216265
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1216266
    invoke-virtual {p0, p1, v3, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1216267
    cmp-long v2, v0, v6

    if-eqz v2, :cond_0

    .line 1216268
    const-string v2, "expiration"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1216269
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1216270
    :cond_0
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1216271
    if-eqz v0, :cond_1

    .line 1216272
    const-string v0, "friendly_names_to_rewrite"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1216273
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1216274
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1216275
    if-eqz v0, :cond_2

    .line 1216276
    const-string v1, "paid_mqtt_rewrite_rules"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1216277
    invoke-static {p0, v0, p2, p3}, LX/7XU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1216278
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1216279
    if-eqz v0, :cond_3

    .line 1216280
    const-string v1, "remaining_quota"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1216281
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1216282
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1216283
    if-eqz v0, :cond_4

    .line 1216284
    const-string v1, "rewrite_rule"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1216285
    invoke-static {p0, v0, p2}, LX/7WG;->a(LX/15i;ILX/0nX;)V

    .line 1216286
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1216287
    if-eqz v0, :cond_5

    .line 1216288
    const-string v1, "total_quota"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1216289
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1216290
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1216291
    return-void
.end method
