.class public final LX/7Ki;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/7Kk;


# direct methods
.method public constructor <init>(LX/7Kk;)V
    .locals 0

    .prologue
    .line 1196216
    iput-object p1, p0, LX/7Ki;->a:LX/7Kk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1196217
    iget-object v0, p0, LX/7Ki;->a:LX/7Kk;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1196218
    :try_start_0
    invoke-static {v0, p1}, LX/7Kk;->b(LX/7Kk;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1196219
    :goto_0
    return-void

    .line 1196220
    :catch_0
    move-exception v1

    .line 1196221
    sget-object v4, LX/7Kk;->a:Ljava/lang/Class;

    const-string v5, "tryNotify(%s): %s"

    new-array p0, v2, [Ljava/lang/Object;

    aput-object p1, p0, v3

    invoke-static {v4, v1, v5, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1196222
    if-eqz p1, :cond_1

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v4, Landroid/os/TransactionTooLargeException;

    if-ne v1, v4, :cond_1

    move v1, v2

    .line 1196223
    :goto_1
    if-eqz v1, :cond_0

    .line 1196224
    const/4 v1, 0x0

    :try_start_1
    invoke-static {v0, v1}, LX/7Kk;->b(LX/7Kk;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1196225
    :catch_1
    move-exception v1

    .line 1196226
    sget-object v4, LX/7Kk;->a:Ljava/lang/Class;

    const-string v5, "tryNotify(%s): Subsequent attempt"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v4, v1, v5, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1196227
    :cond_0
    invoke-static {v0}, LX/7Kk;->h(LX/7Kk;)V

    goto :goto_0

    :cond_1
    move v1, v3

    .line 1196228
    goto :goto_1
.end method
