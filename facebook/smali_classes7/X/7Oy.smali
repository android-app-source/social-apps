.class public final LX/7Oy;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field public final synthetic a:LX/7P0;


# direct methods
.method public constructor <init>(LX/7P0;)V
    .locals 0

    .prologue
    .line 1202409
    iput-object p1, p0, LX/7Oy;->a:LX/7P0;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 1202410
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget-object v1, v0, LX/7P0;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 1202411
    :try_start_0
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    const/4 v2, 0x1

    .line 1202412
    iput-boolean v2, v0, LX/7P0;->h:Z

    .line 1202413
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget-object v0, v0, LX/7P0;->j:Ljava/lang/Object;

    const v2, 0x67a0e592

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1202414
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    invoke-static {v0}, LX/7P0;->a(LX/7P0;)V

    .line 1202415
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 1202408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    .line 1202381
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget-object v1, v0, LX/7P0;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 1202382
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget v0, v0, LX/7P0;->g:I

    if-nez v0, :cond_1

    .line 1202383
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget-boolean v0, v0, LX/7P0;->i:Z

    if-eqz v0, :cond_0

    .line 1202384
    const/4 v0, -0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202385
    :goto_1
    return v0

    .line 1202386
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget-object v0, v0, LX/7P0;->j:Ljava/lang/Object;

    const v2, 0x1a720b17

    invoke-static {v0, v2}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1202387
    :catch_0
    move-exception v0

    .line 1202388
    :try_start_2
    new-instance v2, Ljava/io/InterruptedIOException;

    invoke-direct {v2}, Ljava/io/InterruptedIOException;-><init>()V

    invoke-virtual {v2, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/InterruptedIOException;

    throw v0

    .line 1202389
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1202390
    :cond_1
    :try_start_3
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget v2, v0, LX/7P0;->f:I

    .line 1202391
    iget-object v0, p0, LX/7Oy;->a:LX/7P0;

    iget v0, v0, LX/7P0;->g:I

    .line 1202392
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1202393
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1202394
    iget-object v1, p0, LX/7Oy;->a:LX/7P0;

    iget-object v1, v1, LX/7P0;->e:[B

    array-length v1, v1

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1202395
    iget-object v3, p0, LX/7Oy;->a:LX/7P0;

    iget-object v3, v3, LX/7P0;->e:[B

    invoke-static {v3, v2, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1202396
    if-ge v1, v0, :cond_2

    .line 1202397
    add-int v3, p2, v1

    .line 1202398
    sub-int v1, v0, v1

    .line 1202399
    iget-object v4, p0, LX/7Oy;->a:LX/7P0;

    iget-object v4, v4, LX/7P0;->e:[B

    const/4 v5, 0x0

    invoke-static {v4, v5, p1, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1202400
    :cond_2
    add-int v1, v2, v0

    iget-object v2, p0, LX/7Oy;->a:LX/7P0;

    iget-object v2, v2, LX/7P0;->e:[B

    array-length v2, v2

    rem-int/2addr v1, v2

    .line 1202401
    iget-object v2, p0, LX/7Oy;->a:LX/7P0;

    iget-object v2, v2, LX/7P0;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 1202402
    :try_start_4
    iget-object v3, p0, LX/7Oy;->a:LX/7P0;

    .line 1202403
    iput v1, v3, LX/7P0;->f:I

    .line 1202404
    iget-object v1, p0, LX/7Oy;->a:LX/7P0;

    .line 1202405
    iget v3, v1, LX/7P0;->g:I

    sub-int/2addr v3, v0

    iput v3, v1, LX/7P0;->g:I

    .line 1202406
    iget-object v1, p0, LX/7Oy;->a:LX/7P0;

    iget-object v1, v1, LX/7P0;->j:Ljava/lang/Object;

    const v3, 0x5b88c3d7

    invoke-static {v1, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1202407
    monitor-exit v2

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method
