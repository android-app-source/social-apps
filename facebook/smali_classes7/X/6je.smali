.class public LX/6je;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final addedAdmins:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;"
        }
    .end annotation
.end field

.field public final messageMetadata:LX/6kn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1131776
    new-instance v0, LX/1sv;

    const-string v1, "DeltaAdminAddedToGroupThread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6je;->b:LX/1sv;

    .line 1131777
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6je;->c:LX/1sw;

    .line 1131778
    new-instance v0, LX/1sw;

    const-string v1, "addedAdmins"

    const/16 v2, 0xf

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6je;->d:LX/1sw;

    .line 1131779
    sput-boolean v4, LX/6je;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kn;",
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1131772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1131773
    iput-object p1, p0, LX/6je;->messageMetadata:LX/6kn;

    .line 1131774
    iput-object p2, p0, LX/6je;->addedAdmins:Ljava/util/List;

    .line 1131775
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1131767
    iget-object v0, p0, LX/6je;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1131768
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6je;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1131769
    :cond_0
    iget-object v0, p0, LX/6je;->addedAdmins:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1131770
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'addedAdmins\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6je;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1131771
    :cond_1
    return-void
.end method

.method public static b(LX/1su;)LX/6je;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1131745
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v3, v0

    .line 1131746
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 1131747
    iget-byte v4, v2, LX/1sw;->b:B

    if-eqz v4, :cond_5

    .line 1131748
    iget-short v4, v2, LX/1sw;->c:S

    packed-switch v4, :pswitch_data_0

    .line 1131749
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131750
    :pswitch_0
    iget-byte v4, v2, LX/1sw;->b:B

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 1131751
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 1131752
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131753
    :pswitch_1
    iget-byte v4, v2, LX/1sw;->b:B

    const/16 v5, 0xf

    if-ne v4, v5, :cond_4

    .line 1131754
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v4

    .line 1131755
    new-instance v2, Ljava/util/ArrayList;

    iget v0, v4, LX/1u3;->b:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 1131756
    :goto_1
    iget v5, v4, LX/1u3;->b:I

    if-gez v5, :cond_2

    invoke-static {}, LX/1su;->t()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1131757
    :cond_1
    invoke-static {p0}, LX/6kt;->b(LX/1su;)LX/6kt;

    move-result-object v5

    .line 1131758
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1131759
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1131760
    :cond_2
    iget v5, v4, LX/1u3;->b:I

    if-lt v0, v5, :cond_1

    :cond_3
    move-object v0, v2

    .line 1131761
    goto :goto_0

    .line 1131762
    :cond_4
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131763
    :cond_5
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1131764
    new-instance v1, LX/6je;

    invoke-direct {v1, v3, v0}, LX/6je;-><init>(LX/6kn;Ljava/util/List;)V

    .line 1131765
    invoke-direct {v1}, LX/6je;->a()V

    .line 1131766
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1131717
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1131718
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1131719
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1131720
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaAdminAddedToGroupThread"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1131721
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131722
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131723
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131724
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131725
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131726
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131727
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131728
    iget-object v4, p0, LX/6je;->messageMetadata:LX/6kn;

    if-nez v4, :cond_3

    .line 1131729
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131730
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131731
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131732
    const-string v4, "addedAdmins"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131733
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131734
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131735
    iget-object v0, p0, LX/6je;->addedAdmins:Ljava/util/List;

    if-nez v0, :cond_4

    .line 1131736
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131737
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131738
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131739
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1131740
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1131741
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1131742
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1131743
    :cond_3
    iget-object v4, p0, LX/6je;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1131744
    :cond_4
    iget-object v0, p0, LX/6je;->addedAdmins:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1131678
    invoke-direct {p0}, LX/6je;->a()V

    .line 1131679
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1131680
    iget-object v0, p0, LX/6je;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1131681
    sget-object v0, LX/6je;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131682
    iget-object v0, p0, LX/6je;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1131683
    :cond_0
    iget-object v0, p0, LX/6je;->addedAdmins:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1131684
    sget-object v0, LX/6je;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131685
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6je;->addedAdmins:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1131686
    iget-object v0, p0, LX/6je;->addedAdmins:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kt;

    .line 1131687
    invoke-virtual {v0, p1}, LX/6kt;->a(LX/1su;)V

    goto :goto_0

    .line 1131688
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1131689
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1131690
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1131695
    if-nez p1, :cond_1

    .line 1131696
    :cond_0
    :goto_0
    return v0

    .line 1131697
    :cond_1
    instance-of v1, p1, LX/6je;

    if-eqz v1, :cond_0

    .line 1131698
    check-cast p1, LX/6je;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1131699
    if-nez p1, :cond_3

    .line 1131700
    :cond_2
    :goto_1
    move v0, v2

    .line 1131701
    goto :goto_0

    .line 1131702
    :cond_3
    iget-object v0, p0, LX/6je;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1131703
    :goto_2
    iget-object v3, p1, LX/6je;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1131704
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1131705
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1131706
    iget-object v0, p0, LX/6je;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6je;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1131707
    :cond_5
    iget-object v0, p0, LX/6je;->addedAdmins:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1131708
    :goto_4
    iget-object v3, p1, LX/6je;->addedAdmins:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1131709
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1131710
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1131711
    iget-object v0, p0, LX/6je;->addedAdmins:Ljava/util/List;

    iget-object v3, p1, LX/6je;->addedAdmins:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1131712
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1131713
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1131714
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1131715
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1131716
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1131694
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1131691
    sget-boolean v0, LX/6je;->a:Z

    .line 1131692
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6je;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1131693
    return-object v0
.end method
