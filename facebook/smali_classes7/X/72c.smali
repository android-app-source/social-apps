.class public LX/72c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6vm;


# instance fields
.field public final a:LX/72f;

.field public final b:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:Lcom/facebook/payments/shipping/model/MailingAddress;

.field public final h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;


# direct methods
.method public constructor <init>(LX/72b;)V
    .locals 2

    .prologue
    .line 1164719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164720
    iget-object v0, p1, LX/72b;->a:LX/72f;

    iput-object v0, p0, LX/72c;->a:LX/72f;

    .line 1164721
    iget-object v0, p1, LX/72b;->a:LX/72f;

    sget-object v1, LX/72f;->CHECKOUT:LX/72f;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, LX/72b;->b:Landroid/content/Intent;

    :goto_0
    iput-object v0, p0, LX/72c;->b:Landroid/content/Intent;

    .line 1164722
    iget v0, p1, LX/72b;->c:I

    iput v0, p0, LX/72c;->c:I

    .line 1164723
    iget-object v0, p1, LX/72b;->d:Ljava/lang/String;

    iput-object v0, p0, LX/72c;->d:Ljava/lang/String;

    .line 1164724
    iget-object v0, p1, LX/72b;->e:Ljava/lang/String;

    iput-object v0, p0, LX/72c;->e:Ljava/lang/String;

    .line 1164725
    iget-boolean v0, p1, LX/72b;->f:Z

    iput-boolean v0, p0, LX/72c;->f:Z

    .line 1164726
    iget-object v0, p1, LX/72b;->g:Lcom/facebook/payments/shipping/model/MailingAddress;

    iput-object v0, p0, LX/72c;->g:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1164727
    iget-object v0, p1, LX/72b;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, LX/72c;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1164728
    return-void

    .line 1164729
    :cond_0
    iget-object v0, p1, LX/72b;->b:Landroid/content/Intent;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    goto :goto_0
.end method

.method public static newBuilder()LX/72b;
    .locals 1

    .prologue
    .line 1164730
    new-instance v0, LX/72b;

    invoke-direct {v0}, LX/72b;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/71I;
    .locals 1

    .prologue
    .line 1164731
    sget-object v0, LX/71I;->SHIPPING_ADDRESS:LX/71I;

    return-object v0
.end method
