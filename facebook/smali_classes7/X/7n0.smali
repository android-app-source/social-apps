.class public final LX/7n0;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1237588
    const-class v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;

    const v0, 0x728b9ad

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchEventsCalendarQuery"

    const-string v6, "337702f4a7c64b17dc32648da6f3ac25"

    const-string v7, "viewer"

    const-string v8, "10155211906881729"

    const-string v9, "10155259087401729"

    .line 1237589
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1237590
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1237591
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1237592
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1237593
    sparse-switch v0, :sswitch_data_0

    .line 1237594
    :goto_0
    return-object p1

    .line 1237595
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1237596
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1237597
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1237598
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1237599
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1237600
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1237601
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1237602
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1237603
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1237604
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1237605
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1237606
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1237607
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1237608
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1237609
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1237610
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1237611
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7bc0b807 -> :sswitch_10
        -0x7448a3b3 -> :sswitch_8
        -0x41a91745 -> :sswitch_d
        -0x3b558d3 -> :sswitch_f
        0x101fb19 -> :sswitch_0
        0x180aba4 -> :sswitch_5
        0x5ced2b0 -> :sswitch_7
        0x651874e -> :sswitch_c
        0x1566e1d5 -> :sswitch_6
        0x291d8de0 -> :sswitch_e
        0x3052e0ff -> :sswitch_1
        0x4b46b5f1 -> :sswitch_2
        0x4c6d50cb -> :sswitch_9
        0x5f424068 -> :sswitch_4
        0x61bc9553 -> :sswitch_a
        0x6d2645b9 -> :sswitch_3
        0x73a026b5 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1237612
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1237613
    :goto_1
    return v0

    .line 1237614
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1237615
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
