.class public LX/8OJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/8KA;

.field private final B:LX/0cX;

.field private C:LX/0TD;

.field public final b:Ljava/lang/Object;

.field private final c:Ljava/util/concurrent/locks/Lock;

.field private final d:LX/8Jw;

.field public final e:LX/8KY;

.field private final f:LX/43C;

.field private final g:LX/8Ni;

.field private final h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/11H;

.field private final k:LX/8LX;

.field private final l:LX/8NH;

.field private final m:LX/03V;

.field private final n:LX/8KW;

.field private final o:LX/0SG;

.field public final p:LX/0ad;

.field private final q:Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

.field private final r:Ljava/util/concurrent/ExecutorService;

.field private final s:LX/8GT;

.field public t:Z

.field private final u:LX/8GZ;

.field private final v:LX/7zS;

.field private final w:LX/8OU;

.field private x:LX/0b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/1EZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final z:LX/0UR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1338737
    const-class v0, LX/8OJ;

    sput-object v0, LX/8OJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/8Jw;LX/8KY;LX/43C;LX/8Ni;Lcom/facebook/photos/imageprocessing/FiltersEngine;LX/0Or;LX/11H;LX/8LX;LX/8NH;LX/03V;LX/8KW;LX/0SG;Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;LX/8GZ;Ljava/util/concurrent/ExecutorService;LX/8GT;LX/7zS;LX/0ad;LX/8OU;LX/0UR;LX/8KA;LX/0cX;)V
    .locals 2
    .param p15    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8Jw;",
            "LX/8KY;",
            "LX/43C;",
            "LX/8Ni;",
            "Lcom/facebook/photos/imageprocessing/FiltersEngine;",
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/8LX;",
            "LX/8NH;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/8KW;",
            "LX/0SG;",
            "Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;",
            "LX/8GZ;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/8GT;",
            "LX/7zS;",
            "LX/0ad;",
            "LX/8OU;",
            "LX/0UR;",
            "LX/8KA;",
            "LX/0cX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1338738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338739
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, LX/8OJ;->b:Ljava/lang/Object;

    .line 1338740
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, LX/8OJ;->c:Ljava/util/concurrent/locks/Lock;

    .line 1338741
    iput-object p1, p0, LX/8OJ;->d:LX/8Jw;

    .line 1338742
    iput-object p2, p0, LX/8OJ;->e:LX/8KY;

    .line 1338743
    iput-object p3, p0, LX/8OJ;->f:LX/43C;

    .line 1338744
    iput-object p4, p0, LX/8OJ;->g:LX/8Ni;

    .line 1338745
    iput-object p5, p0, LX/8OJ;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1338746
    iput-object p6, p0, LX/8OJ;->i:LX/0Or;

    .line 1338747
    move-object/from16 v0, p17

    iput-object v0, p0, LX/8OJ;->v:LX/7zS;

    .line 1338748
    iput-object p7, p0, LX/8OJ;->j:LX/11H;

    .line 1338749
    iput-object p8, p0, LX/8OJ;->k:LX/8LX;

    .line 1338750
    iput-object p9, p0, LX/8OJ;->l:LX/8NH;

    .line 1338751
    iput-object p10, p0, LX/8OJ;->m:LX/03V;

    .line 1338752
    iput-object p11, p0, LX/8OJ;->n:LX/8KW;

    .line 1338753
    iput-object p12, p0, LX/8OJ;->o:LX/0SG;

    .line 1338754
    move-object/from16 v0, p18

    iput-object v0, p0, LX/8OJ;->p:LX/0ad;

    .line 1338755
    iput-object p13, p0, LX/8OJ;->q:Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    .line 1338756
    move-object/from16 v0, p14

    iput-object v0, p0, LX/8OJ;->u:LX/8GZ;

    .line 1338757
    move-object/from16 v0, p15

    iput-object v0, p0, LX/8OJ;->r:Ljava/util/concurrent/ExecutorService;

    .line 1338758
    move-object/from16 v0, p16

    iput-object v0, p0, LX/8OJ;->s:LX/8GT;

    .line 1338759
    move-object/from16 v0, p19

    iput-object v0, p0, LX/8OJ;->w:LX/8OU;

    .line 1338760
    move-object/from16 v0, p20

    iput-object v0, p0, LX/8OJ;->z:LX/0UR;

    .line 1338761
    move-object/from16 v0, p21

    iput-object v0, p0, LX/8OJ;->A:LX/8KA;

    .line 1338762
    move-object/from16 v0, p22

    iput-object v0, p0, LX/8OJ;->B:LX/0cX;

    .line 1338763
    const/4 v1, 0x0

    invoke-interface {p3, v1}, LX/43C;->a(Z)V

    .line 1338764
    return-void
.end method

.method private a(Ljava/lang/Exception;)LX/73z;
    .locals 4

    .prologue
    .line 1338765
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 1338766
    iget-object v0, p0, LX/8OJ;->m:LX/03V;

    sget-object v1, LX/8OJ;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrapping "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1338767
    :cond_0
    iget-object v0, p0, LX/8OJ;->B:LX/0cX;

    invoke-static {v0, p1}, LX/8N0;->a(LX/0cX;Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/73w;LX/74b;LX/8KX;ILcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;IZ)LX/8KX;
    .locals 15

    .prologue
    .line 1338768
    invoke-static/range {p5 .. p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338769
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338770
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-static {v4}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v12

    .line 1338771
    const/4 v13, 0x0

    .line 1338772
    invoke-virtual/range {p3 .. p3}, LX/8KX;->c()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 1338773
    :try_start_0
    move-object/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, p5

    move/from16 v3, p7

    invoke-direct {p0, v0, v1, v2, v3}, LX/8OJ;->a(LX/8KX;ILcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Z)V

    .line 1338774
    invoke-virtual/range {p3 .. p3}, LX/8KX;->b()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v8

    move-object v4, p0

    move-object/from16 v5, p3

    move/from16 v6, p6

    move-object/from16 v7, p5

    move/from16 v9, p7

    .line 1338775
    invoke-direct/range {v4 .. v9}, LX/8OJ;->a(LX/8KX;ILcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;LX/434;Z)LX/8KX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 1338776
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v7

    if-eqz v12, :cond_0

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v8, 0x1

    :goto_0
    if-eqz v14, :cond_1

    const/4 v9, 0x1

    :goto_1
    invoke-virtual/range {p3 .. p3}, LX/8KX;->b()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v12

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p4

    invoke-virtual/range {v4 .. v13}, LX/73w;->a(LX/74b;ILjava/lang/String;ZZJJ)V

    return-object v14

    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    :catchall_0
    move-exception v4

    move-object v14, v4

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v7

    if-eqz v12, :cond_2

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v8, 0x1

    :goto_2
    if-eqz v13, :cond_3

    const/4 v9, 0x1

    :goto_3
    invoke-virtual/range {p3 .. p3}, LX/8KX;->b()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v12

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p4

    invoke-virtual/range {v4 .. v13}, LX/73w;->a(LX/74b;ILjava/lang/String;ZZJJ)V

    throw v14

    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    goto :goto_3
.end method

.method private declared-synchronized a(LX/8KX;ILcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;LX/434;Z)LX/8KX;
    .locals 17

    .prologue
    .line 1338777
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 1338778
    invoke-static {v3}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v4

    .line 1338779
    if-eqz v3, :cond_0

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 1338780
    :cond_0
    monitor-exit p0

    return-object p1

    .line 1338781
    :cond_1
    :try_start_1
    new-instance v12, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v12, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1338782
    invoke-virtual/range {p1 .. p1}, LX/8KX;->b()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1338783
    invoke-virtual/range {p1 .. p1}, LX/8KX;->c()Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 1338784
    const/4 v1, 0x0

    :try_start_2
    move-object/from16 v0, p0

    iput-boolean v1, v0, LX/8OJ;->t:Z

    .line 1338785
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    .line 1338786
    if-nez v1, :cond_2

    .line 1338787
    new-instance v1, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v1, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1338788
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->u:LX/8GZ;

    move/from16 v0, p2

    invoke-virtual {v5, v1, v0}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1338789
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8OJ;->u:LX/8GZ;

    invoke-virtual {v1, v4}, LX/8GZ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 1338790
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v9

    .line 1338791
    if-eqz v8, :cond_3

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    if-eqz v9, :cond_0

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1338792
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8OJ;->q:Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    move-object/from16 v0, p4

    iget v3, v0, LX/434;->b:I

    move-object/from16 v0, p4

    iget v4, v0, LX/434;->a:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-nez v8, :cond_5

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v8

    :goto_0
    if-nez v9, :cond_6

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v9

    :goto_1
    new-instance v10, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/high16 v15, 0x3f800000    # 1.0f

    const/high16 v16, 0x3f800000    # 1.0f

    move/from16 v0, v16

    invoke-direct {v10, v11, v14, v15, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v11, 0x1

    invoke-virtual/range {v1 .. v11}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a(Landroid/net/Uri;IIILjava/lang/String;LX/0Px;LX/0Px;LX/0Px;Landroid/graphics/RectF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1338793
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8OJ;->g:LX/8Ni;

    move-object/from16 v0, p4

    iget v4, v0, LX/434;->b:I

    move-object/from16 v0, p4

    iget v5, v0, LX/434;->a:I

    const/4 v6, 0x0

    move/from16 v0, p5

    invoke-virtual {v3, v4, v5, v6, v0}, LX/8Ni;->a(IIZZ)LX/43G;

    move-result-object v5

    .line 1338794
    new-instance v3, LX/8O4;

    move-object/from16 v4, p0

    move-object v6, v13

    move-object v7, v2

    move-object v8, v12

    invoke-direct/range {v3 .. v8}, LX/8O4;-><init>(LX/8OJ;LX/43G;Ljava/io/File;Landroid/net/Uri;Ljava/util/concurrent/CountDownLatch;)V

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8OJ;->r:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v3, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1338795
    invoke-virtual {v12}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 1338796
    invoke-virtual/range {p1 .. p1}, LX/8KX;->c()Ljava/io/File;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, LX/8KX;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1338797
    new-instance v1, LX/8OB;

    const-string v2, "can\'t rename scratch file"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1338798
    :catch_0
    move-exception v1

    .line 1338799
    :try_start_3
    sget-object v2, LX/8OJ;->a:Ljava/lang/Class;

    const-string v3, "Inturrupted"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1338800
    invoke-virtual/range {p1 .. p1}, LX/8KX;->c()Ljava/io/File;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, LX/8KX;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1338801
    new-instance v1, LX/8OB;

    const-string v2, "can\'t rename scratch file"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1338802
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1338803
    :cond_5
    :try_start_4
    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v8

    goto :goto_0

    :cond_6
    invoke-static {v9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v9

    goto :goto_1
.end method

.method private a(Ljava/lang/String;LX/73w;LX/74b;LX/8OL;LX/434;Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Ljava/lang/String;IZZ)LX/8KX;
    .locals 27
    .param p6    # Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1338804
    const/16 v24, 0x0

    .line 1338805
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->p:LX/0ad;

    sget-short v5, LX/8Jz;->l:S

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v25

    .line 1338806
    if-eqz v25, :cond_0

    .line 1338807
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1338808
    :cond_0
    :try_start_0
    invoke-virtual/range {p2 .. p2}, LX/73w;->a()Ljava/lang/String;

    move-result-object v5

    .line 1338809
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->e:LX/8KY;

    move-object/from16 v0, p1

    invoke-virtual {v4, v5, v0}, LX/8KY;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 1338810
    if-eqz v6, :cond_c

    .line 1338811
    move-object/from16 v0, p5

    invoke-static {v6, v0}, LX/8OJ;->a(Ljava/io/File;LX/434;)Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/8OJ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1338812
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v4

    .line 1338813
    iget v5, v4, LX/434;->b:I

    if-lez v5, :cond_1

    iget v5, v4, LX/434;->a:I

    if-gtz v5, :cond_7

    .line 1338814
    :cond_1
    new-instance v4, LX/8O6;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->n:LX/8KW;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "decode persisted file"

    invoke-virtual {v5, v6, v7}, LX/8KW;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/8O6;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1338815
    :catch_0
    move-exception v4

    .line 1338816
    :try_start_1
    invoke-virtual/range {p4 .. p4}, LX/8OL;->e()Z

    move-result v6

    .line 1338817
    if-nez v6, :cond_3

    .line 1338818
    if-eqz v24, :cond_2

    invoke-virtual/range {v24 .. v24}, LX/8KX;->c()Ljava/io/File;

    move-result-object v5

    if-nez v5, :cond_19

    :cond_2
    const/4 v5, 0x0

    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3, v5}, LX/8OJ;->a(LX/73w;LX/74b;Ljava/lang/String;Ljava/lang/String;)V

    .line 1338819
    :cond_3
    if-eqz v24, :cond_4

    .line 1338820
    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, LX/8KX;->a(Z)V

    .line 1338821
    invoke-virtual/range {v24 .. v24}, LX/8KX;->a()V

    .line 1338822
    :cond_4
    if-eqz v6, :cond_5

    .line 1338823
    invoke-virtual/range {p2 .. p3}, LX/73w;->c(LX/74b;)V

    .line 1338824
    const-string v5, "processing"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338825
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, LX/8OJ;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v4

    .line 1338826
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4}, LX/73w;->c(LX/74b;LX/73y;)V

    .line 1338827
    new-instance v5, LX/8OF;

    invoke-direct {v5, v4}, LX/8OF;-><init>(LX/73z;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338828
    :catchall_0
    move-exception v4

    if-eqz v25, :cond_6

    .line 1338829
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :cond_6
    throw v4

    .line 1338830
    :cond_7
    if-eqz p6, :cond_a

    :try_start_2
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v5

    invoke-static {v5}, LX/5iB;->d(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1338831
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->e:LX/8KY;

    const/high16 v7, 0x500000

    invoke-virtual {v5, v6, v7}, LX/8KY;->b(Ljava/io/File;I)LX/8KX;

    move-result-object v7

    .line 1338832
    if-nez v7, :cond_8

    .line 1338833
    new-instance v4, LX/8OB;

    const-string v5, "Failed to create auto-enhance temp files."

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v4

    .line 1338834
    :cond_8
    iget v5, v4, LX/434;->b:I

    iget v4, v4, LX/434;->a:I

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v8

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v9, p6

    move/from16 v10, p8

    move/from16 v11, p10

    invoke-direct/range {v4 .. v11}, LX/8OJ;->a(LX/73w;LX/74b;LX/8KX;ILcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;IZ)LX/8KX;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 1338835
    if-eqz v25, :cond_9

    .line 1338836
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :cond_9
    :goto_1
    return-object v4

    .line 1338837
    :cond_a
    :try_start_3
    new-instance v4, LX/8KX;

    const/4 v5, 0x0

    const/4 v7, 0x1

    invoke-direct {v4, v6, v5, v7}, LX/8KX;-><init>(Ljava/io/File;Ljava/io/File;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1338838
    if-eqz v25, :cond_9

    .line 1338839
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 1338840
    :cond_b
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->e:LX/8KY;

    const/high16 v7, 0x500000

    invoke-virtual {v4, v6, v7}, LX/8KY;->a(Ljava/io/File;I)LX/8KX;

    move-result-object v24

    .line 1338841
    :cond_c
    if-nez v24, :cond_d

    .line 1338842
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->e:LX/8KY;

    const/high16 v7, 0x500000

    const/high16 v8, 0x100000

    const/4 v9, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v4 .. v9}, LX/8KY;->a(Ljava/lang/String;Ljava/lang/String;IIZ)LX/8KX;

    move-result-object v24

    .line 1338843
    :cond_d
    if-nez v24, :cond_e

    .line 1338844
    new-instance v4, LX/8OB;

    const-string v5, "Failed to create temp files"

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v4

    .line 1338845
    :cond_e
    invoke-virtual/range {p2 .. p3}, LX/73w;->b(LX/74b;)V

    .line 1338846
    invoke-static/range {p1 .. p1}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v7

    .line 1338847
    iget v4, v7, LX/434;->b:I

    if-lez v4, :cond_f

    iget v4, v7, LX/434;->a:I

    if-gtz v4, :cond_10

    .line 1338848
    :cond_f
    new-instance v4, LX/8O6;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->n:LX/8KW;

    const-string v6, "decode input file"

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v6}, LX/8KW;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/8O6;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1338849
    :cond_10
    const-string v4, "processing"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338850
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v18

    .line 1338851
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->g:LX/8Ni;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->f:LX/43C;

    invoke-virtual/range {v24 .. v24}, LX/8KX;->c()Ljava/io/File;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8OJ;->p:LX/0ad;

    sget-short v9, LX/8Jz;->w:S

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_11

    const/4 v10, 0x1

    :goto_2
    move-object/from16 v6, p1

    move/from16 v9, p9

    move/from16 v11, p10

    invoke-virtual/range {v4 .. v11}, LX/8Ni;->a(LX/43C;Ljava/lang/String;LX/434;Ljava/io/File;ZZZ)LX/43G;

    move-result-object v4

    .line 1338852
    invoke-virtual/range {v24 .. v24}, LX/8KX;->c()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v20

    .line 1338853
    const-wide/16 v8, 0x1

    cmp-long v5, v20, v8

    if-gez v5, :cond_12

    .line 1338854
    new-instance v4, LX/8OB;

    const-string v5, "empty resized file"

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v4

    .line 1338855
    :cond_11
    const/4 v10, 0x0

    goto :goto_2

    .line 1338856
    :cond_12
    if-eqz p6, :cond_15

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v5

    invoke-static {v5}, LX/5iB;->d(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1338857
    invoke-virtual {v4}, LX/43G;->a()I

    move-result v5

    invoke-virtual {v4}, LX/43G;->b()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v12

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, v24

    move-object/from16 v13, p6

    move/from16 v14, p8

    move/from16 v15, p10

    invoke-direct/range {v8 .. v15}, LX/8OJ;->a(LX/73w;LX/74b;LX/8KX;ILcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;IZ)LX/8KX;

    move-result-object v24

    .line 1338858
    :cond_13
    if-eqz p5, :cond_17

    invoke-virtual {v4}, LX/43G;->a()I

    move-result v5

    move-object/from16 v0, p5

    iget v6, v0, LX/434;->b:I

    if-lt v5, v6, :cond_14

    invoke-virtual {v4}, LX/43G;->b()I

    move-result v5

    move-object/from16 v0, p5

    iget v6, v0, LX/434;->a:I

    if-ge v5, v6, :cond_17

    .line 1338859
    :cond_14
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Resized image too small got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, LX/43G;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, LX/43G;->b()I

    move-result v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " but need "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    iget v5, v0, LX/434;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    iget v5, v0, LX/434;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1338860
    new-instance v5, LX/8OC;

    invoke-direct {v5, v4}, LX/8OC;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1338861
    :cond_15
    invoke-virtual/range {v24 .. v24}, LX/8KX;->c()Ljava/io/File;

    move-result-object v5

    invoke-virtual/range {v24 .. v24}, LX/8KX;->b()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_16

    .line 1338862
    new-instance v4, LX/8OB;

    const-string v5, "can\'t rename scratch file"

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v4

    .line 1338863
    :cond_16
    invoke-virtual/range {v24 .. v24}, LX/8KX;->b()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-gtz v5, :cond_13

    .line 1338864
    new-instance v4, LX/8OB;

    const-string v5, "move failed"

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v4

    .line 1338865
    :cond_17
    invoke-virtual/range {v24 .. v24}, LX/8KX;->b()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v8

    .line 1338866
    move-object/from16 v0, p0

    iget-object v6, v0, LX/8OJ;->w:LX/8OU;

    invoke-virtual/range {v24 .. v24}, LX/8KX;->b()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v9

    move-object/from16 v11, p3

    move/from16 v12, p10

    invoke-virtual/range {v6 .. v12}, LX/8OU;->a(LX/434;LX/434;JLX/74b;Z)V

    .line 1338867
    const-string v26, ""

    iget v8, v7, LX/434;->b:I

    iget v9, v7, LX/434;->a:I

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-virtual {v4}, LX/43G;->a()I

    move-result v12

    invoke-virtual {v4}, LX/43G;->b()I

    move-result v13

    const/4 v14, -0x1

    const/4 v15, -0x1

    const-wide/16 v16, -0x1

    invoke-virtual {v4}, LX/43G;->c()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v22, v0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p7

    move-object/from16 v7, v26

    invoke-virtual/range {v4 .. v23}, LX/73w;->a(LX/74b;Ljava/lang/String;Ljava/lang/String;IIIIIIIIJJJJ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1338868
    if-eqz v25, :cond_18

    .line 1338869
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :cond_18
    move-object/from16 v4, v24

    goto/16 :goto_1

    .line 1338870
    :cond_19
    :try_start_5
    invoke-virtual/range {v24 .. v24}, LX/8KX;->c()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v5

    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;IIJLX/73w;LX/74b;LX/8OL;LX/8OG;LX/8Jv;LX/8Ne;)LX/8OI;
    .locals 16

    .prologue
    .line 1338871
    new-instance v12, LX/14U;

    invoke-direct {v12}, LX/14U;-><init>()V

    .line 1338872
    new-instance v4, LX/8OA;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/8OJ;->x:LX/0b3;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/8OJ;->y:LX/1EZ;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/8OJ;->p:LX/0ad;

    move-object/from16 v5, p9

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v4 .. v11}, LX/8OA;-><init>(LX/8OG;LX/73w;LX/74b;LX/8OL;LX/0b3;LX/1EZ;LX/0ad;)V

    .line 1338873
    invoke-virtual {v12, v4}, LX/14U;->a(LX/4ck;)V

    .line 1338874
    invoke-virtual/range {p8 .. p8}, LX/8OL;->d()LX/4d1;

    move-result-object v5

    invoke-virtual {v12, v5}, LX/14U;->a(LX/4d1;)V

    .line 1338875
    const-wide/16 v8, -0x1

    .line 1338876
    const/4 v10, 0x0

    .line 1338877
    const-string v5, "before upload"

    move-object/from16 v0, p8

    invoke-virtual {v0, v5}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338878
    const/4 v6, 0x0

    .line 1338879
    :goto_0
    if-lez v10, :cond_0

    .line 1338880
    const/4 v5, 0x1

    move-object/from16 v0, p10

    invoke-interface {v0, v5}, LX/8Jv;->a(I)V

    .line 1338881
    :cond_0
    move-object/from16 v0, p10

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p1

    invoke-interface {v0, v1, v2, v3}, LX/8Jv;->a(IILcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a(Z)V

    .line 1338882
    move-wide/from16 v0, p4

    invoke-virtual {v4, v0, v1, v10}, LX/8OA;->a(JI)V

    .line 1338883
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p8

    invoke-static {v0, v1, v2}, LX/8OJ;->a(LX/8OJ;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OL;)V

    .line 1338884
    const-string v5, "after upload with fbuploader"

    move-object/from16 v0, p8

    invoke-virtual {v0, v5}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338885
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->l:LX/8NH;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1338886
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->j:LX/11H;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/8OJ;->l:LX/8NH;

    move-object/from16 v0, p1

    invoke-virtual {v5, v7, v0, v12}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 1338887
    const-wide/16 v14, 0x0

    cmp-long v5, v8, v14

    if-gez v5, :cond_1

    .line 1338888
    new-instance v5, LX/740;

    const-string v6, "No fbid"

    const/4 v7, 0x1

    invoke-direct {v5, v6, v7}, LX/740;-><init>(Ljava/lang/String;Z)V

    throw v5
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1338889
    :catch_0
    move-exception v4

    .line 1338890
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->n:LX/8KW;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/8KW;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1338891
    sget-object v6, LX/8OJ;->a:Ljava/lang/Class;

    const-string v7, "Upload failed: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-static {v6, v4, v7, v8}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1338892
    new-instance v4, LX/8O8;

    invoke-direct {v4, v5}, LX/8O8;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1338893
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v4, v5}, LX/8OA;->a(LX/73z;)V

    .line 1338894
    invoke-interface/range {p11 .. p11}, LX/8Ne;->a()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v6

    move-wide v6, v8

    .line 1338895
    :goto_1
    invoke-virtual/range {p8 .. p8}, LX/8OL;->e()Z

    move-result v8

    if-nez v8, :cond_7

    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-gez v8, :cond_7

    add-int/lit8 v8, v10, 0x1

    invoke-interface/range {p11 .. p11}, LX/8Ne;->b()I

    move-result v9

    if-le v8, v9, :cond_6

    move v4, v8

    .line 1338896
    :goto_2
    const-string v8, "after upload"

    move-object/from16 v0, p8

    invoke-virtual {v0, v8}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338897
    move-object/from16 v0, p10

    invoke-interface {v0, v4}, LX/8Jv;->a(I)V

    .line 1338898
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-gez v8, :cond_4

    .line 1338899
    if-nez v5, :cond_2

    .line 1338900
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->m:LX/03V;

    sget-object v5, LX/8OJ;->a:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "No fbid"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338901
    new-instance v4, LX/740;

    const-string v5, "No fbid"

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, LX/740;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, LX/8OJ;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v5

    .line 1338902
    :cond_2
    new-instance v4, LX/8OD;

    invoke-direct {v4, v5}, LX/8OD;-><init>(LX/73z;)V

    throw v4

    .line 1338903
    :catch_1
    move-exception v5

    move-wide v6, v8

    .line 1338904
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, LX/8OJ;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v5

    .line 1338905
    invoke-virtual {v4, v5}, LX/8OA;->a(LX/73z;)V

    .line 1338906
    const-string v8, "during upload"

    move-object/from16 v0, p8

    invoke-virtual {v0, v8}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338907
    invoke-virtual {v5}, LX/73z;->h()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v5}, LX/73z;->e()I

    move-result v8

    const/16 v9, 0x7a

    if-ne v8, v9, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1338908
    new-instance v4, LX/8O9;

    invoke-direct {v4}, LX/8O9;-><init>()V

    throw v4

    .line 1338909
    :cond_3
    move-object/from16 v0, p11

    invoke-interface {v0, v5}, LX/8Ne;->a(LX/73z;)V

    goto :goto_1

    .line 1338910
    :cond_4
    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p9

    invoke-interface {v0, v5}, LX/8OG;->a(F)V

    .line 1338911
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a(J)V

    .line 1338912
    move-object/from16 v0, p10

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p1

    invoke-interface {v0, v1, v2, v3}, LX/8Jv;->a(IILcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1338913
    invoke-interface/range {p10 .. p10}, LX/8Jv;->a()V

    .line 1338914
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OJ;->j:LX/11H;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/8OJ;->l:LX/8NH;

    move-object/from16 v0, p1

    invoke-virtual {v5, v8, v0, v12}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1338915
    :cond_5
    :goto_3
    new-instance v5, LX/8OI;

    invoke-direct {v5, v6, v7, v4}, LX/8OI;-><init>(JI)V

    return-object v5

    .line 1338916
    :catch_2
    move-exception v5

    .line 1338917
    sget-object v8, LX/8OJ;->a:Ljava/lang/Class;

    const-string v9, "Full scale upload failed: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v5, v9, v10}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_6
    move v10, v8

    move-wide v8, v6

    move-object v6, v5

    goto/16 :goto_0

    :cond_7
    move v4, v10

    goto/16 :goto_2
.end method

.method public static synthetic a(LX/8OJ;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OG;LX/73w;LX/74b;LX/8Jv;LX/8OL;IILX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/photos/upload/operation/UploadRecord;
    .locals 1

    .prologue
    .line 1338918
    invoke-direct/range {p0 .. p9}, LX/8OJ;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OG;LX/73w;LX/74b;LX/8Jv;LX/8OL;IILX/8Ne;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OG;LX/73w;LX/74b;LX/8Jv;LX/8OL;IILX/8Ne;)Lcom/facebook/photos/upload/operation/UploadRecord;
    .locals 16

    .prologue
    .line 1338919
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/741;->VAULT:LX/741;

    move-object v14, v2

    .line 1338920
    :goto_0
    add-int/lit8 v2, p7, -0x1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2, v14}, LX/73w;->a(LX/74b;ILX/741;)V

    .line 1338921
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, LX/8OG;->a(F)V

    .line 1338922
    const/4 v13, 0x0

    .line 1338923
    const-wide/16 v6, -0x1

    .line 1338924
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c()Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c()Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    invoke-static {v2}, LX/5iB;->d(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 1338925
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d()Ljava/lang/String;

    move-result-object v3

    .line 1338926
    :goto_2
    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ac()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    if-nez v2, :cond_11

    .line 1338927
    :cond_0
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->E()LX/434;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c()Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ag()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->D()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k()Z

    move-result v11

    const/4 v2, 0x1

    move/from16 v0, p8

    if-le v0, v2, :cond_5

    const/4 v12, 0x1

    :goto_3
    move-object/from16 v2, p0

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p6

    invoke-direct/range {v2 .. v12}, LX/8OJ;->a(Ljava/lang/String;LX/73w;LX/74b;LX/8OL;LX/434;Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Ljava/lang/String;IZZ)LX/8KX;
    :try_end_1
    .catch LX/8OF; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 1338928
    :try_start_2
    invoke-virtual {v4}, LX/8KX;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a(Ljava/lang/String;)V

    .line 1338929
    invoke-virtual {v4}, LX/8KX;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 1338930
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c()Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1338931
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c()Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    .line 1338932
    invoke-static {v2}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    .line 1338933
    const/4 v2, 0x0

    move v5, v2

    :goto_4
    if-ge v5, v9, :cond_6

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/362;

    .line 1338934
    instance-of v10, v2, LX/5i8;

    if-eqz v10, :cond_1

    .line 1338935
    check-cast v2, LX/5i8;

    invoke-interface {v2}, LX/5i8;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/8GT;->a(Landroid/net/Uri;)V
    :try_end_2
    .catch LX/8OF; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1338936
    :cond_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    .line 1338937
    :cond_2
    sget-object v2, LX/741;->LOCAL:LX/741;

    move-object v14, v2

    goto/16 :goto_0

    .line 1338938
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1338939
    :cond_4
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v3

    goto :goto_2

    .line 1338940
    :cond_5
    const/4 v12, 0x0

    goto :goto_3

    :cond_6
    move-object v15, v4

    :goto_5
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p7

    move/from16 v5, p8

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p6

    move-object/from16 v11, p2

    move-object/from16 v12, p5

    move-object/from16 v13, p9

    .line 1338941
    :try_start_4
    invoke-direct/range {v2 .. v13}, LX/8OJ;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;IIJLX/73w;LX/74b;LX/8OL;LX/8OG;LX/8Jv;LX/8Ne;)LX/8OI;

    move-result-object v2

    .line 1338942
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l()Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-result-object v3

    if-eqz v3, :cond_d

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l()Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->b()Ljava/lang/String;

    move-result-object v7

    :goto_6
    iget-wide v8, v2, LX/8OI;->a:J

    iget v10, v2, LX/8OI;->b:I

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v5, v14

    invoke-virtual/range {v3 .. v10}, LX/73w;->a(LX/74b;LX/741;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 1338943
    if-eqz v15, :cond_7

    .line 1338944
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, LX/8KX;->a(Z)V

    .line 1338945
    :cond_7
    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-wide v4, v2, LX/8OI;->a:J

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8OJ;->o:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v()Z

    move-result v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1338946
    if-eqz v15, :cond_8

    .line 1338947
    invoke-virtual {v15}, LX/8KX;->a()V

    :cond_8
    return-object v3

    .line 1338948
    :catch_0
    move-exception v2

    move-object v4, v13

    .line 1338949
    :goto_7
    :try_start_5
    invoke-virtual/range {p6 .. p6}, LX/8OL;->e()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v2}, LX/8N0;->h()LX/73z;

    move-result-object v5

    invoke-virtual {v5}, LX/73z;->a()Ljava/lang/Exception;

    move-result-object v5

    instance-of v5, v5, LX/8O6;

    if-nez v5, :cond_9

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v8, 0x400000

    cmp-long v5, v6, v8

    if-lez v5, :cond_c

    .line 1338950
    :cond_9
    throw v2
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1338951
    :catch_1
    move-exception v2

    .line 1338952
    :goto_8
    :try_start_6
    invoke-virtual/range {p6 .. p6}, LX/8OL;->e()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1338953
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v14}, LX/73w;->a(LX/74b;LX/741;)V

    .line 1338954
    const-string v3, "uploading"

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338955
    :cond_a
    instance-of v3, v2, LX/8N0;

    if-eqz v3, :cond_e

    .line 1338956
    check-cast v2, LX/8N0;

    .line 1338957
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v14, v2}, LX/73w;->a(LX/74b;LX/741;LX/73y;)V

    .line 1338958
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1338959
    :catchall_0
    move-exception v2

    :goto_9
    if-eqz v4, :cond_b

    .line 1338960
    invoke-virtual {v4}, LX/8KX;->a()V

    :cond_b
    throw v2

    .line 1338961
    :cond_c
    :try_start_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a(Ljava/lang/String;)V

    .line 1338962
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-wide v6

    move-object v15, v4

    goto/16 :goto_5

    .line 1338963
    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_6

    .line 1338964
    :cond_e
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/8OJ;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v3

    .line 1338965
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v14, v3}, LX/73w;->a(LX/74b;LX/741;LX/73y;)V

    .line 1338966
    instance-of v5, v2, LX/8O9;

    if-eqz v5, :cond_f

    .line 1338967
    throw v2

    .line 1338968
    :cond_f
    invoke-virtual {v3}, LX/73z;->k()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1338969
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, LX/8KX;->a(Z)V

    .line 1338970
    :cond_10
    new-instance v2, LX/8OE;

    invoke-direct {v2, v3}, LX/8OE;-><init>(LX/73z;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1338971
    :catchall_1
    move-exception v2

    move-object v4, v13

    goto :goto_9

    :catchall_2
    move-exception v2

    move-object v4, v15

    goto :goto_9

    .line 1338972
    :catch_2
    move-exception v2

    move-object v4, v13

    goto :goto_8

    :catch_3
    move-exception v2

    move-object v4, v15

    goto :goto_8

    .line 1338973
    :catch_4
    move-exception v2

    goto/16 :goto_7

    :cond_11
    move-object v15, v13

    goto/16 :goto_5
.end method

.method private a(LX/73w;LX/74b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 1338688
    new-instance v0, Ljava/io/StringWriter;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/io/StringWriter;-><init>(I)V

    .line 1338689
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1338690
    iget-object v2, p0, LX/8OJ;->n:LX/8KW;

    const/4 v3, 0x0

    .line 1338691
    iget-object v4, v2, LX/8KW;->a:LX/0ps;

    invoke-virtual {v4}, LX/0ps;->b()LX/0V6;

    move-result-object v4

    .line 1338692
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1338693
    invoke-virtual {v1, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v5

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 1338694
    :cond_0
    invoke-virtual {v4}, LX/0V6;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 1338695
    invoke-virtual {v4}, LX/0V6;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1338696
    const-string v5, ", low memory device"

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 1338697
    :cond_1
    iget-object v5, v2, LX/8KW;->a:LX/0ps;

    invoke-virtual {v5, v4}, LX/0ps;->a(LX/0V6;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1338698
    const-string v4, ", low on memory"

    invoke-virtual {v1, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 1338699
    :cond_2
    const-string v4, ", fd open: "

    invoke-virtual {v1, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v4

    invoke-static {}, LX/04V;->getOpenFDCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/io/PrintWriter;->print(I)V

    .line 1338700
    invoke-static {}, LX/04V;->getOpenFDLimits()LX/04W;

    move-result-object v4

    .line 1338701
    if-eqz v4, :cond_3

    .line 1338702
    const-string v5, ", fd hard max: "

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v5

    iget-object v6, v4, LX/04W;->hardLimit:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1338703
    const-string v5, ", fd soft max: "

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v5

    iget-object v4, v4, LX/04W;->softLimit:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1338704
    :cond_3
    const-string v2, ", source"

    const/4 v3, 0x0

    invoke-static {v1, p3, v2, v3}, LX/8KW;->a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1338705
    if-eqz p4, :cond_4

    .line 1338706
    const-string v2, ", tempFile"

    const/4 v3, 0x1

    invoke-static {v1, p4, v2, v3}, LX/8KW;->a(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1338707
    :cond_4
    new-instance v1, LX/8O5;

    invoke-direct {v1, p0, v0}, LX/8O5;-><init>(LX/8OJ;Ljava/io/StringWriter;)V

    .line 1338708
    invoke-virtual {p2}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 1338709
    invoke-interface {v1}, LX/73y;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, LX/73y;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, LX/73y;->e()I

    move-result v7

    invoke-interface {v1}, LX/73y;->f()I

    move-result v8

    invoke-interface {v1}, LX/73y;->g()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1}, LX/73y;->b()Ljava/lang/String;

    move-result-object v10

    invoke-static/range {v4 .. v10}, LX/73w;->a(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 1338710
    sget-object v5, LX/74R;->MEDIA_UPLOAD_DIAGNOSTIC:LX/74R;

    const/4 v6, 0x0

    invoke-static {p1, v5, v4, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1338711
    return-void
.end method

.method private a(LX/8KX;ILcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Z)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 1338712
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338713
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338714
    iget-object v0, p1, LX/8KX;->b:Ljava/io/File;

    move-object v0, v0

    .line 1338715
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338716
    sget-boolean v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->d:Z

    move v0, v0

    .line 1338717
    if-nez v0, :cond_1

    .line 1338718
    iget-object v0, p1, LX/8KX;->b:Ljava/io/File;

    move-object v0, v0

    .line 1338719
    iget-object v1, p1, LX/8KX;->a:Ljava/io/File;

    move-object v1, v1

    .line 1338720
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1338721
    new-instance v0, LX/8OB;

    const-string v1, "can\'t rename scratch file"

    invoke-direct {v0, v1, v4}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v0

    .line 1338722
    :cond_0
    iget-object v0, p1, LX/8KX;->a:Ljava/io/File;

    move-object v0, v0

    .line 1338723
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    .line 1338724
    new-instance v0, LX/8OB;

    const-string v1, "move failed"

    invoke-direct {v0, v1, v4}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v0

    .line 1338725
    :cond_1
    const/16 v6, 0x5a

    .line 1338726
    iget-object v0, p0, LX/8OJ;->p:LX/0ad;

    sget-short v1, LX/8Jz;->x:S

    invoke-interface {v0, v1, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1338727
    if-eqz p4, :cond_3

    iget-object v0, p0, LX/8OJ;->A:LX/8KA;

    invoke-virtual {v0}, LX/8KA;->b()I

    move-result v0

    :goto_0
    move v6, v0

    .line 1338728
    :cond_2
    iget-object v0, p1, LX/8KX;->b:Ljava/io/File;

    move-object v0, v0

    .line 1338729
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 1338730
    invoke-static {v1}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v4

    .line 1338731
    iget-object v0, p0, LX/8OJ;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1338732
    iget-object v2, p1, LX/8KX;->a:Ljava/io/File;

    move-object v2, v2

    .line 1338733
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object v3, p3

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;III)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1338734
    new-instance v0, LX/8OB;

    const-string v1, "AutoEnhance failed"

    invoke-direct {v0, v1, v7}, LX/8OB;-><init>(Ljava/lang/String;Z)V

    throw v0

    .line 1338735
    :cond_3
    iget-object v0, p0, LX/8OJ;->A:LX/8KA;

    invoke-virtual {v0}, LX/8KA;->a()I

    move-result v0

    goto :goto_0

    .line 1338736
    :cond_4
    return-void
.end method

.method private static a(LX/8OJ;LX/0b3;LX/1EZ;)V
    .locals 0

    .prologue
    .line 1338569
    iput-object p1, p0, LX/8OJ;->x:LX/0b3;

    iput-object p2, p0, LX/8OJ;->y:LX/1EZ;

    return-void
.end method

.method private static a(LX/8OJ;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OL;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 1338580
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1338581
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    move-object v0, v0

    .line 1338582
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/8OJ;->c(LX/8OJ;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1338583
    :cond_0
    :goto_0
    return-void

    .line 1338584
    :cond_1
    iget-object v0, p0, LX/8OJ;->p:LX/0ad;

    sget v1, LX/8Jz;->J:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v7

    .line 1338585
    iget-object v0, p0, LX/8OJ;->p:LX/0ad;

    sget v1, LX/8Jz;->K:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v8

    .line 1338586
    iget-object v0, p0, LX/8OJ;->p:LX/0ad;

    sget v1, LX/8Jz;->I:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v9

    .line 1338587
    invoke-static {p1}, LX/8NH;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/io/File;

    move-result-object v1

    .line 1338588
    new-instance v0, LX/7yy;

    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    const-string v6, "image/jpeg"

    invoke-direct/range {v0 .. v6}, LX/7yy;-><init>(Ljava/io/File;JJLjava/lang/String;)V

    .line 1338589
    new-instance v1, LX/7yu;

    invoke-direct {v1, v7, v8, v9}, LX/7yu;-><init>(III)V

    .line 1338590
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1338591
    const-string v3, "X_FB_PHOTO_WATERFALL_ID"

    .line 1338592
    iget-object v4, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    move-object v4, v4

    .line 1338593
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338594
    new-instance v3, LX/7yw;

    sget-object v4, LX/7yt;->FACEBOOK:LX/7yt;

    invoke-static {p0}, LX/8OJ;->c(LX/8OJ;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v2, v1, v5}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;Ljava/lang/String;)V

    .line 1338595
    iget-object v1, p0, LX/8OJ;->v:LX/7zS;

    .line 1338596
    iget-object v2, v1, LX/7zS;->a:LX/7z2;

    move-object v1, v2

    .line 1338597
    invoke-virtual {v1, v0, v3, v10}, LX/7z2;->a(LX/7yy;LX/7yw;LX/7yp;)LX/7z0;

    move-result-object v0

    .line 1338598
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1338599
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1338600
    iput-object v2, p2, LX/8OL;->a:Ljava/util/ArrayList;

    .line 1338601
    :try_start_0
    invoke-virtual {v1, v0}, LX/7z2;->c(LX/7z0;)LX/7zL;

    move-result-object v0

    iget-object v0, v0, LX/7zL;->a:Ljava/lang/String;

    .line 1338602
    iput-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->K:Ljava/lang/String;
    :try_end_0
    .catch LX/7zB; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1338603
    iput-object v10, p2, LX/8OL;->a:Ljava/util/ArrayList;

    .line 1338604
    goto :goto_0

    .line 1338605
    :catch_0
    move-exception v0

    .line 1338606
    :try_start_1
    const-string v1, "during resumable upload"

    invoke-virtual {p2, v1}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338607
    iget-object v1, v0, LX/7zB;->mInnerException:Ljava/lang/Exception;

    if-eqz v1, :cond_2

    .line 1338608
    iget-object v0, v0, LX/7zB;->mInnerException:Ljava/lang/Exception;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338609
    :catchall_0
    move-exception v0

    .line 1338610
    iput-object v10, p2, LX/8OL;->a:Ljava/util/ArrayList;

    .line 1338611
    throw v0

    .line 1338612
    :cond_2
    throw v0
.end method

.method public static a(Ljava/io/File;LX/434;)Z
    .locals 3

    .prologue
    .line 1338613
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1338614
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    .line 1338615
    iget v1, v0, LX/434;->b:I

    iget v2, p1, LX/434;->b:I

    if-lt v1, v2, :cond_0

    iget v0, v0, LX/434;->a:I

    iget v1, p1, LX/434;->a:I

    if-lt v0, v1, :cond_0

    .line 1338616
    const/4 v0, 0x1

    .line 1338617
    :goto_0
    return v0

    .line 1338618
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 1338619
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1338620
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1338621
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v1

    invoke-static {p1}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/8OJ;
    .locals 24

    .prologue
    .line 1338622
    new-instance v1, LX/8OJ;

    invoke-static/range {p0 .. p0}, LX/8Jw;->a(LX/0QB;)LX/8Jw;

    move-result-object v2

    check-cast v2, LX/8Jw;

    invoke-static/range {p0 .. p0}, LX/8KY;->a(LX/0QB;)LX/8KY;

    move-result-object v3

    check-cast v3, LX/8KY;

    invoke-static/range {p0 .. p0}, LX/43D;->a(LX/0QB;)LX/43C;

    move-result-object v4

    check-cast v4, LX/43C;

    invoke-static/range {p0 .. p0}, LX/8Ni;->a(LX/0QB;)LX/8Ni;

    move-result-object v5

    check-cast v5, LX/8Ni;

    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(LX/0QB;)Lcom/facebook/photos/imageprocessing/FiltersEngine;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/imageprocessing/FiltersEngine;

    const/16 v7, 0x2f01

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v8

    check-cast v8, LX/11H;

    invoke-static/range {p0 .. p0}, LX/8LX;->a(LX/0QB;)LX/8LX;

    move-result-object v9

    check-cast v9, LX/8LX;

    invoke-static/range {p0 .. p0}, LX/8NH;->a(LX/0QB;)LX/8NH;

    move-result-object v10

    check-cast v10, LX/8NH;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static/range {p0 .. p0}, LX/8KW;->a(LX/0QB;)LX/8KW;

    move-result-object v12

    check-cast v12, LX/8KW;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a(LX/0QB;)Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    move-result-object v14

    check-cast v14, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    invoke-static/range {p0 .. p0}, LX/8GZ;->a(LX/0QB;)LX/8GZ;

    move-result-object v15

    check-cast v15, LX/8GZ;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v16

    check-cast v16, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/8GT;->a(LX/0QB;)LX/8GT;

    move-result-object v17

    check-cast v17, LX/8GT;

    invoke-static/range {p0 .. p0}, LX/7zS;->a(LX/0QB;)LX/7zS;

    move-result-object v18

    check-cast v18, LX/7zS;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v19

    check-cast v19, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/8OU;->a(LX/0QB;)LX/8OU;

    move-result-object v20

    check-cast v20, LX/8OU;

    invoke-static/range {p0 .. p0}, LX/0UR;->a(LX/0QB;)LX/0UR;

    move-result-object v21

    check-cast v21, LX/0UR;

    invoke-static/range {p0 .. p0}, LX/8KA;->a(LX/0QB;)LX/8KA;

    move-result-object v22

    check-cast v22, LX/8KA;

    invoke-static/range {p0 .. p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v23

    check-cast v23, LX/0cX;

    invoke-direct/range {v1 .. v23}, LX/8OJ;-><init>(LX/8Jw;LX/8KY;LX/43C;LX/8Ni;Lcom/facebook/photos/imageprocessing/FiltersEngine;LX/0Or;LX/11H;LX/8LX;LX/8NH;LX/03V;LX/8KW;LX/0SG;Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;LX/8GZ;Ljava/util/concurrent/ExecutorService;LX/8GT;LX/7zS;LX/0ad;LX/8OU;LX/0UR;LX/8KA;LX/0cX;)V

    .line 1338623
    invoke-static/range {p0 .. p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v2

    check-cast v2, LX/0b3;

    invoke-static/range {p0 .. p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v3

    check-cast v3, LX/1EZ;

    invoke-static {v1, v2, v3}, LX/8OJ;->a(LX/8OJ;LX/0b3;LX/1EZ;)V

    .line 1338624
    return-object v1
.end method

.method private static declared-synchronized b(LX/8OJ;)V
    .locals 4

    .prologue
    .line 1338625
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8OJ;->C:LX/0TD;

    if-nez v0, :cond_0

    .line 1338626
    iget-object v0, p0, LX/8OJ;->p:LX/0ad;

    sget v1, LX/8Jz;->j:I

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 1338627
    iget-object v1, p0, LX/8OJ;->z:LX/0UR;

    const-string v2, "ParallelUploadService"

    const/16 v3, 0x100

    invoke-virtual {v1, v2, v0, v3}, LX/0UR;->a(Ljava/lang/String;II)LX/0TU;

    move-result-object v0

    iput-object v0, p0, LX/8OJ;->C:LX/0TD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1338628
    :cond_0
    monitor-exit p0

    return-void

    .line 1338629
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static c(LX/8OJ;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1338630
    const/4 v0, 0x0

    .line 1338631
    iget-object v1, p0, LX/8OJ;->p:LX/0ad;

    sget-short v2, LX/8Jz;->H:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1338632
    const-string v0, "2"

    .line 1338633
    :cond_0
    :goto_0
    return-object v0

    .line 1338634
    :cond_1
    iget-object v1, p0, LX/8OJ;->p:LX/0ad;

    sget-short v2, LX/8Jz;->L:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1338635
    const-string v0, "1"

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaItem;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    .locals 9
    .param p4    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1338570
    new-instance v0, LX/8NJ;

    new-instance v1, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->o()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    invoke-direct {v0, v1}, LX/8NJ;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V

    invoke-virtual {v0, p4}, LX/8NJ;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8NJ;

    move-result-object v0

    invoke-virtual {v0}, LX/8NJ;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v0

    .line 1338571
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1338572
    iget-object v0, p0, LX/8OJ;->k:LX/8LX;

    invoke-virtual {v0, p2}, LX/8LX;->a(Ljava/lang/String;)LX/73w;

    move-result-object v4

    .line 1338573
    iput-object p3, v4, LX/73w;->k:Ljava/lang/String;

    .line 1338574
    const-string v0, "2.0"

    invoke-virtual {v4, v0}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v5

    .line 1338575
    new-instance v2, LX/8O7;

    invoke-direct {v2}, LX/8O7;-><init>()V

    new-instance v3, LX/8OL;

    invoke-direct {v3}, LX/8OL;-><init>()V

    const/4 v6, 0x0

    iget-object v0, p0, LX/8OJ;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/8Ne;

    move-object v0, p0

    move-object v8, p5

    invoke-virtual/range {v0 .. v8}, LX/8OJ;->a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1338576
    iget-object v1, p0, LX/8OJ;->e:LX/8KY;

    .line 1338577
    iget-object v2, v4, LX/73w;->j:Ljava/lang/String;

    move-object v2, v2

    .line 1338578
    invoke-virtual {v1, v2}, LX/8KY;->a(Ljava/lang/String;)V

    .line 1338579
    return-object v0
.end method

.method public final a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;
    .locals 24
    .param p6    # Lcom/facebook/photos/upload/operation/UploadOperation;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "LX/8O7;",
            "LX/8OL;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/74b;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger$UploadInfo;",
            "LX/8Ne;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1338636
    const/4 v11, 0x1

    .line 1338637
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v22

    .line 1338638
    invoke-virtual/range {p4 .. p6}, LX/73w;->d(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1338639
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->d:LX/8Jw;

    invoke-virtual {v4}, LX/8Jw;->c()LX/8Jv;

    move-result-object v9

    .line 1338640
    new-instance v6, LX/8OH;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, LX/8OH;-><init>(LX/8O7;)V

    .line 1338641
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1338642
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-virtual {v6, v11, v4}, LX/8OH;->a(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1338643
    :try_start_1
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v12

    move-object/from16 v4, p0

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v10, p3

    move-object/from16 v13, p7

    invoke-direct/range {v4 .. v13}, LX/8OJ;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OG;LX/73w;LX/74b;LX/8Jv;LX/8OL;IILX/8Ne;)Lcom/facebook/photos/upload/operation/UploadRecord;
    :try_end_1
    .catch LX/8O9; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 1338644
    :goto_1
    :try_start_2
    iget-wide v12, v4, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v22

    invoke-interface {v0, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338645
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v4}, LX/8O7;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;Lcom/facebook/photos/upload/operation/UploadRecord;)V

    .line 1338646
    add-int/lit8 v11, v11, 0x1

    .line 1338647
    goto :goto_0

    .line 1338648
    :catch_0
    invoke-virtual {v5}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v20

    move-object/from16 v12, p0

    move-object v14, v6

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, v9

    move-object/from16 v18, p3

    move/from16 v19, v11

    move-object/from16 v21, p7

    invoke-direct/range {v12 .. v21}, LX/8OJ;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OG;LX/73w;LX/74b;LX/8Jv;LX/8OL;IILX/8Ne;)Lcom/facebook/photos/upload/operation/UploadRecord;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v4

    goto :goto_1

    .line 1338649
    :catch_1
    move-exception v10

    .line 1338650
    add-int/lit8 v7, v11, -0x1

    .line 1338651
    invoke-virtual/range {p3 .. p3}, LX/8OL;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1338652
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual {v0, v1, v2, v7}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    .line 1338653
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Upload cancelled at photo #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338654
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v4

    sub-int v8, v4, v7

    instance-of v4, v10, LX/8N0;

    if-eqz v4, :cond_1

    move-object v4, v10

    check-cast v4, LX/8N0;

    move-object v9, v4

    :goto_2
    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-virtual/range {v4 .. v9}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;IILX/73y;)V

    .line 1338655
    throw v10

    .line 1338656
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, LX/8OJ;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v9

    goto :goto_2

    .line 1338657
    :cond_2
    invoke-virtual/range {p4 .. p6}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1338658
    return-object v22
.end method

.method public final a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Semaphore;)Ljava/util/Map;
    .locals 25
    .param p6    # Lcom/facebook/photos/upload/operation/UploadOperation;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "LX/8O7;",
            "LX/8OL;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/74b;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger$UploadInfo;",
            "LX/8Ne;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Semaphore;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1338659
    invoke-static/range {p0 .. p0}, LX/8OJ;->b(LX/8OJ;)V

    .line 1338660
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 1338661
    invoke-virtual/range {p4 .. p6}, LX/73w;->d(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1338662
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8OJ;->d:LX/8Jw;

    invoke-virtual {v4}, LX/8Jw;->c()LX/8Jv;

    move-result-object v14

    .line 1338663
    new-instance v10, LX/8OK;

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v4

    move-object/from16 v0, p2

    invoke-direct {v10, v0, v4}, LX/8OK;-><init>(LX/8O7;I)V

    .line 1338664
    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1338665
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 1338666
    new-instance v18, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-direct {v0, v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 1338667
    const/4 v11, 0x1

    .line 1338668
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1338669
    add-int/lit8 v20, v11, 0x1

    .line 1338670
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8OJ;->C:LX/0TD;

    move-object/from16 v24, v0

    new-instance v4, LX/8O3;

    move-object/from16 v5, p0

    move-object/from16 v6, p7

    move-object/from16 v7, p6

    move-object/from16 v8, p9

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v15, p3

    move-object/from16 v16, p1

    move-object/from16 v17, p8

    move-object/from16 v19, p2

    invoke-direct/range {v4 .. v19}, LX/8O3;-><init>(LX/8OJ;LX/8Ne;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/concurrent/Semaphore;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OK;ILX/73w;LX/74b;LX/8Jv;LX/8OL;Ljava/util/Collection;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/atomic/AtomicInteger;LX/8O7;)V

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v11, v20

    .line 1338671
    goto :goto_0

    .line 1338672
    :cond_0
    invoke-static/range {v22 .. v22}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 1338673
    const v4, -0x375916a5

    :try_start_0
    invoke-static {v5, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 1338674
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/operation/UploadRecord;

    .line 1338675
    iget-wide v8, v4, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-interface {v0, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1338676
    :catch_0
    move-exception v10

    .line 1338677
    const/4 v4, 0x1

    invoke-interface {v5, v4}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1338678
    instance-of v4, v10, Ljava/util/concurrent/ExecutionException;

    if-eqz v4, :cond_1

    .line 1338679
    invoke-virtual {v10}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    check-cast v4, Ljava/lang/Exception;

    move-object v10, v4

    .line 1338680
    :cond_1
    invoke-virtual/range {p3 .. p3}, LX/8OL;->e()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1338681
    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->size()I

    move-result v4

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual {v0, v1, v2, v4}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    .line 1338682
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Upload cancelled at photo #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338683
    :cond_2
    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v7

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    sub-int v8, v4, v5

    instance-of v4, v10, LX/8N0;

    if-eqz v4, :cond_4

    move-object v4, v10

    check-cast v4, LX/8N0;

    move-object v9, v4

    :goto_2
    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-virtual/range {v4 .. v9}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;IILX/73y;)V

    .line 1338684
    throw v10

    .line 1338685
    :cond_3
    :try_start_1
    invoke-virtual/range {p4 .. p6}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1338686
    return-object v21

    .line 1338687
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, LX/8OJ;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v9

    goto :goto_2
.end method
