.class public LX/7WE;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1215983
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1215984
    invoke-direct {p0}, LX/7WE;->a()V

    .line 1215985
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1215986
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1215987
    invoke-direct {p0}, LX/7WE;->a()V

    .line 1215988
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1215989
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1215990
    invoke-direct {p0}, LX/7WE;->a()V

    .line 1215991
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1215992
    const v0, 0x7f030ac9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1215993
    invoke-virtual {p0}, LX/7WE;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/7WE;->setBackgroundColor(I)V

    .line 1215994
    const v0, 0x7f0d1b84

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/7WE;->a:Landroid/view/View;

    .line 1215995
    const v0, 0x7f0d1b88

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/7WE;->b:Landroid/widget/LinearLayout;

    .line 1215996
    const v0, 0x7f0d1b89

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/7WE;->c:Landroid/widget/TextView;

    .line 1215997
    const v0, 0x7f0d1b8a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/7WE;->d:Landroid/widget/Button;

    .line 1215998
    const v0, 0x7f0d1b8b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/7WE;->e:Landroid/widget/TextView;

    .line 1215999
    const v0, 0x7f0d1b85

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/7WE;->f:Landroid/widget/LinearLayout;

    .line 1216000
    const v0, 0x7f0d1b86

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/7WE;->g:Landroid/widget/TextView;

    .line 1216001
    const v0, 0x7f0d1b87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/7WE;->h:Landroid/widget/TextView;

    .line 1216002
    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1216003
    if-eqz p1, :cond_0

    .line 1216004
    iget-object v0, p0, LX/7WE;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1216005
    iget-object v0, p0, LX/7WE;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1216006
    iget-object v0, p0, LX/7WE;->g:Landroid/widget/TextView;

    iget-object v1, p0, LX/7WE;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1216007
    iget-object v0, p0, LX/7WE;->h:Landroid/widget/TextView;

    iget-object v1, p0, LX/7WE;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1216008
    iget-object v0, p0, LX/7WE;->h:Landroid/widget/TextView;

    iget-object v1, p0, LX/7WE;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1216009
    :goto_0
    if-eqz p2, :cond_1

    .line 1216010
    iget-object v0, p0, LX/7WE;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1216011
    :goto_1
    return-void

    .line 1216012
    :cond_0
    iget-object v0, p0, LX/7WE;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1216013
    iget-object v0, p0, LX/7WE;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1216014
    iget-object v0, p0, LX/7WE;->c:Landroid/widget/TextView;

    iget-object v1, p0, LX/7WE;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1216015
    iget-object v0, p0, LX/7WE;->d:Landroid/widget/Button;

    iget-object v1, p0, LX/7WE;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1216016
    iget-object v0, p0, LX/7WE;->d:Landroid/widget/Button;

    iget-object v1, p0, LX/7WE;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1216017
    :cond_1
    iget-object v0, p0, LX/7WE;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
