.class public final LX/6pq;
.super LX/6nn;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nn",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/ResetPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic c:LX/6pr;


# direct methods
.method public constructor <init>(LX/6pr;Lcom/facebook/payments/auth/pin/ResetPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 0

    .prologue
    .line 1149675
    iput-object p1, p0, LX/6pq;->c:LX/6pr;

    iput-object p2, p0, LX/6pq;->a:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    iput-object p3, p0, LX/6pq;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-direct {p0}, LX/6nn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1149676
    iget-object v0, p0, LX/6pq;->a:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c()V

    .line 1149677
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 1149678
    iget-object v0, p0, LX/6pq;->a:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/ResetPinFragment;->d()V

    .line 1149679
    iget-object v0, p0, LX/6pq;->a:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/ResetPinFragment;->b()V

    .line 1149680
    iget-object v0, p0, LX/6pq;->a:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/auth/pin/ResetPinFragment;->a(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v0

    .line 1149681
    if-nez v0, :cond_0

    .line 1149682
    iget-object v0, p0, LX/6pq;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    const v1, 0x7f081de4

    invoke-virtual {v0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(I)V

    .line 1149683
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1149684
    iget-object v0, p0, LX/6pq;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->d()V

    .line 1149685
    return-void
.end method
