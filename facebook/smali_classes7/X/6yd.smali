.class public final LX/6yd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public final synthetic b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

.field public final synthetic c:LX/6ye;


# direct methods
.method public constructor <init>(LX/6ye;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V
    .locals 0

    .prologue
    .line 1160169
    iput-object p1, p0, LX/6yd;->c:LX/6ye;

    iput-object p2, p0, LX/6yd;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object p3, p0, LX/6yd;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1160165
    iget-object v0, p0, LX/6yd;->c:LX/6ye;

    iget-object v1, p0, LX/6yd;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1160166
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1160167
    iget-object v0, p0, LX/6yd;->c:LX/6ye;

    iget-object v1, p0, LX/6yd;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, p0, LX/6yd;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/6ye;->a$redex0(LX/6ye;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;Ljava/lang/String;)V

    .line 1160168
    return-void
.end method
