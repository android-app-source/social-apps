.class public LX/71f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0W9;

.field public b:LX/71a;


# direct methods
.method public constructor <init>(LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163530
    iput-object p1, p0, LX/71f;->a:LX/0W9;

    .line 1163531
    return-void
.end method

.method public static a(LX/0QB;)LX/71f;
    .locals 4

    .prologue
    .line 1163532
    const-class v1, LX/71f;

    monitor-enter v1

    .line 1163533
    :try_start_0
    sget-object v0, LX/71f;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1163534
    sput-object v2, LX/71f;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1163535
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1163536
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1163537
    new-instance p0, LX/71f;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-direct {p0, v3}, LX/71f;-><init>(LX/0W9;)V

    .line 1163538
    move-object v0, p0

    .line 1163539
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1163540
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/71f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1163541
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1163542
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6qh;Lcom/facebook/payments/selector/model/SelectorRow;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1163543
    sget-object v0, LX/71e;->a:[I

    invoke-interface {p2}, Lcom/facebook/payments/selector/model/SelectorRow;->a()LX/71l;

    move-result-object v1

    invoke-virtual {v1}, LX/71l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1163544
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No view found for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/facebook/payments/selector/model/SelectorRow;->a()LX/71l;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1163545
    :pswitch_0
    check-cast p2, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;

    .line 1163546
    if-nez p3, :cond_1

    new-instance p3, LX/71m;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/71m;-><init>(Landroid/content/Context;)V

    .line 1163547
    :goto_0
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 1163548
    iput-object p2, p3, LX/71m;->b:Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;

    .line 1163549
    iget-object v0, p3, LX/71m;->a:Landroid/widget/TextView;

    iget-object p1, p3, LX/71m;->b:Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;

    iget-object p1, p1, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163550
    move-object v0, p3

    .line 1163551
    :goto_1
    return-object v0

    .line 1163552
    :pswitch_1
    check-cast p2, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    .line 1163553
    if-nez p3, :cond_2

    new-instance p3, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p3, v0, v1}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 1163554
    :goto_2
    iget-object v0, p0, LX/71f;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    .line 1163555
    iget-object v1, p2, Lcom/facebook/payments/selector/model/OptionSelectorRow;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v1, :cond_3

    iget-object v1, p2, Lcom/facebook/payments/selector/model/OptionSelectorRow;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object p4, p2, Lcom/facebook/payments/selector/model/OptionSelectorRow;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, p4}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    move-object v0, v1

    .line 1163556
    invoke-virtual {p3, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1163557
    iget-boolean v0, p2, Lcom/facebook/payments/selector/model/OptionSelectorRow;->e:Z

    invoke-virtual {p3, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 1163558
    new-instance v0, LX/71d;

    invoke-direct {v0, p0, p2}, LX/71d;-><init>(LX/71f;Lcom/facebook/payments/selector/model/OptionSelectorRow;)V

    invoke-virtual {p3, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1163559
    move-object v0, p3

    .line 1163560
    goto :goto_1

    .line 1163561
    :pswitch_2
    new-instance v0, LX/73W;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/73W;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 1163562
    :pswitch_3
    check-cast p2, Lcom/facebook/payments/selector/model/FooterSelectorRow;

    .line 1163563
    if-nez p3, :cond_4

    new-instance p3, LX/73Y;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/73Y;-><init>(Landroid/content/Context;)V

    .line 1163564
    :goto_4
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0545

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p3, v0}, LX/73Y;->setLeftAndRightPaddingForChildViews(I)V

    .line 1163565
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1163566
    iget-object v0, p2, Lcom/facebook/payments/selector/model/FooterSelectorRow;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, LX/73Y;->setSecurityInfo(Ljava/lang/String;)V

    .line 1163567
    iget-object v0, p2, Lcom/facebook/payments/selector/model/FooterSelectorRow;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1163568
    iget-object v0, p2, Lcom/facebook/payments/selector/model/FooterSelectorRow;->c:Landroid/net/Uri;

    iget-object v1, p2, Lcom/facebook/payments/selector/model/FooterSelectorRow;->b:Ljava/lang/String;

    .line 1163569
    iget-object p1, p3, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {p1, v0}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setLearnMoreUri(Landroid/net/Uri;)V

    .line 1163570
    iget-object p1, p3, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {p1, v1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setLearnMoreText(Ljava/lang/String;)V

    .line 1163571
    :cond_0
    move-object v0, p3

    .line 1163572
    goto :goto_1

    .line 1163573
    :cond_1
    check-cast p3, LX/71m;

    goto/16 :goto_0

    .line 1163574
    :cond_2
    check-cast p3, Lcom/facebook/fig/listitem/FigListItem;

    goto :goto_2

    :cond_3
    iget-object v1, p2, Lcom/facebook/payments/selector/model/OptionSelectorRow;->b:Ljava/lang/String;

    goto :goto_3

    .line 1163575
    :cond_4
    check-cast p3, LX/73Y;

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
