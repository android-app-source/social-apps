.class public LX/8NP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8NQ;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0lB;


# direct methods
.method public constructor <init>(LX/0SG;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337065
    iput-object p1, p0, LX/8NP;->a:LX/0SG;

    .line 1337066
    iput-object p2, p0, LX/8NP;->b:LX/0lB;

    .line 1337067
    return-void
.end method

.method public static a(LX/0QB;)LX/8NP;
    .locals 3

    .prologue
    .line 1337068
    new-instance v2, LX/8NP;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lB;

    invoke-direct {v2, v0, v1}, LX/8NP;-><init>(LX/0SG;LX/0lB;)V

    .line 1337069
    move-object v0, v2

    .line 1337070
    return-object v0
.end method


# virtual methods
.method public final a(LX/8NQ;)LX/14N;
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 1337071
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1337072
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    .line 1337073
    iget-object v2, p0, LX/8NP;->a:LX/0SG;

    iget-object v3, p0, LX/8NP;->b:LX/0lB;

    invoke-static {v0, v1, p1, v2, v3}, LX/8Na;->a(LX/0Pz;LX/14O;LX/8NQ;LX/0SG;LX/0lB;)V

    .line 1337074
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.3/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1337075
    iget-wide v8, p1, LX/8NQ;->i:J

    move-wide v4, v8

    .line 1337076
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/videos"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1337077
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_phase"

    const-string v5, "finish"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337078
    iget-object v3, p1, LX/8NQ;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1337079
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1337080
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_session_id"

    .line 1337081
    iget-object v5, p1, LX/8NQ;->c:Ljava/lang/String;

    move-object v5, v5

    .line 1337082
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337083
    :cond_0
    iget-object v3, p1, LX/8NQ;->K:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object v3, v3

    .line 1337084
    if-eqz v3, :cond_1

    .line 1337085
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "composer_session_events_log"

    iget-object v5, p0, LX/8NP;->b:LX/0lB;

    .line 1337086
    iget-object v6, p1, LX/8NQ;->K:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object v6, v6

    .line 1337087
    invoke-virtual {v5, v6}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337088
    :cond_1
    iget-boolean v3, p1, LX/8NQ;->M:Z

    move v3, v3

    .line 1337089
    if-eqz v3, :cond_2

    .line 1337090
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "is_member_bio_post"

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337091
    :cond_2
    const-string v3, "upload-video-chunk-post"

    .line 1337092
    iput-object v3, v1, LX/14O;->b:Ljava/lang/String;

    .line 1337093
    move-object v1, v1

    .line 1337094
    const-string v3, "POST"

    .line 1337095
    iput-object v3, v1, LX/14O;->c:Ljava/lang/String;

    .line 1337096
    move-object v1, v1

    .line 1337097
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1337098
    move-object v1, v1

    .line 1337099
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1337100
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1337101
    move-object v1, v1

    .line 1337102
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1337103
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1337104
    move-object v0, v1

    .line 1337105
    iput-boolean v7, v0, LX/14O;->n:Z

    .line 1337106
    move-object v0, v0

    .line 1337107
    iput-boolean v7, v0, LX/14O;->p:Z

    .line 1337108
    move-object v0, v0

    .line 1337109
    iget-object v1, p1, LX/8NQ;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1337110
    iput-object v1, v0, LX/14O;->A:Ljava/lang/String;

    .line 1337111
    move-object v0, v0

    .line 1337112
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)LX/14N;
    .locals 1

    .prologue
    .line 1337113
    check-cast p1, LX/8NQ;

    invoke-virtual {p0, p1}, LX/8NP;->a(LX/8NQ;)LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1337114
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
