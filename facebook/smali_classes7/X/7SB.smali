.class public LX/7SB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;
.implements LX/61G;
.implements LX/6Jv;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:[F


# instance fields
.field public final c:LX/0So;

.field private final d:F

.field public e:LX/5PR;

.field public f:LX/5Pg;

.field public g:LX/5Pg;

.field public h:LX/5Pg;

.field public i:LX/5Pg;

.field public j:F

.field private k:LX/5Pf;

.field private l:LX/5Pb;

.field public m:[F

.field private n:[F

.field public o:Lcom/facebook/videocodec/effects/model/DoodleData;

.field private p:LX/6Js;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1208209
    const-class v0, LX/7SB;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7SB;->a:Ljava/lang/String;

    .line 1208210
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 1208211
    sput-object v0, LX/7SB;->b:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1208212
    return-void
.end method

.method public constructor <init>(LX/0So;Z)V
    .locals 3
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1208367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208368
    iput-object p1, p0, LX/7SB;->c:LX/0So;

    .line 1208369
    if-eqz p2, :cond_0

    const/high16 v0, 0x43fa0000    # 500.0f

    :goto_0
    iput v0, p0, LX/7SB;->d:F

    .line 1208370
    new-instance v0, Lcom/facebook/videocodec/effects/model/DoodleData;

    invoke-direct {v0}, Lcom/facebook/videocodec/effects/model/DoodleData;-><init>()V

    iput-object v0, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    .line 1208371
    iget-object v0, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, v0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    .line 1208372
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/7SB;->m:[F

    .line 1208373
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LX/7SB;->n:[F

    .line 1208374
    iget-object v0, p0, LX/7SB;->m:[F

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1208375
    iput v1, p0, LX/7SB;->j:F

    .line 1208376
    return-void

    :cond_0
    move v0, v1

    .line 1208377
    goto :goto_0
.end method

.method private a(LX/7S9;)V
    .locals 12

    .prologue
    .line 1208317
    if-nez p1, :cond_0

    .line 1208318
    :goto_0
    return-void

    .line 1208319
    :cond_0
    sget-object v0, LX/7SA;->b:[I

    iget-object v1, p1, LX/7S9;->a:LX/7S8;

    invoke-virtual {v1}, LX/7S8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1208320
    :pswitch_0
    iget v0, p1, LX/7S9;->b:F

    iget v1, p1, LX/7S9;->c:F

    const/4 v4, 0x0

    .line 1208321
    iget-object v2, p0, LX/7SB;->m:[F

    const/4 v3, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    const/high16 v9, 0x3f800000    # 1.0f

    move v5, v0

    move v6, v1

    move v7, v4

    invoke-static/range {v2 .. v9}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 1208322
    goto :goto_0

    .line 1208323
    :pswitch_1
    invoke-static {p0}, LX/7SB;->h(LX/7SB;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1208324
    sget-object v2, LX/7SB;->a:Ljava/lang/String;

    const-string v3, "Doodle renderer has reached its max number of points"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208325
    :goto_1
    goto :goto_0

    .line 1208326
    :pswitch_2
    invoke-direct {p0, p1}, LX/7SB;->c(LX/7S9;)V

    goto :goto_0

    .line 1208327
    :pswitch_3
    iget-object v0, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    goto :goto_0

    .line 1208328
    :pswitch_4
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 1208329
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LX/7SB;->e:LX/5PR;

    if-nez v2, :cond_4

    .line 1208330
    :cond_1
    :goto_2
    goto :goto_0

    .line 1208331
    :pswitch_5
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/7SB;->e:LX/5PR;

    if-nez v2, :cond_9

    .line 1208332
    :cond_2
    :goto_3
    goto :goto_0

    .line 1208333
    :cond_3
    invoke-static {p0}, LX/7SB;->g(LX/7SB;)V

    .line 1208334
    new-instance v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;

    iget v3, p1, LX/7S9;->b:F

    iget v4, p1, LX/7S9;->c:F

    iget v5, p1, LX/7S9;->d:I

    iget-object v6, p0, LX/7SB;->c:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    long-to-float v6, v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;-><init>(FFIF)V

    .line 1208335
    iget-object v3, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    .line 1208336
    new-instance v4, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    invoke-direct {v4}, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;-><init>()V

    iput-object v4, v3, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    .line 1208337
    iget-object v4, v3, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    iget-object v5, v3, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1208338
    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, LX/7SB;->a(LX/7SB;Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;F)V

    goto :goto_1

    .line 1208339
    :cond_4
    const/4 v5, -0x1

    .line 1208340
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v4, v2, -0x1

    :goto_4
    if-ltz v4, :cond_8

    .line 1208341
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    .line 1208342
    invoke-virtual {v2}, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a()Z

    move-result v6

    if-nez v6, :cond_5

    move v6, v4

    move-object v7, v2

    .line 1208343
    :goto_5
    if-eqz v7, :cond_1

    move v4, v3

    move v5, v3

    .line 1208344
    :goto_6
    if-ge v4, v6, :cond_6

    .line 1208345
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v5, v2

    .line 1208346
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_6

    .line 1208347
    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .line 1208348
    :cond_6
    iget-object v2, p0, LX/7SB;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v10

    long-to-float v4, v10

    .line 1208349
    iput v4, v7, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->b:F

    move v2, v3

    .line 1208350
    :goto_7
    iget-object v3, v7, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 1208351
    add-int v3, v5, v2

    .line 1208352
    iget-object v6, p0, LX/7SB;->e:LX/5PR;

    iget v6, v6, LX/5PR;->d:I

    if-ge v3, v6, :cond_7

    .line 1208353
    iget-object v6, p0, LX/7SB;->i:LX/5Pg;

    iget-object v6, v6, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v6, v3, v4}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208354
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1208355
    :cond_7
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iput-object v8, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    .line 1208356
    const/4 v2, 0x1

    invoke-static {p0, v2}, LX/7SB;->a(LX/7SB;Z)V

    goto/16 :goto_2

    :cond_8
    move v6, v5

    move-object v7, v8

    goto :goto_5

    .line 1208357
    :cond_9
    iget-object v2, p0, LX/7SB;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    long-to-float v3, v2

    .line 1208358
    const/4 v2, 0x0

    :goto_8
    iget-object v4, p0, LX/7SB;->e:LX/5PR;

    iget v4, v4, LX/5PR;->d:I

    if-ge v2, v4, :cond_b

    .line 1208359
    iget-object v4, p0, LX/7SB;->i:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v2}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_a

    .line 1208360
    iget-object v4, p0, LX/7SB;->i:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v2, v3}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208361
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1208362
    :cond_b
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_c
    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    .line 1208363
    invoke-virtual {v2}, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a()Z

    move-result v5

    if-nez v5, :cond_c

    .line 1208364
    iput v3, v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->b:F

    goto :goto_9

    .line 1208365
    :cond_d
    iget-object v2, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    .line 1208366
    const/4 v2, 0x1

    invoke-static {p0, v2}, LX/7SB;->a(LX/7SB;Z)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(LX/7SB;Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;F)V
    .locals 7

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 1208290
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    if-nez v0, :cond_1

    .line 1208291
    const/16 v6, 0x4e20

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1208292
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/7SB;->f:LX/5Pg;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/7SB;->g:LX/5Pg;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Geometry already initialized"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1208293
    new-instance v0, LX/5Pg;

    const v3, 0x9c40

    new-array v3, v3, [F

    const/4 v5, 0x2

    invoke-direct {v0, v3, v5}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7SB;->f:LX/5Pg;

    .line 1208294
    new-instance v0, LX/5Pg;

    const v3, 0xea60

    new-array v3, v3, [F

    const/4 v5, 0x3

    invoke-direct {v0, v3, v5}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7SB;->g:LX/5Pg;

    .line 1208295
    new-instance v0, LX/5Pg;

    new-array v3, v6, [F

    invoke-direct {v0, v3, v1}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7SB;->h:LX/5Pg;

    .line 1208296
    new-instance v0, LX/5Pg;

    new-array v3, v6, [F

    invoke-direct {v0, v3, v1}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7SB;->i:LX/5Pg;

    .line 1208297
    new-instance v0, LX/5PQ;

    invoke-direct {v0, v2}, LX/5PQ;-><init>(I)V

    .line 1208298
    iput v2, v0, LX/5PQ;->a:I

    .line 1208299
    move-object v0, v0

    .line 1208300
    const-string v1, "a_vertex"

    iget-object v2, p0, LX/7SB;->f:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "a_vertexColor"

    iget-object v2, p0, LX/7SB;->g:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "a_vertexCreationTime"

    iget-object v2, p0, LX/7SB;->h:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "a_vertexUndoTime"

    iget-object v2, p0, LX/7SB;->i:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7SB;->e:LX/5PR;

    .line 1208301
    :cond_0
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget v0, v0, LX/5PR;->d:I

    mul-int/lit8 v0, v0, 0x2

    .line 1208302
    iget-object v1, p0, LX/7SB;->f:LX/5Pg;

    iget-object v1, v1, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v2, p1, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->a:F

    invoke-virtual {v1, v0, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208303
    iget-object v1, p0, LX/7SB;->f:LX/5Pg;

    iget-object v1, v1, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    add-int/lit8 v0, v0, 0x1

    iget v2, p1, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->b:F

    invoke-virtual {v1, v0, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208304
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget v0, v0, LX/5PR;->d:I

    mul-int/lit8 v0, v0, 0x3

    .line 1208305
    iget-object v1, p0, LX/7SB;->g:LX/5Pg;

    iget-object v1, v1, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v2, p1, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->c:I

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    invoke-virtual {v1, v0, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208306
    iget-object v1, p0, LX/7SB;->g:LX/5Pg;

    iget-object v1, v1, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    add-int/lit8 v2, v0, 0x1

    iget v3, p1, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->c:I

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208307
    iget-object v1, p0, LX/7SB;->g:LX/5Pg;

    iget-object v1, v1, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    add-int/lit8 v0, v0, 0x2

    iget v2, p1, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->c:I

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    invoke-virtual {v1, v0, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208308
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget v0, v0, LX/5PR;->d:I

    .line 1208309
    iget-object v1, p0, LX/7SB;->h:LX/5Pg;

    iget-object v1, v1, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v2, p1, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->d:F

    invoke-virtual {v1, v0, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208310
    iget-object v1, p0, LX/7SB;->i:LX/5Pg;

    iget-object v1, v1, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0, p2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 1208311
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget v1, v0, LX/5PR;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/5PR;->d:I

    .line 1208312
    iget-object v0, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1208313
    :goto_1
    return-void

    .line 1208314
    :cond_1
    invoke-static {p0}, LX/7SB;->h(LX/7SB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208315
    sget-object v0, LX/7SB;->a:Ljava/lang/String;

    const-string v1, "Doodle renderer has reached its max number of points"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1208316
    goto/16 :goto_0
.end method

.method public static a(LX/7SB;Z)V
    .locals 2

    .prologue
    .line 1208287
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/7SB;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    long-to-float v0, v0

    :goto_0
    iput v0, p0, LX/7SB;->j:F

    .line 1208288
    return-void

    .line 1208289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/7S9;)V
    .locals 20

    .prologue
    .line 1208267
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    if-nez v2, :cond_1

    .line 1208268
    :cond_0
    return-void

    .line 1208269
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Should not be attempting to update an empty line"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1208270
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v3, v3, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    iget-object v3, v3, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;

    .line 1208271
    iget v4, v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->a:F

    .line 1208272
    iget v5, v2, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->b:F

    .line 1208273
    move-object/from16 v0, p1

    iget v6, v0, LX/7S9;->b:F

    .line 1208274
    move-object/from16 v0, p1

    iget v7, v0, LX/7S9;->c:F

    .line 1208275
    add-float v2, v4, v6

    const/high16 v3, 0x40000000    # 2.0f

    div-float v8, v2, v3

    .line 1208276
    add-float v2, v5, v7

    const/high16 v3, 0x40000000    # 2.0f

    div-float v9, v2, v3

    .line 1208277
    sub-float v2, v6, v4

    sub-float v3, v6, v4

    mul-float/2addr v2, v3

    sub-float v3, v7, v5

    sub-float v10, v7, v5

    mul-float/2addr v3, v10

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide/high16 v10, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    double-to-int v10, v2

    .line 1208278
    const/4 v3, 0x0

    .line 1208279
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v10, :cond_0

    .line 1208280
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    float-to-double v14, v3

    sub-double/2addr v12, v14

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v3

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    float-to-double v14, v4

    mul-double/2addr v12, v14

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v3

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    mul-double v14, v14, v16

    float-to-double v0, v3

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    float-to-double v0, v8

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    mul-float v11, v3, v3

    mul-float/2addr v11, v6

    float-to-double v14, v11

    add-double/2addr v12, v14

    double-to-float v11, v12

    .line 1208281
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    float-to-double v14, v3

    sub-double/2addr v12, v14

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v3

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    float-to-double v14, v5

    mul-double/2addr v12, v14

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v3

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    mul-double v14, v14, v16

    float-to-double v0, v3

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    float-to-double v0, v9

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    mul-float v14, v3, v3

    mul-float/2addr v14, v7

    float-to-double v14, v14

    add-double/2addr v12, v14

    double-to-float v12, v12

    .line 1208282
    new-instance v13, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;

    move-object/from16 v0, p1

    iget v14, v0, LX/7S9;->d:I

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7SB;->c:LX/0So;

    invoke-interface {v15}, LX/0So;->now()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-float v15, v0

    invoke-direct {v13, v11, v12, v14, v15}, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;-><init>(FFIF)V

    .line 1208283
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v11}, LX/7SB;->a(LX/7SB;Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;F)V

    .line 1208284
    float-to-double v12, v3

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    int-to-double v0, v10

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v3, v12

    .line 1208285
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1208286
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public static g(LX/7SB;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1208257
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    if-nez v0, :cond_0

    .line 1208258
    :goto_0
    return-void

    .line 1208259
    :cond_0
    iget-object v0, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    move v2, v3

    :goto_1
    if-ltz v1, :cond_1

    .line 1208260
    iget-object v0, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    .line 1208261
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1208262
    iget-object v0, v0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v2, v0

    .line 1208263
    iget-object v0, p0, LX/7SB;->o:Lcom/facebook/videocodec/effects/model/DoodleData;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1208264
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 1208265
    :cond_1
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget-object v1, p0, LX/7SB;->e:LX/5PR;

    iget v1, v1, LX/5PR;->d:I

    sub-int/2addr v1, v2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, LX/5PR;->d:I

    .line 1208266
    invoke-static {p0, v3}, LX/7SB;->a(LX/7SB;Z)V

    goto :goto_0
.end method

.method public static h(LX/7SB;)Z
    .locals 2

    .prologue
    .line 1208256
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget v0, v0, LX/5PR;->d:I

    const/16 v1, 0x4e20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1208255
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 5

    .prologue
    const v4, 0x812f

    const/16 v3, 0x2601

    .line 1208378
    const v0, 0x7f070024

    const v1, 0x7f070023

    invoke-interface {p1, v0, v1}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7SB;->l:LX/5Pb;

    .line 1208379
    invoke-interface {p1}, LX/5Pc;->a()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020b59

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1208380
    new-instance v1, LX/5Pe;

    invoke-direct {v1}, LX/5Pe;-><init>()V

    const/16 v2, 0x2801

    invoke-virtual {v1, v2, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    const/16 v2, 0x2800

    invoke-virtual {v1, v2, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    const/16 v2, 0x2802

    invoke-virtual {v1, v2, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    const/16 v2, 0x2803

    invoke-virtual {v1, v2, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1208381
    iput-object v0, v1, LX/5Pe;->c:Landroid/graphics/Bitmap;

    .line 1208382
    move-object v0, v1

    .line 1208383
    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    iput-object v0, p0, LX/7SB;->k:LX/5Pf;

    .line 1208384
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    if-eqz v0, :cond_0

    .line 1208385
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    sget-object v1, LX/7Sc;->DOODLE_DATA:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1208386
    :cond_0
    return-void
.end method

.method public final a(LX/61H;)V
    .locals 0

    .prologue
    .line 1208254
    return-void
.end method

.method public final a(LX/6Js;)V
    .locals 2

    .prologue
    .line 1208248
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    if-eqz v0, :cond_0

    .line 1208249
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    sget-object v1, LX/7Sc;->DOODLE_DATA:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1208250
    :cond_0
    iput-object p1, p0, LX/7SB;->p:LX/6Js;

    .line 1208251
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    if-eqz v0, :cond_1

    .line 1208252
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    sget-object v1, LX/7Sc;->DOODLE_DATA:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1208253
    :cond_1
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 2

    .prologue
    .line 1208240
    sget-object v0, LX/7SA;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1208241
    sget-object v0, LX/7SB;->a:Ljava/lang/String;

    const-string v1, "Received an event we did not register for"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208242
    :goto_0
    return-void

    .line 1208243
    :pswitch_0
    check-cast p1, LX/7Sg;

    .line 1208244
    iget-object v0, p1, LX/7Sg;->b:LX/7S9;

    move-object v0, v0

    .line 1208245
    invoke-direct {p0, v0}, LX/7SB;->a(LX/7S9;)V

    .line 1208246
    iget-object v0, p1, LX/7Sg;->a:Ljava/util/List;

    move-object v0, v0

    .line 1208247
    invoke-virtual {p0, v0}, LX/7SB;->a(Ljava/util/List;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7S9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1208235
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 1208236
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7S9;

    .line 1208237
    invoke-direct {p0, v0}, LX/7SB;->a(LX/7S9;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1208238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1208239
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 8

    .prologue
    .line 1208220
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget v0, v0, LX/5PR;->d:I

    if-nez v0, :cond_1

    .line 1208221
    :cond_0
    :goto_0
    return-void

    .line 1208222
    :cond_1
    const-string v0, "onDrawFrame"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208223
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1208224
    const-string v0, "GL_BLEND"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208225
    const/4 v0, 0x1

    const/16 v1, 0x303

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 1208226
    const-string v0, "blendFunc"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208227
    const/16 v0, 0xba2

    iget-object v1, p0, LX/7SB;->n:[F

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGetFloatv(I[FI)V

    .line 1208228
    iget-object v0, p0, LX/7SB;->n:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    iget-object v1, p0, LX/7SB;->n:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const v1, 0x3c888889

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1208229
    iget-object v1, p0, LX/7SB;->l:LX/5Pb;

    invoke-virtual {v1}, LX/5Pb;->a()LX/5Pa;

    move-result-object v1

    .line 1208230
    const-string v2, "pointSize"

    invoke-virtual {v1, v2, v0}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v0

    const-string v2, "currentTime"

    iget-object v3, p0, LX/7SB;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    long-to-float v3, v4

    invoke-virtual {v0, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v0

    const-string v2, "lightenDurationMs"

    iget v3, p0, LX/7SB;->d:F

    invoke-virtual {v0, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v0

    const-string v2, "undoDurationMs"

    const/high16 v3, 0x43af0000    # 350.0f

    invoke-virtual {v0, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v0

    const-string v2, "uConstMatrix"

    iget-object v3, p0, LX/7SB;->m:[F

    invoke-virtual {v0, v2, v3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v2, "uMVPMatrix"

    sget-object v3, LX/7SB;->b:[F

    invoke-virtual {v0, v2, v3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v2, "sTexture"

    iget-object v3, p0, LX/7SB;->k:LX/5Pf;

    invoke-virtual {v0, v2, v3}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    .line 1208231
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    invoke-virtual {v1, v0}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1208232
    iget v6, p0, LX/7SB;->j:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_2

    iget-object v6, p0, LX/7SB;->c:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    long-to-float v6, v6

    iget v7, p0, LX/7SB;->j:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x43af0000    # 350.0f

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_2

    const/4 v6, 0x1

    :goto_1
    move v0, v6

    .line 1208233
    if-eqz v0, :cond_0

    .line 1208234
    invoke-static {p0}, LX/7SB;->g(LX/7SB;)V

    goto/16 :goto_0

    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1208217
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    if-eqz v0, :cond_0

    .line 1208218
    iget-object v0, p0, LX/7SB;->p:LX/6Js;

    sget-object v1, LX/7Sc;->DOODLE_DATA:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1208219
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1208216
    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7SB;->e:LX/5PR;

    iget v0, v0, LX/5PR;->d:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1208213
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1208214
    const-string v1, "filter_type"

    const-string v2, "doodle"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208215
    return-object v0
.end method
