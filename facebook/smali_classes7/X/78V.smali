.class public final LX/78V;
.super LX/4mf;
.source ""

# interfaces
.implements LX/39D;


# instance fields
.field public final synthetic b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

.field public c:LX/1wz;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;LX/1wz;)V
    .locals 2

    .prologue
    .line 1172727
    iput-object p1, p0, LX/78V;->b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    .line 1172728
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LX/4mf;-><init>(Lcom/facebook/ui/dialogs/FbDialogFragment;Landroid/content/Context;I)V

    .line 1172729
    iput-object p2, p0, LX/78V;->c:LX/1wz;

    .line 1172730
    iget-object v0, p0, LX/78V;->c:LX/1wz;

    if-eqz v0, :cond_0

    .line 1172731
    iget-object v0, p0, LX/78V;->c:LX/1wz;

    .line 1172732
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 1172733
    iget-object v0, p0, LX/78V;->c:LX/1wz;

    const/4 v1, 0x3

    new-array v1, v1, [LX/31M;

    const/4 p1, 0x0

    sget-object p2, LX/31M;->DOWN:LX/31M;

    aput-object p2, v1, p1

    const/4 p1, 0x1

    sget-object p2, LX/31M;->LEFT:LX/31M;

    aput-object p2, v1, p1

    const/4 p1, 0x2

    sget-object p2, LX/31M;->RIGHT:LX/31M;

    aput-object p2, v1, p1

    invoke-virtual {v0, v1}, LX/1wz;->a([LX/31M;)V

    .line 1172734
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1172709
    return-void
.end method

.method public final a(LX/31M;I)V
    .locals 2

    .prologue
    .line 1172722
    iget-object v0, p0, LX/78V;->b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v1, LX/78U;->INTRO_COLLAPSED:LX/78U;

    if-ne v0, v1, :cond_0

    .line 1172723
    iget-object v0, p0, LX/78V;->b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object v1, LX/7FI;->DISMISS_TOAST:LX/7FI;

    invoke-virtual {v0, v1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FI;)V

    .line 1172724
    iget-object v0, p0, LX/78V;->b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->k()V

    .line 1172725
    iget-object v0, p0, LX/78V;->b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->a(LX/31M;Z)V

    .line 1172726
    :cond_0
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 1172721
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1172720
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 0

    .prologue
    .line 1172719
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1172710
    iget-object v0, p0, LX/78V;->b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    const/4 v1, 0x0

    .line 1172711
    iget-object v2, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v3, LX/78U;->EXPANDED:LX/78U;

    if-ne v2, v3, :cond_2

    .line 1172712
    :cond_0
    :goto_0
    move v0, v1

    .line 1172713
    if-eqz v0, :cond_1

    .line 1172714
    iget-object v0, p0, LX/78V;->b:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1172715
    const/4 v0, 0x0

    .line 1172716
    :goto_1
    return v0

    .line 1172717
    :cond_1
    iget-object v0, p0, LX/78V;->c:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    .line 1172718
    invoke-super {p0, p1}, LX/4mf;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v2, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    invoke-virtual {v2}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->getUpperBound()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
