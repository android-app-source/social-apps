.class public final LX/7mP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7mW;


# direct methods
.method public constructor <init>(LX/7mW;)V
    .locals 0

    .prologue
    .line 1236981
    iput-object p1, p0, LX/7mP;->a:LX/7mW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 1236982
    iget-object v0, p0, LX/7mP;->a:LX/7mW;

    const/4 v6, 0x0

    .line 1236983
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 1236984
    :try_start_0
    iget-object v8, v0, LX/7mW;->f:LX/7mO;

    invoke-static {v0}, LX/7mW;->g(LX/7mW;)J

    move-result-wide v10

    .line 1236985
    iget-object v9, v8, LX/7mO;->a:LX/7mK;

    invoke-virtual {v9}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    iget-object v12, v8, LX/7mO;->c:LX/0Uh;

    invoke-static {v12}, LX/7mN;->a(LX/0Uh;)I

    move-result v12

    invoke-virtual {v8, v10, v11, v9, v12}, LX/7mO;->a(JLandroid/database/sqlite/SQLiteDatabase;I)LX/0Px;

    move-result-object v9

    move-object v6, v9
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1236986
    :goto_0
    move-object v0, v6

    .line 1236987
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1236988
    iget-object v2, p0, LX/7mP;->a:LX/7mW;

    iget-object v2, v2, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 1236989
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mj;

    .line 1236990
    iget-object v4, p0, LX/7mP;->a:LX/7mW;

    invoke-virtual {v0}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/7mV;->e(Ljava/lang/String;)I

    move-result v4

    .line 1236991
    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 1236992
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1236993
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1236994
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/7mP;->a:LX/7mW;

    iget-object v0, v0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1236995
    iget-object v0, p0, LX/7mP;->a:LX/7mW;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1236996
    iput-object v1, v0, LX/7mW;->k:LX/0am;

    .line 1236997
    iget-object v0, p0, LX/7mP;->a:LX/7mW;

    sget-object v1, LX/7mU;->FETCHED:LX/7mU;

    .line 1236998
    iput-object v1, v0, LX/7mW;->j:LX/7mU;

    .line 1236999
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1237000
    iget-object v0, p0, LX/7mP;->a:LX/7mW;

    .line 1237001
    iget-object v1, v0, LX/7mW;->g:LX/0TD;

    new-instance v2, Lcom/facebook/compost/store/CompostDraftStoryStore$5;

    invoke-direct {v2, v0}, Lcom/facebook/compost/store/CompostDraftStoryStore$5;-><init>(LX/7mW;)V

    const v3, -0x8652355

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1237002
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1237003
    :catch_0
    move-exception v8

    .line 1237004
    sget-object v9, LX/7mW;->e:Ljava/lang/String;

    const-string v10, "Error Reading from DB."

    new-array v11, v6, [Ljava/lang/Object;

    invoke-static {v9, v8, v10, v11}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1237005
    iget-object v8, v0, LX/7mW;->f:LX/7mO;

    const/4 v11, 0x0

    .line 1237006
    iget-object v9, v8, LX/7mO;->a:LX/7mK;

    invoke-virtual {v9}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const-string v10, "draft_story"

    invoke-virtual {v9, v10, v11, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1237007
    iget-object v8, v0, LX/7mW;->l:LX/1RX;

    iget-object v9, v0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    const/4 v6, 0x1

    :cond_2
    invoke-virtual {v8, v6}, LX/1RX;->a(Z)V

    move-object v6, v7

    goto/16 :goto_0
.end method
