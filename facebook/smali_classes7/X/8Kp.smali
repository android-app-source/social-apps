.class public LX/8Kp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/8Kp;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/8LV;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;

.field public final f:Ljava/util/Map;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1330761
    const-class v0, LX/8Kp;

    sput-object v0, LX/8Kp;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/8LV;LX/0Ot;LX/0ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;",
            "LX/8LV;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1330754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1330755
    iput-object p1, p0, LX/8Kp;->b:LX/0Ot;

    .line 1330756
    iput-object p2, p0, LX/8Kp;->c:LX/8LV;

    .line 1330757
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    .line 1330758
    iput-object p4, p0, LX/8Kp;->e:LX/0ad;

    .line 1330759
    iput-object p3, p0, LX/8Kp;->d:LX/0Ot;

    .line 1330760
    return-void
.end method

.method public static a(LX/0QB;)LX/8Kp;
    .locals 7

    .prologue
    .line 1330741
    sget-object v0, LX/8Kp;->g:LX/8Kp;

    if-nez v0, :cond_1

    .line 1330742
    const-class v1, LX/8Kp;

    monitor-enter v1

    .line 1330743
    :try_start_0
    sget-object v0, LX/8Kp;->g:LX/8Kp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1330744
    if-eqz v2, :cond_0

    .line 1330745
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1330746
    new-instance v5, LX/8Kp;

    const/16 v3, 0xf39

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v3

    check-cast v3, LX/8LV;

    const/16 v4, 0x271

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v6, v3, p0, v4}, LX/8Kp;-><init>(LX/0Ot;LX/8LV;LX/0Ot;LX/0ad;)V

    .line 1330747
    move-object v0, v5

    .line 1330748
    sput-object v0, LX/8Kp;->g:LX/8Kp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1330749
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1330750
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1330751
    :cond_1
    sget-object v0, LX/8Kp;->g:LX/8Kp;

    return-object v0

    .line 1330752
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1330753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8Kp;)Z
    .locals 3

    .prologue
    .line 1330740
    iget-object v0, p0, LX/8Kp;->e:LX/0ad;

    const v1, -0x6872

    sget-object v2, LX/8K0;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/photos/upload/operation/UploadOperation;Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1330736
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 1330737
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    .line 1330738
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 1330739
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static c(LX/8Kp;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1330762
    invoke-static {p0}, LX/8Kp;->a(LX/8Kp;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1330763
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330764
    sget-object v0, LX/8Kp;->a:Ljava/lang/Class;

    const-string v1, "Canceling optimistic upload for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1330765
    iget-object v0, p0, LX/8Kp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v1, p0, LX/8Kp;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/upload/operation/UploadOperation;

    const-string v2, "optimistic story"

    invoke-virtual {v0, v1, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 1330766
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "J)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1330722
    iget-object v0, p0, LX/8Kp;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1330723
    invoke-static {p0}, LX/8Kp;->a(LX/8Kp;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1330724
    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    if-eqz v0, :cond_1

    .line 1330725
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    .line 1330726
    iget-object v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v4

    .line 1330727
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1330728
    :cond_0
    invoke-static {p0, p2}, LX/8Kp;->c(LX/8Kp;Ljava/lang/String;)V

    .line 1330729
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v1, :cond_2

    .line 1330730
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1330731
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    .line 1330732
    iget-object v1, p0, LX/8Kp;->c:LX/8LV;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    move-object v3, p2

    move-wide v4, p4

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, LX/8LV;->a(Lcom/facebook/ipc/media/MediaItem;Ljava/lang/String;JLcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    .line 1330733
    iget-object v0, p0, LX/8Kp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    invoke-virtual {v0, v1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1330734
    :cond_2
    return-void

    :cond_3
    move v0, v3

    .line 1330735
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 1

    .prologue
    .line 1330721
    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    return-object v0
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 3

    .prologue
    .line 1330710
    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    .line 1330711
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1330712
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330713
    if-eqz v0, :cond_1

    .line 1330714
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1330715
    const-string v2, "cancel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 1330716
    if-nez v1, :cond_0

    invoke-static {v0, p1}, LX/8Kp;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1330717
    :cond_0
    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    .line 1330718
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1330719
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330720
    :cond_1
    return-void
.end method

.method public final c(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 1330703
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330704
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v0, v0

    .line 1330705
    if-eqz v0, :cond_0

    .line 1330706
    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    .line 1330707
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1330708
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330709
    :cond_0
    return-void
.end method

.method public final d(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1330679
    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    .line 1330680
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1330681
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    if-eqz v0, :cond_1

    .line 1330682
    iget-boolean v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v1, v1

    .line 1330683
    iput-boolean v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    .line 1330684
    const/4 v3, 0x0

    .line 1330685
    invoke-static {p1}, LX/8Om;->e(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8Ob;

    move-result-object v1

    .line 1330686
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330687
    iget-object v0, v1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330688
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v2

    .line 1330689
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    .line 1330690
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 1330691
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1330692
    iget-object v0, v1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330693
    iget-boolean v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v2, v2

    .line 1330694
    iput-boolean v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    .line 1330695
    iput-object p1, v1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330696
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v0, v0

    .line 1330697
    if-nez v0, :cond_0

    .line 1330698
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/8Ob;->a(Z)V

    .line 1330699
    :cond_0
    iget-object v0, p0, LX/8Kp;->f:Ljava/util/Map;

    .line 1330700
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1330701
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330702
    :cond_1
    return-void
.end method
