.class public final enum LX/7K7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7K7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7K7;

.field public static final enum VIDEO_SOURCE_HLS:LX/7K7;

.field public static final enum VIDEO_SOURCE_INVALID:LX/7K7;

.field public static final enum VIDEO_SOURCE_RTMP:LX/7K7;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1194495
    new-instance v0, LX/7K7;

    const-string v1, "VIDEO_SOURCE_HLS"

    const-string v2, "hls"

    invoke-direct {v0, v1, v3, v2}, LX/7K7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7K7;->VIDEO_SOURCE_HLS:LX/7K7;

    .line 1194496
    new-instance v0, LX/7K7;

    const-string v1, "VIDEO_SOURCE_RTMP"

    const-string v2, "rtmp"

    invoke-direct {v0, v1, v4, v2}, LX/7K7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7K7;->VIDEO_SOURCE_RTMP:LX/7K7;

    .line 1194497
    new-instance v0, LX/7K7;

    const-string v1, "VIDEO_SOURCE_INVALID"

    const-string v2, "invalid"

    invoke-direct {v0, v1, v5, v2}, LX/7K7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7K7;->VIDEO_SOURCE_INVALID:LX/7K7;

    .line 1194498
    const/4 v0, 0x3

    new-array v0, v0, [LX/7K7;

    sget-object v1, LX/7K7;->VIDEO_SOURCE_HLS:LX/7K7;

    aput-object v1, v0, v3

    sget-object v1, LX/7K7;->VIDEO_SOURCE_RTMP:LX/7K7;

    aput-object v1, v0, v4

    sget-object v1, LX/7K7;->VIDEO_SOURCE_INVALID:LX/7K7;

    aput-object v1, v0, v5

    sput-object v0, LX/7K7;->$VALUES:[LX/7K7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1194492
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1194493
    iput-object p3, p0, LX/7K7;->value:Ljava/lang/String;

    .line 1194494
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7K7;
    .locals 1

    .prologue
    .line 1194500
    const-class v0, LX/7K7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7K7;

    return-object v0
.end method

.method public static values()[LX/7K7;
    .locals 1

    .prologue
    .line 1194499
    sget-object v0, LX/7K7;->$VALUES:[LX/7K7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7K7;

    return-object v0
.end method
