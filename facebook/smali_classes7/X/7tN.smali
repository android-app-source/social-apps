.class public final LX/7tN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1267734
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1267735
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267736
    :goto_0
    return v1

    .line 1267737
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 1267738
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1267739
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1267740
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1267741
    const-string v11, "upcoming_connected_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1267742
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v2

    goto :goto_1

    .line 1267743
    :cond_1
    const-string v11, "upcoming_hosted_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1267744
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 1267745
    :cond_2
    const-string v11, "upcoming_invited_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1267746
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 1267747
    :cond_3
    const-string v11, "upcoming_saved_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1267748
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 1267749
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1267750
    :cond_5
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1267751
    if-eqz v5, :cond_6

    .line 1267752
    invoke-virtual {p1, v1, v9, v1}, LX/186;->a(III)V

    .line 1267753
    :cond_6
    if-eqz v4, :cond_7

    .line 1267754
    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 1267755
    :cond_7
    if-eqz v3, :cond_8

    .line 1267756
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 1267757
    :cond_8
    if-eqz v0, :cond_9

    .line 1267758
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 1267759
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1267760
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267761
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1267762
    if-eqz v0, :cond_0

    .line 1267763
    const-string v1, "upcoming_connected_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267764
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1267765
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1267766
    if-eqz v0, :cond_1

    .line 1267767
    const-string v1, "upcoming_hosted_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267768
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1267769
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1267770
    if-eqz v0, :cond_2

    .line 1267771
    const-string v1, "upcoming_invited_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267772
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1267773
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1267774
    if-eqz v0, :cond_3

    .line 1267775
    const-string v1, "upcoming_saved_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267776
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1267777
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267778
    return-void
.end method
