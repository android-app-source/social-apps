.class public final LX/7Rt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:LX/7Rw;


# direct methods
.method public constructor <init>(LX/7Rw;)V
    .locals 0

    .prologue
    .line 1207940
    iput-object p1, p0, LX/7Rt;->a:LX/7Rw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x53607b25

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1207941
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 1207942
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1207943
    iget-object v1, p0, LX/7Rt;->a:LX/7Rw;

    iget-object v1, v1, LX/7Rw;->c:LX/378;

    .line 1207944
    sget-object v2, LX/7Rp;->APP_FOREGROUNDED:LX/7Rp;

    invoke-static {v1, v2}, LX/378;->a(LX/378;LX/7Rp;)LX/0oG;

    move-result-object v2

    invoke-static {v2}, LX/378;->a(LX/0oG;)V

    .line 1207945
    :cond_0
    :goto_0
    const v1, -0x72619a03

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 1207946
    :cond_1
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1207947
    iget-object v1, p0, LX/7Rt;->a:LX/7Rw;

    iget-object v1, v1, LX/7Rw;->c:LX/378;

    .line 1207948
    sget-object v2, LX/7Rp;->APP_BACKGROUNDED:LX/7Rp;

    invoke-static {v1, v2}, LX/378;->a(LX/378;LX/7Rp;)LX/0oG;

    move-result-object v2

    invoke-static {v2}, LX/378;->a(LX/0oG;)V

    .line 1207949
    goto :goto_0
.end method
