.class public LX/7D6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1182324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/7D7;)LX/5Pg;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x4

    const/4 v1, 0x0

    .line 1182325
    const/16 v0, 0xc

    new-array v8, v0, [F

    .line 1182326
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 1182327
    iget v0, p0, LX/7D7;->a:I

    move v0, v0

    .line 1182328
    int-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v0, v2

    .line 1182329
    const/high16 v2, 0x40000000    # 2.0f

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 1182330
    iget v2, p0, LX/7D7;->c:I

    move v2, v2

    .line 1182331
    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 1182332
    iget v3, p0, LX/7D7;->d:I

    move v3, v3

    .line 1182333
    int-to-float v3, v3

    mul-float/2addr v3, v0

    .line 1182334
    const/high16 v4, -0x40800000    # -1.0f

    add-float/2addr v2, v4

    .line 1182335
    add-float v4, v2, v0

    .line 1182336
    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v3, v5, v3

    .line 1182337
    sub-float v0, v3, v0

    .line 1182338
    new-array v9, v10, [[F

    new-array v5, v10, [F

    aput v2, v5, v1

    aput v3, v5, v11

    const/high16 v6, -0x40800000    # -1.0f

    aput v6, v5, v12

    const/4 v6, 0x0

    aput v6, v5, v13

    aput-object v5, v9, v1

    new-array v5, v10, [F

    aput v4, v5, v1

    aput v3, v5, v11

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, v5, v12

    const/4 v3, 0x0

    aput v3, v5, v13

    aput-object v5, v9, v11

    new-array v3, v10, [F

    aput v2, v3, v1

    aput v0, v3, v11

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v3, v12

    const/4 v2, 0x0

    aput v2, v3, v13

    aput-object v3, v9, v12

    new-array v2, v10, [F

    aput v4, v2, v1

    aput v0, v2, v11

    const/high16 v0, -0x40800000    # -1.0f

    aput v0, v2, v12

    const/4 v0, 0x0

    aput v0, v2, v13

    aput-object v2, v9, v13

    .line 1182339
    sget-object v0, LX/7E4;->a:[[F

    .line 1182340
    iget v2, p0, LX/7D7;->b:I

    move v2, v2

    .line 1182341
    aget-object v2, v0, v2

    .line 1182342
    new-array v0, v10, [F

    move v6, v1

    move v7, v1

    .line 1182343
    :goto_0
    if-ge v6, v10, :cond_0

    .line 1182344
    aget-object v4, v9, v6

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1182345
    add-int/lit8 v3, v7, 0x1

    aget v4, v0, v1

    aput v4, v8, v7

    .line 1182346
    add-int/lit8 v5, v3, 0x1

    aget v4, v0, v11

    aput v4, v8, v3

    .line 1182347
    add-int/lit8 v4, v5, 0x1

    aget v3, v0, v12

    aput v3, v8, v5

    .line 1182348
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v7, v4

    goto :goto_0

    .line 1182349
    :cond_0
    new-instance v0, LX/5Pg;

    invoke-direct {v0, v8, v13}, LX/5Pg;-><init>([FI)V

    return-object v0
.end method

.method public static b(LX/7D7;LX/7D7;)LX/5Pg;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/high16 v5, 0x3b000000    # 0.001953125f

    .line 1182350
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    .line 1182351
    iget v2, p0, LX/7D7;->a:I

    move v2, v2

    .line 1182352
    iget v3, p1, LX/7D7;->a:I

    move v3, v3

    .line 1182353
    sub-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-float v0, v0

    .line 1182354
    const/high16 v1, 0x3f7f0000

    div-float/2addr v1, v0

    .line 1182355
    iget v2, p0, LX/7D7;->c:I

    move v2, v2

    .line 1182356
    int-to-float v2, v2

    .line 1182357
    iget v3, p1, LX/7D7;->c:I

    move v3, v3

    .line 1182358
    int-to-float v3, v3

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    .line 1182359
    iget v3, p0, LX/7D7;->d:I

    move v3, v3

    .line 1182360
    int-to-float v3, v3

    .line 1182361
    iget v4, p1, LX/7D7;->d:I

    move v4, v4

    .line 1182362
    int-to-float v4, v4

    mul-float/2addr v0, v4

    sub-float v0, v3, v0

    .line 1182363
    mul-float/2addr v2, v1

    add-float/2addr v2, v5

    .line 1182364
    mul-float/2addr v0, v1

    add-float/2addr v0, v5

    .line 1182365
    const/16 v3, 0x8

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v2, v3, v4

    const/4 v4, 0x1

    aput v0, v3, v4

    add-float v4, v2, v1

    aput v4, v3, v6

    const/4 v4, 0x3

    aput v0, v3, v4

    const/4 v4, 0x4

    aput v2, v3, v4

    const/4 v4, 0x5

    add-float v5, v0, v1

    aput v5, v3, v4

    const/4 v4, 0x6

    add-float/2addr v2, v1

    aput v2, v3, v4

    const/4 v2, 0x7

    add-float/2addr v0, v1

    aput v0, v3, v2

    .line 1182366
    new-instance v0, LX/5Pg;

    invoke-direct {v0, v3, v6}, LX/5Pg;-><init>([FI)V

    return-object v0
.end method
