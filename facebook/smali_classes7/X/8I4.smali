.class public LX/8I4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/74h;

.field public final b:Landroid/content/ContentResolver;

.field public final c:LX/03V;


# direct methods
.method public constructor <init>(LX/74h;Landroid/content/ContentResolver;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1322083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1322084
    iput-object p1, p0, LX/8I4;->a:LX/74h;

    .line 1322085
    iput-object p2, p0, LX/8I4;->b:Landroid/content/ContentResolver;

    .line 1322086
    iput-object p3, p0, LX/8I4;->c:LX/03V;

    .line 1322087
    return-void
.end method

.method public static a(Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1322082
    const-string v0, "%s <= %d AND %s >= %d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p0, v1, v2

    const/4 v2, 0x3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1322081
    const-string v0, "%s in (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    const-string v2, ","

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/8I4;Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1322079
    iget-object v0, p0, LX/8I4;->a:LX/74h;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, LX/74h;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/8I4;->a(Landroid/database/Cursor;Ljava/util/Map;)V

    .line 1322080
    return-void
.end method

.method private a(Landroid/database/Cursor;Ljava/util/Map;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1322068
    if-nez p1, :cond_0

    .line 1322069
    :goto_0
    return-void

    .line 1322070
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/8I4;->a:LX/74h;

    .line 1322071
    new-instance v1, LX/74f;

    invoke-direct {v1, v0, p1}, LX/74f;-><init>(LX/74h;Landroid/database/Cursor;)V

    move-object v1, v1

    .line 1322072
    :cond_1
    :goto_1
    invoke-virtual {v1}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1322073
    invoke-virtual {v1}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1322074
    if-eqz v0, :cond_1

    .line 1322075
    iget-object v2, v0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v2, v2

    .line 1322076
    iget-wide v4, v2, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v2, v4

    .line 1322077
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1322078
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/8I4;
    .locals 4

    .prologue
    .line 1322064
    new-instance v3, LX/8I4;

    invoke-static {p0}, LX/74h;->b(LX/0QB;)LX/74h;

    move-result-object v0

    check-cast v0, LX/74h;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v1, v2}, LX/8I4;-><init>(LX/74h;Landroid/content/ContentResolver;LX/03V;)V

    .line 1322065
    return-object v3
.end method

.method public static b(LX/8I4;Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1322066
    iget-object v0, p0, LX/8I4;->a:LX/74h;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, LX/74h;->b(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/8I4;->a(Landroid/database/Cursor;Ljava/util/Map;)V

    .line 1322067
    return-void
.end method
