.class public final LX/6gl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1126125
    const-wide/16 v8, 0x0

    .line 1126126
    const-wide/16 v6, 0x0

    .line 1126127
    const-wide/16 v4, 0x0

    .line 1126128
    const/4 v2, 0x0

    .line 1126129
    const/4 v1, 0x0

    .line 1126130
    const/4 v0, 0x0

    .line 1126131
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v10, :cond_8

    .line 1126132
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1126133
    const/4 v0, 0x0

    .line 1126134
    :goto_0
    return v0

    .line 1126135
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_4

    .line 1126136
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1126137
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1126138
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1126139
    const-string v4, "x"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1126140
    const/4 v0, 0x1

    .line 1126141
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1126142
    :cond_1
    const-string v4, "y"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1126143
    const/4 v0, 0x1

    .line 1126144
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v7, v0

    move-wide v10, v4

    goto :goto_1

    .line 1126145
    :cond_2
    const-string v4, "z"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1126146
    const/4 v0, 0x1

    .line 1126147
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 1126148
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1126149
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1126150
    if-eqz v1, :cond_5

    .line 1126151
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1126152
    :cond_5
    if-eqz v7, :cond_6

    .line 1126153
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1126154
    :cond_6
    if-eqz v6, :cond_7

    .line 1126155
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1126156
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_8
    move-wide v10, v6

    move v7, v1

    move v6, v0

    move v1, v2

    move-wide v2, v8

    move-wide v8, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1126157
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1126158
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126159
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 1126160
    const-string v2, "x"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126161
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126162
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126163
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1126164
    const-string v2, "y"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126165
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126166
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126167
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 1126168
    const-string v2, "z"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126169
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126170
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1126171
    return-void
.end method
