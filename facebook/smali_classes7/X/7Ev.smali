.class public final LX/7Ev;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1185507
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1185508
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1185509
    :goto_0
    return v1

    .line 1185510
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1185511
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_6

    .line 1185512
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1185513
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1185514
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1185515
    const-string v5, "aggregated_ranges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1185516
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1185517
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    .line 1185518
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_2

    .line 1185519
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1185520
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_e

    .line 1185521
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1185522
    :goto_3
    move v4, v5

    .line 1185523
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1185524
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1185525
    goto :goto_1

    .line 1185526
    :cond_3
    const-string v5, "ranges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1185527
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1185528
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 1185529
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1185530
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1185531
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_15

    .line 1185532
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1185533
    :goto_5
    move v4, v5

    .line 1185534
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1185535
    :cond_4
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1185536
    goto/16 :goto_1

    .line 1185537
    :cond_5
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1185538
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1185539
    :cond_6
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1185540
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1185541
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1185542
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1185543
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    goto/16 :goto_1

    .line 1185544
    :cond_8
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1185545
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1185546
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1185547
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_8

    if-eqz v10, :cond_8

    .line 1185548
    const-string v11, "length"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1185549
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v9, v7

    move v7, v6

    goto :goto_6

    .line 1185550
    :cond_9
    const-string v11, "offset"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1185551
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v6

    goto :goto_6

    .line 1185552
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1185553
    :cond_b
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1185554
    if-eqz v7, :cond_c

    .line 1185555
    invoke-virtual {p1, v5, v9, v5}, LX/186;->a(III)V

    .line 1185556
    :cond_c
    if-eqz v4, :cond_d

    .line 1185557
    invoke-virtual {p1, v6, v8, v5}, LX/186;->a(III)V

    .line 1185558
    :cond_d
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_e
    move v4, v5

    move v7, v5

    move v8, v5

    move v9, v5

    goto :goto_6

    .line 1185559
    :cond_f
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_12

    .line 1185560
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1185561
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1185562
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_f

    if-eqz v10, :cond_f

    .line 1185563
    const-string v11, "length"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 1185564
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v9, v7

    move v7, v6

    goto :goto_7

    .line 1185565
    :cond_10
    const-string v11, "offset"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 1185566
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v6

    goto :goto_7

    .line 1185567
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_7

    .line 1185568
    :cond_12
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1185569
    if-eqz v7, :cond_13

    .line 1185570
    invoke-virtual {p1, v5, v9, v5}, LX/186;->a(III)V

    .line 1185571
    :cond_13
    if-eqz v4, :cond_14

    .line 1185572
    invoke-virtual {p1, v6, v8, v5}, LX/186;->a(III)V

    .line 1185573
    :cond_14
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_5

    :cond_15
    move v4, v5

    move v7, v5

    move v8, v5

    move v9, v5

    goto :goto_7
.end method
