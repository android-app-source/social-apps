.class public final enum LX/8Pq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Pq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Pq;

.field public static final enum CHOSE_OPTION_FROM_SELECTOR:LX/8Pq;

.field public static final enum OPEN_MORE_OPTIONS:LX/8Pq;

.field public static final enum SET_PRIVACY_TO_FRIENDS:LX/8Pq;

.field public static final enum SET_PRIVACY_TO_ONLY_ME:LX/8Pq;

.field public static final enum SET_PRIVACY_TO_OTHER:LX/8Pq;

.field public static final enum SET_PRIVACY_TO_WIDEST:LX/8Pq;

.field public static final enum SKIPPED_EDUCATOR:LX/8Pq;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1342185
    new-instance v0, LX/8Pq;

    const-string v1, "SET_PRIVACY_TO_WIDEST"

    invoke-direct {v0, v1, v3}, LX/8Pq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pq;->SET_PRIVACY_TO_WIDEST:LX/8Pq;

    .line 1342186
    new-instance v0, LX/8Pq;

    const-string v1, "SET_PRIVACY_TO_FRIENDS"

    invoke-direct {v0, v1, v4}, LX/8Pq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pq;->SET_PRIVACY_TO_FRIENDS:LX/8Pq;

    .line 1342187
    new-instance v0, LX/8Pq;

    const-string v1, "SET_PRIVACY_TO_ONLY_ME"

    invoke-direct {v0, v1, v5}, LX/8Pq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pq;->SET_PRIVACY_TO_ONLY_ME:LX/8Pq;

    .line 1342188
    new-instance v0, LX/8Pq;

    const-string v1, "SET_PRIVACY_TO_OTHER"

    invoke-direct {v0, v1, v6}, LX/8Pq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pq;->SET_PRIVACY_TO_OTHER:LX/8Pq;

    .line 1342189
    new-instance v0, LX/8Pq;

    const-string v1, "OPEN_MORE_OPTIONS"

    invoke-direct {v0, v1, v7}, LX/8Pq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pq;->OPEN_MORE_OPTIONS:LX/8Pq;

    .line 1342190
    new-instance v0, LX/8Pq;

    const-string v1, "SKIPPED_EDUCATOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8Pq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pq;->SKIPPED_EDUCATOR:LX/8Pq;

    .line 1342191
    new-instance v0, LX/8Pq;

    const-string v1, "CHOSE_OPTION_FROM_SELECTOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8Pq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pq;->CHOSE_OPTION_FROM_SELECTOR:LX/8Pq;

    .line 1342192
    const/4 v0, 0x7

    new-array v0, v0, [LX/8Pq;

    sget-object v1, LX/8Pq;->SET_PRIVACY_TO_WIDEST:LX/8Pq;

    aput-object v1, v0, v3

    sget-object v1, LX/8Pq;->SET_PRIVACY_TO_FRIENDS:LX/8Pq;

    aput-object v1, v0, v4

    sget-object v1, LX/8Pq;->SET_PRIVACY_TO_ONLY_ME:LX/8Pq;

    aput-object v1, v0, v5

    sget-object v1, LX/8Pq;->SET_PRIVACY_TO_OTHER:LX/8Pq;

    aput-object v1, v0, v6

    sget-object v1, LX/8Pq;->OPEN_MORE_OPTIONS:LX/8Pq;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8Pq;->SKIPPED_EDUCATOR:LX/8Pq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8Pq;->CHOSE_OPTION_FROM_SELECTOR:LX/8Pq;

    aput-object v2, v0, v1

    sput-object v0, LX/8Pq;->$VALUES:[LX/8Pq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1342193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Pq;
    .locals 1

    .prologue
    .line 1342194
    const-class v0, LX/8Pq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Pq;

    return-object v0
.end method

.method public static values()[LX/8Pq;
    .locals 1

    .prologue
    .line 1342195
    sget-object v0, LX/8Pq;->$VALUES:[LX/8Pq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Pq;

    return-object v0
.end method
