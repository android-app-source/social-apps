.class public LX/8Yg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8Yf;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final c:LX/8Yc;

.field private final d:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

.field public final e:LX/8YX;

.field public final f:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "LX/8YY;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/8bK;

.field public final h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1356635
    new-instance v0, LX/8Ye;

    invoke-direct {v0}, LX/8Ye;-><init>()V

    sput-object v0, LX/8Yg;->a:Ljava/util/Map;

    .line 1356636
    const-class v0, LX/8Yg;

    sput-object v0, LX/8Yg;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/8Yc;Lcom/facebook/richdocument/fonts/FetchFontExecutor;LX/8YX;LX/8bK;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1356482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356483
    iput-object p1, p0, LX/8Yg;->c:LX/8Yc;

    .line 1356484
    iput-object p2, p0, LX/8Yg;->d:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    .line 1356485
    iput-object p3, p0, LX/8Yg;->e:LX/8YX;

    .line 1356486
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/8Yg;->f:Landroid/util/LruCache;

    .line 1356487
    iput-object p4, p0, LX/8Yg;->g:LX/8bK;

    .line 1356488
    iput-object p5, p0, LX/8Yg;->h:LX/03V;

    .line 1356489
    return-void
.end method

.method private static a(LX/8GE;)LX/8Yb;
    .locals 3

    .prologue
    .line 1356634
    new-instance v0, LX/8Yb;

    invoke-interface {p0}, LX/8GE;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, LX/8GE;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8Yb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/8Yg;
    .locals 9

    .prologue
    .line 1356623
    const-class v1, LX/8Yg;

    monitor-enter v1

    .line 1356624
    :try_start_0
    sget-object v0, LX/8Yg;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1356625
    sput-object v2, LX/8Yg;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1356626
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1356627
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1356628
    new-instance v3, LX/8Yg;

    invoke-static {v0}, LX/8Yc;->a(LX/0QB;)LX/8Yc;

    move-result-object v4

    check-cast v4, LX/8Yc;

    invoke-static {v0}, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->a(LX/0QB;)Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    move-result-object v5

    check-cast v5, Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    invoke-static {v0}, LX/8YX;->a(LX/0QB;)LX/8YX;

    move-result-object v6

    check-cast v6, LX/8YX;

    invoke-static {v0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v7

    check-cast v7, LX/8bK;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/8Yg;-><init>(LX/8Yc;Lcom/facebook/richdocument/fonts/FetchFontExecutor;LX/8YX;LX/8bK;LX/03V;)V

    .line 1356629
    move-object v0, v3

    .line 1356630
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1356631
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Yg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1356632
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1356633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1356605
    if-nez p0, :cond_1

    .line 1356606
    :cond_0
    :goto_0
    return-object v0

    .line 1356607
    :cond_1
    sget-object v1, LX/8Yg;->a:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1356608
    sget-object v0, LX/8Yg;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Yf;

    .line 1356609
    iget-object v1, v0, LX/8Yf;->a:Ljava/lang/String;

    iget v2, v0, LX/8Yf;->b:I

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    move-object v0, v1

    .line 1356610
    goto :goto_0

    .line 1356611
    :cond_2
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 1356612
    const-string v1, "HelveticaNeue"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1356613
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_0

    .line 1356614
    :sswitch_0
    const-string v4, "HelveticaNeue"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v4, "PRE_INSTALLED_FONT_SANS_SERIF"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    goto :goto_1

    :sswitch_2
    const-string v4, "HelveticaNeue-Bold"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v3

    goto :goto_1

    :sswitch_3
    const-string v4, "PRE_INSTALLED_FONT_SANS_SERIF_BOLD"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_4
    const-string v4, "HelveticaNeue-Italic"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_5
    const-string v4, "PRE_INSTALLED_FONT_SANS_SERIF_ITALIC"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_6
    const-string v4, "Georgia"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_7
    const-string v4, "PRE_INSTALLED_FONT_SERIF"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x7

    goto :goto_1

    :sswitch_8
    const-string v4, "Georgia-Bold"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v1, 0x8

    goto :goto_1

    :sswitch_9
    const-string v4, "PRE_INSTALLED_FONT_SERIF_BOLD"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v1, 0x9

    goto :goto_1

    :sswitch_a
    const-string v4, "Georgia-Italic"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v1, 0xa

    goto :goto_1

    :sswitch_b
    const-string v4, "PRE_INSTALLED_FONT_SERIF_ITALIC"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v1, 0xb

    goto/16 :goto_1

    .line 1356615
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto/16 :goto_0

    .line 1356616
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto/16 :goto_0

    .line 1356617
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto/16 :goto_0

    .line 1356618
    :pswitch_3
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto/16 :goto_0

    .line 1356619
    :pswitch_4
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto/16 :goto_0

    .line 1356620
    :pswitch_5
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto/16 :goto_0

    .line 1356621
    :cond_4
    const-string v1, "Georgia"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1356622
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6950141f -> :sswitch_b
        -0x586299c1 -> :sswitch_a
        -0x46fb578a -> :sswitch_9
        -0x707e1ac -> :sswitch_8
        0x36dff00 -> :sswitch_1
        0x26654c4f -> :sswitch_5
        0x29d3dfae -> :sswitch_7
        0x3196f4ba -> :sswitch_2
        0x33e03b25 -> :sswitch_4
        0x41d34c64 -> :sswitch_3
        0x5e8578be -> :sswitch_6
        0x7a27ba98 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static a(LX/8Yg;LX/0Px;Ljava/util/Set;ZZ)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/8GE;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1356574
    invoke-direct {p0, p1, p2, p3}, LX/8Yg;->c(LX/0Px;Ljava/util/Set;Z)Ljava/util/Set;

    move-result-object v0

    .line 1356575
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1356576
    iget-object v2, p0, LX/8Yg;->g:LX/8bK;

    invoke-virtual {v2}, LX/8bK;->a()LX/0p3;

    move-result-object v2

    .line 1356577
    sget-object v3, LX/0p3;->MODERATE:LX/0p3;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/0p3;->POOR:LX/0p3;

    if-ne v2, v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 1356578
    :goto_0
    return-object v0

    .line 1356579
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GE;

    .line 1356580
    invoke-direct {p0, v0}, LX/8Yg;->b(LX/8GE;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1356581
    goto :goto_0

    .line 1356582
    :cond_3
    if-eqz p4, :cond_6

    .line 1356583
    iget-object v0, p0, LX/8Yg;->e:LX/8YX;

    invoke-direct {p0, p1, p2, p3}, LX/8Yg;->b(LX/0Px;Ljava/util/Set;Z)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8YX;->a(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0

    .line 1356584
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1356585
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1356586
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8YY;

    .line 1356587
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1356588
    const/4 v3, 0x0

    .line 1356589
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 1356590
    iget-object v3, p0, LX/8Yg;->f:Landroid/util/LruCache;

    invoke-virtual {v3, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Typeface;

    .line 1356591
    if-nez v3, :cond_4

    .line 1356592
    :try_start_0
    new-instance p1, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 1356593
    iget-object p1, p0, LX/8Yg;->f:Landroid/util/LruCache;

    invoke-virtual {p1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356594
    :cond_4
    :goto_3
    move-object v1, v3

    .line 1356595
    :goto_4
    iget-object v3, v2, LX/8YY;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1356596
    invoke-interface {v4, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1356597
    :cond_5
    move-object v0, v4

    .line 1356598
    goto :goto_0

    :cond_6
    move-object v0, v1

    .line 1356599
    goto :goto_0

    :cond_7
    move-object v1, v3

    goto :goto_4

    .line 1356600
    :catch_0
    move-exception p1

    move-object p2, p1

    move-object p1, v3

    .line 1356601
    iget-object v3, p0, LX/8Yg;->e:LX/8YX;

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p3

    invoke-virtual {v3, p3}, LX/8YX;->a(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v3

    .line 1356602
    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "fontCacheKey: "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p3

    const-string p4, ", found: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    :goto_5
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1356603
    iget-object p3, p0, LX/8Yg;->h:LX/03V;

    sget-object p4, LX/8Yg;->b:Ljava/lang/Class;

    invoke-virtual {p4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4, v3, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, p1

    goto :goto_3

    .line 1356604
    :cond_8
    const/4 v3, 0x0

    goto :goto_5
.end method

.method public static a(Ljava/util/Set;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356567
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1356568
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1356569
    if-eqz p1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1356570
    :cond_0
    invoke-static {v0}, LX/8Yg;->a(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 1356571
    if-nez v1, :cond_1

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :cond_1
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1356572
    :cond_2
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1356573
    :cond_3
    return-object v2
.end method

.method private b(LX/8GE;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8GE;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/8Ya;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356552
    invoke-interface {p1}, LX/8GE;->a()Ljava/lang/String;

    move-result-object v0

    .line 1356553
    invoke-interface {p1}, LX/8GE;->b()Ljava/lang/String;

    move-result-object v1

    .line 1356554
    invoke-interface {p1}, LX/8GE;->c()Ljava/lang/String;

    move-result-object v2

    .line 1356555
    new-instance v3, LX/8YU;

    invoke-direct {v3, v0, v1, v2}, LX/8YU;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1356556
    iget-object v0, p0, LX/8Yg;->d:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    .line 1356557
    new-instance v2, LX/8Yb;

    .line 1356558
    iget-object v1, v3, LX/8YU;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1356559
    iget-object p0, v3, LX/8YU;->b:Ljava/lang/String;

    move-object p0, p0

    .line 1356560
    invoke-direct {v2, v1, p0}, LX/8Yb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356561
    sget-object v1, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1356562
    const/4 v1, 0x0

    .line 1356563
    :goto_0
    move-object v0, v1

    .line 1356564
    return-object v0

    .line 1356565
    :cond_0
    sget-object v1, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->b:Ljava/util/Map;

    const/4 p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356566
    iget-object v1, v0, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0TD;

    new-instance p0, LX/8YS;

    invoke-direct {p0, v0, v3, v2}, LX/8YS;-><init>(Lcom/facebook/richdocument/fonts/FetchFontExecutor;LX/8YU;LX/8Yb;)V

    invoke-interface {v1, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method

.method private b(LX/0Px;Ljava/util/Set;Z)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/8GE;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/util/Set",
            "<",
            "LX/8YY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356537
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1356538
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GE;

    .line 1356539
    invoke-static {v0}, LX/8Yg;->a(LX/8GE;)LX/8Yb;

    move-result-object v0

    .line 1356540
    iget-object v4, p0, LX/8Yg;->c:LX/8Yc;

    invoke-virtual {v4, v0}, LX/8Yc;->a(LX/8Yb;)LX/8Ya;

    move-result-object v0

    .line 1356541
    if-eqz v0, :cond_0

    .line 1356542
    invoke-virtual {v0}, LX/8Ya;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1356543
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1356544
    :cond_1
    if-nez p3, :cond_4

    .line 1356545
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1356546
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YY;

    .line 1356547
    iget-object v4, v0, LX/8YY;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1356548
    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1356549
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1356550
    :cond_3
    move-object v0, v2

    .line 1356551
    :goto_2
    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method private c(LX/0Px;Ljava/util/Set;Z)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/8GE;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/util/Set",
            "<",
            "LX/8GE;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1356515
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1356516
    invoke-direct {p0, p1, p2, p3}, LX/8Yg;->b(LX/0Px;Ljava/util/Set;Z)Ljava/util/Set;

    move-result-object v4

    .line 1356517
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GE;

    .line 1356518
    invoke-static {v0}, LX/8Yg;->a(LX/8GE;)LX/8Yb;

    move-result-object v6

    .line 1356519
    iget-object v7, p0, LX/8Yg;->c:LX/8Yc;

    invoke-virtual {v7, v6}, LX/8Yc;->a(LX/8Yb;)LX/8Ya;

    move-result-object v6

    .line 1356520
    if-nez v6, :cond_0

    .line 1356521
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1356522
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1356523
    :cond_1
    iget-object v0, p0, LX/8Yg;->e:LX/8YX;

    invoke-virtual {v0, v4}, LX/8YX;->a(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0

    .line 1356524
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1356525
    invoke-static {v4, v2}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    .line 1356526
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1356527
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1356528
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YY;

    .line 1356529
    new-instance v5, LX/8Yb;

    iget-object v6, v0, LX/8YY;->a:Ljava/lang/String;

    iget-object v7, v0, LX/8YY;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, LX/8Yb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    .line 1356530
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1356531
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GE;

    .line 1356532
    invoke-static {v0}, LX/8Yg;->a(LX/8GE;)LX/8Yb;

    move-result-object v5

    .line 1356533
    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1356534
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1356535
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1356536
    :cond_4
    return-object v3
.end method


# virtual methods
.method public final a(LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/8GE;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/8Ya;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1356504
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1356505
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GE;

    .line 1356506
    invoke-interface {v0}, LX/8GE;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1356507
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1356508
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v2, v0}, LX/8Yg;->c(LX/0Px;Ljava/util/Set;Z)Ljava/util/Set;

    move-result-object v0

    .line 1356509
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1356510
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GE;

    .line 1356511
    invoke-direct {p0, v0}, LX/8Yg;->b(LX/8GE;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1356512
    if-eqz v0, :cond_1

    .line 1356513
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1356514
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;Ljava/util/Set;Z)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/8GE;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356503
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, LX/8Yg;->a(LX/8Yg;LX/0Px;Ljava/util/Set;ZZ)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)Ljava/util/Map;
    .locals 3
    .param p1    # Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentStyle;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356490
    new-instance v0, LX/8Yk;

    invoke-direct {v0, p1}, LX/8Yk;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 1356491
    invoke-virtual {v0}, LX/8Yk;->a()Ljava/util/Set;

    move-result-object v0

    .line 1356492
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->p()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1356493
    :cond_0
    const/4 v1, 0x0

    .line 1356494
    :goto_0
    move-object v1, v1

    .line 1356495
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1356496
    if-eqz v1, :cond_2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1356497
    :cond_2
    invoke-static {v2}, LX/8Yg;->a(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1356498
    const/4 v2, 0x0

    .line 1356499
    :goto_1
    move v2, v2

    .line 1356500
    if-eqz v2, :cond_3

    .line 1356501
    invoke-static {v0, v1}, LX/8Yg;->a(Ljava/util/Set;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 1356502
    :goto_2
    return-object v0

    :cond_3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->p()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, LX/8Yg;->a(LX/0Px;Ljava/util/Set;Z)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    :cond_5
    const/4 v2, 0x1

    goto :goto_1
.end method
