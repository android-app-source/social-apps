.class public final LX/7sr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1266007
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1266008
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1266009
    if-eqz v0, :cond_0

    .line 1266010
    const-string v1, "available_inventory"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266011
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1266012
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266013
    if-eqz v0, :cond_1

    .line 1266014
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266015
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266016
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266017
    if-eqz v0, :cond_2

    .line 1266018
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266019
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266020
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266021
    if-eqz v0, :cond_3

    .line 1266022
    const-string v1, "partner_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266023
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266024
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1266025
    cmp-long v2, v0, v2

    if-eqz v2, :cond_4

    .line 1266026
    const-string v2, "start_sales_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266027
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1266028
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1266029
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 1266030
    const/4 v7, 0x0

    .line 1266031
    const/4 v6, 0x0

    .line 1266032
    const/4 v5, 0x0

    .line 1266033
    const/4 v4, 0x0

    .line 1266034
    const-wide/16 v2, 0x0

    .line 1266035
    const/4 v1, 0x0

    .line 1266036
    const/4 v0, 0x0

    .line 1266037
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_1

    .line 1266038
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266039
    const/4 v0, 0x0

    .line 1266040
    :goto_0
    return v0

    .line 1266041
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266042
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1266043
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1266044
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1266045
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1266046
    const-string v9, "available_inventory"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1266047
    const/4 v1, 0x1

    .line 1266048
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 1266049
    :cond_2
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1266050
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1266051
    :cond_3
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1266052
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1266053
    :cond_4
    const-string v9, "partner_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1266054
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1266055
    :cond_5
    const-string v9, "start_sales_time"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1266056
    const/4 v0, 0x1

    .line 1266057
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 1266058
    :cond_6
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1266059
    if-eqz v1, :cond_7

    .line 1266060
    const/4 v1, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1, v1, v7, v8}, LX/186;->a(III)V

    .line 1266061
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1266062
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1266063
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1266064
    if-eqz v0, :cond_8

    .line 1266065
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1266066
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
