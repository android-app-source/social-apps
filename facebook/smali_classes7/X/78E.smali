.class public LX/78E;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/78E;


# instance fields
.field public final a:LX/17T;


# direct methods
.method public constructor <init>(LX/17T;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1172516
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1172517
    iput-object p1, p0, LX/78E;->a:LX/17T;

    .line 1172518
    new-instance v0, LX/78D;

    invoke-direct {v0, p0}, LX/78D;-><init>(LX/78E;)V

    .line 1172519
    sget-object v1, LX/0ax;->eu:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "{action}"

    aput-object v3, v2, v4

    const-string v3, "{data}"

    aput-object v3, v2, v5

    const-string v3, "{fallback_url}"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1172520
    sget-object v1, LX/0ax;->et:Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "{action}"

    aput-object v3, v2, v4

    const-string v3, "{fallback_url}"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1172521
    return-void
.end method

.method public static a(LX/0QB;)LX/78E;
    .locals 4

    .prologue
    .line 1172503
    sget-object v0, LX/78E;->b:LX/78E;

    if-nez v0, :cond_1

    .line 1172504
    const-class v1, LX/78E;

    monitor-enter v1

    .line 1172505
    :try_start_0
    sget-object v0, LX/78E;->b:LX/78E;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1172506
    if-eqz v2, :cond_0

    .line 1172507
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1172508
    new-instance p0, LX/78E;

    invoke-static {v0}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v3

    check-cast v3, LX/17T;

    invoke-direct {p0, v3}, LX/78E;-><init>(LX/17T;)V

    .line 1172509
    move-object v0, p0

    .line 1172510
    sput-object v0, LX/78E;->b:LX/78E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1172511
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1172512
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1172513
    :cond_1
    sget-object v0, LX/78E;->b:LX/78E;

    return-object v0

    .line 1172514
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1172515
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
