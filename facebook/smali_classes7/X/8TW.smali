.class public LX/8TW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1348724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348725
    iput-object p1, p0, LX/8TW;->a:Ljava/util/List;

    .line 1348726
    return-void
.end method


# virtual methods
.method public final a(J)LX/8Vb;
    .locals 9

    .prologue
    .line 1348727
    iget-object v0, p0, LX/8TW;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8TW;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1348728
    :cond_0
    const/4 v1, 0x0

    .line 1348729
    :cond_1
    return-object v1

    .line 1348730
    :cond_2
    iget-object v0, p0, LX/8TW;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    .line 1348731
    iget-object v1, p0, LX/8TW;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    .line 1348732
    if-eqz v0, :cond_3

    .line 1348733
    iget-wide v4, v1, LX/8Vb;->l:J

    cmp-long v3, v4, p1

    if-gez v3, :cond_4

    iget-wide v4, v0, LX/8Vb;->l:J

    iget-wide v6, v1, LX/8Vb;->l:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    move-object v1, v0

    .line 1348734
    goto :goto_0

    .line 1348735
    :cond_4
    iget-wide v4, v1, LX/8Vb;->l:J

    cmp-long v3, v4, p1

    if-ltz v3, :cond_5

    iget-wide v4, v0, LX/8Vb;->l:J

    iget-wide v6, v1, LX/8Vb;->l:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_5

    iget-wide v4, v0, LX/8Vb;->l:J

    cmp-long v3, v4, p1

    if-lez v3, :cond_5

    :goto_1
    move-object v1, v0

    .line 1348736
    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method
