.class public LX/7Ge;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7Ge;


# instance fields
.field private final a:Ljava/util/Random;


# direct methods
.method public constructor <init>(Ljava/util/Random;)V
    .locals 0
    .param p1    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189657
    iput-object p1, p0, LX/7Ge;->a:Ljava/util/Random;

    .line 1189658
    return-void
.end method

.method public static a(LX/0QB;)LX/7Ge;
    .locals 4

    .prologue
    .line 1189659
    sget-object v0, LX/7Ge;->b:LX/7Ge;

    if-nez v0, :cond_1

    .line 1189660
    const-class v1, LX/7Ge;

    monitor-enter v1

    .line 1189661
    :try_start_0
    sget-object v0, LX/7Ge;->b:LX/7Ge;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1189662
    if-eqz v2, :cond_0

    .line 1189663
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1189664
    new-instance p0, LX/7Ge;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v3

    check-cast v3, Ljava/util/Random;

    invoke-direct {p0, v3}, LX/7Ge;-><init>(Ljava/util/Random;)V

    .line 1189665
    move-object v0, p0

    .line 1189666
    sput-object v0, LX/7Ge;->b:LX/7Ge;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189667
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1189668
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189669
    :cond_1
    sget-object v0, LX/7Ge;->b:LX/7Ge;

    return-object v0

    .line 1189670
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1189671
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JJLX/7G5;)LX/7Gd;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RETURN_TYPE:",
            "Ljava/lang/Object;",
            ">(JJ",
            "LX/7G5",
            "<TRETURN_TYPE;>;)",
            "LX/7Gd",
            "<TRETURN_TYPE;>;"
        }
    .end annotation

    .prologue
    .line 1189672
    new-instance v1, LX/7Gd;

    iget-object v7, p0, LX/7Ge;->a:Ljava/util/Random;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, LX/7Gd;-><init>(JJLX/7G5;Ljava/util/Random;)V

    return-object v1
.end method
