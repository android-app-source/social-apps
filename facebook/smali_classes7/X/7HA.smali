.class public LX/7HA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7HA;


# instance fields
.field private final a:LX/1zD;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1zD;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1zD;",
            "LX/0Ot",
            "<",
            "LX/1zU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1190446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190447
    iput-object p1, p0, LX/7HA;->a:LX/1zD;

    .line 1190448
    iput-object p2, p0, LX/7HA;->b:LX/0Ot;

    .line 1190449
    return-void
.end method

.method public static a(LX/0QB;)LX/7HA;
    .locals 5

    .prologue
    .line 1190475
    sget-object v0, LX/7HA;->c:LX/7HA;

    if-nez v0, :cond_1

    .line 1190476
    const-class v1, LX/7HA;

    monitor-enter v1

    .line 1190477
    :try_start_0
    sget-object v0, LX/7HA;->c:LX/7HA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1190478
    if-eqz v2, :cond_0

    .line 1190479
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1190480
    new-instance v4, LX/7HA;

    invoke-static {v0}, LX/1zD;->a(LX/0QB;)LX/1zD;

    move-result-object v3

    check-cast v3, LX/1zD;

    const/16 p0, 0x12ad

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/7HA;-><init>(LX/1zD;LX/0Ot;)V

    .line 1190481
    move-object v0, v4

    .line 1190482
    sput-object v0, LX/7HA;->c:LX/7HA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1190483
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1190484
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1190485
    :cond_1
    sget-object v0, LX/7HA;->c:LX/7HA;

    return-object v0

    .line 1190486
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1190487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;I)Lcom/facebook/ui/emoji/model/Emoji;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1190450
    invoke-static {p1, p2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 1190451
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    .line 1190452
    const/4 v0, 0x0

    .line 1190453
    add-int v4, p2, v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 1190454
    add-int v0, p2, v1

    invoke-static {p1, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    move v1, v0

    .line 1190455
    :goto_0
    invoke-static {v3, v1}, LX/1zD;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1190456
    :goto_1
    return-object v2

    .line 1190457
    :cond_0
    const/16 v0, 0x200d

    if-eq v1, v0, :cond_4

    .line 1190458
    const/4 v0, 0x0

    .line 1190459
    :goto_2
    move-object v4, v0

    .line 1190460
    if-nez v4, :cond_1

    .line 1190461
    iget-object v0, p0, LX/7HA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zU;

    invoke-virtual {v0, v3, v1}, LX/1zU;->a(II)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 1190462
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1190463
    iget-object v0, p0, LX/7HA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zU;

    invoke-virtual {v0, v3, v1, v4}, LX/1zU;->a(IILjava/util/List;)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v0

    .line 1190464
    if-nez v0, :cond_2

    .line 1190465
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    :cond_2
    move-object v2, v0

    .line 1190466
    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0

    .line 1190467
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1190468
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, p2

    .line 1190469
    :goto_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-ge v0, v5, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x6

    if-ge v5, v6, :cond_5

    .line 1190470
    invoke-static {p1, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v5

    .line 1190471
    invoke-static {v5}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    .line 1190472
    add-int/2addr v0, v6

    .line 1190473
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_5
    move-object v0, v4

    .line 1190474
    goto :goto_2
.end method
