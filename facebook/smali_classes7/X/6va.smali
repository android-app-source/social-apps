.class public final enum LX/6va;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6vZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6va;",
        ">;",
        "LX/6vZ;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6va;

.field public static final enum CONTACT_EMAIL:LX/6va;

.field public static final enum CONTACT_PHONE_NUMBER:LX/6va;

.field public static final enum DOUBLE_ROW_DIVIDER:LX/6va;

.field public static final enum SINGLE_ROW_DIVIDER:LX/6va;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1157043
    new-instance v0, LX/6va;

    const-string v1, "CONTACT_EMAIL"

    invoke-direct {v0, v1, v2}, LX/6va;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6va;->CONTACT_EMAIL:LX/6va;

    .line 1157044
    new-instance v0, LX/6va;

    const-string v1, "CONTACT_PHONE_NUMBER"

    invoke-direct {v0, v1, v3}, LX/6va;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6va;->CONTACT_PHONE_NUMBER:LX/6va;

    .line 1157045
    new-instance v0, LX/6va;

    const-string v1, "DOUBLE_ROW_DIVIDER"

    invoke-direct {v0, v1, v4}, LX/6va;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6va;->DOUBLE_ROW_DIVIDER:LX/6va;

    .line 1157046
    new-instance v0, LX/6va;

    const-string v1, "SINGLE_ROW_DIVIDER"

    invoke-direct {v0, v1, v5}, LX/6va;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6va;->SINGLE_ROW_DIVIDER:LX/6va;

    .line 1157047
    const/4 v0, 0x4

    new-array v0, v0, [LX/6va;

    sget-object v1, LX/6va;->CONTACT_EMAIL:LX/6va;

    aput-object v1, v0, v2

    sget-object v1, LX/6va;->CONTACT_PHONE_NUMBER:LX/6va;

    aput-object v1, v0, v3

    sget-object v1, LX/6va;->DOUBLE_ROW_DIVIDER:LX/6va;

    aput-object v1, v0, v4

    sget-object v1, LX/6va;->SINGLE_ROW_DIVIDER:LX/6va;

    aput-object v1, v0, v5

    sput-object v0, LX/6va;->$VALUES:[LX/6va;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1157048
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6va;
    .locals 1

    .prologue
    .line 1157049
    const-class v0, LX/6va;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6va;

    return-object v0
.end method

.method public static values()[LX/6va;
    .locals 1

    .prologue
    .line 1157050
    sget-object v0, LX/6va;->$VALUES:[LX/6va;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6va;

    return-object v0
.end method
