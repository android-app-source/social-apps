.class public final LX/7vd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

.field public final synthetic b:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/7vi;


# direct methods
.method public constructor <init>(LX/7vi;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1274870
    iput-object p1, p0, LX/7vd;->f:LX/7vi;

    iput-object p2, p0, LX/7vd;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iput-object p3, p0, LX/7vd;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object p4, p0, LX/7vd;->c:Ljava/lang/String;

    iput-object p5, p0, LX/7vd;->d:Ljava/lang/String;

    iput-object p6, p0, LX/7vd;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1274871
    iget-object v0, p0, LX/7vd;->f:LX/7vi;

    iget-object v0, v0, LX/7vi;->g:LX/7xQ;

    iget-object v1, p0, LX/7vd;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v0, v1}, LX/7xQ;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)LX/7xP;

    move-result-object v0

    .line 1274872
    new-instance v1, LX/4ER;

    invoke-direct {v1}, LX/4ER;-><init>()V

    iget-object v2, p0, LX/7vd;->f:LX/7vi;

    iget-object v3, p0, LX/7vd;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1274873
    new-instance v4, LX/4EG;

    invoke-direct {v4}, LX/4EG;-><init>()V

    .line 1274874
    iget-object v5, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274875
    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->PERMALINK:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v5}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1274876
    iget-object v5, v2, LX/7vi;->c:LX/0kv;

    iget-object v6, v2, LX/7vi;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1274877
    if-eqz v5, :cond_0

    .line 1274878
    new-instance v6, Lorg/json/JSONObject;

    const-string v7, "impression_id"

    invoke-static {v7, v5}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v5

    invoke-direct {v6, v5}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 1274879
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1274880
    :cond_0
    iget-object v5, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    if-nez v5, :cond_8

    .line 1274881
    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1274882
    :goto_0
    new-instance v5, LX/4EL;

    invoke-direct {v5}, LX/4EL;-><init>()V

    invoke-virtual {v5, v4}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object v4

    move-object v2, v4

    .line 1274883
    const-string v3, "context"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1274884
    move-object v1, v1

    .line 1274885
    iget-object v2, p0, LX/7vd;->c:Ljava/lang/String;

    .line 1274886
    const-string v3, "customer_email"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274887
    move-object v1, v1

    .line 1274888
    iget-object v2, p0, LX/7vd;->d:Ljava/lang/String;

    .line 1274889
    const-string v3, "customer_name"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274890
    move-object v1, v1

    .line 1274891
    new-instance v2, LX/4Eb;

    invoke-direct {v2}, LX/4Eb;-><init>()V

    iget-object v0, v0, LX/7xP;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Lcom/facebook/payments/currency/CurrencyAmount;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1274892
    const-string v3, "amount_in_hundredths"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1274893
    move-object v0, v2

    .line 1274894
    iget-object v2, p0, LX/7vd;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    .line 1274895
    const-string v3, "currency"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274896
    move-object v0, v0

    .line 1274897
    const-string v2, "amount"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1274898
    move-object v0, v1

    .line 1274899
    iget-object v1, p0, LX/7vd;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1274900
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1274901
    iget-object v6, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_3

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1274902
    new-instance v3, LX/4EZ;

    invoke-direct {v3}, LX/4EZ;-><init>()V

    iget-object v8, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    .line 1274903
    const-string v9, "ticket_tier_id"

    invoke-virtual {v3, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274904
    move-object v3, v3

    .line 1274905
    iget v8, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1274906
    const-string v9, "quantity"

    invoke-virtual {v3, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1274907
    move-object v8, v3

    .line 1274908
    iget-object v3, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_TICKET:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    if-ne v3, v9, :cond_2

    .line 1274909
    iget-object v3, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->x:Ljava/util/Map;

    iget-object v9, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    invoke-interface {v3, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1274910
    iget v2, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    iget-object v9, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    .line 1274911
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    move v11, v3

    .line 1274912
    :goto_2
    add-int v10, v3, v2

    if-ge v11, v10, :cond_1

    .line 1274913
    invoke-virtual {v9, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    invoke-virtual {v10}, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->b()Lorg/json/JSONObject;

    move-result-object v10

    .line 1274914
    invoke-virtual {v10}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1274915
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_2

    .line 1274916
    :cond_1
    move-object v2, v12

    .line 1274917
    const-string v3, "ticket_registration_data"

    invoke-virtual {v8, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1274918
    :cond_2
    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1274919
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 1274920
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1274921
    const-string v2, "tiers"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1274922
    move-object v0, v0

    .line 1274923
    iget-object v1, p0, LX/7vd;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_ORDER:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    if-ne v1, v2, :cond_5

    .line 1274924
    iget-object v1, p0, LX/7vd;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    .line 1274925
    if-eqz v1, :cond_4

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1274926
    :cond_4
    const/4 v2, 0x0

    .line 1274927
    :goto_3
    move-object v1, v2

    .line 1274928
    const-string v2, "order_registration_data"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274929
    :cond_5
    iget-object v1, p0, LX/7vd;->e:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1274930
    iget-object v1, p0, LX/7vd;->e:Ljava/lang/String;

    .line 1274931
    const-string v2, "credential_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274932
    :cond_6
    iget-object v1, p0, LX/7vd;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1274933
    iget-object v1, p0, LX/7vd;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1274934
    const-string v2, "tracking"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1274935
    :cond_7
    new-instance v1, LX/7uH;

    invoke-direct {v1}, LX/7uH;-><init>()V

    move-object v1, v1

    .line 1274936
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1274937
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1274938
    iget-object v1, p0, LX/7vd;->f:LX/7vi;

    iget-object v1, v1, LX/7vi;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1274939
    :cond_8
    new-instance v5, LX/4EG;

    invoke-direct {v5}, LX/4EG;-><init>()V

    .line 1274940
    iget-object v6, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274941
    invoke-static {v5, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    goto/16 :goto_0

    :cond_9
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    invoke-virtual {v2}, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->b()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method
