.class public LX/7Cg;
.super LX/7Ce;
.source ""


# instance fields
.field public final a:LX/7Cp;

.field public final b:LX/7Cp;

.field private final c:LX/7Cp;

.field private final d:LX/7Cp;

.field private final e:LX/7Cp;

.field private final f:LX/7Cp;

.field private final g:LX/7Cp;

.field private final t:LX/7Cp;

.field private u:F

.field private v:F

.field private w:F

.field private x:F


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1181427
    invoke-direct {p0, p1}, LX/7Ce;-><init>(Landroid/view/WindowManager;)V

    .line 1181428
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->a:LX/7Cp;

    .line 1181429
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->c:LX/7Cp;

    .line 1181430
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->d:LX/7Cp;

    .line 1181431
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->b:LX/7Cp;

    .line 1181432
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->e:LX/7Cp;

    .line 1181433
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->f:LX/7Cp;

    .line 1181434
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->g:LX/7Cp;

    .line 1181435
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cg;->t:LX/7Cp;

    .line 1181436
    iput v1, p0, LX/7Cg;->u:F

    .line 1181437
    iput v1, p0, LX/7Cg;->v:F

    .line 1181438
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 3

    .prologue
    .line 1181424
    iget-object v0, p0, LX/7Cg;->c:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->e:LX/7Cp;

    iget-object v2, p0, LX/7Cg;->f:LX/7Cp;

    invoke-virtual {v0, v1, v2, p1}, LX/7Cp;->a(LX/7Cp;LX/7Cp;F)V

    .line 1181425
    iget-object v0, p0, LX/7Cg;->d:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->g:LX/7Cp;

    iget-object v2, p0, LX/7Cg;->t:LX/7Cp;

    invoke-virtual {v0, v1, v2, p1}, LX/7Cp;->a(LX/7Cp;LX/7Cp;F)V

    .line 1181426
    return-void
.end method

.method public final a(FF)V
    .locals 0

    .prologue
    .line 1181421
    invoke-super {p0, p1, p2}, LX/7Ce;->a(FF)V

    .line 1181422
    invoke-virtual {p0, p1, p2}, LX/7Cg;->f(FF)V

    .line 1181423
    return-void
.end method

.method public final a(Landroid/hardware/SensorEvent;)V
    .locals 2

    .prologue
    .line 1181416
    iget-object v0, p0, LX/7Ce;->i:[F

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getQuaternionFromVector([F[F)V

    .line 1181417
    iget-object v0, p0, LX/7Cg;->b:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    invoke-virtual {v0, v1}, LX/7Cp;->a([F)V

    .line 1181418
    iget-object v0, p0, LX/7Cg;->b:LX/7Cp;

    invoke-virtual {v0}, LX/7Cp;->b()V

    .line 1181419
    invoke-virtual {p0}, LX/7Ce;->d()V

    .line 1181420
    return-void
.end method

.method public a([FLX/3IO;)V
    .locals 2

    .prologue
    .line 1181403
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->d:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181404
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->a:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181405
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->b:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181406
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->j:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181407
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->c:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181408
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    invoke-virtual {v0, v1}, LX/7Cp;->b([F)V

    .line 1181409
    iget-object v0, p2, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181410
    iget-object v0, p0, LX/7Ce;->i:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    iget v1, p0, LX/7Ce;->p:F

    sub-float/2addr v0, v1

    iput v0, p2, LX/3IO;->e:F

    .line 1181411
    iget-object v0, p0, LX/7Ce;->i:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    neg-float v0, v0

    iput v0, p2, LX/3IO;->d:F

    .line 1181412
    iget-object v0, p0, LX/7Ce;->i:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p2, LX/3IO;->f:F

    .line 1181413
    iget-object v0, p2, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181414
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, p1}, LX/7Cp;->c([F)V

    .line 1181415
    return-void
.end method

.method public b(FF)V
    .locals 2

    .prologue
    .line 1181401
    neg-float v0, p2

    neg-float v1, p1

    invoke-virtual {p0, v0, v1}, LX/7Cg;->f(FF)V

    .line 1181402
    return-void
.end method

.method public c(FF)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 1181375
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->a:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181376
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->b:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181377
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->j:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181378
    iget-boolean v0, p0, LX/7Ce;->r:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/7Cg;->x:F

    invoke-static {p2, v0}, LX/47g;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/7Cg;->w:F

    invoke-static {p1, v0}, LX/47g;->a(FF)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1181379
    :cond_0
    iget v0, p0, LX/7Cg;->w:F

    sub-float v0, p1, v0

    .line 1181380
    iget v1, p0, LX/7Cg;->x:F

    sub-float v1, p2, v1

    .line 1181381
    iget-object v2, p0, LX/7Cg;->f:LX/7Cp;

    iget-object v3, p0, LX/7Cg;->c:LX/7Cp;

    invoke-virtual {v2, v3}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181382
    iget-object v2, p0, LX/7Ce;->k:LX/7Cp;

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v2, v3, v7, v6, v6}, LX/7Cp;->a(FFFF)V

    .line 1181383
    iget-object v2, p0, LX/7Cg;->f:LX/7Cp;

    iget-object v3, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v2, v3}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181384
    iget v2, p0, LX/7Cg;->v:F

    add-float/2addr v0, v2

    iput v0, p0, LX/7Cg;->v:F

    .line 1181385
    iget-object v0, p0, LX/7Cg;->t:LX/7Cp;

    iget-object v2, p0, LX/7Cg;->d:LX/7Cp;

    invoke-virtual {v0, v2}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181386
    iget v0, p0, LX/7Cg;->u:F

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cg;->u:F

    .line 1181387
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v6, v7, v6}, LX/7Cp;->a(FFFF)V

    .line 1181388
    iget-object v0, p0, LX/7Cg;->t:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181389
    :goto_0
    iget-object v0, p0, LX/7Cg;->e:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->c:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181390
    iget-object v0, p0, LX/7Cg;->g:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->d:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181391
    iput p2, p0, LX/7Cg;->x:F

    .line 1181392
    iput p1, p0, LX/7Cg;->w:F

    .line 1181393
    return-void

    .line 1181394
    :cond_1
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    invoke-virtual {v0, v1}, LX/7Cp;->b([F)V

    .line 1181395
    iget-object v0, p0, LX/7Ce;->i:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    add-float/2addr v0, p1

    .line 1181396
    iget v1, p0, LX/7Ce;->p:F

    add-float/2addr v1, p2

    iget-object v2, p0, LX/7Ce;->i:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    sub-float/2addr v1, v2

    .line 1181397
    iget-object v2, p0, LX/7Cg;->f:LX/7Cp;

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v2, v3, v7, v6, v6}, LX/7Cp;->a(FFFF)V

    .line 1181398
    iput v0, p0, LX/7Cg;->v:F

    .line 1181399
    iget-object v0, p0, LX/7Cg;->t:LX/7Cp;

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v0, v2, v6, v7, v6}, LX/7Cp;->a(FFFF)V

    .line 1181400
    iput v1, p0, LX/7Cg;->u:F

    goto :goto_0
.end method

.method public d(FF)V
    .locals 0

    .prologue
    .line 1181373
    invoke-virtual {p0, p1, p2}, LX/7Cg;->f(FF)V

    .line 1181374
    return-void
.end method

.method public final e(FF)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1181361
    invoke-super {p0, p1, p2}, LX/7Ce;->e(FF)V

    .line 1181362
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v5, v4, v4}, LX/7Cp;->a(FFFF)V

    .line 1181363
    iget-object v0, p0, LX/7Cg;->a:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181364
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    float-to-double v2, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v4, v5, v4}, LX/7Cp;->a(FFFF)V

    .line 1181365
    iget-object v0, p0, LX/7Cg;->a:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181366
    return-void
.end method

.method public f(FF)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1181367
    iget v0, p0, LX/7Cg;->v:F

    add-float/2addr v0, p1

    iput v0, p0, LX/7Cg;->v:F

    .line 1181368
    iget-object v0, p0, LX/7Cg;->c:LX/7Cp;

    iget v1, p0, LX/7Cg;->v:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v5, v4, v4}, LX/7Cp;->a(FFFF)V

    .line 1181369
    iget v0, p0, LX/7Cg;->u:F

    add-float/2addr v0, p2

    iput v0, p0, LX/7Cg;->u:F

    .line 1181370
    iget v0, p0, LX/7Cg;->u:F

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/7Cq;->a(FZ)F

    move-result v0

    iput v0, p0, LX/7Cg;->u:F

    .line 1181371
    iget-object v0, p0, LX/7Cg;->d:LX/7Cp;

    iget v1, p0, LX/7Cg;->u:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v4, v5, v4}, LX/7Cp;->a(FFFF)V

    .line 1181372
    return-void
.end method
