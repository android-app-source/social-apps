.class public final LX/8Pe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0jT;",
        "LX/0jT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public final synthetic e:Lcom/facebook/privacy/PrivacyOperationsClient;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/PrivacyOperationsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 0

    .prologue
    .line 1341963
    iput-object p1, p0, LX/8Pe;->e:Lcom/facebook/privacy/PrivacyOperationsClient;

    iput-object p2, p0, LX/8Pe;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8Pe;->b:Ljava/lang/String;

    iput-object p4, p0, LX/8Pe;->c:Ljava/lang/String;

    iput-object p5, p0, LX/8Pe;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1341964
    check-cast p1, LX/0jT;

    .line 1341965
    iget-object v0, p0, LX/8Pe;->e:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, LX/8Pe;->a:Ljava/lang/String;

    iget-object v2, p0, LX/8Pe;->b:Ljava/lang/String;

    iget-object v3, p0, LX/8Pe;->c:Ljava/lang/String;

    iget-object v4, p0, LX/8Pe;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1341966
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341967
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341968
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341969
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341970
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341971
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1341972
    const-string v5, "editPrivacyFeedStoryParams"

    new-instance v6, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;

    invoke-direct {v6, v1, v2, v3, v4}, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    invoke-virtual {v7, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1341973
    iget-object v5, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v6, "feed_edit_privacy"

    sget-object v8, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v9, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v10, -0x164d7924

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    .line 1341974
    invoke-static {v0, v5}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1341975
    return-object p1
.end method
