.class public LX/6sG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153035
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1153036
    const-string v0, "identifier"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6rc;->forValue(Ljava/lang/String;)LX/6rc;

    move-result-object v0

    .line 1153037
    sget-object v1, LX/6rc;->NOTES:LX/6rc;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1153038
    new-instance v1, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    sget-object v2, LX/6xN;->NOTE:LX/6xN;

    const-string v0, "placeholder_text"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "optional"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/6xe;->OPTIONAL:LX/6xe;

    :goto_1
    const-string v4, "type"

    invoke-virtual {p2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/6xO;->of(Ljava/lang/String;)LX/6xO;

    move-result-object v4

    invoke-static {v2, v3, v0, v4}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v0

    const-string v2, "length"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    .line 1153039
    iput v2, v0, LX/6xM;->f:I

    .line 1153040
    move-object v0, v0

    .line 1153041
    const-string v2, "prefilled_content"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1153042
    iput-object v2, v0, LX/6xM;->e:Ljava/lang/String;

    .line 1153043
    move-object v0, v0

    .line 1153044
    invoke-virtual {v0}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;-><init>(Lcom/facebook/payments/form/model/FormFieldAttributes;)V

    return-object v1

    .line 1153045
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1153046
    :cond_1
    sget-object v0, LX/6xe;->REQUIRED:LX/6xe;

    goto :goto_1
.end method
