.class public final LX/7yY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yX;


# instance fields
.field public final synthetic a:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;


# direct methods
.method public constructor <init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;)V
    .locals 0

    .prologue
    .line 1279472
    iput-object p1, p0, LX/7yY;->a:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1279473
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1279474
    :cond_0
    :goto_0
    return-void

    .line 1279475
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1279476
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279477
    if-eqz v0, :cond_2

    .line 1279478
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1279479
    :cond_3
    iget-object v0, p0, LX/7yY;->a:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1279480
    iput-object v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    .line 1279481
    goto :goto_0
.end method
