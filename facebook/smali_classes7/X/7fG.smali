.class public final enum LX/7fG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7fG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7fG;

.field public static final enum LC:LX/7fG;

.field public static final enum MAIN:LX/7fG;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1221451
    new-instance v0, LX/7fG;

    const-string v1, "MAIN"

    invoke-direct {v0, v1, v2, v2}, LX/7fG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/7fG;->MAIN:LX/7fG;

    .line 1221452
    new-instance v0, LX/7fG;

    const-string v1, "LC"

    invoke-direct {v0, v1, v3, v3}, LX/7fG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/7fG;->LC:LX/7fG;

    .line 1221453
    const/4 v0, 0x2

    new-array v0, v0, [LX/7fG;

    sget-object v1, LX/7fG;->MAIN:LX/7fG;

    aput-object v1, v0, v2

    sget-object v1, LX/7fG;->LC:LX/7fG;

    aput-object v1, v0, v3

    sput-object v0, LX/7fG;->$VALUES:[LX/7fG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1221454
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1221455
    iput p3, p0, LX/7fG;->value:I

    .line 1221456
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7fG;
    .locals 1

    .prologue
    .line 1221457
    const-class v0, LX/7fG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7fG;

    return-object v0
.end method

.method public static values()[LX/7fG;
    .locals 1

    .prologue
    .line 1221458
    sget-object v0, LX/7fG;->$VALUES:[LX/7fG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7fG;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 1221459
    iget v0, p0, LX/7fG;->value:I

    return v0
.end method
