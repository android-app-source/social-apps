.class public LX/8Ga;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1319791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 1319792
    rem-int/lit8 v0, p0, 0x5a

    if-eqz v0, :cond_0

    .line 1319793
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Rotation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1319794
    :cond_0
    div-int/lit8 v0, p0, 0x5a

    rsub-int/lit8 v0, v0, 0x4

    rem-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static a(Landroid/graphics/RectF;I)Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1319795
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1319796
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Landroid/graphics/RectF;->right:F

    sub-float v2, v5, v2

    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Landroid/graphics/RectF;->left:F

    sub-float v4, v5, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1319797
    :goto_0
    return-object v0

    .line 1319798
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1319799
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Landroid/graphics/RectF;->right:F

    sub-float v1, v5, v1

    iget v2, p0, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v5, v2

    iget v3, p0, Landroid/graphics/RectF;->left:F

    sub-float v3, v5, v3

    iget v4, p0, Landroid/graphics/RectF;->top:F

    sub-float v4, v5, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 1319800
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 1319801
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v5, v1

    iget v2, p0, Landroid/graphics/RectF;->left:F

    iget v3, p0, Landroid/graphics/RectF;->top:F

    sub-float v3, v5, v3

    iget v4, p0, Landroid/graphics/RectF;->right:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 1319802
    :cond_2
    if-eqz p1, :cond_3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 1319803
    :cond_3
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    goto :goto_0

    .line 1319804
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Number of counter clock wise rotation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
