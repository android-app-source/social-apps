.class public final LX/8Tc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/8Td;

.field public final synthetic c:LX/8Te;


# direct methods
.method public constructor <init>(LX/8Te;Ljava/lang/String;LX/8Td;)V
    .locals 0

    .prologue
    .line 1348811
    iput-object p1, p0, LX/8Tc;->c:LX/8Te;

    iput-object p2, p0, LX/8Tc;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8Tc;->b:LX/8Td;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1348812
    iget-object v0, p0, LX/8Tc;->c:LX/8Te;

    iget-object v0, v0, LX/8Te;->e:LX/8TD;

    sget-object v1, LX/8TE;->ALL_MATCHES:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1348813
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1348814
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1348815
    iget-object v0, p0, LX/8Tc;->c:LX/8Te;

    iget-object v0, v0, LX/8Te;->e:LX/8TD;

    sget-object v1, LX/8TE;->ALL_MATCHES:LX/8TE;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1348816
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1348817
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 1348818
    iget-object v3, p0, LX/8Tc;->c:LX/8Te;

    iget-object v4, p0, LX/8Tc;->a:Ljava/lang/String;

    .line 1348819
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1348820
    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v3, v4, v0, v1, v2}, LX/8Te;->a$redex0(LX/8Te;Ljava/lang/String;LX/0Px;Ljava/util/List;Ljava/util/List;)V

    .line 1348821
    iget-object v0, p0, LX/8Tc;->b:LX/8Td;

    invoke-interface {v0, v1, v2}, LX/8Td;->a(Ljava/util/List;Ljava/util/List;)V

    .line 1348822
    return-void
.end method
