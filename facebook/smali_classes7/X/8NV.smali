.class public LX/8NV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;LX/60x;JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 1337356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337357
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1337358
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1337359
    iput-object p3, p0, LX/8NV;->a:Ljava/lang/String;

    .line 1337360
    iput-wide p1, p0, LX/8NV;->b:J

    .line 1337361
    iput-object v0, p0, LX/8NV;->c:Ljava/util/Map;

    .line 1337362
    iget-wide v2, p4, LX/60x;->a:J

    .line 1337363
    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 1337364
    iget-object v0, p0, LX/8NV;->c:Ljava/util/Map;

    const-string v4, "video_duration_milliseconds"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337365
    iget v0, p4, LX/60x;->e:I

    int-to-long v2, v0

    .line 1337366
    iget-object v0, p0, LX/8NV;->c:Ljava/util/Map;

    const-string v4, "video_bit_rate_bps"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337367
    iget v0, p4, LX/60x;->g:I

    int-to-long v2, v0

    .line 1337368
    iget-object v0, p0, LX/8NV;->c:Ljava/util/Map;

    const-string v4, "audio_bit_rate_bps"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337369
    iget v0, p4, LX/60x;->b:I

    int-to-long v2, v0

    .line 1337370
    iget-object v0, p0, LX/8NV;->c:Ljava/util/Map;

    const-string v4, "video_width"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337371
    iget v0, p4, LX/60x;->c:I

    int-to-long v2, v0

    .line 1337372
    iget-object v0, p0, LX/8NV;->c:Ljava/util/Map;

    const-string v4, "video_height"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337373
    iget-object v0, p0, LX/8NV;->c:Ljava/util/Map;

    const-string v2, "video_original_file_size"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337374
    iput-object v1, p0, LX/8NV;->d:Ljava/util/Map;

    .line 1337375
    iget-object v0, p0, LX/8NV;->d:Ljava/util/Map;

    const-string v1, "battery"

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337376
    iget-object v0, p0, LX/8NV;->d:Ljava/util/Map;

    const-string v1, "quality"

    invoke-interface {v0, v1, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337377
    return-void
.end method
