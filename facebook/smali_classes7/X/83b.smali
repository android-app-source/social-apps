.class public LX/83b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/83b;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0rq;

.field public final c:LX/0TD;


# direct methods
.method public constructor <init>(LX/0tX;LX/0TD;LX/0rq;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1290547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1290548
    iput-object p1, p0, LX/83b;->a:LX/0tX;

    .line 1290549
    iput-object p2, p0, LX/83b;->c:LX/0TD;

    .line 1290550
    iput-object p3, p0, LX/83b;->b:LX/0rq;

    .line 1290551
    return-void
.end method

.method public static a(LX/0QB;)LX/83b;
    .locals 6

    .prologue
    .line 1290552
    sget-object v0, LX/83b;->d:LX/83b;

    if-nez v0, :cond_1

    .line 1290553
    const-class v1, LX/83b;

    monitor-enter v1

    .line 1290554
    :try_start_0
    sget-object v0, LX/83b;->d:LX/83b;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1290555
    if-eqz v2, :cond_0

    .line 1290556
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1290557
    new-instance p0, LX/83b;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-direct {p0, v3, v4, v5}, LX/83b;-><init>(LX/0tX;LX/0TD;LX/0rq;)V

    .line 1290558
    move-object v0, p0

    .line 1290559
    sput-object v0, LX/83b;->d:LX/83b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1290560
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1290561
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1290562
    :cond_1
    sget-object v0, LX/83b;->d:LX/83b;

    return-object v0

    .line 1290563
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1290564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
