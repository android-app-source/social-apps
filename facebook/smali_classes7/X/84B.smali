.class public final LX/84B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2hA;

.field public final synthetic b:LX/5P1;

.field public final synthetic c:LX/3UJ;


# direct methods
.method public constructor <init>(LX/3UJ;LX/2hA;LX/5P1;)V
    .locals 0

    .prologue
    .line 1290826
    iput-object p1, p0, LX/84B;->c:LX/3UJ;

    iput-object p2, p0, LX/84B;->a:LX/2hA;

    iput-object p3, p0, LX/84B;->b:LX/5P1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1290827
    iget-object v0, p0, LX/84B;->c:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->a:LX/2dj;

    iget-object v1, p0, LX/84B;->a:LX/2hA;

    iget-object v2, p0, LX/84B;->b:LX/5P1;

    .line 1290828
    new-instance v3, LX/4Ez;

    invoke-direct {v3}, LX/4Ez;-><init>()V

    iget-object v4, v1, LX/2hA;->value:Ljava/lang/String;

    .line 1290829
    const-string v5, "source"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1290830
    move-object v3, v3

    .line 1290831
    invoke-virtual {v2}, LX/5P1;->getFilterName()Ljava/lang/String;

    move-result-object v4

    .line 1290832
    const-string v5, "condition"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1290833
    move-object v3, v3

    .line 1290834
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    long-to-int v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1290835
    const-string v5, "timestamp_before"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1290836
    move-object v3, v3

    .line 1290837
    new-instance v4, LX/85b;

    invoke-direct {v4}, LX/85b;-><init>()V

    move-object v4, v4

    .line 1290838
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1290839
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1290840
    iget-object v5, v0, LX/2dj;->l:LX/2dm;

    invoke-virtual {v5, v3}, LX/2dm;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v3, v3

    .line 1290841
    new-instance v4, LX/83x;

    invoke-direct {v4, v0}, LX/83x;-><init>(LX/2dj;)V

    iget-object v5, v0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1290842
    return-object v0
.end method
