.class public LX/79A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ze;


# instance fields
.field private final a:LX/1gy;

.field private final b:LX/01U;


# direct methods
.method public constructor <init>(LX/1gy;LX/01U;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1173885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1173886
    iput-object p1, p0, LX/79A;->a:LX/1gy;

    .line 1173887
    iput-object p2, p0, LX/79A;->b:LX/01U;

    .line 1173888
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1173889
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 1173890
    const-string v1, "Git_Hash"

    iget-object v2, p0, LX/79A;->a:LX/1gy;

    iget-object v2, v2, LX/1gy;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1173891
    iget-object v1, p0, LX/79A;->b:LX/01U;

    sget-object v2, LX/01U;->PROD:LX/01U;

    if-eq v1, v2, :cond_0

    .line 1173892
    const-string v1, "Git_Branch"

    iget-object v2, p0, LX/79A;->a:LX/1gy;

    iget-object v2, v2, LX/1gy;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1173893
    const-string v1, "Build_Time"

    iget-object v2, p0, LX/79A;->a:LX/1gy;

    iget-object v2, v2, LX/1gy;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1173894
    :cond_0
    return-object v0
.end method
