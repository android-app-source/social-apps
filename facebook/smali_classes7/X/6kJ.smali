.class public LX/6kJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final roomDiscoverableMode:Ljava/lang/Integer;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1136074
    new-instance v0, LX/1sv;

    const-string v1, "DeltaRoomDiscoverableMode"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kJ;->b:LX/1sv;

    .line 1136075
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kJ;->c:LX/1sw;

    .line 1136076
    new-instance v0, LX/1sw;

    const-string v1, "roomDiscoverableMode"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kJ;->d:LX/1sw;

    .line 1136077
    sput-boolean v4, LX/6kJ;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1136070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136071
    iput-object p1, p0, LX/6kJ;->threadKey:LX/6l9;

    .line 1136072
    iput-object p2, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    .line 1136073
    return-void
.end method

.method public static a(LX/6kJ;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1136063
    iget-object v0, p0, LX/6kJ;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1136064
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kJ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136065
    :cond_0
    iget-object v0, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1136066
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'roomDiscoverableMode\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kJ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136067
    :cond_1
    iget-object v0, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    sget-object v0, LX/6l2;->a:LX/1sn;

    iget-object v1, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1136068
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'roomDiscoverableMode\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1136069
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136029
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1136030
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1136031
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1136032
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaRoomDiscoverableMode"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136033
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136034
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136035
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136036
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136037
    const-string v4, "threadKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136038
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136039
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136040
    iget-object v4, p0, LX/6kJ;->threadKey:LX/6l9;

    if-nez v4, :cond_4

    .line 1136041
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136042
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136043
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136044
    const-string v4, "roomDiscoverableMode"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136045
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136046
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136047
    iget-object v0, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    if-nez v0, :cond_5

    .line 1136048
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136049
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136050
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136051
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136052
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1136053
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1136054
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1136055
    :cond_4
    iget-object v4, p0, LX/6kJ;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1136056
    :cond_5
    sget-object v0, LX/6l2;->b:Ljava/util/Map;

    iget-object v4, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1136057
    if-eqz v0, :cond_6

    .line 1136058
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136059
    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136060
    :cond_6
    iget-object v4, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1136061
    if-eqz v0, :cond_0

    .line 1136062
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1135992
    invoke-static {p0}, LX/6kJ;->a(LX/6kJ;)V

    .line 1135993
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135994
    iget-object v0, p0, LX/6kJ;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1135995
    sget-object v0, LX/6kJ;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135996
    iget-object v0, p0, LX/6kJ;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1135997
    :cond_0
    iget-object v0, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1135998
    sget-object v0, LX/6kJ;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135999
    iget-object v0, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1136000
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136001
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136002
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136007
    if-nez p1, :cond_1

    .line 1136008
    :cond_0
    :goto_0
    return v0

    .line 1136009
    :cond_1
    instance-of v1, p1, LX/6kJ;

    if-eqz v1, :cond_0

    .line 1136010
    check-cast p1, LX/6kJ;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136011
    if-nez p1, :cond_3

    .line 1136012
    :cond_2
    :goto_1
    move v0, v2

    .line 1136013
    goto :goto_0

    .line 1136014
    :cond_3
    iget-object v0, p0, LX/6kJ;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1136015
    :goto_2
    iget-object v3, p1, LX/6kJ;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1136016
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136017
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136018
    iget-object v0, p0, LX/6kJ;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6kJ;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136019
    :cond_5
    iget-object v0, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1136020
    :goto_4
    iget-object v3, p1, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1136021
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136022
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136023
    iget-object v0, p0, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1136024
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1136025
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1136026
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1136027
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1136028
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136006
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136003
    sget-boolean v0, LX/6kJ;->a:Z

    .line 1136004
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kJ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136005
    return-object v0
.end method
