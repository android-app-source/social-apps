.class public final enum LX/76S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/76S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/76S;

.field public static final enum ANIMATED:LX/76S;

.field public static final enum ANY:LX/76S;

.field public static final enum STATIC:LX/76S;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1171031
    new-instance v0, LX/76S;

    const-string v1, "ANY"

    invoke-direct {v0, v1, v2}, LX/76S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/76S;->ANY:LX/76S;

    .line 1171032
    new-instance v0, LX/76S;

    const-string v1, "STATIC"

    invoke-direct {v0, v1, v3}, LX/76S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/76S;->STATIC:LX/76S;

    .line 1171033
    new-instance v0, LX/76S;

    const-string v1, "ANIMATED"

    invoke-direct {v0, v1, v4}, LX/76S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/76S;->ANIMATED:LX/76S;

    .line 1171034
    const/4 v0, 0x3

    new-array v0, v0, [LX/76S;

    sget-object v1, LX/76S;->ANY:LX/76S;

    aput-object v1, v0, v2

    sget-object v1, LX/76S;->STATIC:LX/76S;

    aput-object v1, v0, v3

    sget-object v1, LX/76S;->ANIMATED:LX/76S;

    aput-object v1, v0, v4

    sput-object v0, LX/76S;->$VALUES:[LX/76S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1171035
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/76S;
    .locals 1

    .prologue
    .line 1171030
    const-class v0, LX/76S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/76S;

    return-object v0
.end method

.method public static values()[LX/76S;
    .locals 1

    .prologue
    .line 1171029
    sget-object v0, LX/76S;->$VALUES:[LX/76S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/76S;

    return-object v0
.end method
