.class public final enum LX/8K2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8K2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8K2;

.field public static final enum CONNECTED:LX/8K2;

.field public static final enum NO_INTERNET:LX/8K2;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1329786
    new-instance v0, LX/8K2;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2}, LX/8K2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8K2;->CONNECTED:LX/8K2;

    new-instance v0, LX/8K2;

    const-string v1, "NO_INTERNET"

    invoke-direct {v0, v1, v3}, LX/8K2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8K2;->NO_INTERNET:LX/8K2;

    .line 1329787
    const/4 v0, 0x2

    new-array v0, v0, [LX/8K2;

    sget-object v1, LX/8K2;->CONNECTED:LX/8K2;

    aput-object v1, v0, v2

    sget-object v1, LX/8K2;->NO_INTERNET:LX/8K2;

    aput-object v1, v0, v3

    sput-object v0, LX/8K2;->$VALUES:[LX/8K2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1329788
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8K2;
    .locals 1

    .prologue
    .line 1329789
    const-class v0, LX/8K2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8K2;

    return-object v0
.end method

.method public static values()[LX/8K2;
    .locals 1

    .prologue
    .line 1329790
    sget-object v0, LX/8K2;->$VALUES:[LX/8K2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8K2;

    return-object v0
.end method
