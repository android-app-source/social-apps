.class public LX/7Rw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Xl;

.field public final b:LX/0Uo;

.field public final c:LX/378;

.field public d:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/7Ru;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Xl;LX/0Uo;LX/378;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1207961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207962
    iput-object p2, p0, LX/7Rw;->b:LX/0Uo;

    .line 1207963
    iput-object p1, p0, LX/7Rw;->a:LX/0Xl;

    .line 1207964
    iput-object p3, p0, LX/7Rw;->c:LX/378;

    .line 1207965
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1207953
    iget-object v0, p0, LX/7Rw;->d:LX/0Yb;

    if-nez v0, :cond_0

    .line 1207954
    new-instance v0, LX/7Rt;

    invoke-direct {v0, p0}, LX/7Rt;-><init>(LX/7Rw;)V

    .line 1207955
    iget-object v1, p0, LX/7Rw;->a:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/7Rw;->d:LX/0Yb;

    .line 1207956
    :cond_0
    iget-object v0, p0, LX/7Rw;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1207957
    iget-object v0, p0, LX/7Rw;->e:LX/7Ru;

    if-nez v0, :cond_1

    .line 1207958
    new-instance v0, LX/7Rv;

    invoke-direct {v0, p0}, LX/7Rv;-><init>(LX/7Rw;)V

    iput-object v0, p0, LX/7Rw;->e:LX/7Ru;

    .line 1207959
    :cond_1
    iget-object v0, p0, LX/7Rw;->c:LX/378;

    iget-object v1, p0, LX/7Rw;->e:LX/7Ru;

    invoke-virtual {v0, v1}, LX/378;->a(LX/7Ru;)V

    .line 1207960
    return-void
.end method
