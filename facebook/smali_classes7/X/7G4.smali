.class public LX/7G4;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1188756
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "sync_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "last_seq_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "max_deltas_able_to_process"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "delta_batch_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "encoding"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "queue_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sync_api_version"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sync_device_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "device_params"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "queue_params"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "entity_fbid"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, LX/7G4;->a:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1188719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0m9;)Z
    .locals 3

    .prologue
    .line 1188751
    invoke-virtual {p0}, LX/0lF;->j()Ljava/util/Iterator;

    move-result-object v0

    .line 1188752
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1188753
    sget-object v1, LX/7G4;->a:Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1188754
    const/4 v0, 0x0

    .line 1188755
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)LX/7GX;
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1188744
    iget-object v0, p0, LX/7G4;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1188745
    iget-object v0, p0, LX/7G4;->c:Ljava/lang/Long;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1188746
    if-eqz p1, :cond_1

    .line 1188747
    :try_start_0
    iget-object v0, p0, LX/7G4;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 1188748
    :goto_0
    new-instance v0, LX/7GX;

    if-nez v12, :cond_0

    iget-object v1, p0, LX/7G4;->b:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, LX/7G4;->c:Ljava/lang/Long;

    iget-object v3, p0, LX/7G4;->d:Ljava/lang/Integer;

    iget-object v4, p0, LX/7G4;->e:Ljava/lang/Integer;

    iget-object v5, p0, LX/7G4;->f:Ljava/lang/String;

    iget-object v6, p0, LX/7G4;->g:Ljava/lang/String;

    iget-object v7, p0, LX/7G4;->h:Ljava/lang/Integer;

    iget-object v8, p0, LX/7G4;->i:Ljava/lang/String;

    iget-object v9, p0, LX/7G4;->j:Ljava/lang/String;

    iget-object v10, p0, LX/7G4;->k:Ljava/lang/String;

    iget-object v11, p0, LX/7G4;->l:Ljava/lang/Long;

    invoke-direct/range {v0 .. v12}, LX/7GX;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    return-object v0

    .line 1188749
    :catch_0
    move-exception v0

    .line 1188750
    const-string v2, "GetIrisDiffsBuilder"

    const-string v3, "Failed to convert sync token %s into a Long."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/7G4;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move-object v12, v1

    goto :goto_0
.end method

.method public final b(LX/0m9;)LX/7G4;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1188720
    invoke-static {p1}, LX/7G4;->a(LX/0m9;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1188721
    const-string v0, "sync_token"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, LX/7G4;->b:Ljava/lang/String;

    .line 1188722
    const-string v0, "last_seq_id"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, LX/7G4;->c:Ljava/lang/Long;

    .line 1188723
    const-string v0, "max_deltas_able_to_process"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    iput-object v0, p0, LX/7G4;->d:Ljava/lang/Integer;

    .line 1188724
    const-string v0, "delta_batch_size"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    iput-object v0, p0, LX/7G4;->e:Ljava/lang/Integer;

    .line 1188725
    const-string v0, "encoding"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_4
    iput-object v0, p0, LX/7G4;->f:Ljava/lang/String;

    .line 1188726
    const-string v0, "queue_type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    :goto_5
    iput-object v0, p0, LX/7G4;->g:Ljava/lang/String;

    .line 1188727
    const-string v0, "sync_api_version"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_6

    move-object v0, v1

    :goto_6
    iput-object v0, p0, LX/7G4;->h:Ljava/lang/Integer;

    .line 1188728
    const-string v0, "sync_device_id"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_7

    move-object v0, v1

    :goto_7
    iput-object v0, p0, LX/7G4;->i:Ljava/lang/String;

    .line 1188729
    const-string v0, "device_params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_8

    move-object v0, v1

    :goto_8
    iput-object v0, p0, LX/7G4;->j:Ljava/lang/String;

    .line 1188730
    const-string v0, "queue_params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_9

    move-object v0, v1

    :goto_9
    iput-object v0, p0, LX/7G4;->k:Ljava/lang/String;

    .line 1188731
    const-string v0, "entity_fbid"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_a

    :goto_a
    iput-object v1, p0, LX/7G4;->l:Ljava/lang/Long;

    .line 1188732
    return-object p0

    .line 1188733
    :cond_0
    const-string v0, "sync_token"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1188734
    :cond_1
    const-string v0, "last_seq_id"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    .line 1188735
    :cond_2
    const-string v0, "max_deltas_able_to_process"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_2

    .line 1188736
    :cond_3
    const-string v0, "delta_batch_size"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_3

    .line 1188737
    :cond_4
    const-string v0, "encoding"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 1188738
    :cond_5
    const-string v0, "queue_type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1188739
    :cond_6
    const-string v0, "sync_api_version"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_6

    .line 1188740
    :cond_7
    const-string v0, "sync_device_id"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 1188741
    :cond_8
    const-string v0, "device_params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 1188742
    :cond_9
    const-string v0, "queue_params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 1188743
    :cond_a
    const-string v0, "entity_fbid"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto/16 :goto_a
.end method
