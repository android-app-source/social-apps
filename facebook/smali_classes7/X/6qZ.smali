.class public final LX/6qZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Z

.field public B:LX/0m9;

.field public C:Landroid/os/Parcelable;

.field public D:Z

.field public E:Z

.field public final a:LX/6qw;

.field public final b:LX/6xg;

.field public final c:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

.field public d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/6re;

.field public f:Ljava/util/Currency;

.field public g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lorg/json/JSONObject;

.field public k:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public l:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

.field public m:I

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6vb;",
            ">;"
        }
    .end annotation
.end field

.field public u:Z

.field public v:Z

.field public w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

.field public y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6so;",
            ">;"
        }
    .end annotation
.end field

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6qw;",
            "LX/6xg;",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;",
            "Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1150826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150827
    sget-object v0, LX/6re;->FIXED_AMOUNT:LX/6re;

    iput-object v0, p0, LX/6qZ;->e:LX/6re;

    .line 1150828
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    iput-object v0, p0, LX/6qZ;->k:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1150829
    sget-object v0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->a:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    iput-object v0, p0, LX/6qZ;->l:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1150830
    const v0, 0x7f081d3e

    iput v0, p0, LX/6qZ;->m:I

    .line 1150831
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1150832
    iput-object v0, p0, LX/6qZ;->t:LX/0Rf;

    .line 1150833
    iput-boolean v2, p0, LX/6qZ;->u:Z

    .line 1150834
    iput-boolean v1, p0, LX/6qZ;->v:Z

    .line 1150835
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1150836
    iput-object v0, p0, LX/6qZ;->w:LX/0Px;

    .line 1150837
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1150838
    iput-object v0, p0, LX/6qZ;->y:LX/0Px;

    .line 1150839
    iput-boolean v2, p0, LX/6qZ;->A:Z

    .line 1150840
    iput-boolean v1, p0, LX/6qZ;->D:Z

    .line 1150841
    iput-boolean v1, p0, LX/6qZ;->E:Z

    .line 1150842
    iput-object p1, p0, LX/6qZ;->a:LX/6qw;

    .line 1150843
    iput-object p2, p0, LX/6qZ;->b:LX/6xg;

    .line 1150844
    iput-object p3, p0, LX/6qZ;->d:LX/0Rf;

    .line 1150845
    iput-object p4, p0, LX/6qZ;->c:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    .line 1150846
    return-void
.end method

.method public static a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)LX/6qZ;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1150696
    new-instance v0, LX/6qZ;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    iget-object v2, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    iget-object v3, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    iget-object v4, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    invoke-direct {v0, v1, v2, v3, v4}, LX/6qZ;-><init>(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)V

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    .line 1150697
    iput-object v1, v0, LX/6qZ;->e:LX/6re;

    .line 1150698
    move-object v0, v0

    .line 1150699
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->e:Ljava/util/Currency;

    .line 1150700
    iput-object v1, v0, LX/6qZ;->f:Ljava/util/Currency;

    .line 1150701
    move-object v0, v0

    .line 1150702
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1150703
    iput-object v1, v0, LX/6qZ;->g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1150704
    move-object v0, v0

    .line 1150705
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    .line 1150706
    iput-object v1, v0, LX/6qZ;->h:LX/0Px;

    .line 1150707
    move-object v0, v0

    .line 1150708
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->h:LX/0Px;

    .line 1150709
    iput-object v1, v0, LX/6qZ;->i:LX/0Px;

    .line 1150710
    move-object v0, v0

    .line 1150711
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->i:Lorg/json/JSONObject;

    .line 1150712
    iput-object v1, v0, LX/6qZ;->j:Lorg/json/JSONObject;

    .line 1150713
    move-object v0, v0

    .line 1150714
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1150715
    iput-object v1, v0, LX/6qZ;->k:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1150716
    move-object v0, v0

    .line 1150717
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->k:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1150718
    iput-object v1, v0, LX/6qZ;->l:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1150719
    move-object v0, v0

    .line 1150720
    iget v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->l:I

    .line 1150721
    iput v1, v0, LX/6qZ;->m:I

    .line 1150722
    move-object v0, v0

    .line 1150723
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->m:Z

    .line 1150724
    iput-boolean v1, v0, LX/6qZ;->n:Z

    .line 1150725
    move-object v0, v0

    .line 1150726
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    .line 1150727
    iput-boolean v1, v0, LX/6qZ;->o:Z

    .line 1150728
    move-object v0, v0

    .line 1150729
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->o:Ljava/lang/String;

    .line 1150730
    iput-object v1, v0, LX/6qZ;->p:Ljava/lang/String;

    .line 1150731
    move-object v0, v0

    .line 1150732
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->p:Ljava/lang/String;

    .line 1150733
    iput-object v1, v0, LX/6qZ;->q:Ljava/lang/String;

    .line 1150734
    move-object v0, v0

    .line 1150735
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->q:Ljava/lang/String;

    .line 1150736
    iput-object v1, v0, LX/6qZ;->r:Ljava/lang/String;

    .line 1150737
    move-object v0, v0

    .line 1150738
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->r:Ljava/lang/String;

    .line 1150739
    iput-object v1, v0, LX/6qZ;->s:Ljava/lang/String;

    .line 1150740
    move-object v0, v0

    .line 1150741
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1150742
    iput-object v1, v0, LX/6qZ;->t:LX/0Rf;

    .line 1150743
    move-object v0, v0

    .line 1150744
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->t:Z

    .line 1150745
    iput-boolean v1, v0, LX/6qZ;->u:Z

    .line 1150746
    move-object v0, v0

    .line 1150747
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->u:Z

    .line 1150748
    iput-boolean v1, v0, LX/6qZ;->v:Z

    .line 1150749
    move-object v0, v0

    .line 1150750
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    .line 1150751
    iput-object v1, v0, LX/6qZ;->w:LX/0Px;

    .line 1150752
    move-object v0, v0

    .line 1150753
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1150754
    iput-object v1, v0, LX/6qZ;->x:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1150755
    move-object v0, v0

    .line 1150756
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->x:LX/0Px;

    .line 1150757
    iput-object v1, v0, LX/6qZ;->y:LX/0Px;

    .line 1150758
    move-object v0, v0

    .line 1150759
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->y:Ljava/lang/String;

    .line 1150760
    iput-object v1, v0, LX/6qZ;->z:Ljava/lang/String;

    .line 1150761
    move-object v0, v0

    .line 1150762
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->z:Z

    .line 1150763
    iput-boolean v1, v0, LX/6qZ;->A:Z

    .line 1150764
    move-object v0, v0

    .line 1150765
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    .line 1150766
    iput-object v1, v0, LX/6qZ;->B:LX/0m9;

    .line 1150767
    move-object v0, v0

    .line 1150768
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->B:Landroid/os/Parcelable;

    .line 1150769
    iput-object v1, v0, LX/6qZ;->C:Landroid/os/Parcelable;

    .line 1150770
    move-object v0, v0

    .line 1150771
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    .line 1150772
    iput-boolean v1, v0, LX/6qZ;->D:Z

    .line 1150773
    move-object v0, v0

    .line 1150774
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->C:Z

    .line 1150775
    iput-boolean v1, v0, LX/6qZ;->E:Z

    .line 1150776
    move-object v0, v0

    .line 1150777
    return-object v0
.end method

.method public static a$redex0(LX/6qZ;Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)LX/6qZ;
    .locals 6

    .prologue
    .line 1150778
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1150779
    iput-object v0, p0, LX/6qZ;->g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1150780
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->b:LX/0Px;

    .line 1150781
    iput-object v0, p0, LX/6qZ;->i:LX/0Px;

    .line 1150782
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->c:LX/0Px;

    .line 1150783
    iput-object v0, p0, LX/6qZ;->h:LX/0Px;

    .line 1150784
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    if-eqz v0, :cond_1

    .line 1150785
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    invoke-static {v0}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/0Px;)LX/0Rf;

    move-result-object v0

    .line 1150786
    iput-object v0, p0, LX/6qZ;->d:LX/0Rf;

    .line 1150787
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    .line 1150788
    new-instance v3, LX/0cA;

    invoke-direct {v3}, LX/0cA;-><init>()V

    .line 1150789
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;

    .line 1150790
    sget-object v5, LX/6qY;->a:[I

    invoke-interface {v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;->a()LX/6rc;

    move-result-object v1

    invoke-virtual {v1}, LX/6rc;->ordinal()I

    move-result v1

    aget v1, v5, v1

    packed-switch v1, :pswitch_data_0

    .line 1150791
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1150792
    :pswitch_0
    sget-object v1, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v3, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 1150793
    :pswitch_1
    sget-object v1, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v3, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1150794
    sget-object v1, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-virtual {v3, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 1150795
    :pswitch_2
    sget-object v1, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-virtual {v3, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 1150796
    :cond_0
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    move-object v0, v1

    .line 1150797
    iput-object v0, p0, LX/6qZ;->t:LX/0Rf;

    .line 1150798
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    .line 1150799
    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    const-class v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    invoke-virtual {v1, v2}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v1

    invoke-virtual {v1}, LX/0wv;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1150800
    iput-object v0, p0, LX/6qZ;->w:LX/0Px;

    .line 1150801
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    const-class v1, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    invoke-virtual {v0, v1}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1150802
    iput-object v0, p0, LX/6qZ;->x:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1150803
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    .line 1150804
    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    const-class v2, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;

    invoke-virtual {v1, v2}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v1

    invoke-virtual {v1}, LX/0wv;->a()LX/0am;

    move-result-object v1

    .line 1150805
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1150806
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;

    iget-boolean v1, v1, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;->a:Z

    .line 1150807
    :goto_2
    move v0, v1

    .line 1150808
    iput-boolean v0, p0, LX/6qZ;->u:Z

    .line 1150809
    :cond_1
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    if-eqz v0, :cond_2

    .line 1150810
    iget-object v0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    .line 1150811
    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->a:Ljava/lang/String;

    .line 1150812
    iput-object v1, p0, LX/6qZ;->z:Ljava/lang/String;

    .line 1150813
    invoke-static {}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->newBuilder()LX/6tR;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1150814
    iput-object v2, v1, LX/6tR;->b:Landroid/net/Uri;

    .line 1150815
    move-object v1, v1

    .line 1150816
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->c:Ljava/lang/String;

    .line 1150817
    iput-object v2, v1, LX/6tR;->d:Ljava/lang/String;

    .line 1150818
    move-object v1, v1

    .line 1150819
    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->d:Ljava/lang/String;

    .line 1150820
    iput-object v0, v1, LX/6tR;->c:Ljava/lang/String;

    .line 1150821
    move-object v0, v1

    .line 1150822
    invoke-virtual {v0}, LX/6tR;->a()Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    move-result-object v0

    .line 1150823
    iput-object v0, p0, LX/6qZ;->l:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1150824
    :cond_2
    return-object p0

    :cond_3
    const/4 v1, 0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 2

    .prologue
    .line 1150825
    new-instance v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/checkout/CheckoutCommonParams;-><init>(LX/6qZ;)V

    return-object v0
.end method
