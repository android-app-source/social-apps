.class public final LX/72S;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

.field public final synthetic d:Z

.field public final synthetic e:Z

.field public final synthetic f:LX/72T;


# direct methods
.method public constructor <init>(LX/72T;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;ZZ)V
    .locals 0

    .prologue
    .line 1164545
    iput-object p1, p0, LX/72S;->f:LX/72T;

    iput-object p2, p0, LX/72S;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object p3, p0, LX/72S;->b:Ljava/lang/String;

    iput-object p4, p0, LX/72S;->c:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    iput-boolean p5, p0, LX/72S;->d:Z

    iput-boolean p6, p0, LX/72S;->e:Z

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1164546
    iget-object v0, p0, LX/72S;->f:LX/72T;

    iget-object v1, p0, LX/72S;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, LX/72S;->f:LX/72T;

    iget-object v2, v2, LX/72T;->d:Landroid/content/Context;

    const v3, 0x7f081e64

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, LX/72T;->a$redex0(LX/72T;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1164547
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1164548
    iget-object v0, p0, LX/72S;->f:LX/72T;

    iget-object v1, p0, LX/72S;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, LX/72S;->b:Ljava/lang/String;

    iget-object v3, p0, LX/72S;->c:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    iget-boolean v4, p0, LX/72S;->d:Z

    iget-boolean v5, p0, LX/72S;->e:Z

    .line 1164549
    iget-object v6, v0, LX/72T;->g:LX/6xb;

    sget-object v7, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    const-string p0, "payflows_success"

    invoke-virtual {v6, v1, v7, p0}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1164550
    if-eqz v5, :cond_0

    .line 1164551
    iget-object v6, v0, LX/72T;->a:LX/6qh;

    new-instance v7, LX/73T;

    sget-object p0, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v7, p0}, LX/73T;-><init>(LX/73S;)V

    invoke-virtual {v6, v7}, LX/6qh;->a(LX/73T;)V

    .line 1164552
    :goto_0
    return-void

    .line 1164553
    :cond_0
    invoke-static {}, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->newBuilder()LX/72i;

    move-result-object v6

    .line 1164554
    iput-object v2, v6, LX/72i;->a:Ljava/lang/String;

    .line 1164555
    move-object v6, v6

    .line 1164556
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->a:Ljava/lang/String;

    .line 1164557
    iput-object v7, v6, LX/72i;->b:Ljava/lang/String;

    .line 1164558
    move-object v6, v6

    .line 1164559
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->c:Ljava/lang/String;

    .line 1164560
    iput-object v7, v6, LX/72i;->c:Ljava/lang/String;

    .line 1164561
    move-object v6, v6

    .line 1164562
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->d:Ljava/lang/String;

    .line 1164563
    iput-object v7, v6, LX/72i;->d:Ljava/lang/String;

    .line 1164564
    move-object v6, v6

    .line 1164565
    const-string v7, "%s, %s"

    iget-object p0, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->e:Ljava/lang/String;

    iget-object p1, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->f:Ljava/lang/String;

    invoke-static {v7, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1164566
    iput-object v7, v6, LX/72i;->e:Ljava/lang/String;

    .line 1164567
    move-object v6, v6

    .line 1164568
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->e:Ljava/lang/String;

    .line 1164569
    iput-object v7, v6, LX/72i;->i:Ljava/lang/String;

    .line 1164570
    move-object v6, v6

    .line 1164571
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->f:Ljava/lang/String;

    .line 1164572
    iput-object v7, v6, LX/72i;->j:Ljava/lang/String;

    .line 1164573
    move-object v6, v6

    .line 1164574
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->g:Ljava/lang/String;

    .line 1164575
    iput-object v7, v6, LX/72i;->f:Ljava/lang/String;

    .line 1164576
    move-object v6, v6

    .line 1164577
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->h:Lcom/facebook/common/locale/Country;

    .line 1164578
    iput-object v7, v6, LX/72i;->g:Lcom/facebook/common/locale/Country;

    .line 1164579
    move-object v6, v6

    .line 1164580
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->b:Ljava/lang/String;

    .line 1164581
    iput-object v7, v6, LX/72i;->h:Ljava/lang/String;

    .line 1164582
    move-object v6, v6

    .line 1164583
    if-eqz v4, :cond_1

    .line 1164584
    const/4 v7, 0x1

    .line 1164585
    iput-boolean v7, v6, LX/72i;->k:Z

    .line 1164586
    :cond_1
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 1164587
    const-string p0, "shipping_address"

    invoke-virtual {v6}, LX/72i;->l()Lcom/facebook/payments/shipping/model/SimpleMailingAddress;

    move-result-object v6

    invoke-virtual {v7, p0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1164588
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1164589
    const-string p0, "extra_activity_result_data"

    invoke-virtual {v6, p0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1164590
    iget-object v7, v0, LX/72T;->a:LX/6qh;

    new-instance p0, LX/73T;

    sget-object p1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {p0, p1, v6}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v7, p0}, LX/6qh;->a(LX/73T;)V

    goto :goto_0
.end method
