.class public final LX/8c8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1373901
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373902
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1373903
    if-eqz v0, :cond_a

    .line 1373904
    const-string v1, "bootstrap_keywords"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373905
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373906
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1373907
    if-eqz v2, :cond_9

    .line 1373908
    const-string v3, "edges"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373909
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1373910
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_8

    .line 1373911
    invoke-virtual {p0, v2, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1373912
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373913
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1373914
    if-eqz v6, :cond_7

    .line 1373915
    const-string v7, "node"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373916
    const-wide/16 v10, 0x0

    .line 1373917
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373918
    const/4 v8, 0x0

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1373919
    if-eqz v8, :cond_0

    .line 1373920
    const-string v9, "keyword_bolded_subtext"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373921
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373922
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1373923
    if-eqz v8, :cond_1

    .line 1373924
    const-string v9, "keyword_bootstrap_type"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373925
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373926
    :cond_1
    const/4 v8, 0x2

    invoke-virtual {p0, v6, v8, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 1373927
    cmpl-double v10, v8, v10

    if-eqz v10, :cond_2

    .line 1373928
    const-string v10, "keyword_cost"

    invoke-virtual {p2, v10}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373929
    invoke-virtual {p2, v8, v9}, LX/0nX;->a(D)V

    .line 1373930
    :cond_2
    const/4 v8, 0x3

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1373931
    if-eqz v8, :cond_3

    .line 1373932
    const-string v9, "keyword_render_type"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373933
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373934
    :cond_3
    const/4 v8, 0x4

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1373935
    if-eqz v8, :cond_4

    .line 1373936
    const-string v9, "keyword_semantic"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373937
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373938
    :cond_4
    const/4 v8, 0x5

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1373939
    if-eqz v8, :cond_5

    .line 1373940
    const-string v9, "keyword_text"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373941
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373942
    :cond_5
    const/4 v8, 0x6

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1373943
    if-eqz v8, :cond_6

    .line 1373944
    const-string v9, "subtext"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373945
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373946
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373947
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373948
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1373949
    :cond_8
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1373950
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373951
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373952
    return-void
.end method
