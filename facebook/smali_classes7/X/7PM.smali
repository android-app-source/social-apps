.class public LX/7PM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1202777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202778
    return-void
.end method

.method public static a(LX/7I0;LX/0So;LX/19Z;LX/0oz;)LX/7PJ;
    .locals 2

    .prologue
    .line 1202779
    :try_start_0
    iget-object v0, p0, LX/7I0;->c:Ljava/lang/String;

    invoke-static {v0}, LX/7PL;->valueOf(Ljava/lang/String;)LX/7PL;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1202780
    :goto_0
    sget-object v1, LX/7PK;->a:[I

    invoke-virtual {v0}, LX/7PL;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1202781
    new-instance v0, LX/7Py;

    invoke-direct {v0}, LX/7Py;-><init>()V

    :goto_1
    return-object v0

    .line 1202782
    :catch_0
    sget-object v0, LX/7PL;->ZERO:LX/7PL;

    goto :goto_0

    .line 1202783
    :pswitch_0
    new-instance v0, LX/7PR;

    invoke-direct {v0, p0, p1}, LX/7PR;-><init>(LX/7I0;LX/0So;)V

    goto :goto_1

    .line 1202784
    :pswitch_1
    new-instance v0, LX/7PU;

    invoke-direct {v0, p1, p2, p0, p3}, LX/7PU;-><init>(LX/0So;LX/19Z;LX/7I0;LX/0oz;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/7PM;
    .locals 1

    .prologue
    .line 1202785
    new-instance v0, LX/7PM;

    invoke-direct {v0}, LX/7PM;-><init>()V

    .line 1202786
    move-object v0, v0

    .line 1202787
    return-object v0
.end method
