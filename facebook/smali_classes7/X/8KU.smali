.class public LX/8KU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public d:Landroid/content/Intent;

.field public e:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1330292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1330293
    iput-object p1, p0, LX/8KU;->a:Landroid/content/Context;

    .line 1330294
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1330281
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/8KU;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1330282
    iget-object v1, p0, LX/8KU;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1330283
    const-string v1, "upload_op"

    iget-object v2, p0, LX/8KU;->c:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1330284
    iget-object v1, p0, LX/8KU;->d:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 1330285
    const-string v1, "retry_intent"

    iget-object v2, p0, LX/8KU;->d:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1330286
    :cond_0
    iget-object v1, p0, LX/8KU;->e:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 1330287
    const-string v1, "eta"

    iget-object v2, p0, LX/8KU;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1330288
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "content://upload/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/8KU;->c:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330289
    iget-object p0, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, p0

    .line 1330290
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1330291
    return-object v0
.end method
