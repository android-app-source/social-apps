.class public LX/77a;
.super LX/2g7;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/77a;


# instance fields
.field private final a:LX/13O;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/13O;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171930
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171931
    iput-object p1, p0, LX/77a;->a:LX/13O;

    .line 1171932
    iput-object p2, p0, LX/77a;->b:LX/0SG;

    .line 1171933
    return-void
.end method

.method public static a(LX/0QB;)LX/77a;
    .locals 5

    .prologue
    .line 1171934
    sget-object v0, LX/77a;->c:LX/77a;

    if-nez v0, :cond_1

    .line 1171935
    const-class v1, LX/77a;

    monitor-enter v1

    .line 1171936
    :try_start_0
    sget-object v0, LX/77a;->c:LX/77a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171937
    if-eqz v2, :cond_0

    .line 1171938
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171939
    new-instance p0, LX/77a;

    invoke-static {v0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v3

    check-cast v3, LX/13O;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/77a;-><init>(LX/13O;LX/0SG;)V

    .line 1171940
    move-object v0, p0

    .line 1171941
    sput-object v0, LX/77a;->c:LX/77a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171942
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171943
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171944
    :cond_1
    sget-object v0, LX/77a;->c:LX/77a;

    return-object v0

    .line 1171945
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171946
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 8

    .prologue
    .line 1171947
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171948
    iget-object v0, p0, LX/77a;->a:LX/13O;

    sget-object v1, LX/77X;->MESSAGE_SENT:LX/77X;

    invoke-virtual {v1}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/13O;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 1171949
    iget-object v2, p0, LX/77a;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 1171950
    iget-object v4, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 1171951
    add-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
