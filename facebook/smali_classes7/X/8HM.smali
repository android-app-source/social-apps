.class public LX/8HM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320892
    return-void
.end method

.method public static a(LX/0QB;)LX/8HM;
    .locals 1

    .prologue
    .line 1320893
    new-instance v0, LX/8HM;

    invoke-direct {v0}, LX/8HM;-><init>()V

    .line 1320894
    move-object v0, v0

    .line 1320895
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1320896
    check-cast p1, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;

    const/4 v1, 0x0

    .line 1320897
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1320898
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v0, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1320899
    :goto_0
    iget-object v3, p1, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1320900
    const-string v3, "contributors[%d]"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1320901
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v5, p1, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->a:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320902
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1320903
    :cond_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "addContributors"

    .line 1320904
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1320905
    move-object v0, v0

    .line 1320906
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/contributors"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1320907
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1320908
    move-object v0, v0

    .line 1320909
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320910
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320911
    move-object v0, v0

    .line 1320912
    iput-object v2, v0, LX/14O;->g:Ljava/util/List;

    .line 1320913
    move-object v0, v0

    .line 1320914
    iget-object v1, p1, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->c:LX/8HP;

    sget-object v2, LX/8HP;->ADD_OPERATION:LX/8HP;

    if-ne v1, v2, :cond_1

    .line 1320915
    const-string v1, "POST"

    .line 1320916
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1320917
    :goto_1
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    .line 1320918
    return-object v0

    .line 1320919
    :cond_1
    const-string v1, "DELETE"

    .line 1320920
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1320921
    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1320922
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1320923
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
