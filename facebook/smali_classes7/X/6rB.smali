.class public final LX/6rB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/payments/selector/model/OptionSelectorRow;",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1151785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1151786
    check-cast p1, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    .line 1151787
    const-string v0, "Shipping"

    iget-object v1, p1, Lcom/facebook/payments/selector/model/OptionSelectorRow;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {v0, v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v0

    .line 1151788
    new-instance v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    iget-object v2, p1, Lcom/facebook/payments/selector/model/OptionSelectorRow;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/selector/model/OptionSelectorRow;->b:Ljava/lang/String;

    iget-boolean v4, p1, Lcom/facebook/payments/selector/model/OptionSelectorRow;->d:Z

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/0Px;)V

    return-object v1
.end method
