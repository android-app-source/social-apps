.class public final enum LX/7LL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7LL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7LL;

.field public static final enum CENTER_CROP:LX/7LL;

.field public static final enum CENTER_INSIDE:LX/7LL;

.field public static final enum NONE:LX/7LL;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1197763
    new-instance v0, LX/7LL;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/7LL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7LL;->NONE:LX/7LL;

    .line 1197764
    new-instance v0, LX/7LL;

    const-string v1, "CENTER_CROP"

    invoke-direct {v0, v1, v3}, LX/7LL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7LL;->CENTER_CROP:LX/7LL;

    .line 1197765
    new-instance v0, LX/7LL;

    const-string v1, "CENTER_INSIDE"

    invoke-direct {v0, v1, v4}, LX/7LL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7LL;->CENTER_INSIDE:LX/7LL;

    .line 1197766
    const/4 v0, 0x3

    new-array v0, v0, [LX/7LL;

    sget-object v1, LX/7LL;->NONE:LX/7LL;

    aput-object v1, v0, v2

    sget-object v1, LX/7LL;->CENTER_CROP:LX/7LL;

    aput-object v1, v0, v3

    sget-object v1, LX/7LL;->CENTER_INSIDE:LX/7LL;

    aput-object v1, v0, v4

    sput-object v0, LX/7LL;->$VALUES:[LX/7LL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1197760
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7LL;
    .locals 1

    .prologue
    .line 1197762
    const-class v0, LX/7LL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7LL;

    return-object v0
.end method

.method public static values()[LX/7LL;
    .locals 1

    .prologue
    .line 1197761
    sget-object v0, LX/7LL;->$VALUES:[LX/7LL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7LL;

    return-object v0
.end method
