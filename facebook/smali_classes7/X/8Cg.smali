.class public LX/8Cg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1311470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/4Gt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1311471
    sget-object v0, LX/8vw;->a:Ljava/util/Map;

    if-nez v0, :cond_4

    .line 1311472
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/8vw;->a:Ljava/util/Map;

    .line 1311473
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1311474
    if-nez v0, :cond_1

    .line 1311475
    sget-object v0, LX/8vw;->a:Ljava/util/Map;

    .line 1311476
    :goto_1
    move-object v0, v0

    .line 1311477
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1311478
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1311479
    new-instance v4, LX/4Gt;

    invoke-direct {v4}, LX/4Gt;-><init>()V

    .line 1311480
    const-string v5, "name"

    invoke-virtual {v4, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1311481
    move-object v4, v4

    .line 1311482
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1311483
    const-string v5, "value"

    invoke-virtual {v4, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1311484
    move-object v1, v4

    .line 1311485
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1311486
    :cond_0
    move-object v0, v2

    .line 1311487
    return-object v0

    .line 1311488
    :cond_1
    :try_start_0
    const/4 v2, 0x1

    .line 1311489
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1311490
    new-instance v1, LX/8vt;

    invoke-direct {v1}, LX/8vt;-><init>()V

    .line 1311491
    invoke-virtual {v1, v2, v2}, LX/8vt;->a(II)Landroid/opengl/EGLSurface;

    move-result-object v2

    .line 1311492
    invoke-virtual {v1, v2}, LX/8vt;->b(Landroid/opengl/EGLSurface;)V

    .line 1311493
    new-instance v0, LX/8vv;

    invoke-direct {v0, v1, v2}, LX/8vv;-><init>(LX/8vt;Landroid/opengl/EGLSurface;)V

    .line 1311494
    :goto_3
    move-object v0, v0

    .line 1311495
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1311496
    new-array v3, v1, [I

    .line 1311497
    const v4, 0x86a2

    invoke-static {v4, v3, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 1311498
    aget v4, v3, v2

    .line 1311499
    new-array v5, v4, [I

    .line 1311500
    const v3, 0x86a3

    invoke-static {v3, v5, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    move v3, v2

    .line 1311501
    :goto_4
    if-ge v3, v4, :cond_8

    .line 1311502
    aget v6, v5, v3

    const v7, 0x9278

    if-ne v6, v7, :cond_7

    .line 1311503
    :goto_5
    move v1, v1

    .line 1311504
    if-eqz v1, :cond_2

    .line 1311505
    sget-object v1, LX/8vw;->a:Ljava/util/Map;

    const-string v2, "etc2_compression"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1311506
    :cond_2
    const/16 v1, 0x1f03

    invoke-static {v1}, Landroid/opengl/GLES10;->glGetString(I)Ljava/lang/String;

    move-result-object v1

    .line 1311507
    const-string v2, "GL_IMG_texture_compression_pvrtc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    move v1, v1

    .line 1311508
    if-eqz v1, :cond_3

    .line 1311509
    sget-object v1, LX/8vw;->a:Ljava/util/Map;

    const-string v2, "pvr_compression"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1311510
    :cond_3
    if-eqz v0, :cond_4

    .line 1311511
    iget-object v1, v0, LX/8vv;->a:LX/8vt;

    invoke-virtual {v1}, LX/8vt;->b()V

    .line 1311512
    iget-object v1, v0, LX/8vv;->a:LX/8vt;

    iget-object v2, v0, LX/8vv;->b:Landroid/opengl/EGLSurface;

    .line 1311513
    iget-object v3, v1, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v3, v2}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1311514
    iget-object v1, v0, LX/8vv;->a:LX/8vt;

    invoke-virtual {v1}, LX/8vt;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1311515
    :cond_4
    :goto_6
    sget-object v0, LX/8vw;->a:Ljava/util/Map;

    goto/16 :goto_1

    .line 1311516
    :catch_0
    move-exception v0

    .line 1311517
    const-class v1, LX/8vw;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error while checking for capabilities"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    :try_start_1
    const/4 v0, 0x0

    goto :goto_3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 1311518
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    move v1, v2

    .line 1311519
    goto :goto_5
.end method
