.class public LX/7U2;
.super LX/7Ty;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1211289
    const-class v0, LX/7U2;

    sput-object v0, LX/7U2;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1FJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1211290
    invoke-static {p1}, LX/7U2;->a(LX/1FJ;)Lgifdrawable/pl/droidsonroids/gif/GifDrawable;

    move-result-object v0

    invoke-direct {p0, v0}, LX/7Ty;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 1211291
    invoke-virtual {p1}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/7U2;->c:LX/1FJ;

    .line 1211292
    return-void
.end method

.method private static a(LX/1FJ;)Lgifdrawable/pl/droidsonroids/gif/GifDrawable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)",
            "Lgifdrawable/pl/droidsonroids/gif/GifDrawable;"
        }
    .end annotation

    .prologue
    .line 1211293
    invoke-static {p0}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1211294
    invoke-virtual {p0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q8;

    .line 1211295
    new-instance v1, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;

    .line 1211296
    iget-object p0, v0, LX/1q8;->b:LX/52j;

    move-object v0, p0

    .line 1211297
    invoke-direct {v1, v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;-><init>(LX/52j;)V

    return-object v1
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 1211298
    invoke-virtual {p0}, LX/7Ty;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1211299
    :goto_0
    return-void

    .line 1211300
    :cond_0
    iget-object v0, p0, LX/7U2;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0
.end method

.method public final finalize()V
    .locals 5

    .prologue
    .line 1211301
    invoke-virtual {p0}, LX/7Ty;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211302
    :goto_0
    return-void

    .line 1211303
    :cond_0
    sget-object v0, LX/7U2;->a:Ljava/lang/Class;

    const-string v1, "finalize: CloseableGifDrawable %x still open. Underlying closeable ref = %x, GIF image = %x"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/7U2;->c:LX/1FJ;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/7U2;->c:LX/1FJ;

    invoke-virtual {v4}, LX/1FJ;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1211304
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, LX/7U2;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1211305
    invoke-virtual {p0}, LX/7U2;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1211306
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
