.class public final LX/8DX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;)V
    .locals 0

    .prologue
    .line 1312735
    iput-object p1, p0, LX/8DX;->a:Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1312736
    iget-object v0, p0, LX/8DX;->a:Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;

    .line 1312737
    :try_start_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 p0, 0x1

    .line 1312738
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1312739
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptedFlagComponent;

    invoke-direct {v3, v1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1312740
    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v4

    .line 1312741
    if-eq v4, p0, :cond_0

    .line 1312742
    invoke-virtual {v2, v3, p0, p0}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 1312743
    :cond_0
    const/4 p2, 0x1

    .line 1312744
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1312745
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 1312746
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 1312747
    new-instance p0, Landroid/content/ComponentName;

    const-class p1, Lcom/facebook/oxygen/preloads/sdk/firstparty/settings/TosAcceptedFlag;

    invoke-direct {p0, v1, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1312748
    invoke-virtual {v2, p0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result p1

    .line 1312749
    if-eq p1, p2, :cond_1

    .line 1312750
    invoke-virtual {v2, p0, p2, p2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 1312751
    new-instance p0, LX/1sd;

    invoke-direct {p0, v2}, LX/1sd;-><init>(Landroid/content/pm/PackageManager;)V

    .line 1312752
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, LX/1sd;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1312753
    invoke-static {v4, v3, p2}, LX/8DZ;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)V
    :try_end_0
    .catch LX/8DY; {:try_start_0 .. :try_end_0} :catch_0

    .line 1312754
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/29H;->c:LX/0Tn;

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1312755
    const-string v1, "tos_dialog_accepted"

    invoke-static {v0, v1}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->a(Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;Ljava/lang/String;)V

    .line 1312756
    return-void

    .line 1312757
    :catch_0
    move-exception v1

    .line 1312758
    iget-object v2, v0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->k:LX/03V;

    const-class v3, LX/8DW;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unable to push ToS accepted setting to appmanager"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
