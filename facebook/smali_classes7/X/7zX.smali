.class public LX/7zX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/31I;


# instance fields
.field private final a:LX/0kL;


# direct methods
.method public constructor <init>(LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1281072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1281073
    iput-object p1, p0, LX/7zX;->a:LX/0kL;

    .line 1281074
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;J)V
    .locals 6

    .prologue
    const/16 v4, 0x14

    const/4 v3, 0x0

    .line 1281075
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "LOGGED: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1281076
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1281077
    const-string v0, "ms"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1281078
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 1281079
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1281080
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    .line 1281081
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1281082
    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1281083
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1281084
    const-string v0, ": "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1281085
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1281086
    if-eqz v0, :cond_1

    .line 1281087
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1281088
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v4, :cond_2

    .line 1281089
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1281090
    :cond_1
    iget-object v0, p0, LX/7zX;->a:LX/0kL;

    new-instance v2, LX/27k;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1281091
    return-void

    .line 1281092
    :cond_2
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
