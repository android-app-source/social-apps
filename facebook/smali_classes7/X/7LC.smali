.class public final LX/7LC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2pw;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/FullScreenVideoPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 1

    .prologue
    .line 1196861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196862
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7LC;->a:Ljava/lang/ref/WeakReference;

    .line 1196863
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1196864
    iget-object v0, p0, LX/7LC;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/FullScreenVideoPlayer;

    .line 1196865
    if-eqz v0, :cond_0

    .line 1196866
    invoke-virtual {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getCurrentMediaTime()I

    move-result v0

    .line 1196867
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
