.class public final LX/7Fh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1187892
    const/4 v15, 0x0

    .line 1187893
    const/4 v14, 0x0

    .line 1187894
    const/4 v13, 0x0

    .line 1187895
    const/4 v12, 0x0

    .line 1187896
    const/4 v11, 0x0

    .line 1187897
    const/4 v10, 0x0

    .line 1187898
    const/4 v9, 0x0

    .line 1187899
    const/4 v8, 0x0

    .line 1187900
    const/4 v7, 0x0

    .line 1187901
    const/4 v6, 0x0

    .line 1187902
    const/4 v5, 0x0

    .line 1187903
    const/4 v4, 0x0

    .line 1187904
    const/4 v3, 0x0

    .line 1187905
    const/4 v2, 0x0

    .line 1187906
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1187907
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1187908
    const/4 v2, 0x0

    .line 1187909
    :goto_0
    return v2

    .line 1187910
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1187911
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_b

    .line 1187912
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1187913
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1187914
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1187915
    const-string v17, "branch_default_page_index"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1187916
    const/4 v5, 0x1

    .line 1187917
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto :goto_1

    .line 1187918
    :cond_2
    const-string v17, "branch_question_id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1187919
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 1187920
    :cond_3
    const-string v17, "branch_response_maps"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1187921
    invoke-static/range {p0 .. p1}, LX/7Ff;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1187922
    :cond_4
    const-string v17, "branch_subquestion_index_int"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1187923
    const/4 v4, 0x1

    .line 1187924
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 1187925
    :cond_5
    const-string v17, "composite_control_node"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1187926
    invoke-static/range {p0 .. p1}, LX/7Fl;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1187927
    :cond_6
    const-string v17, "composite_page_nodes"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1187928
    invoke-static/range {p0 .. p1}, LX/7Fl;->b(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1187929
    :cond_7
    const-string v17, "direct_next_page_index_int"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1187930
    const/4 v3, 0x1

    .line 1187931
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 1187932
    :cond_8
    const-string v17, "node_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1187933
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1187934
    :cond_9
    const-string v17, "qe_next_page_index"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1187935
    const/4 v2, 0x1

    .line 1187936
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto/16 :goto_1

    .line 1187937
    :cond_a
    const-string v17, "random_next_page_indices"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1187938
    invoke-static/range {p0 .. p1}, LX/2gu;->b(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1187939
    :cond_b
    const/16 v16, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1187940
    if-eqz v5, :cond_c

    .line 1187941
    const/4 v5, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v15, v1}, LX/186;->a(III)V

    .line 1187942
    :cond_c
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 1187943
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 1187944
    if-eqz v4, :cond_d

    .line 1187945
    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v5}, LX/186;->a(III)V

    .line 1187946
    :cond_d
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1187947
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1187948
    if-eqz v3, :cond_e

    .line 1187949
    const/4 v3, 0x6

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1187950
    :cond_e
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1187951
    if-eqz v2, :cond_f

    .line 1187952
    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v3}, LX/186;->a(III)V

    .line 1187953
    :cond_f
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1187954
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v3, 0x9

    const/4 v2, 0x0

    .line 1187955
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1187956
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1187957
    if-eqz v0, :cond_0

    .line 1187958
    const-string v1, "branch_default_page_index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187959
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1187960
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1187961
    if-eqz v0, :cond_1

    .line 1187962
    const-string v1, "branch_question_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187963
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1187964
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187965
    if-eqz v0, :cond_2

    .line 1187966
    const-string v1, "branch_response_maps"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187967
    invoke-static {p0, v0, p2, p3}, LX/7Ff;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1187968
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1187969
    if-eqz v0, :cond_3

    .line 1187970
    const-string v1, "branch_subquestion_index_int"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187971
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1187972
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187973
    if-eqz v0, :cond_4

    .line 1187974
    const-string v1, "composite_control_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187975
    invoke-static {p0, v0, p2, p3}, LX/7Fl;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1187976
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187977
    if-eqz v0, :cond_6

    .line 1187978
    const-string v1, "composite_page_nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187979
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1187980
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 1187981
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/7Fl;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1187982
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1187983
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1187984
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1187985
    if-eqz v0, :cond_7

    .line 1187986
    const-string v1, "direct_next_page_index_int"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187987
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1187988
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1187989
    if-eqz v0, :cond_8

    .line 1187990
    const-string v1, "node_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187991
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1187992
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1187993
    if-eqz v0, :cond_9

    .line 1187994
    const-string v1, "qe_next_page_index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187995
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1187996
    :cond_9
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1187997
    if-eqz v0, :cond_a

    .line 1187998
    const-string v0, "random_next_page_indices"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187999
    invoke-virtual {p0, p1, v3}, LX/15i;->e(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->c(Ljava/util/Iterator;LX/0nX;)V

    .line 1188000
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1188001
    return-void
.end method
