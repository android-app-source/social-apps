.class public abstract LX/76y;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/0WV;

.field private final b:LX/14z;


# direct methods
.method public constructor <init>(LX/0WV;LX/14z;)V
    .locals 0

    .prologue
    .line 1171559
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171560
    iput-object p1, p0, LX/76y;->a:LX/0WV;

    .line 1171561
    iput-object p2, p0, LX/76y;->b:LX/14z;

    .line 1171562
    return-void
.end method


# virtual methods
.method public abstract a(I)Z
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 3
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1171563
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171564
    iget-object v0, p0, LX/76y;->a:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v0

    .line 1171565
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    .line 1171566
    invoke-static {v0, v1}, LX/14z;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, LX/76y;->a(I)Z

    move-result v0

    return v0
.end method
