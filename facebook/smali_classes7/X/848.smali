.class public final LX/848;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:J

.field public final synthetic c:LX/2na;

.field public final synthetic d:LX/2hA;

.field public final synthetic e:LX/3UJ;


# direct methods
.method public constructor <init>(LX/3UJ;JJLX/2na;LX/2hA;)V
    .locals 0

    .prologue
    .line 1290804
    iput-object p1, p0, LX/848;->e:LX/3UJ;

    iput-wide p2, p0, LX/848;->a:J

    iput-wide p4, p0, LX/848;->b:J

    iput-object p6, p0, LX/848;->c:LX/2na;

    iput-object p7, p0, LX/848;->d:LX/2hA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1290805
    iget-object v0, p0, LX/848;->e:LX/3UJ;

    iget-object v1, v0, LX/3UJ;->a:LX/2dj;

    iget-wide v2, p0, LX/848;->a:J

    iget-wide v4, p0, LX/848;->b:J

    iget-object v6, p0, LX/848;->c:LX/2na;

    iget-object v7, p0, LX/848;->d:LX/2hA;

    invoke-virtual/range {v1 .. v7}, LX/2dj;->a(JJLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
