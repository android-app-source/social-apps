.class public LX/8Mu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Z
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Z
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Z
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Z

.field public final r:Z

.field public final s:J

.field public final t:I

.field public final u:LX/5Rn;

.field public final v:J

.field public final w:Z

.field public final x:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZJILX/5Rn;JZZZ)V
    .locals 3
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZJI",
            "LX/5Rn;",
            "JZZZ)V"
        }
    .end annotation

    .prologue
    .line 1335411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335412
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1335413
    iput-object p1, p0, LX/8Mu;->a:Ljava/lang/String;

    .line 1335414
    iput-object p4, p0, LX/8Mu;->d:Ljava/lang/String;

    .line 1335415
    iput-object p5, p0, LX/8Mu;->e:Ljava/lang/String;

    .line 1335416
    iput-object p7, p0, LX/8Mu;->f:Ljava/lang/String;

    .line 1335417
    iput-object p8, p0, LX/8Mu;->g:Ljava/lang/String;

    .line 1335418
    invoke-static {p9}, LX/8Mu;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/8Mu;->h:Ljava/lang/String;

    .line 1335419
    if-eqz p6, :cond_2

    .line 1335420
    iput-object p3, p0, LX/8Mu;->b:Ljava/lang/String;

    .line 1335421
    iput-object p2, p0, LX/8Mu;->c:Ljava/lang/String;

    .line 1335422
    :goto_1
    iput-object p10, p0, LX/8Mu;->i:Ljava/lang/String;

    .line 1335423
    sget-object v2, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    if-eq p11, v2, :cond_3

    .line 1335424
    iput-object p11, p0, LX/8Mu;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1335425
    :goto_2
    iput-object p12, p0, LX/8Mu;->k:Ljava/lang/String;

    .line 1335426
    move/from16 v0, p13

    iput-boolean v0, p0, LX/8Mu;->q:Z

    .line 1335427
    move-wide/from16 v0, p19

    iput-wide v0, p0, LX/8Mu;->s:J

    .line 1335428
    move/from16 v0, p21

    iput v0, p0, LX/8Mu;->t:I

    .line 1335429
    move-object/from16 v0, p22

    iput-object v0, p0, LX/8Mu;->u:LX/5Rn;

    .line 1335430
    move-wide/from16 v0, p23

    iput-wide v0, p0, LX/8Mu;->v:J

    .line 1335431
    move-object/from16 v0, p14

    iput-object v0, p0, LX/8Mu;->l:Ljava/lang/String;

    .line 1335432
    move-object/from16 v0, p15

    iput-object v0, p0, LX/8Mu;->m:Ljava/lang/String;

    .line 1335433
    move/from16 v0, p16

    iput-boolean v0, p0, LX/8Mu;->n:Z

    .line 1335434
    move/from16 v0, p17

    iput-boolean v0, p0, LX/8Mu;->o:Z

    .line 1335435
    move/from16 v0, p18

    iput-boolean v0, p0, LX/8Mu;->p:Z

    .line 1335436
    move/from16 v0, p25

    iput-boolean v0, p0, LX/8Mu;->r:Z

    .line 1335437
    move/from16 v0, p26

    iput-boolean v0, p0, LX/8Mu;->w:Z

    .line 1335438
    move/from16 v0, p27

    iput-boolean v0, p0, LX/8Mu;->x:Z

    .line 1335439
    return-void

    .line 1335440
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1335441
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, LX/8Mu;->b:Ljava/lang/String;

    .line 1335442
    iput-object p2, p0, LX/8Mu;->c:Ljava/lang/String;

    goto :goto_1

    .line 1335443
    :cond_3
    const/4 v2, 0x0

    iput-object v2, p0, LX/8Mu;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;ZZZLX/5Rn;JZZZ)LX/8Mu;
    .locals 29

    .prologue
    .line 1335444
    new-instance v1, LX/8Mu;

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v12, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v15, p7

    move-object/from16 v16, p8

    move/from16 v17, p9

    move/from16 v18, p10

    move/from16 v19, p11

    move-wide/from16 v20, p4

    move/from16 v22, p6

    move-object/from16 v23, p12

    move-wide/from16 v24, p13

    move/from16 v26, p15

    move/from16 v27, p16

    move/from16 v28, p17

    invoke-direct/range {v1 .. v28}, LX/8Mu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZJILX/5Rn;JZZZ)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZJIZZZZ)LX/8Mu;
    .locals 29
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZJIZZZZ)",
            "LX/8Mu;"
        }
    .end annotation

    .prologue
    .line 1335445
    new-instance v1, LX/8Mu;

    sget-object v23, LX/5Rn;->NORMAL:LX/5Rn;

    const-wide/16 v24, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p21

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    move/from16 v17, p14

    move/from16 v18, p15

    move/from16 v19, p16

    move-wide/from16 v20, p17

    move/from16 v22, p19

    move/from16 v26, p20

    move/from16 v27, p22

    move/from16 v28, p23

    invoke-direct/range {v1 .. v28}, LX/8Mu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZJILX/5Rn;JZZZ)V

    return-object v1
.end method

.method private static a(Ljava/util/Set;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1335446
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1335447
    :cond_0
    const/4 v0, 0x0

    .line 1335448
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2c

    invoke-static {v1}, LX/0PO;->on(C)LX/0PO;

    move-result-object v1

    invoke-virtual {v1}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
