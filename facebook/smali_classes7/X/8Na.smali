.class public LX/8Na;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1337484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/8NQ;)Lcom/facebook/media/upload/MediaAttachementBody;
    .locals 5

    .prologue
    .line 1337485
    const/4 v0, 0x0

    .line 1337486
    iget-object v1, p0, LX/8NQ;->x:Ljava/lang/String;

    move-object v1, v1

    .line 1337487
    if-eqz v1, :cond_0

    .line 1337488
    iget-object v1, p0, LX/8NQ;->x:Ljava/lang/String;

    move-object v1, v1

    .line 1337489
    const-string v2, "moments_video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1337490
    iget-object v1, p0, LX/8NQ;->A:Landroid/os/Bundle;

    move-object v1, v1

    .line 1337491
    if-eqz v1, :cond_0

    .line 1337492
    const-string v2, "thumbnail_bitmap"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 1337493
    if-eqz v1, :cond_0

    .line 1337494
    new-instance v0, Lcom/facebook/media/upload/MediaAttachementBody;

    const-string v2, "thumb"

    const-string v3, "image/png"

    const-string v4, "thumbnail.jpg"

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/facebook/media/upload/MediaAttachementBody;-><init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V

    .line 1337495
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1337496
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 1337497
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1337498
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 1337499
    :cond_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Pz;LX/14O;LX/8NQ;LX/0SG;LX/0lB;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "LX/14O;",
            "LX/8NQ;",
            "LX/0SG;",
            "LX/0lB;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const-wide/16 v4, 0x0

    .line 1337500
    iget-object v0, p2, LX/8NQ;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1337501
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1337502
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "description"

    .line 1337503
    iget-object v2, p2, LX/8NQ;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1337504
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337505
    :cond_0
    iget-wide v7, p2, LX/8NQ;->e:J

    move-wide v0, v7

    .line 1337506
    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 1337507
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "place"

    .line 1337508
    iget-wide v7, p2, LX/8NQ;->e:J

    move-wide v2, v7

    .line 1337509
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337510
    :cond_1
    iget-object v0, p2, LX/8NQ;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1337511
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1337512
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "text_only_place"

    .line 1337513
    iget-object v2, p2, LX/8NQ;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1337514
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337515
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "checkin_entry_point"

    .line 1337516
    iget-boolean v2, p2, LX/8NQ;->g:Z

    move v2, v2

    .line 1337517
    invoke-static {v2}, LX/5RB;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337518
    iget-object v0, p2, LX/8NQ;->h:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object v0, v0

    .line 1337519
    iget-object v1, v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1337520
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy"

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337521
    :cond_3
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "audience_exp"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337522
    iget-object v0, p2, LX/8NQ;->j:LX/0Px;

    move-object v0, v0

    .line 1337523
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1337524
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "tags"

    .line 1337525
    iget-object v2, p2, LX/8NQ;->j:LX/0Px;

    move-object v2, v2

    .line 1337526
    invoke-static {v2}, LX/8Na;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337527
    :cond_4
    iget-wide v7, p2, LX/8NQ;->n:J

    move-wide v0, v7

    .line 1337528
    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 1337529
    invoke-interface {p3}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 1337530
    iget-wide v7, p2, LX/8NQ;->n:J

    move-wide v2, v7

    .line 1337531
    sub-long/2addr v0, v2

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1337532
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "time_since_original_post"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337533
    :cond_5
    iget-object v0, p2, LX/8NQ;->k:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object v0, v0

    .line 1337534
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1337535
    iget-object v0, p2, LX/8NQ;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1337536
    if-eqz v0, :cond_6

    .line 1337537
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "referenced_sticker_id"

    .line 1337538
    iget-object v2, p2, LX/8NQ;->l:Ljava/lang/String;

    move-object v2, v2

    .line 1337539
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337540
    :cond_6
    iget-object v0, p2, LX/8NQ;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1337541
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1337542
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "composer_session_id"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337543
    :cond_7
    iget-object v0, p2, LX/8NQ;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1337544
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1337545
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "source_type"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337546
    :cond_8
    iget-object v0, p2, LX/8NQ;->v:Ljava/lang/String;

    move-object v0, v0

    .line 1337547
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1337548
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "creator_product"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337549
    :cond_9
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_looping"

    .line 1337550
    iget-boolean v2, p2, LX/8NQ;->w:Z

    move v2, v2

    .line 1337551
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337552
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "target"

    .line 1337553
    iget-wide v7, p2, LX/8NQ;->i:J

    move-wide v2, v7

    .line 1337554
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337555
    iget-object v0, p2, LX/8NQ;->D:Ljava/lang/String;

    move-object v0, v0

    .line 1337556
    if-eqz v0, :cond_a

    .line 1337557
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "sponsor_id"

    .line 1337558
    iget-object v2, p2, LX/8NQ;->D:Ljava/lang/String;

    move-object v2, v2

    .line 1337559
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337560
    :cond_a
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "published"

    .line 1337561
    iget-boolean v2, p2, LX/8NQ;->o:Z

    move v2, v2

    .line 1337562
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337563
    iget-wide v7, p2, LX/8NQ;->p:J

    move-wide v0, v7

    .line 1337564
    cmp-long v0, v0, v4

    if-lez v0, :cond_b

    .line 1337565
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "scheduled_publish_time"

    .line 1337566
    iget-wide v7, p2, LX/8NQ;->p:J

    move-wide v2, v7

    .line 1337567
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337568
    :cond_b
    iget-object v0, p2, LX/8NQ;->r:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v0, v0

    .line 1337569
    if-eqz v0, :cond_c

    .line 1337570
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "proxied_app_id"

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337571
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "proxied_app_name"

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337572
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "android_key_hash"

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337573
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "user_selected_tags"

    .line 1337574
    iget-boolean v2, p2, LX/8NQ;->s:Z

    move v2, v2

    .line 1337575
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337576
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "user_selected_place"

    .line 1337577
    iget-boolean v2, p2, LX/8NQ;->t:Z

    move v2, v2

    .line 1337578
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337579
    :cond_c
    iget-object v0, p2, LX/8NQ;->q:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-object v0, v0

    .line 1337580
    if-eqz v0, :cond_d

    .line 1337581
    iget-object v0, p2, LX/8NQ;->q:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-object v0, v0

    .line 1337582
    invoke-virtual {p4, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1337583
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "product_item"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337584
    :cond_d
    iget-object v0, p2, LX/8NQ;->x:Ljava/lang/String;

    move-object v0, v0

    .line 1337585
    if-eqz v0, :cond_e

    .line 1337586
    iget-object v0, p2, LX/8NQ;->x:Ljava/lang/String;

    move-object v0, v0

    .line 1337587
    const-string v1, "moments_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1337588
    iget-object v0, p2, LX/8NQ;->E:Ljava/lang/String;

    move-object v0, v0

    .line 1337589
    const-string v1, "Moments video uuid must be set before uploading!"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337590
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "custom_type"

    const-string v2, "moments_video"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337591
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "custom_properties[moments_video_uuid]"

    .line 1337592
    iget-object v2, p2, LX/8NQ;->E:Ljava/lang/String;

    move-object v2, v2

    .line 1337593
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337594
    :cond_e
    :goto_0
    iget-object v0, p2, LX/8NQ;->F:Ljava/lang/String;

    move-object v0, v0

    .line 1337595
    if-eqz v0, :cond_f

    .line 1337596
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "container_type"

    .line 1337597
    iget-object v2, p2, LX/8NQ;->F:Ljava/lang/String;

    move-object v2, v2

    .line 1337598
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337599
    :cond_f
    iget-object v0, p2, LX/8NQ;->G:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v0, v0

    .line 1337600
    if-eqz v0, :cond_10

    .line 1337601
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "prompt_id"

    .line 1337602
    iget-object v4, p2, LX/8NQ;->G:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v4, v4

    .line 1337603
    iget-object v4, v4, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "prompt_type"

    .line 1337604
    iget-object v4, p2, LX/8NQ;->G:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v4, v4

    .line 1337605
    iget-object v4, v4, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "prompt_tracking_string"

    .line 1337606
    iget-object v4, p2, LX/8NQ;->G:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v4, v4

    .line 1337607
    iget-object v4, v4, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1337608
    :cond_10
    iget-object v0, p2, LX/8NQ;->H:LX/0Px;

    move-object v0, v0

    .line 1337609
    if-eqz v0, :cond_11

    .line 1337610
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "inspiration_prompts"

    .line 1337611
    iget-object v2, p2, LX/8NQ;->H:LX/0Px;

    move-object v2, v2

    .line 1337612
    invoke-virtual {p4, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337613
    :cond_11
    iget-boolean v0, p2, LX/8NQ;->I:Z

    move v0, v0

    .line 1337614
    if-eqz v0, :cond_12

    .line 1337615
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "post_surfaces_blacklist"

    sget-object v2, LX/7mB;->a:LX/0Px;

    invoke-static {v2}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v2

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337616
    :cond_12
    iget-object v0, p2, LX/8NQ;->L:Ljava/lang/String;

    move-object v0, v0

    .line 1337617
    if-eqz v0, :cond_13

    .line 1337618
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "storyline_mood_id"

    .line 1337619
    iget-object v2, p2, LX/8NQ;->L:Ljava/lang/String;

    move-object v2, v2

    .line 1337620
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337621
    :cond_13
    return-void

    .line 1337622
    :cond_14
    iget-object v0, p2, LX/8NQ;->x:Ljava/lang/String;

    move-object v0, v0

    .line 1337623
    const-string v1, "live_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1337624
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "custom_type"

    .line 1337625
    iget-object v2, p2, LX/8NQ;->x:Ljava/lang/String;

    move-object v2, v2

    .line 1337626
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337627
    iget-object v0, p2, LX/8NQ;->J:Ljava/lang/String;

    move-object v0, v0

    .line 1337628
    const-string v1, "Live video broadcast id must be set before uploading!"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337629
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1337630
    const-string v1, "live_broadcast_id"

    .line 1337631
    iget-object v2, p2, LX/8NQ;->J:Ljava/lang/String;

    move-object v2, v2

    .line 1337632
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1337633
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "custom_properties"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1337634
    :cond_15
    iget-object v0, p2, LX/8NQ;->x:Ljava/lang/String;

    move-object v0, v0

    .line 1337635
    const-string v1, "profile_intro_card"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1337636
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "custom_type"

    .line 1337637
    iget-object v2, p2, LX/8NQ;->x:Ljava/lang/String;

    move-object v2, v2

    .line 1337638
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1337639
    :cond_16
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "custom_type"

    .line 1337640
    iget-object v2, p2, LX/8NQ;->x:Ljava/lang/String;

    move-object v2, v2

    .line 1337641
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337642
    iget-object v0, p2, LX/8NQ;->A:Landroid/os/Bundle;

    move-object v0, v0

    .line 1337643
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1337644
    iget-object v2, p2, LX/8NQ;->y:Ljava/lang/String;

    move-object v2, v2

    .line 1337645
    if-eqz v2, :cond_17

    .line 1337646
    const-string v2, "profile_photo_method"

    .line 1337647
    iget-object v3, p2, LX/8NQ;->y:Ljava/lang/String;

    move-object v3, v3

    .line 1337648
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1337649
    :cond_17
    if-eqz v0, :cond_18

    .line 1337650
    const-string v2, "frame_offset"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1337651
    if-eq v0, v6, :cond_18

    .line 1337652
    const-string v2, "frame_offset"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1337653
    :cond_18
    const-string v0, "expiration_time"

    .line 1337654
    iget-wide v7, p2, LX/8NQ;->z:J

    move-wide v2, v7

    .line 1337655
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1337656
    iget-object v0, p2, LX/8NQ;->B:Ljava/lang/String;

    move-object v0, v0

    .line 1337657
    if-eqz v0, :cond_19

    .line 1337658
    const-string v0, "image_overlay_id"

    .line 1337659
    iget-object v2, p2, LX/8NQ;->B:Ljava/lang/String;

    move-object v2, v2

    .line 1337660
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1337661
    :cond_19
    iget-object v0, p2, LX/8NQ;->C:Ljava/lang/String;

    move-object v0, v0

    .line 1337662
    if-eqz v0, :cond_1a

    .line 1337663
    const-string v0, "msqrd_mask_id"

    .line 1337664
    iget-object v2, p2, LX/8NQ;->C:Ljava/lang/String;

    move-object v2, v2

    .line 1337665
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1337666
    :cond_1a
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "custom_properties"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337667
    invoke-static {p2}, LX/8Na;->a(LX/8NQ;)Lcom/facebook/media/upload/MediaAttachementBody;

    move-result-object v0

    .line 1337668
    if-eqz v0, :cond_e

    .line 1337669
    invoke-virtual {v0}, Lcom/facebook/media/upload/MediaAttachementBody;->a()LX/4cQ;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1337670
    iput-object v0, p1, LX/14O;->l:Ljava/util/List;

    .line 1337671
    goto/16 :goto_0
.end method
