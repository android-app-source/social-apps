.class public final LX/74f;
.super LX/0Rc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rc",
        "<",
        "Lcom/facebook/ipc/media/MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/74h;

.field private final b:Landroid/database/Cursor;

.field private c:Z


# direct methods
.method public constructor <init>(LX/74h;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 1168197
    iput-object p1, p0, LX/74f;->a:LX/74h;

    invoke-direct {p0}, LX/0Rc;-><init>()V

    .line 1168198
    iput-object p2, p0, LX/74f;->b:Landroid/database/Cursor;

    .line 1168199
    iget-object v0, p0, LX/74f;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    iput-boolean v0, p0, LX/74f;->c:Z

    .line 1168200
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1168201
    iget-boolean v0, p0, LX/74f;->c:Z

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168202
    iget-boolean v0, p0, LX/74f;->c:Z

    if-nez v0, :cond_0

    .line 1168203
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "MediaItemCursorIterator"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1168204
    :cond_0
    iget-object v0, p0, LX/74f;->a:LX/74h;

    iget-object v1, p0, LX/74f;->b:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, LX/74h;->b(Landroid/database/Cursor;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1168205
    iget-object v1, p0, LX/74f;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    iput-boolean v1, p0, LX/74f;->c:Z

    .line 1168206
    return-object v0
.end method
