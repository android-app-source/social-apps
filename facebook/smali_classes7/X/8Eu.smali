.class public final LX/8Eu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1316217
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1316218
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316219
    :goto_0
    return v1

    .line 1316220
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316221
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1316222
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1316223
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1316224
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1316225
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1316226
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1316227
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1316228
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1316229
    :cond_4
    const-string v5, "specific_flow_config"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1316230
    const/4 v4, 0x0

    .line 1316231
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_c

    .line 1316232
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316233
    :goto_2
    move v0, v4

    .line 1316234
    goto :goto_1

    .line 1316235
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1316236
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1316237
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1316238
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1316239
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1316240
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316241
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_b

    .line 1316242
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1316243
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1316244
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_8

    if-eqz v6, :cond_8

    .line 1316245
    const-string v7, "__type__"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    const-string v7, "__typename"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1316246
    :cond_9
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_3

    .line 1316247
    :cond_a
    const-string v7, "data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1316248
    const/4 v6, 0x0

    .line 1316249
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v7, :cond_10

    .line 1316250
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316251
    :goto_4
    move v0, v6

    .line 1316252
    goto :goto_3

    .line 1316253
    :cond_b
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1316254
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1316255
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1316256
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_c
    move v0, v4

    move v5, v4

    goto :goto_3

    .line 1316257
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316258
    :cond_e
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_f

    .line 1316259
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1316260
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1316261
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_e

    if-eqz v7, :cond_e

    .line 1316262
    const-string v8, "form_name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 1316263
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 1316264
    :cond_f
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1316265
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1316266
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_4

    :cond_10
    move v0, v6

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1316267
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1316268
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1316269
    if-eqz v0, :cond_0

    .line 1316270
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316271
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1316272
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1316273
    if-eqz v0, :cond_1

    .line 1316274
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316275
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1316276
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1316277
    if-eqz v0, :cond_5

    .line 1316278
    const-string v1, "specific_flow_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316279
    const/4 p1, 0x0

    .line 1316280
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1316281
    invoke-virtual {p0, v0, p1}, LX/15i;->g(II)I

    move-result v1

    .line 1316282
    if-eqz v1, :cond_2

    .line 1316283
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316284
    invoke-static {p0, v0, p1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1316285
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1316286
    if-eqz v1, :cond_4

    .line 1316287
    const-string p1, "data"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316288
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1316289
    const/4 p1, 0x0

    invoke-virtual {p0, v1, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 1316290
    if-eqz p1, :cond_3

    .line 1316291
    const-string v0, "form_name"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316292
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1316293
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1316294
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1316295
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1316296
    return-void
.end method
