.class public final LX/8UW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8St;

.field public final synthetic b:LX/8UX;


# direct methods
.method public constructor <init>(LX/8UX;LX/8St;)V
    .locals 0

    .prologue
    .line 1350899
    iput-object p1, p0, LX/8UW;->b:LX/8UX;

    iput-object p2, p0, LX/8UW;->a:LX/8St;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1350900
    iget-object v0, p0, LX/8UW;->a:LX/8St;

    invoke-virtual {v0, p1}, LX/8St;->a(Ljava/lang/Throwable;)V

    .line 1350901
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1350902
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1350903
    if-nez p1, :cond_0

    .line 1350904
    iget-object v0, p0, LX/8UW;->a:LX/8St;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Empty result"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/8St;->a(Ljava/lang/Throwable;)V

    .line 1350905
    :goto_0
    return-void

    .line 1350906
    :cond_0
    iget-object v1, p0, LX/8UW;->a:LX/8St;

    .line 1350907
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1350908
    check-cast v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;

    invoke-virtual {v1, v0}, LX/8St;->a(Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;)V

    goto :goto_0
.end method
