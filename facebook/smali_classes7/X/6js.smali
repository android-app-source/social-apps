.class public LX/6js;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final link:Ljava/lang/String;

.field public final messageMetadata:LX/6kn;

.field public final mode:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1133357
    new-instance v0, LX/1sv;

    const-string v1, "DeltaJoinableMode"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6js;->b:LX/1sv;

    .line 1133358
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6js;->c:LX/1sw;

    .line 1133359
    new-instance v0, LX/1sw;

    const-string v1, "link"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6js;->d:LX/1sw;

    .line 1133360
    new-instance v0, LX/1sw;

    const-string v1, "mode"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6js;->e:LX/1sw;

    .line 1133361
    sput-boolean v4, LX/6js;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1133362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133363
    iput-object p1, p0, LX/6js;->messageMetadata:LX/6kn;

    .line 1133364
    iput-object p2, p0, LX/6js;->link:Ljava/lang/String;

    .line 1133365
    iput-object p3, p0, LX/6js;->mode:Ljava/lang/Integer;

    .line 1133366
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1133352
    iget-object v0, p0, LX/6js;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1133353
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6js;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133354
    :cond_0
    iget-object v0, p0, LX/6js;->mode:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6ki;->a:LX/1sn;

    iget-object v1, p0, LX/6js;->mode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1133355
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'mode\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6js;->mode:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1133356
    :cond_1
    return-void
.end method

.method public static b(LX/1su;)LX/6js;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1133334
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    move-object v2, v0

    .line 1133335
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v3

    .line 1133336
    iget-byte v4, v3, LX/1sw;->b:B

    if-eqz v4, :cond_3

    .line 1133337
    iget-short v4, v3, LX/1sw;->c:S

    packed-switch v4, :pswitch_data_0

    .line 1133338
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1133339
    :pswitch_0
    iget-byte v4, v3, LX/1sw;->b:B

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 1133340
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v2

    goto :goto_0

    .line 1133341
    :cond_0
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1133342
    :pswitch_1
    iget-byte v4, v3, LX/1sw;->b:B

    const/16 v5, 0xb

    if-ne v4, v5, :cond_1

    .line 1133343
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1133344
    :cond_1
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1133345
    :pswitch_2
    iget-byte v4, v3, LX/1sw;->b:B

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 1133346
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1133347
    :cond_2
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1133348
    :cond_3
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1133349
    new-instance v3, LX/6js;

    invoke-direct {v3, v2, v1, v0}, LX/6js;-><init>(LX/6kn;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1133350
    invoke-direct {v3}, LX/6js;->a()V

    .line 1133351
    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1133367
    if-eqz p2, :cond_2

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1133368
    :goto_0
    if-eqz p2, :cond_3

    const-string v0, "\n"

    move-object v1, v0

    .line 1133369
    :goto_1
    if-eqz p2, :cond_4

    const-string v0, " "

    .line 1133370
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaJoinableMode"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133371
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133372
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133373
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133374
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133375
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133376
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133377
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133378
    iget-object v4, p0, LX/6js;->messageMetadata:LX/6kn;

    if-nez v4, :cond_5

    .line 1133379
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133380
    :goto_3
    iget-object v4, p0, LX/6js;->link:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1133381
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133382
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133383
    const-string v4, "link"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133384
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133385
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133386
    iget-object v4, p0, LX/6js;->link:Ljava/lang/String;

    if-nez v4, :cond_6

    .line 1133387
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133388
    :cond_0
    :goto_4
    iget-object v4, p0, LX/6js;->mode:Ljava/lang/Integer;

    if-eqz v4, :cond_1

    .line 1133389
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133390
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133391
    const-string v4, "mode"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133392
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133393
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133394
    iget-object v0, p0, LX/6js;->mode:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1133395
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133396
    :cond_1
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133397
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133398
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133399
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1133400
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1133401
    :cond_4
    const-string v0, ""

    goto/16 :goto_2

    .line 1133402
    :cond_5
    iget-object v4, p0, LX/6js;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1133403
    :cond_6
    iget-object v4, p0, LX/6js;->link:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1133404
    :cond_7
    sget-object v0, LX/6ki;->b:Ljava/util/Map;

    iget-object v4, p0, LX/6js;->mode:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1133405
    if-eqz v0, :cond_8

    .line 1133406
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133407
    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133408
    :cond_8
    iget-object v4, p0, LX/6js;->mode:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1133409
    if-eqz v0, :cond_1

    .line 1133410
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1133318
    invoke-direct {p0}, LX/6js;->a()V

    .line 1133319
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133320
    iget-object v0, p0, LX/6js;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1133321
    sget-object v0, LX/6js;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133322
    iget-object v0, p0, LX/6js;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1133323
    :cond_0
    iget-object v0, p0, LX/6js;->link:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1133324
    iget-object v0, p0, LX/6js;->link:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1133325
    sget-object v0, LX/6js;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133326
    iget-object v0, p0, LX/6js;->link:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1133327
    :cond_1
    iget-object v0, p0, LX/6js;->mode:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1133328
    iget-object v0, p0, LX/6js;->mode:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1133329
    sget-object v0, LX/6js;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133330
    iget-object v0, p0, LX/6js;->mode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1133331
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133332
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133333
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133289
    if-nez p1, :cond_1

    .line 1133290
    :cond_0
    :goto_0
    return v0

    .line 1133291
    :cond_1
    instance-of v1, p1, LX/6js;

    if-eqz v1, :cond_0

    .line 1133292
    check-cast p1, LX/6js;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133293
    if-nez p1, :cond_3

    .line 1133294
    :cond_2
    :goto_1
    move v0, v2

    .line 1133295
    goto :goto_0

    .line 1133296
    :cond_3
    iget-object v0, p0, LX/6js;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133297
    :goto_2
    iget-object v3, p1, LX/6js;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133298
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133299
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133300
    iget-object v0, p0, LX/6js;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6js;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133301
    :cond_5
    iget-object v0, p0, LX/6js;->link:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1133302
    :goto_4
    iget-object v3, p1, LX/6js;->link:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1133303
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133304
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133305
    iget-object v0, p0, LX/6js;->link:Ljava/lang/String;

    iget-object v3, p1, LX/6js;->link:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133306
    :cond_7
    iget-object v0, p0, LX/6js;->mode:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1133307
    :goto_6
    iget-object v3, p1, LX/6js;->mode:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1133308
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1133309
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133310
    iget-object v0, p0, LX/6js;->mode:Ljava/lang/Integer;

    iget-object v3, p1, LX/6js;->mode:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1133311
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1133312
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1133313
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1133314
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1133315
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1133316
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1133317
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133285
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133286
    sget-boolean v0, LX/6js;->a:Z

    .line 1133287
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6js;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133288
    return-object v0
.end method
