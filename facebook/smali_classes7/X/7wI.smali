.class public LX/7wI;
.super LX/1OM;
.source ""

# interfaces
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/7wN;",
        ">;",
        "LX/1OO",
        "<",
        "LX/7wN;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/11T;

.field public final b:Landroid/content/res/Resources;

.field public c:Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;Landroid/content/Context;LX/11T;)V
    .locals 1
    .param p1    # Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275653
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1275654
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1275655
    iput-object v0, p0, LX/7wI;->d:LX/0Px;

    .line 1275656
    iput-object p1, p0, LX/7wI;->c:Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

    .line 1275657
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/7wI;->b:Landroid/content/res/Resources;

    .line 1275658
    iput-object p3, p0, LX/7wI;->a:LX/11T;

    .line 1275659
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1275651
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030527

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1275652
    new-instance v1, LX/7wN;

    invoke-direct {v1, v0}, LX/7wN;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 1275660
    check-cast p1, LX/7wN;

    .line 1275661
    invoke-virtual {p0, p2}, LX/7wI;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;

    .line 1275662
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->l()I

    move-result v1

    .line 1275663
    iget-object v2, p0, LX/7wI;->b:Landroid/content/res/Resources;

    const v3, 0x7f0f00ea

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1275664
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->j()J

    move-result-wide v2

    .line 1275665
    iget-object v4, p0, LX/7wI;->a:LX/11T;

    invoke-virtual {v4}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v2

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 1275666
    iget-object v5, p0, LX/7wI;->b:Landroid/content/res/Resources;

    const v6, 0x7f081fc5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 1275667
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1275668
    new-instance v3, LX/7wH;

    invoke-direct {v3, p0, v0}, LX/7wH;-><init>(LX/7wI;Ljava/lang/String;)V

    move-object v0, v3

    .line 1275669
    iget-object v3, p1, LX/7wN;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1275670
    iget-object v3, p1, LX/7wN;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1275671
    iget-object v3, p1, LX/7wN;->n:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3, v0}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1275672
    return-void
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1275650
    iget-object v0, p0, LX/7wI;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 1275649
    const/4 v0, 0x1

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1275648
    iget-object v0, p0, LX/7wI;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
