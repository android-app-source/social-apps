.class public final enum LX/8bb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8bb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8bb;

.field public static final enum UNDO_ARCHIVE:LX/8bb;

.field public static final enum UNDO_UNARCHIVE:LX/8bb;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1372190
    new-instance v0, LX/8bb;

    const-string v1, "UNDO_ARCHIVE"

    invoke-direct {v0, v1, v2}, LX/8bb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8bb;->UNDO_ARCHIVE:LX/8bb;

    .line 1372191
    new-instance v0, LX/8bb;

    const-string v1, "UNDO_UNARCHIVE"

    invoke-direct {v0, v1, v3}, LX/8bb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8bb;->UNDO_UNARCHIVE:LX/8bb;

    .line 1372192
    const/4 v0, 0x2

    new-array v0, v0, [LX/8bb;

    sget-object v1, LX/8bb;->UNDO_ARCHIVE:LX/8bb;

    aput-object v1, v0, v2

    sget-object v1, LX/8bb;->UNDO_UNARCHIVE:LX/8bb;

    aput-object v1, v0, v3

    sput-object v0, LX/8bb;->$VALUES:[LX/8bb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1372193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8bb;
    .locals 1

    .prologue
    .line 1372194
    const-class v0, LX/8bb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8bb;

    return-object v0
.end method

.method public static values()[LX/8bb;
    .locals 1

    .prologue
    .line 1372195
    sget-object v0, LX/8bb;->$VALUES:[LX/8bb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8bb;

    return-object v0
.end method
