.class public final LX/8CC;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/doodle/CaptionEditorView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/doodle/CaptionEditorView;)V
    .locals 0

    .prologue
    .line 1310368
    iput-object p1, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/messaging/doodle/CaptionEditorView;B)V
    .locals 0

    .prologue
    .line 1310369
    invoke-direct {p0, p1}, LX/8CC;-><init>(Lcom/facebook/messaging/doodle/CaptionEditorView;)V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 1310370
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_2

    .line 1310371
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v1, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v1, v1, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    .line 1310372
    const/4 v2, 0x0

    .line 1310373
    const/4 v3, 0x0

    .line 1310374
    if-nez v2, :cond_0

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1310375
    :cond_0
    if-nez v3, :cond_1

    const/4 v4, 0x2

    new-array v3, v4, [I

    .line 1310376
    :cond_1
    invoke-virtual {v1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1310377
    invoke-virtual {v1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1310378
    const/4 v4, 0x0

    aget v4, v3, v4

    const/4 p0, 0x1

    aget p0, v3, p0

    invoke-virtual {v2, v4, p0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1310379
    move-object v3, v2

    .line 1310380
    move-object v2, v3

    .line 1310381
    move-object v2, v2

    .line 1310382
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    move v2, v3

    .line 1310383
    move v1, v2

    .line 1310384
    iput-boolean v1, v0, Lcom/facebook/messaging/doodle/CaptionEditorView;->d:Z

    .line 1310385
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    .line 1310386
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-boolean v0, v0, Lcom/facebook/messaging/doodle/CaptionEditorView;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v0, v0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1310387
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v0, v0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1310388
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v1, v1

    sub-float/2addr v1, p4

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1310389
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const/4 v2, 0x0

    iget-object v3, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v3}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getHeight()I

    move-result v3

    iget-object v4, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v4, v4, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v4}, LX/8CB;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v4}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getPaddingBottom()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, LX/0yq;->a(III)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1310390
    iget-object v1, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v1, v1, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v1, v0}, LX/8CB;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1310391
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->g()V

    .line 1310392
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1310393
    iget-object v2, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v2, v2, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v2}, LX/8CB;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1310394
    iget-object v2, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-boolean v2, v2, Lcom/facebook/messaging/doodle/CaptionEditorView;->d:Z

    if-eqz v2, :cond_0

    .line 1310395
    :goto_0
    return v0

    .line 1310396
    :cond_0
    iget-object v2, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v2, v2, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v2}, LX/8CB;->isFocused()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1310397
    iget-object v2, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v2, v2, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v2}, LX/8CB;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 1310398
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1310399
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1310400
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v0, v0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const-string v2, ""

    invoke-virtual {v0, v2}, LX/8CB;->setText(Ljava/lang/CharSequence;)V

    .line 1310401
    :cond_1
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v2, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v2, v2, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/doodle/CaptionEditorView;->removeView(Landroid/view/View;)V

    .line 1310402
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v3}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v4, v4, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v4}, LX/8CB;->getLineHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1310403
    invoke-static {v0, v2}, Lcom/facebook/messaging/doodle/CaptionEditorView;->a$redex0(Lcom/facebook/messaging/doodle/CaptionEditorView;F)V

    .line 1310404
    move v0, v1

    .line 1310405
    goto :goto_0

    .line 1310406
    :cond_2
    iget-object v1, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v1, v1, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v1}, LX/8CB;->requestFocus()Z

    .line 1310407
    iget-object v1, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v1, v1, Lcom/facebook/messaging/doodle/CaptionEditorView;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v2, v2, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    goto :goto_0

    .line 1310408
    :cond_3
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->g()V

    :goto_1
    move v0, v1

    .line 1310409
    goto :goto_0

    .line 1310410
    :cond_4
    iget-object v0, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v3}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, LX/8CC;->a:Lcom/facebook/messaging/doodle/CaptionEditorView;

    iget-object v4, v4, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v4}, LX/8CB;->getLineHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1310411
    invoke-static {v0, v2}, Lcom/facebook/messaging/doodle/CaptionEditorView;->a$redex0(Lcom/facebook/messaging/doodle/CaptionEditorView;F)V

    .line 1310412
    goto :goto_1
.end method
