.class public LX/794;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/78q;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1173764
    const-class v0, LX/794;

    sput-object v0, LX/794;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1173765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1173766
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 13

    .prologue
    .line 1173767
    check-cast p1, LX/78q;

    .line 1173768
    invoke-virtual {p1}, LX/78q;->a()Ljava/util/List;

    move-result-object v9

    .line 1173769
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json-strings"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1173770
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v10

    .line 1173771
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 1173772
    iget-object v0, p1, LX/78q;->e:Ljava/util/Set;

    move-object v0, v0

    .line 1173773
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    .line 1173774
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v0, Ljava/net/URI;

    .line 1173775
    iget-object v2, v8, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1173776
    invoke-direct {v0, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1173777
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1173778
    sget-object v0, LX/794;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ignoring invalid debug attachment"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173779
    const-string v0, "Attachment file not found, skipping: "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1173780
    iget-object v1, v8, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1173781
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1173782
    :catch_0
    move-exception v0

    .line 1173783
    sget-object v1, LX/794;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Ignoring invalid debug attachment"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1173784
    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1173785
    :cond_0
    :try_start_1
    new-instance v0, LX/4cu;

    .line 1173786
    iget-object v2, v8, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1173787
    iget-object v3, v8, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1173788
    const-wide/16 v4, 0x0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, LX/4cu;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 1173789
    new-instance v1, LX/4cQ;

    .line 1173790
    iget-object v2, v8, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1173791
    invoke-direct {v1, v2, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1173792
    :cond_1
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1173793
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1173794
    new-instance v1, LX/4cq;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "text/plain"

    const-string v3, "missing_attachment_report.txt"

    invoke-direct {v1, v0, v2, v3}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 1173795
    new-instance v0, LX/4cQ;

    const-string v2, "missing_attachment_report.txt"

    invoke-direct {v0, v2, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1173796
    :cond_2
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "fbbugreportuploader"

    .line 1173797
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1173798
    move-object v0, v0

    .line 1173799
    const-string v1, "POST"

    .line 1173800
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1173801
    move-object v0, v0

    .line 1173802
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1173803
    iget-object v2, p1, LX/78q;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1173804
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/bugs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1173805
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1173806
    move-object v0, v0

    .line 1173807
    iput-object v9, v0, LX/14O;->g:Ljava/util/List;

    .line 1173808
    move-object v0, v0

    .line 1173809
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1173810
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1173811
    move-object v0, v0

    .line 1173812
    iput-object v10, v0, LX/14O;->l:Ljava/util/List;

    .line 1173813
    move-object v0, v0

    .line 1173814
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1173815
    const/4 v0, 0x1

    .line 1173816
    iget v1, p2, LX/1pN;->b:I

    move v1, v1

    .line 1173817
    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    .line 1173818
    const/4 v0, 0x0

    .line 1173819
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
