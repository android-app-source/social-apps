.class public LX/87P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private b:LX/0tS;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0tS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300044
    iput-object p1, p0, LX/87P;->a:Landroid/content/Context;

    .line 1300045
    iput-object p2, p0, LX/87P;->b:LX/0tS;

    .line 1300046
    return-void
.end method

.method public static b(LX/0QB;)LX/87P;
    .locals 3

    .prologue
    .line 1300047
    new-instance v2, LX/87P;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v1

    check-cast v1, LX/0tS;

    invoke-direct {v2, v0, v1}, LX/87P;-><init>(Landroid/content/Context;LX/0tS;)V

    .line 1300048
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1300033
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300034
    iget-object v0, p0, LX/87P;->b:LX/0tS;

    invoke-virtual {v0}, LX/0tS;->d()I

    move-result v1

    .line 1300035
    iget-object v0, p0, LX/87P;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1300036
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1300037
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v0

    .line 1300038
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1300039
    :goto_0
    new-instance v2, Landroid/graphics/Rect;

    sub-int v4, v1, v0

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-direct {v2, v5, v4, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v2

    .line 1300040
    :goto_1
    return-object v0

    .line 1300041
    :cond_0
    int-to-float v2, v3

    div-float v0, v2, v0

    float-to-int v0, v0

    goto :goto_0

    .line 1300042
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_1
.end method
