.class public final enum LX/8Dp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Dp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Dp;

.field public static final enum FETCH_FEED_FOLLOWUP_UNIT:LX/8Dp;

.field public static final enum FETCH_PAGES_HEADER:LX/8Dp;

.field public static final enum FETCH_PAGES_HEADER_NOCACHE:LX/8Dp;

.field public static final enum FETCH_PAGES_MANAGER_MORE_TAB_DATA:LX/8Dp;

.field public static final enum FETCH_PAGE_IDENTITY_ACTIVITY_DATA:LX/8Dp;

.field public static final enum FETCH_PAGE_IDENTITY_ADMIN_DATA:LX/8Dp;

.field public static final enum FETCH_PAGE_IDENTITY_SECONDARY_DATA_BATCHING:LX/8Dp;

.field public static final enum FETCH_PAGE_INFORMATION_DATA:LX/8Dp;

.field public static final enum FETCH_PAGE_POSTS_BY_OTHERS_FRAGMENT_DATA:LX/8Dp;

.field public static final enum JOIN_EVENT:LX/8Dp;

.field public static final enum LIKE:LX/8Dp;

.field public static final enum PAGE_TIMELINE_DELETE_POST:LX/8Dp;

.field public static final enum PAGE_TIMELINE_EDIT_POST:LX/8Dp;

.field public static final enum PAGE_TIMELINE_TOGGLE_NOTIFY_ME:LX/8Dp;

.field public static final enum REFRESH_PAGE_CARDS:LX/8Dp;

.field public static final enum REPORT_PLACE:LX/8Dp;

.field public static final enum SAVE:LX/8Dp;

.field public static final enum TOGGLE_TV_AIRING_REMINDER:LX/8Dp;

.field public static final enum UNSAVE:LX/8Dp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1313218
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGES_HEADER"

    invoke-direct {v0, v1, v3}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGES_HEADER:LX/8Dp;

    .line 1313219
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGES_HEADER_NOCACHE"

    invoke-direct {v0, v1, v4}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGES_HEADER_NOCACHE:LX/8Dp;

    .line 1313220
    new-instance v0, LX/8Dp;

    const-string v1, "REFRESH_PAGE_CARDS"

    invoke-direct {v0, v1, v5}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->REFRESH_PAGE_CARDS:LX/8Dp;

    .line 1313221
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGE_IDENTITY_SECONDARY_DATA_BATCHING"

    invoke-direct {v0, v1, v6}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGE_IDENTITY_SECONDARY_DATA_BATCHING:LX/8Dp;

    .line 1313222
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGE_IDENTITY_ADMIN_DATA"

    invoke-direct {v0, v1, v7}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGE_IDENTITY_ADMIN_DATA:LX/8Dp;

    .line 1313223
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGE_IDENTITY_ACTIVITY_DATA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGE_IDENTITY_ACTIVITY_DATA:LX/8Dp;

    .line 1313224
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGES_MANAGER_MORE_TAB_DATA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGES_MANAGER_MORE_TAB_DATA:LX/8Dp;

    .line 1313225
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGE_INFORMATION_DATA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGE_INFORMATION_DATA:LX/8Dp;

    .line 1313226
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_PAGE_POSTS_BY_OTHERS_FRAGMENT_DATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_PAGE_POSTS_BY_OTHERS_FRAGMENT_DATA:LX/8Dp;

    .line 1313227
    new-instance v0, LX/8Dp;

    const-string v1, "TOGGLE_TV_AIRING_REMINDER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->TOGGLE_TV_AIRING_REMINDER:LX/8Dp;

    .line 1313228
    new-instance v0, LX/8Dp;

    const-string v1, "REPORT_PLACE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->REPORT_PLACE:LX/8Dp;

    .line 1313229
    new-instance v0, LX/8Dp;

    const-string v1, "JOIN_EVENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->JOIN_EVENT:LX/8Dp;

    .line 1313230
    new-instance v0, LX/8Dp;

    const-string v1, "LIKE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->LIKE:LX/8Dp;

    .line 1313231
    new-instance v0, LX/8Dp;

    const-string v1, "UNSAVE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->UNSAVE:LX/8Dp;

    .line 1313232
    new-instance v0, LX/8Dp;

    const-string v1, "SAVE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->SAVE:LX/8Dp;

    .line 1313233
    new-instance v0, LX/8Dp;

    const-string v1, "FETCH_FEED_FOLLOWUP_UNIT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->FETCH_FEED_FOLLOWUP_UNIT:LX/8Dp;

    .line 1313234
    new-instance v0, LX/8Dp;

    const-string v1, "PAGE_TIMELINE_EDIT_POST"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->PAGE_TIMELINE_EDIT_POST:LX/8Dp;

    .line 1313235
    new-instance v0, LX/8Dp;

    const-string v1, "PAGE_TIMELINE_DELETE_POST"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->PAGE_TIMELINE_DELETE_POST:LX/8Dp;

    .line 1313236
    new-instance v0, LX/8Dp;

    const-string v1, "PAGE_TIMELINE_TOGGLE_NOTIFY_ME"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/8Dp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Dp;->PAGE_TIMELINE_TOGGLE_NOTIFY_ME:LX/8Dp;

    .line 1313237
    const/16 v0, 0x13

    new-array v0, v0, [LX/8Dp;

    sget-object v1, LX/8Dp;->FETCH_PAGES_HEADER:LX/8Dp;

    aput-object v1, v0, v3

    sget-object v1, LX/8Dp;->FETCH_PAGES_HEADER_NOCACHE:LX/8Dp;

    aput-object v1, v0, v4

    sget-object v1, LX/8Dp;->REFRESH_PAGE_CARDS:LX/8Dp;

    aput-object v1, v0, v5

    sget-object v1, LX/8Dp;->FETCH_PAGE_IDENTITY_SECONDARY_DATA_BATCHING:LX/8Dp;

    aput-object v1, v0, v6

    sget-object v1, LX/8Dp;->FETCH_PAGE_IDENTITY_ADMIN_DATA:LX/8Dp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8Dp;->FETCH_PAGE_IDENTITY_ACTIVITY_DATA:LX/8Dp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8Dp;->FETCH_PAGES_MANAGER_MORE_TAB_DATA:LX/8Dp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8Dp;->FETCH_PAGE_INFORMATION_DATA:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8Dp;->FETCH_PAGE_POSTS_BY_OTHERS_FRAGMENT_DATA:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8Dp;->TOGGLE_TV_AIRING_REMINDER:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8Dp;->REPORT_PLACE:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8Dp;->JOIN_EVENT:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8Dp;->LIKE:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8Dp;->UNSAVE:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8Dp;->SAVE:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8Dp;->FETCH_FEED_FOLLOWUP_UNIT:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8Dp;->PAGE_TIMELINE_EDIT_POST:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8Dp;->PAGE_TIMELINE_DELETE_POST:LX/8Dp;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/8Dp;->PAGE_TIMELINE_TOGGLE_NOTIFY_ME:LX/8Dp;

    aput-object v2, v0, v1

    sput-object v0, LX/8Dp;->$VALUES:[LX/8Dp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1313238
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Dp;
    .locals 1

    .prologue
    .line 1313239
    const-class v0, LX/8Dp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Dp;

    return-object v0
.end method

.method public static values()[LX/8Dp;
    .locals 1

    .prologue
    .line 1313240
    sget-object v0, LX/8Dp;->$VALUES:[LX/8Dp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Dp;

    return-object v0
.end method
