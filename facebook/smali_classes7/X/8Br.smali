.class public LX/8Br;
.super LX/6LI;
.source ""


# instance fields
.field public final a:LX/2MA;

.field public final b:LX/0Xl;

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:LX/0Xl;

.field private final e:Landroid/view/LayoutInflater;

.field public final f:LX/6LJ;

.field public final g:Landroid/content/Context;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public final i:LX/2Fa;

.field private final j:LX/8Bz;

.field private k:LX/8Bx;

.field public l:LX/0Yb;

.field public m:LX/0Yb;

.field private n:Z

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2M5;",
            ">;"
        }
    .end annotation
.end field

.field private p:Z


# direct methods
.method public constructor <init>(LX/2MA;LX/0Xl;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;Landroid/view/LayoutInflater;LX/6LJ;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/2Fa;LX/0Or;LX/8Bz;LX/8Bx;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p12    # LX/8Bx;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2MA;",
            "LX/0Xl;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Landroid/view/LayoutInflater;",
            "LX/6LJ;",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2Fa;",
            "LX/0Or",
            "<",
            "LX/2M5;",
            ">;",
            "LX/8Bz;",
            "LX/8Bx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309809
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 1309810
    iput-object p1, p0, LX/8Br;->a:LX/2MA;

    .line 1309811
    iput-object p2, p0, LX/8Br;->b:LX/0Xl;

    .line 1309812
    iput-object p3, p0, LX/8Br;->d:LX/0Xl;

    .line 1309813
    iput-object p5, p0, LX/8Br;->e:Landroid/view/LayoutInflater;

    .line 1309814
    iput-object p4, p0, LX/8Br;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1309815
    iput-object p6, p0, LX/8Br;->f:LX/6LJ;

    .line 1309816
    iput-object p7, p0, LX/8Br;->g:Landroid/content/Context;

    .line 1309817
    iput-object p8, p0, LX/8Br;->h:Lcom/facebook/content/SecureContextHelper;

    .line 1309818
    iput-object p9, p0, LX/8Br;->i:LX/2Fa;

    .line 1309819
    iput-object p10, p0, LX/8Br;->o:LX/0Or;

    .line 1309820
    iput-object p12, p0, LX/8Br;->k:LX/8Bx;

    .line 1309821
    iput-object p11, p0, LX/8Br;->j:LX/8Bz;

    .line 1309822
    return-void
.end method

.method public static f(LX/8Br;)Z
    .locals 2

    .prologue
    .line 1309964
    iget-boolean v0, p0, LX/8Br;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Br;->a:LX/2MA;

    invoke-interface {v0}, LX/2MA;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8Br;->a:LX/2MA;

    invoke-interface {v0}, LX/2MA;->a()LX/2MB;

    move-result-object v0

    sget-object v1, LX/2MB;->NO_INTERNET:LX/2MB;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/8Br;)V
    .locals 3

    .prologue
    .line 1309960
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 1309961
    invoke-virtual {v0, p0}, LX/6LN;->b(LX/6LI;)V

    .line 1309962
    iget-object v0, p0, LX/8Br;->j:LX/8Bz;

    iget-object v1, p0, LX/8Br;->k:LX/8Bx;

    sget-object v2, LX/8By;->HIDDEN:LX/8By;

    invoke-virtual {v0, v1, v2}, LX/8Bz;->a(LX/8Bx;LX/8By;)V

    .line 1309963
    return-void
.end method

.method public static i(LX/8Br;)V
    .locals 5

    .prologue
    .line 1309953
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 1309954
    invoke-virtual {v0, p0}, LX/6LN;->a(LX/6LI;)V

    .line 1309955
    iget-object v0, p0, LX/8Br;->a:LX/2MA;

    .line 1309956
    invoke-interface {v0}, LX/2MA;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/8Br;->l(LX/8Br;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1309957
    new-instance v1, Lcom/facebook/messaging/connectivity/ConnectionStatusNotification$4;

    invoke-direct {v1, p0, v0}, Lcom/facebook/messaging/connectivity/ConnectionStatusNotification$4;-><init>(LX/8Br;LX/2MA;)V

    .line 1309958
    iget-object v0, p0, LX/8Br;->c:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0xbb8

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 1309959
    :cond_0
    return-void
.end method

.method public static l(LX/8Br;)Z
    .locals 2

    .prologue
    .line 1309952
    invoke-direct {p0}, LX/8Br;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8Br;->a:LX/2MA;

    invoke-interface {v0}, LX/2MA;->a()LX/2MB;

    move-result-object v0

    sget-object v1, LX/2MB;->CONNECTED_CAPTIVE_PORTAL:LX/2MB;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 1309965
    iget-boolean v0, p0, LX/8Br;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Br;->a:LX/2MA;

    invoke-interface {v0}, LX/2MA;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(LX/8Br;)V
    .locals 2

    .prologue
    .line 1309945
    iget-object v0, p0, LX/8Br;->a:LX/2MA;

    .line 1309946
    invoke-interface {v0}, LX/2MA;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/2MA;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/8Br;->l(LX/8Br;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1309947
    if-eqz v0, :cond_1

    .line 1309948
    invoke-static {p0}, LX/8Br;->f(LX/8Br;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1309949
    invoke-static {p0}, LX/8Br;->i(LX/8Br;)V

    .line 1309950
    :goto_1
    return-void

    .line 1309951
    :cond_1
    invoke-static {p0}, LX/8Br;->h(LX/8Br;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1309844
    iget-object v0, p0, LX/8Br;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f03018f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/banner/BasicBannerNotificationView;

    .line 1309845
    iget-object v2, p0, LX/8Br;->a:LX/2MA;

    .line 1309846
    iget-object v1, p0, LX/8Br;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2M5;

    .line 1309847
    sget-object v3, LX/8Bq;->a:[I

    invoke-interface {v2}, LX/2MA;->a()LX/2MB;

    move-result-object v4

    invoke-virtual {v4}, LX/2MB;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1309848
    invoke-direct {p0}, LX/8Br;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1309849
    invoke-virtual {v1}, LX/2M5;->b()LX/6LR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309850
    sget-object v1, LX/8By;->AIRPLANE_MODE:LX/8By;

    .line 1309851
    :goto_0
    iget-object v2, p0, LX/8Br;->j:LX/8Bz;

    iget-object v3, p0, LX/8Br;->k:LX/8Bx;

    invoke-virtual {v2, v3, v1}, LX/8Bz;->a(LX/8Bx;LX/8By;)V

    .line 1309852
    return-object v0

    .line 1309853
    :pswitch_0
    invoke-interface {v2}, LX/2MA;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1309854
    invoke-virtual {v1}, LX/2M5;->b()LX/6LR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309855
    sget-object v1, LX/8By;->AIRPLANE_MODE:LX/8By;

    goto :goto_0

    .line 1309856
    :cond_0
    iget-object v2, v1, LX/2M5;->a:LX/0ad;

    sget-char v3, LX/2M8;->h:C

    iget-object v4, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const p1, 0x7f08003f

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1309857
    sget-char v3, LX/2M8;->d:C

    const v4, 0x7f0a0247

    invoke-static {v1, v3, v4}, LX/2M5;->a(LX/2M5;CI)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1309858
    iget-object v4, v1, LX/2M5;->d:LX/2M6;

    .line 1309859
    iput-object v2, v4, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 1309860
    move-object v2, v4

    .line 1309861
    iput-object v3, v2, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 1309862
    move-object v2, v2

    .line 1309863
    sget-object v3, LX/2M7;->ALWAYS:LX/2M7;

    .line 1309864
    iput-object v3, v2, LX/2M6;->h:LX/2M7;

    .line 1309865
    move-object v2, v2

    .line 1309866
    iget-object v3, v1, LX/2M5;->a:LX/0ad;

    sget-short v4, LX/2M8;->g:S

    const/4 p1, 0x0

    invoke-interface {v3, v4, p1}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1309867
    :goto_1
    invoke-virtual {v2}, LX/2M6;->a()LX/6LR;

    move-result-object v2

    move-object v2, v2

    .line 1309868
    invoke-virtual {v0, v2}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309869
    sget-object v1, LX/8By;->NO_INTERNET:LX/8By;

    .line 1309870
    new-instance v3, LX/8Bl;

    invoke-direct {v3, p0, v2}, LX/8Bl;-><init>(LX/8Br;LX/6LR;)V

    .line 1309871
    iput-object v3, v0, Lcom/facebook/common/banner/BasicBannerNotificationView;->a:LX/6LQ;

    .line 1309872
    goto :goto_0

    .line 1309873
    :pswitch_1
    invoke-interface {v2}, LX/2MA;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1309874
    invoke-virtual {v1}, LX/2M5;->b()LX/6LR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309875
    sget-object v1, LX/8By;->AIRPLANE_MODE:LX/8By;

    goto :goto_0

    .line 1309876
    :cond_1
    iget-object v2, v1, LX/2M5;->a:LX/0ad;

    sget-char v3, LX/2M8;->j:C

    iget-object v4, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const p1, 0x7f080584

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1309877
    iget-object v3, v1, LX/2M5;->d:LX/2M6;

    .line 1309878
    iput-object v2, v3, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 1309879
    move-object v2, v3

    .line 1309880
    iget-object v3, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const v4, 0x7f0a0248

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1309881
    iput-object v3, v2, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 1309882
    move-object v2, v2

    .line 1309883
    sget-object v3, LX/2M7;->ALWAYS:LX/2M7;

    .line 1309884
    iput-object v3, v2, LX/2M6;->h:LX/2M7;

    .line 1309885
    move-object v2, v2

    .line 1309886
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    move-result-object v2

    invoke-virtual {v2}, LX/2M6;->a()LX/6LR;

    move-result-object v2

    move-object v1, v2

    .line 1309887
    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309888
    sget-object v1, LX/8By;->WAITING_TO_CONNECT:LX/8By;

    goto/16 :goto_0

    .line 1309889
    :pswitch_2
    invoke-direct {p0}, LX/8Br;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1309890
    invoke-virtual {v1}, LX/2M5;->b()LX/6LR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309891
    sget-object v1, LX/8By;->AIRPLANE_MODE:LX/8By;

    goto/16 :goto_0

    .line 1309892
    :cond_2
    iget-object v2, v1, LX/2M5;->a:LX/0ad;

    sget-char v3, LX/2M8;->f:C

    iget-object v4, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const p1, 0x7f08006c

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1309893
    iget-object v3, v1, LX/2M5;->d:LX/2M6;

    .line 1309894
    iput-object v2, v3, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 1309895
    move-object v2, v3

    .line 1309896
    iget-object v3, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const v4, 0x7f0a0249

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1309897
    iput-object v3, v2, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 1309898
    move-object v2, v2

    .line 1309899
    sget-object v3, LX/2M7;->ALWAYS:LX/2M7;

    .line 1309900
    iput-object v3, v2, LX/2M6;->h:LX/2M7;

    .line 1309901
    move-object v2, v2

    .line 1309902
    const/4 v3, 0x1

    .line 1309903
    iput-boolean v3, v2, LX/2M6;->b:Z

    .line 1309904
    move-object v2, v2

    .line 1309905
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    move-result-object v2

    invoke-virtual {v2}, LX/2M6;->a()LX/6LR;

    move-result-object v2

    move-object v1, v2

    .line 1309906
    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309907
    sget-object v1, LX/8By;->CONNECTING:LX/8By;

    goto/16 :goto_0

    .line 1309908
    :pswitch_3
    iget-object v2, v1, LX/2M5;->a:LX/0ad;

    sget-char v3, LX/2M8;->b:C

    iget-object v4, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const p1, 0x7f080586

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1309909
    iget-object v3, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const v4, 0x7f080587

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1309910
    iget-object v4, v1, LX/2M5;->d:LX/2M6;

    .line 1309911
    iput-object v2, v4, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 1309912
    move-object v2, v4

    .line 1309913
    iget-object v4, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const p1, 0x7f0a024b

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1309914
    iput-object v4, v2, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 1309915
    move-object v2, v2

    .line 1309916
    sget-object v4, LX/2M7;->ALWAYS:LX/2M7;

    .line 1309917
    iput-object v4, v2, LX/2M6;->h:LX/2M7;

    .line 1309918
    move-object v2, v2

    .line 1309919
    const/4 v4, 0x0

    .line 1309920
    iput-boolean v4, v2, LX/2M6;->b:Z

    .line 1309921
    move-object v2, v2

    .line 1309922
    invoke-virtual {v2, v3}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    move-result-object v2

    invoke-virtual {v2}, LX/2M6;->a()LX/6LR;

    move-result-object v2

    move-object v1, v2

    .line 1309923
    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309924
    new-instance v1, LX/8Bm;

    invoke-direct {v1, p0}, LX/8Bm;-><init>(LX/8Br;)V

    .line 1309925
    iput-object v1, v0, Lcom/facebook/common/banner/BasicBannerNotificationView;->a:LX/6LQ;

    .line 1309926
    sget-object v1, LX/8By;->CAPTIVE_PORTAL:LX/8By;

    goto/16 :goto_0

    .line 1309927
    :cond_3
    iget-object v2, v1, LX/2M5;->a:LX/0ad;

    sget-char v3, LX/2M8;->e:C

    iget-object v4, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const p1, 0x7f08006d

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1309928
    iget-object v3, v1, LX/2M5;->d:LX/2M6;

    .line 1309929
    iput-object v2, v3, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 1309930
    move-object v2, v3

    .line 1309931
    iget-object v3, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const v4, 0x7f0a024a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1309932
    iput-object v3, v2, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 1309933
    move-object v2, v2

    .line 1309934
    sget-object v3, LX/2M7;->ALWAYS:LX/2M7;

    .line 1309935
    iput-object v3, v2, LX/2M6;->h:LX/2M7;

    .line 1309936
    move-object v2, v2

    .line 1309937
    const/4 v3, 0x0

    .line 1309938
    iput-boolean v3, v2, LX/2M6;->b:Z

    .line 1309939
    move-object v2, v2

    .line 1309940
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    move-result-object v2

    invoke-virtual {v2}, LX/2M6;->a()LX/6LR;

    move-result-object v2

    move-object v1, v2

    .line 1309941
    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1309942
    sget-object v1, LX/8By;->CONNECTED:LX/8By;

    goto/16 :goto_0

    .line 1309943
    :cond_4
    iget-object v3, v1, LX/2M5;->c:Landroid/content/res/Resources;

    const v4, 0x7f08058a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1309944
    invoke-virtual {v2, v3}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1309832
    iget-object v0, p0, LX/8Br;->l:LX/0Yb;

    if-nez v0, :cond_0

    .line 1309833
    iget-object v0, p0, LX/8Br;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.CONNECTIVITY_CHANGED"

    new-instance v2, LX/8Bo;

    invoke-direct {v2, p0}, LX/8Bo;-><init>(LX/8Br;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/8Br;->l:LX/0Yb;

    .line 1309834
    :cond_0
    iget-object v0, p0, LX/8Br;->m:LX/0Yb;

    if-nez v0, :cond_1

    .line 1309835
    iget-object v0, p0, LX/8Br;->d:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    new-instance v2, LX/8Bp;

    invoke-direct {v2, p0}, LX/8Bp;-><init>(LX/8Br;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/8Br;->m:LX/0Yb;

    .line 1309836
    :cond_1
    iget-object v0, p0, LX/8Br;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1309837
    iget-object v0, p0, LX/8Br;->m:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1309838
    invoke-static {p0}, LX/8Br;->f(LX/8Br;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1309839
    iget-object v0, p0, LX/8Br;->a:LX/2MA;

    invoke-interface {v0}, LX/2MA;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, LX/8Br;->l(LX/8Br;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1309840
    if-eqz v0, :cond_3

    .line 1309841
    invoke-static {p0}, LX/8Br;->i(LX/8Br;)V

    .line 1309842
    :goto_1
    return-void

    .line 1309843
    :cond_3
    invoke-static {p0}, LX/8Br;->h(LX/8Br;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1309829
    iget-object v0, p0, LX/8Br;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1309830
    iget-object v0, p0, LX/8Br;->m:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1309831
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1309823
    sget-object v0, LX/8Bq;->a:[I

    iget-object v1, p0, LX/8Br;->a:LX/2MA;

    invoke-interface {v1}, LX/2MA;->a()LX/2MB;

    move-result-object v1

    invoke-virtual {v1}, LX/2MB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1309824
    const-string v0, "ConnectionStatusNotification - Connected"

    :goto_0
    return-object v0

    .line 1309825
    :pswitch_0
    const-string v0, "ConnectionStatusNotification - No Internet"

    goto :goto_0

    .line 1309826
    :pswitch_1
    const-string v0, "ConnectionStatusNotification - Waiting To Connect"

    goto :goto_0

    .line 1309827
    :pswitch_2
    const-string v0, "ConnectionStatusNotification - Connecting"

    goto :goto_0

    .line 1309828
    :pswitch_3
    const-string v0, "ConnectionStatusNotification - Connected To Captive Portal"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
