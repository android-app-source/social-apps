.class public LX/6kO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final folder:Ljava/lang/Integer;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1136500
    new-instance v0, LX/1sv;

    const-string v1, "DeltaThreadFolder"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kO;->b:LX/1sv;

    .line 1136501
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kO;->c:LX/1sw;

    .line 1136502
    new-instance v0, LX/1sw;

    const-string v1, "folder"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kO;->d:LX/1sw;

    .line 1136503
    sput-boolean v4, LX/6kO;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1136504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136505
    iput-object p1, p0, LX/6kO;->threadKey:LX/6l9;

    .line 1136506
    iput-object p2, p0, LX/6kO;->folder:Ljava/lang/Integer;

    .line 1136507
    return-void
.end method

.method public static a(LX/6kO;)V
    .locals 3

    .prologue
    .line 1136508
    iget-object v0, p0, LX/6kO;->folder:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6l8;->a:LX/1sn;

    iget-object v1, p0, LX/6kO;->folder:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1136509
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'folder\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6kO;->folder:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1136510
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136511
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1136512
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1136513
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1136514
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaThreadFolder"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136515
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136516
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136517
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136518
    const/4 v1, 0x1

    .line 1136519
    iget-object v5, p0, LX/6kO;->threadKey:LX/6l9;

    if-eqz v5, :cond_0

    .line 1136520
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136521
    const-string v1, "threadKey"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136522
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136523
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136524
    iget-object v1, p0, LX/6kO;->threadKey:LX/6l9;

    if-nez v1, :cond_6

    .line 1136525
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136526
    :goto_3
    const/4 v1, 0x0

    .line 1136527
    :cond_0
    iget-object v5, p0, LX/6kO;->folder:Ljava/lang/Integer;

    if-eqz v5, :cond_2

    .line 1136528
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136529
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136530
    const-string v1, "folder"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136531
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136532
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136533
    iget-object v0, p0, LX/6kO;->folder:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1136534
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136535
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136536
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136537
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136538
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1136539
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1136540
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1136541
    :cond_6
    iget-object v1, p0, LX/6kO;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1136542
    :cond_7
    sget-object v0, LX/6l8;->b:Ljava/util/Map;

    iget-object v1, p0, LX/6kO;->folder:Ljava/lang/Integer;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1136543
    if-eqz v0, :cond_8

    .line 1136544
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136545
    const-string v1, " ("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136546
    :cond_8
    iget-object v1, p0, LX/6kO;->folder:Ljava/lang/Integer;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1136547
    if-eqz v0, :cond_2

    .line 1136548
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1136549
    invoke-static {p0}, LX/6kO;->a(LX/6kO;)V

    .line 1136550
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136551
    iget-object v0, p0, LX/6kO;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1136552
    iget-object v0, p0, LX/6kO;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1136553
    sget-object v0, LX/6kO;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136554
    iget-object v0, p0, LX/6kO;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1136555
    :cond_0
    iget-object v0, p0, LX/6kO;->folder:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1136556
    iget-object v0, p0, LX/6kO;->folder:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1136557
    sget-object v0, LX/6kO;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136558
    iget-object v0, p0, LX/6kO;->folder:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1136559
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136560
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136561
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136562
    if-nez p1, :cond_1

    .line 1136563
    :cond_0
    :goto_0
    return v0

    .line 1136564
    :cond_1
    instance-of v1, p1, LX/6kO;

    if-eqz v1, :cond_0

    .line 1136565
    check-cast p1, LX/6kO;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136566
    if-nez p1, :cond_3

    .line 1136567
    :cond_2
    :goto_1
    move v0, v2

    .line 1136568
    goto :goto_0

    .line 1136569
    :cond_3
    iget-object v0, p0, LX/6kO;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1136570
    :goto_2
    iget-object v3, p1, LX/6kO;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1136571
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136572
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136573
    iget-object v0, p0, LX/6kO;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6kO;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136574
    :cond_5
    iget-object v0, p0, LX/6kO;->folder:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1136575
    :goto_4
    iget-object v3, p1, LX/6kO;->folder:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1136576
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136577
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136578
    iget-object v0, p0, LX/6kO;->folder:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kO;->folder:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1136579
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1136580
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1136581
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1136582
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1136583
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136584
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136585
    sget-boolean v0, LX/6kO;->a:Z

    .line 1136586
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kO;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136587
    return-object v0
.end method
