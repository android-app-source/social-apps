.class public LX/7xE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1277208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 2

    .prologue
    .line 1277209
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p0}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1277210
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-direct {v1, p2, v0}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    return-object v1
.end method

.method public static a(LX/0Px;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/payments/currency/CurrencyAmount;"
        }
    .end annotation

    .prologue
    .line 1277198
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-direct {v1, p1, v0}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 1277199
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1277200
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v5, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1277201
    iget-object v6, v5, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v5, v6

    .line 1277202
    invoke-direct {v4, p1, v5}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 1277203
    iget v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    .line 1277204
    invoke-virtual {v4, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(I)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1277205
    invoke-virtual {v2, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    .line 1277206
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1277207
    :cond_0
    return-object v2
.end method

.method public static b(LX/0Px;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1277197
    invoke-static {p0, p1}, LX/7xE;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result v0

    return v0
.end method
