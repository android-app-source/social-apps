.class public LX/7mx;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/Typeface;

.field private final c:Z

.field private final d:Lcom/facebook/delights/floating/DelightsFireworks;

.field private final e:LX/0Zb;

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0W3;Lcom/facebook/delights/floating/DelightsFireworks;LX/0Zb;LX/1Ar;Landroid/view/View$OnClickListener;)V
    .locals 4
    .param p5    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1237562
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 1237563
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v2, LX/0X5;->iJ:J

    invoke-interface {p1, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/7mx;->a:I

    .line 1237564
    const-string v0, "bold"

    sget-wide v2, LX/0X5;->iK:J

    invoke-interface {p1, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    :goto_0
    iput-object v0, p0, LX/7mx;->b:Landroid/graphics/Typeface;

    .line 1237565
    invoke-virtual {p4}, LX/1Ar;->e()Z

    move-result v0

    iput-boolean v0, p0, LX/7mx;->c:Z

    .line 1237566
    iput-object p2, p0, LX/7mx;->d:Lcom/facebook/delights/floating/DelightsFireworks;

    .line 1237567
    iput-object p3, p0, LX/7mx;->e:LX/0Zb;

    .line 1237568
    iput-object p5, p0, LX/7mx;->f:Landroid/view/View$OnClickListener;

    .line 1237569
    return-void

    .line 1237570
    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1237555
    iget-object v0, p0, LX/7mx;->f:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    .line 1237556
    iget-object v0, p0, LX/7mx;->f:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1237557
    :cond_0
    :goto_0
    return-void

    .line 1237558
    :cond_1
    iget-boolean v0, p0, LX/7mx;->c:Z

    if-eqz v0, :cond_0

    .line 1237559
    iget-object v0, p0, LX/7mx;->e:LX/0Zb;

    const-string v1, "factory_delights_nye_2017_post_tapped"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1237560
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1237561
    iget-object v0, p0, LX/7mx;->d:Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/delights/floating/DelightsFireworks;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1237551
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1237552
    iget v0, p0, LX/7mx;->a:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1237553
    iget-object v0, p0, LX/7mx;->b:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1237554
    return-void
.end method
