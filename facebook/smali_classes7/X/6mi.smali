.class public LX/6mi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final time:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1146389
    new-instance v0, LX/1sv;

    const-string v1, "TimeSyncResponse"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mi;->b:LX/1sv;

    .line 1146390
    new-instance v0, LX/1sw;

    const-string v1, "time"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mi;->c:LX/1sw;

    .line 1146391
    sput-boolean v3, LX/6mi;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1146392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146393
    iput-object p1, p0, LX/6mi;->time:Ljava/lang/Long;

    .line 1146394
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1146395
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1146396
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1146397
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1146398
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TimeSyncResponse"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146399
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146400
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146401
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146402
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146403
    const-string v4, "time"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146404
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146405
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146406
    iget-object v0, p0, LX/6mi;->time:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 1146407
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146408
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146409
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146410
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1146411
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1146412
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1146413
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 1146414
    :cond_3
    iget-object v0, p0, LX/6mi;->time:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1146415
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146416
    iget-object v0, p0, LX/6mi;->time:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1146417
    sget-object v0, LX/6mi;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146418
    iget-object v0, p0, LX/6mi;->time:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146419
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146420
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146421
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1146422
    if-nez p1, :cond_1

    .line 1146423
    :cond_0
    :goto_0
    return v0

    .line 1146424
    :cond_1
    instance-of v1, p1, LX/6mi;

    if-eqz v1, :cond_0

    .line 1146425
    check-cast p1, LX/6mi;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146426
    if-nez p1, :cond_3

    .line 1146427
    :cond_2
    :goto_1
    move v0, v2

    .line 1146428
    goto :goto_0

    .line 1146429
    :cond_3
    iget-object v0, p0, LX/6mi;->time:Ljava/lang/Long;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1146430
    :goto_2
    iget-object v3, p1, LX/6mi;->time:Ljava/lang/Long;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1146431
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1146432
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146433
    iget-object v0, p0, LX/6mi;->time:Ljava/lang/Long;

    iget-object v3, p1, LX/6mi;->time:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1146434
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1146435
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1146436
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1146437
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146438
    sget-boolean v0, LX/6mi;->a:Z

    .line 1146439
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mi;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1146440
    return-object v0
.end method
