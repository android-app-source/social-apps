.class public LX/8Gx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/CropProfilePictureParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320309
    const-class v0, LX/8Gx;

    sput-object v0, LX/8Gx;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320308
    return-void
.end method

.method public static a(LX/0QB;)LX/8Gx;
    .locals 1

    .prologue
    .line 1320266
    new-instance v0, LX/8Gx;

    invoke-direct {v0}, LX/8Gx;-><init>()V

    .line 1320267
    move-object v0, v0

    .line 1320268
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1320271
    check-cast p1, Lcom/facebook/photos/data/method/CropProfilePictureParams;

    .line 1320272
    const-string v0, "me/picture/%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 1320273
    iget-object v3, p1, Lcom/facebook/photos/data/method/CropProfilePictureParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1320274
    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1320275
    iget-object v1, p1, Lcom/facebook/photos/data/method/CropProfilePictureParams;->b:Landroid/graphics/RectF;

    move-object v1, v1

    .line 1320276
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 1320277
    const-string v3, "x"

    iget v4, v1, Landroid/graphics/RectF;->left:F

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320278
    const-string v3, "y"

    iget v4, v1, Landroid/graphics/RectF;->top:F

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320279
    const-string v3, "width"

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320280
    const-string v3, "height"

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320281
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1320282
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "scaled_crop_rect"

    invoke-static {v2}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v2

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320283
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320284
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "profile_pic_method"

    .line 1320285
    iget-object v4, p1, Lcom/facebook/photos/data/method/CropProfilePictureParams;->e:Ljava/lang/String;

    move-object v4, v4

    .line 1320286
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320287
    iget-wide v6, p1, Lcom/facebook/photos/data/method/CropProfilePictureParams;->c:J

    move-wide v2, v6

    .line 1320288
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 1320289
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "expiration_time"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320290
    :cond_0
    iget-object v2, p1, Lcom/facebook/photos/data/method/CropProfilePictureParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1320291
    if-eqz v2, :cond_1

    .line 1320292
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "sticker_id"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320293
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    sget-object v3, LX/8Gx;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1320294
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1320295
    move-object v2, v2

    .line 1320296
    const-string v3, "POST"

    .line 1320297
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1320298
    move-object v2, v2

    .line 1320299
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 1320300
    move-object v0, v2

    .line 1320301
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1320302
    move-object v0, v0

    .line 1320303
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320304
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320305
    move-object v0, v0

    .line 1320306
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1320269
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1320270
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
