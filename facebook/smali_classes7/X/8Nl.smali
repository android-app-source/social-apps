.class public final LX/8Nl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Nm;


# direct methods
.method public constructor <init>(LX/8Nm;)V
    .locals 0

    .prologue
    .line 1337870
    iput-object p1, p0, LX/8Nl;->a:LX/8Nm;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1337871
    iget-object v0, p0, LX/8Nl;->a:LX/8Nm;

    iget-object v0, v0, LX/8Nm;->e:LX/8Ku;

    invoke-virtual {v0}, LX/8Ku;->a()V

    .line 1337872
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1337873
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1337874
    if-eqz p1, :cond_0

    .line 1337875
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1337876
    if-nez v0, :cond_1

    .line 1337877
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Story not found"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/8Nl;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1337878
    :goto_0
    return-void

    .line 1337879
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1337880
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1337881
    iget-object v1, p0, LX/8Nl;->a:LX/8Nm;

    iget-object v1, v1, LX/8Nm;->e:LX/8Ku;

    .line 1337882
    iget-object v2, v1, LX/8Ku;->c:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8L4;

    iget-object v3, v1, LX/8Ku;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    iget-object p0, v1, LX/8Ku;->b:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-virtual {v2, v3, p0, p1, v0}, LX/8L4;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLStory;)V

    .line 1337883
    goto :goto_0
.end method
