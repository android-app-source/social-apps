.class public LX/7FO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7FO;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/structuredsurvey/util/clientconstraints/SurveyClientConstraintsResolver;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Random;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/structuredsurvey/util/clientconstraints/SurveyClientConstraintsResolver;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1186221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1186222
    iput-object p1, p0, LX/7FO;->a:LX/0Ot;

    .line 1186223
    iput-object p2, p0, LX/7FO;->b:LX/0Ot;

    .line 1186224
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/7FO;->c:Ljava/util/Random;

    .line 1186225
    return-void
.end method

.method public static a(LX/0QB;)LX/7FO;
    .locals 6

    .prologue
    .line 1186205
    sget-object v0, LX/7FO;->d:LX/7FO;

    if-nez v0, :cond_1

    .line 1186206
    const-class v1, LX/7FO;

    monitor-enter v1

    .line 1186207
    :try_start_0
    sget-object v0, LX/7FO;->d:LX/7FO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1186208
    if-eqz v2, :cond_0

    .line 1186209
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1186210
    new-instance v3, LX/7FO;

    const/16 v4, 0xbc

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 1186211
    new-instance v5, LX/7FL;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v5, p0}, LX/7FL;-><init>(LX/0QB;)V

    move-object v5, v5

    .line 1186212
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v5, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v5

    move-object v5, v5

    .line 1186213
    invoke-direct {v3, v4, v5}, LX/7FO;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1186214
    move-object v0, v3

    .line 1186215
    sput-object v0, LX/7FO;->d:LX/7FO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1186216
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1186217
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1186218
    :cond_1
    sget-object v0, LX/7FO;->d:LX/7FO;

    return-object v0

    .line 1186219
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1186220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1186226
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "survey_integration_touched"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1186227
    const-string v0, "integration_point_id"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "action"

    invoke-virtual {v0, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "leaf_id"

    invoke-virtual {v0, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1186228
    iget-object v0, p0, LX/7FO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1186229
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1186199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1186200
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    .line 1186201
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1186202
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1186203
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, LX/7FO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1186204
    return-void
.end method

.method private a(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;LX/1x4;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1186181
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 1186182
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5

    .line 1186183
    if-nez v4, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    move v0, v2

    .line 1186184
    :goto_1
    return v0

    .line 1186185
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1186186
    :cond_0
    const-class v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;

    invoke-virtual {v1, v4, v3, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    .line 1186187
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1186188
    goto :goto_2

    :cond_2
    move v0, v3

    goto :goto_0

    .line 1186189
    :cond_3
    const-class v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;

    invoke-virtual {v1, v4, v3, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 1186190
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1186191
    :goto_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move v4, v3

    :goto_4
    if-ge v4, v5, :cond_6

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;

    .line 1186192
    iget-object v6, p2, LX/1x4;->f:LX/0P1;

    move-object v6, v6

    .line 1186193
    invoke-direct {p0, v0, v6}, LX/7FO;->a(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;LX/0P1;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v3

    .line 1186194
    goto :goto_1

    .line 1186195
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1186196
    move-object v1, v0

    goto :goto_3

    .line 1186197
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_6
    move v0, v2

    .line 1186198
    goto :goto_1
.end method

.method private a(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;LX/0P1;)Z
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1186151
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1186152
    invoke-virtual {p2, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1186153
    :goto_0
    return v0

    .line 1186154
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1186155
    iget-object v0, p0, LX/7FO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7FN;

    .line 1186156
    sget-object v4, LX/7FN;->a:LX/0Rf;

    move-object v4, v4

    .line 1186157
    invoke-virtual {v4, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1186158
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1186159
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->j()Ljava/lang/String;

    move-result-object v5

    .line 1186160
    invoke-virtual {p2, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1186161
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1186162
    sget-object v9, LX/7FM;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1186163
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 1186164
    :cond_2
    :goto_1
    move v0, v6

    .line 1186165
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1186166
    goto :goto_0

    .line 1186167
    :cond_4
    sget-object v9, LX/7FM;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1186168
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    :goto_2
    move v6, v5

    goto :goto_1

    :cond_5
    move v5, v7

    goto :goto_2

    .line 1186169
    :cond_6
    sget-object v9, LX/7FM;->c:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1186170
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->m()D

    move-result-wide v9

    .line 1186171
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v11

    cmpl-double v5, v11, v9

    if-gtz v5, :cond_2

    move v6, v7

    goto :goto_1

    .line 1186172
    :cond_7
    sget-object v9, LX/7FM;->d:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1186173
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->m()D

    move-result-wide v9

    .line 1186174
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v11

    cmpl-double v5, v11, v9

    if-gez v5, :cond_2

    move v6, v7

    goto :goto_1

    .line 1186175
    :cond_8
    sget-object v9, LX/7FM;->e:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1186176
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->l()D

    move-result-wide v9

    .line 1186177
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v11

    cmpg-double v5, v11, v9

    if-ltz v5, :cond_2

    move v6, v7

    goto :goto_1

    .line 1186178
    :cond_9
    sget-object v9, LX/7FM;->f:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1186179
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->l()D

    move-result-wide v9

    .line 1186180
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v11

    cmpg-double v5, v11, v9

    if-lez v5, :cond_2

    move v6, v7

    goto :goto_1

    :cond_a
    move v6, v7

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0Px;LX/1x4;)Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;",
            ">;",
            "LX/1x4;",
            ")",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;"
        }
    .end annotation

    .prologue
    .line 1186139
    const-string v0, "check_eligibility_start"

    invoke-direct {p0, p1, v0, p2}, LX/7FO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 1186140
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1186141
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    .line 1186142
    invoke-direct {p0, v0, p3}, LX/7FO;->a(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;LX/1x4;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1186143
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1186144
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1186145
    :cond_1
    const-string v0, "check_eligibility_finished"

    invoke-direct {p0, p1, v0, v2}, LX/7FO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 1186146
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1186147
    iget-object v0, p0, LX/7FO;->c:Ljava/util/Random;

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    .line 1186148
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1186149
    const-string v1, "eligible_session_chosen"

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v1, v2}, LX/7FO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1186150
    :cond_2
    :goto_1
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
