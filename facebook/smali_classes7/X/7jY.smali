.class public final LX/7jY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/String;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/03R;

.field public k:LX/03R;

.field private l:LX/7ja;

.field private m:Ljava/lang/String;

.field public n:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1229570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229571
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/7jY;->j:LX/03R;

    .line 1229572
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/7jY;->k:LX/03R;

    .line 1229573
    return-void
.end method

.method public static a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)LX/7jY;
    .locals 3

    .prologue
    .line 1229529
    new-instance v0, LX/7jY;

    invoke-direct {v0}, LX/7jY;-><init>()V

    .line 1229530
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1229531
    iput-object v1, v0, LX/7jY;->a:Ljava/lang/String;

    .line 1229532
    move-object v0, v0

    .line 1229533
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1229534
    iput-object v1, v0, LX/7jY;->b:Ljava/lang/String;

    .line 1229535
    move-object v0, v0

    .line 1229536
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1229537
    iput-object v1, v0, LX/7jY;->c:Ljava/lang/String;

    .line 1229538
    move-object v0, v0

    .line 1229539
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1229540
    iput-object v1, v0, LX/7jY;->d:Ljava/lang/String;

    .line 1229541
    move-object v0, v0

    .line 1229542
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1229543
    iput-object v1, v0, LX/7jY;->e:Ljava/lang/String;

    .line 1229544
    move-object v0, v0

    .line 1229545
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1229546
    iput-object v1, v0, LX/7jY;->f:Ljava/lang/String;

    .line 1229547
    move-object v0, v0

    .line 1229548
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    move-object v1, v1

    .line 1229549
    iput-object v1, v0, LX/7jY;->g:Ljava/lang/Integer;

    .line 1229550
    move-object v0, v0

    .line 1229551
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1229552
    iput-object v1, v0, LX/7jY;->h:Ljava/lang/String;

    .line 1229553
    move-object v0, v0

    .line 1229554
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v1, v1

    .line 1229555
    iput-object v1, v0, LX/7jY;->i:LX/0Px;

    .line 1229556
    move-object v0, v0

    .line 1229557
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    move-object v1, v1

    .line 1229558
    iput-object v1, v0, LX/7jY;->j:LX/03R;

    .line 1229559
    move-object v0, v0

    .line 1229560
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->k:LX/03R;

    move-object v1, v1

    .line 1229561
    iput-object v1, v0, LX/7jY;->k:LX/03R;

    .line 1229562
    move-object v0, v0

    .line 1229563
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->l:LX/7ja;

    move-object v1, v1

    .line 1229564
    iget-object v2, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v2, v2

    .line 1229565
    invoke-virtual {v0, v1, v2}, LX/7jY;->a(LX/7ja;Ljava/lang/String;)LX/7jY;

    move-result-object v0

    .line 1229566
    iget-object v1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v1

    .line 1229567
    iput-object v1, v0, LX/7jY;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1229568
    move-object v0, v0

    .line 1229569
    return-object v0
.end method


# virtual methods
.method public final a(LX/7ja;Ljava/lang/String;)LX/7jY;
    .locals 0

    .prologue
    .line 1229526
    iput-object p1, p0, LX/7jY;->l:LX/7ja;

    .line 1229527
    iput-object p2, p0, LX/7jY;->m:Ljava/lang/String;

    .line 1229528
    return-object p0
.end method

.method public final a()Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;
    .locals 17

    .prologue
    .line 1229525
    new-instance v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7jY;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7jY;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/7jY;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7jY;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/7jY;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7jY;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7jY;->g:Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7jY;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7jY;->i:LX/0Px;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7jY;->j:LX/03R;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7jY;->k:LX/03R;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/7jY;->l:LX/7ja;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/7jY;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7jY;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    const/16 v16, 0x0

    invoke-direct/range {v1 .. v16}, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;LX/0Px;LX/03R;LX/03R;LX/7ja;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;B)V

    return-object v1
.end method
