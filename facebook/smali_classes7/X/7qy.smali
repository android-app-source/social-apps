.class public final LX/7qy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 1261062
    const/16 v27, 0x0

    .line 1261063
    const/16 v26, 0x0

    .line 1261064
    const/16 v23, 0x0

    .line 1261065
    const-wide/16 v24, 0x0

    .line 1261066
    const/16 v22, 0x0

    .line 1261067
    const/16 v21, 0x0

    .line 1261068
    const/16 v20, 0x0

    .line 1261069
    const/16 v19, 0x0

    .line 1261070
    const/16 v18, 0x0

    .line 1261071
    const/4 v13, 0x0

    .line 1261072
    const-wide/16 v16, 0x0

    .line 1261073
    const-wide/16 v14, 0x0

    .line 1261074
    const/4 v12, 0x0

    .line 1261075
    const/4 v11, 0x0

    .line 1261076
    const/4 v10, 0x0

    .line 1261077
    const/4 v9, 0x0

    .line 1261078
    const/4 v8, 0x0

    .line 1261079
    const/4 v7, 0x0

    .line 1261080
    const/4 v6, 0x0

    .line 1261081
    const/4 v5, 0x0

    .line 1261082
    const/4 v4, 0x0

    .line 1261083
    const/4 v3, 0x0

    .line 1261084
    const/4 v2, 0x0

    .line 1261085
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_19

    .line 1261086
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1261087
    const/4 v2, 0x0

    .line 1261088
    :goto_0
    return v2

    .line 1261089
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_11

    .line 1261090
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1261091
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1261092
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v30, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v30

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1261093
    const-string v7, "can_viewer_change_guest_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1261094
    const/4 v2, 0x1

    .line 1261095
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v29, v6

    move v6, v2

    goto :goto_1

    .line 1261096
    :cond_1
    const-string v7, "connection_style"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1261097
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 1261098
    :cond_2
    const-string v7, "cover_photo"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1261099
    invoke-static/range {p0 .. p1}, LX/7qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 1261100
    :cond_3
    const-string v7, "end_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1261101
    const/4 v2, 0x1

    .line 1261102
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1261103
    :cond_4
    const-string v7, "eventProfilePicture"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1261104
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 1261105
    :cond_5
    const-string v7, "event_place"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1261106
    invoke-static/range {p0 .. p1}, LX/7rw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 1261107
    :cond_6
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1261108
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1261109
    :cond_7
    const-string v7, "is_all_day"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1261110
    const/4 v2, 0x1

    .line 1261111
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v12, v2

    move/from16 v23, v7

    goto/16 :goto_1

    .line 1261112
    :cond_8
    const-string v7, "is_event_draft"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1261113
    const/4 v2, 0x1

    .line 1261114
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v11, v2

    move/from16 v22, v7

    goto/16 :goto_1

    .line 1261115
    :cond_9
    const-string v7, "name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1261116
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1261117
    :cond_a
    const-string v7, "scheduled_publish_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1261118
    const/4 v2, 0x1

    .line 1261119
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v20

    move v10, v2

    goto/16 :goto_1

    .line 1261120
    :cond_b
    const-string v7, "start_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1261121
    const/4 v2, 0x1

    .line 1261122
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v18

    move v9, v2

    goto/16 :goto_1

    .line 1261123
    :cond_c
    const-string v7, "timezone"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 1261124
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1261125
    :cond_d
    const-string v7, "viewer_guest_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1261126
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1261127
    :cond_e
    const-string v7, "viewer_has_pending_invite"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 1261128
    const/4 v2, 0x1

    .line 1261129
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v8, v2

    move v14, v7

    goto/16 :goto_1

    .line 1261130
    :cond_f
    const-string v7, "viewer_watch_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1261131
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1261132
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1261133
    :cond_11
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1261134
    if-eqz v6, :cond_12

    .line 1261135
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1261136
    :cond_12
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1261137
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1261138
    if-eqz v3, :cond_13

    .line 1261139
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1261140
    :cond_13
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1261141
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1261142
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1261143
    if-eqz v12, :cond_14

    .line 1261144
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1261145
    :cond_14
    if-eqz v11, :cond_15

    .line 1261146
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1261147
    :cond_15
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1261148
    if-eqz v10, :cond_16

    .line 1261149
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1261150
    :cond_16
    if-eqz v9, :cond_17

    .line 1261151
    const/16 v3, 0xb

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1261152
    :cond_17
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1261153
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1261154
    if-eqz v8, :cond_18

    .line 1261155
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->a(IZ)V

    .line 1261156
    :cond_18
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1261157
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_19
    move/from16 v28, v26

    move/from16 v29, v27

    move/from16 v26, v22

    move/from16 v27, v23

    move/from16 v22, v18

    move/from16 v23, v19

    move-wide/from16 v18, v14

    move v14, v10

    move v15, v11

    move v10, v4

    move v11, v5

    move-wide/from16 v4, v24

    move/from16 v24, v20

    move/from16 v25, v21

    move-wide/from16 v20, v16

    move/from16 v17, v13

    move/from16 v16, v12

    move v13, v9

    move v12, v6

    move v6, v8

    move v9, v3

    move v8, v2

    move v3, v7

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0xf

    const/16 v3, 0xd

    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    .line 1261158
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1261159
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1261160
    if-eqz v0, :cond_0

    .line 1261161
    const-string v1, "can_viewer_change_guest_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261162
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1261163
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1261164
    if-eqz v0, :cond_1

    .line 1261165
    const-string v0, "connection_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261166
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1261167
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1261168
    if-eqz v0, :cond_2

    .line 1261169
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261170
    invoke-static {p0, v0, p2, p3}, LX/7qx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1261171
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1261172
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 1261173
    const-string v2, "end_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261174
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1261175
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1261176
    if-eqz v0, :cond_4

    .line 1261177
    const-string v1, "eventProfilePicture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261178
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1261179
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1261180
    if-eqz v0, :cond_5

    .line 1261181
    const-string v1, "event_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261182
    invoke-static {p0, v0, p2, p3}, LX/7rw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1261183
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1261184
    if-eqz v0, :cond_6

    .line 1261185
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261186
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1261187
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1261188
    if-eqz v0, :cond_7

    .line 1261189
    const-string v1, "is_all_day"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261190
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1261191
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1261192
    if-eqz v0, :cond_8

    .line 1261193
    const-string v1, "is_event_draft"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261194
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1261195
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1261196
    if-eqz v0, :cond_9

    .line 1261197
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261198
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1261199
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1261200
    cmp-long v2, v0, v4

    if-eqz v2, :cond_a

    .line 1261201
    const-string v2, "scheduled_publish_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261202
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1261203
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1261204
    cmp-long v2, v0, v4

    if-eqz v2, :cond_b

    .line 1261205
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261206
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1261207
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1261208
    if-eqz v0, :cond_c

    .line 1261209
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261210
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1261211
    :cond_c
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1261212
    if-eqz v0, :cond_d

    .line 1261213
    const-string v0, "viewer_guest_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261214
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1261215
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1261216
    if-eqz v0, :cond_e

    .line 1261217
    const-string v1, "viewer_has_pending_invite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261218
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1261219
    :cond_e
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1261220
    if-eqz v0, :cond_f

    .line 1261221
    const-string v0, "viewer_watch_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1261222
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1261223
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1261224
    return-void
.end method
