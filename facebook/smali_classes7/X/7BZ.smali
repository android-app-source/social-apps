.class public final enum LX/7BZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7BZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7BZ;

.field public static final enum DEFAULT_KEYWORD_MODE:LX/7BZ;

.field public static final enum ENTITY_ONLY_MODE:LX/7BZ;

.field public static final enum KEYWORD_ONLY_MODE:LX/7BZ;

.field public static final enum SCOPED:LX/7BZ;

.field public static final enum SINGLE_STATE_MODE:LX/7BZ;

.field public static final enum VIDEO_HOME_SEARCH_KEYWORD_ONLY_MODE:LX/7BZ;

.field public static final enum WEB_VIEW_SINGLE_STATE_MODE:LX/7BZ;


# instance fields
.field private mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1179070
    new-instance v0, LX/7BZ;

    const-string v1, "DEFAULT_KEYWORD_MODE"

    const-string v2, "blended"

    invoke-direct {v0, v1, v4, v2}, LX/7BZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    .line 1179071
    new-instance v0, LX/7BZ;

    const-string v1, "KEYWORD_ONLY_MODE"

    const-string v2, "keyword_only"

    invoke-direct {v0, v1, v5, v2}, LX/7BZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    .line 1179072
    new-instance v0, LX/7BZ;

    const-string v1, "ENTITY_ONLY_MODE"

    const-string v2, "entity_only"

    invoke-direct {v0, v1, v6, v2}, LX/7BZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7BZ;->ENTITY_ONLY_MODE:LX/7BZ;

    .line 1179073
    new-instance v0, LX/7BZ;

    const-string v1, "SINGLE_STATE_MODE"

    const-string v2, "single_state_v2"

    invoke-direct {v0, v1, v7, v2}, LX/7BZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7BZ;->SINGLE_STATE_MODE:LX/7BZ;

    .line 1179074
    new-instance v0, LX/7BZ;

    const-string v1, "WEB_VIEW_SINGLE_STATE_MODE"

    const-string v2, "web_view_search_box_single_state_no_pulse"

    invoke-direct {v0, v1, v8, v2}, LX/7BZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7BZ;->WEB_VIEW_SINGLE_STATE_MODE:LX/7BZ;

    .line 1179075
    new-instance v0, LX/7BZ;

    const-string v1, "SCOPED"

    const/4 v2, 0x5

    const-string v3, "scoped"

    invoke-direct {v0, v1, v2, v3}, LX/7BZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7BZ;->SCOPED:LX/7BZ;

    .line 1179076
    new-instance v0, LX/7BZ;

    const-string v1, "VIDEO_HOME_SEARCH_KEYWORD_ONLY_MODE"

    const/4 v2, 0x6

    const-string v3, "video_home_search_keyword_only"

    invoke-direct {v0, v1, v2, v3}, LX/7BZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7BZ;->VIDEO_HOME_SEARCH_KEYWORD_ONLY_MODE:LX/7BZ;

    .line 1179077
    const/4 v0, 0x7

    new-array v0, v0, [LX/7BZ;

    sget-object v1, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    aput-object v1, v0, v4

    sget-object v1, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    aput-object v1, v0, v5

    sget-object v1, LX/7BZ;->ENTITY_ONLY_MODE:LX/7BZ;

    aput-object v1, v0, v6

    sget-object v1, LX/7BZ;->SINGLE_STATE_MODE:LX/7BZ;

    aput-object v1, v0, v7

    sget-object v1, LX/7BZ;->WEB_VIEW_SINGLE_STATE_MODE:LX/7BZ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7BZ;->SCOPED:LX/7BZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7BZ;->VIDEO_HOME_SEARCH_KEYWORD_ONLY_MODE:LX/7BZ;

    aput-object v2, v0, v1

    sput-object v0, LX/7BZ;->$VALUES:[LX/7BZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1179065
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1179066
    iput-object p3, p0, LX/7BZ;->mValue:Ljava/lang/String;

    .line 1179067
    sget-object v0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->e:Ljava/util/Map;

    invoke-interface {v0, p3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1179068
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7BZ;
    .locals 1

    .prologue
    .line 1179078
    const-class v0, LX/7BZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7BZ;

    return-object v0
.end method

.method public static values()[LX/7BZ;
    .locals 1

    .prologue
    .line 1179069
    sget-object v0, LX/7BZ;->$VALUES:[LX/7BZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7BZ;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1179064
    iget-object v0, p0, LX/7BZ;->mValue:Ljava/lang/String;

    return-object v0
.end method
