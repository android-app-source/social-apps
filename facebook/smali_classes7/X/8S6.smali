.class public final LX/8S6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1346089
    iput-object p1, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1346090
    iget-object v0, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    iget-object v1, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, LX/333;->a(Ljava/lang/CharSequence;)V

    .line 1346091
    iget-object v0, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v1, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->t:Ljava/util/List;

    .line 1346092
    iget-object v0, p0, LX/8S6;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8S6;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1346093
    :goto_0
    return-void

    .line 1346094
    :cond_0
    iget-object v0, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->t:Ljava/util/List;

    iput-object v0, p0, LX/8S6;->b:Ljava/util/List;

    .line 1346095
    iget-object v0, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    iget-object v1, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->t:Ljava/util/List;

    .line 1346096
    iput-object v1, v0, LX/8tB;->i:Ljava/util/List;

    .line 1346097
    iget-object v0, p0, LX/8S6;->a:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->e()V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1346098
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1346099
    return-void
.end method
