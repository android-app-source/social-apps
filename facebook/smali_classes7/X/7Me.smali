.class public LX/7Me;
.super LX/2oy;
.source ""


# instance fields
.field public a:Z

.field public b:D

.field public c:D

.field private d:Landroid/view/ViewGroup;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1198939
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Me;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198940
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198937
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Me;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198938
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1198919
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198920
    iput-boolean v2, p0, LX/7Me;->a:Z

    .line 1198921
    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    iput-wide v0, p0, LX/7Me;->b:D

    .line 1198922
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/7Me;->c:D

    .line 1198923
    iput-boolean v2, p0, LX/7Me;->e:Z

    .line 1198924
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Mc;

    invoke-direct {v1, p0}, LX/7Mc;-><init>(LX/7Me;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1198925
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Md;

    invoke-direct {v1, p0}, LX/7Md;-><init>(LX/7Me;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1198926
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Mb;

    invoke-direct {v1, p0}, LX/7Mb;-><init>(LX/7Me;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1198927
    const v0, 0x7f0315b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1198928
    const v0, 0x7f0d0950

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/7Me;->d:Landroid/view/ViewGroup;

    .line 1198929
    return-void
.end method

.method public static g(LX/7Me;)V
    .locals 8

    .prologue
    .line 1198941
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/7Me;->d:Landroid/view/ViewGroup;

    iget-wide v2, p0, LX/7Me;->b:D

    iget-wide v4, p0, LX/7Me;->c:D

    iget-boolean v6, p0, LX/7Me;->e:Z

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;DDZZ)V

    .line 1198942
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1198930
    iget-wide v0, p1, LX/2pa;->d:D

    .line 1198931
    if-nez p2, :cond_0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, LX/7Me;->b:D

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 1198932
    :cond_0
    iput-wide v0, p0, LX/7Me;->b:D

    .line 1198933
    invoke-static {p0}, LX/7Me;->g(LX/7Me;)V

    .line 1198934
    :cond_1
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_2

    .line 1198935
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Me;->a:Z

    .line 1198936
    :cond_2
    return-void
.end method

.method public setShouldCropToFit(Z)V
    .locals 0

    .prologue
    .line 1198917
    iput-boolean p1, p0, LX/7Me;->e:Z

    .line 1198918
    return-void
.end method

.method public setVideoPluginAlignment(LX/7OE;)V
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1198909
    iget-object v0, p0, LX/7Me;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/2p4;

    .line 1198910
    invoke-virtual {v0, v4, v1}, LX/2p4;->addRule(II)V

    .line 1198911
    invoke-virtual {v0, v3, v1}, LX/2p4;->addRule(II)V

    .line 1198912
    sget-object v1, LX/7Ma;->a:[I

    invoke-virtual {p1}, LX/7OE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1198913
    :goto_0
    iget-object v1, p0, LX/7Me;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1198914
    return-void

    .line 1198915
    :pswitch_0
    invoke-virtual {v0, v3}, LX/2p4;->addRule(I)V

    goto :goto_0

    .line 1198916
    :pswitch_1
    invoke-virtual {v0, v4}, LX/2p4;->addRule(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
