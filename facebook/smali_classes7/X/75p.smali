.class public final LX/75p;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Landroid/os/Message;

.field public final synthetic b:Landroid/os/Messenger;

.field public final synthetic c:LX/75q;


# direct methods
.method public constructor <init>(LX/75q;Landroid/os/Message;Landroid/os/Messenger;)V
    .locals 0

    .prologue
    .line 1169995
    iput-object p1, p0, LX/75p;->c:LX/75q;

    iput-object p2, p0, LX/75p;->a:Landroid/os/Message;

    iput-object p3, p0, LX/75p;->b:Landroid/os/Messenger;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1170028
    iget-object v0, p0, LX/75p;->a:Landroid/os/Message;

    const/4 v1, 0x0

    invoke-static {v1, p1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1170029
    :try_start_0
    iget-object v0, p0, LX/75p;->b:Landroid/os/Messenger;

    iget-object v1, p0, LX/75p;->a:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1170030
    :goto_0
    return-void

    .line 1170031
    :catch_0
    move-exception v0

    .line 1170032
    sget-object v1, LX/75q;->b:Ljava/lang/Class;

    const-string v2, "Unable to respond to token refresh request"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1169996
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 1169997
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 1169998
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1169999
    :goto_0
    if-eqz v0, :cond_1

    .line 1170000
    check-cast v0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;

    .line 1170001
    iget-object v2, p0, LX/75p;->a:Landroid/os/Message;

    .line 1170002
    iget-object v3, v0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1170003
    iget-wide v6, v0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->b:J

    move-wide v4, v6

    .line 1170004
    iget-object v6, v0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->c:Ljava/util/List;

    move-object v0, v6

    .line 1170005
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/platform/common/action/PlatformAppCall;->d()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    move v7, v6

    .line 1170006
    :goto_1
    if-eqz v7, :cond_3

    const-string v6, "access_token"

    move-object v9, v6

    .line 1170007
    :goto_2
    if-eqz v7, :cond_4

    const-string v6, "expires_seconds_since_epoch"

    move-object v8, v6

    .line 1170008
    :goto_3
    if-eqz v7, :cond_5

    const-string v6, "permissions"

    .line 1170009
    :goto_4
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1170010
    instance-of v7, v0, Ljava/util/ArrayList;

    if-eqz v7, :cond_6

    .line 1170011
    check-cast v0, Ljava/util/ArrayList;

    .line 1170012
    :goto_5
    invoke-virtual {v10, v9, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170013
    invoke-virtual {v10, v8, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1170014
    invoke-virtual {v10, v6, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1170015
    move-object v0, v10

    .line 1170016
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1170017
    :goto_6
    :try_start_0
    iget-object v0, p0, LX/75p;->b:Landroid/os/Messenger;

    iget-object v1, p0, LX/75p;->a:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1170018
    :goto_7
    return-void

    :cond_0
    move-object v0, v1

    .line 1170019
    goto :goto_0

    .line 1170020
    :cond_1
    iget-object v0, p0, LX/75p;->a:Landroid/os/Message;

    new-instance v2, Lcom/facebook/fbservice/service/ServiceException;

    invoke-direct {v2, p1}, Lcom/facebook/fbservice/service/ServiceException;-><init>(Lcom/facebook/fbservice/service/OperationResult;)V

    invoke-static {v1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    goto :goto_6

    .line 1170021
    :catch_0
    move-exception v0

    .line 1170022
    sget-object v1, LX/75q;->b:Ljava/lang/Class;

    const-string v2, "Unable to respond to token refresh request"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 1170023
    :cond_2
    const/4 v6, 0x0

    move v7, v6

    goto :goto_1

    .line 1170024
    :cond_3
    const-string v6, "com.facebook.platform.extra.ACCESS_TOKEN"

    move-object v9, v6

    goto :goto_2

    .line 1170025
    :cond_4
    const-string v6, "com.facebook.platform.extra.EXPIRES_SECONDS_SINCE_EPOCH"

    move-object v8, v6

    goto :goto_3

    .line 1170026
    :cond_5
    const-string v6, "com.facebook.platform.extra.PERMISSIONS"

    goto :goto_4

    .line 1170027
    :cond_6
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v7

    goto :goto_5
.end method
