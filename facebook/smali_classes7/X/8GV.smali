.class public LX/8GV;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

.field private final e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field private final f:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1319571
    const-class v0, LX/8GV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/8GV;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;Ljava/util/concurrent/ExecutorService;LX/0Uh;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;",
            "Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319573
    iput-object p1, p0, LX/8GV;->b:Landroid/content/Context;

    .line 1319574
    iput-object p2, p0, LX/8GV;->c:LX/0Ot;

    .line 1319575
    iput-object p3, p0, LX/8GV;->d:Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    .line 1319576
    iput-object p4, p0, LX/8GV;->e:Ljava/util/concurrent/ExecutorService;

    .line 1319577
    iput-object p5, p0, LX/8GV;->f:LX/0Uh;

    .line 1319578
    return-void
.end method

.method public static a(LX/0QB;)LX/8GV;
    .locals 1

    .prologue
    .line 1319579
    invoke-static {p0}, LX/8GV;->b(LX/0QB;)LX/8GV;

    move-result-object v0

    return-object v0
.end method

.method private a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 14
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "Landroid/net/Uri;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319580
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v3

    invoke-static {v3}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v12

    .line 1319581
    :goto_0
    invoke-virtual/range {p4 .. p4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v4

    .line 1319582
    iget-object v3, p0, LX/8GV;->d:Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    iget v5, v4, LX/434;->b:I

    int-to-float v5, v5

    mul-float/2addr v5, p1

    float-to-int v5, v5

    iget v4, v4, LX/434;->a:I

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v6, v4

    if-eqz p6, :cond_1

    const/4 v7, 0x0

    :goto_1
    iget-object v4, p0, LX/8GV;->f:LX/0Uh;

    sget v8, LX/7l1;->b:I

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static/range {p2 .. p2}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/5iL;->isFilter(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-static/range {p2 .. p2}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v11

    const/4 v13, 0x0

    move-object/from16 v4, p4

    move-object/from16 v9, p3

    invoke-virtual/range {v3 .. v13}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a(Landroid/net/Uri;IIILjava/lang/String;LX/0Px;LX/0Px;LX/0Px;Landroid/graphics/RectF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1319583
    new-instance v4, LX/8GU;

    move-object/from16 v0, p4

    move/from16 v1, p6

    move/from16 v2, p5

    invoke-direct {v4, p0, v0, v1, v2}, LX/8GU;-><init>(LX/8GV;Landroid/net/Uri;ZZ)V

    .line 1319584
    iget-object v5, p0, LX/8GV;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1319585
    return-object v3

    .line 1319586
    :cond_0
    new-instance v12, Landroid/graphics/RectF;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v12, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 1319587
    :cond_1
    invoke-virtual/range {p4 .. p4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public static b(LX/0QB;)LX/8GV;
    .locals 6

    .prologue
    .line 1319588
    new-instance v0, LX/8GV;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x2d9

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->b(LX/0QB;)Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct/range {v0 .. v5}, LX/8GV;-><init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;Ljava/util/concurrent/ExecutorService;LX/0Uh;)V

    .line 1319589
    return-object v0
.end method


# virtual methods
.method public final a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "Landroid/net/Uri;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319590
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/8GV;->a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1319591
    new-instance v0, Ljava/io/File;

    invoke-static {p3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v3, "Facebook_edited"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1319592
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1319593
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1319594
    :goto_0
    return-void

    .line 1319595
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1319596
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1319597
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1319598
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1319599
    const-wide/16 v2, 0x0

    :try_start_2
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 1319600
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1319601
    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1319602
    iget-object v3, p0, LX/8GV;->b:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1319603
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1319604
    invoke-static {v0}, LX/1pX;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 1319605
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1319606
    :goto_1
    :try_start_3
    sget-object v3, LX/8GV;->a:Ljava/lang/String;

    const-string v4, "failed copy media to gallery"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1319607
    invoke-static {v2}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1319608
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 1319609
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1319610
    invoke-static {v2}, LX/1pX;->a(Ljava/io/Closeable;)V

    throw v0

    .line 1319611
    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_2

    .line 1319612
    :catch_1
    move-exception v0

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1
.end method

.method public final b(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "Landroid/net/Uri;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319613
    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/8GV;->a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
