.class public final LX/85x;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1295783
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 1295784
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1295785
    :goto_0
    return v1

    .line 1295786
    :cond_0
    const-string v11, "notification_status"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1295787
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    .line 1295788
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 1295789
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1295790
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1295791
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1295792
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1295793
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 1295794
    :cond_3
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1295795
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1295796
    :cond_4
    const-string v11, "secondary_subscribe_status"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1295797
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 1295798
    :cond_5
    const-string v11, "subscribe_status"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1295799
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1295800
    :cond_6
    const-string v11, "video_channel_is_viewer_following"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1295801
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1295802
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1295803
    :cond_8
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1295804
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1295805
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1295806
    if-eqz v3, :cond_9

    .line 1295807
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 1295808
    :cond_9
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1295809
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1295810
    if-eqz v0, :cond_a

    .line 1295811
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1295812
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 1295813
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1295814
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1295815
    if-eqz v0, :cond_0

    .line 1295816
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1295817
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1295818
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1295819
    if-eqz v0, :cond_1

    .line 1295820
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1295821
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1295822
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1295823
    if-eqz v0, :cond_2

    .line 1295824
    const-string v1, "notification_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1295825
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1295826
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1295827
    if-eqz v0, :cond_3

    .line 1295828
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1295829
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1295830
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1295831
    if-eqz v0, :cond_4

    .line 1295832
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1295833
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1295834
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1295835
    if-eqz v0, :cond_5

    .line 1295836
    const-string v1, "video_channel_is_viewer_following"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1295837
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1295838
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1295839
    return-void
.end method
