.class public interface abstract LX/6qn;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
.end method

.method public abstract a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
.end method

.method public abstract a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
.end method

.method public abstract a(Lcom/facebook/payments/shipping/model/MailingAddress;)V
.end method

.method public abstract a(Lcom/facebook/payments/shipping/model/ShippingOption;)V
.end method

.method public abstract a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Ljava/lang/String;LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation
.end method
