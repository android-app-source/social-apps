.class public LX/7x3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

.field public b:Z

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field public final e:LX/0tX;

.field private final f:LX/1Ck;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;Landroid/content/Context;LX/0tX;LX/1Ck;)V
    .locals 0
    .param p1    # Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1276958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276959
    iput-object p1, p0, LX/7x3;->a:Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

    .line 1276960
    iput-object p2, p0, LX/7x3;->d:Landroid/content/Context;

    .line 1276961
    iput-object p3, p0, LX/7x3;->e:LX/0tX;

    .line 1276962
    iput-object p4, p0, LX/7x3;->f:LX/1Ck;

    .line 1276963
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1276964
    iget-object v0, p0, LX/7x3;->f:LX/1Ck;

    const-string v1, "initial_fetch"

    new-instance v2, LX/7x1;

    invoke-direct {v2, p0, p1}, LX/7x1;-><init>(LX/7x3;Ljava/lang/String;)V

    new-instance v3, LX/7x2;

    invoke-direct {v3, p0}, LX/7x2;-><init>(LX/7x3;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1276965
    return-void
.end method
