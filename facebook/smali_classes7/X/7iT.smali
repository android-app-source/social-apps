.class public LX/7iT;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:LX/7iM;

.field private c:LX/7iN;

.field private d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Long;

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:LX/0Zb;

.field public s:LX/0So;

.field private final t:LX/0lB;

.field private final u:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1227607
    const-class v0, LX/7iT;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7iT;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/0Zb;LX/0lB;LX/03V;LX/7iM;LX/7iN;LX/7iP;Ljava/lang/Long;)V
    .locals 2
    .param p5    # LX/7iM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/7iN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/7iP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1227526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227527
    iput-object p2, p0, LX/7iT;->r:LX/0Zb;

    .line 1227528
    iput-object p1, p0, LX/7iT;->s:LX/0So;

    .line 1227529
    iput-object p3, p0, LX/7iT;->t:LX/0lB;

    .line 1227530
    iput-object p4, p0, LX/7iT;->u:LX/03V;

    .line 1227531
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7iT;->q:Ljava/util/ArrayList;

    .line 1227532
    iput-object p5, p0, LX/7iT;->b:LX/7iM;

    .line 1227533
    iput-object p6, p0, LX/7iT;->c:LX/7iN;

    .line 1227534
    invoke-static {p7}, LX/7iR;->a(LX/7iP;)I

    move-result v0

    iput v0, p0, LX/7iT;->i:I

    .line 1227535
    invoke-static {p8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->j:Ljava/lang/String;

    .line 1227536
    iget-object v0, p0, LX/7iT;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->f:Ljava/lang/Long;

    .line 1227537
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->d:Ljava/lang/Boolean;

    .line 1227538
    invoke-virtual {p0}, LX/7iT;->a()V

    .line 1227539
    return-void
.end method

.method public static a(LX/7iT;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1227540
    :try_start_0
    iget-object v0, p0, LX/7iT;->t:LX/0lB;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1227541
    :goto_0
    return-object v0

    .line 1227542
    :catch_0
    move-exception v0

    .line 1227543
    iget-object v1, p0, LX/7iT;->u:LX/03V;

    const-string v2, "commerce_logger_write_json_failed"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1227544
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1227545
    iget-object v0, p0, LX/7iT;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1227546
    :goto_0
    return-void

    .line 1227547
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->d:Ljava/lang/Boolean;

    .line 1227548
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->e:Ljava/lang/String;

    .line 1227549
    iget-object v0, p0, LX/7iT;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1227550
    iget-object v0, p0, LX/7iT;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->g:Ljava/lang/Long;

    goto :goto_0
.end method

.method public final a(LX/7iQ;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7iQ;",
            "Ljava/util/Map",
            "<",
            "LX/7iL;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1227551
    iget-object v0, p0, LX/7iT;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1227552
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1227553
    sget-object v2, LX/7iL;->SESSION_ID:LX/7iL;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227554
    sget-object v2, LX/7iL;->LOGGER_CREATION_TIME:LX/7iL;

    iget-object v3, p0, LX/7iT;->f:Ljava/lang/Long;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227555
    sget-object v2, LX/7iL;->LOGGING_START_TIME:LX/7iL;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227556
    sget-object v2, LX/7iL;->LOGGING_STOP_TIME:LX/7iL;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227557
    sget-object v2, LX/7iL;->REF_TYPE:LX/7iL;

    iget v3, p0, LX/7iT;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227558
    sget-object v2, LX/7iL;->REF_ID:LX/7iL;

    iget-object v3, p0, LX/7iT;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227559
    if-eqz p1, :cond_0

    .line 1227560
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1227561
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1227562
    sget-object v4, LX/7iL;->EVENT:LX/7iL;

    iget-object v4, v4, LX/7iL;->value:Ljava/lang/String;

    iget-object v5, p1, LX/7iQ;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227563
    sget-object v4, LX/7iL;->LOGGING_EVENT_TIME:LX/7iL;

    iget-object v4, v4, LX/7iL;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227564
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1227565
    sget-object v0, LX/7iL;->EVENTS:LX/7iL;

    invoke-static {p0, v2}, LX/7iT;->a(LX/7iT;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227566
    sget-object v0, LX/7iL;->LOG_ONLY_SUBEVENTS:LX/7iL;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1227567
    :cond_0
    if-eqz p2, :cond_1

    .line 1227568
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1227569
    :cond_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v2, p0, LX/7iT;->b:LX/7iM;

    iget-object v2, v2, LX/7iM;->value:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/7iT;->c:LX/7iN;

    iget-object v2, v2, LX/7iN;->value:Ljava/lang/String;

    .line 1227570
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1227571
    move-object v2, v0

    .line 1227572
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1227573
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1227574
    :cond_2
    iget-object v0, p0, LX/7iT;->r:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1227575
    invoke-virtual {v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 1227576
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1227577
    iget-object v0, p0, LX/7iT;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1227578
    :goto_0
    return-void

    .line 1227579
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->d:Ljava/lang/Boolean;

    .line 1227580
    iget-object v0, p0, LX/7iT;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->h:Ljava/lang/Long;

    .line 1227581
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p0, LX/7iT;->b:LX/7iM;

    iget-object v1, v1, LX/7iM;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/7iT;->c:LX/7iN;

    iget-object v1, v1, LX/7iN;->value:Ljava/lang/String;

    .line 1227582
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1227583
    move-object v0, v0

    .line 1227584
    sget-object v1, LX/7iL;->SESSION_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227585
    sget-object v1, LX/7iL;->LOGGER_CREATION_TIME:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->f:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227586
    sget-object v1, LX/7iL;->LOGGING_START_TIME:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->g:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227587
    sget-object v1, LX/7iL;->LOGGING_STOP_TIME:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->h:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227588
    sget-object v1, LX/7iL;->EVENTS:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->q:Ljava/util/ArrayList;

    invoke-static {p0, v2}, LX/7iT;->a(LX/7iT;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227589
    iget v1, p0, LX/7iT;->i:I

    sget-object v2, LX/7iP;->UNKNOWN:LX/7iP;

    invoke-static {v2}, LX/7iR;->a(LX/7iP;)I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 1227590
    sget-object v1, LX/7iL;->REF_TYPE:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget v2, p0, LX/7iT;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227591
    sget-object v1, LX/7iL;->REF_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227592
    :cond_1
    iget-object v1, p0, LX/7iT;->k:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1227593
    sget-object v1, LX/7iL;->PAGE_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227594
    :cond_2
    iget-object v1, p0, LX/7iT;->l:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1227595
    sget-object v1, LX/7iL;->COLLECTION_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227596
    :cond_3
    iget-object v1, p0, LX/7iT;->m:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1227597
    sget-object v1, LX/7iL;->PRODUCT_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227598
    :cond_4
    iget-object v1, p0, LX/7iT;->n:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1227599
    sget-object v1, LX/7iL;->AD_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227600
    :cond_5
    iget-object v1, p0, LX/7iT;->p:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1227601
    sget-object v1, LX/7iL;->POST_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227602
    :cond_6
    iget-object v1, p0, LX/7iT;->o:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1227603
    sget-object v1, LX/7iL;->TOP_LEVEL_POST_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    iget-object v2, p0, LX/7iT;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1227604
    :cond_7
    iget-object v1, p0, LX/7iT;->r:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0
.end method

.method public final c(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 1227605
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7iT;->m:Ljava/lang/String;

    .line 1227606
    return-void
.end method
