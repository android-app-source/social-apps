.class public LX/8Gv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320242
    return-void
.end method

.method public static a(LX/0QB;)LX/8Gv;
    .locals 1

    .prologue
    .line 1320243
    new-instance v0, LX/8Gv;

    invoke-direct {v0}, LX/8Gv;-><init>()V

    .line 1320244
    move-object v0, v0

    .line 1320245
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1320215
    check-cast p1, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1320216
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1320217
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1320218
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "format"

    const-string v5, "json"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320219
    iget-object v0, p1, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1320220
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "existing_album_id"

    iget-object v5, p1, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320221
    iget-object v0, p1, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1320222
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy"

    iget-object v2, p1, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320223
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "create_shared_album"

    .line 1320224
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1320225
    move-object v0, v0

    .line 1320226
    const-string v1, "POST"

    .line 1320227
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1320228
    move-object v0, v0

    .line 1320229
    const-string v1, "me/sharedalbums"

    .line 1320230
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1320231
    move-object v0, v0

    .line 1320232
    iput-object v3, v0, LX/14O;->g:Ljava/util/List;

    .line 1320233
    move-object v0, v0

    .line 1320234
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320235
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320236
    move-object v0, v0

    .line 1320237
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    .line 1320238
    return-object v0

    :cond_0
    move v0, v2

    .line 1320239
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1320240
    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1320212
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320213
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1320214
    return-object v0
.end method
