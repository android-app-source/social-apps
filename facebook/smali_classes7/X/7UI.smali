.class public final LX/7UI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public a:LX/4n9;

.field public b:Landroid/graphics/drawable/Drawable;

.field public c:Ljava/lang/Integer;

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:Ljava/lang/Integer;

.field public f:Landroid/widget/ImageView$ScaleType;

.field public g:Landroid/graphics/Matrix;

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1211709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/4n9;)Z
    .locals 2
    .param p1    # LX/4n9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1211710
    iget-object v0, p0, LX/7UI;->a:LX/4n9;

    .line 1211711
    if-nez p1, :cond_0

    if-nez v0, :cond_0

    .line 1211712
    const/4 v1, 0x1

    .line 1211713
    :goto_0
    move v0, v1

    .line 1211714
    return v0

    .line 1211715
    :cond_0
    if-eqz p1, :cond_1

    if-nez v0, :cond_2

    .line 1211716
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1211717
    :cond_2
    iget-object v1, p1, LX/4n9;->a:Landroid/net/Uri;

    iget-object p0, v0, LX/4n9;->a:Landroid/net/Uri;

    invoke-static {v1, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, LX/4n9;->e:Ljava/lang/String;

    iget-object p0, v0, LX/4n9;->e:Ljava/lang/String;

    invoke-static {v1, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, LX/4n9;->f:LX/4n5;

    iget-object p0, v0, LX/4n9;->f:LX/4n5;

    invoke-static {v1, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1211718
    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1211719
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fetchImageParams"

    iget-object v2, p0, LX/7UI;->a:LX/4n9;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "drawableFromFetchImageParams"

    iget-object v2, p0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "resourceId"

    iget-object v2, p0, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "drawable"

    iget-object v2, p0, LX/7UI;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
