.class public final LX/7gE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/ctamessagesend/CtaMessageSend2MutationModels$CtaMessageSendMutation2Model;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7gH;

.field public final synthetic b:LX/2ms;


# direct methods
.method public constructor <init>(LX/2ms;LX/7gH;)V
    .locals 0

    .prologue
    .line 1223990
    iput-object p1, p0, LX/7gE;->b:LX/2ms;

    iput-object p2, p0, LX/7gE;->a:LX/7gH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1223991
    iget-object v0, p0, LX/7gE;->b:LX/2ms;

    iget-object v0, v0, LX/2ms;->k:LX/03V;

    const-string v1, "MessengerAdsActionHandler"

    const-string v2, "send message graphql mutation failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223992
    iget-object v0, p0, LX/7gE;->a:LX/7gH;

    if-eqz v0, :cond_0

    .line 1223993
    iget-object v0, p0, LX/7gE;->a:LX/7gH;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 1223994
    iget-object v2, v0, LX/7gH;->c:LX/7gI;

    iget-object v2, v2, LX/7gI;->g:LX/6CH;

    const-string v3, "MessengerExtensionActionLinkOnClickListener"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "Failed to create messenger thread due to error:"

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object p0, v0, LX/7gH;->a:Ljava/lang/String;

    iget-object p1, v0, LX/7gH;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, p0, p1}, LX/6CH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1223995
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1223996
    return-void
.end method
