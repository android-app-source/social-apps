.class public LX/7Y6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13n;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/13n;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1218929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1218930
    iput-object p1, p0, LX/7Y6;->a:LX/0Ot;

    .line 1218931
    iput-object p2, p0, LX/7Y6;->b:LX/0Ot;

    .line 1218932
    iput-object p3, p0, LX/7Y6;->c:LX/0Ot;

    .line 1218933
    iput-object p4, p0, LX/7Y6;->d:LX/0Ot;

    .line 1218934
    return-void
.end method

.method public static a$redex0(LX/7Y6;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1218935
    iget-object v0, p0, LX/7Y6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13n;

    .line 1218936
    iget-object v1, v0, LX/13n;->a:Landroid/app/Activity;

    move-object v0, v1

    .line 1218937
    instance-of v1, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_0

    .line 1218938
    new-instance v1, LX/6WI;

    invoke-direct {v1, v0}, LX/6WI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, LX/6WI;->a(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/6WI;->b(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v1

    const v2, 0x7f021170

    invoke-virtual {v1, v2}, LX/6WI;->c(I)LX/6WI;

    move-result-object v1

    const v2, 0x7f080036

    new-instance v3, LX/7Y5;

    invoke-direct {v3, p0}, LX/7Y5;-><init>(LX/7Y6;)V

    invoke-virtual {v1, v2, v3}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v1

    .line 1218939
    new-instance v2, Lcom/facebook/zero/ui/OffpeakVideoInterstitialProvider$3;

    invoke-direct {v2, p0, v1}, Lcom/facebook/zero/ui/OffpeakVideoInterstitialProvider$3;-><init>(LX/7Y6;LX/6WI;)V

    invoke-virtual {v0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1218940
    iget-object v0, p0, LX/7Y6;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0df;->L:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1218941
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "offpeak_download_interstitial_impression"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "zero_module"

    .line 1218942
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1218943
    move-object v1, v0

    .line 1218944
    const-string v2, "carrier_id"

    iget-object v0, p0, LX/7Y6;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1218945
    const-string v0, "title"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1218946
    const-string v0, "desc"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1218947
    iget-object v0, p0, LX/7Y6;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1218948
    :cond_0
    return-void
.end method
