.class public final LX/7B3;
.super LX/7B2;
.source ""


# instance fields
.field public final synthetic e:Lcom/facebook/search/api/GraphSearchQuery;

.field public f:LX/7BH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/103;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/7B5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 1

    .prologue
    .line 1177724
    iput-object p1, p0, LX/7B3;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1177725
    invoke-direct {p0, p1, p2}, LX/7B2;-><init>(LX/7B6;LX/7B6;)V

    .line 1177726
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    iput-object v0, p0, LX/7B3;->f:LX/7BH;

    .line 1177727
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    iput-object v0, p0, LX/7B3;->g:Ljava/lang/String;

    .line 1177728
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    iput-object v0, p0, LX/7B3;->h:Ljava/lang/String;

    .line 1177729
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    iput-object v0, p0, LX/7B3;->i:LX/103;

    .line 1177730
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    iput-object v0, p0, LX/7B3;->j:LX/7B5;

    .line 1177731
    iget-boolean v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    iput-boolean v0, p0, LX/7B3;->k:Z

    .line 1177732
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    iput-object v0, p0, LX/7B3;->l:LX/0P1;

    .line 1177733
    return-void
.end method


# virtual methods
.method public final synthetic a()LX/7B6;
    .locals 1

    .prologue
    .line 1177734
    invoke-virtual {p0}, LX/7B3;->b()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 1

    .prologue
    .line 1177735
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    invoke-direct {v0, p0}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(LX/7B3;)V

    return-object v0
.end method
