.class public final enum LX/7Cl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Cl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Cl;

.field public static final enum FULL_SCREEN_CONTROLLER:LX/7Cl;

.field public static final enum PHOTO:LX/7Cl;

.field public static final enum VIDEO:LX/7Cl;

.field public static final enum VIDEO_NEW_FULL_SCREEN:LX/7Cl;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1181533
    new-instance v0, LX/7Cl;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/7Cl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Cl;->PHOTO:LX/7Cl;

    .line 1181534
    new-instance v0, LX/7Cl;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/7Cl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Cl;->VIDEO:LX/7Cl;

    .line 1181535
    new-instance v0, LX/7Cl;

    const-string v1, "VIDEO_NEW_FULL_SCREEN"

    invoke-direct {v0, v1, v4}, LX/7Cl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Cl;->VIDEO_NEW_FULL_SCREEN:LX/7Cl;

    .line 1181536
    new-instance v0, LX/7Cl;

    const-string v1, "FULL_SCREEN_CONTROLLER"

    invoke-direct {v0, v1, v5}, LX/7Cl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Cl;->FULL_SCREEN_CONTROLLER:LX/7Cl;

    .line 1181537
    const/4 v0, 0x4

    new-array v0, v0, [LX/7Cl;

    sget-object v1, LX/7Cl;->PHOTO:LX/7Cl;

    aput-object v1, v0, v2

    sget-object v1, LX/7Cl;->VIDEO:LX/7Cl;

    aput-object v1, v0, v3

    sget-object v1, LX/7Cl;->VIDEO_NEW_FULL_SCREEN:LX/7Cl;

    aput-object v1, v0, v4

    sget-object v1, LX/7Cl;->FULL_SCREEN_CONTROLLER:LX/7Cl;

    aput-object v1, v0, v5

    sput-object v0, LX/7Cl;->$VALUES:[LX/7Cl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1181538
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Cl;
    .locals 1

    .prologue
    .line 1181539
    const-class v0, LX/7Cl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Cl;

    return-object v0
.end method

.method public static values()[LX/7Cl;
    .locals 1

    .prologue
    .line 1181540
    sget-object v0, LX/7Cl;->$VALUES:[LX/7Cl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Cl;

    return-object v0
.end method
