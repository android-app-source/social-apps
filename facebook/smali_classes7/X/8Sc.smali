.class public LX/8Sc;
.super LX/3Tf;
.source ""


# instance fields
.field public final c:Landroid/view/LayoutInflater;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/621",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/Context;

.field private f:Landroid/view/View$OnClickListener;

.field public g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;)V
    .locals 1

    .prologue
    .line 1346911
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 1346912
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/8Sc;->e:Landroid/content/Context;

    .line 1346913
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LX/8Sc;->c:Landroid/view/LayoutInflater;

    .line 1346914
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1346915
    iput-object v0, p0, LX/8Sc;->d:Ljava/util/List;

    .line 1346916
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1346910
    const/4 v0, 0x0

    return v0
.end method

.method public a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1346903
    if-nez p4, :cond_0

    .line 1346904
    new-instance v2, LX/8Sb;

    iget-object v0, p0, LX/8Sc;->e:Landroid/content/Context;

    invoke-direct {v2, v0}, LX/8Sb;-><init>(Landroid/content/Context;)V

    .line 1346905
    :goto_0
    invoke-virtual {p0, p1, p2}, LX/8Sc;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1346906
    check-cast v1, LX/8Sb;

    iget-object v3, p0, LX/8Sc;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v1, v0, v3}, LX/8Sb;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    .line 1346907
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1346908
    iget-object v0, p0, LX/8Sc;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346909
    return-object v2

    :cond_0
    move-object v2, p4

    goto :goto_0
.end method

.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1346898
    if-nez p2, :cond_0

    .line 1346899
    iget-object v0, p0, LX/8Sc;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03045d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1346900
    :goto_0
    invoke-virtual {p0, p1}, LX/8Sc;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/623;

    move-object v1, v2

    .line 1346901
    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, LX/623;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346902
    return-object v2

    :cond_0
    move-object v2, p2

    goto :goto_0
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1346897
    iget-object v0, p0, LX/8Sc;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/8QJ;)V
    .locals 5

    .prologue
    .line 1346883
    const/4 v0, 0x0

    .line 1346884
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1346885
    new-instance v2, LX/623;

    iget-object v3, p0, LX/8Sc;->e:Landroid/content/Context;

    const v4, 0x7f08130c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, LX/8QJ;->a:LX/0Px;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 1346886
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1346887
    new-instance v2, LX/623;

    iget-object v3, p0, LX/8Sc;->e:Landroid/content/Context;

    const v4, 0x7f08130d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, LX/8QJ;->b:LX/0Px;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 1346888
    invoke-interface {v2}, LX/621;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1346889
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1346890
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/8Sc;->d:Ljava/util/List;

    .line 1346891
    iget-object v1, p0, LX/8Sc;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v1, :cond_1

    .line 1346892
    if-eqz v0, :cond_2

    :goto_0
    iput-object v0, p0, LX/8Sc;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1346893
    :cond_1
    const v1, -0x439753c1

    invoke-static {p0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1346894
    return-void

    .line 1346895
    :cond_2
    iget-object v0, p1, LX/8QJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1346896
    iget-object v0, p0, LX/8Sc;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1346882
    iget-object v0, p0, LX/8Sc;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 1346877
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1346881
    iget-object v0, p0, LX/8Sc;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 1346880
    iget-object v0, p0, LX/8Sc;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 1346879
    const/4 v0, 0x1

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 1346878
    const/4 v0, 0x2

    return v0
.end method
