.class public LX/7ED;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7D0;


# instance fields
.field private final a:LX/5Pc;

.field private b:LX/5Pb;

.field private c:LX/5PR;

.field private d:I

.field private e:I

.field private f:I

.field private g:LX/19o;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1184439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184440
    iput v0, p0, LX/7ED;->d:I

    .line 1184441
    iput v0, p0, LX/7ED;->e:I

    .line 1184442
    iput v0, p0, LX/7ED;->f:I

    .line 1184443
    sget-object v0, LX/19o;->CUBEMAP:LX/19o;

    iput-object v0, p0, LX/7ED;->g:LX/19o;

    .line 1184444
    new-instance v0, LX/5Pd;

    invoke-direct {v0, p1}, LX/5Pd;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/7ED;->a:LX/5Pc;

    .line 1184445
    invoke-direct {p0}, LX/7ED;->f()V

    .line 1184446
    return-void
.end method

.method private f()V
    .locals 13

    .prologue
    .line 1184448
    sget-object v0, LX/7EC;->a:[I

    iget-object v1, p0, LX/7ED;->g:LX/19o;

    invoke-virtual {v1}, LX/19o;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1184449
    invoke-static {}, LX/7D3;->newBuilder()LX/7D2;

    move-result-object v0

    const/4 v12, 0x2

    const/4 v2, 0x0

    const/high16 v11, 0x3f000000    # 0.5f

    const v10, 0x3eaaaaab

    const v9, 0x3ba3d700    # 0.004999995f

    .line 1184450
    const/16 v1, 0x30

    new-array v5, v1, [F

    move v4, v2

    move v1, v2

    .line 1184451
    :goto_0
    if-ge v4, v12, :cond_1

    move v3, v1

    move v1, v2

    .line 1184452
    :goto_1
    const/4 v6, 0x3

    if-ge v1, v6, :cond_0

    .line 1184453
    rsub-int/lit8 v6, v4, 0x1

    .line 1184454
    add-int/lit8 v7, v3, 0x1

    int-to-float v8, v1

    add-float/2addr v8, v9

    mul-float/2addr v8, v10

    aput v8, v5, v3

    .line 1184455
    add-int/lit8 v3, v7, 0x1

    int-to-float v8, v6

    add-float/2addr v8, v9

    mul-float/2addr v8, v11

    aput v8, v5, v7

    .line 1184456
    add-int/lit8 v7, v3, 0x1

    add-int/lit8 v8, v1, 0x1

    int-to-float v8, v8

    sub-float/2addr v8, v9

    mul-float/2addr v8, v10

    aput v8, v5, v3

    .line 1184457
    add-int/lit8 v3, v7, 0x1

    int-to-float v8, v6

    add-float/2addr v8, v9

    mul-float/2addr v8, v11

    aput v8, v5, v7

    .line 1184458
    add-int/lit8 v7, v3, 0x1

    add-int/lit8 v8, v1, 0x1

    int-to-float v8, v8

    sub-float/2addr v8, v9

    mul-float/2addr v8, v10

    aput v8, v5, v3

    .line 1184459
    add-int/lit8 v3, v7, 0x1

    add-int/lit8 v8, v6, 0x1

    int-to-float v8, v8

    sub-float/2addr v8, v9

    mul-float/2addr v8, v11

    aput v8, v5, v7

    .line 1184460
    add-int/lit8 v7, v3, 0x1

    int-to-float v8, v1

    add-float/2addr v8, v9

    mul-float/2addr v8, v10

    aput v8, v5, v3

    .line 1184461
    add-int/lit8 v3, v7, 0x1

    add-int/lit8 v6, v6, 0x1

    int-to-float v6, v6

    sub-float/2addr v6, v9

    mul-float/2addr v6, v11

    aput v6, v5, v7

    .line 1184462
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1184463
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v3

    goto :goto_0

    .line 1184464
    :cond_1
    new-instance v1, LX/5Pg;

    invoke-direct {v1, v5, v12}, LX/5Pg;-><init>([FI)V

    move-object v1, v1

    .line 1184465
    iput-object v1, v0, LX/7D2;->a:LX/5Pg;

    .line 1184466
    move-object v0, v0

    .line 1184467
    invoke-virtual {v0}, LX/7D2;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7ED;->c:LX/5PR;

    .line 1184468
    :goto_2
    return-void

    .line 1184469
    :pswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/7D5;->a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)LX/5PR;

    move-result-object v0

    move-object v0, v0

    .line 1184470
    iput-object v0, p0, LX/7ED;->c:LX/5PR;

    goto :goto_2

    .line 1184471
    :pswitch_1
    invoke-static {}, LX/7D3;->newBuilder()LX/7D2;

    move-result-object v0

    iget v1, p0, LX/7ED;->h:I

    int-to-float v1, v1

    invoke-static {v1}, LX/7Cd;->a(F)LX/5Pg;

    move-result-object v1

    .line 1184472
    iput-object v1, v0, LX/7D2;->a:LX/5Pg;

    .line 1184473
    move-object v0, v0

    .line 1184474
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v6, 0x0

    .line 1184475
    new-instance v2, LX/7Cp;

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, -0x40800000    # -1.0f

    invoke-direct {v2, v3, v6, v6, v4}, LX/7Cp;-><init>(FFFF)V

    .line 1184476
    new-instance v3, LX/7Cp;

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v6, v5, v6}, LX/7Cp;-><init>(FFFF)V

    invoke-virtual {v2, v3}, LX/7Cp;->b(LX/7Cp;)V

    .line 1184477
    move-object v1, v2

    .line 1184478
    iput-object v1, v0, LX/7D2;->b:LX/7Cp;

    .line 1184479
    move-object v0, v0

    .line 1184480
    invoke-virtual {v0}, LX/7D2;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7ED;->c:LX/5PR;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1184447
    iget v0, p0, LX/7ED;->d:I

    return v0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1184438
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1184434
    iget v0, p0, LX/7ED;->h:I

    if-eq p1, v0, :cond_0

    .line 1184435
    iput p1, p0, LX/7ED;->h:I

    .line 1184436
    invoke-direct {p0}, LX/7ED;->f()V

    .line 1184437
    :cond_0
    return-void
.end method

.method public final a(LX/19o;)V
    .locals 1

    .prologue
    .line 1184430
    iget-object v0, p0, LX/7ED;->g:LX/19o;

    if-eq p1, v0, :cond_0

    .line 1184431
    iput-object p1, p0, LX/7ED;->g:LX/19o;

    .line 1184432
    invoke-direct {p0}, LX/7ED;->f()V

    .line 1184433
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1184481
    return-void
.end method

.method public final a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V
    .locals 0

    .prologue
    .line 1184429
    return-void
.end method

.method public final a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V
    .locals 0

    .prologue
    .line 1184428
    return-void
.end method

.method public final a([F[F[FII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1184419
    iget v0, p0, LX/7ED;->e:I

    if-ne p4, v0, :cond_0

    iget v0, p0, LX/7ED;->f:I

    if-eq p5, v0, :cond_1

    .line 1184420
    :cond_0
    invoke-static {v1, v1, p4, p5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1184421
    iput p4, p0, LX/7ED;->e:I

    .line 1184422
    iput p5, p0, LX/7ED;->f:I

    .line 1184423
    :cond_1
    const/16 v0, 0x10

    new-array v0, v0, [F

    move-object v2, p2

    move v3, v1

    move-object v4, p1

    move v5, v1

    .line 1184424
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1184425
    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1184426
    iget-object v1, p0, LX/7ED;->b:LX/5Pb;

    invoke-virtual {v1}, LX/5Pb;->a()LX/5Pa;

    move-result-object v1

    const-string v2, "uMVPMatrix"

    invoke-virtual {v1, v2, v0}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uSTMatrix"

    invoke-virtual {v0, v1, p3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, LX/7ED;->c:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1184427
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const v5, 0x46180400    # 9729.0f

    const/4 v4, 0x0

    const v3, 0x8d65

    .line 1184402
    iget-object v0, p0, LX/7ED;->a:LX/5Pc;

    const v1, 0x7f070093

    const v2, 0x7f070092

    invoke-interface {v0, v1, v2}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7ED;->b:LX/5Pb;

    .line 1184403
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v4, v4, v4, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1184404
    new-array v0, v7, [I

    .line 1184405
    invoke-static {v7, v0, v6}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1184406
    aget v0, v0, v6

    iput v0, p0, LX/7ED;->d:I

    .line 1184407
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 1184408
    iget v0, p0, LX/7ED;->d:I

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1184409
    const-string v0, "glBindTexture mTextureID"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1184410
    const/16 v0, 0x2801

    invoke-static {v3, v0, v5}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1184411
    const/16 v0, 0x2800

    invoke-static {v3, v0, v5}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1184412
    const/16 v0, 0x2802

    const v1, 0x812f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1184413
    const/16 v0, 0x2803

    const v1, 0x812f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1184414
    const-string v0, "glTexParameter"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1184415
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1184416
    const/16 v0, 0x404

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 1184417
    const-string v0, "glCullFace"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1184418
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1184394
    iget v0, p0, LX/7ED;->d:I

    if-eq v0, v2, :cond_0

    .line 1184395
    new-array v0, v4, [I

    .line 1184396
    iget v1, p0, LX/7ED;->d:I

    aput v1, v0, v3

    .line 1184397
    invoke-static {v4, v0, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1184398
    iput v2, p0, LX/7ED;->d:I

    .line 1184399
    :cond_0
    return-void
.end method

.method public final d()LX/7DC;
    .locals 1

    .prologue
    .line 1184401
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/7DD;
    .locals 1

    .prologue
    .line 1184400
    const/4 v0, 0x0

    return-object v0
.end method
