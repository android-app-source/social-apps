.class public final LX/6zw;
.super LX/6E7;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E7;",
        "LX/6vq",
        "<",
        "LX/700;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/TextView;

.field public c:LX/7Tj;

.field public d:LX/700;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1161666
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1161667
    const-class v0, LX/6zw;

    invoke-static {v0, p0}, LX/6zw;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1161668
    const v0, 0x7f0312ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1161669
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/6zw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1161670
    invoke-virtual {p0}, LX/6zw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0571

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1161671
    invoke-virtual {p0}, LX/6zw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b0573

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1161672
    invoke-virtual {p0, v0, v1, v0, v1}, LX/6zw;->setPadding(IIII)V

    .line 1161673
    const v0, 0x7f0d2c12

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/6zw;->b:Landroid/widget/TextView;

    .line 1161674
    iget-object v0, p0, LX/6zw;->a:LX/7Tk;

    invoke-virtual {p0}, LX/6zw;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/7Tk;->a(Landroid/content/Context;Z)LX/7Tj;

    move-result-object v0

    iput-object v0, p0, LX/6zw;->c:LX/7Tj;

    .line 1161675
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/6zw;

    const-class p0, LX/7Tk;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/7Tk;

    iput-object v1, p1, LX/6zw;->a:LX/7Tk;

    return-void
.end method


# virtual methods
.method public final onClick()V
    .locals 0

    .prologue
    .line 1161676
    return-void
.end method
