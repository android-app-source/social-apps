.class public LX/6g6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public A:LX/6ek;

.field public B:Lcom/facebook/messaging/model/messages/MessageDraft;

.field public C:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public D:Z

.field public E:Z

.field public F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

.field public G:J

.field public H:Z

.field public I:I

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;"
        }
    .end annotation
.end field

.field public K:Z

.field public L:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

.field public M:J

.field public N:F

.field public O:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threads/ThreadGameData;",
            ">;"
        }
    .end annotation
.end field

.field public P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

.field public Q:Ljava/lang/String;

.field public R:LX/03R;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:LX/03R;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:I

.field public U:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:J

.field public W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

.field public a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field

.field public j:J

.field public k:J

.field public l:J

.field public m:J

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Landroid/net/Uri;

.field public t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public w:Z

.field public x:Z

.field public y:LX/6g5;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1121261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121262
    iput-wide v2, p0, LX/6g6;->d:J

    .line 1121263
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1121264
    iput-object v0, p0, LX/6g6;->h:Ljava/util/List;

    .line 1121265
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1121266
    iput-object v0, p0, LX/6g6;->i:Ljava/util/List;

    .line 1121267
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1121268
    iput-object v0, p0, LX/6g6;->n:Ljava/util/List;

    .line 1121269
    sget-object v0, LX/6g5;->NONE:LX/6g5;

    iput-object v0, p0, LX/6g6;->y:LX/6g5;

    .line 1121270
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    iput-object v0, p0, LX/6g6;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1121271
    iput-wide v2, p0, LX/6g6;->G:J

    .line 1121272
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1121273
    iput-object v0, p0, LX/6g6;->J:Ljava/util/List;

    .line 1121274
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1121275
    iput-object v0, p0, LX/6g6;->O:Ljava/util/Map;

    .line 1121276
    invoke-static {}, Lcom/facebook/messaging/model/threads/RoomThreadData;->newBuilder()LX/6fm;

    move-result-object v0

    invoke-virtual {v0}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v0

    iput-object v0, p0, LX/6g6;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    .line 1121277
    return-void
.end method


# virtual methods
.method public final Y()Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1

    .prologue
    .line 1121278
    iget-object v0, p0, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    if-nez v0, :cond_0

    .line 1121279
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iput-object v0, p0, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1121280
    :cond_0
    iget-object v0, p0, LX/6g6;->P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    if-nez v0, :cond_1

    .line 1121281
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->a:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    iput-object v0, p0, LX/6g6;->P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1121282
    :cond_1
    iget-object v0, p0, LX/6g6;->S:LX/03R;

    if-nez v0, :cond_2

    .line 1121283
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6g6;->S:LX/03R;

    .line 1121284
    :cond_2
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/ThreadSummary;-><init>(LX/6g6;)V

    return-object v0
.end method

.method public final a(F)LX/6g6;
    .locals 0

    .prologue
    .line 1121285
    iput p1, p0, LX/6g6;->N:F

    .line 1121286
    return-object p0
.end method

.method public final a(I)LX/6g6;
    .locals 0

    .prologue
    .line 1121287
    iput p1, p0, LX/6g6;->I:I

    .line 1121288
    return-object p0
.end method

.method public final a(LX/03R;)LX/6g6;
    .locals 0

    .prologue
    .line 1121289
    iput-object p1, p0, LX/6g6;->S:LX/03R;

    .line 1121290
    return-object p0
.end method

.method public final a(LX/6ek;)LX/6g6;
    .locals 0

    .prologue
    .line 1121291
    iput-object p1, p0, LX/6g6;->A:LX/6ek;

    .line 1121292
    return-object p0
.end method

.method public final a(LX/6g5;)LX/6g6;
    .locals 0

    .prologue
    .line 1121293
    iput-object p1, p0, LX/6g6;->y:LX/6g5;

    .line 1121294
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/6g6;
    .locals 0

    .prologue
    .line 1121295
    iput-object p1, p0, LX/6g6;->s:Landroid/net/Uri;

    .line 1121296
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;)LX/6g6;
    .locals 0

    .prologue
    .line 1121297
    iput-object p1, p0, LX/6g6;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 1121298
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)LX/6g6;
    .locals 0

    .prologue
    .line 1121299
    iput-object p1, p0, LX/6g6;->L:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1121300
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6g6;
    .locals 0

    .prologue
    .line 1121301
    iput-object p1, p0, LX/6g6;->p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1121302
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6g6;
    .locals 0

    .prologue
    .line 1121303
    iput-object p1, p0, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1121304
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/NotificationSetting;)LX/6g6;
    .locals 0

    .prologue
    .line 1121305
    iput-object p1, p0, LX/6g6;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1121306
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;
    .locals 1

    .prologue
    .line 1121307
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/RoomThreadData;

    iput-object v0, p0, LX/6g6;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    .line 1121308
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/6g6;
    .locals 0

    .prologue
    .line 1121309
    iput-object p1, p0, LX/6g6;->U:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    .line 1121310
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/6g6;
    .locals 0

    .prologue
    .line 1121311
    iput-object p1, p0, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1121312
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadMediaPreview;)LX/6g6;
    .locals 0
    .param p1    # Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1121313
    iput-object p1, p0, LX/6g6;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    .line 1121314
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;)LX/6g6;
    .locals 0

    .prologue
    .line 1121315
    iput-object p1, p0, LX/6g6;->P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1121316
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;
    .locals 2

    .prologue
    .line 1121317
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1121318
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->b:Ljava/lang/String;

    iput-object v0, p0, LX/6g6;->b:Ljava/lang/String;

    .line 1121319
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    iput-wide v0, p0, LX/6g6;->c:J

    .line 1121320
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    iput-wide v0, p0, LX/6g6;->d:J

    .line 1121321
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->e:J

    iput-wide v0, p0, LX/6g6;->e:J

    .line 1121322
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    iput-wide v0, p0, LX/6g6;->f:J

    .line 1121323
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    iput-object v0, p0, LX/6g6;->g:Ljava/lang/String;

    .line 1121324
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    iput-object v0, p0, LX/6g6;->h:Ljava/util/List;

    .line 1121325
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    iput-object v0, p0, LX/6g6;->i:Ljava/util/List;

    .line 1121326
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    iput-wide v0, p0, LX/6g6;->j:J

    .line 1121327
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->O:J

    iput-wide v0, p0, LX/6g6;->M:J

    .line 1121328
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    iput-wide v0, p0, LX/6g6;->k:J

    .line 1121329
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->l:J

    iput-wide v0, p0, LX/6g6;->l:J

    .line 1121330
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->m:J

    iput-wide v0, p0, LX/6g6;->m:J

    .line 1121331
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    iput-object v0, p0, LX/6g6;->n:Ljava/util/List;

    .line 1121332
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->o:Ljava/lang/String;

    iput-object v0, p0, LX/6g6;->o:Ljava/lang/String;

    .line 1121333
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, LX/6g6;->p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1121334
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->p:Ljava/lang/String;

    iput-object v0, p0, LX/6g6;->q:Ljava/lang/String;

    .line 1121335
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->r:Ljava/lang/String;

    iput-object v0, p0, LX/6g6;->r:Ljava/lang/String;

    .line 1121336
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->s:Landroid/net/Uri;

    iput-object v0, p0, LX/6g6;->s:Landroid/net/Uri;

    .line 1121337
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->u:Z

    iput-boolean v0, p0, LX/6g6;->u:Z

    .line 1121338
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    iput-object v0, p0, LX/6g6;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 1121339
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    iput-boolean v0, p0, LX/6g6;->w:Z

    .line 1121340
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->x:Z

    iput-boolean v0, p0, LX/6g6;->x:Z

    .line 1121341
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->y:LX/6g5;

    iput-object v0, p0, LX/6g6;->y:LX/6g5;

    .line 1121342
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->z:Z

    iput-boolean v0, p0, LX/6g6;->z:Z

    .line 1121343
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    iput-object v0, p0, LX/6g6;->A:LX/6ek;

    .line 1121344
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    iput-object v0, p0, LX/6g6;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 1121345
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    iput-object v0, p0, LX/6g6;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1121346
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iput-object v0, p0, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1121347
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->I:J

    iput-wide v0, p0, LX/6g6;->G:J

    .line 1121348
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->J:Z

    iput-boolean v0, p0, LX/6g6;->H:Z

    .line 1121349
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    iput v0, p0, LX/6g6;->I:I

    .line 1121350
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    iput-object v0, p0, LX/6g6;->J:Ljava/util/List;

    .line 1121351
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->M:Z

    iput-boolean v0, p0, LX/6g6;->K:Z

    .line 1121352
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->N:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iput-object v0, p0, LX/6g6;->L:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1121353
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->P:F

    iput v0, p0, LX/6g6;->N:F

    .line 1121354
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Q:LX/0P1;

    iput-object v0, p0, LX/6g6;->O:Ljava/util/Map;

    .line 1121355
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    iput-object v0, p0, LX/6g6;->P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1121356
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->R:Ljava/lang/String;

    iput-object v0, p0, LX/6g6;->Q:Ljava/lang/String;

    .line 1121357
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->S:LX/03R;

    iput-object v0, p0, LX/6g6;->R:LX/03R;

    .line 1121358
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->T:LX/03R;

    iput-object v0, p0, LX/6g6;->S:LX/03R;

    .line 1121359
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->U:I

    iput v0, p0, LX/6g6;->T:I

    .line 1121360
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    iput-object v0, p0, LX/6g6;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    .line 1121361
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iput-object v0, p0, LX/6g6;->U:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    .line 1121362
    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->H:J

    iput-wide v0, p0, LX/6g6;->V:J

    .line 1121363
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    iput-object v0, p0, LX/6g6;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 1121364
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, LX/6g6;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1121365
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iput-object v0, p0, LX/6g6;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    .line 1121366
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/6g6;
    .locals 0

    .prologue
    .line 1121255
    iput-object p1, p0, LX/6g6;->b:Ljava/lang/String;

    .line 1121256
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/6g6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;)",
            "LX/6g6;"
        }
    .end annotation

    .prologue
    .line 1121367
    iput-object p1, p0, LX/6g6;->h:Ljava/util/List;

    .line 1121368
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/6g6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threads/ThreadGameData;",
            ">;)",
            "LX/6g6;"
        }
    .end annotation

    .prologue
    .line 1121257
    iput-object p1, p0, LX/6g6;->O:Ljava/util/Map;

    .line 1121258
    return-object p0
.end method

.method public final a(Z)LX/6g6;
    .locals 0

    .prologue
    .line 1121259
    iput-boolean p1, p0, LX/6g6;->u:Z

    .line 1121260
    return-object p0
.end method

.method public final b(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121213
    iput-wide p1, p0, LX/6g6;->f:J

    .line 1121214
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/6g6;
    .locals 0

    .prologue
    .line 1121215
    iput-object p1, p0, LX/6g6;->g:Ljava/lang/String;

    .line 1121216
    return-object p0
.end method

.method public final b(Ljava/util/List;)LX/6g6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;)",
            "LX/6g6;"
        }
    .end annotation

    .prologue
    .line 1121217
    iput-object p1, p0, LX/6g6;->i:Ljava/util/List;

    .line 1121218
    return-object p0
.end method

.method public final b(Z)LX/6g6;
    .locals 0

    .prologue
    .line 1121219
    iput-boolean p1, p0, LX/6g6;->w:Z

    .line 1121220
    return-object p0
.end method

.method public final c(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121221
    iput-wide p1, p0, LX/6g6;->c:J

    .line 1121222
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/6g6;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1121223
    iput-object p1, p0, LX/6g6;->o:Ljava/lang/String;

    .line 1121224
    return-object p0
.end method

.method public final c(Ljava/util/List;)LX/6g6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;)",
            "LX/6g6;"
        }
    .end annotation

    .prologue
    .line 1121225
    iput-object p1, p0, LX/6g6;->n:Ljava/util/List;

    .line 1121226
    return-object p0
.end method

.method public final c(Z)LX/6g6;
    .locals 0

    .prologue
    .line 1121227
    iput-boolean p1, p0, LX/6g6;->x:Z

    .line 1121228
    return-object p0
.end method

.method public final d(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121229
    iput-wide p1, p0, LX/6g6;->d:J

    .line 1121230
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/6g6;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1121231
    iput-object p1, p0, LX/6g6;->q:Ljava/lang/String;

    .line 1121232
    return-object p0
.end method

.method public final d(Ljava/util/List;)LX/6g6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;)",
            "LX/6g6;"
        }
    .end annotation

    .prologue
    .line 1121233
    iput-object p1, p0, LX/6g6;->J:Ljava/util/List;

    .line 1121234
    return-object p0
.end method

.method public final d(Z)LX/6g6;
    .locals 0

    .prologue
    .line 1121235
    iput-boolean p1, p0, LX/6g6;->z:Z

    .line 1121236
    return-object p0
.end method

.method public final e(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121237
    iput-wide p1, p0, LX/6g6;->j:J

    .line 1121238
    return-object p0
.end method

.method public final e(Z)LX/6g6;
    .locals 0

    .prologue
    .line 1121239
    iput-boolean p1, p0, LX/6g6;->D:Z

    .line 1121240
    return-object p0
.end method

.method public final f(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121241
    iput-wide p1, p0, LX/6g6;->k:J

    .line 1121242
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/6g6;
    .locals 0

    .prologue
    .line 1121243
    iput-object p1, p0, LX/6g6;->Q:Ljava/lang/String;

    .line 1121244
    return-object p0
.end method

.method public final f(Z)LX/6g6;
    .locals 0

    .prologue
    .line 1121245
    iput-boolean p1, p0, LX/6g6;->E:Z

    .line 1121246
    return-object p0
.end method

.method public final g(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121247
    iput-wide p1, p0, LX/6g6;->l:J

    .line 1121248
    return-object p0
.end method

.method public final h(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121249
    iput-wide p1, p0, LX/6g6;->m:J

    .line 1121250
    return-object p0
.end method

.method public final h(Z)LX/6g6;
    .locals 0

    .prologue
    .line 1121251
    iput-boolean p1, p0, LX/6g6;->K:Z

    .line 1121252
    return-object p0
.end method

.method public final j(J)LX/6g6;
    .locals 1

    .prologue
    .line 1121253
    iput-wide p1, p0, LX/6g6;->M:J

    .line 1121254
    return-object p0
.end method
