.class public final LX/6ye;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yL;


# instance fields
.field public a:LX/6qh;

.field public final b:Landroid/content/Context;

.field public final c:Ljava/util/concurrent/Executor;

.field private final d:LX/03V;

.field private final e:LX/0Zb;

.field private final f:LX/6yZ;

.field public final g:LX/6yt;

.field public final h:LX/6xb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/03V;LX/0Zb;LX/6yZ;LX/6yt;LX/6xb;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160299
    iput-object p1, p0, LX/6ye;->b:Landroid/content/Context;

    .line 1160300
    iput-object p2, p0, LX/6ye;->c:Ljava/util/concurrent/Executor;

    .line 1160301
    iput-object p3, p0, LX/6ye;->d:LX/03V;

    .line 1160302
    iput-object p4, p0, LX/6ye;->e:LX/0Zb;

    .line 1160303
    iput-object p5, p0, LX/6ye;->f:LX/6yZ;

    .line 1160304
    iput-object p6, p0, LX/6ye;->g:LX/6yt;

    .line 1160305
    iput-object p7, p0, LX/6ye;->h:LX/6xb;

    .line 1160306
    return-void
.end method

.method public static a$redex0(LX/6ye;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1160289
    invoke-virtual {p0, p1}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;)V

    .line 1160290
    iget-object v0, p0, LX/6ye;->a:LX/6qh;

    if-eqz v0, :cond_0

    .line 1160291
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1160292
    const-string v1, "encoded_credential_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1160293
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1160294
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1160295
    new-instance v0, LX/73T;

    sget-object v2, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v0, v2, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1160296
    iget-object v1, p0, LX/6ye;->a:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(LX/73T;)V

    .line 1160297
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/6ye;
    .locals 8

    .prologue
    .line 1160170
    new-instance v0, LX/6ye;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/6yZ;->a(LX/0QB;)LX/6yZ;

    move-result-object v5

    check-cast v5, LX/6yZ;

    invoke-static {p0}, LX/6yt;->a(LX/0QB;)LX/6yt;

    move-result-object v6

    check-cast v6, LX/6yt;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v7

    check-cast v7, LX/6xb;

    invoke-direct/range {v0 .. v7}, LX/6ye;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/03V;LX/0Zb;LX/6yZ;LX/6yt;LX/6xb;)V

    .line 1160171
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 1160229
    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    if-nez v0, :cond_0

    .line 1160230
    new-instance v0, LX/6z0;

    invoke-direct {v0}, LX/6z0;-><init>()V

    move-object v0, v0

    .line 1160231
    iget-object v1, p2, LX/6y8;->a:Ljava/lang/String;

    .line 1160232
    iput-object v1, v0, LX/6z0;->a:Ljava/lang/String;

    .line 1160233
    move-object v0, v0

    .line 1160234
    iget v1, p2, LX/6y8;->c:I

    .line 1160235
    iput v1, v0, LX/6yz;->c:I

    .line 1160236
    move-object v0, v0

    .line 1160237
    check-cast v0, LX/6z0;

    iget v1, p2, LX/6y8;->d:I

    .line 1160238
    iput v1, v0, LX/6yz;->d:I

    .line 1160239
    move-object v0, v0

    .line 1160240
    check-cast v0, LX/6z0;

    iget-object v1, p2, LX/6y8;->e:Ljava/lang/String;

    .line 1160241
    iput-object v1, v0, LX/6yz;->b:Ljava/lang/String;

    .line 1160242
    move-object v0, v0

    .line 1160243
    check-cast v0, LX/6z0;

    iget-object v1, p2, LX/6y8;->f:Ljava/lang/String;

    .line 1160244
    iput-object v1, v0, LX/6yz;->e:Ljava/lang/String;

    .line 1160245
    move-object v0, v0

    .line 1160246
    check-cast v0, LX/6z0;

    iget-object v1, p2, LX/6y8;->g:Lcom/facebook/common/locale/Country;

    invoke-virtual {v1}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v1

    .line 1160247
    iput-object v1, v0, LX/6yz;->f:Ljava/lang/String;

    .line 1160248
    move-object v0, v0

    .line 1160249
    check-cast v0, LX/6z0;

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->c:LX/6xg;

    .line 1160250
    iput-object v1, v0, LX/6yz;->a:LX/6xg;

    .line 1160251
    move-object v0, v0

    .line 1160252
    check-cast v0, LX/6z0;

    .line 1160253
    new-instance v1, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;

    invoke-direct {v1, v0}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;-><init>(LX/6z0;)V

    move-object v0, v1

    .line 1160254
    iget-object v1, p0, LX/6ye;->g:LX/6yt;

    .line 1160255
    iget-object v2, v1, LX/6yt;->a:LX/6yv;

    invoke-virtual {v2, v0}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1160256
    new-instance v1, LX/6yc;

    invoke-direct {v1, p0, p1}, LX/6yc;-><init>(LX/6ye;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)V

    iget-object v2, p0, LX/6ye;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1160257
    move-object v0, v0

    .line 1160258
    :goto_0
    return-object v0

    .line 1160259
    :cond_0
    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1160260
    new-instance v0, LX/6z3;

    invoke-direct {v0}, LX/6z3;-><init>()V

    move-object v0, v0

    .line 1160261
    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v1

    .line 1160262
    iput-object v1, v0, LX/6z3;->a:Ljava/lang/String;

    .line 1160263
    move-object v0, v0

    .line 1160264
    iget v1, p2, LX/6y8;->c:I

    .line 1160265
    iput v1, v0, LX/6yz;->c:I

    .line 1160266
    move-object v0, v0

    .line 1160267
    check-cast v0, LX/6z3;

    iget v1, p2, LX/6y8;->d:I

    .line 1160268
    iput v1, v0, LX/6yz;->d:I

    .line 1160269
    move-object v0, v0

    .line 1160270
    check-cast v0, LX/6z3;

    iget-object v1, p2, LX/6y8;->e:Ljava/lang/String;

    .line 1160271
    iput-object v1, v0, LX/6yz;->b:Ljava/lang/String;

    .line 1160272
    move-object v0, v0

    .line 1160273
    check-cast v0, LX/6z3;

    iget-object v1, p2, LX/6y8;->f:Ljava/lang/String;

    .line 1160274
    iput-object v1, v0, LX/6yz;->e:Ljava/lang/String;

    .line 1160275
    move-object v0, v0

    .line 1160276
    check-cast v0, LX/6z3;

    iget-object v1, p2, LX/6y8;->g:Lcom/facebook/common/locale/Country;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/locale/Country;

    invoke-virtual {v1}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v1

    .line 1160277
    iput-object v1, v0, LX/6yz;->f:Ljava/lang/String;

    .line 1160278
    move-object v0, v0

    .line 1160279
    check-cast v0, LX/6z3;

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->c:LX/6xg;

    .line 1160280
    iput-object v1, v0, LX/6yz;->a:LX/6xg;

    .line 1160281
    move-object v0, v0

    .line 1160282
    check-cast v0, LX/6z3;

    .line 1160283
    new-instance v1, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;

    invoke-direct {v1, v0}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;-><init>(LX/6z3;)V

    move-object v0, v1

    .line 1160284
    iget-object v1, p0, LX/6ye;->g:LX/6yt;

    .line 1160285
    iget-object p2, v1, LX/6yt;->b:LX/6yw;

    invoke-virtual {p2, v0}, LX/6u5;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    move-object v0, p2

    .line 1160286
    new-instance v1, LX/6yd;

    invoke-direct {v1, p0, p1, v2}, LX/6yd;-><init>(LX/6ye;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V

    iget-object v2, p0, LX/6ye;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1160287
    move-object v0, v0

    .line 1160288
    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 1160219
    const-string v0, "extra_mutation"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1160220
    const-string v1, "action_delete_payment_card"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1160221
    invoke-virtual {p0, p1}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)V

    .line 1160222
    const-string v0, "extra_fb_payment_card"

    invoke-virtual {p2, v0}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1160223
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1160224
    iget-object v1, p0, LX/6ye;->g:LX/6yt;

    new-instance v2, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;-><init>(Ljava/lang/String;)V

    .line 1160225
    iget-object v3, v1, LX/6yt;->c:LX/6yx;

    invoke-virtual {v3, v2}, LX/6u5;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 1160226
    new-instance v2, LX/6ya;

    invoke-direct {v2, p0, p1, v0}, LX/6ya;-><init>(LX/6ye;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V

    iget-object v0, p0, LX/6ye;->c:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1160227
    move-object v0, v1

    .line 1160228
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1160218
    iget-object v0, p0, LX/6ye;->b:Landroid/content/Context;

    const v1, 0x7f081e80

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2Oo;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1160210
    iget-object v0, p0, LX/6ye;->d:LX/03V;

    const-class v1, LX/6ye;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Attempted to delete a fbpaymentcard, but received a response with an error"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1160211
    iget-object v0, p0, LX/6ye;->f:LX/6yZ;

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v1}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v0

    invoke-interface {v0, p2}, LX/6xu;->i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    .line 1160212
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1160213
    iget-object v2, p0, LX/6ye;->e:LX/0Zb;

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/6xt;

    move-result-object v0

    invoke-interface {p3}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/6xt;->a(Ljava/lang/String;)LX/6xt;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/6xt;->b(Ljava/lang/String;)LX/6xt;

    move-result-object v0

    .line 1160214
    iget-object v3, v0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-object v0, v3

    .line 1160215
    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1160216
    new-instance v0, LX/31Y;

    iget-object v2, p0, LX/6ye;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/6yb;

    invoke-direct {v2, p0}, LX/6yb;-><init>(LX/6ye;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1160217
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1160208
    iput-object p1, p0, LX/6ye;->a:LX/6qh;

    .line 1160209
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;)V
    .locals 4

    .prologue
    .line 1160206
    iget-object v0, p0, LX/6ye;->h:LX/6xb;

    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    const-string v3, "payflows_success"

    invoke-virtual {v0, v1, v2, v3}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1160207
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1160188
    iget-object v0, p0, LX/6ye;->h:LX/6xb;

    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    invoke-virtual {v0, v1, v2, p2}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/Throwable;)V

    .line 1160189
    const-class v0, LX/2Oo;

    invoke-static {p2, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 1160190
    if-nez v0, :cond_0

    .line 1160191
    iget-object v0, p0, LX/6ye;->b:Landroid/content/Context;

    invoke-static {v0, p2}, LX/6up;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    .line 1160192
    :goto_0
    return-void

    .line 1160193
    :cond_0
    iget-object v1, p0, LX/6ye;->b:Landroid/content/Context;

    const v2, 0x7f081e7b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1160194
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1160195
    iget-object v2, p0, LX/6ye;->a:LX/6qh;

    if-nez v2, :cond_1

    .line 1160196
    :goto_1
    goto :goto_0

    .line 1160197
    :cond_1
    new-instance v2, LX/6dy;

    iget-object p1, p0, LX/6ye;->b:Landroid/content/Context;

    const p2, 0x7f080016

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v1, p1}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160198
    iput-object v0, v2, LX/6dy;->d:Ljava/lang/String;

    .line 1160199
    move-object v2, v2

    .line 1160200
    const/4 p1, 0x1

    .line 1160201
    iput-boolean p1, v2, LX/6dy;->f:Z

    .line 1160202
    move-object v2, v2

    .line 1160203
    invoke-virtual {v2}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v2

    .line 1160204
    invoke-static {v2}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->b(Lcom/facebook/messaging/dialog/ConfirmActionParams;)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v2

    .line 1160205
    iget-object p1, p0, LX/6ye;->a:LX/6qh;

    invoke-virtual {p1, v2}, LX/6qh;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)V
    .locals 3

    .prologue
    .line 1160185
    iget-object v0, p0, LX/6ye;->f:LX/6yZ;

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v1}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v0

    invoke-interface {v0, p1}, LX/6xu;->g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    .line 1160186
    iget-object v1, p0, LX/6ye;->e:LX/0Zb;

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1160187
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V
    .locals 3

    .prologue
    .line 1160179
    iget-object v0, p0, LX/6ye;->f:LX/6yZ;

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v1}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v0

    invoke-interface {v0, p1}, LX/6xu;->h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    .line 1160180
    iget-object v1, p0, LX/6ye;->e:LX/0Zb;

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/6xt;

    move-result-object v0

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6xt;->a(Ljava/lang/String;)LX/6xt;

    move-result-object v0

    .line 1160181
    iget-object v2, v0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-object v0, v2

    .line 1160182
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1160183
    iget-object v0, p0, LX/6ye;->a:LX/6qh;

    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v1, v2}, LX/73T;-><init>(LX/73S;)V

    invoke-virtual {v0, v1}, LX/6qh;->a(LX/73T;)V

    .line 1160184
    return-void
.end method

.method public final b(Ljava/lang/Throwable;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V
    .locals 3

    .prologue
    .line 1160172
    iget-object v0, p0, LX/6ye;->d:LX/03V;

    const-class v1, LX/6ye;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Attempted to delete a fbpaymentcard, but received a response with an error"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1160173
    iget-object v0, p0, LX/6ye;->f:LX/6yZ;

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v1}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v0

    invoke-interface {v0, p2}, LX/6xu;->i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    .line 1160174
    iget-object v1, p0, LX/6ye;->e:LX/0Zb;

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/6xt;

    move-result-object v0

    invoke-interface {p3}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6xt;->a(Ljava/lang/String;)LX/6xt;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6xt;->b(Ljava/lang/String;)LX/6xt;

    move-result-object v0

    .line 1160175
    iget-object v2, v0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-object v0, v2

    .line 1160176
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1160177
    iget-object v0, p0, LX/6ye;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/6up;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    .line 1160178
    return-void
.end method
