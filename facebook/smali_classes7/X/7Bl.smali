.class public LX/7Bl;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7Bl;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179600
    const-string v0, "search_bootstrap_pull_schedule"

    const/4 v1, 0x1

    new-instance v2, LX/7Bk;

    invoke-direct {v2}, LX/7Bk;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1179601
    return-void
.end method

.method public static a(LX/0QB;)LX/7Bl;
    .locals 3

    .prologue
    .line 1179602
    sget-object v0, LX/7Bl;->a:LX/7Bl;

    if-nez v0, :cond_1

    .line 1179603
    const-class v1, LX/7Bl;

    monitor-enter v1

    .line 1179604
    :try_start_0
    sget-object v0, LX/7Bl;->a:LX/7Bl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1179605
    if-eqz v2, :cond_0

    .line 1179606
    :try_start_1
    new-instance v0, LX/7Bl;

    invoke-direct {v0}, LX/7Bl;-><init>()V

    .line 1179607
    move-object v0, v0

    .line 1179608
    sput-object v0, LX/7Bl;->a:LX/7Bl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1179609
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1179610
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1179611
    :cond_1
    sget-object v0, LX/7Bl;->a:LX/7Bl;

    return-object v0

    .line 1179612
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1179613
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
