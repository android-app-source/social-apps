.class public LX/8LU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Z


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1332630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1332631
    iput-wide p1, p0, LX/8LU;->a:J

    .line 1332632
    iput-wide p1, p0, LX/8LU;->b:J

    .line 1332633
    iput-wide p1, p0, LX/8LU;->c:J

    .line 1332634
    iput v0, p0, LX/8LU;->d:I

    .line 1332635
    iput v0, p0, LX/8LU;->e:I

    .line 1332636
    iput v0, p0, LX/8LU;->f:I

    .line 1332637
    iput v0, p0, LX/8LU;->g:I

    .line 1332638
    iput v0, p0, LX/8LU;->h:I

    .line 1332639
    iput v0, p0, LX/8LU;->i:I

    .line 1332640
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8LU;->j:Z

    .line 1332641
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1332642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1332643
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/8LU;->a:J

    .line 1332644
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/8LU;->b:J

    .line 1332645
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/8LU;->c:J

    .line 1332646
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, LX/8LU;->d:I

    .line 1332647
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, LX/8LU;->e:I

    .line 1332648
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, LX/8LU;->f:I

    .line 1332649
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, LX/8LU;->g:I

    .line 1332650
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, LX/8LU;->h:I

    .line 1332651
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, LX/8LU;->i:I

    .line 1332652
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, LX/8LU;->j:Z

    .line 1332653
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1332627
    iget v0, p0, LX/8LU;->e:I

    iget v1, p0, LX/8LU;->d:I

    add-int/2addr v0, v1

    iput v0, p0, LX/8LU;->e:I

    .line 1332628
    const/4 v0, 0x0

    iput v0, p0, LX/8LU;->d:I

    .line 1332629
    return-void
.end method
