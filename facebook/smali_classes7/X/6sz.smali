.class public final LX/6sz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/6t3;


# direct methods
.method public constructor <init>(LX/6t3;I)V
    .locals 0

    .prologue
    .line 1153914
    iput-object p1, p0, LX/6sz;->b:LX/6t3;

    iput p2, p0, LX/6sz;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x68e8e15a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1153915
    iget-object v1, p0, LX/6sz;->b:LX/6t3;

    iget v2, p0, LX/6sz;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1153916
    iput-object v2, v1, LX/6t3;->b:Ljava/lang/Integer;

    .line 1153917
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1153918
    const-string v2, "extra_mutation"

    const-string v3, "mutation_selected_price"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153919
    const-string v2, "selected_price_index"

    iget-object v3, p0, LX/6sz;->b:LX/6t3;

    iget-object v3, v3, LX/6t3;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1153920
    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->MUTATION:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1153921
    iget-object v1, p0, LX/6sz;->b:LX/6t3;

    iget-object v1, v1, LX/6t3;->d:LX/6qh;

    invoke-virtual {v1, v2}, LX/6qh;->a(LX/73T;)V

    .line 1153922
    const v1, -0x53ac2e7a

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
