.class public LX/732;
.super LX/6u5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6u5",
        "<",
        "Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165170
    const-class v0, LX/732;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/732;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165171
    invoke-direct {p0, p1}, LX/6u5;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1165172
    return-void
.end method

.method public static b(LX/0QB;)LX/732;
    .locals 2

    .prologue
    .line 1165173
    new-instance v1, LX/732;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/732;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1165174
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1165139
    check-cast p1, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;

    .line 1165140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1165141
    iget-object v0, p1, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    if-eqz v0, :cond_0

    .line 1165142
    iget-object v0, p1, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    .line 1165143
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "addressee"

    iget-object v4, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165144
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "label"

    iget-object v4, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165145
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "street"

    iget-object v4, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165146
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "building"

    iget-object v4, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165147
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "city"

    iget-object v4, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165148
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "state"

    iget-object v4, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->f:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165149
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "postal_code"

    iget-object v4, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->g:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165150
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "country_code"

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->h:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165151
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->c:Z

    if-eqz v0, :cond_1

    .line 1165152
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "default"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165153
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "DELETE"

    .line 1165154
    :goto_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    sget-object v3, LX/732;->c:Ljava/lang/String;

    .line 1165155
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1165156
    move-object v2, v2

    .line 1165157
    iput-object v0, v2, LX/14O;->c:Ljava/lang/String;

    .line 1165158
    move-object v0, v2

    .line 1165159
    const-string v2, "%d"

    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1165160
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1165161
    move-object v0, v0

    .line 1165162
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1165163
    move-object v0, v0

    .line 1165164
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1165165
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1165166
    move-object v0, v0

    .line 1165167
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1165168
    :cond_2
    const-string v0, "POST"

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1165169
    const-string v0, "edit_mailing_address"

    return-object v0
.end method
