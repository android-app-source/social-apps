.class public final LX/8NJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:F

.field private B:F

.field private C:LX/5Rn;

.field private D:J

.field public E:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/PostChannelFeedbackState;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:J

.field private N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Z

.field private P:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:Z

.field private T:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field private U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

.field private b:J

.field public c:J

.field private d:J

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:LX/434;

.field private t:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private u:Z

.field private v:J

.field private w:Lcom/facebook/share/model/ComposerAppAttribution;

.field private x:Z

.field private y:Z

.field private z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V
    .locals 2

    .prologue
    .line 1336736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1336737
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/8NJ;->d:J

    .line 1336738
    sget-object v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iput-object v0, p0, LX/8NJ;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1336739
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1336740
    iput-object v0, p0, LX/8NJ;->I:LX/0Px;

    .line 1336741
    iput-object p1, p0, LX/8NJ;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    .line 1336742
    sget-object v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, LX/8NJ;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1336743
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8NJ;->k:Z

    .line 1336744
    return-void
.end method


# virtual methods
.method public final a(F)LX/8NJ;
    .locals 0

    .prologue
    .line 1336717
    iput p1, p0, LX/8NJ;->A:F

    .line 1336718
    return-object p0
.end method

.method public final a(I)LX/8NJ;
    .locals 0

    .prologue
    .line 1336719
    iput p1, p0, LX/8NJ;->r:I

    .line 1336720
    return-object p0
.end method

.method public final a(J)LX/8NJ;
    .locals 1

    .prologue
    .line 1336721
    iput-wide p1, p0, LX/8NJ;->c:J

    .line 1336722
    return-object p0
.end method

.method public final a(LX/0Px;)LX/8NJ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/8NJ;"
        }
    .end annotation

    .prologue
    .line 1336723
    iput-object p1, p0, LX/8NJ;->l:LX/0Px;

    .line 1336724
    return-object p0
.end method

.method public final a(LX/434;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336725
    iput-object p1, p0, LX/8NJ;->s:LX/434;

    .line 1336726
    return-object p0
.end method

.method public final a(LX/5Rn;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336727
    iput-object p1, p0, LX/8NJ;->C:LX/5Rn;

    .line 1336728
    return-object p0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8NJ;
    .locals 1

    .prologue
    .line 1336729
    if-eqz p1, :cond_0

    .line 1336730
    iget-boolean v0, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v0

    .line 1336731
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1336732
    :cond_0
    iput-object p1, p0, LX/8NJ;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1336733
    return-object p0
.end method

.method public final a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336734
    iput-object p1, p0, LX/8NJ;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1336735
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336745
    iput-object p1, p0, LX/8NJ;->T:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1336746
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336749
    iput-object p1, p0, LX/8NJ;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1336750
    return-object p0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336747
    iput-object p1, p0, LX/8NJ;->E:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    .line 1336748
    return-object p0
.end method

.method public final a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336762
    iput-object p1, p0, LX/8NJ;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1336763
    return-object p0
.end method

.method public final a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/8NJ;
    .locals 0
    .param p1    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1336760
    iput-object p1, p0, LX/8NJ;->P:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1336761
    return-object p0
.end method

.method public final a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336758
    iput-object p1, p0, LX/8NJ;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1336759
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336764
    iput-object p1, p0, LX/8NJ;->e:Ljava/lang/String;

    .line 1336765
    return-object p0
.end method

.method public final a(Z)LX/8NJ;
    .locals 0

    .prologue
    .line 1336756
    iput-boolean p1, p0, LX/8NJ;->f:Z

    .line 1336757
    return-object p0
.end method

.method public final a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;
    .locals 58

    .prologue
    .line 1336755
    new-instance v2, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8NJ;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8NJ;->b:J

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/8NJ;->c:J

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/8NJ;->d:J

    move-object/from16 v0, p0

    iget-object v10, v0, LX/8NJ;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/8NJ;->f:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, LX/8NJ;->g:Z

    move-object/from16 v0, p0

    iget-object v13, v0, LX/8NJ;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/8NJ;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/8NJ;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8NJ;->k:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->l:LX/0Px;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->m:LX/0Px;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->o:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->p:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->q:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8NJ;->r:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->s:LX/434;

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->s:LX/434;

    move-object/from16 v24, v0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8NJ;->u:Z

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8NJ;->v:J

    move-wide/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8NJ;->x:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8NJ;->y:Z

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->z:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8NJ;->A:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/8NJ;->B:F

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->C:LX/5Rn;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8NJ;->D:J

    move-wide/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->E:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->F:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->G:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->H:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->I:LX/0Px;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->J:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->K:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->L:Ljava/lang/String;

    move-object/from16 v45, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8NJ;->M:J

    move-wide/from16 v46, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->N:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8NJ;->O:Z

    move/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->P:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object/from16 v50, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->Q:LX/0Px;

    move-object/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->R:Ljava/lang/String;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8NJ;->S:Z

    move/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->T:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->U:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/8NJ;->V:Ljava/lang/String;

    move-object/from16 v56, v0

    const/16 v57, 0x0

    invoke-direct/range {v2 .. v57}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;JJJLjava/lang/String;ZZLcom/facebook/bitmaps/SphericalPhotoMetadata;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;Ljava/lang/String;ZLX/0Px;LX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/434;Lcom/facebook/auth/viewercontext/ViewerContext;ZJLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;FFLX/5Rn;JLcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Ljava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;B)V

    return-object v2

    :cond_0
    new-instance v24, LX/434;

    const/16 v25, 0x1

    const/16 v26, 0x1

    invoke-direct/range {v24 .. v26}, LX/434;-><init>(II)V

    goto/16 :goto_0
.end method

.method public final b(F)LX/8NJ;
    .locals 0

    .prologue
    .line 1336753
    iput p1, p0, LX/8NJ;->B:F

    .line 1336754
    return-object p0
.end method

.method public final b(J)LX/8NJ;
    .locals 1

    .prologue
    .line 1336751
    iput-wide p1, p0, LX/8NJ;->d:J

    .line 1336752
    return-object p0
.end method

.method public final b(LX/0Px;)LX/8NJ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;)",
            "LX/8NJ;"
        }
    .end annotation

    .prologue
    .line 1336713
    iput-object p1, p0, LX/8NJ;->m:LX/0Px;

    .line 1336714
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336715
    iput-object p1, p0, LX/8NJ;->j:Ljava/lang/String;

    .line 1336716
    return-object p0
.end method

.method public final b(Z)LX/8NJ;
    .locals 0

    .prologue
    .line 1336671
    iput-boolean p1, p0, LX/8NJ;->g:Z

    .line 1336672
    return-object p0
.end method

.method public final c(J)LX/8NJ;
    .locals 1

    .prologue
    .line 1336673
    iput-wide p1, p0, LX/8NJ;->v:J

    .line 1336674
    return-object p0
.end method

.method public final c(LX/0Px;)LX/8NJ;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;)",
            "LX/8NJ;"
        }
    .end annotation

    .prologue
    .line 1336675
    iput-object p1, p0, LX/8NJ;->Q:LX/0Px;

    .line 1336676
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/8NJ;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1336677
    iput-object p1, p0, LX/8NJ;->o:Ljava/lang/String;

    .line 1336678
    return-object p0
.end method

.method public final c(Z)LX/8NJ;
    .locals 0

    .prologue
    .line 1336679
    iput-boolean p1, p0, LX/8NJ;->k:Z

    .line 1336680
    return-object p0
.end method

.method public final d(J)LX/8NJ;
    .locals 1

    .prologue
    .line 1336681
    iput-wide p1, p0, LX/8NJ;->D:J

    .line 1336682
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336683
    iput-object p1, p0, LX/8NJ;->p:Ljava/lang/String;

    .line 1336684
    return-object p0
.end method

.method public final d(Z)LX/8NJ;
    .locals 0

    .prologue
    .line 1336685
    iput-boolean p1, p0, LX/8NJ;->x:Z

    .line 1336686
    return-object p0
.end method

.method public final e(J)LX/8NJ;
    .locals 1

    .prologue
    .line 1336687
    iput-wide p1, p0, LX/8NJ;->M:J

    .line 1336688
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336689
    iput-object p1, p0, LX/8NJ;->q:Ljava/lang/String;

    .line 1336690
    return-object p0
.end method

.method public final e(Z)LX/8NJ;
    .locals 0

    .prologue
    .line 1336691
    iput-boolean p1, p0, LX/8NJ;->y:Z

    .line 1336692
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336693
    iput-object p1, p0, LX/8NJ;->z:Ljava/lang/String;

    .line 1336694
    return-object p0
.end method

.method public final f(Z)LX/8NJ;
    .locals 0

    .prologue
    .line 1336711
    iput-boolean p1, p0, LX/8NJ;->O:Z

    .line 1336712
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336695
    iput-object p1, p0, LX/8NJ;->J:Ljava/lang/String;

    .line 1336696
    return-object p0
.end method

.method public final g(Z)LX/8NJ;
    .locals 0

    .prologue
    .line 1336697
    iput-boolean p1, p0, LX/8NJ;->S:Z

    .line 1336698
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336699
    iput-object p1, p0, LX/8NJ;->K:Ljava/lang/String;

    .line 1336700
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336701
    iput-object p1, p0, LX/8NJ;->L:Ljava/lang/String;

    .line 1336702
    return-object p0
.end method

.method public final j(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336703
    iput-object p1, p0, LX/8NJ;->N:Ljava/lang/String;

    .line 1336704
    return-object p0
.end method

.method public final k(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336705
    iput-object p1, p0, LX/8NJ;->R:Ljava/lang/String;

    .line 1336706
    return-object p0
.end method

.method public final l(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336707
    iput-object p1, p0, LX/8NJ;->U:Ljava/lang/String;

    .line 1336708
    return-object p0
.end method

.method public final m(Ljava/lang/String;)LX/8NJ;
    .locals 0

    .prologue
    .line 1336709
    iput-object p1, p0, LX/8NJ;->V:Ljava/lang/String;

    .line 1336710
    return-object p0
.end method
