.class public LX/70k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1162713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162714
    iput-object p1, p0, LX/70k;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1162715
    iput-object p2, p0, LX/70k;->b:Landroid/view/View;

    .line 1162716
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1162709
    iget-object v0, p0, LX/70k;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1162710
    iget-object v0, p0, LX/70k;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1162711
    iget-object v0, p0, LX/70k;->b:Landroid/view/View;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1162712
    return-void
.end method

.method public final a(LX/1DI;)V
    .locals 3

    .prologue
    .line 1162696
    invoke-static {}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->newBuilder()LX/62Q;

    move-result-object v0

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    .line 1162697
    iput-object v1, v0, LX/62Q;->a:LX/1lD;

    .line 1162698
    move-object v0, v0

    .line 1162699
    iget-object v1, p0, LX/70k;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1162700
    iput-object v1, v0, LX/62Q;->b:Ljava/lang/String;

    .line 1162701
    move-object v0, v0

    .line 1162702
    invoke-virtual {v0}, LX/62Q;->a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-result-object v0

    .line 1162703
    iget-object v1, p0, LX/70k;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1, v0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    .line 1162704
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1162705
    iget-object v0, p0, LX/70k;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1162706
    iget-object v0, p0, LX/70k;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1162707
    iget-object v0, p0, LX/70k;->b:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1162708
    return-void
.end method
