.class public LX/8G7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1318679
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "frame_prefs_manager/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "last_frame_pack_nuxed_start_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/8G7;->b:LX/0Tn;

    .line 1318680
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "frame_prefs_manager/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "last_viewed_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/8G7;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1318690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318691
    return-void
.end method

.method public static a(LX/0QB;)LX/8G7;
    .locals 1

    .prologue
    .line 1318689
    invoke-static {p0}, LX/8G7;->b(LX/0QB;)LX/8G7;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8G7;
    .locals 2

    .prologue
    .line 1318685
    new-instance v1, LX/8G7;

    invoke-direct {v1}, LX/8G7;-><init>()V

    .line 1318686
    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1318687
    iput-object v0, v1, LX/8G7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1318688
    return-object v1
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 1318683
    iget-object v0, p0, LX/8G7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/8G7;->b:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1318684
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 1318681
    iget-object v0, p0, LX/8G7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/8G7;->c:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1318682
    return-void
.end method
