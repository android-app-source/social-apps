.class public final LX/8a1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1366267
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 1366268
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1366269
    :goto_0
    return v1

    .line 1366270
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 1366271
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1366272
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1366273
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 1366274
    const-string v10, "is_compressed"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1366275
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 1366276
    :cond_1
    const-string v10, "is_font_family"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1366277
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 1366278
    :cond_2
    const-string v10, "resource_name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1366279
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1366280
    :cond_3
    const-string v10, "resource_version"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1366281
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1366282
    :cond_4
    const-string v10, "url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1366283
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1366284
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1366285
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1366286
    if-eqz v3, :cond_7

    .line 1366287
    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 1366288
    :cond_7
    if-eqz v0, :cond_8

    .line 1366289
    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 1366290
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1366291
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1366292
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1366293
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1366294
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1366295
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1366296
    if-eqz v0, :cond_0

    .line 1366297
    const-string v1, "is_compressed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366298
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1366299
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1366300
    if-eqz v0, :cond_1

    .line 1366301
    const-string v1, "is_font_family"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366302
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1366303
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366304
    if-eqz v0, :cond_2

    .line 1366305
    const-string v1, "resource_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366306
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366307
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366308
    if-eqz v0, :cond_3

    .line 1366309
    const-string v1, "resource_version"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366310
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366311
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366312
    if-eqz v0, :cond_4

    .line 1366313
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366314
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366315
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1366316
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1366317
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1366318
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1366319
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/8a1;->a(LX/15i;ILX/0nX;)V

    .line 1366320
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1366321
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1366322
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1366323
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1366324
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1366325
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1366326
    invoke-static {p0, p1}, LX/8a1;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1366327
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1366328
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method
