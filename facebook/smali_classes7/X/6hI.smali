.class public final LX/6hI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Mw;


# direct methods
.method public constructor <init>(LX/2Mw;)V
    .locals 0

    .prologue
    .line 1128036
    iput-object p1, p0, LX/6hI;->a:LX/2Mw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1128037
    sget-object v0, LX/2Mw;->a:Ljava/lang/String;

    const-string v1, "Failed to fetch the logged-in user\'s Montage thread FBID."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1128038
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1128039
    check-cast p1, Ljava/lang/Long;

    .line 1128040
    if-eqz p1, :cond_0

    .line 1128041
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1128042
    iget-object v0, p0, LX/6hI;->a:LX/2Mw;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2Mw;->a(J)V

    .line 1128043
    :cond_0
    iget-object v0, p0, LX/6hI;->a:LX/2Mw;

    iget-object v0, v0, LX/2Mw;->f:LX/2Mx;

    .line 1128044
    iget-object v1, v0, LX/2Mx;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/6gE;->m:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1128045
    return-void
.end method
