.class public LX/6t6;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;",
        "LX/6t4;",
        ">;"
    }
.end annotation


# instance fields
.field public l:LX/6qh;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;)V
    .locals 0

    .prologue
    .line 1153985
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1153986
    return-void
.end method


# virtual methods
.method public final a(LX/6E2;)V
    .locals 2

    .prologue
    .line 1153987
    check-cast p1, LX/6t4;

    .line 1153988
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;

    .line 1153989
    iget-object v1, p1, LX/6t4;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1153990
    invoke-virtual {v0, v1}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->setTitle(Ljava/lang/String;)V

    .line 1153991
    iget-object v1, p1, LX/6t4;->b:LX/0Px;

    move-object v1, v1

    .line 1153992
    invoke-virtual {v0, v1}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->setPrices(LX/0Px;)V

    .line 1153993
    iget-object v1, p1, LX/6t4;->c:Ljava/lang/Integer;

    move-object v1, v1

    .line 1153994
    invoke-virtual {v0, v1}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->setSelectedPriceIndex(Ljava/lang/Integer;)V

    .line 1153995
    new-instance v1, LX/6t5;

    invoke-direct {v1, p0, p1}, LX/6t5;-><init>(LX/6t6;LX/6t4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->setCustomAmountButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 1153996
    iget-object v1, p0, LX/6t6;->l:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1153997
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1153998
    iput-object p1, p0, LX/6t6;->l:LX/6qh;

    .line 1153999
    return-void
.end method
