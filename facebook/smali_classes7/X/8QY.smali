.class public LX/8QY;
.super LX/8QM;
.source ""


# instance fields
.field private final j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

.field private final k:Z

.field private final l:Landroid/content/res/Resources;

.field private final m:Z

.field private final n:I


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;Landroid/content/res/Resources;ZZI)V
    .locals 2

    .prologue
    .line 1343318
    sget-object v0, LX/8vA;->TAG_EXPANSION:LX/8vA;

    invoke-direct {p0, v0}, LX/8QM;-><init>(LX/8vA;)V

    .line 1343319
    iput-object p1, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 1343320
    iput-boolean p3, p0, LX/8QY;->k:Z

    .line 1343321
    iput-object p2, p0, LX/8QY;->l:Landroid/content/res/Resources;

    .line 1343322
    iput-boolean p4, p0, LX/8QY;->m:Z

    .line 1343323
    iput p5, p0, LX/8QY;->n:I

    .line 1343324
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_1

    .line 1343325
    iget-boolean v0, p0, LX/8QY;->k:Z

    if-eqz v0, :cond_0

    .line 1343326
    const v0, 0x7f0200ca

    iput v0, p0, LX/8QY;->e:I

    .line 1343327
    :goto_0
    return-void

    .line 1343328
    :cond_0
    const v0, 0x7f0200c9

    iput v0, p0, LX/8QY;->e:I

    goto :goto_0

    .line 1343329
    :cond_1
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_4

    .line 1343330
    iget-boolean v0, p0, LX/8QY;->k:Z

    if-eqz v0, :cond_2

    .line 1343331
    const v0, 0x7f0200ce

    iput v0, p0, LX/8QY;->e:I

    goto :goto_0

    .line 1343332
    :cond_2
    iget-boolean v0, p0, LX/8QY;->m:Z

    if-eqz v0, :cond_3

    .line 1343333
    const v0, 0x7f0200cd

    iput v0, p0, LX/8QY;->e:I

    goto :goto_0

    .line 1343334
    :cond_3
    const v0, 0x7f0200d1

    iput v0, p0, LX/8QY;->e:I

    goto :goto_0

    .line 1343335
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, LX/8QY;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1343317
    iget-boolean v0, p0, LX/8QY;->m:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1343309
    iget-boolean v0, p0, LX/8QY;->k:Z

    if-eqz v0, :cond_0

    .line 1343310
    const-string v0, ""

    .line 1343311
    :goto_0
    return-object v0

    .line 1343312
    :cond_0
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_1

    .line 1343313
    iget-object v0, p0, LX/8QY;->l:Landroid/content/res/Resources;

    const v1, 0x7f0812f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1343314
    :cond_1
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_2

    .line 1343315
    iget-object v0, p0, LX/8QY;->l:Landroid/content/res/Resources;

    const v1, 0x7f0812f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1343316
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1343308
    invoke-virtual {p0}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1343289
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_0

    .line 1343290
    const v0, 0x7f0208a2

    .line 1343291
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0209f0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1343302
    if-ne p1, p0, :cond_0

    .line 1343303
    const/4 v0, 0x1

    .line 1343304
    :goto_0
    return v0

    .line 1343305
    :cond_0
    instance-of v0, p1, LX/8QY;

    if-nez v0, :cond_1

    .line 1343306
    const/4 v0, 0x0

    goto :goto_0

    .line 1343307
    :cond_1
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    check-cast p1, LX/8QY;

    iget-object v1, p1, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1343301
    iget v0, p0, LX/8QY;->n:I

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 1343295
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_0

    .line 1343296
    const v0, 0x7f0200cc

    .line 1343297
    :goto_0
    return v0

    .line 1343298
    :cond_0
    iget-object v0, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_1

    .line 1343299
    const v0, 0x7f0200d0

    goto :goto_0

    .line 1343300
    :cond_1
    invoke-virtual {p0}, LX/8QL;->d()I

    move-result v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1343294
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1343293
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/8QY;->j:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final n()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1343292
    const/4 v0, 0x0

    return-object v0
.end method
