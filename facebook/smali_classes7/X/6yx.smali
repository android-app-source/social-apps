.class public LX/6yx;
.super LX/6u5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6u5",
        "<",
        "Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160614
    invoke-direct {p0, p1}, LX/6u5;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1160615
    return-void
.end method

.method public static b(LX/0QB;)LX/6yx;
    .locals 2

    .prologue
    .line 1160616
    new-instance v1, LX/6yx;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/6yx;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1160617
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1160618
    check-cast p1, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;

    .line 1160619
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1160620
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_disabled"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160621
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "remove_payments_card"

    .line 1160622
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1160623
    move-object v1, v1

    .line 1160624
    const-string v2, "POST"

    .line 1160625
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1160626
    move-object v1, v1

    .line 1160627
    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;->b:Ljava/lang/String;

    .line 1160628
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1160629
    move-object v1, v1

    .line 1160630
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1160631
    move-object v0, v1

    .line 1160632
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1160633
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1160634
    move-object v0, v0

    .line 1160635
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160636
    const-string v0, "remove_credit_card"

    return-object v0
.end method
