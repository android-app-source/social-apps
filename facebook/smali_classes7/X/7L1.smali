.class public final LX/7L1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/FullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1196738
    iput-object p1, p0, LX/7L1;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x34ec1777

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1196739
    const-string v1, "state"

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/7L1;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/7L1;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v1

    invoke-interface {v1}, LX/7Kd;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p3}, LX/0Yf;->isInitialStickyBroadcast()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1196740
    iget-object v1, p0, LX/7L1;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-static {v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->q(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1196741
    :cond_0
    const/16 v1, 0x27

    const v2, 0x6558c0dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
