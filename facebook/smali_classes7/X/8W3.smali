.class public final LX/8W3;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/8W7;


# direct methods
.method public constructor <init>(LX/8W7;)V
    .locals 0

    .prologue
    .line 1353710
    iput-object p1, p0, LX/8W3;->a:LX/8W7;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1353711
    iget-object v0, p0, LX/8W3;->a:LX/8W7;

    iget-object v0, v0, LX/8W7;->c:LX/8TD;

    sget-object v1, LX/8TE;->WEB_CLIENT_ERROR:LX/8TE;

    const-string v2, "Quicksilver WebViewClient received an error: Requested URL: %s, Error Code: %d, Error Description: %s"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, p4, v3, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1353712
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 1353713
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1353714
    iget-object v0, p0, LX/8W3;->a:LX/8W7;

    iget-object v0, v0, LX/8W7;->c:LX/8TD;

    sget-object v1, LX/8TE;->WEB_CLIENT_ERROR:LX/8TE;

    const-string v2, "Quicksilver WebViewClient received an error: Requested URL: %s, Error Code: %d, Error Description: %s"

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1353715
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    .line 1353716
    return-void
.end method
