.class public final LX/8O3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/photos/upload/operation/UploadRecord;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Ne;

.field public final synthetic b:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public final synthetic c:Ljava/util/concurrent/Semaphore;

.field public final synthetic d:Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

.field public final synthetic e:LX/8OK;

.field public final synthetic f:I

.field public final synthetic g:LX/73w;

.field public final synthetic h:LX/74b;

.field public final synthetic i:LX/8Jv;

.field public final synthetic j:LX/8OL;

.field public final synthetic k:Ljava/util/Collection;

.field public final synthetic l:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic m:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final synthetic n:LX/8O7;

.field public final synthetic o:LX/8OJ;


# direct methods
.method public constructor <init>(LX/8OJ;LX/8Ne;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/concurrent/Semaphore;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OK;ILX/73w;LX/74b;LX/8Jv;LX/8OL;Ljava/util/Collection;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/atomic/AtomicInteger;LX/8O7;)V
    .locals 0

    .prologue
    .line 1338433
    iput-object p1, p0, LX/8O3;->o:LX/8OJ;

    iput-object p2, p0, LX/8O3;->a:LX/8Ne;

    iput-object p3, p0, LX/8O3;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    iput-object p4, p0, LX/8O3;->c:Ljava/util/concurrent/Semaphore;

    iput-object p5, p0, LX/8O3;->d:Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    iput-object p6, p0, LX/8O3;->e:LX/8OK;

    iput p7, p0, LX/8O3;->f:I

    iput-object p8, p0, LX/8O3;->g:LX/73w;

    iput-object p9, p0, LX/8O3;->h:LX/74b;

    iput-object p10, p0, LX/8O3;->i:LX/8Jv;

    iput-object p11, p0, LX/8O3;->j:LX/8OL;

    iput-object p12, p0, LX/8O3;->k:Ljava/util/Collection;

    iput-object p13, p0, LX/8O3;->l:Lcom/facebook/common/callercontext/CallerContext;

    iput-object p14, p0, LX/8O3;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p15, p0, LX/8O3;->n:LX/8O7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1338434
    iget-object v0, p0, LX/8O3;->o:LX/8OJ;

    iget-object v1, p0, LX/8O3;->a:LX/8Ne;

    iget-object v2, p0, LX/8O3;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    iget-object v3, p0, LX/8O3;->c:Ljava/util/concurrent/Semaphore;

    const/4 v5, 0x1

    .line 1338435
    iget-object v4, v0, LX/8OJ;->p:LX/0ad;

    sget-short v6, LX/8Jz;->m:S

    invoke-interface {v4, v6, v5}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1338436
    :goto_0
    move-object v9, v1

    .line 1338437
    :try_start_0
    iget-object v0, p0, LX/8O3;->o:LX/8OJ;

    iget-object v1, p0, LX/8O3;->d:Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    new-instance v2, LX/8OW;

    iget-object v3, p0, LX/8O3;->e:LX/8OK;

    iget v4, p0, LX/8O3;->f:I

    invoke-direct {v2, v3, v4}, LX/8OW;-><init>(LX/8OK;I)V

    iget-object v3, p0, LX/8O3;->g:LX/73w;

    iget-object v4, p0, LX/8O3;->h:LX/74b;

    iget-object v5, p0, LX/8O3;->i:LX/8Jv;

    iget-object v6, p0, LX/8O3;->j:LX/8OL;

    iget v7, p0, LX/8O3;->f:I

    iget-object v8, p0, LX/8O3;->k:Ljava/util/Collection;

    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v8

    iget-object v10, p0, LX/8O3;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static/range {v0 .. v10}, LX/8OJ;->a(LX/8OJ;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OG;LX/73w;LX/74b;LX/8Jv;LX/8OL;IILX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/photos/upload/operation/UploadRecord;
    :try_end_0
    .catch LX/8O9; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1338438
    :goto_1
    iget-object v1, p0, LX/8O3;->o:LX/8OJ;

    iget-object v1, v1, LX/8OJ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1338439
    :try_start_1
    iget-object v2, p0, LX/8O3;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1338440
    iget-object v2, p0, LX/8O3;->n:LX/8O7;

    iget-object v3, p0, LX/8O3;->d:Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-virtual {v2, v3, v0}, LX/8O7;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;Lcom/facebook/photos/upload/operation/UploadRecord;)V

    .line 1338441
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338442
    return-object v0

    .line 1338443
    :catch_0
    iget-object v0, p0, LX/8O3;->o:LX/8OJ;

    iget-object v1, p0, LX/8O3;->d:Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-virtual {v1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v1

    new-instance v2, LX/8OW;

    iget-object v3, p0, LX/8O3;->e:LX/8OK;

    iget v4, p0, LX/8O3;->f:I

    invoke-direct {v2, v3, v4}, LX/8OW;-><init>(LX/8OK;I)V

    iget-object v3, p0, LX/8O3;->g:LX/73w;

    iget-object v4, p0, LX/8O3;->h:LX/74b;

    iget-object v5, p0, LX/8O3;->i:LX/8Jv;

    iget-object v6, p0, LX/8O3;->j:LX/8OL;

    iget v7, p0, LX/8O3;->f:I

    iget-object v8, p0, LX/8O3;->k:Ljava/util/Collection;

    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v8

    iget-object v10, p0, LX/8O3;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static/range {v0 .. v10}, LX/8OJ;->a(LX/8OJ;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/8OG;LX/73w;LX/74b;LX/8Jv;LX/8OL;IILX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v0

    goto :goto_1

    .line 1338444
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1338445
    :cond_0
    iget-object v4, v0, LX/8OJ;->i:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8Ne;

    .line 1338446
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_2
    invoke-interface {v4, v5}, LX/8Ne;->a(Z)V

    .line 1338447
    invoke-interface {v4, v3}, LX/8Ne;->a(Ljava/util/concurrent/Semaphore;)V

    move-object v1, v4

    .line 1338448
    goto :goto_0

    .line 1338449
    :cond_1
    const/4 v5, 0x0

    goto :goto_2
.end method
