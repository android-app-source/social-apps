.class public LX/6mp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0B2;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[B

.field public final c:LX/0AL;

.field public final d:LX/0AM;

.field public final e:I

.field public final f:J

.field public final g:I

.field public final h:Ljava/lang/String;

.field public volatile i:LX/05u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05u",
            "<*>;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/lang/String;[BLX/0AL;LX/0AM;IJILjava/lang/String;)V
    .locals 2
    .param p4    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1146662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146663
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/6mp;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1146664
    iput-object p1, p0, LX/6mp;->a:Ljava/lang/String;

    .line 1146665
    iput-object p2, p0, LX/6mp;->b:[B

    .line 1146666
    iput-object p3, p0, LX/6mp;->c:LX/0AL;

    .line 1146667
    iput-object p4, p0, LX/6mp;->d:LX/0AM;

    .line 1146668
    iput p5, p0, LX/6mp;->e:I

    .line 1146669
    iput-wide p6, p0, LX/6mp;->f:J

    .line 1146670
    iput p8, p0, LX/6mp;->g:I

    .line 1146671
    iput-object p9, p0, LX/6mp;->h:Ljava/lang/String;

    .line 1146672
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1146673
    iget v0, p0, LX/6mp;->g:I

    return v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1146674
    :try_start_0
    iget-object v0, p0, LX/6mp;->i:LX/05u;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v2, -0x27fa4d91

    invoke-static {v0, p1, p2, v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1146675
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0

    .line 1146676
    :catch_0
    return-void
.end method

.method public final a(LX/05u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05u",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1146677
    invoke-static {p1}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1146678
    iget-object v0, p0, LX/6mp;->i:LX/05u;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/01n;->b(Z)V

    .line 1146679
    iput-object p1, p0, LX/6mp;->i:LX/05u;

    .line 1146680
    return-void

    .line 1146681
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1146682
    iget-object v0, p0, LX/6mp;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1146683
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1146684
    iget-object v0, p0, LX/6mp;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method
