.class public final LX/7yS;
.super LX/0eW;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1279273
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)LX/7yS;
    .locals 3

    .prologue
    .line 1279277
    new-instance v0, LX/7yS;

    invoke-direct {v0}, LX/7yS;-><init>()V

    .line 1279278
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/2addr v1, v2

    .line 1279279
    iput v1, v0, LX/7yS;->a:I

    iput-object p0, v0, LX/7yS;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 1279280
    move-object v0, v1

    .line 1279281
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1279282
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/0eW;->d(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/7yR;I)LX/7yR;
    .locals 2

    .prologue
    .line 1279274
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/0eW;->e(I)I

    move-result v0

    mul-int/lit8 v1, p2, 0x4

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1279275
    iput v0, p1, LX/7yR;->a:I

    iput-object v1, p1, LX/7yR;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 1279276
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
