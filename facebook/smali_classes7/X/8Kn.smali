.class public LX/8Kn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/8Kn;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0hy;

.field public final c:LX/17Y;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hy;LX/17Y;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0hy;",
            "LX/17Y;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1330611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1330612
    iput-object p1, p0, LX/8Kn;->a:Landroid/content/Context;

    .line 1330613
    iput-object p2, p0, LX/8Kn;->b:LX/0hy;

    .line 1330614
    iput-object p3, p0, LX/8Kn;->c:LX/17Y;

    .line 1330615
    iput-object p4, p0, LX/8Kn;->d:LX/0Or;

    .line 1330616
    return-void
.end method

.method public static a(LX/0QB;)LX/8Kn;
    .locals 7

    .prologue
    .line 1330598
    sget-object v0, LX/8Kn;->e:LX/8Kn;

    if-nez v0, :cond_1

    .line 1330599
    const-class v1, LX/8Kn;

    monitor-enter v1

    .line 1330600
    :try_start_0
    sget-object v0, LX/8Kn;->e:LX/8Kn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1330601
    if-eqz v2, :cond_0

    .line 1330602
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1330603
    new-instance v6, LX/8Kn;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v4

    check-cast v4, LX/0hy;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/8Kn;-><init>(Landroid/content/Context;LX/0hy;LX/17Y;LX/0Or;)V

    .line 1330604
    move-object v0, v6

    .line 1330605
    sput-object v0, LX/8Kn;->e:LX/8Kn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1330606
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1330607
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1330608
    :cond_1
    sget-object v0, LX/8Kn;->e:LX/8Kn;

    return-object v0

    .line 1330609
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1330610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 1330562
    const/4 v1, 0x1

    .line 1330563
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v3, LX/8LS;->TARGET:LX/8LS;

    if-ne v2, v3, :cond_3

    :cond_0
    :goto_0
    move v0, v1

    .line 1330564
    if-eqz v0, :cond_1

    .line 1330565
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v0

    .line 1330566
    if-nez v0, :cond_1

    .line 1330567
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1330568
    const-string v0, "/photo.php?v=%s"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p2, v1, v3

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1330569
    sget-object v1, LX/0ax;->do:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1330570
    iget-object v1, p0, LX/8Kn;->c:LX/17Y;

    iget-object v2, p0, LX/8Kn;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1330571
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "com.facebook.photos.upload.video."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1330572
    move-object v0, v0

    .line 1330573
    :goto_1
    return-object v0

    .line 1330574
    :cond_1
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v0

    .line 1330575
    if-eqz v0, :cond_2

    .line 1330576
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v0

    .line 1330577
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1330578
    :goto_2
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330579
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330580
    iget-object v1, p0, LX/8Kn;->b:LX/0hy;

    const/4 p0, 0x1

    .line 1330581
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330582
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330583
    const-string v2, "%s_%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1330584
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    .line 1330585
    iput-object v2, v3, LX/23u;->T:Ljava/lang/String;

    .line 1330586
    move-object v3, v3

    .line 1330587
    new-instance v4, LX/3dM;

    invoke-direct {v4}, LX/3dM;-><init>()V

    invoke-virtual {v4, p0}, LX/3dM;->c(Z)LX/3dM;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/3dM;->g(Z)LX/3dM;

    move-result-object v4

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1330588
    iput-object v2, v4, LX/3dM;->D:Ljava/lang/String;

    .line 1330589
    move-object v2, v4

    .line 1330590
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1330591
    iput-object v2, v3, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1330592
    move-object v2, v3

    .line 1330593
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1330594
    new-instance v3, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-direct {v3, v2}, Lcom/facebook/ipc/feed/ViewPermalinkParams;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    move-object v2, v3

    .line 1330595
    invoke-interface {v1, v2}, LX/0hz;->a(Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 1330596
    goto :goto_1

    .line 1330597
    :cond_2
    iget-object v0, p0, LX/8Kn;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-wide v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    const-wide/16 v5, 0x0

    cmp-long v2, v3, v5

    if-gtz v2, :cond_0

    const/4 v1, 0x0

    goto/16 :goto_0
.end method
