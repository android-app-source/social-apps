.class public final LX/7On;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/io/OutputStream;

.field public final synthetic c:J

.field public final synthetic d:LX/7Oo;

.field private e:J

.field private f:Z

.field private g:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(LX/7Oo;JLjava/io/OutputStream;J)V
    .locals 2

    .prologue
    .line 1202121
    iput-object p1, p0, LX/7On;->d:LX/7Oo;

    iput-wide p2, p0, LX/7On;->a:J

    iput-object p4, p0, LX/7On;->b:Ljava/io/OutputStream;

    iput-wide p5, p0, LX/7On;->c:J

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 1202122
    iget-wide v0, p0, LX/7On;->a:J

    iput-wide v0, p0, LX/7On;->e:J

    .line 1202123
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7On;->f:Z

    .line 1202124
    iget-object v0, p0, LX/7On;->b:Ljava/io/OutputStream;

    iput-object v0, p0, LX/7On;->g:Ljava/io/OutputStream;

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 1202145
    iget-object v0, p0, LX/7On;->g:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 1202146
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 1202147
    iget-object v0, p0, LX/7On;->g:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 1202148
    return-void
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 1202144
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "write ONE byte?!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final write([BII)V
    .locals 4

    .prologue
    .line 1202125
    iget-boolean v0, p0, LX/7On;->f:Z

    if-eqz v0, :cond_1

    .line 1202126
    iget-wide v0, p0, LX/7On;->e:J

    iget-wide v2, p0, LX/7On;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1202127
    iget-object v0, p0, LX/7On;->d:LX/7Oo;

    iget-object v0, v0, LX/7Oo;->e:LX/3Da;

    if-nez v0, :cond_0

    .line 1202128
    iget-object v0, p0, LX/7On;->d:LX/7Oo;

    iget-object v1, p0, LX/7On;->d:LX/7Oo;

    iget-object v1, v1, LX/7Oo;->a:LX/7Op;

    iget-object v1, v1, LX/7Op;->b:LX/1Ln;

    iget-object v2, p0, LX/7On;->d:LX/7Oo;

    .line 1202129
    iget-object v3, v2, LX/7Oo;->b:LX/37C;

    move-object v2, v3

    .line 1202130
    iget-object v3, p0, LX/7On;->d:LX/7Oo;

    invoke-virtual {v3}, LX/7Oo;->b()LX/3Dd;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v1

    .line 1202131
    iput-object v1, v0, LX/7Oo;->e:LX/3Da;

    .line 1202132
    :cond_0
    iget-object v0, p0, LX/7On;->d:LX/7Oo;

    iget-object v0, v0, LX/7Oo;->e:LX/3Da;

    iget-wide v2, p0, LX/7On;->c:J

    invoke-interface {v0, v2, v3}, LX/3Da;->a(J)Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, LX/7On;->g:Ljava/io/OutputStream;

    .line 1202133
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7On;->f:Z

    .line 1202134
    :cond_1
    iget-object v0, p0, LX/7On;->g:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 1202135
    iget-wide v0, p0, LX/7On;->e:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/7On;->e:J

    .line 1202136
    :goto_0
    return-void

    .line 1202137
    :cond_2
    iget-wide v0, p0, LX/7On;->e:J

    iget-wide v2, p0, LX/7On;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    iget-wide v0, p0, LX/7On;->e:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/7On;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 1202138
    iget-wide v0, p0, LX/7On;->c:J

    iget-wide v2, p0, LX/7On;->e:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 1202139
    iget-object v1, p0, LX/7On;->g:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 1202140
    iget-wide v2, p0, LX/7On;->c:J

    iput-wide v2, p0, LX/7On;->e:J

    .line 1202141
    add-int v1, p2, v0

    sub-int v0, p3, v0

    invoke-virtual {p0, p1, v1, v0}, LX/7On;->write([BII)V

    goto :goto_0

    .line 1202142
    :cond_3
    iget-object v0, p0, LX/7On;->g:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 1202143
    iget-wide v0, p0, LX/7On;->e:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/7On;->e:J

    goto :goto_0
.end method
