.class public LX/6kI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final newMessage:LX/6k4;

.field public final replacedMessageId:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1135988
    new-instance v0, LX/1sv;

    const-string v1, "DeltaReplaceMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kI;->b:LX/1sv;

    .line 1135989
    new-instance v0, LX/1sw;

    const-string v1, "newMessage"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kI;->c:LX/1sw;

    .line 1135990
    new-instance v0, LX/1sw;

    const-string v1, "replacedMessageId"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kI;->d:LX/1sw;

    .line 1135991
    sput-boolean v4, LX/6kI;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6k4;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1135984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135985
    iput-object p1, p0, LX/6kI;->newMessage:LX/6k4;

    .line 1135986
    iput-object p2, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    .line 1135987
    return-void
.end method

.method public static a(LX/6kI;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1135979
    iget-object v0, p0, LX/6kI;->newMessage:LX/6k4;

    if-nez v0, :cond_0

    .line 1135980
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'newMessage\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135981
    :cond_0
    iget-object v0, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1135982
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'replacedMessageId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135983
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135951
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1135952
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1135953
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1135954
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaReplaceMessage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135955
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135956
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135957
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135958
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135959
    const-string v4, "newMessage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135960
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135961
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135962
    iget-object v4, p0, LX/6kI;->newMessage:LX/6k4;

    if-nez v4, :cond_3

    .line 1135963
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135964
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135965
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135966
    const-string v4, "replacedMessageId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135967
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135968
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135969
    iget-object v0, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1135970
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135971
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135972
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135973
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135974
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1135975
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1135976
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1135977
    :cond_3
    iget-object v4, p0, LX/6kI;->newMessage:LX/6k4;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1135978
    :cond_4
    iget-object v0, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1135914
    invoke-static {p0}, LX/6kI;->a(LX/6kI;)V

    .line 1135915
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135916
    iget-object v0, p0, LX/6kI;->newMessage:LX/6k4;

    if-eqz v0, :cond_0

    .line 1135917
    sget-object v0, LX/6kI;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135918
    iget-object v0, p0, LX/6kI;->newMessage:LX/6k4;

    invoke-virtual {v0, p1}, LX/6k4;->a(LX/1su;)V

    .line 1135919
    :cond_0
    iget-object v0, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1135920
    sget-object v0, LX/6kI;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135921
    iget-object v0, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1135922
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135923
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135924
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135929
    if-nez p1, :cond_1

    .line 1135930
    :cond_0
    :goto_0
    return v0

    .line 1135931
    :cond_1
    instance-of v1, p1, LX/6kI;

    if-eqz v1, :cond_0

    .line 1135932
    check-cast p1, LX/6kI;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135933
    if-nez p1, :cond_3

    .line 1135934
    :cond_2
    :goto_1
    move v0, v2

    .line 1135935
    goto :goto_0

    .line 1135936
    :cond_3
    iget-object v0, p0, LX/6kI;->newMessage:LX/6k4;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1135937
    :goto_2
    iget-object v3, p1, LX/6kI;->newMessage:LX/6k4;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1135938
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135939
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135940
    iget-object v0, p0, LX/6kI;->newMessage:LX/6k4;

    iget-object v3, p1, LX/6kI;->newMessage:LX/6k4;

    invoke-virtual {v0, v3}, LX/6k4;->a(LX/6k4;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135941
    :cond_5
    iget-object v0, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1135942
    :goto_4
    iget-object v3, p1, LX/6kI;->replacedMessageId:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1135943
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1135944
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135945
    iget-object v0, p0, LX/6kI;->replacedMessageId:Ljava/lang/String;

    iget-object v3, p1, LX/6kI;->replacedMessageId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1135946
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1135947
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1135948
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1135949
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1135950
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135928
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135925
    sget-boolean v0, LX/6kI;->a:Z

    .line 1135926
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kI;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135927
    return-object v0
.end method
