.class public LX/7yF;
.super LX/62U;
.source ""


# instance fields
.field public final m:Landroid/support/design/widget/TextInputLayout;

.field public final n:Lcom/facebook/fig/textinput/FigEditText;

.field public o:LX/7ws;

.field public p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

.field public q:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

.field public final r:Landroid/text/TextWatcher;

.field public final s:Landroid/view/View$OnFocusChangeListener;

.field public t:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1278831
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1278832
    new-instance v0, LX/7yC;

    invoke-direct {v0, p0}, LX/7yC;-><init>(LX/7yF;)V

    iput-object v0, p0, LX/7yF;->r:Landroid/text/TextWatcher;

    .line 1278833
    new-instance v0, LX/7yD;

    invoke-direct {v0, p0}, LX/7yD;-><init>(LX/7yF;)V

    iput-object v0, p0, LX/7yF;->s:Landroid/view/View$OnFocusChangeListener;

    .line 1278834
    const-class v0, LX/7yF;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/7yF;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    move-object v0, p1

    .line 1278835
    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p0, LX/7yF;->m:Landroid/support/design/widget/TextInputLayout;

    .line 1278836
    const v0, 0x7f0d0eba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    .line 1278837
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7yF;

    invoke-static {p0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object p0

    check-cast p0, LX/7xH;

    iput-object p0, p1, LX/7yF;->t:LX/7xH;

    return-void
.end method
