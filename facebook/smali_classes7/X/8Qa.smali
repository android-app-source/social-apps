.class public final enum LX/8Qa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Qa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Qa;

.field public static final enum FullIndexEducation:LX/8Qa;

.field public static final enum GroupMallAdsEducation:LX/8Qa;

.field public static final enum ReshareEducation:LX/8Qa;

.field public static final enum TagExpansionEducation:LX/8Qa;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1343398
    new-instance v0, LX/8Qa;

    const-string v1, "ReshareEducation"

    const-string v2, "reshare_education_type"

    invoke-direct {v0, v1, v3, v2}, LX/8Qa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Qa;->ReshareEducation:LX/8Qa;

    .line 1343399
    new-instance v0, LX/8Qa;

    const-string v1, "TagExpansionEducation"

    const-string v2, "tag_expansion_education_type"

    invoke-direct {v0, v1, v4, v2}, LX/8Qa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Qa;->TagExpansionEducation:LX/8Qa;

    .line 1343400
    new-instance v0, LX/8Qa;

    const-string v1, "FullIndexEducation"

    const-string v2, "fullindex_education_type"

    invoke-direct {v0, v1, v5, v2}, LX/8Qa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Qa;->FullIndexEducation:LX/8Qa;

    .line 1343401
    new-instance v0, LX/8Qa;

    const-string v1, "GroupMallAdsEducation"

    const-string v2, "group_mall_ads_education_type"

    invoke-direct {v0, v1, v6, v2}, LX/8Qa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Qa;->GroupMallAdsEducation:LX/8Qa;

    .line 1343402
    const/4 v0, 0x4

    new-array v0, v0, [LX/8Qa;

    sget-object v1, LX/8Qa;->ReshareEducation:LX/8Qa;

    aput-object v1, v0, v3

    sget-object v1, LX/8Qa;->TagExpansionEducation:LX/8Qa;

    aput-object v1, v0, v4

    sget-object v1, LX/8Qa;->FullIndexEducation:LX/8Qa;

    aput-object v1, v0, v5

    sget-object v1, LX/8Qa;->GroupMallAdsEducation:LX/8Qa;

    aput-object v1, v0, v6

    sput-object v0, LX/8Qa;->$VALUES:[LX/8Qa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1343403
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1343404
    iput-object p3, p0, LX/8Qa;->name:Ljava/lang/String;

    .line 1343405
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Qa;
    .locals 1

    .prologue
    .line 1343406
    const-class v0, LX/8Qa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Qa;

    return-object v0
.end method

.method public static values()[LX/8Qa;
    .locals 1

    .prologue
    .line 1343407
    sget-object v0, LX/8Qa;->$VALUES:[LX/8Qa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Qa;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1343408
    iget-object v0, p0, LX/8Qa;->name:Ljava/lang/String;

    return-object v0
.end method
