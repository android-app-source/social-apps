.class public LX/7Op;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;
.implements LX/1Ln;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0po;",
        "LX/1Ln",
        "<",
        "LX/37C;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1Ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1Ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ln;LX/1Ln;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1202235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202236
    iput-object p1, p0, LX/7Op;->a:LX/1Ln;

    .line 1202237
    iput-object p2, p0, LX/7Op;->b:LX/1Ln;

    .line 1202238
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1202232
    iget-object v0, p0, LX/7Op;->a:LX/1Ln;

    instance-of v0, v0, LX/0po;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    instance-of v0, v0, LX/0po;

    if-nez v0, :cond_1

    .line 1202233
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Composing storages are not trimmable"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1202234
    :cond_1
    return-void
.end method


# virtual methods
.method public final U_()V
    .locals 1

    .prologue
    .line 1202204
    invoke-direct {p0}, LX/7Op;->c()V

    .line 1202205
    iget-object v0, p0, LX/7Op;->a:LX/1Ln;

    check-cast v0, LX/0po;

    invoke-interface {v0}, LX/0po;->U_()V

    .line 1202206
    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    check-cast v0, LX/0po;

    invoke-interface {v0}, LX/0po;->U_()V

    .line 1202207
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/3Dd;)LX/3Da;
    .locals 3

    .prologue
    .line 1202225
    check-cast p1, LX/37C;

    .line 1202226
    invoke-virtual {p1}, LX/37C;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1202227
    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    invoke-interface {v0, p1, p2}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v0

    .line 1202228
    :goto_0
    return-object v0

    .line 1202229
    :cond_0
    iget-object v0, p0, LX/7Op;->a:LX/1Ln;

    invoke-interface {v0, p1, p2}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v1

    .line 1202230
    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    invoke-interface {v0, p1, p2}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v2

    .line 1202231
    new-instance v0, LX/7Oo;

    invoke-direct {v0, p0, p1, v1, v2}, LX/7Oo;-><init>(LX/7Op;LX/37C;LX/3Da;LX/3Da;)V

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1202224
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Could be done, but underlying storages don\'t support it"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1202220
    check-cast p1, LX/37C;

    .line 1202221
    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    invoke-interface {v0, p1}, LX/1Ln;->a(Ljava/lang/Object;)V

    .line 1202222
    iget-object v0, p0, LX/7Op;->a:LX/1Ln;

    invoke-interface {v0, p1}, LX/1Ln;->a(Ljava/lang/Object;)V

    .line 1202223
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/3Da;
    .locals 3

    .prologue
    .line 1202211
    check-cast p1, LX/37C;

    .line 1202212
    invoke-virtual {p1}, LX/37C;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1202213
    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    invoke-interface {v0, p1}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;

    move-result-object v0

    .line 1202214
    :goto_0
    return-object v0

    .line 1202215
    :cond_0
    iget-object v0, p0, LX/7Op;->a:LX/1Ln;

    invoke-interface {v0, p1}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;

    move-result-object v1

    .line 1202216
    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    invoke-interface {v0, p1}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;

    move-result-object v2

    .line 1202217
    if-nez v1, :cond_1

    if-nez v2, :cond_1

    .line 1202218
    const/4 v0, 0x0

    goto :goto_0

    .line 1202219
    :cond_1
    new-instance v0, LX/7Oo;

    invoke-direct {v0, p0, p1, v1, v2}, LX/7Oo;-><init>(LX/7Op;LX/37C;LX/3Da;LX/3Da;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1202208
    iget-object v0, p0, LX/7Op;->a:LX/1Ln;

    check-cast v0, LX/0po;

    invoke-interface {v0}, LX/0po;->b()V

    .line 1202209
    iget-object v0, p0, LX/7Op;->b:LX/1Ln;

    check-cast v0, LX/0po;

    invoke-interface {v0}, LX/0po;->b()V

    .line 1202210
    return-void
.end method
