.class public final LX/7oA;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1240795
    const-class v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    const v0, -0x7aaa7799

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "EventTicketOrderPollStatusQuery"

    const-string v6, "3fc39d47a3ebed14f4c415a68a6bd538"

    const-string v7, "node"

    const-string v8, "10155069963951729"

    const-string v9, "10155259086761729"

    .line 1240796
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1240797
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1240798
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1240799
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1240800
    sparse-switch v0, :sswitch_data_0

    .line 1240801
    :goto_0
    return-object p1

    .line 1240802
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1240803
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7d1a28b1 -> :sswitch_1
        0x4991ffac -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1240804
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1240805
    :goto_1
    return v0

    .line 1240806
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1240807
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
