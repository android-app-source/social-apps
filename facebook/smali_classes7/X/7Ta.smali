.class public LX/7Ta;
.super LX/7TY;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1210834
    invoke-direct {p0, p1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1210835
    return-void
.end method

.method private g(I)LX/7TZ;
    .locals 1

    .prologue
    .line 1210836
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1210837
    sget-object v0, LX/7TZ;->VIEW_TYPE_BOTTOM_PADDING:LX/7TZ;

    .line 1210838
    :goto_0
    return-object v0

    .line 1210839
    :cond_0
    invoke-virtual {p0, p1}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 1210840
    invoke-virtual {v0}, LX/3Ai;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1210841
    sget-object v0, LX/7TZ;->VIEW_TYPE_REGULAR_ITEM:LX/7TZ;

    goto :goto_0

    .line 1210842
    :cond_1
    sget-object v0, LX/7TZ;->VIEW_TYPE_DESCRIPTION_ONLY_ITEM:LX/7TZ;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1210810
    iget-object v0, p0, LX/34c;->c:Landroid/content/Context;

    move-object v1, v0

    .line 1210811
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1210812
    const/4 v0, 0x0

    .line 1210813
    sget-object v3, LX/7TZ;->VIEW_TYPE_REGULAR_ITEM:LX/7TZ;

    invoke-virtual {v3}, LX/7TZ;->ordinal()I

    move-result v3

    if-ne p2, v3, :cond_1

    .line 1210814
    const v0, 0x7f0301d2

    invoke-virtual {v2, v0, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1210815
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1210816
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0a4a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1210817
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1210818
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 1210819
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for creating view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1210820
    :cond_1
    sget-object v3, LX/7TZ;->VIEW_TYPE_DESCRIPTION_ONLY_ITEM:LX/7TZ;

    invoke-virtual {v3}, LX/7TZ;->ordinal()I

    move-result v3

    if-ne p2, v3, :cond_2

    .line 1210821
    const v0, 0x7f0301d0

    invoke-virtual {v2, v0, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1210822
    :cond_2
    sget-object v2, LX/7TZ;->VIEW_TYPE_BOTTOM_PADDING:LX/7TZ;

    invoke-virtual {v2}, LX/7TZ;->ordinal()I

    move-result v2

    if-ne p2, v2, :cond_0

    .line 1210823
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1210824
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0a4c

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {v2, v3, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1210825
    :cond_3
    new-instance v1, LX/7TW;

    invoke-direct {v1, v0}, LX/7TW;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1210826
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 1210827
    :goto_0
    return-void

    .line 1210828
    :cond_0
    invoke-direct {p0, p2}, LX/7Ta;->g(I)LX/7TZ;

    move-result-object v0

    .line 1210829
    sget-object v1, LX/7TZ;->VIEW_TYPE_UNKNOWN:LX/7TZ;

    if-ne v0, v1, :cond_1

    .line 1210830
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for binding view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1210831
    :cond_1
    invoke-virtual {p0, p2}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 1210832
    check-cast p1, LX/7TW;

    .line 1210833
    invoke-virtual {p0, p1, v0}, LX/7Ta;->a(LX/7TW;LX/3Ai;)V

    goto :goto_0
.end method

.method public final a(LX/7TW;LX/3Ai;)V
    .locals 2

    .prologue
    .line 1210801
    invoke-virtual {p2}, LX/3Ai;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1210802
    iget-object v0, p1, LX/7TW;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1210803
    iget-object v0, p2, LX/3Ai;->d:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 1210804
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1210805
    iget-object v0, p1, LX/7TW;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1210806
    iget-object v1, p2, LX/3Ai;->d:Ljava/lang/CharSequence;

    move-object v1, v1

    .line 1210807
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210808
    :cond_0
    :goto_0
    return-void

    .line 1210809
    :cond_1
    invoke-super {p0, p1, p2}, LX/7TY;->a(LX/7TW;LX/3Ai;)V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1210800
    invoke-direct {p0, p1}, LX/7Ta;->g(I)LX/7TZ;

    move-result-object v0

    invoke-virtual {v0}, LX/7TZ;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1210799
    invoke-super {p0}, LX/7TY;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
