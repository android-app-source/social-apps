.class public final LX/7lw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;


# instance fields
.field public final synthetic a:LX/7lx;

.field private final b:Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7lx;Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;)V
    .locals 0
    .param p2    # Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1236118
    iput-object p1, p0, LX/7lw;->a:LX/7lx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236119
    iput-object p2, p0, LX/7lw;->b:Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;

    .line 1236120
    return-void
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1236107
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1236108
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v2, "post_failure_data.txt"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1236109
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1236110
    iget-object v3, p0, LX/7lw;->a:LX/7lx;

    iget-object v3, v3, LX/7lx;->c:LX/0lB;

    iget-object v4, p0, LX/7lw;->b:Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;

    invoke-virtual {v3, v1, v4}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 1236111
    const-string v1, "post_failure_data.txt"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1236112
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 1236113
    :catch_0
    move-exception v0

    .line 1236114
    sget-object v1, LX/7lx;->a:Ljava/lang/Class;

    const-string v2, "Exception saving report data file"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1236115
    throw v0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 1236117
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 1236116
    iget-object v0, p0, LX/7lw;->a:LX/7lx;

    iget-object v0, v0, LX/7lx;->h:LX/0W3;

    sget-wide v2, LX/0X5;->bf:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
