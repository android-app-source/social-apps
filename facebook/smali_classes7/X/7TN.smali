.class public LX/7TN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7TL;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/616;

.field private final c:LX/5Pc;

.field private final d:Lcom/facebook/libyuv/YUVColorConverter;

.field private final e:LX/03V;

.field private f:LX/61A;

.field private g:LX/619;

.field private h:LX/617;

.field private i:LX/617;

.field private j:Landroid/media/MediaFormat;

.field private k:LX/7Sx;

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:LX/7Sz;

.field private q:LX/7T3;

.field private r:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1210522
    const/16 v0, 0x15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/7TN;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/616;LX/5Pc;Lcom/facebook/libyuv/YUVColorConverter;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1210523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210524
    iput-object p1, p0, LX/7TN;->b:LX/616;

    .line 1210525
    iput-object p2, p0, LX/7TN;->c:LX/5Pc;

    .line 1210526
    iput-object p3, p0, LX/7TN;->d:Lcom/facebook/libyuv/YUVColorConverter;

    .line 1210527
    iput-object p4, p0, LX/7TN;->e:LX/03V;

    .line 1210528
    return-void
.end method

.method private a(Ljava/nio/ByteBuffer;LX/614;J)V
    .locals 11

    .prologue
    .line 1210529
    iget-object v0, p0, LX/7TN;->k:LX/7Sx;

    iget v3, v0, LX/7Sx;->d:I

    .line 1210530
    iget-object v0, p0, LX/7TN;->k:LX/7Sx;

    iget v8, v0, LX/7Sx;->e:I

    .line 1210531
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1210532
    iget-object v0, p0, LX/7TN;->f:LX/61A;

    iget v0, v0, LX/61A;->b:I

    packed-switch v0, :pswitch_data_0

    .line 1210533
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported color format"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1210534
    :pswitch_1
    mul-int v1, v3, v8

    .line 1210535
    iget-object v0, p0, LX/7TN;->f:LX/61A;

    iget v0, v0, LX/61A;->c:I

    .line 1210536
    if-eqz v0, :cond_0

    rem-int v2, v1, v0

    if-nez v2, :cond_1

    .line 1210537
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 1210538
    :goto_0
    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x2

    add-int v9, v1, v2

    .line 1210539
    invoke-virtual {p2}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ge v1, v9, :cond_2

    .line 1210540
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Encoder buffer too small"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1210541
    :cond_1
    rem-int v2, v1, v0

    sub-int v2, v0, v2

    .line 1210542
    add-int v0, v1, v2

    goto :goto_0

    .line 1210543
    :cond_2
    invoke-virtual {p2}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1210544
    invoke-virtual {p2}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1210545
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1210546
    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1210547
    mul-int/lit8 v1, v3, 0x4

    neg-int v7, v8

    move-object v0, p1

    move v5, v3

    move v6, v3

    invoke-static/range {v0 .. v7}, Lcom/facebook/libyuv/YUVColorConverter;->a(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I

    .line 1210548
    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v1, p2

    move v3, v9

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, LX/614;->a(IIJI)V

    .line 1210549
    :goto_1
    return-void

    .line 1210550
    :pswitch_2
    mul-int v0, v3, v8

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v10, v0, 0x2

    .line 1210551
    invoke-virtual {p2}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-ge v0, v10, :cond_3

    .line 1210552
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Encoder buffer too small"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1210553
    :cond_3
    mul-int v0, v3, v8

    .line 1210554
    div-int/lit8 v1, v0, 0x4

    .line 1210555
    invoke-virtual {p2}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1210556
    invoke-virtual {p2}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1210557
    invoke-virtual {p2}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1210558
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1210559
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1210560
    add-int/2addr v0, v1

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1210561
    mul-int/lit8 v1, v3, 0x4

    div-int/lit8 v5, v3, 0x2

    div-int/lit8 v7, v3, 0x2

    neg-int v9, v8

    move-object v0, p1

    move v8, v3

    invoke-static/range {v0 .. v9}, Lcom/facebook/libyuv/YUVColorConverter;->a(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I

    .line 1210562
    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v1, p2

    move v3, v10

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, LX/614;->a(IIJI)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(JZ)Z
    .locals 12

    .prologue
    .line 1210568
    :goto_0
    iget-object v0, p0, LX/7TN;->q:LX/7T3;

    if-nez v0, :cond_0

    .line 1210569
    const-string v0, "readNextFrame"

    const v1, -0x41f52b40

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1210570
    :try_start_0
    iget-object v0, p0, LX/7TN;->p:LX/7Sz;

    const/4 v5, 0x0

    .line 1210571
    iget-boolean v6, v0, LX/7Sz;->p:Z

    if-nez v6, :cond_2

    .line 1210572
    const/4 v5, 0x0

    .line 1210573
    :goto_1
    move-object v0, v5

    .line 1210574
    iput-object v0, p0, LX/7TN;->q:LX/7T3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1210575
    const v0, -0x71e10b53

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1210576
    iget-object v0, p0, LX/7TN;->q:LX/7T3;

    if-nez v0, :cond_0

    .line 1210577
    :goto_2
    return p3

    .line 1210578
    :catchall_0
    move-exception v0

    const v1, -0x5f90c597

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1210579
    :cond_0
    iget-object v0, p0, LX/7TN;->h:LX/617;

    invoke-virtual {v0, p1, p2}, LX/617;->a(J)LX/614;

    move-result-object v0

    .line 1210580
    if-nez v0, :cond_1

    .line 1210581
    const/4 p3, 0x0

    goto :goto_2

    .line 1210582
    :cond_1
    const-string v1, "convertColor"

    const v2, 0x4123cbdd

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1210583
    :try_start_1
    iget-object v1, p0, LX/7TN;->q:LX/7T3;

    iget-object v1, v1, LX/7T3;->a:Ljava/nio/ByteBuffer;

    iget-object v2, p0, LX/7TN;->q:LX/7T3;

    iget-wide v2, v2, LX/7T3;->b:J

    invoke-direct {p0, v1, v0, v2, v3}, LX/7TN;->a(Ljava/nio/ByteBuffer;LX/614;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1210584
    const v1, 0x2d53fa02

    invoke-static {v1}, LX/02m;->a(I)V

    .line 1210585
    iget-object v1, p0, LX/7TN;->h:LX/617;

    invoke-virtual {v1, v0}, LX/617;->a(LX/614;)V

    .line 1210586
    const/4 v0, 0x0

    iput-object v0, p0, LX/7TN;->q:LX/7T3;

    goto :goto_0

    .line 1210587
    :catchall_1
    move-exception v0

    const v1, -0x7479d666

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1210588
    :cond_2
    iget-object v6, v0, LX/7Sz;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1210589
    iget v7, v0, LX/7Sz;->c:I

    iget v8, v0, LX/7Sz;->d:I

    iget-object v6, v0, LX/7Sz;->e:LX/60w;

    iget v9, v6, LX/60w;->openGlConstant:I

    const/16 v10, 0x1401

    iget-object v11, v0, LX/7Sz;->n:Ljava/nio/ByteBuffer;

    move v6, v5

    invoke-static/range {v5 .. v11}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 1210590
    iput-boolean v5, v0, LX/7Sz;->p:Z

    .line 1210591
    new-instance v5, LX/7T3;

    iget-object v6, v0, LX/7Sz;->n:Ljava/nio/ByteBuffer;

    iget-wide v7, v0, LX/7Sz;->o:J

    invoke-direct {v5, v6, v7, v8}, LX/7T3;-><init>(Ljava/nio/ByteBuffer;J)V

    goto :goto_1
.end method


# virtual methods
.method public final a(J)LX/614;
    .locals 1

    .prologue
    .line 1210563
    iget-object v0, p0, LX/7TN;->i:LX/617;

    invoke-virtual {v0, p1, p2}, LX/617;->a(J)LX/614;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/614;)V
    .locals 4

    .prologue
    .line 1210564
    iget-object v0, p0, LX/7TN;->i:LX/617;

    invoke-virtual {v0, p1}, LX/617;->a(LX/614;)V

    .line 1210565
    iget-object v0, p0, LX/7TN;->r:Ljava/util/Queue;

    invoke-virtual {p1}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v1

    iget-wide v2, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1210566
    iget v0, p0, LX/7TN;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7TN;->l:I

    .line 1210567
    return-void
.end method

.method public final a(LX/7Sx;)V
    .locals 5

    .prologue
    .line 1210490
    iget-object v0, p0, LX/7TN;->b:LX/616;

    invoke-virtual {v0}, LX/616;->a()LX/61A;

    move-result-object v0

    iput-object v0, p0, LX/7TN;->f:LX/61A;

    .line 1210491
    sget-object v0, LX/7TN;->a:LX/0Rf;

    iget-object v1, p0, LX/7TN;->f:LX/61A;

    iget v1, v1, LX/61A;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported color format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/7TN;->f:LX/61A;

    iget v2, v2, LX/61A;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1210492
    new-instance v0, LX/618;

    sget-object v1, LX/612;->CODEC_VIDEO_H264:LX/612;

    iget v2, p1, LX/7Sx;->d:I

    iget v3, p1, LX/7Sx;->e:I

    iget-object v4, p0, LX/7TN;->f:LX/61A;

    iget v4, v4, LX/61A;->b:I

    invoke-direct {v0, v1, v2, v3, v4}, LX/618;-><init>(LX/612;III)V

    iget v1, p1, LX/7Sx;->j:I

    .line 1210493
    iput v1, v0, LX/618;->e:I

    .line 1210494
    move-object v0, v0

    .line 1210495
    iget v1, p1, LX/7Sx;->o:I

    .line 1210496
    iput v1, v0, LX/618;->g:I

    .line 1210497
    move-object v0, v0

    .line 1210498
    iget v1, p1, LX/7Sx;->n:I

    .line 1210499
    iput v1, v0, LX/618;->f:I

    .line 1210500
    move-object v0, v0

    .line 1210501
    invoke-virtual {v0}, LX/618;->a()Landroid/media/MediaFormat;

    move-result-object v0

    iput-object v0, p0, LX/7TN;->j:Landroid/media/MediaFormat;

    .line 1210502
    iget-object v1, p0, LX/7TN;->f:LX/61A;

    iget-object v1, v1, LX/61A;->a:Ljava/lang/String;

    iget-object v2, p0, LX/7TN;->j:Landroid/media/MediaFormat;

    sget-object v3, LX/610;->BUFFERS:LX/610;

    .line 1210503
    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 1210504
    invoke-static {v0, v2, v3}, LX/616;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;LX/610;)LX/617;

    move-result-object v0

    move-object v0, v0

    .line 1210505
    iput-object v0, p0, LX/7TN;->h:LX/617;

    .line 1210506
    iget-object v0, p0, LX/7TN;->h:LX/617;

    invoke-virtual {v0}, LX/617;->a()V

    .line 1210507
    iput-object p1, p0, LX/7TN;->k:LX/7Sx;

    .line 1210508
    return-void
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 5

    .prologue
    .line 1210509
    const-string v0, "mime"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1210510
    iget-object v1, p0, LX/7TN;->b:LX/616;

    invoke-virtual {v1, v0}, LX/616;->b(Ljava/lang/String;)LX/619;

    move-result-object v0

    iput-object v0, p0, LX/7TN;->g:LX/619;

    .line 1210511
    new-instance v0, LX/7Sz;

    iget-object v1, p0, LX/7TN;->c:LX/5Pc;

    iget-object v2, p0, LX/7TN;->k:LX/7Sx;

    iget-object v3, p0, LX/7TN;->g:LX/619;

    iget-object v3, v3, LX/619;->c:LX/60w;

    invoke-direct {v0, v1, v2, v3}, LX/7Sz;-><init>(LX/5Pc;LX/7Sx;LX/60w;)V

    iput-object v0, p0, LX/7TN;->p:LX/7Sz;

    .line 1210512
    iget-object v1, p0, LX/7TN;->g:LX/619;

    iget-object v1, v1, LX/619;->a:Ljava/lang/String;

    iget-object v2, p0, LX/7TN;->p:LX/7Sz;

    .line 1210513
    iget-object v0, v2, LX/7Sz;->i:Landroid/view/Surface;

    move-object v2, v0

    .line 1210514
    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 1210515
    const-string v3, "OMX.qcom.video.decoder.avc"

    invoke-static {v1, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, LX/616;->f:LX/0Rf;

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1210516
    const-string v3, "max-input-size"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1210517
    :cond_0
    invoke-static {v0, p1, v2}, LX/616;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/view/Surface;)LX/617;

    move-result-object v0

    move-object v0, v0

    .line 1210518
    iput-object v0, p0, LX/7TN;->i:LX/617;

    .line 1210519
    iget-object v0, p0, LX/7TN;->i:LX/617;

    invoke-virtual {v0}, LX/617;->a()V

    .line 1210520
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/7TN;->r:Ljava/util/Queue;

    .line 1210521
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1210489
    iget-boolean v0, p0, LX/7TN;->o:Z

    return v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1210484
    iget-boolean v0, p0, LX/7TN;->o:Z

    if-eqz v0, :cond_0

    .line 1210485
    iget v0, p0, LX/7TN;->l:I

    iget v1, p0, LX/7TN;->m:I

    if-eq v0, v1, :cond_0

    .line 1210486
    iget-object v0, p0, LX/7TN;->e:LX/03V;

    const-string v1, "VideoTranscoderOnGPU_mismatched_frame_count"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "input count="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/7TN;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", output count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LX/7TN;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210487
    :cond_0
    invoke-virtual {p0}, LX/7TN;->c()V

    .line 1210488
    return-void
.end method

.method public final b(J)V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1210455
    iget-boolean v0, p0, LX/7TN;->o:Z

    if-eqz v0, :cond_1

    .line 1210456
    :cond_0
    :goto_0
    return-void

    .line 1210457
    :cond_1
    iget-boolean v0, p0, LX/7TN;->n:Z

    if-eqz v0, :cond_2

    .line 1210458
    invoke-direct {p0, p1, p2, v7}, LX/7TN;->a(JZ)Z

    move-result v0

    .line 1210459
    if-eqz v0, :cond_0

    .line 1210460
    iget-object v0, p0, LX/7TN;->h:LX/617;

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v4, v5}, LX/617;->a(J)LX/614;

    move-result-object v1

    .line 1210461
    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    move v3, v2

    invoke-virtual/range {v1 .. v6}, LX/614;->a(IIJI)V

    .line 1210462
    iget-object v0, p0, LX/7TN;->h:LX/617;

    invoke-virtual {v0, v1}, LX/617;->a(LX/614;)V

    .line 1210463
    iput-boolean v7, p0, LX/7TN;->o:Z

    goto :goto_0

    .line 1210464
    :cond_2
    invoke-direct {p0, p1, p2, v2}, LX/7TN;->a(JZ)Z

    .line 1210465
    iget-object v0, p0, LX/7TN;->i:LX/617;

    invoke-virtual {v0, p1, p2}, LX/617;->b(J)LX/614;

    move-result-object v0

    .line 1210466
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/614;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1210467
    invoke-virtual {v0}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v1

    .line 1210468
    iget v2, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_3

    .line 1210469
    iput-boolean v7, p0, LX/7TN;->n:Z

    .line 1210470
    iget-object v1, p0, LX/7TN;->i:LX/617;

    invoke-virtual {v1, v0}, LX/617;->b(LX/614;)V

    goto :goto_0

    .line 1210471
    :cond_3
    iget-object v2, p0, LX/7TN;->i:LX/617;

    invoke-virtual {v2, v0}, LX/617;->b(LX/614;)V

    .line 1210472
    iget-object v0, p0, LX/7TN;->r:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1210473
    iget-object v2, p0, LX/7TN;->g:LX/619;

    iget-boolean v2, v2, LX/619;->b:Z

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 1210474
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 1210475
    :goto_1
    iget-object v2, p0, LX/7TN;->p:LX/7Sz;

    const/4 v10, 0x1

    .line 1210476
    iget-boolean v9, v2, LX/7Sz;->p:Z

    if-nez v9, :cond_5

    move v9, v10

    :goto_2
    invoke-static {v9}, LX/0PB;->checkState(Z)V

    .line 1210477
    iget-object v9, v2, LX/7Sz;->h:LX/7T1;

    invoke-virtual {v9}, LX/7T1;->a()V

    .line 1210478
    iget-object v9, v2, LX/7Sz;->h:LX/7T1;

    invoke-virtual {v9}, LX/7T1;->b()V

    .line 1210479
    iput-wide v0, v2, LX/7Sz;->o:J

    .line 1210480
    iput-boolean v10, v2, LX/7Sz;->p:Z

    .line 1210481
    goto :goto_0

    .line 1210482
    :cond_4
    iget-wide v0, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    goto :goto_1

    .line 1210483
    :cond_5
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public final b(LX/614;)V
    .locals 1

    .prologue
    .line 1210453
    iget-object v0, p0, LX/7TN;->h:LX/617;

    invoke-virtual {v0, p1}, LX/617;->b(LX/614;)V

    .line 1210454
    return-void
.end method

.method public final c(J)LX/614;
    .locals 3

    .prologue
    .line 1210449
    iget-object v0, p0, LX/7TN;->h:LX/617;

    invoke-virtual {v0, p1, p2}, LX/617;->b(J)LX/614;

    move-result-object v0

    .line 1210450
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/614;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1210451
    iget v1, p0, LX/7TN;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/7TN;->m:I

    .line 1210452
    :cond_0
    return-object v0
.end method

.method public final c()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1210426
    iget-object v0, p0, LX/7TN;->i:LX/617;

    if-eqz v0, :cond_0

    .line 1210427
    iget-object v0, p0, LX/7TN;->i:LX/617;

    invoke-virtual {v0}, LX/617;->b()V

    .line 1210428
    iput-object v1, p0, LX/7TN;->i:LX/617;

    .line 1210429
    :cond_0
    iget-object v0, p0, LX/7TN;->h:LX/617;

    if-eqz v0, :cond_1

    .line 1210430
    iget-object v0, p0, LX/7TN;->h:LX/617;

    invoke-virtual {v0}, LX/617;->b()V

    .line 1210431
    iput-object v1, p0, LX/7TN;->h:LX/617;

    .line 1210432
    :cond_1
    iget-object v0, p0, LX/7TN;->p:LX/7Sz;

    if-eqz v0, :cond_4

    .line 1210433
    iget-object v0, p0, LX/7TN;->p:LX/7Sz;

    const/4 v7, 0x0

    .line 1210434
    iget-object v2, v0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eq v2, v3, :cond_2

    .line 1210435
    iget-object v2, v0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, v0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v0, LX/7Sz;->m:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 1210436
    iget-object v2, v0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, v0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v0, LX/7Sz;->l:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 1210437
    iget-object v2, v0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, v0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 1210438
    iget-object v2, v0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, v0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 1210439
    :cond_2
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v2, v0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1210440
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v2, v0, LX/7Sz;->l:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1210441
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v2, v0, LX/7Sz;->m:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1210442
    iget-object v2, v0, LX/7Sz;->i:Landroid/view/Surface;

    if-eqz v2, :cond_3

    .line 1210443
    iget-object v2, v0, LX/7Sz;->i:Landroid/view/Surface;

    invoke-virtual {v2}, Landroid/view/Surface;->release()V

    .line 1210444
    :cond_3
    iput-object v7, v0, LX/7Sz;->f:LX/7T2;

    .line 1210445
    iput-object v7, v0, LX/7Sz;->i:Landroid/view/Surface;

    .line 1210446
    iput-object v7, v0, LX/7Sz;->g:Landroid/graphics/SurfaceTexture;

    .line 1210447
    iput-object v1, p0, LX/7TN;->p:LX/7Sz;

    .line 1210448
    :cond_4
    return-void
.end method

.method public final d()Landroid/media/MediaFormat;
    .locals 1

    .prologue
    .line 1210425
    iget-object v0, p0, LX/7TN;->j:Landroid/media/MediaFormat;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1210424
    iget-object v0, p0, LX/7TN;->k:LX/7Sx;

    invoke-virtual {v0}, LX/7Sx;->a()I

    move-result v0

    return v0
.end method
