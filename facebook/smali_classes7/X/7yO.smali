.class public LX/7yO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:F

.field private final b:Landroid/graphics/PointF;

.field public final c:Landroid/graphics/RectF;

.field private final d:I

.field private final e:I

.field private final f:F

.field public final g:[B

.field public final h:I

.field public final i:I


# direct methods
.method public constructor <init>(FFFFFFFIIF[BII)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1279225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279226
    iput p1, p0, LX/7yO;->a:F

    .line 1279227
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p2, p3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, LX/7yO;->b:Landroid/graphics/PointF;

    .line 1279228
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p4, p5, p6, p7}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, LX/7yO;->c:Landroid/graphics/RectF;

    .line 1279229
    iput p8, p0, LX/7yO;->d:I

    .line 1279230
    iput p9, p0, LX/7yO;->e:I

    .line 1279231
    iput p10, p0, LX/7yO;->f:F

    .line 1279232
    iput-object p11, p0, LX/7yO;->g:[B

    .line 1279233
    iput p12, p0, LX/7yO;->h:I

    .line 1279234
    iput p13, p0, LX/7yO;->i:I

    .line 1279235
    return-void
.end method
