.class public LX/6s1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152861
    iput-object p1, p0, LX/6s1;->a:LX/6rs;

    .line 1152862
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1152863
    const-string v0, "option_id"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152864
    const-string v0, "option_title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152865
    const/4 v0, 0x0

    .line 1152866
    const-string v1, "option_price_list"

    invoke-virtual {p2, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1152867
    iget-object v0, p0, LX/6s1;->a:LX/6rs;

    invoke-virtual {v0, p1}, LX/6rs;->i(Ljava/lang/String;)LX/6rr;

    move-result-object v0

    const-string v1, "option_price_list"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1152868
    :cond_0
    new-instance v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    const-string v2, "option_id"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "option_title"

    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/0Px;)V

    return-object v1
.end method
