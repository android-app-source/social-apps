.class public LX/7SI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[I

.field public b:Z

.field public final c:[LX/5Pf;

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(II)V
    .locals 9

    .prologue
    const/16 v8, 0x1908

    const/4 v7, 0x1

    const/4 v6, 0x0

    const v5, 0x812f

    const/16 v4, 0x2601

    .line 1208499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208500
    new-array v0, v7, [I

    iput-object v0, p0, LX/7SI;->a:[I

    .line 1208501
    iput-boolean v6, p0, LX/7SI;->b:Z

    .line 1208502
    const/4 v0, 0x2

    new-array v0, v0, [LX/5Pf;

    iput-object v0, p0, LX/7SI;->c:[LX/5Pf;

    .line 1208503
    iput p1, p0, LX/7SI;->d:I

    .line 1208504
    iput p2, p0, LX/7SI;->e:I

    .line 1208505
    iget-object v0, p0, LX/7SI;->a:[I

    invoke-static {v7, v0, v6}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    .line 1208506
    iget v0, p0, LX/7SI;->d:I

    iget v1, p0, LX/7SI;->e:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1208507
    iget-object v1, p0, LX/7SI;->c:[LX/5Pf;

    new-instance v2, LX/5Pe;

    invoke-direct {v2}, LX/5Pe;-><init>()V

    invoke-virtual {v2, p1, p2, v0, v8}, LX/5Pe;->a(IILjava/nio/ByteBuffer;I)LX/5Pe;

    move-result-object v2

    const/16 v3, 0x2801

    invoke-virtual {v2, v3, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v2

    const/16 v3, 0x2800

    invoke-virtual {v2, v3, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v2

    const/16 v3, 0x2802

    invoke-virtual {v2, v3, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v2

    const/16 v3, 0x2803

    invoke-virtual {v2, v3, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v2

    invoke-virtual {v2}, LX/5Pe;->a()LX/5Pf;

    move-result-object v2

    aput-object v2, v1, v6

    .line 1208508
    iget-object v1, p0, LX/7SI;->c:[LX/5Pf;

    new-instance v2, LX/5Pe;

    invoke-direct {v2}, LX/5Pe;-><init>()V

    invoke-virtual {v2, p1, p2, v0, v8}, LX/5Pe;->a(IILjava/nio/ByteBuffer;I)LX/5Pe;

    move-result-object v0

    const/16 v2, 0x2801

    invoke-virtual {v0, v2, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v2, 0x2800

    invoke-virtual {v0, v2, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v2, 0x2802

    invoke-virtual {v0, v2, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v2, 0x2803

    invoke-virtual {v0, v2, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    aput-object v0, v1, v7

    .line 1208509
    return-void
.end method


# virtual methods
.method public final a()LX/5Pf;
    .locals 2

    .prologue
    .line 1208510
    iget-boolean v0, p0, LX/7SI;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7SI;->c:[LX/5Pf;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7SI;->c:[LX/5Pf;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method
