.class public LX/6x2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wy;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wy",
        "<",
        "Lcom/facebook/payments/form/model/ItemFormData;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/6x7;

.field public final c:Landroid/content/Intent;

.field public d:LX/6x9;

.field private e:LX/6qh;

.field public f:Lcom/facebook/payments/form/model/ItemFormData;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6x7;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158363
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, LX/6x2;->c:Landroid/content/Intent;

    .line 1158364
    iput-object p1, p0, LX/6x2;->a:Landroid/content/Context;

    .line 1158365
    iput-object p2, p0, LX/6x2;->b:LX/6x7;

    .line 1158366
    return-void
.end method

.method public static a(LX/0QB;)LX/6x2;
    .locals 5

    .prologue
    .line 1158294
    const-class v1, LX/6x2;

    monitor-enter v1

    .line 1158295
    :try_start_0
    sget-object v0, LX/6x2;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1158296
    sput-object v2, LX/6x2;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1158297
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158298
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1158299
    new-instance p0, LX/6x2;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/6x7;->b(LX/0QB;)LX/6x7;

    move-result-object v4

    check-cast v4, LX/6x7;

    invoke-direct {p0, v3, v4}, LX/6x2;-><init>(Landroid/content/Context;LX/6x7;)V

    .line 1158300
    move-object v0, p0

    .line 1158301
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1158302
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6x2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1158303
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1158304
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/payments/form/model/FormFieldAttributes;ILjava/lang/String;)Lcom/facebook/payments/ui/PaymentFormEditTextView;
    .locals 5
    .param p2    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 1158351
    new-instance v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6x2;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;)V

    .line 1158352
    invoke-virtual {v0, p2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setId(I)V

    .line 1158353
    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setBackgroundResource(I)V

    .line 1158354
    iget-object v1, p1, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1158355
    iget-object v1, p1, Lcom/facebook/payments/form/model/FormFieldAttributes;->e:LX/6xO;

    invoke-virtual {v1}, LX/6xO;->getInputType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1158356
    const v1, 0x7fffffff

    iget v2, p1, Lcom/facebook/payments/form/model/FormFieldAttributes;->f:I

    if-eq v1, v2, :cond_0

    .line 1158357
    iget v1, p1, Lcom/facebook/payments/form/model/FormFieldAttributes;->f:I

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setMaxLength(I)V

    .line 1158358
    :cond_0
    iget-object v1, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {v1}, LX/6x7;->b()I

    move-result v1

    iget-object v2, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {v2}, LX/6x7;->c()I

    move-result v2

    iget-object v3, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {v3}, LX/6x7;->b()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setPadding(IIII)V

    .line 1158359
    new-instance v1, LX/6x0;

    invoke-direct {v1, p0, p2, p1, p3}, LX/6x0;-><init>(LX/6x2;ILcom/facebook/payments/form/model/FormFieldAttributes;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/text/TextWatcher;)V

    .line 1158360
    iget-object v1, p1, Lcom/facebook/payments/form/model/FormFieldAttributes;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1158361
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1158346
    invoke-virtual {p0}, LX/6x2;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1158347
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1158348
    const-string v1, "extra_activity_result_data"

    iget-object v2, p0, LX/6x2;->c:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1158349
    iget-object v1, p0, LX/6x2;->e:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v2, v3, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, LX/6qh;->a(LX/73T;)V

    .line 1158350
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1158344
    iput-object p1, p0, LX/6x2;->e:LX/6qh;

    .line 1158345
    return-void
.end method

.method public final a(LX/6x9;)V
    .locals 0

    .prologue
    .line 1158342
    iput-object p1, p0, LX/6x2;->d:LX/6x9;

    .line 1158343
    return-void
.end method

.method public final a(LX/6xD;Lcom/facebook/payments/form/model/PaymentsFormData;)V
    .locals 7

    .prologue
    .line 1158306
    check-cast p2, Lcom/facebook/payments/form/model/ItemFormData;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1158307
    const-string v0, "ItemFormData is not set"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/ItemFormData;

    iput-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    .line 1158308
    iget-object v0, p0, LX/6x2;->c:Landroid/content/Intent;

    const-string v1, "extra_parcelable"

    iget-object v2, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v2, v2, Lcom/facebook/payments/form/model/ItemFormData;->c:Landroid/os/Parcelable;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1158309
    iget-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v0, v0, Lcom/facebook/payments/form/model/ItemFormData;->b:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    if-eqz v0, :cond_3

    .line 1158310
    new-array v0, v4, [Landroid/view/View;

    .line 1158311
    new-instance v1, Lcom/facebook/payments/ui/MediaGridTextLayout;

    iget-object v2, p0, LX/6x2;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/payments/ui/MediaGridTextLayout;-><init>(Landroid/content/Context;)V

    .line 1158312
    iget-object v2, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v2, v2, Lcom/facebook/payments/form/model/ItemFormData;->b:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/MediaGridTextLayout;->setViewParams(Lcom/facebook/payments/ui/MediaGridTextLayoutParams;)V

    .line 1158313
    move-object v1, v1

    .line 1158314
    aput-object v1, v0, v5

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158315
    const v0, 0x7f031392

    invoke-virtual {p1, v0}, LX/6xD;->a(I)V

    .line 1158316
    :cond_0
    :goto_0
    iget-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v0, v0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object v1, LX/6xN;->PRICE:LX/6xN;

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1158317
    new-array v1, v4, [Landroid/view/View;

    iget-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v0, v0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object v2, LX/6xN;->PRICE:LX/6xN;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    const v2, 0x7f0d017d

    const-string v3, "extra_numeric"

    invoke-direct {p0, v0, v2, v3}, LX/6x2;->a(Lcom/facebook/payments/form/model/FormFieldAttributes;ILjava/lang/String;)Lcom/facebook/payments/ui/PaymentFormEditTextView;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-virtual {p1, v1}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158318
    :cond_1
    iget-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget v0, v0, Lcom/facebook/payments/form/model/ItemFormData;->a:I

    if-le v0, v4, :cond_2

    .line 1158319
    const v0, 0x7f031392

    invoke-virtual {p1, v0}, LX/6xD;->a(I)V

    .line 1158320
    new-array v0, v4, [Landroid/view/View;

    iget-object v1, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget v1, v1, Lcom/facebook/payments/form/model/ItemFormData;->a:I

    .line 1158321
    new-instance v2, LX/73e;

    iget-object v3, p0, LX/6x2;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/73e;-><init>(Landroid/content/Context;)V

    .line 1158322
    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, LX/73e;->setBackgroundResource(I)V

    .line 1158323
    iget-object v3, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {v3}, LX/6x7;->b()I

    move-result v3

    iget-object v4, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {v4}, LX/6x7;->c()I

    move-result v4

    iget-object v6, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {v6}, LX/6x7;->b()I

    move-result v6

    iget-object p2, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {p2}, LX/6x7;->c()I

    move-result p2

    invoke-virtual {v2, v3, v4, v6, p2}, LX/73e;->setPadding(IIII)V

    .line 1158324
    new-instance v3, LX/6x1;

    invoke-direct {v3, p0}, LX/6x1;-><init>(LX/6x2;)V

    .line 1158325
    iput-object v3, v2, LX/73e;->g:LX/6x1;

    .line 1158326
    const/4 v3, 0x1

    .line 1158327
    if-gt v3, v1, :cond_4

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 1158328
    iput v3, v2, LX/73e;->d:I

    .line 1158329
    iput v3, v2, LX/73e;->e:I

    .line 1158330
    iput v1, v2, LX/73e;->f:I

    .line 1158331
    iget-object v4, v2, LX/73e;->b:Lcom/facebook/fig/button/FigToggleButton;

    new-instance v6, LX/73c;

    invoke-direct {v6, v2}, LX/73c;-><init>(LX/73e;)V

    invoke-virtual {v4, v6}, Lcom/facebook/fig/button/FigToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1158332
    iget-object v4, v2, LX/73e;->c:Lcom/facebook/fig/button/FigToggleButton;

    new-instance v6, LX/73d;

    invoke-direct {v6, v2}, LX/73d;-><init>(LX/73e;)V

    invoke-virtual {v4, v6}, Lcom/facebook/fig/button/FigToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1158333
    invoke-static {v2}, LX/73e;->b(LX/73e;)V

    .line 1158334
    move-object v1, v2

    .line 1158335
    aput-object v1, v0, v5

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158336
    const v0, 0x7f031341

    invoke-virtual {p1, v0}, LX/6xD;->a(I)V

    .line 1158337
    :cond_2
    return-void

    .line 1158338
    :cond_3
    new-array v1, v4, [Landroid/view/View;

    iget-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v0, v0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object v2, LX/6xN;->TITLE:LX/6xN;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    const v2, 0x7f0d017e

    const-string v3, "extra_title"

    invoke-direct {p0, v0, v2, v3}, LX/6x2;->a(Lcom/facebook/payments/form/model/FormFieldAttributes;ILjava/lang/String;)Lcom/facebook/payments/ui/PaymentFormEditTextView;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-virtual {p1, v1}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158339
    iget-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v0, v0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object v1, LX/6xN;->SUBTITLE:LX/6xN;

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158340
    new-array v1, v4, [Landroid/view/View;

    iget-object v0, p0, LX/6x2;->f:Lcom/facebook/payments/form/model/ItemFormData;

    iget-object v0, v0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object v2, LX/6xN;->SUBTITLE:LX/6xN;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    const v2, 0x7f0d017f

    const-string v3, "extra_subtitle"

    invoke-direct {p0, v0, v2, v3}, LX/6x2;->a(Lcom/facebook/payments/form/model/FormFieldAttributes;ILjava/lang/String;)Lcom/facebook/payments/ui/PaymentFormEditTextView;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-virtual {p1, v1}, LX/6xD;->a([Landroid/view/View;)V

    goto/16 :goto_0

    .line 1158341
    :cond_4
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1158305
    iget-object v0, p0, LX/6x2;->b:LX/6x7;

    invoke-virtual {v0}, LX/6x7;->d()Z

    move-result v0

    return v0
.end method
