.class public final LX/7mS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/7mW;


# direct methods
.method public constructor <init>(LX/7mW;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1237044
    iput-object p1, p0, LX/7mS;->b:LX/7mW;

    iput-object p2, p0, LX/7mS;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1237034
    iget-object v0, p0, LX/7mS;->b:LX/7mW;

    iget-object v1, p0, LX/7mS;->a:Ljava/lang/String;

    const/4 v3, 0x1

    .line 1237035
    iget-object v2, v0, LX/7mW;->f:LX/7mO;

    .line 1237036
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 1237037
    sget-object v4, LX/7mL;->a:LX/0U1;

    invoke-virtual {v4, v1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 1237038
    iget-object v5, v2, LX/7mO;->a:LX/7mK;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "draft_story"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, p0, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1237039
    iget-object v4, v0, LX/7mW;->l:LX/1RX;

    iget-object v2, v0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, LX/1RX;->a(Z)V

    .line 1237040
    move v0, v3

    .line 1237041
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1237042
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 1237043
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method
