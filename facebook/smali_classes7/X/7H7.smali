.class public LX/7H7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7H7;


# instance fields
.field public final a:LX/0hB;


# direct methods
.method public constructor <init>(LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1190423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190424
    iput-object p1, p0, LX/7H7;->a:LX/0hB;

    .line 1190425
    return-void
.end method

.method public static a(LX/0QB;)LX/7H7;
    .locals 4

    .prologue
    .line 1190426
    sget-object v0, LX/7H7;->b:LX/7H7;

    if-nez v0, :cond_1

    .line 1190427
    const-class v1, LX/7H7;

    monitor-enter v1

    .line 1190428
    :try_start_0
    sget-object v0, LX/7H7;->b:LX/7H7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1190429
    if-eqz v2, :cond_0

    .line 1190430
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1190431
    new-instance p0, LX/7H7;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-direct {p0, v3}, LX/7H7;-><init>(LX/0hB;)V

    .line 1190432
    move-object v0, p0

    .line 1190433
    sput-object v0, LX/7H7;->b:LX/7H7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1190434
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1190435
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1190436
    :cond_1
    sget-object v0, LX/7H7;->b:LX/7H7;

    return-object v0

    .line 1190437
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1190438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1190439
    iget-object v0, p0, LX/7H7;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1190440
    iget-object v0, p0, LX/7H7;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iget-object v1, p0, LX/7H7;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
