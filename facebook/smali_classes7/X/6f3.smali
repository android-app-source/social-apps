.class public final enum LX/6f3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6f3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6f3;

.field public static final enum GRAPH:LX/6f3;

.field public static final enum MQTT:LX/6f3;

.field public static final enum SMS:LX/6f3;

.field public static final enum UNKNOWN:LX/6f3;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1118773
    new-instance v0, LX/6f3;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/6f3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f3;->UNKNOWN:LX/6f3;

    .line 1118774
    new-instance v0, LX/6f3;

    const-string v1, "MQTT"

    invoke-direct {v0, v1, v3}, LX/6f3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f3;->MQTT:LX/6f3;

    .line 1118775
    new-instance v0, LX/6f3;

    const-string v1, "GRAPH"

    invoke-direct {v0, v1, v4}, LX/6f3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f3;->GRAPH:LX/6f3;

    .line 1118776
    new-instance v0, LX/6f3;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v5}, LX/6f3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f3;->SMS:LX/6f3;

    .line 1118777
    const/4 v0, 0x4

    new-array v0, v0, [LX/6f3;

    sget-object v1, LX/6f3;->UNKNOWN:LX/6f3;

    aput-object v1, v0, v2

    sget-object v1, LX/6f3;->MQTT:LX/6f3;

    aput-object v1, v0, v3

    sget-object v1, LX/6f3;->GRAPH:LX/6f3;

    aput-object v1, v0, v4

    sget-object v1, LX/6f3;->SMS:LX/6f3;

    aput-object v1, v0, v5

    sput-object v0, LX/6f3;->$VALUES:[LX/6f3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1118778
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6f3;
    .locals 1

    .prologue
    .line 1118779
    const-class v0, LX/6f3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6f3;

    return-object v0
.end method

.method public static values()[LX/6f3;
    .locals 1

    .prologue
    .line 1118780
    sget-object v0, LX/6f3;->$VALUES:[LX/6f3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6f3;

    return-object v0
.end method
