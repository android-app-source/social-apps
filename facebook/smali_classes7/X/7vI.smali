.class public final LX/7vI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/7vJ;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1274360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274361
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7vI;->j:Z

    .line 1274362
    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/model/EventUser;)V
    .locals 1

    .prologue
    .line 1274363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274364
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7vI;->j:Z

    .line 1274365
    iget-object v0, p1, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    move-object v0, v0

    .line 1274366
    iput-object v0, p0, LX/7vI;->a:LX/7vJ;

    .line 1274367
    iget-object v0, p1, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1274368
    iput-object v0, p0, LX/7vI;->b:Ljava/lang/String;

    .line 1274369
    iget-object v0, p1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1274370
    iput-object v0, p0, LX/7vI;->c:Ljava/lang/String;

    .line 1274371
    iget-object v0, p1, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1274372
    iput-object v0, p0, LX/7vI;->d:Ljava/lang/String;

    .line 1274373
    iget-object v0, p1, Lcom/facebook/events/model/EventUser;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1274374
    iput-object v0, p0, LX/7vI;->e:Ljava/lang/String;

    .line 1274375
    iget v0, p1, Lcom/facebook/events/model/EventUser;->g:I

    move v0, v0

    .line 1274376
    iput v0, p0, LX/7vI;->g:I

    .line 1274377
    iget-object v0, p1, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v0

    .line 1274378
    iput-object v0, p0, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1274379
    iget-object v0, p1, Lcom/facebook/events/model/EventUser;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-object v0, v0

    .line 1274380
    iput-object v0, p0, LX/7vI;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1274381
    iget-boolean v0, p1, Lcom/facebook/events/model/EventUser;->j:Z

    move v0, v0

    .line 1274382
    iput-boolean v0, p0, LX/7vI;->j:Z

    .line 1274383
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/model/EventUser;
    .locals 1

    .prologue
    .line 1274384
    new-instance v0, Lcom/facebook/events/model/EventUser;

    invoke-direct {v0, p0}, Lcom/facebook/events/model/EventUser;-><init>(LX/7vI;)V

    return-object v0
.end method
