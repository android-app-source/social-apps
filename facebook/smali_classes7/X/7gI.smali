.class public LX/7gI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/6CD;

.field private final f:LX/2ms;

.field public final g:LX/6CH;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/6CD;LX/2ms;LX/6CH;)V
    .locals 0
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLStoryActionLink;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/6CD;",
            "LX/2ms;",
            "LX/6CH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1224060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224061
    iput-object p2, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1224062
    iput-object p1, p0, LX/7gI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1224063
    iput-object p3, p0, LX/7gI;->c:LX/03V;

    .line 1224064
    iput-object p4, p0, LX/7gI;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1224065
    iput-object p5, p0, LX/7gI;->e:LX/6CD;

    .line 1224066
    iput-object p6, p0, LX/7gI;->f:LX/2ms;

    .line 1224067
    iput-object p7, p0, LX/7gI;->g:LX/6CH;

    .line 1224068
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v2, -0x3aa781c5

    invoke-static {v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1224069
    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1224070
    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1224071
    iget-object v0, p0, LX/7gI;->c:LX/03V;

    const-string v1, "MessengerExtensionActionLinkOnClickListener"

    const-string v3, "actionlink url is null"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224072
    const v0, -0x16c1dc9a

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1224073
    :goto_0
    return-void

    .line 1224074
    :cond_0
    iget-object v0, p0, LX/7gI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1224075
    if-nez v0, :cond_1

    .line 1224076
    iget-object v0, p0, LX/7gI;->c:LX/03V;

    const-string v1, "MessengerExtensionActionLinkOnClickListener"

    const-string v3, "parentStoryProps is null"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224077
    const v0, -0xc5908af

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1224078
    :cond_1
    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-static {v0}, LX/2ms;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v3

    .line 1224079
    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-static {v0}, LX/2ms;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v4

    .line 1224080
    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v5

    .line 1224081
    iget-object v0, p0, LX/7gI;->e:LX/6CD;

    .line 1224082
    new-instance v6, LX/6CC;

    invoke-direct {v6, v0}, LX/6CC;-><init>(LX/6CD;)V

    move-object v6, v6

    .line 1224083
    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    .line 1224084
    iput-object v0, v6, LX/6CC;->i:Ljava/lang/String;

    .line 1224085
    move-object v0, v6

    .line 1224086
    iput-object v4, v0, LX/6CC;->b:Ljava/lang/String;

    .line 1224087
    move-object v7, v0

    .line 1224088
    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->v()Ljava/lang/String;

    move-result-object v0

    .line 1224089
    :goto_1
    iput-object v0, v7, LX/6CC;->c:Ljava/lang/String;

    .line 1224090
    move-object v0, v7

    .line 1224091
    iput-object v3, v0, LX/6CC;->e:Ljava/lang/String;

    .line 1224092
    move-object v7, v0

    .line 1224093
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 1224094
    :goto_2
    iput-object v0, v7, LX/6CC;->f:Ljava/lang/String;

    .line 1224095
    move-object v0, v7

    .line 1224096
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;->j()Ljava/lang/String;

    move-result-object v1

    .line 1224097
    :cond_2
    iput-object v1, v0, LX/6CC;->g:Ljava/lang/String;

    .line 1224098
    move-object v0, v0

    .line 1224099
    iget-object v1, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh()Ljava/lang/String;

    move-result-object v1

    .line 1224100
    iput-object v1, v0, LX/6CC;->d:Ljava/lang/String;

    .line 1224101
    move-object v0, v0

    .line 1224102
    sget-object v1, LX/6CI;->STORY_ATTACHMENT:LX/6CI;

    .line 1224103
    iput-object v1, v0, LX/6CC;->k:LX/6CI;

    .line 1224104
    move-object v0, v0

    .line 1224105
    sget-object v1, LX/6CE;->NEWSFEED_AD:LX/6CE;

    iget-object v1, v1, LX/6CE;->value:Ljava/lang/String;

    .line 1224106
    iput-object v1, v0, LX/6CC;->j:Ljava/lang/String;

    .line 1224107
    move-object v0, v0

    .line 1224108
    iget-object v1, p0, LX/7gI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi()LX/0Px;

    move-result-object v1

    .line 1224109
    iput-object v1, v0, LX/6CC;->h:Ljava/util/List;

    .line 1224110
    move-object v0, v0

    .line 1224111
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6CC;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1224112
    iget-object v1, p0, LX/7gI;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v1, v0, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1224113
    invoke-virtual {v6}, LX/6CC;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1224114
    iget-object v0, p0, LX/7gI;->f:LX/2ms;

    iget-object v1, p0, LX/7gI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    new-instance v5, LX/7gH;

    invoke-direct {v5, p0, v4, v3}, LX/7gH;-><init>(LX/7gI;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1, v5}, LX/2ms;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7gH;)V

    .line 1224115
    :cond_3
    const v0, 0x43dd46f8

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 1224116
    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method
