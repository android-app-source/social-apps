.class public LX/8Mj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7ma;

.field private final b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field public final c:LX/1EZ;

.field private final d:LX/0qn;

.field private final e:LX/0bH;

.field public final f:LX/0SG;

.field public final g:LX/03V;

.field public final h:LX/1RW;

.field public final i:J

.field public j:J


# direct methods
.method public constructor <init>(LX/7ma;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/1EZ;LX/0qn;LX/0bH;LX/0SG;LX/03V;LX/1RW;LX/0ad;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1335013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335014
    iput-object p1, p0, LX/8Mj;->a:LX/7ma;

    .line 1335015
    iput-object p2, p0, LX/8Mj;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1335016
    iput-object p3, p0, LX/8Mj;->c:LX/1EZ;

    .line 1335017
    iput-object p4, p0, LX/8Mj;->d:LX/0qn;

    .line 1335018
    iput-object p5, p0, LX/8Mj;->e:LX/0bH;

    .line 1335019
    iput-object p6, p0, LX/8Mj;->f:LX/0SG;

    .line 1335020
    iput-object p7, p0, LX/8Mj;->g:LX/03V;

    .line 1335021
    iput-object p8, p0, LX/8Mj;->h:LX/1RW;

    .line 1335022
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/8Mj;->j:J

    .line 1335023
    sget-wide v0, LX/1aO;->ag:J

    const-wide/16 v2, 0x7d0

    invoke-interface {p9, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/8Mj;->i:J

    .line 1335024
    return-void
.end method

.method public static a(LX/5Ro;)LX/8Kx;
    .locals 1

    .prologue
    .line 1335025
    sget-object v0, LX/5Ro;->OFFLINE_POSTING_HEADER:LX/5Ro;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5Ro;->COMPOST:LX/5Ro;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5Ro;->NOTIFICATION:LX/5Ro;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1335026
    :goto_0
    if-eqz v0, :cond_2

    sget-object v0, LX/8Kx;->UserRetry:LX/8Kx;

    :goto_1
    return-object v0

    .line 1335027
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1335028
    :cond_2
    sget-object v0, LX/8Kx;->AutoRetry:LX/8Kx;

    goto :goto_1
.end method

.method public static a(LX/7ml;)Z
    .locals 2

    .prologue
    .line 1335029
    sget-object v0, LX/8Mi;->a:[I

    .line 1335030
    iget-object v1, p0, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1335031
    invoke-virtual {v1}, LX/7mk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1335032
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1335033
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(LX/8Mj;LX/7ml;LX/5Ro;)V
    .locals 5

    .prologue
    .line 1335034
    iget-object v0, p0, LX/8Mj;->d:LX/0qn;

    .line 1335035
    iget-object v1, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1335036
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1335037
    iget-object v0, p1, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    move-object v0, v0

    .line 1335038
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1335039
    :goto_0
    return-void

    .line 1335040
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v1

    .line 1335041
    iget-object v2, p0, LX/8Mj;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->setRetrySource(LX/5Ro;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v4

    .line 1335042
    invoke-static {v2, v1, v3, v4}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 1335043
    iget-object v1, p0, LX/8Mj;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1335044
    iget-object v2, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v2, v2

    .line 1335045
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->c(Ljava/lang/String;)V

    .line 1335046
    iget-object v1, p0, LX/8Mj;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 1335047
    iget-object v1, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1335048
    invoke-static {v1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/composer/publish/common/PendingStory;->b(JZ)V

    .line 1335049
    iget-object v0, p0, LX/8Mj;->e:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
