.class public final LX/6pA;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/0gc;)V
    .locals 0

    .prologue
    .line 1148921
    iput-object p1, p0, LX/6pA;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 1148922
    iget-object v0, p0, LX/6pA;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    .line 1148923
    invoke-static {v0, p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;I)LX/6pM;

    move-result-object v1

    .line 1148924
    invoke-virtual {v1}, LX/6pM;->getAnalyticsEvent()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1148925
    const-string v2, "p2p_settings"

    invoke-virtual {v1}, LX/6pM;->getAnalyticsEvent()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    .line 1148926
    :cond_0
    iget-object v2, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {v1, v2, p0, p1}, LX/6pM;->getFragment(Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;Landroid/content/res/Resources;I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    move-object v0, v1

    .line 1148927
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1148928
    iget-object v0, p0, LX/6pA;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    invoke-virtual {v0}, LX/6pY;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
