.class public LX/6kG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final acknowledged:Ljava/lang/Boolean;

.field public final answered:Ljava/lang/Boolean;

.field public final callId:Ljava/lang/String;

.field public final duration:Ljava/lang/Long;

.field public final eventType:Ljava/lang/Integer;

.field public final messageMetadata:LX/6kn;

.field public final startTime:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 1135704
    new-instance v0, LX/1sv;

    const-string v1, "DeltaRTCEventLog"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kG;->b:LX/1sv;

    .line 1135705
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kG;->c:LX/1sw;

    .line 1135706
    new-instance v0, LX/1sw;

    const-string v1, "answered"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kG;->d:LX/1sw;

    .line 1135707
    new-instance v0, LX/1sw;

    const-string v1, "startTime"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kG;->e:LX/1sw;

    .line 1135708
    new-instance v0, LX/1sw;

    const-string v1, "duration"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kG;->f:LX/1sw;

    .line 1135709
    new-instance v0, LX/1sw;

    const-string v1, "eventType"

    const/16 v2, 0x8

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kG;->g:LX/1sw;

    .line 1135710
    new-instance v0, LX/1sw;

    const-string v1, "acknowledged"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kG;->h:LX/1sw;

    .line 1135711
    new-instance v0, LX/1sw;

    const-string v1, "callId"

    const/16 v2, 0xb

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kG;->i:LX/1sw;

    .line 1135712
    sput-boolean v5, LX/6kG;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1135695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135696
    iput-object p1, p0, LX/6kG;->messageMetadata:LX/6kn;

    .line 1135697
    iput-object p2, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    .line 1135698
    iput-object p3, p0, LX/6kG;->startTime:Ljava/lang/Long;

    .line 1135699
    iput-object p4, p0, LX/6kG;->duration:Ljava/lang/Long;

    .line 1135700
    iput-object p5, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    .line 1135701
    iput-object p6, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    .line 1135702
    iput-object p7, p0, LX/6kG;->callId:Ljava/lang/String;

    .line 1135703
    return-void
.end method

.method private static a(LX/6kG;)V
    .locals 4

    .prologue
    .line 1135690
    iget-object v0, p0, LX/6kG;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1135691
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kG;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135692
    :cond_0
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6ky;->a:LX/1sn;

    iget-object v1, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1135693
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'eventType\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1135694
    :cond_1
    return-void
.end method

.method public static b(LX/1su;)LX/6kG;
    .locals 12

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x2

    const/4 v7, 0x0

    .line 1135660
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 1135661
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1135662
    iget-byte v8, v0, LX/1sw;->b:B

    if-eqz v8, :cond_7

    .line 1135663
    iget-short v8, v0, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 1135664
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1135665
    :pswitch_0
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xc

    if-ne v8, v9, :cond_0

    .line 1135666
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v1

    goto :goto_0

    .line 1135667
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1135668
    :pswitch_1
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_1

    .line 1135669
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 1135670
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1135671
    :pswitch_2
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_2

    .line 1135672
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1135673
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1135674
    :pswitch_3
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_3

    .line 1135675
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1135676
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1135677
    :pswitch_4
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0x8

    if-ne v8, v9, :cond_4

    .line 1135678
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    .line 1135679
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1135680
    :pswitch_5
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_5

    .line 1135681
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0

    .line 1135682
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1135683
    :pswitch_6
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xb

    if-ne v8, v9, :cond_6

    .line 1135684
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1135685
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1135686
    :cond_7
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1135687
    new-instance v0, LX/6kG;

    invoke-direct/range {v0 .. v7}, LX/6kG;-><init>(LX/6kn;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 1135688
    invoke-static {v0}, LX/6kG;->a(LX/6kG;)V

    .line 1135689
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135713
    if-eqz p2, :cond_6

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1135714
    :goto_0
    if-eqz p2, :cond_7

    const-string v0, "\n"

    move-object v2, v0

    .line 1135715
    :goto_1
    if-eqz p2, :cond_8

    const-string v0, " "

    move-object v1, v0

    .line 1135716
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaRTCEventLog"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135717
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135718
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135719
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135720
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135721
    const-string v0, "messageMetadata"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135722
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135723
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135724
    iget-object v0, p0, LX/6kG;->messageMetadata:LX/6kn;

    if-nez v0, :cond_9

    .line 1135725
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135726
    :goto_3
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1135727
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135728
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135729
    const-string v0, "answered"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135730
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135731
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135732
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    .line 1135733
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135734
    :cond_0
    :goto_4
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1135735
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135736
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135737
    const-string v0, "startTime"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135738
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135739
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135740
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    if-nez v0, :cond_b

    .line 1135741
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135742
    :cond_1
    :goto_5
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1135743
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135744
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135745
    const-string v0, "duration"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135746
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135747
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135748
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    if-nez v0, :cond_c

    .line 1135749
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135750
    :cond_2
    :goto_6
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1135751
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135752
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135753
    const-string v0, "eventType"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135754
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135755
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135756
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    if-nez v0, :cond_d

    .line 1135757
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135758
    :cond_3
    :goto_7
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1135759
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135760
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135761
    const-string v0, "acknowledged"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135762
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135763
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135764
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    .line 1135765
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135766
    :cond_4
    :goto_8
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1135767
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135768
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135769
    const-string v0, "callId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135770
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135771
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135772
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    if-nez v0, :cond_10

    .line 1135773
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135774
    :cond_5
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135775
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135776
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135777
    :cond_6
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1135778
    :cond_7
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1135779
    :cond_8
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1135780
    :cond_9
    iget-object v0, p0, LX/6kG;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1135781
    :cond_a
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1135782
    :cond_b
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1135783
    :cond_c
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1135784
    :cond_d
    sget-object v0, LX/6ky;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1135785
    if-eqz v0, :cond_e

    .line 1135786
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135787
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135788
    :cond_e
    iget-object v5, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1135789
    if-eqz v0, :cond_3

    .line 1135790
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1135791
    :cond_f
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1135792
    :cond_10
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1135628
    invoke-static {p0}, LX/6kG;->a(LX/6kG;)V

    .line 1135629
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135630
    iget-object v0, p0, LX/6kG;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1135631
    sget-object v0, LX/6kG;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135632
    iget-object v0, p0, LX/6kG;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1135633
    :cond_0
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1135634
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1135635
    sget-object v0, LX/6kG;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135636
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1135637
    :cond_1
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1135638
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1135639
    sget-object v0, LX/6kG;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135640
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1135641
    :cond_2
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1135642
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1135643
    sget-object v0, LX/6kG;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135644
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1135645
    :cond_3
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1135646
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1135647
    sget-object v0, LX/6kG;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135648
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1135649
    :cond_4
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1135650
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1135651
    sget-object v0, LX/6kG;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135652
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1135653
    :cond_5
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1135654
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1135655
    sget-object v0, LX/6kG;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135656
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1135657
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135658
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135659
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135571
    if-nez p1, :cond_1

    .line 1135572
    :cond_0
    :goto_0
    return v0

    .line 1135573
    :cond_1
    instance-of v1, p1, LX/6kG;

    if-eqz v1, :cond_0

    .line 1135574
    check-cast p1, LX/6kG;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135575
    if-nez p1, :cond_3

    .line 1135576
    :cond_2
    :goto_1
    move v0, v2

    .line 1135577
    goto :goto_0

    .line 1135578
    :cond_3
    iget-object v0, p0, LX/6kG;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1135579
    :goto_2
    iget-object v3, p1, LX/6kG;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1135580
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135581
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135582
    iget-object v0, p0, LX/6kG;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kG;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135583
    :cond_5
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1135584
    :goto_4
    iget-object v3, p1, LX/6kG;->answered:Ljava/lang/Boolean;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1135585
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1135586
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135587
    iget-object v0, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kG;->answered:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135588
    :cond_7
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1135589
    :goto_6
    iget-object v3, p1, LX/6kG;->startTime:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1135590
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1135591
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135592
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    iget-object v3, p1, LX/6kG;->startTime:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135593
    :cond_9
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1135594
    :goto_8
    iget-object v3, p1, LX/6kG;->duration:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1135595
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1135596
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135597
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    iget-object v3, p1, LX/6kG;->duration:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135598
    :cond_b
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1135599
    :goto_a
    iget-object v3, p1, LX/6kG;->eventType:Ljava/lang/Integer;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1135600
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1135601
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135602
    iget-object v0, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kG;->eventType:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135603
    :cond_d
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1135604
    :goto_c
    iget-object v3, p1, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1135605
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1135606
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135607
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135608
    :cond_f
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1135609
    :goto_e
    iget-object v3, p1, LX/6kG;->callId:Ljava/lang/String;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1135610
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1135611
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135612
    iget-object v0, p0, LX/6kG;->callId:Ljava/lang/String;

    iget-object v3, p1, LX/6kG;->callId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 1135613
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1135614
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 1135615
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1135616
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 1135617
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1135618
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 1135619
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1135620
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 1135621
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 1135622
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 1135623
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 1135624
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 1135625
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 1135626
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 1135627
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135570
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135567
    sget-boolean v0, LX/6kG;->a:Z

    .line 1135568
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kG;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135569
    return-object v0
.end method
