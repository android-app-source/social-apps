.class public LX/6nh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/base/fragment/FbFragment;

.field public final b:LX/6nc;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/payments/auth/pin/model/PaymentPin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6ng;)V
    .locals 1

    .prologue
    .line 1147508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147509
    iget-object v0, p1, LX/6ng;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    iput-object v0, p0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 1147510
    iget-object v0, p1, LX/6ng;->b:LX/6nc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6nc;

    iput-object v0, p0, LX/6nh;->b:LX/6nc;

    .line 1147511
    iget-object v0, p1, LX/6ng;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6nh;->c:Ljava/lang/String;

    .line 1147512
    iget-object v0, p1, LX/6ng;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    iput-object v0, p0, LX/6nh;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1147513
    return-void
.end method

.method public static newBuilder()LX/6ng;
    .locals 2

    .prologue
    .line 1147514
    new-instance v0, LX/6ng;

    invoke-direct {v0}, LX/6ng;-><init>()V

    return-object v0
.end method
