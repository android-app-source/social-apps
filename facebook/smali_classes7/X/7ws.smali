.class public LX/7ws;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

.field public final c:LX/7wO;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z


# direct methods
.method public constructor <init>(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276714
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7ws;->e:Z

    .line 1276715
    iput p1, p0, LX/7ws;->a:I

    .line 1276716
    iput-object p2, p0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1276717
    iput-object p3, p0, LX/7ws;->c:LX/7wO;

    .line 1276718
    iput-object p4, p0, LX/7ws;->d:Ljava/lang/String;

    .line 1276719
    return-void
.end method

.method public static a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276720
    new-instance v0, LX/7ws;

    invoke-direct {v0, p0, p1, p2, p3}, LX/7ws;-><init>(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/7wO;)LX/7ws;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1276721
    new-instance v0, LX/7ws;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2, p0, v2}, LX/7ws;-><init>(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/7ws;
    .locals 4

    .prologue
    .line 1276722
    new-instance v0, LX/7ws;

    const/4 v1, -0x1

    const/4 v2, 0x0

    sget-object v3, LX/7wO;->ERROR:LX/7wO;

    invoke-direct {v0, v1, v2, v3, p0}, LX/7ws;-><init>(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)V

    return-object v0
.end method
