.class public LX/6jC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6jC;


# instance fields
.field private final a:LX/2Uq;


# direct methods
.method public constructor <init>(LX/2Uq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1130338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130339
    iput-object p1, p0, LX/6jC;->a:LX/2Uq;

    .line 1130340
    return-void
.end method

.method public static a(LX/0QB;)LX/6jC;
    .locals 4

    .prologue
    .line 1130325
    sget-object v0, LX/6jC;->b:LX/6jC;

    if-nez v0, :cond_1

    .line 1130326
    const-class v1, LX/6jC;

    monitor-enter v1

    .line 1130327
    :try_start_0
    sget-object v0, LX/6jC;->b:LX/6jC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1130328
    if-eqz v2, :cond_0

    .line 1130329
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1130330
    new-instance p0, LX/6jC;

    invoke-static {v0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v3

    check-cast v3, LX/2Uq;

    invoke-direct {p0, v3}, LX/6jC;-><init>(LX/2Uq;)V

    .line 1130331
    move-object v0, p0

    .line 1130332
    sput-object v0, LX/6jC;->b:LX/6jC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1130333
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1130334
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1130335
    :cond_1
    sget-object v0, LX/6jC;->b:LX/6jC;

    return-object v0

    .line 1130336
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1130337
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
