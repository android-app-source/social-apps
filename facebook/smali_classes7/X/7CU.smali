.class public LX/7CU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Re;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7CT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Rd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1180936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1180937
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1180938
    iput-object v0, p0, LX/7CU;->a:LX/0Ot;

    .line 1180939
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1180940
    iput-object v0, p0, LX/7CU;->b:LX/0Ot;

    .line 1180941
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1180942
    iput-object v0, p0, LX/7CU;->c:LX/0Ot;

    .line 1180943
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1180944
    iput-object v0, p0, LX/7CU;->d:LX/0Ot;

    .line 1180945
    return-void
.end method

.method private a(LX/0Px;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1180946
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1180947
    iget-object v0, p0, LX/7CU;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->cj:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 1180948
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1180949
    if-eqz v4, :cond_1

    iget-object v1, p0, LX/7CU;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7CT;

    .line 1180950
    invoke-static {v0}, LX/7CT;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1180951
    iget-object v6, v1, LX/7CT;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/text/Collator;

    invoke-virtual {v6, v7}, Ljava/text/Collator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v6

    .line 1180952
    invoke-virtual {v6}, Ljava/text/CollationKey;->toByteArray()[B

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v6, v7}, LX/1u4;->a([BZ)Ljava/lang/String;

    move-result-object v6

    move-object v0, v6

    .line 1180953
    :goto_1
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1180954
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1180955
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1180956
    :cond_1
    iget-object v1, p0, LX/7CU;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Rd;

    invoke-virtual {v1, v0}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1180957
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7CU;
    .locals 5

    .prologue
    .line 1180958
    new-instance v0, LX/7CU;

    invoke-direct {v0}, LX/7CU;-><init>()V

    .line 1180959
    const/16 v1, 0x295

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3517

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x12d5

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1032

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 1180960
    iput-object v1, v0, LX/7CU;->a:LX/0Ot;

    iput-object v2, v0, LX/7CU;->b:LX/0Ot;

    iput-object v3, v0, LX/7CU;->c:LX/0Ot;

    iput-object v4, v0, LX/7CU;->d:LX/0Ot;

    .line 1180961
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1180962
    iget-object v0, p0, LX/7CU;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->bb:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7CU;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->bx:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    const/4 v3, -0x1

    .line 1180963
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1180964
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    .line 1180965
    const/4 v1, 0x0

    move v2, v3

    move v0, v3

    :goto_0
    if-ge v1, v5, :cond_4

    .line 1180966
    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    .line 1180967
    invoke-static {v6}, Ljava/lang/Character;->charCount(I)I

    move-result v7

    .line 1180968
    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {v6}, Ljava/lang/Character;->isISOControl(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1180969
    :cond_1
    if-lez v2, :cond_7

    .line 1180970
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v2, v0

    move v0, v3

    .line 1180971
    :goto_1
    add-int/2addr v1, v7

    move v9, v0

    move v0, v2

    move v2, v9

    .line 1180972
    goto :goto_0

    .line 1180973
    :cond_2
    if-ne v2, v3, :cond_3

    move v0, v1

    .line 1180974
    :cond_3
    add-int v2, v1, v7

    move v9, v2

    move v2, v0

    move v0, v9

    goto :goto_1

    .line 1180975
    :cond_4
    if-lez v2, :cond_5

    .line 1180976
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1180977
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1180978
    :goto_2
    invoke-direct {p0, v0}, LX/7CU;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1180979
    :cond_6
    iget-object v0, p0, LX/7CU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Re;

    invoke-virtual {v0, p1}, LX/2Re;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    goto :goto_2

    :cond_7
    move v9, v2

    move v2, v0

    move v0, v9

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1180980
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/7CU;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
