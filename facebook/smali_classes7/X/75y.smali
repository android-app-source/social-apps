.class public LX/75y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/HTTPResponseHandler;


# instance fields
.field private final mBuffer:LX/161;

.field public mBufferInputStream:LX/762;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mErrorReporter:LX/03V;

.field public mEventHandler:LX/6Xa;

.field private final mHeadersArrivedCondition:Ljava/lang/Object;

.field private final mHostname:Ljava/lang/String;

.field public final mHttpFlowStatistics:LX/1iW;

.field public mLoomLogId:J

.field public mLoomMatchId:I

.field private mNetworkException:LX/5oc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile mReadState:LX/75x;

.field public final mRequestStatsObserver:LX/4ib;

.field public mResponse:Lorg/apache/http/HttpResponse;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/161;LX/6Xa;LX/03V;LX/4ib;LX/1iW;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1170332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170333
    sget-object v0, LX/75x;->NO_RESPONSE:LX/75x;

    iput-object v0, p0, LX/75y;->mReadState:LX/75x;

    .line 1170334
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    .line 1170335
    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1170336
    if-eqz p3, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1170337
    iput-object p1, p0, LX/75y;->mHostname:Ljava/lang/String;

    .line 1170338
    iput-object p2, p0, LX/75y;->mBuffer:LX/161;

    .line 1170339
    iput-object p3, p0, LX/75y;->mEventHandler:LX/6Xa;

    .line 1170340
    iput-object p4, p0, LX/75y;->mErrorReporter:LX/03V;

    .line 1170341
    iput-object p5, p0, LX/75y;->mRequestStatsObserver:LX/4ib;

    .line 1170342
    iput-object p6, p0, LX/75y;->mHttpFlowStatistics:LX/1iW;

    .line 1170343
    return-void

    :cond_0
    move v0, v2

    .line 1170344
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1170345
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;LX/161;LX/6Xa;LX/03V;LX/4ib;LX/1iW;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 1170325
    invoke-direct/range {p0 .. p6}, LX/75y;-><init>(Ljava/lang/String;LX/161;LX/6Xa;LX/03V;LX/4ib;LX/1iW;)V

    .line 1170326
    int-to-long v0, p7

    iput-wide v0, p0, LX/75y;->mLoomLogId:J

    .line 1170327
    const/4 v0, -0x1

    iput v0, p0, LX/75y;->mLoomMatchId:I

    .line 1170328
    iget-wide v0, p0, LX/75y;->mLoomLogId:J

    invoke-static {v0, v1}, Lcom/facebook/loom/logger/api/LoomLogger;->a(J)I

    move-result v0

    iput v0, p0, LX/75y;->mLoomMatchId:I

    .line 1170329
    iget v0, p0, LX/75y;->mLoomMatchId:I

    iget-wide v2, p0, LX/75y;->mLoomLogId:J

    invoke-static {v0, v2, v3, p8}, Lcom/facebook/loom/logger/api/LoomLogger;->a(IJLjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/75y;->mLoomMatchId:I

    .line 1170330
    iget-wide v0, p0, LX/75y;->mLoomLogId:J

    invoke-static {v0, v1}, Lcom/facebook/loom/logger/api/LoomLogger;->b(J)I

    move-result v0

    iput v0, p0, LX/75y;->mLoomMatchId:I

    .line 1170331
    return-void
.end method

.method private handleError(Lcom/facebook/proxygen/HTTPRequestError;)V
    .locals 4
    .param p1    # Lcom/facebook/proxygen/HTTPRequestError;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1170306
    if-nez p1, :cond_0

    .line 1170307
    new-instance p1, Lcom/facebook/proxygen/HTTPRequestError;

    const-string v0, "Error is null"

    sget-object v1, Lcom/facebook/proxygen/HTTPRequestError$HTTPRequestStage;->Unknown:Lcom/facebook/proxygen/HTTPRequestError$HTTPRequestStage;

    sget-object v2, Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;->Unknown:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    invoke-direct {p1, v0, v1, v2}, Lcom/facebook/proxygen/HTTPRequestError;-><init>(Ljava/lang/String;Lcom/facebook/proxygen/HTTPRequestError$HTTPRequestStage;Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;)V

    .line 1170308
    :cond_0
    iget-object v0, p0, LX/75y;->mHttpFlowStatistics:LX/1iW;

    if-eqz v0, :cond_1

    .line 1170309
    iget-object v0, p1, Lcom/facebook/proxygen/HTTPRequestError;->mErrCode:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    move-object v0, v0

    .line 1170310
    sget-object v1, Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;->Canceled:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    if-ne v0, v1, :cond_4

    .line 1170311
    iget-object v0, p0, LX/75y;->mHttpFlowStatistics:LX/1iW;

    const-string v1, "cancelled"

    .line 1170312
    iput-object v1, v0, LX/1iW;->i:Ljava/lang/String;

    .line 1170313
    :cond_1
    :goto_0
    iget-object v0, p0, LX/75y;->mRequestStatsObserver:LX/4ib;

    invoke-virtual {v0}, LX/4ib;->getRequestStats()LX/4iZ;

    move-result-object v0

    .line 1170314
    iget-wide v2, p0, LX/75y;->mLoomLogId:J

    invoke-static {v2, v3}, Lcom/facebook/loom/logger/api/LoomLogger;->e(J)I

    move-result v1

    iput v1, p0, LX/75y;->mLoomMatchId:I

    .line 1170315
    if-eqz v0, :cond_2

    .line 1170316
    iget-object v1, p0, LX/75y;->mEventHandler:LX/6Xa;

    iget-wide v2, p0, LX/75y;->mLoomLogId:J

    invoke-interface {v1, v0, v2, v3}, LX/6Xa;->decorateStatistics(LX/4iZ;J)V

    .line 1170317
    :cond_2
    sget-object v0, LX/75x;->ERROR:LX/75x;

    iput-object v0, p0, LX/75y;->mReadState:LX/75x;

    .line 1170318
    new-instance v0, LX/5oc;

    invoke-direct {v0, p1}, LX/5oc;-><init>(Lcom/facebook/proxygen/HTTPRequestError;)V

    iput-object v0, p0, LX/75y;->mNetworkException:LX/5oc;

    .line 1170319
    iget-object v0, p0, LX/75y;->mBufferInputStream:LX/762;

    if-eqz v0, :cond_3

    .line 1170320
    iget-object v0, p0, LX/75y;->mBufferInputStream:LX/762;

    iget-object v1, p0, LX/75y;->mNetworkException:LX/5oc;

    invoke-virtual {v0, v1}, LX/762;->setError(LX/5oc;)V

    .line 1170321
    :cond_3
    return-void

    .line 1170322
    :cond_4
    iget-object v0, p0, LX/75y;->mHttpFlowStatistics:LX/1iW;

    const-string v1, "error"

    .line 1170323
    iput-object v1, v0, LX/1iW;->i:Ljava/lang/String;

    .line 1170324
    goto :goto_0
.end method

.method private handleOnResponse(ILjava/lang/String;[Lorg/apache/http/Header;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [Lorg/apache/http/Header;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1170279
    const/4 v1, 0x1

    new-array v1, v1, [LX/75x;

    sget-object v2, LX/75x;->NO_RESPONSE:LX/75x;

    aput-object v2, v1, v0

    invoke-static {p0, v1}, LX/75y;->verifyState(LX/75y;[LX/75x;)V

    .line 1170280
    iget-wide v2, p0, LX/75y;->mLoomLogId:J

    invoke-static {v2, v3}, Lcom/facebook/loom/logger/api/LoomLogger;->c(J)I

    move-result v1

    iput v1, p0, LX/75y;->mLoomMatchId:I

    .line 1170281
    if-nez p2, :cond_0

    .line 1170282
    const-string p2, "empty"

    .line 1170283
    :cond_0
    if-nez p3, :cond_1

    .line 1170284
    new-array p3, v0, [Lorg/apache/http/Header;

    .line 1170285
    :cond_1
    new-instance v1, LX/75z;

    sget-object v2, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-direct {v1, v2, p1, p2}, LX/75z;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    iput-object v1, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    .line 1170286
    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_2

    .line 1170287
    iget-object v1, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    aget-object v2, p3, v0

    invoke-interface {v1, v2}, Lorg/apache/http/HttpResponse;->addHeader(Lorg/apache/http/Header;)V

    .line 1170288
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1170289
    :cond_2
    iget-object v0, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    .line 1170290
    const-string v4, "Content-Length"

    invoke-interface {v0, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v6

    .line 1170291
    const-wide/16 v4, -0x1

    .line 1170292
    if-eqz v6, :cond_3

    .line 1170293
    :try_start_0
    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 1170294
    :cond_3
    :goto_1
    move-wide v0, v4

    .line 1170295
    new-instance v2, LX/762;

    iget-object v3, p0, LX/75y;->mBuffer:LX/161;

    invoke-direct {v2, v3}, LX/762;-><init>(LX/161;)V

    iput-object v2, p0, LX/75y;->mBufferInputStream:LX/762;

    .line 1170296
    new-instance v2, Lorg/apache/http/entity/InputStreamEntity;

    iget-object v3, p0, LX/75y;->mBufferInputStream:LX/762;

    invoke-direct {v2, v3, v0, v1}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 1170297
    iget-object v0, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    const-string v1, "Content-Encoding"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 1170298
    if-eqz v0, :cond_4

    .line 1170299
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/apache/http/entity/InputStreamEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 1170300
    :cond_4
    iget-object v0, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    const-string v1, "Content-Type"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 1170301
    if-eqz v0, :cond_5

    .line 1170302
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 1170303
    :cond_5
    iget-object v0, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1170304
    sget-object v0, LX/75x;->HEADERS_ARRIVED:LX/75x;

    iput-object v0, p0, LX/75y;->mReadState:LX/75x;

    .line 1170305
    return-void

    :catch_0
    goto :goto_1
.end method

.method private logException(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1170269
    iget-object v0, p0, LX/75y;->mErrorReporter:LX/03V;

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1170270
    return-void
.end method

.method public static varargs verifyState(LX/75y;[LX/75x;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1170271
    iget-object v0, p0, LX/75y;->mReadState:LX/75x;

    sget-object v3, LX/75x;->ERROR:LX/75x;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1170272
    array-length v5, p1

    move v3, v2

    move v4, v2

    :goto_1
    if-ge v3, v5, :cond_2

    aget-object v0, p1, v3

    .line 1170273
    iget-object v6, p0, LX/75y;->mReadState:LX/75x;

    if-ne v6, v0, :cond_1

    move v0, v1

    :goto_2
    or-int/2addr v4, v0

    .line 1170274
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1170275
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1170276
    goto :goto_2

    .line 1170277
    :cond_2
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1170278
    return-void
.end method

.method public static waitForHeaders(LX/75y;)V
    .locals 5

    .prologue
    .line 1170256
    iget-object v1, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    monitor-enter v1

    .line 1170257
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/75y;->mReadState:LX/75x;

    sget-object v2, LX/75x;->HEADERS_ARRIVED:LX/75x;

    invoke-virtual {v0, v2}, LX/75x;->compareTo(Ljava/lang/Enum;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gez v0, :cond_0

    .line 1170258
    :try_start_1
    iget-object v0, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    const-wide/32 v2, 0xea60

    const v4, 0x3b34e2d6

    invoke-static {v0, v2, v3, v4}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1170259
    :catch_0
    goto :goto_0

    .line 1170260
    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1170261
    iget-object v0, p0, LX/75y;->mNetworkException:LX/5oc;

    if-eqz v0, :cond_1

    .line 1170262
    iget-object v0, p0, LX/75y;->mNetworkException:LX/5oc;

    throw v0

    .line 1170263
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1170264
    :cond_1
    iget-object v0, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1170265
    :cond_2
    iget-object v0, p0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "null response received at: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/75y;->mReadState:LX/75x;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1170266
    :goto_1
    new-instance v1, LX/4bK;

    invoke-direct {v1, v0}, LX/4bK;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1170267
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "null response status line received: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/75y;->mReadState:LX/75x;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1170268
    :cond_4
    return-void
.end method


# virtual methods
.method public onBody()V
    .locals 3

    .prologue
    .line 1170217
    :try_start_0
    iget-object v0, p0, LX/75y;->mBufferInputStream:LX/762;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1170218
    const/4 v0, 0x2

    new-array v0, v0, [LX/75x;

    const/4 v1, 0x0

    sget-object v2, LX/75x;->HEADERS_ARRIVED:LX/75x;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/75x;->BODY_ARRIVED:LX/75x;

    aput-object v2, v0, v1

    invoke-static {p0, v0}, LX/75y;->verifyState(LX/75y;[LX/75x;)V

    .line 1170219
    iget-object v0, p0, LX/75y;->mBufferInputStream:LX/762;

    invoke-virtual {v0}, LX/762;->onBody()V

    .line 1170220
    sget-object v0, LX/75x;->BODY_ARRIVED:LX/75x;

    iput-object v0, p0, LX/75y;->mReadState:LX/75x;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1170221
    :goto_0
    return-void

    .line 1170222
    :catch_0
    move-exception v0

    .line 1170223
    const-string v1, "error_on_body"

    invoke-direct {p0, v1, v0}, LX/75y;->logException(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onEOM()V
    .locals 6

    .prologue
    .line 1170242
    :try_start_0
    iget-object v2, p0, LX/75y;->mHttpFlowStatistics:LX/1iW;

    if-eqz v2, :cond_0

    .line 1170243
    iget-object v2, p0, LX/75y;->mHttpFlowStatistics:LX/1iW;

    const-string v3, "done"

    .line 1170244
    iput-object v3, v2, LX/1iW;->i:Ljava/lang/String;

    .line 1170245
    :cond_0
    iget-object v2, p0, LX/75y;->mBufferInputStream:LX/762;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1170246
    const/4 v2, 0x2

    new-array v2, v2, [LX/75x;

    const/4 v3, 0x0

    sget-object v4, LX/75x;->HEADERS_ARRIVED:LX/75x;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LX/75x;->BODY_ARRIVED:LX/75x;

    aput-object v4, v2, v3

    invoke-static {p0, v2}, LX/75y;->verifyState(LX/75y;[LX/75x;)V

    .line 1170247
    sget-object v2, LX/75x;->RESPONSE_COMPLETED:LX/75x;

    iput-object v2, p0, LX/75y;->mReadState:LX/75x;

    .line 1170248
    iget-object v2, p0, LX/75y;->mBufferInputStream:LX/762;

    invoke-virtual {v2}, LX/762;->onEOM()V

    .line 1170249
    iget-object v2, p0, LX/75y;->mRequestStatsObserver:LX/4ib;

    invoke-virtual {v2}, LX/4ib;->getRequestStats()LX/4iZ;

    move-result-object v2

    .line 1170250
    iget-wide v4, p0, LX/75y;->mLoomLogId:J

    invoke-static {v4, v5}, Lcom/facebook/loom/logger/api/LoomLogger;->d(J)I

    move-result v3

    iput v3, p0, LX/75y;->mLoomMatchId:I

    .line 1170251
    if-eqz v2, :cond_1

    .line 1170252
    iget-object v3, p0, LX/75y;->mEventHandler:LX/6Xa;

    iget-wide v4, p0, LX/75y;->mLoomLogId:J

    invoke-interface {v3, v2, v4, v5}, LX/6Xa;->decorateStatistics(LX/4iZ;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1170253
    :cond_1
    :goto_0
    return-void

    .line 1170254
    :catch_0
    move-exception v0

    .line 1170255
    const-string v1, "error_on_eom"

    invoke-direct {p0, v1, v0}, LX/75y;->logException(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onError(Lcom/facebook/proxygen/HTTPRequestError;)V
    .locals 4
    .param p1    # Lcom/facebook/proxygen/HTTPRequestError;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1170233
    iget-object v1, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    monitor-enter v1

    .line 1170234
    :try_start_0
    invoke-direct {p0, p1}, LX/75y;->handleError(Lcom/facebook/proxygen/HTTPRequestError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1170235
    :try_start_1
    iget-object v0, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    const v2, -0x64e59250

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1170236
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 1170237
    :catch_0
    move-exception v0

    .line 1170238
    :try_start_2
    const-string v2, "error_on_error"

    invoke-direct {p0, v2, v0}, LX/75y;->logException(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1170239
    :try_start_3
    iget-object v0, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    const v2, 0x1f0ebb4f

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    goto :goto_0

    .line 1170240
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1170241
    :catchall_1
    move-exception v0

    iget-object v2, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    const v3, -0x160e09b2

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    throw v0
.end method

.method public onResponse(ILjava/lang/String;[Lorg/apache/http/Header;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [Lorg/apache/http/Header;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1170224
    iget-object v1, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    monitor-enter v1

    .line 1170225
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, LX/75y;->handleOnResponse(ILjava/lang/String;[Lorg/apache/http/Header;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1170226
    :try_start_1
    iget-object v0, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    const v2, -0x8f74b

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1170227
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 1170228
    :catch_0
    move-exception v0

    .line 1170229
    :try_start_2
    const-string v2, "error_on_response"

    invoke-direct {p0, v2, v0}, LX/75y;->logException(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1170230
    :try_start_3
    iget-object v0, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    const v2, 0x50756fa7

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    goto :goto_0

    .line 1170231
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1170232
    :catchall_1
    move-exception v0

    iget-object v2, p0, LX/75y;->mHeadersArrivedCondition:Ljava/lang/Object;

    const v3, -0x76f74407

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    throw v0
.end method
