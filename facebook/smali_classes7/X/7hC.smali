.class public LX/7hC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/7hE;",
        "Lcom/facebook/audience/upload/protocol/ShotCreateResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7hC;


# instance fields
.field private final a:LX/0lp;

.field public final b:LX/0W9;

.field private final c:LX/0gK;


# direct methods
.method public constructor <init>(LX/0lp;LX/0W9;LX/0gK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1225607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225608
    iput-object p1, p0, LX/7hC;->a:LX/0lp;

    .line 1225609
    iput-object p2, p0, LX/7hC;->b:LX/0W9;

    .line 1225610
    iput-object p3, p0, LX/7hC;->c:LX/0gK;

    .line 1225611
    return-void
.end method

.method public static a(LX/0QB;)LX/7hC;
    .locals 6

    .prologue
    .line 1225612
    sget-object v0, LX/7hC;->d:LX/7hC;

    if-nez v0, :cond_1

    .line 1225613
    const-class v1, LX/7hC;

    monitor-enter v1

    .line 1225614
    :try_start_0
    sget-object v0, LX/7hC;->d:LX/7hC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1225615
    if-eqz v2, :cond_0

    .line 1225616
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1225617
    new-instance p0, LX/7hC;

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v3

    check-cast v3, LX/0lp;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-static {v0}, LX/0gK;->b(LX/0QB;)LX/0gK;

    move-result-object v5

    check-cast v5, LX/0gK;

    invoke-direct {p0, v3, v4, v5}, LX/7hC;-><init>(LX/0lp;LX/0W9;LX/0gK;)V

    .line 1225618
    move-object v0, p0

    .line 1225619
    sput-object v0, LX/7hC;->d:LX/7hC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1225620
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1225621
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1225622
    :cond_1
    sget-object v0, LX/7hC;->d:LX/7hC;

    return-object v0

    .line 1225623
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1225624
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(LX/7hE;)Ljava/lang/String;
    .locals 13

    .prologue
    .line 1225625
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1225626
    iget-object v1, p0, LX/7hC;->a:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v1

    .line 1225627
    invoke-virtual {v1}, LX/0nX;->f()V

    .line 1225628
    const-string v2, "input"

    invoke-virtual {v1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225629
    iget-object v2, p0, LX/7hC;->c:LX/0gK;

    invoke-virtual {v2}, LX/0gK;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1225630
    const/4 v3, 0x0

    .line 1225631
    invoke-virtual {v1}, LX/0nX;->f()V

    .line 1225632
    const-string v2, "client_mutation_id"

    invoke-virtual {v1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225633
    iget-object v2, p1, LX/7hE;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1225634
    invoke-virtual {v1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225635
    const-string v2, "actor_id"

    invoke-virtual {v1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225636
    iget-object v2, p1, LX/7hE;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1225637
    invoke-virtual {v1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225638
    const-string v2, "unpublished_media"

    invoke-virtual {v1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225639
    iget-object v2, p1, LX/7hE;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1225640
    invoke-virtual {v1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225641
    const-string v2, "media_type"

    invoke-virtual {v1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225642
    iget-object v2, p1, LX/7hE;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1225643
    invoke-virtual {v1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225644
    const-string v2, "backstage_post_type"

    invoke-virtual {v1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225645
    iget-object v2, p1, LX/7hE;->k:Ljava/lang/String;

    move-object v2, v2

    .line 1225646
    invoke-virtual {v1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225647
    iget-object v2, p1, LX/7hE;->n:LX/0Px;

    move-object v2, v2

    .line 1225648
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1225649
    const-string v2, "direct_spaces"

    invoke-virtual {v1, v2}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1225650
    iget-object v2, p1, LX/7hE;->n:LX/0Px;

    move-object v5, v2

    .line 1225651
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p0

    move v4, v3

    :goto_0
    if-ge v4, p0, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7hG;

    .line 1225652
    invoke-virtual {v2}, LX/7hG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225653
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 1225654
    :cond_0
    invoke-virtual {v1}, LX/0nX;->e()V

    .line 1225655
    :cond_1
    iget-object v2, p1, LX/7hE;->l:LX/0Px;

    move-object v4, v2

    .line 1225656
    if-eqz v4, :cond_3

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1225657
    const-string v2, "specific_users"

    invoke-virtual {v1, v2}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1225658
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 1225659
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225660
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1225661
    :cond_2
    invoke-virtual {v1}, LX/0nX;->e()V

    .line 1225662
    const-string v2, "is_private"

    .line 1225663
    iget-boolean v3, p1, LX/7hE;->m:Z

    move v3, v3

    .line 1225664
    invoke-virtual {v1, v2, v3}, LX/0nX;->a(Ljava/lang/String;Z)V

    .line 1225665
    :cond_3
    invoke-virtual {v1}, LX/0nX;->g()V

    .line 1225666
    :goto_2
    invoke-virtual {v1}, LX/0nX;->g()V

    .line 1225667
    invoke-virtual {v1}, LX/0nX;->flush()V

    .line 1225668
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1225669
    :cond_4
    invoke-virtual {v1}, LX/0nX;->f()V

    .line 1225670
    const-string v3, "client_mutation_id"

    invoke-virtual {v1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225671
    iget-object v3, p1, LX/7hE;->e:Ljava/lang/String;

    move-object v3, v3

    .line 1225672
    invoke-virtual {v1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225673
    const-string v3, "actor_id"

    invoke-virtual {v1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225674
    iget-object v3, p1, LX/7hE;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1225675
    invoke-virtual {v1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225676
    const-string v3, "posts_info"

    invoke-virtual {v1, v3}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1225677
    const/4 v6, 0x0

    .line 1225678
    invoke-virtual {v1}, LX/0nX;->f()V

    .line 1225679
    const-string v4, "unpublished_media"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225680
    iget-object v4, p1, LX/7hE;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1225681
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225682
    iget-object v4, p1, LX/7hE;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1225683
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1225684
    const-string v4, "reaction_to"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225685
    iget-object v4, p1, LX/7hE;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1225686
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225687
    :cond_5
    iget-object v4, p1, LX/7hE;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1225688
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1225689
    const-string v4, "thread_id"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225690
    iget-object v4, p1, LX/7hE;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1225691
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225692
    :cond_6
    const-string v4, "media_type"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225693
    iget-object v4, p1, LX/7hE;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1225694
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225695
    const-string v4, "caption"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225696
    iget-object v4, p1, LX/7hE;->g:Ljava/lang/String;

    move-object v4, v4

    .line 1225697
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225698
    const-string v4, "message"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225699
    iget-object v4, p1, LX/7hE;->h:Ljava/lang/String;

    move-object v4, v4

    .line 1225700
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225701
    const-string v4, "exif_time"

    .line 1225702
    iget v5, p1, LX/7hE;->i:I

    move v5, v5

    .line 1225703
    invoke-virtual {v1, v4, v5}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 1225704
    const-string v4, "timezone_offset_seconds"

    iget-object v5, p0, LX/7hC;->b:LX/0W9;

    .line 1225705
    sget v9, LX/7hL;->a:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_7

    .line 1225706
    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v9

    .line 1225707
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v9

    .line 1225708
    invoke-virtual {v9}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v9

    int-to-long v9, v9

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    long-to-int v9, v9

    sput v9, LX/7hL;->a:I

    .line 1225709
    :cond_7
    sget v9, LX/7hL;->a:I

    move v5, v9

    .line 1225710
    invoke-virtual {v1, v4, v5}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 1225711
    const-string v4, "location"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225712
    iget-object v4, p1, LX/7hE;->j:Ljava/lang/String;

    move-object v4, v4

    .line 1225713
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225714
    const-string v4, "backstage_post_type"

    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1225715
    iget-object v4, p1, LX/7hE;->k:Ljava/lang/String;

    move-object v4, v4

    .line 1225716
    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225717
    iget-object v4, p1, LX/7hE;->l:LX/0Px;

    move-object v7, v4

    .line 1225718
    if-eqz v7, :cond_9

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-lez v4, :cond_9

    .line 1225719
    const-string v4, "specific_users"

    invoke-virtual {v1, v4}, LX/0nX;->f(Ljava/lang/String;)V

    move v5, v6

    .line 1225720
    :goto_3
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_8

    .line 1225721
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225722
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 1225723
    :cond_8
    invoke-virtual {v1}, LX/0nX;->e()V

    .line 1225724
    const-string v4, "private"

    .line 1225725
    iget-boolean v5, p1, LX/7hE;->m:Z

    move v5, v5

    .line 1225726
    invoke-virtual {v1, v4, v5}, LX/0nX;->a(Ljava/lang/String;Z)V

    .line 1225727
    :cond_9
    iget-object v4, p1, LX/7hE;->n:LX/0Px;

    move-object v4, v4

    .line 1225728
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    .line 1225729
    const-string v4, "direct_spaces"

    invoke-virtual {v1, v4}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1225730
    iget-object v4, p1, LX/7hE;->n:LX/0Px;

    move-object v7, v4

    .line 1225731
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v6

    :goto_4
    if-ge v5, v8, :cond_a

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7hG;

    .line 1225732
    invoke-virtual {v4}, LX/7hG;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1225733
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_4

    .line 1225734
    :cond_a
    invoke-virtual {v1}, LX/0nX;->e()V

    .line 1225735
    :cond_b
    iget-object v4, p1, LX/7hE;->o:LX/0Px;

    move-object v5, v4

    .line 1225736
    if-eqz v5, :cond_d

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v4

    if-lez v4, :cond_d

    .line 1225737
    const-string v4, "inspiration_prompts"

    invoke-virtual {v1, v4}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1225738
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    :goto_5
    if-ge v6, v7, :cond_c

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    .line 1225739
    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 1225740
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 1225741
    :cond_c
    invoke-virtual {v1}, LX/0nX;->e()V

    .line 1225742
    :cond_d
    invoke-virtual {v1}, LX/0nX;->g()V

    .line 1225743
    invoke-virtual {v1}, LX/0nX;->e()V

    .line 1225744
    invoke-virtual {v1}, LX/0nX;->g()V

    .line 1225745
    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1225746
    check-cast p1, LX/7hE;

    .line 1225747
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1225748
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "q"

    iget-object v0, p0, LX/7hC;->c:LX/0gK;

    invoke-virtual {v0}, LX/0gK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Mutation DirectMessageCreateMutation : DirectMessageCreateResponsePayload { direct_message_create(<input>) { threads { root_message { id } } } }"

    :goto_0
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1225749
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "method"

    const-string v3, "POST"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1225750
    invoke-direct {p0, p1}, LX/7hC;->b(LX/7hE;)Ljava/lang/String;

    move-result-object v0

    .line 1225751
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "query_params"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1225752
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "query_name"

    const-string v3, "backstage-graphql"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1225753
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "backstage-graphql"

    .line 1225754
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1225755
    move-object v0, v0

    .line 1225756
    const-string v2, "POST"

    .line 1225757
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1225758
    move-object v0, v0

    .line 1225759
    const-string v2, "graphql"

    .line 1225760
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1225761
    move-object v0, v0

    .line 1225762
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1225763
    move-object v0, v0

    .line 1225764
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1225765
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1225766
    move-object v0, v0

    .line 1225767
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1225768
    :cond_0
    const-string v0, "Mutation FBBackstagePostsCreateCoreMutation : BackstagePostsCreateResponsePayload { backstage_posts_create(<input>) { posts { id } } }"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1225769
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1225770
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1225771
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1225772
    iget-object v2, p0, LX/7hC;->c:LX/0gK;

    invoke-virtual {v2}, LX/0gK;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1225773
    const-string v2, "direct_message_create"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v2, "threads"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1225774
    const-string v3, "root_message"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v3, "id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1225775
    :cond_0
    const-string v2, "backstage_posts_create"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v2, "posts"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1225776
    const-string v3, "id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1225777
    :cond_1
    new-instance v0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/audience/upload/protocol/ShotCreateResult;-><init>(LX/0Px;)V

    return-object v0
.end method
