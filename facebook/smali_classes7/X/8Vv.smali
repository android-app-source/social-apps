.class public LX/8Vv;
.super LX/1OM;
.source ""

# interfaces
.implements LX/8VY;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/8VY;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field public final c:LX/8TS;


# direct methods
.method public constructor <init>(LX/8TS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1353503
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1353504
    iput-object p1, p0, LX/8Vv;->c:LX/8TS;

    .line 1353505
    return-void
.end method

.method public static a(LX/0QB;)LX/8Vv;
    .locals 4

    .prologue
    .line 1353506
    const-class v1, LX/8Vv;

    monitor-enter v1

    .line 1353507
    :try_start_0
    sget-object v0, LX/8Vv;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1353508
    sput-object v2, LX/8Vv;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1353509
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1353510
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1353511
    new-instance p0, LX/8Vv;

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v3

    check-cast v3, LX/8TS;

    invoke-direct {p0, v3}, LX/8Vv;-><init>(LX/8TS;)V

    .line 1353512
    move-object v0, p0

    .line 1353513
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1353514
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Vv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1353515
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1353516
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1353517
    packed-switch p2, :pswitch_data_0

    .line 1353518
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353519
    new-instance v0, LX/8W2;

    invoke-direct {v0, v1}, LX/8W2;-><init>(Landroid/view/View;)V

    :goto_0
    return-object v0

    .line 1353520
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353521
    new-instance v0, LX/8W0;

    invoke-direct {v0, v1}, LX/8W0;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1353522
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077d

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353523
    new-instance v0, LX/8WC;

    invoke-direct {v0, v1}, LX/8WC;-><init>(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1353524
    move-object v0, p1

    check-cast v0, LX/8Vr;

    .line 1353525
    packed-switch p2, :pswitch_data_0

    .line 1353526
    iget-object v1, p0, LX/8Vv;->a:LX/0Px;

    add-int/lit8 v3, p2, -0x1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Vb;

    :goto_0
    move-object v3, v1

    .line 1353527
    const/4 v5, 0x0

    move v1, p2

    move v4, v2

    invoke-interface/range {v0 .. v5}, LX/8Vr;->a(IILX/8Vb;ZLX/8Vp;)V

    .line 1353528
    return-void

    .line 1353529
    :pswitch_0
    iget-object v1, p0, LX/8Vv;->a:LX/0Px;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Vb;

    goto :goto_0

    .line 1353530
    :pswitch_1
    const/4 v1, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Z)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setData"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1353531
    iget-object v0, p0, LX/8Vv;->c:LX/8TS;

    invoke-virtual {v0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    iget-object v0, v0, LX/8Vb;->a:Ljava/lang/String;

    .line 1353532
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result p3

    if-nez p3, :cond_0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_2

    .line 1353533
    :cond_0
    :goto_0
    iput-object p2, p0, LX/8Vv;->b:Ljava/lang/String;

    .line 1353534
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, LX/8Vv;->a:LX/0Px;

    .line 1353535
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1353536
    return-void

    .line 1353537
    :cond_1
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1353538
    :cond_2
    new-instance p3, LX/8Vu;

    invoke-direct {p3, p0, v0}, LX/8Vu;-><init>(LX/8Vv;Ljava/lang/String;)V

    invoke-static {p1, p3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1353539
    iget-object v0, p0, LX/8Vv;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 1353540
    packed-switch p1, :pswitch_data_0

    .line 1353541
    iget-object v2, p0, LX/8Vv;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1353542
    :goto_0
    :pswitch_0
    return v0

    .line 1353543
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1353544
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1353545
    iget-object v0, p0, LX/8Vv;->a:LX/0Px;

    if-nez v0, :cond_0

    .line 1353546
    const/4 v0, 0x0

    .line 1353547
    :goto_0
    return v0

    .line 1353548
    :cond_0
    iget-object v0, p0, LX/8Vv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 1353549
    iget-object v0, p0, LX/8Vv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1353550
    :cond_1
    iget-object v0, p0, LX/8Vv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
