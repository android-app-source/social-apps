.class public final LX/8a8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 46

    .prologue
    .line 1366519
    const/16 v42, 0x0

    .line 1366520
    const/16 v41, 0x0

    .line 1366521
    const/16 v40, 0x0

    .line 1366522
    const/16 v39, 0x0

    .line 1366523
    const/16 v38, 0x0

    .line 1366524
    const/16 v37, 0x0

    .line 1366525
    const/16 v36, 0x0

    .line 1366526
    const/16 v35, 0x0

    .line 1366527
    const/16 v34, 0x0

    .line 1366528
    const/16 v33, 0x0

    .line 1366529
    const/16 v32, 0x0

    .line 1366530
    const/16 v31, 0x0

    .line 1366531
    const/16 v30, 0x0

    .line 1366532
    const/16 v29, 0x0

    .line 1366533
    const/16 v28, 0x0

    .line 1366534
    const/16 v27, 0x0

    .line 1366535
    const/16 v26, 0x0

    .line 1366536
    const/16 v25, 0x0

    .line 1366537
    const/16 v24, 0x0

    .line 1366538
    const/16 v23, 0x0

    .line 1366539
    const/16 v22, 0x0

    .line 1366540
    const/16 v21, 0x0

    .line 1366541
    const/16 v20, 0x0

    .line 1366542
    const/16 v19, 0x0

    .line 1366543
    const/16 v18, 0x0

    .line 1366544
    const/16 v17, 0x0

    .line 1366545
    const/16 v16, 0x0

    .line 1366546
    const/4 v15, 0x0

    .line 1366547
    const/4 v14, 0x0

    .line 1366548
    const/4 v13, 0x0

    .line 1366549
    const/4 v12, 0x0

    .line 1366550
    const/4 v11, 0x0

    .line 1366551
    const/4 v10, 0x0

    .line 1366552
    const/4 v9, 0x0

    .line 1366553
    const/4 v8, 0x0

    .line 1366554
    const/4 v7, 0x0

    .line 1366555
    const/4 v6, 0x0

    .line 1366556
    const/4 v5, 0x0

    .line 1366557
    const/4 v4, 0x0

    .line 1366558
    const/4 v3, 0x0

    .line 1366559
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1

    .line 1366560
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1366561
    const/4 v3, 0x0

    .line 1366562
    :goto_0
    return v3

    .line 1366563
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1366564
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_27

    .line 1366565
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v43

    .line 1366566
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1366567
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v44

    sget-object v45, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    if-eq v0, v1, :cond_1

    if-eqz v43, :cond_1

    .line 1366568
    const-string v44, "__type__"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_2

    const-string v44, "__typename"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_3

    .line 1366569
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v42

    goto :goto_1

    .line 1366570
    :cond_3
    const-string v44, "attribution_annotation"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_4

    .line 1366571
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v41

    goto :goto_1

    .line 1366572
    :cond_4
    const-string v44, "audio_play_mode"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_5

    .line 1366573
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v40

    goto :goto_1

    .line 1366574
    :cond_5
    const-string v44, "audio_title"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_6

    .line 1366575
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v39

    goto :goto_1

    .line 1366576
    :cond_6
    const-string v44, "audio_url"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_7

    .line 1366577
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    goto/16 :goto_1

    .line 1366578
    :cond_7
    const-string v44, "base_url"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_8

    .line 1366579
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    goto/16 :goto_1

    .line 1366580
    :cond_8
    const-string v44, "block_title"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_9

    .line 1366581
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    goto/16 :goto_1

    .line 1366582
    :cond_9
    const-string v44, "copyright_annotation"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_a

    .line 1366583
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v35

    goto/16 :goto_1

    .line 1366584
    :cond_a
    const-string v44, "display_height"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_b

    .line 1366585
    const/4 v5, 0x1

    .line 1366586
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v34

    goto/16 :goto_1

    .line 1366587
    :cond_b
    const-string v44, "display_width"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_c

    .line 1366588
    const/4 v4, 0x1

    .line 1366589
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v33

    goto/16 :goto_1

    .line 1366590
    :cond_c
    const-string v44, "document_element_type"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_d

    .line 1366591
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v32

    goto/16 :goto_1

    .line 1366592
    :cond_d
    const-string v44, "element_text"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_e

    .line 1366593
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 1366594
    :cond_e
    const-string v44, "element_video"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_f

    .line 1366595
    invoke-static/range {p0 .. p1}, LX/8ZV;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 1366596
    :cond_f
    const-string v44, "enable_ad_network_bridging"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_10

    .line 1366597
    const/4 v3, 0x1

    .line 1366598
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 1366599
    :cond_10
    const-string v44, "feedback"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_11

    .line 1366600
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 1366601
    :cond_11
    const-string v44, "feedback_options"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_12

    .line 1366602
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    goto/16 :goto_1

    .line 1366603
    :cond_12
    const-string v44, "html_source"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_13

    .line 1366604
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto/16 :goto_1

    .line 1366605
    :cond_13
    const-string v44, "id"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_14

    .line 1366606
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 1366607
    :cond_14
    const-string v44, "l"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_15

    .line 1366608
    invoke-static/range {p0 .. p1}, LX/8aO;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1366609
    :cond_15
    const-string v44, "list_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_16

    .line 1366610
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v23

    goto/16 :goto_1

    .line 1366611
    :cond_16
    const-string v44, "location_annotation"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_17

    .line 1366612
    invoke-static/range {p0 .. p1}, LX/8aB;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1366613
    :cond_17
    const-string v44, "map_locations"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_18

    .line 1366614
    invoke-static/range {p0 .. p1}, LX/8aB;->b(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1366615
    :cond_18
    const-string v44, "map_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_19

    .line 1366616
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    goto/16 :goto_1

    .line 1366617
    :cond_19
    const-string v44, "margin_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1a

    .line 1366618
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    goto/16 :goto_1

    .line 1366619
    :cond_1a
    const-string v44, "photo"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1b

    .line 1366620
    invoke-static/range {p0 .. p1}, LX/8ZQ;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1366621
    :cond_1b
    const-string v44, "poster_image"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1c

    .line 1366622
    invoke-static/range {p0 .. p1}, LX/8ZQ;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1366623
    :cond_1c
    const-string v44, "presentation_state"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1d

    .line 1366624
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto/16 :goto_1

    .line 1366625
    :cond_1d
    const-string v44, "related_article_objs"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1e

    .line 1366626
    invoke-static/range {p0 .. p1}, LX/8aT;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1366627
    :cond_1e
    const-string v44, "slideEdges"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1f

    .line 1366628
    invoke-static/range {p0 .. p1}, LX/8aV;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1366629
    :cond_1f
    const-string v44, "subtitle_annotation"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_20

    .line 1366630
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1366631
    :cond_20
    const-string v44, "text_background_color"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_21

    .line 1366632
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1366633
    :cond_21
    const-string v44, "title_annotation"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_22

    .line 1366634
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1366635
    :cond_22
    const-string v44, "video_autoplay_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_23

    .line 1366636
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 1366637
    :cond_23
    const-string v44, "video_control_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_24

    .line 1366638
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 1366639
    :cond_24
    const-string v44, "video_looping_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_25

    .line 1366640
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto/16 :goto_1

    .line 1366641
    :cond_25
    const-string v44, "visual_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_26

    .line 1366642
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto/16 :goto_1

    .line 1366643
    :cond_26
    const-string v44, "webview_presentation_style"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_0

    .line 1366644
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto/16 :goto_1

    .line 1366645
    :cond_27
    const/16 v43, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1366646
    const/16 v43, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366647
    const/16 v42, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v42

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366648
    const/16 v41, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366649
    const/16 v40, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366650
    const/16 v39, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v39

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366651
    const/16 v38, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v38

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366652
    const/16 v37, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366653
    const/16 v36, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1366654
    if-eqz v5, :cond_28

    .line 1366655
    const/16 v5, 0x8

    const/16 v35, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v5, v1, v2}, LX/186;->a(III)V

    .line 1366656
    :cond_28
    if-eqz v4, :cond_29

    .line 1366657
    const/16 v4, 0x9

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 1366658
    :cond_29
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1366659
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1366660
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1366661
    if-eqz v3, :cond_2a

    .line 1366662
    const/16 v3, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1366663
    :cond_2a
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366664
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366665
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366666
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366667
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366668
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366669
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366670
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366671
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366672
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366673
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366674
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366675
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1366676
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1366677
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1366678
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1366679
    const/16 v3, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1366680
    const/16 v3, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1366681
    const/16 v3, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1366682
    const/16 v3, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1366683
    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1366684
    const/16 v3, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1366685
    const/16 v3, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1366686
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x13

    const/16 v5, 0xf

    const/16 v4, 0xa

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1366687
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1366688
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1366689
    if-eqz v0, :cond_0

    .line 1366690
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366691
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1366692
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366693
    if-eqz v0, :cond_1

    .line 1366694
    const-string v1, "attribution_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366695
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366696
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1366697
    if-eqz v0, :cond_2

    .line 1366698
    const-string v0, "audio_play_mode"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366699
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366700
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366701
    if-eqz v0, :cond_3

    .line 1366702
    const-string v1, "audio_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366703
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366704
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366705
    if-eqz v0, :cond_4

    .line 1366706
    const-string v1, "audio_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366707
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366708
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366709
    if-eqz v0, :cond_5

    .line 1366710
    const-string v1, "base_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366711
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366712
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366713
    if-eqz v0, :cond_6

    .line 1366714
    const-string v1, "block_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366715
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366716
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366717
    if-eqz v0, :cond_7

    .line 1366718
    const-string v1, "copyright_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366719
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366720
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1366721
    if-eqz v0, :cond_8

    .line 1366722
    const-string v1, "display_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366723
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1366724
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1366725
    if-eqz v0, :cond_9

    .line 1366726
    const-string v1, "display_width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366727
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1366728
    :cond_9
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1366729
    if-eqz v0, :cond_a

    .line 1366730
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366731
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366732
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366733
    if-eqz v0, :cond_b

    .line 1366734
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366735
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366736
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366737
    if-eqz v0, :cond_c

    .line 1366738
    const-string v1, "element_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366739
    invoke-static {p0, v0, p2, p3}, LX/8ZV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366740
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1366741
    if-eqz v0, :cond_d

    .line 1366742
    const-string v1, "enable_ad_network_bridging"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366743
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1366744
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366745
    if-eqz v0, :cond_e

    .line 1366746
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366747
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366748
    :cond_e
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1366749
    if-eqz v0, :cond_f

    .line 1366750
    const-string v0, "feedback_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366751
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366752
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366753
    if-eqz v0, :cond_10

    .line 1366754
    const-string v1, "html_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366755
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366756
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366757
    if-eqz v0, :cond_11

    .line 1366758
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366759
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366760
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366761
    if-eqz v0, :cond_12

    .line 1366762
    const-string v1, "l"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366763
    invoke-static {p0, v0, p2, p3}, LX/8aO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366764
    :cond_12
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1366765
    if-eqz v0, :cond_13

    .line 1366766
    const-string v0, "list_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366767
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366768
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366769
    if-eqz v0, :cond_14

    .line 1366770
    const-string v1, "location_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366771
    invoke-static {p0, v0, p2, p3}, LX/8aB;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366772
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366773
    if-eqz v0, :cond_15

    .line 1366774
    const-string v1, "map_locations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366775
    invoke-static {p0, v0, p2, p3}, LX/8aB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366776
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366777
    if-eqz v0, :cond_16

    .line 1366778
    const-string v0, "map_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366779
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366780
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366781
    if-eqz v0, :cond_17

    .line 1366782
    const-string v0, "margin_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366783
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366784
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366785
    if-eqz v0, :cond_18

    .line 1366786
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366787
    invoke-static {p0, v0, p2, p3}, LX/8ZQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366788
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366789
    if-eqz v0, :cond_19

    .line 1366790
    const-string v1, "poster_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366791
    invoke-static {p0, v0, p2, p3}, LX/8ZQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366792
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366793
    if-eqz v0, :cond_1a

    .line 1366794
    const-string v0, "presentation_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366795
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366796
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366797
    if-eqz v0, :cond_1b

    .line 1366798
    const-string v1, "related_article_objs"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366799
    invoke-static {p0, v0, p2, p3}, LX/8aT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366800
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366801
    if-eqz v0, :cond_1c

    .line 1366802
    const-string v1, "slideEdges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366803
    invoke-static {p0, v0, p2, p3}, LX/8aV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366804
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366805
    if-eqz v0, :cond_1d

    .line 1366806
    const-string v1, "subtitle_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366807
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366808
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366809
    if-eqz v0, :cond_1e

    .line 1366810
    const-string v1, "text_background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366811
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366812
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366813
    if-eqz v0, :cond_1f

    .line 1366814
    const-string v1, "title_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366815
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366816
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366817
    if-eqz v0, :cond_20

    .line 1366818
    const-string v0, "video_autoplay_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366819
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366820
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366821
    if-eqz v0, :cond_21

    .line 1366822
    const-string v0, "video_control_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366823
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366824
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366825
    if-eqz v0, :cond_22

    .line 1366826
    const-string v0, "video_looping_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366827
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366828
    :cond_22
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366829
    if-eqz v0, :cond_23

    .line 1366830
    const-string v0, "visual_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366831
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366832
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366833
    if-eqz v0, :cond_24

    .line 1366834
    const-string v0, "webview_presentation_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366835
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366836
    :cond_24
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1366837
    return-void
.end method
