.class public LX/720;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w1",
        "<",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1Ck;

.field public final c:LX/03V;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/737;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/70k;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163880
    const-class v0, LX/720;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/720;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/03V;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/737;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163863
    iput-object p1, p0, LX/720;->b:LX/1Ck;

    .line 1163864
    iput-object p2, p0, LX/720;->c:LX/03V;

    .line 1163865
    iput-object p3, p0, LX/720;->d:LX/0Or;

    .line 1163866
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1163878
    iget-object v0, p0, LX/720;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1163879
    return-void
.end method

.method public final bridge synthetic a(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1163877
    check-cast p2, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;

    invoke-virtual {p0, p1, p2}, LX/720;->a(LX/6zj;Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;)V

    return-void
.end method

.method public final a(LX/6zj;Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;)V
    .locals 4

    .prologue
    .line 1163870
    iget-object v0, p0, LX/720;->e:LX/70k;

    invoke-virtual {v0}, LX/70k;->a()V

    .line 1163871
    new-instance v1, LX/71z;

    invoke-direct {v1, p0, p1, p2}, LX/71z;-><init>(LX/720;LX/6zj;Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;)V

    .line 1163872
    iget-object v0, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v0, v0

    .line 1163873
    check-cast v0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    iget-boolean v0, v0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/720;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/737;

    invoke-virtual {v0}, LX/737;->g()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1163874
    :goto_0
    iget-object v2, p0, LX/720;->b:LX/1Ck;

    const-string v3, "fetch_shipping_addresses"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1163875
    return-void

    .line 1163876
    :cond_0
    iget-object v0, p0, LX/720;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/737;

    invoke-virtual {v0}, LX/737;->h()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/70k;)V
    .locals 0

    .prologue
    .line 1163868
    iput-object p1, p0, LX/720;->e:LX/70k;

    .line 1163869
    return-void
.end method

.method public final bridge synthetic b(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1163867
    return-void
.end method
