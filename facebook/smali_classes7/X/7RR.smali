.class public LX/7RR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1206654
    const-class v0, LX/7RR;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7RR;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1206655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)Landroid/media/MediaCodec;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1206656
    const v1, 0xac44

    .line 1206657
    const v0, 0xfa00

    .line 1206658
    if-eqz p0, :cond_1

    .line 1206659
    iget v3, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->sampleRate:I

    .line 1206660
    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->channels:I

    .line 1206661
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->bitRate:I

    .line 1206662
    :goto_0
    new-instance v5, Landroid/media/MediaFormat;

    invoke-direct {v5}, Landroid/media/MediaFormat;-><init>()V

    .line 1206663
    const-string v6, "mime"

    const-string v7, "audio/mp4a-latm"

    invoke-virtual {v5, v6, v7}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206664
    const-string v6, "aac-profile"

    invoke-virtual {v5, v6, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1206665
    const-string v2, "sample-rate"

    invoke-virtual {v5, v2, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1206666
    const-string v2, "channel-count"

    invoke-virtual {v5, v2, v1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1206667
    const-string v1, "bitrate"

    invoke-virtual {v5, v1, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1206668
    const/4 v0, 0x0

    move v2, v0

    move-object v1, v4

    move-object v0, v4

    .line 1206669
    :goto_1
    const/4 v3, 0x3

    if-ge v2, v3, :cond_2

    .line 1206670
    :try_start_0
    const-string v1, "audio/mp4a-latm"

    invoke-static {v1}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1206671
    if-eqz v1, :cond_0

    .line 1206672
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    :try_start_1
    invoke-virtual {v1, v5, v3, v6, v7}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1206673
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1206674
    :cond_1
    sget-object v3, LX/7RR;->a:Ljava/lang/String;

    const-string v5, "AudioStreamingConfig is null. Using default values"

    invoke-static {v3, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v1

    move v1, v2

    goto :goto_0

    .line 1206675
    :catch_0
    move-exception v0

    .line 1206676
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v3, "MediaCodec creation failed"

    invoke-direct {v1, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1206677
    :catch_1
    move-exception v0

    move-object v1, v4

    .line 1206678
    goto :goto_2

    .line 1206679
    :catch_2
    move-exception v0

    .line 1206680
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v3, "MediaCodec audio encoder configure failed"

    invoke-direct {v1, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1206681
    :cond_2
    if-nez v1, :cond_4

    .line 1206682
    if-nez v0, :cond_3

    .line 1206683
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Audio encoder failed to create"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 1206684
    :cond_3
    throw v0

    .line 1206685
    :cond_4
    return-object v1
.end method
