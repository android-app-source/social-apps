.class public LX/75q;
.super LX/4hr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4hr",
        "<",
        "LX/75o;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/0aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1170033
    const-class v0, LX/75q;

    sput-object v0, LX/75q;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0aG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/75o;",
            ">;",
            "LX/0aG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1170055
    const/high16 v0, 0x10000

    invoke-direct {p0, p1, v0}, LX/4hr;-><init>(LX/0Or;I)V

    .line 1170056
    iput-object p2, p0, LX/75q;->c:LX/0aG;

    .line 1170057
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Message;LX/4ht;)V
    .locals 11

    .prologue
    .line 1170034
    check-cast p2, LX/75o;

    .line 1170035
    iget-object v0, p2, LX/4ht;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1170036
    new-instance v1, LX/75f;

    .line 1170037
    iget-object v2, p2, LX/4ht;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1170038
    invoke-direct {v1, v0, v2}, LX/75f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1170039
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, v1, LX/75f;->e:LX/0am;

    .line 1170040
    move-object v0, v1

    .line 1170041
    new-instance v4, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;

    iget-object v5, v0, LX/75f;->a:Ljava/lang/String;

    iget-object v6, v0, LX/75f;->b:Ljava/lang/String;

    iget-object v7, v0, LX/75f;->c:LX/0am;

    iget-object v8, v0, LX/75f;->d:LX/0am;

    iget-object v9, v0, LX/75f;->e:LX/0am;

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0am;LX/0am;LX/0am;B)V

    move-object v0, v4

    .line 1170042
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1170043
    const-string v2, "app_info"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1170044
    iget-object v0, p0, LX/75q;->c:LX/0aG;

    const-string v2, "platform_authorize_app"

    const v3, 0x6266cb38

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 1170045
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1170046
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 1170047
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 1170048
    iget v2, p1, Landroid/os/Message;->arg1:I

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 1170049
    iget v2, p1, Landroid/os/Message;->arg2:I

    iput v2, v1, Landroid/os/Message;->arg2:I

    .line 1170050
    const v2, 0x10001

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1170051
    move-object v1, v1

    .line 1170052
    iget-object v2, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 1170053
    new-instance v3, LX/75p;

    invoke-direct {v3, p0, v1, v2}, LX/75p;-><init>(LX/75q;Landroid/os/Message;Landroid/os/Messenger;)V

    invoke-static {v0, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1170054
    :cond_0
    return-void
.end method
