.class public LX/8V7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/8TS;


# direct methods
.method private constructor <init>(LX/0tX;LX/1Ck;LX/8TS;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1351803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1351804
    iput-object p1, p0, LX/8V7;->a:LX/0tX;

    .line 1351805
    iput-object p2, p0, LX/8V7;->b:LX/1Ck;

    .line 1351806
    iput-object p3, p0, LX/8V7;->c:LX/8TS;

    .line 1351807
    return-void
.end method

.method public static b(LX/0QB;)LX/8V7;
    .locals 4

    .prologue
    .line 1351808
    new-instance v3, LX/8V7;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v2

    check-cast v2, LX/8TS;

    invoke-direct {v3, v0, v1, v2}, LX/8V7;-><init>(LX/0tX;LX/1Ck;LX/8TS;)V

    .line 1351809
    return-object v3
.end method


# virtual methods
.method public final a()LX/0zO;
    .locals 7

    .prologue
    .line 1351810
    iget-object v0, p0, LX/8V7;->c:LX/8TS;

    .line 1351811
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1351812
    iget-object v1, v0, LX/8TO;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1351813
    new-instance v1, LX/8V2;

    invoke-direct {v1}, LX/8V2;-><init>()V

    move-object v1, v1

    .line 1351814
    new-instance v2, LX/4GN;

    invoke-direct {v2}, LX/4GN;-><init>()V

    .line 1351815
    new-instance v3, LX/4GO;

    invoke-direct {v3}, LX/4GO;-><init>()V

    .line 1351816
    iget-object v4, p0, LX/8V7;->c:LX/8TS;

    .line 1351817
    iget-object v5, v4, LX/8TS;->j:LX/8TP;

    move-object v4, v5

    .line 1351818
    iget-object v5, v4, LX/8TP;->b:Ljava/lang/String;

    move-object v4, v5

    .line 1351819
    sget-object v5, LX/8V6;->a:[I

    iget-object v6, p0, LX/8V7;->c:LX/8TS;

    .line 1351820
    iget-object p0, v6, LX/8TS;->j:LX/8TP;

    move-object v6, p0

    .line 1351821
    iget-object p0, v6, LX/8TP;->a:LX/8Ta;

    move-object v6, p0

    .line 1351822
    invoke-virtual {v6}, LX/8Ta;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1351823
    :goto_0
    const-string v4, "key"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1351824
    const-string v3, "app_id"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351825
    const-string v0, "input"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1351826
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0

    .line 1351827
    :pswitch_0
    const-string v5, "thread_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351828
    goto :goto_0

    .line 1351829
    :pswitch_1
    const-string v5, "group_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351830
    goto :goto_0

    .line 1351831
    :pswitch_2
    const-string v5, "story_token"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351832
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
