.class public final LX/7yP;
.super LX/0eW;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1279236
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method public static a(LX/0eX;IIZI)I
    .locals 2

    .prologue
    .line 1279247
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eX;->b(I)V

    .line 1279248
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p4, v1}, LX/0eX;->c(III)V

    .line 1279249
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, LX/0eX;->b(III)V

    .line 1279250
    const/4 v0, 0x0

    .line 1279251
    invoke-virtual {p0, v0, p1, v0}, LX/0eX;->b(III)V

    .line 1279252
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p3, v1}, LX/0eX;->a(IZZ)V

    .line 1279253
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v0

    .line 1279254
    move v0, v0

    .line 1279255
    return v0
.end method

.method public static a(Ljava/nio/ByteBuffer;)LX/7yP;
    .locals 3

    .prologue
    .line 1279242
    new-instance v0, LX/7yP;

    invoke-direct {v0}, LX/7yP;-><init>()V

    .line 1279243
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/2addr v1, v2

    .line 1279244
    iput v1, v0, LX/7yP;->a:I

    iput-object p0, v0, LX/7yP;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 1279245
    move-object v0, v1

    .line 1279246
    return-object v0
.end method


# virtual methods
.method public final c()LX/7yQ;
    .locals 3

    .prologue
    .line 1279237
    new-instance v0, LX/7yQ;

    invoke-direct {v0}, LX/7yQ;-><init>()V

    .line 1279238
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1279239
    iput v1, v0, LX/7yQ;->a:I

    iput-object v2, v0, LX/7yQ;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 1279240
    :goto_0
    move-object v0, v1

    .line 1279241
    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
