.class public final enum LX/6hi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6hi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6hi;

.field public static final enum CLIENT_ONLY:LX/6hi;

.field public static final enum MUST_UPDATE_SERVER:LX/6hi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1128553
    new-instance v0, LX/6hi;

    const-string v1, "MUST_UPDATE_SERVER"

    invoke-direct {v0, v1, v2}, LX/6hi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6hi;->MUST_UPDATE_SERVER:LX/6hi;

    .line 1128554
    new-instance v0, LX/6hi;

    const-string v1, "CLIENT_ONLY"

    invoke-direct {v0, v1, v3}, LX/6hi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6hi;->CLIENT_ONLY:LX/6hi;

    .line 1128555
    const/4 v0, 0x2

    new-array v0, v0, [LX/6hi;

    sget-object v1, LX/6hi;->MUST_UPDATE_SERVER:LX/6hi;

    aput-object v1, v0, v2

    sget-object v1, LX/6hi;->CLIENT_ONLY:LX/6hi;

    aput-object v1, v0, v3

    sput-object v0, LX/6hi;->$VALUES:[LX/6hi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1128552
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6hi;
    .locals 1

    .prologue
    .line 1128556
    const-class v0, LX/6hi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6hi;

    return-object v0
.end method

.method public static values()[LX/6hi;
    .locals 1

    .prologue
    .line 1128551
    sget-object v0, LX/6hi;->$VALUES:[LX/6hi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6hi;

    return-object v0
.end method
