.class public LX/6jh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final actorFbId:Ljava/lang/Long;

.field public final amendedMessageId:Ljava/lang/String;

.field public final mutation:LX/6kb;

.field public final threadKey:LX/6l9;

.field public final timestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0xa

    const/4 v4, 0x1

    .line 1132015
    new-instance v0, LX/1sv;

    const-string v1, "DeltaAmendMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jh;->b:LX/1sv;

    .line 1132016
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    invoke-direct {v0, v1, v6, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jh;->c:LX/1sw;

    .line 1132017
    new-instance v0, LX/1sw;

    const-string v1, "amendedMessageId"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jh;->d:LX/1sw;

    .line 1132018
    new-instance v0, LX/1sw;

    const-string v1, "timestamp"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jh;->e:LX/1sw;

    .line 1132019
    new-instance v0, LX/1sw;

    const-string v1, "actorFbId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jh;->f:LX/1sw;

    .line 1132020
    new-instance v0, LX/1sw;

    const-string v1, "mutation"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jh;->g:LX/1sw;

    .line 1132021
    sput-boolean v4, LX/6jh;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;LX/6kb;)V
    .locals 0

    .prologue
    .line 1132152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132153
    iput-object p1, p0, LX/6jh;->threadKey:LX/6l9;

    .line 1132154
    iput-object p2, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    .line 1132155
    iput-object p3, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    .line 1132156
    iput-object p4, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    .line 1132157
    iput-object p5, p0, LX/6jh;->mutation:LX/6kb;

    .line 1132158
    return-void
.end method

.method public static a(LX/6jh;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1132141
    iget-object v0, p0, LX/6jh;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1132142
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132143
    :cond_0
    iget-object v0, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1132144
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'amendedMessageId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132145
    :cond_1
    iget-object v0, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 1132146
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'timestamp\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132147
    :cond_2
    iget-object v0, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 1132148
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'actorFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132149
    :cond_3
    iget-object v0, p0, LX/6jh;->mutation:LX/6kb;

    if-nez v0, :cond_4

    .line 1132150
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'mutation\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132151
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1132089
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1132090
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1132091
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1132092
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaAmendMessage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132093
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132094
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132095
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132096
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132097
    const-string v4, "threadKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132098
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132099
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132100
    iget-object v4, p0, LX/6jh;->threadKey:LX/6l9;

    if-nez v4, :cond_3

    .line 1132101
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132102
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132103
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132104
    const-string v4, "amendedMessageId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132105
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132106
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132107
    iget-object v4, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 1132108
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132109
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132110
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132111
    const-string v4, "timestamp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132112
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132113
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132114
    iget-object v4, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    if-nez v4, :cond_5

    .line 1132115
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132116
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132117
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132118
    const-string v4, "actorFbId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132119
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132120
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132121
    iget-object v4, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    if-nez v4, :cond_6

    .line 1132122
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132123
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132124
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132125
    const-string v4, "mutation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132126
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132127
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132128
    iget-object v0, p0, LX/6jh;->mutation:LX/6kb;

    if-nez v0, :cond_7

    .line 1132129
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132130
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132131
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132132
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132133
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1132134
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1132135
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1132136
    :cond_3
    iget-object v4, p0, LX/6jh;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1132137
    :cond_4
    iget-object v4, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1132138
    :cond_5
    iget-object v4, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1132139
    :cond_6
    iget-object v4, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1132140
    :cond_7
    iget-object v0, p0, LX/6jh;->mutation:LX/6kb;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1132069
    invoke-static {p0}, LX/6jh;->a(LX/6jh;)V

    .line 1132070
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132071
    iget-object v0, p0, LX/6jh;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1132072
    sget-object v0, LX/6jh;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132073
    iget-object v0, p0, LX/6jh;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1132074
    :cond_0
    iget-object v0, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1132075
    sget-object v0, LX/6jh;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132076
    iget-object v0, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1132077
    :cond_1
    iget-object v0, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1132078
    sget-object v0, LX/6jh;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132079
    iget-object v0, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132080
    :cond_2
    iget-object v0, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1132081
    sget-object v0, LX/6jh;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132082
    iget-object v0, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132083
    :cond_3
    iget-object v0, p0, LX/6jh;->mutation:LX/6kb;

    if-eqz v0, :cond_4

    .line 1132084
    sget-object v0, LX/6jh;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132085
    iget-object v0, p0, LX/6jh;->mutation:LX/6kb;

    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    .line 1132086
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132087
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132088
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132026
    if-nez p1, :cond_1

    .line 1132027
    :cond_0
    :goto_0
    return v0

    .line 1132028
    :cond_1
    instance-of v1, p1, LX/6jh;

    if-eqz v1, :cond_0

    .line 1132029
    check-cast p1, LX/6jh;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1132030
    if-nez p1, :cond_3

    .line 1132031
    :cond_2
    :goto_1
    move v0, v2

    .line 1132032
    goto :goto_0

    .line 1132033
    :cond_3
    iget-object v0, p0, LX/6jh;->threadKey:LX/6l9;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1132034
    :goto_2
    iget-object v3, p1, LX/6jh;->threadKey:LX/6l9;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1132035
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1132036
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132037
    iget-object v0, p0, LX/6jh;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6jh;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132038
    :cond_5
    iget-object v0, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1132039
    :goto_4
    iget-object v3, p1, LX/6jh;->amendedMessageId:Ljava/lang/String;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1132040
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1132041
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132042
    iget-object v0, p0, LX/6jh;->amendedMessageId:Ljava/lang/String;

    iget-object v3, p1, LX/6jh;->amendedMessageId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132043
    :cond_7
    iget-object v0, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1132044
    :goto_6
    iget-object v3, p1, LX/6jh;->timestamp:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1132045
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1132046
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132047
    iget-object v0, p0, LX/6jh;->timestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6jh;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132048
    :cond_9
    iget-object v0, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1132049
    :goto_8
    iget-object v3, p1, LX/6jh;->actorFbId:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1132050
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1132051
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132052
    iget-object v0, p0, LX/6jh;->actorFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jh;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132053
    :cond_b
    iget-object v0, p0, LX/6jh;->mutation:LX/6kb;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1132054
    :goto_a
    iget-object v3, p1, LX/6jh;->mutation:LX/6kb;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1132055
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1132056
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132057
    iget-object v0, p0, LX/6jh;->mutation:LX/6kb;

    iget-object v3, p1, LX/6jh;->mutation:LX/6kb;

    invoke-virtual {v0, v3}, LX/6kb;->a(LX/6kb;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_d
    move v2, v1

    .line 1132058
    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 1132059
    goto/16 :goto_2

    :cond_f
    move v3, v2

    .line 1132060
    goto/16 :goto_3

    :cond_10
    move v0, v2

    .line 1132061
    goto :goto_4

    :cond_11
    move v3, v2

    .line 1132062
    goto :goto_5

    :cond_12
    move v0, v2

    .line 1132063
    goto :goto_6

    :cond_13
    move v3, v2

    .line 1132064
    goto :goto_7

    :cond_14
    move v0, v2

    .line 1132065
    goto :goto_8

    :cond_15
    move v3, v2

    .line 1132066
    goto :goto_9

    :cond_16
    move v0, v2

    .line 1132067
    goto :goto_a

    :cond_17
    move v3, v2

    .line 1132068
    goto :goto_b
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132025
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132022
    sget-boolean v0, LX/6jh;->a:Z

    .line 1132023
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jh;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132024
    return-object v0
.end method
