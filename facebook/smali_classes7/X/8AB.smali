.class public final enum LX/8AB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8AB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8AB;

.field public static final enum ADD_FEATURED_CONTAINER_NULL_STATE:LX/8AB;

.field public static final enum ALBUM:LX/8AB;

.field public static final enum ALBUMSTAB:LX/8AB;

.field public static final enum COMPOSER_ADD_MORE_MEDIA:LX/8AB;

.field public static final enum COMPOST:LX/8AB;

.field public static final enum COVERPHOTO:LX/8AB;

.field public static final enum CREATIVECAM:LX/8AB;

.field public static final enum EVENT:LX/8AB;

.field public static final enum FACEWEB:LX/8AB;

.field public static final enum FAVORITE_MEDIA_PICKER:LX/8AB;

.field public static final enum FAVORITE_PHOTOS:LX/8AB;

.field public static final enum FEED:LX/8AB;

.field public static final enum FUNDRAISER_CREATION:LX/8AB;

.field public static final enum GOODWILL_COMPOSER:LX/8AB;

.field public static final enum GROUP:LX/8AB;

.field public static final enum HOLIDAY_CARDS:LX/8AB;

.field public static final enum LIVE_VIDEO:LX/8AB;

.field public static final enum MARKETPLACE:LX/8AB;

.field public static final enum MEDIA_COMMENT:LX/8AB;

.field public static final enum MEDIA_GALLERY:LX/8AB;

.field public static final enum PAGE:LX/8AB;

.field public static final enum PAGE_COVER_PHOTO:LX/8AB;

.field public static final enum PAGE_MSG_SAVED_REPLY:LX/8AB;

.field public static final enum PAGE_PHOTO_ONLY:LX/8AB;

.field public static final enum PAGE_PROFILE_PIC:LX/8AB;

.field public static final enum PAGE_SERVICE:LX/8AB;

.field public static final enum PAGE_SHOP_INVOICE_MANUAL_RECEIPT:LX/8AB;

.field public static final enum PAGE_VIDEO_ONLY:LX/8AB;

.field public static final enum PERMALINK:LX/8AB;

.field public static final enum PHOTOSTAB:LX/8AB;

.field public static final enum PHOTOS_BY_CATEGORY:LX/8AB;

.field public static final enum PHOTO_COMMENT:LX/8AB;

.field public static final enum PHOTO_MENU_UPLOAD:LX/8AB;

.field public static final enum PHOTO_REMINDER_MORE_PHOTOS:LX/8AB;

.field public static final enum PLACES_HOME:LX/8AB;

.field public static final enum PLACE_CREATION:LX/8AB;

.field public static final enum PLACE_PROFILE_PIC_SUGGESTS:LX/8AB;

.field public static final enum PROFILEPIC:LX/8AB;

.field public static final enum PROFILEPIC_NUX:LX/8AB;

.field public static final enum REACTION:LX/8AB;

.field public static final enum SAMPLE_APP:LX/8AB;

.field public static final enum SEARCH:LX/8AB;

.field public static final enum SLIDESHOW_EDIT:LX/8AB;

.field public static final enum SOUVENIR:LX/8AB;

.field public static final enum STORYLINE:LX/8AB;

.field public static final enum SUGGEST_EDITS:LX/8AB;

.field public static final enum TEST:LX/8AB;

.field public static final enum TIMELINE:LX/8AB;

.field public static final enum VIDEO_HOME:LX/8AB;

.field public static final enum WIDGET:LX/8AB;


# instance fields
.field private final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1305932
    new-instance v0, LX/8AB;

    const-string v1, "FEED"

    const-string v2, "feed"

    invoke-direct {v0, v1, v4, v2}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->FEED:LX/8AB;

    .line 1305933
    new-instance v0, LX/8AB;

    const-string v1, "TIMELINE"

    const-string v2, "native_timeline"

    invoke-direct {v0, v1, v5, v2}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->TIMELINE:LX/8AB;

    .line 1305934
    new-instance v0, LX/8AB;

    const-string v1, "EVENT"

    const-string v2, "native_event"

    invoke-direct {v0, v1, v6, v2}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->EVENT:LX/8AB;

    .line 1305935
    new-instance v0, LX/8AB;

    const-string v1, "PAGE"

    const-string v2, "native_page"

    invoke-direct {v0, v1, v7, v2}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE:LX/8AB;

    .line 1305936
    new-instance v0, LX/8AB;

    const-string v1, "PAGE_PHOTO_ONLY"

    const-string v2, "page_photo_only"

    invoke-direct {v0, v1, v8, v2}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE_PHOTO_ONLY:LX/8AB;

    .line 1305937
    new-instance v0, LX/8AB;

    const-string v1, "PAGE_VIDEO_ONLY"

    const/4 v2, 0x5

    const-string v3, "page_video_only"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE_VIDEO_ONLY:LX/8AB;

    .line 1305938
    new-instance v0, LX/8AB;

    const-string v1, "PAGE_PROFILE_PIC"

    const/4 v2, 0x6

    const-string v3, "page_profile_pic"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE_PROFILE_PIC:LX/8AB;

    .line 1305939
    new-instance v0, LX/8AB;

    const-string v1, "PAGE_SHOP_INVOICE_MANUAL_RECEIPT"

    const/4 v2, 0x7

    const-string v3, "page_shop_invoice_manual_receipt"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE_SHOP_INVOICE_MANUAL_RECEIPT:LX/8AB;

    .line 1305940
    new-instance v0, LX/8AB;

    const-string v1, "PAGE_COVER_PHOTO"

    const/16 v2, 0x8

    const-string v3, "page_cover_photo"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE_COVER_PHOTO:LX/8AB;

    .line 1305941
    new-instance v0, LX/8AB;

    const-string v1, "PAGE_MSG_SAVED_REPLY"

    const/16 v2, 0x9

    const-string v3, "page_msg_saved_reply"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE_MSG_SAVED_REPLY:LX/8AB;

    .line 1305942
    new-instance v0, LX/8AB;

    const-string v1, "PHOTOSTAB"

    const/16 v2, 0xa

    const-string v3, "photos_tab"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PHOTOSTAB:LX/8AB;

    .line 1305943
    new-instance v0, LX/8AB;

    const-string v1, "ALBUMSTAB"

    const/16 v2, 0xb

    const-string v3, "albums_tab"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->ALBUMSTAB:LX/8AB;

    .line 1305944
    new-instance v0, LX/8AB;

    const-string v1, "ALBUM"

    const/16 v2, 0xc

    const-string v3, "album"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->ALBUM:LX/8AB;

    .line 1305945
    new-instance v0, LX/8AB;

    const-string v1, "PROFILEPIC"

    const/16 v2, 0xd

    const-string v3, "profile_pic"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PROFILEPIC:LX/8AB;

    .line 1305946
    new-instance v0, LX/8AB;

    const-string v1, "PROFILEPIC_NUX"

    const/16 v2, 0xe

    const-string v3, "profile_pic_nux"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PROFILEPIC_NUX:LX/8AB;

    .line 1305947
    new-instance v0, LX/8AB;

    const-string v1, "COVERPHOTO"

    const/16 v2, 0xf

    const-string v3, "cover_photo"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->COVERPHOTO:LX/8AB;

    .line 1305948
    new-instance v0, LX/8AB;

    const-string v1, "FAVORITE_PHOTOS"

    const/16 v2, 0x10

    const-string v3, "favorite_photos"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->FAVORITE_PHOTOS:LX/8AB;

    .line 1305949
    new-instance v0, LX/8AB;

    const-string v1, "ADD_FEATURED_CONTAINER_NULL_STATE"

    const/16 v2, 0x11

    const-string v3, "afc_null_state"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->ADD_FEATURED_CONTAINER_NULL_STATE:LX/8AB;

    .line 1305950
    new-instance v0, LX/8AB;

    const-string v1, "FAVORITE_MEDIA_PICKER"

    const/16 v2, 0x12

    const-string v3, "favorite_media_picker"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->FAVORITE_MEDIA_PICKER:LX/8AB;

    .line 1305951
    new-instance v0, LX/8AB;

    const-string v1, "FACEWEB"

    const/16 v2, 0x13

    const-string v3, "face_web"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->FACEWEB:LX/8AB;

    .line 1305952
    new-instance v0, LX/8AB;

    const-string v1, "COMPOSER_ADD_MORE_MEDIA"

    const/16 v2, 0x14

    const-string v3, "composer_add_more"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    .line 1305953
    new-instance v0, LX/8AB;

    const-string v1, "PHOTO_COMMENT"

    const/16 v2, 0x15

    const-string v3, "photo_comment"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PHOTO_COMMENT:LX/8AB;

    .line 1305954
    new-instance v0, LX/8AB;

    const-string v1, "MEDIA_COMMENT"

    const/16 v2, 0x16

    const-string v3, "media_comment"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->MEDIA_COMMENT:LX/8AB;

    .line 1305955
    new-instance v0, LX/8AB;

    const-string v1, "GROUP"

    const/16 v2, 0x17

    const-string v3, "group"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->GROUP:LX/8AB;

    .line 1305956
    new-instance v0, LX/8AB;

    const-string v1, "SAMPLE_APP"

    const/16 v2, 0x18

    const-string v3, "sample_app"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->SAMPLE_APP:LX/8AB;

    .line 1305957
    new-instance v0, LX/8AB;

    const-string v1, "TEST"

    const/16 v2, 0x19

    const-string v3, "test"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->TEST:LX/8AB;

    .line 1305958
    new-instance v0, LX/8AB;

    const-string v1, "HOLIDAY_CARDS"

    const/16 v2, 0x1a

    const-string v3, "holiday_cards"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->HOLIDAY_CARDS:LX/8AB;

    .line 1305959
    new-instance v0, LX/8AB;

    const-string v1, "PLACE_PROFILE_PIC_SUGGESTS"

    const/16 v2, 0x1b

    const-string v3, "place_profile_pic_suggests"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PLACE_PROFILE_PIC_SUGGESTS:LX/8AB;

    .line 1305960
    new-instance v0, LX/8AB;

    const-string v1, "PLACES_HOME"

    const/16 v2, 0x1c

    const-string v3, "places_home"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PLACES_HOME:LX/8AB;

    .line 1305961
    new-instance v0, LX/8AB;

    const-string v1, "WIDGET"

    const/16 v2, 0x1d

    const-string v3, "widget"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->WIDGET:LX/8AB;

    .line 1305962
    new-instance v0, LX/8AB;

    const-string v1, "SUGGEST_EDITS"

    const/16 v2, 0x1e

    const-string v3, "suggest_edits"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->SUGGEST_EDITS:LX/8AB;

    .line 1305963
    new-instance v0, LX/8AB;

    const-string v1, "SEARCH"

    const/16 v2, 0x1f

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->SEARCH:LX/8AB;

    .line 1305964
    new-instance v0, LX/8AB;

    const-string v1, "PLACE_CREATION"

    const/16 v2, 0x20

    const-string v3, "place_creation"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PLACE_CREATION:LX/8AB;

    .line 1305965
    new-instance v0, LX/8AB;

    const-string v1, "PHOTOS_BY_CATEGORY"

    const/16 v2, 0x21

    const-string v3, "photos_by_category"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PHOTOS_BY_CATEGORY:LX/8AB;

    .line 1305966
    new-instance v0, LX/8AB;

    const-string v1, "MEDIA_GALLERY"

    const/16 v2, 0x22

    const-string v3, "media_gallery"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->MEDIA_GALLERY:LX/8AB;

    .line 1305967
    new-instance v0, LX/8AB;

    const-string v1, "PERMALINK"

    const/16 v2, 0x23

    const-string v3, "permalink"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PERMALINK:LX/8AB;

    .line 1305968
    new-instance v0, LX/8AB;

    const-string v1, "PHOTO_MENU_UPLOAD"

    const/16 v2, 0x24

    const-string v3, "photo_menu_upload"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PHOTO_MENU_UPLOAD:LX/8AB;

    .line 1305969
    new-instance v0, LX/8AB;

    const-string v1, "REACTION"

    const/16 v2, 0x25

    const-string v3, "reaction"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->REACTION:LX/8AB;

    .line 1305970
    new-instance v0, LX/8AB;

    const-string v1, "PHOTO_REMINDER_MORE_PHOTOS"

    const/16 v2, 0x26

    const-string v3, "photo_reminder_more_photos"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PHOTO_REMINDER_MORE_PHOTOS:LX/8AB;

    .line 1305971
    new-instance v0, LX/8AB;

    const-string v1, "GOODWILL_COMPOSER"

    const/16 v2, 0x27

    const-string v3, "goodwill_composer"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->GOODWILL_COMPOSER:LX/8AB;

    .line 1305972
    new-instance v0, LX/8AB;

    const-string v1, "SOUVENIR"

    const/16 v2, 0x28

    const-string v3, "souvenir"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->SOUVENIR:LX/8AB;

    .line 1305973
    new-instance v0, LX/8AB;

    const-string v1, "CREATIVECAM"

    const/16 v2, 0x29

    const-string v3, "creativecam"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->CREATIVECAM:LX/8AB;

    .line 1305974
    new-instance v0, LX/8AB;

    const-string v1, "PAGE_SERVICE"

    const/16 v2, 0x2a

    const-string v3, "page_service"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->PAGE_SERVICE:LX/8AB;

    .line 1305975
    new-instance v0, LX/8AB;

    const-string v1, "MARKETPLACE"

    const/16 v2, 0x2b

    const-string v3, "marketplace"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->MARKETPLACE:LX/8AB;

    .line 1305976
    new-instance v0, LX/8AB;

    const-string v1, "COMPOST"

    const/16 v2, 0x2c

    const-string v3, "compost"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->COMPOST:LX/8AB;

    .line 1305977
    new-instance v0, LX/8AB;

    const-string v1, "SLIDESHOW_EDIT"

    const/16 v2, 0x2d

    const-string v3, "slideshow_edit"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->SLIDESHOW_EDIT:LX/8AB;

    .line 1305978
    new-instance v0, LX/8AB;

    const-string v1, "VIDEO_HOME"

    const/16 v2, 0x2e

    const-string v3, "video_home"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->VIDEO_HOME:LX/8AB;

    .line 1305979
    new-instance v0, LX/8AB;

    const-string v1, "FUNDRAISER_CREATION"

    const/16 v2, 0x2f

    const-string v3, "fundraiser_creation"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->FUNDRAISER_CREATION:LX/8AB;

    .line 1305980
    new-instance v0, LX/8AB;

    const-string v1, "LIVE_VIDEO"

    const/16 v2, 0x30

    const-string v3, "live_video"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->LIVE_VIDEO:LX/8AB;

    .line 1305981
    new-instance v0, LX/8AB;

    const-string v1, "STORYLINE"

    const/16 v2, 0x31

    const-string v3, "storyline"

    invoke-direct {v0, v1, v2, v3}, LX/8AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8AB;->STORYLINE:LX/8AB;

    .line 1305982
    const/16 v0, 0x32

    new-array v0, v0, [LX/8AB;

    sget-object v1, LX/8AB;->FEED:LX/8AB;

    aput-object v1, v0, v4

    sget-object v1, LX/8AB;->TIMELINE:LX/8AB;

    aput-object v1, v0, v5

    sget-object v1, LX/8AB;->EVENT:LX/8AB;

    aput-object v1, v0, v6

    sget-object v1, LX/8AB;->PAGE:LX/8AB;

    aput-object v1, v0, v7

    sget-object v1, LX/8AB;->PAGE_PHOTO_ONLY:LX/8AB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8AB;->PAGE_VIDEO_ONLY:LX/8AB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8AB;->PAGE_PROFILE_PIC:LX/8AB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8AB;->PAGE_SHOP_INVOICE_MANUAL_RECEIPT:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8AB;->PAGE_COVER_PHOTO:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8AB;->PAGE_MSG_SAVED_REPLY:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8AB;->PHOTOSTAB:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8AB;->ALBUMSTAB:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8AB;->ALBUM:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8AB;->PROFILEPIC:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8AB;->PROFILEPIC_NUX:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8AB;->COVERPHOTO:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8AB;->FAVORITE_PHOTOS:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8AB;->ADD_FEATURED_CONTAINER_NULL_STATE:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/8AB;->FAVORITE_MEDIA_PICKER:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/8AB;->FACEWEB:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/8AB;->PHOTO_COMMENT:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/8AB;->MEDIA_COMMENT:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/8AB;->GROUP:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/8AB;->SAMPLE_APP:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/8AB;->TEST:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/8AB;->HOLIDAY_CARDS:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/8AB;->PLACE_PROFILE_PIC_SUGGESTS:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/8AB;->PLACES_HOME:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/8AB;->WIDGET:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/8AB;->SUGGEST_EDITS:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/8AB;->SEARCH:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/8AB;->PLACE_CREATION:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/8AB;->PHOTOS_BY_CATEGORY:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/8AB;->MEDIA_GALLERY:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/8AB;->PERMALINK:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/8AB;->PHOTO_MENU_UPLOAD:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/8AB;->REACTION:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/8AB;->PHOTO_REMINDER_MORE_PHOTOS:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/8AB;->GOODWILL_COMPOSER:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/8AB;->SOUVENIR:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/8AB;->CREATIVECAM:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/8AB;->PAGE_SERVICE:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/8AB;->MARKETPLACE:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/8AB;->COMPOST:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/8AB;->SLIDESHOW_EDIT:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/8AB;->VIDEO_HOME:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/8AB;->FUNDRAISER_CREATION:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/8AB;->LIVE_VIDEO:LX/8AB;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/8AB;->STORYLINE:LX/8AB;

    aput-object v2, v0, v1

    sput-object v0, LX/8AB;->$VALUES:[LX/8AB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1305983
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1305984
    iput-object p3, p0, LX/8AB;->analyticsName:Ljava/lang/String;

    .line 1305985
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8AB;
    .locals 1

    .prologue
    .line 1305986
    const-class v0, LX/8AB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8AB;

    return-object v0
.end method

.method public static values()[LX/8AB;
    .locals 1

    .prologue
    .line 1305987
    sget-object v0, LX/8AB;->$VALUES:[LX/8AB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8AB;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1305988
    iget-object v0, p0, LX/8AB;->analyticsName:Ljava/lang/String;

    return-object v0
.end method
