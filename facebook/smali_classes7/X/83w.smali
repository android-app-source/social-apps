.class public final LX/83w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;",
        ">;",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2dj;


# direct methods
.method public constructor <init>(LX/2dj;)V
    .locals 0

    .prologue
    .line 1290701
    iput-object p1, p0, LX/83w;->a:LX/2dj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290702
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1290703
    if-eqz p1, :cond_0

    .line 1290704
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290705
    check-cast v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1290706
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290707
    check-cast v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 1290708
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
