.class public LX/6hU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1128286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128287
    return-void
.end method

.method public static a(LX/6kG;)LX/6hT;
    .locals 4

    .prologue
    .line 1128389
    iget-object v1, p0, LX/6kG;->messageMetadata:LX/6kn;

    .line 1128390
    const/4 v0, 0x0

    .line 1128391
    iget-object v2, v1, LX/6kn;->threadKey:LX/6l9;

    iget-object v2, v2, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 1128392
    iget-object v0, v1, LX/6kn;->threadKey:LX/6l9;

    iget-object v0, v0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1128393
    :cond_0
    new-instance v1, LX/6hT;

    invoke-direct {v1}, LX/6hT;-><init>()V

    .line 1128394
    iget-object v2, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    invoke-static {v2}, LX/03R;->valueOf(Ljava/lang/Boolean;)LX/03R;

    move-result-object v2

    iput-object v2, v1, LX/6hT;->d:LX/03R;

    .line 1128395
    iput-object v0, v1, LX/6hT;->a:Ljava/lang/String;

    .line 1128396
    iget-object v0, p0, LX/6kG;->startTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, LX/6hT;->b:J

    .line 1128397
    iget-object v0, p0, LX/6kG;->duration:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, LX/6hT;->c:J

    .line 1128398
    iget-object v0, p0, LX/6kG;->acknowledged:Ljava/lang/Boolean;

    invoke-static {v0}, LX/03R;->valueOf(Ljava/lang/Boolean;)LX/03R;

    move-result-object v0

    iput-object v0, v1, LX/6hT;->g:LX/03R;

    .line 1128399
    return-object v1
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1128377
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->I()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->k()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->p()I

    move-result v2

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->K()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v2, p1

    invoke-static/range {v0 .. v6}, LX/6hU;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;JLX/0lF;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .locals 8

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x0

    .line 1128378
    const-wide/16 v0, 0x0

    .line 1128379
    const-string v2, "c_s"

    invoke-virtual {p4, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1128380
    const-string v2, "c_s"

    invoke-virtual {p4, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide p2

    .line 1128381
    :cond_0
    const-string v2, "c_d"

    invoke-virtual {p4, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1128382
    const-string v0, "c_d"

    invoke-virtual {p4, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v0

    .line 1128383
    :cond_1
    const-string v2, "c_t"

    invoke-virtual {p4, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1128384
    const-string v2, "c_t"

    invoke-virtual {p4, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0lF;->b(I)I

    move-result v2

    .line 1128385
    if-eq v2, v3, :cond_2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    const/4 v2, 0x1

    .line 1128386
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p1

    invoke-static/range {v0 .. v6}, LX/6hU;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    .line 1128387
    return-object v0

    :cond_2
    move v2, v6

    .line 1128388
    goto :goto_0

    :cond_3
    move v2, v6

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1128296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1128297
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1128298
    if-nez p1, :cond_0

    .line 1128299
    :goto_0
    if-nez p2, :cond_1

    .line 1128300
    :goto_1
    if-nez p3, :cond_2

    .line 1128301
    :goto_2
    if-nez p4, :cond_3

    .line 1128302
    :goto_3
    if-nez p5, :cond_4

    .line 1128303
    :goto_4
    if-nez p6, :cond_5

    .line 1128304
    :goto_5
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1128305
    new-instance v2, LX/5Zg;

    invoke-direct {v2}, LX/5Zg;-><init>()V

    .line 1128306
    iput-object p0, v2, LX/5Zg;->l:Ljava/lang/String;

    .line 1128307
    move-object v2, v2

    .line 1128308
    iput-object v0, v2, LX/5Zg;->h:LX/0Px;

    .line 1128309
    move-object v0, v2

    .line 1128310
    iput-object v1, v0, LX/5Zg;->b:LX/0Px;

    .line 1128311
    move-object v0, v0

    .line 1128312
    invoke-virtual {v0}, LX/5Zg;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    .line 1128313
    new-instance v1, LX/5Zl;

    invoke-direct {v1}, LX/5Zl;-><init>()V

    .line 1128314
    iput-object v0, v1, LX/5Zl;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 1128315
    move-object v0, v1

    .line 1128316
    invoke-virtual {v0}, LX/5Zl;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    return-object v0

    .line 1128317
    :cond_0
    new-instance v2, LX/5Ze;

    invoke-direct {v2}, LX/5Ze;-><init>()V

    const-string v3, "senderID"

    .line 1128318
    iput-object v3, v2, LX/5Ze;->b:Ljava/lang/String;

    .line 1128319
    move-object v2, v2

    .line 1128320
    new-instance v3, LX/5Zf;

    invoke-direct {v3}, LX/5Zf;-><init>()V

    .line 1128321
    iput-object p1, v3, LX/5Zf;->a:Ljava/lang/String;

    .line 1128322
    move-object v3, v3

    .line 1128323
    invoke-virtual {v3}, LX/5Zf;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    move-result-object v3

    .line 1128324
    iput-object v3, v2, LX/5Ze;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    .line 1128325
    move-object v2, v2

    .line 1128326
    invoke-virtual {v2}, LX/5Ze;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1128327
    :cond_1
    new-instance v2, LX/5Ze;

    invoke-direct {v2}, LX/5Ze;-><init>()V

    const-string v3, "peerUserID"

    .line 1128328
    iput-object v3, v2, LX/5Ze;->b:Ljava/lang/String;

    .line 1128329
    move-object v2, v2

    .line 1128330
    new-instance v3, LX/5Zf;

    invoke-direct {v3}, LX/5Zf;-><init>()V

    .line 1128331
    iput-object p2, v3, LX/5Zf;->a:Ljava/lang/String;

    .line 1128332
    move-object v3, v3

    .line 1128333
    invoke-virtual {v3}, LX/5Zf;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    move-result-object v3

    .line 1128334
    iput-object v3, v2, LX/5Ze;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    .line 1128335
    move-object v2, v2

    .line 1128336
    invoke-virtual {v2}, LX/5Ze;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1128337
    :cond_2
    new-instance v2, LX/5Ze;

    invoke-direct {v2}, LX/5Ze;-><init>()V

    const-string v3, "answered"

    .line 1128338
    iput-object v3, v2, LX/5Ze;->b:Ljava/lang/String;

    .line 1128339
    move-object v2, v2

    .line 1128340
    new-instance v3, LX/5Zf;

    invoke-direct {v3}, LX/5Zf;-><init>()V

    invoke-virtual {p3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1128341
    iput-object p1, v3, LX/5Zf;->a:Ljava/lang/String;

    .line 1128342
    move-object v3, v3

    .line 1128343
    invoke-virtual {v3}, LX/5Zf;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    move-result-object v3

    .line 1128344
    iput-object v3, v2, LX/5Ze;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    .line 1128345
    move-object v2, v2

    .line 1128346
    invoke-virtual {v2}, LX/5Ze;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 1128347
    :cond_3
    new-instance v2, LX/5Ze;

    invoke-direct {v2}, LX/5Ze;-><init>()V

    const-string v3, "duration"

    .line 1128348
    iput-object v3, v2, LX/5Ze;->b:Ljava/lang/String;

    .line 1128349
    move-object v2, v2

    .line 1128350
    new-instance v3, LX/5Zf;

    invoke-direct {v3}, LX/5Zf;-><init>()V

    invoke-virtual {p4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1128351
    iput-object p1, v3, LX/5Zf;->a:Ljava/lang/String;

    .line 1128352
    move-object v3, v3

    .line 1128353
    invoke-virtual {v3}, LX/5Zf;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    move-result-object v3

    .line 1128354
    iput-object v3, v2, LX/5Ze;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    .line 1128355
    move-object v2, v2

    .line 1128356
    invoke-virtual {v2}, LX/5Ze;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_3

    .line 1128357
    :cond_4
    new-instance v2, LX/5Ze;

    invoke-direct {v2}, LX/5Ze;-><init>()V

    const-string v3, "timestamp"

    .line 1128358
    iput-object v3, v2, LX/5Ze;->b:Ljava/lang/String;

    .line 1128359
    move-object v2, v2

    .line 1128360
    new-instance v3, LX/5Zf;

    invoke-direct {v3}, LX/5Zf;-><init>()V

    invoke-virtual {p5}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1128361
    iput-object p1, v3, LX/5Zf;->a:Ljava/lang/String;

    .line 1128362
    move-object v3, v3

    .line 1128363
    invoke-virtual {v3}, LX/5Zf;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    move-result-object v3

    .line 1128364
    iput-object v3, v2, LX/5Ze;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    .line 1128365
    move-object v2, v2

    .line 1128366
    invoke-virtual {v2}, LX/5Ze;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_4

    .line 1128367
    :cond_5
    new-instance v2, LX/5Ze;

    invoke-direct {v2}, LX/5Ze;-><init>()V

    const-string v3, "videoCall"

    .line 1128368
    iput-object v3, v2, LX/5Ze;->b:Ljava/lang/String;

    .line 1128369
    move-object v2, v2

    .line 1128370
    new-instance v3, LX/5Zf;

    invoke-direct {v3}, LX/5Zf;-><init>()V

    invoke-virtual {p6}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1128371
    iput-object p1, v3, LX/5Zf;->a:Ljava/lang/String;

    .line 1128372
    move-object v3, v3

    .line 1128373
    invoke-virtual {v3}, LX/5Zf;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    move-result-object v3

    .line 1128374
    iput-object v3, v2, LX/5Ze;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel$ValueModel;

    .line 1128375
    move-object v2, v2

    .line 1128376
    invoke-virtual {v2}, LX/5Ze;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_5
.end method

.method public static b(LX/6kG;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1128289
    iget-object v1, p0, LX/6kG;->messageMetadata:LX/6kn;

    .line 1128290
    const/4 v2, 0x0

    .line 1128291
    iget-object v3, v1, LX/6kn;->threadKey:LX/6l9;

    iget-object v3, v3, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v3, :cond_0

    .line 1128292
    iget-object v2, v1, LX/6kn;->threadKey:LX/6l9;

    iget-object v2, v2, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1128293
    :cond_0
    iget-object v3, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    .line 1128294
    iget-object v3, p0, LX/6kG;->eventType:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v6, v0

    .line 1128295
    :goto_0
    iget-object v0, v1, LX/6kn;->adminText:Ljava/lang/String;

    iget-object v1, v1, LX/6kn;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/6kG;->answered:Ljava/lang/Boolean;

    iget-object v4, p0, LX/6kG;->duration:Ljava/lang/Long;

    iget-object v5, p0, LX/6kG;->startTime:Ljava/lang/Long;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/6hU;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    return-object v0

    :cond_2
    move v6, v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1128288
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->I()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->o()I

    move-result v2

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->J()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v2, p1

    invoke-static/range {v0 .. v6}, LX/6hU;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    return-object v0
.end method
