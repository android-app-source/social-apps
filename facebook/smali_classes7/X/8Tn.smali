.class public LX/8Tn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Ve;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1349092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1349093
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1349094
    iput-object v0, p0, LX/8Tn;->a:LX/0Ot;

    .line 1349095
    return-void
.end method

.method public static a(LX/0QB;)LX/8Tn;
    .locals 4

    .prologue
    .line 1349096
    const-class v1, LX/8Tn;

    monitor-enter v1

    .line 1349097
    :try_start_0
    sget-object v0, LX/8Tn;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1349098
    sput-object v2, LX/8Tn;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1349099
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1349100
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1349101
    new-instance v3, LX/8Tn;

    invoke-direct {v3}, LX/8Tn;-><init>()V

    .line 1349102
    const/16 p0, 0x3084

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1349103
    iput-object p0, v3, LX/8Tn;->a:LX/0Ot;

    .line 1349104
    move-object v0, v3

    .line 1349105
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1349106
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Tn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1349107
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1349108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/0Px;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349109
    if-nez p1, :cond_0

    .line 1349110
    const/4 v0, 0x0

    .line 1349111
    :goto_0
    return-object v0

    .line 1349112
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1349113
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    .line 1349114
    invoke-virtual {p0, v0, p2}, LX/8Tn;->a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;Ljava/lang/String;)LX/8Vb;

    move-result-object v0

    .line 1349115
    if-eqz v0, :cond_1

    .line 1349116
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1349117
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1349118
    goto :goto_0
.end method

.method public static b(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349119
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel$MessageThreadModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1349120
    :cond_0
    const/4 v0, 0x0

    .line 1349121
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel$MessageThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel$MessageThreadModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;Ljava/lang/String;)LX/8Vb;
    .locals 5
    .param p1    # Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1349122
    if-nez p1, :cond_1

    .line 1349123
    :cond_0
    :goto_0
    return-object v0

    .line 1349124
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->m()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    move-result-object v1

    .line 1349125
    if-eqz v1, :cond_0

    .line 1349126
    invoke-static {}, LX/8Vb;->b()LX/8Va;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 1349127
    iput-object v2, v0, LX/8Va;->d:Ljava/lang/String;

    .line 1349128
    move-object v0, v0

    .line 1349129
    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1349130
    iput-object v2, v0, LX/8Va;->c:Ljava/lang/String;

    .line 1349131
    move-object v0, v0

    .line 1349132
    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 1349133
    iput-object v2, v0, LX/8Va;->a:Ljava/lang/String;

    .line 1349134
    move-object v2, v0

    .line 1349135
    iget-object v0, p0, LX/8Tn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ve;

    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8Ve;->a(Ljava/lang/String;)LX/8Vd;

    move-result-object v0

    .line 1349136
    iput-object v0, v2, LX/8Va;->k:LX/8Vd;

    .line 1349137
    move-object v0, v2

    .line 1349138
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1349139
    iput-object v1, v0, LX/8Va;->h:Ljava/lang/String;

    .line 1349140
    move-object v0, v0

    .line 1349141
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->l()I

    move-result v1

    int-to-long v2, v1

    .line 1349142
    iput-wide v2, v0, LX/8Va;->f:J

    .line 1349143
    move-object v0, v0

    .line 1349144
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->l()I

    move-result v1

    int-to-long v2, v1

    .line 1349145
    iput-wide v2, v0, LX/8Va;->g:J

    .line 1349146
    move-object v0, v0

    .line 1349147
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 1349148
    iput-object v1, v0, LX/8Va;->j:Ljava/lang/String;

    .line 1349149
    move-object v0, v0

    .line 1349150
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->a()Z

    move-result v1

    .line 1349151
    iput-boolean v1, v0, LX/8Va;->m:Z

    .line 1349152
    move-object v0, v0

    .line 1349153
    iput-object p2, v0, LX/8Va;->l:Ljava/lang/String;

    .line 1349154
    move-object v0, v0

    .line 1349155
    invoke-virtual {v0}, LX/8Va;->a()LX/8Vb;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1    # Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349156
    if-nez p1, :cond_0

    .line 1349157
    const/4 v0, 0x0

    .line 1349158
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/8Tn;->a(LX/0Px;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;)Ljava/util/List;
    .locals 2
    .param p1    # Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1349159
    if-nez p1, :cond_0

    .line 1349160
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;->j()LX/0Px;

    move-result-object v1

    invoke-direct {p0, v1, v0}, LX/8Tn;->a(LX/0Px;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
