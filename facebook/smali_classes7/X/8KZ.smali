.class public final enum LX/8KZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8KZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8KZ;

.field public static final enum MEDIA_PROCESSING_FAILED:LX/8KZ;

.field public static final enum MEDIA_PROCESSING_SUCCESS:LX/8KZ;

.field public static final enum PROCESSING:LX/8KZ;

.field public static final enum PUBLISHING:LX/8KZ;

.field public static final enum UPLOADING:LX/8KZ;

.field public static final enum UPLOAD_FAILED:LX/8KZ;

.field public static final enum UPLOAD_SUCCESS:LX/8KZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1330503
    new-instance v0, LX/8KZ;

    const-string v1, "PROCESSING"

    invoke-direct {v0, v1, v3}, LX/8KZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8KZ;->PROCESSING:LX/8KZ;

    .line 1330504
    new-instance v0, LX/8KZ;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v4}, LX/8KZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8KZ;->UPLOADING:LX/8KZ;

    .line 1330505
    new-instance v0, LX/8KZ;

    const-string v1, "PUBLISHING"

    invoke-direct {v0, v1, v5}, LX/8KZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8KZ;->PUBLISHING:LX/8KZ;

    .line 1330506
    new-instance v0, LX/8KZ;

    const-string v1, "UPLOAD_FAILED"

    invoke-direct {v0, v1, v6}, LX/8KZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8KZ;->UPLOAD_FAILED:LX/8KZ;

    .line 1330507
    new-instance v0, LX/8KZ;

    const-string v1, "UPLOAD_SUCCESS"

    invoke-direct {v0, v1, v7}, LX/8KZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8KZ;->UPLOAD_SUCCESS:LX/8KZ;

    .line 1330508
    new-instance v0, LX/8KZ;

    const-string v1, "MEDIA_PROCESSING_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8KZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8KZ;->MEDIA_PROCESSING_SUCCESS:LX/8KZ;

    .line 1330509
    new-instance v0, LX/8KZ;

    const-string v1, "MEDIA_PROCESSING_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8KZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8KZ;->MEDIA_PROCESSING_FAILED:LX/8KZ;

    .line 1330510
    const/4 v0, 0x7

    new-array v0, v0, [LX/8KZ;

    sget-object v1, LX/8KZ;->PROCESSING:LX/8KZ;

    aput-object v1, v0, v3

    sget-object v1, LX/8KZ;->UPLOADING:LX/8KZ;

    aput-object v1, v0, v4

    sget-object v1, LX/8KZ;->PUBLISHING:LX/8KZ;

    aput-object v1, v0, v5

    sget-object v1, LX/8KZ;->UPLOAD_FAILED:LX/8KZ;

    aput-object v1, v0, v6

    sget-object v1, LX/8KZ;->UPLOAD_SUCCESS:LX/8KZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8KZ;->MEDIA_PROCESSING_SUCCESS:LX/8KZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8KZ;->MEDIA_PROCESSING_FAILED:LX/8KZ;

    aput-object v2, v0, v1

    sput-object v0, LX/8KZ;->$VALUES:[LX/8KZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1330511
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8KZ;
    .locals 1

    .prologue
    .line 1330512
    const-class v0, LX/8KZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8KZ;

    return-object v0
.end method

.method public static values()[LX/8KZ;
    .locals 1

    .prologue
    .line 1330513
    sget-object v0, LX/8KZ;->$VALUES:[LX/8KZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8KZ;

    return-object v0
.end method
