.class public final enum LX/6hM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6hM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6hM;

.field public static final enum SINGLE_IMAGE_LANDSCAPE_HEIGHT:LX/6hM;

.field public static final enum SINGLE_IMAGE_NO_SIZE_WIDTH_HEIGHT:LX/6hM;

.field public static final enum SINGLE_IMAGE_PORTRAIT_SQUARE_HEIGHT:LX/6hM;

.field public static final enum THREE_IMAGE_WIDTH_HEIGHT:LX/6hM;

.field public static final enum TWO_IMAGE_WIDTH_HEIGHT:LX/6hM;

.field public static final enum VIDEO_STICKER_WIDTH:LX/6hM;


# instance fields
.field public final dp:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1128069
    new-instance v0, LX/6hM;

    const-string v1, "TWO_IMAGE_WIDTH_HEIGHT"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v4, v2}, LX/6hM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6hM;->TWO_IMAGE_WIDTH_HEIGHT:LX/6hM;

    .line 1128070
    new-instance v0, LX/6hM;

    const-string v1, "THREE_IMAGE_WIDTH_HEIGHT"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v5, v2}, LX/6hM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6hM;->THREE_IMAGE_WIDTH_HEIGHT:LX/6hM;

    .line 1128071
    new-instance v0, LX/6hM;

    const-string v1, "SINGLE_IMAGE_LANDSCAPE_HEIGHT"

    const/16 v2, 0x118

    invoke-direct {v0, v1, v6, v2}, LX/6hM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6hM;->SINGLE_IMAGE_LANDSCAPE_HEIGHT:LX/6hM;

    .line 1128072
    new-instance v0, LX/6hM;

    const-string v1, "SINGLE_IMAGE_PORTRAIT_SQUARE_HEIGHT"

    const/16 v2, 0x140

    invoke-direct {v0, v1, v7, v2}, LX/6hM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6hM;->SINGLE_IMAGE_PORTRAIT_SQUARE_HEIGHT:LX/6hM;

    .line 1128073
    new-instance v0, LX/6hM;

    const-string v1, "SINGLE_IMAGE_NO_SIZE_WIDTH_HEIGHT"

    const/16 v2, 0x140

    invoke-direct {v0, v1, v8, v2}, LX/6hM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6hM;->SINGLE_IMAGE_NO_SIZE_WIDTH_HEIGHT:LX/6hM;

    .line 1128074
    new-instance v0, LX/6hM;

    const-string v1, "VIDEO_STICKER_WIDTH"

    const/4 v2, 0x5

    const/16 v3, 0x70

    invoke-direct {v0, v1, v2, v3}, LX/6hM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6hM;->VIDEO_STICKER_WIDTH:LX/6hM;

    .line 1128075
    const/4 v0, 0x6

    new-array v0, v0, [LX/6hM;

    sget-object v1, LX/6hM;->TWO_IMAGE_WIDTH_HEIGHT:LX/6hM;

    aput-object v1, v0, v4

    sget-object v1, LX/6hM;->THREE_IMAGE_WIDTH_HEIGHT:LX/6hM;

    aput-object v1, v0, v5

    sget-object v1, LX/6hM;->SINGLE_IMAGE_LANDSCAPE_HEIGHT:LX/6hM;

    aput-object v1, v0, v6

    sget-object v1, LX/6hM;->SINGLE_IMAGE_PORTRAIT_SQUARE_HEIGHT:LX/6hM;

    aput-object v1, v0, v7

    sget-object v1, LX/6hM;->SINGLE_IMAGE_NO_SIZE_WIDTH_HEIGHT:LX/6hM;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6hM;->VIDEO_STICKER_WIDTH:LX/6hM;

    aput-object v2, v0, v1

    sput-object v0, LX/6hM;->$VALUES:[LX/6hM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1128076
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1128077
    iput p3, p0, LX/6hM;->dp:I

    .line 1128078
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6hM;
    .locals 1

    .prologue
    .line 1128079
    const-class v0, LX/6hM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6hM;

    return-object v0
.end method

.method public static values()[LX/6hM;
    .locals 1

    .prologue
    .line 1128080
    sget-object v0, LX/6hM;->$VALUES:[LX/6hM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6hM;

    return-object v0
.end method
