.class public final enum LX/7wO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7wO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7wO;

.field public static final enum ERROR:LX/7wO;

.field public static final enum FIELD_CHECKBOX:LX/7wO;

.field public static final enum FIELD_HEADING:LX/7wO;

.field public static final enum FIELD_SELECT_DROPDOWN:LX/7wO;

.field public static final enum FIELD_SELECT_EXPANDED:LX/7wO;

.field public static final enum FIELD_SELECT_MULTIPLE:LX/7wO;

.field public static final enum FIELD_TEXT:LX/7wO;

.field public static final enum GAP:LX/7wO;

.field public static final enum IMAGE:LX/7wO;

.field public static final enum ITEM_HEADING:LX/7wO;

.field public static final enum PARAGRAPH:LX/7wO;

.field public static final enum REGISTRATION_FOOTER:LX/7wO;

.field public static final enum REGISTRATION_HEADER:LX/7wO;

.field public static final enum SEPARATOR:LX/7wO;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1275737
    new-instance v0, LX/7wO;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v3}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->IMAGE:LX/7wO;

    .line 1275738
    new-instance v0, LX/7wO;

    const-string v1, "PARAGRAPH"

    invoke-direct {v0, v1, v4}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->PARAGRAPH:LX/7wO;

    .line 1275739
    new-instance v0, LX/7wO;

    const-string v1, "SEPARATOR"

    invoke-direct {v0, v1, v5}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->SEPARATOR:LX/7wO;

    .line 1275740
    new-instance v0, LX/7wO;

    const-string v1, "GAP"

    invoke-direct {v0, v1, v6}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->GAP:LX/7wO;

    .line 1275741
    new-instance v0, LX/7wO;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v7}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->ERROR:LX/7wO;

    .line 1275742
    new-instance v0, LX/7wO;

    const-string v1, "FIELD_TEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->FIELD_TEXT:LX/7wO;

    .line 1275743
    new-instance v0, LX/7wO;

    const-string v1, "FIELD_HEADING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->FIELD_HEADING:LX/7wO;

    .line 1275744
    new-instance v0, LX/7wO;

    const-string v1, "FIELD_CHECKBOX"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->FIELD_CHECKBOX:LX/7wO;

    .line 1275745
    new-instance v0, LX/7wO;

    const-string v1, "FIELD_SELECT_DROPDOWN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->FIELD_SELECT_DROPDOWN:LX/7wO;

    .line 1275746
    new-instance v0, LX/7wO;

    const-string v1, "FIELD_SELECT_EXPANDED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->FIELD_SELECT_EXPANDED:LX/7wO;

    .line 1275747
    new-instance v0, LX/7wO;

    const-string v1, "FIELD_SELECT_MULTIPLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->FIELD_SELECT_MULTIPLE:LX/7wO;

    .line 1275748
    new-instance v0, LX/7wO;

    const-string v1, "ITEM_HEADING"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->ITEM_HEADING:LX/7wO;

    .line 1275749
    new-instance v0, LX/7wO;

    const-string v1, "REGISTRATION_HEADER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->REGISTRATION_HEADER:LX/7wO;

    .line 1275750
    new-instance v0, LX/7wO;

    const-string v1, "REGISTRATION_FOOTER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/7wO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wO;->REGISTRATION_FOOTER:LX/7wO;

    .line 1275751
    const/16 v0, 0xe

    new-array v0, v0, [LX/7wO;

    sget-object v1, LX/7wO;->IMAGE:LX/7wO;

    aput-object v1, v0, v3

    sget-object v1, LX/7wO;->PARAGRAPH:LX/7wO;

    aput-object v1, v0, v4

    sget-object v1, LX/7wO;->SEPARATOR:LX/7wO;

    aput-object v1, v0, v5

    sget-object v1, LX/7wO;->GAP:LX/7wO;

    aput-object v1, v0, v6

    sget-object v1, LX/7wO;->ERROR:LX/7wO;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7wO;->FIELD_TEXT:LX/7wO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7wO;->FIELD_HEADING:LX/7wO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7wO;->FIELD_CHECKBOX:LX/7wO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7wO;->FIELD_SELECT_DROPDOWN:LX/7wO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7wO;->FIELD_SELECT_EXPANDED:LX/7wO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7wO;->FIELD_SELECT_MULTIPLE:LX/7wO;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7wO;->ITEM_HEADING:LX/7wO;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7wO;->REGISTRATION_HEADER:LX/7wO;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7wO;->REGISTRATION_FOOTER:LX/7wO;

    aput-object v2, v0, v1

    sput-object v0, LX/7wO;->$VALUES:[LX/7wO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1275736
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7wO;
    .locals 1

    .prologue
    .line 1275752
    const-class v0, LX/7wO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7wO;

    return-object v0
.end method

.method public static values()[LX/7wO;
    .locals 1

    .prologue
    .line 1275735
    sget-object v0, LX/7wO;->$VALUES:[LX/7wO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7wO;

    return-object v0
.end method
