.class public LX/8Fa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "LX/8Fb;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1318023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318024
    iput-object p1, p0, LX/8Fa;->a:LX/0Ot;

    .line 1318025
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1318026
    iget-object v0, p0, LX/8Fa;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Json node %s is null"

    invoke-static {v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1318027
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1318028
    check-cast p1, Ljava/lang/String;

    .line 1318029
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1318030
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1318031
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    .line 1318032
    const-string v3, "SELECT page_id, access_token FROM page WHERE page_id = \"%s\""

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object p1, v4, p0

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1318033
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1318034
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetchPageAccessToken"

    .line 1318035
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1318036
    move-object v1, v1

    .line 1318037
    const-string v2, "GET"

    .line 1318038
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1318039
    move-object v1, v1

    .line 1318040
    const-string v2, "fql"

    .line 1318041
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1318042
    move-object v1, v1

    .line 1318043
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1318044
    move-object v0, v1

    .line 1318045
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1318046
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1318047
    move-object v0, v0

    .line 1318048
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1318049
    const/4 v0, 0x0

    .line 1318050
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1318051
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    .line 1318052
    const-string v2, "data"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1318053
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v2

    if-lez v2, :cond_2

    .line 1318054
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    .line 1318055
    const-string v2, "page_id"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 1318056
    if-nez v2, :cond_0

    .line 1318057
    const-string v1, "page_id"

    invoke-direct {p0, v1}, LX/8Fa;->c(Ljava/lang/String;)V

    .line 1318058
    :goto_0
    return-object v0

    .line 1318059
    :cond_0
    const-string v3, "access_token"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1318060
    if-nez v1, :cond_1

    .line 1318061
    const-string v1, "access_token"

    invoke-direct {p0, v1}, LX/8Fa;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1318062
    :cond_1
    new-instance v0, LX/8Fb;

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/8Fb;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    goto :goto_0

    .line 1318063
    :cond_2
    const-string v1, "data"

    invoke-direct {p0, v1}, LX/8Fa;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
