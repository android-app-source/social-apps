.class public LX/7S6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61C;
.implements LX/61G;
.implements LX/6Jv;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[F


# instance fields
.field private final c:LX/5PR;

.field private d:LX/5Pb;

.field private e:Lcom/facebook/videocodec/effects/model/ColorFilter;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Z

.field private h:LX/6Js;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1208174
    const-class v0, LX/7S6;

    sput-object v0, LX/7S6;->a:Ljava/lang/Class;

    .line 1208175
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 1208176
    sput-object v0, LX/7S6;->b:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1208177
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1208172
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    invoke-direct {v0}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>()V

    invoke-direct {p0, v0}, LX/7S6;-><init>(Lcom/facebook/videocodec/effects/model/ColorFilter;)V

    .line 1208173
    return-void
.end method

.method public constructor <init>(Lcom/facebook/videocodec/effects/model/ColorFilter;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 1208163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208164
    iput-boolean v1, p0, LX/7S6;->g:Z

    .line 1208165
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7S6;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1208166
    invoke-virtual {p0, p1}, LX/7S6;->a(Lcom/facebook/videocodec/effects/model/ColorFilter;)V

    .line 1208167
    new-instance v0, LX/5PQ;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x5

    .line 1208168
    iput v1, v0, LX/5PQ;->a:I

    .line 1208169
    move-object v0, v0

    .line 1208170
    const-string v1, "aPosition"

    new-instance v2, LX/5Pg;

    new-array v3, v5, [F

    fill-array-data v3, :array_0

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "aTextureCoord"

    new-instance v2, LX/5Pg;

    new-array v3, v5, [F

    fill-array-data v3, :array_1

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7S6;->c:LX/5PR;

    .line 1208171
    return-void

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1208162
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 3

    .prologue
    .line 1208157
    const v0, 0x7f070010

    const v1, 0x7f07000f

    iget-boolean v2, p0, LX/7S6;->g:Z

    invoke-interface {p1, v0, v1, v2}, LX/5Pc;->a(IIZ)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7S6;->d:LX/5Pb;

    .line 1208158
    iget-object v0, p0, LX/7S6;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1208159
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    if-eqz v0, :cond_0

    .line 1208160
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    sget-object v1, LX/7Sc;->COLOR_FILTER_CHANGE:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1208161
    :cond_0
    return-void
.end method

.method public final a(LX/61H;)V
    .locals 0

    .prologue
    .line 1208156
    return-void
.end method

.method public final a(LX/6Js;)V
    .locals 2

    .prologue
    .line 1208145
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    if-eqz v0, :cond_0

    .line 1208146
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    sget-object v1, LX/7Sc;->COLOR_FILTER_CHANGE:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1208147
    :cond_0
    iput-object p1, p0, LX/7S6;->h:LX/6Js;

    .line 1208148
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    if-eqz v0, :cond_1

    .line 1208149
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    sget-object v1, LX/7Sc;->COLOR_FILTER_CHANGE:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1208150
    :cond_1
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 2

    .prologue
    .line 1208178
    sget-object v0, LX/7S4;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1208179
    sget-object v0, LX/7S6;->a:Ljava/lang/Class;

    const-string v1, "Received an event we did not register for"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1208180
    :goto_0
    return-void

    .line 1208181
    :pswitch_0
    check-cast p1, LX/7Sf;

    .line 1208182
    iget-object v0, p1, LX/7Sf;->a:Lcom/facebook/videocodec/effects/model/ColorFilter;

    move-object v0, v0

    .line 1208183
    invoke-virtual {p0, v0}, LX/7S6;->a(Lcom/facebook/videocodec/effects/model/ColorFilter;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/videocodec/effects/model/ColorFilter;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1208151
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Must provide non null filter"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1208152
    iput-object p1, p0, LX/7S6;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1208153
    iget-object v0, p0, LX/7S6;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1208154
    return-void

    .line 1208155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ZLX/5Pc;)V
    .locals 0

    .prologue
    .line 1208141
    iput-boolean p1, p0, LX/7S6;->g:Z

    .line 1208142
    if-eqz p2, :cond_0

    .line 1208143
    invoke-virtual {p0, p2}, LX/7S6;->a(LX/5Pc;)V

    .line 1208144
    :cond_0
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 4

    .prologue
    .line 1208124
    const-string v0, "onDrawFrame"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208125
    iget-object v0, p0, LX/7S6;->d:LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    .line 1208126
    iget-object v1, p0, LX/7S6;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    .line 1208127
    if-eqz v1, :cond_0

    .line 1208128
    const-string v1, "saturation"

    iget-object v2, p0, LX/7S6;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1208129
    iget v3, v2, Lcom/facebook/videocodec/effects/model/ColorFilter;->b:F

    move v2, v3

    .line 1208130
    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v1

    const-string v2, "brightness"

    iget-object v3, p0, LX/7S6;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1208131
    iget p4, v3, Lcom/facebook/videocodec/effects/model/ColorFilter;->c:F

    move v3, p4

    .line 1208132
    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v1

    const-string v2, "contrast"

    iget-object v3, p0, LX/7S6;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1208133
    iget p4, v3, Lcom/facebook/videocodec/effects/model/ColorFilter;->d:F

    move v3, p4

    .line 1208134
    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v1

    const-string v2, "hue"

    iget-object v3, p0, LX/7S6;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1208135
    iget p4, v3, Lcom/facebook/videocodec/effects/model/ColorFilter;->e:F

    move v3, p4

    .line 1208136
    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v1

    const-string v2, "hueColorize"

    iget-object v3, p0, LX/7S6;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1208137
    iget-boolean p4, v3, Lcom/facebook/videocodec/effects/model/ColorFilter;->f:Z

    move v3, p4

    .line 1208138
    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;Z)LX/5Pa;

    .line 1208139
    :cond_0
    const-string v1, "uSurfaceTransformMatrix"

    invoke-virtual {v0, v1, p1}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uVideoTransformMatrix"

    invoke-virtual {v0, v1, p2}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uSceneTransformMatrix"

    invoke-virtual {v0, v1, p3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, LX/7S6;->c:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1208140
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1208123
    iget-boolean v0, p0, LX/7S6;->g:Z

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1208120
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    if-eqz v0, :cond_0

    .line 1208121
    iget-object v0, p0, LX/7S6;->h:LX/6Js;

    sget-object v1, LX/7Sc;->COLOR_FILTER_CHANGE:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1208122
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1208119
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1208113
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1208114
    const-string v1, "filter_type"

    const-string v2, "color"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208115
    const-string v1, "filter_id"

    iget-object v2, p0, LX/7S6;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1208116
    iget-object p0, v2, Lcom/facebook/videocodec/effects/model/ColorFilter;->a:Ljava/lang/String;

    move-object v2, p0

    .line 1208117
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208118
    return-object v0
.end method
