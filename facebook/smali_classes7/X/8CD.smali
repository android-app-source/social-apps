.class public final LX/8CD;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/doodle/ColourIndicator;

.field private b:F

.field private c:F


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/doodle/ColourIndicator;)V
    .locals 1

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 1310473
    iput-object p1, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 1310474
    iput v0, p0, LX/8CD;->b:F

    .line 1310475
    iput v0, p0, LX/8CD;->c:F

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/messaging/doodle/ColourIndicator;B)V
    .locals 0

    .prologue
    .line 1310476
    invoke-direct {p0, p1}, LX/8CD;-><init>(Lcom/facebook/messaging/doodle/ColourIndicator;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 10

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 1310477
    iget-object v0, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget-boolean v0, v0, Lcom/facebook/messaging/doodle/ColourIndicator;->m:Z

    if-eqz v0, :cond_0

    .line 1310478
    const/16 v1, 0xff

    .line 1310479
    const/16 v0, 0xe6

    .line 1310480
    :goto_0
    iget-object v2, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget-object v2, v2, Lcom/facebook/messaging/doodle/ColourIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1310481
    iget-object v0, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget-object v0, v0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1310482
    iget-object v0, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->invalidate()V

    .line 1310483
    return-void

    .line 1310484
    :cond_0
    iget v0, p0, LX/8CD;->b:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget v0, p0, LX/8CD;->c:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 1310485
    :cond_1
    iget-object v0, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->getRight()I

    move-result v0

    iget-object v1, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v1}, Lcom/facebook/messaging/doodle/ColourIndicator;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget v1, v1, Lcom/facebook/messaging/doodle/ColourIndicator;->d:F

    sub-float/2addr v0, v1

    iput v0, p0, LX/8CD;->b:F

    .line 1310486
    iget-object v0, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget v0, v0, Lcom/facebook/messaging/doodle/ColourIndicator;->d:F

    iput v0, p0, LX/8CD;->c:F

    .line 1310487
    :cond_2
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1310488
    iget-object v1, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget-object v2, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget v2, v2, Lcom/facebook/messaging/doodle/ColourIndicator;->g:F

    mul-float/2addr v2, v0

    .line 1310489
    iput v2, v1, Lcom/facebook/messaging/doodle/ColourIndicator;->f:F

    .line 1310490
    iget-object v1, p0, LX/8CD;->a:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget v2, p0, LX/8CD;->c:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v0

    iget v4, p0, LX/8CD;->b:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1310491
    iput v2, v1, Lcom/facebook/messaging/doodle/ColourIndicator;->d:F

    .line 1310492
    float-to-double v2, v0

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    const-wide v4, 0x406fe00000000000L    # 255.0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 1310493
    float-to-double v2, v0

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    const-wide v4, 0x406cc00000000000L    # 230.0

    mul-double/2addr v2, v4

    double-to-int v0, v2

    goto :goto_0
.end method

.method public final d(LX/0wd;)V
    .locals 1

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 1310494
    invoke-super {p0, p1}, LX/0xh;->d(LX/0wd;)V

    .line 1310495
    iput v0, p0, LX/8CD;->b:F

    .line 1310496
    iput v0, p0, LX/8CD;->c:F

    .line 1310497
    return-void
.end method
