.class public final enum LX/6qJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6qJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6qJ;

.field public static final enum FINGERPRINT:LX/6qJ;

.field public static final enum NOT_REQUIRED:LX/6qJ;

.field public static final enum PIN:LX/6qJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1150348
    new-instance v0, LX/6qJ;

    const-string v1, "NOT_REQUIRED"

    invoke-direct {v0, v1, v2}, LX/6qJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qJ;->NOT_REQUIRED:LX/6qJ;

    .line 1150349
    new-instance v0, LX/6qJ;

    const-string v1, "PIN"

    invoke-direct {v0, v1, v3}, LX/6qJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qJ;->PIN:LX/6qJ;

    .line 1150350
    new-instance v0, LX/6qJ;

    const-string v1, "FINGERPRINT"

    invoke-direct {v0, v1, v4}, LX/6qJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qJ;->FINGERPRINT:LX/6qJ;

    .line 1150351
    const/4 v0, 0x3

    new-array v0, v0, [LX/6qJ;

    sget-object v1, LX/6qJ;->NOT_REQUIRED:LX/6qJ;

    aput-object v1, v0, v2

    sget-object v1, LX/6qJ;->PIN:LX/6qJ;

    aput-object v1, v0, v3

    sget-object v1, LX/6qJ;->FINGERPRINT:LX/6qJ;

    aput-object v1, v0, v4

    sput-object v0, LX/6qJ;->$VALUES:[LX/6qJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1150352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6qJ;
    .locals 1

    .prologue
    .line 1150353
    const-class v0, LX/6qJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6qJ;

    return-object v0
.end method

.method public static values()[LX/6qJ;
    .locals 1

    .prologue
    .line 1150354
    sget-object v0, LX/6qJ;->$VALUES:[LX/6qJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6qJ;

    return-object v0
.end method
