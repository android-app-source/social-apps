.class public final LX/7at;
.super LX/4th;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4th",
        "<",
        "LX/7as;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3K9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3K9",
            "<",
            "LX/7as;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/maps/GoogleMapOptions;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6an;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    invoke-direct {p0}, LX/4th;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7at;->e:Ljava/util/List;

    iput-object p1, p0, LX/7at;->b:Landroid/view/ViewGroup;

    iput-object p2, p0, LX/7at;->c:Landroid/content/Context;

    iput-object p3, p0, LX/7at;->d:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method

.method private f()V
    .locals 4

    iget-object v0, p0, LX/7at;->a:LX/3K9;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4th;->a:LX/4td;

    move-object v0, v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7at;->c:Landroid/content/Context;

    invoke-static {v0}, LX/7av;->a(Landroid/content/Context;)I

    iget-object v0, p0, LX/7at;->c:Landroid/content/Context;

    invoke-static {v0}, LX/7bP;->a(Landroid/content/Context;)LX/7bR;

    move-result-object v0

    iget-object v1, p0, LX/7at;->c:Landroid/content/Context;

    invoke-static {v1}, LX/1or;->a(Ljava/lang/Object;)LX/1ot;

    move-result-object v1

    iget-object v2, p0, LX/7at;->d:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v0, v1, v2}, LX/7bR;->a(LX/1ot;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/android/gms/maps/internal/IMapViewDelegate;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LX/7at;->a:LX/3K9;

    new-instance v2, LX/7as;

    iget-object v3, p0, LX/7at;->b:Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, LX/7as;-><init>(Landroid/view/ViewGroup;Lcom/google/android/gms/maps/internal/IMapViewDelegate;)V

    invoke-interface {v1, v2}, LX/3K9;->a(LX/4td;)V

    iget-object v0, p0, LX/7at;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6an;

    iget-object v1, p0, LX/4th;->a:LX/4td;

    move-object v1, v1

    check-cast v1, LX/7as;

    invoke-virtual {v1, v0}, LX/7as;->a(LX/6an;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/1of; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_2
    :try_start_1
    iget-object v0, p0, LX/7at;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/1of; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/3K9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3K9",
            "<",
            "LX/7as;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LX/7at;->a:LX/3K9;

    invoke-direct {p0}, LX/7at;->f()V

    return-void
.end method
