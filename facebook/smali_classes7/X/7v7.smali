.class public final enum LX/7v7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7v7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7v7;

.field public static final enum BIRTHDAYS_CLICK_CAMERA:LX/7v7;

.field public static final enum BIRTHDAYS_CLICK_COMPOSER_BUTTON:LX/7v7;

.field public static final enum BIRTHDAYS_CLICK_MESSENGER:LX/7v7;

.field public static final enum BIRTHDAYS_CLICK_PROFILE:LX/7v7;

.field public static final enum BIRTHDAYS_CLICK_ROW:LX/7v7;

.field public static final enum BIRTHDAYS_CLICK_SELFIE_CAM_BUTTON:LX/7v7;

.field public static final enum BIRTHDAYS_CLICK_STICKER_ICON:LX/7v7;

.field public static final enum BIRTHDAYS_ENTRY:LX/7v7;

.field public static final enum BIRTHDAYS_EVENTS_DASHBOARD_ENTRY:LX/7v7;

.field public static final enum BIRTHDAYS_FETCH_MORE_BIRTHDAYS:LX/7v7;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1273643
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_EVENTS_DASHBOARD_ENTRY"

    const-string v2, "birthday_events_dashboard_entry"

    invoke-direct {v0, v1, v4, v2}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_EVENTS_DASHBOARD_ENTRY:LX/7v7;

    .line 1273644
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_ENTRY"

    const-string v2, "birthday_view_entry"

    invoke-direct {v0, v1, v5, v2}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_ENTRY:LX/7v7;

    .line 1273645
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_CLICK_COMPOSER_BUTTON"

    const-string v2, "birthday_click_compose_button"

    invoke-direct {v0, v1, v6, v2}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_CLICK_COMPOSER_BUTTON:LX/7v7;

    .line 1273646
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_CLICK_SELFIE_CAM_BUTTON"

    const-string v2, "birthday_click_selfie_cam_button"

    invoke-direct {v0, v1, v7, v2}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_CLICK_SELFIE_CAM_BUTTON:LX/7v7;

    .line 1273647
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_CLICK_ROW"

    const-string v2, "birthday_click_row"

    invoke-direct {v0, v1, v8, v2}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_CLICK_ROW:LX/7v7;

    .line 1273648
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_FETCH_MORE_BIRTHDAYS"

    const/4 v2, 0x5

    const-string v3, "birthday_fetch_more_birthdays"

    invoke-direct {v0, v1, v2, v3}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_FETCH_MORE_BIRTHDAYS:LX/7v7;

    .line 1273649
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_CLICK_CAMERA"

    const/4 v2, 0x6

    const-string v3, "birthday_click_camera"

    invoke-direct {v0, v1, v2, v3}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_CLICK_CAMERA:LX/7v7;

    .line 1273650
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_CLICK_MESSENGER"

    const/4 v2, 0x7

    const-string v3, "birthday_click_messenger"

    invoke-direct {v0, v1, v2, v3}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_CLICK_MESSENGER:LX/7v7;

    .line 1273651
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_CLICK_PROFILE"

    const/16 v2, 0x8

    const-string v3, "birthday_click_profile"

    invoke-direct {v0, v1, v2, v3}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_CLICK_PROFILE:LX/7v7;

    .line 1273652
    new-instance v0, LX/7v7;

    const-string v1, "BIRTHDAYS_CLICK_STICKER_ICON"

    const/16 v2, 0x9

    const-string v3, "birthday_click_sticker_icon"

    invoke-direct {v0, v1, v2, v3}, LX/7v7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7v7;->BIRTHDAYS_CLICK_STICKER_ICON:LX/7v7;

    .line 1273653
    const/16 v0, 0xa

    new-array v0, v0, [LX/7v7;

    sget-object v1, LX/7v7;->BIRTHDAYS_EVENTS_DASHBOARD_ENTRY:LX/7v7;

    aput-object v1, v0, v4

    sget-object v1, LX/7v7;->BIRTHDAYS_ENTRY:LX/7v7;

    aput-object v1, v0, v5

    sget-object v1, LX/7v7;->BIRTHDAYS_CLICK_COMPOSER_BUTTON:LX/7v7;

    aput-object v1, v0, v6

    sget-object v1, LX/7v7;->BIRTHDAYS_CLICK_SELFIE_CAM_BUTTON:LX/7v7;

    aput-object v1, v0, v7

    sget-object v1, LX/7v7;->BIRTHDAYS_CLICK_ROW:LX/7v7;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7v7;->BIRTHDAYS_FETCH_MORE_BIRTHDAYS:LX/7v7;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7v7;->BIRTHDAYS_CLICK_CAMERA:LX/7v7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7v7;->BIRTHDAYS_CLICK_MESSENGER:LX/7v7;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7v7;->BIRTHDAYS_CLICK_PROFILE:LX/7v7;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7v7;->BIRTHDAYS_CLICK_STICKER_ICON:LX/7v7;

    aput-object v2, v0, v1

    sput-object v0, LX/7v7;->$VALUES:[LX/7v7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1273657
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1273658
    iput-object p3, p0, LX/7v7;->name:Ljava/lang/String;

    .line 1273659
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7v7;
    .locals 1

    .prologue
    .line 1273656
    const-class v0, LX/7v7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7v7;

    return-object v0
.end method

.method public static values()[LX/7v7;
    .locals 1

    .prologue
    .line 1273655
    sget-object v0, LX/7v7;->$VALUES:[LX/7v7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7v7;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1273654
    iget-object v0, p0, LX/7v7;->name:Ljava/lang/String;

    return-object v0
.end method
