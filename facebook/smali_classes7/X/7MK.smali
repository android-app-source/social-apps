.class public LX/7MK;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1198571
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1198572
    iput-object p1, p0, LX/7MK;->a:LX/0ad;

    .line 1198573
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1198570
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1198569
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1198559
    if-eqz p2, :cond_0

    instance-of v0, p2, Landroid/view/View;

    if-nez v0, :cond_1

    .line 1198560
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v1, p2

    .line 1198561
    check-cast v1, Landroid/view/View;

    .line 1198562
    new-instance v0, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v0, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1198563
    const/16 v3, 0xbb8

    .line 1198564
    iput v3, v0, LX/0hs;->t:I

    .line 1198565
    iget-object v3, p0, LX/7MK;->a:LX/0ad;

    sget-char v4, LX/34q;->b:C

    const v5, 0x7f080d49

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1198566
    sget-object v3, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v3}, LX/0ht;->a(LX/3AV;)V

    .line 1198567
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    move v3, v2

    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 1198568
    invoke-virtual {v0}, LX/0ht;->d()V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1198557
    const-string v0, "4542"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1198558
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHANNEL_FEED_SAVE_OVERLAY_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
