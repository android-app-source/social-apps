.class public LX/73z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/73y;


# static fields
.field public static a:I

.field private static b:I

.field private static final c:Ljava/util/regex/Pattern;

.field private static final d:Ljava/util/regex/Pattern;

.field private static final e:Ljava/util/regex/Pattern;

.field private static final f:Ljava/util/regex/Pattern;

.field private static final q:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final r:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final s:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final t:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final u:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public g:Ljava/lang/Exception;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Z

.field public l:Z

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:LX/73x;

.field private p:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 1167334
    const v0, 0x14d81e

    sput v0, LX/73z;->a:I

    .line 1167335
    const v0, 0x14d830

    sput v0, LX/73z;->b:I

    .line 1167336
    const-string v0, "\"is_transient\"[\\s]*:[\\s]*([truefalsTRUEFALS]+)\\W"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/73z;->c:Ljava/util/regex/Pattern;

    .line 1167337
    const-string v0, "\"code\"[\\s]*:[\\s]*(\\d+)\\W"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/73z;->d:Ljava/util/regex/Pattern;

    .line 1167338
    const-string v0, "\"error_code\"[\\s]*:[\\s]*(\\d+)\\W"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/73z;->e:Ljava/util/regex/Pattern;

    .line 1167339
    const-string v0, "\"error_subcode\"[\\s]*:[\\s]*(\\d+)\\W"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/73z;->f:Ljava/util/regex/Pattern;

    .line 1167340
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/73z;->q:LX/0Rf;

    .line 1167341
    const/16 v0, 0x170

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/73z;->r:LX/0Rf;

    .line 1167342
    const/16 v0, 0x12e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x133

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x134

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x198

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0x1f6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x1f7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Integer;

    const/16 v7, 0x1f8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/73z;->s:LX/0Rf;

    .line 1167343
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x14d81b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v3, LX/73z;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/73z;->t:LX/0Rf;

    .line 1167344
    const v0, 0x149604

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x14d7f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x14d813

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x14d822

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v4, LX/73z;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v5, 0x14dbe0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v6, 0x8

    new-array v6, v6, [Ljava/lang/Integer;

    const v7, 0x14ff19

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    const v7, 0x1558e9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const v7, 0x16339b

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    const/4 v7, 0x3

    const v8, 0x18857c

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const v8, 0x189112

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const v8, 0x189113

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const v8, 0x189117

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const v8, 0x164ef4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/73z;->u:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1167345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167346
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1167347
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/73z;-><init>(Ljava/lang/Exception;Z)V

    .line 1167348
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;Z)V
    .locals 0

    .prologue
    .line 1167349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167350
    iput-object p1, p0, LX/73z;->g:Ljava/lang/Exception;

    .line 1167351
    iput-boolean p2, p0, LX/73z;->p:Z

    .line 1167352
    invoke-virtual {p0, p1}, LX/73z;->a(Ljava/lang/Exception;)V

    .line 1167353
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1167354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167355
    iput-boolean p1, p0, LX/73z;->p:Z

    .line 1167356
    return-void
.end method

.method private static a(Ljava/lang/String;I)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1167357
    sget-object v0, LX/73z;->d:Ljava/util/regex/Pattern;

    invoke-static {p0, v0, p1}, LX/73z;->a(Ljava/lang/String;Ljava/util/regex/Pattern;I)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/regex/Pattern;I)I
    .locals 4

    .prologue
    .line 1167358
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1167359
    invoke-virtual {v2, p2}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1167360
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 1167361
    if-eqz v2, :cond_0

    .line 1167362
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1167363
    :goto_0
    move-wide v0, v2

    .line 1167364
    long-to-int v0, v0

    return v0

    :catch_0
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1167365
    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p0

    move v2, p2

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;I)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1167366
    sget-object v0, LX/73z;->e:Ljava/util/regex/Pattern;

    invoke-static {p0, v0, p1}, LX/73z;->a(Ljava/lang/String;Ljava/util/regex/Pattern;I)I

    move-result v0

    return v0
.end method

.method private static c(Ljava/lang/String;I)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1167323
    sget-object v0, LX/73z;->f:Ljava/util/regex/Pattern;

    invoke-static {p0, v0, p1}, LX/73z;->a(Ljava/lang/String;Ljava/util/regex/Pattern;I)I

    move-result v0

    return v0
.end method

.method private static d(Ljava/lang/String;I)LX/03R;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1167324
    sget-object v0, LX/73z;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1167325
    invoke-virtual {v0, p1}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1167326
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 1167327
    if-eqz v0, :cond_1

    .line 1167328
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1167329
    sget-object v0, LX/03R;->YES:LX/03R;

    .line 1167330
    :goto_0
    return-object v0

    .line 1167331
    :cond_0
    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1167332
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 1167333
    :cond_1
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 1167322
    iget-object v0, p0, LX/73z;->g:Ljava/lang/Exception;

    return-object v0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 1167190
    iput-object p1, p0, LX/73z;->g:Ljava/lang/Exception;

    .line 1167191
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_8

    move-object v0, p1

    .line 1167192
    check-cast v0, LX/2Oo;

    .line 1167193
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v3

    .line 1167194
    if-eqz v3, :cond_1

    .line 1167195
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    const-string v4, "<no error message>"

    .line 1167196
    if-eqz v0, :cond_24

    :goto_0
    move-object v0, v0

    .line 1167197
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v4

    .line 1167198
    if-eqz v4, :cond_0

    .line 1167199
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1167200
    :cond_0
    iput-object v0, p0, LX/73z;->h:Ljava/lang/String;

    .line 1167201
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    iput v0, p0, LX/73z;->i:I

    .line 1167202
    sget-object v0, LX/73z;->r:LX/0Rf;

    iget v4, p0, LX/73z;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1167203
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    .line 1167204
    :goto_1
    iput v6, p0, LX/73z;->j:I

    .line 1167205
    iput-boolean v2, p0, LX/73z;->k:Z

    .line 1167206
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167207
    instance-of v1, p1, LX/4cm;

    if-eqz v1, :cond_3

    .line 1167208
    iput-boolean v2, p0, LX/73z;->m:Z

    .line 1167209
    :goto_2
    iget-boolean v1, p0, LX/73z;->m:Z

    if-eqz v1, :cond_4

    .line 1167210
    sget-object v1, LX/73x;->TRANSIENT_ERROR:LX/73x;

    iput-object v1, p0, LX/73z;->o:LX/73x;

    move-object v1, v0

    .line 1167211
    :goto_3
    iput-object v1, p0, LX/73z;->n:Ljava/lang/String;

    .line 1167212
    return-void

    .line 1167213
    :cond_1
    const-string v0, "<no error result>"

    iput-object v0, p0, LX/73z;->h:Ljava/lang/String;

    .line 1167214
    iput v6, p0, LX/73z;->i:I

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 1167215
    :cond_3
    sget-object v1, LX/73z;->q:LX/0Rf;

    iget v2, p0, LX/73z;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, LX/73z;->m:Z

    goto :goto_2

    .line 1167216
    :cond_4
    iget v1, p0, LX/73z;->i:I

    const/16 v2, 0x78

    if-ne v1, v2, :cond_5

    .line 1167217
    sget-object v1, LX/73x;->CANT_UPLOAD_TO_ALBUM:LX/73x;

    iput-object v1, p0, LX/73z;->o:LX/73x;

    move-object v1, v0

    goto :goto_3

    .line 1167218
    :cond_5
    iget v1, p0, LX/73z;->i:I

    const/16 v2, 0x7a

    if-ne v1, v2, :cond_6

    .line 1167219
    sget-object v1, LX/73x;->CANT_UPLOAD_ALBUM_PHOTO:LX/73x;

    iput-object v1, p0, LX/73z;->o:LX/73x;

    move-object v1, v0

    goto :goto_3

    .line 1167220
    :cond_6
    if-eqz v0, :cond_7

    .line 1167221
    sget-object v1, LX/73x;->PERMANENT_API_ERROR_WITH_VALID_MESSAGE:LX/73x;

    iput-object v1, p0, LX/73z;->o:LX/73x;

    move-object v1, v0

    goto :goto_3

    .line 1167222
    :cond_7
    sget-object v1, LX/73x;->PERMANENT_ERROR:LX/73x;

    iput-object v1, p0, LX/73z;->o:LX/73x;

    move-object v1, v0

    .line 1167223
    goto :goto_3

    .line 1167224
    :cond_8
    iput-boolean v7, p0, LX/73z;->k:Z

    .line 1167225
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, LX/1Bz;->getStackTraceAsString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    iput-object v0, p0, LX/73z;->h:Ljava/lang/String;

    .line 1167226
    instance-of v0, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_17

    .line 1167227
    check-cast p1, Lorg/apache/http/client/HttpResponseException;

    .line 1167228
    invoke-virtual {p1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    iput v0, p0, LX/73z;->j:I

    .line 1167229
    iget-object v0, p0, LX/73z;->h:Ljava/lang/String;

    const/4 v4, -0x1

    .line 1167230
    if-nez v0, :cond_25

    move v3, v4

    .line 1167231
    :cond_9
    :goto_5
    move v3, v3

    .line 1167232
    if-eq v3, v6, :cond_a

    iget-object v0, p0, LX/73z;->h:Ljava/lang/String;

    const-string v4, "<html>"

    invoke-static {v0, v4, v3}, LX/73z;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, LX/73z;->h:Ljava/lang/String;

    const-string v4, "<!DOCTYPE "

    invoke-static {v0, v4, v3}, LX/73z;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_a
    move v0, v2

    .line 1167233
    :goto_6
    if-nez v0, :cond_23

    .line 1167234
    iget-object v4, p0, LX/73z;->h:Ljava/lang/String;

    invoke-static {v4, v3}, LX/73z;->a(Ljava/lang/String;I)I

    move-result v5

    .line 1167235
    if-ltz v5, :cond_e

    .line 1167236
    iget-object v4, p0, LX/73z;->h:Ljava/lang/String;

    invoke-static {v4, v3}, LX/73z;->c(Ljava/lang/String;I)I

    move-result v4

    .line 1167237
    :goto_7
    if-gez v5, :cond_b

    move v0, v2

    .line 1167238
    :cond_b
    iget-object v8, p0, LX/73z;->h:Ljava/lang/String;

    invoke-static {v8, v3}, LX/73z;->d(Ljava/lang/String;I)LX/03R;

    move-result-object v3

    move v9, v0

    move v0, v5

    move-object v5, v3

    move v3, v4

    move v4, v9

    .line 1167239
    :goto_8
    if-eqz v4, :cond_f

    .line 1167240
    iput-boolean v2, p0, LX/73z;->l:Z

    .line 1167241
    iput-boolean v2, p0, LX/73z;->m:Z

    .line 1167242
    iput v6, p0, LX/73z;->i:I

    .line 1167243
    sget-object v0, LX/73x;->NETWORK_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167244
    :cond_c
    const-string v0, "<no error result>"

    goto :goto_4

    :cond_d
    move v0, v7

    .line 1167245
    goto :goto_6

    .line 1167246
    :cond_e
    iget-object v4, p0, LX/73z;->h:Ljava/lang/String;

    invoke-static {v4, v3}, LX/73z;->b(Ljava/lang/String;I)I

    move-result v5

    move v4, v6

    goto :goto_7

    .line 1167247
    :cond_f
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167248
    sget-object v4, LX/03R;->UNSET:LX/03R;

    if-eq v5, v4, :cond_10

    .line 1167249
    iput v0, p0, LX/73z;->i:I

    .line 1167250
    invoke-virtual {v5}, LX/03R;->asBoolean()Z

    move-result v0

    iput-boolean v0, p0, LX/73z;->m:Z

    .line 1167251
    :goto_9
    iget-boolean v0, p0, LX/73z;->m:Z

    if-eqz v0, :cond_15

    .line 1167252
    sget-object v0, LX/73x;->TRANSIENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167253
    :cond_10
    iget v4, p0, LX/73z;->j:I

    const/16 v5, 0x1f4

    if-ne v4, v5, :cond_14

    .line 1167254
    iput v0, p0, LX/73z;->i:I

    .line 1167255
    if-ne v3, v6, :cond_11

    .line 1167256
    :goto_a
    sget-object v3, LX/73z;->t:LX/0Rf;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1167257
    iput-boolean v2, p0, LX/73z;->m:Z

    goto :goto_9

    :cond_11
    move v0, v3

    .line 1167258
    goto :goto_a

    .line 1167259
    :cond_12
    sget-object v2, LX/73z;->u:LX/0Rf;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1167260
    iput-boolean v7, p0, LX/73z;->m:Z

    goto :goto_9

    .line 1167261
    :cond_13
    iget-object v0, p0, LX/73z;->h:Ljava/lang/String;

    const-string v2, "Internal Server Error"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/73z;->m:Z

    goto :goto_9

    .line 1167262
    :cond_14
    iput v6, p0, LX/73z;->i:I

    .line 1167263
    sget-object v0, LX/73z;->s:LX/0Rf;

    iget v2, p0, LX/73z;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/73z;->m:Z

    goto :goto_9

    .line 1167264
    :cond_15
    iget v0, p0, LX/73z;->i:I

    sget v2, LX/73z;->b:I

    if-ne v0, v2, :cond_16

    .line 1167265
    sget-object v0, LX/73x;->PERMANENT_CONTENT_REJECTED_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167266
    :cond_16
    sget-object v0, LX/73x;->PERMANENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167267
    :cond_17
    iput v6, p0, LX/73z;->j:I

    .line 1167268
    iput v6, p0, LX/73z;->i:I

    .line 1167269
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 1167270
    instance-of v3, p1, LX/740;

    if-eqz v3, :cond_1a

    .line 1167271
    check-cast p1, LX/740;

    .line 1167272
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167273
    iget-boolean v0, p1, LX/740;->mRetryMightWork:Z

    move v0, v0

    .line 1167274
    iput-boolean v0, p0, LX/73z;->m:Z

    .line 1167275
    iget-object v0, p1, LX/740;->mDiagnostic:LX/73x;

    move-object v0, v0

    .line 1167276
    if-eqz v0, :cond_18

    .line 1167277
    iget-object v0, p1, LX/740;->mDiagnostic:LX/73x;

    move-object v0, v0

    .line 1167278
    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167279
    :cond_18
    iget-boolean v0, p0, LX/73z;->m:Z

    if-eqz v0, :cond_19

    .line 1167280
    sget-object v0, LX/73x;->TRANSIENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167281
    :cond_19
    sget-object v0, LX/73x;->PERMANENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167282
    :cond_1a
    instance-of v3, p1, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;

    if-eqz v3, :cond_1c

    .line 1167283
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167284
    check-cast p1, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;

    .line 1167285
    iget-boolean v0, p1, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;->mRetryMightWork:Z

    move v0, v0

    .line 1167286
    iput-boolean v0, p0, LX/73z;->m:Z

    .line 1167287
    iget-boolean v0, p0, LX/73z;->m:Z

    if-eqz v0, :cond_1b

    .line 1167288
    sget-object v0, LX/73x;->TRANSIENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167289
    :cond_1b
    sget-object v0, LX/73x;->PERMANENT_PROCESSING_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167290
    :cond_1c
    const-class v3, Ljava/io/FileNotFoundException;

    if-ne v0, v3, :cond_1d

    .line 1167291
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167292
    iput-boolean v7, p0, LX/73z;->m:Z

    .line 1167293
    sget-object v0, LX/73x;->PERMANENT_PROCESSING_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167294
    :cond_1d
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 1167295
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1167296
    const-string v4, "org.apache.http."

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1e

    const-string v4, "javax.net."

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1e

    const-string v4, "java.net."

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1e

    instance-of v3, p1, Ljava/io/IOException;

    if-nez v3, :cond_1e

    instance-of v3, p1, Ljava/lang/SecurityException;

    if-eqz v3, :cond_2b

    :cond_1e
    const/4 v3, 0x1

    :goto_b
    move v3, v3

    .line 1167297
    if-eqz v3, :cond_1f

    .line 1167298
    iput-boolean v2, p0, LX/73z;->l:Z

    .line 1167299
    iput-boolean v2, p0, LX/73z;->m:Z

    .line 1167300
    sget-object v0, LX/73x;->NETWORK_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167301
    :cond_1f
    const-class v3, LX/4cl;

    if-ne v0, v3, :cond_20

    .line 1167302
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167303
    iput-boolean v7, p0, LX/73z;->m:Z

    .line 1167304
    sget-object v0, LX/73x;->PERMANENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167305
    :cond_20
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_21

    .line 1167306
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167307
    iput-boolean v7, p0, LX/73z;->m:Z

    .line 1167308
    sget-object v0, LX/73x;->PERMANENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167309
    :cond_21
    instance-of v0, p1, LX/74H;

    if-eqz v0, :cond_22

    .line 1167310
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167311
    iput-boolean v7, p0, LX/73z;->m:Z

    .line 1167312
    sget-object v0, LX/73x;->PERMANENT_VIDEO_PROCESSING_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    .line 1167313
    :cond_22
    iput-boolean v7, p0, LX/73z;->l:Z

    .line 1167314
    iput-boolean v2, p0, LX/73z;->m:Z

    .line 1167315
    sget-object v0, LX/73x;->TRANSIENT_ERROR:LX/73x;

    iput-object v0, p0, LX/73z;->o:LX/73x;

    goto/16 :goto_3

    :cond_23
    move v4, v0

    move-object v5, v1

    move v3, v6

    move v0, v7

    goto/16 :goto_8

    :cond_24
    move-object v0, v4

    goto/16 :goto_0

    .line 1167316
    :cond_25
    const/4 v3, 0x0

    .line 1167317
    :goto_c
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_2a

    .line 1167318
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 1167319
    const/16 v8, 0x61

    if-lt v5, v8, :cond_26

    const/16 v8, 0x7a

    if-le v5, v8, :cond_29

    :cond_26
    const/16 v8, 0x41

    if-lt v5, v8, :cond_27

    const/16 v8, 0x5a

    if-le v5, v8, :cond_29

    :cond_27
    const/16 v8, 0x30

    if-lt v5, v8, :cond_28

    const/16 v8, 0x39

    if-le v5, v8, :cond_29

    :cond_28
    const/16 v8, 0x20

    if-ne v5, v8, :cond_9

    .line 1167320
    :cond_29
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_2a
    move v3, v4

    .line 1167321
    goto/16 :goto_5

    :cond_2b
    const/4 v3, 0x0

    goto :goto_b
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167189
    iget-boolean v0, p0, LX/73z;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/73z;->g:Ljava/lang/Exception;

    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167188
    iget-object v0, p0, LX/73z;->g:Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167187
    iget-object v0, p0, LX/73z;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1167186
    iget v0, p0, LX/73z;->i:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1167185
    iget v0, p0, LX/73z;->j:I

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1167181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1167182
    const-string v1, "Network:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/73z;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1167183
    const-string v1, ";Retry:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/73z;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1167184
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1167180
    iget-boolean v0, p0, LX/73z;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1167179
    iget-boolean v0, p0, LX/73z;->k:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/73z;->i:I

    sget v1, LX/73z;->a:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
