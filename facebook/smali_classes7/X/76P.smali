.class public LX/76P;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/76P;


# instance fields
.field public final a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171009
    iput-object p1, p0, LX/76P;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    .line 1171010
    return-void
.end method

.method public static a(LX/0QB;)LX/76P;
    .locals 4

    .prologue
    .line 1171011
    sget-object v0, LX/76P;->b:LX/76P;

    if-nez v0, :cond_1

    .line 1171012
    const-class v1, LX/76P;

    monitor-enter v1

    .line 1171013
    :try_start_0
    sget-object v0, LX/76P;->b:LX/76P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171014
    if-eqz v2, :cond_0

    .line 1171015
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171016
    new-instance p0, LX/76P;

    invoke-static {v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v3

    check-cast v3, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-direct {p0, v3}, LX/76P;-><init>(Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;)V

    .line 1171017
    move-object v0, p0

    .line 1171018
    sput-object v0, LX/76P;->b:LX/76P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171019
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171020
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171021
    :cond_1
    sget-object v0, LX/76P;->b:LX/76P;

    return-object v0

    .line 1171022
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171023
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
