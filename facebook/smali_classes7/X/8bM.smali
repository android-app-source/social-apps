.class public LX/8bM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8bM;


# instance fields
.field public a:Landroid/app/Activity;

.field public b:I

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/richdocument/utils/HostingActivityStateMonitor$HostingActivityStateListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1371991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1371992
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/8bM;->c:Ljava/util/Set;

    .line 1371993
    return-void
.end method

.method public static a(LX/0QB;)LX/8bM;
    .locals 3

    .prologue
    .line 1371979
    sget-object v0, LX/8bM;->d:LX/8bM;

    if-nez v0, :cond_1

    .line 1371980
    const-class v1, LX/8bM;

    monitor-enter v1

    .line 1371981
    :try_start_0
    sget-object v0, LX/8bM;->d:LX/8bM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1371982
    if-eqz v2, :cond_0

    .line 1371983
    :try_start_1
    new-instance v0, LX/8bM;

    invoke-direct {v0}, LX/8bM;-><init>()V

    .line 1371984
    move-object v0, v0

    .line 1371985
    sput-object v0, LX/8bM;->d:LX/8bM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1371986
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1371987
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1371988
    :cond_1
    sget-object v0, LX/8bM;->d:LX/8bM;

    return-object v0

    .line 1371989
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1371990
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1371971
    iget-object v0, p0, LX/8bM;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClD;

    .line 1371972
    iget v2, p0, LX/8bM;->b:I

    .line 1371973
    if-nez v2, :cond_2

    iget-object v3, v0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1371974
    invoke-static {v0}, LX/ClD;->j(LX/ClD;)V

    .line 1371975
    :cond_0
    :goto_1
    goto :goto_0

    .line 1371976
    :cond_1
    return-void

    .line 1371977
    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v3, v0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1371978
    invoke-static {v0}, LX/ClD;->k(LX/ClD;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1371966
    if-eqz p1, :cond_0

    .line 1371967
    iput-object p1, p0, LX/8bM;->a:Landroid/app/Activity;

    .line 1371968
    iget-object v0, p0, LX/8bM;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1371969
    const/4 v0, 0x1

    iput v0, p0, LX/8bM;->b:I

    .line 1371970
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1371962
    iget-object v0, p0, LX/8bM;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1371963
    iget-object v0, p0, LX/8bM;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1371964
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/8bM;->a:Landroid/app/Activity;

    .line 1371965
    return-void
.end method

.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1371961
    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1371960
    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1371956
    iget-object v0, p0, LX/8bM;->a:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 1371957
    const/4 v0, 0x0

    iput v0, p0, LX/8bM;->b:I

    .line 1371958
    invoke-direct {p0}, LX/8bM;->d()V

    .line 1371959
    :cond_0
    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1371952
    iget-object v0, p0, LX/8bM;->a:Landroid/app/Activity;

    if-ne p1, v0, :cond_0

    .line 1371953
    const/4 v0, 0x1

    iput v0, p0, LX/8bM;->b:I

    .line 1371954
    invoke-direct {p0}, LX/8bM;->d()V

    .line 1371955
    :cond_0
    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1371949
    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1371951
    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1371950
    return-void
.end method
