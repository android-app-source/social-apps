.class public LX/6zc;
.super LX/6E7;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E7;",
        "LX/6vq",
        "<",
        "LX/701;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/701;

.field public c:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

.field public d:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1161274
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1161275
    const-class v0, LX/6zc;

    invoke-static {v0, p0}, LX/6zc;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1161276
    const v0, 0x7f030f00

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1161277
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/6zc;->setOrientation(I)V

    .line 1161278
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/6zc;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1161279
    invoke-virtual {p0}, LX/6zc;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0571

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1161280
    invoke-virtual {p0, v0, v0, v0, v0}, LX/6zc;->setPadding(IIII)V

    .line 1161281
    const v0, 0x7f0d2495

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    iput-object v0, p0, LX/6zc;->c:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    .line 1161282
    const v0, 0x7f0d0b20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/6zc;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1161283
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/6zc;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object p0

    check-cast p0, LX/6xb;

    iput-object p0, p1, LX/6zc;->a:LX/6xb;

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 3

    .prologue
    .line 1161264
    iget-object v0, p0, LX/6zc;->a:LX/6xb;

    iget-object v1, p0, LX/6zc;->b:LX/701;

    iget-object v1, v1, LX/701;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, LX/6zc;->b:LX/701;

    iget-object v2, v2, LX/701;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v2}, LX/6zU;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6xb;->b(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V

    .line 1161265
    iget-object v0, p0, LX/6zc;->a:LX/6xb;

    iget-object v1, p0, LX/6zc;->b:LX/701;

    iget-object v1, v1, LX/701;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, LX/6zc;->b:LX/701;

    iget-object v2, v2, LX/701;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V

    .line 1161266
    iget-object v0, p0, LX/6zc;->b:LX/701;

    iget-object v0, v0, LX/701;->c:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 1161267
    iget-object v0, p0, LX/6zc;->b:LX/701;

    iget-object v0, v0, LX/701;->c:Landroid/content/Intent;

    iget-object v1, p0, LX/6zc;->b:LX/701;

    iget v1, v1, LX/701;->d:I

    invoke-virtual {p0, v0, v1}, LX/6E7;->a(Landroid/content/Intent;I)V

    .line 1161268
    :goto_0
    return-void

    .line 1161269
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1161270
    const-string v1, "extra_user_action"

    iget-object v2, p0, LX/6zc;->b:LX/701;

    iget-object v2, v2, LX/701;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161271
    const-string v1, "extra_section_type"

    sget-object v2, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1161272
    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->USER_ACTION:LX/73S;

    invoke-direct {v1, v2, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1161273
    invoke-virtual {p0, v1}, LX/6E7;->a(LX/73T;)V

    goto :goto_0
.end method
