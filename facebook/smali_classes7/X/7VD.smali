.class public LX/7VD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/63k;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7VD;


# instance fields
.field private final a:LX/1oy;


# direct methods
.method public constructor <init>(LX/1oy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1214129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1214130
    iput-object p1, p0, LX/7VD;->a:LX/1oy;

    .line 1214131
    return-void
.end method

.method public static a(LX/0QB;)LX/7VD;
    .locals 4

    .prologue
    .line 1214132
    sget-object v0, LX/7VD;->b:LX/7VD;

    if-nez v0, :cond_1

    .line 1214133
    const-class v1, LX/7VD;

    monitor-enter v1

    .line 1214134
    :try_start_0
    sget-object v0, LX/7VD;->b:LX/7VD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1214135
    if-eqz v2, :cond_0

    .line 1214136
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1214137
    new-instance p0, LX/7VD;

    invoke-static {v0}, LX/1ow;->c(LX/0QB;)LX/1ow;

    move-result-object v3

    check-cast v3, LX/1oy;

    invoke-direct {p0, v3}, LX/7VD;-><init>(LX/1oy;)V

    .line 1214138
    move-object v0, p0

    .line 1214139
    sput-object v0, LX/7VD;->b:LX/7VD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1214140
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1214141
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1214142
    :cond_1
    sget-object v0, LX/7VD;->b:LX/7VD;

    return-object v0

    .line 1214143
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1214144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1214145
    iget-object v0, p0, LX/7VD;->a:LX/1oy;

    invoke-interface {v0, p1}, LX/1oy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
